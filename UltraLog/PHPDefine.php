<?php
define("REG_LONG",1);
define("REG_STRING",2);
define("REG_KEY_ITEMS",3);

define("APP_LOGEXP_ID",1);
define("APP_DBMAKER_ID",2);
define("APP_BACKUP_ID",3);
define("APP_DELETE_ID",4);
define("APP_ULTRADOG_ID",5);
define("APP_SMDR_ID",6);
define("APP_REC_SYN_ID",11);
define("APP_RECORD_ID",12);
define("APP_PHP_ID",41);

// AP to AP
define("CMD_ALIVE","Alive");
define("CMD_ERROR","Error");
define("CMD_END","End");
define("CMD_REBOOT","Reboot");
define("CMD_READ_KEY_SERIAL","ReadKey");
define("CMD_START_RECORD","StartRecord");

define("CMD_STOP_RECORD","StopRecord");
define("CMD_CHANNEL_START","ChannelStart");
define("CMD_CHANNEL_STOP","ChannelStop");
define("CMD_READ_REG","ReadReg");
define("CMD_EANBLE_BACKUP_DEVICE","EnableBackupDevice");
define("CMD_DISABLE_BACKUP_DEVICE","DisableBackupDevice");
define("CMD_EANBLE_SUBBACKUP_DEVICE","EnableSubBackupDevice");
define("CMD_DISABLE_SUBBACKUP_DEVICE","DisableSubBackupDevice");
define("CMD_BACKUP_RESTART","BackupRestart");
define("CMD_SUBBACKUP_RESTART","SubBackupRestart");
define("CMD_MANUAL_BACKUP","ManualBackup");
define("CMD_MANUAL_DELETE","ManualDelete");
define("CMD_CHANNEL_RESET","ChannelReset");
define("CMD_BACKUP_READY","BackupReady");
define("CMD_DISK_READY","DiskReady");
define("CMD_ACK","Command_Ack");
define("CMD_ABORT_JOB","AbortJob");
define("CMD_RESUME_JOB","ResumJob");
define("CMD_STOP_SPEAKER","StopSpeaker");
define("CMD_SET_ALARM","SetAlarm");

define("PARM_KEY_SYSTEM","KeySystem");
define("PARM_KEY_INFORMATION","KeyInformation");
define("PARM_KEY_CONFIG","KeyConfig");
define("PARM_KEY_SCHEDULE","KeySchedule");
define("PARM_KEY_OFFICE_TIME","KeyOfficeTime");
define("PARM_KEY_HOLIDAY","KeyHoliday");
define("PARM_KEY_SMDR","KeySMDR");
define("PARM_KEY_SELECT_RECORD","KeySelectRecord");
define("PARM_KEY_MAILCALLOUT","KeyMailCallOut");

define("PARM_SECTION_SYSTEM","SectionSystem");
define("PARM_SECTION_DELETE","SectionDelete");
define("PARM_SECTION_BACKUP","SectionBackup");
define("PARM_SECTION_CID","SectionCID");
define("PARM_SECTION_DATABASE","SectionDatabase");
define("PARM_SECTION_INSTALL","SectionInstall");
define("PARM_SECTION_RECORD_CTRL","SectionRecordCtrl");
define("PARM_SECTION_DOG","SectionDog");

define("PARM_SECTION_AGENT_NAME","SectionAgentName");
define("PARM_SECTION_AGENT_ID","SectionAgentID");
define("PARM_SECTION_ACD","SectionACD");
define("PARM_SECTION_DEPARTMENT","SectionDepartment");
define("PARM_SECTION_CHANNEL_NUMBER","SectionChannelNumber");
define("PARM_SECTION_CHANNEL_TYPE","SectionChannelType");

define("PARM_SECTION_REC","SectionRec");
define("PARM_SECTION_FORMAT","SectionFormat");
define("PARM_SECTION_AGC","SectionAGC");
define("PARM_SECTION_GAIN","SectionGain");
define("PARM_SECTION_VOLTAGE","SectionVoltage");
define("PARM_SECTION_VOICESEN","SectionVoiceSen");
define("PARM_SECTION_CIDSTYLE","SectionCIDStyle");
define("PARM_SECTION_LINE_CHECK","SectionLineCheck");

define("PARM_SECTION_MAIL_DATA","SectionMailData");
define("PARM_SECTION_LEVEL_ADDR","SectionLevelAddr");

define("PARM_CT_ALARM","CTAlarm");
define("PARM_DP_ALARM","DPAlarm");
define("PARM_NGX_ALARM","NGXAlarm");

define("ALIVE_INTERVAL",20);

define("APP_DEAD",-1);
define("APP_UNKNOW",0);
define("APP_ALIVE",1);

// Function Bit
define("FUN_ADVANCE_SEARCH_BACKUP",2147483648);
define("FUN_ADVANCE_STATIC_EXPORT",1073741824);
define("FUN_ADVANCE_CUSTOMIZATION",536870912);
define("FUN_ADVANCE_VERIFY_MD5",32768);
define("FUN_ADVANCE_FULL_SMDR",16384);

define("FUN_CTI_ENABLE",2147483648);
define("FUN_CTI_DISPLAY",1073741824);
define("FUN_CTI_STATIC_AGENT",536870912);
define("FUN_CTI_STATIC_IVR",268435456);
define("FUN_CTI_STATIC_EXTENSION",134217728);
define("FUN_CTI_STATIC_TRACE",67108864);
define("FUN_CTI_STATIC_ONWORK",33554432);
define("FUN_CTI_STATIC_DUTYWORK",16777216);
define("FUN_CTI_STATIC_OFFWORK",8388608);
define("FUN_CTI_STATIC_PUTTHROUGH",4194304);
define("FUN_CTI_STATIC_WORKEFFICIENCY",2097152);
define("FUN_CTI_STATIC_CALLRATE",1048576);
define("FUN_CTI_STATIC_TRAFFIC",524288);
define("FUN_CTI_STATIC_MISSPICKUP",262144);
define("FUN_CTI_STATIC_CALLLOSS",131072);
define("FUN_CTI_STATIC_QUEUECALLLOSS",65536);
define("FUN_CTI_STATIC_IVRCALLLOSS",32768);
define("FUN_CTI_STATIC_SEATCALLLOSS",16384);
define("FUN_CTI_STATIC_TRAFFICLOSS",8192);
define("FUN_CTI_STATIC_SEATSTATE",4096);
define("FUN_CTI_STATIC_SEATSATISFACTION",2048);
define("FUN_CTI_STATIC_ATTENDANCE",1024);
define("FUN_CTI_STATIC_AGENTSTATE",512);
define("FUN_CTI_STATIC_SEATTRAFFIC",256);
define("FUN_CTI_STATIC_SEATCASE",128);
define("FUN_CTI_STATIC_INBOUND",64);
define("FUN_CTI_STATIC_OUTBOUND",32);
define("FUN_CTI_STATIC_OUTTRANS",16);
define("FUN_CTI_STATIC_INFLOW",8);
define("FUN_CTI_STATIC_INFLOWS2",4);

define("FUN_CTI_STATIC_ALONE",1);

define("FUN_EXPAND_FIELD1",2147483648);
define("FUN_EXPAND_FIELD2",1073741824);
define("FUN_EXPAND_FIELD3",536870912);
define("FUN_EXPAND_FIELD4",268435456);
define("FUN_EXPAND_FIELD5",134217728);
define("FUN_EXPAND_FIELD6",67108864);
define("FUN_EXPAND_FIELD7",33554432);
define("FUN_EXPAND_FIELD8",16777216);
define("FUN_EXPAND_FIELD9",8388608);

define("FUN_EXTRA_FIELD1",2147483648);
define("FUN_EXTRA_FIELD2",1073741824);
define("FUN_EXTRA_FIELD3",536870912);
define("FUN_EXTRA_FIELD4",268435456);
define("FUN_EXTRA_FIELD5",134217728);
define("FUN_EXTRA_FIELD6",67108864);
define("FUN_EXTRA_FIELD7",33554432);
define("FUN_EXTRA_FIELD8",16777216);
define("FUN_EXTRA_FIELD9",8388608);
define("FUN_EXTRA_FIELD10",4194304);
define("FUN_EXTRA_FIELD11",2097152);
define("FUN_EXTRA_FIELD12",1048576);
define("FUN_EXTRA_FIELD13",524288);
define("FUN_EXTRA_FIELD14",262144);
define("FUN_EXTRA_FIELD15",131072);
define("FUN_EXTRA_FIELD16",65536);
define("FUN_EXTRA_FIELD17",32768);
define("FUN_EXTRA_FIELD18",16384);
define("FUN_EXTRA_FIELD19",8192);
define("FUN_EXTRA_FIELD20",4096);
define("FUN_EXTRA_FIELD21",2048);
define("FUN_EXTRA_FIELD22",1024);
define("FUN_EXTRA_FIELD23",512);
define("FUN_EXTRA_FIELD24",256);
define("FUN_EXTRA_FIELD25",128);
define("FUN_EXTRA_FIELD26",64);
define("FUN_EXTRA_FIELD27",32);
define("FUN_EXTRA_FIELD28",16);
define("FUN_EXTRA_FIELD29",8);
define("FUN_EXTRA_FIELD30",4);
define("FUN_EXTRA_FIELD31",2);
define("FUN_EXTRA_FIELD32",1);
?>