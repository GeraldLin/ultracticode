var ULTRALOGPATH = "";

var l = "";
var agentid = "";

//Request PHP & CallBack, (strData, strFun)
function RequestPHP() {
	var a = RequestPHP.arguments, c = a.length;

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			if (c >= 2) {
				var s = XMLHttpRequestObject.responseText;
				s = "\""+ s.replace(/^\s+|\s+$/g,"") +"\"";
				for(var i = 2; i < c; i++) {
					s += ", \""+a[i]+"\"";
				}
				evil(a[1]+"("+s+")");
			}
		}
	}
	XMLHttpRequestObject.send(null);
}

//Request PHP & CallBack, (strData, strFun)
function RequestPHP_RET() {
	var a = RequestPHP_RET.arguments, c = a.length;

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (isIE) {
		XMLHttpRequestObject.open("POST", a[0], false);
	}
	else {
		XMLHttpRequestObject.open("POST", a[0], false);
	}
	XMLHttpRequestObject.send(null);
	var s = XMLHttpRequestObject.responseText;
	XMLHttpRequestObject = null;
	delete XMLHttpRequestObject;

	if (c >= 2) {
		s = "\""+ s.replace(/^\s+|\s+$/g,"") +"\"";
		for(var i = 2; i < c; i++) {
			s += ", \""+a[i]+"\"";
		}
		evil(a[1]+"("+s+")");
	}
}

//Request PHP & CallBack, (strData)
function RequestPHP_RUN() {
	var a = RequestPHP_RUN.arguments, c = a.length;

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			var s = XMLHttpRequestObject.responseText;
			s = s.replace(/^\s+|\s+$/g,"");
			evil(s);
		}
	}
	XMLHttpRequestObject.send(null);
}

//Request PHP & CallBack, (strData, sendData, strFun) Large Send Data
function RequestPHP_LAR() {
	var a = RequestPHP_LAR.arguments, c = a.length;

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			if (c >= 3) {
				var s = XMLHttpRequestObject.responseText;
				s = "\""+ s.replace(/^\s+|\s+$/g,"") +"\"";
				for(var i = 3; i < c; i++) {
					s += ", \""+a[i]+"\"";
				}
				evil(a[2]+"("+s+")");
			}
		}
	}
	XMLHttpRequestObject.send(a[1]);
}

//gerald 2020-08-18
function RequestPHP_POST() {
	var a = RequestPHP_POST.arguments, c = a.length;
	var t = "&puttime="+new Date().getTime();

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			if (c >= 3) {
				var s = XMLHttpRequestObject.responseText;
				s = "\""+ s.replace(/^\s+|\s+$/g,"") +"\"";
				for(var i = 3; i < c; i++) {
					s += ", \""+a[i]+"\"";
				}
				evil(a[2]+"("+s+")");
			}
		}
	}
	XMLHttpRequestObject.send(a[1]+t);
}

//gerald 2020-08-18
function RequestPHP_POST_RUN() {
	var a = RequestPHP_POST_RUN.arguments, c = a.length;
	var t = "&puttime="+new Date().getTime();

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			var s = XMLHttpRequestObject.responseText;
			s = s.replace(/^\s+|\s+$/g,"");
			evil(s);
		}
	}
	XMLHttpRequestObject.send(a[1]+t);
}

//gerald 2020-08-18
function RequestPHP_POST_RUN_SP() {
	var a = RequestPHP_POST_RUN_SP.arguments, c = a.length;
	var t = "&puttime="+new Date().getTime();

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			var s = XMLHttpRequestObject.responseText;
			s = s.replace(/^\s+|\s+$/g,"");
			s = 'a = ' + JSON.stringify(a) + ';' + s;
			return Function(s)();
		}
	}
	XMLHttpRequestObject.send(a[1]+t);
}

//get OS value
function GetOS() {
	
   //var str = navigator.appVersion.split(";");
   //return str[2];
   
	var OS_VER = navigator.appVersion;
	if (OS_VER.indexOf("98") > 0) {				//Windows 98
		getOS = "0.98";
	}
	else if (OS_VER.indexOf("95") > 0) {		//Windows 95
		getOS = "0.95";
	}
	else if (OS_VER.indexOf("NT 5.0") > 0) {	//Windows 2000
		getOS = "5.0";
	}
	else if (OS_VER.indexOf("NT 5.1") > 0) {	//Windows XP
		getOS = "5.1";
	}
	else if (OS_VER.indexOf("NT 5.2") > 0) {	//Windows Server 2003
		getOS = "5.2";
	}
	else if (OS_VER.indexOf("NT 6.0") > 0) {	//Windows Vista
		getOS = "6.0";
	}
	else if (OS_VER.indexOf("NT 6.1") > 0) {	//Windows 7
		getOS = "6.1";
	}
	else if (OS_VER.indexOf("NT 6.2") > 0) {	//Windows 8
		getOS = "6.2";
	}
	else if (OS_VER.indexOf("NT 6.3") > 0) {	//Windows 8
		getOS = "6.3";
	}
	else if (OS_VER.indexOf("NT 10.0") > 0) {	//Windows 10
		getOS = "10.0";
	}
	else {
		getOS = "-1";
	}
	return getOS;
}

//get key value
function QueryString(key) {
	var value = null;
	var query = window.location.search.substring(1);
	var pairs = query.split("&");

	for (var i=0; i<pairs.length; i++) {
		var pos = pairs[i].indexOf('=');
		if(pos > 0) {
			var argname = pairs[i].substring(0,pos);
			if (argname===key) {
				value = pairs[i].substring(pos+1);
				break;
			}
		}
	}
	return value;
}

//gerald 2020-08-18
//get key value
function X_QueryString2(key) {
	var value = null;
	var query = window.top.location.search.substring(1);
	var pairs = query.split("&");

	for (var i=0; i<pairs.length; i++) {
		var pos = pairs[i].indexOf('=');
		if(pos > 0) {
			var argname = pairs[i].substring(0,pos);
			if (argname===key) {
				value = pairs[i].substring(pos+1);
				break;
			}
		}
	}
	if (value === null) {
		//alert("*"+query+"~~"+key+"QAQ"+value+"><><"+ window.top.location);
	}
	return value;
}

//gerald 2020-08-18
//get key value
function QueryString2(key) {

	if (typeof(evil(key)) === 'undefined') {
//console.log("1 "+key+" "+window.parent.location);
		if (window.parent !== this.window) {
//console.log("1-1 "+key+" "+window.parent.location);
			return window.parent.QueryString2(key);
		}
	}
	else if (evil(key) === "") {
//console.log("2 "+key+" "+window.parent.location);
		if (window.parent !== this.window) {
//console.log("3 "+key+" "+window.parent.location);
			return window.parent.QueryString2(key);
		}
	}
	else {
//console.log("4 "+key+" "+window.parent.location);
		return evil(key);
	}
}

function switchlanguage() {

	if (switchlanguage.arguments.length === 1) {
		switch(QueryString2("l")) {
			case "c":	l = "c";	break;
			case "g":	l = "g";	break;
			case "e":	l = "e";	break;
			case "j":	l = "j";	break;
			default:	l = "e";	break;
		}
	}
	else {
		l = switchlanguage.arguments[1];
	}
	return evil(l+"_"+switchlanguage.arguments[0]);
}

function language_init() { //gerald 2018-06-12
	var l = QueryString2("l");

	$("span[data-mlang],a[data-mlang],option[data-mlang],button[data-mlang]").each(function(index, value) {
//console.log("5-1 "+l+"_"+$(this).attr("data-mlang"));
		$(this).text(evil(l+"_"+$(this).attr("data-mlang")));
	});

	$("input[data-mlang]").each(function(index, value) {
//console.log("5-2 "+l+"_"+$(this).attr("data-mlang"));
		$(this).val(evil(l+"_"+$(this).attr("data-mlang")));
	});

	$("img[data-mlang]").each(function(index, value) {
//console.log("5-3 "+l+"_"+$(this).attr("data-mlang"));
		$(this).attr('title', evil(l+"_"+$(this).attr("data-mlang")));
	});
}

function language_init4() { //gerald 2018-06-12
	var l = QueryString("l");
	$("span[data-mlang],a[data-mlang],option[data-mlang]").each(function(index, value) {
		$(this).text(evil(l+"_"+$(this).attr("data-mlang")));
	});

	$("input[data-mlang]").each(function(index, value) {
		$(this).val(evil(l+"_"+$(this).attr("data-mlang")));
	});


	$("img[data-mlang]").each(function(index, value) {
		$(this).attr('title', evil(l+"_"+$(this).attr("data-mlang")));
	});
}

function language_init2(l) {

	obj = document.getElementsByTagName("span");
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].tag) !== "undefined") {
				if (obj[i].tag !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].tag);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('input');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].tag) !== "undefined") {
				if (obj[i].tag !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].tag);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('a');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].tag) !== "undefined") {
				if (obj[i].tag !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].tag);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('img');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].tag) !== "undefined") {
				if (obj[i].tag !== "") {
					obj[i].alt = evil(l+"_"+obj[i].tag);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('option');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].tag) !== "undefined") {
				if (obj[i].tag !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].tag);
				}
			}
		//}
	}
}

function language_init3(l) { // for IE9

	obj = document.getElementsByTagName("span");
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].title) !== "undefined") {
				if (obj[i].title !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].title);
					obj[i].title = evil(l+"_"+obj[i].title);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('input');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].title) !== "undefined") {
				if (obj[i].title !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].title);
					obj[i].title = evil(l+"_"+obj[i].title);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('a');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].title) !== "undefined") {
				if (obj[i].title !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].title);
					obj[i].title = evil(l+"_"+obj[i].title);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('img');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].title) !== "undefined") {
				if (obj[i].title !== "") {
					obj[i].alt = evil(l+"_"+obj[i].title);
					obj[i].title = evil(l+"_"+obj[i].title);
				}
			}
		//}
	}

	obj = document.getElementsByTagName('option');
	for (i in obj){
		//if (isNaN(i) === false) {
			if (typeof(obj[i].title) !== "undefined") {
				if (obj[i].title !== "") {
					obj[i].innerText = evil(l+"_"+obj[i].title);
					obj[i].title = evil(l+"_"+obj[i].title);
				}
			}
		//}
	}
}

function getfocus() {
	var a=getfocus.arguments;

	if (a[1] === "m") {
//		alert(event.clientX +" - "+ event.clientY);
		setsignouttime(new Date());
	}
	else if (a[1] === "k") {
//		alert(event.keyCode);
		setsignouttime(new Date());
	}
	return a[0];
}

function setsignouttime2(t) {

	autosignoutime = new Date(t.getTime() + (autosignoutimecnt*60*1000));
	document.getElementById("s_autosignout").innerText = "";
}

function setsignouttime(t) {

	window.top.window.setsignouttime2(t);
}

function signout_init() {
	//var uid = QueryString("uid");

	strData = "../php/ReadPerson.php?";
	rawData = "&uid="+uid;
	RequestPHP_POST(strData, rawData, "_signout_init");
}

function _signout_init(s) {

	if (s !== "") {
		var pairs = s.split("@!@");
		autosignoutimecnt = parseInt(pairs[0], 10);
		autowarningcnt = parseInt(pairs[1], 10);
		autosignoutime = new Date(new Date().getTime() + (autosignoutimecnt*60*1000));
		sop = setTimeout("signoutproc();",1000);
	}
}

function signoutproc() {

	clearTimeout(sop);
	var d = autosignoutime.getTime() - new Date().getTime();
	if (d <= 0) {
		MM_logout(2);
	}
	else if ((d < ((autowarningcnt+1)*60*1000)) && (d > (autowarningcnt*60*1000))) {
		sop = setTimeout("signoutproc();",1000);
	}
	else if (d <= (autowarningcnt*60*1000)) {
		document.getElementById("s_autosignout").innerHTML = "&nbsp;"+ switchlanguage("AutoSignOutRemain") +" "+ SecToTime(Math.ceil(d/1000)) +"&nbsp;";
		sop = setTimeout("signoutproc();",1000);
	}
	else {
		sop = setTimeout("signoutproc();",30000);
	}
}

function writeoplog() {
	a=writeoplog.arguments;	p=a.length-3;

	strData = "../php/writeoplog.php?";
	strData += "&uid="+a[0];
	strData += "&act="+a[1];
	strData += "&sact="+a[2];
	strData += "&rma=";
	strData += "&opt="+getNow2();
	for (var i=1; i<=8; i++) {
		if (i <= p) {
			strData += "&p"+i+"="+a[i+2];
		}
		else {
			strData += "&p"+i+"=";
		}
	}
	strData += "&time="+(new Date().getTime()-new Date().getTimezoneOffset()*60000);

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", strData);
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
		}
	}
	XMLHttpRequestObject.send(null);
}

function checkPassword(m, obj, str) {

	strData = "../php/checkpassword.php?";
	strData += "&m="+m;
	strData += "&pxd="+obj;
	strData += "&acc="+((str === "") ? uid : str);
	strData += "&time="+new Date().getTime();

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", strData, false);
	XMLHttpRequestObject.send(null);
	var s = XMLHttpRequestObject.responseText;
	XMLHttpRequestObject = null;
	delete XMLHttpRequestObject;

	s = s.replace(/^\s+|\s+$/g,"");
	if (s === 1) {
		var re=/^(.[a-zA-Z]+[0-9]+|.[0-9]+[a-zA-Z]+|[a-zA-Z]+.[0-9]+|[0-9]+.[a-zA-Z]+|[a-zA-Z]+[0-9]+.|[0-9]+[a-zA-Z]+.)/;
		if((!re.test(obj))||(String(obj).length<8)){
			return false;
		}
	}
	else if (s === 2) {
		return false;
	}
	return true;
}

function doresize() {

	if (resizetimeout !== null) clearTimeout(resizetimeout);
	resizetimeout = setTimeout("adjustobj();", 100);
}

function adjustobj() {

	changeGridWidth(getWidth(), getHeight());
}

function getWidth() {

	if (self.innerWidth) {
		obj_w = Math.ceil(self.innerWidth);
	}
	else if (document.documentElement && document.documentElement.clientWidth) {
		obj_w = Math.ceil(document.documentElement.clientWidth);
	}
	else if (document.body) {
		obj_w = Math.ceil(document.body.clientWidth);
	}
	return obj_w;
}

function getHeight() {

	if (self.innerHeight) {
		obj_h = Math.ceil(self.innerHeight);
	}
	else if (document.documentElement && document.documentElement.clientHeight) {
		obj_h = Math.ceil(document.documentElement.clientHeight);
	}
	else if (document.body) {
		obj_h = Math.ceil(document.body.clientHeight);
	}
	return obj_h;
	//adjustobj(obj_h);
}

//Check & Text Enable
function chktxtEnable(cid, tid) {

	t = tid.split(",");
	if (document.getElementById(cid).checked) {
		c = t.length;
		for (var i=0; i<c; i++) {
			document.getElementById(t[i]).disabled = false;
		}
	}
	else {
		c = t.length;
		for (var i=0; i<c; i++) {
			document.getElementById(t[i]).disabled = true;
		}
	}
}

//Check & Text Enable
function chktxtEnable2(cid, oid, tid, sid) {

	o = oid.split(",");
	t = tid.split(",");
	s = sid.split(",");
	if (document.getElementById(cid).checked) {
		c = o.length;
		for (var i=0; i<c; i++) {
			document.getElementById(o[i]).disabled = false;
		}
		c = t.length;
		for (var i=0; i<c; i++) {
			document.getElementById(t[i]).disabled = true;
			if (s[i] !== "") document.getElementById(s[i]).disabled = true;
		}
	}
	else {
		c = o.length;
		for (var i=0; i<c; i++) {
			document.getElementById(o[i]).disabled = true;
		}
		c = t.length;
		for (var i=0; i<c; i++) {
			document.getElementById(t[i]).disabled = false;
			if (document.getElementById(t[i]).checked) {
				if (s[i] !== "") document.getElementById(s[i]).disabled = false;
			}
		}
	}
}

//Option & Text Enable
function opttxtEnable(oid, tid) {

	o = oid.split(",");
	t = tid.split(",");
	c = o.length;
	for (var i=0; i<c; i++) {
		if (document.getElementById(o[i]).checked) {
			document.getElementById(t[i]).disabled = false;
		}
		else {
			document.getElementById(t[i]).disabled = true;
		}
	}
}

//Option & Object Display
function optobjdisplay(sid, oid) {

	s = document.getElementsByName(sid);
	for (var i=0; i<s.length; i++) {
		if (s[i].checked) {
			document.getElementById(oid + "_" + s[i].value).style.display = "";
		}
		else {
			document.getElementById(oid + "_" + s[i].value).style.display = "none";
		}
	}
}

function check_all(obj,cName) {

	var checkboxs = document.getElementsByName(cName);
	for(var i=0;i<checkboxs.length;i++){checkboxs[i].checked = obj.checked;}
}

function check_all2(obj, cType, cName, cMax) {

    switch(cType) {
        case '1':
        	n = 255;
			for(var i=0;i<=n;i++) {
				cId2 = cId = ReplaceAll(cName, "@r", i); cId = ReplaceAll(cId, "@c", 0);
				if (document.getElementById(cId) === null) break;
				for(var j=1;j<=cMax;j++) {
					cId3 = ReplaceAll(cId2, "@c", j);
					//document.getElementById(cId3).innerText = ((obj.checked) ? unescape("%u2605") : "");
					document.getElementById(cId3).innerText = ((obj.checked) ? "V" : "");
				}
				if (i === n) n += 255;
			}
            break;
        case '2':
        	n = 255;
			for(var i=0;i<=n;i++) {
				cId = ReplaceAll(cName, "@r", i);
				if (document.getElementById(cId) === null) break;
				//document.getElementById(cId).innerText = ((obj.checked) ? unescape("%u2605") : "");
				document.getElementById(cId).innerText = ((obj.checked) ? "V" : "");
				if (i === n) n += 255;
			}
            break;
        case '3':
			for(var i=1;i<=cMax;i++) {
				cId = cName + i;
				//document.getElementById(cId).innerText = ((obj.checked) ? unescape("%u2605") : "");
				document.getElementById(cId).innerText = ((obj.checked) ? "V" : "");
			}
            break;
    }
}

//check month days 28, 29, 30, 31
function check_day(i_y, i_m)
{
    var c;
    switch(i_m) {
        case '01':
        case '03':
        case '05':
        case '07':
        case '08':
        case '10':
        case '12':
            c=31;
            break;
        case '02':
            c=28;	// February 28 days
            if ((parseInt(i_y)%4,10)===0) {
                c=29;	// February 29 days
            }
            break;
        case '04':
        case '06':
        case '09':
        case '11':
            c=30;
            break;
    }
    return c;
}

function setTableSize() {
	var tableWidBuf, relTableWidBuf;

	tableWidBuf = parseInt(document.getElementById("data_table").style.width.substring(0,
		document.getElementById("data_table").style.width.length),10);
	relTableWidBuf = parseInt((document.body.clientWidth*tableWidth),10);

	if(tableWidBuf < relTableWidBuf) {
		document.getElementById("TableContain").style.width = tableWidBuf+"px";
	}else {
		document.getElementById("TableContain").style.width = relTableWidBuf+"px";
	}

	//document.getElementById("TableContain").style.width = (document.body.clientWidth*tableWidth)+"px";
	document.getElementById("TableContain").style.height = (document.body.clientHeight*tableHeight)+"px";

	document.getElementById("mpDiv").style.width = (document.body.clientWidth*WMediaPlayerX)+"px";
	document.getElementById("MediaPlayer").style.width = (document.body.clientWidth*WMediaPlayerX)+"px";
	document.getElementById("mpDiv").style.left=((document.body.clientWidth*(1-WMediaPlayerX))/2)+"px";

	//setTimeout("setTableSize()",3000);
}

function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr;

 	for(i=0; a&&i<a.length&&(x=a[i])&&x.oSrc; i++)
 		x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
	var d=document;

	if(d.images){
		if(!d.MM_p)	d.MM_p=new Array();
    	var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
    	for(i=0; i<a.length; i++)
    		if (a[i].indexOf("#")!==0){
    			d.MM_p[j]=new Image;
    			d.MM_p[j++].src=a[i];
    		}
    }
}

function MM_findObj(n, d) { //v4.01
	var p,i,x;

	if(!d) d=document;
	if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
	}
	if(!(x=d[n])&&d.all) x=d.all[n];
	for(i=0; !x&&i<d.forms.length; i++)
		x=d.forms[i][n];
	for(i=0; !x&&d.layers&&i<d.layers.length; i++)
		x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n);
	return x;
}

function MM_swapImage() { //v3.0
	var i,j=0,x,a=MM_swapImage.arguments;

	document.MM_sr=new Array;
	for(i=0; i<(a.length-2); i+=3)
		if((x=MM_findObj(a[i]))!==null){
			document.MM_sr[j++]=x;
			if(!x.oSrc) x.oSrc=x.src;
			x.src=a[i+2];
		}
}

function CreateNewElement(inputObj, elementObj){
	//alert(elementObj);
	var newElement = document.createElement(elementObj);
	inputObj.appendChild(newElement);
	return newElement;
}

function CreateNewTextNode(inputObj, textNodeObj){
	var newTextNode = document.createTextNode(textNodeObj);
	inputObj.appendChild(newTextNode);
	return newTextNode;
}

function CreateNewOption(selectObj, optionValue, optionText){
	var newOption = document.createElement("option");
	newOption.value = optionValue;
	newOption.text = optionText;
	selectObj.add(newOption);
}

function CreateNewAttribute(inputObj, AttributeArr){
	for (var key in AttributeArr) {
		inputObj.setAttribute(key, AttributeArr[key]);
	}
}

function checkDate(obj) { //YYYY/MM/DD
	var re=/((1|2)(\d\d\d)(\/|-|.)((0)([1-9])|(1)([0-2]))(\/|-|.)(([0-2])(\d)|(3)(0|1))|(1|2)(\d\d\d)((0)([1-9])|(1)([0-2]))(([0-2])(\d)|(3)(0|1)))/;
	if((!re.test(obj.value))||(String(obj.value).length===0)){
		obj.select();
		obj.focus();
		obj.value = getCurDate();
	}
	else {
 		if(!(String(obj.value).length===0)) {
 			obj.value=obj.value.replace(/\./g,'');
			obj.value=obj.value.replace(/\-/g,'');
			obj.value=obj.value.replace(/\//g,'');
			//check month 30 days or February 29 days
			re=/(((0)(2|4|6|9)|(1)(1))(3)(1)|(0)(2)(3)(0)|(\d)(\d)(0)(0))/;
			if(re.test(Mid(obj.value,5,4)) || (Mid(obj.value,5,4)==='0229' && !(parseInt(Left(obj.value,4),10)%4)===0)) {
				obj.select();
				obj.focus();
				obj.value = getCurDate();
			}
			else {
				var start = new Date(parseInt(Left(obj.value,4),10), parseInt(Mid(obj.value,5,2),10)-1, parseInt(Mid(obj.value,7,2),10));
				var end = new Date(getCurDate());
				if(start > end) {
					obj.select();
					obj.focus();
					obj.value = getCurDate();
				}
				else {
					obj.value=Left(obj.value,4)+"/"+Mid(obj.value,5,2)+"/"+Mid(obj.value,7,2);
				}
			}
		}
	}
}

function checkDate2(obj) { //MM/DD
	var re=/(0)([1|3|5|7|8])(|(\/|-|.))((0)([1-9])|([1-2])(\d)|(3)(0|1))|(1)([0|2])(|(\/|-|.))((0)([1-9])|([1-2])(\d)|(3)(0|1))|(0)([4|6|9])(|(\/|-|.))((0)([1-9])|([1-2])(\d)|(3)(0))|(1)(1)(|(\/|-|.))((0)([1-9])|([1-2])(\d)|(3)(0))|(0)(2)(|(\/|-|.))((0)([1-9])|(1)(\d)|(2)([1-9]))/;
	if((!re.test(obj.value))&&!(String(obj.value).length===0)){
		obj.select();
		obj.focus();
		obj.value = "01/01";
	}
	else {
 		if(!(String(obj.value).length===0)) {
			var outCome_matc = obj.value.match(re);
			obj.value = outCome_matc[0];
 			obj.value=obj.value.replace(/\./g,'');
			obj.value=obj.value.replace(/\-/g,'');
			obj.value=obj.value.replace(/\//g,'');
			obj.value=Left(obj.value,2)+"/"+Mid(obj.value,3,2);
		}
	}
}

function checkDate3(obj) {
	var re=/((1|2)(\d\d\d)(\/|-|\.)((0)([1-9])|(1)([0-2]))(\/|-|\.)(([0-2])(\d)|(3)(0|1))|(1|2)(\d\d\d)((0)([1-9])|(1)([0-2]))(([0-2])(\d)|(3)(0|1)))/;
	if((!re.test(obj.value))&&!(String(obj.value).length===0)){
		return false;
	}
	else {
		if(!(String(obj.value).length===0)) {
 			var newobj=obj.value.replace(/\./g,'');
			newobj=newobj.replace(/-/g,'');
			newobj=newobj.replace(/\//g,'');
			if(re.test(Mid(newobj,5,4)) || Mid(newobj,5,4)==='0229' && !(parseInt(Left(newobj,4),10)%4)===0) {
				return false;
			}
		}
	}
	obj.value=obj.value.replace(/./g,'/');
	obj.value=obj.value.replace(/\-/g,'/');
	return true;
}

function checkDate4(obj) {
	var re=/^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) (0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]))/;
	if((!re.test(obj.value))&&!(String(obj.value).length===0)){
		obj.value = getNow();
	}
	else if(String(obj.value).length===0){
		obj.value = getNow();
	}
	else {
		obj.value = obj.value.match(re)[0];
	}
}

function getCurDate() {
	var m, d;
	var monNames = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

	dCurrentDate = new Date();
	m = dCurrentDate.getMonth();
	d = dCurrentDate.getDate().toString();
	if(d.length === 1)	{d = "0" + d}
	return dCurrentDate.getFullYear() + "/" + monNames[m] + "/" + d;
}

function checkTime(obj) {
	var re=/(((0|1)(\d)|(2)([0-3]))(:)([0-5])(\d)|((0|1)(\d)))/;
	if((!re.test(obj.value)) && !(String(obj.value).length===0)) {
		obj.select();
		obj.focus();
		obj.value = "00:00";
		return false;
	}
	else {
		if(!(String(obj.value).length===0)) {
			obj.value=obj.value.replace(/\:/g,'');
			obj.value=Left(obj.value,2)+":"+Mid(obj.value,3,2);
			re=/((0|1)(\d)|(2)([0-3]))(:)([0-5])(\d)/;
			if(!re.test(obj.value)) {
				obj.select();
				obj.focus();
				obj.value = "00:00";
				return false;
			}
		}
	}
}

function IsNumber(obj) {
	re = /\D/;
	if (re.test(obj.value)) {
		str = obj.value;
		str = str.substr(0, str.length -1);
		obj.value = str;
		return false;
	}
}

function IsDateKey() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	//alert(intCode);
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 13) isValidKey = true; // 13 enter
	if (intCode === 32) isValidKey = true; // 32 spacebar
	if (intCode === 47) isValidKey = true; // 47 /
	window.event.returnValue = isValidKey;
}

function IsTimeKey() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	//alert(intCode);
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 13) isValidKey = true; // 13 enter
	if (intCode === 32) isValidKey = true; // 32 spacebar
	if (intCode === 58) isValidKey = true;  // 58 :
	window.event.returnValue = isValidKey;
}

function IsDateTimeKey(obj) {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	//alert(intCode);
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 13) isValidKey = true; // 13 enter
	if (intCode === 32) isValidKey = true; // 32 spacebar
	if (intCode === 47) isValidKey = true; // 47 /
	if (intCode === 58) isValidKey = true; // 58 :
/*
	if (obj.value.length === 0) {
		var pattern = /^([1-9])$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 1) {
		var pattern = /^([1-9][0-9]{1})$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 2) {
		var pattern = /^([1-9][0-9]{2})$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 3) {
		var pattern = /^([1-9][0-9]{3})$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 4) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 5) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0|1))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 6) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2]))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 7) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 8) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0|1|2|3))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 9) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 10) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) )$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 11) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) (0|1|2))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 12) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) (0[0-9]|1[0-9]|2[0-3]))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 13) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) (0[0-9]|1[0-9]|2[0-3]):)$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 14) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) (0[0-9]|1[0-9]|2[0-3]):([0-5]))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
	else if (obj.value.length === 15) {
		var pattern = /^([1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1]) (0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]))$/;
		if (!pattern.test(obj.value + String.fromCharCode(intCode))) isValidKey = false;
	}
*/
	window.event.returnValue = isValidKey;
}

function InStr(Start, strSearch, charSearchFor) {
	for (i=Start-1; i < strSearch.length-charSearchFor.length; i++) {
		if (charSearchFor === Mid(strSearch, i, charSearchFor.length)) {
			return i;
		}
	}
	return 0;
}

function IsNumKey() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 45) isValidKey = true; // 45 -
	window.event.returnValue = isValidKey;
}

function checkNum(obj) {

	var s = obj.value;
	s = s.replace(/-/g,"0");
	if (isNaN(s)) {
		obj.value = "";
	}
}

function IsNumKey2() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 44) isValidKey = true; // 44 ,
	if (intCode === 45) isValidKey = true; // 45 -
	window.event.returnValue = isValidKey;
}

function checkNum2(obj) {

	var s = obj.value;
	s = s.replace(/-/g,"0");
	s = s.replace(/,/g,"0");
	if (isNaN(s)) {
		obj.value = "";
	}
}

function IsNumKey3() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 35) isValidKey = true; // 35 #
	if (intCode === 42) isValidKey = true; // 42 *
	if (intCode === 44) isValidKey = true; // 44 ,
	window.event.returnValue = isValidKey;
}

function checkNum3(obj) {

	var s = obj.value;
	s = s.replace(/\#/g,"0");
	s = s.replace(/\*/g,"0");
	s = s.replace(/\,/g,"0");
	if (isNaN(s)) {
		obj.value = "";
	}
}

function IsNumKey4() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 44) isValidKey = true; // 44 ,
	window.event.returnValue = isValidKey;
}

function checkNum4(obj) {

	var s = obj.value;
	s = s.replace(/\,/g,"0");
	if (isNaN(s)) {
		obj.value = "";
	}
}

function IsDBKey() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 45) isValidKey = true; // 13 -
	window.event.returnValue = isValidKey;
}

function checkDB(obj) {

	if (isNaN(obj.value)) {
		obj.value = 6;
	}
	else {
		var DB = parseInt(obj.value,10);
		if (DB > 31)
			obj.value = 6;
		if (DB < -31)
			obj.value = 6;
	}
}

function IsVoltageKey() {
	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	window.event.returnValue = isValidKey;
}

function checkVoltage(obj) {

	if (isNaN(obj.value)) {
		obj.value = 26;
	}
	else {
		var Voltage = parseInt(obj.value,10);
		if (Voltage > 48)
			obj.value = 26;
		//if (Voltage < 5)
		//	obj.value = 26;
		if (Voltage < 0)
			obj.value = 26;
	}
}

function checkAGC(obj) {
	var Voltage = parseInt(obj.value,10);
	if (Voltage > 255)
		obj.value = 6;
	if (Voltage < 0)
		obj.value = 0;
}

function IsNumberKey() {

	var isValidKey = false;
	var intCode = window.event.keyCode;
	if (intCode >= 48 && intCode <= 57) isValidKey = true;
	if (intCode === 13) isValidKey = true; // 13 enter
	window.event.returnValue = isValidKey;
}

function checkNoZoro(obj, n) {
	var i = parseInt(obj.value,10);
	if (i === 0)
		obj.value = n;
}

function check2KValue(obj, n) {

	var pattern = /^([1-9]\d{0,2}|[1]\d\d\d|[2][0][0][0])$/;
	var i = parseInt(obj.value,10);
	if (pattern.test(i))
		obj.value = i;
	else
		obj.value = n;
}

function checkMaxValue(obj, n) {

	var pattern = /^([1-9]\d{0,2}|[1][0-3]\d{1,2}|[1][4][0-3]\d|[1][4][4][0])$/;
	var i = parseInt(obj.value,10);
	if (pattern.test(i))
		obj.value = i;
	else
		obj.value = n;
}

function checkpercent(obj) {

	if ((obj.value === "") || (isNaN(obj.value))) obj.value = 0;
	if (obj.value > 100) obj.value = 100;
}

function AutoValidKey1(obj) {	// YYYY/MM/dd HH:mm:ss

	var intCode = window.event.keyCode;
	if (intCode !== 8) {
		var pattern = /^(\d{4,4})$/;
		if (pattern.test(obj.value) && (intCode !== 191))
			obj.value += "/";

		var pattern = /^(\d{4,4}\/\d{2,2})$/;
		if (pattern.test(obj.value) && (intCode !== 191))
			obj.value += "/";

		var pattern = /^(\d{4,4}\/\d{2,2}\/\d{2,2})$/;
		if (pattern.test(obj.value) && (intCode !== 32))
			obj.value += " ";

		var pattern = /^(\d{4,4}\/\d{2,2}\/\d{2,2} \d{2,2})$/;
		if (pattern.test(obj.value) && (intCode !== 186))
			obj.value += ":";
	}
}

function AutoValidKey2(obj) {	// YYYY/MM/dd

	var intCode = window.event.keyCode;
	if (intCode !== 8) {
		var pattern = /^(\d{4,4})$/;
		if (pattern.test(obj.value) && (intCode !== 191))
			obj.value += "/";

		var pattern = /^(\d{4,4}\/\d{2,2})$/;
		if (pattern.test(obj.value) && (intCode !== 191))
			obj.value += "/";
	}
}

function AutoValidKey3(obj) {	// MM/dd/YYYY

	var intCode = window.event.keyCode;
	if (intCode !== 8) {
		var pattern = /^(\d{2,2})$/;
		if (pattern.test(obj.value) && (intCode !== 191))
			obj.value += "/";

		var pattern = /^(\d{2,2}\/\d{2,2})$/;
		if (pattern.test(obj.value) && (intCode !== 191))
			obj.value += "/";
	}
}

function getFirstDay() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1.setDate(1);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d+" 00:00";
}

function getFirstDay2() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1.setDate(1);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d;
}

function getNow() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d+" "+t1_h+":"+t1_n;
}

function getNow2() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);
	t1_s = "00"+(t1.getSeconds());
	t1_s = t1_s.substring(t1_s.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d+" "+t1_h+":"+t1_n+":"+t1_s;
}

function getNow3() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d;
}


function getNow4() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d+" "+"23:59";
}

function NumFormat(number, form) {
	var forms = form.split('.'), number = '' + number, numbers = number.split('.'), leftnumber = numbers[0].split(''),
			exec = function (lastMatch) {
				if (lastMatch === '0' || lastMatch === '#') {
					if (leftnumber.length) {
						return leftnumber.pop();
					}
					else if (lastMatch === '0') {
						return lastMatch;
					}
					else {
						return '';
					}
				}
				else {
					return lastMatch;
				}
		    }, string

	string = forms[0].split('').reverse().join('').replace(/./g, exec).split('').reverse().join('');
	string = leftnumber.join('') + string;

	if (forms[1] && forms[1].length) {
		leftnumber = (numbers[1] && numbers[1].length) ? numbers[1].split('').reverse() : [];
		string += '.' + forms[1].replace(/./g, exec);
	}
	return string.replace(/\.$/, '');
}

function HourToTime(h) {

	//var hh = h % 24;
	//var dd = (h - hh) / 24;
	//return dd+"-"+Right(("00"+hh), 2);
	return h;
}

function MinToTime(m) {

	m = ((m > 1440)?1440:m);
	var mm = m % 60;
	var hh = (m - mm) / 60;
	return Right(("00"+hh), 2)+":"+Right(("00"+mm), 2);
}

function SecToTime(s) {

	s = ((s > 86400)?86400:s);
	var ss = s % 3600;
	var hh = (s - ss) / 3600;
	var sss = ss % 60;
	var mm = (ss - sss) / 60;
	return Right(("00"+hh), 2)+":"+Right(("00"+mm), 2)+":"+Right(("00"+sss), 2);
}

function SecToTime2(s) {

	s = ((s > 86400)?86400:s);
	var ss = s % 3600;
	var hh = (s - ss) / 3600;
	var sss = ss % 60;
	var mm = (ss - sss) / 60;
	return Right(("00"+hh), 2)+" : "+Right(("00"+mm), 2)+" : "+Right(("00"+sss), 2);
}

function Sec2Hms(s) {

	d = s % 3600;
	hh = (s - d) / 3600;
	ss = d % 60;
	mm = (d - ss) / 60;

	str = 0;
	if ((hh === 0) && (mm === 0)) {
		str = ss+"&quot;";
	}
	else if (hh === 0) {
		str = mm+"&#39;"+Right(("00"+ss), 2)+"&quot;";
	}
	else {
		str = hh+":"+Right(("00"+mm), 2)+"&#39;"+Right(("00"+ss), 2)+"&quot;";
	}
	return str;
}

function DateDiff(d1, d2) {

	d1 = new Date(d1);
	d2 = new Date(d2);
	return Math.ceil((Date.parse(d2.toString(), 10) - Date.parse(d1.toString(), 10))/(1000*60*60*24));
}

function calshortspace(c) {

	m = parseInt(c, 10);
	var mm = (m % (1024 * 1024));
	var tt = (m - mm) / (1024 * 1024);
	var mmm = mm % 1024;
	var gg = (mm - mmm) / 1024;

	if (tt > 0) s = tt+((tt >= 100)?"":".")+Left(Right(("000"+gg), 3),((tt >= 100)?0:((tt >= 10)?1:2)))+" TB"
	else if (gg > 0) s = gg+((gg >= 100)?"":".")+Left(Right(("000"+mmm), 3),((gg >= 100)?0:((gg >= 10)?1:2)))+" GB"
	else s = mmm+" MB"
	return s;
}

function Mid(str, start, len) {
	if (start < 0 || len < 0)
		return '';
	return String(str).substr(start-1, len);
}

function Left(str, n) {
	if (n <= 0)
		return '';
	else if (n > String(str).length)
		return str;
	else
		return String(str).substring(0,n);
}

function Right(str, n) {
	if (n <= 0)
		return '';
	else if (n > String(str).length)
		return str;
	else {
		var iLen = String(str).length;
		return String(str).substring(iLen, iLen - n);
	}
}

function commafy(num) {
	var re = /(-?\d+)(\d{3})/;
	num = num + "";
	while (re.test(num)) {
		num = num.replace(re, "$1,$2");
	}
	return num;
}

function ReplaceAll(strSource, strFind, strRepl) {
	var str = new String(strSource);
	while (str.indexOf(strFind) !== -1) {
		str = str.replace(strFind, strRepl);
	}
	return str;
}

function getabsolute(obj) {
	var tmpobj = obj, x = 0, y = 0;
	do {
		x = x + tmpobj.offsetLeft;
		y = y + tmpobj.offsetTop;
		tmpobj = tmpobj.offsetParent;
	} while(tmpobj !== document.body)
	return [x,y];
}

String.prototype.blength = function() {
	var arr = this.match(/[^\x00-\xff]/ig);
	return  arr === null ? this.length : this.length + arr.length;
}

function encode64(input) {
	var output = "";
	var chr1, chr2, chr3 = "";
	var enc1, enc2, enc3, enc4 = "";
	var i = 0;

	input = escape(utf8_encode(input));
	do {
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);

		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;

		if (isNaN(chr2)) {
			enc3 = enc4 = 64;
		} else if (isNaN(chr3)) {
			enc4 = 64;
		}
		output += Base64KeyStr.charAt(enc1) + Base64KeyStr.charAt(enc2) + Base64KeyStr.charAt(enc3) + Base64KeyStr.charAt(enc4);
	} while (i < input.length);
	return output;
}

function decode64(input) {
	var output = "";
	var chr1, chr2, chr3 = "";
	var enc1, enc2, enc3, enc4 = "";
	var i = 0;

	input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
	do {
		enc1 = Base64KeyStr.indexOf(input.charAt(i++));
		enc2 = Base64KeyStr.indexOf(input.charAt(i++));
		enc3 = Base64KeyStr.indexOf(input.charAt(i++));
		enc4 = Base64KeyStr.indexOf(input.charAt(i++));

		chr1 = (enc1 << 2) | (enc2 >> 4);
		chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
		chr3 = ((enc3 & 3) << 6) | enc4;

		output = output + String.fromCharCode(chr1);
		if (enc3 !== 64) {
			output = output + String.fromCharCode(chr2);
		}
		if (enc4 !== 64) {
			output = output + String.fromCharCode(chr3);
		}
	} while (i < input.length);

	return unescape(utf8_decode(output));
}

function utf8_encode(str) {
    str = str.replace(/\r\n/g, "\n");
    var s = "";

    for (var n = 0; n < str.length; n++) {
        var c = str.charCodeAt(n);

        if (c < 128) {
            s += String.fromCharCode(c);
        }
        else if ((c > 127) && (c < 2048)) {
            s += String.fromCharCode((c >> 6) | 192);
            s += String.fromCharCode((c & 63) | 128);
        }
        else {
            s += String.fromCharCode((c >> 12) | 224);
            s += String.fromCharCode(((c >> 6) & 63) | 128);
            s += String.fromCharCode((c & 63) | 128);
        }
    }
    return s;
}

function utf8_decode(str) {
    var s = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while (i < str.length) {
        c = str.charCodeAt(i);

        if (c < 128) {
            s += String.fromCharCode(c);
            i++;
        }
        else if ((c > 191) && (c < 224)) {
            c2 = str.charCodeAt(i + 1);
            s += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = str.charCodeAt(i + 1);
            c3 = str.charCodeAt(i + 2);
            s += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
    return s;
}

function drawplayfile() {

	var showDraw = window.top.document.getElementById("browserframe").contentWindow.document.getElementById("browserframe2").contentWindow;
	if (showDraw.document.getElementById("draw_view").checked === true || showDraw.document.getElementById("p_draw_view").checked === true){
		var l = QueryString2("l");
		var uid = QueryString2("uid");

		strData = "../php/DrawPlayFile.php?";
		strData += "&uid="+uid;
		strData += "&port="+location.port;
		strData += "&time="+(new Date().getTime()-new Date().getTimezoneOffset()*60000);

		var XMLHttpRequestObject = false;
		if (window.XMLHttpRequest) {
			XMLHttpRequestObject = new XMLHttpRequest();
		}
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		XMLHttpRequestObject.open("POST", strData);
		XMLHttpRequestObject.onreadystatechange = function() {
			if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
				//alert(XMLHttpRequestObject.responseText);	// show error
				var s = XMLHttpRequestObject.responseText;
				s = s.replace(/^\s+|\s+$/g,"");
				if (s !== "" && s !== "-1") {
					window.top.document.getElementById("browserframe").contentWindow.document.getElementById("browserframe2").contentWindow.document.getElementById("Painter").src = s;
				}
				else {
					alert(switchlanguage("FileIsNtExist"));
				}
			}
		}
		XMLHttpRequestObject.send(null);
	}
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname.replace(/%20/, "") + "=" ;
	var ca = document.cookie.split(';');

	for (var i = 0; i < ca.length; i++) {
		var c = ca[i].replace(/%20/, "");
		while (c.charAt(0) === ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function ctiGetFid(i, string) {
	var fid = "";
	var checkboxs = document.getElementsByName("c"+i);
	for(var j = 0; j < checkboxs.length; j++) {
		if (checkboxs[j].checked) {
			fid += ((fid === "") ? "" : string)
			if (isIE) {
				fid += document.getElementById("s"+i+"_"+j).innerText;
			} else {
				fid += document.getElementById("s"+i+"_"+j).textContent;
			}
		}
	}
	return fid;
}

function SpcCharConv(s) {

	s = s.replace(/\%/g, "%25");
	s = s.replace(/\#/g, "%23");
	s = s.replace(/\&/g, "%26");
	s = s.replace(/\+/g, "%2B");
	s = s.replace(/\=/g, "%3D");
	s = s.replace(/\ /g, "%20");
	s = s.replace(/\//g, "%2F");
	s = s.replace(/\\/g, "%5C");
	s = s.replace(/\?/g, "%3F");
	return s;
}

function evil(fn) {

	var Fn = Function;
	//console.log("fn: "+fn);
	return new Fn('return ' + fn)();
}

function checkNaN(n) {
	
	var re = /^[0-9]+$/;
	if (re.test(n)) {
		return true;	
	}
	return false;
}