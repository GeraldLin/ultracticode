//var uid = QueryString2("uid");
//var resizetimeout = null;
var lastfocusobject = null;
var lastfocusindex = null;

var isIE = (navigator.userAgent.search("MSIE") > -1) || (navigator.userAgent.search("Trident/7.0") > -1);		//IE
var isFirefox = navigator.userAgent.search("Firefox") > -1;	//Firefox
var isOpera = navigator.userAgent.search("Opera") > -1;		//Opera
var isSafari = navigator.userAgent.search("Safari") > -1;	//Chrome&Safari

$(document).ready(function() {	//gerald 2018-06-12

	l = "c";

	language_init();
	if ($("#SearchIt").length) {
		$("#SearchIt").click(function() {
			SearchIt();
		});
	}

	show_init();
});

function show_init() {

	$("#SelType").append("<option selected disabled>"+ switchlanguage("SelType0", "c") +"</option>");
	$("#SelType").append("<option value='CAR' style=\'font-weight: bolder;\'>"+ switchlanguage("SelType1", "c") +"</option>");
	$("#SelType").append("<option value='FIR' style=\'font-weight: bolder;\'>"+ switchlanguage("SelType2", "c") +"</option>");
	$("#SelType").append("<option value='AH' style=\'font-weight: bolder;\'>"+ switchlanguage("SelType3", "c") +"</option>");
	$("#SelType").append("<option value='OTH' style=\'font-weight: bolder;\'>"+ switchlanguage("SelType4", "c") +"</option>");
}

function SearchIt() {

	var stype =  $("select[name=SelType]").val();
	if (stype === null) return;

	$("#ShowData>tbody").empty();

	strData = "php/SearchInsInfo.php?";
	rawData = "&userId="+userId;
	rawData += "&ownerId="+ownerId;
	rawData += "&riskCode="+stype;
	RequestPHP_POST(strData, rawData, "_SearchIt", stype);
}

function _SearchIt(s, stype) {
//console.log(s);
//return;
//console.log(window.atob(s));

	$("#ShowData")[0].style.display = "";
	var obj = evil(window.atob(s));

	if (stype === "CAR") {
		$("#ShowData").css("width","300%");
		$("#ShowData>tbody").append("<tr id=\'title\' style=\'color:white;background-color:#FE4365;\'></tr>");
		$("#title").append("<td style=\'width:40px;\'>"+ switchlanguage("Field001000", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001003", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001004", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001005", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001006", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001007", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001008", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001009", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001010", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001011", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001012", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001013", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001014", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001015", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001016", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001017", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001018", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001019", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001020", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field001021", "c") +"</td>");

		for (i = 0; i < obj.data.length; i++) {
			$("#ShowData>tbody").append("<tr id=\'content"+ i +"\' onclick=\'FocusIt(this, "+ i +")\' style=\'color:white;background-color:"+ (((i % 2) === 0) ? "#007766;": "#005544;") +"\'></tr>");
			$("#content"+ i).append("<td align='center'>"+ (i+1) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insuredname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].identifynumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].birthday) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].phonenumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].email) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].address) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].policyno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].printno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insperiod) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insperiod) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].licenseno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].engineno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].brandname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].carkindcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].exhaustscale) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumpremium) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumamount) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].extracomcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].businessnature) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handlername) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handler1name) +"</td>");
		}
	}
	else if (stype === "FIR") {
		$("#ShowData").css("width","200%");
		$("#ShowData>tbody").append("<tr id=\'title\' style=\'color:white;background-color:#FE4365;\'></tr>");
		$("#title").append("<td>"+ switchlanguage("Field002000", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002003", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002004", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002005", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002006", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002007", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002008", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002009", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002010", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002011", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002012", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002013", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002014", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002015", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002016", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field002017", "c") +"</td>");

		for (i = 0; i < obj.data.length; i++) {
			$("#ShowData>tbody").append("<tr id=\'content"+ i +"\' onclick=\'FocusIt(this, "+ i +")\' style=\'color:white;background-color:"+ (((i % 2) === 0) ? "#007766;": "#005544;") +"\'></tr>");
			$("#content"+ i).append("<td align='center'>"+ (i+1) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insuredname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].identifynumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].birthday) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].phonenumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].email) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].address) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].buildingAddress) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].policyno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insperiod) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].mortgageepeople) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].buildarea) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumpremium) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumamount) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].extracomcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].businessnature) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handlername) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handler1name) +"</td>");
		}
	}
	else if (stype === "AH") {
		$("#ShowData").css("width","200%");
		$("#ShowData>tbody").append("<tr id=\'title\' style=\'color:white;background-color:#FE4365;\'></tr>");
		$("#title").append("<td>"+ switchlanguage("Field003000", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003003", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003004", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003005", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003006", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003007", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003008", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003009", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003010", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003011", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003012", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003013", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003014", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003015", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003016", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field003017", "c") +"</td>");

		for (i = 0; i < obj.data.length; i++) {
			$("#ShowData>tbody").append("<tr id=\'content"+ i +"\' onclick=\'FocusIt(this, "+ i +")\' style=\'color:white;background-color:"+ (((i % 2) === 0) ? "#007766;": "#005544;") +"\'></tr>");
			$("#content"+ i).append("<td align='center'>"+ (i+1) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insuredname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].identifynumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].birthday) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].phonenumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].email) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].address) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].policyno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insperiod) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].creditcardno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].payername) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumpremium) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumamount) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].extracomcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].businessnature) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handlername) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handler1name) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].remark) +"</td>");
		}
	}
	else if (stype === "OTH") {
		$("#ShowData").css("width","200%");
		$("#ShowData>tbody").append("<tr id=\'title\' style=\'color:white;background-color:#FE4365;\'></tr>");
		$("#title").append("<td>"+ switchlanguage("Field004000", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field000002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004001", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004002", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004003", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004004", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004005", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004006", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004007", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004008", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004009", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004010", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004011", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004012", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004013", "c") +"</td>");
		$("#title").append("<td>"+ switchlanguage("Field004014", "c") +"</td>");

		for (i = 0; i < obj.data.length; i++) {
			$("#ShowData>tbody").append("<tr id=\'content"+ i +"\' onclick=\'FocusIt(this, "+ i +")\' style=\'color:white;background-color:"+ (((i % 2) === 0) ? "#007766;": "#005544;") +"\'></tr>");
			$("#content"+ i).append("<td align='center'>"+ (i+1) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].riskcname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insuredname) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].identifynumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].birthday) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].phonenumber) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].email) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].address) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].policyno) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].insperiod) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumpremium) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].sumamount) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].extracomcode) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].businessnature) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handlername) +"</td>");
			$("#content"+ i).append("<td>"+ decodeURI(obj.data[i].handler1name) +"</td>");
		}
	}
}

function FocusIt(obj, idx) {

	if (lastfocusobject !== null) lastfocusobject.style.opacity = "1.0";
	if (lastfocusindex !== null) lastfocusobject.style.backgroundColor = (((lastfocusindex % 2) === 0) ? "#007766": "#005544");

	obj.style.opacity = "0.8";
	obj.style.backgroundColor = "#00C0C0";
	lastfocusobject = obj
	lastfocusindex = idx;
}

function changeGridWidth(w, h) {

	h = h-75;
	document.getElementById("browserframe").style.height = h;
}