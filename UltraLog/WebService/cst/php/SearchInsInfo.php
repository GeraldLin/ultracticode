<?php

	//$ip = '192.168.190.111';	//Test
	//$userId = 'A123456789';	//Test
	//$ownerId = "BI086";		//Test
	//$riskCode = "BI086";		//Test
	$ip = getenv("REMOTE_ADDR");

	/*foreach (array_keys($_POST) as $key) $$key = $_POST[$key];
	foreach (array_keys($_GET) as $key) {
		$$key = (isset($$key) ? $$key : $_GET[$key]);
	}*/
	$xPost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$xGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	if (is_array($xPost)) foreach (array_keys($xPost) as $key) $$key = $xPost[$key];
	if (is_array($xGet)) {
		foreach (array_keys($xGet) as $key) {
			$$key = (isset($$key) ? $$key : $xGet[$key]);
		}
	}

	$sa = array("count" => "0", "status" => "", "errMsg" => "", "data" => array());

	$WebService = "192.168.112.6:8880";	// 測試
	//$WebService = "192.168.2.112:8880";	// 正式
	$cipher = "AES-256-CBC";
	$key = "U2FsdGVkX19wb+zKKyWZ8AZJ8rZ3f/Xf";
	$iv = "xrYLG/7pmrb4IHS6";

	$s = "<ctiPolicyDataQueryParamVo>".
		 "<ip>".$ip."</ip>".
		 "<ownerId>".$ownerId."</ownerId>".
		 "<userId>".$userId."</userId>".
		 "<riskCode>".$riskCode."</riskCode>".
		"</ctiPolicyDataQueryParamVo>";
//echo " 1.".$s." 11";
	//$s = "<ctiPolicyDataQueryParamVo><ip>192.168.190.32</ip><ownerId>A123456789</ownerId><riskCode>CAR</riskCode><userId>BI086</userId></ctiPolicyDataQueryParamVo>";
	$ss = openssl_encrypt($s, $cipher, $key, 0, $iv);
//echo " 2.".$ss." 22";
	//echo $ss;

	if ($ss != "") {
		try {
			$client = new SoapClient("http://".$WebService."/CWP/webService/ctiDataQueryService?wsdl");
		}
		catch (Exception $e) {
			writelog("SoapClient Exception:".$e);
			$sa["errMsg"] = MyUrlEncode("SoapClient Exception:".$e);
			print_r(htmlspecialchars(urldecode(json_encode($sa))));
			return;
		}
		try {
			$s1 = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://server.ctiPolicyDataQueryService.webService.cwp.tlg.com/"><soapenv:Header/><soapenv:Body><ser:policyDataQuery><str>';
			$s2 = '</str></ser:policyDataQuery></soapenv:Body></soapenv:Envelope>';
			$sss = $s1.$ss.$s2;
//echo " 3.".$sss." 33";
			//$rs = $client->policyDataQuery($ss);
			$rs = $client->__doRequest($sss, "http://".$WebService."/CWP/webService/ctiDataQueryService?wsdl", "", SOAP_1_1);
//echo " 4.".$rs." 44";
			//echo var_dump($rs);	//Test
//return;
			$doc2 = new DOMDocument();
			$doc2->loadXML($rs);
			$re = $doc2->getElementsByTagName('return')->item(0)->nodeValue;
		}
		catch (Exception $e) {
			writelog("PolicyDataQuery() Exception:".$e);
			$sa["errMsg"] = MyUrlEncode("PolicyDataQuery() Exception:".$e);
			print_r(htmlspecialchars(urldecode(json_encode($sa))));
			return;
		}
		//$xml = openssl_decrypt(MyUrlEncode($re), $cipher, $key, 0, $iv);
		$xml = openssl_decrypt($re, $cipher, $key, 0, $iv);
//echo $xml;
		//$xml = "<cwpCtiResponseVo><errMsg></errMsg><status>S0000</status><count>2</count><ctiCarPolicyDataQueryResultVo><address>700台***中西**</address><birthday>197***/09</birthday><businessnature>J12051</businessnature><email>dp07**</email><extracomcode>A</extracomcode><handler1name>AJ****韻琴</handler1name><handlername>AA6***1249  </handlername><identifynumber>A123***789</identifynumber><insperiod>28-12月-19~28-12月-20</insperiod><insuredname>王**</insuredname><mobile>******</mobile>".
		//		"<phonenumber>0212**</phonenumber><policyno>181***0052</policyno><riskcname>任意險</riskcname><riskcode>A01</riskcode><sumamount>2200000</sumamount><sumpremium>289</sumpremium><brandname>馬自達(國產)</brandname><carkindcode>自用小客車</carkindcode><engineno>DE*****54687</engineno><exhaustscale>1900</exhaustscale><licenseno>A****00</licenseno></ctiCarPolicyDataQueryResultVo><ctiCarPolicyDataQueryResultVo><address>111台北市士林**</address><birthday>1972/01/01</birthday><businessnature>J12051</businessnature>".
		//		"<email>dp07**</email><extracomcode>A</extracomcode><handler1name>A*******琴</handler1name><handlername>AA6****49  </handlername><identifynumber>A123****789</identifynumber><insperiod>25-12月-19~25-12月-20</insperiod><insuredname>T**</insuredname><mobile>******</mobile><phonenumber>0212**</phonenumber><policyno>1812****0028</policyno><riskcname>任意險</riskcname><riskcode>A01</riskcode><sumamount>11100000</sumamount><sumpremium>4663</sumpremium><brandname>裕隆(國產)</brandname><carkindcode>自用小客車</carkindcode>".
		//		"<engineno>998****2A</engineno><exhaustscale>1789</exhaustscale></ctiCarPolicyDataQueryResultVo></cwpCtiResponseVo>";
		
		//$xml = "<cwpCtiResponseVo><errMsg></errMsg><status>S0000</status><count>2</count><ctiFirPolicyDataQueryResultVo><address>545南投*****１號</address><businessnature>000000</businessnature><handler1name>B***5  黃*雅</handler1name><handlername>A0******994  顏*翠</handlername><identifynumber>045****3</identifynumber>".
		//	"<insperiod>2021/12/31~2022/12/31</insperiod><insuredname>法人被保人</insuredname><phonenumber>0255554444</phonenumber><policyno>1800******0280</policyno><riskcname>住宅火險</riskcname><riskcode>F02</riskcode><sumamount>2000000</sumamount><sumpremium>1762</sumpremium>".
		//	"<buildarea>30</buildarea><buildingAddress>236新北**************1號</buildingAddress></ctiFirPolicyDataQueryResultVo><ctiFirPolicyDataQueryResultVo><address>545南投**************１號</address><birthday>1981/01/01</birthday><businessnature>000000</businessnature><handler1name>B***5  黃*雅</handler1name>".
		//	"<handlername>A0***994  顏*翠</handlername><identifynumber>B12****780</identifynumber><insperiod>2021/12/24~2022/12/24</insperiod><insuredname>被保**********長很長</insuredname><phonenumber>0255554444</phonenumber><policyno>180*******0277</policyno><riskcname>住宅火險</riskcname><riskcode>F02</riskcode>".
		//	"<sumamount>2000000</sumamount><sumpremium>1762</sumpremium><buildarea>50</buildarea><buildingAddress>236新*********路168號</buildingAddress></ctiFirPolicyDataQueryResultVo></cwpCtiResponseVo>";
		
		//$xml = "<cwpCtiResponseVo><errMsg></errMsg><status>S0000</status><count>2</count><ctiAhPolicyDataQueryResultVo><address>200******愛區</address><birthday>199***21</birthday><businessnature>000000</businessnature>".
		//	"<handler1name>BK*** 賢</handler1name><handlername>A03*** 淳</handlername><identifynumber>A12***789</identifynumber><insperiod>2021/12/04~2022/12/04</insperiod><insuredname>中信產測試</insuredname><policyno>1805***006</policyno><riskcname>個人傷害險</riskcname><riskcode>PA</riskcode>".
		//	"<sumamount>339000</sumamount><sumpremium>211</sumpremium><mainschemecode>VIP3</mainschemecode><remark>N</remark></ctiAhPolicyDataQueryResultVo><ctiAhPolicyDataQueryResultVo><address>200***愛區</address><birthday>1991/12/21</birthday>".
		//	"<businessnature>000000</businessnature><handler1name>BK*** 賢</handler1name><handlername>A03*** 淳</handlername><identifynumber>A12***89</identifynumber><insperiod>2021/12/02~2022/12/02</insperiod><insuredname>中信產測試</insuredname><policyno>18052******0005</policyno><riskcname>個人傷害險</riskcname>".
		//	"<riskcode>PA</riskcode><sumamount>339000</sumamount><sumpremium>211</sumpremium><mainschemecode>VIP3</mainschemecode><remark>N</remark></ctiAhPolicyDataQueryResultVo></cwpCtiResponseVo>";

		//$xml = "<cwpCtiResponseVo><errMsg></errMsg><status>S0000</status><count>2</count><ctiOthPolicyDataQueryResultVo><address>112臺北市北投區**</address><birthday>198******/12</birthday><businessnature>000000</businessnature><email>samp**</email><handler1name>BJ******霖</handler1name>".
		//	"<handlername>A03******淳</handlername><identifynumber>A1******6789</identifynumber><insperiod>2022/11/02~2023/11/02</insperiod><insuredname>王**</insuredname><mobile>******</mobile><phonenumber>******</phonenumber><policyno>180******00010</policyno><riskcname>寵物綜合保險</riskcname>".
		//	"<riskcode>PE</riskcode><sumamount>140000</sumamount><sumpremium>4267</sumpremium></ctiOthPolicyDataQueryResultVo><ctiOthPolicyDataQueryResultVo><address>106台北市大安**</address><birthday>2009/06/18</birthday><businessnature>000000</businessnature>".
		//	"<email>@****</email><handler1name>BA******淳</handler1name><handlername>A0******淳</handlername><identifynumber>A12******789</identifynumber><insperiod>2022/06/16~2023/06/16</insperiod><insuredname>賴**</insuredname><mobile>****</mobile><phonenumber>****</phonenumber>".
		//	"<policyno>1802******007</policyno><riskcname>公共意外責任保險</riskcname><riskcode>PB</riskcode><sumamount>33000000</sumamount><sumpremium>36580</sumpremium></ctiOthPolicyDataQueryResultVo></cwpCtiResponseVo>";

		if ($xml != "") {
			try {
				$doc = new DOMDocument();
				//$doc->loadXML(iconv("BIG5", "UTF-8", $xml));
				$doc->loadXML($xml);
			}
			catch (Exception $e) {
				writelog("DOMDocument Exception:".$e);
				$sa["errMsg"] = MyUrlEncode("DOMDocument Exception:".$e);
				print_r(htmlspecialchars(urldecode(json_encode($sa))));
				return;
			}
			$errMsg = $doc->getElementsByTagName('errMsg')->item(0)->nodeValue;
			$status = $doc->getElementsByTagName('status')->item(0)->nodeValue;
			$count = $doc->getElementsByTagName('count')->item(0)->nodeValue;
			$sa["count"] = MyUrlEncode($count);
			$sa["status"] = MyUrlEncode($status);
			$sa["errMsg"] = MyUrlEncode($errMsg);

			if ($status == 'S0000') {
				if ($riskCode == "CAR") {
					$qa = $doc->getElementsByTagName('ctiCarPolicyDataQueryResultVo');
					for ($i = 0; $i < $qa->length; $i++) {
						$xml_riskcode = $qa[$i]->getElementsByTagName('riskcode')->item(0)->textContent;
						$address = $qa[$i]->getElementsByTagName('address')->item(0)->textContent;
						$birthday = $qa[$i]->getElementsByTagName('birthday')->item(0)->textContent;
						$businessnature = $qa[$i]->getElementsByTagName('businessnature')->item(0)->textContent;
						$handler1name = $qa[$i]->getElementsByTagName('handler1name')->item(0)->textContent;
						$handlername = $qa[$i]->getElementsByTagName('handlername')->item(0)->textContent;
						$identifynumber = $qa[$i]->getElementsByTagName('identifynumber')->item(0)->textContent;
						$insperiod = $qa[$i]->getElementsByTagName('insperiod')->item(0)->textContent;
						$insuredname = $qa[$i]->getElementsByTagName('insuredname')->item(0)->textContent;
						$policyno = $qa[$i]->getElementsByTagName('policyno')->item(0)->textContent;
						$riskcname = $qa[$i]->getElementsByTagName('riskcname')->item(0)->textContent;
						$sumamount = $qa[$i]->getElementsByTagName('sumamount')->item(0)->textContent;
						$sumpremium = $qa[$i]->getElementsByTagName('sumpremium')->item(0)->textContent;
						$phonenumber = $qa[$i]->getElementsByTagName('phonenumber')->item(0)->textContent;
						$mobile = $qa[$i]->getElementsByTagName('mobile')->item(0)->textContent;
						$email = $qa[$i]->getElementsByTagName('email')->item(0)->textContent;
						$extracomcode = $qa[$i]->getElementsByTagName('extracomcode')->item(0)->textContent;
						$printno = $qa[$i]->getElementsByTagName('printno')->item(0)->textContent;
						$brandname = $qa[$i]->getElementsByTagName('brandname')->item(0)->textContent;
						$carkindcode = $qa[$i]->getElementsByTagName('carkindcode')->item(0)->textContent;
						$engineno = $qa[$i]->getElementsByTagName('engineno')->item(0)->textContent;
						$exhaustscale = $qa[$i]->getElementsByTagName('exhaustscale')->item(0)->textContent;
						$licenseno = $qa[$i]->getElementsByTagName('licenseno')->item(0)->textContent;

						if ((substr($phonenumber, 0, 2) == "09" ) && (substr($mobile, 0, 2) != "09" )) {
							$tmp = $phonenumber;
							$phonenumber = $mobile;
							$mobile = $tmp;
						}

						$sa["data"][$i] = array();
						$sa["data"][$i]["address"] = MyUrlEncode($address);
						$sa["data"][$i]["birthday"] = MyUrlEncode($birthday);
						$sa["data"][$i]["businessnature"] = MyUrlEncode($businessnature);
						$sa["data"][$i]["handler1name"] = MyUrlEncode($handler1name);
						$sa["data"][$i]["handlername"] = MyUrlEncode($handlername);
						$sa["data"][$i]["identifynumber"] = MyUrlEncode($identifynumber);
						$sa["data"][$i]["insperiod"] = MyUrlEncode($insperiod);
						$sa["data"][$i]["insuredname"] = MyUrlEncode($insuredname);
						$sa["data"][$i]["policyno"] = MyUrlEncode($policyno);
						$sa["data"][$i]["riskcode"] = MyUrlEncode($xml_riskcode);
						$sa["data"][$i]["riskcname"] = MyUrlEncode($riskcname);
						$sa["data"][$i]["sumamount"] = MyUrlEncode($sumamount);
						$sa["data"][$i]["sumpremium"] = MyUrlEncode($sumpremium);
						$sa["data"][$i]["phonenumber"] = MyUrlEncode($phonenumber);
						$sa["data"][$i]["mobile"] = MyUrlEncode($mobile);
						$sa["data"][$i]["email"] = MyUrlEncode($email);
						$sa["data"][$i]["extracomcode"] = MyUrlEncode($extracomcode);
						$sa["data"][$i]["printno"] = MyUrlEncode($printno);
						$sa["data"][$i]["brandname"] = MyUrlEncode($brandname);
						$sa["data"][$i]["carkindcode"] = MyUrlEncode($carkindcode);
						$sa["data"][$i]["engineno"] = MyUrlEncode($engineno);						
						$sa["data"][$i]["exhaustscale"] = MyUrlEncode($exhaustscale);
						$sa["data"][$i]["licenseno"] = MyUrlEncode($licenseno);
					}
				}
				elseif ($riskCode == "FIR") {
					$qa = $doc->getElementsByTagName('ctiFirPolicyDataQueryResultVo');
					for ($i = 0; $i < $qa->length; $i++) {
						$xml_riskcode = $qa[$i]->getElementsByTagName('riskcode')->item(0)->textContent;
						$address = $qa[$i]->getElementsByTagName('address')->item(0)->textContent;
						$birthday = $qa[$i]->getElementsByTagName('birthday')->item(0)->textContent;
						$businessnature = $qa[$i]->getElementsByTagName('businessnature')->item(0)->textContent;
						$handler1name = $qa[$i]->getElementsByTagName('handler1name')->item(0)->textContent;
						$handlername = $qa[$i]->getElementsByTagName('handlername')->item(0)->textContent;
						$identifynumber = $qa[$i]->getElementsByTagName('identifynumber')->item(0)->textContent;
						$insperiod = $qa[$i]->getElementsByTagName('insperiod')->item(0)->textContent;
						$insuredname = $qa[$i]->getElementsByTagName('insuredname')->item(0)->textContent;
						$policyno = $qa[$i]->getElementsByTagName('policyno')->item(0)->textContent;
						$riskcname = $qa[$i]->getElementsByTagName('riskcname')->item(0)->textContent;
						$sumamount = $qa[$i]->getElementsByTagName('sumamount')->item(0)->textContent;
						$sumpremium = $qa[$i]->getElementsByTagName('sumpremium')->item(0)->textContent;
						$phonenumber = $qa[$i]->getElementsByTagName('phonenumber')->item(0)->textContent;
						$mobile = $qa[$i]->getElementsByTagName('mobile')->item(0)->textContent;
						$email = $qa[$i]->getElementsByTagName('email')->item(0)->textContent;
						$extracomcode = $qa[$i]->getElementsByTagName('extracomcode')->item(0)->textContent;
						$mortgageepeople = $qa[$i]->getElementsByTagName('mortgageepeople')->item(0)->textContent;
						$buildarea = $qa[$i]->getElementsByTagName('buildarea')->item(0)->textContent;
						$buildingAddress = $qa[$i]->getElementsByTagName('buildingAddress')->item(0)->textContent;

						if ((substr($phonenumber, 0, 2) == "09" ) && (substr($mobile, 0, 2) != "09" )) {
							$tmp = $phonenumber;
							$phonenumber = $mobile;
							$mobile = $tmp;
						}

						$sa["data"][$i] = array();
						$sa["data"][$i]["address"] = MyUrlEncode($address);
						$sa["data"][$i]["birthday"] = MyUrlEncode($birthday);
						$sa["data"][$i]["businessnature"] = MyUrlEncode($businessnature);
						$sa["data"][$i]["handler1name"] = MyUrlEncode($handler1name);
						$sa["data"][$i]["handlername"] = MyUrlEncode($handlername);
						$sa["data"][$i]["identifynumber"] = MyUrlEncode($identifynumber);
						$sa["data"][$i]["insperiod"] = MyUrlEncode($insperiod);
						$sa["data"][$i]["insuredname"] = MyUrlEncode($insuredname);
						$sa["data"][$i]["policyno"] = MyUrlEncode($policyno);
						$sa["data"][$i]["riskcode"] = MyUrlEncode($xml_riskcode);
						$sa["data"][$i]["riskcname"] = MyUrlEncode($riskcname);
						$sa["data"][$i]["sumamount"] = MyUrlEncode($sumamount);
						$sa["data"][$i]["sumpremium"] = MyUrlEncode($sumpremium);
						$sa["data"][$i]["phonenumber"] = MyUrlEncode($phonenumber);
						$sa["data"][$i]["mobile"] = MyUrlEncode($mobile);
						$sa["data"][$i]["email"] = MyUrlEncode($email);
						$sa["data"][$i]["extracomcode"] = MyUrlEncode($extracomcode);
						$sa["data"][$i]["mortgageepeople"] = MyUrlEncode($mortgageepeople);
						$sa["data"][$i]["buildarea"] = MyUrlEncode($buildarea);
						$sa["data"][$i]["buildingAddress"] = MyUrlEncode($buildingAddress);
					}
				}
				elseif ($riskCode == "AH") {
					$qa = $doc->getElementsByTagName('ctiAhPolicyDataQueryResultVo');
					for ($i = 0; $i < $qa->length; $i++) {
						$xml_riskcode = $qa[$i]->getElementsByTagName('riskcode')->item(0)->textContent;
						$address = $qa[$i]->getElementsByTagName('address')->item(0)->textContent;
						$birthday = $qa[$i]->getElementsByTagName('birthday')->item(0)->textContent;
						$businessnature = $qa[$i]->getElementsByTagName('businessnature')->item(0)->textContent;
						$handler1name = $qa[$i]->getElementsByTagName('handler1name')->item(0)->textContent;
						$handlername = $qa[$i]->getElementsByTagName('handlername')->item(0)->textContent;
						$identifynumber = $qa[$i]->getElementsByTagName('identifynumber')->item(0)->textContent;
						$insperiod = $qa[$i]->getElementsByTagName('insperiod')->item(0)->textContent;
						$insuredname = $qa[$i]->getElementsByTagName('insuredname')->item(0)->textContent;
						$policyno = $qa[$i]->getElementsByTagName('policyno')->item(0)->textContent;
						$riskcname = $qa[$i]->getElementsByTagName('riskcname')->item(0)->textContent;
						$sumamount = $qa[$i]->getElementsByTagName('sumamount')->item(0)->textContent;
						$sumpremium = $qa[$i]->getElementsByTagName('sumpremium')->item(0)->textContent;
						$phonenumber = $qa[$i]->getElementsByTagName('phonenumber')->item(0)->textContent;
						$mobile = $qa[$i]->getElementsByTagName('mobile')->item(0)->textContent;
						$email = $qa[$i]->getElementsByTagName('email')->item(0)->textContent;
						$extracomcode = $qa[$i]->getElementsByTagName('extracomcode')->item(0)->textContent;
						$mainschemecode = $qa[$i]->getElementsByTagName('mainschemecode')->item(0)->textContent;
						$creditcardno = $qa[$i]->getElementsByTagName('creditcardno')->item(0)->textContent;
						$payername = $qa[$i]->getElementsByTagName('payername')->item(0)->textContent;
						$remark = $qa[$i]->getElementsByTagName('remark')->item(0)->textContent;

						if ((substr($phonenumber, 0, 2) == "09" ) && (substr($mobile, 0, 2) != "09" )) {
							$tmp = $phonenumber;
							$phonenumber = $mobile;
							$mobile = $tmp;
						}

						$sa["data"][$i] = array();
						$sa["data"][$i]["address"] = MyUrlEncode($address);
						$sa["data"][$i]["birthday"] = MyUrlEncode($birthday);
						$sa["data"][$i]["businessnature"] = MyUrlEncode($businessnature);
						$sa["data"][$i]["handler1name"] = MyUrlEncode($handler1name);
						$sa["data"][$i]["handlername"] = MyUrlEncode($handlername);
						$sa["data"][$i]["identifynumber"] = MyUrlEncode($identifynumber);
						$sa["data"][$i]["insperiod"] = MyUrlEncode($insperiod);
						$sa["data"][$i]["insuredname"] = MyUrlEncode($insuredname);
						$sa["data"][$i]["policyno"] = MyUrlEncode($policyno);
						$sa["data"][$i]["riskcode"] = MyUrlEncode($xml_riskcode);
						$sa["data"][$i]["riskcname"] = MyUrlEncode($riskcname);
						$sa["data"][$i]["sumamount"] = MyUrlEncode($sumamount);
						$sa["data"][$i]["sumpremium"] = MyUrlEncode($sumpremium);
						$sa["data"][$i]["phonenumber"] = MyUrlEncode($phonenumber);
						$sa["data"][$i]["mobile"] = MyUrlEncode($mobile);
						$sa["data"][$i]["email"] = MyUrlEncode($email);
						$sa["data"][$i]["extracomcode"] = MyUrlEncode($extracomcode);
						$sa["data"][$i]["mainschemecode"] = MyUrlEncode($mainschemecode);
						$sa["data"][$i]["creditcardno"] = MyUrlEncode($creditcardno);
						$sa["data"][$i]["payername"] = MyUrlEncode($payername);
						$sa["data"][$i]["remark"] = MyUrlEncode($remark);
					}
				}
				elseif ($riskCode == "OTH") {
					$qa = $doc->getElementsByTagName('ctiOthPolicyDataQueryResultVo');
					for ($i = 0; $i < $qa->length; $i++) {					
						$xml_riskcode = $qa[$i]->getElementsByTagName('riskcode')->item(0)->textContent;
						$address = $qa[$i]->getElementsByTagName('address')->item(0)->textContent;
						$birthday = $qa[$i]->getElementsByTagName('birthday')->item(0)->textContent;
						$businessnature = $qa[$i]->getElementsByTagName('businessnature')->item(0)->textContent;
						$handler1name = $qa[$i]->getElementsByTagName('handler1name')->item(0)->textContent;
						$handlername = $qa[$i]->getElementsByTagName('handlername')->item(0)->textContent;
						$identifynumber = $qa[$i]->getElementsByTagName('identifynumber')->item(0)->textContent;
						$insperiod = $qa[$i]->getElementsByTagName('insperiod')->item(0)->textContent;
						$insuredname = $qa[$i]->getElementsByTagName('insuredname')->item(0)->textContent;
						$policyno = $qa[$i]->getElementsByTagName('policyno')->item(0)->textContent;
						$riskcname = $qa[$i]->getElementsByTagName('riskcname')->item(0)->textContent;
						$sumamount = $qa[$i]->getElementsByTagName('sumamount')->item(0)->textContent;
						$sumpremium = $qa[$i]->getElementsByTagName('sumpremium')->item(0)->textContent;
						$phonenumber = $qa[$i]->getElementsByTagName('phonenumber')->item(0)->textContent;
						$mobile = $qa[$i]->getElementsByTagName('mobile')->item(0)->textContent;
						$email = $qa[$i]->getElementsByTagName('email')->item(0)->textContent;
						$extracomcode = $qa[$i]->getElementsByTagName('extracomcode')->item(0)->textContent;
						$mainschemecode = $qa[$i]->getElementsByTagName('mainschemecode')->item(0)->textContent;
						$remark = $qa[$i]->getElementsByTagName('remark')->item(0)->textContent;

						if ((substr($phonenumber, 0, 2) == "09" ) && (substr($mobile, 0, 2) != "09" )) {
							$tmp = $phonenumber;
							$phonenumber = $mobile;
							$mobile = $tmp;
						}

						$sa["data"][$i] = array();
						$sa["data"][$i]["address"] = MyUrlEncode($address);
						$sa["data"][$i]["birthday"] = MyUrlEncode($birthday);
						$sa["data"][$i]["businessnature"] = MyUrlEncode($businessnature);
						$sa["data"][$i]["handler1name"] = MyUrlEncode($handler1name);
						$sa["data"][$i]["handlername"] = MyUrlEncode($handlername);
						$sa["data"][$i]["identifynumber"] = MyUrlEncode($identifynumber);
						$sa["data"][$i]["insperiod"] = MyUrlEncode($insperiod);
						$sa["data"][$i]["insuredname"] = MyUrlEncode($insuredname);
						$sa["data"][$i]["policyno"] = MyUrlEncode($policyno);
						$sa["data"][$i]["riskcode"] = MyUrlEncode($xml_riskcode);
						$sa["data"][$i]["riskcname"] = MyUrlEncode($riskcname);
						$sa["data"][$i]["sumamount"] = MyUrlEncode($sumamount);
						$sa["data"][$i]["sumpremium"] = MyUrlEncode($sumpremium);
						$sa["data"][$i]["phonenumber"] = MyUrlEncode($phonenumber);
						$sa["data"][$i]["mobile"] = MyUrlEncode($mobile);
						$sa["data"][$i]["email"] = MyUrlEncode($email);
						$sa["data"][$i]["extracomcode"] = MyUrlEncode($extracomcode);
						$sa["data"][$i]["mainschemecode"] = MyUrlEncode($mainschemecode);
						$sa["data"][$i]["remark"] = MyUrlEncode($remark);
					}
				}
				//print_r($sa);
			}
		}	
	}
	
	print_r(htmlspecialchars(base64_encode(json_encode($sa))));
	//echo urldecode(json_encode($sa));	//Test

	function MyUrlEncode($s) {
		$entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
		$replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
		return str_replace($entities, $replacements, urlencode($s));
	}

	function writelog($s) {
		global $path;

		$st = date("Y/m/d H:i:s");
		$filename = "../log/InsQuery".date("Ymd").".log";
		$fp = fopen($filename, "a");
		fputs($fp, $st." -- ".$s."\r\n");
		fclose($fp);
	}
?>