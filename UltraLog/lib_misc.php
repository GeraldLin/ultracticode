<?php
	date_default_timezone_set('Asia/Taipei');

	// ���t�ήɶ�
	function GetSystemTime() {
		$s = date("Y/m/d H:i:s");
		return $s;
	}

	// ���t�Τ��
	function ComputeDate($d_diffcnt) {
		$s = date("Y/m/d", (time() + $d_diffcnt));
		return $s;
	}

	// ���t�ήɶ�
	function ComputeTime($d_diffcnt) {
		$s = date("Y/m/d H:i:s", (time() + $d_diffcnt));
		return $s;
	}

	// �p��ɶ��t
	function GetDiffTime($s_srctime) {
		//$s = time() - strtotime($s_srctime);
		$dateTime = new DateTime($s_srctime);
		$s = time() - $dateTime->format('U');
		return $s;
	}

	// ���t�άP��
	function GetWeekDay($s_datestr) {

		$s = "0";
		if(preg_match("/^[1-9][0-9]{3}(\/|-|.)(0[1-9]|1[0-2])(\/|-|.)(0[1-9]|[1|2][0-9]|3[0|1])$/", $s_datestr)){
			if (checkdate(substr($str, 5, 2), substr($str, 8, 2), substr($str, 0, 4))) {
				$s = date("w", strtotime($s_datestr));
			}
			else {
				$s = date("w");
			}
		}
		else {
			$s = date("w");
		}
		return ($s+1);
	}

	// �P�_�ɮ׬O�_�s�b
	function CheckFile($filepath) {
		$s = "1";
		if (file_exists($filepath)) {
			$s = "0";
		}
		return $s;
	}

	function CheckFileSHA256($EncodeFormat, $FileName, $ExtraSHA256) {
		$FILE_SHA256 = 0;
		$FILE_NOT_SHA256 = 1;
		
		$ret = $FILE_NOT_SHA256;
		if (file_exists($FileName)) {
			$SEGMENT_SIZE = GetValueOfOneSecond($EncodeFormat);
			
			$str = "";
			$handle = fopen($FileName, "rb");
			if ($handle) {
				while (!feof($handle)) {
					$InBuffer = fread($handle, $SEGMENT_SIZE);
					//echo dechex(ord($InBuffer[0]));
					$str .= $InBuffer[0];
				}
				fclose($handle);
			}
			$SHA256Str = hash("sha256", $str);
			if ($SHA256Str = $ExtraSHA256) {
				$ret = $FILE_SHA256;
			}
		}
		return $ret;
	}
	
	Function GetValueOfOneSecond($EncodeFormatMode) {
		$FORMAT_PCM = 0;
		$FORMAT_OKI_ADPCM_6K = 1;
		$FORMAT_OKI_ADPCM_8K = 2;
		$FORMAT_MSGSM_WAVE = 3;
		$FORMAT_G723 = 4;
		$FORMAT_DVI_ADPCM_8K = 5;
		$FORMAT_DVI_ADPCM_RAW = 6;
		$FORMAT_PCM_ALAW = 8;
		$FORMAT_G729A_WAVE = 18;
		$FORMAT_LINEAR_16 = 20;
		$FORMAT_LINEAR_U8 = 21;
		$FORMAT_G723_RAW = 104;

		$ret = 0;
		switch ($EncodeFormatMode) {
			Case $FORMAT_OKI_ADPCM_6K:      $ret = 3000;	break;
			Case $FORMAT_OKI_ADPCM_8K:      $ret = 4000;	break;
			Case $FORMAT_PCM:               $ret = 8000;	break;
			Case $FORMAT_PCM_ALAW:          $ret = 8000;	break;
			Case $FORMAT_LINEAR_16:         $ret = 16000;	break;
			Case $FORMAT_LINEAR_U8:         $ret = 8000;	break;
			Case $FORMAT_G723:              $ret = 662.5;	break;
			Case $FORMAT_G729A_WAVE:        $ret = 1000;	break;
			Case $FORMAT_DVI_ADPCM_8K:      $ret = 4055;	break;
			Case $FORMAT_MSGSM_WAVE:        $ret = 1625;	break;
			Case $FORMAT_DVI_ADPCM_RAW:     $ret = 4055;	break;
			Case $FORMAT_G723_RAW:          $ret = 662.5;	break;
			default:                  		$ret = 4000;	break;
		}
		return $ret;
	}
?>