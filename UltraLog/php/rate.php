﻿<?php
    include("lib_declare.php");
	include("PhpOffice/vendor/autoload.php");

	/*foreach (array_keys($_POST) as $key) $$key = $_POST[$key];
	foreach (array_keys($_GET) as $key) {
		$$key = (isset($$key) ? $$key : $_GET[$key]);
	}*/
	$xPost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$xGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	if (is_array($xPost)) foreach (array_keys($xPost) as $key) $$key = $xPost[$key];
	if (is_array($xGet)) {
		foreach (array_keys($xGet) as $key) {
			$$key = (isset($$key) ? $$key : $xGet[$key]);
		}
	}
    // echo $start." ".$end;
    

    $Language = "Big5";				//readregini("System", "Database", "Language");
    $dbstage = "192.168.106.17";				//readregini("System", "Database", "DBStage");

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    try{

        $cn = new COM("ADODB.Connection");
        $rs = new COM("ADODB.Recordset");
        $strConnect = "Provider=".SQLODBCSTR.";Data Source=".$dbstage.";User Id=uivr;Password=uivr";
        $cn->Open($strConnect);


        $sql = "SELECT WorkerName,WorkerNo From [callcenterdb].[dbo].[tbworker]";
        
        if ($sql != "") {
            //echo $sql;
            $rs = $cn->Execute($sql);
            $a = array();
            $b = array();
            $num = array();
            while(!$rs->EOF){
                $WorkerName = iconv("big5", "utf-8", $rs->Fields['WorkerName']->Value);
                $WName = $rs->Fields['WorkerName']->Value;
                $WorkerNo = $rs->Fields['WorkerNo']->Value;
                $a[count($a)] = $WorkerName;
                $b[count($b)] = $WName;
                $num[count($num)] = $WorkerNo;
                $rs->MoveNext;
            }
        }
        //var_dump($a);
        //return;
        
        $rs->Close;
        $cn->Close;
        
        $strConnect = "Provider=".SQLODBCSTR.";Data Source=".$dbstage.";User Id=uivr;Password=uivr";
        $cn->Open($strConnect);
        
        $spreadsheet = new Spreadsheet();
        
        $k = 0;
        while ($k < count($a)){

            $start2 = substr($start,0,4)."-".substr($start,5,2)."-".substr($start,8,2);
            $end2 = substr($end,0,4)."-".substr($end,5,2)."-".substr($end,8,2);

            $strSQL = "SELECT CallTime, CallerNo, Score FROM [callcenterdb].[dbo].[tbcallcdr] ";
            $strSQL .= "WHERE CallTime between '".$start2." 00:00:00' and '".$end2." 23:59:59' and WorkerNo = '".$num[$k]."'";

            if ($strSQL != "") {
                if($k == 0){
                    $sheet = $spreadsheet->getActiveSheet();
                    $rs = $cn->Execute($strSQL);
                    $i = 3;

                    $workerID = $num[$k].' 客服-'.$a[$k];
                    $sheet->setTitle($workerID);
                    $sheet->setCellValue('A1', $workerID);
                    $sheet->setCellValue('C1', $start.' ~ '.$end);
                    while(!$rs->EOF){
                        $CallTime = $rs->Fields['CallTime']->Value;
                        $CallerNo = $rs->Fields['CallerNo']->Value;
                        $OrgScore = strval($rs->Fields['Score']->Value);
                        $fscore = substr($OrgScore, 1);
                        $lscore = substr($OrgScore, -1);
                        
                        $sheet->setCellValue('A'.$i, $CallTime);
                        $sheet->setCellValue('B'.$i, $CallerNo);
                        $sheet->setCellValue('C'.$i, $fscore);
                        $sheet->setCellValue('D'.$i, $lscore);
                        $i++;
                        $rs->MoveNext;
                    }
                    $sheet->setCellValue('B1', '日期範圍');
                    $sheet->setCellValue('A2', '來電時間');
                    $sheet->setCellValue('B2', '來電號碼');
                    $sheet->setCellValue('C2', '服務人員服務');
                    $sheet->setCellValue('D2', '服務熱忱、親切有禮、耐心答覆');
                    
                    $sheet->getColumnDimension('A')->setWidth(19);
                    $sheet->getColumnDimension('B')->setWidth(9);
                    $sheet->getColumnDimension('C')->setWidth(24);
                    $sheet->getColumnDimension('D')->setWidth(31);
                }else{
                    $sheet = $spreadsheet->createSheet();
                    $rs = $cn->Execute($strSQL);
                    $i = 3;

                    $workerID = $num[$k].' 客服-'.$a[$k];
                    $sheet->setTitle($workerID);
                    $sheet->setCellValue('A1', $workerID);
                    $sheet->setCellValue('C1', $start.' ~ '.$end);
                    while(!$rs->EOF){
                        $CallTime = $rs->Fields['CallTime']->Value;
                        $CallerNo = $rs->Fields['CallerNo']->Value;
                        $OrgScore = strval($rs->Fields['Score']->Value);
                        $fscore = substr($OrgScore, 1);
                        $lscore = substr($OrgScore, -1);

                        $sheet->setCellValue('A'.$i, $CallTime);
                        $sheet->setCellValue('B'.$i, $CallerNo);
                        $sheet->setCellValue('C'.$i, $fscore);
                        $sheet->setCellValue('D'.$i, $lscore);
                        $i++;
                        $rs->MoveNext;
                    }
                    $sheet->setCellValue('B1', '日期範圍');
                    $sheet->setCellValue('A2', '來電時間');
                    $sheet->setCellValue('B2', '來電號碼');
                    $sheet->setCellValue('C2', '服務人員服務');
                    $sheet->setCellValue('D2', '服務熱忱、親切有禮、耐心答覆');
                    
                    $sheet->getColumnDimension('A')->setWidth(19);
                    $sheet->getColumnDimension('B')->setWidth(9);
                    $sheet->getColumnDimension('C')->setWidth(24);
                    $sheet->getColumnDimension('D')->setWidth(31);
                }
            }
            $k++;
        }
        $rs->Close;
        $cn->Close;

        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle('值機滿意度調查');
        $sheet->setCellValue('A1', '日期範圍');
        $sheet->setCellValue('C1', '服務人員服務');
        $sheet->setCellValue('G1', '服務熱忱、親切有禮、耐心答覆');
        $sheet->mergeCells('C1:F1');
        $sheet->mergeCells('G1:J1');
        $sheet->setCellValue('B1', $start.' ~ '.$end);
        $sheet->setCellValue('A2', '值機員');
        $sheet->setCellValue('B2', '全部通數');
        $sheet->setCellValue('C2', '非常滿意');
        $sheet->setCellValue('D2', '滿意');
        $sheet->setCellValue('E2', '不滿意');
        $sheet->setCellValue('F2', '未評價');
        $sheet->setCellValue('G2', '非常滿意');
        $sheet->setCellValue('H2', '滿意');
        $sheet->setCellValue('I2', '不滿意');
        $sheet->setCellValue('J2', '未評價');

        
        $strConnect = "Provider=".SQLODBCSTR.";Data Source=".$dbstage.";User Id=uivr;Password=uivr";
        $cn->Open($strConnect);

        $k = 0;
        $i = 3;

        $Date_List_a1=explode("/",$end);
        $Date_List_a2=explode("/",$start);

        $d1=mktime(0,0,0,(int)$Date_List_a1[1],(int)$Date_List_a1[2],(int)$Date_List_a1[0]);
        $d2=mktime(0,0,0,(int)$Date_List_a2[1],(int)$Date_List_a2[2],(int)$Date_List_a2[0]);
        $Days=round(($d1-$d2)/3600/24) + 1;

        $Day1 = $start;
        while ($k < count($b)){
            
            //$wkn = iconv("utf-8","big5",$a[$k]);
            $start = $Day1;
            $Date = 0;
            while($Date < $Days) {

                $strSQL = "SELECT tbworker.WorkerNo, WorkerName, Score From [callcenterdb].[dbo].[tbWorker] ";
                $strSQL .= "left join [callcenterdb].[dbo].[tbcallcdr] on tbworker.WorkerNo = tbcallcdr.WorkerNo ";
                $strSQL .= "WHERE CallTime between '".$start." 00:00:00' and '".$start." 23:59:59' and WorkerName = '".$b[$k]."' and tbworker.WorkerNo = '".$num[$k]."'";
                
                $Date_List_a2=explode("/",$start);
                $Day = $Date_List_a2[1]."/".$Date_List_a2[2];
                
                $workerID = $Day." | ".$num[$k].' 客服-'.$a[$k];
                $sheet->setCellValue('A'.$i, $workerID);
                if ($strSQL != "") {
                    $rs = $cn->Execute($strSQL);
                    //echo $strSQL;
                    $array = array();
                    while (!$rs->EOF) {
                        $Score = strval($rs->Fields['Score']->Value);
                        $frate = substr($Score, 1);
                        $lrate = substr($Score, -1);
                        
                        $array[1][$frate]++;
                        $array[2][$lrate]++;

                        $rs->MoveNext;
                    }
                    
                    $sheet->setCellValue('C'.$i, $array[1][1]);
                    $sheet->setCellValue('D'.$i, $array[1][2]);
                    $sheet->setCellValue('E'.$i, $array[1][3]);
                    $sheet->setCellValue('F'.$i, $array[1][0]);
                    $sheet->setCellValue('G'.$i, $array[2][1]);
                    $sheet->setCellValue('H'.$i, $array[2][2]);
                    $sheet->setCellValue('I'.$i, $array[2][3]);
                    $sheet->setCellValue('J'.$i, $array[2][0]);
                    $sheet->setCellValue('B'.$i, '=C'.$i.'+D'.$i.'+E'.$i.'+F'.$i.'+G'.$i.'+H'.$i.'+I'.$i.'+J'.$i);
                }
                $Date++;
                $start = date ("Y/m/d", strtotime("+1 days", strtotime($start)));
                $i++;
            }
            $k++;
        }
        
        $sheet->getColumnDimension('A')->setWidth(23);
        $sheet->getColumnDimension('B')->setWidth(22);
        $sheet->getColumnDimension('C')->setWidth(10);
        $sheet->getColumnDimension('D')->setWidth(10);
        $sheet->getColumnDimension('E')->setWidth(10);
        $sheet->getColumnDimension('F')->setWidth(10);
        $sheet->getColumnDimension('G')->setWidth(10);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(10);
        $sheet->getColumnDimension('J')->setWidth(10);
        $sheet->getColumnDimension('K')->setWidth(10);

        $FileName = $start.'.xlsx';
        
        $writer = new Xlsx($spreadsheet);
        $writer->save("../component/temp/".$FileName);

        if (file_exists("../component/temp/".$FileName)) {
            print_r(htmlspecialchars($FileName));
        }
    }
    catch (Exception $e) {
        print_r(htmlspecialchars($e));
        //echo "";
    }

?>