﻿<?php
    include("lib_declare.php");
	include("PhpOffice/vendor/autoload.php");

	/*foreach (array_keys($_POST) as $key) $$key = $_POST[$key];
	foreach (array_keys($_GET) as $key) {
		$$key = (isset($$key) ? $$key : $_GET[$key]);
	}*/
	$xPost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$xGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	if (is_array($xPost)) foreach (array_keys($xPost) as $key) $$key = $xPost[$key];
	if (is_array($xGet)) {
		foreach (array_keys($xGet) as $key) {
			$$key = (isset($$key) ? $$key : $xGet[$key]);
		}
	}

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    $Language = "Big5";				//readregini("System", "Database", "Language");
    $dbstage = "192.168.106.17";				//readregini("System", "Database", "DBStage");
    $year = $year+1911;
    if (strlen($month) == 1) {
        $month = "0".$month;
    }

    $today = date("Y-m-d");
    $firstday = date("Y-m-01", strtotime(date("$year-$month-d")));
    $monthlast = date("Y-m-d", strtotime("$firstday +1 month -1 day"));

    if (strtotime($today) < strtotime($monthlast)){
        $lastday = $today;
    } else {
        $lastday = $monthlast;
    }

    // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    // $spreadsheet = $reader->load($fileName);
	
	$myfileName = '../component/DataSample.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($myfileName);

    $sheet = $spreadsheet->getActiveSheet(0);
    $sheet->setTitle($month."月");
    $sheet->setCellValue('I1', $year."年".$month."月客服每日電話紀錄表");
    
    $cn = new COM("ADODB.Connection");
    $rs = new COM("ADODB.Recordset");
    $strConnect = "Provider=".SQLODBCSTR.";Data Source=".$dbstage.";Initial catalog=callcenterdb;User Id=uivr;Password=uivr";
    //echo $strConnect;
    $cn->Open($strConnect);

    $SQLstr = "SELECT CallTime, tbprodtype.ProdName, tbprodtype.ProdType, tbacceptex.ItemData1, tbacceptex.ItemData2, tbacceptex.ItemData3, tbacceptex.ItemData4,";
    $SQLstr .= " tbacceptex.ItemData5, tbacceptex.ItemData6, A1.CustName, A1.CustTypeName, A1.ItemData2 as Id, A1.MobileNo, tbcallcdr.SrvType, tbsrvsubtype.SrvSubName,";
    $SQLstr .= " tbaccept.AcptCont, tbaccept.DealTime, (datediff(n, AcptTime, DealTime)) as hour, datediff(n, AcptTime, GETDATE()) as minute,  tbaccept.AcptSubType FROM tbcallcdr";
    $SQLstr .= " left join tbaccept on tbcallcdr.SerialNo = tbaccept.SerialNo";
    $SQLstr .= " left join tbprodtype on tbcallcdr.ProdType = tbprodtype.ProdType";
    $SQLstr .= " left join tbacceptex on tbcallcdr.SerialNo = tbacceptex.SerialNo";
    $SQLstr .= " left join (Select CustTypeName, CustName, ItemData2, CustomNo, MobileNo From tbcusttype";
    $SQLstr .= " left join tbcustomer on tbcustomer.CustType = tbcusttype.CustType) A1 on tbcallcdr.CustomNo = A1.CustomNo";
    $SQLstr .= " left join tbsrvsubtype on tbcallcdr.SrvSubType = tbsrvsubtype.SrvSubType";
    $SQLstr .= " where tbprodtype.ProdType not in ('9', '11', '14') and CallTime between '".$firstday." 00:00:00' and '".$lastday." 23:59:59'";
    $SQLstr .= " order by CallTime ASC";

    if($SQLstr != ""){

        $rs = $cn->Execute($SQLstr);
        $i = 4;
        while(!$rs->EOF){

            $CallTime = iconv("big5", "utf-8", $rs->Fields['CallTime']->Value);
            $ProdType = iconv("big5", "utf-8", $rs->Fields['ProdType']->Value);
            $ProdName = iconv("big5", "utf-8", $rs->Fields['ProdName']->Value);
            $CustTypeName = iconv("big5", "utf-8", $rs->Fields['CustTypeName']->Value);
            $CustName = iconv("big5", "utf-8", $rs->Fields['CustName']->Value);
            $CustId = $rs->Fields['Id']->Value;
            $PhoneNo = $rs->Fields['MobileNo']->Value;
            $DealTime = iconv("big5", "utf-8", $rs->Fields['DealTime']->Value);
            $DealA = $rs->Fields['hour']->Value;
            $TAcpt = $rs->Fields['minute']->Value;
            $SrvType = $rs->Fields['SrvType']->Value;
            $SrvSubName = iconv("big5", "utf-8", $rs->Fields['SrvSubName']->Value);
            $YZS = $rs->Fields['AcptSubType']->Value;
            $AcptCont = iconv("big5", "utf-8", $rs->Fields['AcptCont']->Value);

            if ($ProdType = "1" || $ProdType = "2") {
                $carnum = iconv("big5", "utf-8", $rs->Fields['ItemData1']->Value);
                $CardType = iconv("big5", "utf-8", $rs->Fields['ItemData2']->Value);
                $QType = iconv("big5", "utf-8", $rs->Fields['ItemData3']->Value);
                $InsNo = iconv("big5", "utf-8", $rs->Fields['ItemData4']->Value);
                $Cust = iconv("big5", "utf-8", $rs->Fields['ItemData5']->Value);
                
                $sheet->setCellValue('C'.$i, $Cust);
                $sheet->setCellValue('F'.$i, $CardType);
                $sheet->setCellValue('G'.$i, $QType);
                $sheet->setCellValue('J'.$i, $insnum);
                $sheet->setCellValue('K'.$i, $carnum);
            }
            if ($ProdType = "3" || $ProdType = "4") {
                $FireType = iconv("big5", "utf-8", $rs->Fields['ItemData2']->Value);
                $CardType = iconv("big5", "utf-8", $rs->Fields['ItemData3']->Value);
                $QType = iconv("big5", "utf-8", $rs->Fields['ItemData4']->Value);
                $InsNo = iconv("big5", "utf-8", $rs->Fields['ItemData5']->Value);
                $Cust = iconv("big5", "utf-8", $rs->Fields['ItemData6']->Value);

                $sheet->setCellValue('C'.$i, $Cust);
                $sheet->setCellValue('E'.$i, $FireType);
                $sheet->setCellValue('F'.$i, $CardType);
                $sheet->setCellValue('G'.$i, $QType);
                $sheet->setCellValue('J'.$i, $insnum);
            }
            else {
                $CardType = iconv("big5", "utf-8", $rs->Fields['ItemData1']->Value);
                $QType = iconv("big5", "utf-8", $rs->Fields['ItemData2']->Value);
                $InsNo = iconv("big5", "utf-8", $rs->Fields['ItemData3']->Value);
                $Cust = iconv("big5", "utf-8", $rs->Fields['ItemData4']->Value);
                
                $sheet->setCellValue('C'.$i, $Cust);
                $sheet->setCellValue('F'.$i, $CardType);
                $sheet->setCellValue('G'.$i, $QType);
                $sheet->setCellValue('J'.$i, $insnum);
            }

            if ($YZS = "1") {
                $QAZ = "是";
            }else {
                $QAZ = "否";
            }

            $Call = explode(" ", $CallTime);
            
            $sheet->setCellValue('A'.$i, $Call[0]);
            $sheet->setCellValue('B'.$i, $Call[1]." ".$Call[2]);
            $sheet->setCellValue('D'.$i, $ProdName);
            $sheet->setCellValue('H'.$i, $CustTypeName);
            $sheet->setCellValue('I'.$i, $CustName);
            $sheet->setCellValue('L'.$i, $CustId);
            $sheet->setCellValue('M'.$i, $PhoneNo);

            switch ($SrvType) {
                case 1:
                    $sheet->setCellValue('N'.$i, $SrvSubName);
                    break;
                case 2:
                    $sheet->setCellValue('O'.$i, $SrvSubName);
                    break;
                case 3:
                    $sheet->setCellValue('P'.$i, $SrvSubName);
                    break;
                case 4:
                    $sheet->setCellValue('Q'.$i, $SrvSubName);
                    break;
                case 5:
                    $sheet->setCellValue('R'.$i, $SrvSubName);
                    break;
                case 6:
                    $sheet->setCellValue('S'.$i, $SrvSubName);
                    break;
                case 7:
                    $sheet->setCellValue('T'.$i, $SrvSubName);
                    break;
            }

            if($DealTime != ""){
                $DoneTime = ($DealA/60);
                $sheet->setCellValue('U'.$i, floor($DealTime));
                $sheet->setCellValue('V'.$i, floor($DoneTime));
                if ($DoneTime < 0 and $DoneTime <= 48) {
                    // $sheet->getRowDimension($i)->setRGB('#AEEEEE');
                    foreach (range('A', 'Y') as $char) { 
                        $sheet->getStyle($char.$i)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
                    }
                } else if ($DoneTime > 48) {
                    // $sheet->getRowDimension($i)->setRGB('#AEEEEE');
                    foreach (range('A', 'Y') as $char) { 
                        $sheet->getStyle($char.$i)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
                    }
                }
            } else {
                $DoneTime = ($TAcpt/60);
                $sheet->setCellValue('U'.$i, "未結案");
                $sheet->setCellValue('V'.$i, floor($DoneTime));
                if ($DoneTime > 0 and $DoneTime <= 48) {
                    foreach (range('A', 'Y') as $char) { 
                        $sheet->getStyle($char.$i)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
                    }
                } else if ($DoneTime > 48) {
                    foreach (range('A', 'Y') as $char) { 
                        $sheet->getStyle($char.$i)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
                    }
                }
            }
            $sheet->setCellValue('W'.$i, $QAZ);
            $sheet->setCellValue('X'.$i, $AcptCont);
            $i++;
            $rs->MoveNext;
        }
    }

    $File = date ("YmdH-i-s");
    
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer = new Xlsx($spreadsheet);
    $writer->save("../component/temp/".$File.".xlsx");

    if (file_exists("../component/temp/".$File.".xlsx")) {
        print_r(htmlspecialchars($File.".xlsx"));
    }


?>