<?php
	$ultralogpath = "C:\\UltraLog\\System\\";

	// Ū���@�����U��(Ini �Ҧ�)
	function readregini($s_profile, $s_keyname, $s_valuename) {
		global $ultralogpath;

		$s = "";
		$s_profile = $ultralogpath.$s_profile.".ini";
		if (file_exists($s_profile)) {
			//$ia = parse_ini_file($s_profile, true);
			$cont = iconv('UTF-16le//IGNORE', 'UTF-8', implode("", @file($s_profile)));
			$ia = parse_ini_string_m($cont);
			if (isset($ia[$s_keyname])) {
				if (isset($ia[$s_keyname][$s_valuename])) {
					$s = $ia[$s_keyname][$s_valuename];
				}
			}
		}
		return iconv('UTF-8', 'Big5', $s);
	}

	// �g�J�@�����U��(Ini �Ҧ�)
	function writeregini($s_profile, $s_keyname, $s_valuename, $s_value) {
		global $ultralogpath;

		$ia = array();
		$s_profile = $ultralogpath.$s_profile.".ini";
		if (file_exists($s_profile)) {
			//$ia = parse_ini_file($s_profile, true);
			$cont = iconv('UTF-16le//IGNORE', 'UTF-8', implode("", @file($s_profile)));
			$ia = parse_ini_string_m($cont);
		}
		$ia[$s_keyname][$s_valuename] = $s_value;
		$fp = fopen($s_profile, "w");
		fwrite($fp, iconv('UTF-8', 'UTF-16le//IGNORE', PHP_EOL));
		fwrite($fp, iconv('UTF-8', 'UTF-16le//IGNORE', arr2ini($ia)));
		fclose($fp);
	}

	function arr2ini(array $a, array $parent = array())	{
		$out = '';
		foreach ($a as $k => $v) {
			if (is_array($v)) {
				$sec = array_merge((array) $parent, (array) $k);
				$out .= '['.join('.', $sec).']'.PHP_EOL;
				$out .= arr2ini($v, $sec);
			}
			else {
				$v = str_replace('"', '\"', $v);
				$out .= "$k=$v".PHP_EOL;
			}
		}
		$out .= PHP_EOL;
		return $out;
	}

	function parse_ini_string_m($str) {

		if(empty($str)) return false;
		$lines = explode("\n", $str);
		$ret = Array();
		$inside_section = false;

		foreach($lines as $line) {
			$line = trim($line);
			if ((!$line) || ($line[0] == "#") || ($line[0] == ";")) continue;
			if (($line[0] == "[") && ($endIdx = strpos($line, "]"))) {
				$inside_section = substr($line, 1, $endIdx-1);
				continue;
			}
			if (!strpos($line, '=')) continue;

			$tmp = explode("=", $line, 2);
			if ($inside_section) {
				$key = rtrim($tmp[0]);
				$value = ltrim($tmp[1]);
		
				if (preg_match("/^\".*\"$/", $value) || preg_match("/^'.*'$/", $value)) {
					$value = mb_substr($value, 1, mb_strlen($value) - 2);
				}
		
				$t = preg_match("^\[(.*?)\]^", $key, $matches);
				if (!empty($matches) && isset($matches[0])) {
					$arr_name = preg_replace('#\[(.*?)\]#is', '', $key);
					if (!isset($ret[$inside_section][$arr_name]) || !is_array($ret[$inside_section][$arr_name])) {
						$ret[$inside_section][$arr_name] = array();
					}
					if (isset($matches[1]) && !empty($matches[1])) {
						$ret[$inside_section][$arr_name][$matches[1]] = $value;
					} else {
						$ret[$inside_section][$arr_name][] = $value;
					}
				} else {
					$ret[$inside_section][trim($tmp[0])] = $value;
				}
			} else {
				$ret[trim($tmp[0])] = ltrim($tmp[1]);
			}
		}
		return $ret;
	}
?>