//get key value
function QueryString2(key) {
	var value = null;
	var query = window.top.location.search.substring(1);
	var pairs = query.split("&");

	for(var i=0; i<pairs.length; i++) {
		var pos = pairs[i].indexOf('=');
		if(pos > 0) {
			var argname = pairs[i].substring(0,pos);
			if (argname.toLowerCase()===key.toLowerCase()) {
				value = pairs[i].substring(pos+1);
				break;
			}
		}
	}
	//return value;
	return "c";
}

function TurnOnClock(obj){
	var NowDate = new Date();
	document.getElementById(obj).innerHTML = NowDate.getFullYear()+ "/" + (NowDate.getMonth()+1) + "/" + NowDate.getDate() + " " +
											 NowDate.toLocaleTimeString();
	
	if(NowDate.getHours() >= 23 && NowDate.getHours() < 1){
		window.open('','_self'); 
		window.close();
	}
}

//Request PHP & CallBack, (strData, sendData, strFun) Large Send Data
function RequestPHP_LAR() {
	var a = RequestPHP_LAR.arguments, c = a.length;
	
	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			if (c >= 3) {
				var s = XMLHttpRequestObject.responseText;
				s = "\""+ s.replace(/^\s+|\s+$/g,"") +"\"";
				// s = s.replace(/^\s+|\s+$/g,"");
				for(var i = 3; i < c; i++) {
					s += ", \""+a[i]+"\"";
				}
				eval(a[2]+"("+s+")");
			}
		}
	}
	XMLHttpRequestObject.send(a[1]);
}

function RequestPHP_LAR_JSON() {
	var a = RequestPHP_LAR_JSON.arguments, c = a.length;
	
	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", a[0]);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			if (c >= 3) {
				var s = XMLHttpRequestObject.responseText;
				// s = "\""+ s.replace(/^\s+|\s+$/g,"") +"\"";
				s = s.replace(/^\s+|\s+$/g,"");
				for(var i = 3; i < c; i++) {
					s += ", \""+a[i]+"\"";
				}
				eval(a[2]+"("+s+")");
			}
		}
	}
	XMLHttpRequestObject.send(a[1]);
}

function selmenu(num){
	switch(num){
		case 1:
			strData = "../php/php_index.php?num="+num;
			window.top.location = strData;
			break;
		case 2:
			strData = "../html/PhoneCallNum.html?num="+num;
			window.top.location = strData;
			break;
		case 3:
			strData = "../html/TimeCallNum.html?num="+num;
			window.top.location = strData;
			break;
		case 4:
			strData = "../html/SeatStatusPie.html?num="+num;
			window.top.location = strData;
			break;
		case 5:
			strData = "../html/GuestWaitTime.html?num="+num;
			window.top.location = strData;
			break;
		case 6:
			strData = "../html/GuestWaitAg.html?num="+num;
			window.top.location = strData;
			break;
		case 7:
			strData = "../html/InLineCount.html?num="+num;
			window.top.location = strData;
			break;
	}
}

function language_init() {
	var l = QueryString2("l");
	obj = document.getElementsByTagName("span");
	for (var i in obj){
		if (typeof(obj[i].id) != "undefined") {
			if ((obj[i].id).substring(0, 2) === "l_") {
				obj[i].innerText = switchlanguage((obj[i].id).substring(2), l);
			}
		}
	}
	
	obj = document.getElementsByTagName("input");
	for (var i in obj){
		if (typeof(obj[i].id) != "undefined") {
			if ((obj[i].id).substring(0, 2) === "l_") {
				obj[i].value = switchlanguage((obj[i].id).substring(2), l);
			}
		}
	}
	
	obj = document.getElementsByTagName("span");
	for (var i in obj){
		if (typeof(obj[i].className) != "undefined") {
			if ((obj[i].className).substring(0, 2) === "l_") {
				obj[i].innerText = switchlanguage((obj[i].className).substring(2), l);
			}
		}
	}
	
	obj = document.getElementsByTagName("input");
	for (var i in obj){
		if (typeof(obj[i].className) != "undefined") {
			if ((obj[i].className).substring(0, 2) === "l_") {
				obj[i].value = switchlanguage((obj[i].className).substring(2), l);
			}
		}
	}
}

function switchlanguage() {

	if (switchlanguage.arguments.length === 1) {
		switch(QueryString2("l")) {
			case "c":	l = "c";	break;
			case "g":	l = "g";	break;
			case "e":	l = "e";	break;
			case "j":	l = "j";	break;
			default:	l = "e";	break;
		}
	}
	else {
		l = switchlanguage.arguments[1];
	}
	return eval(l+"_"+switchlanguage.arguments[0]);
}

function checkDate3(obj) {
	var re=/((1|2)(\d\d\d)(\/|-|\.)((0)([1-9])|(1)([0-2]))(\/|-|\.)(([0-2])(\d)|(3)(0|1))|(1|2)(\d\d\d)((0)([1-9])|(1)([0-2]))(([0-2])(\d)|(3)(0|1)))/;
	if((!re.test(obj.value))&&!(String(obj.value).length===0)){
		return false;
	}
	else {
		if(!(String(obj.value).length===0)) {
 			var newobj=obj.value.replace(/\./g,'');
			newobj=newobj.replace(/-/g,'');
			newobj=newobj.replace(/\//g,'');
			if(re.test(Mid(newobj,5,4)) || Mid(newobj,5,4)==='0229' && !(parseInt(Left(newobj,4),10)%4)===0) {
				return false;
			}
		}
	}
	obj.value=obj.value.replace(/\./g,'/');
	obj.value=obj.value.replace(/-/g,'/');
	return true;
}

function getNow2() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);
	
	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);
	
	return t1_y+"/"+t1_m+"/"+t1_d;
}

function getEx2() {
	var t1 = new Date();
	var exdate = t1.getDate()-1;
	
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+exdate;
	t1_d = t1_d.substring(t1_d.length-2);
	
	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);
	
	return t1_y+"/"+t1_m+"/"+t1_d;
}

function getNow() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);
	
	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);
	
	return t1_y+"/"+t1_m+"/"+t1_d+" "+t1_h+":"+t1_n;
}

function getExNow() {
	var t1 = new Date();
	var exdate = t1.getDate()-1;
	
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+exdate;
	t1_d = t1_d.substring(t1_d.length-2);
	
	t1_h = "00"+(t1.getHours());
	t1_h = t1_h.substring(t1_h.length-2);
	
	t1_n = "00"+(t1.getMinutes());
	t1_n = t1_n.substring(t1_n.length-2);
	
	return t1_y+"/"+t1_m+"/"+t1_d+" "+"23:59";
}

function getFirstDay() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1.setDate(1);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d+" 00:00";
}

function getFirstDate() {
	var t1 = new Date();
	t1_y = t1.getFullYear().toString();
	t1_m = "00"+(t1.getMonth()+1).toString();
	t1_m = t1_m.substring(t1_m.length-2);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	t1.setDate(1);
	t1_d = "00"+t1.getDate().toString();
	t1_d = t1_d.substring(t1_d.length-2);

	return t1_y+"/"+t1_m+"/"+t1_d;
}

function sortNumber(a,b)
{
	return a - b;
}

function sortNumber_mi(a,b)
{
	return b - a;
}

var oldObjSort = function(oldObj){
    var keyArray = [],
        newObj   = {};

    for(key in oldObj){
        keyArray.push(key);
    }

    // console.log(keyArray);         //結果為未排序的array -> ["c", "a", "e", "b", "d"]

    keyArray.sort(sortNumber_mi);

    // console.log(keyArray);         //結果為排序過的array -> ["a", "b", "c", "d", "e"]

    for(i in keyArray){
        var key = keyArray[i];
		
		if(key != "total"){						  //依陣列中的key順序，將Value依序存入新的object
			 newObj[i] = oldObj[key];
		}
		else{
			newObj[key] = oldObj[key];
		}
       
    }

    return newObj;                 //回傳一個排列過的object
};