var mode = "";
var start = 0;
var end = 0;
var range = "";
var ar_data = null;
var t_str_all = "";
var ST = "";

function initdata() {
	
	strData = "../php/LineInCallInit.php?";
	RawData = "&time="+new Date().getTime();
	
	RequestPHP_LAR(strData, RawData, "_initdata");
}

function _initdata(s){
	if(s !== ""){
		var ar_s = s.split("@!!@");
		var ar_year = ar_s[0].split("@!@");
		ar_year = ar_year.sort(sortNumber_mi);
		var ar_calledno = ar_s[1].split("@!@");
		
		for(var i = 0 ; i < ar_year.length ; i++){
			var new_option = new Option(ar_year[i], ar_year[i]);
			document.getElementById("Sel_Year").options.add(new_option);
		}
		
		for(var i = 0 ; i < ar_calledno.length ; i++){
			var new_option = new Option(ar_calledno[i], ar_calledno[i]);
			document.getElementById("Sel_calledno").options.add(new_option);
		}
		
		document.getElementById("ch_LineIn").checked = true;
		document.getElementById("S_Year").checked = true;
		document.getElementById("S_calledno").checked = true;
	}
	
	document.getElementById("l_Export").disabled = true;
	document.getElementById("l_Print").disabled = true;
}

function SearchData(type){
	
	var In = 0;
	var Out = 0;
	var calledno = document.getElementById("Sel_calledno").value;
	
	if(document.getElementById("ch_LineIn").checked === true){
		In = 1;
	}
	if(document.getElementById("ch_LineOut").checked === true){
		Out = 1;
	}
	
	if(type === "year"){
		mode = "year";
		range = document.getElementById("Sel_Year").value;
	}
	else{
		range = document.getElementById("Sel_Year").value;
		mode = type;
	}
	
	
	
	if(mode !== 0 && range !== "" && (In === 1 || Out === 1)){
		
		document.getElementById("reading").style.display = "";
		ST = getNow();
		
		strData = "../php/SearchLineInCount.php?";
		RawData = "&mode="+mode;
		RawData += "&range="+range;
		RawData += "&In="+In;
		RawData += "&Out="+Out;
		RawData += "&calledno="+calledno;
		RawData += "&ST="+ST;
		
		RequestPHP_LAR_JSON(strData, RawData, "_SearchData");
	}
}

function _SearchData(s){
	
	var strTable = "";
	document.getElementById("reading").style.display = "none";
	
	if(s !== "" && s !== "-1" || s === "0"){

		if(mode === "year"){
			ar_data = s;
			if(ar_data){
				strTable = "<table id='t_data' border='1' width='100%'>"+
							"<tr>"+
								"<td colspan='2' rowspan='2' class='workf' >"+ switchlanguage("IVRCount") +"</td>"+
								"<td colspan='12' class='workf' >"+ range + switchlanguage("LineInNum") +"</td>"+
							"</tr>"+
							"<tr>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(1)'>1"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(2)'>2"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(3)'>3"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(4)'>4"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(5)'>5"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(6)'>6"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(7)'>7"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(8)'>8"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(9)'>9"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(10)'>10"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(11)'>11"+ switchlanguage("Month") +"</a></td>"+
								"<td><a href='javascript:void(0);' onclick='SearchData(12)'>12"+ switchlanguage("Month") +"</a></td>"+
							"</tr>"+
							"<tr>"+
								"<td colspan='2'>"+ switchlanguage("hidenum") +"</td>";
				
				for(var i = 1 ; i <= 12 ; i++){
					
					strTable += "<td align='right'>"+ ar_data[i]["total"]["hide"] +"</td>";
					
				}
				
				strTable += "</tr>"+
							"<tr>"+
								"<td colspan='2'>"+ switchlanguage("blackrefuse") +"</td>";
								
				for(var i = 1 ; i <= 12 ; i++){
					
					strTable += "<td align='right'>"+ ar_data[i]["total"]["black"] +"</td>";
					
				}
				
				strTable += "</tr>"+
							"<tr>"+
								"<td rowspan='3'>"+ switchlanguage("shownum") +"</td>"+
								"<td>"+ switchlanguage("OnLine") +"</td>";
								
				for(var i = 1 ; i <= 12 ; i++){
					
					strTable += "<td align='right'>"+ ar_data[i]["total"]["yes"] +"</td>";
					
				}
				
				strTable += "</tr>"+
							"<tr>"+
								"<td>"+ switchlanguage("IVRnotOnLine") +"</td>";
								
				for(var i = 1 ; i <= 12 ; i++){
					
					strTable += "<td align='right'>"+ ar_data[i]["total"]["IVR_no"] +"</td>";
					
				}
				
				strTable += "</tr>"+
							"<tr>"+
								"<td>"+ switchlanguage("ACDnotOnLine") +"</td>";
								
				for(var i = 1 ; i <= 12 ; i++){
					
					strTable += "<td align='right'>"+ ar_data[i]["total"]["ACD_no"] +"</td>";
					
				}
				
				strTable += "</tr>"+
							"<tr  class='w_field_t'>"+
								"<td colspan='2'>"+ switchlanguage("total") +"</td>";
								
				for(var i = 1 ; i <= 12 ; i++){
					
					strTable += "<td class='w_field' align='right'>"+ ar_data[i]["total"]["total"] +"</td>";
					
				}
				
				strTable += "</tr></table>";
				
				document.getElementById("t_data_div").innerHTML = strTable;
				
				t_str_all += strTable;
				
				for(var i in ar_data){
					
					strTable = "<table id='t_data_m' border='1' width='100%'>"+
								"<tr>"+
									"<td colspan='3' class='workf'>"+ range+ "/" + i +"</td>"+
									"<td colspan='3' class='workf'>"+ switchlanguage("shownum") +"</td>"+
								"</tr>"+
								"<tr>"+
									"<td class='workf'>"+ switchlanguage("Date") +"</td>"+
									"<td class='workf'>"+ switchlanguage("hidenum") +"</td>"+
									"<td class='workf'>"+ switchlanguage("blackrefuse") +"</td>"+
									"<td class='workf'>"+ switchlanguage("OnLine") +"</td>"+
									"<td class='workf'>"+ switchlanguage("IVRnotOnLine") +"</td>"+
									"<td class='workf'>"+ switchlanguage("ACDnotOnLine") +"</td>"+
								"</tr>";
					
					// if(i === mode){
						for(var j in ar_data[i]){
							if(j !== "total"){
								strTable += "<tr>"+
												"<td>"+ j +"</td>"+
												"<td align='right'>"+ ar_data[i][j]["hide"] +"</td>"+
												"<td align='right'>"+ ar_data[i][j]["black"] +"</td>"+
												"<td align='right'>"+ ar_data[i][j]["yes"] +"</td>"+
												"<td align='right'>"+ ar_data[i][j]["IVR_no"] +"</td>"+
												"<td align='right'>"+ ar_data[i][j]["ACD_no"] +"</td>"+
											"</tr>";
							}
							else{
								strTable += "<tr>"+
												"<td class='w_field_t'>"+ switchlanguage(j) +"</td>"+
												"<td class='w_field'>"+ ar_data[i][j]["hide"] +"</td>"+
												"<td class='w_field'>"+ ar_data[i][j]["black"] +"</td>"+
												"<td class='w_field'>"+ ar_data[i][j]["yes"] +"</td>"+
												"<td class='w_field'>"+ ar_data[i][j]["IVR_no"] +"</td>"+
												"<td class='w_field'>"+ ar_data[i][j]["ACD_no"] +"</td>"+
											"</tr>";
							}
							
						}
					// }
					
					strTable += "</table>";
					
					t_str_all += strTable;
				}
				
				document.getElementById("SearchInfo").innerHTML = "查詢時間："+ST;
				
				var ex = document.getElementById("l_Export");
				var pr = document.getElementById("l_Print");
				
				ex.setAttribute("onclick", "location.href='../config/Temp/LineInCount.xls'");
				pr.setAttribute("onclick", "printTable('t_all')");
				ex.disabled = false;
				pr.disabled = false;
				
				SearchData(1);
			}
		}
		else if(mode !== ""){
				
			strTable = "<table id='t_data_m' border='1' width='100%'>"+
							"<tr>"+
								"<td colspan='3' class='workf'>"+ range+ "/" + mode +"</td>"+
								"<td colspan='3' class='workf'>"+ switchlanguage("shownum") +"</td>"+
							"</tr>"+
							"<tr>"+
								"<td class='workf'>"+ switchlanguage("Date") +"</td>"+
								"<td class='workf'>"+ switchlanguage("hidenum") +"</td>"+
								"<td class='workf'>"+ switchlanguage("blackrefuse") +"</td>"+
								"<td class='workf'>"+ switchlanguage("OnLine") +"</td>"+
								"<td class='workf'>"+ switchlanguage("IVRnotOnLine") +"</td>"+
								"<td class='workf'>"+ switchlanguage("ACDnotOnLine") +"</td>"+
							"</tr>";
			
			for(var i in ar_data){
				if(i === mode){
					for(var j in ar_data[i]){
						if(j !== "total"){
							strTable += "<tr>"+
											"<td>"+ j +"</td>"+
											"<td align='right'>"+ ar_data[i][j]["hide"] +"</td>"+
											"<td align='right'>"+ ar_data[i][j]["black"] +"</td>"+
											"<td align='right'>"+ ar_data[i][j]["yes"] +"</td>"+
											"<td align='right'>"+ ar_data[i][j]["IVR_no"] +"</td>"+
											"<td align='right'>"+ ar_data[i][j]["ACD_no"] +"</td>"+
										"</tr>";
						}
						else{
							strTable += "<tr>"+
											"<td class='w_field_t'>"+ switchlanguage(j) +"</td>"+
											"<td class='w_field'>"+ ar_data[i][j]["hide"] +"</td>"+
											"<td class='w_field'>"+ ar_data[i][j]["black"] +"</td>"+
											"<td class='w_field'>"+ ar_data[i][j]["yes"] +"</td>"+
											"<td class='w_field'>"+ ar_data[i][j]["IVR_no"] +"</td>"+
											"<td class='w_field'>"+ ar_data[i][j]["ACD_no"] +"</td>"+
										"</tr>";
						}
						
					}
				}
			}
			
			strTable += "</table>";
			document.getElementById("t_data_div_m").innerHTML = strTable;
		}
		
	}
	else{
		alert(switchlanguage("noData"));
	}
}

function printTable(table){
	// var pr_data = document.getElementById(table).innerHTML;
	var printPage = window.open("", "printPage", "");
	printPage.document.open();
	printPage.document.write("<OBJECT classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2' height=0 id=wc name=wc width=0></OBJECT>");
	printPage.document.write("<HTML><head></head><BODY onload='javascript:wc.execwb(7,1);window.close()'>");
	printPage.document.write("<PRE>");
	printPage.document.write(t_str_all);
	printPage.document.write("</PRE>");
	printPage.document.close("</BODY></HTML>");
}