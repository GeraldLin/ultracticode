function SearchData(){
	
	var CS = 0;
	var PS = 0;
	
	var start = document.getElementById("A_data").value;
	var end = document.getElementById("B_data").value;
	
	if(document.getElementById("ch_CustomService").checked === true){
		CS = 1;
	}
	if(document.getElementById("ch_ProjectSupport").checked === true){
		PS = 1;
	}
	
	if(CS === 1 || PS === 1){
		
		document.getElementById("reading").style.display = "";
		
		strData = "../php/SearchSeatStatusPie.php?";
		RawData = "&start="+start;
		RawData += "&end="+end;
		RawData += "&CS="+CS;
		RawData += "&PS="+PS;
		
		RequestPHP_LAR_JSON(strData, RawData, "_SearchData");
	}
	
}

function _SearchData(s){
	document.getElementById("reading").style.display = "none";
	if(s === "1"){
		init();
	}
	else{
		alert(switchlanguage("noData"));
	}
}

function init(){
	strData = "../php/ReadSeatStatusPie.php?";
	RawData = "";
	
	RequestPHP_LAR_JSON(strData, RawData, "_init");
}

function _init(s){
	document.getElementById("reading").style.display = "none";
	
	if(s !== "-1"){
		// var ar_data = new Array();
		var ar_data = s;
		// ar_data.sort(sortNumber);
		ar_data = oldObjSort(ar_data);
		
		
		var strWork = "<table id='workdata' border='1' width='100%'>";
		document.getElementById("dbase").innerHTML = "";
		
		for(var workno in ar_data){
			if(workno !== "total"){
				strWork += "<tr>";
				var WorkName = ar_data[workno]["total"]["WorkerName"];
				for(var seatno in ar_data[workno]){
					if(seatno !== "total"){
						var objblock;
						var others = ((ar_data[workno][seatno]["others"]) ? ar_data[workno][seatno]["others"] : "00:00:00");
						var deal = ((ar_data[workno][seatno]["deal"]) ? ar_data[workno][seatno]["deal"] : "00:00:00");
						var use = ((ar_data[workno][seatno]["use"]) ? ar_data[workno][seatno]["use"] : "00:00:00");
						var rest = ((ar_data[workno][seatno]["rest"]) ? ar_data[workno][seatno]["rest"] : "00:00:00");
						var intalk = ((ar_data[workno][seatno]["intalk"]) ? ar_data[workno][seatno]["intalk"] : "00:00:00");
						var outtalk = ((ar_data[workno][seatno]["outtalk"]) ? ar_data[workno][seatno]["outtalk"] : "00:00:00");
						
						objblock = Object.create(CircleObj);
						objblock.agname = WorkName;
						objblock.extno = seatno;
						objblock.logintime = "";
						objblock.statetime = [use, intalk, outtalk, rest, others];
						objblock.init("dbase", "1");
					}
					else{
						
						var talk = ((ar_data[workno]["total"]["talk"]) ? ar_data[workno]["total"]["talk"] : "00:00:00");
						var deal = ((ar_data[workno]["total"]["deal"]) ? ar_data[workno]["total"]["deal"] : "00:00:00");
						var use = ((ar_data[workno]["total"]["use"]) ? ar_data[workno]["total"]["use"] : "00:00:00");
						var meal = ((ar_data[workno]["total"]["meal"]) ? ar_data[workno]["total"]["meal"] : "00:00:00");
						var meeting = ((ar_data[workno]["total"]["meeting"]) ? ar_data[workno]["total"]["meeting"] : "00:00:00");
						var restroom = ((ar_data[workno]["total"]["restroom"]) ? ar_data[workno]["total"]["restroom"] : "00:00:00");
						
						strWork += "<td align='center' width='16%'>"+ ar_data[workno]["total"]["WorkerName"] +"</td>";
						strWork += "<td align='center'>"+ use +"</td>";
						strWork += "<td align='center'>"+ talk +"</td>";
						strWork += "<td align='center'>"+ deal +"</td>";
						strWork += "<td align='center'>"+ meeting +"</td>";
						strWork += "<td align='center'>"+ meal +"</td>";
						strWork += "<td align='center'>"+ restroom +"</td>";
					}
				}
				strWork += "</tr>";
			}
			else if(workno === "total"){
				for(var i in ar_data[workno]["total"]){
					document.getElementById("s_"+i).innerHTML = ar_data[workno]["total"][i];
				}
			}
		}
		
		strWork += "</table>";
		
		document.getElementById("workcount").innerHTML = strWork;
	}
	else{
		alert(switchlanguage("noData"));
	}
	
}

function showex(id){
	var strex = "";
	var change_left = 0;
	var change_top = 0;
	
	if(id === "exworker"){
		strex = "<table border='1'>"+
				"	<tr>"+
				"		<td>"+
							switchlanguage("exuse")+"<br>"+
							switchlanguage("extalk")+"<br>"+
							switchlanguage("exdeal")+"<br>"+
							switchlanguage("exdmeet")+"<br>"+
							switchlanguage("exdmeal")+"<br>"+
							switchlanguage("exdrestroom")+
				"		</td>"+
				"	</tr>"+
				"</table>";
		document.getElementById("gshow").innerHTML = strex;
		change_left = -300;
		change_top = 50;
	}
	else if(id === "expie"){
		strex = "<table border='1'>"+
				"	<tr>"+
				"		<td>"+
							switchlanguage("exuse")+"<br>"+
							switchlanguage("exin")+"<br>"+
							switchlanguage("exout")+"<br>"+
							switchlanguage("exrest")+"<br>"+
							switchlanguage("exother")+
				"		</td>"+
				"	</tr>"+
				"</table>";
		document.getElementById("gshow").innerHTML = strex;
		change_left = 0;
		change_top = 50;
	}
	else if(id === "extotal"){
		strex = "<table border='1'>"+
				"	<tr>"+
				"		<td>"+
							switchlanguage("exause")+"<br>"+
							switchlanguage("exnotuse")+"<br>"+
							switchlanguage("exlogout")+"<br>"+
							switchlanguage("expickup")+"<br>"+
							switchlanguage("exbusy")+"<br>"+
							switchlanguage("exin")+"<br>"+
							switchlanguage("exout")+"<br>"+
							switchlanguage("exinring")+"<br>"+
							switchlanguage("exoutring")+"<br>"+
							switchlanguage("exdeal")+"<br>"+
							switchlanguage("exdmeal")+"<br>"+
							switchlanguage("exdrestroom")+"<br>"+
							switchlanguage("exdmeet")+"<br>"+
							switchlanguage("exother")+"<br>"+
				"		</td>"+
				"	</tr>"+
				"</table>";
		document.getElementById("gshow").innerHTML = strex;
		change_left = 0;
		change_top = 50;
	}
	
	var gshowf=document.getElementById(id);
	var gposition = $('#'+id).offset(); //物件座標xy  
	var gx = gposition.left;
	var gy = gposition.top;   
	
	var gshowoj = document.getElementById("gshow");
	gshowoj.style.position = 'absolute';
	gshowoj.style.top = (gy+change_top)+'px';
	gshowoj.style.left = (gx+change_left)+'px';
	gshowoj.style.display = "block";
}

function hideex(){
	document.getElementById("gshow").style.display="none";
}