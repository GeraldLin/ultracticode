﻿// ===========================================================================================================
// Tranditional Chinese Version - c
// ===========================================================================================================
var	c_menu_1 = "WallBoard";
var	c_menu_2 = "電話通數一覽表";
var	c_menu_3 = "分時通量統計";
var	c_menu_4 = "座席狀態統計";
var	c_menu_5 = "顧客等待時間統計";
var	c_menu_6 = "人員振鈴時間統計";
var	c_menu_7 = "進線統計";
var	c_state_1 = "值機";
var	c_state_2 = "非值機";
var	c_state_3 = "未登錄";
var	c_state_4 = "通話狀態";
var	c_state_5 = "撥入振鈴";
var	c_state_6 = "正在外撥";
var	c_state_7 = "話後處理";
var	c_state_8 = "用餐";
var	c_state_9 = "洗手間";
var	c_state_10 = "開會";
var	c_state_11 = "其他業務";
var c_state_12 = "摘機狀態";
var c_state_13 = "延時態";
var c_state_14 = "休息";

var c_ename = "名稱：";
var c_eseatno = "座席號碼：";
var c_enote = "備註：";
var c_submit = "確認";
var c_cancel = "取消";
var c_NameRepeat = "名稱重複";
var c_succeed = "成功";
var c_failed = "失敗";
var c_LineIn = "進線";
var c_LineOut = "外撥";
var c_Date = "日期";
var c_Month = "月";
var c_Year = "年";
var c_Year2 = "年";
var c_Search = "查詢";
var c_Export = "匯出";
var c_Print = "列印";
var c_user = "使用者";
var c_total = "合計";
var c_noData = "無資料";
var c_SeatWorker = "值機人員";
var c_Ext = "分機";
var c_LoginTime = "登入時間";
var c_TalkTab = "通 話";
var c_DocumentTab = "文 書";
var c_StandbyTab = "待 機";
var c_RestTab = "休 息";
var c_StartEndTime = "起訖時間";
var c_wait = "待機";
var c_talkin = "通話狀態-進線";
var c_talkout = "通話狀態-撥出";
var c_guestwaittime = "顧客等待時間";
var c_AnsNum = "接通數量";
var c_longestwait = "最長等待時間";
var c_Ans15 = "15秒內接通數量";
var c_Ans1530 = "15至30秒內接通數量";
var c_Ans3060 = "30至60秒內接通數量";
var c_Ans60 = "60秒以上接通數量";
var c_Ans3 = "3秒內接通數量";
var c_Ans36 = "3至6秒內接通數量";
var c_Ans610 = "6至10秒內接通數量";
var c_Ans10 = "10秒以上接通數量";

var c_AnsTotal = "總接通數量";
var c_IVRCount = "IVR統計";
var c_LineInNum = "進線數量";
var c_hidenum = "未顯示號碼";
var c_blackrefuse = "黑名單拒絕";
var c_shownum = "顯示號碼";
var c_OnLine = "接通";
var c_IVRnotOnLine = "IVR掛斷";
var c_ACDnotOnLine = "ACD掛斷";
var c_notOnLine = "未接通";
var c_CustomService = "客服中心";
var c_ProjectSupport = "專案客服";
var c_searcherr = "查詢輸入錯誤";
var c_others = "其他";
var c_inbound = "進線";
var c_outbound = "外撥";
var c_calledno = "受理分機";
var c_time = "時間";
var c_averagewait = "平均接通時間";

var c_exuse = "待機：值機時等待狀態時間統計";
var c_exause = "值機：值機時間統計";
var c_exin = "進線：客戶來電時間統計";
var c_exout = "外撥：值機人員撥打給客戶時間統計";
var c_exrest = "休息：包含用餐、洗手間的時間統計";
var c_exother = "其他：除上述狀態外其他狀態時間總和";
var c_extalk = "通話狀態：值機員通話狀態總時間統計";
var c_exdeal = "話後處理：值機員通話結束後作業時間統計";
var c_exdmeet = "開會：開會時間統計";
var c_exdmeal = "用餐：用餐時間統計";
var c_exdrestroom = "洗手間：洗手間時間統計";
var c_exnotuse = "非值機：值機以外的活動時間統計";
var c_exlogout = "未登入：非登入時間統計";
var c_expickup = "摘機狀態：值機員拿起話筒的時間統計";
var c_exbusy = "延時態：忙碌時間統計";
var c_exinring = "撥入振鈴：客戶進線響鈴時間統計";
var c_exoutring = "正在外撥：值機員外撥響鈴時間統計";

var c_second = "秒";

// ===========================================================================================================
// Englist Version - e
// ===========================================================================================================

// ===========================================================================================================
// Version - g
// ===========================================================================================================