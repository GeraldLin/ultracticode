﻿$(document).ready(function() {
	Calendar.setup({
		button         :    "m_date_a",
		inputField     :    "A_data",				// id of the input field
		ifFormat       :    "%Y/%m/%d",		// format of the input field
		showsTime      :    true
	});
	Calendar.setup({
		button         :    "m_date_b",
		inputField     :    "B_data",
		ifFormat       :    "%Y/%m/%d",
		showsTime      :    true
	});
	var Last = new Date();
	Last.setMonth(Last.getMonth()-1);
	var day = ("0" + Last.getDate()).slice(-2);
	var month = ("0" + (Last.getMonth() + 1)).slice(-2);
	var Lastmonth = Last.getFullYear()+"/"+(month)+"/"+(day) ;
	$('#A_data').val(Lastmonth);

	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"/"+(month)+"/"+(day) ;
	$('#B_data').val(today);

	addOption();
})

function SearchData() {
	
	var date_array_a = document.getElementById('A_data') .value.split('/');
	var day_a = date_array_a[2];
	var month_a = date_array_a[1] - 1;
	var year_a = date_array_a[0];
	source_date_a = new Date(year_a,month_a,day_a);

	var date_array_b = document.getElementById('B_data') .value.split('/');
	var day_b = date_array_b[2];
	var month_b = date_array_b[1] - 1;
	var year_b = date_array_b[0];
	source_date_b = new Date(year_b,month_b,day_b);

	if(A_data.value ==="") {
		alert("請輸入日期!");
	}
	else if(year_a !== source_date_a.getFullYear()) { 
		alert("請輸入正確日期");
	} 
	else if(month_a !== source_date_a.getMonth()) { 
		alert("請輸入正確日期");
	} 
	else if(day_a !== source_date_a.getDate()) { 
		alert("請輸入正確日期");
	} 
	else if(B_data.value ==="") {
		alert("請輸入日期!");
	}
	else if(year_b !== source_date_b.getFullYear()) { 
		alert("請輸入正確日期");
	} 
	else if(month_b !== source_date_b.getMonth()) { 
		alert("請輸入正確日期");
	} 
	else if(day_b !== source_date_b.getDate()) { 
		alert("請輸入正確日期");
	} 
	else{
		// alert(1);
		var start = document.getElementById("A_data").value;
		var end = document.getElementById("B_data").value;

		document.getElementById("reading").style.display = "";
		document.getElementById('l_Search').disabled=true;
		
		strData = "../php/rate.php?";
		RawData = "&start="+start;
		RawData += "&end="+end;

		var XMLHttpRequestObject = false;
		if (window.XMLHttpRequest) {
			XMLHttpRequestObject = new XMLHttpRequest();
		}
		else if (window.ActiveXObject) {
			  XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		XMLHttpRequestObject.open("POST", strData+RawData);
		XMLHttpRequestObject.onreadystatechange = function() {
			if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
				alert(XMLHttpRequestObject.responseText);	// show error
				var s = XMLHttpRequestObject.responseText;
				if (s === "") {
					alert("Search Fail");
				}
				else {
					
					xls_file_name = start+'.xlsx';
					SearchData2(xls_file_name);
				}
				document.getElementById("reading").style.display = "none";
				document.getElementById('l_Search').disabled=false;
			}
		}
		XMLHttpRequestObject.send(RawData);
	}
}

function SearchData2(xls_file_name) {

	strData = "../php/rate2.php?";
	RawData = "&exportfilename="+xls_file_name;

	/*var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
  		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", strData);
	XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			alert(XMLHttpRequestObject.responseText);	// show error
		}
	}
	XMLHttpRequestObject.send(RawData);*/

	var iframe = document.createElement("iframe");
		iframe.src = strData+RawData;
		iframe.style.display = "none";
		document.body.appendChild(iframe);
}

function addOption(){
	var Dday = new Date();
	var PYear = Dday.getFullYear();
	var DYear = PYear-1911;
	var Year_ID = document.getElementById('ser_year');
	var Month_ID = document.getElementById('ser_month');

	for(var i = DYear ; i >= DYear-3 ; i--){
		var new_option = new Option(i, i);
		Year_ID.options.add(new_option);
	}
	
	for(var i = 1 ; i <= 12 ; i++){
		var new_option = new Option(i, i);
		Month_ID.options.add(new_option);
	}
}

function Download(){

	var searchy = document.getElementById('ser_year').value;
	var searchm = document.getElementById('ser_month').value;
	
	document.getElementById("loading").style.display = "";
	document.getElementById('t_Search').disabled=true;

	strData = "../php/phonerecord.php?";
	RawData = "&year="+searchy;
	RawData += "&month="+searchm;

	var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		  XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	XMLHttpRequestObject.open("POST", strData+RawData);
	XMLHttpRequestObject.onreadystatechange = function() {
		if (XMLHttpRequestObject.readyState === 4 && XMLHttpRequestObject.status === 200) {
			//alert(XMLHttpRequestObject.responseText);	// show error
			var s = XMLHttpRequestObject.responseText;
			if (s === "") {
				alert("Search Fail");
			}
			else {
				 SearchData2(s);
			}
			document.getElementById("loading").style.display = "none";
			document.getElementById('t_Search').disabled=false;
		}
	}
	XMLHttpRequestObject.send(RawData);
}