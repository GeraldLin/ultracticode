var mode = 0;
var start = 0;
var end = 0;
var ST = "";

function initdata() {
	
	strData = "../php/PhoneCallNumInit.php?";
	RawData = "&time="+new Date().getTime();
	
	RequestPHP_LAR(strData, RawData, "_initdata");
}

function _initdata(s){
	if(s !== ""){
		var ar_year = s.split("@!@");
		ar_year = ar_year.sort(sortNumber_mi);
		
		var thismonth = new Date().getMonth();
		
		for(var i = 0 ; i < ar_year.length ; i++){
			var new_option = new Option(ar_year[i], ar_year[i]);
			var new_option1 = new Option(ar_year[i], ar_year[i]);
			document.getElementById("Sel_Year").options.add(new_option);
			document.getElementById("Sel_Month_Year").options.add(new_option1);
		}
		
		for(var i = 1 ; i <= 12 ; i++){
			var new_option = new Option(i, i);
			document.getElementById("Sel_Month").options.add(new_option);
		}
		
		document.getElementById("ch_LineIn").checked = true;
		document.getElementById("S_Date").checked = true;
		document.getElementById("ch_CustomService").checked = true;
		document.getElementById("Sel_Month").value = thismonth+1;
	}
	
	document.getElementById("l_Export").disabled = true;
	document.getElementById("l_Print").disabled = true;
}

function SearchData(){
	
	mode = 0;
	var range = "";
	var In = 0;
	var Out = 0;
	var CS = 0;
	var PS = 0;
	
	if(document.getElementById("ch_LineIn").checked === true){
		In = 1;
	}
	if(document.getElementById("ch_LineOut").checked === true){
		Out = 1;
	}
	
	if(document.getElementById("S_Date").checked === true){
		mode = 1;
		range = document.getElementById("D_data").value;
		var ar_theday = range.split("/");
		start = parseInt(ar_theday[2]);
		end = parseInt(ar_theday[2])+1;
	}
	else if(document.getElementById("S_Month").checked === true){
		mode = 2;
		range = document.getElementById("Sel_Month_Year").value+"-"+document.getElementById("Sel_Month").value;
		start = 1;
		
		var yy = parseInt(document.getElementById("Sel_Month_Year").value);
		var mm = parseInt(document.getElementById("Sel_Month").value);
		
		end = new Date(yy, mm, 0).getDate() + 1;
	}
	else if(document.getElementById("S_Year").checked === true){
		mode = 3;
		range = document.getElementById("Sel_Year").value;
		start = 1;
		end = 13;
	}
	
	if(document.getElementById("ch_CustomService").checked === true){
		CS = 1;
	}
	if(document.getElementById("ch_ProjectSupport").checked === true){
		PS = 1;
	}
	
	if(mode !== 0 && range !== "" && (In === 1 || Out === 1) && (CS === 1 || PS === 1)){
		
		document.getElementById("reading").style.display = "";
		ST = getNow();
		
		strData = "../php/SearchTimeCallNum.php?";
		RawData = "&mode="+mode;
		RawData += "&range="+range;
		RawData += "&In="+In;
		RawData += "&Out="+Out;
		RawData += "&CS="+CS;
		RawData += "&PS="+PS;
		RawData += "&ST="+ST;
		
		RequestPHP_LAR_JSON(strData, RawData, "_SearchData");
	}
}

function _SearchData(s){
	var ar_data = null;
	document.getElementById("reading").style.display = "none";
	
	if(s !== "" && s !== "-1"){
		
		// eval("ar_data = "+ s +";");
		ar_data = s
		
		var strTable = "<table id='t_data' border='0' width='100%' ><tr><td><table width='100%' border='1'><tr><td class='workf'>"+ 
						switchlanguage("time") +"</td></tr>";
		if(mode === 1){
			var strTable1 = "<td><table width='20%' border='1' style='table-layout:fixed;'><tr>";
		}
		else{
			var strTable1 = "<td><table width='100%' border='1' style='table-layout:fixed;'><tr>";
		}
		
		for(var i = start ; i < end ; i++){
			strTable1 += "<td class='workf'  >"+ i +"</td>";
		}
		strTable1 += "<td class='workf'  >"+ switchlanguage("total") +"</td></tr>";
		
		if(ar_data){
			for(var i in ar_data){
				
				if(i !== "total"){
					strTable += "<tr><td class='workf' style='width:40px;'>"+ ar_data[i][0]["WorkerName"] +"</td></tr>";
					strTable1 += "<tr>";
					for(var j in ar_data[i]){
						if(j !== "total" && j !== "0"){
							strTable1 += "<td align='right' >"+ ar_data[i][j]["callnum"] +"</td>";
						}
						
					}
					strTable1 += "<td width='5%' align='right'>"+ ar_data[i]["total"]["callnum"] +"</td></tr>";
				}
				
			}
			strTable += "<tr><td class='workf'>"+ switchlanguage("total") +"</td>";
			strTable1 += "<tr>";
			for(var i in ar_data["total"]){
				strTable1 += "<td align='right' class='w_field'>"+ ar_data["total"][i]["callnum"] +"</td>";
				
			}
			strTable += "</tr></table>";
			strTable1 += "</tr></table>";
			
			document.getElementById("t_data_div").innerHTML = strTable+strTable1;
		}
		
		document.getElementById("SearchInfo").innerHTML = "查詢時間："+ST;
		
		var ex = document.getElementById("l_Export");
		var pr = document.getElementById("l_Print");
		
		ex.setAttribute("onclick", "location.href='../config/Temp/TimeCallNum.xls'");
		pr.setAttribute("onclick", "printTable('t_data_div')");
		ex.disabled = false;
		pr.disabled = false;
	}
	else{
		alert(switchlanguage("noData"));
	}
}

function printTable(table){
	var pr_data = document.getElementById(table).innerHTML;
	var printPage = window.open("", "printPage", "");
	printPage.document.open();
	printPage.document.write("<OBJECT classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2' height=0 id=wc name=wc width=0></OBJECT>");
	printPage.document.write("<HTML><head></head><BODY onload='javascript:wc.execwb(7,1);window.close()'>");
	printPage.document.write("<PRE>");
	printPage.document.write(pr_data);
	printPage.document.write("</PRE>");
	printPage.document.close("</BODY></HTML>");
}