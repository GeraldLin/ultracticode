
var CircleObj = {
	dbe : null,
	color : ["#23B24C","#FF8021","#FFFF00","#FFA488", "#888888"],
	agname : "",
	extno : "",
	logintime : "",
	statetime : new Array(),
	stateduration : new Array(),
	totalduration: 0,

	init:function(base, idx) {

		this.totalduration = 0;
		for (var i = 0; i < this.statetime.length; i++) {
			tmp = this.statetime[i].split(":");
			this.stateduration[i] = parseInt(tmp[0], 10) * 3600 + parseInt(tmp[1], 10) * 60 + parseInt(tmp[2], 10);
			this.totalduration += this.stateduration[i];
		}

		dbe = document.getElementById(base);
		var ccdiv = document.createElement("div");
		ccdiv.name = "ccdiv_"+idx; ccdiv.id = "ccdiv_"+idx;
		ccdiv.className = "ccdivstyle";

		var s1 = document.createElement("div");
		s1.className = "ccs1style";
		s1.innerHTML = "<span>"+ switchlanguage("SeatWorker") +" :</span>&nbsp;<span id=\"ag1_"+idx +"\">"+ this.agname +"</span>";

		var s2 = document.createElement("div");
		s2.className = "ccs2style";
		s2.innerHTML = "<span>"+ switchlanguage("Ext") +" :</span>&nbsp;<span id=\"ag2_"+idx +"\">"+ this.extno +"</span>&nbsp;&nbsp;<span style='display:none;'>"+ switchlanguage("LoginTime") +" :</span>&nbsp;<span id=\"ag3_"+idx +"\">"+ this.logintime +"</span>";
		
		//1
		var s3 = document.createElement("div");
		s3.className = "ccs3style";
		var s4 = document.createElement("div");
		s4.id = "sp1_1_"+idx;
		s4.className = "ccs4style";
		s4.innerHTML = switchlanguage("wait");
		s3.appendChild(s4);
		var s5 = document.createElement("div");
		s5.id = "sp1_2_"+idx;
		s5.className = "ccs5style";
		s5.innerHTML = this.statetime[0];
		s3.appendChild(s5);
		
		//2
		var s6 = document.createElement("div");
		s6.id = "sp2_1_"+idx;
		s6.className = "ccs6style";
		s6.innerHTML = switchlanguage("inbound");
		s3.appendChild(s6);
		var s7 = document.createElement("div");
		s7.id = "sp2_2_"+idx;
		s7.className = "ccs7style";
		s7.innerHTML = this.statetime[1];
		s3.appendChild(s7);
		
		//3
		var s8 = document.createElement("div");
		s8.id = "sp3_1_"+idx;
		s8.className = "ccs8style";
		s8.innerHTML = switchlanguage("outbound");
		s3.appendChild(s8);
		var s9 = document.createElement("div");
		s9.id = "sp3_2_"+idx;
		s9.className = "ccs9style";
		s9.innerHTML = this.statetime[2];
		s3.appendChild(s9);
		
		//4
		var s10 = document.createElement("div");
		s10.id = "sp4_1_"+idx;
		s10.className = "ccs10style";
		s10.innerHTML = switchlanguage("state_14");
		s3.appendChild(s10);
		var s11 = document.createElement("div");
		s11.id = "sp4_2_"+idx;
		s11.className = "ccs11style";
		s11.innerHTML = this.statetime[3];
		s3.appendChild(s11);
		
		//5
		var s12 = document.createElement("div");
		s12.id = "sp5_1_"+idx;
		s12.className = "ccs12style";
		s12.innerHTML = switchlanguage("others");
		s3.appendChild(s12);
		var s13 = document.createElement("div");
		s13.id = "sp5_2_"+idx;
		s13.className = "ccs13style";
		s13.innerHTML = this.statetime[4];
		s3.appendChild(s13);
		
		// var s14 = document.createElement("div");
		// s14.id = "sp6_1_"+idx;
		// s14.className = "ccs14style";
		// s14.innerHTML = switchlanguage("RestTab");
		// s3.appendChild(s14);
		// var s15 = document.createElement("div");
		// s15.id = "sp6_2_"+idx;
		// s15.className = "ccs15style";
		// s15.innerHTML = this.statetime[5];
		// s3.appendChild(s15);

		var ccanvas = document.createElement("canvas");
		ccanvas.id = "ca_"+idx;
		ccanvas.width = 90;
		ccanvas.height = 90;
		ccanvas.className = "cccanvasstyle";
		var ctx = ccanvas.getContext("2d");
		var startPoint = 1.25 * Math.PI;
		for (var i = 0; i < this.stateduration.length; i++) {
			ctx.fillStyle = this.color[i];
			ctx.strokeStyle = this.color[i];
			ctx.beginPath();
			ctx.moveTo(45, 45);
			ctx.arc(45,45,42,startPoint,startPoint+Math.PI*2*(Math.round(this.stateduration[i] / this.totalduration * 100) / 100),false);
			ctx.fill();
			ctx.stroke();
			startPoint += Math.PI*2*(Math.round(this.stateduration[i] / this.totalduration * 100) / 100);
		}

		ccdiv.appendChild(s1);
		ccdiv.appendChild(s2);
		ccdiv.appendChild(s3);
		ccdiv.appendChild(ccanvas);
		dbe.appendChild(ccdiv);

	},
	setting:function(idx, p1, p2, p3, p4) {

		document.getElementById("ag1_"+idx).innerText = this.agname = p1;
		document.getElementById("ag2_"+idx).innerText = this.extno = p2;
		document.getElementById("ag3_"+idx).innerText = this.logintime = p3;
		this.statetime = p4

		document.getElementById("sp1_2_"+idx).innerHTML = this.statetime[0];
		document.getElementById("sp2_2_"+idx).innerHTML = this.statetime[1];
		document.getElementById("sp3_2_"+idx).innerHTML = this.statetime[2];
		document.getElementById("sp4_2_"+idx).innerHTML = this.statetime[3];
		document.getElementById("sp5_2_"+idx).innerHTML = this.statetime[4];
		// document.getElementById("sp6_2_"+idx).innerHTML = this.statetime[5];

		this.totalduration = 0;
		for (var i = 0; i < this.statetime.length; i++) {
			tmp = this.statetime[i].split(":");
			this.stateduration[i] = parseInt(tmp[0], 10) * 3600 + parseInt(tmp[1], 10) * 60 + parseInt(tmp[2], 10);
			this.totalduration += this.stateduration[i];
		}

		var ccanvas = document.getElementById("ca_"+idx);
		var ctx = ccanvas.getContext("2d");
		ctx.clearRect(0, 0, ccanvas.width, ccanvas.height);
		var startPoint = 1.25 * Math.PI;
		for (var i = 0; i < this.stateduration.length; i++) {
			ctx.fillStyle = this.color[i];
			ctx.strokeStyle = this.color[i];
			ctx.beginPath();
			ctx.moveTo(45, 45);
			ctx.arc(45,45,42,startPoint,startPoint+Math.PI*2*(Math.round(this.stateduration[i] / this.totalduration * 100) / 100),false);
			ctx.fill();
			ctx.stroke();
			startPoint += Math.PI*2*(Math.round(this.stateduration[i] / this.totalduration * 100) / 100);
		}
	}
};
