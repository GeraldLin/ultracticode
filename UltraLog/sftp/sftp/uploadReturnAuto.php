<?php
	/*foreach (array_keys($_POST) as $key) $$key = $_POST[$key];
	foreach (array_keys($_GET) as $key) {
		$$key = (isset($$key) ? $$key : $_GET[$key]);
	}*/
	$xPost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$xGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	if (is_array($xPost)) foreach (array_keys($xPost) as $key) $$key = $xPost[$key];
	if (is_array($xGet)) {
		foreach (array_keys($xGet) as $key) {
			$$key = (isset($$key) ? $$key : $xGet[$key]);
		}
	}

$isTest = 0;
if($isTest){
	for($i=0;$i<100;$i++){
		print_r(htmlspecialchars($SerialNo = date("Ymdhis") . (string)(600000+$i)));
		print_r(htmlspecialchars("\r\n"));
	}
	return;	
}
ini_set('memory_limit', '256M');
error_reporting(0);
logf("start uploadReturnAuto.php");
logs("開始匯入Excel");
header("Content-Type:text/html; charset=utf-8");
include('db.php');
include "PHPExcel-master/PHPExcel.php";
include("PHPExcel-master/PHPExcel/IOFactory.php");

$newPath = './upload/自動退貨.xls';

$uploader = $this->_objectManager->create('Magento\MediaStorage\Model\File\Uploader',['fileId' => 'file_name']);
$fInam = $uploader->validateFile();

//if(isset($_FILES["UploadFile"])) 
if(isset($fInam["UploadFile"])) 
{
	if($fInam["UploadFile"]["error"] == 0)
	{
		//$tmpPath = $_FILES["UploadFile"]["tmp_name"];
		//$filename = $_FILES["UploadFile"]["name"];
		$tmpPath = $fInam["UploadFile"]["tmp_name"];
		$filename = $fInam["UploadFile"]["name"];
		if(move_uploaded_file($tmpPath, $newPath))
		{
			xls2db($newPath, "costcodb", "Return");
		}else
		{
			logfs("上傳失敗");
		}
	}
	else
	{
		//var_dump($_FILES["UploadFile"]["error"]);
		logfs("上傳失敗");
	}
}
else
{
	logfs("檔案不存在");
}
logf("end uploadReturnAuto.php");

function xls2db($xlsDir, $dbname, $table) {
	global $cn,$AgentNo,$AgentName;

	//讓PHPExcel決定使用哪種格式讀取Excel
	$excel= PHPExcel_IOFactory::load($xlsDir); 

	//getAllSheets() 將所有Sheet轉成Sheet陣列
	$sheetTemp = $excel->getAllSheets();
	$sheetArr = $sheetTemp[0]->toArray();
	unset($sheetArr[0]); //刪除第一列(表頭)
	
	//檢查資料完整性
	$tempTable = array();
	foreach ($sheetArr as $i => $row)
	{
		/*
		$tempTable[$i]['date'] = $row[0];
		$tempTable[$i]['SerialNo'] = $row[1];
		$tempTable[$i]['MemberCard'] = $row[3];
		$tempTable[$i]['MemberAcc'] = $row[4];
		$tempTable[$i]['MemberName'] = $row[5];
		$tempTable[$i]['OrderNo'] = $row[6];
		$tempTable[$i]['Consignment'] = $row[7];
		$tempTable[$i]['ItemNo'] = $row[8];
		$tempTable[$i]['ItemName'] = $row[9];
		$tempTable[$i]['Reason'] = $row[11];
		$tempTable[$i]['ItemAmount'] = $row[14];
		$tempTable[$i]['ShipperPost'] = $row[18];
		$tempTable[$i]['ShipperAddr'] = $row[19].$row[20];
		*/
		$tempTable[$i][0] = $row[0]; //date
		$tempTable[$i][1] = $row[1]; //SerialNo
		$tempTable[$i][2] = $row[3]; //MemberCard
		$tempTable[$i][3] = $row[4]; //MemberAcc
		$tempTable[$i][4] = $row[5]; //MemberName
		$tempTable[$i][5] = $row[6]; //OrderNo
		$tempTable[$i][6] = $row[7]; //Consignment
		$tempTable[$i][7] = $row[8]; //Tracking ID
		$tempTable[$i][8] = $row[9]; //ItemNo
		$tempTable[$i][9] = $row[10]; //ItemName
		$tempTable[$i][10] = $row[12]; //Reason
		$tempTable[$i][11] = $row[15]; //ItemAmount
		$tempTable[$i][12] = $row[17]; //ShipperName
		$tempTable[$i][13] = $row[18]; //ShipperPhone
		$tempTable[$i][14] = $row[19]; //ShipperPost
		$tempTable[$i][15] = $row[20].$row[21]; //ShipperAddr
		$tempTable[$i][16] = $row[14]; //RequestID
	}
	//檢查資料完整性
	/*
	$err = '';
	foreach($tempTable as $i => $row){
		foreach($row as $key => $col){
			if($col == '') $err .= "未填寫{$i}列{$key}欄,";
		}
	}
	if($err !== ''){
		logfs("匯入失敗 : Excel檔缺少必要欄位");
		return;
	}
	*/

	$sql = "
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @insAcpt int SET @insAcpt = 0
	DECLARE @insRtn int SET @insRtn = 0
	DECLARE @M int SET @M = 0
	BEGIN TRAN ImportAutoReturn
	";
	foreach ($tempTable as $i => $row){
		$row = str_replace("'","''", $row);
		$RMA_NO = $row[1];
		if( !isset($WorkOrderArr[$RMA_NO]) ) {
			$WorkOrderArr[$RMA_NO] = date("Ymdhis") . (string)(600000+$i);
			$sql .= "
			IF NOT EXISTS(select top 1 CustomNo from [callcenterdb].[dbo].[tbcustomer] where CustomNo = '{$row[2]}')
			BEGIN
					INSERT INTO [callcenterdb].[dbo].[tbcustomer](CustomNo, CustName) values('{$row[2]}', '{$row[4]}')
			END
			ELSE
			BEGIN
				UPDATE [callcenterdb].[dbo].[tbcustomer] set CustName='{$row[4]}' where CustomNo='{$row[2]}'
			END
			";
			$sql .= "
			IF NOT EXISTS(select top 1 GId from [callcenterdb].[dbo].[tbaccept] where [SerialNo] = '{$WorkOrderArr[$RMA_NO]}')
				INSERT INTO [callcenterdb].[dbo].[tbaccept]
				([GId]
				,[SerialNo]
				,[CustomTel]
				,[SeatNo]
				,[WorkerNo]
				,[CustomNo]
				,[AcptType]
				,[ACDTime]
				,[AcptTime]
				,[PutTime]
				,[RelTime]
				,[ACWTime]
				,[AcptLen]
				,[AcptCont]
				,[DealWorker]
				,[DealTime]
				,[DealState]
				,[DealLog]
				,[AnsId]
				,[AcceptScore]
				,[AcptSubType]
				,[RecdRootPath]
				,[RecdFile]
				,[RecdCallID]
				,[SrvSubType]
				,[DepartmentNo]
				,[MediaType]
				,[ProdType]
				,[ProdModelType]
				,[CallBackID]
				,[CallBackTime]
				,[CallBackWorkerNo]
				,[Score]
				,[ScoreId]
				,[EcType]) VALUES
				(NEWID() --gid v
				,'{$WorkOrderArr[$RMA_NO]}' --serialno v {$WorkOrderArr[$RMA_NO]}  20200429033043600001
				,'{$row[13]}' --customtel
				,'' --seatno
				,'{$AgentNo}' --workerno v
				,'{$row[2]}' --customno
				,'2' --acpttype
				,getdate() --acdtime
				,getdate() --acpttime
				,getdate() --puttime
				,getdate() --realtime
				,getdate() --acwtime
				,'0' --acptlen
				,'自動退貨' --acptcont
				,'{$AgentNo}' --dealworker
				,getdate() --dealtime
				,4 --dealstate
				,convert(varchar, getdate(), 120) + ' [{$AgentName}-{$AgentNo}]'	 --deallog 2020-04-24 12:49:32 [方敘蒨-730]
				,1
				,0
				,21
				,''
				,'' --file
				,'' --recdcallid
				,4
				,0
				,0
				,6
				,0
				,0
				,NULL
				,NULL
				,0
				,0
				,2)
			ELSE
				SET @M = @M + 1
			SET @insAcpt = @insAcpt + @@ROWCOUNT
			";
		}
		//虛擬訂單號$sn = 工單 + RMA_NO後6碼
		$sn = $WorkOrderArr[$RMA_NO] . substr($RMA_NO, -6);
		$sql .= "
			INSERT INTO [{$dbname}].[dbo].[{$table}] (WorkOrder,           ImportFlag, date, SerialNo, MemberCard,   MemberAcc,    MemberName,   OrderNo,      Consignment,  ANo,          ItemNo,       ItemName,     Reason,        ItemAmount,    ShipperName,   ShipperPhone,  ShipperPost,   ShipperAddr,   AgentNo,       AgentName,     Comment,          ShipState,   SubmitFlag, TempLevel, Dropship_Depot, TranCode) 
								              VALUES($WorkOrderArr[$RMA_NO],1, getdate(), N'{$sn}',     N'{$row[2]}', N'{$row[3]}', N'{$row[4]}', N'{$row[5]}', N'{$row[6]}', N'{$row[7]}', N'{$row[8]}', N'{$row[9]}', N'{$row[10]}', N'{$row[11]}', N'{$row[12]}', N'{$row[13]}', N'{$row[14]}', N'{$row[15]}', '{$AgentNo}', '{$AgentName}', N'自動退貨{$row[0]}', N'逆物流', N'', (select top 1 TempLevel from costcodb.dbo.VendorInfo where ItemNo='{$row[8]}'), (select top 1 Dropship_Depot from costcodb.dbo.VendorInfo where ItemNo='{$row[8]}'), N'{$row[16]}');
			SET @insRtn = @insRtn + @@ROWCOUNT
		";
	}
	$sql .= "
		IF @M = 0
			COMMIT TRANSACTION ImportAutoReturn;
		ELSE
			ROLLBACK TRANSACTION
		SET XACT_ABORT OFF;
		select @insAcpt as insAcpt, @insRtn as insRtn, @M as M;
	";
	logfs(print_r($WorkOrderArr, true));
	logf($sql);
	$rs = sqlsrv_query($cn, $sql);
	if( $rs === false ) {
		if( ($errors = sqlsrv_errors() ) != null) {
			foreach( $errors as $error ) {
				logfs("SQLSTATE: ".$error['SQLSTATE']);
				logfs("code: ".$error['code']);
				logfs("message: ".$error['message']);
			}
		}
	}
	else{
		if($row = sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC)){
			if( $row['M'] == 0 ){
				logfs("已匯入工單 {$row['insAcpt']} 筆");
				logfs("已匯入退貨記錄 {$row['insRtn']} 筆");				
			}
			else{
				logfs("匯入失敗，資料庫已有重複的工單序號 {$row['M']} 筆");						
			}
		}
	}
}

function logf($m){
	$m = date('h:i:s')." ".$m."\r\n";
	file_put_contents("./log/uploadReturnAuto".date('ymd').".log", $m, FILE_APPEND);
}
function logs($m){
	$m = date('h:i:s')." ".$m."\r\n";
	print_r(htmlspecialchars($m."<br/>"));
}
function logfs($m){
	$m = date('h:i:s')." ".$m."\r\n";
	print_r(htmlspecialchars($m."<br/>"));
	file_put_contents("./log/uploadReturnAuto".date('ymd').".log", $m, FILE_APPEND);
}
