set PATH1=C:\AppServ\www\UltraLog\costco\upload\csv\Ambient\
mkdir "%PATH1%"
set PATH2=C:\AppServ\www\UltraLog\costco\upload\csv\Coldchain\
mkdir "%PATH2%"
set PATH3=C:\AppServ\www\UltraLog\costco\upload\csv\Large\
mkdir "%PATH3%"

WinSCP.com /command ^
    "open sftp://Costco:3F7FC66E8490475B@60.249.123.88/ -rawsettings ProxyMethod=3 ProxyHost=10.120.7.62" ^
    "get ""/usr/home/Costco/COSTCOCS/PICKUP/Ambient/PENDING/*"" -filemask=*.csv ""%PATH1%""" ^
    "mv ""/usr/home/Costco/COSTCOCS/PICKUP/Ambient/PENDING/*"" ""/usr/home/Costco/COSTCOCS/PICKUP/Ambient/PROCESSED/*""" ^
    "get ""/usr/home/Costco/COSTCOCS/PICKUP/Coldchain/PENDING/*"" -filemask=*.csv ""%PATH2%""" ^
    "mv ""/usr/home/Costco/COSTCOCS/PICKUP/Coldchain/PENDING/*"" ""/usr/home/Costco/COSTCOCS/PICKUP/Coldchain/PROCESSED/*""" ^
    "get ""/usr/home/Costco/COSTCOCS/PICKUP/Large/PENDING/*"" -filemask=*.csv ""%PATH3%""" ^
    "mv ""/usr/home/Costco/COSTCOCS/PICKUP/Large/PENDING/*"" ""/usr/home/Costco/COSTCOCS/PICKUP/Large/PROCESSED/*""" ^
    "exit"

pause
