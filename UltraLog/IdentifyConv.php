<?php
	//A 台北市(10) B 台中市(11) C 基隆市(12) D 台南市(13) E 高雄市(14) F 台北縣(15) G 宜蘭縣(16) H 桃園縣(17)
	//J 新竹縣(18) K 苗栗縣(19) L 台中縣(20) M 南投縣(21) N 彰化縣(22)
	//P 雲林縣(23) Q 嘉義縣(24) R 台南縣(25) S 高雄縣(26) T 屏東縣(27) U 花蓮縣(28) V 台東縣(29)
	//W 金門縣(32) X 澎湖縣(30) Y 陽明山(31) Z 馬祖(33) 
	
	$xPost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$xGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	if (is_array($xPost)) foreach (array_keys($xPost) as $key) $$key = $xPost[$key];
	if (is_array($xGet)) {
		foreach (array_keys($xGet) as $key) {
			$$key = (isset($$key) ? $$key : $xGet[$key]);
		}
	}
	
	//$idn = $_POST["idn"];
	//$url_r = parse_url($_SERVER["REQUEST_URI"]);    //取的url, 再分析url
	//parse_str($url_r["query"]);
	//echo $idn;
	$eng = null;
	$eng["A"] = (1*1) + (0*9);
	$eng["B"] = (1*1) + (1*9);
	$eng["C"] = (1*1) + (2*9);
	$eng["D"] = (1*1) + (3*9);
	$eng["E"] = (1*1) + (4*9);
	$eng["F"] = (1*1) + (5*9);
	$eng["G"] = (1*1) + (6*9);
	$eng["H"] = (1*1) + (7*9);
	$eng["I"] = (3*1) + (4*9);
	$eng["J"] = (1*1) + (8*9);
	$eng["K"] = (1*1) + (9*9);
	$eng["L"] = (2*1) + (0*9);
	$eng["M"] = (2*1) + (1*9);
	$eng["N"] = (2*1) + (2*9);
	$eng["O"] = (2*1) + (4*9);
	$eng["P"] = (2*1) + (3*9);
	$eng["Q"] = (2*1) + (4*9);
	$eng["R"] = (2*1) + (5*9);
	$eng["S"] = (2*1) + (6*9);
	$eng["T"] = (2*1) + (7*9);
	$eng["U"] = (2*1) + (8*9);
	$eng["V"] = (2*1) + (9*9);
	$eng["W"] = (3*1) + (2*9);
	$eng["X"] = (3*1) + (0*9);
	$eng["Y"] = (3*1) + (1*9);
	$eng["Z"] = (3*1) + (3*9);
	
	$arrNum = str_split($idn);
	$remainder = 10 - ($arrNum[8]);
	$totalNum = (($arrNum[0])*8)+(($arrNum[1])*7)+(($arrNum[2])*6)+(($arrNum[3])*5)+(($arrNum[4])*4)+
				(($arrNum[5])*3)+(($arrNum[6])*2)+(($arrNum[7])*1);
	$s = "";
	// echo $remainder;
	// print_r($arrNum);
	//if ((strlen($idn) == 9) && (($arrNum[0] == 1) || ($arrNum[0] == 2))) {
	if ((strlen($idn) == 9) && (($arrNum[0] == 1) || ($arrNum[0] == 2) || ($arrNum[0] == 8) || ($arrNum[0] == 9))) {
		foreach($eng as $i => $val){
			// echo $val;
			if((($totalNum + $val) % 10) == ($remainder % 10)){
				$s .= (($s == "") ? "" : ",").$i;
			}
		}
	}
	
	print_r(htmlspecialchars($s));
?>