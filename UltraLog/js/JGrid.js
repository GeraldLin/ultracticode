/*-------------------------------------------------------------

--------------------------------------------------------------*/
function JPoint(){
	this.x = 0;
	this.y = 0;
	
	this.set = function(x,y){
		self.x = x;
		self.y = y;
	}
}

function JPos(){
	JPoint.call(this);
	var self = this;
	
	this.getAbsPos = function(pTarget){
			self.x = 0;
			self.y = 0;
			while(pTarget.offsetParent){
					self.x += pTarget.offsetLeft;
					self.y += pTarget.offsetTop;
					pTarget = pTarget.offsetParent;
			}
			self.x += pTarget.offsetLeft;
			self.y += pTarget.offsetTop;
	}
	
	this.getMousePos = function(evt){
		evt = evt || window.event;
		if(evt.pageX || evt.pageY){
			self.x = evt.pageX;
			self.y = evt.pageY;
		}else{
			self.x = evt.clientX + document.body.scrollLeft - document.body.clientLeft;
			self.y = evt.clientY + document.body.scrollTop - document.body.clientTop;
		}
	}
	
	this.reset = function(){
		self.set(0,0)
	}
}

function JGrid(pBody,pWidth,pHeight){
	var self		=	this;
	
	this.debug	=	true;
	this.width	=	pWidth		|| 100;
	this.height	=	pHeight		|| 100;
	this.clientHeight = null;
	this.clientWidth	= null;
	
	this.filePath 				= "../images/";
	var PIC_DESC 			= this.filePath + "sort.desc.gif";
	var PIC_ASC				= this.filePath + "sort.asc.gif";
	var PIC_SPLITBAR		= this.filePath + "splitbar.gif";
	var PIC_BG_TITLE		= this.filePath + "bg.title.gif";
	var PIC_BG_ROWNUM	= this.filePath + "bg.rownum.gif";
	var PIC_HELP				= this.filePath + "pixel.gif";
	var PIC_FIND				= this.filePath + "tool.find.png";
	var PIC_WAIT				= this.filePath + "wait.gif";
	
	//***************************************************************
	var $= function(pObjID){
		try{
			return document.getElementById(pObjID);
		}catch(e){
			debugMsg("object: " + pObjID + "  不存在！");
		}
	}
	//***************************************************************
	
	var htmlObject = function(pTag){
		try{
			return document.createElement(pTag);
		}catch(e){
			debugMsg("不能产生控件：　" + pTag );
			return false;
		}
	}
	
	//***************************************************************
	
	this.body						=	$(pBody)	|| document.body;
	this.rowCountAreaWidth 	= 30;
	this.titleAreaHeight 		= 25;
	this.splitBarWidth			= 2;//这里取的是splitbar.gif的宽度。
	this.adjustBlockWidth		= 10;
	this.minColumnWidth		= 10;//栏位的最小宽度，这个值应大于splitBarWidth
	
	this.lineColor					= "#ffffff";//表格线色，对FireFox不起作用，因为在FireFox下，table的bordercolor不起作用。
	
	this.fieldDefaultLabel		= "";
	this.fieldDefaultWidth		= 100;//这个应大于minColumnWidth;
	this.fieldDefaultAlign		= "left";//对齐方式在FireFox下不起作用，还没有弄解决办法。
	this.fieldDefaultClassName = "";
	
	var adjustWidth				= 0;//不能改，这个值正好：3。
	
	var realWidth					= 2;	
	var isIE 						= (document.all == null) ? false : true;
	
	var oOutline 		= null;
	var oFrameTable = null;
	
	var oTopLeftAreaTd		=	null;
	var oTitleAreaTd			= null;
	var oRowNumAreaTd 	= null;
	var oDataTableAreaTd = null;
	
	var oTopLeftArea	=	null;
	var oTitleArea			= null;
	var oTitleAreaInner	= null;
	var oRowNumArea 	= null;
	var oDataTableArea = null;
	var oRowNumTable = null;
	var oDataTable		= null;
	var oColGroup			= null;//主数据table的colGroup
	
	var oExtendArea		= 0;
	var extendHeight 	= 1;		//扩展区的高度
	var extendAreaInitHeight = 1;
	var extended			= false;
	
	var toolBtnHeight	= 0;
	var toolBtnWidth	= 0;
	var oToolBtnsTable	= null;
	var colorSheet	= new Array("#0033FF","#33CC00","#007575","#FF00FF","#CCCC00","#00B7B7","#99FF33","#71FFFF","#FF3333","#CCFF66");
	
	var oWaiting			= null;//等待提示，是否拿掉？
	
	this.activeCell			= null;//被鼠标点中的单元格。
	this.oActiveRow		= null;
	
	var mouseDown		= false;//用于调整栏位宽度
	var orgPos				= new JPos();
	var newPos			= new JPos();
	var oAdjustBar		= null;
	var adjustIndex		= null;
	var oAdjustTitle		= null;
	
	var sortField			= null;
	//***************************************************************
	
	var debugMsg	= function(pMsg){
		if(self.debug)
			alert(pMsg);
	}
	
	//***************************************************************
	
	var isRate = function(pRateString){
		if(!isNaN(pRateString)) return false;
		if(pRateString.substr(pRateString.length-1,1) != "%")
			return false;
		if(isNaN(pRateString.substring(0,pRateString.length - 1)))
			return false;
		return true;
	}
	
	//***************************************************************
	
	var check = function(){
		if(isNaN(self.width)  && !isRate(self.width)){
			debugMsg(self.width + " :不是一个有效的Width值，必须以数字或百分数为值" );
			return false;
		}
		if(isNaN(self.height)  && !isRate(self.height)){
			debugMsg(self.height + " :不是一个有效的Width值，必须以数字或百分数为值" );			
			return false;
		}
		return true;
	}
	
	//***************************************************************
	
	var createWaiting = function(){
		oWaiting = htmlObject("DIV");
		self.body.appendChild(oWaiting);
		oWaiting.className = "oWaiting";
		orgPos.reset();
		orgPos.getAbsPos(oOutline);
		oWaiting.style.cssText = "background-image: url(" + PIC_WAIT +");background-repeat: no-repeat;width:120px;height:23px;"
											+ "display:none;top:position:absolute;top:" + (orgPos.y) + "px;left:" + (orgPos.x) + "px;"
	}
	
	var showWaiting = function(pShow){
			oWaiting.style.display = (pShow ? "" : "none");
	}
	
	//***************************************************************
	
	var oTopLeftAreaTd_click = function(p){
		alert(arguments.length)
	}
	
	//***************************************************************
	
	var createFrameTable = function(){
		oFrameTable 	= htmlObject("TABLE");
		oFrameTable.border = 0;
		oFrameTable.className = "oFrameTable";
		oFrameTable.style.cssText += "border-collapse:collapse;width:100%;height:" + pHeight + "px";	
				
		var tTbody 	= htmlObject("TBODY");
		oFrameTable.appendChild(tTbody);
		
		var tTr1			= htmlObject("TR");
		tTbody.appendChild(tTr1);
		oTopLeftAreaTd = htmlObject("TD");
		tTr1.appendChild(oTopLeftAreaTd);
		oTopLeftAreaTd.className = "oTopLeftAreaTd";
		oTopLeftAreaTd.align = "center";
		oTopLeftAreaTd.valign = "middle";
				
		oTopLeftAreaTd.style.cssText += "width:" + self.rowCountAreaWidth + "px;height:"
																   + self.titleAreaHeight + "px;";
		
		tHelpImg = htmlObject("IMG");
		oTopLeftAreaTd.appendChild(tHelpImg);
		tHelpImg.src = PIC_HELP;
		
		oTopLeftAreaTd.onclick = oTopLeftAreaTd_click;
																		   
		oTitleAreaTd		= htmlObject("TD");
		tTr1.appendChild(oTitleAreaTd);
		oTitleAreaTd.className = "oTitleAreaTd";
		
		
		
		var tTr2				= htmlObject("TR");
		tTbody.appendChild(tTr2);
		oRowNumAreaTd		= htmlObject("TD");
		oRowNumAreaTd.className = "oRowNumAreaTd";
		oRowNumAreaTd.style.cssText += "height:" + (pHeight - self.titleAreaHeight - 4 ) + "px";
				
		oDataTableAreaTd	= htmlObject("TD");
		oDataTableAreaTd.className = "oDataTableAreaTd";
		
		
		
		tTr2.appendChild(oRowNumAreaTd);
		tTr2.appendChild(oDataTableAreaTd);
		
		tTbody = null;
		tTr1 = null;
		tTr2 = null;
	}
	
	//***************************************************************
	
	var findValue = function(){
		var tValue = prompt("请输入要查询的关键词\n如果要查询多个关键词，请以英文逗号分隔","");
		if(tValue == null) return;
		var tValues = tValue.split(",");
		
		alert(oDataTable.innerText)
		for(var i = 0;tValue = tValues[i];i++){
			//oDataTable.innerHTML = oDataTable.innerHTML.replace(tValue,"<span style='background-color:" + colorSheet[i] + "'>" + tValue + "</span>");	
		}
	}
	
	var help = function(){
		alert("谢谢使用JGrid\nAuthor:xlingFairy\nBlog:http://xling.blueidea.com");
	}
	
	//***************************************************************
	var extendToolButton_mouseover = function(){
		this.className = "oToolBtnActive";
	}
	
	var extendToolButton_mouseout	= function(){
		this.className = "oToolBtn";
	}
	//***************************************************************
	
	var createExtendToolButton = function(pTip,pImage,pFunction){
		var tBtn = htmlObject("DIV");
		tBtn.style.cssText = "width:" + toolBtnWidth 
												 + "px;height:" 
												 + toolBtnHeight 
												 + "px;background-image:url(" + pImage + ");"
												 + "background-repeat: no-repeat;background-position: center center;";
		
		
		tBtn.onmouseover = extendToolButton_mouseover;
		tBtn.onmouseout = extendToolButton_mouseout;		
		tBtn.onclick = pFunction;
		
		var tCell = oToolBtnsTable.rows[0].insertCell(-1);
		tCell.appendChild(tBtn);
		tCell.width = toolBtnHeight + 2;
		tCell.height = extendHeight;
		tCell.align = "center";
		tCell.vAlign = "middle";
		tCell.title = pTip;
		
		tBtn = null;
		tCell = null;
	}
	
	//***************************************************************	
	var oExtendArea_mouseover = function(){
			if(!extended){
				oOutline.style.height = oOutline.clientHeight + extendHeight - extendAreaInitHeight + "px";
				extended = true;
			}		
	}
	
	var oExtendArea_mouseout = function(){
			if(extended) {
				oOutline.style.height = oOutline.clientHeight - extendHeight + extendAreaInitHeight + "px";
				extended = false;
			}		
	}
	//***************************************************************	
	
	var createExtendArea = function(){
		oExtendArea = htmlObject("DIV");
		oExtendArea.className = "oExtendArea";
		oExtendArea.style.cssText = "width:100%;height:" + extendHeight + "px;margin-top:auto;line-height:" + extendHeight + "px;";
		
		oExtendArea.onmouseover = oExtendArea_mouseover;
		
		oExtendArea.onmouseout = oExtendArea_mouseout;
		
		oToolBtnsTable = htmlObject("TABLE");
		oExtendArea.appendChild(oToolBtnsTable);
		
		oToolBtnsTable.insertRow(-1);		
		
		createExtendToolButton("查找",PIC_FIND,findValue);
		createExtendToolButton("帮助",PIC_HELP,help);
	}
	
	//***************************************************************	
	var createOutline = function(){
		oOutline = htmlObject("DIV");
		self.body.appendChild(oOutline);
		oOutline.className = "oOutline";

		with(oOutline.style){
			width 	= 	(isRate(self.width) ? self.width : self.width + "px");
			height 	= 	(isRate(self.height) ? self.height : self.height + "px");
			overflow =	 "hidden";
		}
		oOutline.style.height = oOutline.clientHeight + extendAreaInitHeight + "px";

		createFrameTable();
		oOutline.appendChild(oFrameTable);
		createExtendArea();
		oOutline.appendChild(oExtendArea);
		
		self.clientWidth = oOutline.clientWidth;
		self.clientHeight = oOutline.clientHeight;
	}
	
	
	//***************************************************************
	var oDataTable_mouseover = function(evt){
			evt = evt || window.event;
			var obj = evt.srcElement || evt.target;
			if(obj.tagName == "TD")
				obj.title = obj.innerHTML;
			obj = (evt.srcElement || evt.target).parentNode;
			if(obj.tagName == "TR"  && obj.className != "oActiveRow"){
				obj.baseClassName = obj.className;
				obj.className = "rowActive";
			}
			
			
			obj = null;		
	}
	
	var oDataTable_mouseout = function(evt){
			evt = evt || window.event;
			var obj = (evt.srcElement || evt.target).parentNode; 
			if(obj.tagName == "TR" && obj.className != "oActiveRow"){
				obj.className = obj.baseClassName;
			}
			
			obj = null;		
	}
	
	var oDataTable_click = function(evt){
			evt = evt || window.event;
			var obj = evt.srcElement || evt.target;
		if(obj.tagName == "TD"){
				self.oActiveCell == null ? null : self.oActiveCell.className = "";
//				obj.className = "oActiveCell"
				obj.className = ""
				self.oActiveCell = obj;
			}
			
			obj = null;			
	}
	
	var oDataTable_keydown = function(){
			evt = arguments[0] || window.event;
			var obj = self.oActiveCell;
			var refObj = null;
			var tCol = obj.cellIndex;
			var tRow = obj.parentNode.rowIndex;
			
			refObj = obj;
			
			switch(evt.keyCode || evt.which){
				case 9:
				case 37://左
					obj = obj.previousSibling || null;
					refObj = obj;/*--------------------------------*/
					break;
				case 39://右
					obj = obj.nextSibling || null;
					break;
				case 38://上
					tRow = (tRow > 0 ? tRow - 1 : 0);
					obj = oDataTable.rows[tRow].cells[tCol];
					break;
				case 40://下
					tRow = (tRow ==oDataTable.rows.length - 1 ? tRow : tRow + 1);
					obj = oDataTable.rows[tRow].cells[tCol];
					break;
			}
			
			if(obj){
				obj.click();
				oDataTableArea.scrollLeft = refObj.offsetLeft;
				oDataTableArea.scrollTop	= refObj.offsetTop;
			}
			
			obj = null;
			refObj = null;
			tColl = null;
			tRow = null;		
	}
	//***************************************************************
	var oDataTableArea_scroll = function(){
			oRowNumArea.scrollTop = this.scrollTop;
			oTitleArea.scrollLeft		  = this.scrollLeft;		
	}
	//***************************************************************	
	
	this.bindDataTable = function(pTab){

		oDataTable = $(pTab);
		oDataTable.border = 0;
		oDataTable.borderColor = self.lineColor;		
		oDataTable.width = 100;
		/*------------
		oDataTable.width = 100;这一句至关重要，不能删除它。否则会引影FireFox下的单元格内容截断。值是大于0的数字。
		----------------*/
		oDataTable.className = "oDataTable";		
		with(oDataTable.style){
			borderCollapse = "collapse";
			tableLayout="fixed";
		}
		
		oDataTable.onmouseover = oDataTable_mouseover;
		oDataTable.onmouseout = oDataTable_mouseout;
		oDataTable.onclick = oDataTable_click;
		oDataTable.onkeydown = oDataTable_keydown;
		
		oDataTableArea = htmlObject("DIV");
		oDataTableArea.appendChild(oDataTable);
		oDataTableAreaTd.appendChild(oDataTableArea);
		oDataTableArea.style.cssText = "overflow:auto;width:" 
														+ (self.width - self.rowCountAreaWidth - 4) 

														+ "px;height:" + (pHeight - self.titleAreaHeight - 4) 
														+ "px;margin:0px";

		
		oDataTableArea.onscroll = oDataTableArea_scroll;
			
		var tTbody = oDataTable.firstChild;
		oColGroup = htmlObject("COLGROUP");//把这个colGroup的追加放到了另外一个方法里了。
		
		var tCol;
		for(var i=0;i<oDataTable.rows[0].cells.length;i++){
			tCol = htmlObject("COL");
			oColGroup.appendChild(tCol);
		}
		
		tCol = null;
		tTbody= null;
	}
	//***************************************************************
	
	var getFieldLabel = function(pField){
		return pField[0] || self.fieldDefaultLabel;
	}
	
	var getFieldWidth = function(pField){
		var w;
		w = pField[1] || self.fieldDefaultWidth;
		return (w < self.minColumnWidth ? self.minColumnWidth : w);
	}
	
	var getFieldAlign = function(pField){
		return pField[2] || self.fieldDefaultAlign;
	}
	
	var getFieldClassName = function(pField){
		return pField[3] || self.fieldDefaultClassName;
	}
	
	//***************************************************************
	
	var updateWidth = function(pAdjustWidth){
		mouseDown = false;

		var w = parseInt(oAdjustTitle.childNodes[0].style.width);
		
		if(w <= self.minColumnWidth && pAdjustWidth < 0)//必须是小等于10
			return false;
			
		if(w + pAdjustWidth < self.minColumnWidth)//pAdjustWidth些时一定小于3或为负了！
			pAdjustWidth = self.minColumnWidth - w;
			

		oAdjustTitle.childNodes[0].style.width = w + pAdjustWidth + "px";
		
		w = parseInt(oAdjustTitle.style.width);		
		oAdjustTitle.style.width = w + pAdjustWidth + "px";
		
		w = parseInt(oTitleAreaInner.style.width);
		oTitleAreaInner.style.width = w + pAdjustWidth + "px";
		
		w = parseInt(oColGroup.childNodes[adjustIndex].style.width);
		oColGroup.childNodes[adjustIndex].style.width = w + pAdjustWidth  + "px";
		
		if(!isIE)
			oDataTable.width = oDataTable.clientWidth + pAdjustWidth + 3;
		
		reBindRowNum();
		/*
		var t;
		var adjustHeight = isIE ? -3 : 2;		
		for(var i = 0;t = oRowNumTable.rows[i];i++)
			try{
				oRowNumTable.rows[i].cells[0].height = t.cells[0].clientHeight + adjustHeight
			}catch(e){
				alert(i + "\n" + e.message);
			};
		*/
			
		//oRowNumTable.height = oDataTable.clientHeight;
	}
	
	//***************************************************************
	var fieldLabel_selectstart = function(){
		return false;	
	}
	var fieldLabel_mouseover = function(){
		mouseDown = false;	
	}
	
	//排序
	var fieldLabel_click = function(){
		if(oDataTable.rows.length < 2 ) return false;
		
		if(sortField != null)
			sortField.style.backgroundImage = "url()";
		
		sortField = this;
		this.style.backgroundImage = "url(" + (this.sortMode ? PIC_DESC : PIC_ASC) + ")";
		this.style.backgroundRepeat = "no-repeat";
		this.style.backgroundPositionX = "95%";
		this.style.backgroundPositionY = "center";
		
		
		
		var tab_array = new Array();
		this.sortMode = (this.sortMode == null ? true : !this.sortMode);
		
		var tRow;
		for(var i = 0;tRow = oDataTable.rows[i];i++){
			tab_array.push(new Array(oDataTable.rows[i].cells[this.index].innerHTML.toLowerCase(),tRow));	
		}
		
		var sortMode = this.sortMode;
		tab_array.sort(function (){
								var arrA = arguments[0] , arrB = arguments[1];
								var a = arrA[0] , b = arrB[0];
								//========================================
								if(/^(\+|-)?\d+($|\.\d+$)/.test(a) && /^(\+|-)?\d+($|\.\d+$)/.test(b)){
									a=evil(a);	//2020-11-03 gerald for eval
									b=evil(b);	//2020-11-03 gerald for eval
								}else{
									a=a.toString();
									b=b.toString();
								}
								
								return (sortMode ? (a>b ? 1:(a<b ? -1:0)) : (a<b ? 1:(a>b ? -1:0)));
								//========================================																			
							}
							);
		
		var tTbody = oDataTable.getElementsByTagName("TBODY")[0];
		try{//这样做是为了提高排序效率．
			for(i=0;tTbody.appendChild(tab_array[i][1]);i++){};
		}catch(e){}
		
		tTbody = null;
		tRow = null;
		tab_array = null;
		reBindRowNum2();
	}
	//***************************************************************
	
	var createFieldLabel = function(pLabel,pWidth,pIndex){
		var tLabel = htmlObject("DIV");
		tLabel.innerHTML = pLabel;
		tLabel.style.cssText = "width:" + (pWidth - self.splitBarWidth)
													+ "px;height:" + self.titleAreaHeight 
													+ "px;float:left;-moz-user-select:none;overflow:hidden";
													+ "background-repeat: no-repeat;background-position: right center;";
		
		tLabel.index = pIndex;//////////////////////////////////
		tLabel.onselectstart = fieldLabel_selectstart;
		tLabel.onmouseover = fieldLabel_mouseover;
		tLabel.onclick = fieldLabel_click;
		
		return tLabel;
	}
	
	//***************************************************************
	
	var oAdjustBar_mouseout = function(){
		oAdjustBar.style.display = "none";		
	}
	
	var oAdjustBar_mousedown = function(){
			evt = arguments[0] || window.event;
			
			if((isIE && evt.button !== 1) || (!isIE && evt.button !==0)){
				return false;
			}			
			
			mouseDown = true;
			orgPos.getMousePos(evt);		
	}
	
	var oAdjustBar_mousemove = function(){
			if(!mouseDown) return false;
			evt = arguments[0] || window.event;
			newPos.getMousePos(evt);
			oAdjustBar.style.left = newPos.x - self.adjustBlockWidth  + "px";//////////////////////////////////			
	}
	
	var oAdjustBar_mouseup = function(){
			newPos.getMousePos(evt);		
			updateWidth(newPos.x - orgPos.x);
			orgPos.reset();
			mouseDown = false;	
	}
	
	//***************************************************************
	
	var createAdjustBar = function(){
		oAdjustBar = htmlObject("DIV");
		oOutline.appendChild(oAdjustBar);
		
		if(isIE)
			oAdjustBar.style.cssText = "cursor:e-resize;position:absolute;top:0px;left:0px;width:" + self.adjustBlockWidth * 2 +"px;height:"+(self.titleAreaHeight)
										+ "px;background-color:#cccccc;display:none;filter: Alpha(Opacity=50);";
		else
			oAdjustBar.style.cssText = "cursor:e-resize;position:absolute;top:0px;left:0px;width:" + self.adjustBlockWidth * 2 +"px;height:"+(self.titleAreaHeight)
										+ "px;background-color:#cccccc;display:none;-moz-opacity: .5;";
			
		
		oAdjustBar.onmouseout = oAdjustBar_mouseout;
		oAdjustBar.onmousedown = oAdjustBar_mousedown;
		oAdjustBar.onmousemove = oAdjustBar_mousemove;		
		oAdjustBar.onmouseup = oAdjustBar_mouseup;
	}
	
	//***************************************************************
	var splitBar_mouseover = function(){
			evt = arguments[0] || window.event;
			var obj=evt.target || evt.srcElement;
			orgPos.getAbsPos(this);
			with(oAdjustBar.style){
				top = orgPos.y + 2 +  "px";
				left = orgPos.x - self.adjustBlockWidth * 2 / 3 - oTitleArea.scrollLeft + "px";
				display = "";
			}
			adjustIndex = obj.index;//////////////////////////
			oAdjustTitle = obj.parentNode;
			
			obj = null;		
	}
	//***************************************************************
	
	var createSplitBar = function(pIndex){
		var tBar = htmlObject("DIV");
		tBar.style.cssText = "float:right;width:" + self.splitBarWidth + "px;height:" + (self.titleAreaHeight) 
									+ "px;background-image: url(" + PIC_SPLITBAR 
									+ ");background-position: right center;background-repeat: no-repeat;";
		
		tBar.index = pIndex;////////////////////////////////////////
		
		tBar.onmouseover = splitBar_mouseover;
		
		return tBar;
	}
	
	//***************************************************************
	var titleItem_mouseover = function(){
		this.className = "oTitleItemActive";		
	}
	var titleItem_mouseout = function(){
		this.className = "oTitleItem";
	}
	//***************************************************************
	
	var createField = function(pFieldLabel,pFieldWidth,pIndex){
		var tTitleItem = htmlObject("DIV");
		tTitleItem.className = "oTitleItem";
		oTitleAreaInner.appendChild(tTitleItem);
		
		pFieldWidth += (isIE ? adjustWidth : 0);
		/*--------------------------------
		至关重要，如果不这样做的话IE下的标题的位置和FireFox的标题位置会不一致。
		---------------------------------*/
		
		tTitleItem.style.cssText += "text-indent:2px;float:left;width:" + (pFieldWidth) 
										  + "px;height:" 
										  + self.titleAreaHeight 
										  + "px;line-height:"
										  + self.titleAreaHeight
										  + "px;overflow:hidden;cursor:default;";
		
		var tLabel = createFieldLabel(pFieldLabel,pFieldWidth,pIndex);
		tTitleItem.appendChild(tLabel);
		
		tTitleItem.label = tLabel;
		tTitleItem.appendChild(createSplitBar(pIndex));
		createAdjustBar();
		
		
		tTitleItem.onmouseover = titleItem_mouseover;
		tTitleItem.onmouseout = titleItem_mouseout;
		
		tTitleItem = null;
		tLabel = null;
	}
	
	//***************************************************************
	
	this.bindTitle = function(pFields){
		oTitleArea = htmlObject("DIV");
		oTitleAreaTd.appendChild(oTitleArea);		
		
		oTitleArea.style.cssText += "overflow:hidden;width:" + (self.width - self.rowCountAreaWidth - 4) 
												+ "px;height:" 
												+ self.titleAreaHeight + "px;";
		oTitleAreaInner = htmlObject("DIV");
		oTitleArea.appendChild(oTitleAreaInner);
		
		var tField;
		var tCol = null;
		for(var i =0;tCol = oColGroup.childNodes[i];i++){
			tField = pFields[i] || [self.fieldDefaultLabel,self.fieldDefaultWidth,self.fieldDefaultAlign];
			realWidth += getFieldWidth(tField);
			
			oColGroup.childNodes[i].className = getFieldClassName(tField);
			oColGroup.childNodes[i].style.cssText += "width:" + getFieldWidth(tField) + "px;text-align:" + getFieldAlign(tField);			

			createField(getFieldLabel(tField),getFieldWidth(tField),i);	
		}
		
		tField = null;
		tCol = null;
				
		if(!isIE)
			oDataTable.width = realWidth;
		
		oTitleAreaInner.style.width = (realWidth + 200) + "px";
		
		setColGroup();
		bindRowNum();
	}
	
	//***************************************************************
	var oRowNumTable_click = function(){
			evt = arguments[0] || window.event;
			var obj = (evt.target || evt.srcElement).parentNode;
			if(obj.tagName == "TR"){
				obj = oDataTable.rows[obj.rowIndex];			
				if(obj.className != "oActiveRow"){
					if(self.oActiveRow != null) 
						self.oActiveRow.className = self.oActiveRow.baseClassName;
					obj.baseClassName = obj.className;
					obj.className = "oActiveRow";
					self.oActiveRow = obj;
				}else{
					obj.className = obj.baseClassName;
					self.oActiveRow = null;
				}
			}
			obj = null;		
	}
	//***************************************************************
	
	var bindRowNum = function(){
		oRowNumArea = htmlObject("DIV");
		oRowNumAreaTd.appendChild(oRowNumArea)		
		oRowNumArea.style.cssText = "overflow:hidden;width:" + self.rowCountAreaWidth + "px;height:"
																					  + oRowNumAreaTd.style.height;
		oRowNumTable = htmlObject("TABLE");
		oRowNumArea.appendChild(oRowNumTable);
		
		oRowNumTable.border = 0;
		oRowNumTable.borderColor= self.lineColor;
		oRowNumTable.width = self.rowCountAreaWidth + 3 + "px";
//		oRowNumTable.height = oDataTable.clientHeight;
		oRowNumTable.className = "oRowNumTable";
		with(oRowNumTable.style){
			borderCollapse = "collapse";
			tableLayout="fixed";
		}

		var tTBody = htmlObject("TBODY");
		oRowNumTable.appendChild(tTBody);
		
		var adjustHeight = isIE ? -3 : 2;
		
		var tTr,tTd,t;
		/*
		var tTrc = htmlObject("TR");
		var tTdc = htmlObject("TD");
		for(var i=0;t=oDataTable.rows[i];i++){
			tTr = tTrc.cloneNode(true);
			tTBody.appendChild(tTr);
			tTd = tTdc.cloneNode(true);
			tTr.appendChild(tTd);
			tTd.height = t.clientHeight + adjustHeight;
			tTd.innerHTML = i + 1;
		}
		经测试，这个要比下面的慢。
		*/
		//i=0則序號多一行
		for(var i = 0;t = oDataTable.rows[i];i++){
			tTr = htmlObject("TR");
			tTBody.appendChild(tTr);
			
			tTd = htmlObject("TD");
			tTr.appendChild(tTd);
//			tTd.height = t.clientHeight + adjustHeight;
			tTd.innerHTML = i + 1;
		}
		tTBody.appendChild(tTr);	
		tTr = null;
		tTd = null;
		t = null;
		
		oRowNumTable.onclick = oRowNumTable_click;
		
		var tRow = null,tCell = null;
		for(var i=0;tRow = oRowNumTable.rows[i];i++){
			tCell = tRow.insertCell(0);
			tCell.innerHTML = i + 1 ;
			tCell.width = self.rowCountAreaWidth;
			tCell.className = "oRowNum";
			try{
//			oDataTable.rows[i].className = (i % 2 == 0 ? "rowStyle1" : "rowStyle2");
			}catch(e){
				//因为oRowNumTable比oDataTable多一行。
			}
		}
		tRow = null;
		tCell = null;
		
	clean();
	}
	
	
	//***************************************************************
	
	var reBindRowNum = function(){
		oRowNumArea.removeChild(oRowNumTable);
		oRowNumTable = htmlObject("TABLE");
		oRowNumArea.appendChild(oRowNumTable);
		
		oRowNumTable.border = 0;
		oRowNumTable.borderColor= self.lineColor;
		oRowNumTable.width = self.rowCountAreaWidth + 3 + "px";
//		oRowNumTable.height = oDataTable.clientHeight;
		oRowNumTable.className = "oRowNumTable";
		with(oRowNumTable.style){
			borderCollapse = "collapse";
			tableLayout="fixed";
		}

		var tTBody = htmlObject("TBODY");
		oRowNumTable.appendChild(tTBody);
		
		var adjustHeight = isIE ? -3 : 2;
		
		var tTr,tTd,t;
		for(var i = 0;t = oDataTable.rows[i];i++){
			tTr = htmlObject("TR");
			tTBody.appendChild(tTr);
			
			tTd = htmlObject("TD");
			tTr.appendChild(tTd);
//			tTd.height = t.clientHeight + adjustHeight;
			tTd.innerHTML = i + 1;
		}
		tTBody.appendChild(tTr);	
		tTr = null;
		tTd = null;
		t = null;
		
		oRowNumTable.onclick = oRowNumTable_click;
	}	
	
	var reBindRowNum2 = function(){
		
		reBindRowNum();
		
		var tRow = null,tCell = null;
		for(var i=0;tRow = oRowNumTable.rows[i];i++){
			tCell = tRow.insertCell(0);
			tCell.innerHTML = i + 1;
			tCell.width = self.rowCountAreaWidth;
			tCell.className = "oRowNum";
			try{
	//			oDataTable.rows[i].className = (i % 2 == 0 ? "rowStyle1" : "rowStyle2");
			}catch(e){
				//因为oRowNumTable比oDataTable多一行。
			}
		}
		tRow = null;
		tCell = null;		
	}
	
	//***************************************************************
	var setColGroup = function(){
		oDataTable.appendChild(oColGroup);	
		var tCol = htmlObject("COL");
		tCol.width = self.rowCountAreaWidth;
		tCol = null;
	}
	
	//***************************************************************
	
	var clean = function(){
		oFrameTable
		oTopLeftAreaTd
		oTitleAreaTd
		oRowNumAreaTd = null;
		oDataTableAreaTd = null;
		oTopLeftArea = null;
		oDataTableArea = null;
		oToolBtnsTable = null;
		oWaiting = null;			
	}
	
	//***************************************************************
	
	this.create = function(){
		if(!check())
			return false;
		createOutline();
	}
	
	this.runtimeHTML = function(){
		return oOutline.innerHTML;
	}
}