<?php
	include("../inc/Udata.inc");
	include("priority.php");
	include("PHPDefine.php");
	include("RSA.php");
	include("lib_regvalue.php");

	define("ULTRALOGPATH", ":14000");

	define("SQLACC", "ultra");
	define("SQLPXD", "g7t3l2o6U1r4la5");

	define("CLIENTMAX", 0);
	define("ALLTAB", 10);
	define("TREENUM", 10);
	define("AUTOSIGNOUT", 5);
	define("AUTOWARNING", 1);
	define("HOSTTIMEOUT", 300);
	define("MAXRECORD", 500);

	define("FUN_PLAY", 1);
	define("FUN_MONITOR", 2);
	define("FUN_STATIS", 4);

	define("SQLODBCSTR", "SQLNCLI11");
	
	define("DEFAULT_STR", "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");

	$d = glob("../config/store_acc"."_*");
	foreach ($d as $filename) {
		if(file_exists($filename)) {
			if(filemtime($filename) <= strtotime("-1 hour")) {
				unlink($filename);
			}
		}
	}

	
	$account = DEFAULT_STR;
	$password = DEFAULT_STR;

	define("ENCRYPT128", "D82C8D1619AD8176D665453CFB2E55F0F457C545A9DED88F18ECEE47145A72C0C0C7C76D30BD3DCAEFC96F40275BDC0A9F61408E3AFB633E50CDF1B20DE6F466");

	//************************************************************************************************************************
	function getDbAccount() {
	  return getDbLoginData("account", "DBStageAcc");
	}

	function getDbPassword() {
	  return getDbLoginData("password", "DBStagePwd");
	}

	function getDbLoginData($variable, $key){
	  global $$variable;
	  if (($$variable == "") || ($$variable == DEFAULT_STR)) {
	    $ini = readregini("System", "Database", $key);
	    $$variable = private_decrypt($ini);
	    $$variable = encodingConvent($$variable);
	    // $$variable = iconv('UTF-8', 'BIG-5', $$variable);
	  }
	  return $$variable;
	}

	//************************************************************************************************************************
	function getIVRDbAccount() {
	  return getIVRDbLoginData("ivraccount", "DBStageAcc");
	}

	function getIVRDbPassword() {
	  return getIVRDbLoginData("ivrpassword", "DBStagePwd");
	}

	function getIVRDbLoginData($variable, $key){
	  global $$variable;
	  if ($$variable == "") {
	    $ini = readregini("Misc", "IVRInfo", $key);
	    $$variable = private_decrypt($ini);
	    $$variable = encodingConvent($$variable);
	    // $$variable = iconv('UTF-8', 'BIG-5', $$variable);
	  }
	  return $$variable;
	}

	// �^�� URL ���e
	function remote_url($url, $ssl = false) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if ($ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		}
		$c = curl_exec($ch);
		curl_close($ch);
		return $c;
	}

	function httpPost($url, $data, $ssl = false) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if ($ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		}
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
	}

	// True �^�� $b, False �^�� $c
	function iif($a, $b, $c) {
		if ($a == true) {
			return $b;
		}
		else {
			return $c;
		}
	}

	// �R���ؿ����ɮ�
	function dir_delete($file) {
		if(file_exists($file)) {
			if(is_dir($file)) {
			    $handle = opendir($file);
			    while(($filename = readdir($handle)) <> false) {
					if($filename <> "." && $filename <> "..") {
						dir_delete($file."/".$filename);
					}
			    }
			    closedir($handle);
			    rmdir($file);
			}
			else {
			    unlink($file);
			}
		}
	}

	// ��W�ؿ����ɮ�
	function dir_rename($file, $file2) {
		if(file_exists($file)) {
			if(!is_dir($file)) {
			    rename($file, $file2);
			}
		}
	}

	// Ū���@�����U��
	function readreg($s_profile, $s_keyname, $s_valuename) {
		/*//$cm = new COM("RegValue.RegValueCtl");
		$cm = new_ocx("RegValue.RegValueCtl");
		$str = $cm->ReadRegString($s_profile, $s_keyname, $s_valuename);
		return $str;*/
	}

	// Ū���w�˥ؿ����U��
	function readpathreg() {
		/*//$cm = new COM("RegValue.RegValueCtl") or die("Registry !! Create Registry Fail!!!");
		$cm = new_ocx("RegValue.RegValueCtl");
		$str = $cm->GetRegPath_C();*/
		$str = readregini("System", "Install", "Path");
		return $str;
	}

	// �}�C�Ƨǥ� ����
	function cmp($a,$b) {
		if ($a == $b) return 0;
		return ($a < $b) ? -1 : 1;
	}

	// �}�C�Ƨǥ� �ϧ�
	function rcmp($a,$b) {
		if ($a == $b) return 0;
		return ($a > $b) ? -1 : 1;
	}

	// ���W��� XXXX/XX/XX
	function standarddate($d) {
		$str = substr($d,0,4);
		if (is_numeric(substr($d,6,1))) {
			$str .= "/".substr($d,5,2);
			if (is_numeric(substr($d,9,1))) {
				$str .= "/".substr($d,8,2);
			}
			else {
				$str .= "/"."0".substr($d,7,1);
			}
		}
		else {
			$str .= "/"."0".substr($d,5,1);
			if (is_numeric(substr($d,8,1))) {
				$str .= "/".substr($d,7,2);
			}
			else {
				$str .= "/"."0".substr($d,7,1);
			}
		}
		return $str;
	}

	// ���ܮɤ��� HHHH:MM:SS
	function sec2hms($s) {

		$d = fmod($s, 3600);
		$hh = ($s - $d) / 3600;
		$ss = fmod($d, 60);
		$mm = ($d - $ss) / 60;
		//return (($hh < 100) ? sprintf('%02d', $hh) : $hh).':'.sprintf('%02d', $mm).':'.sprintf('%02d', $ss);

		$str = 0;
		if (($hh == 0) && ($mm == 0)) {
			$str = $ss."";
		}
		else if ($hh == 0) {
			$str = $mm.":".sprintf('%02d', $ss)."";
		}
		else {
			$str = $hh.':'.sprintf('%02d', $mm).":".sprintf('%02d', $ss)."";
		}
		return $str;
	}

	//function random_int($p1, $p2) {
	//
	//	return rand(16, 128);
	//}

	//function random_bytes($p1) {
	//
	//	return "0x" & dechex(rand(1111, 9999));
	//}

	function json_encode_this($p1, $p2) {

		return json_encode($p1);
	}

	function uchr($codes) {
		if (is_scalar($codes)) $codes= func_get_args();
		$str= '';
		foreach ($codes as $code) $str.= html_entity_decode('&#'.$code.';',ENT_NOQUOTES,'UTF-8');
		return $str;
	}

	function utf8ToUnicode($str, $order="little") {

		$ucs2string = "";
		$n = strlen($str);

		for ($i=0; $i<$n; $i++) {
			$v = $str[$i];
			$ord = ord($v);
			if ($ord <= 0x7F) {	//0xxxxxxx
				if ($order=="little") {
					$ucs2string .= $v.chr(0);
				}
				else {
					$ucs2string .= chr(0).$v;
				}
			}
			elseif ($ord < 0xE0 && ord($str[$i+1]) > 0x80) {	//110xxxxx 10xxxxxx
				$a = (ord($str[$i]) & 0x3F) << 6;
				$b = ord($str[$i+1]) & 0x3F;
				$ucsCode = dechex($a+$b);		//echot($ucsCode);
				$h = intval(substr($ucsCode, 0, 2), 16);
				$l = intval(substr($ucsCode, 2, 2), 16);
				if ($order=="little") {
					$ucs2string .= chr($l).chr($h);
				}
				else {
					$ucs2string .= chr($h).chr($l);
				}
				$i++;
			}
			elseif ($ord < 0xF0  && ord($str[$i+1]) > 0x80 && ord($str[$i+2]) > 0x80) {	//1110xxxx 10xxxxxx 10xxxxxx
				$a = (ord($str[$i]) & 0x1F) << 12;
				$b = (ord($str[$i+1]) & 0x3F ) << 6;
				$c =  ord($str[$i+2]) & 0x3F;
				$ucsCode = dechex($a+$b+$c);	//echot($ucsCode);
				$h = intval(substr($ucsCode, 0, 2), 16);
				$l = intval(substr($ucsCode, 2, 2), 16);
				if ($order=="little") {
					$ucs2string .= chr($l).chr($h);
					//print_r(htmlspecialchars($l." ".$h." - "));
				}
				else {
					$ucs2string .= chr($h).chr($l);
				}
				$i +=2;
			}
		}
		//print_r(htmlspecialchars(" - "));
		return $ucs2string;
	}

	//�[�K�r��
	function ultra_encrypt($s) {

		//if (!function_exists("random_int")) {
		//	$rnd = rand(16, 128);
		//}
		//else {
			$rnd = random_int(16, 128);
		//}
		$ctr = $rnd;
		$tmp = dechex($rnd);
		$sl = strlen($s);
		for ($i = 0; $i < $sl; $i++) {
			$ctr = (($ctr == strlen(ENCRYPT128)) ? 0 : $ctr);
			$tmp .= substr(ENCRYPT128, $ctr++, 1);
			$ctr = (($ctr == strlen(ENCRYPT128)) ? 0 : $ctr);
			$tmp .= ($s[$i] ^ substr(ENCRYPT128, $ctr++, 1));
		}
		return urlencode(base64_encode($tmp));
	}

	//�ѱK�r��
	function ultra_decrypt($s) {

		$s = base64_decode(urldecode($s));
		$ctr = hexdec(substr($s, 0, 2));
		$ctr = (($ctr == strlen(ENCRYPT128)) ? 0 : $ctr);
		$s = substr($s, 2);
		$tmp = "";
		$sl = strlen($s);
		for ($i = 0; $i < $sl; $i++) {
			$ctr = ((++$ctr == strlen(ENCRYPT128)) ? 0 : $ctr);
			$tmp .= $s[++$i] ^ substr(ENCRYPT128, $ctr, 1);
			$ctr = ((++$ctr == strlen(ENCRYPT128)) ? 0 : $ctr);
		}
		return $tmp;
	}

	//************************************************************************************************************************

	// �O�d �n�J��$uid �b��$Account �K�X$Password �\��$Func �}�l�ɶ�$StartDate �����ɶ�$EndDate �D��$Server �j��$Channel ����$Extension �s��$AgentID �H�W$AgentName ����$Department �s��$ACD
	function store_acc($uid, $Account, $Password, $Func, $StartDate, $EndDate, $Server, $Channel, $Extension, $AgentID, $AgentName, $Department, $ACD, $NowServer, $NowSel, $GroupName, $Unit) {
		if ($uid == "") {
			//$uid = md5(microtime()."_".rand(0, 4999)."_".rand(5000, 9999));
			//$uid = md5(microtime()."_".rand(0, 4999)."_".rand(5000, 9999)).md5(microtime()."_".rand(5000, 9999)."_".rand(0, 4999));
			$uid = hash("sha256", microtime()."_".bin2hex(random_bytes(3))."_".bin2hex(random_bytes(3))).hash("sha256", microtime()."_".bin2hex(random_bytes(3))."_".bin2hex(random_bytes(3)));
			$new = 1;
		}
		$xuid = hash("sha256", $uid);
		//if(file_exists("../config/store_acc")) {
		if(file_exists("../config/store_acc"."_".$xuid)) {
			//$s = implode("", @file("../config/store_acc"));
			$s = implode("", @file("../config/store_acc"."_".$xuid));
			if ($s != "") {
				//$accdata = unserialize(ultra_decrypt($s));
				$accdata = new Udata(); $accdata->d = (($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				foreach($accdata->d as $index => $value) {
					$a = $accdata->value($index);
					if ($a["Account"] == $Account) {
						$accdata->delete($index);
						if ($new == 1) {
							delete_playrestorefilename($a["Account"]);
							dir_delete("../config/".$a["Account"]."/Temp");
						}
						break;
					}
				}
			}
			else {
				$accdata = new Udata;
			}
		}
		else {
			$accdata = new Udata;
		}
		$accdata->value($uid, array("Account" => "$Account", "Password" => "$Password", "Func" => "$Func", "StartDate" => "$StartDate",
			"EndDate" => "$EndDate", "Server" => "$Server", "Channel" => "$Channel", "Extension" => "$Extension", "AgentID" => iconv('Big5', 'UTF-8', "$AgentID"), "AgentName" => iconv('Big5', 'UTF-8', "$AgentName"), "Department" => iconv('Big5', 'UTF-8', "$Department"), "ACD" => iconv('Big5', 'UTF-8', "$ACD"),
			"NowServer" => "$NowServer", "NowSel" => "$NowSel", "GroupName" => "$GroupName", "Unit" => "$Unit"));
		//$s = serialize($accdata);
		$s = json_encode_this($accdata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/store_acc", "w");
		//$fp = fopen("../config/store_acc"."_".$xuid, "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/store_acc"."_".$xuid, ultra_encrypt($s));
		store_statismap($uid, $Account);
		return $uid;
	}

	// �O�d �n�J��$uid �b����$a
	function store_acc2($uid, $a) {

		$xuid = hash("sha256", $uid);
		//if(file_exists("../config/store_acc")) {
		if(file_exists("../config/store_acc"."_".$xuid)) {
			//$s = implode("", @file("../config/store_acc"));
			$s = implode("", @file("../config/store_acc"."_".$xuid));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$accdata->value($uid, $a);
			//$s = serialize($accdata);
			$s = json_encode_this($accdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/store_acc", "w");
			//$fp = fopen("../config/store_acc"."_".$xuid, "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/store_acc"."_".$xuid, ultra_encrypt($s));
		}
	}

	// �O�d �n�J��$uid �b��$Account �K�X$Password �n�J�ɶ�$LoginTime
	function store_accextra($uid, $Account, $Password, $LoginTime) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(file_exists("../config/".$Account."/store_accextra")) {
			$s = implode("", @file("../config/".$Account."/store_accextra"));
			//$accextradata = unserialize(ultra_decrypt($s));
			$accextradata = new Udata(); $accextradata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$accextradata = new Udata;
		}
		$la = $accextradata->value("LoginTime");
		if($la) {
			if (count($la) > 10 ) {array_shift($la);}
			array_push($la, $LoginTime);
		}
		else {
			$la = array();
			array_push($la, $LoginTime);
		}
		$accextradata->value("LoginTime", $la);
		//$s = serialize($accextradata);
		$s = json_encode_this($accextradata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$Account."/store_accextra", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/store_accextra", ultra_encrypt($s));
	}

	// ���� �n�J��$uid
	function carry_acc($uid) {

		$xuid = hash("sha256", $uid);
		//if(file_exists("../config/store_acc")) {
		if(file_exists("../config/store_acc"."_".$xuid)) {
			//$s = implode("", @file("../config/store_acc"));
			$s = implode("", @file("../config/store_acc"."_".$xuid));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $accdata->value($uid);
		}
	}

	// �ˬd �n�J��$uid
	function check_acc($uid) {

		$xuid = hash("sha256", $uid);
		//if(file_exists("../config/store_acc")) {
		if(file_exists("../config/store_acc"."_".$xuid)) {
			//$s = implode("", @file("../config/store_acc"));
			$s = implode("", @file("../config/store_acc"."_".$xuid));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $accdata->value($uid);
		}
	}

	// ���� �n�J��$uid �b��$Account �K�X$Password �n�J�ɶ�$LoginTime
	function carry_accextra($Account) {

		if(file_exists("../config/".$Account."/store_accextra")) {
			$s = implode("", @file("../config/".$Account."/store_accextra"));
			//$accextradata = unserialize(ultra_decrypt($s));
			$accextradata = new Udata(); $accextradata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$la = $accextradata->value("LoginTime");
			if($la) {
				$c = count($la);
				if ($c > 1) {
					return $la[($c-2)].",".$la[($c-1)];
				}
				else {
					return $la[0].",".$la[0];
				}
			}
		}
	}

	// �ˬd �b��$Account
	function check_acc2($Account) {

		if(file_exists("../config/store_acc")) {
			$s = implode("", @file("../config/store_acc"));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach($accdata->d as $index => $value) {
				$a = $accdata->value($index);
				if ($a["Account"] == $Account) {
					return 1;
				}
			}
		}
		return 0;
	}

	// �ˬd �b���ƶq
	function count_acc() {

		if(file_exists("../config/store_acc")) {
			$s = implode("", @file("../config/store_acc"));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return count($accdata->d);
		}

		$c = 0;
		$d = glob("../config/store_acc"."_*");
		foreach ($d as $filename) {
			if(file_exists($filename)) {
				if(filemtime($filename) <= strtotime("-1 day")) {
					unlink($filename);
				}
				else {
					$c++;
				}
			}
		}
		return $c;
	}

	// �R�� �n�J��$uid
	function delete_acc($uid) {

		if(file_exists("../config/store_acc")) {
			$s = implode("", @file("../config/store_acc"));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$accdata->delete($uid);
			//$s = serialize($accdata);
			$s = json_encode_this($accdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/store_acc", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/store_acc", ultra_encrypt($s));
		}

		$xuid = hash("sha256", $uid);
		if(file_exists("../config/store_acc"."_".$xuid)) {
			unlink("../config/store_acc"."_".$xuid);
		}
	}

	// �R�� �L�ɵn�J��$uid
	function delete_acc2($t) {

		if(file_exists("../config/store_acc")) {
			$s = implode("", @file("../config/store_acc"));
			//$accdata = unserialize(ultra_decrypt($s));
			$accdata = new Udata(); $accdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			if ((CLIENTMAX !== 0) and (count($accdata->d) >= CLIENTMAX)) {
				$u = ""; $ut = 0;
				foreach($accdata->d as $index => $value) {
					list($usec, $sec) = explode(" ", $index); $nt = (float)$usec + (float)$sec;
					if ((microtime(true) - $nt) > $t) {
						$u = $index; $ut = (($ut == 0) ? $nt : min($ut, $nt));
					}
				}
				if ($u !== "") {
					$accdata->delete($u);
					//$s = serialize($accdata);
					$s = json_encode_this($accdata, JSON_UNESCAPED_UNICODE);
					//$fp = fopen("../config/store_acc", "w");
					//fputs($fp, ultra_encrypt($s));
					//fclose($fp);
					file_put_contents("../config/store_acc", ultra_encrypt($s));
				}
			}
		}

		$d = glob("../config/store_acc"."_*");
		foreach ($d as $filename) {
			if(file_exists($filename)) {
				if(filemtime($filename) <= strtotime("-1 hour")) {
					unlink($filename);
				}
			}
		}
	}

	//************************************************************************************************************************

	// �O�d �g�J��$uid ��s�ɶ�$ut
	function store_svrwrite($uid, $ut) {

		if(file_exists("../config/store_svrwrite")) {
			$s = implode("", @file("../config/store_svrwrite"));
			//$svrwrite = unserialize(ultra_decrypt($s));
			$svrwrite = new Udata(); $svrwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$svrwrite = new Udata;
		}
		$wt = time();
		$svrwrite->value("write", array("uid" => "$uid", "ut" => "$ut", "wt" => "$wt"));
		//$s = serialize($svrwrite);
		$s = json_encode_this($svrwrite, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/store_svrwrite", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/store_svrwrite", ultra_encrypt($s));
	}

	// ����
	function carry_svrwrite() {

		if(file_exists("../config/store_svrwrite")) {
			$s = implode("", @file("../config/store_svrwrite"));
			//$svrwrite = unserialize(ultra_decrypt($s));
			$svrwrite = new Udata(); $svrwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $svrwrite->value("write");
		}
		else {
			return "not";
		}
	}

	// �ˬd
	function check_svrwrite() {

		if(file_exists("../config/store_svrwrite")) {
			$s = implode("", @file("../config/store_svrwrite"));
			//$svrwrite = unserialize(ultra_decrypt($s));
			$svrwrite = new Udata(); $svrwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $svrwrite->value("write");
		}
		else {
			return "not";
		}
	}

	// �R��
	function delete_svrwrite() {

		if(file_exists("../config/store_svrwrite")) {
			$s = implode("", @file("../config/store_svrwrite"));
			//$svrwrite = unserialize(ultra_decrypt($s));
			$svrwrite = new Udata(); $svrwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$svrwrite->delete("write");
			//$s = serialize($svrwrite);
			$s = json_encode_this($svrwrite, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/store_svrwrite", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/store_svrwrite", ultra_encrypt($s));
		}
	}

	//************************************************************************************************************************

	// �O�d �D��$Server �g�J��$uid ��s�ɶ�$ut
	function store_serverwrite($Server, $uid, $ut) {

		if(file_exists("../config/store_serverwrite")) {
			$s = implode("", @file("../config/store_serverwrite"));
			//$serverwrite = unserialize(ultra_decrypt($s));
			$serverwrite = new Udata(); $serverwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$serverwrite = new Udata;
		}
		$wt = time();
		$serverwrite->value(strtoupper($Server), array("uid" => "$uid", "ut" => "$ut", "wt" => "$wt"));
		//$s = serialize($serverwrite);
		$s = json_encode_this($serverwrite, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/store_serverwrite", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/store_serverwrite", ultra_encrypt($s));
	}

	// ���� �D��$Server
	function carry_serverwrite($Server) {

		if(file_exists("../config/store_serverwrite")) {
			$s = implode("", @file("../config/store_serverwrite"));
			//$serverwrite = unserialize(ultra_decrypt($s));
			$serverwrite = new Udata(); $serverwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $serverwrite->value(strtoupper($Server));
		}
		else {
			return "not";
		}
	}

	// �ˬd �D��$Server
	function check_serverwrite($Server) {

		if(file_exists("../config/store_serverwrite")) {
			$s = implode("", @file("../config/store_serverwrite"));
			//$serverwrite = unserialize(ultra_decrypt($s));
			$serverwrite = new Udata(); $serverwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $serverwrite->value(strtoupper($Server));
		}
		else {
			return "not";
		}
	}

	// �O�d �D��$Server �g�J��$uid ��s�ɶ�$ut
	function store_serverwrite2($Server, $uid, $ut) {

		if(file_exists("../config/store_serverwrite")) {
			$s = implode("", @file("../config/store_serverwrite"));
			//$serverwrite = unserialize(ultra_decrypt($s));
			$serverwrite = new Udata(); $serverwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$serverwrite = new Udata;
		}
		$wt = time();
		$CSA = explode(",",$Server);
		foreach ($CSA as $value) {
			$serverwrite->value(strtoupper($value), array("uid" => "!ALL!"."$uid", "ut" => "$ut", "wt" => "$wt"));
		}
		//$s = serialize($serverwrite);
		$s = json_encode_this($serverwrite, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/store_serverwrite", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/store_serverwrite", ultra_encrypt($s));
	}

	// �ˬd �D��$Server �g�J��$uid
	function check_serverwrite2($Server, $uid, $wt) {

		$str = "";
		$cut = "";
		$CSA = explode(",",$Server);
		if(file_exists("../config/store_serverwrite")) {
			$s = implode("", @file("../config/store_serverwrite"));
			//$serverwrite = unserialize(ultra_decrypt($s));
			$serverwrite = new Udata(); $serverwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach ($CSA as $value) {
				if (($value <> "") && ($value <> "<ALL>")) {
					$c = $serverwrite->value(strtoupper($value));
					if (($c["uid"] == "") || (substr($c["uid"], 0, 5) <> "!ALL!") || (substr($c["uid"], 5) == $uid) || ($c["wt"] < $wt)) {
						$str .= iif(($str == ""), $value, ",".$value);
						if (date("Y-m-d H:i:s", strtotime($c["ut"])) < date("Y-m-d H:i:s", strtotime($cut))) $cut = $c["ut"];
					}
				}
			}
		}
		else {
			foreach ($CSA as $value) {
				if (($value <> "") && ($value <> "<ALL>")) {
					$str .= iif(($str == ""), $value, ",".$value);
				}
			}
		}
		return $str."&".$cut;
	}

	// �R�� �D��$Server
	function delete_serverwrite($Server) {

		if(file_exists("../config/store_serverwrite")) {
			$s = implode("", @file("../config/store_serverwrite"));
			//$serverwrite = unserialize(ultra_decrypt($s));
			$serverwrite = new Udata(); $serverwrite->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$serverwrite->delete(strtoupper($Server));
			//$s = serialize($serverwrite);
			$s = json_encode_this($serverwrite, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/store_serverwrite", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/store_serverwrite", ultra_encrypt($s));
		}
	}

	//************************************************************************************************************************

	// �O�d �D�����A$ServerArr
	function store_svrarr($SA) {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$svrdata = new Udata;
		}
		$c = count($SA);
		for ($i=1; $i<=$c; $i++) {
			$svrdata->value(strtoupper($SA[$i]["Server"]), $SA[$i]);
		}
		//$s = serialize($svrdata);
		$s = json_encode_this($svrdata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/store_svrdata", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/store_svrdata", ultra_encrypt($s));
	}

	// ���� �D��$ServerStr
	function carry_svrarr($SStr) {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$sa = explode(",",$SStr);
			foreach ($sa as $value) {
				$d = $svrdata->value(strtoupper($value));
				if ($d) {
					$str .= "&".$value.",".$d["TotalChannel"].",".
						$d["HDRemainSpace"].",".$d["HDRemainPercent"].",".$d["HDRemainState"].",".
						$d["BackupDeviceNum"].",".
						$d["BackupRemainSpaceA"].",".$d["BackupRemainPercentA"].",".$d["BackupRemainStateA"].",".
						$d["BackupRemainSpaceB"].",".$d["BackupRemainPercentB"].",".$d["BackupRemainStateB"].",".
						$d["RecCapacity"].",".$d["intReserveA"].",".$d["CurrentState"].",".
						$d["HostState"].",".
						$d["NewError"].",".
						$d["BackupNow"].",".$d["BackupPercent"].",".
						$d["DeleteNow"].",".$d["DeletePercent"];
				}
				else {
					$str .= "&".$value.","."0".",".
						"0".","."0".","."1".",".
						"0".",".
						"0".","."0".","."1".",".
						"0".","."0".","."1".",".
						"0".","."0".","."0".",".
						"0".",".
						"0".",".
						"0".","."0".",".
						"0".","."0";
				}
			}
			return $str;
		}
	}

	// ���� �D��$ServerStr
	function carry_svrarrall() {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach($svrdata->d as $index => $value) {
				$d = $svrdata->value($index);
				$str .= iif(($str == ""), "", "@!!@").$index."@!@".$d["TotalChannel"]."@!@".
					$d["HDRemainSpace"]."@!@".$d["HDRemainPercent"]."@!@".$d["HDRemainState"]."@!@".
					$d["BackupDeviceNum"]."@!@".
					$d["BackupRemainSpaceA"]."@!@".$d["BackupRemainPercentA"]."@!@".$d["BackupRemainStateA"]."@!@".
					$d["BackupRemainSpaceB"]."@!@".$d["BackupRemainPercentB"]."@!@".$d["BackupRemainStateB"]."@!@".
					$d["RecCapacity"]."@!@".$d["intReserveA"]."@!@".$d["CurrentState"]."@!@".
					$d["HostState"]."@!@".
					$d["NewError"]."@!@".
					$d["BackupNow"]."@!@".$d["BackupPercent"]."@!@".
					$d["DeleteNow"]."@!@".$d["DeletePercent"];
			}
			return $str;
		}
	}

	// �O�d �D��$Server
	function store_svr($Server, $TotalChannel, $HDRemainSpace, $HDRemainPercent, $HDRemainState, $BackupDeviceNum, $BackupRemainSpaceA, $BackupRemainPercentA, $BackupRemainStateA, $BackupRemainSpaceB, $BackupRemainPercentB, $BackupRemainStateB, $RecCapacity, $IntReserveA, $CurrentState, $HostState, $NewError, $BackupNow, $BackupPercent, $DeleteNow, $DeletePercent) {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$svrdata = new Udata;
		}
		$svrdata->value(strtoupper($Server), array("TotalChannel" => "$TotalChannel",
			"HDRemainSpace" => "$HDRemainSpace", "HDRemainPercent" => "$HDRemainPercent", "HDRemainState" => "$HDRemainState",
			"BackupDeviceNum" => "$BackupDeviceNum",
			"BackupRemainSpaceA" => "$BackupRemainSpaceA", "BackupRemainPercentA" => "$BackupRemainPercentA", "BackupRemainStateA" => "$BackupRemainStateA",
			"BackupRemainSpaceB" => "$BackupRemainSpaceB", "BackupRemainPercentB" => "$BackupRemainPercentB", "BackupRemainStateB" => "$BackupRemainStateB",
			"RecCapacity" => "$RecCapacity", "intReserveA" => "$intReserveA", "CurrentState" => "$CurrentState",
			"HostState" => "$HostState",
			"NewError" => "$NewError",
			"BackupNow" => "$BackupNow", "BackupPercent" => "$BackupPercent",
			"DeleteNow" => "$DeleteNow", "DeletePercent" => "$DeletePercent"));
		//$s = serialize($svrdata);
		$s = json_encode_this($svrdata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/store_svrdata", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/store_svrdata", ultra_encrypt($s));
	}

	// ���� �D��$Server
	function carry_svr($var) {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $svrdata->value(strtoupper($var));
		}
	}

	// �ˬd �D��$Server
	function check_svr($var) {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $svrdata->value(strtoupper($var));
		}
	}

	// �R�� �D��$Server
	function delete_svr($var) {

		if(file_exists("../config/store_svrdata")) {
			$s = implode("", @file("../config/store_svrdata"));
			//$svrdata = unserialize(ultra_decrypt($s));
			$svrdata = new Udata(); $svrdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$svrdata->delete(strtoupper($var));
			//$s = serialize($svrdata);
			$s = json_encode_this($svrdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/store_svrdata", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/store_svrdata", ultra_encrypt($s));
		}
	}

	//************************************************************************************************************************

	// �O�d �D��$Server �j��$ChannelArr
	function store_serverarr($Server, $CA) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$serverdata = new Udata;
		}
		$c = count($CA);
		for ($i=1; $i<=$c; $i++) {
			$serverdata->value($CA[$i]["Channel"], $CA[$i]);
		}
		//$s = serialize($serverdata);
		$s = json_encode_this($serverdata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/server_".$Server, "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/server_".$Server, ultra_encrypt($s));
	}

	// ���� �D��$Server �b�����$Acc
	function carry_serverarr($Server, $Acc) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$str = "";
			$l = readregini("System" ,"Database" , "Language");
			if ($serverdata->d) {
				foreach($serverdata->d as $index => $value) {
					$sa = $serverdata->value($index);
					if (CheckSyntax2($l, $Acc, FUN_MONITOR, strtoupper($Server), strtoupper($sa["Channel"]), strtoupper($sa["Extension"]), strtoupper($sa["AgentID"]), strtoupper($sa["AgentName"]), strtoupper($sa["Department"]), strtoupper($sa["ACD"])) == true) {
					//if (CheckSyntax($l, $Acc, FUN_MONITOR, strtoupper($sa["Channel"]), strtoupper($sa["Extension"]), strtoupper($sa["AgentID"]), strtoupper($sa["AgentName"]), strtoupper($sa["Department"]), strtoupper($sa["ACD"])) == true) {
						$str .= iif(($str == ""), "", "&").$sa["Channel"].",".$sa["FileName"].",".$sa["Extension"].",".$sa["AgentID"].",".$sa["AgentName"].",".$sa["Department"].",".$sa["ACD"].",".
							$sa["Number"].",".$sa["CallType"].",".$sa["State"].",".$sa["StateTime"].",".$sa["UpdatedTime"];
					}
				}
			}
			return addslashes($str);
		}
	}

	// ���� �D��$Server �j��$ChannelStr
	function carry_serverarr2($Server, $CStr) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$c = explode(",",$CStr);
			foreach ($c as $value) {
				$d = $serverdata->value($value);
				if ($d) {
					$str .= "&".$value.",".$d["FileName"].",".$d["Extension"].",".$d["AgentID"].",".$d["AgentName"].",".$d["Department"].",".$d["ACD"].",".
						$d["Number"].",".$d["CallType"].",".$d["State"].",".$d["StateTime"].",".$d["UpdatedTime"];
				}
			}
			return $str;
		}
	}

	// �O�d �D��$Server �j��$ChannelArr
	function store_serverarracc($Server, $CA) {

		$CSA = explode(",",$Server);
		foreach ($CSA as $value) {
			if (($value <> "") && ($value <> "<ALL>")) {
				if(file_exists("../config/server_".$value)) {
					$s = implode("", @file("../config/server_".$value));
					//$serverdata = unserialize(ultra_decrypt($s));
					$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				}
				else {
					$serverdata = new Udata;
				}
				$c = count($CA);
				for ($i=1; $i<=$c; $i++) {
					if ($value == $CA[$i]["Server"]) {
						$serverdata->value($CA[$i]["Channel"], $CA[$i]);
					}
				}
				//$s = serialize($serverdata);
				$s = json_encode_this($serverdata, JSON_UNESCAPED_UNICODE);
				//$fp = fopen("../config/server_".$value, "w");
				//fputs($fp, ultra_encrypt($s));
				//fclose($fp);
				file_put_contents("../config/server_".$value, ultra_encrypt($s));
			}
		}
	}

	// ���� �D��$Server �b�����$Acc
	function carry_serverarracc($Server, $Acc) {

		$str = "";
		$CSA = explode(",",$Server);
		foreach ($CSA as $value) {
			if (($value <> "") && ($value <> "<ALL>")) {
				if(file_exists("../config/server_".$value)) {
					$s = implode("", @file("../config/server_".$value));
					//$serverdata = unserialize(ultra_decrypt($s));
					$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
					$l = readregini("System" ,"Database" , "Language");
					if ($serverdata->d) {
						foreach($serverdata->d as $index => $value2) {
							$sa = $serverdata->value($index);
							if (CheckSyntax2($l, $Acc, FUN_MONITOR, strtoupper($value), strtoupper($sa["Channel"]), strtoupper($sa["Extension"]), strtoupper($sa["AgentID"]), strtoupper($sa["AgentName"]), strtoupper($sa["Department"]), strtoupper($sa["ACD"])) == true) {
							//if (CheckSyntax($l, $Acc, FUN_MONITOR, strtoupper($sa["Channel"]), strtoupper($sa["Extension"]), strtoupper($sa["AgentID"]), strtoupper($sa["AgentName"]), strtoupper($sa["Department"]), strtoupper($sa["ACD"])) == true) {
								$str .= iif(($str == ""), "", "&").$value.",".$sa["Channel"].",".$sa["FileName"].",".$sa["Extension"].",".$sa["AgentID"].",".$sa["AgentName"].",".$sa["Department"].",".$sa["ACD"].",".
									$sa["Number"].",".$sa["CallType"].",".$sa["State"].",".$sa["StateTime"].",".$sa["UpdatedTime"];
							}
						}
					}
				}
			}
		}
		return addslashes($str);
	}

	// ���� �D��$Server
	function carry_serverarrall($Server) {

		$CSA = explode(",",$Server);
		foreach ($CSA as $value) {
			if (($value <> "") && ($value <> "<ALL>")) {
				if(file_exists("../config/server_".$value)) {
					$s = implode("", @file("../config/server_".$value));
					//$serverdata = unserialize(ultra_decrypt($s));
					$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
					foreach($serverdata->d as $index => $value2) {
						$sa = $serverdata->value($index);
						$str .= iif(($str == ""), "", "&").$value.",".$sa["Channel"].",".$sa["FileName"].",".$sa["Extension"].",".$sa["AgentID"].",".$sa["AgentName"].",".$sa["Department"].",".$sa["ACD"].",".
							$sa["Number"].",".$sa["CallType"].",".$sa["State"].",".$sa["StateTime"].",".$sa["UpdatedTime"];
					}
				}
			}
		}
		return $str;
	}

	// ���� �D��$Server
	function carry_serverarrall2() {

		$FA = glob("../config/server_*");
		foreach ($FA as $value) {
			if(file_exists($value)) {
				$s = implode("", @file($value));
				//$serverdata = unserialize(ultra_decrypt($s));
				$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				foreach($serverdata->d as $index => $value2) {
					$sa = $serverdata->value($index);
					$str .= iif(($str == ""), "", "@!!@").$sa["Server"]."@!@".$sa["Channel"]."@!@".$sa["FileName"]."@!@".$sa["Extension"]."@!@".$sa["AgentID"]."@!@".$sa["AgentName"]."@!@".$sa["Department"]."@!@".$sa["ACD"]."@!@".
						$sa["Number"]."@!@".$sa["CallType"]."@!@".$sa["State"]."@!@".$sa["StateTime"]."@!@".$sa["UpdatedTime"];
				}
			}
		}
		return $str;
	}

	// �O�d �D��$Server �j��$Channel ����$Extension �H���N�X$AgentID �H���m�W$AgentName ����$Department �s��$ACD ���X$Number ���X�J$CallType ���A$State ���A�ɶ�$StateTime ��s�ɶ�$UpdatedTime
	function store_server($Server, $Channel, $FileName, $Extension, $AgentID, $AgentName, $Department, $ACD, $Number, $CallType, $State, $StateTime, $UpdatedTime) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$serverdata = new Udata;
		}
		$serverdata->value($Channel, array("Channel" => "$Channel", "FileName" => "$FileName", "Extension" => "$Extension", "AgentID" => "$AgentID", "AgentName" => "$AgentName", "Department" => "$Department",
			"ACD" => "$ACD", "Number" => "$Number", "CallType" => "$CallType", "State" => "$State", "StateTime" => "$StateTime", "UpdatedTime" => "$UpdatedTime"));
		$s = serialize($serverdata);
		$fp = fopen("../config/server_".$Server, "w");
		fputs($fp, ultra_encrypt($s));
		fclose($fp);
	}

	// ���� �D��$Server
	function carry_server($Server, $var) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $serverdata->value($var);
		}
	}

	// �ˬd �D��$Server
	function check_server($Server, $var) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $serverdata->value($var);
		}
	}

	// �R�� �D��$Server
	function delete_server($Server, $var) {

		if(file_exists("../config/server_".$Server)) {
			$s = implode("", @file("../config/server_".$Server));
			//$serverdata = unserialize(ultra_decrypt($s));
			$serverdata = new Udata(); $serverdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$serverdata->delete($var);
			//$s = serialize($serverdata);
			$s = json_encode_this($serverdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/server_".$Server, "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/server_".$Server, ultra_encrypt($s));
		}
	}

	// �R�� �D��$Server
	function delete_serverall($Server) {

		dir_delete("../config/server_".$Server);
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password ���$selected ���ƤW��$maxrecord �j�M�Ҧ�$srchtype �X�R���$expandfield �B�~���$expandfield
	function store_select($Account, $Password, $selected, $maxrecord, $srchtype, $expandfield, $extrafield) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(file_exists("../config/".$Account."/store_selectdata")) {
			$s = implode("", @file("../config/".$Account."/store_selectdata"));
			//$selectdata = unserialize(ultra_decrypt($s));
			$selectdata = new Udata(); $selectdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$selectdata = new Udata;
		}
		$selectdata->value($Account, array("Selected" => "$selected", "MaxRecord" => "$maxrecord", "SrchType" => "$srchtype", "ExpandField" => "$expandfield", "ExtraField" => "$extrafield"));
		//$s = serialize($selectdata);
		$s = json_encode_this($selectdata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$Account."/store_selectdata", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/store_selectdata", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password
	function carry_select($Account, $Password) {

		if(file_exists("../config/".$Account."/store_selectdata")) {
			$s = implode("", @file("../config/".$Account."/store_selectdata"));
			//$selectdata = unserialize(ultra_decrypt($s));
			$selectdata = new Udata(); $selectdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $selectdata->value($Account);
		}
	}

	// �ˬd �b��$Account �K�X$Password
	function check_select($Account, $Password) {

		if(file_exists("../config/".$Account."/store_selectdata")) {
			$s = implode("", @file("../config/".$Account."/store_selectdata"));
			//$selectdata = unserialize(ultra_decrypt($s));
			$selectdata = new Udata(); $selectdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $selectdata->value($Account);
		}
	}

	// �R�� �b��$Account �K�X$Password
	function delete_select($Account, $Password) {

		dir_delete("../config/".$Account."/store_selectdata");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �W��$cid ����$condition
	function store_condition($Account, $Password, $cid, $condition) {

		if(file_exists("../config/".$Account."/store_conditon")) {
			$s = implode("", @file("../config/".$Account."/store_conditon"));
			//$conditiondata = unserialize(ultra_decrypt($s));
			$conditiondata = new Udata(); $conditiondata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$conditiondata = new Udata;
		}
		$conditiondata->value($cid."@!@", array("condition" => "$condition"));
		//$s = serialize($conditiondata);
		$s = json_encode_this($conditiondata, JSON_UNESCAPED_UNICODE);
		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		//$fp = fopen("../config/".$Account."/store_conditon", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/store_conditon", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password
	function carry_condition($Account, $Password) {

		if(file_exists("../config/".$Account."/store_conditon")) {
			$s = implode("", @file("../config/".$Account."/store_conditon"));
			//$conditiondata = unserialize(ultra_decrypt($s));
			$conditiondata = new Udata(); $conditiondata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$cs = "";
			foreach($conditiondata->d as $index => $value) {
				$cs .= iif(($cs == ""), "", ",").str_replace("@!@", "", $index);
			}
			return $cs;
		}
	}

	// ���� �b��$Account �K�X$Password �W��$cid
	function carry_onecondition($Account, $Password, $cid) {

		if(file_exists("../config/".$Account."/store_conditon")) {
			$s = implode("", @file("../config/".$Account."/store_conditon"));
			//$conditiondata = unserialize(ultra_decrypt($s));
			$conditiondata = new Udata(); $conditiondata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$ca = $conditiondata->value($cid."@!@");
			if (is_array($ca)) {
				$l = readregini("System" ,"Database" , "Language");
				foreach(array_keys($ca) as $key) {
					$ca[$key] = iconv($Language, 'utf-8', stripslashes($ca[$key]));
				}
			}
			return $ca;
		}
	}

	// �R�� �b��$Account �K�X$Password �W��$cid
	function delete_condition($Account, $Password, $cid) {

		if(file_exists("../config/".$Account."/store_conditon")) {
			$s = implode("", @file("../config/".$Account."/store_conditon"));
			//$conditiondata = unserialize(ultra_decrypt($s));
			$conditiondata = new Udata(); $conditiondata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$conditiondata->delete($cid."@!@");
			//$s = serialize($conditiondata);
			$s = json_encode_this($conditiondata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/".$Account."/store_conditon", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/".$Account."/store_conditon", ultra_encrypt($s));
		}
	}

	// �R�� �b��$Account �K�X$Password
	function delete_allcondition($Account, $Password) {

		dir_delete("../config/".$Account."/store_conditon");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �Ǹ�$findex �����X$fname
	function store_myfolder($Account, $Password, $findex, $fname) {

		if(file_exists("../config/".$Account."/store_myfolder")) {
			$s = implode("", @file("../config/".$Account."/store_myfolder"));
			//$myfolderdata = unserialize(ultra_decrypt($s));
			$myfolderdata = new Udata(); $myfolderdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$myfolderdata = new Udata;
		}
		if($findex == "") {$findex = microtime();}
		$myfolderdata->value($findex, array("fname" => "$fname"));
		//$s = serialize($myfolderdata);
		$s = json_encode_this($myfolderdata, JSON_UNESCAPED_UNICODE);
		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		//$fp = fopen("../config/".$Account."/store_myfolder", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/store_myfolder", ultra_encrypt($s));
		return $file;
	}

	// ���� �b��$Account �K�X$Password
	function carry_myfolder($Account, $Password) {

		if(file_exists("../config/".$Account."/store_myfolder")) {
			$s = implode("", @file("../config/".$Account."/store_myfolder"));
			//$myfolderdata = unserialize(ultra_decrypt($s));
			$myfolderdata = new Udata(); $myfolderdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $myfolderdata;
		}
	}

	// ���� �b��$Account �K�X$Password �Ǹ�$findex
	function carry_onemyfolder($Account, $Password, $findex) {

		if(file_exists("../config/".$Account."/store_myfolder")) {
			$s = implode("", @file("../config/".$Account."/store_myfolder"));
			//$myfolderdata = unserialize(ultra_decrypt($s));
			$myfolderdata = new Udata(); $myfolderdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $myfolderdata->value($findex);
		}
	}

	// �ˬd �b��$Account �K�X$Password
	function check_myfolder($Account, $Password) {

		if(file_exists("../config/".$Account."/store_myfolder")) {
			$s = implode("", @file("../config/".$Account."/store_myfolder"));
			//$myfolderdata = unserialize(ultra_decrypt($s));
			$myfolderdata = new Udata(); $myfolderdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $myfolderdata;
		}
	}

	// �R�� �b��$Account �K�X$Password �����X$myfolder
	function delete_myfolder($Account, $Password, $findex) {

		if(file_exists("../config/".$Account."/store_myfolder")) {
			$s = implode("", @file("../config/".$Account."/store_myfolder"));
			//$myfolderdata = unserialize(ultra_decrypt($s));
			$myfolderdata = new Udata(); $myfolderdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$myfolderdata->delete($findex);
			//$s = serialize($myfolderdata);
			$s = json_encode_this($myfolderdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/".$Account."/store_myfolder", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/".$Account."/store_myfolder", ultra_encrypt($s));
		}
	}

	// �R�� �b��$Account �K�X$Password
	function delete_allmyfolder($Account, $Password) {

		dir_delete("../config/".$Account."/store_myfolder");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �ɦW$file ���j�M���$sdata
	function store_search($Account, $Password, $folder, $file, $sdata) {

		if($file <> "") {
			if(file_exists("../config/".$Account."/".$folder."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$folder."/".$file));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			}
			else {
				$searchdata = new Udata;
			}
			$c = count($sdata);
			for ($i=1; $i<=$c; $i++) {
				$searchdata->value($sdata[$i][1]."-".$sdata[$i][2]."-".$sdata[$i][3], $sdata[$i]);
			}
			//$s = serialize($searchdata);
			$s = json_encode_this($searchdata, JSON_UNESCAPED_UNICODE);
			if(!is_dir("../config/".$Account)) {
				mkdir("../config/".$Account);
			}
			if(!is_dir("../config/".$Account."/".$folder)) {
				mkdir("../config/".$Account."/".$folder);
			}
			//$fp = fopen("../config/".$Account."/".$folder."/".$file, "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/".$Account."/".$folder."/".$file, ultra_encrypt($s));
			return $file;
		}
		else {
			$file = $Account."-".microtime();
			if(file_exists("../config/".$Account."/".$folder."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$folder."/".$file));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			}
			else {
				$searchdata = new Udata;
			}
			$c = count($sdata);
			for ($i=1; $i<=$c; $i++) {
				$searchdata->value($sdata[$i][1]."-".$sdata[$i][2]."-".$sdata[$i][3], $sdata[$i]);
			}
			//$s = serialize($searchdata);
			$s = json_encode_this($searchdata, JSON_UNESCAPED_UNICODE);
			if(!is_dir("../config/".$Account)) {
				mkdir("../config/".$Account);
			}
			if(!is_dir("../config/".$Account."/".$folder)) {
				mkdir("../config/".$Account."/".$folder);
			}
			//$fp = fopen("../config/".$Account."/".$folder."/".$file, "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/".$Account."/".$folder."/".$file, ultra_encrypt($s));
			return $file;
		}
	}

	// �t�s �b��$Account �K�X$Password �ӷ��ؿ�$dfolder �ӷ��ɦW$dfile �ت��ؿ�$sfolder �ت��ɦW$sfile ����$condi
	function restore_search($Account, $Password, $sfolder, $sfile, $dfolder, $dfile, $condi) {

		if(file_exists("../config/".$Account."/".$sfolder."/".$sfile)) {
			$s = implode("", @file("../config/".$Account."/".$sfolder."/".$sfile));
			//$sdata = unserialize(ultra_decrypt($s));
			$sdata = new Udata(); $sdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());

			if(file_exists("../config/".$Account."/".$dfolder."/".$dfile)) {
				$s = implode("", @file("../config/".$Account."/".$dfolder."/".$dfile));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			}
			else {
				$searchdata = new Udata;
			}
			$c = count($condi);
			$i = 0; $j = 0;
			foreach($sdata->d as $index => $value) {
				$sa = $sdata->value($index);
				$i++;
				if($i == $condi[$j]) {
					$searchdata->value($sa[1]."-".$sa[2]."-".$sa[3], $sa);
					if($j++ >= $c) {break;}
				}
			}
			//$s = serialize($searchdata);
			$s = json_encode_this($searchdata, JSON_UNESCAPED_UNICODE);
			if(!is_dir("../config/".$Account)) {
				mkdir("../config/".$Account);
			}
			if(!is_dir("../config/".$Account."/".$dfolder)) {
				mkdir("../config/".$Account."/".$dfolder);
			}
			//$fp = fopen("../config/".$Account."/".$dfolder."/".$dfile, "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/".$Account."/".$dfolder."/".$dfile, ultra_encrypt($s));
		}
	}

	// ���� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function carry_search($Account, $Password, $folder, $file) {

		if ($folder == "") {
			if(file_exists("../config/".$Account."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$file));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				return $searchdata;
			}
		}
		else {
			if(file_exists("../config/".$Account."/".$folder."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$folder."/".$file));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				return $searchdata;
			}
		}
	}

	// �ˬd �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function check_search($Account, $Password, $folder, $file) {

		if ($folder == "") {
			if(file_exists("../config/".$Account."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$file));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				return $searchdata;
			}
		}
		else {
			if(file_exists("../config/".$Account."/".$folder."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$folder."/".$file));
				//$searchdata = unserialize(ultra_decrypt($s));
				$searchdata = new Udata(); $searchdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				return $searchdata;
			}
		}
	}

	// �R�� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function delete_search($Account, $Password, $folder, $file) {

		if ($folder == "") {
			dir_delete("../config/".$Account."/".$file);
		}
		else {
			dir_delete("../config/".$Account."/".$folder."/".$file);
		}
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �ɮ�$file
	function store_display($Account, $Password, $folder, $file) {

		if(file_exists("../config/".$Account."/Temp/store_display")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_display"));
			//$displaydata = unserialize(ultra_decrypt($s));
			$displaydata = new Udata(); $displaydata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$displaydata = new Udata;
		}
		$d = $displaydata->value($Account);
		if ($d) {
			$n = ALLTAB;
			$c = intval($d["nowcnt"]);
			$o = intval($d["nowold"]);
			$l = intval($d["nowsel"]);
			if ($c >= $n) {
				$l = $o;
				$o = ($o % $n) + 1;
			}
			else {
				if ($c == 0) {
					$o = 1;
				}
				$c++;
				$l = $c;
			}
			$t = explode("&",$d["tab"]);
			$fa = explode(",",$t[0]);	$ga = explode(",",$t[1]);
			$fa[$l] = $folder."*|*|*".$file;
			$tmp = implode(",",$fa)."&".implode(",",$ga);
			$d["nowcnt"] = $c;
			$d["nowold"] = $o;
			$d["nowsel"] = $l;
			$d["tab"] = $tmp;
			$t = $l."&".$ga[$l];
		}
		else {
			$n = ALLTAB;
			$tmp = ",".$folder."*|*|*".$file;
			for ($i=2; $i<=$n; $i++) {
				$tmp .= ",";
			}
			$tmp .= "&".","."1";
			for ($i=2; $i<=$n; $i++) {
				$tmp .= ",".$i;
			}
			$d["nowcnt"] = 1;
			$d["nowold"] = 1;
			$d["nowsel"] = 1;
			$d["tab"] = $tmp;
			$t = "1"."&"."1";
		}
		$displaydata->value($Account, $d);
		//$s = serialize($displaydata);
		$s = json_encode_this($displaydata, JSON_UNESCAPED_UNICODE);
		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_display", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_display", ultra_encrypt($s));
		return $t;
	}

	// ���� �b��$Account �K�X$Password ���$sel
	function carry_display($Account, $Password, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_display")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_display"));
			//$displaydata = unserialize(ultra_decrypt($s));
			$displaydata = new Udata(); $displaydata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$d = $displaydata->value($Account);
			if ($d) {
				$c = intval($d["nowcnt"]);
				if ($c > 0) {
					$t = explode("&",$d["tab"]);
					$fa = explode(",",$t[0]);	$ga = explode(",",$t[1]);
					$tmp = $sel."&".$fa[$sel]."&".$ga[$sel];
					return $tmp;
				}
				else {
					return;
				}
			}
		}
	}

	// ���� �b��$Account �K�X$Password
	function carry_alldisplay($Account, $Password) {

		if(file_exists("../config/".$Account."/Temp/store_display")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_display"));
			//$displaydata = unserialize(ultra_decrypt($s));
			$displaydata = new Udata(); $displaydata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$d = $displaydata->value($Account);
			if ($d) {
				$n = ALLTAB;
				$c = $d["nowcnt"];
				for ($i=1; $i<=$n; $i++) {
					if ($i <= $c) {$tmp .= "1";}
					else {$tmp .= "0";}
				}
				$t = explode("&",$d["tab"]);
				$tmp .= "&".$t[1];
				return $tmp;
			}
		}
		$n = ALLTAB;
		for ($i=1; $i<=$n; $i++) {
			$tmp .= "0";
		}
		$tmp .= "&";
		for ($i=1; $i<=$n; $i++) {
			$tmp .= ",".$i;
		}
		return $tmp;
	}

	// ���� �b��$Account �K�X$Password ���$sel
	function store_nowseldisplay($Account, $Password, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_display")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_display"));
			//$displaydata = unserialize(ultra_decrypt($s));
			$displaydata = new Udata(); $displaydata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$d = $displaydata->value($Account);
			if ($d) {
				$d["nowsel"] = intval($sel);
				$displaydata->value($Account, $d);
				//$s = serialize($displaydata);
				$s = json_encode_this($displaydata, JSON_UNESCAPED_UNICODE);
				//$fp = fopen("../config/".$Account."/Temp/store_display", "w");
				//fputs($fp, ultra_encrypt($s));
				//fclose($fp);
				file_put_contents("../config/".$Account."/Temp/store_display", ultra_encrypt($s));
			}
		}
	}

	// ���� �b��$Account �K�X$Password
	function carry_nowseldisplay($Account, $Password) {

		if(file_exists("../config/".$Account."/Temp/store_display")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_display"));
			//$displaydata = unserialize(ultra_decrypt($s));
			$displaydata = new Udata(); $displaydata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$d = $displaydata->value($Account);
			if ($d) {
				return $d["nowsel"];
			}
		}
		return "0";
	}

	// �R�� �b��$Account �K�X$Password ���$sel
	function delete_seldisplay($Account, $Password, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_display")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_display"));
			//$displaydata = unserialize(ultra_decrypt($s));
			$displaydata = new Udata(); $displaydata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$d = $displaydata->value($Account);
			if ($d) {
				$c = intval($d["nowcnt"]);
				$d["nowcnt"] = $c - 1;
				$o = intval($d["nowold"]);
				$l = intval($d["nowsel"]);
				if ($c == 1) {
					$d["nowold"] = 0;
				}
				elseif ($sel < $o) {
					$d["nowold"] = $o - 1;
				}
				if ($c == 1) {
					$d["nowsel"] = 0;
				}
				elseif (($sel < $l) || (($sel == $l) && ($l == $c))) {
					$d["nowsel"] = $l - 1;
				}
				$t = explode("&",$d["tab"]);
				$fa = explode(",",$t[0]);	$ga = explode(",",$t[1]);
				$g = $ga[$sel];
				unset($fa[$sel]);	unset($ga[$sel]);
				array_push($fa, "");	array_push($ga, $g);
				$d["tab"] = implode(",", $fa)."&".implode (",", $ga);
				$displaydata->value($Account, $d);
				//$s = serialize($displaydata);
				$s = json_encode_this($displaydata, JSON_UNESCAPED_UNICODE);
				//$fp = fopen("../config/".$Account."/Temp/store_display", "w");
				//fputs($fp, ultra_encrypt($s));
				//fclose($fp);
				file_put_contents("../config/".$Account."/Temp/store_display", ultra_encrypt($s));
			}
		}
	}

	// �R�� �b��$Account �K�X$Password
	function delete_display($Account, $Password, $file) {

		dir_delete("../config/".$Account."/Temp/store_display");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �ɮ�$file
	function store_restore($Account, $Password, $folder, $file) {

		if(file_exists("../config/".$Account."/Temp/store_restore")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_restore"));
			//$restoredata = unserialize(ultra_decrypt($s));
			$restoredata = new Udata(); $restoredata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$restoredata = new Udata;
		}
		$r = $restoredata->value($Account);
		if ($r) {
			$n = ALLTAB;
			$c = intval($r["nowcnt"]);
			$o = intval($r["nowold"]);
			$l = intval($r["nowsel"]);
			if ($c >= $n) {
				$l = $o;
				$o = ($o % $n) + 1;
			}
			else {
				if ($c == 0) {
					$o = 1;
				}
				$c++;
				$l = $c;
			}
			$t = explode("&",$r["tab"]);
			$fa = explode(",",$t[0]);	$ga = explode(",",$t[1]);
			$fa[$l] = $folder."*|*|*".$file;
			$tmp = implode(",",$fa)."&".implode(",",$ga);
			$r["nowcnt"] = $c;
			$r["nowold"] = $o;
			$r["nowsel"] = $l;
			$r["tab"] = $tmp;
			$t = $l."&".$ga[$l];
		}
		else {
			$n = ALLTAB;
			$tmp = ",".$folder."*|*|*".$file;
			for ($i=2; $i<=$n; $i++) {
				$tmp .= ",";
			}
			$tmp .= "&".","."1";
			for ($i=2; $i<=$n; $i++) {
				$tmp .= ",".$i;
			}
			$r["nowcnt"] = 1;
			$r["nowold"] = 1;
			$r["nowsel"] = 1;
			$r["tab"] = $tmp;
			$t = "1"."&"."1";
		}
		$restoredata->value($Account, $r);
		//$s = serialize($restoredata);
		$s = json_encode_this($restoredata, JSON_UNESCAPED_UNICODE);
		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_restore", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_restore", ultra_encrypt($s));
		return $t;
	}

	// ���� �b��$Account �K�X$Password ���$sel
	function carry_restore($Account, $Password, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_restore")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_restore"));
			//$restoredata = unserialize(ultra_decrypt($s));
			$restoredata = new Udata(); $restoredata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$r = $restoredata->value($Account);
			if ($r) {
				$c = intval($r["nowcnt"]);
				if ($c > 0) {
					$t = explode("&",$r["tab"]);
					$fa = explode(",",$t[0]);	$ga = explode(",",$t[1]);
					$tmp = $sel."&".$fa[$sel]."&".$ga[$sel];
					return $tmp;
				}
				else {
					return;
				}
			}
		}
	}

	// ���� �b��$Account �K�X$Password
	function carry_allrestore($Account, $Password) {

		if(file_exists("../config/".$Account."/Temp/store_restore")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_restore"));
			//$restoredata = unserialize(ultra_decrypt($s));
			$restoredata = new Udata(); $restoredata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$r = $restoredata->value($Account);
			if ($r) {
				$n = ALLTAB;
				$c = $r["nowcnt"];
				for ($i=1; $i<=$n; $i++) {
					if ($i <= $c) {$tmp .= "1";}
					else {$tmp .= "0";}
				}
				$t = explode("&",$r["tab"]);
				$tmp .= "&".$t[1];
				return $tmp;
			}
		}
		$n = ALLTAB;
		for ($i=1; $i<=$n; $i++) {
			$tmp .= "0";
		}
		$tmp .= "&";
		for ($i=1; $i<=$n; $i++) {
			$tmp .= ",".$i;
		}
		return $tmp;
	}

	// ���� �b��$Account �K�X$Password ���$sel
	function store_nowselrestore($Account, $Password, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_restore")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_restore"));
			//$restoredata = unserialize(ultra_decrypt($s));
			$restoredata = new Udata(); $restoredata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$r = $restoredata->value($Account);
			if ($r) {
				$r["nowsel"] = intval($sel);
				$restoredata->value($Account, $r);
				//$s = serialize($restoredata);
				$s = json_encode_this($restoredata, JSON_UNESCAPED_UNICODE);
				//$fp = fopen("../config/".$Account."/Temp/store_restore", "w");
				//fputs($fp, ultra_encrypt($s));
				//fclose($fp);
				file_put_contents("../config/".$Account."/Temp/store_restore", ultra_encrypt($s));
			}
		}
	}

	// ���� �b��$Account �K�X$Password
	function carry_nowselrestore($Account, $Password) {

		if(file_exists("../config/".$Account."/Temp/store_restore")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_restore"));
			//$restoredata = unserialize(ultra_decrypt($s));
			$restoredata = new Udata(); $restoredata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$r = $restoredata->value($Account);
			if ($r) {
				return $r["nowsel"];
			}
		}
		return "0";
	}

	// �R�� �b��$Account �K�X$Password ���$sel
	function delete_selrestore($Account, $Password, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_restore")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_restore"));
			//$restoredata = unserialize(ultra_decrypt($s));
			$restoredata = new Udata(); $restoredata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$r = $restoredata->value($Account);
			if ($r) {
				$c = intval($r["nowcnt"]);
				$r["nowcnt"] = $c - 1;
				$o = intval($r["nowold"]);
				$l = intval($r["nowsel"]);
				if ($c == 1) {
					$r["nowold"] = 0;
				}
				elseif ($sel < $o) {
					$r["nowold"] = $o - 1;
				}
				if ($c == 1) {
					$r["nowsel"] = 0;
				}
				elseif (($sel < $l) || (($sel == $l) && ($l == $c))) {
					$r["nowsel"] = $l - 1;
				}
				$t = explode("&",$r["tab"]);
				$fa = explode(",",$t[0]);	$ga = explode(",",$t[1]);
				$g = $ga[$sel];
				unset($fa[$sel]);	unset($ga[$sel]);
				array_push($fa, "");	array_push($ga, $g);
				$r["tab"] = implode(",", $fa)."&".implode (",", $ga);
				$restoredata->value($Account, $r);
				//$s = serialize($restoredata);
				$s = json_encode_this($restoredata, JSON_UNESCAPED_UNICODE);
				//$fp = fopen("../config/".$Account."/Temp/store_restore", "w");
				//fputs($fp, ultra_encrypt($s));
				//fclose($fp);
				file_put_contents("../config/".$Account."/Temp/store_restore", ultra_encrypt($s));
			}
		}
	}

	// �R�� �b��$Account �K�X$Password
	function delete_restore($Account, $Password, $file) {

		dir_delete("../config/".$Account."/Temp/store_restore");
	}

	//************************************************************************************************************************

	// �t�s �b��$Account �K�X$Password �ӷ��ؿ�$dfolder �ӷ��ɦW$dfile �ت��ؿ�$sfolder �ت��ɦW$dfile ����$condi
	function store_download($Account, $Password, $sfolder, $sfile, $dfolder, $dfile, $condi) {

		if(file_exists("../config/".$Account."/".$sfolder."/".$sfile)) {
			$s = implode("", @file("../config/".$Account."/".$sfolder."/".$sfile));
			//$sdata = unserialize(ultra_decrypt($s));
			$sdata = new Udata(); $sdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());

			$downloaddata = new Udata;
			$c = count($condi);
			$i = 0; $j = 0;
			foreach($sdata->d as $index => $value) {
				$sa = $sdata->value($index);
				$i++;
				if($i == $condi[$j]) {
					$downloaddata->value($sa[1]."-".$sa[2]."-".$sa[3], $sa);
					if($j++ >= $c) {break;}
				}
			}
			//$s = serialize($downloaddata);
			$s = json_encode_this($downloaddata, JSON_UNESCAPED_UNICODE);
			if(!is_dir("../config/".$Account)) {
				mkdir("../config/".$Account);
			}
			if(!is_dir("../config/".$Account."/".$dfolder)) {
				mkdir("../config/".$Account."/".$dfolder);
			}
			//$fp = fopen("../config/".$Account."/".$dfolder."/".$dfile, "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/".$Account."/".$dfolder."/".$dfile, ultra_encrypt($s));
		}
	}

	// ���� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function carry_download($Account, $Password, $folder, $file) {

		if ($folder == "") {
			if(file_exists("../config/".$Account."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$file));
				//$downloaddata = unserialize(ultra_decrypt($s));
				$downloaddata = new Udata(); $downloaddata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				return $downloaddata;
			}
		}
		else {
			if(file_exists("../config/".$Account."/".$folder."/".$file)) {
				$s = implode("", @file("../config/".$Account."/".$folder."/".$file));
				//$downloaddata = unserialize(ultra_decrypt($s));
				$downloaddata = new Udata(); $downloaddata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				return $downloaddata;
			}
		}
	}

	// �R�� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function delete_download($Account, $Password, $folder, $file) {

		if ($folder == "") {
			dir_delete("../config/".$Account."/".$file);
		}
		else {
			dir_delete("../config/".$Account."/".$folder."/".$file);
		}
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �D�����$HostInfoArr
	function store_hostarr($Account, $Password, $HA) {

		if(file_exists("../config/".$Account."/Temp/store_hostinfo")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_hostinfo"));
			//$hostdata = unserialize(ultra_decrypt($s));
			$hostdata = new Udata(); $hostdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$hostdata = new Udata;
		}
		$c = count($HA);
		for ($i=1; $i<=$c; $i++) {
			$hostdata->value($HA[$i]["Server"], $HA[$i]);
		}
		//$s = serialize($hostdata);
		$s = json_encode_this($hostdata, JSON_UNESCAPED_UNICODE);
		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_hostinfo", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_hostinfo", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password
	function carry_hostarr($Account, $Password, $HStr) {

		if(file_exists("../config/".$Account."/Temp/store_hostinfo")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_hostinfo"));
			//$ha = unserialize(ultra_decrypt($s));
			$ha = new Udata(); $ha->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$sa = explode(",",$HStr);
			$s = "";
			foreach ($sa as $value) {
				$d = $ha->value(strtoupper($value));
				if ($d) {
					if ($s == "") {
						$s = $d["Server"].",".$d["ComputerName"].",".$d["Hypocorism"].",".$d["IP"].",".$d["Port"].",".$d["Serial"].",".$d["Language"];
					}
					else {
						$s .= "&".$d["Server"].",".$d["ComputerName"].",".$d["Hypocorism"].",".$d["IP"].",".$d["Port"].",".$d["Serial"].",".$d["Language"];
					}
				}
			}
			return $s;
		}
	}

	// ���� �b��$Account �K�X$Password �D��$Server
	function carry_host($Account, $Password, $Server) {

		if(file_exists("../config/".$Account."/Temp/store_hostinfo")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_hostinfo"));
			//$hostdata = unserialize(ultra_decrypt($s));
			$hostdata = new Udata(); $hostdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $hostdata->value(strtoupper($Server));
		}
	}

	// �R�� �b��$Account �K�X$Password
	function delete_host($Account, $Password) {

		dir_delete("../config/".$Account."/Temp/store_hostinfo");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �D��$Server �j��$Channel
	function store_rightopen($Account, $Password, $Server, $Channel) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_rightopen", "w");
		//fputs($fp, $Server.",".$Channel);
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_rightopen", $Server.",".$Channel);
	}

	// ���� �b��$Account �K�X$Password �D��$Server �j��$Channel
	function carry_rightopen($Account) {

		if(file_exists("../config/".$Account."/Temp/store_rightopen")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_rightopen"));
			return $s;
		}
	}

	// ���� �b��$Account
	function delete_rightopen($Account) {

		dir_delete("../config/".$Account."/Temp/store_rightopen");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password ���v����$list
	function store_history($Account, $Password, $list) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_history", "w");
		//fputs($fp, $list);
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_history", $list);
	}

	// ���� �b��$Account �K�X$Password ���v����$list
	function carry_history($Account) {

		if(file_exists("../config/".$Account."/Temp/store_history")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_history"));
			return $s;
		}
	}

	// ���� �b��$Account
	function delete_history($Account) {

		dir_delete("../config/".$Account."/Temp/store_history");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password ��ñ$sel ���ɮ�$file
	function store_playfile($Account, $sel, $file) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		if(file_exists("../config/".$Account."/Temp/store_playfile")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_playfile"));
			//$playfile = unserialize(ultra_decrypt($s));
			$playfile = new Udata(); $playfile->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$playfile = new Udata;
		}
		$playfile->value($sel, array("file" => "$file"));
		//$s = serialize($playfile);
		$s = json_encode_this($playfile, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$Account."/Temp/store_playfile", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_playfile", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password ��ñ$sel
	function carry_playfile($Account, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_playfile")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_playfile"));
			//$playfile = unserialize(ultra_decrypt($s));
			$playfile = new Udata(); $playfile->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$p = $playfile->value($sel);
			return $p["file"];
		}
	}

	// �O�d �b��$Account �K�X$Password ���ɮ�$file
	function store_playfile2($Account, $file) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_playfile", "w");
		//fputs($fp, ultra_encrypt($file));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_playfile", ultra_encrypt($file));
	}

	// ���� �b��$Account �K�X$Password ���ɮ�$file
	function carry_playfile2($Account) {

		if(file_exists("../config/".$Account."/Temp/store_playfile")) {
			$file = implode("", @file("../config/".$Account."/Temp/store_playfile"));
			return ultra_decrypt($file);
		}
	}

	// ���� �b��$Account
	function delete_playfile($Account) {

		dir_delete("../config/".$Account."/Temp/store_playfile");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password ��ñ$sel ���ɮ�$file
	function store_playrestorefile($Account, $sel, $file) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		if(file_exists("../config/".$Account."/Temp/store_playrestorefile")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_playrestorefile"));
			//$playfile = unserialize(ultra_decrypt($s));
			$playfile = new Udata(); $playfile->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$playfile = new Udata;
		}
		$playfile->value($sel, array("file" => "$file"));
		//$s = serialize($playfile);
		$s = json_encode_this($playfile, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$Account."/Temp/store_playrestorefile", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_playrestorefile", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password ��ñ$sel
	function carry_playrestorefile($Account, $sel) {

		if(file_exists("../config/".$Account."/Temp/store_playrestorefile")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_playrestorefile"));
			//$playfile = unserialize(ultra_decrypt($s));
			$playfile = new Udata(); $playfile->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$p = $playfile->value($sel);
			return $p[file];
		}
	}

	// �O�d �b��$Account �K�X$Password ���ɮ�$file
	function store_playrestorefile2($Account, $file) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_playrestorefile", "w");
		//fputs($fp, ultra_encrypt($file));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_playrestorefile", ultra_encrypt($file));
	}

	// ���� �b��$Account �K�X$Password ���ɮ�$file
	function carry_playrestorefile2($Account) {

		if(file_exists("../config/".$Account."/Temp/store_playrestorefile")) {
			$file = implode("", @file("../config/".$Account."/Temp/store_playrestorefile"));
			return ultra_decrypt($file);
		}
	}

	// �O�d �b��$Account �K�X$Password ���ɮ�$filename
	function store_playrestorefilename($Account, $filename) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		if(file_exists("../config/".$Account."/Temp/store_playrestorefilename")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_playrestorefilename"));
			//$playfilename = unserialize(ultra_decrypt($s));
			$playfilename = new Udata(); $playfilename->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$playfilename = new Udata;
		}
		$playfilename->value($filename, "1");
		//$s = serialize($playfilename);
		$s = json_encode_this($playfilename, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$Account."/Temp/store_playrestorefilename", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_playrestorefilename", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password
	function delete_playrestorefilename($Account) {

		if(file_exists("../config/".$Account."/Temp/store_playrestorefilename")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_playrestorefilename"));
			//$playfile = unserialize(ultra_decrypt($s));
			$playfile = new Udata(); $playfile->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach($playfile->d as $index => $value) {
				if (strstr($index, "http") === false) {
					if (is_file($index)) {
						unlink($index);
					}
				}
				else {
					remote_url($index);
				}
			}
		}
	}

	// ���� �b��$Account
	function delete_playrestorefile($Account) {

		dir_delete("../config/".$Account."/Temp/store_playrestorefile");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �Ҧ�$type �έp���$sdata
	function store_statis($uid, $folder, $type, $sdata) {

		if ($type <> "") {
			if(!is_dir("../config/@StatisData")) {
				mkdir("../config/@StatisData");
			}
			if(!is_dir("../config/@StatisData/".$uid)) {
				mkdir("../config/@StatisData/".$uid);
			}
			if(!is_dir("../config/@StatisData/".$uid."/".$folder)) {
				mkdir("../config/@StatisData/".$uid."/".$folder);
			}

			if(file_exists("../config/@StatisData/".$uid."/".$folder."/store_statis")) {
				$s = implode("", @file("../config/@StatisData/".$uid."/".$folder."/store_statis"));
				//$statisdata = unserialize(ultra_decrypt($s));
				$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				$f = $statisdata->value($type);
				$statisdata->delete($type);
				dir_delete($f["file"]);
			}
			else {
				$statisdata = new Udata;
			}
			$file = "../config/@StatisData/".$uid."/".$folder."/"."statis_".microtime();
			$fp = fopen($file, "w");
			fputs($fp, $sdata);
			fclose($fp);

			$statisdata->value($type, array("file" => "$file"));
			//$s = serialize($statisdata);
			$s = json_encode_this($statisdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/@StatisData/".$uid."/".$folder."/store_statis", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/@StatisData/".$uid."/".$folder."/store_statis", ultra_encrypt($s));
		}
	}

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �Ҧ�$type �έp���$sdata1 �έp���$sdata2
	function store_statis2($uid, $folder, $type, $sdata1, $sdata2) {

		if ($type <> "") {
			if(!is_dir("../config/@StatisData")) {
				mkdir("../config/@StatisData");
			}
			if(!is_dir("../config/@StatisData/".$uid)) {
				mkdir("../config/@StatisData/".$uid);
			}
			if(!is_dir("../config/@StatisData/".$uid."/".$folder)) {
				mkdir("../config/@StatisData/".$uid."/".$folder);
			}

			if(file_exists("../config/@StatisData/".$uid."/".$folder."/store_statis")) {
				$s = implode("", @file("../config/@StatisData/".$uid."/".$folder."/store_statis"));
				//$statisdata = unserialize(ultra_decrypt($s));
				$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
				$f = $statisdata->value($type);
				$statisdata->delete($type);
				$fa = explode(",",$f["file"]);
				dir_delete($fa[0]);
				dir_delete($fa[1]);
			}
			else {
				$statisdata = new Udata;
			}
			$file1 = "../config/@StatisData/".$uid."/".$folder."/"."statis_".microtime();
			$fp = fopen($file1, "w");
			fputs($fp, $sdata1);
			fclose($fp);
			$file2 = "../config/@StatisData/".$uid."/".$folder."/"."statis_".microtime();
			$fp = fopen($file2, "w");
			fputs($fp, $sdata2);
			fclose($fp);

			$file = $file1.",".$file2;
			$statisdata->value($type, array("file" => "$file"));
			//$s = serialize($statisdata);
			$s = json_encode_this($statisdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/@StatisData/".$uid."/".$folder."/store_statis", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/@StatisData/".$uid."/".$folder."/store_statis", ultra_encrypt($s));
		}
	}

	// ���� �b��$Account �K�X$Password �Ҧ�$type
	function carry_statis($uid, $folder, $type) {

		if(file_exists("../config/@StatisData/".$uid."/".$folder."/store_statis")) {
			$s = implode("", @file("../config/@StatisData/".$uid."/".$folder."/store_statis"));
			//$statisdata = unserialize(ultra_decrypt($s));
			$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$f = $statisdata->value($type);
			return $f['file'];
		}
	}

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �Ҧ�$mode ����$type
	function store_statistype($uid, $folder, $mode, $type) {

		if ($type <> "") {
			if(!is_dir("../config/@StatisData")) {
				mkdir("../config/@StatisData");
			}
			if(!is_dir("../config/@StatisData/".$uid)) {
				mkdir("../config/@StatisData/".$uid);
			}
			if(!is_dir("../config/@StatisData/".$uid."/".$folder)) {
				mkdir("../config/@StatisData/".$uid."/".$folder);
			}

			if(file_exists("../config/@StatisData/".$uid."/".$folder."/store_statistype")) {
				$s = implode("", @file("../config/@StatisData/".$uid."/".$folder."/store_statistype"));
				//$statisdata = unserialize(ultra_decrypt($s));
				$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			}
			else {
				$statisdata = new Udata;
			}

			$statisdata->value($mode, array("type" => "$type"));
			//$s = serialize($statisdata);
			$s = json_encode_this($statisdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/@StatisData/".$uid."/".$folder."/store_statistype", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/@StatisData/".$uid."/".$folder."/store_statistype", ultra_encrypt($s));
		}
	}

	// ���� �b��$Account �K�X$Password �Ҧ�$mode
	function carry_statistype($uid, $folder, $mode) {

		if(file_exists("../config/@StatisData/".$uid."/".$folder."/store_statistype")) {
			$s = implode("", @file("../config/@StatisData/".$uid."/".$folder."/store_statistype"));
			//$statisdata = unserialize(ultra_decrypt($s));
			$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			$t = $statisdata->value($mode);
			return $t["type"];
		}
	}

	// �ɦW$file ���j�M���$sdata, print
	function store_statisprint($uid, $folder, $file, $sdata) {

			if(!is_dir("../config/@StatisData")) {
				mkdir("../config/@StatisData");
			}
			if(!is_dir("../config/@StatisData/".$uid)) {
				mkdir("../config/@StatisData/".$uid);
			}
			if(!is_dir("../config/@StatisData/".$uid."/".$folder)) {
				mkdir("../config/@StatisData/".$uid."/".$folder);
			}
			//$fp = fopen("../config/@StatisData/".$uid."/".$folder."/".$file, "w");
			//fputs($fp, $sdata);
			//fclose($fp);
			file_put_contents("../config/@StatisData/".$uid."/".$folder."/".$file, $sdata);
			return $file;
	}

	// �ɦW$file ���j�M���$sdata, print
	function carry_statisprint($uid, $folder, $file) {

		if(file_exists("../config/@StatisData/".$uid."/".$folder."/".$file)) {
			$s = implode("", @file("../config/@StatisData/".$uid."/".$folder."/".$file));
			return $s;
		}
	}

	// �ɦW$file ���j�M���$sdata, print
	function delete_statisprint($uid, $folder, $file) {

		dir_delete("../config/@StatisData/".$uid."/".$folder."/".$file);
	}

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �Ҧ�$mode ����$type
	function store_statismap($uid, $Account) {

		if (($uid <> "") && ($Account <> "")) {
			if(!is_dir("../config/@StatisData")) {
				mkdir("../config/@StatisData");
			}

			if(file_exists("../config/@StatisData/store_statismap")) {
				$s = implode("", @file("../config/@StatisData/store_statismap"));
				//$statisdata = unserialize(ultra_decrypt($s));
				$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			}
			else {
				$statisdata = new Udata;
			}

			$statisdata->value($uid, array("Account" => "$Account"));
			//$s = serialize($statisdata);
			$s = json_encode_this($statisdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/@StatisData/store_statismap", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/@StatisData/store_statismap", ultra_encrypt($s));
		}
	}

	// �R�� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function delete_statis($Account) {

		if(file_exists("../config/@StatisData/store_statismap")) {
			$s = implode("", @file("../config/@StatisData/store_statismap"));
			//$statisdata = unserialize(ultra_decrypt($s));
			$statisdata = new Udata(); $statisdata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach($statisdata->d as $index => $value) {
				$a = $statisdata->value($index);
				if ($a["Account"] == $Account) {
					dir_delete("../config/@StatisData/".$index);
					$statisdata->delete($index);
				}
			}

			//$s = serialize($statisdata);
			$s = json_encode_this($statisdata, JSON_UNESCAPED_UNICODE);
			//$fp = fopen("../config/@StatisData/store_statismap", "w");
			//fputs($fp, ultra_encrypt($s));
			//fclose($fp);
			file_put_contents("../config/@StatisData/store_statismap", ultra_encrypt($s));
		}

	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �۰ʵn�X$AutoSignOut �۰ʧiĵ$AutoWarning �n�J����$StartIndex �e�q�Ҧ�$CapacityType
	function store_persion($Account, $Password, $AutoSignOut, $AutoWarning, $StartIndex, $CapacityType) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(file_exists("../config/".$Account."/store_persion")) {
			$s = implode("", @file("../config/".$Account."/store_persion"));
			//$persiondata = unserialize(ultra_decrypt($s));
			$persiondata = new Udata(); $persiondata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
		}
		else {
			$persiondata = new Udata;
		}
		$persiondata->value($Account, array("AutoSignOut" => "$AutoSignOut", "AutoWarning" => "$AutoWarning", "StartIndex" => "$StartIndex", "CapacityType" => "$CapacityType"));
		//$s = serialize($persiondata);
		$s = json_encode_this($persiondata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$Account."/store_persion", "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$Account."/store_persion", ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password
	function carry_persion($Account) {

		if(file_exists("../config/".$Account."/store_persion")) {
			$s = implode("", @file("../config/".$Account."/store_persion"));
			//$persiondata = unserialize(ultra_decrypt($s));
			$persiondata = new Udata(); $persiondata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			return $persiondata->value($Account);
		}
	}

	// �R�� �b��$Account
	function delete_persion($Account) {

		dir_delete("../config/".$Account."/store_persion");
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �ɮ�$file ���$sdate �έp���$sdata
	function store_agentdate($Account, $Password, $folder, $file, $sdate, $sdata) {

		if(!is_dir("../config/".$folder)) {
			mkdir("../config/".$folder);
		}

		$agentdatedata = new Udata;
		$agentdatedata->value($sdate, $sdata);
		//$s = serialize($agentdatedata);
		$s = json_encode_this($agentdatedata, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$folder."/".$file, "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);
		file_put_contents("../config/".$folder."/".$file, ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password ���$sdate �ؿ�$folder �ɮ�$file
	function carry_agentdate($Account, $folder, $file) {

		if(file_exists("../config/".$folder."/".$file)) {
			$s = implode("", @file("../config/".$folder."/".$file));
			//$agentdatedata = unserialize(ultra_decrypt($s));
			$agentdatedata = new Udata(); $agentdatedata->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach($agentdatedata->d as $index => $value) {
				$a = array($index, $value);
			}
		}
		else {
			$a = array("2020/01/01", null);
		}
		return $a;
	}

	// �R�� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function delete_agentdate($Account, $Password, $folder, $file) {

		dir_delete("../config/".$folder."/".$file);
	}

	//************************************************************************************************************************

	// �O�d �b��$Account �K�X$Password �ؿ�$folder �ɮ�$file ���$sdate �έp���$sdata
	function store_authoritydate($Account, $Password, $folder, $file, $sdate, $sdata) {

		if(!is_dir("../config/".$folder)) {
			mkdir("../config/".$folder);
		}

		$authoritydate = new Udata;
		$authoritydate->value($sdate, $sdata);
		//$s = serialize($authoritydate);
		$s = json_encode_this($authoritydate, JSON_UNESCAPED_UNICODE);
		//$fp = fopen("../config/".$folder."/".$file, "w");
		//fputs($fp, ultra_encrypt($s));
		//fclose($fp);]
		file_put_contents("../config/".$folder."/".$file, ultra_encrypt($s));
	}

	// ���� �b��$Account �K�X$Password ���$sdate �ؿ�$folder �ɮ�$file
	function carry_authoritydate($Account, $folder, $file) {

		if(file_exists("../config/".$folder."/".$file)) {
			$s = implode("", @file("../config/".$folder."/".$file));
			//$authoritydate = unserialize(ultra_decrypt($s));
			$authoritydate = new Udata(); $authoritydate->d =(($dd = json_decode(ultra_decrypt($s), true)) ? $dd["d"] : array());
			foreach($authoritydate->d as $index => $value) {
				$a = array($index, $value);
			}
		}
		else {
			$a = array("2020/01/01", null);
		}
		return $a;
	}

	// �R�� �b��$Account �K�X$Password �ؿ�$folder �ɦW$file
	function delete_authoritydate($Account, $Password, $folder, $file) {

		dir_delete("../config/".$folder."/".$file);
	}

	//************************************************************************************************************************

	// �P�_�v�� �y�t$Language �b�����$Acc �l�\��$Fun �j��$Channel ����$Extension �s��$AgentID �H�W$AgentName ����$Department �s��$ACD
	function CheckSyntax($Language, $Acc, $Fun, $Channel, $Extension, $AgentID, $AgentName, $Department, $ACD) {

		$b = true;
		$Acc["Channel"] = iconv($Language, 'utf-8', $Acc["Channel"]);
		if (($Acc["Channel"] <> "<ALL>") && ($Acc["Channel"] <> "")) {
			$e = strpos(",".$Acc["Channel"].",", ",".$Channel."~@".",");
			if ($e !== false) {
				if (((int)substr($Acc["Channel"], $e + strlen(",".$Channel."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Channel"].",", ",".$Channel.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["Extension"] = iconv($Language, 'utf-8', $Acc["Extension"]);
		if (($Acc["Extension"] <> "<ALL>") && ($Acc["Extension"] <> "")) {
			$e = strpos(",".$Acc["Extension"].",", ",".$Extension."~@");
			if ($e !== false) {
				if (((int)substr($Acc["Extension"], $e + strlen(",".$Extension."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Extension"].",", ",".$Extension.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["AgentID"] = iconv($Language, 'utf-8', $Acc["AgentID"]);
		if (($Acc["AgentID"] <> "<ALL>") && ($Acc["AgentID"] <> "")) {
			$e = strpos(",".$Acc["AgentID"].",", ",".$AgentID."~@");
			if ($e !== false) {
				if (((int)substr($Acc["AgentID"], $e + strlen(",".$AgentID."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["AgentID"].",", ",".$AgentID.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["AgentName"] = iconv($Language, 'utf-8', $Acc["AgentName"]);
		if (($Acc["AgentName"] <> "<ALL>") && ($Acc["AgentName"] <> "")) {
			$e = strpos(",".$Acc["AgentName"].",", ",".$AgentName."~@");
			if ($e !== false) {
				if (((int)substr($Acc["AgentName"], $e + strlen(",".$AgentName."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["AgentName"].",", ",".$AgentName.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["Department"] = iconv($Language, 'utf-8', $Acc["Department"]);
		if (($Acc["Department"] <> "<ALL>") && ($Acc["Department"] <> "")) {
			$e = strpos(",".$Acc["Department"].",", ",".$Department."~@");
			if ($e !== false) {
				if (((int)substr($Acc["Department"], $e + strlen(",".$Department."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Department"].",", ",".$Department.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["ACD"] = iconv($Language, 'utf-8', $Acc["ACD"]);
		if (($Acc["ACD"] <> "<ALL>") && ($Acc["ACD"] <> "")) {
			$e = strpos(",".$Acc["ACD"].",", ",".$ACD."~@");
			if ($e !== false) {
				if (((int)substr($Acc["ACD"], $e + strlen(",".$ACD."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["ACD"].",", ",".$ACD.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		return $b;
	}

	// �P�_�v�� �y�t$Language �b�����$Acc �l�\��$Fun �D��$Server �j��$Channel ����$Extension �s��$AgentID �H�W$AgentName ����$Department �s��$ACD
	function CheckSyntax2($Language, $Acc, $Fun, $Server, $Channel, $Extension, $AgentID, $AgentName, $Department, $ACD) {

		$b = true;
		$Acc["Server"] = iconv($Language, 'utf-8', $Acc["Server"]);
		//if (($Acc["Server"] <> "<ALL>") && ($Acc["Server"] <> "")) {					//Server�t��<ALL>,......
		if ((strpos($Acc["Server"], "<ALL>") === false) && ($Acc["Server"] <> "")) {	//�令�ηj�M��
			$e = strpos(",".$Acc["Server"].",", ",".$Server."~@");
			if ($e !== false) {
				if (((int)substr($Acc["Server"], $e + strlen(",".$Server."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Server"].",", ",".$Server.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["Channel"] = iconv($Language, 'utf-8', $Acc["Channel"]);
		if (($Acc["Channel"] <> "<ALL>") && ($Acc["Channel"] <> "")) {
			$e = strpos(",".$Acc["Channel"].",", ",".$Channel."~@".",");
			if ($e !== false) {
				if (((int)substr($Acc["Channel"], $e + strlen(",".$Channel."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Channel"].",", ",".$Channel.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["Extension"] = iconv($Language, 'utf-8', $Acc["Extension"]);
		if (($Acc["Extension"] <> "<ALL>") && ($Acc["Extension"] <> "")) {
			$e = strpos(",".$Acc["Extension"].",", ",".$Extension."~@");
			if ($e !== false) {
				if (((int)substr($Acc["Extension"], $e + strlen(",".$Extension."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Extension"].",", ",".$Extension.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["AgentID"] = iconv($Language, 'utf-8', $Acc["AgentID"]);
		if (($Acc["AgentID"] <> "<ALL>") && ($Acc["AgentID"] <> "")) {
			$e = strpos(",".$Acc["AgentID"].",", ",".$AgentID."~@");
			if ($e !== false) {
				if (((int)substr($Acc["AgentID"], $e + strlen(",".$AgentID."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["AgentID"].",", ",".$AgentID.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["AgentName"] = iconv($Language, 'utf-8', $Acc["AgentName"]);
		if (($Acc["AgentName"] <> "<ALL>") && ($Acc["AgentName"] <> "")) {
			$e = strpos(",".$Acc["AgentName"].",", ",".$AgentName."~@");
			if ($e !== false) {
				if (((int)substr($Acc["AgentName"], $e + strlen(",".$AgentName."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["AgentName"].",", ",".$AgentName.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["Department"] = iconv($Language, 'utf-8', $Acc["Department"]);
		if (($Acc["Department"] <> "<ALL>") && ($Acc["Department"] <> "")) {
			$e = strpos(",".$Acc["Department"].",", ",".$Department."~@");
			if ($e !== false) {
				if (((int)substr($Acc["Department"], $e + strlen(",".$Department."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["Department"].",", ",".$Department.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		$Acc["ACD"] = iconv($Language, 'utf-8', $Acc["ACD"]);
		if (($Acc["ACD"] <> "<ALL>") && ($Acc["ACD"] <> "")) {
			$e = strpos(",".$Acc["ACD"].",", ",".$ACD."~@");
			if ($e !== false) {
				if (((int)substr($Acc["ACD"], $e + strlen(",".$ACD."~@") - 1, 1) & $Fun) != 0) {
					return true;
				}
				else {
					$b = false;
				}
			}
			elseif ((strpos(",".$Acc["ACD"].",", ",".$ACD.",")) !== false) {
				return true;
			}
			else {
				$b = false;
			}
		}
		return $b;
	}

	// �ѪR�y�k �b�����$Acc �l�\��$Fun
	function ParseSQLSyntax($Acc, $Fun) {

		$s = "";
		if (($Acc["Channel"] <> "<ALL>") && ($Acc["Channel"] <> "")) {
			$str = "";
			$c = explode(",",$Acc["Channel"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[Channel] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[Channel] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}

		if (($Acc["Extension"] <> "<ALL>") && ($Acc["Extension"] <> "")) {
			$str = "";
			$c = explode(",",$Acc["Extension"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[Extension] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[Extension] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}

		if (($Acc["AgentID"] <> "<ALL>") && ($Acc["AgentID"] <> "")) {
			$str = "";
			$c = explode(",",$Acc["AgentID"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[AgentID] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[AgentID] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}

		if (($Acc["AgentName"] <> "<ALL>") && ($Acc["AgentName"] <> "")) {
			$str = "";
			$c = explode(",",$Acc["AgentName"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[AgentName] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[AgentName] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}

		if (($Acc["Department"] <> "<ALL>") && ($Acc["Department"] <> "")) {
			$str = "";
			$c = explode(",",$Acc["Department"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[Department] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[Department] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}

		if (($Acc["ACD"] <> "<ALL>") && ($Acc["ACD"] <> "")) {
			$str = "";
			$c = explode(",",$Acc["ACD"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[ACD] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[ACD] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}
		if ($s <> '') $s = "(".$s.")";

		if (!strstr($Acc["Server"], "<ALL>")) {
			$str = "";
			$c = explode(",",$Acc["Server"]);
			foreach ($c as $value) {
				$cc = explode("~@",$value);
				if (($cc[0] <> '') && ($cc[0] <> '<ALL>')) {
					if (count($cc) == 1) {
						$str .= iif(($str == ""), "", " Or ")."[Server] = '".$cc[0]."'";
					}
					else {
						if (((int)$cc[1] & $Fun) <> 0) {
							$str .= iif(($str == ""), "", " Or ")."[Server] = '".$cc[0]."'";
						}
					}
				}
			}
			if ($str <> '') {
				//$s .= iif(($s == ""), "", " And ")."(".$str.")";
				$s .= iif(($s == ""), "", " Or ")."(".$str.")";
			}
		}
		if ($s <> '') $s = "(".$s.")";
		//return $s;
		return iconv('UTF-8', 'Big5', $s);
	}

	// �O�d �b��$Account �K�X$Password ��ñ$sel
	function store_seltab($Account, $sel) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_seltab", "w");
		//fputs($fp, $sel);
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_seltab", $sel);
	}

	// ���� �b��$Account �K�X$Password ��ñ$sel
	function carry_seltab($Account) {

		if(file_exists("../config/".$Account."/Temp/store_seltab")) {
			$sel = implode("", @file("../config/".$Account."/Temp/store_seltab"));
			return $sel;
		}
	}

	// �O�d �b��$Account �K�X$Password ��ñ$sel
	function store_selrestoretab($Account, $sel) {

		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_selrestoretab", "w");
		//fputs($fp, $sel);
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_selrestoretab", $sel);
	}

	// ���� �b��$Account �K�X$Password ��ñ$sel
	function carry_selrestoretab($Account) {

		if(file_exists("../config/".$Account."/Temp/store_selrestoretab")) {
			$sel = implode("", @file("../config/".$Account."/Temp/store_selrestoretab"));
			return $sel;
		}
	}

	// �ɦW$file ���j�M���$sdata, BDE - Backup/Delete/Error
	function store_search_BDElog($Account, $file, $sdata) {
			//$fp = fopen("../config/".$Account."/Temp/".$file, "w");
			//fputs($fp, $sdata);
			//fclose($fp);
			file_put_contents("../config/".$Account."/Temp/".$file, $sdata);
			return $file;
	}

	// �ɦW$file ���j�M���$sdata, BDE - Backup/Delete/Error
	function carry_search_BDElog($Account, $file) {
		if(file_exists("../config/".$Account."/Temp/".$file)) {
			$s = implode("", @file("../config/".$Account."/Temp/".$file));
			return $s;
		}
	}

	// �ɦW$file ���j�M���$sdata, BDE - Backup/Delete/Error
	function delete_search_BDElog($Account, $file) {

		dir_delete("../config/".$Account."/Temp/".$file);
	}

	// �ɦW$file ���j�M���$sdata, OP
	function store_search_OPlog($Account, $file, $sdata) {
			//$fp = fopen("../config/".$Account."/Temp/".$file, "w");
			//fputs($fp, $sdata);
			//fclose($fp);
			file_put_contents("../config/".$Account."/Temp/".$file, $sdata);
			return $file;
	}

	// �ɦW$file ���j�M���$sdata, OP
	function carry_search_OPlog($Account, $file) {
		if(file_exists("../config/".$Account."/Temp/".$file)) {
			$s = implode("", @file("../config/".$Account."/Temp/".$file));
			return $s;
		}
	}

	// �ɦW$file ���j�M���$sdata, OP
	function delete_search_OPlog($Account, $file) {

		dir_delete("../config/".$Account."/Temp/".$file);
	}

	// �ɦW$file �j�q�Ŀ�r��$chk
	function store_checkbox($Account, $file, $chk) {
			//$fp = fopen("../config/".$Account."/Temp/".$file, "a");
			//fputs($fp, $chk);
			//fclose($fp);
			file_put_contents("../config/".$Account."/Temp/".$file, $chk, FILE_APPEND);
			return $file;
	}

	// �ɦW$file �j�q�Ŀ�r��$chk
	function carry_checkbox($Account, $file) {
		if(file_exists("../config/".$Account."/Temp/".$file)) {
			$s = implode("", @file("../config/".$Account."/Temp/".$file));
			return $s;
		}
	}

	// �ɦW$file �j�q�Ŀ�r��$chk
	function delete_checkbox($Account, $file) {

		dir_delete("../config/".$Account."/Temp/".$file);
	}

	// �ɦW$file �v�����$sdata
	function store_authority_data($file, $sdata) {
			//$fp = fopen("../config/".$file, "w");
			//fputs($fp, $sdata);
			//fclose($fp);
			file_put_contents("../config/".$file, $sdata);
			return $file;
	}

	// �ɦW$file
	function carry_authority_data($file) {
		if(file_exists("../config/".$file)) {
			$s = implode("", @file("../config/".$file));
			return $s;
		}
	}

	// �ɦW$file ���$sdata
	function store_extra_data($file, $sdata) {
		$fa = explode("/", $file); $fs = ""; $fc = 1;
		if (is_array($fa)) {
			foreach ($fa as $index => $value) {
				if ($value != "") {
					$fs .= "/".$value;
					if(!is_dir("../config".$fs)) {
						mkdir("../config".$fs);
					}
				}
				if (++$fc >= count($fa)) break;;
			}

			//$fp = fopen("../config/".$file, "w");
			//fputs($fp, $sdata);
			//fclose($fp);
			file_put_contents("../config/".$file, $sdata);
			return $file;
		}
	}

	// �ɦW$file
	function carry_extra_data($file) {
		if(file_exists("../config/".$file)) {
			$s = implode("", @file("../config/".$file));
			return $s;
		}
	}

	// �ɦW$file
	function carry_file_content($Account, $folder, $file, $pos, $size) {
		if(file_exists("../config/".$Account."/".$folder."/".$file)) {
			if ($stream = fopen("../config/".$Account."/Temp/".$file, 'r')) {
			    $s = stream_get_contents($stream, $size, $pos);
			    fclose($stream);
			}
			return $s;
		}
	}

	// �b��$Account ������$sdata
	function store_acclimit($Account, $sdata) {
		if(!is_dir("../config/".$Account)) {
			mkdir("../config/".$Account);
		}
		if(!is_dir("../config/".$Account."/Temp")) {
			mkdir("../config/".$Account."/Temp");
		}
		//$fp = fopen("../config/".$Account."/Temp/store_acclimit", "w");
		//fputs($fp, $sdata);
		//fclose($fp);
		file_put_contents("../config/".$Account."/Temp/store_acclimit", $sdata);
	}

	// �b��$Account
	function carry_acclimit($Account) {
		if(file_exists("../config/".$Account."/Temp/store_acclimit")) {
			$s = implode("", @file("../config/".$Account."/Temp/store_acclimit"));
			return $s;
		}
	}

	// ���X�R��� $ValueStr
	function carry_expand_field($ValueStr) {
		$s = ((($ValueStr & FUN_EXPAND_FIELD1) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD2) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD3) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD4) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD5) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD6) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD7) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD8) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXPAND_FIELD9) == 0) ? 0 : 1);
		return $s;
	}

	// ���B�~��� $ValueStr
	function carry_extra_field($ValueStr) {
		$s = ((($ValueStr & FUN_EXTRA_FIELD1) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD2) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD3) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD4) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD5) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD6) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD7) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD8) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD9) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD10) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD11) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD12) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD13) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD14) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD15) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD16) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD17) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD18) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD19) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD20) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD21) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD22) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD23) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD24) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD25) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD26) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD27) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD28) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD29) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD30) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD31) == 0) ? 0 : 1);
		$s .= ((($ValueStr & FUN_EXTRA_FIELD32) == 0) ? 0 : 1);
		return $s;
	}

	function get_browser_version(){
        $browser_version = array("MSIE","Firefox","Opera","Safari","Netscape");
        $i = 0;
        while($i < count($browser_version)) {
         	$ba = get_browser();
			//if(strstr($_SERVER["HTTP_USER_AGENT"],$browser_version[$i])) {
			if(strstr($ba["browser_name_pattern"],$browser_version[$i])) {
				return $browser_version[$i];
			}
			$i++;
        }
        return "Unknow";
	}

	function encodingConvent($op) {
		return encodingConventBase($op, true);
	}

	function encodingConventUtf8($op) {
		return encodingConventBase($op, false);
	}

	function encodingConventBase($op, $isToBig5) {
		/*$cm = new_ocx("RegValue.RegValueCtl");
		$Language = $cm->ReadRegString("System" ,"Database" , "Language");*/
		$Language = readregini("System" ,"Database" , "Language");
		$encoding = mb_detect_encoding($op);
		if ($isToBig5 == true) {
			return iconv($encoding, $Language, $op);
		} else {
			return iconv($Language, "UTF-8", $op);
		}
	}
	
	//2020-11-04 gerald for SQL Injection
	function mssql_escape($data) {

		if(is_numeric($data))
			return $data;
		$unpacked = unpack('H*hex', $data);
		return '0x'.$unpacked['hex'];
	}
	
	function NormalizationIP($s) {

		$pat = "/^(((1?\d{1,2})|(2[0-4]\d)|(25[0-5]))\.){3}((1?\d{1,2})|(2[0-4]\d)|(25[0-5]))$/";
		preg_match($pat, $s, $out);
		if (is_array($out) && (count($out) > 0)) {
			return $out[0];
		}
	}
?>