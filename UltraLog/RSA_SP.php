<?php
	define("PRIVATE_KEY", 'D:/AP/AVR/AppServ/www/UltraLog/unisoon_ultralog.key');
	define("PUBLIC_KEY", 'D:/AP/AVR/AppServ/www/UltraLog/unisoon_ultralog.pem');

	/*foreach (array_keys($_POST) as $key) $$key = $_POST[$key];
	foreach (array_keys($_GET) as $key) {
		$$key = (isset($$key) ? $$key : $_GET[$key]);
	}*/
	$xPost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$xGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	if (is_array($xPost)) foreach (array_keys($xPost) as $key) $$key = $xPost[$key];
	if (is_array($xGet)) {
		foreach (array_keys($xGet) as $key) {
			$$key = (isset($$key) ? $$key : $xGet[$key]);
		}
	}

	$s = "";
	switch ($Mode) {
		case 'encrypt':
			$s = public_encrypt($Code);
			break;
		case 'encrypt2':
			$ULC = 0x35;

			$str = "";
			for ($i=0; $i<strlen($Code); $i+=4) {
				$str .= chr(hexdec(substr($Code, $i, 2)) ^ $ULC);
			}
			$s = public_encrypt($str);
			break;
		case 'decrypt':
			$s = private_decrypt(str_replace(" ", "+", $Code));
			break;
		default:
			break;
	}
	print_r(htmlspecialchars($s));

	function public_encrypt($plain_text) {

		if (file_exists(PUBLIC_KEY)) {
			$fp = fopen(PUBLIC_KEY, "r");
			$pub_key = fread($fp, 8192);
			fclose($fp);
			$pub_key_res = openssl_get_publickey($pub_key);
			if (!$pub_key_res) {
				return;
			}
			openssl_public_encrypt($plain_text, $crypt_text, $pub_key_res, OPENSSL_PKCS1_OAEP_PADDING);
			openssl_free_key($pub_key_res);
			return base64_encode($crypt_text);
		}
	}
	
	function private_decrypt($encrypted_text) {

		if (file_exists(PRIVATE_KEY)) {
			$fp = fopen(PRIVATE_KEY, "r");
			$priv_key = fread($fp, 8192);
			fclose($fp);
			$private_key_res = openssl_get_privatekey($priv_key);
			// $private_key_res = openssl_get_privatekey($priv_key, PASSPHRASE); // �p�G�ϥαK�X
			if (!$private_key_res) {
				return;
			}
			openssl_private_decrypt(base64_decode($encrypted_text), $decrypted, $private_key_res, OPENSSL_PKCS1_OAEP_PADDING);
			openssl_free_key($private_key_res);
			return $decrypted;
		}
	}
?>