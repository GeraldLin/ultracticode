//---------------------------------------------------------------------------
#ifndef ProcttsH
#define ProcttsH

//---------------------------------------------------------------------------
//處理TTS代理服務器來的消息
void ProcTTSMsg(US msgtype, const UC * msgbuf);
//停止TTS合成
void StopTTS(int nChn);
//取下一語音流
void GetNextTTSStream(int nChn);
//---------------------------------------------------------------------------
#endif
