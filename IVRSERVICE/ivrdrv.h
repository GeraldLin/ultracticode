//-----------------------------------------------------------------------------
#ifndef __IVRDRV_H__
#define __IVRDRV_H__
//-----------------------------------------------------------------------------
#define MYLIBAPI __declspec(dllexport)

MYLIBAPI char *GetIvrDrvVersion();
MYLIBAPI char *GetDevice();
MYLIBAPI short GetDeviceId();
MYLIBAPI short SetWriteDebugId(short WriteDebugId);

MYLIBAPI short MAININIT(short InitParam);
MYLIBAPI short MAINEXIT();
MYLIBAPI short GetChnNum();
MYLIBAPI short GetConfNum();
MYLIBAPI short GetFaxChnNum();
MYLIBAPI short GetRecChnNum();
MYLIBAPI short GetTrunkNum();
MYLIBAPI short GetSeatNum();
MYLIBAPI short GetChnNumByType(short ChType);
MYLIBAPI short GetStartChIndexByType(short ChType);
MYLIBAPI short GetChStyle(short ChnNo);
MYLIBAPI short GetChStyleIndex(short ChnNo);
MYLIBAPI short GetChType(short ChnNo);
MYLIBAPI short GetChIndex(short ChnNo);
MYLIBAPI short GetLgChType(short ChnNo);
MYLIBAPI short GetLgChIndex(short ChnNo);

MYLIBAPI short ADDPLAYINDEX(short VocIndex, LPCTSTR FileName, LPCTSTR AliasName);
MYLIBAPI short POSTPLAYFILE(short ChType, short ChIndex, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTPLAYMEM(short ChType, short ChIndex, LPCTSTR MemBuf, long BufSize, long PlayOffset, long PlayLength, short PlayParam, short PlayRule);
MYLIBAPI short POSTPLAYINDEX(short ChType, short ChIndex, short VocIndex, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTPLAYTTSSTR(short ChType, short ChIndex, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTPLAYTTSFILE(short ChType, short ChIndex, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTSENDDTMF(short ChType, short ChIndex, LPCTSTR DtmfStr, short SendParam, LPCTSTR SendRule);
MYLIBAPI short POSTSTOPPLAY(short ChType, short ChIndex, short StopParam);

MYLIBAPI short POSTSTARTRECVFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short CRCType, short RecvParam, LPCTSTR RecvRule);
MYLIBAPI short POSTSTOPRECVFSK(short ChType, short ChIndex, short FskChIndex);
MYLIBAPI short POSTSENDFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short FskCMD, long FskDataAddr, short FskDataLen, short CRCType, short SendParam, LPCTSTR SendRule);
MYLIBAPI short POSTSTOPSENDFSK(short ChType, short ChIndex, short FskChIndex);

MYLIBAPI short POSTRECORDFILE(short ChType, short ChIndex, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
MYLIBAPI short POSTRECORDMEM(short ChType, short ChIndex, long RecdLength, short RecdParam, LPCTSTR RecdRule);
MYLIBAPI short POSTSTOPRECORD(short ChType, short ChIndex, short StopParam);
MYLIBAPI short POSTSETRPPARAM(short ChType, short ChIndex, short SetParam, LPCTSTR SetRule);

MYLIBAPI short CONFPLAYFILE(short ConfNo, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFPLAYINDEX(short ConfNo, short VocIndex, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFPLAYTTSSTR(short ConfNo, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFPLAYTTSFILE(short ConfNo, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFSTOPPLAY(short ConfNo, short StopParam);
MYLIBAPI short CONFRECORDFILE(short ConfNo, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
MYLIBAPI short CONFSTOPRECORD(short ConfNo, short StopParam);
MYLIBAPI short CONFSETPARAM(short ConfNo, short SetParam, LPCTSTR SetRule);

MYLIBAPI short POSTSENDFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, short FaxParam, LPCTSTR FaxRule);
MYLIBAPI short POSTRECVFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, short FaxParam, LPCTSTR FaxRule);
MYLIBAPI short POSTSTOPFAX(short ChType, short ChIndex, short FaxChIndex, short StopParam);
MYLIBAPI short POSTCHECKTONE(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam);

MYLIBAPI short POSTCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
MYLIBAPI short POSTCANCELCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag);
MYLIBAPI short POSTSAM(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called);
MYLIBAPI short POSTACM(short ChType, short ChIndex, short AcmParam);
MYLIBAPI short POSTACK(short ChType, short ChIndex, short AckParam);
MYLIBAPI short POSTFLASH(short ChType, short ChIndex, short FlashParam);
MYLIBAPI short POSTRELEASE(short ChType, short ChIndex, short Reason);

MYLIBAPI short POSTTALK(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short TalkParam);
MYLIBAPI short POSTMONITOR(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short MonitorParam);
MYLIBAPI short POSTDISCON(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short DisconParam);
MYLIBAPI short POSTSETVCPARAM(short ChType, short ChIndex, short VCMode, short VCParam);

MYLIBAPI short CONFCREATE(short ChType, short ChIndex, short ConfNo, short TalkNum, short ListenNum, short ConfParam);
MYLIBAPI short CONFDESTROY(short ChType, short ChIndex, short ConfNo, short DestroyParam);
MYLIBAPI short CONFJOIN(short ChType, short ChIndex, short ConfNo, short JoinParam);
MYLIBAPI short CONFUNJOIN(short ChType, short ChIndex, short ConfNo, short UnjoinParam);
MYLIBAPI short CONFLISTEN(short ChType, short ChIndex, short ConfNo, short ListenParam);
MYLIBAPI short CONFUNLISTEN(short ChType, short ChIndex, short ConfNo, short UnlistenParam);

MYLIBAPI void IvrDrvLoop();

MYLIBAPI void SetFuncOnFireCALLIN(void (*onFireCALLIN)(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled));
MYLIBAPI void SetFuncOnFireRECVSAM(void (*onFireRECVSAM)(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called));
MYLIBAPI void SetFuncOnFireRECVACM(void (*onFireRECVACM)(short ChType, short ChIndex, short AcmParam));
MYLIBAPI void SetFuncOnFireRECVACK(void (*onFireRECVACK)(short ChType, short ChIndex, short AckParam));
MYLIBAPI void SetFuncOnFireRELEASE(void (*onFireRELEASE)(short ChType, short ChIndex, short ReleaseType, short ReleaseReason));
MYLIBAPI void SetFuncOnFirePLAYRESULT(void (*onFirePLAYRESULT)(short ChType, short ChIndex, short Result));
MYLIBAPI void SetFuncOnFireRECORDRESULT(void (*onFireRECORDRESULT)(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen));
MYLIBAPI void SetFuncOnFireRECVDTMF(void (*onFireRECVDTMF)(short ChType, short ChIndex, LPCTSTR Dtmfs));
MYLIBAPI void SetFuncOnFireCONFPLAYRESULT(void (*onFireCONFPLAYRESULT)(short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFRECORDRESULT(void (*onFireCONFRECORDRESULT)(short ConfNo, short Result, long RecdSize, long RecdLen));
MYLIBAPI void SetFuncOnFireSENDFAXRESULT(void (*onFireSENDFAXRESULT)(short ChType, short ChIndex, short FaxChIndex, short Result, short Param));
MYLIBAPI void SetFuncOnFireRECVFAXRESULT(void (*onFireRECVFAXRESULT)(short ChType, short ChIndex, short FaxChIndex, short Result, short Param));
MYLIBAPI void SetFuncOnFireFLASH(void (*onFireFLASH)(short ChType, short ChIndex));
MYLIBAPI void SetFuncOnFireLINEALARM(void (*onFireLINEALARM)(short ChType, short ChIndex, short AlarmType, short AlarmParam));
MYLIBAPI void SetFuncOnFireRECORDMEMRESULT(void (*onFireRECORDMEMRESULT)(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize));
MYLIBAPI void SetFuncOnFireCONFCREATERESULT(void (*onFireCONFCREATERESULT)(short ChType, short ChIndex, short ConfNo, short CreateId, short Result));
MYLIBAPI void SetFuncOnFireCONFDESTROYRESULT(void (*onFireCONFDESTROYRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFJOINRESULT(void (*onFireCONFJOINRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFLISTENRESULT(void (*onFireCONFLISTENRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFUNJOINRESULT(void (*onFireCONFUNJOINRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFUNLISTENRESULT(void (*onFireCONFUNLISTENRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCHECKTONERESULT(void (*onFireCHECKTONERESULT)(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result));
MYLIBAPI void SetFuncOnFireSENDFSKRESULT(void (*onFireSENDFSKRESULT)(short ChType, short ChIndex, short FskChIndex, short Result, short Param));
MYLIBAPI void SetFuncOnFireRECVFSK(void (*onFireRECVFSK)(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param));
//-----------------------------------------------------------------------------
#endif
