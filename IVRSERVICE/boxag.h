//---------------------------------------------------------------------------
#ifndef __BOXAG_H_
#define __BOXAG_H_
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//坐席話務統計參數
class CAgentCallCountParam
{
public:
  int CallInWaitOverTimeLen; //呼入超時應答時長
  int CallOutWaitOverTimeLen; //呼出超時應答時長
  int CallTalkOverTimeLen; //通話超時時長
  int CallACWOverTimeLen; //話后處理超時時長
  
  int LogoutTimeSumLen; //累計未登錄時長

  int LoginTimeSumLen; //累計登錄時長
  int BusyCount; //累計示忙次數
  int BusyTimeSumLen; //累計示忙時長
  int BusyTimeAvgLen; //平均示忙時長
  int LeaveCount; //累計離席次數
  int LeaveTimeSumLen; //累計離席時長
  int LeaveTimeAvgLen; //平均離席時長
  int IdelTimeSumLen; //累計示閑時長
  
  int CallInCount; //來話分配總次數
  int CallInAnsCount; //來話應答總次數
  int CallInNoAnsCount; //來話未應答總次數
  int CallInAbandonCount; //來話放棄總次數
  
  int CallInWaitTimeSumLen; //呼入累計等待時長
  int CallInWaitTimeMaxLen; //呼入最長等待時長
  int CallInWaitTimeMinLen; //呼入最短等待時長
  int CallInWaitTimeAvgLen; //呼入平均等待時長
  int CallInWaitOverTimeCount; //呼入應答超時總次數
  
  int CallInAbandonTimeSumLen; //呼入累計放棄前等待時長
  int CallInAbandonTimeMaxLen; //呼入放棄前最長等待時長
  int CallInAbandonTimeMinLen; //呼入放棄前最短等待時長
  int CallInAbandonTimeAvgLen; //呼入放棄前平均等待時長
  
  int CallInTalkTimeLen; //呼入通話時長
  int CallInTalkTimeSumLen; //呼入累計通話時長
  int CallInTalkTimeMaxLen; //呼入最長通話時長
  int CallInTalkTimeMinLen; //呼入最短通話時長
  int CallInTalkTimeAvgLen; //呼入平均通話時長
  int CallInTalkOverTimeCount; //呼入通話超時總次數
  
  int CallInACWTimeLen; //呼入話后處理時長
  int CallInACWTimeSumLen; //呼入累計話后處理時長
  int CallInACWTimeMaxLen; //呼入最長話后處理時長
  int CallInACWTimeMinLen; //呼入最短話后處理時長
  int CallInACWTimeAvgLen; //呼入平均話后處理時長
  int CallInACWOverTimeCount; //呼入話后處理超時總次數
  
  int CallInHandleTimeLen; //呼入話務處理時長
  int CallInHandleTimeSumLen; //呼入累計話務處理時長
  int CallInHandleTimeMaxLen; //呼入最長話務處理時長
  int CallInHandleTimeMinLen; //呼入最短話務處理時長
  int CallInHandleTimeAvgLen; //呼入平均話務處理時長
  
  int CallInAbandonRate; //來話放棄率
  int CallInAnswerRate; //來話接通率
  
  int CallOutCount; //呼出總次數
  int CallOutAnsCount; //呼出應答總次數
  int CallOutNoAnsCount; //呼出未應答總次數
  
  int CallOutWaitTimeLen; //呼出等待時長
  int CallOutWaitTimeSumLen; //呼出累計等待時長
  int CallOutWaitTimeMaxLen; //呼出最長等待時長
  int CallOutWaitTimeMinLen; //呼出最短等待時長
  int CallOutWaitTimeAvgLen; //呼出平均等待時長
  int CallOutWaitOverTimeCount; //呼出應答超時總次數
  
  int CallOutTalkTimeLen; //呼出通話時長
  int CallOutTalkTimeSumLen; //呼出累計通話時長
  int CallOutTalkTimeMaxLen; //呼出最長通話時長
  int CallOutTalkTimeMinLen; //呼出最短通話時長
  int CallOutTalkTimeAvgLen; //呼出平均通話時長
  int CallOutTalkOverTimeCount; //呼出通話超時總次數
  
  int CallOutACWTimeLen; //呼出話后處理時長
  int CallOutACWTimeSumLen; //呼出累計話后處理時長
  int CallOutACWTimeMaxLen; //呼出最長話后處理時長
  int CallOutACWTimeMinLen; //呼出最短話后處理時長
  int CallOutACWTimeAvgLen; //呼出平均話后處理時長
  int CallOutACWOverTimeCount; //呼出話后處理超時總次數
  
  int CallOutHandleTimeLen; //呼出話務處理時長
  int CallOutHandleTimeSumLen; //呼出累計話務處理時長
  int CallOutHandleTimeMaxLen; //呼出最長話務處理時長
  int CallOutHandleTimeMinLen; //呼出最短話務處理時長
  int CallOutHandleTimeAvgLen; //呼出平均話務處理時長

  int CallOutFailRate; //呼出失敗率
  int CallOutAnswerRate; //呼出接通率
  
  int CallTalkOverTimeCount; //通話超時總次數
  int CallTalkTimeSumLen; //累計通話總時長
  int CallAnsCount; //累計通話總次數

  int CallACWOverTimeCount; //話后處理超時總次數
  int CallACWTimeSumLen; //累計話后處理總時長
  
  int CallHandleTimeSumLen; //累計話務處理時長
  int CallHandleTimeMaxLen; //最長話務處理時長
  int CallHandleTimeMinLen; //最短話務處理時長
  int CallHandleTimeAvgLen; //平均話務處理時長

  int HoldCallCount; //保持總次數
  int HoldCallTimeSumLen; //累計保持時長
  int HoldCallTimeMaxLen; //最長保持時長
  int HoldCallTimeMinLen; //最短保持時長
  int HoldCallTimeAvgLen; //平均保持時長

  int TranCallCount; //轉接總次數

  int CallInOut; //呼入呼出標志
  int RingID;

  int OldStatus; //之前狀態
  time_t OldStatusStartTime; //之前狀態開始時間
  int OldStatusDuration; //之前狀態持續時長

  int CurStatus; //當前狀態
  //AG_STATUS_LOGOUT		        	0	//未登錄
  //AG_STATUS_LOGIN		        	  1	//登錄
  //AG_STATUS_BUSY		        	  2	//示忙態
  //AG_STATUS_IDEL		            3	//空閑態
  //AG_STATUS_INRING		      	  4	//呼入振鈴
  //AG_STATUS_OUTSEIZE 	      	  5	//呼出占用
  //AG_STATUS_OUTRING		      	  6	//呼出振鈴
  //AG_STATUS_TALK		      		  7	//說話狀態
  //AG_STATUS_HOLD		      		  8	//保持狀態
  //AG_STATUS_ACW 							  9 //話后處理
  //AG_STATUS_LEAVE 							10 //離席小休態
  //AG_STATUS_ABAND 							11 //來話放棄
  //AG_STATUS_IVR 				        12 //占用IVR
  //AG_STATUS_CONSULT			        13 //咨詢狀態
  //AG_STATUS_CONF			          14 //會議狀態
  //AG_STATUS_CONNECT		          15 //電話持續接通時長
  //AG_STATUS_HANDLECALL          16 //整個呼叫處理時長

  time_t LoginTime; //登錄時間
  char LoginGID[MAX_CHAR64_LEN];
  char CRDSerialNo[MAX_CHAR64_LEN];

  time_t LogoutTime; //登出時間
  char LogoutGID[MAX_CHAR64_LEN];

  time_t StartTalkTime; //開始通話時間
  int  CallConnectedTimeLen; //本次電話接通時長
  int  CallHandleTimeLen; //本次電話處理時長

  time_t CurStatusStartTime; //當前狀態開始時間
  time_t CurStatusEndTime; //當前狀態結束時間

public:
  void Reset();
  void Init();

  CAgentCallCountParam();
  virtual ~CAgentCallCountParam();

  //設置呼叫工單號
  void SetCDRSerialNo(const char *pszSerialNo);
  //改變狀態
  int ChangeStatus(int nStatus);
  //累計來話次數
  void IncCallInCount();
  //累計放棄來話次數
  void IncCallInAbandonCount();
  //累計來話應答次數
  void IncCallInAnsCount();
  //累計呼出次數
  void IncCallOutCount();
  //累計呼出應答次數
  void IncCallOutAnsCount();

  //累計轉接次數
  void IncTranCallCount();
};

//話務組話務統計參數
class CGroupCallCountParam
{
public:
  int CallInWaitOverTimeLen; //呼入超時應答時長
  int CallOutWaitOverTimeLen; //呼出超時應答時長
  int CallTalkOverTimeLen; //通話超時時長
  int CallACWOverTimeLen; //話后處理超時時長
  
  int ACDCount; //ACD分配總次數
  int ACDFailCount; //ACD分配失敗總次數

  int CallInCount; //來話分配總次數
  int CallInAnsCount; //來話應答總次數
  int CallInNoAnsCount; //來話未應答總次數
  int CallInAbandonCount; //來話放棄總次數
  
  int CallInWaitTimeSumLen; //呼入累計等待時長
  int CallInWaitTimeMaxLen; //呼入最長等待時長
  int CallInWaitTimeMinLen; //呼入最短等待時長
  int CallInWaitTimeAvgLen; //呼入平均等待時長
  int CallInWaitOverTimeCount; //呼入應答超時總次數
  
  int CallInAbandonTimeSumLen; //呼入累計放棄前等待時長
  int CallInAbandonTimeMaxLen; //呼入放棄前最長等待時長
  int CallInAbandonTimeMinLen; //呼入放棄前最短等待時長
  int CallInAbandonTimeAvgLen; //呼入放棄前平均等待時長

  int CallInTalkTimeLen; //呼入通話時長
  int CallInTalkTimeSumLen; //呼入累計通話時長
  int CallInTalkTimeMaxLen; //呼入最長通話時長
  int CallInTalkTimeMinLen; //呼入最短通話時長
  int CallInTalkTimeAvgLen; //呼入平均通話時長
  int CallInTalkOverTimeCount; //呼入通話超時總次數
  
  int CallInACWTimeLen; //呼入話后處理時長
  int CallInACWTimeSumLen; //呼入累計話后處理時長
  int CallInACWTimeMaxLen; //呼入最長話后處理時長
  int CallInACWTimeMinLen; //呼入最短話后處理時長
  int CallInACWTimeAvgLen; //呼入平均話后處理時長
  int CallInACWOverTimeCount; //呼入話后處理超時總次數
  
  int CallInHandleTimeLen; //呼入話務處理時長
  int CallInHandleTimeSumLen; //呼入累計話務處理時長
  int CallInHandleTimeMaxLen; //呼入最長話務處理時長
  int CallInHandleTimeMinLen; //呼入最短話務處理時長
  int CallInHandleTimeAvgLen; //呼入平均話務處理時長
  
  int CallInAbandonRate; //來話放棄率
  int CallInAnswerRate; //來話接通率
  
  int CallOutCount; //呼出總次數
  int CallOutAnsCount; //呼出應答總次數
  int CallOutNoAnsCount; //呼出未應答總次數
  
  int CallOutWaitTimeSumLen; //呼出累計等待時長
  int CallOutWaitTimeMaxLen; //呼出最長等待時長
  int CallOutWaitTimeMinLen; //呼出最短等待時長
  int CallOutWaitTimeAvgLen; //呼出平均等待時長
  int CallOutWaitOverTimeCount; //呼出應答超時總次數
  
  int CallOutTalkTimeLen; //呼出通話時長
  int CallOutTalkTimeSumLen; //呼出累計通話時長
  int CallOutTalkTimeMaxLen; //呼出最長通話時長
  int CallOutTalkTimeMinLen; //呼出最短通話時長
  int CallOutTalkTimeAvgLen; //呼出平均通話時長
  int CallOutTalkOverTimeCount; //呼出通話超時總次數
  
  int CallOutACWTimeLen; //呼出話后處理時長
  int CallOutACWTimeSumLen; //呼出累計話后處理時長
  int CallOutACWTimeMaxLen; //呼出最長話后處理時長
  int CallOutACWTimeMinLen; //呼出最短話后處理時長
  int CallOutACWTimeAvgLen; //呼出平均話后處理時長
  int CallOutACWOverTimeCount; //呼出話后處理超時總次數
  
  int CallOutHandleTimeLen; //呼出話務處理時長
  int CallOutHandleTimeSumLen; //呼出累計話務處理時長
  int CallOutHandleTimeMaxLen; //呼出最長話務處理時長
  int CallOutHandleTimeMinLen; //呼出最短話務處理時長
  int CallOutHandleTimeAvgLen; //呼出平均話務處理時長

  int CallOutFailRate; //呼出失敗率
  int CallOutAnswerRate; //呼出接通率
  
  int CallTalkOverTimeCount; //通話超時總次數
  int CallTalkTimeSumLen; //累計通話總時長
  int CallAnsCount; //累計通話總次數

  int CallACWOverTimeCount; //話后處理超時總次數
  int CallACWTimeSumLen; //累計話后處理總時長
  
  int CallHandleTimeSumLen; //累計話務處理時長
  int CallHandleTimeMaxLen; //最長話務處理時長
  int CallHandleTimeMinLen; //最短話務處理時長
  int CallHandleTimeAvgLen; //平均話務處理時長

  int HoldCallCount; //保持總次數
  int HoldCallTimeSumLen; //累計保持時長
  int HoldCallTimeMaxLen; //最長保持時長
  int HoldCallTimeMinLen; //最短保持時長
  int HoldCallTimeAvgLen; //平均保持時長

  int TranCallCount; //轉接總次數

  int LogOutCount; //未登錄話務員數
  int LoginWorkerCount; //已登錄話務員數
  int IdelWorkerCount; //示閑話務員數
  int BusyWorkerCount; //示忙話務員數
  int LeaveWorkerCount; //離席話務員數
  int SetBusyWorkerCount; //設定示忙話務員數
  int SetLeaveWorkerCount; //設定離席小休話務員數

  int CallInRingCount; //呼入振鈴數
  int CallInTalkCount; //呼入通話數
  
  int CallOutRingCount; //正在呼出數
  int CallOutTalkCount; //呼出通話數

  int CallTalkCount; //正在通話數
  int CallACWCount; //正在話后處理數
  int CallHoldCount; //正在保持數

  int CallUseCount; //正在使用數，包含正在電話和話后處理

  int IvrChnTotalCount; //IVR通道總數數
  int IvrChnBusyCount; //IVR占用數
  int IvrChnIdelCount; //IVR空閑數
  int IvrChnBlockCount; //IVR阻塞數

  int LoginWorkerCountB; //已登錄備用話務員數
  int IdelWorkerCountB; //備用示閑話務員數

public:
  void Reset();
  void Init();
  void ResetStatusCount();

  CGroupCallCountParam();
  virtual ~CGroupCallCountParam();

  //累計來話次數
  void IncCallInCount();
  //累計放棄來話次數
  void IncCallInAbandonCount(int nTimeLen);
  //累計來話應答次數
  void IncCallInAnsCount();
  //累計呼出次數
  void IncCallOutCount();
  //累計呼出應答次數
  void IncCallOutAnsCount();
  //累計轉接次數
  void IncTranCallCount();

  //累計來話振鈴時長
  void AddCallInWaitTimeLen(int nTimeLen);
  //累計來話通話時長
  void AddCallInTalkTimeLen(int nTimeLen);
  //累計來話保持時長
  void AddCallInHoldTimeLen(int nTimeLen);
  //累計來話話后處理時長
  void AddCallInACWTimeLen(int nTimeLen);
  //累計來話話務處理時長
  void AddCallInHandleTimeLen(int nTimeLen);
  
  //累計呼出振鈴時長
  void AddCallOutWaitTimeLen(int nTimeLen);
  //累計呼出通話時長
  void AddCallOutTalkTimeLen(int nTimeLen);
  //累計呼出保持時長
  void AddCallOutHoldTimeLen(int nTimeLen);
  //累計呼出話后處理時長
  void AddCallOutACWTimeLen(int nTimeLen);
  //累計呼出話務處理時長
  void AddCallOutHandleTimeLen(int nTimeLen);
};


//話務員類
class CWorker
{
public:
  bool LoginId; //話務員登錄標志(false-未登錄 true-登錄成功)
  time_t LoginTime; //登錄時間
  time_t LogoutTime; //登出時間
  time_t IdelTime; //變為空閑時間，用于增加閑置時間最長的優先分配的ACD規則 //2016-02-12
  UC TelLogInOutFlag; //通過電話登錄登出標志：0-否，1-登錄，2-登出
  
  UC DisturbId; //可打擾(0)/免打擾(>0 1-免打擾 2-等待IVR返回 3-外出值班模式，即呼叫綁定的外線號碼)
  UC OldDisturbId; //可打擾(0)/免打擾(>0)
  time_t DisturbTime; //設置(免打擾/打擾)時間
  short NoAnswerCount; //連續未應答的來話次數
  UC PreDisturbId; //保留退出前的免打擾狀態值 2016-07-04

  UC CallDirection; //呼叫服務模式 0-呼入呼出 1-呼入 2-呼出
  
  UC DutyNo; //輪班班次
  UC AutoRecordId; //錄音標志

  UC LeaveId; //在席(0)/離席(>0)標志
  UC OldLeaveId; //在席(0)/離席(>0)標志
  time_t LeaveTime; //設置(在席/離席)時間
  CStringX LeaveReason; //離席原因 
  char UnicodeLeaveReason[128];
  
  US PreWorkerNo; //登錄前的話務員工號,用來判斷是否需要清除實時話務統計數據
  UC PreGroupNo; //退出前登錄的組號
  UC PreGroupNoList[MAX_GROUP_NUM_FORWORKER];
  US PreDutyNo;
  US WorkerNo; //話務員工號（1-65535 0表示未登錄）
  CStringX WorkerName; //話務員姓名
  CStringX AccountNo; //登錄賬號
  CStringX DepartmentNo; //部門編號
  char UnicodeWorkerName[128];
  
  UC WorkerGrade; //話務席類別(1-普通話務員席、2-班長席、3-管理員席)
  UC GroupNo[MAX_GROUP_NUM_FORWORKER]; //業務組號(1-31 0表示不按組號分配)
  UC Level[MAX_GROUP_NUM_FORWORKER]; //話務席級別(1-15 0表示不按級別分配)
  UC AcdedGroupNo; //分配了的組號
  CStringX DutyPhone; //外出值班綁定的電話號碼

  UC oldWorkerAutoLoginID; //系統啟動時是否自動登錄INI里設定的默認話務員
  US oldWorkerNo; //系統啟動時的話務員工號
  CStringX oldWorkerName; //系統啟動時的話務員姓名
  UC oldWorkerGrade; //系統啟動時的話務席類別(1-普通話務員席、2-班長席、3-管理員席)
  UC oldGroupNo[MAX_GROUP_NUM_FORWORKER]; //系統啟動時的業務組號(1-31 0表示不按組號分配)
  UC oldLevel[MAX_GROUP_NUM_FORWORKER]; //系統啟動時的話務席級別(1-15 0表示不按級別分配)
  
  US DelayTime; //延時分配話務時間(秒)
  UC GetAGState; //實時取座席狀態標志
  UC GetChState; //實時取通道狀態標志
  UC GetWaitQueue; //實時取ACD等待隊列標志
  UC GetConfState; //實時取會議狀態標志

  CAgentCallCountParam WorkerCallCountParam;

public:
  void Init()
  {
    LoginId = false;
    LoginTime = 0;
    LogoutTime = 0;
    IdelTime = time(0); //2016-02-12
    TelLogInOutFlag = 0;

    DisturbId = 0;
    OldDisturbId = 0;
    DisturbTime = 0;
    NoAnswerCount = 0;
    PreDisturbId = 0; //2016-07-04

    CallDirection = 0;

    DutyNo = 0;
    AutoRecordId = 1;

    LeaveId = 0;
    OldLeaveId = 0;
    LeaveTime = 0;
    LeaveReason.Empty();
    memset(UnicodeLeaveReason, 0, 128);
    
    PreWorkerNo = 0;
    PreGroupNo = 0;
    PreDutyNo = 0;
    WorkerNo = 0;
    WorkerName.Empty();
    AccountNo.Empty();
    DepartmentNo.Empty();
    memset(UnicodeWorkerName, 0, 128);

    oldWorkerAutoLoginID = 1;
    WorkerGrade = 0;
    oldWorkerNo = 0;
    oldWorkerName.Empty();
    oldWorkerGrade = 0;
    for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
    {
      GroupNo[i] = 0;
      Level[i] = 0;
      oldGroupNo[i] = 0;
      oldLevel[i] = 0;
    }
    AcdedGroupNo = 0;
    DutyPhone.Empty();

    DelayTime = 3;
    GetAGState = 0;
    GetChState = 0;
    GetWaitQueue = 0;
    GetConfState = 0;
  }

  CWorker() {Init();}
  virtual ~CWorker(){}

  void ClearWorkerParam()
  {
    WorkerNo = 0;
    WorkerName.Empty();
    WorkerGrade = 0;
    for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
    {
      GroupNo[i] = 0;
      Level[i] = 0;
    }
  }

  //保存座席配置文件配置的初始話務員工號及姓名信息
  void SetOldWorkerParam(int workerno, const char *workername, int workergrade)
  {
    oldWorkerNo = (US)workerno; 
    oldWorkerName = workername; 
    oldWorkerGrade = (UC)workergrade;
  }
  //保存座席配置文件配置的初始話務員組號及技能級別信息
  void SetOldWorkerGroupNo(int groupno, int level)
  {
    for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
    {
      if (oldGroupNo[i] == 0)
      {
        oldGroupNo[i] = (UC)groupno;
        oldLevel[i] = (UC)level;
        break;
      }
    }
  }
  //設置話務員登錄時的工號及姓名信息
  void WorkerLogin(int workerno, const char *workername, int workergrade)
  {
    WorkerNo = (US)workerno; 
    WorkerName = workername; 
    WorkerGrade = (UC)workergrade;
    
    LoginTime = time(0); 
    LoginId =  true;
    NoAnswerCount = 0;
  }
  void WorkerLogin(int workerno)
  {
    WorkerNo = (US)workerno; 
    
    LoginTime = time(0); 
    LoginId =  true;
    NoAnswerCount = 0;
  }
  //設置話務員登錄時的組號及技能級別信息
  void SetWorkerGroupNo(int groupno, int level)
  {
    for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
    {
      if (GroupNo[i] == 0)
      {
        GroupNo[i] = (UC)groupno;
        Level[i] = (UC)level;
        break;
      }
    }
  }
  //登錄INI文件中設置自的話務員
  int LoginIniWorker()
  {
    if (oldWorkerNo != 0)
    {
      LoginId =  true;
      LoginTime = time(0);
      NoAnswerCount = 0;
      WorkerNo = oldWorkerNo;
      WorkerName = oldWorkerName;
      WorkerGrade = oldWorkerGrade;
      DisturbId = 0;
      LeaveId = 0;
      for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
      {
        GroupNo[i] = oldGroupNo[i];
        Level[i] = oldLevel[i];
      }
      return 0;
    }
    else
    {
      Logout();
      ClearState();
      return 1;
    }
  }
  //恢復原話務員參數
  int OldWorkerLogin()
  {
    if (oldWorkerAutoLoginID == 1 && oldWorkerNo != 0)
    {
      LoginId =  true;
      WorkerNo = oldWorkerNo;
      WorkerName = oldWorkerName;
      WorkerGrade = oldWorkerGrade;
      DisturbId = 0;
      LeaveId = 0;
      for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
      {
        GroupNo[i] = oldGroupNo[i];
        Level[i] = oldLevel[i];
      }
      return 0;
    }
    else
    {
      Logout();
      ClearState();
      ClearWorkerParam();
      return 1;
    }
  }
  //話務員退出
  void Logout()
  {
    LoginId = false;
    DisturbId = 0;
    LeaveId = 0;
    NoAnswerCount = 0;
    LogoutTime = time(0);
    ClearWorkerParam();
  }
  //取員登錄狀態
  bool isLogin() 
  {
    return LoginId;
  }

  //免打擾設置
  void SetDisturb(int disturb) 
  {
    DisturbId = (UC)disturb;
    OldDisturbId = DisturbId;
    DisturbTime = time(0);
    if (disturb == 0)
      NoAnswerCount = 0;
  }
  //取免打擾狀態
  bool isDisturb() 
  {
    return (DisturbId == 0 || DisturbId == 3) ? false : true;
  }

  //離席設置
  void SetGoOutDutyTel(int flag, const CStringX dutytel) 
  {
    if (flag == 0)
    {
      DisturbId = 0;
      DutyPhone.Empty();
    }
    else
    {
      DisturbId = 3;
      DutyPhone = dutytel;
    }
  }
  void SetGoOutDutyTel(int flag, const char *dutytel) 
  {
    if (flag == 0)
    {
      DisturbId = 0;
      DutyPhone.Empty();
    }
    else
    {
      DisturbId = 3;
      DutyPhone = dutytel;
    }
  }
  //離席設置
  void SetLeave(int leave, const CStringX reason) 
  {
    LeaveId = (UC)leave;
    OldLeaveId = LeaveId;
    LeaveReason = reason;
    LeaveTime = time(0);
    if (leave == 0)
      NoAnswerCount = 0;
  }
  //取在席狀態
  bool isLeave() 
  {
    return (LeaveId != 0) ? true : false;
  }
  //是否為可分配話務狀態
  bool isIdelForACD()
  {
    return (isLogin() && !isDisturb() && !isLeave() && CallDirection!=2) ? true : false;
  }

  //是需要的工號嗎
  bool isWantWorkerNo(int workerno) 
  {
    return (WorkerNo == (US)workerno) ? true : false;
  }
  //是需要的組號嗎
  bool isGroupNo(int groupno, int &index)
  {
    index = -1;
    if (groupno == 0)
    {
      index = 0;
      return true;
    }
    for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i++)
    {
      if (GroupNo[i] == (UC)groupno)
      {
        index = i;
      }
    }
    if (groupno == 0 || index >= 0)
    {
      if (index < 0)
        index = 0;
      return true;
    }
    return false;
  }

  bool isWantGroup(int groupno, int level, int levelrule)
  {
    int nTemp;
    //if (groupno == 0)
    //  return true;
    if (level == 0 || levelrule == 0)
      return (isGroupNo(groupno, nTemp)) ? true : false;
    if (levelrule == 1)
      return ((isGroupNo(groupno, nTemp)) && Level[nTemp] == (UC)level) ? true : false;
    else if (levelrule == 2)
      return ((isGroupNo(groupno, nTemp)) && Level[nTemp] >= (UC)level) ? true : false;
    else if (levelrule == 3)
      return ((isGroupNo(groupno, nTemp)) && Level[nTemp] <= (UC)level) ? true : false;
    else
      return (isGroupNo(groupno, nTemp)) ? true : false;
  }

  bool isGroupNo(int groupno, int groupindex, int &index)
  {
    index = -1;
    if (groupno == 0)
    {
      index = 0;
      return true;
    }
    if (GroupNo[groupindex] == (UC)groupno)
    {
      index = groupindex;
    }
    if (groupno == 0 || index >= 0)
    {
      if (index < 0)
        index = 0;
      return true;
    }
    return false;
  }
  bool isWantGroup(int groupno, int level, int levelrule, int groupindex)
  {
    int nTemp;
    //if (groupno == 0)
    //  return true;
    if (level == 0 || levelrule == 0)
      return (isGroupNo(groupno, groupindex, nTemp)) ? true : false;
    if (levelrule == 1)
      return ((isGroupNo(groupno, groupindex, nTemp)) && Level[nTemp] == (UC)level) ? true : false;
    else if (levelrule == 2)
      return ((isGroupNo(groupno, groupindex, nTemp)) && Level[nTemp] >= (UC)level) ? true : false;
    else if (levelrule == 3)
      return ((isGroupNo(groupno, groupindex, nTemp)) && Level[nTemp] <= (UC)level) ? true : false;
    else
      return (isGroupNo(groupno, groupindex, nTemp)) ? true : false;
  }
  char *GetGroupNoList()
  {
    char szTemp[16];
    static char szGroupNoList[128];
    memset(szGroupNoList, 0, 128);
    sprintf(szGroupNoList, "%d", GroupNo[0]);
    for (int i=1; i<MAX_GROUP_NUM_FORWORKER; i++)
    {
      if (GroupNo[i] > 0)
      {
        sprintf(szTemp, ",%d", GroupNo[i]);
        strcat(szGroupNoList, szTemp);
      }
    }
    return szGroupNoList;
  }
  //清除累計的話務量
  void ClearWork()
  {
    DelayTime = 3;
    GetAGState = 0;
    GetChState = 0;
    GetWaitQueue = 0;
    GetConfState = 0;

    WorkerCallCountParam.Reset();
  }
  void ClearState()
  {
    DelayTime = 3;
    GetAGState = 0;
    GetChState = 0;
    GetWaitQueue = 0;
    GetConfState = 0;
  }

};

//坐席硬件類
class CSeat
{
public:
  US ClientId; //電腦坐席網絡客戶端編號(0x06NN 06-坐席節點 NN-節點序號，為0x06FF表示未連接電腦)
  UC TcpLinkId; //電腦網絡連接標志(0-未連接或斷開 1-網絡連接成功)

  char WSClientId[64]; //web坐席客戶端id
  
  UC LoginId; //坐席登錄標志(0-未登錄 1-登錄成功)
  time_t LogTime; //登錄時間

  LANGID SeatLangID; //坐席語言類型

  UC LoginSwitchID; //坐席是否登錄到交換機
  UC AutoRecordId;
  char LoginSwitchPSW[32]; //坐席登錄到交換機的密碼
  char LoginSwitchGroupID[32]; //坐席登錄到交換機的組號
  
  UC SeatType; //坐席類型(固定端口坐席: 1-內線電話座席 2-內線電腦座席 3-數字線電話座席 4-數字線電腦座席) (不固定端口坐席: 5-遠程電話座席,6-遠端IP座席)
  US PortNo; //對應的端口號
  UC GroupNo; //坐席組號，默認為0
  CStringX SeatNo; //坐席分機號
  CStringX SeatDIDNo; //坐席分機外線直撥號碼
  CStringX PhoneNo; //當坐席類型為內線時與座席號相同，否則為實際對應的外線號碼
  US RouteNo; //當為外線座席時呼出的路由號
  CStringX SeatIP; //座席登錄實際傳遞的ip
  CStringX BandIP; //座席客戶端綁定的ip
  CStringX IPPhoneBandIP; //ip電話綁定的ip
  CStringX DepartmentNo; //部門編號
  CStringX LocaAreaCode; //坐席分機所在地區號，針對多地部署的情況

  CStringX ForwardNo; //無條件呼叫轉移號碼
  CStringX BusyForwardNo; //遇忙呼叫轉移號碼
  CStringX NoAnsForwardNo; //久叫不應呼叫轉移號碼

  US nChn; //對應的通道號,坐席電話對應的通道狀態參考該通道的"信令狀態"
  char CustPhone[MAX_TELECODE_LEN]; //關聯的客戶電話號碼 //2014-01-13 針對IPO收不到主叫，而通過中繼監錄收主叫
  char IVRPhone[MAX_TELECODE_LEN]; //關聯的IVR號碼 //2014-07-14

  //2015-11-25 針對語音中繼的修改增加的參數
  UC ACDByVocTrkId; //通過語音中繼分配呼叫座席標志
  US nVocTrknChn; //通過語音中繼呼叫座席臨時綁定的語音中繼通道號
  US nCallInnChn; //通過語音中繼呼叫座席臨時綁定的呼入通道號
  US nTranAGnChn; //轉接的通道號
  //char CustPhone[MAX_TELECODE_LEN]; //關聯的客戶電話號碼
  CStringX DialParam; //關聯的附加參數

  CAgentCallCountParam SeatCallCountParam;

public:
  void Init()
  {
    ClientId = 0x06FF;
    TcpLinkId = 0;
    memset(WSClientId, 0, 64);

    LoginId = 0;
    LogTime = 0;
    
    SeatLangID = 1;
    AutoRecordId = 1;

    LoginSwitchID = 1;
    memset(LoginSwitchPSW, 0, 32);
    memset(LoginSwitchGroupID, 0, 32);
    
    SeatType = 0;
    PortNo = 0xFFFF;
    GroupNo = 0;
    SeatNo = "0";
    SeatDIDNo.Empty();
    PhoneNo = "";
    RouteNo = 0;
    SeatIP = "";
    BandIP = "";
    IPPhoneBandIP = "";
    DepartmentNo = "";
    LocaAreaCode = "";

    ForwardNo = "";
    BusyForwardNo = "";
    NoAnsForwardNo = "";
    nChn = 0xFFFF;
    memset(CustPhone, 0, MAX_TELECODE_LEN);
	  memset(IVRPhone, 0, MAX_TELECODE_LEN);
    
    //2015-11-25 針對語音中繼的修改增加的參數
    ACDByVocTrkId = 0;
    nVocTrknChn = 0xFFFF;
    nCallInnChn = 0xFFFF;
    nTranAGnChn = 0xFFFF;
    //memset(CustPhone, 0, MAX_TELECODE_LEN);
    DialParam = "";
  }
  CSeat() {Init();}
  virtual ~CSeat(){}

  //座席電腦網絡連接
  void TcpLinked(int clientid)
  {
    ClientId=(US)clientid;
    TcpLinkId=1;
  }
  //座席電腦網絡斷開
  void TcpUnLinked()
  {
    ClientId=0x06FF;
    TcpLinkId=0;
  }
  //座席電腦網絡是否已連接
  bool isTcpLinked() 
  {
    return (TcpLinkId==1) ? true : false;
  }
  
  //設置座席登錄標志
  void SeatLogin()
  {
    LoginId=1;
    LogTime = time(0);
  }
  //設置座席登出標志
  void SeatLogout()
  {
    LoginId=0;
    memset(WSClientId, 0, 64);
  }
  //坐席是否登錄
  bool isLogin() 
  {
    return (LoginId==1) ? true : false;
  }
  //是否為可服務狀態
  bool isIdelForACD()
  {
    return isLogin();
  }

  void SetSeatParam(int seattype, int groupno, const CStringX seatno, const CStringX phoneno, int routeno, const CStringX seatip, int nchn, int portno)
  {
    SeatType = seattype;
    GroupNo = groupno;
    SeatNo = seatno;
    PhoneNo = phoneno;
    RouteNo = routeno;
    SeatIP = seatip;
    nChn = nchn;
    PortNo = portno;
  }
  void SetSeatIP(const CStringX seatip)
  {
    SeatIP = seatip;
  }
  void SetBandIP(const CStringX bandip)
  {
    CStringX ArrString[2];

    int num = SplitString(bandip.C_Str(), ';', 2, ArrString);
    if (num == 0)
    {
      BandIP = "";
      IPPhoneBandIP = "";
    }
    else if (num == 1)
    {
      BandIP = bandip;
      IPPhoneBandIP = bandip;
    }
    else if (num == 2)
    {
      BandIP = ArrString[0];
      IPPhoneBandIP = ArrString[1];
    }
    else
    {
      BandIP = bandip;
      IPPhoneBandIP = bandip;
    }
  }
  //是需要的坐席號嗎
  bool isWantSeatNo(const char *seatno) 
  {
    if (strlen(seatno) == 0)
      return false;
    return (SeatNo.Compare(seatno) == 0 || SeatDIDNo.Compare(seatno) == 0) ? true : false;
  }
  bool isWantBandIP(const char *bandip) 
  {
    return (BandIP.Compare(bandip) == 0) ? true : false;
  }
  //是否是需要的坐席組
  bool isWantGroupNo(int groupno) 
  {
    return (groupno == 0 || groupno == GroupNo) ? true : false;
  }
  //是需要坐席外線的號碼
  bool isWantPhoneNo(const char *phoneno) 
  {
    return (PhoneNo.Compare(phoneno) == 0) ? true : false;
  }
  //是需要的坐席類型嗎
  bool isWantSeatType(int seattype) 
  {
    if (seattype == 0) //為0時表示不管坐席類型
      return true;
    return ((UC)seattype==SeatType) ? true : false;
  }
  //清除累計的話務量
  void ClearWork()
  {
    SeatCallCountParam.Reset();
  }
};
//被服務的線路類信息
class CSrvLine
{
public:
  unsigned int timer;
  UC lnState; //線路狀態: 0-無線路 1-有服務的線路 2-保持音樂 3-通話 4-會議通話 5-被監聽 6-靜音

  US nChn; //被服務的通道號
  UL SessionNo; //會話序列號
  UL CmdAddr; //當前指令行地址

public:
  void Init()
  {
    timer = 0;
    lnState = 0;
    nChn = 0xFFFF;
    SessionNo = 0;
    CmdAddr = 0;
  }
  CSrvLine() 
  {
    Init();
  }
  virtual ~CSrvLine(){}
};
//通用坐席類
class CAgent
{
public:
  unsigned int timer;
  unsigned int delaytimer; //ACD延時分配計時器

  US nAG; //座席邏輯編號
  CWorker m_Worker; //話務員
  CSeat m_Seat; //座席電話
  CSrvLine m_SrvLine[MAX_AG_SRVLINE_NUM]; //服務的線路

  UC svState; //座席當前服務狀態:
              //AG_SV_IDEL		        	0	//空閑
              //AG_SV_DELAY		        	1	//由忙轉空閑延時態
              //AG_SV_INSEIZE 	      	2	//呼入占用
              //AG_SV_OUTSEIZE 	      	3	//呼出占用
              //AG_SV_INRING		      	4	//呼入振鈴
              //AG_SV_OUTRING		      	5	//呼出振鈴
              //AG_SV_CONN		      		6	//電話接通狀態
              //AG_SV_WAIT_REL		      7	//等待坐席電話掛機
              //AG_SV_PLAY							8 //放音狀態
              //AG_SV_ACW 							9 //事后處理
              //AG_SV_HOLD 							10//保持狀態
              //AG_SV_WAIT_RETURN				11//等待IVR返回狀態（如：客戶密碼驗證）
  
  UC DelayState; //是否處于ACD延時分配狀態 0-否 1-事后處理狀態 2-掛機延時狀態
  UC CanTransferId; //允許轉接標志

  CH StateChangeTime[20]; //狀態改變時間
  UC ExtnType; //分機號碼類型：0-辦公電話，1-坐席分機

  US nQue; //呼出緩沖序號
  US nAcd; //ACD編號，用來拒絕應答 //2015-10-14 add
  US nCfc; //申請的會議編號(用于會議通話): 0-未申請 >1已申請的會議室編號

  CStringX AcceptSerialNo; //坐席電腦傳來的受理號
  CStringX AcceptParam; //坐席電腦傳來的附加參數

  CStringX FlwState; //業務流程傳遞過來的狀態信息
  CH CdrSerialNo[MAX_CHAR64_LEN];
  CH RecordFileName[MAX_CHAR128_LEN];
  CH IMId[MAX_CHAR64_LEN];
  
  CH ExtRecordServer[MAX_CHAR128_LEN]; //第3放錄音系統錄音服務器
  CH ExtRecordFileName[MAX_CHAR128_LEN]; //第3放錄音系統錄音文件名
  CH ExtRecordCallID[MAX_CHAR128_LEN]; //第3放錄音系統錄音callid

  UC TranAGId; //IVR轉接呼叫標志: 0-否 1-是IVR通過callseat指令轉接 2-是IVR通過transfer指令轉接
  US TrannChn; //發起轉接的通道號
  UC PickupId; //代接標志
  UC LockId; //臨時鎖定標志

  US TranDesnAG; //2015-11-08 人工轉接目的坐席邏輯編號，用來向轉接的目的坐席發送消息

  US ExternalId; //外線標志(用來判斷內線之間呼叫不累計呼叫次數)：0-內線，1-外線 //2015-12-26

  UC SendTrnIVRHangonFlag; //2016-07-19發送轉IVR掛機標志

public:  
  CAgent() 
  {
    timer = 0;
    delaytimer = 0;
    nAG = 0xFFFF;
    nQue = 0xFFFF;
    nAcd = 0xFFFF;
    nCfc = 0;
    svState = 0;
    DelayState = 0;
    CanTransferId = 0;
    memset(CdrSerialNo, 0, MAX_CHAR64_LEN);
    memset(RecordFileName, 0, MAX_CHAR128_LEN);
    memset(IMId, 0, MAX_CHAR64_LEN);
    memset(StateChangeTime, 0, 20);
    ExtnType = 0;

    memset(ExtRecordServer, 0, MAX_CHAR128_LEN);
    memset(ExtRecordFileName, 0, MAX_CHAR128_LEN);
    memset(ExtRecordCallID, 0, MAX_CHAR128_LEN);

    TranAGId = 0;
    TrannChn = 0xFFFF;
    PickupId = 0;
    LockId = 0;

    TranDesnAG = 0xFFFF; //2015-11-08 人工轉接目的坐席邏輯編號

    ExternalId = 1; //外線標志：0-內線，1-外線 2015-12-26

    SendTrnIVRHangonFlag = 0; //2016-07-19發送轉IVR掛機標志
  }
  virtual ~CAgent(){}

  //取話務員工號
  int GetWorkerNo()
  {
    return m_Worker.WorkerNo;
  }
  char *GetWorkerNoStr()
  {
    static char temp[16];
    sprintf(temp, "%d", m_Worker.WorkerNo);
    return temp;
  }
  //是需要的工號嗎
  bool isWantWorkerNo(int workerno) 
  {
    return m_Worker.isWantWorkerNo(workerno);
  }
  //取話務員姓名
  CStringX GetWorkerName() 
  {
    return m_Worker.WorkerName;
  }
  //取話務員應答總次數
  int GetWorkerAnsCount()
  {
    return m_Worker.WorkerCallCountParam.CallAnsCount;
  }
  //取話務員服務總時長
  int GetWorkerSrvTimeLen()
  {
    return m_Worker.WorkerCallCountParam.CallTalkTimeSumLen;
  }
  UC GetGroupNo()
  {
    return m_Worker.GroupNo[0];
  }
  UC GetAcdedGroupNo()
  {
    if (m_Worker.AcdedGroupNo == 0)
    {
      return m_Worker.GroupNo[0];
    }
    else
    {
      return m_Worker.AcdedGroupNo;
    }
  }
  UC GetSeatGroupNo()
  {
    return m_Seat.GroupNo;
  }
  UC GetLevel()
  {
    return m_Worker.Level[0];
  }

  //取坐席類型
  int GetSeatType() 
  {
    return m_Seat.SeatType;
  }
  //取坐席號
  CStringX GetSeatNo() 
  {
    return m_Seat.SeatNo;
  } 
  //取坐席對應的外線號碼
  CStringX GetPhoneNo() 
  {
    if (m_Worker.DisturbId == 3 && m_Worker.DutyPhone.GetLength() > 0)
      return m_Worker.DutyPhone;
    else
      return m_Seat.PhoneNo;
  } 
  //取遠端坐席呼出路由號
  US GetRouteNo()
  {
    return m_Seat.RouteNo;
  }
  //取坐席占用的通道序號
  US GetSeatnChn()
  {
    return m_Seat.nChn;
  }
  //是需要的坐席號嗎
  bool isWantSeatNo(const char *seatno) 
  {
    return m_Seat.isWantSeatNo(seatno);
  }
  bool isWantBandIP(const char *bandip) 
  {
    return m_Seat.isWantBandIP(bandip);
  }
  //是需要的坐席號嗎
  bool isWantPhoneNo(const char *phoneno) 
  {
    return m_Seat.isWantPhoneNo(phoneno);
  }
  //是需要的坐席類型嗎
  bool isWantSeatType(int seattype) 
  {
    return m_Seat.isWantSeatType(seattype);
  }
  bool isWantSeatGroupNo(int seatgroupno)
  {
    return m_Seat.isWantGroupNo(seatgroupno);
  }
  //座席電腦網絡是否已連接
  bool isTcpLinked() 
  {
    return m_Seat.isTcpLinked();
  }
  US GetClientId()
  {
    return m_Seat.ClientId;
  }

  //取座席應答總次數
  int GetSeatAnsCount()
  {
    return m_Seat.SeatCallCountParam.CallAnsCount;
  }
  //取座席服務總時長
  int GetSeatSrvTimeLen()
  {
    return m_Seat.SeatCallCountParam.CallTalkTimeSumLen;
  }

  bool isLogin()
  {
    return (m_Seat.isLogin() && m_Worker.isLogin());
  }
  //當前服務狀態是否空閑
  bool isIdelForSrv()
  {
    return (svState==0) ? true : false;
  }
  
  //是否為可分配話務狀態
  bool isIdelForACD(int seatgroupno=0)
  {
    if (seatgroupno == 0)
      return (isIdelForSrv() && DelayState == 0 && m_Worker.isIdelForACD() && m_Seat.isIdelForACD() && LockId == 0) ? true : false;
    else
      return (isIdelForSrv() && DelayState == 0) ? true : false; //如果按坐席組號分配就不判斷是否登錄
  }

  //是否為可外叫狀態
  bool isIdelForDialOut()
  {
    return (svState==0 || svState == AG_SV_HANGUP || svState == CHN_SNG_IN_ARRIVE || svState == CHN_SNG_IN_WAIT) ? true : false;
  }

  //是否是需要的座席
  bool isWantAgent(int seattype, int seatgroupno, int groupno, int level, int levelrule, int groupindex, bool bOnlyPC=false)
  {
    if (bOnlyPC == true && GetClientId() == 0x06FF)
      return false;
    if (groupindex == 0)
      return m_Seat.isWantSeatType(seattype) && ((seatgroupno > 0 && m_Seat.isWantGroupNo(seatgroupno)) || m_Worker.isWantGroup(groupno, level, levelrule));
    else
      return m_Seat.isWantSeatType(seattype) && ((seatgroupno > 0 && m_Seat.isWantGroupNo(seatgroupno)) || m_Worker.isWantGroup(groupno, level, levelrule, groupindex-1));
  }

  void SeatLogout()
  {
    m_Seat.TcpUnLinked();
    m_Seat.SeatLogout();
  }

  void WorkerLgout()
  {
    m_Worker.Logout();
  }

  //設置坐席服務狀態
  void SetAgentsvState(int svstate)
  {
    svState = (UC)svstate;
    if (svState == 0 || svState == AG_SV_CONN || svState == AG_SV_IMCHAT || svState == AG_SV_PROC_EMAIL || svState == AG_SV_PROC_FAX)
    {
      TranAGId = 0;
    }
    if (svState == 0)
    {
      memset(m_Seat.CustPhone, 0, MAX_TELECODE_LEN);
      TranDesnAG = 0xFFFF; //2015-11-08 人工轉接目的坐席邏輯編號
    }
    strcpy(StateChangeTime, MyGetNow());
    
    //2016-02-12
    if (svstate != AG_SV_IDEL)
      m_Worker.IdelTime = 0;
    else if (svstate == AG_SV_IDEL && m_Worker.IdelTime == 0)
      m_Worker.IdelTime = time(0);
  }
  bool isThesvState(int svstate)
  {
    return svState==svstate;
  }
  int GetsvState()
  {
    return svState;
  }

  int SetSrvLine(int nChn, UL sessionno, UL cmdaddr)
  {
    for (int i = 0; i < MAX_AG_SRVLINE_NUM; i ++)
    {
      if (m_SrvLine[i].lnState == 0)
      {
        m_SrvLine[i].nChn = nChn;
        m_SrvLine[i].SessionNo = sessionno;
        m_SrvLine[i].CmdAddr = cmdaddr;
        m_SrvLine[i].lnState = 1;
        return i;
      }
    }
    return -1;
  }
  void ClearSrvLine()
  {
    for (int i = 0; i < MAX_AG_SRVLINE_NUM; i ++)
    {
      if (m_SrvLine[i].lnState == 1)
      {
        m_SrvLine[i].nChn = 0xFFFF;
        m_SrvLine[i].lnState = 0;
      }
    }
    CanTransferId = 0;
  }
  int GetSrvnChn()
  {
    return m_SrvLine[0].nChn;
  }
  UL GetSrvSessionNo()
  {
    return m_SrvLine[0].SessionNo;
  }
  UL GetSrvCmdAddr()
  {
    return m_SrvLine[0].CmdAddr;
  }
  
  //改變狀態
  int ChangeStatus(int nStatus)
  {
    m_Seat.SeatCallCountParam.ChangeStatus(nStatus);
    return m_Worker.WorkerCallCountParam.ChangeStatus(nStatus);
    strcpy(StateChangeTime, MyGetNow());
  }
  //累計來話次數
  void IncCallInCount()
  {
    m_Seat.SeatCallCountParam.IncCallInCount();
    m_Worker.WorkerCallCountParam.IncCallInCount();
  }
  //累計放棄來話次數
  void IncCallInAbandonCount()
  {
    m_Seat.SeatCallCountParam.IncCallInAbandonCount();
    m_Worker.WorkerCallCountParam.IncCallInAbandonCount();
  }
  //累計來話應答次數
  void IncCallInAnsCount()
  {
    m_Seat.SeatCallCountParam.IncCallInAnsCount();
    m_Worker.WorkerCallCountParam.IncCallInAnsCount();
  }
  //累計呼出次數
  void IncCallOutCount()
  {
    m_Seat.SeatCallCountParam.IncCallOutCount();
    m_Worker.WorkerCallCountParam.IncCallOutCount();
  }
  //累計呼出應答次數
  void IncCallOutAnsCount()
  {
    m_Seat.SeatCallCountParam.IncCallOutAnsCount();
    m_Worker.WorkerCallCountParam.IncCallOutAnsCount();
  }
  //累計轉接次數
  void IncTranCallCount()
  {
    m_Seat.SeatCallCountParam.IncTranCallCount();
    m_Worker.WorkerCallCountParam.IncTranCallCount();
  }
  bool IsWantWorkerStatus(int nStatus)
  {
    return (m_Worker.WorkerCallCountParam.CurStatus == nStatus);
  }
  void SetCDRSerialNo(const char *pszSerialNo)
  {
    m_Worker.WorkerCallCountParam.SetCDRSerialNo(pszSerialNo);
  }
};

//話務組
class CWorkerGroup
{
public:
  int state; //是否有效 0-否 1-是
  UC GroupNo; //技能組號
  CStringX GroupName; //技能組名稱

  CGroupCallCountParam GroupCallCountParam;

public:
  void Init()
  {
    state = 0;
    GroupNo = 0;
    GroupName.Empty();
  }
  CWorkerGroup() {Init();}
  virtual ~CWorkerGroup(){}
};

class CWorkerGroupMng
{
public:
  int MaxGroupNum;
  CWorkerGroup *m_pWorkerGroup;

public:
  CWorkerGroupMng()
  {
    MaxGroupNum = 0;
    m_pWorkerGroup = NULL;
  }
  int InitData(int groupnum)
  {
    if (groupnum > 0)
    {
      m_pWorkerGroup = new CWorkerGroup[groupnum];
      if (m_pWorkerGroup)
      {
        MaxGroupNum = groupnum;
        return 0;
      }
    }
    return 1;
  }
  virtual ~CWorkerGroupMng()
  {
    if (m_pWorkerGroup != NULL)
    {
      delete []m_pWorkerGroup;
      MaxGroupNum = 0;
      m_pWorkerGroup = NULL;
    }
  }
};

//坐席管理類
class CAgentMng
{
public:
  int MaxUseNum; //實際設置的坐席數
  CAgent *m_Agent[MAX_AG_NUM]; //坐席指針數組（下標為座席邏輯編號，0開始）
  US m_ClientId[MAX_AG_NUM];  //座席邏輯編號對應的客戶端編號（下標為座席邏輯編號，0開始）

  int AgLoginCount; //登錄坐席數
  int AgLogoutCount; //登出坐席數
  int AgIdelCount; //空閑坐席數
  int AgBusyCount; //忙坐席數
  int AgLeavelCount; //示忙離席數
  int AgTalkCount; //通話離席數

  CGroupCallCountParam AllCallCountParam;
  CWorkerGroup WorkerGroup[MAX_AG_GROUP];
  short lastAllocnAG[MAX_AG_GROUP]; //組號對應的最后振鈴的坐席邏輯編號
  short lastAllocGroupIndexnAG[MAX_AG_GROUP][MAX_GROUP_NUM_FORWORKER]; //組號對應的最后振鈴的坐席邏輯編號

  short lastAnsnAG[MAX_AG_GROUP]; //組號對應的最后應答的坐席邏輯編號
  short lastAnsGroupIndexnAG[MAX_AG_GROUP][MAX_GROUP_NUM_FORWORKER]; //話務員屬于多群組是組號對應的最后應答的坐席邏輯編號
  
  short lastAllocLevelnAG[MAX_AG_GROUP][MAX_AG_LEVEL]; //技能級別對應的最后振鈴的坐席邏輯編號
  short lastAnsLevelnAG[MAX_AG_GROUP][MAX_AG_LEVEL]; //技能級別對應的最后應答的坐席邏輯編號

  short lastAllocLevelGroupIndexnAG[MAX_AG_GROUP][MAX_AG_LEVEL][MAX_GROUP_NUM_FORWORKER]; //技能級別對應的最后振鈴的坐席邏輯編號
  short lastAnsLevelGroupIndexnAG[MAX_AG_GROUP][MAX_AG_LEVEL][MAX_GROUP_NUM_FORWORKER]; //技能級別對應的最后應答的坐席邏輯編號

public:
  CAgentMng();
  virtual ~CAgentMng();

  void InitLastnAG();
  
  bool Alloc_nAG(int &nAG); //分配坐席內存
  void Free_nAG(int nAG); //釋放坐席內存
  bool isnAGAvail(int nAG);
  
  void SetClientId(int nAG, US clientid);
  void ClearClientId(int nAG);
  int  GetnAGByClientId(US clientid);
  bool isTheClientIdLogin(US clientid);
  
  int GetnAGByWorkerNo(int workerno);
  bool istheWorkerNoExist(int workerno);
  int GetnAGBySeatNo(const char *seatno);
  int GetnAGByWSClientId(const char *wsclientid);
  bool istheSeatNoExist(const char *seatno);
  bool istheBandIPExist(const char *bandip);
  int GetnAGByPhoneNo(const char *phoneno);
  int GetnAGByPort(int porttype, int portno);
  int GetnAGByAccountNo(const char *accountno);
  int GetnAGByWorkerName(const char *workername);
  int GetnAGByDutyNo(const char *dutyno);
  int GetnAGByIMId(const char *imid);

  char *GetGroupNoList(int nAG);

  void ResetAllStatusCountData();
  void ResetAllStatusCount();

  int CountLoginAgentByGroupNo(UC groupno);
  int CountIdleAgentByGroupNo(UC groupno);
  int CountTalkAgentByGroupNo(UC groupno);
  int CountLoginAgent(UC *pGroupNoList);
  void CountLoginIdleAgent(UC *pGroupNoList, int &logins, int &idles, int &talks);
};

//主要用來監視ACD組的登錄數、排隊數
class CACDSplit
{
public:
  short state;
  char m_ACDSplitID[32]; //ACDSplit號碼
  short m_nGroupNo; //對應的坐席組號
  short m_nAvailAgents; //可用的座席數
  short m_nCallsInQueue; //排隊電話數
  short m_nLoginAgents; //登錄的座席數
  
public:
  void ResetData()
  {
    state = 0;
    memset(m_ACDSplitID, 0, 32);
    m_nGroupNo = 0;
    m_nAvailAgents = 0;
    m_nCallsInQueue = 0;
    m_nLoginAgents = 0;
  }
  CACDSplit()
  {
    ResetData();
  }
  virtual ~CACDSplit()
  {
  }
};

#define MAX_ACDSPLIT_NUM    64

class CACDSplitMng
{
public:
  short m_nACDSplitNum;
  CACDSplit m_pACDSplitList[MAX_ACDSPLIT_NUM];
  
public:
  CACDSplitMng()
  {
    m_nACDSplitNum = MAX_ACDSPLIT_NUM;
  }
  virtual ~CACDSplitMng()
  {
  }
  void ResetData(const char *pszACDSplitID)
  {
    for (int i=0; i<MAX_ACDSPLIT_NUM; i++)
    {
      if (strcmp(m_pACDSplitList[i].m_ACDSplitID, pszACDSplitID) == 0)
      {
        m_pACDSplitList[i].ResetData();
        break;
      }
    }
  }
  void SetData(const char *pszACDSplitID, short nGroupNo)
  {
    int i;
    for (i=0; i<MAX_ACDSPLIT_NUM; i++)
    {
      if (strcmp(m_pACDSplitList[i].m_ACDSplitID, pszACDSplitID) == 0)
      {
        m_pACDSplitList[i].m_nGroupNo = nGroupNo;
        break;
      }
    }
    for (i=0; i<MAX_ACDSPLIT_NUM; i++)
    {
      if (strlen(m_pACDSplitList[i].m_ACDSplitID) == 0)
      {
        strcpy(m_pACDSplitList[i].m_ACDSplitID, pszACDSplitID);
        m_pACDSplitList[i].m_nGroupNo = nGroupNo;
        break;
      }
    }
  }
  void SetData(const char *pszACDSplitID, short nAvailAgents, short nCallsInQueue, short nLoginAgents)
  {
    int i;
    for (i=0; i<MAX_ACDSPLIT_NUM; i++)
    {
      if (strcmp(m_pACDSplitList[i].m_ACDSplitID, pszACDSplitID) == 0)
      {
        m_pACDSplitList[i].m_nAvailAgents = nAvailAgents;
        m_pACDSplitList[i].m_nCallsInQueue = nCallsInQueue;
        m_pACDSplitList[i].m_nLoginAgents = nLoginAgents;
        break;
      }
    }
    for (i=0; i<MAX_ACDSPLIT_NUM; i++)
    {
      if (strlen(m_pACDSplitList[i].m_ACDSplitID) == 0)
      {
        strcpy(m_pACDSplitList[i].m_ACDSplitID, pszACDSplitID);
        m_pACDSplitList[i].m_nAvailAgents = nAvailAgents;
        m_pACDSplitList[i].m_nCallsInQueue = nCallsInQueue;
        m_pACDSplitList[i].m_nLoginAgents = nLoginAgents;
        break;
      }
    }
  }
  CACDSplit *GetACDSplit(const char *pszACDSplitID)
  {
    for (int i=0; i<MAX_ACDSPLIT_NUM; i++)
    {
      if (strcmp(m_pACDSplitList[i].m_ACDSplitID, pszACDSplitID) == 0)
      {
        return &m_pACDSplitList[i];
      }
    }
    return NULL;
  }
};

class CLogerStatusSet
{
public:
  short nLoginId; //0-斷開 1-連接 2-已登錄

  unsigned char LogerClientId;

  bool bGetChnStatus;
  bool bGetAgStatus;
  bool bGetAcdStatus;
  bool bGetTrkCallCount;
  bool bGetAgCallCount;

  bool bGetIVRMsg;

  char LoginUserName[32];
  int LoginLevel; //登錄級別 1-administrator 2-manager 3-operator 4-monitor 5-board
public:
  void Init()
  {
    bGetChnStatus = true;
    bGetAgStatus = true;
    bGetAcdStatus = true;
    bGetTrkCallCount = false;
    bGetAgCallCount = false;
    
    bGetIVRMsg = false;
    memset(LoginUserName, 0, 32);
    LoginLevel = 0;
  }
  CLogerStatusSet()
  {
    nLoginId = 0;
    LogerClientId = 0;
    bGetChnStatus = true;
    bGetAgStatus = true;
    bGetAcdStatus = true;
    bGetTrkCallCount = false;
    bGetAgCallCount = false;
    
    bGetIVRMsg = false;
    memset(LoginUserName, 0, 32);
    LoginLevel = 0;
  }
  virtual ~CLogerStatusSet(){}

};

//IVR號碼對應的部門編號
class CIVRDept
{
public:
  short state;
  char m_IVRNO[32]; //IVR號碼
  short m_nDeptNo; //對應的坐席組號

public:
  CIVRDept()
  {
    state = 0;
    memset(m_IVRNO, 0, 32);
    m_nDeptNo = 0;
  }
  virtual ~CIVRDept()
  {
  }
};

#define MAX_IVRNO_NUM    32

class CIVRDeptMng
{
public:
  short m_nIVRNONum;
  CIVRDept m_IVRDeptList[MAX_IVRNO_NUM];
  
public:
  CIVRDeptMng()
  {
    m_nIVRNONum = 0;
  }
  virtual ~CIVRDeptMng()
  {
  }
  void SetIVRDept(const char *pszIVRNO, short m_nDeptNo)
  {
    for (int i=0; i<MAX_IVRNO_NUM; i++)
    {
      if (m_IVRDeptList[i].state == 0)
      {
        m_IVRDeptList[i].state = 1;
        strncpy(m_IVRDeptList[i].m_IVRNO, pszIVRNO, 31);
        m_IVRDeptList[i].m_nDeptNo = m_nDeptNo;
        m_nIVRNONum++;
        break;
      }
    }
  }
  short GetDeptNo(const char *pszIVRNO)
  {
    for (int i=0; i<m_nIVRNONum; i++)
    {
      if (m_IVRDeptList[i].state == 1 && strcmp(m_IVRDeptList[i].m_IVRNO, pszIVRNO) == 0)
      {
        return m_IVRDeptList[i].m_nDeptNo;
      }
    }
    return 0;
  }
};

//---------------------------------------------------------------------------
#endif
