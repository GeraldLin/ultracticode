//---------------------------------------------------------------------------
/*
IVR參數配制類定義
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "ivrcfg.h"

//---------------------------------------------------------------------------
CIVRCfg::CIVRCfg()
{
  isDigitOneSayYao = true;
  isMCITestFlag = false;
  AutoSendACMId = true;	//號碼收全后自動送ACM信號，默認為true
  AutoSendANCId = false; //自動發送ANC信號
  DebugId = 0;
  VoiceCardInitState = 0;
  isWriteTimerDogLogId = false;
  WriteTimerDogLogCount = 250;
  CountCycle = 1;
  isCountCall = true;
  isWriteCountCallTxt = true;
  isWriteCountCallDB = false;
  isAutoDelPSTNPreCode = true;
  isAutoDelMobilePreCode0 = true;
  isAutoAddTWMobilePreCode0 = false;
  isAutoWritePSTNPreCode = false;
  isSendRouteIdelTrkNum = true;
  isClearCallerNoChar = true;
  isSendACDCallToAgent = false;
  isHangonAutoCancelCall = false;
  isRecordTranOutCall = true;
  isWriteSeatStatus = false;
  isWriteSeatStatusOnLogout = true;
  isOnlySendMySeatStatus = true;
  isOnlySendACDQueueToLeader = false;
  isKeepLastSeatStatus = false;
  isUpdateSeatStateToDB = false;
  isUpdateACDQueueToDB = false;
  isUpdateAgentSumDataToDB = false;
  isUpdateGroupSumDataToDB = false;
  isSendSeatStateToGW = false;
  isAppendSeatDailDTMF = true;
  isSeatRecordAfterTalk = false;
  RecordOneFileForLocaCall = 0;
  isAutoAddDailOutPreCode = false;
  isAutoAddTranOutPreCode = false;
  isAutoDelDailOutPreCode = true;
  isAutoAddDailOutIPCode = false;
  isAutoDelDailOutIPCode = true;
  isCDRSwapCallerCalledNo = false;
  isUpdateDialOutId = false;
  isUpdateCRMCallId = false;
  isInsertDialOutListId = false;
  strcpy(UpdateDialOutIdFieldName, "DialId");
  strcpy(UpdateDialOutRelFieldName, "HangoffTime");
  isInsertIVRDTMFTraceId = false;
  isCDRInsertDeptNo = false;
  isCDRInsertOrgCallNo = true;
  isCDRInsertCallUCID = false;
  isSumCallExtnToExtn = true; //2015-12-26
  isAgentLoginSwitch = false;
  isSetREADYonAgentLogin = false;
  isSetNOTREADYonAgentLogout = false;
  isSetReadyStateonAgentSetDisturb = 0;
  isAgentLoginACDSplit = false;
  isWorkerLogOutAfterunTcpLink = true;
  isACDToAgentWhenChBlock = true;
  AutoSetAgentLeaveNoAnsCount = 3;
  MinCDRCalledNoLen = 3;
  isCTILinkCanRecord = false;
  isExternalRecord = false;
  isOnlyRecSystem = false;
  isInsertRecCallidList = false;
  isWriteAlarmToDB = 0;
  isHangoffOnIVRCallout = false;
  isCheckPasswordWebLogin = true;
  isOnlyPickupCallExistSeatNo = true;
  MaxWaitIVRReturnTimelen = 180;

  GetIdleSeatCountType = 1;
  MaxTranOutTalkTimeLen = 900;

  ControlCallModal = 0;
  SP_InsertCDR = 0;
  SP_UpDateAreaName = 0;
  isDispSQLMsg = false;

  isClearLastACDnAG0Hour = false;
  ClearCallCountType = 1;
  MinChangeDutyLen = 2;
  MaxWorkerNoLen = 0;
  isInsertCallTrace = false;
  memset(DialOutPreCode, 0, 20);
  memset(WaitVocFile, 0, 256);
  for (int i=0; i<24; i++)
  {
    memset(ClearTimeList[i], 0, 5);
  }
  ClearTimeListNum = 0;
  AnsAfterRingCount = 1;
  memset(AppPath, 0, MAX_PATH_LEN);
  memset(INIFileName, 0, MAX_PATH_FILE_LEN);
  ProcLocaAreaCodeId = 0;
  ProcLocaAreaCodeId1 = 0;
  LocaTelCodeLen = 8;
  memset(LocaAreaCode, 0, 20);
  memset(CentrexCode, 0, 20);

  isDumpCRMRecord = false;
  SourDBId = 0;
  memset(QuerySqls, 0, MAX_SQLS_LEN);
  DestDBId = 0;
  memset(InsertSqls, 0, MAX_SQLS_LEN);

  isQueryCustomerByWebservice = false;
  memset(QueryCustomerWebserviceAddr, 0, 64);
  memset(QueryCustomerWebserviceParam, 0, 128);
}

CIVRCfg::~CIVRCfg()
{
}

void CIVRCfg::ReadIniFile()
{
	CH pzTemp[25], param[128];
	int i, j=0, ntemp;
  CStringX ArrString[24], ArrString1[24];

  if (GetPrivateProfileInt("CONFIGURE","DigitOneSayYao", 1, INIFileName) == 0)
    isDigitOneSayYao = false;
  if (GetPrivateProfileInt("CONFIGURE","MCITestFlag", 0, INIFileName) == 1)
    isMCITestFlag = true;

  VoiceCardInitState = GetPrivateProfileInt("CONFIGURE","VoiceCardInitState", 0, INIFileName);

  if (GetPrivateProfileInt("CONFIGURE","WriteTimerDogLogId", 0, INIFileName) == 1)
    isWriteTimerDogLogId = true;
  WriteTimerDogLogCount = GetPrivateProfileInt("CONFIGURE","WriteTimerDogLogCount", 250, INIFileName);

  if (GetPrivateProfileInt("CONFIGURE","AutoSendACM", 1, INIFileName) == 0)
    AutoSendACMId = false;
  if (GetPrivateProfileInt("CONFIGURE","AutoSendANC", 0, INIFileName) == 1)
    AutoSendANCId = true;
  DebugId = GetPrivateProfileInt("CONFIGURE","Debug", 0, INIFileName);

  CountCycle = GetPrivateProfileInt("CALLCOUNT","CountCycle", 1, INIFileName);
  if (GetPrivateProfileInt("CALLCOUNT","AutoCountCall", 1, INIFileName) == 0)
    isCountCall = false;
  if (GetPrivateProfileInt("CALLCOUNT","WriteCountCallTxt", 1, INIFileName) == 0)
    isWriteCountCallTxt = false;
  if (GetPrivateProfileInt("CALLCOUNT","WriteCountCallDB", 0, INIFileName) == 1)
    isWriteCountCallDB = true;
  if (GetPrivateProfileInt("CALLCOUNT","SendRouteIdelTrkNum", 1, INIFileName) == 0)
    isSendRouteIdelTrkNum = false;
  if (GetPrivateProfileInt("CONFIGURE","SendACDCallToAgent", 0, INIFileName) == 1)
    isSendACDCallToAgent = true;
  if (GetPrivateProfileInt("CONFIGURE","HangonAutoCancelCall", 0, INIFileName) == 1)
    isHangonAutoCancelCall = true;
  if (GetPrivateProfileInt("CONFIGURE","RecordTranOutCall", 1, INIFileName) == 0)
    isRecordTranOutCall = false;
  if (GetPrivateProfileInt("CONFIGURE","WriteSeatStatus", 0, INIFileName) == 1)
    isWriteSeatStatus = true;
  if (GetPrivateProfileInt("CONFIGURE","WriteSeatStatusOnLogout", 1, INIFileName) == 0)
    isWriteSeatStatusOnLogout = false;
  if (GetPrivateProfileInt("CONFIGURE","OnlySendMySeatStatus", 1, INIFileName) == 0)
    isOnlySendMySeatStatus = false;
  if (GetPrivateProfileInt("CONFIGURE","OnlySendACDQueueToLeader", 0, INIFileName) == 1)
    isOnlySendACDQueueToLeader = true;
  if (GetPrivateProfileInt("CONFIGURE","KeepLastSeatStatus", 0, INIFileName) == 1)
    isKeepLastSeatStatus = true;
  if (GetPrivateProfileInt("CONFIGURE","UpdateSeatStateToDB", 0, INIFileName) == 1)
    isUpdateSeatStateToDB = true;
  if (GetPrivateProfileInt("CONFIGURE","UpdateACDQueueToDB", 0, INIFileName) == 1)
    isUpdateACDQueueToDB = true;
  if (GetPrivateProfileInt("CONFIGURE","UpdateAgentSumDataToDB", 0, INIFileName) == 1)
    isUpdateAgentSumDataToDB = true;
  if (GetPrivateProfileInt("CONFIGURE","UpdateGroupSumDataToDB", 0, INIFileName) == 1)
    isUpdateGroupSumDataToDB = true;
  if (GetPrivateProfileInt("CONFIGURE","SendSeatStateToGW", 0, INIFileName) == 1)
    isSendSeatStateToGW = true;
  if (GetPrivateProfileInt("CONFIGURE","AppendSeatDailDTMF", 1, INIFileName) == 0)
    isAppendSeatDailDTMF = false;
  if (GetPrivateProfileInt("CONFIGURE","SeatRecordAfterTalk", 0, INIFileName) == 1)
    isSeatRecordAfterTalk = true;
  RecordOneFileForLocaCall = GetPrivateProfileInt("CONFIGURE","RecordOneFileForLocaCall", 0, INIFileName);
  if (GetPrivateProfileInt("CONFIGURE","CDRSwapCallerCalledNo", 0, INIFileName) == 1)
    isCDRSwapCallerCalledNo = true;
  if (GetPrivateProfileInt("CONFIGURE","UpdateDialOutId", 0, INIFileName) == 1)
    isUpdateDialOutId = true;
  if (GetPrivateProfileInt("CONFIGURE","UpdateCRMCallId", 0, INIFileName) == 1)
    isUpdateCRMCallId = true;
  if (GetPrivateProfileInt("CONFIGURE","InsertDialOutListId", 0, INIFileName) == 1)
    isInsertDialOutListId = true;
  GetPrivateProfileString("CONFIGURE", "UpdateDialOutIdFieldName", "Id", UpdateDialOutIdFieldName, 49, INIFileName);
  GetPrivateProfileString("CONFIGURE", "UpdateDialOutRelFieldName", "RelTime", UpdateDialOutRelFieldName, 49, INIFileName);
  isWriteAlarmToDB = GetPrivateProfileInt("CONFIGURE","WriteAlarmToDB", 0, INIFileName);

  if (GetPrivateProfileInt("CONFIGURE","InsertIVRDTMFTraceId", 0, INIFileName) == 1)
    isInsertIVRDTMFTraceId = true;
  if (GetPrivateProfileInt("CONFIGURE","CDRInsertDeptNo", 0, INIFileName) == 1)
    isCDRInsertDeptNo = true;
  if (GetPrivateProfileInt("CONFIGURE","CDRInsertOrgCallNo", 1, INIFileName) == 0)
    isCDRInsertOrgCallNo = false;
  if (GetPrivateProfileInt("CONFIGURE","CDRInsertCallUCID", 0, INIFileName) == 1)
    isCDRInsertCallUCID = true;
  if (GetPrivateProfileInt("CONFIGURE","SumCallExtnToExtn", 1, INIFileName) == 0)
    isSumCallExtnToExtn = false;

  if (GetPrivateProfileInt("CONFIGURE","AgentLoginSwitch", 0, INIFileName) == 1)
    isAgentLoginSwitch = true;
  if (GetPrivateProfileInt("CONFIGURE","SetREADYonAgentLogin", 0, INIFileName) == 1)
    isSetREADYonAgentLogin = true;
  if (GetPrivateProfileInt("CONFIGURE","SetNOTREADYonAgentLogout", 0, INIFileName) == 1)
    isSetNOTREADYonAgentLogout = true;
  isSetReadyStateonAgentSetDisturb = GetPrivateProfileInt("CONFIGURE","SetReadyStateonAgentSetDisturb", 0, INIFileName);
  AgentLogOnOffMode = GetPrivateProfileInt("CONFIGURE","AgentLogOnOffMode", 0, INIFileName);
  if (GetPrivateProfileInt("CONFIGURE","AgentLoginACDSplit", 0, INIFileName) == 1)
    isAgentLoginACDSplit = true;
  if (GetPrivateProfileInt("CONFIGURE","WorkerLogOutAfterunTcpLink", 1, INIFileName) == 0)
    isWorkerLogOutAfterunTcpLink = false;
  if (GetPrivateProfileInt("CONFIGURE","CheckPasswordWebLogin", 1, INIFileName) == 0)
    isCheckPasswordWebLogin = false;
  if (GetPrivateProfileInt("CONFIGURE","OnlyPickupCallExistSeatNo", 1, INIFileName) == 0)
    isOnlyPickupCallExistSeatNo = false;
  
  if (GetPrivateProfileInt("CONFIGURE","CTILinkCanRecord", 0, INIFileName) == 1)
    isCTILinkCanRecord = true;
  if (GetPrivateProfileInt("CONFIGURE","ExternalRecord", 0, INIFileName) == 1)
    isExternalRecord = true;
  if (GetPrivateProfileInt("CONFIGURE","OnlyRecSystem", 0, INIFileName) == 1)
    isOnlyRecSystem = true;
  if (GetPrivateProfileInt("CONFIGURE","InsertRecCallidList", 0, INIFileName) == 1)
    isInsertRecCallidList = true;
  MinCDRCalledNoLen = GetPrivateProfileInt("CONFIGURE","MinCDRCalledNoLen", 3, INIFileName);

  if (GetPrivateProfileInt("CONFIGURE","ACDToAgentWhenChBlock", 1, INIFileName) == 0)
    isACDToAgentWhenChBlock = false;
  AutoSetAgentLeaveNoAnsCount = GetPrivateProfileInt("CONFIGURE","AutoSetAgentLeaveNoAnsCount", 0, INIFileName);
  MaxWaitIVRReturnTimelen = GetPrivateProfileInt("CONFIGURE","MaxWaitIVRReturnTimelen", 180, INIFileName);

  TranCallFailReturnType = GetPrivateProfileInt("CONFIGURE","TranCallFailReturnType", 0, INIFileName);
  
  if (GetPrivateProfileInt("PSTN","AutoAddDailOutPreCode", 0, INIFileName) == 1)
    isAutoAddDailOutPreCode = true;
  if (GetPrivateProfileInt("PSTN","AutoAddTranOutPreCode", 0, INIFileName) == 1)
    isAutoAddTranOutPreCode = true;
  if (GetPrivateProfileInt("PSTN","AutoDelDailOutPreCode", 1, INIFileName) == 0)
    isAutoDelDailOutPreCode = false;
  if (GetPrivateProfileInt("PSTN","AutoAddDailOutIPCode", 0, INIFileName) == 1)
    isAutoAddDailOutIPCode = true;
  if (GetPrivateProfileInt("PSTN","AutoDelDailOutIPCode", 1, INIFileName) == 0)
    isAutoDelDailOutIPCode = false;

  GetIdleSeatCountType = GetPrivateProfileInt("CONFIGURE","GetIdleSeatCountType", 1, INIFileName);
  MaxTranOutTalkTimeLen = GetPrivateProfileInt("CONFIGURE","MaxTranOutTalkTimeLen", 900, INIFileName);
  ControlCallModal = GetPrivateProfileInt("CONFIGURE","ControlCallModal", 0, INIFileName);
  SP_InsertCDR = GetPrivateProfileInt("CONFIGURE","SP_InsertCDR", 1, INIFileName);
  SP_UpDateAreaName = GetPrivateProfileInt("CONFIGURE","SP_UpDateAreaName", 0, INIFileName);
  if (GetPrivateProfileInt("CONFIGURE","DispSQLMsg", 0, INIFileName) == 1)
    isDispSQLMsg = true;
  GetPrivateProfileString("CONFIGURE", "WaitVocFile", "callcenter/wait.wav", WaitVocFile, 100, INIFileName);
  if (GetPrivateProfileInt("CONFIGURE","InsertCallTrace", 0, INIFileName) == 1)
    isInsertCallTrace = true;

  if (GetPrivateProfileInt("CONFIGURE","ClearLastACDnAG0Hour", 0, INIFileName) == 1)
    isClearLastACDnAG0Hour = true;
  ClearCallCountType = GetPrivateProfileInt("CALLCOUNT","ClearCallCountType", 1, INIFileName);
  MinChangeDutyLen = GetPrivateProfileInt("CALLCOUNT","MinChangeDutyLen", 2, INIFileName);
  if (ClearCallCountType == 5)
  {
    GetPrivateProfileString("CALLCOUNT", "ClearTimeList", "", param, 120, INIFileName);
    ntemp = SplitString(param, ';', 24, ArrString);
    for (i=0; i<ntemp; i++)
    {
      if (ArrString[i].GetLength() == 5)
      {
        strcpy(pzTemp, ArrString[i].C_Str());
        if (strncmp(pzTemp, "00", 2) >= 0 && strncmp(pzTemp, "23", 2) <= 0 && pzTemp[2] == ':' && strncmp(&pzTemp[3], "00", 2) >= 0 && strncmp(&pzTemp[3], "59", 2) <= 0)
        {
          strcpy(ClearTimeList[j], pzTemp);
          j++;
        }
      }
    }
    ClearTimeListNum = j;
    if (j == 0)
    {
      ClearCallCountType = 0;
    }
  }
	
	for (i = 0; i < MAX_CHANGE_NUM; i ++)
	{
		memset(OldCalled[i], 0, 20);
		memset(NewCalled[i], 0, 20);
		memset(OldCaller[i], 0, 20);
		memset(NewCaller[i], 0, 20);
	}
	for (i = 0; i < MAX_CHANGE_EXT_NUM; i ++)
	{
		strcpy(PSTNCode[i], CenterCode);
    memset(PreCode[i], 0, 20);
    memset(IPPreCode[i], 0, 20);
	}
  memset(VOIPDefaultCalled, 0, 20);
	for (i = 0; i < MAX_CHANGE_VOIP_NUM; i ++)
	{
    memset(NewVOIPCalled[i], 0, 20);
	}

	CalledChangeId = GetPrivateProfileInt("CHANGE","CalledChangeId", 0, INIFileName);
	ntemp = GetPrivateProfileInt("CHANGE","CalledChangeNum", 0, INIFileName);
	if (CalledChangeId == 1 && ntemp > 0)
	{
		for (i = 0; i < ntemp && i < MAX_CHANGE_NUM; i ++)
		{
			sprintf(pzTemp, "OldCalled[%d]", i);
			GetPrivateProfileString("CHANGE", pzTemp, "", OldCalled[i], 20, INIFileName);
			sprintf(pzTemp, "NewCalled[%d]", i);
			GetPrivateProfileString("CHANGE", pzTemp, "", NewCalled[i], 20, INIFileName);
		}
	}

	CallerChangeId = GetPrivateProfileInt("CHANGE","CallerChangeId", 0, INIFileName);
	ntemp = GetPrivateProfileInt("CHANGE","CallerChangeNum", 0, INIFileName);
	if (CallerChangeId == 1 && ntemp > 0)
	{
		for (i = 0; i < ntemp && i < MAX_CHANGE_NUM; i ++)
		{
			sprintf(pzTemp, "OldCaller[%d]", i);
			GetPrivateProfileString("CHANGE", pzTemp, "", OldCaller[i], 20, INIFileName);
			sprintf(pzTemp, "NewCaller[%d]", i);
			GetPrivateProfileString("CHANGE", pzTemp, "", NewCaller[i], 20, INIFileName);
		}
	}
  
  ProcLocaAreaCodeId = GetPrivateProfileInt("CONFIGURE","ProcLocaAreaCodeId", 0, INIFileName);
  ProcLocaAreaCodeId1 = GetPrivateProfileInt("CONFIGURE","ProcLocaAreaCodeId1", 0, INIFileName);
  LocaTelCodeLen = GetPrivateProfileInt("CONFIGURE","LocaTelCodeLen", 8, INIFileName);
  LocaTelCodeLen1 = GetPrivateProfileInt("CONFIGURE","LocaTelCodeLen1", 8, INIFileName);
  GetPrivateProfileString("CONFIGURE", "LocaAreaCode", "", LocaAreaCode, 20, INIFileName);
  GetPrivateProfileString("CONFIGURE", "LocaAreaCode1", "", LocaAreaCode1, 20, INIFileName);
  GetPrivateProfileString("CONFIGURE", "LocaAreaCode2", "", LocaAreaCode2, 20, INIFileName);
  GetPrivateProfileString("CONFIGURE", "LocaAreaCode3", "", LocaAreaCode3, 20, INIFileName);
  GetPrivateProfileString("CONFIGURE", "LocaAreaCode4", "", LocaAreaCode4, 20, INIFileName);
  
  MaxWorkerNoLen = GetPrivateProfileInt("CONFIGURE","MaxWorkerNoLen", 0, INIFileName);
  //模擬外線參數
  GetPrivateProfileString("PSTN", "DialOutPreCode", "9", DialOutPreCode, 20, INIFileName);
  AnsAfterRingCount = GetPrivateProfileInt("PSTN","AnsAfterRingCount", 1, INIFileName);
  GetPrivateProfileString("PSTN", "PSTNCode", "400", CenterCode, 20, INIFileName);
  if (GetPrivateProfileInt("PSTN","ClearCallerNoChar", 1, INIFileName) == 0)
    isClearCallerNoChar = false; 

  //讀對應通道轉換新的模擬外線被叫號碼
  if (GetPrivateProfileInt("PSTN","AutoDelMobilePreCode0", 1, INIFileName) == 0)
    isAutoDelMobilePreCode0 = false;
  if (GetPrivateProfileInt("PSTN","AutoAddTWMobilePreCode0", 0, INIFileName) == 1)
    isAutoAddTWMobilePreCode0 = true;
  if (GetPrivateProfileInt("PSTN","AutoDelPreCode", 1, INIFileName) == 0)
    isAutoDelPSTNPreCode = false;
  GetPrivateProfileString("PSTN", "CentrexCode", "", CentrexCode, 20, INIFileName);
  if (GetPrivateProfileInt("PSTN","AutoWritePreCode", 0, INIFileName) == 1)
    isAutoWritePSTNPreCode = true;
  PSTNChangeId = GetPrivateProfileInt("PSTN","PSTNChangeId", 0, INIFileName);
	ntemp = GetPrivateProfileInt("PSTN","PSTNNum", 0, INIFileName);
	if (PSTNChangeId == 1 && ntemp > 0)
	{
		for (i = 0; i < ntemp && i < MAX_CHANGE_EXT_NUM; i ++)
		{
			sprintf(pzTemp, "PSTNCode[%d]", i);
			GetPrivateProfileString("PSTN", pzTemp, "", PSTNCode[i], 20, INIFileName);
			sprintf(pzTemp, "PreCode[%d]", i);
			GetPrivateProfileString("PSTN", pzTemp, "", PreCode[i], 20, INIFileName);
      sprintf(pzTemp, "IPPreCode[%d]", i);
      GetPrivateProfileString("PSTN", pzTemp, "", IPPreCode[i], 20, INIFileName);
      
      sprintf(pzTemp, "NotAddPreCode[%d]", i);
      GetPrivateProfileString("PSTN", pzTemp, "", param, 120, INIFileName);
      PBXExtrnList[i].ExtrnPreNum = 0;
      j=0;
      if (strlen(param) > 0)
      {
        int nExtrnPreNum=SplitString(param, ';', 20, ArrString);
        for (int n=0; n<nExtrnPreNum; n++)
        {
          if (ArrString[n].GetLength() > 0)
          {
            int nItemNum=SplitString(ArrString[n].C_Str(), ',', 2, ArrString1);
            if (nItemNum>=2)
            {
              if (ArrString1[0].GetLength() > 0)
              {
                PBXExtrnList[i].PBXExtrn[j].ExtrnLen = atoi(ArrString1[1].C_Str());
                if (PBXExtrnList[i].PBXExtrn[j].ExtrnLen > 0)
                {
                  strncpy(PBXExtrnList[i].PBXExtrn[j].ExtrnPreCode, ArrString1[0].C_Str(), 9);
                  j++;
                  PBXExtrnList[i].ExtrnPreNum = j;
                }
              }
            }
          }
        }
      }
		}
	}

  //模擬監錄通道
  ntemp = GetPrivateProfileInt("RECORD","LineNum", 0, INIFileName);
  for (i = 0; i < MAX_CHANNEL_REC_NUM; i ++)
  {
    if (i < ntemp)
    {
      sprintf(pzTemp, "LineCode[%d]", i);
      GetPrivateProfileString("RECORD", pzTemp, "", RecLineCode[i], 20, INIFileName);
    }
    else
    {
      strcpy(RecLineCode[i], "R");
    }
  }

  if (GetPrivateProfileInt("CONFIGURE","HangoffOnIVRCallout", 0, INIFileName) == 1) //2015-10-20
    isHangoffOnIVRCallout = true;
  //交換機呼出字冠參數
  GetPrivateProfileString("SWITCH", "DialOutPreCode", "", SWTDialOutPreCode, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "DialOutPreCode1", "", SWTDialOutPreCode1, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "DialOutPreCode2", "", SWTDialOutPreCode2, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "DialOutPreCode3", "", SWTDialOutPreCode3, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "DialOutPreCode4", "", SWTDialOutPreCode4, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "PreCode", "", SWTPreCode, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "IPPreCode", "", SWTIPPreCode, 20, INIFileName);
  GetPrivateProfileString("SWITCH", "PreCodeEx", "", SWTPreCodeEx, 20, INIFileName);

  GetPrivateProfileString("SWITCH", "NotAddPreCode", "", param, 120, INIFileName);
  SWTPBXExtrnList.ExtrnPreNum = 0;
  j=0;
  if (strlen(param) > 0)
  {
    int nExtrnPreNum=SplitString(param, ';', 20, ArrString);
    for (int n=0; n<nExtrnPreNum; n++)
    {
      if (ArrString[n].GetLength() > 0)
      {
        int nItemNum=SplitString(ArrString[n].C_Str(), ',', 2, ArrString1);
        if (nItemNum>=2)
        {
          if (ArrString1[0].GetLength() > 0)
          {
            SWTPBXExtrnList.PBXExtrn[j].ExtrnLen = atoi(ArrString1[1].C_Str());
            if (SWTPBXExtrnList.PBXExtrn[j].ExtrnLen > 0)
            {
              strncpy(SWTPBXExtrnList.PBXExtrn[j].ExtrnPreCode, ArrString1[0].C_Str(), 9);
              j++;
              SWTPBXExtrnList.ExtrnPreNum = j;
            }
          }
        }
      }
    }
  }

  GetPrivateProfileString("SWITCH", "OXEAlternateCallDTMF", ",1", OXEAlternateCallDTMF, 20, INIFileName);

	//讀對應VOIP通道轉換新的被叫號碼
	VOIPChangeId = GetPrivateProfileInt("VOIP","VOIPChangeId", 0, INIFileName);
	ntemp = GetPrivateProfileInt("VOIP","VOIPChangeNum", 0, INIFileName);
	GetPrivateProfileString("VOIP", "VOIPDefaultCalled", "", VOIPDefaultCalled, 20, INIFileName);
  if (VOIPChangeId == 1 && ntemp > 0)
	{
		for (i = 0; i < ntemp && i < MAX_CHANGE_VOIP_NUM; i ++)
		{
			sprintf(pzTemp, "NewVOIPCalled[%d]", i);
			GetPrivateProfileString("VOIP", pzTemp, "", NewVOIPCalled[i], 20, INIFileName);
		}
	}

  MaxInRingOverTime = GetPrivateProfileInt("CONFIGURE","MaxInRingOverTime", 0, INIFileName);
  MaxOutRingOverTime = GetPrivateProfileInt("CONFIGURE","MaxOutRingOverTime", 0, INIFileName);
  MaxTalkOverTime = GetPrivateProfileInt("CONFIGURE","MaxTalkOverTime", 0, INIFileName);
  MaxACWOverTime = GetPrivateProfileInt("CONFIGURE","MaxACWOverTime", 0, INIFileName);

	GetPrivateProfileString("MEDIA", "MemIndexPath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "%smemvoc", AppPath);
    if (MyCheckPath(param) == 0)
      strcpy(param, "c:/ivrvoc/memvoc");
  }
  MemIndexPath = param;

	GetPrivateProfileString("MEDIA", "PlayFilePath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "%sivrvoc", AppPath);
    if (MyCheckPath(param) == 0)
      strcpy(param, "c:/ivrvoc");
  }
  PlayFilePath = param;

	GetPrivateProfileString("MEDIA", "BoxRecPath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "%sivrvoc", AppPath);
    if (MyCheckPath(param) == 0)
      strcpy(param, "c:/ivrvoc");
  }
  BoxRecPath = param;

	GetPrivateProfileString("MEDIA", "SeatRecPath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "d:/record");
    //if (MyCheckPath(param) == 0)
    //  strcpy(param, "c:/record");
  }
  SeatRecPath = param;

	GetPrivateProfileString("MEDIA", "ConfRecPath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "d:/record");
    if (MyCheckPath(param) == 0)
      strcpy(param, "c:/record");
  }
  ConfRecPath = param;

	GetPrivateProfileString("MEDIA", "SendFaxPath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "%ssendfax", AppPath);
    if (MyCheckPath(param) == 0)
      strcpy(param, "c:/sendfax");
  }
  SendFaxPath = param;

	GetPrivateProfileString("MEDIA", "RecvFaxPath", "", param, 120, INIFileName);
  if (strlen(param) == 0)
  {
    sprintf(param, "%srecvfax", AppPath);
    if (MyCheckPath(param) == 0)
      strcpy(param, "c:/recvfax");
  }
  RecvFaxPath = param;

  if (GetPrivateProfileInt("DUMPCRMDB","DumpCRMRecord", 0, INIFileName) == 1)
    isDumpCRMRecord = true;
  SourDBId = GetPrivateProfileInt("DUMPCRMDB","SourDBId", 0, INIFileName);
  GetPrivateProfileString("DUMPCRMDB", "QuerySqls", "", QuerySqls, MAX_SQLS_LEN-1, INIFileName);
  DestDBId = GetPrivateProfileInt("DUMPCRMDB","DestDBId", 0, INIFileName);
  GetPrivateProfileString("DUMPCRMDB", "InsertSqls", "", InsertSqls, MAX_SQLS_LEN-1, INIFileName);
  if (strlen(QuerySqls) == 0 || strlen(InsertSqls) == 0)
  {
    isDumpCRMRecord = false;
  }

  if (GetPrivateProfileInt("WEBSERVICE","QueryCustomerByWebservice", 0, INIFileName) == 1)
    isQueryCustomerByWebservice = true;
  GetPrivateProfileString("WEBSERVICE", "QueryCustomerWebserviceAddr", "", QueryCustomerWebserviceAddr, 63, INIFileName);
  GetPrivateProfileString("WEBSERVICE", "QueryCustomerWebserviceParam", "", QueryCustomerWebserviceParam, 127, INIFileName);
}
void CIVRCfg::ChangeCode(char *callerno, char *calledno)
{
  if (CalledChangeId == 1)
  {
    for (int i = 0; i < MAX_CHANGE_NUM; i ++)
    {
      if (strlen(OldCalled[i]) > 0  && strlen(NewCalled[i]) > 0 
        && strcmp(OldCalled[i], calledno) == 0)
      {
        strcpy(calledno, NewCalled[i]);
        break;
      }
    }
  }
	if (CallerChangeId == 1)
  {
    for (int i = 0; i < MAX_CHANGE_NUM; i ++)
    {
      if (strlen(OldCaller[i]) > 0  && strlen(NewCaller[i]) > 0 
        && strcmp(OldCaller[i], callerno) == 0)
      {
        strcpy(callerno, NewCaller[i]);
        break;
      }
    }
  }
}
void CIVRCfg::WritePSTNPreCode(int lineno, const char *precode)
{
  CH pzTemp[25];
  if (isAutoWritePSTNPreCode == false)
  {
    return;
  }
  sprintf(pzTemp, "PreCode[%d]", lineno);
  WritePrivateProfileString("PSTN", pzTemp, precode, INIFileName);
}
//-----------------------------------------------------------------------------
CChCount::CChCount()
{
  Init();
}

CChCount::~CChCount()
{
}

void CChCount::Init()
{
  TrkInCount = 0;
  TrkInAnsCount = 0;
  TrkInCur = 0;
  TrkInMin = 0;
  TrkInMax = 0;

  TrkOutCount = 0;
  TrkOutAnsCount = 0;
  TrkOutCur = 0;
  TrkOutMin = 0;
  TrkOutMax = 0;

  TrkCallCount = 0;
  TrkBusyCount = 0;
  TrkBusyMin = 0;
  TrkBusyMax = 0;
  
  TrkBusy0 = 0;
  TrkBusy0Per = 100;
  
  TrkBusy0_25 = 0;
  TrkBusy0_25Per = 0;
  
  TrkBusy25_50 = 0;
  TrkBusy25_50Per = 0;
  
  TrkBusy50_75 = 0;
  TrkBusy50_75Per = 0;
  
  TrkBusy75_90 = 0;
  TrkBusy75_90Per = 0;
  
  TrkBusy90_100 = 0;
  TrkBusy90_100Per = 0;

  TrkBusyTotal = 0;
  
  AcdCallCount = 0;
  AcdCallFailCount = 0;
  AcdCallSuccCount = 0;
  AcdCallGiveUpCount = 0;
  AcdCallAnsCount = 0;
  AcdCallAvgWaitTime = 0;
  AcdCallTotalWaitTime = 0;
  AcdCallAvgAnsTime = 0;
  AcdCallTotalAnsTime = 0;
  AcdCallAvgGiveUpTime = 0;
  AcdCallTotalGiveUpTime = 0;
  
  AgLoginCur = 0;
  AgLoginMax = 0;
  AgLoginMin = 0;
  AgBusyCur = 0;
  AgBusyMin = 0;
  AgBusyMax = 0;
  
  AgBusy0 = 0;
  AgBusy0Per = 100;
  
  AgBusy0_25 = 0;
  AgBusy0_25Per = 0;
  
  AgBusy25_50 = 0;
  AgBusy25_50Per = 0;
  
  AgBusy50_75 = 0;
  AgBusy50_75Per = 0;
  
  AgBusy75_90 = 0;
  AgBusy75_90Per = 0;
  
  AgBusy90_100 = 0;
  AgBusy90_100Per = 0;

  AgBusyTotal = 0;
  memset(strTrkData, 0, 256);
  memset(strAgData, 0, 256);
}
void CChCount::InitTrkData()
{
  TrkInCount = 0;
  TrkInAnsCount = 0;
  TrkInCur = 0;
  TrkInMin = 0;
  TrkInMax = 0;
  
  TrkOutCount = 0;
  TrkOutAnsCount = 0;
  TrkOutCur = 0;
  TrkOutMin = 0;
  TrkOutMax = 0;
  
  TrkCallCount = 0;
  TrkBusyCount = 0;
  TrkBusyMin = 0;
  TrkBusyMax = 0;
  
  TrkBusy0 = 0;
  TrkBusy0Per = 100;
  
  TrkBusy0_25 = 0;
  TrkBusy0_25Per = 0;
  
  TrkBusy25_50 = 0;
  TrkBusy25_50Per = 0;
  
  TrkBusy50_75 = 0;
  TrkBusy50_75Per = 0;
  
  TrkBusy75_90 = 0;
  TrkBusy75_90Per = 0;
  
  TrkBusy90_100 = 0;
  TrkBusy90_100Per = 0;
  
  TrkBusyTotal = 0;
  memset(strTrkData, 0, 256);
}
void CChCount::InitAgData()
{
  AcdCallCount = 0;
  AcdCallFailCount = 0;
  AcdCallSuccCount = 0;
  AcdCallGiveUpCount = 0;
  AcdCallAnsCount = 0;
  AcdCallAvgWaitTime = 0;
  AcdCallTotalWaitTime = 0;
  AcdCallAvgAnsTime = 0;
  AcdCallTotalAnsTime = 0;
  AcdCallAvgGiveUpTime = 0;
  AcdCallTotalGiveUpTime = 0;
  
  AgLoginCur = 0;
  AgLoginMax = 0;
  AgLoginMin = 0;
  AgBusyCur = 0;
  AgBusyMin = 0;
  AgBusyMax = 0;
  
  AgBusy0 = 0;
  AgBusy0Per = 100;
  
  AgBusy0_25 = 0;
  AgBusy0_25Per = 0;
  
  AgBusy25_50 = 0;
  AgBusy25_50Per = 0;
  
  AgBusy50_75 = 0;
  AgBusy50_75Per = 0;
  
  AgBusy75_90 = 0;
  AgBusy75_90Per = 0;
  
  AgBusy90_100 = 0;
  AgBusy90_100Per = 0;
  
  AgBusyTotal = 0;
  memset(strAgData, 0, 256);
}

const CChCount& CChCount::operator=(const CChCount& chcount)
{
  TrkInCount = chcount.TrkInCount;
  TrkInAnsCount = chcount.TrkInAnsCount;
  TrkInCur = chcount.TrkInCur;
  TrkInMin = chcount.TrkInMin;
  TrkInMax = chcount.TrkInMax;
  
  TrkOutCount = chcount.TrkOutCount;
  TrkOutAnsCount = chcount.TrkOutAnsCount;
  TrkOutCur = chcount.TrkOutCur;
  TrkOutMin = chcount.TrkOutMin;
  TrkOutMax = chcount.TrkOutMax;
  
  TrkCallCount = chcount.TrkCallCount;
  TrkBusyCount = chcount.TrkBusyCount;
  TrkBusyMin = chcount.TrkBusyMin;
  TrkBusyMax = chcount.TrkBusyMax;
  
  TrkBusy0 = chcount.TrkBusy0;
  TrkBusy0Per = chcount.TrkBusy0Per;
  
  TrkBusy0_25 = chcount.TrkBusy0_25;
  TrkBusy0_25Per = chcount.TrkBusy0_25Per;
  
  TrkBusy25_50 = chcount.TrkBusy25_50;
  TrkBusy25_50Per = chcount.TrkBusy25_50Per;
  
  TrkBusy50_75 = chcount.TrkBusy50_75;
  TrkBusy50_75Per = chcount.TrkBusy50_75Per;
  
  TrkBusy75_90 = chcount.TrkBusy75_90;
  TrkBusy75_90Per = chcount.TrkBusy75_90Per;
  
  TrkBusy90_100 = chcount.TrkBusy90_100;
  TrkBusy90_100Per = chcount.TrkBusy90_100Per;

  TrkBusyTotal = chcount.TrkBusyTotal;
  
  AcdCallCount = chcount.AcdCallCount;
  AcdCallFailCount = chcount.AcdCallFailCount;
  AcdCallSuccCount = chcount.AcdCallSuccCount;
  AcdCallGiveUpCount = chcount.AcdCallGiveUpCount;
  AcdCallAnsCount = chcount.AcdCallAnsCount;
  AcdCallAvgWaitTime = chcount.AcdCallAvgWaitTime;
  AcdCallTotalWaitTime = chcount.AcdCallTotalWaitTime;
  AcdCallAvgGiveUpTime = chcount.AcdCallAvgGiveUpTime;
  AcdCallTotalGiveUpTime = chcount.AcdCallTotalGiveUpTime;
  
  AgLoginCur = chcount.AgLoginCur;
  AgLoginMax = chcount.AgLoginMax;
  AgLoginMin = chcount.AgLoginMin;
  AgBusyCur = chcount.AgBusyCur;
  AgBusyMin = chcount.AgBusyMin;
  AgBusyMax = chcount.AgBusyMax;
  
  AgBusy0 = chcount.AgBusy0;
  AgBusy0Per = chcount.AgBusy0Per;
  
  AgBusy0_25 = chcount.AgBusy0_25;
  AgBusy0_25Per = chcount.AgBusy0_25Per;
  
  AgBusy25_50 = chcount.AgBusy25_50;
  AgBusy25_50Per = chcount.AgBusy25_50Per;
  
  AgBusy50_75 = chcount.AgBusy50_75;
  AgBusy50_75Per = chcount.AgBusy50_75Per;
  
  AgBusy75_90 = chcount.AgBusy75_90;
  AgBusy75_90Per = chcount.AgBusy75_90Per;
  
  AgBusy90_100 = chcount.AgBusy90_100;
  AgBusy90_100Per = chcount.AgBusy90_100Per;

  AgBusyTotal = chcount.AgBusyTotal;

  memcpy(strTrkData, chcount.strTrkData, 256);
  
  return *this;
}
void CChCount::SetTrkData(const CChCount& chcount)
{
  TrkInCount = chcount.TrkInCount;
  TrkInAnsCount = chcount.TrkInAnsCount;
  TrkInCur = chcount.TrkInCur;
  TrkInMin = chcount.TrkInMin;
  TrkInMax = chcount.TrkInMax;
  
  TrkOutCount = chcount.TrkOutCount;
  TrkOutAnsCount = chcount.TrkOutAnsCount;
  TrkOutCur = chcount.TrkOutCur;
  TrkOutMin = chcount.TrkOutMin;
  TrkOutMax = chcount.TrkOutMax;
  
  TrkCallCount = chcount.TrkCallCount;
  TrkBusyCount = chcount.TrkBusyCount;
  TrkBusyMin = chcount.TrkBusyMin;
  TrkBusyMax = chcount.TrkBusyMax;
  
  TrkBusy0 = chcount.TrkBusy0;
  TrkBusy0Per = chcount.TrkBusy0Per;
  
  TrkBusy0_25 = chcount.TrkBusy0_25;
  TrkBusy0_25Per = chcount.TrkBusy0_25Per;
  
  TrkBusy25_50 = chcount.TrkBusy25_50;
  TrkBusy25_50Per = chcount.TrkBusy25_50Per;
  
  TrkBusy50_75 = chcount.TrkBusy50_75;
  TrkBusy50_75Per = chcount.TrkBusy50_75Per;
  
  TrkBusy75_90 = chcount.TrkBusy75_90;
  TrkBusy75_90Per = chcount.TrkBusy75_90Per;
  
  TrkBusy90_100 = chcount.TrkBusy90_100;
  TrkBusy90_100Per = chcount.TrkBusy90_100Per;
  
  TrkBusyTotal = chcount.TrkBusyTotal;
  memcpy(strTrkData, chcount.strTrkData, 256);
  memcpy(strAgData, chcount.strAgData, 256);
}
void CChCount::SetAgData(const CChCount& chcount)
{
  AcdCallCount = chcount.AcdCallCount;
  AcdCallFailCount = chcount.AcdCallFailCount;
  AcdCallSuccCount = chcount.AcdCallSuccCount;
  AcdCallGiveUpCount = chcount.AcdCallGiveUpCount;
  AcdCallAnsCount = chcount.AcdCallAnsCount;
  AcdCallAvgWaitTime = chcount.AcdCallAvgWaitTime;
  AcdCallTotalWaitTime = chcount.AcdCallTotalWaitTime;
  AcdCallAvgGiveUpTime = chcount.AcdCallAvgGiveUpTime;
  AcdCallTotalGiveUpTime = chcount.AcdCallTotalGiveUpTime;
  
  AgLoginCur = chcount.AgLoginCur;
  AgLoginMax = chcount.AgLoginMax;
  AgLoginMin = chcount.AgLoginMin;
  AgBusyCur = chcount.AgBusyCur;
  AgBusyMin = chcount.AgBusyMin;
  AgBusyMax = chcount.AgBusyMax;
  
  AgBusy0 = chcount.AgBusy0;
  AgBusy0Per = chcount.AgBusy0Per;
  
  AgBusy0_25 = chcount.AgBusy0_25;
  AgBusy0_25Per = chcount.AgBusy0_25Per;
  
  AgBusy25_50 = chcount.AgBusy25_50;
  AgBusy25_50Per = chcount.AgBusy25_50Per;
  
  AgBusy50_75 = chcount.AgBusy50_75;
  AgBusy50_75Per = chcount.AgBusy50_75Per;
  
  AgBusy75_90 = chcount.AgBusy75_90;
  AgBusy75_90Per = chcount.AgBusy75_90Per;
  
  AgBusy90_100 = chcount.AgBusy90_100;
  AgBusy90_100Per = chcount.AgBusy90_100Per;
  
  AgBusyTotal = chcount.AgBusyTotal;
  memcpy(strAgData, chcount.strAgData, 256);
}
