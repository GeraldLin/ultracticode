// TCPServer.cpp: implementation of the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#include "stdafx.h"
#include "tcpagserver.h"
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

CTCPAgServer::CTCPAgServer()
{
	IsOpen = false;
  isIVRInit = false;
	hdll = NULL;
  memset(m_szAppPath, 0, MAX_PATH_LEN);
  strcpy(m_szDllFile, "tmagserver.dll");
  IVRServerId = 0x0001;
  IVRServerPort = 5227;
}

CTCPAgServer::~CTCPAgServer()
{
  ReleaseDll();
}

bool CTCPAgServer::LoadDll()
{
  if (strlen(m_szAppPath) > 0)
    sprintf(m_szDllFile, "%s\\tmagserver.dll", m_szAppPath);
  else
    strcpy(m_szDllFile, "tmagserver.dll");
	hdll = LoadLibrary(m_szDllFile);
	if ( hdll != NULL )
	{
    AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(  !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MyTrace(0, "GetProcAddress tmtcpserverag.dll eror");
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MyTrace(0, "LoadLibrary tmtcpserverag.dll eror");
		return false;
	}
}

void CTCPAgServer::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}

bool CTCPAgServer::ReadIni(char *filename)
{
  isIVRInit = false;
  IVRServerId = CombineID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "IVRServerId", 1, filename));
  IVRServerPort = GetPrivateProfileInt("IVRSERVER", "AGServerPort", 5227, filename);

  return true;
}    

bool CTCPAgServer::ReadWebSetupIni(char *filename)
{
  isIVRInit = false;
  IVRServerId = CombineID(NODE_IVR,GetPrivateProfileInt("TCPLINK", "IVRServerID", 1, filename));
  IVRServerPort = GetPrivateProfileInt("TCPLINK", "AGServerPort", 5227, filename);

  return true;
}    

//IVR�o�e������Ȥ��
int CTCPAgServer::SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf)
{
	if (Clientid == 0) return 0;
	int SendedLen, MsgLen;
  MsgLen = strlen(MsgBuf);
  SendedLen = AppSendData(Clientid, MsgId, (UC *)MsgBuf, MsgLen);
  if (SendedLen != MsgLen)
  {
    MyTrace(0, "Send msg to AG error Clientid=0x%04x MsgId=0x%04x MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPAgServer::SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len)
{
	if (Clientid == 0) return 0;
	int SendedLen;
  SendedLen = AppSendData(Clientid, MsgId, MsgBuf, len);
  if (SendedLen != len)
  {
    MyTrace(0, "Send msg to AG error Clientid=0x%04x MsgId=0x%04x MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPAgServer::InitIVRServer()
{
  if (isIVRInit == true)
    return 2;
  if (IsOpen == true)
  {
    AppInitS(IVRServerId, IVRServerPort, OnIVRLogin_AG, OnIVRClose_AG, OnIVRReceiveData_AG);
    MyTrace(9, "Init AGServer ServerId=0x%04x ServerPort=%d", 
      IVRServerId, IVRServerPort);
    isIVRInit = true;
    return 0;
  }
  else
  {
    return 1;
  }
}
int CTCPAgServer::InitTCPLink()
{
  int nResult = 0;

  if (LoadDll() == false)
    return 1;
  if (InitIVRServer() != 0)
    nResult = 2;
  return nResult;
}
