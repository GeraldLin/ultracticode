//---------------------------------------------------------------------------
#ifndef __LGOINOUTFIFO_H_
#define __LGOINOUTFIFO_H_
//---------------------------------------------------------------------------
#include <afxmt.h>

typedef struct
{
  UC LoginOut; //網絡登錄登出標志：0-退出，1-登錄
  UC NodeType; //節點類型
	US ServerId; //服務端節點TCP編號
	US ClientId; //客戶端節點TCP編號
  US NodeId; //節點編號

}VXML_LGOINOUT_STRUCT;

class CLogInOutFifo
{
  unsigned short        Head;
  unsigned short        Tail;
  unsigned short 				Len;
  unsigned short 				resv;
  VXML_LGOINOUT_STRUCT  *pMsgs;

  CCriticalSection m_cCriticalSection;

public:
	CLogInOutFifo(unsigned short len)
	{
		Head=0;
		Tail=0;
		resv=0;
		Len=len;
		pMsgs=new VXML_LGOINOUT_STRUCT[Len];
	}
	
	~CLogInOutFifo()
	{
		if (pMsgs != NULL)
    {
      delete []pMsgs;
      pMsgs = NULL;
    }
	}
	
	bool	IsFull()
	{
		unsigned short temp=Tail+1;
		return temp==Len?Head==0:temp==Head;
	}
	bool 	IsEmpty()
	{
    bool bResult;
    
    m_cCriticalSection.Lock();
    Head==Tail ? bResult=true : bResult=false;
    m_cCriticalSection.Unlock();

		return bResult;
	}
  VXML_LGOINOUT_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
  	if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(US ServerId, US ClientId, UC NodeType, US NodeId, UC LoginOut);
  unsigned short  Read(US &ServerId, US &ClientId, UC &NodeType, US &NodeId, UC &LoginOut);
  unsigned short  Read(VXML_LGOINOUT_STRUCT *LogInOutMsg);
};

//---------------------------------------------------------------------------
#endif
