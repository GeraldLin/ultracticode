////////////////////////////////////////////////////////////////////////
//
// odsErrorCode.h
//
//		ODS 錯誤代碼和錯誤信息表
//
//
////////////////////////////////////////////////////////////////////////

#if !defined(_ODS_20_ERROR_CODE_H_)
#define _ODS_20_ERROR_CODE_H_


// 錯誤碼基點
#define ODS_ERR_BASE			6000

// 以下是所有錯誤代碼以及錯誤信息的定義
#define ODS_ERR_OK				0
#define ODS_MSG_OK				_T("成功")	

#define ODS_ERR_NET				( ODS_ERR_BASE + 1 )
#define ODS_MSG_NET				_T("網絡通訊錯誤")

#define ODS_ERR_DUPBATCH		( ODS_ERR_BASE + 2 )
#define ODS_MSG_DUPBATCH		_T("批信息重復")

#define ODS_ERR_NOSRV			( ODS_ERR_BASE + 3 )
#define ODS_MSG_NOSRV			_T("在指定的計算機上無法找到文檔服務")

#define ODS_ERR_BADPARAM		( ODS_ERR_BASE + 4 )
#define ODS_MSG_BADPARAM		_T("參數非法")

#define ODS_ERR_CREATEDIR		( ODS_ERR_BASE + 6 )
#define ODS_MSG_CREATEDIR		_T("無法創建目錄")

#define ODS_ERR_OVERWRITE		( ODS_ERR_BASE + 7 )
#define ODS_MSG_OVERWRITE		_T("目錄或文件已經存在，無法覆蓋")

#define ODS_ERR_EMPTYDIR		( ODS_ERR_BASE + 8 )
#define ODS_MSG_EMPTYDIR		_T("目錄為空")

#define ODS_ERR_NAMEOVERFLOW	( ODS_ERR_BASE + 9 )
#define ODS_MSG_NAMEOVERFLOW	_T("目錄名或文件名太長，操作無法完成")

#define ODS_ERR_COPYDIR			( ODS_ERR_BASE + 10 )
#define ODS_MSG_COPYDIR			_T("復制目錄時文件系統發生錯誤")

#define ODS_ERR_REMOVEDIR		( ODS_ERR_BASE + 11 )
#define ODS_MSG_REMOVEDIR		_T("刪除目錄時文件系統發生錯誤")

#define ODS_ERR_MOVEDIR			( ODS_ERR_BASE + 12 )
#define ODS_MSG_MOVEDIR			_T("移動目錄時文件系統發生錯誤")

#define ODS_ERR_NODIR			( ODS_ERR_BASE + 13 )
#define ODS_MSG_NODIR			_T("指定的目錄不存在")

#define ODS_ERR_NOFILE			( ODS_ERR_BASE + 14 )
#define ODS_MSG_NOFILE			_T("文件不存在")

#define ODS_ERR_BROWSEDIR		( ODS_ERR_BASE + 15 )
#define ODS_MSG_BROWSEDIR		_T("瀏覽目錄時文件系統發生錯誤")

#define ODS_ERR_READFILE		( ODS_ERR_BASE + 16 )
#define ODS_MSG_READFILE		_T("讀文件時錯誤")

#define ODS_ERR_WRITEFILE		( ODS_ERR_BASE + 17 )
#define ODS_MSG_WRITEFILE		_T("寫文件時錯誤")

#define ODS_ERR_OPENFILE		( ODS_ERR_BASE + 18 )
#define ODS_MSG_OPENFILE		_T("打開文件時錯誤")

#define ODS_ERR_POSFILE			( ODS_ERR_BASE + 19 )
#define ODS_MSG_POSFILE			_T("移動文件指針失敗")

#define ODS_ERR_MALLOC			( ODS_ERR_BASE + 20 )
#define ODS_MSG_MALLOC			_T("內存不足，分配內存錯誤")

#define ODS_ERR_RECURSUBDIR		( ODS_ERR_BASE + 21 )
#define ODS_MSG_RECURSUBDIR		_T("要歸并的目錄中有子目錄，無法完成歸并")

#define ODS_ERR_LARGE			( ODS_ERR_BASE + 22 )
#define ODS_MSG_LARGE			_T("文件太大，操作無法完成")

#define ODS_ERR_UNKNOWNPACK		( ODS_ERR_BASE + 23 )
#define ODS_MSG_UNKNOWNPACK		_T("卷的歸并格式無法識別")

#define ODS_ERR_SETATTR			( ODS_ERR_BASE + 24 )
#define ODS_MSG_SETATTR			_T("無法設置文件屬性")

#define ODS_ERR_OPENSCM			( ODS_ERR_BASE + 25 )
#define ODS_MSG_OPENSCM			_T("無法打開指定計算機上的服務控制器")

#define ODS_ERR_OPENSERVICE		( ODS_ERR_BASE + 26 )
#define ODS_MSG_OPENSERVICE		_T("無法打開指定的服務")

#define ODS_ERR_DELSERVICE		( ODS_ERR_BASE + 27 )
#define ODS_MSG_DELSERVICE		_T("無法刪除指定的服務")

#define ODS_ERR_STOPSERVICE		( ODS_ERR_BASE + 28 )
#define ODS_MSG_STOPSERVICE		_T("無法停止指定的服務")

#define ODS_ERR_REPORTSTATUS	( ODS_ERR_BASE + 29 )
#define ODS_MSG_REPORTSTATUS	_T("無法向服務控制器報告服務狀態")

#define ODS_ERR_INSTSERVICE		( ODS_ERR_BASE + 30 )
#define ODS_MSG_INSTSERVICE		_T("無法安裝指定的服務")

#define ODS_ERR_STARTSERVICE	( ODS_ERR_BASE + 31 )
#define ODS_MSG_STARTSERVICE	_T("無法啟動指定的服務")

#define ODS_ERR_QUERYSERVICE	( ODS_ERR_BASE + 32 )
#define ODS_MSG_QUERYSERVICE	_T("無法取得指定服務的狀態信息")

#define ODS_ERR_INITMFC			( ODS_ERR_BASE + 33 )
#define ODS_MSG_INITMFC			_T("程序無法運行:可能是系統中安裝的 MFC 庫的版本太舊")

#define ODS_ERR_STARTSVCDISP	( ODS_ERR_BASE + 34 )
#define ODS_MSG_STARTSVCDISP	_T("無法啟動服務程序的主函數")

#define ODS_ERR_REGSVCHCTRL		( ODS_ERR_BASE + 35 )
#define ODS_MSG_REGSVCHCTRL		_T("無法注冊服務程序的控制函數")

#define ODS_ERR_RPCUSEPROTOCOL	( ODS_ERR_BASE + 36 )
#define ODS_MSG_RPCUSEPROTOCOL	_T("無法配置 RPC 使用的協議")

#define ODS_ERR_RPCREGIF		( ODS_ERR_BASE + 37 )
#define ODS_MSG_RPCREGIF		_T("無法注冊 RPC 接口")

#define ODS_ERR_RPCLISTEN		( ODS_ERR_BASE + 38 )
#define ODS_MSG_RPCLISTEN		_T("無法開始 RPC 偵聽")

#define ODS_ERR_REGISTRY		( ODS_ERR_BASE + 39 )
#define ODS_MSG_REGISTRY		_T("操作注冊表失敗")

#define ODS_ERR_CREATEMUTEX		( ODS_ERR_BASE + 40 )
#define ODS_MSG_CREATEMUTEX		_T("無法創建互斥量")

#define ODS_ERR_MAXCONNECT		( ODS_ERR_BASE + 41 )
#define ODS_MSG_MAXCONNECT		_T("受客戶連接數或數據庫連接數限制，無法響應客戶的連接請求")

#define ODS_ERR_MAXDOCOPENED	( ODS_ERR_BASE + 42 )
#define ODS_MSG_MAXDOCOPENED	_T("受同一客戶同時打開的文檔數限制，無法打開更多的文檔")

#define ODS_ERR_LOSTCLIENT		( ODS_ERR_BASE + 43 )
#define ODS_MSG_LOSTCLIENT		_T("客戶連接異常中斷，可能是客戶程序發生了異常")

#define ODS_ERR_RPCBINDING		( ODS_ERR_BASE + 44 )
#define ODS_MSG_RPCBINDING		_T("無法連接文檔服務:無法完成 RPC 綁定")

#define ODS_ERR_PING			( ODS_ERR_BASE + 45 )
#define ODS_MSG_PING			_T("無法連接文檔服務:服務可能沒有運行")

#define ODS_ERR_GETCOMPUTER		( ODS_ERR_BASE + 46 )
#define ODS_MSG_GETCOMPUTER		_T("無法得到計算機名")

#define ODS_ERR_DBCONNECT		( ODS_ERR_BASE + 47 )
#define ODS_MSG_DBCONNECT		_T("無法連接數據庫")

#define ODS_ERR_DATABASE		( ODS_ERR_BASE + 48 )
#define ODS_MSG_DATABASE		_T("數據庫操作失敗")

#define ODS_ERR_FILETRANS		( ODS_ERR_BASE + 49 )
#define ODS_MSG_FILETRANS		_T("文件傳輸時錯誤，發送和接受的字節數不匹配")

#define ODS_ERR_NOSPACE			( ODS_ERR_BASE + 50 )
#define ODS_MSG_NOSPACE			_T("設備上已沒有可用空間，無法接收或移動文檔")

#define ODS_ERR_NOCDGROUP		( ODS_ERR_BASE + 51 )
#define ODS_MSG_NOCDGROUP		_T("指定的光盤組不存在")

#define ODS_ERR_NOVOLUME		( ODS_ERR_BASE + 52 )
#define ODS_MSG_NOVOLUME		_T("指定的卷不存在")

#define ODS_ERR_NODOCUMENT		( ODS_ERR_BASE + 53 )
#define ODS_MSG_NODOCUMENT		_T("指定的文檔不存在，或正被其他程序使用")

#define ODS_ERR_NODEVICE		( ODS_ERR_BASE + 54 )
#define ODS_MSG_NODEVICE		_T("指定的設備不存在")

#define ODS_ERR_LOCATION		( ODS_ERR_BASE + 55 )
#define ODS_MSG_LOCATION		_T("定位信息格式不正確")

#define ODS_ERR_DIFFPACK		( ODS_ERR_BASE + 56 )
#define ODS_MSG_DIFFPACK		_T("要求的歸并格式和卷當前的歸并格式不符")

#define ODS_ERR_UNZIP			( ODS_ERR_BASE + 57 )
#define ODS_MSG_UNZIP			_T("無法從 ZIP 文件中解壓縮出指定的文件")

#define ODS_ERR_TEMPFILE		( ODS_ERR_BASE + 58 )
#define ODS_MSG_TEMPFILE		_T("操作系統無法創建臨時文件")

#define ODS_ERR_DEVREADONLY		( ODS_ERR_BASE + 59 )
#define ODS_MSG_DEVREADONLY		_T("文檔處于只讀介質(如光盤)上，無法修改")

#define ODS_ERR_DOCREADONLY		( ODS_ERR_BASE + 60 )
#define ODS_MSG_DOCREADONLY		_T("文檔以只讀方式打開，無法修改")

#define ODS_ERR_SINGLEBACK		( ODS_ERR_BASE + 61 )
#define ODS_MSG_SINGLEBACK		_T("文檔中不能存在孤立的負數頁號")

#define ODS_ERR_FILENAMEDUP		( ODS_ERR_BASE + 62 )
#define ODS_MSG_FILENAMEDUP		_T("文檔內不能有重名文件")

#define ODS_ERR_VOLMAKECD		( ODS_ERR_BASE + 63 )
#define ODS_MSG_VOLMAKECD		_T("文檔所在的卷正在刻盤，拒絕訪問")

#define ODS_ERR_DEVNOTREADONLY	( ODS_ERR_BASE + 64 )
#define ODS_MSG_DEVNOTREADONLY	_T("文檔并非處于只讀介質(如光盤)上，無法廢棄或增補")

#define ODS_ERR_NEEDNAMEONCOPY	( ODS_ERR_BASE + 65 )
#define ODS_MSG_NEEDNAMEONCOPY	_T("在同一光盤組中復制文檔時，必須給出新的文檔名稱")


#endif //_ODS_20_ERROR_CODE_H_