//---------------------------------------------------------------------------
#ifndef __SQLSFIFO_H_
#define __SQLSFIFO_H_
//---------------------------------------------------------------------------
#include <afxmt.h>
//---------------------------------------------------------------------------

typedef struct
{
  unsigned short MsgType;
  char SeatNo[32];
  char WorkerNo[32];
  char SqlsBuf[1024];
  char MsgBuf[1024];

}VXML_SQLS_STRUCT;

class CSQLSfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_SQLS_STRUCT          *pSqls;
  
  CCriticalSection m_cCriticalSection;
  
public:
  CSQLSfifo(unsigned short len)
  {
    Head=0;
    Tail=0;
    resv=0;
    Len=len;
    pSqls=new VXML_SQLS_STRUCT[Len];
  }
  
  ~CSQLSfifo()
  {
    if (pSqls != NULL)
    {
      delete []pSqls;
      pSqls = NULL;
    }
  }
  
  bool	IsFull()
  {
    unsigned short temp=Tail+1;
    return temp==Len?Head==0:temp==Head;
  }
  bool 	IsEmpty()
  {
    bool bResult;
    
    m_cCriticalSection.Lock();
    Head==Tail ? bResult=true : bResult=false;
    m_cCriticalSection.Unlock();
    
    return bResult;
  }
  VXML_SQLS_STRUCT &GetOnHead()
  {return pSqls[Head];}
  
  void DiscardHead()
  {	
    if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(const char *SeatNo, const char *WorkerNo, const char *SqlsBuf, const char *MsgBuf, unsigned short MsgType);
  unsigned short  Read(char *SeatNo, char *WorkerNo, char *SqlsBuf, char *MsgBuf, unsigned short &MsgType);
};

//---------------------------------------------------------------------------
#endif
