// TCPClient.h: interface for the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TCPDBSERVER_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_)
#define AFX_TCPDBSERVER_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_GW_NUM  16

class CTCPClient  
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnAppLogin,
                          ONAPPCLOSE OnAppClose,
                          ONAPPRECEIVEDATA OnAppReceiveData
                          );
private:
	TServer  DBServer; //IVR流程服務器
  TServer  IMServer; //IM數據網關

  void ReleaseDll();
public:
	CTCPClient();
	virtual ~CTCPClient();
  unsigned short ClientId;
  unsigned short SessionId;

  unsigned short GetDBServerId()
  {return DBServer.Serverid;}
  void SetDBLinked() {DBServer.IsConnected=true;}
  void SetDBUnlink() {DBServer.IsConnected=false;}
  bool IsDBConnected()
  {return DBServer.IsConnected;}

  unsigned short GetIMServerId()
  {return IMServer.Serverid;}
  void SetIMLinked() {IMServer.IsConnected=true;}
  void SetIMUnlink() {IMServer.IsConnected=false;}
  bool IsIMConnected()
  {return IMServer.IsConnected;}

  char m_szAppPath[MAX_PATH_LEN];
  char m_szDllFile[MAX_PATH_LEN];
  bool LoadDll();
  bool IsOpen; //dll is opend

  bool ReadIni(const char *filename);
  bool ReadWebSetupIni(const char *filename);

  int ConnectDBServer();
  int ConnectIMServer();
  int ConnectServer();

	//申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;

  int SendMessage(unsigned short ServerId, unsigned short MsgId, int MsgLen, const unsigned char *MsgBuf);
  int DBSendMessage(unsigned short MsgId, const char *MsgBuf);
  int IMSendMessage(unsigned short MsgId, const char *MsgBuf);

  //組合對象編碼
  unsigned short CombineID( unsigned char ObjectType, unsigned char ObjectNo );
	//組合消息類型及消息編號
  unsigned short CombineMessageID(unsigned char MsgType, unsigned char MsgNo);
  //分解對象標識及編號
  void GetClientID(unsigned short Msg_wParamHi, unsigned char &ClientType, unsigned char &ClientNo);
  //分解消息類型及消息編號
  void GetMsgID(unsigned short Msg_wParamLo, unsigned char &MsgType, unsigned char &MsgNo);
};
//tcllinkc.dll callback function,
extern void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnDBLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnDBClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnIMReceiveData(unsigned short remoteid,unsigned short msgtype,
                            const unsigned char *buf,
                            unsigned long len); //need define in main.cpp
extern void OnIMLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIMClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

//---------------------------------------------------------------------------------
#endif // !defined(AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_)
