//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procctilink.h"
#include "extern.h"
//---------------------------------------------------------------------------
//發送消息到交換機
void SendMsg2SWT(US ClientId, US MsgId, const CH *msg)
{
  pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRSWT<<8) | MsgId, msg);
  MyTrace(2, "0x%04x %s", ClientId, msg);
}
void SendMsg2SWT(US ClientId, US MsgId, const CStringX &msg)
{
  SendMsg2SWT(ClientId, MsgId, msg.C_Str());
}

int Proc_Msg_From_SWT(CXMLMsg &SWTMsg)
{
  unsigned short MsgId=SWTMsg.GetMsgId();
  
  if(MsgId >= MAX_SWTIVRMSG_NUM)	//IVR-->SWITCH
  {
    //指令編號超出范圍
    MyTrace(0, "SWTmsgid = %d is out of range", MsgId);
    return 1;
  }
  
  if(0!=SWTMsg.ParseRevMsg(pFlwRule->SWTIVRMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(0, "parse SWT msg error: MsgId = %d MsgBuf = %s", MsgId, SWTMsg.GetBuf().C_Str());
    return 2;
  }
  return 0;
}
//設置發送到SWT的消息頭
void Set_IVR2SWT_Header(int MsgId)
{
  IVRSndMsg.GetBuf().Format("<%s switchid='%d'", pFlwRule->IVRSWTMsgRule[MsgId].MsgNameEng, 1);
}
void Set_IVR2SWT_Header(int MsgId, int nSwitchID)
{
  IVRSndMsg.GetBuf().Format("<%s switchid='%ld'", pFlwRule->IVRSWTMsgRule[MsgId].MsgNameEng, nSwitchID);
}
//增加發送到AG的消息屬性
void Set_IVR2SWT_Item(int MsgId, int AttrId, const char *value)
{
  IVRSndMsg.AddItemToBuf(pFlwRule->IVRSWTMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2SWT_Item(int MsgId, int AttrId, const CStringX &value)
{
  IVRSndMsg.AddItemToBuf(pFlwRule->IVRSWTMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2SWT_Item(int MsgId, int AttrId, int value)
{
  IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRSWTMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2SWT_Item(int MsgId, int AttrId, long value)
{
  IVRSndMsg.AddLongItemToBuf(pFlwRule->IVRSWTMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2SWT_Item(int MsgId, int AttrId, bool value)
{
  if (value == true)
  {
    IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRSWTMsgRule[MsgId].Attribute[AttrId].AttrNameEng, 1);
  }
  else
  {
    IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRSWTMsgRule[MsgId].Attribute[AttrId].AttrNameEng, 0);
  }
}
void Set_IVR2SWT_Tail()
{
  IVRSndMsg.AddTailToBuf();
}

void ReadSwitchPortFlwCode(short nPortID)
{
  char szTemp[16], szItemList[128];
  CStringX strItemList[5];

  if (pBoxchn->pChn_str[nPortID].ChStyle != CH_A_EXTN)
  {
    return;
  }
  sprintf(szTemp, "PORT[%d]", nPortID);
  GetPrivateProfileString("PORT", szTemp, "", szItemList, 128, g_szCTILINKINIFileName);
  if (strlen(szItemList) > 0)
  {
    if (SplitString(szItemList, ';', 5, strItemList) >= 5)
    {
      if (strlen(pIVRCfg->PSTNCode[pBoxchn->pChn_str[nPortID].ChStyleIndex]) == 0)
      {
        strncpy(pIVRCfg->PSTNCode[pBoxchn->pChn_str[nPortID].ChStyleIndex], strItemList[4].C_Str(), 19);
      }
    }
  }
}
void BandVopChnByCustPhone(int nChn)
{
  int nChn1;

  if (pVopGroup == NULL)
    return;
  nChn1 = pVopGroup->GetChnNoByCustPhone(pChn->CustPhone);
  MyTrace(3, "BandVopChnByCustPhone nChn=%d nChn1=%d CustPhone=%s", 
    nChn, nChn1, pChn->CustPhone);
  if (pBoxchn->isnChnAvail(nChn1))
  {
    pChn->nSwtPortID = nChn1;
    pChn->SwtLinkType[0] = 3;
    pChn->SwtLinkChn[0] = nChn1;
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      strcpy(pChn1->CallerNo, pChn->CallerNo);
      strcpy(pChn1->CalledNo, pChn->CalledNo);
      pChn1->OrgCallerNo = pChn->OrgCallerNo;
      pChn1->OrgCalledNo = pChn->OrgCalledNo;
      pChn1->timer = 0;
      pChn1->CallTime = pChn->CallTime;
      pChn1->AnsTime = time(0);
      pChn1->ssState = CHN_SNG_IN_TALK;
      pChn1->lnState = CHN_LN_SEIZE; //占用
      if (pChn1->CallInOut == 0)
      {
        if (pChn->CallInOut == 2)
          pChn1->CallInOut = CALL_OUT;
        else
          pChn1->CallInOut = CALL_IN;
      }
      
      MyTrace(3, "BandVopChnByCustPhone nChn=%d nChn1=%d CustPhone=%s success", 
        nChn, nChn1, pChn->CustPhone);
      DispChnStatus(nChn1);
    }
  }
}
//--------------------------發送消息到CTILinkService處理---------------------------------------------------
//連接CTILinkService
void Send_SWTMSG_connectlink()
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_connectlink);
  Set_IVR2SWT_Item(SWTMSG_connectlink, 2, 10);
  Set_IVR2SWT_Item(SWTMSG_connectlink, 3, "");
  Set_IVR2SWT_Item(SWTMSG_connectlink, 4, "");
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_connectlink, IVRSndMsg.GetBuf());
}
//發送設置CTILINK控制的端口邏輯編號
void Send_SWTMSG_setlinkportid(int nChn)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_setportstatus);
  Set_IVR2SWT_Item(SWTMSG_setportstatus, 2, nChn);
  Set_IVR2SWT_Item(SWTMSG_setportstatus, 3, pChn->DeviceID);
  Set_IVR2SWT_Item(SWTMSG_setportstatus, 4, 255);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_setportstatus, IVRSndMsg.GetBuf());
}
//設置坐席狀態 AM_LOG_IN = 0,AM_LOG_OUT = 1,AM_NOT_READY = 2,AM_READY = 3,AM_WORK_NOT_READY = 4,AM_WORK_READY = 5
void Send_SWTMSG_setagentstatus(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, LPCTSTR pszPassword, short nStatus)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_setagentstatus);
  Set_IVR2SWT_Item(SWTMSG_setagentstatus, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_setagentstatus, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_setagentstatus, 4, pszAgentID);
  Set_IVR2SWT_Item(SWTMSG_setagentstatus, 5, pszGroupID);
  Set_IVR2SWT_Item(SWTMSG_setagentstatus, 6, pszPassword);
  Set_IVR2SWT_Item(SWTMSG_setagentstatus, 7, nStatus);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_setagentstatus, IVRSndMsg.GetBuf());
}
//設置轉移號碼 nTranType: 0-取消所有轉移 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移 4-同振綁定
void Send_SWTMSG_settranphone(short nPortID, const char *pszDeviceID, short nTranType, const char *pszTranPhone)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_settranphone);
  Set_IVR2SWT_Item(SWTMSG_settranphone, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_settranphone, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_settranphone, 4, nTranType);
  Set_IVR2SWT_Item(SWTMSG_settranphone, 5, pszTranPhone);
  Set_IVR2SWT_Tail();

  SendMsg2SWT(CTILinkClientId, SWTMSG_settranphone, IVRSndMsg.GetBuf());
}
//設置留言指示燈
void Send_SWTMSG_setlampstatus(short nPortID, const char *pszDeviceID, short nStatus)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_setlampstatus);
  Set_IVR2SWT_Item(SWTMSG_setlampstatus, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_setlampstatus, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_setlampstatus, 4, nStatus);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_setlampstatus, IVRSndMsg.GetBuf());
}
//應答來話
void Send_SWTMSG_answercall(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_answercall);
  Set_IVR2SWT_Item(SWTMSG_answercall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_answercall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_answercall, 4, nConnID);
  Set_IVR2SWT_Tail();

  SendMsg2SWT(CTILinkClientId, SWTMSG_answercall, IVRSndMsg.GetBuf());
}
//拒絕來話
void Send_SWTMSG_refusecall(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_refusecall);
  Set_IVR2SWT_Item(SWTMSG_refusecall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_refusecall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_refusecall, 4, nConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_refusecall, IVRSndMsg.GetBuf());
}
//發起呼叫 2016-07-31 增加傳遞外呼字冠
void Send_SWTMSG_makecall(short nPortID, const char *pszDeviceID, long nCallID, const char *pszCallerNo, const char *pszCalledNo, short nRouteNo, const char *pszCallData, int nCalledType, const char *pszPreCode)
{
  char szCalledNo[128];
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_makecall);
  Set_IVR2SWT_Item(SWTMSG_makecall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_makecall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_makecall, 4, nCallID);
  Set_IVR2SWT_Item(SWTMSG_makecall, 5, pszCallerNo);
  //如果是外線則自動判斷是否加撥字冠
  if (nCalledType == 1)
  {
    if (strlen(pszPreCode) == 0) //2016-07-31 增加傳遞外呼字冠判斷
      AutoAddCallOutPreCode(pszCalledNo, szCalledNo, 0);
    else
      sprintf(szCalledNo, "%s%s", pszPreCode, pszCalledNo);
    
    Set_IVR2SWT_Item(SWTMSG_makecall, 6, szCalledNo);
  }
  else
  {
    sprintf(szCalledNo, "%s%s", pszPreCode, pszCalledNo);
    Set_IVR2SWT_Item(SWTMSG_makecall, 6, szCalledNo);
  }
  
  Set_IVR2SWT_Item(SWTMSG_makecall, 7, nRouteNo);
  Set_IVR2SWT_Item(SWTMSG_makecall, 8, pszCallData);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_makecall, IVRSndMsg.GetBuf());
}
//將來中繼來話綁定到分機
void Send_SWTMSG_bandcall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_bandcall);
  Set_IVR2SWT_Item(SWTMSG_bandcall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_bandcall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_bandcall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_bandcall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_bandcall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_bandcall, IVRSndMsg.GetBuf());
}
//將來中繼來話綁定到提示語音
void Send_SWTMSG_bandvoice(short nPortID, const char *pszDeviceID, long nConnID, short nVoiceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_bandvoice);
  Set_IVR2SWT_Item(SWTMSG_bandvoice, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_bandvoice, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_bandvoice, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_bandvoice, 5, nVoiceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_bandvoice, IVRSndMsg.GetBuf());
}
//保持電話
void Send_SWTMSG_holdcall(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_holdcall);
  Set_IVR2SWT_Item(SWTMSG_holdcall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_holdcall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_holdcall, 4, nConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_holdcall, IVRSndMsg.GetBuf());
}
//取消保持電話
void Send_SWTMSG_unholdcall(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_unholdcall);
  Set_IVR2SWT_Item(SWTMSG_unholdcall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_unholdcall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_unholdcall, 4, nConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_unholdcall, IVRSndMsg.GetBuf());
}
//轉接電話
void Send_SWTMSG_transfercall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID, short nTranType, const char *pszCallData, int nCalledType)
{
  char szCalledNo[128];
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_transfercall);
  Set_IVR2SWT_Item(SWTMSG_transfercall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_transfercall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_transfercall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_transfercall, 5, nDesPortID);

  //如果是外線則自動判斷是否加撥字冠
  if (nCalledType == 1)
  {
    AutoAddCallOutPreCode(pszDesDeviceID, szCalledNo, 1);
    Set_IVR2SWT_Item(SWTMSG_transfercall, 6, szCalledNo);
  }
  else
  {
    Set_IVR2SWT_Item(SWTMSG_transfercall, 6, pszDesDeviceID);
  }
  Set_IVR2SWT_Item(SWTMSG_transfercall, 7, nTranType);

  //2015-10-24 增加傳送UCID
  if (pIVRCfg->isCDRInsertCallUCID == true)
  {
    int nChn = nPortID;
    char szTemp[2048];
    if (strlen(pChn->CallUCID) == 0)
    {
      strcpy(pChn->CallUCID, pChn->CdrSerialNo);
    }
    sprintf(szTemp, "$S_UCID=%s;%s", pChn->CallUCID, pszCallData);
    Set_IVR2SWT_Item(SWTMSG_transfercall, 8, szTemp);
  }
  else
  {
    Set_IVR2SWT_Item(SWTMSG_transfercall, 8, pszCallData);
  }

  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_transfercall, IVRSndMsg.GetBuf());
}
//停止轉接電話
void Send_SWTMSG_stoptransfer(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_stoptransfer);
  Set_IVR2SWT_Item(SWTMSG_stoptransfer, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_stoptransfer, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_stoptransfer, 4, nConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_stoptransfer, IVRSndMsg.GetBuf());
}
//穿梭通話
void Send_SWTMSG_swapcall(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_swapcall);
  Set_IVR2SWT_Item(SWTMSG_swapcall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_swapcall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_swapcall, 4, nConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_swapcall, IVRSndMsg.GetBuf());
}
//代接電話
void Send_SWTMSG_pickupcall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_pickupcall);
  Set_IVR2SWT_Item(SWTMSG_pickupcall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_pickupcall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_pickupcall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_pickupcall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_pickupcall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_pickupcall, IVRSndMsg.GetBuf());
}
//強插通話
void Send_SWTMSG_breakincall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_breakincall);
  Set_IVR2SWT_Item(SWTMSG_breakincall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_breakincall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_breakincall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_breakincall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_breakincall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_breakincall, IVRSndMsg.GetBuf());
}
//監聽電話
void Send_SWTMSG_listencall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_listencall);
  Set_IVR2SWT_Item(SWTMSG_listencall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_listencall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_listencall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_listencall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_listencall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_listencall, IVRSndMsg.GetBuf());
}
//強拆電話
void Send_SWTMSG_removecall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_removecall);
  Set_IVR2SWT_Item(SWTMSG_removecall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_removecall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_removecall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_removecall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_removecall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_removecall, IVRSndMsg.GetBuf());
}
//三方通話
void Send_SWTMSG_threeconf(short nPortID, const char *pszDeviceID, long nHeldConnID, long nActConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_threeconf);
  Set_IVR2SWT_Item(SWTMSG_threeconf, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_threeconf, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_threeconf, 4, nHeldConnID);
  Set_IVR2SWT_Item(SWTMSG_threeconf, 5, nActConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_threeconf, IVRSndMsg.GetBuf());
}
//釋放呼叫
void Send_SWTMSG_releasecall(short nPortID, const char *pszDeviceID, long nConnID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_releasecall);
  Set_IVR2SWT_Item(SWTMSG_releasecall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_releasecall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_releasecall, 4, nConnID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_releasecall, IVRSndMsg.GetBuf());
}
//分機掛機
void Send_SWTMSG_hangup(short nPortID, const char *pszDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_hangup);
  Set_IVR2SWT_Item(SWTMSG_hangup, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_hangup, 3, pszDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_hangup, IVRSndMsg.GetBuf());
}
//將電話保持到話務臺隊列
void Send_SWTMSG_putacdqueuecall(short nPortID, const char *pszDeviceID, long nConnID, short nQueueNo)
{

}
//從話務臺隊列提取電話
void Send_SWTMSG_getacdqueuecall(short nPortID, const char *pszDeviceID, long nConnID, short nQueueNo)
{

}
//發送交換機操作指令
void Send_SWTMSG_sendswitchcmd(short nPortID, const char *pszDeviceID, short nCmdID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_sendswitchcmd);
  Set_IVR2SWT_Item(SWTMSG_sendswitchcmd, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_sendswitchcmd, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_sendswitchcmd, 4, nCmdID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_sendswitchcmd, IVRSndMsg.GetBuf());
}
//發送DTMF指令
void Send_SWTMSG_senddtmf(short nPortID, const char *pszDeviceID, long nConnID, const char *pszDTMFs)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_senddtmf);
  Set_IVR2SWT_Item(SWTMSG_senddtmf, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_senddtmf, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_senddtmf, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_senddtmf, 5, pszDTMFs);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_senddtmf, IVRSndMsg.GetBuf());
}
//接管其他坐席的電話
void Send_SWTMSG_takeovercall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_takeovercall);
  Set_IVR2SWT_Item(SWTMSG_takeovercall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_takeovercall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_takeovercall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_takeovercall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_takeovercall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_takeovercall, IVRSndMsg.GetBuf());
}
//電話重新定向振鈴
void Send_SWTMSG_redirectcall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_redirectcall);
  Set_IVR2SWT_Item(SWTMSG_redirectcall, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_redirectcall, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_redirectcall, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_redirectcall, 5, nDesPortID);
  Set_IVR2SWT_Item(SWTMSG_redirectcall, 6, pszDesDeviceID);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_redirectcall, IVRSndMsg.GetBuf());
}
//加入會議
void Send_SWTMSG_addtoconf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, long nDesConnID, long nConfID)
{

}
//退出會議
void Send_SWTMSG_removefromconf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, long nDesConnID, long nConfID)
{

}
//發送VOC狀態到交換機
void Send_SWTMSG_sendivrstatus(short nVopNo, short nStatus)
{
  if (CTILinkClientId == 0 || pVopGroup == NULL)
    return;
  if (nVopNo >= pVopGroup->m_nVopNum)
    return;
  Set_IVR2SWT_Header(SWTMSG_sendivrstatus);
  Set_IVR2SWT_Item(SWTMSG_sendivrstatus, 2, pVopGroup->m_pVops[nVopNo].m_szIVRHostId);
  Set_IVR2SWT_Item(SWTMSG_sendivrstatus, 3, nStatus);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_sendivrstatus, IVRSndMsg.GetBuf());
}
//發送啟動錄音
void Send_SWTMSG_recordfile(short nPortID, const char *pszDeviceID, long nConnID, const char *pszFileName, long nRecdLength, short nRecdParam, LPCTSTR pszRule)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_recordfile);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 5, pszFileName);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 6, nRecdLength);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 7, nRecdParam);
  Set_IVR2SWT_Item(SWTMSG_recordfile, 8, pszRule);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_recordfile, IVRSndMsg.GetBuf());
}
//發送停止錄音
void Send_SWTMSG_stoprecord(short nPortID, const char *pszDeviceID, long nConnID, short nStopFlag)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_stoprecord);
  Set_IVR2SWT_Item(SWTMSG_stoprecord, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_stoprecord, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_stoprecord, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_stoprecord, 5, nStopFlag);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_stoprecord, IVRSndMsg.GetBuf());
}
//文件放音
void Send_SWTMSG_playfile(short nPortID, const char *pszDeviceID, long nConnID, const char *pszFileName, short nPlayParam, LPCTSTR pszRule)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_playfile);
  Set_IVR2SWT_Item(SWTMSG_playfile, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_playfile, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_playfile, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_playfile, 5, pszFileName);
  Set_IVR2SWT_Item(SWTMSG_playfile, 6, nPlayParam);
  Set_IVR2SWT_Item(SWTMSG_playfile, 7, pszRule);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_playfile, IVRSndMsg.GetBuf());
}
//索引放音
void Send_SWTMSG_playindex(short nPortID, const char *pszDeviceID, long nConnID, short nIndex, short nPlayParam, LPCTSTR pszRule)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_playindex);
  Set_IVR2SWT_Item(SWTMSG_playindex, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_playindex, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_playindex, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_playindex, 5, nIndex);
  Set_IVR2SWT_Item(SWTMSG_playindex, 6, nPlayParam);
  Set_IVR2SWT_Item(SWTMSG_playindex, 7, pszRule);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_playindex, IVRSndMsg.GetBuf());
}
//停止放音
void Send_SWTMSG_stopplay(short nPortID, const char *pszDeviceID, long nConnID, short nStopFlag)
{
  if (CTILinkClientId == 0)
    return;
  Set_IVR2SWT_Header(SWTMSG_stopplay);
  Set_IVR2SWT_Item(SWTMSG_stopplay, 2, nPortID);
  Set_IVR2SWT_Item(SWTMSG_stopplay, 3, pszDeviceID);
  Set_IVR2SWT_Item(SWTMSG_stopplay, 4, nConnID);
  Set_IVR2SWT_Item(SWTMSG_stopplay, 5, nStopFlag);
  Set_IVR2SWT_Tail();
  
  SendMsg2SWT(CTILinkClientId, SWTMSG_stopplay, IVRSndMsg.GetBuf());
}

//--------------------------CTILinkService發來的消息處理---------------------------------------------------
//連接CTILinkService結果
int Proc_SWTMSG_onconnectlink(CXMLMsg &SWTMsg)
{
	short nResult;

  nResult = atoi(SWTMsg.GetAttrValue(4).C_Str());
  //MyTrace(3, "Proc_SWTMSG_onconnectlink result=%d", nResult);

  SendRunStatusToClient(CTILinkClientId, 0x0222);

  g_nSwitchType = g_nSwitchMode; //atoi(SWTMsg.GetAttrValue(3).C_Str());
  if (nResult == 0)
  {
    SWTConnectedId = true;
  }
  else
  {
    SWTConnectedId = false;
    
    if (g_nSwitchMode != 0)
    {
      pBoxchn->ResetAllChn();
      ResetAllAG();
      if (pVopGroup != NULL)
        pVopGroup->ReleaseAllVop();
    }
  }
  return 0;
}
//返回CTILink接口與交換機的連接狀態
int Proc_SWTMSG_onlinkstatus(CXMLMsg &SWTMsg)
{
  g_nCTILinkStatus = atoi(SWTMsg.GetAttrValue(3).C_Str());
  if (g_nCTILinkStatus == 0)
  {
    g_dwRegServerState = g_dwRegServerState & 0xEF;
    WriteRegServiceRunState();
    WriteAlarmList(1002, ALARM_LEVEL_MINOR, 0, "CTILink Action");
  }
  else if (g_nCTILinkStatus == 1)
  {
    g_dwRegServerState = g_dwRegServerState | 0x10;
    WriteRegServiceRunState();
    WriteAlarmList(1001, ALARM_LEVEL_MAJOR, 1, "CTILink broken");
  }
  else if (g_nCTILinkStatus == 8)
  {
    g_dwRegServerState = g_dwRegServerState | 0x10;
    WriteRegServiceRunState();
    WriteAlarmList(1001, ALARM_LEVEL_MAJOR, 1, "AES fail");
  }
  else if (g_nCTILinkStatus == 9)
  {
    g_dwRegServerState = g_dwRegServerState | 0x10;
    WriteRegServiceRunState();
    WriteAlarmList(1001, ALARM_LEVEL_MAJOR, 1, "CTILink flash broken");
  }
  if (g_nMultiRunMode == 0 || g_nCTILINKDownSwitchOverId == 0) //2016-10-05
  {
    return 0;
  }
  //2016-05-19如果本機網絡沒有故障，那就不應切換了，可能是AES的問題
  if (CheckLocaNetCable(g_szAdapterName) == 0)
  {
    return 0;
  }
  if (g_nCTILinkStatus == 1)
  {
    //鏈路故障
    if (g_nHostType == 0)
    {
      //本機為主機，切換到備機運行
      if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
        return 0;
      CloseVoiceCard(); //2016-05-01 這里應該要關閉語音卡
      g_nMasterStatus = 0;
      g_nStandbyStatus = 1;
      //發送切換指令
      MyTrace(0, "StandbyCTIServer Switchover to Active because MasterCTILINK LinkStatusFail!!!");
      SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because MasterCTILINK LinkStatusFail");
      SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterCTILINK LinkStatusFail");
      WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server CTILINK is interrupted and is switched to the standby server.");
    } 
    else
    {
      //本機為備機，切換到主機運行
      if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
        return 0;
      CloseVoiceCard(); //2016-05-01 這里應該要關閉語音卡
      g_nMasterStatus = 1;
      g_nStandbyStatus = 0;
      //發送切換指令
      MyTrace(0, "MasterCTIServer Switchover to Active because StandbyCTILINK LinkStatusFail!!!");
      SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because StandbyCTILINK LinkStatusFail");
      SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyCTILINK LinkStatusFail");
      WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server CTILINK is interrupted and is switched to the main server.");
    }
    strcpy(g_szSwitchTime, MyGetNow());
    SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
  }
  return 0;
}
//返回交換機設備配置數量
int Proc_SWTMSG_ongetdevnnum(CXMLMsg &SWTMsg)
{
  if (pBoxchn->isInited())
  {
    return 1; //pBoxchn->ReleaseAll();
  }
	int MaxChnNum = atoi(SWTMsg.GetAttrValue(2).C_Str());
  if (pBoxchn->Init(MaxChnNum) == 0)
  {
    pBoxchn->InitChnIdx(g_nSwitchType, MaxChnNum);
    pBoxchn->TotalTrkNum = 0;
    pBoxchn->TotalSeatNum = 0;
    pBoxchn->TotalIvrNum = 0;
    pBoxchn->TotalRecNum = 0;
  }
  return 0;
}
//返回交換機設備的配置參數
int Proc_SWTMSG_ongetdevparam(CXMLMsg &SWTMsg)
{
  if (!pBoxchn->isInited() || g_bChnInit == true)
  {
    return 1;
  }
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  short nChStyle = atoi(SWTMsg.GetAttrValue(4).C_Str());
  short nChStyleIndex = atoi(SWTMsg.GetAttrValue(5).C_Str());
  short nChType = atoi(SWTMsg.GetAttrValue(6).C_Str());
  short nChIndex = atoi(SWTMsg.GetAttrValue(7).C_Str());
  short nLgChType = atoi(SWTMsg.GetAttrValue(8).C_Str());
  short nLgChIndex = atoi(SWTMsg.GetAttrValue(9).C_Str());
  int nAG;

  if (nChStyle > 0)
  {
    MyTrace(3, "DeviceID=%s nPortID=%d nChStyle=%d nChStyleIndex=%d nChType=%d nChIndex=%d nLgChType=%d nLgChIndex=%d",
      SWTMsg.GetAttrValue(3).C_Str(), nPortID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex);
  }
  
  pBoxchn->SetChData(nPortID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex);
  pBoxchn->SetChtypeData(nChType, nChIndex, nPortID);
  pBoxchn->SetChDeviceID(nPortID, SWTMsg.GetAttrValue(3).C_Str());

  pBoxchn->pChn_str[nPortID].CTILinkClientId = SWTMsg.GetClientId();

  //MyTrace(3, "nChn=%d CTILinkClientId=%04x", 
  //  nPortID, pBoxchn->pChn_str[nPortID].CTILinkClientId);
  //設置語音資源通道與交換機端口的對應關系
  if (pVopGroup)
    pVopGroup->SetDeviceID(nPortID, nPortID, SWTMsg.GetAttrValue(3).C_Str());
  if (nLgChType == CHANNEL_TRUNK)
  {
    pBoxchn->SetTrunkCh(nLgChIndex, nPortID);
    pBoxchn->TotalTrkNum ++;

    if (pVopGroup)
      pBoxchn->pChn_str[nPortID].cVopChn = pVopGroup->GetVopChnByChnNo(nPortID);
    
    if (pBoxchn->pChn_str[nPortID].cVopChn != NULL)
    {
      pBoxchn->pChn_str[nPortID].SeizeVopChnState = 1;
      pBoxchn->pChn_str[nPortID].VOC_TrunkId = 1; //IVR語音中繼標志
      pBoxchn->pChn_str[nPortID].cVopChn->SeizeVopChn(nPortID);
      MyTrace(3, "nChn=%d banding VOPCHN Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d", 
        nPortID,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nIndex,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nVopNo,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnNo,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnType,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nChIndex,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nSwtPortID);
    }
  }
  else if (nLgChType == CHANNEL_SEAT)
  {
    pBoxchn->SetSeatCh(nLgChIndex, nPortID, SWTMsg.GetAttrValue(3).C_Str(), 1);
    pBoxchn->TotalSeatNum++;

    nAG = pAgentMng->GetnAGBySeatNo(SWTMsg.GetAttrValue(3).C_Str());
    if (!pAgentMng->isnAGAvail(nAG))
    {
      if (pAgentMng->Alloc_nAG(nAG))
      {
        pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
        pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
        pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(1, 1, SWTMsg.GetAttrValue(3), SWTMsg.GetAttrValue(3), 0, "", nPortID, 0xFFFF);
        pBoxchn->pChn_str[nPortID].nAG = nAG;
      }
    }
    
    if (pVopGroup)
      pBoxchn->pChn_str[nPortID].cVopChn = pVopGroup->GetVopChnByChnNo(nPortID);
    
    if (pBoxchn->pChn_str[nPortID].cVopChn != NULL)
    {
      pBoxchn->pChn_str[nPortID].SeizeVopChnState = 1;
      pBoxchn->pChn_str[nPortID].cVopChn->SeizeVopChn(nPortID);
      MyTrace(3, "nChn=%d banding VOPCHN Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d", 
        nPortID,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nIndex,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nVopNo,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnNo,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnType,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nChIndex,
        pBoxchn->pChn_str[nPortID].cVopChn->m_nSwtPortID);
    }
  }
  else if (nLgChType == CHANNEL_IVR)
  {
    if (g_nSwitchType == 13) //edit 2013-09-30
    {
      pBoxchn->SetIvrCh(nLgChIndex, nPortID, NULL);
      pBoxchn->TotalIvrNum ++;
    }
    else
    {
      if (pVopGroup)
      {
        pBoxchn->SetIvrCh(nLgChIndex, nPortID, pVopGroup->GetVopChnByChnNo(nPortID));
        pBoxchn->TotalIvrNum ++;
      }

      if (pVopGroup)
        pBoxchn->pChn_str[nPortID].cVopChn = pVopGroup->GetVopChnByChnNo(nPortID);
      if (pBoxchn->pChn_str[nPortID].cVopChn != NULL)
      {
        pBoxchn->pChn_str[nPortID].SeizeVopChnState = 1;
        pBoxchn->pChn_str[nPortID].cVopChn->SeizeVopChn(nPortID);
        MyTrace(3, "nChn=%d banding VOPCHN Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d", 
          nPortID,
          pBoxchn->pChn_str[nPortID].cVopChn->m_nIndex,
          pBoxchn->pChn_str[nPortID].cVopChn->m_nVopNo,
          pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnNo,
          pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnType,
          pBoxchn->pChn_str[nPortID].cVopChn->m_nChIndex,
          pBoxchn->pChn_str[nPortID].cVopChn->m_nSwtPortID);
      }
      ReadSwitchPortFlwCode(nPortID);
    }
  }
  else if (nLgChType == CHANNEL_REC)
  {
    pBoxchn->SetRecCh(nLgChIndex, nPortID);
    pBoxchn->TotalRecNum ++;
  }
  if ((nPortID+1) == pBoxchn->MaxChnNum)
  {
    if (pBoxchn->pIdelTrkChnQueue == NULL)
    {
      pBoxchn->pIdelTrkChnQueue = new CIdelTrkChnQueue;
      if (pBoxchn->pIdelTrkChnQueue != NULL)
      {
        pBoxchn->pIdelTrkChnQueue->InitQueue(pBoxchn->TotalTrkNum);
        for (int i = 0; i < pBoxchn->TotalTrkNum; i++)
        {
          pBoxchn->pIdelTrkChnQueue->TrkNoJoinInQueueTail(i);
        }
      }
    }
    if (pAgentMng->MaxUseNum == 0)
    {
      //讀內線端口號碼配置
      ReadWorkerGroupName();
      pBoxchn->Read_SeatNoIniFile(g_szServiceINIFileName);
    }
    SetLocalSeatChn();

    if (LogerStatusSet[nLOGn].nLoginId == 2)
    {
      SendMonitorONOFFStatus(nLOGn);
      SendAllCHStateToLOG(nLOGn);
      SendAllAGStateToLOG(nLOGn);
      SendAllVOPStateToLOG(nLOGn);
    }
    g_bChnInit = true;
  }
	return 0;
}
//端口狀態事件,端口狀態值：0x00-空閑 0x02-阻塞
int Proc_SWTMSG_onportstatus(CXMLMsg &SWTMsg)
{
  CAgent *pAgent;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (atoi(SWTMsg.GetAttrValue(4).C_Str()) == 0)
  {
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      if (g_nSwitchType != 13) //2015-11-26
      {
        pAgent->SetAgentsvState(AG_SV_IDEL);
        DispAgentStatus(pAgent->nAG);
        
        AgentStatusCount();
        SendOneGroupStatusMeteCount(pAgent->nAG);
        SendSystemStatusMeteCount();
      }
    }
    if (pChn->ssState == CHN_SNG_OT_HANGOFF)
      pChn->ssState = 0;
    pChn->hwState = 0;
    DispChnStatus(nChn);
  }
  else if (atoi(SWTMsg.GetAttrValue(4).C_Str()) == 1)
  {
    //摘機
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      pAgent->SetAgentsvState(AG_SV_HANGUP);
      DispAgentStatus(pAgent->nAG);
      
      AgentStatusCount();
      SendOneGroupStatusMeteCount(pAgent->nAG);
      SendSystemStatusMeteCount();
    }
    if (pChn->TranIVRId == 5)
    {
      Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, pChn->CallerNo, pChn->CalledNo, 0, pChn->CallData.C_Str(), 1);
    }
    if (pChn->ssState == 0)
    {
      pChn->ssState = CHN_SNG_OT_HANGOFF;
      DispChnStatus(nChn);
    }
  }
  else if (atoi(SWTMsg.GetAttrValue(4).C_Str()) == 9)
  {
    pChn->hwState = 2;
    DispChnStatus(nChn);
  }
	return 0;
}
//坐席狀態事件,在席狀態值：0-離線 1-上線
int Proc_SWTMSG_onagentstatus(CXMLMsg &SWTMsg)
{
  if (pIVRCfg->isAgentLoginSwitch == false)
    return 0;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  CAgent *pAgent=NULL;
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    switch (atoi(SWTMsg.GetAttrValue(4).C_Str()))
    {
    case 0: //AM_LOG_IN
      if (pAgent->m_Worker.DisturbId != 0 || pAgent->m_Worker.LeaveId != 0)
      {
        Send_SWTMSG_setagentstatus(0, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
      }
      break;
    case 1: //AM_LOG_OUT
      pAgent->m_Worker.DisturbId = 0;
      pAgent->m_Worker.LeaveId = 1;
      DispAgentStatus(pAgent->nAG);
      break;
    case 2: //AM_NOT_READY
      pAgent->m_Worker.DisturbId = 1;
      pAgent->m_Worker.LeaveId = 0;
      DispAgentStatus(pAgent->nAG);
      break;
    case 3: //AM_READY
      if (pAgent->m_Worker.DisturbId != 0 || pAgent->m_Worker.LeaveId != 0)
      {
        Send_SWTMSG_setagentstatus(0, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
        break;
      }
      pAgent->m_Worker.DisturbId = 0;
      pAgent->m_Worker.LeaveId = 0;
      DispAgentStatus(pAgent->nAG);
      break;
    case 4: //AM_WORK_NOT_READY
      break;
    case 5: //AM_WORK_READY
      break;
    }
  }
  return 0;
}
//房間狀態事件
int Proc_SWTMSG_onroomstatus(CXMLMsg &SWTMsg)
{
	return 0;
}
//留言燈狀態事件
int Proc_SWTMSG_onlampevent(CXMLMsg &SWTMsg)
{
	return 0;
}
//叫醒狀態事件
int Proc_SWTMSG_onwakeevent(CXMLMsg &SWTMsg)
{
	return 0;
}
//話務臺隊列事件
int Proc_SWTMSG_onconsoleevent(CXMLMsg &SWTMsg)
{
	return 0;
}
//收到呼叫話單事件
int Proc_SWTMSG_onbillevent(CXMLMsg &SWTMsg)
{
	return 0;
}
//呼叫事件
int Proc_SWTMSG_oncallevent(CXMLMsg &SWTMsg)
{
	short nPortID, nPortID1, nDirection;
  int nChn, nChn1, nCallerType=1, nCallType, nStatus, len;
  char OrgCalledNo[MAX_TELECODE_LEN], CdrSerialNo[MAX_CHAR64_LEN], RecordFileName[MAX_CHAR128_LEN], DialParam[256], szTemp[MAX_TELECODE_LEN], szTranParam[128], szSWTACDTimeLen[32]; //2017-01-08
  CAgent *pAgent=NULL;
  
  nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  nPortID1 = atoi(SWTMsg.GetAttrValue(6).C_Str());
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (AuthPassId == false && g_nCallInCount > MAXCALLCOUNTONAUTHFAIL)
  {
    MyTrace(0, "OnCallEvent nChn=%d License Data is error!!!", nChn);
    return 1;
  }
  //呼叫方向nDirection: 1-入局呼叫 2-分機呼出 3-轉接呼叫
  nDirection = atoi(SWTMsg.GetAttrValue(5).C_Str());
  //呼叫狀態 1-電話呼入振鈴 2-電話呼入應答 3-呼出聽撥號音 4-呼出正在撥號 5-呼出聽回鈴音 6-呼出聽忙音
  nStatus = atoi(SWTMsg.GetAttrValue(11).C_Str());
  //呼叫類型 1-直接呼入 2-直接呼出 3-轉移呼入 4-代接呼入 5-強插呼出 6-監聽呼出 6-遇忙轉移呼入 7-無人應答轉移呼入
  nCallType = atoi(SWTMsg.GetAttrValue(14).C_Str());

  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
  {
    if (nDirection == 1 || nDirection == 3)
    {
      if (SWTMsg.GetAttrValue(7).GetLength() > 0)
        strncpy(pChn->CallerNo, SWTMsg.GetAttrValue(7).C_Str(), MAX_TELECODE_LEN-1);
      
      if (SWTMsg.GetAttrValue(9).GetLength() > 0)
        pChn->OrgCallerNo = SWTMsg.GetAttrValue(9).C_Str();
      
      if (SWTMsg.GetAttrValue(10).GetLength() > 0)
        pChn->OrgCalledNo = SWTMsg.GetAttrValue(10).C_Str();
      
      if (SWTMsg.GetAttrValue(13).GetLength() > 0)
        strncpy(pChn->CustPhone, SWTMsg.GetAttrValue(13).C_Str(), MAX_TELECODE_LEN-1);
      
      if (SWTMsg.GetAttrValue(8).GetLength() > 0)
      {
        strncpy(pChn->CalledNo, SWTMsg.GetAttrValue(8).C_Str(), MAX_TELECODE_LEN-1);
        pChn->VOC_TrunkRecvCallEvent = 1;
      }
      MyTrace(3, "Proc_SWTMSG_oncallevent: nChn=%d CallerNo=%s CalledNo=%s OrgCallerNo=%s OrgCalledNo=%s CustPhone=%s",
        nChn, pChn->CallerNo, pChn->CalledNo, pChn->OrgCallerNo.C_Str(), pChn->OrgCalledNo.C_Str(), pChn->CustPhone);
      DispChnStatus(nChn);
    }
    return 0;
  }
  MyTrace(3, "Proc_SWTMSG_oncallevent: nChn=%d TranIVRId=%d pChn->CdrSerialNo=%s RecordFileName=%s",
    nChn, pChn->TranIVRId, pChn->CdrSerialNo, pChn->RecordFileName);

  if (nCallType == 9)
  {
    strcpy(pChn->CallerNo, SWTMsg.GetAttrValue(7).C_Str());
    strcpy(pChn->CalledNo, SWTMsg.GetAttrValue(8).C_Str());
    pChn->timer = 0;
    pChn->CallTime = time(0);
    pChn->ssState = CHN_SNG_OT_WAIT;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_OUT;
    pChn->ConnID = atol(SWTMsg.GetAttrValue(4).C_Str());
    DispChnStatus(nChn);

    return 0;
  }
  else if (nCallType == 10)
  {
    char sqls[1024], szCdrSerialNo[64];
    struct tm *local;
    time_t lt;
    lt=time(NULL);
    local=localtime(&lt);
    
    sprintf(szCdrSerialNo, "%04d%02d%02d%02d%02d%02d%d%03d", 
      local->tm_year+1900, local->tm_mon+1, local->tm_mday,
      local->tm_hour, local->tm_min, local->tm_sec,
      pChn->lgChnType, pChn->lgChnNo);

    if (g_nDBType == DB_MYSQL)
      sprintf(sqls, "CALL sp_InsertCDR('%s',%d,%d,'%s','%s',%d,%d,%d,'%s');",
      szCdrSerialNo,
      pChn->lgChnType,
      pChn->lgChnNo,
      SWTMsg.GetAttrValue(7).C_Str(),
      SWTMsg.GetAttrValue(8).C_Str(),
      pChn->CallInOut,
      1,
      nCallType,
      "");
    else if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "exec sp_InsertCDR '%s',%d,%d,'%s','%s',%d,%d,%d,'%s'",
      szCdrSerialNo,
      pChn->lgChnType,
      pChn->lgChnNo,
      SWTMsg.GetAttrValue(7).C_Str(),
      SWTMsg.GetAttrValue(8).C_Str(),
      1,
      1,
      nCallType,
      "");
    SendExecSql2DB(sqls);

    pAgent = GetAgentBynChn(nChn);
    if (pAgent)
    {
      if (pAgent->isLogin() == true && pAgent->isTcpLinked() == true)
      {
        sprintf(szCdrSerialNo, "提醒：有電話%s呼入", SWTMsg.GetAttrValue(7).C_Str());
        Set_IVR2AG_Header(AGMSG_onsendmessage, pAgent->GetClientId()<<16);
        Set_IVR2AG_Item(AGMSG_onsendmessage, 1, pAgent->GetSeatNo());
        Set_IVR2AG_IntItem(AGMSG_onsendmessage, 2, pAgent->GetWorkerNo());
        Set_IVR2AG_Item(AGMSG_onsendmessage, 3, pAgent->GetWorkerName());
        Set_IVR2AG_Item(AGMSG_onsendmessage, 4, szCdrSerialNo);
        Set_IVR2AG_Tail();
        SendMsg2AG(pAgent->GetClientId(), AGMSG_onsendmessage, IVRSndMsg.GetBuf());
      }
    }
    return 0;
  }

  if (pChn->TranIVRId == 2)
  {
    DBUpdateCallCDR_UpdateFlag(nChn);
    pBoxchn->ResetChn(nChn);
    pChn->TranIVRId = 2;
  }
  else if (pChn->TranIVRId == 5 || pChn->TranIVRId == 6)
  {
    UL sessionid1, cmdattr1, sessionid2, cmdattr2;
    UC TranIVRId = pChn->TranIVRId;
    US TranWaitTime = pChn->TranWaitTime;
    sessionid1 = pChn->SessionNo;
    cmdattr1 = pChn->CmdAddr;
    sessionid2 = pChn->TranSessionNo;
    cmdattr2 = pChn->TranCmdAddr;
    DBUpdateCallCDR_UpdateFlag(nChn);
    pBoxchn->ResetChn(nChn);
    pChn->SessionNo = sessionid1;
    pChn->CmdAddr = cmdattr1;
    pChn->TranSessionNo = sessionid2;
    pChn->TranCmdAddr = cmdattr2;
    pChn->TranIVRId = TranIVRId;
    pChn->TranWaitTime = TranWaitTime;
  }
  else if (pChn->TranIVRId == 7)
  {
    strcpy(CdrSerialNo, pChn->CdrSerialNo);
    strcpy(RecordFileName, pChn->RecordFileName);
    pBoxchn->ResetChn(nChn);
    pChn->TranIVRId = 7;
    strcpy(pChn->CdrSerialNo, CdrSerialNo);
    strcpy(pChn->RecordFileName, RecordFileName);
  }
  else
  {
    DBUpdateCallCDR_UpdateFlag(nChn);
    pBoxchn->ResetChn(nChn);
  }
  if (pChn->TranIVRId != 7)
    pChn->GenerateSerialNo();
  
  pChn->nSwtPortID = nPortID1;

  if (nDirection == 1 || nDirection == 3)
  {
    pChn->CallInOut = 1;
    ChCount.TrkInCount ++;
    ChCount.TrkCallCount ++;
  }
  else if (nDirection == 2)
  {
    pChn->CallInOut = 2;
    ChCount.TrkOutCount ++;
    ChCount.TrkCallCount ++;
  }
  g_nCallInCount++;

  pChn->timer = 0;
  pChn->CallTime = time(0);
  pChn->ssState = CHN_SNG_IN_ARRIVE;
  //pChn->hwState = CHN_HW_VALID;
  pChn->lnState = CHN_LN_SEIZE; //占用

  pChn->CallInId = 1; //有呼叫進入
  pChn->ConnID = atol(SWTMsg.GetAttrValue(4).C_Str());

  memset(OrgCalledNo, 0, MAX_TELECODE_LEN);
  memset(CdrSerialNo, 0, MAX_CHAR64_LEN);
  memset(RecordFileName, 0, MAX_CHAR128_LEN);
  memset(DialParam, 0, 256);
  memset(szSWTACDTimeLen, 0, 32); //2017-01-08
  if (SWTMsg.GetAttrValue(12).GetLength() > 0)
  {
    pChn->CallData = SWTMsg.GetAttrValue(12).C_Str(); //傳遞的呼叫參數
    strcpy(pChn->CallUCID, GetParamByName(SWTMsg.GetAttrValue(12).C_Str(), "$S_UCID", "", MAX_CHAR128_LEN-1));
    strcpy(OrgCalledNo, GetParamByName(SWTMsg.GetAttrValue(12).C_Str(), "$S_OrgCalledNo", "", MAX_TELECODE_LEN-1)); //2016-04-19增加傳遞原被叫號碼
    strcpy(CdrSerialNo, GetParamByName(SWTMsg.GetAttrValue(12).C_Str(), "$S_CDRSerialNo", "", MAX_CHAR64_LEN-1));
    strcpy(RecordFileName, GetParamByName(SWTMsg.GetAttrValue(12).C_Str(), "$S_RecdFileName", "", MAX_CHAR128_LEN-1));
    strcpy(DialParam, GetParamByName(SWTMsg.GetAttrValue(12).C_Str(), "$S_DialParam", ""));
    strcpy(szSWTACDTimeLen, GetParamByName(SWTMsg.GetAttrValue(12).C_Str(), "$S_SWTACDTimeLen", "", 31)); //2017-01-08
  }
  pChn->SWTACDTimeLen = atoi(szSWTACDTimeLen); //2017-01-08

  if (SWTMsg.GetAttrValue(7).GetLength() > 0)
  {
    strncpy(pChn->CallerNo, SWTMsg.GetAttrValue(7).C_Str(), MAX_TELECODE_LEN-1);
  }
  else
  {
    strcpy(pChn->CallerNo, "000");
  }
  strncpy(pChn->CalledNo, SWTMsg.GetAttrValue(8).C_Str(), MAX_TELECODE_LEN-1);

  //實際通話的客戶電話號碼
  pAgent = GetAgentBynChn(nChn);
  if (pAgent == NULL)
  {
    strncpy(pChn->CustPhone, SWTMsg.GetAttrValue(13).C_Str(), MAX_TELECODE_LEN-1);
  }
  else
  {
    //2015-11-25 針對語音中繼的修改
    MyTrace(3, "Proc_SWTMSG_oncallevent: nChn=%d m_Seat.CustPhone=%s m_Seat.DialParam=%s",
      nChn, pAgent->m_Seat.CustPhone, pAgent->m_Seat.DialParam.C_Str());
    if (strlen(pAgent->m_Seat.CustPhone) > 0)
    {
      strcpy(pChn->CustPhone, pAgent->m_Seat.CustPhone);
      if (pAgent->m_Seat.DialParam.GetLength() > 0)
      	strcpy(DialParam, pAgent->m_Seat.DialParam.C_Str());
      MyTrace(3, "Proc_SWTMSG_oncallevent: nChn=%d CustPhone=%s DialParam=%s",
        nChn, pChn->CustPhone, DialParam);
    }
    else
    {
      strncpy(pChn->CustPhone, SWTMsg.GetAttrValue(13).C_Str(), MAX_TELECODE_LEN-1);
    }
  }
  
  if (nDirection == 2)
  {
    if (strlen(pChn->CustPhone) == 0)
      strcpy(pChn->CustPhone, pChn->CalledNo);
    //呼出
    if (pIVRCfg->isAutoDelDailOutPreCode)
    {
      len = strlen(pIVRCfg->SWTDialOutPreCode);
      int len1 = strlen(pIVRCfg->SWTDialOutPreCode1);
      int len2 = strlen(pIVRCfg->SWTDialOutPreCode2);
      int len3 = strlen(pIVRCfg->SWTDialOutPreCode3);
      int len4 = strlen(pIVRCfg->SWTDialOutPreCode4);
      if (len > 0 && len < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len]);
      }
      else if (len1 > 0 && len1 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode1, len1) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len1]);
      }
      else if (len2 > 0 && len2 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode2, len2) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len2]);
      }
      else if (len3 > 0 && len3 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode3, len3) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len3]);
      }
      else if (len4 > 0 && len4 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode4, len4) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len4]);
      }
    }
    if (pIVRCfg->isAutoDelDailOutIPCode)
    {
      len = strlen(pIVRCfg->SWTIPPreCode);
      if (len > 0 && len < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTIPPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len]);
      }
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
    }
    if (GetDialOutId(SWTMsg.GetAttrValue(12).C_Str(), nChn) == true)
    {
      UpdateDialOutTime(pChn->ManDialOutId, pChn->ManDialOutListId);
    }
    else if (pIVRCfg->isInsertDialOutListId == true)
    {
      strcpy(pChn->ManDialOutListId, MyGetGUID());
      pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
        InsertDialOutListRecord(pChn->ManDialOutListId, pChn->CustPhone, pAgent->GetWorkerNoStr());
      else
        InsertDialOutListRecord(pChn->ManDialOutListId, pChn->CustPhone, "");
    }
    GetCRMCallId(SWTMsg.GetAttrValue(12).C_Str(), nChn);
  }
  else
  {
    if (strlen(pChn->CustPhone) == 0)
      strcpy(pChn->CustPhone, pChn->CallerNo);
  }
  if (strlen(pChn->CustPhone) > 0)
  {
    len = strlen(pIVRCfg->CentrexCode);
    if (strlen(pChn->CustPhone) > 7 && pIVRCfg->isAutoDelPSTNPreCode == true && len > 0)
    {
      if (strncmp(pChn->CustPhone, pIVRCfg->CentrexCode, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    if (pIVRCfg->isAutoDelMobilePreCode0 == true)
    {
      if (strncmp(pChn->CustPhone, "013", 3) == 0 || strncmp(pChn->CustPhone, "015", 3) == 0 || strncmp(pChn->CustPhone, "018", 3) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[1], MAX_TELECODE_LEN-2);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    if (pIVRCfg->isAutoAddTWMobilePreCode0 == true)
    {
      if (pChn->CustPhone[0] == '9' && (int)strlen(pChn->CustPhone) == 9)
      {
        sprintf(szTemp, "0%s", pChn->CustPhone);
        strcpy(pChn->CustPhone, szTemp);
      }
    }
    //2014-06-13 add
    if (pIVRCfg->ProcLocaAreaCodeId == 1 && nDirection == 1 && strlen(pChn->CustPhone) > pIVRCfg->LocaTelCodeLen)
    {
      len = strlen(pIVRCfg->LocaAreaCode);
      if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
      {
        if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode, len) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      len = strlen(pIVRCfg->LocaAreaCode1);
      if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1+len) && len > 0)
      {
        if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode1, len) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      len = strlen(pIVRCfg->LocaAreaCode2);
      if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
      {
        if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode2, len) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      len = strlen(pIVRCfg->LocaAreaCode3);
      if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
      {
        if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode3, len) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      len = strlen(pIVRCfg->LocaAreaCode4);
      if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
      {
        if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode4, len) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      strcpy(pChn->CallerNo, pChn->CustPhone);
    }

    if (nDirection == 2)
    {
      if (pIVRCfg->ProcLocaAreaCodeId == 2)
      {
        pAgent = GetAgentBynChn(nChn);
        if (pAgent)
        {
          if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && pAgent->m_Seat.LocaAreaCode.GetLength() > 0 && (int)strlen(pChn->CustPhone) > 5)
          {
            sprintf(szTemp, "%s%s", pAgent->m_Seat.LocaAreaCode.C_Str(), pChn->CustPhone);
            strcpy(pChn->CustPhone, szTemp);
          }
          if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && strlen(pIVRCfg->LocaAreaCode1) > 0 && (int)strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1)
          {
            sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode1, pChn->CustPhone);
            strcpy(pChn->CustPhone, szTemp);
          }
        }
      }
      strcpy(pChn->CalledNo, pChn->CustPhone);
    }
    else
    {
      if (pIVRCfg->ProcLocaAreaCodeId == 2)
      {
        if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && strlen(pIVRCfg->LocaAreaCode) > 0 && (int)strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen)
        {
          sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode, pChn->CustPhone);
          strcpy(pChn->CustPhone, szTemp);
        }
        if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && strlen(pIVRCfg->LocaAreaCode1) > 0 && (int)strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1)
        {
          sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode1, pChn->CustPhone);
          strcpy(pChn->CustPhone, szTemp);
        }
      }
      strcpy(pChn->CallerNo, pChn->CustPhone);
    }
  }
  
  pChn->OrgCallerNo = SWTMsg.GetAttrValue(9).C_Str();
  //2016-04-19增加傳遞原被叫號碼
  if (strlen(OrgCalledNo) > 0)
  {
    pChn->OrgCalledNo = pChn->CalledNo;
    strcpy(pChn->CalledNo, OrgCalledNo);
  } 
  else
  {
    pChn->OrgCalledNo = SWTMsg.GetAttrValue(10).C_Str();
  }
  if (pChn->lgChnType == CHANNEL_TRUNK)
  {
    pBoxchn->pIdelTrkChnQueue->TrkNoDelFromQueue(pChn->lgChnNo);
  }
  
  //2014-01-13 針對IPO收不到主叫，而通過中繼監錄收主叫
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    if (strlen(pAgent->m_Seat.CustPhone) > 6 && strlen(pChn->CustPhone) < 4)
    {
      strcpy(pChn->CustPhone, pAgent->m_Seat.CustPhone);
      if (nDirection == 1 || nDirection == 3)
        strcpy(pChn->CallerNo, pChn->CustPhone);
    }
  }

  DispChnStatus(nChn);
  if (nDirection == 2)
    DBInsertCallTrace(nChn, "DIALOUT");
  else
    DBInsertCallTrace(nChn, "RINGIN");

  pChn->nCallType = nCallType;
  
  if (pAgent == NULL)
  {
    if (pChn->TranIVRId == 0) //不是轉IVR，而是坐席轉接電話，就不寫話單
    {
      //呼叫的通道不是坐席通道,是IVR通道
      pChn->GenerateCdrSerialNo();
      DBInsertCallCDR(nChn, 2, nCallType);
    }
    else if (pChn->TranIVRId == 5 && g_nSwitchMode == 13)
    {
      pChn->GenerateCdrSerialNo();
      DBInsertCallCDR(nChn, 2, nCallType);
    }
  } 
  else
  {
    MyTrace(3, "Proc_SWTMSG_oncallevent: nChn=%d nAG=%d CdrSerialNo=%s pChn->CdrSerialNo=%s pAgent->CdrSerialNo=%s RecordFileName=%s DialParam=%s",
      nChn, pChn->nAG, CdrSerialNo, pChn->CdrSerialNo, pAgent->CdrSerialNo, pChn->RecordFileName, DialParam);

    pAgent->ExternalId = 1; //2015-12-26

    //呼叫的通道是坐席通道
    if (pAgent->TranAGId == 0)
    {
      //2015-12-26 外線標志(用來判斷內線之間呼叫不累計呼叫次數)
      if (strlen(pChn->CustPhone) < 6 && strcmp(pChn->CustPhone, "000") != 0)
      {
        //內線呼叫
        nCallerType = 2;
        pAgent->ExternalId = 0;
      }
      else
      {
        //外線
        nCallerType = 1;
        pAgent->ExternalId = 1;
      }
      
      //是直接呼入到坐席的呼叫，非轉接呼叫,非代接呼叫
      if (nDirection != 2)
      {
        pChn->GenerateCdrSerialNo();
        strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
        DBInsertCallCDR(nChn, 1, nCallType);
      }
      else
      {
        pChn->GenerateCdrSerialNo();
        strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
        DBInsertCallCDR(nChn, 1, nCallType);
        DBUpdateCallCDR_WorkerNoGroupNo(nChn, pAgent->nAG);
      }
    }
    else if (pAgent->TranAGId == 1)
    {
      //2016-06-03 增加判斷主叫類型
      if (strlen(pChn->CustPhone) < 6 && strcmp(pChn->CustPhone, "000") != 0)
      {
        //內線呼叫
        nCallerType = 2;
      }
      else
      {
        //外線
        nCallerType = 1;
      }
      //IVR轉接坐席的呼叫或是代接
      strcpy(pChn->RecordFileName, RecordFileName); //add 2012-02-21
      strcpy(pAgent->RecordFileName, RecordFileName);
    }
    else if (pAgent->TranAGId == 2)
    {
      //2016-06-03 增加判斷主叫類型
      if (strlen(pChn->CustPhone) < 6 && strcmp(pChn->CustPhone, "000") != 0)
      {
        //內線呼叫
        nCallerType = 2;
      }
      else
      {
        //外線
        nCallerType = 1;
      }
      //IVR轉接坐席的呼叫或是代接
      strcpy(pChn->CdrSerialNo, CdrSerialNo);
      strcpy(pChn->RecordFileName, RecordFileName);
      strcpy(pAgent->CdrSerialNo, CdrSerialNo);
      strcpy(pAgent->RecordFileName, RecordFileName);
      DBUpdateCallCDR_RingTime(nChn);
      DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);
    }
  }
  if (strlen(DialParam) == 0)
  {
    sprintf(DialParam, "1`%s`%s`%s", pChn->CdrSerialNo, pChn->RecordFileName, pIVRCfg->SeatRecPath.C_Str());
  }
  else if (strncmp(DialParam, "ItemData1=", 10) == 0)
  {
    strcpy(szTranParam, DialParam);
    sprintf(DialParam, "1`%s`%s`%s```%s`", pChn->CdrSerialNo, pChn->RecordFileName, pIVRCfg->SeatRecPath.C_Str(), szTranParam);
  }

  if (pAgent != NULL)
  {
    MyTrace(3, "Proc_SWTMSG_oncallevent: nChn=%d nAG=%d CdrSerialNo=%s pChn->CdrSerialNo=%s pAgent->CdrSerialNo=%s RecordFileName=%s DialParam=%s",
      nChn, pChn->nAG, CdrSerialNo, pChn->CdrSerialNo, pAgent->CdrSerialNo, pChn->RecordFileName, DialParam);
    if (nCallType == 0)
    {
      if (nDirection != 2)
      {
        if (strlen(pChn->CallerNo) < 5)
          DBUpdateCallCDR_ACDTime(nChn, 255, 0); //內線通話
        else
          DBUpdateCallCDR_ACDTime(nChn, 0, 0); //外線通話
      }
      else
      {
        if (strlen(pChn->CalledNo) > 0 && strlen(pChn->CalledNo) < 5)
          DBUpdateCallCDR_ACDTime(nChn, 255, 0); //內線通話
        else
          DBUpdateCallCDR_ACDTime(nChn, 0, 0); //外線通話
      }
    }
    if (nCallType == 1 || nCallType == 2)
    {
      //2013-08-09
      if (nDirection != 2)
      {
        if (strlen(pChn->CallerNo) < 5)
          DBUpdateCallCDR_ACDTime(nChn, 255, 0); //內線通話
        else
          DBUpdateCallCDR_ACDTime(nChn, 0, 0); //外線通話
      }
      else
      {
        if (strlen(pChn->CalledNo) > 0 && strlen(pChn->CalledNo) < 5)
          DBUpdateCallCDR_ACDTime(nChn, 255, 0); //內線通話
        else
          DBUpdateCallCDR_ACDTime(nChn, 0, 0); //外線通話
      }

      DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);
      DBUpdateCallCDR_RingTime(nChn);
    }
    
    if (nDirection == 1 || nDirection == 3)
    {
      WriteSeatStatus(pAgent, AG_STATUS_INRING, pAgent->m_Worker.LeaveId, 1, 0);

      pAgent->SetAgentsvState(AG_SV_INRING);
      DispAgentStatus(pAgent->nAG);
      if (pIVRCfg->isSendACDCallToAgent == true && pAgent->GetClientId() != 0x06FF)
      {
        sprintf(SendMsgBuf, "<onacdcallin acdid='%d' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='%s' orgcalledno='%s' calltype='%d' funcno='0' param='%s'/>", 
          pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CustPhone, pChn->CalledNo, pChn->OrgCallerNo.C_Str(), pChn->OrgCalledNo.C_Str(), nCallType, DialParam);
        SendMsg2AG(pAgent->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
      }
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        //2016-01-13
        if (nCallType == 4) //發送代接結果
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=;", 
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnSuccess);
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
        else if (nCallType == 5) //發送監聽結果
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=;", 
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnSuccess);
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
        else if (nCallType == 6) //發送強插結果
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=;", 
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnSuccess);
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }

        if (strlen(CdrSerialNo) > 0)
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s;orgcallerno=%s;orgcalledno=%s;callertype=%d;calltype=%d;cdrserialno=%s;ucid=%s;param=%s;", //2015-10-20增加彈屏主叫類型(1-外線，2-內線)、呼叫類型（1-直接呼入 2-直接呼出 3-轉移呼入）
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), pChn->CustPhone, pChn->CalledNo, pChn->OrgCallerNo.C_Str(), pChn->OrgCalledNo.C_Str(), nCallerType, nCallType, CdrSerialNo, pChn->CallUCID, DialParam); //2015-10-24 增加傳遞UCID
        else
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s;orgcallerno=%s;orgcalledno=%s;callertype=%d;calltype=%d;cdrserialno=%s;ucid=%s;param=%s;", //2015-10-20增加彈屏主叫類型(1-外線，2-內線)、呼叫類型（1-直接呼入 2-直接呼出 3-轉移呼入）
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), pChn->CustPhone, pChn->CalledNo, pChn->OrgCallerNo.C_Str(), pChn->OrgCalledNo.C_Str(), nCallerType, nCallType, pChn->CdrSerialNo, pChn->CallUCID, DialParam); //2015-10-24 增加傳遞UCID
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
    }
    else if (nDirection == 2)
    {
      pAgent->m_Worker.AcdedGroupNo = pAgent->GetGroupNo();
      if (nStatus == 5)
      {
        pAgent->SetAgentsvState(AG_SV_OUTRING);
        WriteSeatStatus(pAgent, AG_STATUS_OUTRING, pAgent->m_Worker.LeaveId, 2, 0);
      }
      else
      {
        pAgent->SetAgentsvState(AG_SV_OUTSEIZE);
        WriteSeatStatus(pAgent, AG_STATUS_OUTSEIZE, pAgent->m_Worker.LeaveId, 2, 0);
      }
      UpdateDialOutSerialNo(pChn->ManDialOutId, pChn->ManDialOutListId, pChn->CdrSerialNo);
      DBUpdateCallCDR_DialOutID(nChn, pChn->ManDialOutId);
      DBUpdateCallCDR_CRMCallID(nChn, pChn->CRMCallId);

      DBUpdateCallCDR_RecdFile(pAgent->nAG, pAgent->ExtRecordServer, pAgent->ExtRecordFileName, pAgent->ExtRecordCallID);

      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=%d;callerno=%s;calledno=%s;cdrserialno=%s;param=", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutRing, pChn->CallerNo, pChn->CustPhone, pChn->CdrSerialNo);
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }

      DispAgentStatus(pAgent->nAG);
    }
    AgentStatusCount();
    SendOneGroupStatusMeteCount(pAgent->nAG);
    SendSystemStatusMeteCount();
  }
  //2015-10-22 針對不同IVR群組實時占用數的修改
  if (pChn->lgChnType == CHANNEL_IVR)
  {
    CIVRCallCountParam *pIVRCallCountParam;
    if (nDirection == 2)
    {
      SumIVRChnOnlines();
      MyTrace(3, "IncIVRInCount(%s)", pChn->CallerNo);
      pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInCount(pChn->CallerNo);
    }
    else
    {
      SumIVRChnOnlines();
      if (gIVRCallCountParamMng.GetIvrNoType == 1)
      {
        MyTrace(3, "IncIVRInCount(%s)", pChn->CalledNo);
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInCount(pChn->CalledNo);
      }
      else
      {
        MyTrace(3, "IncIVRInCount(%s)", pChn->OrgCalledNo.C_Str());
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInCount(pChn->OrgCalledNo.C_Str());
      }
    }
    SendIVRCallCountData(pIVRCallCountParam);
  }
  
  //關聯的通道
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID1);
  if (pBoxchn->isnChnAvail(nChn1))
  {
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      if (strlen(pChn->CallerNo) > 6)
        strcpy(pChn1->CallerNo, pChn->CallerNo);
      strcpy(pChn1->CalledNo, pChn->CalledNo);
      pChn1->OrgCallerNo = SWTMsg.GetAttrValue(9).C_Str();
      pChn1->OrgCalledNo = SWTMsg.GetAttrValue(10).C_Str();
      pChn1->timer = 0;
      pChn1->CallTime = time(0);
      pChn1->ssState = CHN_SNG_IN_ARRIVE;
      //pChn1->hwState = CHN_HW_VALID;
      pChn1->lnState = CHN_LN_SEIZE; //占用
      
      if (nDirection == 2)
        pChn1->CallInOut = CALL_OUT;
      else
        pChn1->CallInOut = CALL_IN;
      
      pChn1->CallInId = 1; //有呼叫進入
      pChn1->ConnID = atol(SWTMsg.GetAttrValue(4).C_Str());
      DispChnStatus(nChn1);
    }
  }
  
  return 0;
}
//收到后續地址事件
int Proc_SWTMSG_oncallsam(CXMLMsg &SWTMsg)
{
  int nChn, nChn1, samtype, len;
  char szTemp[64];
  
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  short nPortID1 = atoi(SWTMsg.GetAttrValue(5).C_Str());
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID1);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
    return 1;

  //后續地址類型nSamType：0-入局呼叫接收主叫號碼 1-入局呼叫收到撥的被叫號碼 2-本局呼叫或轉移呼叫收到撥的被叫號碼 3-呼出收到撥的呼出號碼
  samtype = atoi(SWTMsg.GetAttrValue(9).C_Str());
  if (samtype == 0 || samtype == 1)
    return 0;

  MyTrace(3, "Proc_SWTMSG_oncallsam nChn=%d ssState=%d CallerNo=%s CalledNo=%s OrgCalledNo=%s", 
    nChn, pChn->ssState, pChn->CallerNo, pChn->CalledNo, pChn->OrgCalledNo.C_Str());

  pChn->nSwtPortID = nPortID1;
  if (strcmp(pChn->CallerNo, "000") == 0 && SWTMsg.GetAttrValue(7).GetLength() > 0)
  {
    strncpy(pChn->CallerNo, SWTMsg.GetAttrValue(7).C_Str(), MAX_TELECODE_LEN-1);
  }

  if (samtype == 0 || samtype == 1)
  {
    if (strlen(pChn->CalledNo) == 0 && SWTMsg.GetAttrValue(8).GetLength() > 0)
      strncpy(pChn->CalledNo, SWTMsg.GetAttrValue(8).C_Str(), MAX_TELECODE_LEN-1);
  }
  else
  {
    strncpy(pChn->CalledNo, SWTMsg.GetAttrValue(8).C_Str(), MAX_TELECODE_LEN-1);
    strcpy(pChn->CustPhone, pChn->CalledNo); //2013-08-14

    if (strlen(pIVRCfg->DialOutPreCode) == strlen(pChn->CalledNo))
    {
      if (strcmp(pChn->CalledNo, pIVRCfg->DialOutPreCode) == 0)
        DBUpdateCallCDR_SrvType(nChn, 0, 0); //外線通話
      else
        DBUpdateCallCDR_SrvType(nChn, 255, 0); //內線通話
    }
  }
  
  //實際通話的客戶電話號碼
  if (pChn->OrgCalledNo.GetLength() == 0 && SWTMsg.GetAttrValue(11).GetLength() > 0)
    pChn->OrgCalledNo = SWTMsg.GetAttrValue(11);
  
  if (SWTMsg.GetAttrValue(10).GetLength() > 0)
  {
    pChn->CallData = SWTMsg.GetAttrValue(10).C_Str(); //傳遞的呼叫參數
  }

  DBInsertCallTrace(nChn, "DIALING");

  pChn->DtmfPauseTimer = 0;
  
  if (pChn->CallInOut == 2)
  {
    if (pIVRCfg->isAutoDelDailOutPreCode)
    {
      len = strlen(pIVRCfg->SWTDialOutPreCode);
      int len1 = strlen(pIVRCfg->SWTDialOutPreCode1);
      int len2 = strlen(pIVRCfg->SWTDialOutPreCode2);
      int len3 = strlen(pIVRCfg->SWTDialOutPreCode3);
      int len4 = strlen(pIVRCfg->SWTDialOutPreCode4);
      if (len > 0 && len < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len]);
      }
      else if (len1 > 0 && len1 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode1, len1) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len1]);
      }
      else if (len2 > 0 && len2 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode2, len2) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len2]);
      }
      else if (len3 > 0 && len3 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode3, len3) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len3]);
      }
      else if (len4 > 0 && len4 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode4, len4) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len4]);
      }
    }
    if (pIVRCfg->isAutoDelDailOutIPCode)
    {
      len = strlen(pIVRCfg->SWTIPPreCode);
      if (len > 0 && len < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTIPPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len]);
      }
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
      strcpy(pChn->CalledNo, pChn->CustPhone);
      pChn->GenerateRecordFileName();
    }
    if (pIVRCfg->isAutoDelMobilePreCode0 == true)
    {
      if (strncmp(pChn->CustPhone, "013", 3) == 0 || strncmp(pChn->CustPhone, "015", 3) == 0 || strncmp(pChn->CustPhone, "018", 3) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[1], MAX_TELECODE_LEN-2);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    DBUpdateCallCDR_Phone(nChn);
  }
  DispChnStatus(nChn);

  if (pBoxchn->isnChnAvail(nChn1))
  {
    pChn->SwtLinkType[0] = 3;
    pChn->SwtLinkChn[0] = nChn1;
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      if (strlen(pChn->CallerNo) > 6)
        strcpy(pChn1->CallerNo, pChn->CallerNo);
      strcpy(pChn1->CalledNo, pChn->CalledNo);
      pChn1->OrgCallerNo = pChn->OrgCallerNo;
      pChn1->OrgCalledNo = pChn->OrgCalledNo;
      pChn1->timer = 0;
      pChn1->CallTime = pChn->CallTime;
      pChn1->AnsTime = time(0);
      pChn1->ssState = pChn->ssState;
      pChn1->lnState = CHN_LN_SEIZE; //占用
      
      if (pChn1->CallInOut == 0)
      {
        if (pChn->CallInOut == 2)
          pChn1->CallInOut = CALL_OUT;
        else
          pChn1->CallInOut = CALL_IN;
      }
      
      pChn1->ConnID = pChn->ConnID;
      DispChnStatus(nChn1);
    }
  }
	return 0;
}
//號碼收全事件
int Proc_SWTMSG_oncallacm(CXMLMsg &SWTMsg)
{
  int nChn, nChn1;
  CAgent *pAgent=NULL;
  char Caller[MAX_TELECODE_LEN], Called[MAX_TELECODE_LEN];
  
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  short nPortID1 = atoi(SWTMsg.GetAttrValue(5).C_Str());
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID1);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
    return 1;

  MyTrace(3, "Proc_SWTMSG_oncallacm nChn=%d ssState=%d CallerNo=%s CalledNo=%s OrgCalledNo=%s", 
    nChn, pChn->ssState, pChn->CallerNo, pChn->CalledNo, pChn->OrgCalledNo.C_Str());

  pChn->nSwtPortID = nPortID1;

  strncpy(Caller, SWTMsg.GetAttrValue(7).C_Str(), MAX_TELECODE_LEN-1);
  strncpy(Called, SWTMsg.GetAttrValue(8).C_Str(), MAX_TELECODE_LEN-1);
  
  //if (strlen(Caller) > 0 && (strlen(pChn->CallerNo) == 0 || strcmp(pChn->CallerNo, "000") == 0))
  if (strlen(Caller) > 0 && (strlen(pChn->CallerNo) == 0 || strcmp(pChn->CallerNo, "000") == 0))
  {
    strcpy(pChn->CallerNo, Caller);
  }
  
  if (strlen(Called) > 0)
  {
    strcpy(pChn->CalledNo, Called);
    strcpy(pChn->RecvDialDtmf, Called);
    SendSeatDialDTMFEvent(nChn, pChn->RecvDialDtmf);
  }
  pChn->DtmfPauseTimer = 0;
  DispChnStatus(nChn);
  //DBUpdateCallCDR_Phone(nChn);
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    WriteSeatStatus(pAgent, AG_STATUS_OUTRING, 0);
  }
  if (pBoxchn->isnChnAvail(nChn1))
  {
    pChn->SwtLinkType[0] = 3;
    pChn->SwtLinkChn[0] = nChn1;
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      if (strlen(pChn->CallerNo) > 6)
        strcpy(pChn1->CallerNo, pChn->CallerNo);
      strcpy(pChn1->CalledNo, pChn->CalledNo);
      pChn1->OrgCallerNo = pChn->OrgCallerNo;
      pChn1->OrgCalledNo = pChn->OrgCalledNo;
      pChn1->timer = 0;
      pChn1->CallTime = pChn->CallTime;
      pChn1->AnsTime = time(0);
      pChn1->ssState = pChn->ssState;
      pChn1->lnState = CHN_LN_SEIZE; //占用
      
      if (pChn1->CallInOut == 0)
      {
        if (pChn->CallInOut == 2)
          pChn1->CallInOut = CALL_OUT;
        else
          pChn1->CallInOut = CALL_IN;
      }
      
      pChn1->ConnID = pChn->ConnID;
      DispChnStatus(nChn1);
    }
  }
	return 0;
}
//收到振鈴事件
int Proc_SWTMSG_oncallring(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nChn, nChn1, nFlag;
  
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  short nDesPortID = atoi(SWTMsg.GetAttrValue(5).C_Str());
  long nConnID = atol(SWTMsg.GetAttrValue(4).C_Str());
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nDesPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
    return 1;

  nFlag = atoi(SWTMsg.GetAttrValue(7).C_Str());

  pChn->nSwtPortID = nDesPortID;

  if (pChn->TranIVRId == 6)
  {
    SendCallseatResult1(nChn, OnACDRing, "");
  }
  else
  {
    SendDialoutResult(nChn, OnOutRing);
  }
  pChn->ssState = CHN_SNG_OT_RING;
  DispChnStatus(nChn);
  DBUpdateCallCDR_RingTime(nChn);

  if (pBoxchn->isnChnAvail(nChn1))
  {
    pChn->SwtLinkType[0] = 3;
    pChn->SwtLinkChn[0] = nChn1;
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      if (strlen(pChn->CallerNo) > 6)
        strcpy(pChn1->CallerNo, pChn->CallerNo);
      strcpy(pChn1->CalledNo, pChn->CalledNo);
      pChn1->OrgCallerNo = pChn->OrgCallerNo;
      pChn1->OrgCalledNo = pChn->OrgCalledNo;
      pChn1->timer = 0;
      pChn1->CallTime = pChn->CallTime;
      pChn1->AnsTime = time(0);
      pChn1->ssState = CHN_SNG_OT_RING;
      pChn1->lnState = CHN_LN_SEIZE; //占用
      if (pChn1->CallInOut == 0)
      {
        if (pChn->CallInOut == 2)
          pChn1->CallInOut = CALL_OUT;
        else
          pChn1->CallInOut = CALL_IN;
      }
      
      pChn1->ConnID = nConnID;
      DispChnStatus(nChn1);
    }
  }
  
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    pAgent->SetAgentsvState(AG_SV_OUTRING);
    WriteSeatStatus(pAgent, AG_STATUS_OUTRING, 0);
    DispAgentStatus(pAgent->nAG);

    UpdateDialOutRingTime(pChn->ManDialOutId, pChn->ManDialOutListId);
    
    if (pAgent->GetClientId() != 0x06FF)
    {
      sprintf(SendMsgBuf, "<onmakecall acdid='%d' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
        pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CallerNo, pChn->CustPhone, pChn->CallData.C_Str(), OnOutRing, "");
      SendMsg2AG(pAgent->GetClientId(), AGMSG_onmakecall, SendMsgBuf);
    }
    if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
    {
      sprintf(SendMsgBuf, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=%d;callerno=%s;calledno=%s;cdrserialno=%s;param=%s", 
        pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutRing, pChn->CallerNo, pChn->CustPhone, pChn->CdrSerialNo, pChn->CallData.C_Str());
      SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
    }
  }
  return 0;
}
//收到呼叫失敗事件
int Proc_SWTMSG_oncallfail(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nChn, nChn1, nFlag;
  
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  short nDesPortID = atoi(SWTMsg.GetAttrValue(5).C_Str());
  long nConnID = atol(SWTMsg.GetAttrValue(4).C_Str());
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nDesPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
    return 1;
  nFlag = atoi(SWTMsg.GetAttrValue(7).C_Str());

  if (pChn->TranIVRId == 5) //2016-12-30 增加在這里發送結果
  {
    if (nFlag == 9)
    {
      pChn->lnState = 0;
      pChn->TranIVRId = 0;
      pChn->CalloutTimer = 0;
      DispChnStatus(nChn);
    }
    SendCalloutResult1(nChn, nChn, OnOutFail, "CalloutTimer overflow");
  }
  else if (pChn->TranIVRId == 6) //2016-12-30
  {
    if (nFlag == 9)
    {
      pChn->lnState = 0;
      pChn->TranIVRId = 0;
      pChn->CalloutTimer = 0;
      DispChnStatus(nChn);
    }
    pAgent = GetAgentBynChn(nChn);
    if (pAgent)
    {
      pAgent->SetAgentsvState(AG_SV_IDEL);
      DispAgentStatus(pAgent->nAG);
    }
    SendCallseatResult1(nChn, OnACDFail, "");
  }
  else
  {
    SendDialoutResult(nChn, OnOutFail);
  }

  if (pChn->ssState == 0) //2016-12-30
    return 0;
  
  pChn->ssState = CHN_SNG_OT_FAIL;
  DispChnStatus(nChn);
  if (pBoxchn->isnChnAvail(nChn1))
  {
    pChn->SwtLinkType[0] = 3;
    pChn->SwtLinkChn[0] = nChn1;
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      if (strlen(pChn->CallerNo) > 6)
        strcpy(pChn1->CallerNo, pChn->CallerNo);
      strcpy(pChn1->CalledNo, pChn->CalledNo);
      pChn1->OrgCallerNo = pChn->OrgCallerNo;
      pChn1->OrgCalledNo = pChn->OrgCalledNo;
      pChn1->timer = 0;
      pChn1->CallTime = pChn->CallTime;
      pChn1->AnsTime = time(0);
      pChn1->ssState = CHN_SNG_OT_FAIL;
      pChn1->lnState = CHN_LN_SEIZE; //占用
      if (pChn1->CallInOut == 0)
      {
        if (pChn->CallInOut == 2)
          pChn1->CallInOut = CALL_OUT;
        else
          pChn1->CallInOut = CALL_IN;
      }
      
      pChn1->ConnID = nConnID;
      DispChnStatus(nChn1);
    }
  }
  
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    UpdateDialOutFailTime(pChn->ManDialOutId, pChn->ManDialOutListId);

    int nCallResult=OnOutFail;

    if (nFlag == 1)
    {
      nCallResult = OnOutBusy;
    }
    else if (nFlag == 2)
    {
      nCallResult = OnOutTimeout;
    }
    else if (nFlag == 3)
    {
      nCallResult = OnOutUNN;
    }

    if (pAgent->GetClientId() != 0x06FF && strlen(pChn->CalledNo) >= 3)
    {
      sprintf(SendMsgBuf, "<onmakecall acdid='%d' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
        pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CallerNo, pChn->CustPhone, pChn->CallData.C_Str(), nCallResult, "");
      SendMsg2AG(pAgent->GetClientId(), AGMSG_onmakecall, SendMsgBuf);
    }
    if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
    {
      sprintf(SendMsgBuf, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=%d;callerno=%s;calledno=%s;cdrserialno=%s;param=%s", 
        pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), nCallResult, pChn->CallerNo, pChn->CustPhone, pChn->CdrSerialNo, pChn->CallData.C_Str());
      SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
    }
  }
	return 0;
}
//收到應答事件
int Proc_SWTMSG_oncallack(CXMLMsg &SWTMsg)
{
  CStringX ArrString[4];
  CAgent *pAgent=NULL;
  int nChn, nChn1, nFlag, len, nArrNum;
  char CdrSerialNo[MAX_CHAR64_LEN], RecordFileName[MAX_CHAR128_LEN], DialParam[256], szTemp[MAX_TELECODE_LEN];
  
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  short nDesPortID = atoi(SWTMsg.GetAttrValue(5).C_Str());
  long nConnID = atol(SWTMsg.GetAttrValue(4).C_Str());
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nDesPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
    return 1;

  nFlag = atoi(SWTMsg.GetAttrValue(7).C_Str());

  if (strncmp(SWTMsg.GetAttrValue(6).C_Str(), "TRK::", 5) == 0)
  {
    strcpy(pChn->TrunkGroupNumber, &(SWTMsg.GetAttrValue(6).C_Str())[5]);
  }

  MyTrace(3, "Proc_SWTMSG_oncallack nChn=%d ssState=%d CallerNo=%s CalledNo=%s OrgCalledNo=%s", 
    nChn, pChn->ssState, pChn->CallerNo, pChn->CalledNo, pChn->OrgCalledNo.C_Str());

  switch (nFlag)
  {
  case 1: //入局呼叫轉到分機應答
    pChn->nSwtPortID = nDesPortID;

    //2016-11-22
    nArrNum = SplitString(SWTMsg.GetAttrValue(9).C_Str(), ',', 2, ArrString);
    if (nArrNum == 1)
    {
      if (ArrString[0].GetLength() > 0)
        strcpy(pChn->CalledNo, ArrString[0].C_Str());
    }
    else if (nArrNum == 2)
    {
      if (ArrString[0].GetLength() > 0)
        strcpy(pChn->CalledNo, ArrString[0].C_Str());
      if (ArrString[1].GetLength() > 0)
        pChn->OrgCalledNo = ArrString[1];
    }

    //實際通話的客戶電話號碼
    if (pChn->lgChnType == CHANNEL_IVR)
    {
      CIVRCallCountParam *pIVRCallCountParam;
      if (gIVRCallCountParamMng.GetIvrNoType == 1)
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInAnsCount(pChn->CalledNo);
      else
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInAnsCount(pChn->OrgCalledNo.C_Str());
      SendIVRCallCountData(pIVRCallCountParam);
    }
    
    if (SWTMsg.GetAttrValue(11).GetLength() > 0)
    {
      strncpy(pChn->CustPhone, SWTMsg.GetAttrValue(11).C_Str(), MAX_TELECODE_LEN-1);
      strcpy(pChn->CallerNo, pChn->CustPhone);
    }
    else
    {
      if (SWTMsg.GetAttrValue(8).GetLength() > 0)
      {
        strcpy(pChn->CallerNo, SWTMsg.GetAttrValue(8).C_Str());
        strcpy(pChn->CustPhone, pChn->CallerNo);
      }
    }
    if (strlen(pChn->CustPhone) > 0)
    {
      len = strlen(pIVRCfg->CentrexCode);
      if (strlen(pChn->CustPhone) > 7 && pIVRCfg->isAutoDelPSTNPreCode == true && len > 0)
      {
        if (strncmp(pChn->CustPhone, pIVRCfg->CentrexCode, len) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      if (pIVRCfg->isAutoDelMobilePreCode0 == true)
      {
        if (strncmp(pChn->CustPhone, "013", 3) == 0 || strncmp(pChn->CustPhone, "015", 3) == 0 || strncmp(pChn->CustPhone, "018", 3) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CustPhone[1], MAX_TELECODE_LEN-2);
          memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
        }
      }
      if (pIVRCfg->isAutoAddTWMobilePreCode0 == true)
      {
        if (pChn->CustPhone[0] == '9' && (int)strlen(pChn->CustPhone) == 9)
        {
          sprintf(szTemp, "0%s", pChn->CustPhone);
          strcpy(pChn->CustPhone, szTemp);
        }
      }
      if (pIVRCfg->ProcLocaAreaCodeId == 1 && strlen(pChn->CustPhone) > pIVRCfg->LocaTelCodeLen)
      {
        len = strlen(pIVRCfg->LocaAreaCode);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode1);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode1, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode2);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode2, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode3);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode3, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode4);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode4, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
      }

      if (pIVRCfg->ProcLocaAreaCodeId == 2)
      {
        if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && strlen(pIVRCfg->LocaAreaCode) > 0 && (int)strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen)
        {
          sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode, pChn->CustPhone);
          strcpy(pChn->CustPhone, szTemp);
        }
        if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && strlen(pIVRCfg->LocaAreaCode1) > 0 && (int)strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1)
        {
          sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode1, pChn->CustPhone);
          strcpy(pChn->CustPhone, szTemp);
        }
      }
      
      strcpy(pChn->CallerNo, pChn->CustPhone);
    }
    
    if (SWTMsg.GetAttrValue(10).GetLength() > 0)
    {
      pChn->CallData = SWTMsg.GetAttrValue(10).C_Str(); //傳遞的呼叫參數
    }
    pChn->nAnswerId = 1;
    //2014-01-09增加，用來傳中繼并線收到的主叫
    if (pBoxchn->isnChnAvail(nChn1))
    {
      MyTrace(3, "Proc_SWTMSG_oncallack nChn=%d pChn->CallerNo=%s nChn1=%d pChn1->CallerNo=%s",
        nChn, pChn->CallerNo, nChn1, pChn1->CallerNo);
      if (pChn1->lgChnType == CHANNEL_TRUNK)
      {
        if (strlen(pChn->CallerNo) < 4 && strlen(pChn1->CallerNo) > 6)
        {
          strcpy(pChn->CallerNo, pChn1->CallerNo);
          strcpy(pChn->CustPhone, pChn1->CallerNo);
          DBUpdateCallCDR_Phone(nChn);
        }
      }
    }
    
    if (pIVRCfg->ControlCallModal == 0)
    {
      if (pChn->ssState == CHN_SNG_IDLE || pChn->ssState == CHN_SNG_IN_ARRIVE)
      {
        SendCallinEvent(nChn, 1, 0, pChn->CallData.C_Str());
      }
      else
      {
        SendAnswerResult(nChn, OnInAnswer, "");
      }
    }
    else
    {
      if (pChn->lgChnType == CHANNEL_IVR)
      {
        //2016-03-30增加判斷是否有制定轉接的IVR群組號碼
        char szIVRNO[32];
        memset(szIVRNO, 0,32);
        strcpy(szIVRNO, GetParamByName(pChn->CallData.C_Str(), "$S_IVRNO", "", 31));
        if (strlen(szIVRNO) > 0)
        {
          if (g_nParserAccessCodeType == 1) //2016-03-31
            strcpy(pChn->CalledNo, szIVRNO);
          else
            pChn->OrgCalledNo = szIVRNO;
        }
        SendCallinEvent(nChn, 1, 0, pChn->CallData.C_Str());
      }
    }
    
    pChn->CanPlayOrRecId = 1;
    pChn->AnsTime = time(0);
    pChn->ssState = CHN_SNG_IN_TALK;
    if (pChn->lgChnType == CHANNEL_IVR)
      pChn->IVRStartTime = time(0);
    
    DispChnStatus(nChn);
    DBInsertCallTrace(nChn, "ANSWERIN");

    if (pBoxchn->isnChnAvail(nChn1))
    {
      pChn->SwtLinkType[0] = 3;
      pChn->SwtLinkChn[0] = nChn1;
      if (pChn1->lgChnType == CHANNEL_TRUNK)
      {
        if (strlen(pChn->CallerNo) > 6)
          strcpy(pChn1->CallerNo, pChn->CallerNo);
        strcpy(pChn1->CalledNo, pChn->CalledNo);
        pChn1->OrgCallerNo = pChn->OrgCallerNo;
        pChn1->OrgCalledNo = pChn->OrgCalledNo;
        pChn1->timer = 0;
        pChn1->CallTime = pChn->CallTime;
        pChn1->AnsTime = time(0);
        pChn1->ssState = CHN_SNG_IN_TALK;
        pChn1->lnState = CHN_LN_SEIZE; //占用
        if (pChn1->CallInOut == 0)
        {
          if (pChn->CallInOut == 2)
            pChn1->CallInOut = CALL_OUT;
          else
            pChn1->CallInOut = CALL_IN;
        }
        
        pChn1->ConnID = nConnID;
        DispChnStatus(nChn1);
      }
      else
      {
        if (pChn->cVopChn == NULL)
          BandVopChnByCustPhone(nChn);
      }
    }
    else
    {
      BandVopChnByCustPhone(nChn);
    }

    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      MyTrace(3, "Proc_SWTMSG_oncallack nChn=%d TranAGId=%d nAcd=%d nCallType=%d AcdedGroupNo=%d", nChn, pAgent->TranAGId, pAgent->nAcd, pChn->nCallType, pAgent->m_Worker.AcdedGroupNo);
      if (pAgent->TranAGId != 1 && pACDQueue->isnAcdAvail(pAgent->nAcd) == false && pChn->nCallType != 4) //2016-04-17增加判斷，如果是ACD分配的電話，在這里就不送應答結果，在ACD排隊處理那里會送 2016-04-28增加判斷如果是代接也不送
      {
        SendAnswerCallResult(pAgent->nAG);
      }
      else if (pChn->nCallType == 4) //2016-06-27代接時
      {
        SendAnswerCallResult(pAgent->nAG);
      }
      pAgent->nAcd = 0xFFFF; //2015-10-14 用來拒接 //2016-04-17

      if (pAgent->TranAGId == 0)
        DBInsertIVRDTMFTrace(nChn, 13, pAgent->nAG);
      
      pAgent->SetAgentsvState(AG_SV_CONN);
      WriteSeatStatus(pAgent, AG_STATUS_TALK, 1, 1, 1);
      if (pAgent->m_Worker.AcdedGroupNo > 0)
        DBUpdateCallCDR_SeatAnsTime(pAgent->nAG, pAgent->m_Worker.AcdedGroupNo);
      else
        DBUpdateCallCDR_SeatAnsTime(pAgent->nAG, pAgent->GetGroupNo());
      DispAgentStatus(pAgent->nAG);

      MyTrace(3, "Proc_SWTMSG_oncallack nChn=%d nCallType=%d ControlCallModal=%d isSeatRecordAfterTalk=%d RecState=%d AutoRecordId=%d CdrSerialNo=%s RecordFileName=%s", 
        nChn,pChn->nCallType, pIVRCfg->ControlCallModal, pIVRCfg->isSeatRecordAfterTalk, pChn->curRecData.RecState, pAgent->m_Worker.AutoRecordId, pChn->CdrSerialNo, pChn->RecordFileName);

      if (pIVRCfg->ControlCallModal == 1 && pIVRCfg->isSeatRecordAfterTalk == false && pChn->curRecData.RecState == 0)
      {
        if (pAgent->m_Worker.AutoRecordId == 1 && pAgent->m_Seat.AutoRecordId == 1)
        {
          char szTemp[256], pszError[128];
          
          //呼出的號碼是否為內部分機
          if (pIVRCfg->RecordOneFileForLocaCall == 2)
          {
            int nAG = pAgentMng->GetnAGBySeatNo(pChn->CustPhone);
            if (pAgentMng->isnAGAvail(nAG))
            {
              nChn1 = pAG->GetSeatnChn();
              //以下是本通道不錄
              if (pBoxchn->isnChnAvail(nChn1) && pChn1->ssState != 0 && strlen(pChn1->RecordFileName) > 0)
              {
                strcpy(pChn->RecordFileName, pChn1->RecordFileName);
                strcpy(pAgent->RecordFileName, pChn->RecordFileName);
                MyTrace(3, "Proc_SWTMSG_oncallack not recording nChn=%d RecordFileName=%s",nChn, pChn->RecordFileName);
                DBUpdateCallCDR_RecdFile(pAgent->nAG);
                return 0;
              }
            }
          }
          else if (pIVRCfg->RecordOneFileForLocaCall == 4)
          {
            int nAG = pAgentMng->GetnAGBySeatNo(pChn->CustPhone);
            if (pAgentMng->isnAGAvail(nAG))
            {
              nChn1 = pAG->GetSeatnChn();
              //以下是本通道不錄
              if (pBoxchn->isnChnAvail(nChn1) && pChn1->ssState != 0 && strlen(pChn1->RecordFileName) > 0)
              {
                strcpy(pChn->RecordFileName, pChn1->RecordFileName);
                strcpy(pAgent->RecordFileName, pChn->RecordFileName);
              }
            }
          }

          if (strlen(pChn->RecordFileName) == 0)
          {
            pChn->GenerateRecordFileName();  //edit by zgj 2011-07-22
          }
          strcpy(pAgent->RecordFileName, pChn->RecordFileName);
          nChn1 = pAgent->TrannChn;
          if (pBoxchn->isnChnAvail(nChn1))
          {
            strcpy(pAgent->CdrSerialNo, pChn1->CdrSerialNo);
          }
          else
          {
            strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
          }
          
          sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
          CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
          
          MyTrace(3, "Proc_SWTMSG_oncallack1 nChn=%d nChn1=%d ChnCdrSerialNo=%s ChnRecordFileName=%s AgCdrSerialNo=%s AgRecordFileName=%s pAgent->GetSeatnChn=%d FileName=%s", 
            nChn, nChn1, pChn->CdrSerialNo, pChn->RecordFileName, pAgent->CdrSerialNo, pAgent->RecordFileName, pAgent->GetSeatnChn(), FilePath.C_Str());
          
          if (pIVRCfg->isExternalRecord == 1 || CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
          {
            if (pIVRCfg->isCTILinkCanRecord == false)
            {
              if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
              {
                DBUpdateCallCDR_RecdFile(pAgent->nAG);
              }
            }
            else
            {
              Send_SWTMSG_recordfile(nChn, pChn->DeviceID, pChn->ConnID, FilePath.C_Str(), 0, 0, "");
              DBUpdateCallCDR_RecdFile(pAgent->nAG);
            }
          }
        }
      }
      //2015-12-14
      AgentStatusCount();
      SendOneGroupStatusMeteCount(pAgent->nAG);
      SendSystemStatusMeteCount();

      Send_ReqCallID_To_REC(pAgent->nAG);
    }
    break;
  case 2: //分機呼出應答
    pChn->nSwtPortID = nDesPortID;

    if (SWTMsg.GetAttrValue(9).GetLength() > 0)
    {
      strcpy(pChn->CalledNo, SWTMsg.GetAttrValue(9).C_Str());
      strcpy(pChn->CustPhone, pChn->CalledNo);
    }

    //實際通話的客戶電話號碼
//     if (SWTMsg.GetAttrValue(11).GetLength() > 0)
//     {
//       strncpy(pChn->CustPhone, SWTMsg.GetAttrValue(11).C_Str(), MAX_TELECODE_LEN-1);
//     }
    if (SWTMsg.GetAttrValue(8).GetLength() > 0)
    {
      strcpy(pChn->CallerNo, SWTMsg.GetAttrValue(8).C_Str());
    }

    if (pChn->lgChnType == CHANNEL_IVR)
    {
      CIVRCallCountParam *pIVRCallCountParam;
      pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInAnsCount(pChn->CallerNo);
      SendIVRCallCountData(pIVRCallCountParam);
    }

    if (pIVRCfg->isAutoDelDailOutPreCode)
    {
      len = strlen(pIVRCfg->SWTDialOutPreCode);
      int len1 = strlen(pIVRCfg->SWTDialOutPreCode1);
      int len2 = strlen(pIVRCfg->SWTDialOutPreCode2);
      int len3 = strlen(pIVRCfg->SWTDialOutPreCode3);
      int len4 = strlen(pIVRCfg->SWTDialOutPreCode4);
      if (len > 0 && len < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len]);
      }
      else if (len1 > 0 && len1 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode1, len1) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len1]);
      }
      else if (len2 > 0 && len2 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode2, len2) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len2]);
      }
      else if (len3 > 0 && len3 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode3, len3) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len3]);
      }
      else if (len4 > 0 && len4 < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTDialOutPreCode4, len4) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len4]);
      }
    }
    if (pIVRCfg->isAutoDelDailOutIPCode)
    {
      len = strlen(pIVRCfg->SWTIPPreCode);
      if (len > 0 && len < (int)strlen(pChn->CustPhone) && strncmp(pChn->CustPhone, pIVRCfg->SWTIPPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CustPhone[len]);
      }
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
      strcpy(pChn->CalledNo, pChn->CustPhone);
    }

    if (pIVRCfg->ProcLocaAreaCodeId == 2)
    {
      pAgent = GetAgentBynChn(nChn);
      if (pAgent)
      {
        if (pChn->CustPhone[0] != '0' && pChn->CustPhone[0] != '9' && pAgent->m_Seat.LocaAreaCode.GetLength() > 0 && (int)strlen(pChn->CustPhone) > 5)
        {
          sprintf(szTemp, "%s%s", pAgent->m_Seat.LocaAreaCode.C_Str(), pChn->CustPhone);
          strcpy(pChn->CustPhone, szTemp);
          strcpy(pChn->CalledNo, pChn->CustPhone);
        }
      }
    }
    
    memset(CdrSerialNo, 0, MAX_CHAR64_LEN);
    memset(RecordFileName, 0, MAX_CHAR128_LEN);
    memset(DialParam, 0, 256);

    if (SWTMsg.GetAttrValue(10).GetLength() > 0)
    {
      if (SWTMsg.GetAttrValue(10).CompareN("DIALOUT:", 8) == 0)
      {
        //strcpy(DialParam, SWTMsg.GetAttrValue(10).C_Str());
        sprintf(DialParam, "%s`%s`%s`%s", SWTMsg.GetAttrValue(10).C_Str(), pChn->CdrSerialNo, pChn->RecordFileName, pIVRCfg->SeatRecPath.C_Str());
      }
      else
      {
        pChn->CallData = SWTMsg.GetAttrValue(10).C_Str(); //傳遞的呼叫參數
        strcpy(CdrSerialNo, GetParamByName(SWTMsg.GetAttrValue(10).C_Str(), "$S_CDRSerialNo", "", MAX_CHAR64_LEN-1));
        strcpy(RecordFileName, GetParamByName(SWTMsg.GetAttrValue(10).C_Str(), "$S_RecdFileName", "", MAX_CHAR128_LEN-1));
        strcpy(DialParam, GetParamByName(SWTMsg.GetAttrValue(10).C_Str(), "$S_DialParam", ""));
        //如果是代接的需要更改DialParam里CdrSerialNo
      }
    }
    if (strlen(DialParam) == 0)
    {
      sprintf(DialParam, "0`%s`%s`%s", pChn->CdrSerialNo, pChn->RecordFileName, pIVRCfg->SeatRecPath.C_Str());
    }
    pChn->nAnswerId = 1;
    MyTrace(3, "Proc_SWTMSG_oncallack: nChn=%d CdrSerialNo=%s RecordFileName=%s DialParam=%s",
      nChn, pChn->CdrSerialNo, pChn->RecordFileName, DialParam);
    
    if (pIVRCfg->ControlCallModal == 0)
    {
      if (pChn->ssState == CHN_SNG_IDLE || pChn->ssState == CHN_SNG_IN_ARRIVE)
      {
        SendCallinEvent(nChn, 1, 0, pChn->CallData.C_Str());
      }
      else
      {
        SendDialoutResult(nChn, OnOutAnswer);
      }
    }
    else
    {
      if (pChn->TranIVRId == 5 && pChn->lgChnType == CHANNEL_IVR)
      {
        MyTrace(3, "Proc_SWTMSG_oncallack: nChn=%d SendCalloutResult1 SessionNo=%ld CmdAddr=%ld",
          nChn, pChn->SessionNo, pChn->CmdAddr);
        SendCalloutResult1(nChn, nChn, OnOutAnswer, "");
        pChn->TranIVRId = 0; //2015-12-28 防止再發失敗結果在FLW
      }
      else if (pChn->TranIVRId == 6 && pChn->lgChnType == CHANNEL_SEAT)
      {
        SendCallseatResult1(nChn, OnACDAnswer, "");
        pChn->TranIVRId = 0; //2015-12-28 防止再發失敗結果在FLW
      }
    }

    pChn->CanPlayOrRecId = 1;
    pChn->AnsTime = time(0);
    pChn->ssState = CHN_SNG_OT_TALK;

    DispChnStatus(nChn);
    DBInsertCallTrace(nChn, "ANSWEROUT");
    
    if (pBoxchn->isnChnAvail(nChn1))
    {
      pChn->SwtLinkType[0] = 3;
      pChn->SwtLinkChn[0] = nChn1;
      if (pChn1->lgChnType == CHANNEL_TRUNK)
      {
        strcpy(pChn1->CallerNo, pChn->CallerNo);
        strcpy(pChn1->CalledNo, pChn->CalledNo);
        pChn1->OrgCallerNo = pChn->OrgCallerNo;
        pChn1->OrgCalledNo = pChn->OrgCalledNo;
        pChn1->timer = 0;
        pChn1->CallTime = pChn->CallTime;
        pChn1->AnsTime = time(0);
        pChn1->ssState = CHN_SNG_OT_TALK;
        pChn1->lnState = CHN_LN_SEIZE; //占用
        if (pChn1->CallInOut == 0)
        {
          if (pChn->CallInOut == 2)
            pChn1->CallInOut = CALL_OUT;
          else
            pChn1->CallInOut = CALL_IN;
        }
        
        pChn1->ConnID = nConnID;
        DispChnStatus(nChn1);
      }
      else
      {
        if (pChn->cVopChn == NULL)
          BandVopChnByCustPhone(nChn);
      }
    }
    else
    {
      BandVopChnByCustPhone(nChn);
    }
    
    DBUpdateCallCDR_Phone(nChn);
    DBUpdateCallCDR_AnsTime(nChn);

    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      pAgent->nAcd = 0xFFFF; //2015-10-14 用來拒接
      DBInsertIVRDTMFTrace(nChn, 12, pAgent->nAG);
      pAgent->SetAgentsvState(AG_SV_CONN);
      WriteSeatStatus(pAgent, AG_STATUS_TALK, 1, 2, 1);
      strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo); //add by zgj 2012-05-11
      DBUpdateCallCDR_SeatAnsTime(pAgent->nAG, pAgent->m_Worker.AcdedGroupNo);
      DispAgentStatus(pAgent->nAG);
      
      UpdateDialOutAnsTime(pChn->ManDialOutId, pChn->ManDialOutListId);
      //2015-11-25 針對語音中繼的修改
      if (strlen(pAgent->m_Seat.CustPhone) > 0)
      {
        strcpy(pChn->CustPhone, pAgent->m_Seat.CustPhone);
      }

      if (pIVRCfg->isSendACDCallToAgent == true && pAgent->GetClientId() != 0x06FF)
      {
        sprintf(SendMsgBuf, "<onmakecall acdid='%d' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
          pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CallerNo, pChn->CustPhone, DialParam, OnOutAnswer, "");
        SendMsg2AG(pAgent->GetClientId(), AGMSG_onmakecall, SendMsgBuf);
      }
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=%d;callerno=%s;calledno=%s;cdrserialno=%s;param=%s",
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutAnswer, pChn->CallerNo, pChn->CustPhone, pChn->CdrSerialNo, DialParam);
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      if (RECSystemLogId == true)
      {
        sprintf(DialParam, "%s`%s`%s`", pAgent->ExtRecordCallID, pAgent->ExtRecordFileName, pAgent->ExtRecordServer);
        SendCommonResult(pAgent->nAG, AGMSG_onanswercall, 9, DialParam);
      }

      if (pIVRCfg->ControlCallModal == 1 && pIVRCfg->isSeatRecordAfterTalk == false && pChn->curRecData.RecState == 0)
      {
        if (pAgent->m_Worker.AutoRecordId == 1 && pAgent->m_Seat.AutoRecordId == 1)
        {
          char szTemp[256], pszError[128];
          
          //呼出的號碼是否為內部分機
          if (pIVRCfg->RecordOneFileForLocaCall == 1)
          {
            int nAG = pAgentMng->GetnAGBySeatNo(pChn->CustPhone);
            if (pAgentMng->isnAGAvail(nAG))
            {
              nChn1 = pAG->GetSeatnChn();
              //以下是本通道不錄
              if (pBoxchn->isnChnAvail(nChn1) && pChn1->ssState != 0 && strlen(pChn1->RecordFileName) > 0)
              {
                strcpy(pChn->RecordFileName, pChn1->RecordFileName);
                strcpy(pAgent->RecordFileName, pChn->RecordFileName);
                MyTrace(3, "Proc_SWTMSG_oncallack not recording nChn=%d RecordFileName=%s",nChn, pChn->RecordFileName);
                DBUpdateCallCDR_RecdFile(pAgent->nAG);
                return 0;
              }
            }
          }
          else if (pIVRCfg->RecordOneFileForLocaCall == 3)
          {
            int nAG = pAgentMng->GetnAGBySeatNo(pChn->CustPhone);
            if (pAgentMng->isnAGAvail(nAG))
            {
              nChn1 = pAG->GetSeatnChn();
              //以下是本通道不錄
              if (pBoxchn->isnChnAvail(nChn1) && pChn1->ssState != 0 && strlen(pChn1->RecordFileName) > 0)
              {
                strcpy(pChn->RecordFileName, pChn1->RecordFileName);
                strcpy(pAgent->RecordFileName, pChn->RecordFileName);
              }
            }
          }

          if (strlen(pChn->RecordFileName) == 0)
          {
            pChn->GenerateRecordFileName();  //edit by zgj 2011-07-22
          }
          strcpy(pAgent->RecordFileName, pChn->RecordFileName);
          
          sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
          CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
          
          if (pIVRCfg->isExternalRecord == 1 || CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
          {
            if (pIVRCfg->isCTILinkCanRecord == false)
            {
              if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
              {
                DBUpdateCallCDR_RecdFile(pAgent->nAG);
              }
            }
            else
            {
              Send_SWTMSG_recordfile(nChn, pChn->DeviceID, pChn->ConnID, FilePath.C_Str(), 0, 0, "");
              DBUpdateCallCDR_RecdFile(pAgent->nAG);
            }
          }
        }
      }
      if (RECSystemLogId == true && strlen(pAgent->ExtRecordFileName) > 0)
      {
        DBUpdateCallCDR_RecdFile(pAgent->nAG, pAgent->ExtRecordServer, pAgent->ExtRecordFileName, pAgent->ExtRecordCallID);
      }
      Send_ReqCallID_To_REC(pAgent->nAG);
    }
    break;
  case 9:
    {
      pChn->ssState = CHN_SNG_OT_TALK;
      pChn->CanPlayOrRecId = 1; //允許開始放錄音
      pChn->AnsTime = time(0);
      DispChnStatus(nChn);
    }
    break;
  case 10:
    {
      pChn->ssState = CHN_SNG_OT_TALK;
      pChn->CanPlayOrRecId = 1; //允許開始放錄音
      pChn->AnsTime = time(0);
      DispChnStatus(nChn);
      SendCallinEvent(nChn, 1, 0, pChn->CallData.C_Str());
    }
    break;
  }
	return 0;
}
//收到釋放事件
int Proc_SWTMSG_oncallrelease(CXMLMsg &SWTMsg)
{
	short nPortID, nPortID1, VopNo, VopChnNo;
  int nChn, nChn1, nResult;

  nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  nPortID1 = atoi(SWTMsg.GetAttrValue(5).C_Str());
  nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID1);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;

    nResult = atoi(SWTMsg.GetAttrValue(7).C_Str());
    if (nResult == 1)
    {
      if (pChn->ssState == CHN_SNG_OT_TALK || pChn->ssState == CHN_SNG_IN_TALK)
        UpdateDialOutHangonTime(pChn->ManDialOutId, pChn->ManDialOutListId);
      else if (pChn->ssState == CHN_SNG_OT_RING || pChn->ssState == CHN_SNG_IN_ARRIVE)
        UpdateDialOutFailTime(pChn->ManDialOutId, pChn->ManDialOutListId, 83);
      else
        UpdateDialOutFailTime(pChn->ManDialOutId, pChn->ManDialOutListId, 85);
      memset(pChn->ManDialOutId, 0, 32);
      memset(pChn->ManDialOutListId, 0, 64);
      memset(pChn->CRMCallId, 0, 64);
    }

    if (nResult == 1)
    {
      DBInsertCallTrace(nChn, "HANGON");

      if (pChn->lgChnType == CHANNEL_IVR || pChn->lgChnType == CHANNEL_REC)
      {
        if (pVopGroup != NULL || g_nSwitchType != 13) //edit 2013-09-30
        {
          if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
          {
            if (VopNo == 0)
            {
              POSTRELEASE(0, VopChnNo, 0);
              MyTrace(3, "Proc_SWTMSG_oncallrelease POSTRELEASE nChn=%d VopChnNo=%d", nChn, VopChnNo);
            }
            else
            {
              SendRelease2VOP(nChn, VopNo, VopChnNo);
            }
          }
        }
      }
      if (g_nSwitchType == 13 && pVopGroup == NULL && pBoxchn->isnChnAvail(nChn1)) //edit 2013-09-30
      {
        SendLinkChnEvent(nChn1, OnLinkHangon, "");
      }
      Release_Chn(nChn, 0, 1);
    }
    else if (nResult == 0)
    {
      DBUpdateCallCDR_RelTime(nChn);
      CAgent *pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        SendCommonResult(pAgent->nAG, AGMSG_onreleasecall, 1, "");
      }
    }
    else if (nResult == 2)
    {
      if (strlen(pChn->CdrSerialNo) > 0)
      {
        char sqls[256];
        sprintf(sqls, "delete from tbCallcdr where SerialNo='%s'", pChn->CdrSerialNo);
        SendExecSql2DB(sqls);
      }
    }
    else if (nResult == 3)
    {
      CAgent *pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        pBoxchn->ResetChn(nChn);
        DispChnStatus(nChn);

        pAgent->SetAgentsvState(AG_SV_IDEL);
        DispAgentStatus(pAgent->nAG);

        AgentStatusCount();
        SendOneGroupStatusMeteCount(pAgent->nAG);
        SendSystemStatusMeteCount();
      }
      else
      {
        HangonIVR(nChn);
        if (pChn->TranIVRId == 5)
        {
          SendCalloutResult1(nChn, 0xFFFF, OnOutFail, "");
        }
        pBoxchn->ResetChn(nChn);
        DispChnStatus(nChn);
      }
    }
    else
    {
      pBoxchn->ResetChn(nChn);
      DispChnStatus(nChn);
    }
  }

  if (pBoxchn->isnChnAvail(nChn1))
  {
    if (pChn1->lgChnType == CHANNEL_TRUNK)
    {
      //Release_Chn(nChn1, 0, 1);
      DBUpdateCallCDR_UpdateFlag(nChn);
      pBoxchn->ResetChn(nChn);
      DispChnStatus(nChn);
      MyTrace(3, "Proc_SWTMSG_oncallrelease ResetChn nChn=%d", nChn);
    }
  }
  return 0;
}
//應答來話結果
int Proc_SWTMSG_onanswercall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  int nFlag = atoi(SWTMsg.GetAttrValue(5).C_Str());

  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;

    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      MyTrace(3, "Proc_SWTMSG_onanswercall nChn=%d nFlag=%d TranAGId=%d", nChn, nFlag, pAgent->TranAGId);
      //增加應答結果 2016-02-06
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        //if (nFlag == 1 || (nFlag == 0 && pAgent->TranAGId != 1)) //2016-03-03 增加判斷條件：以免在ACD狀態下重復發送 onanswercall
        if (nFlag == 1)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=onanswercall;seatno=%s;result=%d", 
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), nFlag);
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
      }
    }
  }
	return 0;
}
//拒絕來話結果
int Proc_SWTMSG_onrefusecall(CXMLMsg &SWTMsg)
{
	return 0;
}
//發起呼叫結果
int Proc_SWTMSG_onmakecall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  int nResult = atoi(SWTMsg.GetAttrValue(6).C_Str());
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      if (nResult == 1)
      {
        SendSeatMakeCallFailResult(pAgent->nAG, "", 1, "");
      }
    }
    if (nResult == 1)
    {
      if (pChn->TranIVRId == 6)
      {
        SendCallseatResult1(nChn, OnACDFail, "");
      }
      else if (pChn->TranIVRId == 5)
      {
        SendDialoutResult(nChn, OnOutFail);
      }
    }
  }
	return 0;
}
//將來中繼來話綁定到分機結果
int Proc_SWTMSG_onbandcall(CXMLMsg &SWTMsg)
{
	return 0;
}
//將來中繼來話綁定到提示語音結果
int Proc_SWTMSG_onbandvoice(CXMLMsg &SWTMsg)
{
	return 0;
}
//保持電話結果
int Proc_SWTMSG_onholdcall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nResult;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      nResult = atoi(SWTMsg.GetAttrValue(5).C_Str());
      SendCommonResult(pAgent->nAG, AGMSG_onhold, nResult, "");
      if (nResult == 0 || nResult == 9) //2015-12-07
      {
        if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=holdstate;seatno=%s;state=1",
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str());
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
          //2015-12-07
          if (nResult == 9)
          {
            sprintf(SendMsgBuf, "wsclientid=%s;cmd=reheldresult;seatno=%s;result=0",
              pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str());
            SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
          }
        }
        WriteSeatStatus(pAgent, AG_STATUS_HOLD, 0);

        pAgent->SetAgentsvState(AG_SV_HOLD);
        DispAgentStatus(pAgent->nAG);
      }
    }
  }

	return 0;
}
//取消保持電話結果
int Proc_SWTMSG_onunholdcall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nResult;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      nResult = atoi(SWTMsg.GetAttrValue(5).C_Str());
      SendCommonResult(pAgent->nAG, AGMSG_onhold, nResult, "");
      if (nResult == 0)
      {
        if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=holdstate;seatno=%s;state=0",
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str());
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
        WriteSeatStatus(pAgent, AG_STATUS_TALK, 0);

        pAgent->SetAgentsvState(AG_SV_CONN);
        DispAgentStatus(pAgent->nAG);

        if (pIVRCfg->isInsertRecCallidList == true)
          Send_ReqCallID_To_REC(pAgent->nAG); //2016-02-12 取消保持后重新請求REC系統的CALLID
      }
    }
  }

  return 0;
}
//轉接電話結果
int Proc_SWTMSG_ontransfercall(CXMLMsg &SWTMsg)
{
  char sqls[128];
  int nAG, nAG1;
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  
  short nPortID1 = atoi(SWTMsg.GetAttrValue(5).C_Str());
  int nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID1);
  
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
    return 1;
  
  int nAcd = pChn->nAcd;
  short nResult = atoi(SWTMsg.GetAttrValue(7).C_Str());
  CAgent *pAgent = GetAgentBynChn(nChn);

  MyTrace(3, "Proc_SWTMSG_ontransfercall nChn=%d nChn1=%d nAcd=%d", nChn, nChn1, nAcd);
  if (nResult == 0) //轉接成功
  {
    if (pACDQueue->isnAcdAvail(nAcd))
    {
      if (pBoxchn->isnChnAvail(nChn1))
      {
        nAG1 = pAcd->Calleds[0].nAG;
        nAG = pChn1->nAG;
        if (pAgentMng->isnAGAvail(nAG))
        {
          if (pAgentMng->isnAGAvail(nAG1))
          {
            if (nAG1 != nAG)
            {
              //不是原來轉接的坐席，被其他坐席代接了
              SendStopACDCallIn(nAG1);
              SetAgentsvState(nAG1, AG_SV_IDEL);
              pAcd->Calleds[0].nAG = nAG;
              pChn->nCallType = 4; //代接
              pChn1->nCallType = 4; //代接
              pAG->PickupId = 1;
              if (strlen(pChn1->CdrSerialNo) > 0)
              {
                //如果代接的電話先摘機再發送代接指令，則將代接通道的話單號傳遞，以便將信息記錄到代接通道
                sprintf(sqls, "delete from tbCallcdr where SerialNo='%s'", pChn1->CdrSerialNo);
                SendExecSql2DB(sqls);
              }
            }
          }
        }
      }
      else
      {
        nChn1 = pChn->nSwtPortID;
        if (pBoxchn->isnChnAvail(nChn1))
        {
          if (pChn1->cVopChn)
          {
            if (pChn1->cVopChn->m_nVopChnType == 6 || pChn1->cVopChn->m_nVopChnType == 7)
            {
              char szTemp[256];
              sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pChn->RecordFileName);
              CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
              
              MyTrace(3, "Proc_SWTMSG_ontransfercall nChn=%d ChnCdrSerialNo=%s ChnRecordFileName=%s FileName=%s", 
                nChn, pChn->CdrSerialNo, pChn->RecordFileName, FilePath.C_Str());
              
              if (pIVRCfg->isExternalRecord == 1 || CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
              {
                if (SetChnMonitorRecordRule(nChn1, FilePath.C_Str(), 0, 0, 1) == 0)
                {
                  pChn->nSwtPortID = -1;
                  pChn->SwtLinkType[0] = 0;
                  pChn->SwtLinkChn[0] = 0xFFFF;

                  strcpy(pChn1->CdrSerialNo, pChn->CdrSerialNo);
                  DBUpdateCallCDR_ChnRecdFile(nChn);
                  MyTrace(3, "Proc_SWTMSG_ontransfercall nChn1=%d pChn1->CdrSerialNo=%s pChn->CdrSerialNo=%s",
                    nChn1, pChn1->CdrSerialNo, pChn->CdrSerialNo);
                }
              }
            }
          }
        }
      }
      if (pAcd->acdState == 3) //2014-11-13增加 當ProcACDData處理超時把狀態改為重新分配時，這時已分配的額作息已應答了，這時應該改為等待應答，然后ProcACDData處理發送分配坐席應答結果
      {
        pAcd->acdState = 2;
        pAcd->waittimer = 0;
        pAcd->ringtimer = 0;
      }
      pAcd->Calleds[0].CalloutResult = CALLOUT_CONN;
      pChn->nAcd = 0xFFFF;
    }
    else
    {
      if (pChn->TranIVRId == 0)
      {
        SendTransferIVRResult(nChn, OnSuccess, "");
        DBUpdateCallCDR_RelReason(nChn, 2);
      }
      else if (pChn->TranIVRId == 3)
      {
        SendTransferResult1(nChn, nChn1, OnOutAnswer, "");
        pChn->TranIVRId = 0;
        DBUpdateCallCDR_RelReason(nChn, 2);
        nAG = pChn->TranAG;
        if (!pAgentMng->isnAGAvail(nAG))
        {
          DBUpdateCallCDR_SeatAnsTime(nChn);
        }
        if (nPortID1 < 0)
        {
          nChn1 = pChn->nSwtPortID;
          if (pBoxchn->isnChnAvail(nChn1))
          {
            if (pChn1->cVopChn)
            {
              if (pChn1->cVopChn->m_nVopChnType == 6 || pChn1->cVopChn->m_nVopChnType == 7)
              {
                char szTemp[256];
                sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pChn->RecordFileName);
                CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
                
                MyTrace(3, "Proc_SWTMSG_ontransfercall nChn=%d ChnCdrSerialNo=%s ChnRecordFileName=%s FileName=%s", 
                  nChn, pChn->CdrSerialNo, pChn->RecordFileName, FilePath.C_Str());
                
                if (pIVRCfg->isExternalRecord == 1 || CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
                {
                  if (SetChnMonitorRecordRule(nChn1, FilePath.C_Str(), 0, 0, 1) == 0)
                  {
                    pChn->nSwtPortID = -1;
                    pChn->SwtLinkType[0] = 0;
                    pChn->SwtLinkChn[0] = 0xFFFF;

                    strcpy(pChn1->CdrSerialNo, pChn->CdrSerialNo);
                    DBUpdateCallCDR_ChnRecdFile(nChn);
                    MyTrace(3, "Proc_SWTMSG_ontransfercall nChn1=%d pChn1->CdrSerialNo=%s pChn->CdrSerialNo=%s",
                      nChn1, pChn1->CdrSerialNo, pChn->CdrSerialNo);
                  }
                }
              }
            }
          }
        }
      }
      else
      {
        Release_Chn(nChn, 1, 1);
      }
    }
    if (pAgent != NULL)
    {
      SendSeatTranCallFailResult(pAgent->nAG, SWTMsg.GetAttrValue(6).C_Str(), "", OnSuccess, "");
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnSuccess, "");
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      pAgent->IncTranCallCount();
    }
  }
  else if (nResult == 1 || nResult == 3) //轉接失敗
  {
    if (pACDQueue->isnAcdAvail(nAcd))
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_FAIL;
      //pChn->nAcd = 0xFFFF; //2013-06-28 隱形錯誤，當分配座席失敗時繼續等待時不能清除該nAcd標志，不然后面有座席應答的時候pChn->nAcd會=65535，而找不到nAcd
    }
    else if (pChn->TranIVRId == 3)
    {
      SendTransferResult1(nChn, nChn1, OnOutFail, "");
      pChn->TranIVRId = 0;
    }
    else if (pChn->TranIVRId == 4)
    {
      SendCalloutResult1(nChn, nChn1, OnOutFail, "");
    }
    else
    {
      SendTransferIVRResult(nChn, OnOutFail, "");
    }
    if (pAgent != NULL)
    {
      SendSeatTranCallFailResult(pAgent->nAG, SWTMsg.GetAttrValue(6).C_Str(), "", OnOutFail, "");
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutFail, "");
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      pAgent->TranDesnAG = 0xFFFF;
    }
  }
  else if (nResult == 4) //遇忙 //2015-07-15增加失敗類型遇忙時
  {
    if (pACDQueue->isnAcdAvail(nAcd))
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_SLB; //CALLOUT_FAIL; 2015-07-15改成遇忙
      //pChn->nAcd = 0xFFFF; //2013-06-28 隱形錯誤，當分配座席失敗時繼續等待時不能清除該nAcd標志，不然后面有座席應答的時候pChn->nAcd會=65535，而找不到nAcd
    }
    else if (pChn->TranIVRId == 3)
    {
      SendTransferResult1(nChn, nChn1, OnOutBusy, "");
      pChn->TranIVRId = 0;
    }
    else if (pChn->TranIVRId == 4)
    {
      SendCalloutResult1(nChn, nChn1, OnOutBusy, "");
    }
    else
    {
      SendTransferIVRResult(nChn, OnOutBusy, "");
    }
    if (pAgent != NULL)
    {
      SendSeatTranCallFailResult(pAgent->nAG, SWTMsg.GetAttrValue(6).C_Str(), "", OnOutBusy, "");
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutBusy, "");
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }

      if (pAgent->svState == AG_SV_ACDLOCK)
        SetAgentsvState(pAgent, AG_SV_IDEL);

      pAgent->TranDesnAG = 0xFFFF;

      DispAgentStatus(pAgent->nAG);
    }
  }
  else if (nResult == 5) //空號 //2015-07-15增加失敗類型號碼不存在空號時
  {
    if (pACDQueue->isnAcdAvail(nAcd))
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_UNN;
      //pChn->nAcd = 0xFFFF; //2013-06-28 隱形錯誤，當分配座席失敗時繼續等待時不能清除該nAcd標志，不然后面有座席應答的時候pChn->nAcd會=65535，而找不到nAcd
    }
    else if (pChn->TranIVRId == 3)
    {
      SendTransferResult1(nChn, nChn1, OnOutUNN, "");
      pChn->TranIVRId = 0;
    }
    else if (pChn->TranIVRId == 4)
    {
      SendCalloutResult1(nChn, nChn1, OnOutUNN, "");
    }
    else
    {
      SendTransferIVRResult(nChn, OnOutUNN, "");
    }
    if (pAgent != NULL)
    {
      SendSeatTranCallFailResult(pAgent->nAG, SWTMsg.GetAttrValue(6).C_Str(), "", OnOutUNN, "");
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutUNN, "");
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      pAgent->TranDesnAG = 0xFFFF;
    }
  }
  else if (nResult == 2) //轉接振鈴
  {
    if (pACDQueue->isnAcdAvail(nAcd))
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_RING;
    }
    else if (pChn->TranIVRId == 3)
    {
      nAG = pChn->TranAG;
      if (!pAgentMng->isnAGAvail(nAG))
      {
        DBUpdateCallCDR_RingTime(nChn);
      }

      SendTransferResult1(nChn, nChn1, OnOutRing, "");
    }
    else if (pChn->TranIVRId == 4)
    {
      SendCalloutResult1(nChn, nChn1, OnOutRing, "");
    }
    //2015-10-13 add
    if (pAgent != NULL)
    {
      SendSeatTranCallFailResult(pAgent->nAG, SWTMsg.GetAttrValue(6).C_Str(), "", OnOutRing, "");
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutRing, "");
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
    }
  }
  else if (nResult == 9) //盲轉成功
  {
    if (pACDQueue->isnAcdAvail(nAcd))
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_CONN;
      pChn->nAcd = 0xFFFF;
    }
    else
    {
      if (pChn->TranIVRId == 0)
      {
        SendTransferIVRResult(nChn, OnSuccess, "");
      }
      else if (pChn->TranIVRId == 3)
      {
        nAG = pChn->TranAG;
        if (!pAgentMng->isnAGAvail(nAG))
        {
          DBUpdateCallCDR_SeatAnsTime(nChn);
        }
        SendTransferResult1(nChn, nChn1, OnTranTalk, "");
        pChn->TranIVRId = 0;
      }
      else if (pChn->TranIVRId == 4)
      {
        SendCalloutResult1(nChn, nChn1, OnTranTalk, "");
      }
      else
      {
        Release_Chn(nChn, 1, 1);
      }
    }
    if (pAgent != NULL)
    {
      SendSeatTranCallFailResult(pAgent->nAG, SWTMsg.GetAttrValue(6).C_Str(), "", OnSuccess, "");
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnSuccess, "");
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      pAgent->IncTranCallCount();
    }
  }
  
	return 0;
}
//停止轉接電話結果
int Proc_SWTMSG_onstoptransfercall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      SendCommonResult(pAgent->nAG, AGMSG_onstoptrancall, atoi(SWTMsg.GetAttrValue(5).C_Str()), "");
    }
  }
  return 0;
}
//穿梭通話結果
int Proc_SWTMSG_onswapcall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      SendCommonResult(pAgent->nAG, AGMSG_onswaphold, atoi(SWTMsg.GetAttrValue(5).C_Str()), "");
    }
  }
	return 0;
}
//代接電話結果
int Proc_SWTMSG_onpickupcall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      if (atoi(SWTMsg.GetAttrValue(5).C_Str()) == 1)
      {
        SendSeatPickupFailResult(pAgent->nAG, "", OnFail, "");
        
        if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnFail, "");
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
      }
    }
  }
	return 0;
}
//強插通話結果
int Proc_SWTMSG_onbreakincall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      if (atoi(SWTMsg.GetAttrValue(5).C_Str()) == 1)
      {
        SendSeatBreakinFailResult(pAgent->nAG, "", OnFail, "");
      }
    }
  }
	return 0;
}
//監聽電話結果
int Proc_SWTMSG_onlistencall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      if (atoi(SWTMsg.GetAttrValue(5).C_Str()) == 1)
      {
        SendSeatListenFailResult(pAgent->nAG, "", OnFail, "");
      }
    }
  }
	return 0;
}
//強拆電話結果
int Proc_SWTMSG_onremovecall(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      
    }
  }
	return 0;
}
//三方通話結果
int Proc_SWTMSG_onthreeconf(CXMLMsg &SWTMsg)
{
  CAgent *pAgent=NULL;
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      
    }
  }
	return 0;
}
//將電話保持到話務臺隊列結果
int Proc_SWTMSG_onconsolehold(CXMLMsg &SWTMsg)
{
	return 0;
}
//從話務臺隊列提取電話結果
int Proc_SWTMSG_onconsolepickup(CXMLMsg &SWTMsg)
{
	return 0;
}
//發送交換機操作指令結果
int Proc_SWTMSG_onsendswitchcmd(CXMLMsg &SWTMsg)
{
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  int nResult = atoi(SWTMsg.GetAttrValue(4).C_Str());
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    if (nResult == 9)
    {
      if (strlen(pIVRCfg->OXEAlternateCallDTMF) > 0)
        SendDTMFStr(nChn, pIVRCfg->OXEAlternateCallDTMF);
      return 0;
    }
    SendSendSwitchCMDResult(nChn, nResult);
  }
  return 0;
}
//收到呼叫通話連接事件
int Proc_SWTMSG_oncallconnected(CXMLMsg &SWTMsg)
{
  CAgent *pAgent;
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  short nPortID1 = atoi(SWTMsg.GetAttrValue(5).C_Str());
  int nChn1 = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID1);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->lgChnType == CHANNEL_TRUNK) //add by zgj 2011-11-30
      return 1;
    
    if (atoi(SWTMsg.GetAttrValue(7).C_Str()) == 1)
    {
      if (pIVRCfg->ControlCallModal == 1 && pChn->curRecData.RecState == 0)
      {
        if (pBoxchn->isnChnAvail(nChn1))
        {
          if (pChn1->lgChnType == CHANNEL_TRUNK) //2014-05-19  防止轉接后會把轉接后的錄音停了
            pBoxchn->ClearnSwtPortID(nPortID1);

          if (pChn->nSwtPortID < 0)
            pChn->nSwtPortID = nPortID1;
        }
        pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          if (pAgent->m_Worker.AutoRecordId == 1 && pAgent->m_Seat.AutoRecordId == 1)
          {
            char szTemp[256], pszError[128];
            
            if (strlen(pChn->RecordFileName) == 0)
            {
              pChn->GenerateRecordFileName();  //edit by zgj 2011-07-22
            }
            strcpy(pAgent->RecordFileName, pChn->RecordFileName);
            strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo); //add by zgj 2012-05-11

            sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
            CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
            
            if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
            {
              BandVopChnByCustPhone(nChn);
              if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
              {
                DBUpdateCallCDR_RecdFile(pAgent->nAG);
              }
            }
          }
        }
      }
      if (pBoxchn->isnChnAvail(nChn1))
      {
        if (pIVRCfg->ControlCallModal == 1 && pChn1->curRecData.RecState == 0)
        {
          pAgent = GetAgentBynChn(nChn1);
          if (pAgent != NULL)
          {
            if (pAgent->m_Worker.AutoRecordId == 1 && pAgent->m_Seat.AutoRecordId == 1)
            {
              char szTemp[256], pszError[128];
              
              if (strlen(pChn1->RecordFileName) == 0)
              {
                pChn1->GenerateRecordFileName();  //edit by zgj 2011-07-22
              }
              strcpy(pAgent->RecordFileName, pChn1->RecordFileName);
              
              sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
              CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
              
              if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
              {
                if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
                {
                  DBUpdateCallCDR_RecdFile(pAgent->nAG);
                }
              }
            }
          }
        }
      }
      SendCallConnectedEvent(nChn, OnCallConnected);
    }
    else
    {
      SendCallConnectedEvent(nChn, OnCallUnConnected);
    }
  }
  return 0;
}
//錄音結果
int Proc_SWTMSG_onrecordresult(CXMLMsg &SWTMsg)
{
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  int nResult = atoi(SWTMsg.GetAttrValue(5).C_Str());
  
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (nResult == 10)
      pChn->curRecState = 10;
    else
      pChn->curRecState = 5;
  }
  return 0;
}
//放音結果
int Proc_SWTMSG_onplayresult(CXMLMsg &SWTMsg)
{
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  int nResult = atoi(SWTMsg.GetAttrValue(5).C_Str());
  
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (nResult == 10)
      pChn->curPlayState = 10; //放音錯誤
    else
      pChn->curPlayState = 2; //結束停止
  }
  return 0;
}
//收碼結果
int Proc_SWTMSG_onrecvdtmf(CXMLMsg &SWTMsg)
{
  short nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  int nResult = atoi(SWTMsg.GetAttrValue(5).C_Str());
  
  int len, len1, len2;
  if (pBoxchn->isnChnAvail(nChn))
  {
    len1 = strlen(pChn->curRecvDtmf);
    len2 = strlen(SWTMsg.GetAttrValue(6).C_Str());
    if ((len1+len2) >= MAX_DTMFBUF_LEN)
    {
      len = len1+len2-MAX_DTMFBUF_LEN+1;
      memcpy(pChn->curRecvDtmf, &pChn->curRecvDtmf[len], MAX_DTMFBUF_LEN-len);
      pChn->curRecvDtmf[MAX_DTMFBUF_LEN-len-1] = 0;
      strncat(pChn->curRecvDtmf, SWTMsg.GetAttrValue(6).C_Str(), len2);
      pChn->curRecvDtmf[MAX_DTMFBUF_LEN] = 0;
    }
    else
    {
      strncat(pChn->curRecvDtmf, SWTMsg.GetAttrValue(6).C_Str(), len2);
    }
  }
  
  return 0;
}
//查詢ACD話務組統計狀態結果 v3.3
int Proc_SWTMSG_onqueryacdsplit(CXMLMsg &SWTMsg)
{
  short nAvailAgents, nCallsInQueue, nLoginAgents;

  nAvailAgents = atoi(SWTMsg.GetAttrValue(3).C_Str());
  nCallsInQueue = atoi(SWTMsg.GetAttrValue(4).C_Str());
  nLoginAgents = atoi(SWTMsg.GetAttrValue(5).C_Str());
  gACDSplitMng.SetData(SWTMsg.GetAttrValue(2).C_Str(), nAvailAgents, nCallsInQueue, nLoginAgents);

  if (SWTMsg.GetAttrValue(2).Compare("ALL") == 0)
  {
    SendACDQueueInfoToAll(nCallsInQueue, SWTMsg.GetAttrValue(6));
  }

  return 0;
}
//agent登錄交換機事件 v3.3
int Proc_SWTMSG_onagentloggedon(CXMLMsg &SWTMsg)
{
  int workerno, groupno;
  char szAgentName[32];
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  CAgent *pAgent=NULL;
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    CACDSplit *pACDSplit = gACDSplitMng.GetACDSplit(SWTMsg.GetAttrValue(5).C_Str());
    if (pACDSplit)
      groupno = pACDSplit->m_nGroupNo;
    else
      groupno = 0;
    workerno = atoi(SWTMsg.GetAttrValue(4).C_Str());
    sprintf(szAgentName, "AG%d", workerno);
    if (pAgent->m_Worker.WorkerName.GetLength() == 0)
      pAgent->m_Worker.WorkerLogin(workerno, szAgentName, 1);
    else
      pAgent->m_Worker.WorkerLogin(workerno);
    pAgent->m_Worker.SetWorkerGroupNo(groupno, 1);
    pAgent->ChangeStatus(AG_STATUS_LOGIN);
    MyTrace(3, "WriteWorkerLoginStatus nAG=%d Proc_SWTMSG_onagentloggedon()", pAgent->nAG);
    WriteWorkerLoginStatus(pAgent);

    DispAgentStatus(pAgent->nAG);
  }
  return 0;
}
//agent登出交換機事件 v3.3
int Proc_SWTMSG_onagentloggedoff(CXMLMsg &SWTMsg)
{
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  CAgent *pAgent=NULL;
  pAgent = GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    pAgent->m_Worker.Logout();
    pAgent->m_Worker.ClearState();
    SendCommonResult(pAgent->nAG, AGMSG_onworkerlogout, OnSuccess, "");
    DispAgentStatus(pAgent->nAG);
  }
  return 0;
}
//設置轉移號碼結果
int Proc_SWTMSG_onsettranphone(CXMLMsg &SWTMsg)
{
  char sqls[256];
  int nPortID = atoi(SWTMsg.GetAttrValue(2).C_Str());
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(g_nSwitchType, nPortID);
  
  if (pBoxchn->isnChnAvail(nChn)) //設置呼叫轉移會話序列號 2016-07-19
  {
    sprintf(SendMsgBuf, "<onsetforwarding sessionid='%lu' cmdaddr='%lu' chantype='0' channo='0' result='%d' forwardtype='%d' forwardonoff='%d' forwarddeviceno='%s' errorbuf=''/>", 
      pChn->SetForwardingSessionNo, pChn->SetForwardingCmdAddr, atoi(SWTMsg.GetAttrValue(7).C_Str()), atoi(SWTMsg.GetAttrValue(4).C_Str()), atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
    SendMsg2XML(MSG_onsetforwarding, pChn->SetForwardingSessionNo, SendMsgBuf);
    pChn->SetForwardingSessionNo = 0;
    pChn->SetForwardingCmdAddr = 0;
    
    sprintf(sqls, "update tbAgState set FwdType=%d,FwdOnOff=%d,FwdNo='%s' WHERE SeatNo='%s'",
      atoi(SWTMsg.GetAttrValue(4).C_Str()), atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str(), SWTMsg.GetAttrValue(3).C_Str());
    SendExecSql2DB(sqls);
  }
  else if (g_SetForwardingSessionNo > 0)
  {
    sprintf(SendMsgBuf, "<onsetforwarding sessionid='%lu' cmdaddr='%lu' chantype='0' channo='0' result='%d' forwardtype='%d' forwardonoff='%d' forwarddeviceno='%s' errorbuf=''/>", 
      g_SetForwardingSessionNo, g_SetForwardingCmdAddr, atoi(SWTMsg.GetAttrValue(7).C_Str()), atoi(SWTMsg.GetAttrValue(4).C_Str()), atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
    SendMsg2XML(MSG_onsetforwarding, g_SetForwardingSessionNo, SendMsgBuf);
    g_SetForwardingSessionNo = 0;
    g_SetForwardingCmdAddr = 0;
    
    sprintf(sqls, "update tbAgState set FwdType=%d,FwdOnOff=%d,FwdNo='%s' WHERE SeatNo='%s'",
      atoi(SWTMsg.GetAttrValue(4).C_Str()), atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str(), SWTMsg.GetAttrValue(3).C_Str());
    SendExecSql2DB(sqls);
  }
  
  return 0;
}

//自定義一個函數指針類型
typedef int (*Proc_SwtMSG)(CXMLMsg &SWTMsg);
Proc_SwtMSG proc_swtmsg[]=
{
	Proc_SWTMSG_onconnectlink,	//連接CTILinkService結果
	Proc_SWTMSG_onlinkstatus, //返回CTILink接口與交換機的連接狀態
	Proc_SWTMSG_ongetdevnnum,	//返回交換機設備配置數量
	Proc_SWTMSG_ongetdevparam,	//返回交換機設備的配置參數
	Proc_SWTMSG_onportstatus,	//端口狀態事件

	Proc_SWTMSG_onagentstatus,	//坐席狀態事件
	Proc_SWTMSG_onroomstatus,	//房間狀態事件
	Proc_SWTMSG_onlampevent,	//留言燈狀態事件
	Proc_SWTMSG_onwakeevent,	//叫醒狀態事件
	Proc_SWTMSG_onconsoleevent,	//話務臺隊列事件
	Proc_SWTMSG_onbillevent, //收到呼叫話單事件

	Proc_SWTMSG_oncallevent, //呼叫事件
	Proc_SWTMSG_oncallsam, //收到后續地址事件
	Proc_SWTMSG_oncallacm, //號碼收全事件
	Proc_SWTMSG_oncallring, //收到振鈴事件
	Proc_SWTMSG_oncallfail, //收到呼叫失敗事件
	Proc_SWTMSG_oncallack, //收到應答事件
	Proc_SWTMSG_oncallrelease, //收到釋放事件

	Proc_SWTMSG_onanswercall,	//應答來話結果
	Proc_SWTMSG_onrefusecall,	//拒絕來話結果
	Proc_SWTMSG_onmakecall,	//發起呼叫結果
	Proc_SWTMSG_onbandcall,	//將來中繼來話綁定到分機結果
	Proc_SWTMSG_onbandvoice,	//將來中繼來話綁定到提示語音結果
	Proc_SWTMSG_onholdcall,	//保持電話結果
	Proc_SWTMSG_onunholdcall,	//取消保持電話結果
	Proc_SWTMSG_ontransfercall,	//轉接電話結果
  Proc_SWTMSG_onstoptransfercall,	//停止轉接電話結果
	Proc_SWTMSG_onswapcall,	//穿梭通話結果
	Proc_SWTMSG_onpickupcall,	//代接電話結果
	Proc_SWTMSG_onbreakincall,	//強插通話結果
	Proc_SWTMSG_onlistencall,	//監聽電話結果
	Proc_SWTMSG_onremovecall,	//強拆電話結果
	Proc_SWTMSG_onthreeconf,	//三方通話結果
	Proc_SWTMSG_onconsolehold,	//將電話保持到話務臺隊列結果
	Proc_SWTMSG_onconsolepickup,	//從話務臺隊列提取電話結果
  Proc_SWTMSG_onsendswitchcmd,	//發送交換機操作指令結果
  Proc_SWTMSG_oncallconnected, //收到呼叫通話連接事件
  Proc_SWTMSG_onrecordresult,	//錄音結果
  Proc_SWTMSG_onplayresult,	//放音結果
  Proc_SWTMSG_onrecvdtmf,	//收碼結果
  Proc_SWTMSG_onqueryacdsplit, //查詢ACD話務組統計狀態結果
  Proc_SWTMSG_onagentloggedon, //agent登錄交換機事件
  Proc_SWTMSG_onagentloggedoff, //agent登出交換機事件
  Proc_SWTMSG_onsettranphone, //設置轉移號碼結果
};
//處理監控發來的消息
void ProcSWTMsg(CXMLMsg &SWTMsg)
{
	if (SWTMsg.GetMsgId() < MAX_SWTIVRMSG_NUM)
	{
    //TRACE("SWTMsg.GetMsgId()=%d\n", SWTMsg.GetMsgId());
    proc_swtmsg[SWTMsg.GetMsgId()](SWTMsg);
	}
}
