// TCPServer.cpp: implementation of the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#include "stdafx.h"
#include "tcplink.h"
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

CTCPLink::CTCPLink()
{
	IsOpen = false;
  isIVRInit = false;
	hdll = NULL;
  strcpy(DllFile, "tmtcpserver.dll");
  IVRServerId = 0x0001;
  IVRServerPort = 5225;
  LoadDll();
}

CTCPLink::~CTCPLink()
{
  ReleaseDll();
}

bool CTCPLink::LoadDll()
{
	hdll = LoadLibrary(DllFile);
	if ( hdll != NULL )
	{
    AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(  !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MyTrace(0, "GetProcAddress tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MyTrace(0, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		return false;
	}
}

void CTCPLink::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}

bool CTCPLink::ReadIni(char *filename)
{
  m_nMultiRunMode = GetPrivateProfileInt("CONFIGURE", "MultiRunMode", 0, filename);
  m_nHostType = GetPrivateProfileInt("CONFIGURE", "HostType", 0, filename);

  isIVRInit = false;
  IVRServerId = CombineID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "IVRServerId", 1, filename));
  IVRServerPort = GetPrivateProfileInt("IVRSERVER", "IVRServerPort", 5225, filename);

  CRMServer.IsConnected=false;
  GetPrivateProfileString("DUMPCRMDB", "GWServerIP", "127.0.0.1", CRMServer.ServerIP, 30, filename);
  CRMServer.ServerPort = GetPrivateProfileInt("DUMPCRMDB", "GWServerPort", 5003, filename);
  CRMServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("DUMPCRMDB", "GWServerID", 0, filename));

  GWServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "GWServerIP", "127.0.0.1", GWServer.ServerIP, 30, filename);
  GWServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "GWServerPort", 5001, filename);
  GWServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("IVRSERVER", "GWServerID", 0, filename));
  
  MasterIVRServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "MASTERIVRServerIP", "127.0.0.1", MasterIVRServer.ServerIP, 30, filename);
  MasterIVRServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "MASTERIVRServerPort", 5227, filename);
  MasterIVRServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "MASTERIVRServerID", 1, filename));

  return true;
}    

bool CTCPLink::ReadWebSetupIni(char *filename)
{
  m_nMultiRunMode = GetPrivateProfileInt("CONFIGURE", "MultiRunMode", 0, filename);
  m_nHostType = GetPrivateProfileInt("CONFIGURE", "HostType", 0, filename);

  isIVRInit = false;
  IVRServerId = CombineID(NODE_IVR,GetPrivateProfileInt("TCPLINK", "IVRServerID", 1, filename));
  IVRServerPort = GetPrivateProfileInt("TCPLINK", "IVRServerPort", 5225, filename);
  
  CRMServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "CRMServerIP", "127.0.0.1", CRMServer.ServerIP, 30, filename);
  CRMServer.ServerPort = GetPrivateProfileInt("TCPLINK", "CRMServerPort", 5003, filename);
  CRMServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("TCPLINK", "CRMServerID", 0, filename));

  GWServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "GWServerIP", "127.0.0.1", GWServer.ServerIP, 30, filename);
  GWServer.ServerPort = GetPrivateProfileInt("TCPLINK", "GWServerPort", 5001, filename);
  GWServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("TCPLINK", "GWServerID", 0, filename));

  MasterIVRServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "MASTERIVRServerIP", "127.0.0.1", MasterIVRServer.ServerIP, 30, filename);
  MasterIVRServer.ServerPort = GetPrivateProfileInt("TCPLINK", "MASTERIVRServerPort", 5227, filename);
  MasterIVRServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("TCPLINK", "MASTERIVRServerID", 1, filename));

  return true;
}    

int CTCPLink::SendMessage(US ServerId, US MsgId, int MsgLen, const CH *MsgBuf)
{
  int SendedLen;
  
  SendedLen = AppSendData(ServerId, MsgId, (UC *)MsgBuf, MsgLen);
  if (SendedLen != MsgLen)
    MyTrace(0, "Send msg error ServerId=%04x MsgId=%04x MsgBuf=%s", ServerId, MsgId, MsgBuf);
  return SendedLen;
}
//IVR發送消息到客戶端
int CTCPLink::SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf)
{
	if (Clientid == 0) return 0;
	int SendedLen, MsgLen;
  MsgLen = strlen(MsgBuf);
  SendedLen = AppSendData(Clientid, MsgId, (UC *)MsgBuf , MsgLen);
  //printf("Clientid=%04x MsgId=%04x MsgBuf=%s\n", Clientid, MsgId, MsgBuf);
  if (SendedLen != MsgLen)
  {
    MyTrace(0, "IVR send msg error Clientid=%04x MsgId=%d MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPLink::SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len)
{
	if (Clientid == 0) return 0;
	int SendedLen;
  SendedLen = AppSendData(Clientid, MsgId, MsgBuf , len);
  if (SendedLen != len)
  {
    MyTrace(0, "IVR send msg error Clientid=%04x MsgId=%d MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPLink::SendMessage2Master(US MsgId, const CH *MsgBuf)
{
  return SendMessage(MasterIVRServer.Serverid, (MSGTYPE_HAIVR<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPLink::CRMSendMessage(US MsgId, const CH *MsgBuf)
{
  return SendMessage(CRMServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPLink::CRMSendMessage(US MsgId, const CStringX &MsgBuf)
{
  return SendMessage(CRMServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, MsgBuf.GetLength(), MsgBuf.C_Str());
}
int CTCPLink::GWSendMessage(US MsgId, const CH *MsgBuf)
{
  return SendMessage(GWServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPLink::GWSendMessage(US MsgId, const CStringX &MsgBuf)
{
  return SendMessage(GWServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, MsgBuf.GetLength(), MsgBuf.C_Str());
}
int CTCPLink::InitIVRServer()
{
  if (isIVRInit == true)
    return 2;
  if (IsOpen == true)
  {
    AppInitS(IVRServerId, IVRServerPort, OnIVRLogin, OnIVRClose, OnIVRReceiveData);
    MyTrace(0, "Init TCPLink ServerId=0x%04x ServerPort=%d", 
      IVRServerId, IVRServerPort);
    isIVRInit = true;
    return 0;
  }
  else
  {
    return 1;
  }
}
int CTCPLink::ConnectCRMServer()
{
  if (IsOpen == true)
  {
    if ((CRMServer.Serverid&0xFF) == 0) return 0;
    AppInit(CRMServer.Serverid,
      IVRServerId,
      CRMServer.ServerIP,
      CRMServer.ServerPort,
      OnCRMLogin,
      OnCRMClose,
      OnCRMReceiveData);
    MyTrace(0, "Connect CRMServer LocaClientId=0x%04x ServerId=0x%04x ServerPort=%d ServerIP=%s", 
      IVRServerId, CRMServer.Serverid, CRMServer.ServerPort, CRMServer.ServerIP);
    return 0;
  }
  else
  {
    //加載動態連接庫失敗
    return 1;
  }
}
int CTCPLink::ConnectGWServer()
{
  if (IsOpen == true)
  {
    if ((GWServer.Serverid&0xFF) == 0) return 0;
    AppInit(GWServer.Serverid,
      IVRServerId,
      GWServer.ServerIP,
      GWServer.ServerPort,
      OnGWLogin,
      OnGWClose,
      OnGWReceiveData);
    MyTrace(0, "Connect GWServer LocaClientId=0x%04x ServerId=0x%04x ServerPort=%d ServerIP=%s", 
      IVRServerId, GWServer.Serverid, GWServer.ServerPort, GWServer.ServerIP);
    return 0;
  }
  else
  {
    //加載動態連接庫失敗
    return 1;
  }
}
int CTCPLink::InitTCPLink()
{
  ConnectCRMServer();
  ConnectGWServer();
  InitIVRServer();
  return 0;
}
