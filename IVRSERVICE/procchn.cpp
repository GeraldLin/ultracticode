//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procchn.h"
#include "extern.h"

//---------------------------------------------------------------------------
int nCallInCount=0;
//-----------------------通道事物處理函數--------------------------------------
void DispChnStatus(int nChn)
{
  SendOneCHStateToAll(nChn);
}

//計時器
void TimerAdd()
{
  static time_t oldt=0;
  static time_t oldRegt=0;
  time_t newt;
  int tcount;
	
  time(&newt);
  if (oldt == 0)
  {
    oldt=newt;
    return;
  }
  if (g_nWriteRegRunDog == 1)
  {
    if ((newt-oldRegt) >= 60)
    {
      WriteRegRunTimerDog();
      oldRegt=newt;
    }
  }

	if(newt!=oldt)
	{
		tcount = newt-oldt;
    oldt=newt; //每秒一次
    pBoxchn->TimerAdd(tcount);
    pBoxconf->TimerAdd(tcount);
    pCallbuf->TimerAdd(tcount);
    pCalloutQue->TimerAdd(tcount);
    pACDQueue->TimerAdd(tcount);
    AgentTimerAdd(tcount);
  }
}
//通道事物處理
void ProcessChn()
{
  int nChn;
  static int authcount=0, timercount=0;
  static bool authfail=false, writerundays=false, bcountseatstatus=false;
  CAgent *pAgent=NULL;

  struct tm *local;
  time_t lt;
  char authdate[20];
  lt=time(NULL);
  local=localtime(&lt);

  if (pIVRCfg->isWriteTimerDogLogId == true && timercount++ > pIVRCfg->WriteTimerDogLogCount)
  {
    timercount = 0;
    MyTrace(0, "Timer Action");
  }
  if (local->tm_hour == 23)
    writerundays = true;
  if (writerundays == true && local->tm_hour == 0 && local->tm_min > 4)
  {
    if (g_dwRegRunDays > 1)
      strcpy(g_szLastDate, MyIncDays(g_szFirstDate, g_dwRegRunDays-1));
    g_dwRegRunDays++;
    WriteRegRunDays();
    writerundays = false;
    MyTrace(0, "License RegRunDays=%d FirstDate=%s LastDate=%s", g_dwRegRunDays, g_szFirstDate, g_szLastDate);
  }
  if (authfail == true)
    return;
  if (authcount++ > 100)
  {
    //判斷授權運行結束時間
    sprintf(authdate, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
//     if ((strcmp(g_szLastDate, authdate) > 0 || strcmp(g_szLastDate, AuthEndDate) > 0) && g_nCallInCount > MAXCALLCOUNTONAUTHFAIL)
//     {
//       //當前日期小于注冊表累計的日期
//       authfail = true;
//       AuthPassId = false;
//       MyTrace(0, "License RegData is error g_szLastDate=%s authdate=%s AuthEndDate=%s g_nCallInCount=%d !!!",
//         g_szLastDate, authdate, AuthEndDate, g_nCallInCount);
//     }
//     else if (strcmp(authdate, AuthEndDate) > 0 || (AuthPassId == false && g_nCallInCount > MAXCALLCOUNTONAUTHFAIL))
    if (strcmp(authdate, AuthEndDate) > 0 || (AuthPassId == false && g_nCallInCount > MAXCALLCOUNTONAUTHFAIL))
    {
      authfail = true;
      AuthPassId = false;
      MyTrace(0, "License RegData is error AuthPassId=%d g_szLastDate=%s authdate=%s AuthEndDate=%s g_nCallInCount=%d !!!",
        AuthPassId == false ? 0 : 1, g_szLastDate, authdate, AuthEndDate, g_nCallInCount);
    }
    authcount = 0;
  }

  int TrkChnBusyCount = 0; //中繼占用數
  int TrkChnIdelCount = 0; //中繼空閑數
  int TrkChnBlockCount = 0; //中繼阻塞數
  int TrkChnInCount = 0; //中繼呼入數
  int TrkChnOutCount = 0; //中繼呼出數
  
  int IvrChnTotalCount = 0; //IVR總數
  int IvrChnBusyCount = 0; //IVR占用數
  int IvrChnIdelCount = 0; //IVR空閑數
  int IvrChnBlockCount = 0; //IVR阻塞數

  int AgLoginCount = 0; //登錄坐席數
  int AgLogoutCount = 0; //登出坐席數
  int AgIdelCount = 0; //空閑坐席數
  int AgBusyCount = 0; //忙坐席數
  int AgLeavelCount = 0; //示忙離席數
  int AgTalkCount = 0; //通話離席數
  
  for (nChn = 0; nChn < pBoxchn->MaxChnNum; nChn ++)
	{
    if (pChn->state == 0) continue;

    ProcessChnCall(nChn);
    ProcessChnDTMF(nChn);
    ProcessChnPlay(nChn);
    ProcessChnRec(nChn);
    //2015-05-30
    if (pChn->lgChnType == 2)
      Send_ReqCallID_To_REC1(nChn);

    if (pChn->lgChnType == CHANNEL_TRUNK)
    {
      if (pChn->hwState != 0)
      {
        TrkChnBlockCount ++;
        IvrChnBlockCount ++;
      }
      if (pChn->lnState != 0)
      {
        TrkChnBusyCount ++;
        IvrChnBusyCount ++;
      }
      else
      {
        if (pChn->hwState == 0)
        {
          TrkChnIdelCount ++;
          IvrChnIdelCount ++;
        }
      }
      if (pChn->CallInOut == 1)
        TrkChnInCount ++;
      else if (pChn->CallInOut == 2)
        TrkChnOutCount ++;
    }
    else if (pChn->lgChnType == CHANNEL_SEAT)
    {
      pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        if (pAgent->isLogin() == true)
          AgLoginCount ++;
        else
          AgLogoutCount ++;
        if (pAgent->svState != 0)
        {
          AgBusyCount ++;
          if (pAgent->isLogin() == true && (pAgent->svState == AG_SV_CONN || pAgent->svState == AG_SV_HOLD))
          {
            AgTalkCount ++;
          }
        }
        else
        {
          if (pAgent->isLogin() == true)
            AgIdelCount ++;
        }
        if (pAgent->isLogin() == true && (pAgent->m_Worker.isDisturb() == true || pAgent->m_Worker.isLeave() == true))
          AgLeavelCount ++;
      }
    }
    else if (pChn->lgChnType == CHANNEL_IVR)
    {
      IvrChnTotalCount ++;
      if (pChn->hwState != 0)
        IvrChnBlockCount ++;
      if (pChn->lnState != 0)
        IvrChnBusyCount ++;
      else
      {
        if (pChn->hwState == 0)
          IvrChnIdelCount ++;
      }
    }
  }
  pAgentMng->AllCallCountParam.IvrChnTotalCount = IvrChnTotalCount;
  pAgentMng->AllCallCountParam.IvrChnIdelCount = IvrChnIdelCount;
  pAgentMng->AllCallCountParam.IvrChnBusyCount = IvrChnBusyCount;
  pAgentMng->AllCallCountParam.IvrChnBlockCount = IvrChnBlockCount;

  if (RECSystemLogId == false)
  {
    if (pBoxchn->TrkChnBusyCount != TrkChnBusyCount ||
      pBoxchn->TrkChnIdelCount != TrkChnIdelCount ||
      pBoxchn->TrkChnBlockCount != TrkChnBlockCount)
    {
      SendQueryTrunkGroupToAllLOG("TRK", TrkChnIdelCount, TrkChnBusyCount, TrkChnBlockCount);
    }
    pBoxchn->TrkChnBusyCount = TrkChnBusyCount;
    pBoxchn->TrkChnIdelCount = TrkChnIdelCount;
    pBoxchn->TrkChnBlockCount = TrkChnBlockCount;

    pBoxchn->TrkChnInCount = TrkChnInCount;
    pBoxchn->TrkChnOutCount = TrkChnOutCount;
    
    if (pBoxchn->IvrChnBusyCount != IvrChnBusyCount ||
      pBoxchn->IvrChnIdelCount != IvrChnIdelCount ||
      pBoxchn->IvrChnBlockCount != IvrChnBlockCount)
    {
      SendQueryTrunkGroupToAllLOG("IVR", IvrChnIdelCount, IvrChnBusyCount, IvrChnBlockCount);
      if (bcountseatstatus == false)
      {
        AgentStatusCount();
        bcountseatstatus = true;
      }
      SendSystemStatusMeteCount();
    }
    pBoxchn->IvrChnBusyCount = IvrChnBusyCount;
    pBoxchn->IvrChnIdelCount = IvrChnIdelCount;
    pBoxchn->IvrChnBlockCount = IvrChnBlockCount;
    
    pAgentMng->AgLoginCount = AgLoginCount;
    pAgentMng->AgLogoutCount = AgLogoutCount;
    pAgentMng->AgIdelCount = AgIdelCount;
    pAgentMng->AgBusyCount = AgBusyCount;
    pAgentMng->AgLeavelCount = AgLeavelCount;
    pAgentMng->AgTalkCount = AgTalkCount;
    return;
  }
  if (pAgentMng->AgLoginCount != AgLoginCount ||
      pAgentMng->AgLogoutCount != AgLogoutCount ||
      pAgentMng->AgIdelCount != AgIdelCount ||
      pAgentMng->AgBusyCount != AgBusyCount ||
      pAgentMng->AgLeavelCount != AgLeavelCount ||
      pAgentMng->AgTalkCount != AgTalkCount)
  {
    Send_AGState_To_REC(AgLoginCount, AgLogoutCount, AgIdelCount, AgBusyCount, AgTalkCount, AgLeavelCount);
  }

  if (pBoxchn->TrkChnBusyCount != TrkChnBusyCount ||
    pBoxchn->TrkChnIdelCount != TrkChnIdelCount ||
    pBoxchn->TrkChnBlockCount != TrkChnBlockCount)
  {
    SendQueryTrunkGroupToAllLOG("TRK", TrkChnIdelCount, TrkChnBusyCount, TrkChnBlockCount);
  }
  pBoxchn->TrkChnBusyCount = TrkChnBusyCount;
  pBoxchn->TrkChnIdelCount = TrkChnIdelCount;
  pBoxchn->TrkChnBlockCount = TrkChnBlockCount;

  pBoxchn->TrkChnInCount = TrkChnInCount;
  pBoxchn->TrkChnOutCount = TrkChnOutCount;
  
  if (pBoxchn->IvrChnBusyCount != IvrChnBusyCount ||
    pBoxchn->IvrChnIdelCount != IvrChnIdelCount ||
    pBoxchn->IvrChnBlockCount != IvrChnBlockCount)
  {
    SendQueryTrunkGroupToAllLOG("IVR", IvrChnIdelCount, IvrChnBusyCount, IvrChnBlockCount);
    Send_IVRState_To_REC(IvrChnIdelCount, IvrChnBusyCount, IvrChnBlockCount);
    if (bcountseatstatus == false)
    {
      AgentStatusCount();
      bcountseatstatus = true;
    }
    SendSystemStatusMeteCount();
  }
  pBoxchn->IvrChnBusyCount = IvrChnBusyCount;
  pBoxchn->IvrChnIdelCount = IvrChnIdelCount;
  pBoxchn->IvrChnBlockCount = IvrChnBlockCount;
  
  pAgentMng->AgLoginCount = AgLoginCount;
  pAgentMng->AgLogoutCount = AgLogoutCount;
  pAgentMng->AgIdelCount = AgIdelCount;
  pAgentMng->AgBusyCount = AgBusyCount;
  pAgentMng->AgLeavelCount = AgLeavelCount;
  pAgentMng->AgTalkCount = AgTalkCount;
}
void SumIVRChnOnlines()
{
  CIVRCallCountParam *pIVRCallCountParam;

  gIVRCallCountParamMng.InitAllOnlines();
  for (int nChn = 0; nChn < pBoxchn->MaxChnNum; nChn ++)
  {
    if (pChn->state == 0) continue;

    if (pChn->lgChnType == CHANNEL_IVR)
    {
      if (pChn->CallInOut == 1)
      {
        if (gIVRCallCountParamMng.GetIvrNoType == 1)
          pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInOnlines(pChn->CalledNo);
        else
          pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInOnlines(pChn->OrgCalledNo.C_Str());
      }
      else if (pChn->CallInOut == 2)
      {
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVROutOnlines(pChn->CallerNo);
      }
    }
  }
}
//處理通道呼叫事物
void ProcessChnCall(int nChn)
{
  CH Sam[MAX_TELECODE_LEN];
  int VxmlId, CheckId, nChn1;

  if (pIVRCfg->isOnlyRecSystem == true)
  {
    return;
  }
	
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE) 
  {
    //2013-12-19增加交換機版本先呼座席在呼外線可能等不到結果
    if (pChn->TranIVRId == 6 && pChn->CalloutTimer > pChn->TranWaitTime)
    {
      MyTrace(3, "ProcessChnCall nChn=%d TranIVRId=%d CalloutTimer=%d TranWaitTime=%d",
        nChn, pChn->TranIVRId, pChn->CalloutTimer, pChn->TranWaitTime);
      pChn->lnState = 0;
      pChn->TranIVRId = 0;
      DispChnStatus(nChn);
      CAgent *pAgent = GetAgentBynChn(nChn);
      if (pAgent)
      {
        pAgent->SetAgentsvState(AG_SV_IDEL);
        DispAgentStatus(pAgent->nAG);
      }
      SendCallseatResult1(nChn, OnACDFail, "CalloutTimer overflow");
    }
    else if (pChn->TranIVRId == 5 && pChn->CalloutTimer > pChn->TranWaitTime)
    {
      MyTrace(3, "ProcessChnCall nChn=%d TranIVRId=%d CalloutTimer=%d TranWaitTime=%d",
        nChn, pChn->TranIVRId, pChn->CalloutTimer, pChn->TranWaitTime);
      pChn->lnState = 0;
      pChn->TranIVRId = 0;
      pChn->CalloutTimer = 0;
      DispChnStatus(nChn);
      SendCalloutResult1(nChn, nChn, OnOutFail, "CalloutTimer overflow");
    }
    else if (pChn->TranIVRId == 3 && pChn->CalloutTimer > pChn->TranWaitTime)
    {
      MyTrace(3, "ProcessChnCall nChn=%d TranIVRId=%d CalloutTimer=%d TranWaitTime=%d",
        nChn, pChn->TranIVRId, pChn->CalloutTimer, pChn->TranWaitTime);

      Send_SWTMSG_stoptransfer(nChn, pChn->DeviceID, pChn->ConnID);
      SendTransferResult1(nChn, 0xFFFF, OnOutTimeout, ""); //2015-07-15改為無人應答
      pChn->TranIVRId = 0;
    }

    if (pIVRCfg->ControlCallModal == 1)
      return;
    if (pChn->lgChnType == CHANNEL_TRUNK)
    {
      return;
    }
    switch (pChn->ssState)
    {
		case CHN_SNG_DELAY: //由忙轉空閑延時態
			{
  			pChn->ssState = CHN_SNG_IDLE;
			}
			break; //end of case CHN_SNG_DELAY: //由忙轉空閑延時態
		case CHN_SNG_IDLE: //空閑
			{
        if (pChn->CallInId == CHN_LN_SEIZE) //呼入占用
        {
          pChn->timer = 0;
          pChn->DtmfPauseTimer = 0;
          //pChn->CallInOut = CALL_IN; //呼叫方向：1-呼入
          pChn->lnState = CHN_LN_SEIZE; //占用
          pChn->ssState = CHN_SNG_IN_ARRIVE;
          pChn->CallTime = time(0);
          nCallInCount ++;
        }
      }
			break; //end of case CHN_SNG_IDLE: //空閑
		case CHN_SNG_IN_ARRIVE: //呼入到達
			{
        //檢查接入號碼
        CheckId = pParser->CheckCalledACM(pChn->CalledNo, VxmlId);
        if (CheckId == 0 || (CheckId == 3 && pChn->DtmfPauseTimer >= 4)) //當檢查標志是3時，表示未收到需要的最短號碼，延時4秒
        {
          //自動發送ACM信號
          SendACM(nChn, 0);
				  //號碼轉換
          pIVRCfg->ChangeCode(pChn->CallerNo, pChn->CalledNo);
          //發送電話呼入事件
          pChn->GenerateCdrSerialNo();
          if (pChn->CallInOut == CALL_OUT)
          {
            SendCallinEvent(nChn, VxmlId, 1, pChn->CallData.C_Str());
          }
          else
          {
            SendCallinEvent(nChn, VxmlId, 0, pChn->CallData.C_Str());
          }

					if (pChn->ChStyle != CH_A_EXTN) //模擬外線呼入時，在未應答前不能放錄音,其他類型允許
					{
						pChn->CanPlayOrRecId = 1; //允許開始放錄音
					}
          if (pChn->CallInOut == CALL_OUT)
            pChn->ssState = CHN_SNG_OT_WAIT;
          else
            pChn->ssState = CHN_SNG_IN_WAIT;
          pChn->CalledPoint = strlen(pChn->CalledNo);
          DispChnStatus(nChn);
        }
			}
			break; //end of case CHN_SNG_IN_ARRIVE: //呼入到達
    case CHN_SNG_IN_WAIT: //呼入等待后續號碼
		case CHN_SNG_OT_WAIT: //呼出等待后續號碼
			{
//         if (strlen(pChn->CalledNo) > pChn->CalledPoint)
//         {
// 					memset(Sam, 0, MAX_TELECODE_LEN);
// 					strncpy(Sam, &pChn->CalledNo[pChn->CalledPoint], strlen(pChn->CalledNo) - pChn->CalledPoint);
//           SendSamEvent(nChn, Sam, "", "", "");
//           pChn->CalledPoint = strlen(pChn->CalledNo);
//         }
      }
      break;
    case CHN_SNG_IN_TALK: //呼入應答
    case CHN_SNG_OT_TALK: //呼出應答
      {
        pChn->CanPlayOrRecId = 1;
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
          Release_Chn(nChn, 0, 1);
          break;
        }
      }
      break; //end of case CHN_SNG_OT_TALK: //呼出應答
    }
    return;
  }

  //======================================板卡版本通道狀態處理==================================================
  switch (pChn->ssState)
  {
		case CHN_SNG_DELAY: //由忙轉空閑延時態
			{
				//if (pChn->timer > 0)
        //{
					pChn->ssState = CHN_SNG_IDLE;
				//}
			}
			break; //end of case CHN_SNG_DELAY: //由忙轉空閑延時態
		case CHN_SNG_IDLE: //空閑
			{
        if (pChn->CallInId == 1) //呼入振鈴
        {
          pChn->timer = 0;
          pChn->DtmfPauseTimer = 0;
          pChn->lnState = CHN_LN_SEIZE; //占用
          pChn->ssState = CHN_SNG_IN_ARRIVE;
          pChn->CallTime = time(0);
          nCallInCount ++;
        }
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
					Release_Chn(nChn, 0, 1);
					break;
        }
      }
			break; //end of case CHN_SNG_IDLE: //空閑
		case CHN_SNG_IN_ARRIVE: //呼入到達
			{
        if (pChn->timer > TIMERS * 15)
				{
					//呼入超時
					Release_Chn(nChn, 1, 1);
					break;
				}
        
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
					Release_Chn(nChn, 0, 1);
					break;
        }

        //檢查接入號碼
        CheckId = pParser->CheckCalledACM(pChn->CalledNo, VxmlId);
        //printf("CheckId=%d\n",CheckId);
        if (CheckId == 0 || (CheckId == 3 && pChn->DtmfPauseTimer >= 4)) //當檢查標志是3時，表示未收到需要的最短號碼，延時4秒
        {
          if (pIVRCfg->isAutoSendACM())
          {
            //自動發送ACM信號
            SendACM(nChn, 0);
				    //號碼轉換
            pIVRCfg->ChangeCode(pChn->CallerNo, pChn->CalledNo);
            //發送電話呼入事件
            pChn->GenerateCdrSerialNo();
            SendCallinEvent(nChn, VxmlId, 0, pChn->CallData.C_Str());
            if (pChn->lgChnType == 2)
            {
              DBInsertCallCDR(nChn, 1, 2);
              DBUpdateCallCDR_AnsWorkerNo(nChn, pChn->nAG);
            }
            else
            {
              DBInsertCallCDR(nChn, 2, 1);
            }

					  if (pChn->ChStyle != CH_A_EXTN) //模擬外線呼入時，在未應答前不能放錄音,其他類型允許
					  {
						  pChn->CanPlayOrRecId = 1; //允許開始放錄音
					  }
            pChn->ssState = CHN_SNG_IN_RING;
            pChn->CalledPoint = strlen(pChn->CalledNo);
            DispChnStatus(nChn);
          }
          else
          {
            //發送電話呼入事件
            pChn->GenerateCdrSerialNo();
            SendCallinEvent(nChn, VxmlId, 0, pChn->CallData.C_Str());
            if (pChn->lgChnType == 2)
            {
              DBInsertCallCDR(nChn, 1, 2);
              DBUpdateCallCDR_AnsWorkerNo(nChn, pChn->nAG);
            }
            else
            {
              DBInsertCallCDR(nChn, 2, 1);
            }

            pChn->timer = 0;
            pChn->DtmfPauseTimer = 0;
            pChn->ssState = CHN_SNG_IN_WAIT;
            pChn->CalledPoint = strlen(pChn->CalledNo);
            DispChnStatus(nChn);
          }
        }
        else if (CheckId == 2)
        {
          if (pChn->ChStyle == CH_E_SS1 || pChn->ChStyle == CH_E_TUP 
            || pChn->ChStyle == CH_E_ISDN_U || pChn->ChStyle == CH_E_ISDN_N 
            || pChn->ChStyle == CH_E_ISUP)
          {
            SendACM(nChn, 0);
            PlayTone(nChn, 904, 3);
            pChn->ssState = CHN_SNG_IN_TALK;
          }
          else if (pChn->ChStyle == CH_A_EXTN || pChn->ChStyle == CH_A_SEAT)
          {
            SendACK(nChn, 1);
            PlayTone(nChn, 904, 3);
            pChn->ssState = CHN_SNG_IN_TALK;
          }
        }
			}
			break; //end of case CHN_SNG_IN_ARRIVE: //呼入到達
		case CHN_SNG_IN_WAIT: //呼入等待后續號碼
			{
				if (pChn->timer > TIMERS * 8)
				{
					//振鈴超時
					Release_Chn(nChn, 1, 1);
					break;
				}
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
					Release_Chn(nChn, 0, 1);
					break;
        }
        if (strlen(pChn->CalledNo) > pChn->CalledPoint)
        {
					memset(Sam, 0, MAX_TELECODE_LEN);
					strncpy(Sam, &pChn->CalledNo[pChn->CalledPoint], strlen(pChn->CalledNo) - pChn->CalledPoint);
          SendSamEvent(nChn, Sam, "", "", "");
          //DBUpdateCallCDR_Phone(nChn);
          pChn->CalledPoint = strlen(pChn->CalledNo);
        }
      }
      break;
		case CHN_SNG_IN_RING: //呼入振鈴
			{
				if (pChn->timer > TIMERS * 90)
				{
					//振鈴超時
					Release_Chn(nChn, 1, 1);
					break;
				}
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
					Release_Chn(nChn, 0, 1);
					break;
        }
        if (pChn->ChStyle == CH_A_RECD && pChn->CallInOut == CALL_OUT && pChn->WaitDialTimer > 2)
        {
          pChn->CanPlayOrRecId = 1;
          pChn->AnsTime = time(0);
          pChn->ssState = CHN_SNG_OT_TALK;
          SendDialoutResult(nChn, OnOutAnswer);
          DBUpdateCallCDR_AnsTime(nChn);
          DispChnStatus(nChn);
          break;
        }
        if (pIVRCfg->isAutoSendANC())
        {
          //自動發送ANC信號
          SendACK(nChn, 1);
          pChn->CanPlayOrRecId = 1;
          pChn->AnsTime = time(0);
          pChn->ssState = CHN_SNG_IN_TALK;
          pChn->IVRStartTime = time(0);
        }
			}
			break; //end of case CHN_SNG_IN_RING: //呼入振鈴
		case CHN_SNG_IN_TALK: //呼入應答
			{
				pChn->CanPlayOrRecId = 1;
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
					Release_Chn(nChn, 0, 1);
					break;
        }
			}
			break; //end of case CHN_SNG_IN_TALK: //呼入應答
    //case CHN_SNG_OT_DTMF:
    //  break;
    case CHN_SNG_OT_RING: //呼出振鈴
      {
        if (pChn->lnState == CHN_LN_RELEASE)
        {
          Release_Chn(nChn, 0, 1);
          break;
        }
      }
      break; //end of case CHN_SNG_OT_RING: //呼出振鈴
		case CHN_SNG_OT_TALK: //呼出應答
			{
				pChn->CanPlayOrRecId = 1;
        //是否對方已釋放
        if (pChn->lnState == CHN_LN_RELEASE)
        {
					Release_Chn(nChn, 0, 1);
					break;
        }
        //釋放模擬中繼之間的超時通話，以免掛死
        if (pChn->ChStyle == CH_A_EXTN && pChn->LinkType[0] == 1)
        {
          nChn1 = pChn->LinkChn[0];
          if (pBoxchn->isnChnAvail(nChn1))
          {
            if (pChn1->ChStyle == CH_A_EXTN)
            {
              if ((time(0)-pChn->AnsTime) > pIVRCfg->MaxTranOutTalkTimeLen)
                Release_Chn(nChn, 1, 1);
            }
          }
        }
			}
			break; //end of case CHN_SNG_OT_TALK: //呼出應答
  }
}
//處理通道放音事務
void ProcessChnPlay(int nChn)
{
  int PlayRuleId;

  //放音狀態處理
	if (pChn->curPlayData.state == 1)
	{
		if (pChn->curPlayRuleId > MAX_PLAYRULE_BUF) //edit 2012-07-18
      return;
    PlayRuleId = pChn->curPlayRuleId;
    //檢查放音任務
		if (pChn->curPlayData.PlayState == 1 || pChn->curPlayData.PlayState == 2)
		{
			//檢查放音結果
			switch (pChn->curPlayState)
			{
			case 1: //放音正在進行
				{
					if (pChn->curPlayData.PlayState == 1)
					{
						SendChnPlayResult(nChn, OnPlaying, "");
            pChn->curPlayData.PlayState = 2;
						pChn->svState = CHN_SV_PLAYING;
						pChn->PlayedTimer = 0;
					}
					if (pChn->PlayRule[PlayRuleId].duration > 0)
					{
						if (pChn->PlayedTimer > (UL)TIMERS * pChn->PlayRule[PlayRuleId].duration)
						{
							//放音時長已到，停止放音
							StopPlay(nChn);
              pChn->curPlayState = 0;
							pChn->PlayedTimer = 0;
						
							//判斷放音次數是否完成
							pChn->PlayPauseTimer = 0;
							if (pChn->PlayRule[PlayRuleId].repeat == 0)
							{
								//無限次循環放音
								if (pChn->PlayRule[PlayRuleId].pause > 0)
								{
									//需要暫停放音
									pChn->curPlayData.PlayState = 3;
									pChn->PlayPauseTimer = 0;
								}
								else
								{
									//不需要暫停放音,啟動下一遍放音
									if (Start_ChnPlay(nChn) == 0)
                  {
										pChn->PlayedTimer = 0;
										pChn->curPlayData.PlayState = 2;
                  }
								}
							}
							else
							{
								//有限次循環放音
								//判斷是否達到放音次數
								pChn->curPlayData.PlayedTime ++; //放音次數加1
								if (pChn->curPlayData.PlayedTime < pChn->PlayRule[PlayRuleId].repeat)
								{
									//規定的放音次數還未完成,進入放音每次之間暫停
									if (pChn->PlayRule[PlayRuleId].pause > 0)
									{
										//需要暫停放音
										pChn->curPlayData.PlayState = 3;
										pChn->PlayPauseTimer = 0;
									}
									else
									{
										//不需要暫停放音,啟動下一遍放音
										if (Start_ChnPlay(nChn) == 0)
                    {
											pChn->PlayedTimer = 0;
											pChn->curPlayData.PlayState = 2;
                    }
									}
								}
								else
								{
									//規定的放音次數已完成
									//放音次數完成
									if (pChn->curPlayData.PlayEndReleaseId == 0)
									{
										SendChnPlayResult(nChn, OnPlayEnd, "");
                    StopClearPlayDtmfBuf(nChn);
									}
									else
									{
										Release_Chn(nChn, 1, 1);
									}
								}
							}
						}
					}
          //pChn->curPlayState = 0;
				}
				break; //end of case 0: //放音正在進行
			case 2: //放音正常結束
				{
          if (pChn->curPlayData.PlayTypeId == 4)
          {
            if (pChn->curPlayData.CurBufId == 0 || pChn->curPlayData.CurBufId == 1)
            {
              pChn->curPlayData.CurBufId = 2;
              if (pChn->curPlayData.TTSResult[0] == 1 || pChn->PlayRule[PlayRuleId].repeat == 0
                || (pChn->curPlayData.PlayedTime+1) < pChn->PlayRule[PlayRuleId].repeat)
                GetNextTTSStream(nChn);
              if (pChn->curPlayData.TTSResult[0] == 1)
              {
                Start_ChnPlay(nChn);
                break;
              }
            }
            else
            {
              pChn->curPlayData.CurBufId = 1;
              if (pChn->curPlayData.TTSResult[1] == 1 || pChn->PlayRule[PlayRuleId].repeat == 0
                || (pChn->curPlayData.PlayedTime+1) < pChn->PlayRule[PlayRuleId].repeat)
                GetNextTTSStream(nChn);
              if (pChn->curPlayData.TTSResult[1] == 1)
              {
                Start_ChnPlay(nChn);
                break;
              }
            }
          }
          pChn->PlayPauseTimer = 0;
					if (pChn->PlayRule[PlayRuleId].repeat == 0)
					{
						//循環放音
            if (pChn->PlayRule[PlayRuleId].pause > 0)
						{
							//需要暫停放音
							pChn->curPlayData.PlayState = 3;
							pChn->PlayPauseTimer = 0;
						}
						else
						{
							//不需要暫停放音,啟動下一遍放音
							if (Start_ChnPlay(nChn) == 0)
              {
								pChn->PlayedTimer = 0;
								pChn->curPlayData.PlayState = 2;
              }
						}
					}
					else
					{
						//非循環放音
            //判斷是否達到放音次數
						pChn->curPlayData.PlayedTime ++; //放音次數加1
						if (pChn->curPlayData.PlayedTime < pChn->PlayRule[PlayRuleId].repeat)
						{
							//規定的放音次數還未完成,進入放音每次之間暫停
							if (pChn->PlayRule[PlayRuleId].pause > 0)
							{
								//需要暫停放音
                pChn->curPlayData.PlayState = 3;
								pChn->PlayPauseTimer = 0;
							}
							else
							{
								//不需要暫停放音,啟動下一遍放音
								if (Start_ChnPlay(nChn) == 0)
                {
                  pChn->PlayedTimer = 0;
									pChn->curPlayData.PlayState = 2;
                }
							}
						}
						else
						{
							//規定的放音次數已完成
							if (pChn->PlayRule[PlayRuleId].pause > 0)
							{
								//需要暫停放音
								pChn->curPlayData.PlayState = 3;
								pChn->PlayPauseTimer = 0;
							}
							else
							{
								//放音次數完成后發送結束放音消息
								if (pChn->curPlayData.PlayEndReleaseId == 0)
								{
									SendChnPlayResult(nChn, OnPlayEnd, "");
                  pChn->curPlayData.PlayState = 0; //add edit by zgj 2006-09-30
                  StopClearPlayDtmfBuf(nChn);
								}
								else
								{
									Release_Chn(nChn, 1, 1);
								}
							}
						}
					}
				}
        pChn->curPlayState = 0;
				break; //end of case 1: //放音正常結束
			case 3: //放音操作因檢測到DTMF 按鍵字符而結束
			case 4: //放音操作因檢測到bargein而結束
			case 5: //放音操作因檢測到對端掛機而停止
			case 6: //放音操作被應用程序停止
			case 7: //放音操作被暫停
			case 8: //放音操作因下總線而停止（只針對數字卡）
			case 9: //放音操作因網絡中斷而停止
				{
					//清除放音緩沖區
					Stop_Play_Clear_Buf(nChn);
				}
				break;
      case 10: //外置語音機放音錯誤
        {
          SendChnPlayResult(nChn, OnPlayError, "");
          pChn->curPlayData.PlayState = 0; //add edit by zgj 2006-09-30
          StopClearPlayDtmfBuf(nChn);
        }
        break;
			default: //調用失敗
				{
				}
				break;
			} //end of switch (pChn->curPlayState)
		}
		else if (pChn->curPlayData.PlayState == 3) //當次放音結束
		{
			//當次放音結束
			if (pChn->PlayPauseTimer >= (UL)TIMERS * pChn->PlayRule[PlayRuleId].pause)
			{
				//到重新放音時間
				if (pChn->curPlayData.PlayedTime < pChn->PlayRule[PlayRuleId].repeat)
				{
					//放音次數還未完成,繼續放下一遍
					if (Start_ChnPlay(nChn) == 0)
          {
            pChn->curPlayData.PlayState = 2;
						pChn->PlayedTimer = 0;
          }
				}
				else
				{
					//放音次數完成
					if (pChn->curPlayData.PlayEndReleaseId == 0)
					{
						SendChnPlayResult(nChn, OnPlayEnd, "");
            StopClearPlayDtmfBuf(nChn);
					}
					else
					{
						Release_Chn(nChn, 1, 1);
					}
				}
			}
		} //end of else if (pChn->curPlayData.PlayState == 3)
	} //end of //放音狀態處理
}
//處理通道收碼事務
void ProcessChnDTMF(int nChn)
{
	CH cDtmf;
	int i, DtmfRuleId;
  
  //if (!pBoxchn->isnChnAvail(nChn)) return;
	//收碼處理
	if (pChn->curDtmfRuleId < MAX_DTMFRULE_BUF)
	{
		DtmfRuleId = pChn->curDtmfRuleId;
    if (strlen(pChn->curRecvDtmf) > 0)
		{
			//判斷DTMF按鍵
			for (i = 0; i < (int)strlen(pChn->curRecvDtmf); i ++)
			{
				cDtmf = pChn->curRecvDtmf[i];
				//是否為收一位發一位
				if (pChn->DtmfRule[DtmfRuleId].transtype == 0)
				{
					//是否為允許接收的DTMF按鍵
					if (pChn->DtmfRule[DtmfRuleId].digits.Find(cDtmf) >= 0)
					{
						if (pChn->RecvDtmfId != 1)
						{
							pChn->RecvDtmfId = 1;
							pChn->svState = CHN_SV_DTMFRECV;
              //是否為收1位按鍵的例外情況,如總機提示按鍵:請撥3位分機號,查號請撥0,技術部請撥1
              if (pChn->DtmfRule[DtmfRuleId].onebitdigits.Find(cDtmf) >= 0) 
              {
                if (pChn->DtmfRule[DtmfRuleId].firstdigitdelay == 0 || (pChn->DtmfRule[DtmfRuleId].pbxpredigits.GetLength() && pChn->DtmfRule[DtmfRuleId].pbxpredigits.Find(cDtmf) < 0))
                {
                  pChn->RecvDtmfBuf[0] = cDtmf;
                  pChn->RecvDtmfBuf[1] = '\0';
                  //發送放音收碼結果
                  SendChnPlayResult(nChn, OnRecvDTMF, "");
                  //清除放音緩沖區
                  if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
                    Stop_Play_Clear_Buf(nChn);
                  //清除收碼緩沖區
                  Stop_Dtmf_Clear_Buf(nChn);
                  memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
                  return;
                }
                else
                {
                  pChn->PbxFirstDigitId = 1;
                }
              }
						}
            else
            {
              pChn->PbxFirstDigitId = 0;
            }
						//收碼計時器清0
						pChn->DtmfPauseTimer = 0;
						//判斷是否已停止放音(在需要按鍵停止放音時)
						if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
						{
							if (pChn->curPlayData.state == 1)
							{
								Stop_Play_Clear_Buf(nChn);
							}
						}

						//符合收碼規則
						if (strlen(pChn->RecvDtmfBuf) < MAX_DTMFBUF_LEN)
						{
							pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf)] = cDtmf;
							pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf) + 1] = '\0';
						}
					}
					//是否達到允許收碼的最大長度
					if (strlen(pChn->RecvDtmfBuf) >= pChn->DtmfRule[DtmfRuleId].maxlength 
            && pChn->DtmfRule[DtmfRuleId].maxlength > 0)
					{
						//發送放音收碼結果
						SendChnPlayResult(nChn, OnRecvDTMF, "");
            //清除放音緩沖區
						if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
              Stop_Play_Clear_Buf(nChn);
						//清除收碼緩沖區
						Stop_Dtmf_Clear_Buf(nChn);
					}
					else
					{
						//是否為允許接收的DTMF結束按鍵
            if (pChn->DtmfRule[DtmfRuleId].termdigits.Find(cDtmf) >= 0)
						{
							//判斷是否已停止放音(在需要按鍵停止放音時)
							if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
							{
								if (pChn->curPlayData.state == 1)
                {
									Stop_Play_Clear_Buf(nChn);
								}
							}
							
              //判斷已收碼長度
              if (strlen(pChn->RecvDtmfBuf) == 0)
							{
                if (pChn->DtmfRule[DtmfRuleId].minlength == 0)
                {
                  //發送放音收碼結果
                  TransferOneDTMF(nChn, OnRecvDTMF, cDtmf);
                }
                else
                {
                  //發送放音收碼結果
                  TransferOneDTMF(nChn, OnErrDTMF, cDtmf);
                }
								//清除放音緩沖區
								if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
                  Stop_Play_Clear_Buf(nChn);
								//清除收碼緩沖區
								Stop_Dtmf_Clear_Buf(nChn);
							}
							else if (strlen(pChn->RecvDtmfBuf) >= pChn->DtmfRule[DtmfRuleId].minlength) //判斷是否已到最小收碼長度
							{
							  //收碼結果是否包含結束按鍵
							  if (pChn->DtmfRule[DtmfRuleId].termid == 1)
							  {
								  if (strlen(pChn->RecvDtmfBuf) < MAX_DTMFBUF_LEN)
								  {
									  pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf)] = cDtmf;
									  pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf) + 1] = '\0';
								  }
							  }
								//發送放音收碼結果
								SendChnPlayResult(nChn, OnRecvDTMF, "");
                //清除放音緩沖區
								if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
                  Stop_Play_Clear_Buf(nChn);
								//清除收碼緩沖區
								Stop_Dtmf_Clear_Buf(nChn);
							}
							else
							{
							  //收碼結果是否包含結束按鍵
							  if (pChn->DtmfRule[DtmfRuleId].termid == 1)
							  {
								  if (strlen(pChn->RecvDtmfBuf) < MAX_DTMFBUF_LEN)
								  {
									  pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf)] = cDtmf;
									  pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf) + 1] = '\0';
								  }
							  }
								//發送放音收碼結果
								SendChnPlayResult(nChn, OnErrDTMF, "");
                //清除放音緩沖區
								if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
                  Stop_Play_Clear_Buf(nChn);
								//清除收碼緩沖區
								Stop_Dtmf_Clear_Buf(nChn);
							}
						}
            else
            {
              //判斷是否達到特殊字冠的收碼最大長度
              for (int a=0; a<10; a++)
              {
                if (pChn->DtmfRule[DtmfRuleId].specpredigitsmaxlength[a] > 0)
                {
                  if (strlen(pChn->RecvDtmfBuf) >= pChn->DtmfRule[DtmfRuleId].specpredigitsmaxlength[a]
                    && strncmp(pChn->RecvDtmfBuf, pChn->DtmfRule[DtmfRuleId].specpredigits[a].C_Str(), pChn->DtmfRule[DtmfRuleId].specpredigits[a].GetLength()) == 0)
                  {
                    //發送放音收碼結果
                    SendChnPlayResult(nChn, OnRecvDTMF, "");
                    //清除放音緩沖區
                    if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
                      Stop_Play_Clear_Buf(nChn);
                    //清除收碼緩沖區
                    Stop_Dtmf_Clear_Buf(nChn);
                    break;
                  }
                }
              }
            }
					}
				}
				else
				{
					//收一位發一位
					if (pChn->DtmfRule[DtmfRuleId].digits.Find(cDtmf) >= 0)
					{
						if (pChn->RecvDtmfId != 1)
						{
							pChn->RecvDtmfId = 1;
							pChn->svState = CHN_SV_DTMFRECV;
						}
						//收碼計時器清0
						pChn->DtmfPauseTimer = 0;
						//判斷是否已停止放音(在需要按鍵停止放音時)
						if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
						{
							if (pChn->curPlayData.state == 1)
							{
								Stop_Play_Clear_Buf(nChn);
							}
						}

						//符合收碼規則
						if (strlen(pChn->RecvDtmfBuf) < MAX_DTMFBUF_LEN)
						{
							pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf)] = cDtmf;
							pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf) + 1] = '\0';
						}
            //TransferOneDTMF(nChn, OnRecvDTMF, cDtmf);
            SendChnPlayResult(nChn, OnRecvDTMF, "");
					}
					//是否達到允許收碼的最大長度
					if (strlen(pChn->RecvDtmfBuf) >= pChn->DtmfRule[DtmfRuleId].maxlength)
					{
						//發送放音收碼結果
						SendChnPlayResult(nChn, OnPlayEnd, "");
            //清除放音緩沖區
						if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
              Stop_Play_Clear_Buf(nChn);
						//清除收碼緩沖區
						Stop_Dtmf_Clear_Buf(nChn);
					}
					else
					{
						//是否為允許接收的DTMF結束按鍵
						if (pChn->DtmfRule[DtmfRuleId].termdigits.Find(cDtmf) >= 0)
						{
							//判斷是否已停止放音(在需要按鍵停止放音時)
							if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
							{
								if (pChn->curPlayData.state == 1)
								{
									Stop_Play_Clear_Buf(nChn);
								}
							}
							
							//收碼結果是否包含結束按鍵
							if (pChn->DtmfRule[DtmfRuleId].termid == 1)
							{
								if (strlen(pChn->RecvDtmfBuf) < MAX_DTMFBUF_LEN)
								{
									pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf)] = cDtmf;
									pChn->RecvDtmfBuf[strlen(pChn->RecvDtmfBuf) + 1] = '\0';
								}
							}
              //發送放音收碼結果
              //TransferOneDTMF(nChn, OnPlayEnd, cDtmf);
              SendChnPlayResult(nChn, OnPlayEnd, "");
              //清除放音緩沖區
							if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
                Stop_Play_Clear_Buf(nChn);
							//清除收碼緩沖區
							Stop_Dtmf_Clear_Buf(nChn);
						}
					}
				}
			}
			memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
		}
		else
		{
			if (pChn->RecvDtmfId == 1 
        && (pChn->DtmfPauseTimer > (UL)TIMERS * pChn->DtmfRule[DtmfRuleId].timeout) 
        || (pChn->PbxFirstDigitId == 1 && pChn->DtmfPauseTimer > (UL)TIMERS * pChn->DtmfRule[DtmfRuleId].firstdigitdelay))
			{
				//超出收碼間隔等待時長
				//是否為收一位發一位
				if (pChn->DtmfRule[DtmfRuleId].transtype == 0)
				{
				  if (strlen(pChn->RecvDtmfBuf) >= pChn->DtmfRule[DtmfRuleId].minlength)
          {
            //發送放音收碼結果
            SendChnPlayResult(nChn, OnRecvDTMF, "");
          }
          else
          {
            //發送放音收碼結果
            if (pChn->PbxFirstDigitId == 1)
              SendChnPlayResult(nChn, OnRecvDTMF, "");
            else
              SendChnPlayResult(nChn, OnErrDTMF, "");
          }
				  //清除放音緩沖區
				  if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
            Stop_Play_Clear_Buf(nChn);
				  //清除收碼緩沖區
				  Stop_Dtmf_Clear_Buf(nChn);
        }
        else
        {
				  //發送放音收碼結果
				  SendChnPlayResult(nChn, OnPlayEnd, "");
          //清除放音緩沖區
				  if (pChn->DtmfRule[DtmfRuleId].breakid == 1)
            Stop_Play_Clear_Buf(nChn);
				  //清除收碼緩沖區
				  Stop_Dtmf_Clear_Buf(nChn);
        }
			}
		}
	} //end of //收碼處理
}
//處理通道錄音事務
void ProcessChnRec(int nChn)
{
  CH filename[MAX_PATH_FILE_LEN];
	long RecTime;//, handle;
	long RecBytes;
	CH cDtmf;
	int i;

  //if (!pBoxchn->isnChnAvail(nChn)) return;
	//錄音狀態處理
	if (pChn->curRecData.state == 1)
	{
		if (pChn->curRecData.RecState == 1)
		{
			//正在放錄音前嘀聲
			switch (pChn->curPlayState)
			{
			case 1: //放音正在進行
        pChn->curPlayState = 0;
				break;
			case 2: //放音正常結束
			case 3: //放音操作因檢測到DTMF 按鍵字符而結束
			case 4: //放音操作因檢測到bargein而結束
			case 5: //放音操作因檢測到對端掛機而停止
			case 6: //放音操作被應用程序停止
			case 7: //放音操作被暫停
			case 8: //放音操作因下總線而停止（只針對數字卡）
			case 9: //放音操作因網絡中斷而停止
				{
					pChn->curRecData.RecState = 2;
          memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
				}
        pChn->curPlayState = 0;
				break;
      case 10: //外置語音機錄音錯誤
        {
          pChn->curRecData.RecState = 2;
          memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
        }
        pChn->curPlayState = 0;
        break;
			default: //調用失敗
				{
				}
				break;
			}
		}
		else if (pChn->curRecData.RecState == 2 || pChn->curRecData.RecState == 3)
		{
			//檢查錄音結果
			switch (pChn->curRecState)
			{
			case 1: //錄音正在進行
				{
					if (pChn->curRecData.RecState != 3)
					{
            pChn->curRecData.RecState = 3;
						pChn->svState = CHN_SV_RECORDING;
						pChn->RecTimer = 0;
					}
          pChn->curRecState = 0;
				}
				break;
			case 3: //錄音操作因檢測到DTMF 按鍵字符而結束
			case 5: //錄音操作自然結束（到達指定的字節長度或時間）
				{
					RecTime = pChn->RecTimer;
					if (RecTime > 0)
					{
						RecTime = RecTime / TIMERS;
					}
					else
					{
						RecTime = 0;
					}
          pChn->curRecData.RecordFile.CopyOutWithTerm(filename);
          RecBytes = MyCheckFileSize(filename);
					SendChnRecResult(nChn, OnRecordEnd, RecTime, RecBytes);
          StopClearRecDtmfBuf(nChn);
				}
        pChn->curRecState = 0;
				break;
			case 2: //錄音操作被應用程序停止
			case 4: //錄音操作因檢測到對端掛機而結束
			case 7: //錄音操作因寫文件失敗而停止
			case 8: //放音操作因網絡中斷而停止
			case 10: //放音分配資源失敗
				{
					SendChnRecResult(nChn, OnRecordError, 0, 0);
          StopClearRecDtmfBuf(nChn);
				}
				break;
			}

		  //檢查錄音結束按鍵
      if (strlen(pChn->curRecvDtmf) > 0)
		  {
			  //判斷DTMF按鍵
			  for (i = 0; i < (int)strlen(pChn->curRecvDtmf); i ++)
			  {
				  cDtmf = pChn->curRecvDtmf[i];
          if (pChn->curRecData.TermChar.Find(cDtmf) >= 0)
          {
            //發送按鍵停止錄音
					  RecTime = pChn->RecTimer;
					  if (RecTime > 0)
					  {
						  RecTime = RecTime / TIMERS;
					  }
					  else
					  {
						  RecTime = 0;
					  }
            pChn->curRecData.RecordFile.CopyOutWithTerm(filename);
            RecBytes = MyCheckFileSize(filename);
					  SendChnRecResult(nChn, OnRecordEnd, RecTime, RecBytes);
            StopClearRecDtmfBuf(nChn);
          }
        }
        memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
      }
		}
	}
}
//-----------------------------------------------------------------------------
//會議事務處理
void ProcessConf()
{
  ProcessConfPlay();
  ProcessConfRec();
}
void SendConfPlayResult(int nCfc, int result)
{
	sprintf(SendMsgBuf, "<%s sessionid='%lu' cmdaddr='%lu' chantype='%d' channo='%d' result='%d' cfcno='%d' errorbuf=''/>", 
    pCfc->curPlayData.MsgName.C_Str(), pCfc->curPlayData.SessionNo, pCfc->curPlayData.CmdAddr, pCfc->curPlayData.lgChnType, pCfc->curPlayData.lgChnNo, result, nCfc);
	SendMsg2XML(pCfc->curPlayData.OnMsgId, pCfc->curPlayData.SessionNo, SendMsgBuf);
}
//處理會議放音事務
void ProcessConfPlay()
{
  US nCfc;

	for (nCfc = 0; nCfc < pBoxconf->MaxCfcNum; nCfc ++)
  {
	  if (pCfc->curPlayData.state == 0) continue;
    //文件放音
	  if (pCfc->curPlayData.PlayState == 1 || pCfc->curPlayData.PlayState == 2)
	  {
		  //檢查放音結果
		  switch (pCfc->curPlayState)
		  {
		  case 1: //放音正在進行
			  {
				  if (pCfc->curPlayData.PlayState == 1)
				  {
					  SendConfPlayResult(nCfc, OnSuccess);
            pCfc->curPlayData.PlayState = 2;
					  pCfc->PlayedTimer = 0;
				  }
				  if (pCfc->curPlayRule.duration > 0)
				  {
					  if (pCfc->PlayedTimer > (UL)TIMERS * pCfc->curPlayRule.duration)
					  {
						  //放音時長已到，停止放音
						  StopPlay(nCfc);
              pCfc->curPlayState = 0;
						  pCfc->PlayedTimer = 0;
					  
						  //判斷放音次數是否完成
						  pCfc->PlayPauseTimer = 0;
						  if (pCfc->curPlayRule.repeat == 0)
						  {
							  //無限次循環放音
							  if (pCfc->curPlayRule.pause > 0)
							  {
								  //需要暫停放音
								  pCfc->curPlayData.PlayState = 3;
								  pCfc->PlayPauseTimer = 0;
							  }
							  else
							  {
								  //不需要暫停放音,啟動下一遍放音
								  Start_ConfPlay(nCfc);
								  pCfc->PlayedTimer = 0;
								  pCfc->curPlayData.PlayState = 2;
							  }
						  }
						  else
						  {
							  //有限次循環放音
							  //判斷是否達到放音次數
							  pCfc->curPlayData.PlayedTime ++; //放音次數加1
							  if (pCfc->curPlayData.PlayedTime < pCfc->curPlayRule.repeat)
							  {
								  //規定的放音次數還未完成,進入放音每次之間暫停
								  if (pCfc->curPlayRule.pause > 0)
								  {
									  //需要暫停放音
									  pCfc->curPlayData.PlayState = 3;
									  pCfc->PlayPauseTimer = 0;
								  }
								  else
								  {
									  //不需要暫停放音,啟動下一遍放音
									  Start_ConfPlay(nCfc);
									  pCfc->PlayedTimer = 0;
									  pCfc->curPlayData.PlayState = 2;
								  }
							  }
							  else
							  {
								  //規定的放音次數已完成
								  //放音次數完成
									SendConfPlayResult(nCfc, OnSuccess);
                  //清除放音緩沖區
									Stop_Play_Clear_ConfBuf(nCfc);
							  }
						  }
					  }
				  }
          //pChn->curPlayState = 0;
			  }
			  break; //end of case 0: //放音正在進行
		  case 2: //放音正常結束
			  {
				  pCfc->PlayPauseTimer = 0;
				  if (pCfc->curPlayRule.repeat == 0)
				  {
					  //循環放音
					  if (pCfc->curPlayRule.pause > 0)
					  {
						  //需要暫停放音
						  pCfc->curPlayData.PlayState = 3;
						  pCfc->PlayPauseTimer = 0;
					  }
					  else
					  {
						  //不需要暫停放音,啟動下一遍放音
						  Start_ConfPlay(nCfc);
						  pCfc->PlayedTimer = 0;
						  pCfc->curPlayData.PlayState = 2;
					  }
				  }
				  else
				  {
					  //非循環放音
					  //判斷是否達到放音次數
					  pCfc->curPlayData.PlayedTime ++; //放音次數加1
					  if (pCfc->curPlayData.PlayedTime < pCfc->curPlayRule.repeat)
					  {
						  //規定的放音次數還未完成,進入放音每次之間暫停
						  if (pCfc->curPlayRule.pause > 0)
						  {
							  //需要暫停放音
							  pCfc->curPlayData.PlayState = 3;
							  pCfc->PlayPauseTimer = 0;
						  }
						  else
						  {
							  //不需要暫停放音,啟動下一遍放音
							  Start_ConfPlay(nCfc);
							  pCfc->PlayedTimer = 0;
							  pCfc->curPlayData.PlayState = 2;
						  }
					  }
					  else
					  {
						  //規定的放音次數已完成
						  if (pCfc->curPlayRule.pause > 0)
						  {
							  //需要暫停放音
							  pCfc->curPlayData.PlayState = 3;
							  pCfc->PlayPauseTimer = 0;
						  }
						  else
						  {
							  //放音次數完成后發送結束放音消息
								SendConfPlayResult(nCfc, OnSuccess);
                //清除放音緩沖區
								Stop_Play_Clear_ConfBuf(nCfc);
						  }
					  }
				  }
			  }
        pCfc->curPlayState = 0;
			  break; //end of case 1: //放音正常結束
		  case 3: //放音操作因檢測到DTMF 按鍵字符而結束
		  case 4: //放音操作因檢測到bargein而結束
		  case 5: //放音操作因檢測到對端掛機而停止
		  case 6: //放音操作被應用程序停止
		  case 7: //放音操作被暫停
		  case 8: //放音操作因下總線而停止（只針對數字卡）
		  case 9: //放音操作因網絡中斷而停止
			  {
				  //清除放音緩沖區
				  Stop_Play_Clear_ConfBuf(nCfc);
			  }
			  break;
		  default: //調用失敗
			  {
			  }
			  break;
		  } //end of switch (pChn->curPlayState)
	  }
	  else if (pCfc->curPlayData.PlayState == 3) //當次放音結束
	  {
		  //當次放音結束
		  if (pCfc->PlayPauseTimer >= (UL)TIMERS * pCfc->curPlayRule.pause)
		  {
			  //到重新放音時間
			  if (pCfc->curPlayData.PlayedTime < pCfc->curPlayRule.repeat)
			  {
				  //放音次數還未完成,繼續放下一遍
				  Start_ConfPlay(nCfc);
				  pCfc->curPlayData.PlayState = 2;
				  pCfc->PlayedTimer = 0;
			  }
			  else
			  {
				  //放音次數完成
					SendConfPlayResult(nCfc, OnSuccess);
          //清除放音緩沖區
					Stop_Play_Clear_ConfBuf(nCfc);
			  }
		  }
    }
  }
}

//處理會議錄音事務
void ProcessConfRec()
{
  CH filename[MAX_PATH_FILE_LEN];
	long RecTime;//, handle;
	long RecBytes;
  US nCfc;

	for (nCfc = 0; nCfc < pBoxconf->MaxCfcNum; nCfc ++)
  {
	  if (pCfc->curRecData.state == 0) continue;

		if (pCfc->curRecData.RecState == 1)
		{
			//正在放錄音前嘀聲
			switch (pCfc->curPlayState)
			{
			case 1: //放音正在進行
        pCfc->curPlayState = 0;
				break;
			case 2: //放音正常結束
			case 3: //放音操作因檢測到DTMF 按鍵字符而結束
			case 4: //放音操作因檢測到bargein而結束
			case 5: //放音操作因檢測到對端掛機而停止
			case 6: //放音操作被應用程序停止
			case 7: //放音操作被暫停
			case 8: //放音操作因下總線而停止（只針對數字卡）
			case 9: //放音操作因網絡中斷而停止
				{
					pCfc->curRecData.RecState = 2;
				}
        pCfc->curPlayState = 0;
				break;
			default: //調用失敗
				{
				}
				break;
			}
		}
		else if (pCfc->curRecData.RecState == 2 || pCfc->curRecData.RecState == 3)
		{
			//檢查錄音結果
			switch (pCfc->curRecState)
			{
			case 1: //錄音正在進行
				{
					if (pCfc->curRecData.RecState != 3)
					{
						pCfc->curRecData.RecState = 3;
						pCfc->RecTimer = 0;
					}
          pCfc->curRecState = 0;
				}
				break;
			case 2: //錄音操作被應用程序停止
				{
					Stop_Rec_Clear_ConfBuf(nCfc);
				}
				break;
			case 3: //錄音操作因檢測到DTMF 按鍵字符而結束
				{
					RecTime = pCfc->RecTimer;
					if (RecTime > 0)
					{
						RecTime = RecTime / TIMERS;
					}
					else
					{
						RecTime = 0;
					}
          pCfc->curRecData.RecordFile.CopyOutWithTerm(filename);
          RecBytes = MyCheckFileSize(filename);
          SendConfRecResult(nCfc, OnRecordEnd, RecTime, RecBytes);
					Stop_Rec_Clear_ConfBuf(nCfc);
				}
        pCfc->curRecState = 0;
				break;
			case 4: //錄音操作因檢測到對端掛機而結束
				{
					Stop_Rec_Clear_ConfBuf(nCfc);
				}
				break;
			case 5: //錄音操作自然結束（到達指定的字節長度或時間）
				{
					RecTime = pCfc->RecTimer;
					if (RecTime > 0)
					{
						RecTime = RecTime / TIMERS;
					}
					else
					{
						RecTime = 0;
					}
          pCfc->curRecData.RecordFile.CopyOutWithTerm(filename);
          RecBytes = MyCheckFileSize(filename);
					SendConfRecResult(nCfc, OnRecordEnd, RecTime, RecBytes);
          Stop_Rec_Clear_ConfBuf(nCfc);
				}
        pCfc->curRecState = 0;
				break;
			case 7: //錄音操作因寫文件失敗而停止
			case 8: //放音操作因網絡中斷而停止
			case 10: //放音分配資源失敗
				{
					SendConfRecResult(nCfc, OnRecordError, 0, 0);
          Stop_Rec_Clear_ConfBuf(nCfc);
				}
				break;
			}
		}
	}
}
//解析器退出時，清除相關通道狀態
void ResetAllChnForParserLogout(UC NodeId)
{
  //2016-02-12 改為不主動掛斷電話
  /*for (int nChn = 0; nChn < pBoxchn->MaxChnNum; nChn ++)
  {
    if (pChn->state == 0) continue;
    if (pChn->ssState != CHN_SNG_IDLE)
    {
      Release_Chn(nChn, 0, 0);
    }
  }*/
  for (int nOut = 0; nOut < pCallbuf->MaxOutNum; nOut ++)
  {
    if (pOut->state == 0) continue;
    if (pOut->Session.VxmlId == NodeId)
    {
      pOut->CancelId = 2;
    }
  }
}
void ResetAllChnForRECLogout()
{
  //2016-02-12 改為不主動掛斷電話
  /*
  for (int nChn = 0; nChn < pBoxchn->MaxChnNum; nChn ++)
  {
    if (pChn->state == 0) continue;
    if (pChn->ssState != CHN_SNG_IDLE)
    {
      Release_Chn(nChn, 0, 0); 
    }
  }*/
}
//-----------------------------------------------------------------------------
//產生呼出序列號
UL GetCallOutSerialNo(int nChn)
{
  static UL serialno=0;
  return ((serialno++)%65535) | ((nChn<<16)&0xFFFF0000);
}
//處理呼出隊列
void ProcCalloutQue()
{
  int nChn, nChn1=0xFFFF, nOut, nAcd, nAG, lgchntype, lgchnno, chState, routeno;
  CH Sam[MAX_TELECODE_LEN];
  CVopChn *pVopChn=NULL;

  for (int nQue = 0; nQue < pCalloutQue->MaxQueNum; nQue ++)
  {
    if (pQue->state == 0) continue;

    nOut = pQue->nOut;
    nAcd = pQue->nAcd;
    nChn = pQue->OutnChn;
    nAG = pQue->nAG;
    switch (pQue->CalloutResult)
    {
    case CALLOUT_NOCALL: //0-還未呼出
      //判斷被叫方是否為純電腦坐席
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (pAG->GetClientId() != 0x06FF)
        {
          if (pACDQueue->isnAcdAvail(nAcd)) //2016-11-11這里應該要判斷下
          {
            if (pAcd->Session.MediaType > 0)
            {
              sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='%d' param='%s'/>", 
                pAG->GetClientId()<<16, pAcd->Session.SessionNo, 0, 0, pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(),
                pAcd->CallSeatType, pAcd->Session.FuncNo, pAcd->Session.Param.C_Str());
              SendMsg2AG(pAG->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);

              pQue->CalledNoPoint = strlen(pQue->CalledNo);
              pQue->CalloutResult = CALLOUT_CALLING;
              break; //純電腦坐席不執行電話呼叫
            }
          }

          if (pQue->OrgCallType == 1 && pCallbuf->isnOutAvail(nOut))
          {
            pBoxchn->GetLgChBynChn(pOut->Session.nChn, lgchntype, lgchnno);
            sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='%d' param='%s'/>", 
              pAG->GetClientId()<<16, pOut->Session.SessionNo, lgchntype, lgchnno, pOut->Session.CallerNo.C_Str(), pOut->Session.CalledNo.C_Str(),
              pOut->CallSeatType, pOut->Session.FuncNo, pOut->Session.Param.C_Str());
          }
          else if (pACDQueue->isnAcdAvail(nAcd))
          {
            pBoxchn->GetLgChBynChn(pAcd->Session.nChn, lgchntype, lgchnno);
            sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='%d' param='%s'/>", 
              pAG->GetClientId()<<16, pAcd->Session.SessionNo, lgchntype, lgchnno, pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(),
              pAcd->CallSeatType, pAcd->Session.FuncNo, pAcd->Session.Param.C_Str());
          }
	        
          if (pAG->m_Seat.ACDByVocTrkId == 0) //2015-11-25 針對語音中繼的修改,如果是1就表示不從這里彈屏，而是從Proc_SWTMSG_oncallevent消息彈屏
            SendMsg2AG(pAG->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);

        }
        MyTrace(3, "ProcCalloutQue nAG=%d WebSocketLogId=%s WSClientId=%s", nAG, WebSocketLogId==true?"true":"false", pAG->m_Seat.WSClientId);
        if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
        {
          if (pQue->OrgCallType == 2)
          {
            sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s;param=%s",
              pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(), pAcd->Session.Param.C_Str());
            SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
          }
          else if (pQue->OrgCallType == 1)
          {
            sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s;param=%s",
              pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), pOut->Session.CallerNo.C_Str(), pOut->Session.CalledNo.C_Str(), pOut->Session.Param.C_Str());
            SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
          }
        }

        if (pBoxchn->isnChnAvail(nChn))
        if ((pAG->GetSeatType() == SEATTYPE_AG_PC && pAG->isTcpLinked() && (pChn->ssState == CHN_SNG_IN_TALK || pChn->ssState == CHN_SNG_OT_TALK)) 
          || (pAG->GetSeatType() == SEATTYPE_RT_TEL && pChn->ssState == CHN_SNG_OT_TALK))
        {
          //如果是純電腦坐席或遠程坐席已經綁定了通道,就不要呼叫坐席通道了
          pQue->CalledNoPoint = strlen(pQue->CalledNo);
          pQue->CalloutResult = CALLOUT_CALLING;
          chState = GetAgentChState(pAG);
          if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
            PlayTone(nChn, 920, 0); //放分配來話提示音
          break; //純電腦坐席不執行電話呼叫
        }
      }
      //取呼出路由 2013-09-30
      if (pQue->OrgCallType == 1)
        routeno = pOut->Calleds[pQue->CallPoint].RouteNo;
      else
        routeno = 0;
      //執行電話呼叫
      if (Callout(pQue->OutnChn, pQue->OutRouteNo,
                  ((pQue->CallType << 8) & 0xFF00) | pQue->CallerType,
                  pQue->CallerNo.C_Str(), 
                  pQue->CalledNo, 
                  pQue->OrgCalledNo.C_Str(), routeno) != 0)
      {
        //呼出失敗
        pQue->CalloutResult = CALLOUT_NOTRUNK;
        if (pAgentMng->isnAGAvail(nAG))
        {
          pAG->nQue = 0xFFFF;
          if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
          {
            pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
            pAG->m_Seat.nChn = 0xFFFF;
          }
          SetAgentsvState(nAG, AG_SV_IDEL);
        }
      }
      else
      {
        pQue->CalledNoPoint = strlen(pQue->CalledNo);
        if (pQue->CalloutResult == CALLOUT_NOCALL)
        {
          pQue->CalloutResult = CALLOUT_CALLING;
        }
      }
      break;
    case CALLOUT_CALLING:	//1-正在呼出(可發送后續地址)
      if (pQue->timer > (UL)TIMERS*pQue->RingTime)
      {
        //超時呼出失敗
        CancelCallout(pQue->OutnChn);
        pQue->CalloutResult = CALLOUT_NOBODY;
        pQue->state = 0;
        if (pQue->OrgCallType == 1)
        {
          pOut->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_NOBODY;
          pOut->Calleds[pQue->CallPoint].pQue_str = NULL;
        }
        else
        {
          pAcd->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_NOBODY;
          pAcd->Calleds[pQue->CallPoint].pQue_str = NULL;
        }
        if (pAgentMng->isnAGAvail(nAG))
        {
          pAG->nQue = 0xFFFF;
          if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
          {
            pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
            pAG->m_Seat.nChn = 0xFFFF;
          }
          SendStopACDCallIn(nAG);
          SetAgentsvState(nAG, AG_SV_IDEL);
        }
        break;
      }
			//是否有后續號碼發送
			if (strlen(pQue->CalledNo) > pQue->CalledNoPoint)
			{
        memset(Sam, 0, MAX_TELECODE_LEN);
				strncpy(Sam, &pQue->CalledNo[pQue->CalledNoPoint], 
          strlen(pQue->CalledNo) - pQue->CalledNoPoint);
				//發送后續地址
        if (Callinfo(pQue->OutnChn, Sam) == 0)
				{
					pQue->CalledNoPoint = strlen(pQue->CalledNo);
				}
			}
      //取消呼出標志
      if (pQue->CancelId == 1) 
      {
        CancelCallout(pQue->OutnChn);
        pQue->state = 0;
        if (pAgentMng->isnAGAvail(nAG))
        {
          pAG->nQue = 0xFFFF;
          if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
          {
            pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
            pAG->m_Seat.nChn = 0xFFFF;
          }
          SendStopACDCallIn(nAG);
          SetAgentsvState(nAG, AG_SV_IDEL);
        }
      }
      break;
    case CALLOUT_RING: //2-正在振鈴
      if (pQue->timer > (UL)TIMERS*pQue->RingTime)
      {
        //超時呼出失敗
        CancelCallout(pQue->OutnChn);
        pQue->CalloutResult = CALLOUT_NOBODY;
        pQue->state = 0;

        if (pQue->OrgCallType == 1)
        {
          pOut->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_NOBODY;
          pOut->Calleds[pQue->CallPoint].pQue_str = NULL;
        }
        else
        {
          pAcd->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_NOBODY;
          pAcd->Calleds[pQue->CallPoint].pQue_str = NULL;
        }
        if (pAgentMng->isnAGAvail(nAG))
        {
          pAG->nQue = 0xFFFF;
          if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
          {
            pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
            pAG->m_Seat.nChn = 0xFFFF;
          }
          SendStopACDCallIn(nAG);
          SetAgentsvState(nAG, AG_SV_IDEL);
        }
        break;
      }
      //取消呼出標志
      if (pQue->CancelId == 1) 
      {
        CancelCallout(pQue->OutnChn);
        if (pAgentMng->isnAGAvail(nAG))
        {
          pAG->nQue = 0xFFFF;
          if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
          {
            pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
            pAG->m_Seat.nChn = 0xFFFF;
          }
          SendStopACDCallIn(nAG);
          SetAgentsvState(nAG, AG_SV_IDEL);
        }
        pQue->state = 0;
      }
      break;
    case CALLOUT_CONN: //3-用戶應答
      if (pQue->OrgCallType == 1)
      {
        pOut->Calleds[pQue->CallPoint].CalloutResult = pQue->CalloutResult;
        pOut->Calleds[pQue->CallPoint].pQue_str = NULL;
      }
      else
      {
        pAcd->Calleds[pQue->CallPoint].CalloutResult = pQue->CalloutResult;
        pAcd->Calleds[pQue->CallPoint].pQue_str = NULL;
      }
      if (pAgentMng->isnAGAvail(nAG))
      {
        pAG->nQue = 0xFFFF;
      }
      pQue->state = 0;
      break;
    case CALLOUT_SLB: //4-用戶市忙
    case CALLOUT_STB:	//5-用戶長忙
    case CALLOUT_NOBODY: //6-無人應答
    case CALLOUT_NOTRUNK:	//7-無空閑出中繼
    case CALLOUT_UNN:	//8-空號
    case CALLOUT_FAIL: //9-失敗
    case CALLOUT_CNG:	//10-設備擁塞
    case CALLOUT_CGC:	//11-電路群擁塞
    case CALLOUT_ADI:	//12-地址不全
    case CALLOUT_SST:	//13-專用信號音
    case CALLOUT_OFF:	//14-用戶關機
    case CALLOUT_BAR:	//15-接入拒絕
      if (pQue->OrgCallType == 1)
      {
        pOut->Calleds[pQue->CallPoint].CalloutResult = pQue->CalloutResult;
        pOut->Calleds[pQue->CallPoint].pQue_str = NULL;
      }
      else
      {
        pAcd->Calleds[pQue->CallPoint].CalloutResult = pQue->CalloutResult;
        pAcd->Calleds[pQue->CallPoint].pQue_str = NULL;
      }
      UnLink_All_By_ChnNo(pQue->OutnChn, 1);
      DBUpdateCallCDR_UpdateFlag(pQue->OutnChn);
      pBoxchn->ResetChn(pQue->OutnChn);
      DispChnStatus(pQue->OutnChn);
      
      if (pAgentMng->isnAGAvail(nAG))
      {
        pAG->nQue = 0xFFFF;
        if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
        {
          pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
          pAG->m_Seat.nChn = 0xFFFF;
        }
        SetAgentsvState(nAG, AG_SV_IDEL);
      }
      pQue->state = 0;
      break;
    case CALLOUT_REFUSE: //坐席拒絕來電
      CancelCallout(pQue->OutnChn);
      if (pQue->OrgCallType == 1)
      {
        pOut->Calleds[pQue->CallPoint].CalloutResult = pQue->CalloutResult;
        pOut->Calleds[pQue->CallPoint].pQue_str = NULL;
      }
      else
      {
        pAcd->Calleds[pQue->CallPoint].CalloutResult = pQue->CalloutResult;
        pAcd->Calleds[pQue->CallPoint].pQue_str = NULL;
      }
      UnLink_All_By_ChnNo(pQue->OutnChn, 1);
      DBUpdateCallCDR_UpdateFlag(pQue->OutnChn);
      pBoxchn->ResetChn(pQue->OutnChn);
      DispChnStatus(pQue->OutnChn);

      if (pAgentMng->isnAGAvail(nAG))
      {
        pAG->nQue = 0xFFFF;
        if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
        {
          pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
          pAG->m_Seat.nChn = 0xFFFF;
        }
        SetAgentsvState(nAG, AG_SV_IDEL);
      }
      pQue->state = 0;
      break;
    }
  }
}
//設置呼出隊列數據,返回結果: 0-成功設置 1-沒有中繼 2-呼出緩沖溢出 3-坐席號不存在
int SetCalloutQue(int nOut, int calledpoint)
{
  int ChIndex, nQue, nChn1=0xFFFF, nAG=0xFFFF;
  UC ChType;

  //-------------------------------------板卡版本---------------------------------------------
  if (calledpoint == 0 && pBoxchn->isnChnAvail(pOut->OutnChn))
  {
    //指定通道呼出
    nChn1 = pOut->OutnChn;
    pOut->Calleds[calledpoint].CurRouteNo = 0;
    pOut->Calleds[calledpoint].CurPoint = 0;
  }
  else
  {
    if (pOut->Calleds[calledpoint].CalledType == 1)
    {
      //呼叫的號碼是外線
      if (pOut->CalledNum ==1 && pOut->CallMode == 0 && pOut->RouteNum > 1)
      {
        nChn1 = pBoxchn->Get_IdelChnNo_By_Route(
          pOut->RouteNo[(pOut->CalledTimes)%(pOut->RouteNum)], 
          ChType, ChIndex);
        pOut->Calleds[calledpoint].CurRouteNo = pOut->RouteNo[(pOut->CalledTimes)%(pOut->RouteNum)];
        pOut->Calleds[calledpoint].CurPoint = (pOut->CalledTimes)%(pOut->RouteNum);
      }
      else
      {
        nChn1 = pBoxchn->Get_IdelChnNo_By_Route(pOut->Calleds[calledpoint].RouteNo, 
          ChType, ChIndex);
        pOut->Calleds[calledpoint].CurRouteNo = pOut->Calleds[calledpoint].RouteNo;
        if (calledpoint <= pOut->RouteNum)
          pOut->Calleds[calledpoint].CurPoint = calledpoint;
        else
          pOut->Calleds[calledpoint].CurPoint = 0;
      }
    }
    else
    {
      //呼叫的號碼為內線
      nAG = pAgentMng->GetnAGBySeatNo(pOut->Calleds[calledpoint].CalledNo);
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (pAG->GetSeatType() == SEATTYPE_RT_TEL) //5-遠程座席
        {
          nChn1 = pAG->GetSeatnChn();
          if (!pBoxchn->isnChnAvail(nChn1))
            nChn1 = pBoxchn->Get_IdelChnNo_By_Route(pAG->GetRouteNo(), ChType, ChIndex);
        }
        else
        {
          nChn1 = pAG->GetSeatnChn();
          //呼叫指定的坐席號時只要該坐席登錄且空閑就行,不需要判斷是否有話務員登錄及可分配
          if (pBoxchn->isTheChIdel(nChn1, true) != 0)
          {
            nChn1 = 0xFFFF;
          }
        }
        pOut->Calleds[calledpoint].CurRouteNo = 0;
        pOut->Calleds[calledpoint].CurPoint = 0;
      }
      else
      {
        //該坐席號不存在
        return 3; //空號
      }
    }
  }
  if (pBoxchn->isnChnAvail(nChn1))
  {
    nQue = pCalloutQue->Get_Idle_nQue();
    if (pCalloutQue->isnQueAvail(nQue))
    {
      pQue->state = 1;
      pQue->timer = 0;
      pQue->RingTime = pOut->RingTime;
      pQue->CallerType = pOut->CallerType;
      pQue->CallType = pOut->CallType;
      pQue->CallerNo = pOut->CallerNo;
      strncpy(pQue->CalledNo, pOut->Calleds[calledpoint].CalledNo, MAX_TELECODE_LEN-1);
      pQue->OrgCalledNo = pOut->OrgCalledNo;
      pQue->CalledNoPoint = 0;
      pQue->OutnChn = nChn1;
      pQue->OutRouteNo = pOut->Calleds[calledpoint].CurRouteNo;
      pQue->CancelId = 0;
      pQue->CalloutResult = 0;
      pQue->OrgCallType = 1;
      pQue->nOut = nOut;
      pQue->nAcd = 0xFFFF;
      pQue->nAG = nAG;
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (pAG->GetSeatType() == SEATTYPE_RT_TEL) //5-遠程座席
        {
          pAG->m_Seat.nChn = nChn1; //綁定遠程坐席與呼出通道的關系
          pChn1->nAG = nAG;
        }
        pAG->nQue = nQue;
        SetAgentsvState(nAG, AG_SV_INSEIZE);
      }
      pQue->CallPoint = calledpoint;

      pOut->Calleds[calledpoint].pQue_str = pQue;
      pOut->Calleds[calledpoint].CalloutResult = CALLOUT_CALLING;
      pOut->Calleds[calledpoint].OutnChn = nChn1;
      pOut->Calleds[calledpoint].nAG = nAG;

      pChn1->timer = 0;
	    pChn1->CalloutId = 1; //通道呼出占用標志
	    pChn1->nQue = nQue;
      strncpy(pChn1->CustPhone, pOut->Session.CallerNo.C_Str(), MAX_TELECODE_LEN-1);

      return 0; //成功設置
    }
    else
    {
      return 2; //呼出緩沖溢出
    }
  }
  return 1; //沒有中繼
}
//發送呼出結果
void SendCallResult(int nOut, int calledpoint, int result, const char *error)
{
  int nChn, nChn1, nChn2, nAG, nAG1, nCfc, nResult;

  nChn = pOut->Session.nChn;
  if (pBoxchn->isnChnAvail(nChn) && result != OnOutRing)
  {
    pChn->nOut = 0xFFFF;
  }
  if (pOut->Session.CmdId == 1)
  {
    SendCalloutResult(nOut, calledpoint, result, error);
  }
  else
  {
    if (pBoxchn->isnChnAvail(nChn))
    {
      if (result != OnOutRing && result != OnOutAnswer && result != OnTranTalk)
      {
        //轉接失敗,則重新與被轉接方通話
        nChn1 = pChn->TranControlnChn;
        if (pBoxchn->isnChnAvail(nChn1))
        {
          StopClearPlayDtmfBuf(nChn);
          StopClearPlayDtmfBuf(nChn1);
          nResult = RouterTalk(nChn, nChn1);
          if (nResult == 0)
          {
            pChn->RecMixerFlag = 1;
            pChn->LinkType[0] = 1;
            pChn->LinkChn[0] = nChn1;
            pChn->HoldingnChn = 0xFFFF;
            pChn1->RecMixerFlag = 1;
            pChn1->LinkType[0] = 1;
            pChn1->LinkChn[0] = nChn;
            pChn1->HoldingnChn = 0xFFFF;
          }
          pChn->TranControlnChn = 0xFFFF;
          pChn1->TranControlnChn = 0xFFFF;
          pChn->TranStatus = 0;
          pChn1->TranStatus = 0;
          
          nAG = pChn->nAG;
          if (pAgentMng->isnAGAvail(nAG))
          {
            WriteSeatStatus(pAG, AG_STATUS_TALK, 0);
            pAG->SetAgentsvState(AG_SV_CONN);
            DispAgentStatus(nAG);
          }
        }
        else
        {
          if (pOut->TranCallFailReturn > 0)
          {
            SetTranCallFailReturnTypeACDData(nChn, pOut->nAG, pOut->TranCallFailReturn);
          }
          else
          {
            Release_Chn(nChn, 1, 1);
          }
        }
      }
      else if (result == OnOutRing)
      {
        nChn1 = pChn->TranControlnChn; //被轉接的通道
        nChn2 = pOut->Calleds[0].OutnChn; //轉接的目的通道

        if (pChn->TranStatus == 0)
        {
          //控制轉接方已提前掛機了
          if (pOut->Calleds[0].CalledType == 1)
          {
            //轉接的是外線則被轉接方與目的通道交換,這是可以聽到外面的回鈴音
            StopClearPlayDtmfBuf(nChn);
            nResult = RouterTalk(nChn, nChn2);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn2;
              pChn->HoldingnChn = 0xFFFF;
              
              pChn2->RecMixerFlag = 1;
              pChn2->LinkType[0] = 1;
              pChn2->LinkChn[0] = nChn;
              pChn2->HoldingnChn = 0xFFFF;
            }
          }
          
          DBUpdateCallCDR_RingTime(nChn);
          if (pOut->Calleds[0].CalledType == 1)
          {
            //轉接的是外線
            DBUpdateCallCDR_WorkerNo(nChn, pOut->Calleds[0].CalledNo, pOut->Calleds[0].CalledNo);
          }
          else
          {
            //轉接的是坐席
            DBUpdateCallCDR_WorkerNo(nChn, pChn->nAG);
          }
          nAG = pChn2->nAG;
          if (pAgentMng->isnAGAvail(nAG))
          {
            pAG->SetCDRSerialNo(pChn->CdrSerialNo);
            WriteSeatStatus(pAG, AG_STATUS_INRING, 0, 1, 0);
            pAG->SetAgentsvState(AG_SV_INRING);
            DispAgentStatus(nAG);
          }
        }
        else
        {
          if (pOut->TranMode == 1)
          {
            //盲轉
            pOut->Session.SessionNo = 0;
            pOut->Session.CmdAddr = 0;
            pOut->Session.nChn = nChn1;
            
            pChn->TranStatus = 0;
            pChn->nOut = 0xFFFF;
            pChn->TranControlnChn = 0xFFFF;
            pChn->HoldingnChn = 0xFFFF;
            
            pChn1->TranStatus = 0;
            pChn1->nOut = nOut;
            pChn1->TranControlnChn = 0xFFFF;
            pChn1->HoldingnChn = 0xFFFF;
            
            if (pOut->Calleds[0].CalledType == 1)
            {
              //轉接的是外線則被轉接方與目的通道交換,這是可以聽到外面的回鈴音
              StopClearPlayDtmfBuf(nChn1);
              nResult = RouterTalk(nChn1, nChn2);
              if (nResult == 0)
              {
                pChn1->RecMixerFlag = 1;
                pChn1->LinkType[0] = 1;
                pChn1->LinkChn[0] = nChn2;
                pChn1->HoldingnChn = 0xFFFF;
                
                pChn2->RecMixerFlag = 1;
                pChn2->LinkType[0] = 1;
                pChn2->LinkChn[0] = nChn1;
                pChn2->HoldingnChn = 0xFFFF;
              }
            }
            Release_Chn(nChn, 1, 1);
            DBUpdateCallCDR_RelReason(nChn1, 2);
            DBUpdateCallCDR_RelTime(nChn1);
            //插入新的話單
            if (strlen(pOut->Session.CdrSerialNo) > 0)
            {
              strcpy(pChn1->CdrSerialNo, pOut->Session.CdrSerialNo);
              strcpy(pChn1->RecordFileName, pOut->Session.RecordFileName);
            }
            else
            {
              pChn1->GenerateCdrSerialNo();
            }
            nAG = pChn2->nAG;
            if (pAgentMng->isnAGAvail(nAG))
            {
              pAG->SetCDRSerialNo(pChn1->CdrSerialNo);
              DBInsertCallCDR(nChn1, 1, 3, pAG->GetWorkerNoStr());
              
              WriteSeatStatus(pAG, AG_STATUS_INRING, 0, 1, 0);
              pAG->SetAgentsvState(AG_SV_INRING);
              DispAgentStatus(nAG);
            }
            else
            {
              DBInsertCallCDR(nChn1, 1, 3);
            }
            DBUpdateCallCDR_ACDTime(nChn1, 0, 0);
            if (pOut->Calleds[0].CalledType == 1)
            {
              //轉接的是外線
              DBUpdateCallCDR_WorkerNo(nChn1, pOut->Calleds[0].CalledNo, pOut->Calleds[0].CalledNo);
            }
            else
            {
              //轉接的是坐席
              DBUpdateCallCDR_WorkerNo(nChn1, nAG);
            }
            SendCdrSerialNo(nChn1);
          }
          else
          {
            pChn->TranStatus = 4; //轉接振鈴
            if (pOut->Calleds[0].CalledType == 1)
            {
              StopClearPlayDtmfBuf(nChn);
              nResult = RouterTalk(nChn, nChn2);
              if (nResult == 0)
              {
                pChn->RecMixerFlag = 1;
                pChn->LinkType[0] = 1;
                pChn->LinkChn[0] = nChn2;
                pChn->HoldingnChn = 0xFFFF;
                
                pChn2->RecMixerFlag = 1;
                pChn2->LinkType[0] = 1;
                pChn2->LinkChn[0] = nChn;
                pChn2->HoldingnChn = 0xFFFF;
              }
            }
            nAG = pChn->nAG;
            nAG1 = pChn2->nAG;
            if (pAgentMng->isnAGAvail(nAG) && pAgentMng->isnAGAvail(nAG1))
            {
              pAG1->SetCDRSerialNo(pAG->GetWorkerNoStr());
            }
          }
        }
      }
      else if (result == OnOutAnswer)
      {
        //轉接的目的通道應答處理
        pChn->nOut = 0xFFFF;
        nChn1 = pChn->TranControlnChn; //被轉接通道,如果提前釋放就沒有效
        nChn2 = pOut->Calleds[0].OutnChn; //轉接的目的通道
        
        if (pChn->TranStatus == 0)
        {
          //轉接目的方未應答前,控制轉接方已掛機時處理
          if (pOut->Calleds[0].CalledType == 2)
          {
            StopClearPlayDtmfBuf(nChn);
            nResult = RouterTalk(nChn, nChn2);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn2;
              pChn2->RecMixerFlag = 1;
              pChn2->LinkType[0] = 1;
              pChn2->LinkChn[0] = nChn;
            }
            nAG = pChn2->nAG;
            if (pAgentMng->isnAGAvail(nAG))
            {
              strcpy(pAG->CdrSerialNo, pChn->CdrSerialNo);
              DBUpdateCallCDR_SeatAnsTime(pAG->nAG, pAG->GetAcdedGroupNo());
              if (g_nSwitchMode == 0 && pAG->m_Worker.AutoRecordId == 1 && pAG->m_Seat.AutoRecordId == 1)
              {
                char szTemp[256], pszError[128];
                
                //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
                strcpy(pAG->RecordFileName, pChn->RecordFileName);
                
                sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAG->RecordFileName);
                CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
                
                if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
                {
                  if (SetChnRecordRule(pAG->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
                  {
                    DBUpdateCallCDR_RecdFile(pAG->nAG);
                  }
                }
              }
              
              WriteSeatStatus(pAG, AG_STATUS_TALK, 1, 1, 1);
              pAG->SetAgentsvState(AG_SV_CONN);
              DispAgentStatus(nAG);
            }
          }
          else
          {
            //轉接的是外線
            DBUpdateCallCDR_SeatAnsTime(nChn, pOut->Calleds[0].CalledNo, pOut->Calleds[0].CalledNo);
            pChn2->TranOutForAgent = 1;
            strcpy(pChn2->CdrSerialNo, pChn->CdrSerialNo);
            if (g_nSwitchMode == 0 && pIVRCfg->isRecordTranOutCall == true)
            {
              char szTemp[256], pszError[128];
              
              pChn->GenerateRecordFileName();
              
              sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pChn->RecordFileName);
              CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
              
              if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
              {
                if (SetChnRecordRule(nChn2, 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
                {
                  DBUpdateCallCDR_ChnRecdFile(nChn);
                }
              }
            }
          }
        }
        else if (pChn->TranStatus == 3 || pChn->TranStatus == 4)
        {
          //TranStatus=3表示還未收到目的方振鈴直接收到應答時處理;TranStatus=4表示收到目的方振鈴后再收到目的方應答時處理
          pChn->TranDestionnChn = nChn2;
          strcpy(pChn2->TranCdrSerialNo, pOut->Session.CdrSerialNo);
          strcpy(pChn2->TranRecordFileName, pOut->Session.RecordFileName);
          MyTrace(3, "SendCallResult nChn=%d nChn1=%d nChn2=%d TranCdrSerialNo=%s TranRecordFileName=%s",
            nChn, nChn1, nChn2, pChn->TranCdrSerialNo, pChn->TranRecordFileName);
          pChn2->TranStatus =2;
          pChn2->TranControlnChn = nChn;
          
          if (pOut->TranMode != 3)
          {
            pChn->TranStatus = 5;
            //不是會議轉接
            StopClearPlayDtmfBuf(nChn);
            nResult = RouterTalk(nChn, nChn2);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn2;
              pChn2->RecMixerFlag = 1;
              pChn2->LinkType[0] = 1;
              pChn2->LinkChn[0] = nChn;
            }
            nAG = pChn->nAG;
            if (pAgentMng->isnAGAvail(nAG))
            {
              WriteSeatStatus(pAG, AG_STATUS_CONSULT, 0);
            }
          }
          else
          {
            //會議轉接
            nCfc = CreateThreeConf(nChn);
            MyTrace(3, "nChn=%d nChn1=%d nChn2=%d pChn->TranStatus=%d pOut->Calleds[0].CalledType=%d",
              nChn, nChn1, nChn2, pChn->TranStatus, pOut->Calleds[0].CalledType);
            if (nCfc > 0)
            {
              if (pChn->TranStatus == 4 && pOut->Calleds[0].CalledType == 1)
              {
                //先斷開控制轉接方與轉接目的通道的連接
                UnRouterTalk(nChn, nChn2);
                pChn->LinkType[0] = 0;
                pChn->LinkChn[0] = 0xFFFF;
                pChn2->LinkType[0] = 0;
                pChn2->LinkChn[0] = 0xFFFF;
                StopClearPlayDtmfBuf(nChn); //2012-07-13
              }
              else
              {
                StopClearPlayDtmfBuf(nChn);
              }
              StopClearPlayDtmfBuf(nChn1);

              pChn->TranStatus = 0;
              pChn1->TranStatus = 0;
              pChn2->TranStatus = 0;
              pChn->HoldingnChn = 0xFFFF;
              pChn1->HoldingnChn = 0xFFFF;
              if (JoinThreeConf(nCfc, nChn, nChn1, nChn2) == 0)
              {
                nAG = pChn->nAG;
                if (pAgentMng->isnAGAvail(nAG))
                {
                  WriteSeatStatus(pAG, AG_STATUS_CONF, 0);
                  pAG->SetAgentsvState(AG_SV_CONN);
                  DispAgentStatus(nAG);
                }
              }
            }
            else
            {
              if (pChn->TranStatus == 3 || pOut->Calleds[0].CalledType != 1)
              {
                StopClearPlayDtmfBuf(nChn);
                nResult = RouterTalk(nChn, nChn2);
                if (nResult == 0)
                {
                  pChn->RecMixerFlag = 1;
                  pChn->LinkType[0] = 1;
                  pChn->LinkChn[0] = nChn2;
                  pChn2->RecMixerFlag = 1;
                  pChn2->LinkType[0] = 1;
                  pChn2->LinkChn[0] = nChn;
                }
              }
              pChn->TranStatus = 5;
              nAG = pChn->nAG;
              if (pAgentMng->isnAGAvail(nAG))
              {
                WriteSeatStatus(pAG, AG_STATUS_CONSULT, 0);
              }
            }
          }
          nAG = pChn->nAG;
          nAG1 = pChn2->nAG;
          if (pAgentMng->isnAGAvail(nAG) && pAgentMng->isnAGAvail(nAG1))
          {
            pAG1->SetCDRSerialNo(pAG->GetWorkerNoStr());
          }
        }
      }
    }
    if (pOut->Session.CmdId == 2)
      SendTransferResult(nOut, calledpoint, result, error);
    else if (pOut->Session.CmdId == 3)
      SendSeatTranCallFailResult(pChn->nAG, "", "", result, "");
  }
}
//處理呼出緩沖隊列
void ProcCalloutBuf()
{
  char pszError[128];
  int i, j, CallingCount, FailCount;
  int nGetQueResult, onResult=OnOutFail;
  int nChn, nChn1, nAG, nAG1;

  for (int nOut = 0; nOut < pCallbuf->MaxOutNum; nOut ++)
  {
    if (pOut->state == 0) continue;

    nChn = pOut->Session.nChn; //控制轉接的通道
    
    if (pBoxchn->isnChnAvail(nChn))
    {
      nAG1 = pChn->nAG;
      nChn1 = pChn->TranControlnChn; //被轉接的通道
    }
    else
    {
      nAG1 = 0xFFFF;
      nChn1 = 0xFFFF;
    }
    
    switch (pOut->CallStep)
    {
    case 0: //等待處理呼叫隊列
      //處理被叫
      if (pOut->CancelId == 2)
      {
        if (pBoxchn->isnChnAvail(nChn))
        {
          pChn->TranControlnChn= 0xFFFF;
          pChn->TranStatus = 0;
          pChn->nOut = 0xFFFF;
        }
        if (pBoxchn->isnChnAvail(nChn1))
        {
          pChn1->TranControlnChn= 0xFFFF;
          pChn1->TranStatus = 0;
        }
        pOut->state = 0;
        break; //edit 2009-04-11
      }
      if (pOut->CalledTimes >= pOut->CallTimes)
      {
        SendCallResult(nOut, 0, OnOutFail, "call times finished but fail");
        pOut->state = 0;
        break;
      }
      if (pOut->timer < pOut->IntervalTime) break;

      CallingCount = 0;
      pOut->timer = 0;
      if (pOut->CallMode == 0)
      {
        //順呼處理
        nGetQueResult = SetCalloutQue(nOut, pOut->CallPoint);
        if (nGetQueResult == 0)
          CallingCount ++;
        else if (nGetQueResult == 1)
          pOut->Calleds[pOut->CallPoint].CalloutResult = CALLOUT_NOTRUNK;
        else if (nGetQueResult == 3)
          pOut->Calleds[pOut->CallPoint].CalloutResult = CALLOUT_UNN;
        else
          pOut->Calleds[pOut->CallPoint].CalloutResult = CALLOUT_FAIL;
        pOut->CallPoint = (pOut->CallPoint+1)%(pOut->CalledNum);
        if (pOut->CallPoint == pOut->StartPoint)
          pOut->CalledTimes++;
      }
      else
      {
        //同呼處理
        for (i=0; i<pOut->CalledNum; i++)
        {
          nGetQueResult = SetCalloutQue(nOut, i);
          if (nGetQueResult == 0)
            CallingCount ++;
          else if (nGetQueResult == 1)
            pOut->Calleds[i].CalloutResult = CALLOUT_NOTRUNK;
          else if (nGetQueResult == 3)
            pOut->Calleds[i].CalloutResult = CALLOUT_UNN;
          else
            pOut->Calleds[i].CalloutResult = CALLOUT_FAIL;
        }
        pOut->CalledTimes++;
      }
      if (CallingCount == 0)
      {
        if (pOut->CalledTimes >= pOut->CallTimes)
        {
          //規定的呼叫次數已完成，發送線路忙的呼出結果
          if (nGetQueResult == 1)
            SendCallResult(nOut, 0, OnOutNoTrk, "line busy");
          else if (nGetQueResult == 3)
            SendCallResult(nOut, 0, OnOutUNN, "seatno unn");
          else
            SendCallResult(nOut, 0, OnOutFail, "callbuf overflow");
          pOut->state = 0;
        }
        else
        {
          //規定的呼叫次數未完成，繼續等待呼叫
          for (i=0; i<pOut->CalledNum; i++)
          {
            pOut->Calleds[i].CalloutResult = 0;
          }
          if (pOut->CallMode == 1)
            pOut->timer = 0;
          else
            pOut->timer = pOut->IntervalTime;
        }
        break;
      }
      pOut->CallStep = 1;
      if (pOut->Session.CmdId == 1)
      {
        DBUpdateCallCDR_ACDTime(nChn, 0, 0);
        SendCalloutResult(nOut, 0, OnOutCalling, "");
      }
      break;
    case 1: //等待呼叫結果
    case 2: //等待應答結果
      if (pOut->CancelId == 2)
      {
        if (pBoxchn->isnChnAvail(nChn))
        {
          pChn->TranControlnChn= 0xFFFF;
          pChn->TranStatus = 0;
          pChn->nOut = 0xFFFF;
        }
        if (pBoxchn->isnChnAvail(nChn1))
        {
          pChn1->TranControlnChn= 0xFFFF;
          pChn1->TranStatus = 0;
        }
        for (i=0; i<pOut->CalledNum; i++)
        {
          //取消呼出
          if (pOut->Calleds[i].pQue_str != NULL)
            pOut->Calleds[i].pQue_str->CancelId = 1;
          //釋放已應答的通道
          if (pOut->Calleds[i].CalloutResult == CALLOUT_CONN)
            Release_Chn(pOut->Calleds[i].OutnChn, 1, 0);
        }
        pOut->state = 0;
        break; //edit 2009-04-11
      }
      if (pOut->CalledTimes >= pOut->CallTimes && pOut->timer > (UL)TIMERS*pOut->RingTime)
      {
        //超時,取消呼出
        for (i=0; i<pOut->CalledNum; i++)
        {
          //取消呼出
          if (pOut->Calleds[i].pQue_str != NULL)
            pOut->Calleds[i].pQue_str->CancelId = 1;
          //釋放已應答的通道
          if (pOut->Calleds[i].CalloutResult == CALLOUT_CONN)
            Release_Chn(pOut->Calleds[i].OutnChn, 1, 0);
        }
        SendCallResult(nOut, 0, OnOutTimeout, "timeout");
        pOut->state = 0;
        break;
      }
      FailCount = 0;
      for (i=0; i<pOut->CalledNum; i++)
      {
        nAG = pOut->Calleds[i].nAG;
        switch (pOut->Calleds[i].CalloutResult)
        {
        case CALLOUT_RING: //2-正在振鈴
          //發送呼出振鈴結果
          if ((pOut->CalledNum == 1 || pOut->CallMode == 0) && pOut->CallStep == 1)
          {
            SendCallResult(nOut, i, OnOutRing, "");
            if (pAgentMng->isnAGAvail(nAG))
            {
              //if (pAcd->ACDRule.SeatNo.Compare("0") == 0) //當不是呼叫指定坐席號時累計話務量
              WriteSeatStatus(pAG, AG_STATUS_INRING, 0, 1, 0);
              SetAgentsvState(nAG, AG_SV_INRING);
            }
            if (pOut->Session.CmdId == 1)
            {
              DBUpdateCallCDR_RingTime(nChn);
              DBUpdateCallCDR_WorkerNo(nChn, nAG1);
            }
            pOut->CallStep = 2;
          }
          pOut->Calleds[i].CalloutResult = 0;
          break;
        case CALLOUT_CONN: //3-用戶應答
          //發送呼出應答結果
          SendCallResult(nOut, i, OnOutAnswer, "");
          
          if (pAgentMng->isnAGAvail(nAG)) 
          {
            //if (pAG->GetSeatNo().Compare("0") == 0) //當不是呼叫指定坐席號時累計話務量
            {
              //記錄最后應答的坐席序號
              pAgentMng->lastAnsnAG[0] = nAG;
              pAgentMng->lastAnsnAG[pAG->GetGroupNo()] = nAG;
              pAgentMng->lastAnsLevelnAG[0][0] = nAG;
              pAgentMng->lastAnsLevelnAG[0][pAG->GetLevel()] = nAG;
              pAgentMng->lastAnsLevelnAG[pAG->GetGroupNo()][pAG->GetLevel()] = nAG;
              
              WriteSeatStatus(pAG, AG_STATUS_TALK, 1, 1, 1);
            }
            pAG->CanTransferId = 1;
            SetAgentsvState(nAG, AG_SV_CONN);
          }
          if (pOut->Session.CmdId == 1 && pIVRCfg->ControlCallModal == 1 && pBoxchn->isnChnAvail(nChn) && pAgentMng->isnAGAvail(nAG1))
          {
            //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
            strcpy(pAG1->CdrSerialNo, pChn->CdrSerialNo);
            strcpy(pAG1->RecordFileName, pChn->RecordFileName);
            DBUpdateCallCDR_SeatAnsTime(pAG1->nAG, pAG1->GetGroupNo());
            if (pAG1->m_Worker.AutoRecordId == 1 && pAG1->m_Seat.AutoRecordId == 1)
            {
              char szTemp[256];
              sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAG1->RecordFileName);
              CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
              
              if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
              {
                if (SetChnRecordRule(pAG1->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
                {
                  DBUpdateCallCDR_RecdFile(pAG1->nAG);
                }
              }
            }
          }

          for (j=0; j<pOut->CalledNum; j++)
          {
            if (i==j) continue;
            if (pOut->Calleds[j].pQue_str != NULL)
              pOut->Calleds[j].pQue_str->CancelId = 1;
            if (pOut->Calleds[j].CalloutResult == CALLOUT_CONN)
            {
              pOut->Calleds[j].CalloutResult = 0;
              Release_Chn(pOut->Calleds[j].OutnChn, 1, 0);
            }
          }
          if (pOut->Session.FuncNo > 0)
          {
            SendCallFlwEvent(1, nOut, i);
          }
          pOut->state = 0;
          pOut->Calleds[i].CalloutResult = 0;
          break;
        case CALLOUT_NOBODY: //6-無人應答
          onResult = OnOutTimeout;
          FailCount++;
          if (pOut->CallMode == 0)
            pOut->Calleds[i].CalloutResult = 0;
          break;
        case CALLOUT_SLB: //4-用戶市忙
        case CALLOUT_STB:	//5-用戶長忙
          onResult = OnOutBusy;
          FailCount++;
          if (pOut->CallMode == 0)
            pOut->Calleds[i].CalloutResult = 0;
          break;
        case CALLOUT_UNN:	//8-空號
          onResult = OnOutUNN;
          FailCount++;
          if (pOut->CallMode == 0)
            pOut->Calleds[i].CalloutResult = 0;
          break;
        case CALLOUT_NOTRUNK:	//7-無空閑出中繼
          onResult = OnOutNoTrk;
          FailCount++;
          if (pOut->CallMode == 0)
            pOut->Calleds[i].CalloutResult = 0;
          break;
        case CALLOUT_FAIL: //9-失敗
        case CALLOUT_CNG:	//10-設備擁塞
        case CALLOUT_CGC:	//11-電路群擁塞
        case CALLOUT_ADI:	//12-地址不全
        case CALLOUT_SST:	//13-專用信號音
        case CALLOUT_OFF:	//14-用戶關機
        case CALLOUT_BAR:	//15-接入拒絕
        case CALLOUT_REFUSE: //16-坐席拒絕來電
          onResult = OnOutFail;
          FailCount++;
          if (pOut->CallMode == 0)
            pOut->Calleds[i].CalloutResult = 0;
          break;
        }
      }
      if (pOut->state == 1 
        && ((pOut->CallMode == 1 && FailCount >= pOut->CalledNum) 
        ||(pOut->CallMode == 0 && FailCount == 1)))
      {
        if (pOut->CallMode == 0)
        {
          if (pOut->CallPoint == pOut->StartPoint)
          {
            //pOut->CalledTimes++;
            pOut->timer = 0;
          }
          else
          {
            pOut->timer = pOut->IntervalTime;
          }
        }
        else
        {
          for (i=0; i<pOut->CalledNum; i++)
          {
            pOut->Calleds[i].CalloutResult = 0;
          }
          //pOut->CalledTimes++;
          pOut->timer = 0;
        }
        if (pOut->CalledTimes >= pOut->CallTimes)
        {
          SendCallResult(nOut, 0, onResult, "call times finished but fail");
          pOut->state = 0;
        }
        else
        {
          pOut->CallStep = 0;
        }
      }
      break;
    }
  }
}

//中繼實時話務量統計
void TrkCallMeteCount()
{
  static int tcount=0;
  if (pIVRCfg->isCountCall == false)
  {
    return;
  }
  if ((tcount++)<1000)
  {
    return;
  }
  tcount = 0;
  if (pBoxchn->TotalTrkNum <= 0) 
  {
    return;
  }
  ChCount.TrkInCur = 0;
  ChCount.TrkOutCur = 0;
  ChCount.TrkBusyCount = 0;
  //計算當前在線數
  for (int nChn = 0; nChn < pBoxchn->MaxChnNum; nChn ++)
  {
    if (pChn->state == 0) continue;
    
    if (pChn->ssState != CHN_SNG_IDLE)
    {
      if (pChn->lgChnType == 1)
      {
        ChCount.TrkBusyCount ++;
        if (pChn->CallInOut == 1)
        {
          ChCount.TrkInCur ++;
        }
        else if (pChn->CallInOut == 2)
        {
          ChCount.TrkOutCur ++;
        }
      }
    }
  }

  if (ChCount.TrkInCur > ChCount.TrkInMax)
    ChCount.TrkInMax = ChCount.TrkInCur;
  if (ChCount.TrkInCur < ChCount.TrkInMin || ChCount.TrkInMin == 0)
    ChCount.TrkInMin = ChCount.TrkInCur;
  
  if (ChCount.TrkOutCur > ChCount.TrkOutMax)
    ChCount.TrkOutMax = ChCount.TrkOutCur;
  if (ChCount.TrkOutCur < ChCount.TrkOutMin || ChCount.TrkOutMin == 0)
    ChCount.TrkOutMin = ChCount.TrkOutCur;
  
  if (ChCount.TrkBusyCount > ChCount.TrkBusyMax)
    ChCount.TrkBusyMax = ChCount.TrkBusyCount;
  if (ChCount.TrkBusyCount < ChCount.TrkBusyMin || ChCount.TrkBusyMin == 0)
    ChCount.TrkBusyMin = ChCount.TrkBusyCount;
  //計算中繼占用率
  int nCurBusyPer = ChCount.TrkBusyCount * 100 / pBoxchn->TotalTrkNum;
  if (nCurBusyPer == 0)
  {
    ChCount.TrkBusy0 ++;
  } 
  else if (nCurBusyPer > 0 && nCurBusyPer <= 25)
  {
    ChCount.TrkBusy0_25 ++;
  }
  else if (nCurBusyPer > 25 && nCurBusyPer <= 50)
  {
    ChCount.TrkBusy25_50 ++;
  }
  else if (nCurBusyPer > 50 && nCurBusyPer <= 75)
  {
    ChCount.TrkBusy50_75 ++;
  }
  else if (nCurBusyPer > 75 && nCurBusyPer <= 90)
  {
    ChCount.AgBusy75_90 ++;
  }
  else
  {
    ChCount.AgBusy90_100 ++;
  }
  ChCount.TrkBusyTotal ++;
  //沒半小時記錄及發送統計結果
  static bool bTotalId=false;
  char LogFile[256], sqls[512];
  FILE	*Ftrace;
  int nCountTimeId;
  
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  if ((pIVRCfg->CountCycle == 2 && local->tm_min == 28) || local->tm_min == 58)
    bTotalId = true;
  if (((pIVRCfg->CountCycle == 2 && local->tm_min == 29) || local->tm_min == 59) && bTotalId == true)
  {
    if (pIVRCfg->CountCycle == 2)
    {
      nCountTimeId = local->tm_hour*2;
      if (local->tm_min == 59)
      {
        nCountTimeId ++;
      }
    }
    else
    {
      nCountTimeId = local->tm_hour;
    }
    //計算中繼占用百分率
    if (ChCount.TrkBusyTotal > 0)
    {
      ChCount.TrkBusy0_25Per = ChCount.TrkBusy0_25*100/ChCount.TrkBusyTotal;
      ChCount.TrkBusy25_50Per = ChCount.TrkBusy25_50*100/ChCount.TrkBusyTotal;
      ChCount.TrkBusy50_75Per = ChCount.TrkBusy50_75*100/ChCount.TrkBusyTotal;
      ChCount.TrkBusy75_90Per = ChCount.TrkBusy75_90*100/ChCount.TrkBusyTotal;
      ChCount.TrkBusy90_100Per = ChCount.TrkBusy90_100*100/ChCount.TrkBusyTotal;
      ChCount.TrkBusy0Per = 100-ChCount.TrkBusy0_25Per-ChCount.TrkBusy25_50Per-ChCount.TrkBusy50_75Per-ChCount.TrkBusy75_90Per-ChCount.TrkBusy90_100Per;
    }
    //寫統計數據到文件
    if (pIVRCfg->isWriteCountCallTxt == true)
    {
      sprintf(LogFile, "%s\\TRK_%04d%02d.log", g_szCountPath, local->tm_year+1900, local->tm_mon+1);
      Ftrace = fopen(LogFile, "a+t");
      if (Ftrace != NULL)
      {
        fprintf(Ftrace, "%04d-%02d-%02d,%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
          nCountTimeId,
          
          ChCount.TrkCallCount,
          ChCount.TrkBusyMin,
          ChCount.TrkBusyMax,
          
          ChCount.TrkInCount,
          ChCount.TrkInAnsCount,
          ChCount.TrkInMin,
          ChCount.TrkInMax,
          
          ChCount.TrkOutCount,
          ChCount.TrkOutAnsCount,
          ChCount.TrkOutMin,
          ChCount.TrkOutMax,
          
          ChCount.TrkBusy0Per,
          ChCount.TrkBusy0_25Per,
          ChCount.TrkBusy25_50Per,
          ChCount.TrkBusy50_75Per,
          ChCount.TrkBusy75_90Per,
          ChCount.TrkBusy90_100Per);
        
        fclose(Ftrace);
        Ftrace = NULL;
      }
    }
    //發送統計數據到流程解析器寫數據庫
    if (pIVRCfg->isWriteCountCallDB == true)
    {
      if (MyStrCmpNoCase(g_szDBType, "MYSQL") == 0)
      {
        sprintf(sqls, "insert into tbTrkCount values (0,'%04d-%02d-%02d','%02d:%02d',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
          nCountTimeId,
          
          ChCount.TrkCallCount,
          ChCount.TrkBusyMin,
          ChCount.TrkBusyMax,
          
          ChCount.TrkInCount,
          ChCount.TrkInAnsCount,
          ChCount.TrkInMin,
          ChCount.TrkInMax,
          
          ChCount.TrkOutCount,
          ChCount.TrkOutAnsCount,
          ChCount.TrkOutMin,
          ChCount.TrkOutMax,
          
          ChCount.TrkBusy0Per,
          ChCount.TrkBusy0_25Per,
          ChCount.TrkBusy25_50Per,
          ChCount.TrkBusy50_75Per,
          ChCount.TrkBusy75_90Per,
          ChCount.TrkBusy90_100Per);
      }
      else
      {
        sprintf(sqls, "insert into tbTrkCount values ('%04d-%02d-%02d','%02d:%02d',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
          nCountTimeId,
          
          ChCount.TrkCallCount,
          ChCount.TrkBusyMin,
          ChCount.TrkBusyMax,
          
          ChCount.TrkInCount,
          ChCount.TrkInAnsCount,
          ChCount.TrkInMin,
          ChCount.TrkInMax,
          
          ChCount.TrkOutCount,
          ChCount.TrkOutAnsCount,
          ChCount.TrkOutMin,
          ChCount.TrkOutMax,
          
          ChCount.TrkBusy0Per,
          ChCount.TrkBusy0_25Per,
          ChCount.TrkBusy25_50Per,
          ChCount.TrkBusy50_75Per,
          ChCount.TrkBusy75_90Per,
          ChCount.TrkBusy90_100Per);
      }
      sprintf(SendMsgBuf, "<execsql sessionid='%d' cmdaddr='0' chantype='0' channo='0' sqls=\"%s\"/>", 
        0x02010000, sqls);
      SendMsg2XML(MSG_onexecsql, 0x02010000, SendMsgBuf);
    }
      
    sprintf(sqls, "%d,%04d-%02d-%02d,%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
      nCountTimeId,
      local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
      
      ChCount.TrkCallCount,
      ChCount.TrkBusyMin,
      ChCount.TrkBusyMax,
      
      ChCount.TrkInCount,
      ChCount.TrkInAnsCount,
      ChCount.TrkInMin,
      ChCount.TrkInMax,
      
      ChCount.TrkOutCount,
      ChCount.TrkOutAnsCount,
      ChCount.TrkOutMin,
      ChCount.TrkOutMax,
      
      ChCount.TrkBusy0Per,
      ChCount.TrkBusy0_25Per,
      ChCount.TrkBusy25_50Per,
      ChCount.TrkBusy50_75Per,
      ChCount.TrkBusy75_90Per,
      ChCount.TrkBusy90_100Per);
    SendTrkCountData(nCountTimeId, sqls);
    strcpy(ChCount.strTrkData, sqls);

    TodayChCount[nCountTimeId].SetTrkData(ChCount);
    ChCount.InitTrkData();
    if (nCountTimeId == 47)
    {
      for (int i = 0; i < 48; i ++)
      {
        TodayChCount[i].InitTrkData();
      }
    }
    bTotalId = false;
  }
}
//發送即時統計數據
void SendNowTrkCallMeteCount()
{
  char sqls[512];
  int nCountTimeId;
  
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

  if (pIVRCfg->CountCycle == 2)
  {
    nCountTimeId = local->tm_hour*2;
    if (local->tm_min == 59)
    {
      nCountTimeId ++;
    }
  }
  else
  {
    nCountTimeId = local->tm_hour;
  }
  //計算中繼占用百分率
  if (ChCount.TrkBusyTotal > 0)
  {
    ChCount.TrkBusy0_25Per = ChCount.TrkBusy0_25*100/ChCount.TrkBusyTotal;
    ChCount.TrkBusy25_50Per = ChCount.TrkBusy25_50*100/ChCount.TrkBusyTotal;
    ChCount.TrkBusy50_75Per = ChCount.TrkBusy50_75*100/ChCount.TrkBusyTotal;
    ChCount.TrkBusy75_90Per = ChCount.TrkBusy75_90*100/ChCount.TrkBusyTotal;
    ChCount.TrkBusy90_100Per = ChCount.TrkBusy90_100*100/ChCount.TrkBusyTotal;
    ChCount.TrkBusy0Per = 100-ChCount.TrkBusy0_25Per-ChCount.TrkBusy25_50Per-ChCount.TrkBusy50_75Per-ChCount.TrkBusy75_90Per-ChCount.TrkBusy90_100Per;
  }
  sprintf(sqls, "%d,%04d-%02d-%02d,%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    nCountTimeId,
    local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
    
    ChCount.TrkCallCount,
    ChCount.TrkBusyMin,
    ChCount.TrkBusyMax,
    
    ChCount.TrkInCount,
    ChCount.TrkInAnsCount,
    ChCount.TrkInMin,
    ChCount.TrkInMax,
    
    ChCount.TrkOutCount,
    ChCount.TrkOutAnsCount,
    ChCount.TrkOutMin,
    ChCount.TrkOutMax,
    
    ChCount.TrkBusy0Per,
    ChCount.TrkBusy0_25Per,
    ChCount.TrkBusy25_50Per,
    ChCount.TrkBusy50_75Per,
    ChCount.TrkBusy75_90Per,
    ChCount.TrkBusy90_100Per);
  SendTrkCountData(nCountTimeId, sqls);
}
//發送即時路由空閑中繼數據
void SendRouteIdelTrkNum(bool bFirstId)
{
  int i;
  int nTemp = 0;
  
  if (pIVRCfg->isSendRouteIdelTrkNum == false && bFirstId == false)
  {
    return;
  }
  if (pBoxchn->TotalTrkNum <= 0) 
  {
    return;
  }
  ChCount.TrkInCur = 0;
  ChCount.TrkOutCur = 0;
  ChCount.TrkBusyCount = 0;
  for (i=0; i<MAX_ROUTE_NUM; i++)
  {
    pBoxchn->Routes[i].TrkBusyNum = 0;
  }
  //計算當前在線數
  for (int nChn = 0; nChn < pBoxchn->MaxChnNum; nChn ++)
  {
    if (pChn->state == 0) continue;
    
    if (pChn->ssState != CHN_SNG_IDLE)
    {
      if (pChn->lgChnType == 1)
      {
        pBoxchn->Routes[0].TrkBusyNum ++;
        if (pChn->RouteNo > 0 && pChn->RouteNo < MAX_ROUTE_NUM)
        {
          pBoxchn->Routes[pChn->RouteNo].TrkBusyNum ++;
        }
      }
    }
  }
  
  for (i=0; i<MAX_ROUTE_NUM; i++)
  {
    if (pBoxchn->Routes[i].TrkTotalNum == 0)
    {
      continue;
    }
    if (pBoxchn->Routes[i].OldTrkBusyNum != pBoxchn->Routes[i].TrkBusyNum || bFirstId == true)
    {
      if (pBoxchn->Routes[i].TrkTotalNum > pBoxchn->Routes[i].TrkBusyNum)
      {
        nTemp = pBoxchn->Routes[i].TrkTotalNum - pBoxchn->Routes[i].TrkBusyNum;
      } 
      else
      {
        nTemp = 0;
      }

      if (pParser->VxmlLogId[1] == 1)
      {
        sprintf(SendMsgBuf, "<onrouteideltrks sessionid='%d' cmdaddr='0' chantype='0' channo='0' routeno='%d' idelnum='%d'/>", 
          0x02010000, i, nTemp);
        SendMsg2XML(MSG_onrouteideltrks, 0x02010000, SendMsgBuf);
      }
      
      pBoxchn->Routes[i].OldTrkBusyNum = pBoxchn->Routes[i].TrkBusyNum;
    }
  }
}
//-----------------------------------------------------------------------------
//發送詳細話單號
void SendCdrSerialNo(int nChn)
{
  if (pChn->SessionNo == 0)
  {
    return;
  }
  sprintf(SendMsgBuf, "<onnewcallparam sessionid='%lu' cmdaddr='%lu' chantype='%d' channo='%d' callerno='' calledno='' workerno='' cdrserialno='%s' param=''/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, pChn->CdrSerialNo);
  SendMsg2XML(MSG_onnewcallparam, pChn->SessionNo, SendMsgBuf);
}
void SendCdrSerialNo(int nChn, const char *CdrSerialNo)
{
  if (pChn->SessionNo == 0)
  {
    return;
  }
  sprintf(SendMsgBuf, "<onnewcallparam sessionid='%lu' cmdaddr='%lu' chantype='%d' channo='%d' callerno='' calledno='' workerno='' cdrserialno='%s' param=''/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, CdrSerialNo);
  SendMsg2XML(MSG_onnewcallparam, pChn->SessionNo, SendMsgBuf);
}
void SendCdrSerialNo(int nChn, int nChn1)
{
  if (pBoxchn->isnChnAvail(nChn) && pBoxchn->isnChnAvail(nChn1))
  {
    SendCdrSerialNo(nChn, pChn1->CdrSerialNo);
  }
}
void SendCdrSerialNo(const char *SerialNo, int nChnType, int nChnNo, const char *CdrSerialNo)
{
  sprintf(SendMsgBuf, "<onnewcallparam sessionid='%s' cmdaddr='%d' chantype='%d' channo='%d' callerno='' calledno='' workerno='' cdrserialno='%s' param=''/>", 
    SerialNo, 0, nChnType, nChnNo, CdrSerialNo);
  SendMsg2XML(MSG_onnewcallparam, atol(SerialNo), SendMsgBuf);
}
char *GetLeft0WorkerNo(int workerno)
{
  static char szWorkerNo[32];

  sprintf(szWorkerNo, "%d", workerno);
  if ((int)strlen(szWorkerNo) < pIVRCfg->MaxWorkerNoLen)
  {
    if (pIVRCfg->MaxWorkerNoLen == 2)
      sprintf(szWorkerNo, "%02d", workerno);
    else if (pIVRCfg->MaxWorkerNoLen == 3)
      sprintf(szWorkerNo, "%03d", workerno);
    else if (pIVRCfg->MaxWorkerNoLen == 4)
      sprintf(szWorkerNo, "%04d", workerno);
    else if (pIVRCfg->MaxWorkerNoLen == 5)
      sprintf(szWorkerNo, "%05d", workerno);
    else if (pIVRCfg->MaxWorkerNoLen == 6)
      sprintf(szWorkerNo, "%06d", workerno);
  }
  return szWorkerNo;
}
char *GetLeft0WorkerNo(const char *workerno)
{
  return GetLeft0WorkerNo(atoi(workerno));
}
//寫詳細通話記錄
void DBInsertCallCDR(int nChn, int nAnsId, int nCallType, const char *TranWorkerNo)
{
  char sqls[1024], CallerNo[64], CalledNo[64], szTemp[64], sqls2[1024];

  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }

  memset(CallerNo, 0, 64);
  memset(CalledNo, 0, 64);
  
  if (pChn->CallInOut == 1)
  {
    strncpy(CallerNo, pChn->CustPhone, 63);
    strncpy(CalledNo, pChn->CalledNo, 63);

    if (pIVRCfg->isDumpCRMRecord == true)
    {
      MyStringReplace(pIVRCfg->QuerySqls, "$S_CallerNo", CallerNo, 0, 8, 1024, sqls);
      MyStringReplace(pIVRCfg->InsertSqls, "$S_CallerNo", CallerNo, 0, 8, 1024, sqls2);
      SendDumpSql2DB(sqls, sqls2);
    }
    if (pIVRCfg->isQueryCustomerByWebservice == true && pTcpLink->IsGWConnected())
    {
      SendQueryCustomer2Webservice(CallerNo, CalledNo, pIVRCfg->QueryCustomerWebserviceAddr, pIVRCfg->QueryCustomerWebserviceParam);
    }
  }
  else
  {
    if (pIVRCfg->isCDRSwapCallerCalledNo == false)
    {
      strncpy(CallerNo, pChn->CustPhone, 63);
      strncpy(CalledNo, pChn->CallerNo, 63);
    }
    else
    {
      strncpy(CalledNo, pChn->CustPhone, 63);
      strncpy(CallerNo, pChn->CallerNo, 63);
    }
  }

  if (pIVRCfg->ProcLocaAreaCodeId == 2)
  {
    if (pChn->CallInOut == 1)
    {
      if (CallerNo[0] != '0' && CallerNo[0] != '9' && strlen(pIVRCfg->LocaAreaCode) > 0 && (int)strlen(CallerNo) == pIVRCfg->LocaTelCodeLen)
      {
        sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode, CallerNo);
        strcpy(CallerNo, szTemp);
      }
    }
    else if (pChn->CallInOut == 2)
    {
      CAgent *pAgent = GetAgentBynChn(nChn);
      if (pAgent)
      {
        if (CallerNo[0] != '0' && CallerNo[0] != '9' && pAgent->m_Seat.LocaAreaCode.GetLength() > 0 && (int)strlen(CallerNo) > 5)
        {
          sprintf(szTemp, "%s%s", pAgent->m_Seat.LocaAreaCode.C_Str(), CallerNo);
          strcpy(CallerNo, szTemp);
        }
      }
    }
  }
  if (pIVRCfg->isAutoAddTWMobilePreCode0 == true)
  {
    if (CallerNo[0] == '9' && (int)strlen(CallerNo) == 9)
    {
      sprintf(szTemp, "0%s", CallerNo);
      strcpy(CallerNo, szTemp);
    }
  }
  
  if (pIVRCfg->SP_InsertCDR == 0)
  {
    sprintf(sqls, "insert into tbCallcdr (SerialNo,ChnType,ChnNo,CallerNo,CalledNo,CallInOut,AnsId,CallType,TranWorkerNo,CallTime,AnsTime,AcdTime,WaitTime,RingTime,SeatRelTime,RelTime,ACWTime) values ('%s',%d,%d,'%s','%s',%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
      pChn->CdrSerialNo,
      pChn->lgChnType,
      pChn->lgChnNo,
      CallerNo, //pChn->CustPhone,
      CalledNo, //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo,
      pChn->CallInOut,
      nAnsId,
      nCallType,
      GetLeft0WorkerNo(TranWorkerNo),
      g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc);
  }
  else
  {
    if (g_nDBType == DB_MYSQL)
    sprintf(sqls, "CALL sp_InsertCDR('%s',%d,%d,'%s','%s',%d,%d,%d,'%s');",
      pChn->CdrSerialNo,
      pChn->lgChnType,
      pChn->lgChnNo,
      CallerNo, //pChn->CustPhone,
      CalledNo, //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo,
      pChn->CallInOut,
      nAnsId,
      nCallType,
      GetLeft0WorkerNo(TranWorkerNo));
    else if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "exec sp_InsertCDR '%s',%d,%d,'%s','%s',%d,%d,%d,'%s'",
      pChn->CdrSerialNo,
      pChn->lgChnType,
      pChn->lgChnNo,
      CallerNo, //pChn->CustPhone,
      CalledNo, //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo,
      pChn->CallInOut,
      nAnsId,
      nCallType,
      GetLeft0WorkerNo(TranWorkerNo));
    else
      sprintf(sqls, "insert into tbCallcdr (SerialNo,ChnType,ChnNo,CallerNo,CalledNo,CallInOut,AnsId,CallType,TranWorkerNo,CallTime,AnsTime,AcdTime,WaitTime,RingTime,SeatRelTime,RelTime,ACWTime) values ('%s',%d,%d,'%s','%s',%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
      pChn->CdrSerialNo,
      pChn->lgChnType,
      pChn->lgChnNo,
      CallerNo, //pChn->CustPhone,
      CalledNo, //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo,
      pChn->CallInOut,
      nAnsId,
      nCallType,
      GetLeft0WorkerNo(TranWorkerNo),
      g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc);
  }

  SendExecSql2DB(sqls);

  //2017-01-08增加交換機硬排隊在轉到分機前已等待的時長
  if (pChn->CallInOut == 1 && pChn->SWTACDTimeLen > 0)
  {
    sprintf(sqls, "update tbCallcdr set CallTime=dateadd(s,-%d,%s),AnsTime=dateadd(s,-%d,%s),AcdTime=dateadd(s,-%d,%s),WaitTime=dateadd(s,-%d,%s) where SerialNo='%s'", 
      pChn->SWTACDTimeLen, g_szSQLNowFunc, pChn->SWTACDTimeLen, g_szSQLNowFunc, pChn->SWTACDTimeLen, g_szSQLNowFunc, pChn->SWTACDTimeLen, g_szSQLNowFunc, pChn->CdrSerialNo);
    SendExecSql2DB(sqls);
  }

  if (pIVRCfg->isCDRInsertDeptNo == true)
  {
    int nDeptNo=0;
    if (pChn->CallInOut == 1)
    {
      if (pChn->lgChnType == 1 || pChn->lgChnType == 3)
      {
        nDeptNo = gIVRDeptMng.GetDeptNo(pChn->CalledNo);
      }
      else if (pChn->lgChnType == 2)
      {
        CAgent *pAgent = GetAgentBynChn(nChn);
        if (pAgent)
        {
          nDeptNo = atoi(pAgent->m_Worker.DepartmentNo.C_Str());
          if (nDeptNo == 0)
          {
            nDeptNo = atoi(pAgent->m_Seat.DepartmentNo.C_Str());
          }
        }
      }
    }
    else
    {
      if (pChn->lgChnType == 2)
      {
        CAgent *pAgent = GetAgentBynChn(nChn);
        if (pAgent)
        {
          nDeptNo = atoi(pAgent->m_Worker.DepartmentNo.C_Str());
          if (nDeptNo == 0)
          {
            nDeptNo = atoi(pAgent->m_Seat.DepartmentNo.C_Str());
          }
        }
      }
    }
    if (nDeptNo > 0)
    {
      sprintf(sqls, "update tbCallcdr set DepartmentNo='%d' where SerialNo='%s'", nDeptNo,  pChn->CdrSerialNo);
      SendExecSql2DB(sqls);
    }
  }
  if (pIVRCfg->isCDRInsertOrgCallNo == true)
  {
    if (pChn->OrgCallerNo.GetLength() > 0 || pChn->OrgCalledNo.GetLength() > 0)
    {
      sprintf(sqls, "update tbCallcdr set OrgCallerNo='%s',OrgCalledNo='%s' where SerialNo='%s'", 
        pChn->OrgCallerNo.C_Str(),  pChn->OrgCalledNo.C_Str(), pChn->CdrSerialNo);
      SendExecSql2DB(sqls);
    }
  }
  if (pIVRCfg->isCDRInsertCallUCID == true)
  {
    
    if (strlen(pChn->CallUCID) == 0)
    {
      strcpy(pChn->CallUCID, pChn->CdrSerialNo);
    }
    sprintf(sqls, "update tbCallcdr set callucid='%s' where SerialNo='%s'", pChn->CallUCID,  pChn->CdrSerialNo);
    SendExecSql2DB(sqls);
  }
}
void DBUpdateCallCDR_AnsId(int nChn, int nAnsId)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsId=%d where SerialNo='%s'",
    nAnsId,
    pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新主被叫號碼
void DBUpdateCallCDR_Phone(int nChn)
{
  char sqls[1024], CallerNo[64], CalledNo[64], szTemp[64];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  if (strlen(pChn->CustPhone) == 0)
  {
    return;
  }

  memset(CallerNo, 0, 64);
  memset(CalledNo, 0, 64);
  
  if (pChn->CallInOut == 1)
  {
    if (pIVRCfg->isOnlyRecSystem == true)
    {
      strncpy(CallerNo, pChn->CustPhone, 63);
      strncpy(CalledNo, pChn->CalledNo, 63);
    }
    else
    {
      return; //2014-12-05 如果是呼入的電話這里就直接返回，防止把通過IVR呼入的話單中的被叫號碼改了
    }
  }
  else
  {
    if (pIVRCfg->isCDRSwapCallerCalledNo == false)
    {
      strncpy(CallerNo, pChn->CustPhone, 63);
      strncpy(CalledNo, pChn->CallerNo, 63);
    }
    else
    {
      strncpy(CalledNo, pChn->CustPhone, 63);
      strncpy(CallerNo, pChn->CallerNo, 63);
    }
  }
  
  if (pIVRCfg->ProcLocaAreaCodeId == 2)
  {
    if (pChn->CallInOut == 1)
    {
      if (CallerNo[0] != '0' && CallerNo[0] != '9' && strlen(pIVRCfg->LocaAreaCode) > 0 && (int)strlen(CallerNo) == pIVRCfg->LocaTelCodeLen)
      {
        sprintf(szTemp, "%s%s", pIVRCfg->LocaAreaCode, CallerNo);
        strcpy(CallerNo, szTemp);
      }
    }
    else if (pChn->CallInOut == 2)
    {
      CAgent *pAgent = GetAgentBynChn(nChn);
      if (pAgent)
      {
        if (CallerNo[0] != '0' && CallerNo[0] != '9' && pAgent->m_Seat.LocaAreaCode.GetLength() > 0 && (int)strlen(CallerNo) > 5)
        {
          sprintf(szTemp, "%s%s", pAgent->m_Seat.LocaAreaCode.C_Str(), CallerNo);
          strcpy(CallerNo, szTemp);
        }
      }
    }
  }
  if (pIVRCfg->isAutoAddTWMobilePreCode0 == true)
  {
    if (CallerNo[0] == '9' && (int)strlen(CallerNo) == 9)
    {
      sprintf(szTemp, "0%s", CallerNo);
      strcpy(CallerNo, szTemp);
    }
  }

  if (pIVRCfg->SP_InsertCDR == 0)
  {
    sprintf(sqls, "update tbCallcdr set CallerNo='%s',CalledNo='%s' where SerialNo='%s'",
      CallerNo, //pChn->CustPhone,
      CalledNo, //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo,
      pChn->CdrSerialNo);
  }
  else
  {
    if (g_nDBType == DB_MYSQL)
    sprintf(sqls, "CALL sp_UpdateCDRPhone('%s','%s','%s');",
      pChn->CdrSerialNo,
      CallerNo, //pChn->CustPhone,
      CalledNo); //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo);
    else if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "exec sp_UpdateCDRPhone '%s','%s','%s'",
      pChn->CdrSerialNo,
      CallerNo, //pChn->CustPhone,
      CalledNo); //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo);
    else
      sprintf(sqls, "update tbCallcdr set CallerNo='%s',CalledNo='%s' where SerialNo='%s'",
      CallerNo, //pChn->CustPhone,
      CalledNo, //pChn->CallInOut == 2 ? pChn->CallerNo : pChn->CalledNo,
      pChn->CdrSerialNo);
  }
  //if (pChn->cdrSQLBuffer.CompareNoCase(sqls) == 0)
  //  return;
  SendExecSql2DB(sqls);
  //pChn->cdrSQLBuffer = sqls;
}
//更新呼叫方向(1-呼入 2-呼出 3-內部)
void DBUpdateCallCDR_CallInOut(int nChn, int nInOut)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set CallInOut=%d where SerialNo='%s'",
    nInOut,
    pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新呼入系統應答時間
void DBUpdateCallCDR_AnsTime(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsTime=%s,AcdTime=%s,WaitTime=%s,RingTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新等待分配坐席時間
void DBUpdateCallCDR_WaitTime(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0 || pChn->WritedCallCDR_WaitTimeId == 1)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsId=1,WaitTime=%s,AcdTime=%s,RingTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
  pChn->WritedCallCDR_WaitTimeId = 1;
}
//更新分配坐席時間
void DBUpdateCallCDR_ACDTime(int nChn, int nSrvType, int nSrvSubType)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsId=1,SeatAnsId=3,SrvType=%d,SrvSubType=%d,AcdTime=%s,RingTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    nSrvType,nSrvSubType,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_SrvType(int nChn, int nSrvType, int nSrvSubType)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set SrvType=%d,SrvSubType=%d where SerialNo='%s'",
    nSrvType,nSrvSubType, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//添加分配過的工號
void DBUpdateCallCDR_AnsWorkerNo(int nChn, int nAG)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  if (g_nDBType == DB_SQLSERVER)
    sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s' where SerialNo='%s'",
    pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
  else
    sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s' where SerialNo='%s'",
    pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
  
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_WorkerNo(int nChn, int nAG)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  if (pChn->CallInOut == 2)
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
  }
  else
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats=rtrim(RingSeats)+'%s,',RingWorkerNos=rtrim(RingWorkerNos)+'%s,' where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats=CONCAT(RingSeats,'%s,'),RingWorkerNos=CONCAT(RingWorkerNos,'%s,') where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
  }
  
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_WorkerNoGroupNo(int nChn, int nAG) //2016-07-21 增加呼出寫組號
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  if (pChn->CallInOut == 2)
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',GroupNo=%d,RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetGroupNo(), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
  }
  else
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',GroupNo=%d,RingSeats=rtrim(RingSeats)+'%s,',RingWorkerNos=rtrim(RingWorkerNos)+'%s,' where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetGroupNo(), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats=CONCAT(RingSeats,'%s,'),RingWorkerNos=CONCAT(RingWorkerNos,'%s,') where SerialNo='%s'",
      pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), pChn->CdrSerialNo);
  }
  
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_WorkerNo(int nChn, const char *SeatNo, const char *WorkerNo)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  if (pChn->CallInOut == 2)
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
  }
  else
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats=rtrim(RingSeats)+'%s,',RingWorkerNos=rtrim(RingWorkerNos)+'%s,' where SerialNo='%s'",
      SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatNo='%s',WorkerNo='%s',RingSeats=CONCAT(RingSeats,'%s,'),RingWorkerNos=CONCAT(RingWorkerNos,'%s,') where SerialNo='%s'",
      SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
  }
  
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_WorkerNo(int nChn, const char *SeatNo, const char *WorkerNo, int nSeatAnsId)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  if (pChn->CallInOut == 2)
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatAnsId=%d,SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      nSeatAnsId, SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatAnsId=%d,SeatNo='%s',WorkerNo='%s',RingSeats='%s',RingWorkerNos='%s' where SerialNo='%s'",
      nSeatAnsId, SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
  }
  else
  {
    if (g_nDBType == DB_SQLSERVER)
      sprintf(sqls, "update tbCallcdr set SeatAnsId=%d,SeatNo='%s',WorkerNo='%s',RingSeats=rtrim(RingSeats)+'%s,',RingWorkerNos=rtrim(RingWorkerNos)+'%s,' where SerialNo='%s'",
      nSeatAnsId, SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
    else
      sprintf(sqls, "update tbCallcdr set SeatAnsId=%d,SeatNo='%s',WorkerNo='%s',RingSeats=CONCAT(RingSeats,'%s,'),RingWorkerNos=CONCAT(RingWorkerNos,'%s,') where SerialNo='%s'",
      nSeatAnsId, SeatNo, WorkerNo, SeatNo, GetLeft0WorkerNo(WorkerNo), pChn->CdrSerialNo);
  }
  
  SendExecSql2DB(sqls);
}
//更新坐席振鈴時間
void DBUpdateCallCDR_RingTime(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  if (pChn->DBUpdateCallCDR_RingTimeId == 1)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsId=1,SeatAnsId=2,RingTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
  pChn->DBUpdateCallCDR_RingTimeId = 1;
}
//更新坐席應答時間
void DBUpdateCallCDR_SeatAnsTime(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsId=1,SeatAnsId=1,SeatAnsTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_SeatAnsTime(int nChn, const char *SeatNo, const char *WorkerNo)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set AnsId=1,SeatAnsId=1,SeatNo='%s',WorkerNo='%s',GroupNo=%d,DutyNo=%d,SeatAnsTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    SeatNo, GetLeft0WorkerNo(WorkerNo), 0, 0, g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_SeatAnsTime(int nAG, int AcdGroupNo)
{
  char sqls[1024];
  int nAcdGroupNo;
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pAG->CdrSerialNo) == 0)
  {
    return;
  }

  //2016-06-03 如果分配的組號為0，就用話務員所在第1組號
  if (AcdGroupNo == 0)
    nAcdGroupNo = pAG->GetAcdedGroupNo();
  else
    nAcdGroupNo = AcdGroupNo;

  sprintf(sqls, "update tbCallcdr set AnsId=1,SeatAnsId=1,SeatNo='%s',WorkerNo='%s',GroupNo=%d,DutyNo=%d,SeatAnsTime=%s,SeatRelTime=%s,RelTime=%s,ACWTime=%s where SerialNo='%s'",
    pAG->GetSeatNo().C_Str(), GetLeft0WorkerNo(pAG->GetWorkerNo()), nAcdGroupNo, pAG->m_Worker.DutyNo, g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc, pAG->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_SeatAnsId(int nChn, int seatansid)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set SeatAnsId=%d where SerialNo='%s'",
    seatansid, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新錄音文件名
void DBUpdateCallCDR_RecdFile(int nAG)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pAG->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set RecdRootPath='%s',RecdFile='%s' where SerialNo='%s'",
    pIVRCfg->SeatRecPath.C_Str(), pAG->RecordFileName, pAG->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_RecdFile(int nAG, const char *rootpath, const char *filename, const char *callid)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(callid) == 0)
  {
    return;
  }
  if (strlen(pAG->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set RecdRootPath='%s',RecdFile='%s',remark='%s' where SerialNo='%s' and len(remark)=0",
    rootpath, filename, callid, pAG->CdrSerialNo);
  SendExecSql2DB(sqls);
  
  //2016-02-12
  if (pIVRCfg->isInsertRecCallidList == true)
  {
    sprintf(sqls, "insert into tbRECCallIdList (SerialNo,CallId,RecdRootPath,RecdFile) values ('%s','%s','%s','%s')",
      pAG->CdrSerialNo, callid, rootpath, filename);
    SendExecSql2DB(sqls);
  }
}
//2015-05-30
void DBUpdateCallCDR_RecdFile(const char *serialno, const char *rootpath, const char *filename, const char *callid)
{
  char sqls[1024];

  sprintf(sqls, "update tbCallcdr set RecdRootPath='%s',RecdFile='%s',remark='%s' where SerialNo='%s' and len(remark)=0",
    rootpath, filename, callid, serialno);
  SendExecSql2DB(sqls);
  
  //2016-02-12
  if (pIVRCfg->isInsertRecCallidList == true)
  {
    sprintf(sqls, "insert into tbRECCallIdList (SerialNo,CallId,RecdRootPath,RecdFile) values ('%s','%s','%s','%s')",
      serialno, callid, rootpath, filename);
    SendExecSql2DB(sqls);
  }
}
void DBUpdateCallCDR_ChnRecdFile(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set RecdRootPath='%s',RecdFile='%s' where SerialNo='%s'",
    pIVRCfg->SeatRecPath.C_Str(), pChn->RecordFileName, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新坐席掛機時間
void DBUpdateCallCDR_SeatRelTime(int nAG)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pAG->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set SeatRelTime=%s,ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc, pAG->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_ChnSeatRelTime(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set SeatRelTime=%s,ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新外線掛機時間時間
void DBUpdateCallCDR_RelTime(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (pChn->WritedCallCDR_RelTimeId == 1)
  {
    return;
  }
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  if (pChn->CallInOut == 2 && (int)strlen(pChn->CalledNo) < pIVRCfg->MinCDRCalledNoLen && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    sprintf(sqls, "delete from tbCallcdr where SerialNo='%s'",
      pChn->CdrSerialNo);
    SendExecSql2DB(sqls);
    return;
  }
  DBUpdateCallCDR_Phone(nChn);

  pChn->WritedCallCDR_RelTimeId = 1;

  if (pChn->lgChnType == 2)
  sprintf(sqls, "update tbCallcdr set RelTime=%s,SeatRelTime=%s,ACWTime=%s,CallType=%d,UpdateFlag=1 where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,pChn->nCallType,pChn->CdrSerialNo);
  else
  sprintf(sqls, "update tbCallcdr set RelTime=%s,CallType=%d,UpdateFlag=1 where SerialNo='%s'",
    g_szSQLNowFunc,pChn->nCallType,pChn->CdrSerialNo);

  SendExecSql2DB(sqls);

  if (pIVRCfg->SP_UpDateAreaName == 1)
  {
    if (g_nDBType == DB_MYSQL)
    {
      sprintf(sqls, "CALL sp_SeachAreaName('%s','%s','tbCallCDR');", pChn->CdrSerialNo, pChn->CustPhone);
      SendExecSql2DB(sqls);
    }
    else if (g_nDBType == DB_SQLSERVER)
    {
      sprintf(sqls, "exec sp_SeachAreaName '%s','%s','tbCallCDR'", pChn->CdrSerialNo, pChn->CustPhone);
      SendExecSql2DB(sqls);
    }
  }
  
  memset(pChn->CdrSerialNo, 0, MAX_CHAR64_LEN);
}
//通過中繼監錄通道的掛機時間來寫通話結束時間 2014-05-20
void DBUpdateCallCDR_RelTimeByRecCh(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  MyTrace(3, "DBUpdateCallCDR_RelTimeByRecCh nChn=%d pChn->CdrSerialNo=%s",
    nChn, pChn->CdrSerialNo);

  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set RelTime=%s,SeatRelTime=%s,ACWTime=%s,UpdateFlag=1 where SerialNo='%s'",
    g_szSQLNowFunc,g_szSQLNowFunc,g_szSQLNowFunc,pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
  
  if (pIVRCfg->SP_UpDateAreaName == 1)
  {
    if (g_nDBType == DB_MYSQL)
    {
      sprintf(sqls, "CALL sp_SeachAreaName('%s','');", pChn->CdrSerialNo);
      SendExecSql2DB(sqls);
    }
    else if (g_nDBType == DB_SQLSERVER)
    {
      sprintf(sqls, "exec sp_SeachAreaName '%s',''", pChn->CdrSerialNo);
      SendExecSql2DB(sqls);
    }
  }
  
  memset(pChn->CdrSerialNo, 0, MAX_CHAR64_LEN);
}
//釋放結束原因nRelReason: 0-正常通話后釋放,1-外線放棄,2-轉接
void DBUpdateCallCDR_RelReason(int nChn, int nRelReason)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set RelReason=%d where SerialNo='%s'",
    nRelReason,pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//修改話單完成更新標志
void DBUpdateCallCDR_UpdateFlag(int nChn)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  sprintf(sqls, "update tbCallcdr set UpdateFlag=1 where SerialNo='%s'",
    pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
//更新坐席話后處理完成時間
void DBUpdateCallCDR_ACWTime(int nAG)
{
  char sqls[1024];
  
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (strlen(pAG->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set ACWTime=%s where SerialNo='%s'",
    g_szSQLNowFunc, pAG->CdrSerialNo);
  SendExecSql2DB(sqls);
  memset(pAG->CdrSerialNo, 0, MAX_CHAR64_LEN);
}
void DBUpdateCallCDR_DialOutID(int nChn, const char *dialid)
{
  char sqls[1024];
  
  if (pIVRCfg->isUpdateDialOutId == false)
  {
    return;
  }
  if (strlen(dialid) == 0)
  {
    return;
  }
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set DialId='%s' where SerialNo='%s'",
    dialid, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBUpdateCallCDR_CRMCallID(int nChn, const char *callid)
{
  char sqls[1024];
  
  if (pIVRCfg->isUpdateCRMCallId == false)
  {
    return;
  }
  if (strlen(callid) == 0)
  {
    return;
  }
  if (pIVRCfg->ControlCallModal == 0)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) == 0)
  {
    return;
  }
  
  sprintf(sqls, "update tbCallcdr set CRMCallId='%s' where SerialNo='%s'",
    callid, pChn->CdrSerialNo);
  SendExecSql2DB(sqls);
}
void DBInsertIVRDTMFTrace(int nChn, int flag, int nAG)
{
  char sqls[1024];
  
  if (pIVRCfg->isInsertIVRDTMFTraceId == false)
  {
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  
  if (pChn->CallInOut == 2)
    sprintf(sqls, "insert into tbivrdtmftrace (IVRNodeGId,CallerNo,TraceTime,TraceMENU,TraceDTMF,CDRSerailNo,Flag,SeatNo,WorkerNo) VALUES ('0','%s',%s,'','','%s',%d,'%s','%d')",
      pChn->CalledNo, g_szSQLNowFunc, pChn->CdrSerialNo, flag, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo());
  else
    sprintf(sqls, "insert into tbivrdtmftrace (IVRNodeGId,CallerNo,TraceTime,TraceMENU,TraceDTMF,CDRSerailNo,Flag,SeatNo,WorkerNo) VALUES ('0','%s',%s,'','','%s',%d,'%s','%d')",
    pChn->CallerNo, g_szSQLNowFunc, pChn->CdrSerialNo, flag, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo());
  SendExecSql2DB(sqls);
}
void DBInsertCallTrace(int nChn, const char *status)
{
  char sqls[1024];

  if (pIVRCfg->isInsertCallTrace == false)
    return;
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  
  sprintf(sqls, "exec sp_CallTrace '%s','%s','%s','%s','%s'",
    pChn->CdrSerialNo, pChn->CallerNo, pChn->CalledNo, pChn->OrgCalledNo.C_Str(), status);
  SendExecSql2DB(sqls);
}
//-----------------------------------------------------------------------------
//修改人工服務呼出結果
bool GetDialOutId(const char *dialparam, int nChn)
{
  char szTemp[256];
  CStringX ArrString[2];

  if (strncmp(dialparam, "DIALOUT:", 8) == 0)
  {
    strcpy(szTemp, &dialparam[8]);
    int num = SplitString(szTemp, ';', 2, ArrString);
    if (num == 1)
    {
      strcpy(pChn->ManDialOutId, ArrString[0].C_Str());
      return true;
    }
    else if (num == 2)
    {
      strcpy(pChn->ManDialOutId, ArrString[0].C_Str());
      strcpy(pChn->ManDialOutListId, ArrString[1].C_Str());
      return true;
    }
  }
  return false;
}
void InsertDialOutListRecord(const char *diallistid, const char *calledno, const char *workerno)
{
  char sqls[256];

  sprintf(sqls, "insert into tbdialoutlist (DialListId,DialId,CalledNo,CallTime,CallResult,TalkWorkerNo) values ('%s','0','%s',%s,80,'%s')",
    diallistid, calledno, g_szSQLNowFunc, workerno);
  SendExecSql2DB(sqls);
}
void UpdateDialOutTime(const char *dialid, const char *diallistid)
{
  char sqls[256];

  if (strlen(dialid) > 0)
  {
    sprintf(sqls, "update tbdialouttele set LastCallTime=%s,CalledTimes=CalledTimes+1,LastResult=80 where %s='%s'",
      g_szSQLNowFunc, pIVRCfg->UpdateDialOutIdFieldName, dialid);
    SendExecSql2DB(sqls);
  }

  if (strlen(diallistid) > 0)
  {
    sprintf(sqls, "update tbdialoutlist set CallTime=%s,CallResult=80 where DialListId='%s'",
      g_szSQLNowFunc, diallistid);
    SendExecSql2DB(sqls);
  }
}
void UpdateDialOutSerialNo(const char *dialid, const char *diallistid, const char *serialno)
{
  char sqls[256];
  
  if (strlen(dialid) > 0)
  {
    sprintf(sqls, "update tbdialouttele set SerialNo='%s' where %s='%s'",
      serialno, pIVRCfg->UpdateDialOutIdFieldName, dialid);
    SendExecSql2DB(sqls);
  }

  if (strlen(diallistid) > 0)
  {
    sprintf(sqls, "update tbdialoutlist set SerialNo='%s' where DialListId='%s'",
      serialno, diallistid);
    SendExecSql2DB(sqls);
  }
}
void UpdateDialOutRingTime(const char *dialid, const char *diallistid)
{
  char sqls[256];
  
  if (strlen(dialid) > 0)
  {
    sprintf(sqls, "update tbdialouttele set LastResult=81 where %s='%s'",
      pIVRCfg->UpdateDialOutIdFieldName, dialid);
    SendExecSql2DB(sqls);
  }
  
  if (strlen(diallistid) > 0)
  {
    sprintf(sqls, "update tbdialoutlist set CallResult=81 where DialListId='%s'",
      diallistid);
    SendExecSql2DB(sqls);
  }
}
void UpdateDialOutFailTime(const char *dialid, const char *diallistid, int dialreault)
{
  char sqls[256];
  
  if (strlen(dialid) > 0)
  {
    sprintf(sqls, "update tbdialouttele set %s=%s,LastResult=%d where %s='%s'",
      pIVRCfg->UpdateDialOutRelFieldName, g_szSQLNowFunc, dialreault, pIVRCfg->UpdateDialOutIdFieldName, dialid);
    SendExecSql2DB(sqls);
  }
  
  if (strlen(diallistid) > 0)
  {
    sprintf(sqls, "update tbdialoutlist set RelTime=%s,CallResult=%d where DialListId='%s'",
      g_szSQLNowFunc, dialreault, diallistid);
    SendExecSql2DB(sqls);
  }
}
void UpdateDialOutAnsTime(const char *dialid, const char *diallistid)
{
  char sqls[256];
  
  if (strlen(dialid) > 0)
  {
    sprintf(sqls, "update tbdialouttele set AnsTime=%s,AnsTimes=AnsTimes+1,LastResult=86 where %s='%s'",
      g_szSQLNowFunc, pIVRCfg->UpdateDialOutIdFieldName, dialid);
    SendExecSql2DB(sqls);
  }
  
  if (strlen(diallistid) > 0)
  {
    sprintf(sqls, "update tbdialoutlist set AnsTime=%s,CallResult=86 where DialListId='%s'",
      g_szSQLNowFunc, diallistid);
    SendExecSql2DB(sqls);
  }
}
void UpdateDialOutHangonTime(const char *dialid, const char *diallistid)
{
  char sqls[256];
  
  if (strlen(dialid) > 0)
  {
    sprintf(sqls, "update tbdialouttele set LastResult=86,%s=%s where %s='%s'",
      pIVRCfg->UpdateDialOutRelFieldName, g_szSQLNowFunc, pIVRCfg->UpdateDialOutIdFieldName, dialid);
    SendExecSql2DB(sqls);
  }
  
  if (strlen(diallistid) > 0)
  {
    sprintf(sqls, "update tbdialoutlist set CallResult=86,RelTime=%s where DialListId='%s'",
      g_szSQLNowFunc, diallistid);
    SendExecSql2DB(sqls);
  }
}
bool GetCRMCallId(const char *dialparam, int nChn)
{
  if (strncmp(dialparam, "CRMCALLID:", 10) == 0)
  {
    strncpy(pChn->CRMCallId, &dialparam[10], 63);
    return true;
  }
  else if (strncmp(dialparam, "CRMCALLID%3A", 12) == 0)
  {
    strncpy(pChn->CRMCallId, &dialparam[12], 63);
    return true;
  }
  return false;
}
//-----------------------------------------------------------------------------
void SendIVRCallCountData(CIVRCallCountParam *pIVRCallCountParam)
{
  if (pIVRCfg->isUpdateGroupSumDataToDB == false)
    return;
  if (pIVRCallCountParam == NULL)
    return;
  char szCountData[512];
  char sqls[1024];

  sprintf(szCountData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", //2016-01-07
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInCount, //IVR呼入總次數
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInAnsCount, //IVR應答總次數
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInTranAGCount, //IVR轉座席總次數
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInTranAGAnsCount, //IVR轉座席後應答總次數
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInTranAGAbandonCount, //IVR轉座席失敗或放棄總次數
  
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInWaitTimeMaxLen, //IVR轉座席最大等待時長
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInWaitTimeMinLen, //IVR轉座席最小等待時長
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInWaitTimeAvgLen, //IVR轉座席平均等待時長

    gIVRCallCountParamMng.IVRCallCountParam[0].IvrChnBusyCount, //本群組總IVR線路占用總數
    gIVRCallCountParamMng.IVRCallCountParam[0].IvrChnInBusyCount, //本群組呼入IVR線路占用數
    gIVRCallCountParamMng.IVRCallCountParam[0].IvrChnOutBusyCount, //本群組呼出IVR線路占用數
    
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRInAnswerRate //IVR轉座席接通率 //2016-01-07
    );
  sprintf(sqls, "exec sp_UpdateIVRSumData '%s','%s','%s'", 
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRNo,
    gIVRCallCountParamMng.IVRCallCountParam[0].IVRName,
    szCountData);
  SendExecSql2DB(sqls);

  sprintf(szCountData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", //2016-01-07
    pIVRCallCountParam->IVRInCount, //IVR呼入總次數
    pIVRCallCountParam->IVRInAnsCount, //IVR應答總次數
    pIVRCallCountParam->IVRInTranAGCount, //IVR轉座席總次數
    pIVRCallCountParam->IVRInTranAGAnsCount, //IVR轉座席後應答總次數
    pIVRCallCountParam->IVRInTranAGAbandonCount, //IVR轉座席失敗或放棄總次數
    
    pIVRCallCountParam->IVRInWaitTimeMaxLen, //IVR轉座席最大等待時長
    pIVRCallCountParam->IVRInWaitTimeMinLen, //IVR轉座席最小等待時長
    pIVRCallCountParam->IVRInWaitTimeAvgLen, //IVR轉座席平均等待時長
    
    pIVRCallCountParam->IvrChnBusyCount, //本群組總IVR線路占用總數
    pIVRCallCountParam->IvrChnInBusyCount, //本群組呼入IVR線路占用數
    pIVRCallCountParam->IvrChnOutBusyCount, //本群組呼出IVR線路占用數

    pIVRCallCountParam->IVRInAnswerRate //IVR轉座席接通率 //2016-01-07
    );
  sprintf(sqls, "exec sp_UpdateIVRSumData '%s','%s','%s'", 
    pIVRCallCountParam->IVRNo,
    pIVRCallCountParam->IVRName,
    szCountData);
  SendExecSql2DB(sqls);
}
void SendAllIVRCallCountData()
{
  if (pIVRCfg->isUpdateGroupSumDataToDB == false)
    return;
  char szCountData[512];
  char sqls[1024];

  for (int i=0; i<gIVRCallCountParamMng.MaxIvrNum; i++)
  {
    sprintf(szCountData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", //2016-01-07
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInCount, //IVR呼入總次數
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInAnsCount, //IVR應答總次數
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInTranAGCount, //IVR轉座席總次數
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInTranAGAnsCount, //IVR轉座席後應答總次數
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInTranAGAbandonCount, //IVR轉座席失敗或放棄總次數
      
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInWaitTimeMaxLen, //IVR轉座席最大等待時長
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInWaitTimeMinLen, //IVR轉座席最小等待時長
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInWaitTimeAvgLen, //IVR轉座席平均等待時長

      gIVRCallCountParamMng.IVRCallCountParam[i].IvrChnBusyCount, //本群組總IVR線路占用總數
      gIVRCallCountParamMng.IVRCallCountParam[i].IvrChnInBusyCount, //本群組呼入IVR線路占用數
      gIVRCallCountParamMng.IVRCallCountParam[i].IvrChnOutBusyCount, //本群組呼出IVR線路占用數

      gIVRCallCountParamMng.IVRCallCountParam[i].IVRInAnswerRate //IVR轉座席接通率 //2016-01-07
      );
    sprintf(sqls, "exec sp_UpdateIVRSumData '%s','%s','%s'", 
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRNo,
      gIVRCallCountParamMng.IVRCallCountParam[i].IVRName,
      szCountData);
    SendExecSql2DB(sqls);
  }
}

