//---------------------------------------------------------------------------
#include "stdafx.h"
#include "functime.h"
//---------------------------------------------------------------------------
//---------------日期時間函數------------------------------------------------
int dtStr2time_t(const char *datetimestr, time_t tDateTime)
{
  //有效格式：YYYY-MM-DD HH:MI:SS
  struct tm local;
  int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
  bool IsLeap;
  CH szTemp[20];

  if (strlen(datetimestr) != 19) 
    return 1;
  if (datetimestr[4] != '-' || datetimestr[7] != '-' || datetimestr[10] != ' ' 
    || datetimestr[13] != ':' || datetimestr[16] != ':')
    return 1;
  //分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
    tm_year = atoi(szTemp);
    IsLeap = IsLeapYear(tm_year);
  }
  else
  {
    return 1;
  }
  //分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mon = atoi(szTemp);
    if (tm_mon < 1 || tm_mon > 12)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mday = atoi(szTemp);
    if (tm_mday < 1) return 1;
    if (tm_mon == 1 || tm_mon == 3 || tm_mon == 5 || tm_mon == 7 || tm_mon == 8 || tm_mon == 10 || tm_mon == 12)
    {
      if (tm_mday > 31) return 1;
    }
    else if (tm_mon == 2)
    {
      if (IsLeap == true)
      {
        if (tm_mday > 29) return 1;
      }
      else
      {
        if (tm_mday > 28) return 1;
      }
    }
    else if (tm_mon == 4 || tm_mon == 6 || tm_mon == 9 || tm_mon == 11)
    {
      if (tm_mday > 30) return 1;
    }
  }
  else
  {
    return 1;
  }
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_hour = atoi(szTemp);
    if (tm_hour < 0 || tm_hour > 23)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_min = atoi(szTemp);
    if (tm_min < 0 || tm_min > 59)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_sec = atoi(szTemp);
    if (tm_sec < 0 || tm_sec > 59)
      return 1;
  }
  else
  {
    return 1;
  }
  try
  {
    local.tm_year = tm_year-1900;
    local.tm_mon = tm_mon-1;
    local.tm_mday = tm_mday;
    local.tm_hour = tm_hour;
    local.tm_min = tm_min;
    local.tm_sec = tm_sec;
    tDateTime = mktime(&local);
  }
  catch (...)
  {
    return 1;
  }

  return 0;
}
int dtStr2tm(const char *datetimestr, struct tm *tDateTime)
{
  //有效格式：YYYY-MM-DD HH:MI:SS
  int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
  bool IsLeap;
  CH szTemp[20];

  if (strlen(datetimestr) != 19) 
    return 1;
  if (datetimestr[4] != '-' || datetimestr[7] != '-' || datetimestr[10] != ' ' 
    || datetimestr[13] != ':' || datetimestr[16] != ':')
    return 1;
  //分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
    tm_year = atoi(szTemp);
    IsLeap = IsLeapYear(tm_year);
  }
  else
  {
    return 1;
  }
  //分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mon = atoi(szTemp);
    if (tm_mon < 1 || tm_mon > 12)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mday = atoi(szTemp);
    if (tm_mday < 1) return 1;
    if (tm_mon == 1 || tm_mon == 3 || tm_mon == 5 || tm_mon == 7 || tm_mon == 8 || tm_mon == 10 || tm_mon == 12)
    {
      if (tm_mday > 31) return 1;
    }
    else if (tm_mon == 2)
    {
      if (IsLeap == true)
      {
        if (tm_mday > 29) return 1;
      }
      else
      {
        if (tm_mday > 28) return 1;
      }
    }
    else if (tm_mon == 4 || tm_mon == 6 || tm_mon == 9 || tm_mon == 11)
    {
      if (tm_mday > 30) return 1;
    }
  }
  else
  {
    return 1;
  }
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_hour = atoi(szTemp);
    if (tm_hour < 0 || tm_hour > 23)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_min = atoi(szTemp);
    if (tm_min < 0 || tm_min > 59)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_sec = atoi(szTemp);
    if (tm_sec < 0 || tm_sec > 59)
      return 1;
  }
  else
  {
    return 1;
  }
  try
  {
    tDateTime->tm_year = tm_year-1900;
    tDateTime->tm_mon = tm_mon-1;
    tDateTime->tm_mday = tm_mday;
    tDateTime->tm_hour = tm_hour;
    tDateTime->tm_min = tm_min;
    tDateTime->tm_sec = tm_sec;
  }
  catch (...)
  {
    return 1;
  }

  return 0;
}
CH *time_t2str(time_t lt)
{
	static CH nowtime[25];
  struct tm *local;
  local=localtime(&lt);

	if (local == NULL)
    return "";
  sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;
}
CH *time_t2HourMinstr(time_t lt)
{
  static CH nowtime[25];
  struct tm *local;
  local=localtime(&lt);
  
  if (local == NULL)
    return "";
  sprintf(nowtime, "%02d:%02d:%02d", local->tm_hour, local->tm_min, local->tm_sec);
  return nowtime;
}
CH *time_t2HourMinstr1(time_t lt)
{
  static CH nowtime[25];
  struct tm *local;
  local=localtime(&lt);
  
  if (local == NULL)
    return "";
  sprintf(nowtime, "%02d:%02d:%02d", local->tm_hour, local->tm_min, local->tm_sec);
  return nowtime;
}
CH *CTime2str(CTime tm)
{
	static CH nowtime[25];

	sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", tm.GetYear()+1900, tm.GetMonth(), tm.GetDay(), tm.GetHour(), tm.GetMinute(), tm.GetSecond());
	return nowtime;
}
//取當前日期時間(yyyy-mm-dd hh-mi-ss)
CH *MyGetNow(void)
{
	static CH nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;
}
CH *MyGetToday(void)
{
  static CH nowtime[20];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  sprintf(nowtime, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
  return nowtime;
}
CH *MyGetNowEx(void)
{
  static CH nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  sprintf(nowtime, "%04d/%02d/%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
  return nowtime;
}
//取當前時間字符串
CH *GetNowHourStr() 
{
	static CH nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(nowtime, "%04d%02d%02d%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour);
	return nowtime;
}
UC MyGetNow( CH *Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(Result, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
  return 0;
}
//取當前日期(yyyy-mm-dd)
UC MyGetDate( CH *Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(Result, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
  return 0;
}
//取日期(yyyy-mm-dd)
UC MyGetDate( const CH *vDateTime, CH *Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    sprintf( Result, "%04d-%02d-%02d", sysTime.tm_year+1900, sysTime.tm_mon+1, sysTime.tm_mday);
    return 0;
  }
  return 1;
}
//取當前時間(yyyy-mm-dd)
UC MyGetTime( CH *Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(Result, "%02d:%02d:%02d", local->tm_hour, local->tm_min, local->tm_sec);
  return 0;
}
//取當前時間串(hhmmss)
CH *MyGetTimeStr( void )
{
  static CH nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

  sprintf(nowtime, "%02d%02d%02d", local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;  
}
//取當前日期時間串(yyyymmddhhmmss)
CH *MyGetNowStr( void )
{
  static CH nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(nowtime, "%04d%02d%02d%02d%02d%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;
}
//取時間(yyyy-mm-dd)
UC MyGetTime( const CH *vDateTime, CH *Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    sprintf( Result, "%02d:%02d:%02d", sysTime.tm_hour, sysTime.tm_min, sysTime.tm_sec);
    return 0;
  }
  return 1;
}
//取當前年(yyyy)
UC MyGetYear( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_year+1900;
  return 0;
}
//取年(yyyy)
UC MyGetYear( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_year;
    return 0;
  }
  return 1;
}
//取當前月(mm)
UC MyGetMonth( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_mon+1;
  return 0;
}
//取月(mm)
UC MyGetMonth( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_mon;
    return 0;
  }
  return 1;
}
//取當前日(dd)
UC MyGetDay( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_mday;
  return 0;
}
//取日(dd)
UC MyGetDay( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_mday;
    return 0;
  }
  return 1;
}
//取當前時(hh)
UC MyGetHour( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_hour;
  return 0;
}
//取時(hh)
UC MyGetHour( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_hour;
    return 0;
  }
  return 1;
}
//取當前分(mi)
UC MyGetMinute( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_min;
  return 0;
}
//取分(mi)
UC MyGetMinute( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_min;
    return 0;
  }
  return 1;
}
//取當前秒(ss)
UC MyGetSecond( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_sec;
  return 0;
}
//取秒(ss)
UC MyGetSecond( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_sec;
    return 0;
  }
  return 1;
}
//取當前毫秒(ms)
UC MyGetMsecond( US &Result )
{
  Result=0;
  return 0;
}
//取毫秒(ms)
UC MyGetMsecond( const CH *vDateTime, US &Result )
{
  return 0;
}
//取當前星期(ww)
UC MyGetWeekDay( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_wday;
  return 0;
}
//取星期(ww)
UC MyGetWeekDay( const CH *vDateTime, US &Result )
{
  tm sysTime;
  if (dtStr2tm(vDateTime, &sysTime) == 0)
  {
    Result = sysTime.tm_wday;
    return 0;
  }
  return 1;
}
//日期時間合法性判斷
UC MyIsDateTime( const CH *vDateTime )
{
  tm sysTime;
  return dtStr2tm(vDateTime, &sysTime);
}
//取日期時間間隔
UC MyDTBetween( const CH *vDateTime1, const CH *vDateTime2, const UC vType, int &Result )
{
    long sec = 0;
    time_t dt1, dt2;

    dt1 = time(NULL);
    dt2 = time(NULL);

    if (dtStr2time_t(vDateTime1, dt1) != 0)
      return 1;
    if (dtStr2time_t(vDateTime2, dt2) != 0)
      return 1;
    sec = (long)difftime(dt1, dt2);
    switch (vType)
    {
        case 1: //year
            Result = sec / 946080000;
            break;
        case 2: //month
            Result = sec / 2592000;
            break;
        case 3: //day
            Result = sec / 86400;
            break;
        case 4: //hour
            Result = sec / 3600;
            break;
        case 5: //minute
            Result = sec / 60;
            break;
        case 6: //second
            Result = sec;
            break;
        case 7: //msecond
            Result = sec * 1000;
            break;
        case 8: //week
            Result = sec / 604800;
            break;
        default:
            return 1;
    }
    return 0;
}
//取日期時間增量
UC MyIncDateTime( const CH *vDateTime, const UC vType, const int vNumber, CH *Result )
{
 //   UC result = 0;
    struct tm *local;
    time_t dt;
    dt = time(NULL);

    if (dtStr2time_t(vDateTime, dt) != 0)
      return 1;
    
    switch (vType)
    {
        case 1: //year
            dt = dt + vNumber * 946080000;
            break;
        case 2: //month
            dt = dt + vNumber * 2592000;
            break;
        case 3: //day
            dt = dt + vNumber * 86400;
            break;
        case 4: //hour
            dt = dt + vNumber * 3600;
            break;
        case 5: //minute
            dt = dt + vNumber * 60;
            break;
        case 6: //second
            dt = dt + vNumber;
            break;
        case 7: //msecond
            dt = dt + vNumber * 1000;
            break;
        case 8: //week
            dt = dt + vNumber * 604800;
            break;
    }
    local = localtime(&dt);
    sprintf(Result, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
    return 0;
}
CH *MyIncDays(const CH *vDate, int nDays)
{
  static char szTemp[20];
  struct tm *local;
  time_t dt;
  dt = time(NULL);

  memset(szTemp, 0, 20);
  sprintf(szTemp, "%s 00:00:00", vDate);
  
  if (dtStr2time_t(szTemp, dt) != 0)
  {
    memset(szTemp, 0, 20);
    strcpy(szTemp, vDate);
    return szTemp;
  }
  dt = dt + nDays * 86400;
  local = localtime(&dt);
  sprintf(szTemp, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
  return szTemp;
}
//日期時間范圍判斷(返回值：0-時間vDateTime在vDateTime1與vDateTime2范圍內)
UC MyInDateTime( const CH *vDateTime, const CH *vDateTime1, const CH *vDateTime2 )
{
   // long sec = 0;
    time_t dt, dt1, dt2;

    dt = time(NULL);
    dt1 = time(NULL);
    dt2 = time(NULL);

    if (dtStr2time_t(vDateTime, dt) != 0)
      return 2;

    if (dtStr2time_t(vDateTime1, dt1) != 0)
      return 2;

    if (dtStr2time_t(vDateTime2, dt2) != 0)
      return 2;

    if (dt >= dt1 && dt <= dt2)
      return 0;
    else
      return 1;
}
//啟動定時器
UC MyTimer( const UC vTimerId, const US vTimeLen )
{
    return 0;
}
//判斷閏年
bool IsLeapYear(US year)
{
  if ((year%4) == 0 && ((year%100) != 0 || (year%400) == 0))
    return true;
  else
    return false;
}
//某月有多少天
US DaysInAMonth(US year, US month)
{
  if (month == 2)
  {
    if ((year%4) == 0 && ((year%100) != 0 || (year%400) == 0))
    {
      return 29;
    }
    else
    {
      return 28;
    }
  }
  else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
  {
    return 31;
  }
  else if (month == 4 || month == 6 || month == 9 || month == 11)
  {
    return 30;
  }
  return 0;
}
//某年有多少天
US DaysInAYear(US year)
{
  if ((year%4) == 0 && ((year%100) != 0 || (year%400) == 0))
    return 366;
  else
    return 365;
}
