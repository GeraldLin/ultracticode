//---------------------------------------------------------------------------
/*
钡Μ秈钉
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "rmsgfifo.h"

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
//---------------------------------------------------------------------------
unsigned short CMsgfifo::Write(US ClientId, US MsgId, const char *MsgBuf, US MsgLen)         
{
  m_cCriticalSection.Lock();
	if(IsFull()) //full
  {
    m_cCriticalSection.Unlock();
		return 0;
  }

  pMsgs[Tail].ClientId = ClientId;
  pMsgs[Tail].MsgId = MsgId;
  if (MsgLen >= 4096)
    pMsgs[Tail].MsgLen = 4095;
  else
    pMsgs[Tail].MsgLen = MsgLen;
  memset(pMsgs[Tail].MsgBuf, 0, 4096);
  memcpy(pMsgs[Tail].MsgBuf, MsgBuf, pMsgs[Tail].MsgLen);
  pMsgs[Tail].MsgBuf[pMsgs[Tail].MsgLen] = 0;

	if(++Tail >= Len)
    Tail=0;

  m_cCriticalSection.Unlock();
	return 1;
}

unsigned short CMsgfifo::Read(US &ClientId, US &MsgId, char *MsgBuf, US &MsgLen)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
    return 0;//Buf Null
  }
	
	ClientId = pMsgs[Head].ClientId;
	MsgId = pMsgs[Head].MsgId;
  MsgLen = pMsgs[Head].MsgLen;
  memcpy(MsgBuf, pMsgs[Head].MsgBuf, MsgLen);
  MsgBuf[MsgLen] = 0;

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}
unsigned short CMsgfifo::Read(VXML_RECV_MSG_STRUCT *RecvMsg)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
    return 0;//Buf Null
  }
	
  RecvMsg->ClientId = pMsgs[Head].ClientId;
	RecvMsg->MsgId = pMsgs[Head].MsgId;
  RecvMsg->MsgLen = pMsgs[Head].MsgLen;
  memcpy(RecvMsg->MsgBuf, pMsgs[Head].MsgBuf, pMsgs[Head].MsgLen+1);

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}

//秆猂XML才﹃
int CMsgfifo::ParseXMLstr(const VXML_SEND_MSG_CMD_RULE_STRUCT *MsgRule, short MsgId, CH *XMLstr, VXML_CMD_ATTRIBUTE_STRUCT *CmdAttr)
{
  CH *pos1, *pos2, AttrName[50], AttrValue[MAX_VAR_DATA_LEN];
  int i, xmllen, len;
  
  CmdAttr->AttrNums = 0;
  if (MsgId >= MAX_XMLIVRMSG_NUM)
  {
    //絪腹禬璖瞅
    return 1;
  }
  xmllen = strlen(XMLstr);

  sprintf(AttrName, "<%s ", MsgRule[MsgId].MsgNameEng);
  pos1 = strstr(XMLstr, AttrName);
  if (pos1 == NULL || pos1 != XMLstr)
  {
    //嘿ぃ才
    MyTrace(0, "MsgId = '%d' but MsgName is not '%s' error\n", MsgId, MsgRule[MsgId].MsgNameEng);
    return 1;
  }
  CmdAttr->MsgId = MsgId;
  memset(CmdAttr->CmdName, 0, MAX_CMD_LABEL_LEN);
  strcpy(CmdAttr->CmdName, MsgRule[MsgId].MsgNameEng);
  CmdAttr->AttrNums = MsgRule[MsgId].AttrNum;
  for (i = 0; i < MsgRule[MsgId].AttrNum; i ++)
  {
    strcpy(CmdAttr->AttrName[i], MsgRule[MsgId].Attribute[i].AttrNameEng);
    strcpy(CmdAttr->AttrValue[i], MsgRule[MsgId].Attribute[i].Default);

    sprintf(AttrName, " %s=\"", MsgRule[MsgId].Attribute[i].AttrNameEng);
    pos1 = strstr(XMLstr, AttrName);
    if (pos1 == NULL)
    {
      sprintf(AttrName, " %s='", MsgRule[MsgId].Attribute[i].AttrNameEng);
      pos1 = strstr(XMLstr, AttrName);
    }
    if (pos1 != NULL)
    {
      pos1 = pos1 + strlen(AttrName);
      if (pos1 > XMLstr + xmllen)
      {
        continue;
      }
        
      pos2 = strstr(pos1, "\" ");
      if (pos2 == NULL)
      {
        pos2 = strstr(pos1, "' ");
      }
      if (pos2 == NULL)
      {
        pos2 = strstr(pos1, "\"/>");
      }
      if (pos2 == NULL)
      {
        pos2 = strstr(pos1, "'/>");
      }
      if (pos2 != NULL)
      {
        len = pos2 - pos1;
        if (len >= MAX_VAR_DATA_LEN)
        {
          len = MAX_VAR_DATA_LEN-1;
        }
        memset(AttrValue, 0, MAX_VAR_DATA_LEN);
        strncpy(AttrValue, pos1, len);
        strncpy(CmdAttr->AttrValue[i], AttrValue, MAX_VAR_DATA_LEN-1);
      }
    }
  }
  
  return 0;
}
//妮┦把计浪琩
int CMsgfifo::Parse_Msg_Attr_Value(CFlwRule *FlwRule, CH *ValueStr, UC VarType, UC MacroType, US MacroNo, CH *ReturnValueStr, CH *ErrMsg)
{
    long IntValue;
    double FloatValue;
    int i, j, len, len1, StrLen;

    if (MacroType == 0)
    {
        if (VarType == 0)
        {
            strcpy(ReturnValueStr, ValueStr);
            return 0;
        }
        else if (VarType == 1)
        {
            if (strcmp(ValueStr, "0") == 0 || strcmp(ValueStr, "1") == 0)
            {
                strcpy(ReturnValueStr, ValueStr);
                return 0;
            }
            else
            {
                //ぃ琌ΤBOOL计
                sprintf(ErrMsg, "把计: %s ぃ琌ΤBOOL计", ValueStr);
                return 1;
            }
        }
        else if (VarType == 2)
        {
            if (Check_long_Str(ValueStr, IntValue) == 0 )
            {
                strcpy(ReturnValueStr, ValueStr);
                return 0;
            }
            else
            {
                //ぃ琌Τ俱计
                sprintf(ErrMsg, "把计: %s ぃ琌Τ俱计", ValueStr);
                return 1;
            }
        }
        else if (VarType == 3)
        {
            if (Check_Float_Str(ValueStr, FloatValue) == 0 )
            {
                strcpy(ReturnValueStr, ValueStr);
                return 0;
            }
            else
            {
                //ぃ琌Τ疊翴计
                sprintf(ErrMsg, "把计: %s ぃ琌Τ疊翴计", ValueStr);
                return 1;
            }
        }
        else if (VarType == 4)
        {
            if (strlen(ValueStr) == 1 && isprint(ValueStr[0]) != 0)
            {
                strcpy(ReturnValueStr, ValueStr);
                return 0;
            }
            else
            {
                //ぃ琌Τ才
                sprintf(ErrMsg, "把计: %s ぃ琌Τ才", ValueStr);
                return 1;
            }
        }
        else if (VarType == 5)
        {
            strcpy(ReturnValueStr, ValueStr);
            return 0;
        }
        else
        {
            //把计岿粇
            sprintf(ErrMsg, "钡Μ砏玥VarType=%d﹚竡岿粇", VarType);
            return 1;
        }
    }
    else if (MacroType == 1)
    {
        if (Check_long_Str(ValueStr, IntValue) == 0 )
        {
            if (MacroNo < MAX_INT_RANGE_NUM)
            {
                if (1)
                {
                    if ((UL)IntValue >= FlwRule->IntRange[MacroNo].lownum && (UL)IntValue <= FlwRule->IntRange[MacroNo].upnum)
                    {
                        strcpy(ReturnValueStr, ValueStr);
                        return 0;
                    }
                    else
                    {
                        //把计禬璖瞅
                        sprintf(ErrMsg, "把计: %s 禬璖瞅<%lu - %lu>", ValueStr, FlwRule->IntRange[MacroNo].lownum, FlwRule->IntRange[MacroNo].upnum);
                        return 2;
                    }
                }
                else
                {
                    //Щ﹚竡禬璖瞅
                    sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d ぃ", MacroNo);
                    return 1;
                }
            }
            else
            {
                //Щ﹚竡禬璖瞅
                sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d 禬璖瞅", MacroNo);
                return 1;
            }
        }
        else
        {
            //ぃ琌Τ俱计岿粇
            sprintf(ErrMsg, "把计: %s ぃ琌Τ俱计", ValueStr);
            return 3;
        }
    }
    else if (MacroType == 2)
    {
        if (strlen(ValueStr) == 1)
        {
            if (MacroNo < MAX_CHAR_RANGE_NUM)
            {
                if (1)
                {
                    len = FlwRule->CharRange[MacroNo].len;
                    for (i = 0; i < len; i ++)
                    {
                        if (FlwRule->CharRange[MacroNo].chars[i] == ValueStr[0])
                        {
                            strcpy(ReturnValueStr, ValueStr);
                            return 0;
                        }
                    }
                    //赣把计ぃ砏﹚璖瞅ず
                    sprintf(ErrMsg, "把计: %s ぃ琌砏﹚才<%s>", ValueStr, FlwRule->CharRange[MacroNo].chars);
                    return 1;
                }
                else
                {
                    //Щ﹚竡禬璖瞅
                    sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d ぃ", MacroNo);
                    return 1;
                }
            }
            else
            {
                //Щ﹚竡禬璖瞅
                sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d 禬璖瞅", MacroNo);
                return 1;
            }
        }
        else
        {
            //把计ぃ虫才
            sprintf(ErrMsg, "把计: %s ぃ琌Τ虫才", ValueStr);
            return 1;
        }
    }
    else if (MacroType == 3)
    {
        if (MacroNo < MAX_STR_RANGE_NUM)
        {
            if (1)
            {
                len = strlen(ValueStr);
                StrLen = len;
                len1 = FlwRule->StrRange[MacroNo].len;
                for (i = 0; i < len; i ++)
                {
                    for (j = 0; j < len1; j ++)
                    {
                        if (FlwRule->StrRange[MacroNo].chars[j] == ValueStr[i])
                        {
                            StrLen --;
                            break;
                        }
                    }
                }
                if (StrLen == 0)
                {
                    strcpy(ReturnValueStr, ValueStr);
                    return 0;
                }
                else
                {
                    //赣把计ぃ砏﹚璖瞅ず
                    sprintf(ErrMsg, "把计: %s いΤぃ琌砏﹚才<%s>", ValueStr, FlwRule->StrRange[MacroNo].chars);
                    return 1;
                }
            }
            else
            {
                //Щ﹚竡禬璖瞅
                sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d ぃ", MacroNo);
                return 1;
            }
        }
        else
        {
            //把计
            sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d 禬璖瞅", MacroNo);
            return 1;
        }
    }
    else if (MacroType == 4)
    {
        if (strlen(ValueStr) > 0)
        {
            if (MacroNo < MAX_MACRO_INT_NUM)
            {
                if (1)
                {
                    len = FlwRule->MacroIntRange[MacroNo].stringnum;
                    for (i = 0; i < len; i ++)
                    {
                        if (strcmp(FlwRule->MacroIntRange[MacroNo].Es[i].str, ValueStr) == 0)
                        {
                            //temp = IntToStr(FlwRule->MacroIntRange[MacroNo].values);
                            //strcpy(ReturnValueStr, temp.c_str());
                            Int_To_Str(FlwRule->MacroIntRange[MacroNo].Es[i].value, ReturnValueStr);
                            return 0;
                        }
                    }
                    //赣把计ぃ砏﹚璖瞅ず
                    sprintf(ErrMsg, "把计: %s ぃ琌砏﹚Щ﹚竡才﹃", ValueStr);
                    return 1;
                }
                else
                {
                    //Щ﹚竡禬璖瞅
                    sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d ぃ", MacroNo);
                    return 1;
                }
            }
            else
            {
                //Щ﹚竡禬璖瞅
                sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d 禬璖瞅", MacroNo);
                return 1;
            }
        }
        else
        {
            //把计
            sprintf(ErrMsg, "把计: %s 把计", ValueStr);
            return 1;
        }
    }
    else if (MacroType == 5)
    {
        if (strlen(ValueStr) > 0)
        {
            if (MacroNo < MAX_MACRO_STR_NUM)
            {
                if (1)
                {
                    len = FlwRule->MacroStrRange[MacroNo].stringnum;
                    for (i = 0; i < len; i ++)
                    {
                        if (strcmp(FlwRule->MacroStrRange[MacroNo].Es[i].str, ValueStr) == 0)
                        {
                            strcpy(ReturnValueStr, ValueStr);
                            return 0;
                        }
                    }
                    //赣把计ぃ砏﹚璖瞅ず
                    return 1;
                }
                else
                {
                    //Щ﹚竡禬璖瞅
                    sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d ぃ", MacroNo);
                    return 1;
                }
            }
            else
            {
                //Щ﹚竡禬璖瞅
                sprintf(ErrMsg, "Щ﹚竡砏玥腹: %d 禬璖瞅", MacroNo);
                return 1;
            }
        }
        else
        {
            //把计
            sprintf(ErrMsg, "把计: %s 把计", ValueStr);
            return 1;
        }
    }
    else
    {
        //礚Щ﹚竡摸
        sprintf(ErrMsg, "Щ﹚竡摸: %d ぃ", MacroType);
        return 1;
    }
}
//秆猂
int CMsgfifo::Parse_Msg(CFlwRule *FlwRule, const VXML_SEND_MSG_CMD_RULE_STRUCT *MsgRule, VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	CH ReturnValueStr[MAX_VAR_DATA_LEN], ErrMsg[MAX_EXP_STRING_LEN];
	US MsgId;

	MsgId = AttrData->MsgId;
	if (strcmp(AttrData->CmdName, MsgRule[MsgId].MsgNameEng) != 0)
  {
		MyTrace(0, "MsgId = '%d' but MsgName is not '%s' error", MsgId, MsgRule[MsgId].MsgNameEng);
		return 1;
  }
	for (int i = 0; i < AttrData->AttrNums; i ++)
	{
	  //printf("RuleAttrName = %s RecvAttrName = %s RecvAttrValue = %s\n", FlwRule->XMLIVRMsgRule[MsgId].Attribute[i].AttrNameEng, AttrData->AttrName[i], AttrData->AttrValue[i]);
    
    if (strcmp(AttrData->AttrName[i], MsgRule[MsgId].Attribute[i].AttrNameEng) == 0
        && strlen(AttrData->AttrName[i]) > 0)
    {
      if (Parse_Msg_Attr_Value(FlwRule, AttrData->AttrValue[i],
          MsgRule[MsgId].Attribute[i].VarType,
          MsgRule[MsgId].Attribute[i].MacroType,
          MsgRule[MsgId].Attribute[i].MacroNo, ReturnValueStr, ErrMsg) == 0)
      {
        strncpy(AttrData->AttrValue[i], ReturnValueStr, MAX_VAR_DATA_LEN);
      }
			else
			{
	      MyTrace(0, "AttrName = %s AttrValue = %s value is error\n", AttrData->AttrName[i], MsgRule[MsgId].Attribute[i].AttrNameEng);
				return 1;
			}
    }
		else
		{
      MyTrace(0, "AttrName = %s but defined is = %s Parse is error\n", AttrData->AttrName[i], AttrData->AttrValue[i]);
			return 1;
		}
	}
	return 0;
}
//钡Μ狝叭竟め狠祇癳
int CXMLRcvMsg::ParseRevMsgWithCheck(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule)
{
  int result=ParseRevMsg(MsgRule);
  
  /*if(result==0)//妮┦把计浪琩,蠢传莱Щ	
		for (int i = 0; i < AttrNum; i ++)
		{
				result=FlwRuleMng.Parse_Msg_Attr_Value(MsgRule.Attribute[i],AttrValues[i]);
  			if (result)
  				break;
  	}		*/
  return result;
}
//祇癳め狠狝叭竟祇癳(LOG狝叭竟АノΩΑ)
int CXMLRcvMsg::ParseSndMsgWithCheck(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule)
{
  int result=ParseSndMsg(MsgRule);
  return result;	
}
