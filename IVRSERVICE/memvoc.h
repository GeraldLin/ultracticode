//---------------------------------------------------------------------------
#ifndef __MEMVOC_H_
#define __MEMVOC_H_
//---------------------------------------------------------------------------
//#include ""
//---------------------------------------------------------------------------
//內存索引放音文件列表結構
typedef struct
{
	UC state;		//數據狀態 0: 無 1：有
	US IndexNo; //索引號
	CStringX FileName;	//語音文件名稱
	//CH Alias[MAX_USERNAME_LEN];	//別名

}VXML_MEM_PLAY_LIST_STRUCT;

class CMemvoc
{
public:
	CMemvoc();
	virtual ~CMemvoc();
	
	VXML_MEM_PLAY_LIST_STRUCT *Index;
	//讀內存索引放音文件對照表
	void Read_MemPlayIndex(const char *pszMemVocINIFileName, const char *pszMemVocPath);
};
//---------------------------------------------------------------------------
#endif
