//---------------------------------------------------------------------------
#include "stdafx.h"
#include "funcfile.h"

//---------------------------------------------------------------------------
//在文件名前插入根路徑
CStringX AddRootPath_str(const CStringX rootpath, const char *filename)
{
#ifdef WIN32 //win32環境
  static CStringX FilePath;
  if (g_VOCInLinux == 0) //2013-09-30
  {
    if (strlen(filename) > 2 && isalpha(filename[0]) && filename[1] == ':' && (filename[2] == '/' || filename[2] == '\\'))
      FilePath = filename;
    else
      FilePath.Format("%s/%s", rootpath.C_Str(), filename);
  }
  else //2013-09-30
  {
    if (filename[0] == '/')
      FilePath = filename;
    else
      FilePath.Format("%s/%s", rootpath.C_Str(), filename);
  }
  return FilePath;
#else //LINUX環境
  static CStringX FilePath;
  if (filename[0] == '/')
    FilePath = filename;
  else
    FilePath.Format("%s/%s", rootpath.C_Str(), filename);
  return FilePath;
#endif
}
CStringX AddRootPath(const CStringX rootpath, const CStringX filename)
{
#ifdef WIN32 //win32環境
  static CStringX FilePath;
  if (g_VOCInLinux == 0) //2013-09-30
  {
    if (filename.GetLength() > 2 && isalpha(filename[0]) && filename[1] == ':' && (filename[2] == '/' || filename[2] == '\\'))
      FilePath = filename;
    else
      FilePath.Format("%s/%s", rootpath.C_Str(), filename.C_Str());
  }
  else //2013-09-30
  {
    if (filename.GetLength() ==0)
      return "";
    if (filename[0] == '/')
      FilePath = filename;
    else
      FilePath.Format("%s/%s", rootpath.C_Str(), filename.C_Str());
  }
  return FilePath;
#else //LINUX環境
  static CStringX FilePath;
  if (filename.GetLength() ==0)
    return "";
  if (filename[0] == '/')
    FilePath = filename;
  else
    FilePath.Format("%s/%s", rootpath.C_Str(), filename.C_Str());
  return FilePath;
#endif
}
//檢查目錄
int MyCheckPath(const char *DirName)
{
#ifdef WIN32 //win32環境
	CFileFind FileFind;
  CH path[MAX_VAR_DATA_LEN];

  strcpy(path, DirName);
  strcat(path, "\\*.*");
  return FileFind.FindFile(path);
#else //LINUX環境
  if (access(DirName, F_OK) == 0)
    return 1;
  else
    return 0;
#endif
}
//創建目錄
int MyCreatePath(const char *DirName)
{
#ifdef WIN32 //win32環境
	if (CreateDirectory(DirName,NULL)==0)
	{
		DWORD ret=GetLastError();
		if (ret==183) //目錄已存在
			return 2;
		else
			return 4; //4：失敗；
	}
	return 0; //成功
#else //LINUX環境
  return mkdir(DirName, 0777);
#endif
}
//刪除目錄
int MyDeletePath(const char *DirName)
{
#ifdef WIN32 //win32環境
	CFileFind tempFind;
	char tempFileFind[200]; 

	sprintf(tempFileFind,"%s\\*.*",DirName); 
	BOOL IsFinded=(BOOL)tempFind.FindFile(tempFileFind); 
	while(IsFinded) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots()) 
		{ 
			char foundFileName[200]; 
			strcpy(foundFileName,tempFind.GetFileName().GetBuffer(200)); 
			if(tempFind.IsDirectory()) 
			{ 
				char tempDir[200]; 
				sprintf(tempDir,"%s\\%s",DirName,foundFileName); 
				RemoveDirectory(tempDir); 
			} 
			else 
			{ 
				char tempFileName[200]; 
				sprintf(tempFileName,"%s\\%s",DirName,foundFileName); 
				DeleteFile(tempFileName); 
			} 
		} 
	} 
	tempFind.Close(); 
	if(!RemoveDirectory(DirName)) 
	{ 
		return 1; 
	} 
	return 0; 
#else //LINUX環境
  char temp[32];
  sprintf(temp, "rm %s -rf &", DirName);
  system(temp);
  return 0;
#endif
}
#ifndef WIN32 //win32環境
unsigned int FileCount, GetMaxFiles, PathLen;
char FileAttr[MAX_PATH_FILE_LEN];
VXML_GETFILEATTR_STRUCT *GetFileList;

int fnGetFileNum(const char *file, const struct stat *sb, int flag)
{
  if (flag == FTW_F)
  {
    if (fnmatch(FileAttr, file, FNM_PATHNAME) == 0)
    {
      FileCount ++;
    }
  }
  return 0;
}
int fnGetFileName(const char *file, const struct stat *sb, int flag)
{
  if (flag == FTW_F)
  {
    if (fnmatch(FileAttr, file, FNM_PATHNAME) == 0)
    {
      if (strlen(file) > PathLen)
      {
        GetFileList[FileCount].FileName = &file[PathLen];
        GetFileList[FileCount].FileSize = sb->st_size;
        GetFileList[FileCount].ChangeTime = sb->st_mtime;
        FileCount ++;
      }
    }
    if (FileCount >= MAX_GET_FILE_NUM)
      return 1;
  }
  return 0;
}
#endif
//取指定目錄下的文件數
int MyGetFileNum(const char *DirName, const char *FileName)
{
#ifdef WIN32 //win32環境
	int FileNum = 0;
	CFileFind tempFind;
	char tempFileFind[200]; 

	sprintf(tempFileFind,"%s\\%s",DirName, FileName); 
	BOOL IsFinded=(BOOL)tempFind.FindFile(tempFileFind); 
	while(IsFinded) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots() && !tempFind.IsDirectory()) 
		{ 
			FileNum ++;
		} 
	} 
	tempFind.Close(); 
	return FileNum;
#else //LINUX環境 
  FileCount = 0;
  sprintf(FileAttr, "%s/%s", DirName, FileName);
  ftw(DirName, fnGetFileNum, MAX_GET_FILE_NUM);
  return FileCount;
#endif
}
//取指定目錄、特性的文件名
int MyGetFileName(const char *DirName, const char *FileName, int MaxFileNum, VXML_GETFILEATTR_STRUCT *FileNames)
{
#ifdef WIN32 //win32環境
	int FileNum = 0;
	CFileFind tempFind;
	char tempFileFind[2048], tempFileName[MAX_VAR_DATA_LEN];
	CTime ChangeTime;
	DWORD FileSize;

	sprintf(tempFileFind,"%s\\%s", DirName, FileName); 
	BOOL IsFinded=(BOOL)tempFind.FindFile(tempFileFind); 
	while(IsFinded && FileNum < MaxFileNum) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots()) 
		{ 
			char foundFileName[MAX_VAR_DATA_LEN]; 
			strcpy(foundFileName, tempFind.GetFileName().GetBuffer(MAX_VAR_DATA_LEN)); 
			if(!tempFind.IsDirectory()) 
			{ 
				strcpy(FileNames[FileNum].FileName, foundFileName);
				tempFind.GetLastWriteTime(FileNames[FileNum].ChangeTime);
				FileNames[FileNum].FileSize = tempFind.GetLength();
				FileNum ++;
			} 
		} 
	} 
	tempFind.Close(); 
	//按文件修改時間排序
	for(int i = 1; i < FileNum; i ++) 
	{
		for (int j = FileNum-1; j >= i; j --) 
		{
			if(FileNames[j].ChangeTime > FileNames[j-1].ChangeTime) 
			{
				strcpy(tempFileName, FileNames[j-1].FileName);
				ChangeTime = FileNames[j-1].ChangeTime;
				FileSize = FileNames[j-1].FileSize;

				strcpy(FileNames[j-1].FileName, FileNames[j].FileName);
				FileNames[j-1].ChangeTime = FileNames[j].ChangeTime;
				FileNames[j-1].FileSize = FileNames[j].FileSize;

				strcpy(FileNames[j].FileName, tempFileName);
				FileNames[j].ChangeTime = ChangeTime;
				FileNames[j].FileSize = FileSize;
			} 
		}
	}
	return FileNum; 
#else //LINUX環境
  CStringX tempFileName;
  int FileSize;
  time_t ChangeTime;
  
  FileCount = 0;
  PathLen = strlen(DirName)+1;
  sprintf(FileAttr, "%s/%s", DirName, FileName);
  GetFileList = FileNames;
  ftw(DirName, fnGetFileName, MaxFileNum);
  //按文件修改時間排序
  for(unsigned int i = 1; i < FileCount; i ++) 
  {
    for (unsigned int j = FileCount-1; j >= i; j --) 
    {
      if(FileNames[j].ChangeTime > FileNames[j-1].ChangeTime) 
      {
        tempFileName = FileNames[j-1].FileName;
        ChangeTime = FileNames[j-1].ChangeTime;
        FileSize = FileNames[j-1].FileSize;
        
        FileNames[j-1].FileName = FileNames[j].FileName;
        FileNames[j-1].ChangeTime = FileNames[j].ChangeTime;
        FileNames[j-1].FileSize = FileNames[j].FileSize;
        
        FileNames[j].FileName = tempFileName;
        FileNames[j].ChangeTime = ChangeTime;
        FileNames[j].FileSize = FileSize;
      } 
    }
  }
  if (FileCount > (unsigned int)MaxFileNum)
    FileCount = MaxFileNum;
  return FileCount;
#endif
}
//檢查文件屬性
int MyCheckFileAttr(const char *FilePath, VXML_GETFILEATTR_STRUCT *FileNames)
{
#ifdef WIN32 //win32環境
	CFileFind tempFind;

	BOOL IsFinded=(BOOL)tempFind.FindFile(FilePath); 
	if(IsFinded) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots()) 
		{ 
			char foundFileName[250]; 
			strcpy(foundFileName, tempFind.GetFileName().GetBuffer(250)); 
			if(!tempFind.IsDirectory()) 
			{ 
				strcpy(FileNames->FileName, foundFileName);
				tempFind.GetLastWriteTime(FileNames->ChangeTime);
				FileNames->FileSize = tempFind.GetLength();
        tempFind.Close();
				return 1;
			} 
		} 
	} 
	tempFind.Close(); 
	return 0;
#else //LINUX環境
  struct stat buf;
  if (access(FilePath, F_OK) == 0)
  {
    stat(FilePath, &buf);
    //printf("MyCheckFileAttr FilePath=%s st_size=%d st_mtime=%s\n",FilePath, buf.st_size, time_t2str(buf.st_mtime));
    FileNames->FileSize = buf.st_size;
    FileNames->ChangeTime = buf.st_mtime;
    return 1;
  }
  FileNames->FileSize = 0;
  FileNames->ChangeTime = 0;
  return 0;
#endif
}
//取文件大小
long MyCheckFileSize(const char *FilePath)
{
#ifdef WIN32 //win32環境
	CFileFind tempFind;
  long size = 0;

	BOOL IsFinded=(BOOL)tempFind.FindFile(FilePath); 
	if(IsFinded) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots()) 
		{ 
			if(!tempFind.IsDirectory()) 
			{ 
        size = tempFind.GetLength();
        tempFind.Close();
        return size;
			} 
		} 
	} 
	tempFind.Close(); 
	return size;
#else //LINUX環境
  struct stat buf;
  if (access(FilePath, F_OK) == 0)
  {
    stat(FilePath, &buf);
    //printf("MyCheckFileSize FilePath=%s st_size=%d\n",FilePath, buf.st_size);
    return buf.st_size;
  }
  else
  {
    return 0;
  }
#endif
}
//檢查文件是否存在
int MyCheckFileExist(const char *FilePath)
{
#ifdef WIN32 //win32環境
	CFileFind tempFind;

	BOOL IsFinded=(BOOL)tempFind.FindFile(FilePath); 
	if(IsFinded) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots()) 
		{ 
			if(!tempFind.IsDirectory()) 
			{ 
				return 1;
			} 
		} 
	} 
	tempFind.Close(); 
	return 0; //文件不存在
#else //LINUX環境
  if (access(FilePath, F_OK) == 0)
    return 1;
  else
    return 0;
#endif
}
//拷貝文件
int MyCopyFile(const char *sFileName, const char *dFileName)
{
#ifdef WIN32 //win32環境
  return CopyFile(sFileName, dFileName, false);
#else //LINUX環境
  char temp[512];
  sprintf(temp, "cp %s %s &", sFileName, dFileName);
  return system(temp);
#endif
}
//
int DeleteFiles(const char *dir)
{
#ifdef WIN32 //win32環境
	CFileFind ff;
	CString path = (CString)dir;

	BOOL res = ff.FindFile(path);

	while(res)
	{
		res = ff.FindNextFile();
		if (!ff.IsDots() && !ff.IsDirectory())
			DeleteFile(ff.GetFilePath());
	}
	return 0;
#else //LINUX環境
	return remove(dir);
#endif
}
int CreateAllDirectories(const char *dir)
{
#ifdef WIN32 //win32環境
  // remove ending / if exists
  CString strDir = (CString)dir;
  if (strDir.GetLength() == 0)
    return 1;
  if(strDir.Right(1)=="\\" || strDir.Right(1)=="/")
    strDir=strDir.Left(strDir.GetLength()-1);
  
  // base case . . .if directory exists
  if(GetFileAttributes(strDir)!=-1)
    return 0;
  
  // recursive call, one less directory
  int nFound1 = strDir.ReverseFind('\\');
  int nFound2 = strDir.ReverseFind('/');
  if (nFound1>nFound2)
    CreateAllDirectories(strDir.Left(nFound1));
  else
    CreateAllDirectories(strDir.Left(nFound2));
  
  // actual work
  if (strDir.GetLength() > 0)
  {
    CreateDirectory(strDir,NULL);
  }
  return 0;
#else //LINUX環境
  char temp[512];
  if(MyCheckPath(dir)==1)
    return 0;
  
  sprintf(temp, "mkdir -p -m 0777 %s", dir);
  return system(temp);
#endif
}
int GetHDSpace(const char *drive, int &FreeSpace, int &TotalSpace, int &UserSpace)
{
#ifdef WIN32 //win32環境
  ULARGE_INTEGER freespace,totalspace,userspace;

  if (GetDiskFreeSpaceEx(drive, &userspace,&totalspace,&freespace))
  {
    FreeSpace = (int)(freespace.QuadPart/1024/1024);
    TotalSpace = (int)(totalspace.QuadPart/1024/1024);
    UserSpace = (int)(userspace.QuadPart/1024/1024);
    return 0;
  }

  FreeSpace = 0;
  TotalSpace = 0;
  UserSpace = 0;
  return 1;
#else //LINUX環境
  FreeSpace = 0;
  TotalSpace = 0;
  UserSpace = 0;
  return 1;
#endif
}
#ifdef WIN32 //win32環境
/*******************************************************************************
*
* FUNCTION : TakeOwnership
*
* PURPOSE  : This function is used to take the Owneship of a specified file
*
* INPUT    : lpszFile     :name of the file for which permissions are to be set
*            
* RETURNS  : TRUE, if the function succeeds, else FALSE
*
********************************************************************************/

BOOL TakeOwnership(LPCTSTR lpszFile)
{
	int file[256];
	char error[256];
	DWORD description;
	SECURITY_DESCRIPTOR sd;
	SECURITY_INFORMATION info_owner=OWNER_SECURITY_INFORMATION;
		
	TOKEN_USER *owner = (TOKEN_USER*)file;
	HANDLE token;
	
	
	InitializeSecurityDescriptor(&sd,SECURITY_DESCRIPTOR_REVISION);
	if(OpenProcessToken(GetCurrentProcess(),TOKEN_READ | TOKEN_ADJUST_PRIVILEGES,&token))
	{
		//To Get the Current Process Token & opening it to adjust the previleges
		if(SetPrivilege(token,SE_TAKE_OWNERSHIP_NAME,FALSE))
		{
			//Enabling the privilege
			if(GetTokenInformation(token,TokenUser,owner,sizeof(file),&description))
			{
				//Get the information on the opened token
				if(SetSecurityDescriptorOwner(&sd,owner->User.Sid,FALSE))
				{
					//replace any owner information present on the security descriptor
					if(SetFileSecurity(lpszFile,info_owner,&sd))
						return(TRUE);
					else
					{
						// Call GetLastError to determine whether the function succeeded.
						sprintf(error,"Error in SetFileSecurity Error No : %d",GetLastError());
						//MessageBox(NULL,error,NULL,MB_OK);
					}//SetFileSecurity
				}
				else
				{
					sprintf(error,"Error in SetSecurityDescriptorOwner Error No : %d",GetLastError());
					//MessageBox(NULL,error,NULL,MB_OK);
				}//SetSecurityDescriptorOwner
			}
			else
			{
				sprintf(error,"Error in GetTokenInformation Error No : %d",GetLastError());
				//MessageBox(NULL,error,NULL,MB_OK);
			}//GetTokenInformation
		}
		else
		{
			sprintf(error,"Error in SetPrivilege No : %d",GetLastError());
			//MessageBox(NULL,error,NULL,MB_OK);
		}//SetPrivilege
	}
	else
	{
		sprintf(error,"Error in OpenProcessToken No : %d",GetLastError());
		//MessageBox(NULL,error,NULL,MB_OK);
	}//OpenProcessToken

	SetPrivilege(token,SE_TAKE_OWNERSHIP_NAME,TRUE);//Disabling the set previlege

	return(FALSE);
}


/*******************************************************************************
*
* FUNCTION : SetPrivilege
*
* PURPOSE : This function is used to enable or disable the privileges of a user
*
* INPUT    : hToken       :handle of the user token
*            lpszPrivilege:name of the privilege to be set
*            bChange      :if this flag is FALSE, then it enables the specified
*                          privilege. Otherwise It disables all privileges.
*
* RETURNS  : TRUE, if the function succeeds, else FALSE
*
********************************************************************************/


BOOL SetPrivilege(HANDLE hToken,LPCTSTR lpszPrivilege,BOOL bChange)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;
	BOOL bReturnValue = FALSE;

	if (lpszPrivilege != NULL && !bChange)
	{
		if (LookupPrivilegeValue( 
			NULL,            // lookup privilege on local system
			lpszPrivilege,   // privilege to lookup 
			&luid )) 
		{      // receives LUID of privilege
			tp.PrivilegeCount = 1;
			tp.Privileges[0].Luid = luid;
			tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		}
	}

	AdjustTokenPrivileges(hToken,bChange,&tp,
		sizeof(TOKEN_PRIVILEGES), 
		(PTOKEN_PRIVILEGES) NULL,
		(PDWORD) NULL);  
	// Call GetLastError to determine whether the function succeeded.
	
	if (GetLastError() == ERROR_SUCCESS) 
	{ 
		bReturnValue = TRUE;
	} 
	 
	return bReturnValue;
} 


/*******************************************************************************
*
* FUNCTION : SetPermission
*
* PURPOSE : This function is used to the Permissions of a file
*
* INPUT    : lpszFile	  :name of the file for which permissions are to be set
*            lpszAccess   :Access rights for the specified file.
*            dwAccessMask :access rights to be granted to the specified SID              
*
* RETURNS  : TRUE, if the function succeeds, else FALSE
*
********************************************************************************/

BOOL SetPermission(LPCTSTR lpszFile, LPCTSTR lpszAccess, DWORD dwAccessMask)
{
	int buff[512];
	char domain[512];
	char error[256];
	
	DWORD domain_size=sizeof(domain);
	DWORD acl_size;

	SECURITY_DESCRIPTOR sd1;
	SECURITY_INFORMATION info_dacl=DACL_SECURITY_INFORMATION;
	PSID sid = (PSID*)buff;
	ACL *acl;
	SID_NAME_USE sidname;
	DWORD sid_size=sizeof(buff);
	
	InitializeSecurityDescriptor(&sd1,SECURITY_DESCRIPTOR_REVISION);
	//to get the SID 
	if(LookupAccountName(NULL,lpszAccess,sid,&sid_size,domain,&domain_size,&sidname))
	{
		acl_size=sizeof(ACL)+sizeof(ACCESS_ALLOWED_ACE)-sizeof(DWORD)+GetLengthSid(buff);
		acl = (ACL *)malloc(acl_size);
		//To calculate the size of an ACL, 
		InitializeAcl(acl,acl_size,ACL_REVISION);

		if(AddAccessAllowedAce(acl,ACL_REVISION,dwAccessMask,sid))
		{
			if(SetSecurityDescriptorDacl(&sd1,TRUE,acl,FALSE))
			{
				if(SetFileSecurity(lpszFile,info_dacl,&sd1))
					return(TRUE);
				else
				{
					sprintf(error,"Error in SetFileSecurity Error No : %d",GetLastError());
					//MessageBox(NULL,error,NULL,MB_OK);
				}//SetFileSecurity
			}
			else
			{
				sprintf(error,"Error in SetSecurityDescriptorDacl Error No : %d",GetLastError());
				//MessageBox(NULL,error,NULL,MB_OK);
			}//SetSecurityDescriptorDacl
		}
		else
		{
			sprintf(error,"Error in AddAccessAllowedAce Error No : %d",GetLastError());
			//MessageBox(NULL,error,NULL,MB_OK);
		}//AddAccessAllowedAce
	}
	else
	{
		sprintf(error,"Error in LookupAccountName No : %d",GetLastError());
		//MessageBox(NULL,error,NULL,MB_OK);
	}//LookupAccountName

	free(acl);
	return(FALSE);
}
#endif

int DeleteDir(LPCTSTR lpszName)
{
#ifdef WIN32 //win32環境
	int ret=0;
	char name1[256];
	WIN32_FIND_DATA info;
    HANDLE hp;
    char *cp;
	
	sprintf(name1, "%s\\*.*",lpszName);
    hp = FindFirstFile(name1,&info);
    if(!hp || hp==INVALID_HANDLE_VALUE)
        return(ret);
    do
    {
		cp = info.cFileName;
        if(cp[1]==0 && *cp=='.')
            continue;
        else if(cp[2]==0 && *cp=='.' && cp[1]=='.')
            continue;
        sprintf(name1,"%s\\%s",lpszName,info.cFileName);
		if(info.dwFileAttributes&FILE_ATTRIBUTE_READONLY)
		{
			SetFileAttributes(name1,info.dwFileAttributes&~FILE_ATTRIBUTE_READONLY);
		}
		if(info.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)
		{
			TakeOwnership(name1);
			SetPermission(name1,"everyone",GENERIC_ALL);
			DeleteDir(name1);
		}
		else
		{
			TakeOwnership(name1);
			SetPermission(name1,"everyone",GENERIC_ALL);
			DeleteFile(name1);
		}

    }
    while(FindNextFile(hp,&info));
	FindClose(hp);
	if(info.dwFileAttributes&FILE_ATTRIBUTE_READONLY)
	{
		SetFileAttributes(lpszName,info.dwFileAttributes&~FILE_ATTRIBUTE_READONLY);
	}
	if(RemoveDirectory(lpszName))
	{
		//printf("success\n");
		ret=1;
	}	
	else
	{
		//printf("error %d\n",GetLastError());
	}
	return(ret);
#else //LINUX環境
	return remove(lpszName);
#endif
}
int CreateFullPathIfNotExist(const char *PathFileName)
{
#ifdef WIN32 //win32環境
  CString strDir = (CString)(PathFileName);
  int nFound1 = strDir.ReverseFind('\\');
  int nFound2 = strDir.ReverseFind('/');
  if (nFound1<nFound2)
    nFound1 = nFound2;
  CreateAllDirectories(strDir.Left(nFound1));
  if(GetFileAttributes(strDir.Left(nFound1))==-1)
	{
		return 1;
	}
	return 0;
#else //LINUX環境
  char *nFound = strrchr(PathFileName,'/');
  char path[512];
  
  memset(path, 0, 512);
  strncpy(path, PathFileName, int(nFound - PathFileName));
  
  if (CreateAllDirectories(path) !=0)
  {
    return 1;
  }
  if(MyCheckPath(path)!=1)
  {
    return 1;
  }
  return 0;
#endif
}
