//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procag.h"
#include "extern.h"

//-------------------------------讀坐席配置參數--------------------------------
int nAGn; //當前坐席編號

void ReadWorkerGroupName()
{
  CH szTemp[32], szGroupName[64];
  for (int i=0; i<MAX_AG_GROUP; i++)
  {
    sprintf(szTemp, "GroupName[%d]", i);
    GetPrivateProfileString("GROUPNAME", szTemp, "", szGroupName, 64, g_szServiceINIFileName);
    if (strlen(szGroupName) == 0)
    {
      sprintf(szGroupName, "Group%d", i);
    }
    pAgentMng->WorkerGroup[i].GroupName = szGroupName;
    //話務組對應的交換機的ACDSplit組號
    sprintf(szTemp, "SwitchGroupID[%d]", i);
    GetPrivateProfileString("GROUPNAME", szTemp, "", szGroupName, 64, g_szServiceINIFileName);
    if (strlen(szGroupName) > 0)
    {
      gACDSplitMng.SetData(szGroupName, i);
    }
  }
}
//讀內線分機電話配置
void ReadLocaSeatParam(char *filename, int maxseatnum)
{
	CH pzTemp[32], txtline[128], szAccountNo[64];
	CStringX ArrString[13], ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER];
  int port, SeatNum, groupnum;
  int SeatType, SeatGroupNo, GroupNo, Level, Grade, WorkerNo, LoginMode;
  int nAG, nChn;

  SeatNum = GetPrivateProfileInt("SEAT","SeatNum", 0, filename);
  if (SeatNum == 0)
  {
    SeatNum = maxseatnum;
    sprintf(pzTemp, "%d", SeatNum);
    WritePrivateProfileString("SEAT", "SeatNum", pzTemp, filename);
  }
	if (SeatNum > 0)
	{
    for (port = 0; port < SeatNum && port < MAX_AG_NUM && port < AuthMaxSeat; port ++)
		{
			sprintf(pzTemp, "Seat[%d]", port);
			GetPrivateProfileString("SEAT", pzTemp, "", txtline, 120, filename);
      ArrString[5] = "";
      ArrString[6] = "";
      ArrString[7] = "";
      ArrString[8] = "";
      ArrString[9] = "";
      ArrString[10] = "";
      ArrString[11] = "";
      ArrString[12] = ""; //2015-06-14增加角色權限
      if (SplitString(txtline, ',', 13, ArrString) >= 5)
      {
        if (strlen(ArrString[0].C_Str()) == 0)
        {
          continue;
        }
        if (strlen(ArrString[8].C_Str()) == 0)
          LoginMode = 1;
        else
          LoginMode = atoi(ArrString[8].C_Str());

        if (strlen(ArrString[12].C_Str()) == 0)
          Grade = 1;
        else
          Grade = atoi(ArrString[12].C_Str());

        if (g_nCardType == 9)
          SeatType = SEATTYPE_AG_PC;
        else
          SeatType = atoi(ArrString[1].C_Str());

        SeatGroupNo = atoi(ArrString[2].C_Str());
        WorkerNo = atoi(ArrString[5].C_Str());
        
        if (g_nSwitchType == 0)
          nChn = pBoxchn->Get_nChn_By_LgChn(CHANNEL_SEAT, port);//pBoxchn->Get_ChnNo_By_ChStyle(CH_A_SEAT, port);
        else
          nChn = 0xFFFF;

        //if (pBoxchn->isnChnAvail(nChn) && pAgentMng->Alloc_nAG(nAG))
        if (pAgentMng->Alloc_nAG(nAG))
        {
          sprintf(pzTemp, "Seat[%d].AccountNo", nAG);
          GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 60, filename);
          pAgentMng->m_Agent[nAG]->m_Worker.AccountNo = szAccountNo;
          pAgentMng->m_Agent[nAG]->m_Worker.oldWorkerAutoLoginID = (UC)LoginMode;

          if (strlen(ArrString[9].C_Str()) == 0)
            pAgentMng->m_Agent[nAG]->m_Seat.AutoRecordId = 1;
          else
            pAgentMng->m_Agent[nAG]->m_Seat.AutoRecordId = atoi(ArrString[9].C_Str());
          
          pAgentMng->m_Agent[nAG]->m_Seat.DepartmentNo = ArrString[10];
          pAgentMng->m_Agent[nAG]->m_Seat.LocaAreaCode = ArrString[11];
          pAgentMng->m_Agent[nAG]->ExtnType = 1;

          switch (SeatType)
          {
          case SEATTYPE_AG_TEL:
            if (ArrString[5].GetLength() > 0 && ArrString[5].Compare("0") != 0)
            {
              pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerParam(WorkerNo, ArrString[6].C_Str(), Grade);
              if (LoginMode == 1)
                pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[6].C_Str(), Grade);
              groupnum = SplitString(ArrString[3].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString1);
              SplitString(ArrString[4].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString2);
              for (int i=0; i<groupnum; i++)
              {
                GroupNo = atoi(ArrString1[i].C_Str());
                if (ArrString2[i].GetLength() > 0)
                  Level = atoi(ArrString2[i].C_Str());
                else
                  Level = 1;
                pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerGroupNo(GroupNo, Level);
                if (LoginMode == 1)
                  pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
              }
            }
            pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[7]);

            pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
            pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
            if (g_nSwitchMode == 0)
            {
              pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, port);
              pBoxchn->SetSeatCh(port, nChn, ArrString[0].C_Str(), 1);
            }
            else
              pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, 0xFFFF);
            
            sprintf(pzTemp, "Seat[%d].DIDNo", nAG);
            GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 20, filename);
            pAgentMng->m_Agent[nAG]->m_Seat.SeatDIDNo = szAccountNo;
            
            sprintf(pzTemp, "Seat[%d].LoginSwitchID", nAG);
            pAgentMng->m_Agent[nAG]->m_Seat.LoginSwitchID = GetPrivateProfileInt("SEAT",pzTemp, pIVRCfg->isAgentLoginSwitch==true?1:0, filename);
            
            sprintf(pzTemp, "Seat[%d].LoginSwitchPSW", nAG);
            GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 30, filename);
            strcpy(pAgentMng->m_Agent[nAG]->m_Seat.LoginSwitchPSW, szAccountNo);
            
            sprintf(pzTemp, "Seat[%d].LoginSwitchGroupID", nAG);
            GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 30, filename);
            strcpy(pAgentMng->m_Agent[nAG]->m_Seat.LoginSwitchGroupID, szAccountNo);
            
            if (pBoxchn->isnChnAvail(nChn))
              pBoxchn->pChn_str[nChn].nAG = nAG;
            break;
          case SEATTYPE_AG_PC:
            if (ArrString[5].GetLength() > 0 && ArrString[5].Compare("0") != 0) //add 2010-11-08
            {
              pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerParam(WorkerNo, ArrString[6].C_Str(), Grade);
              if (LoginMode == 1)
                pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[6].C_Str(), Grade);
              groupnum = SplitString(ArrString[3].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString1);
              SplitString(ArrString[4].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString2);
              for (int i=0; i<groupnum; i++)
              {
                GroupNo = atoi(ArrString1[i].C_Str());
                if (ArrString2[i].GetLength() > 0)
                  Level = atoi(ArrString2[i].C_Str());
                else
                  Level = 1;
                pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerGroupNo(GroupNo, Level);
                if (LoginMode == 1)
                  pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
              }
            }
            pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[7]);

            pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
            pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
            if (g_nSwitchMode == 0)
            {
              pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, port);
              pBoxchn->SetSeatCh(port, nChn, ArrString[0].C_Str(), 2);
            }
            else
              pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, 0xFFFF);
            
            sprintf(pzTemp, "Seat[%d].DIDNo", nAG);
            GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 20, filename);
            pAgentMng->m_Agent[nAG]->m_Seat.SeatDIDNo = szAccountNo;
            
            sprintf(pzTemp, "Seat[%d].LoginSwitchID", nAG);
            pAgentMng->m_Agent[nAG]->m_Seat.LoginSwitchID = GetPrivateProfileInt("SEAT",pzTemp, pIVRCfg->isAgentLoginSwitch==true?1:0, filename);
            
            sprintf(pzTemp, "Seat[%d].LoginSwitchPSW", nAG);
            GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 30, filename);
            strcpy(pAgentMng->m_Agent[nAG]->m_Seat.LoginSwitchPSW, szAccountNo);
            
            sprintf(pzTemp, "Seat[%d].LoginSwitchGroupID", nAG);
            GetPrivateProfileString("SEAT", pzTemp, "", szAccountNo, 30, filename);
            strcpy(pAgentMng->m_Agent[nAG]->m_Seat.LoginSwitchGroupID, szAccountNo);

            if (pBoxchn->isnChnAvail(nChn))
              pBoxchn->pChn_str[nChn].nAG = nAG;
            break;
          }
        }
      }
		}
  }
}
int SetOneLocaSeatParam(int nSeatType, int nPortNo, const char *pszSeatData, int &nAG, char *pszError)
{
  CStringX ArrString[9], ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER];
  int groupnum;
  int SeatType, SeatGroupNo, GroupNo, Level, WorkerNo, LoginMode;
  int nChn;
  
  //MyTrace(5, "SetOneLocaSeatParam SeatType=%d PortNo=%d SeatData=%s",
  //  nSeatType, nPortNo, pszSeatData);
  if (nSeatType == SEATTYPE_AG_TEL || nSeatType == SEATTYPE_AG_PC)
  {
    nAG = pAgentMng->GetnAGByPort(nSeatType, nPortNo);
    if (!pAgentMng->isnAGAvail(nAG))
    {
      nChn = pBoxchn->Get_ChnNo_By_ChStyle(CH_A_SEAT, nPortNo);
      if (!pBoxchn->isnChnAvail(nChn))
      {
        strcpy(pszError, "seat port error");
        return 1;
      }
      if (!pAgentMng->Alloc_nAG(nAG))
      {
        strcpy(pszError, "number of seat is overflow");
        return 1;
      }
    }
    else
    {
      nChn = pAgentMng->m_Agent[nAG]->m_Seat.nChn;
    }
    if (pAG->isTcpLinked())
    {
      strcpy(pszError, "had login");
      return 2;
    }
    if (!pAG->isIdelForSrv())
    {
      strcpy(pszError, "the seat is not idel");
      return 3;
    }
    pAG->m_Worker.Init();
    pAG->m_Seat.Init();
    if (SplitString(pszSeatData, ',', 9, ArrString) >= 5)
    {
      SeatType = atoi(ArrString[1].C_Str());
      SeatGroupNo = atoi(ArrString[2].C_Str());
      WorkerNo = atoi(ArrString[5].C_Str());
      
      if (strlen(ArrString[8].C_Str()) == 0)
        LoginMode = 1;
      else
        LoginMode = atoi(ArrString[8].C_Str());

      pAgentMng->m_Agent[nAG]->m_Worker.oldWorkerAutoLoginID = (UC)LoginMode;

      switch (SeatType)
      {
      case SEATTYPE_AG_TEL:
        if (ArrString[5].GetLength() > 0)
        {
          pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerParam(WorkerNo, ArrString[6].C_Str(), 1);
          if (LoginMode == 1)
            pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[6].C_Str(), 1);
          groupnum = SplitString(ArrString[3].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString1);
          SplitString(ArrString[4].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString2);
          for (int i=0; i<groupnum; i++)
          {
            GroupNo = atoi(ArrString1[i].C_Str());
            if (ArrString2[i].GetLength() > 0)
              Level = atoi(ArrString2[i].C_Str());
            else
              Level = 1;
            pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerGroupNo(GroupNo, Level);
            if (LoginMode == 1)
              pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
          }
        }
        pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[7]);
        
        pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
        pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
        pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, nPortNo);
        if (pBoxchn->isnChnAvail(nChn))
          pBoxchn->pChn_str[nChn].nAG = nAG;
        break;
      case SEATTYPE_AG_PC:
        if (ArrString[5].GetLength() > 0) //add 2010-11-08
        {
          pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerParam(WorkerNo, ArrString[6].C_Str(), 1);
          if (LoginMode == 1)
            pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[6].C_Str(), 1);
          groupnum = SplitString(ArrString[3].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString1);
          SplitString(ArrString[4].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString2);
          for (int i=0; i<groupnum; i++)
          {
            GroupNo = atoi(ArrString1[i].C_Str());
            if (ArrString2[i].GetLength() > 0)
              Level = atoi(ArrString2[i].C_Str());
            else
              Level = 1;
            pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerGroupNo(GroupNo, Level);
            if (LoginMode == 1)
              pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
          }
        }
        pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[7]);
        
        pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
        pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
        pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, nPortNo);
        if (pBoxchn->isnChnAvail(nChn))
          pBoxchn->pChn_str[nChn].nAG = nAG;
        break;
      }
    }
    return 0;
  }
  return 4;
}
void SetLocalSeatChn()
{
  int nAG, nChn;

  for (nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->GetSeatType() == SEATTYPE_AG_TEL || pAG->GetSeatType() == SEATTYPE_AG_PC)
    {
      nChn = pBoxchn->Get_ChnNo_By_DeviceID(pAG->m_Seat.SeatNo.C_Str());
      if (pBoxchn->isnChnAvail(nChn))
      {
        if (pBoxchn->pChn_str[nChn].lgChnType == CHANNEL_SEAT)
        {
          pAG->m_Seat.PortNo = pBoxchn->pChn_str[nChn].lgChnNo;
          pAG->m_Seat.nChn = nChn;
          pBoxchn->pChn_str[nChn].nAG = nAG;
          MyTrace(3, "SetLocalSeatChn nAG=%d SeatNo=%s nChn=%d",
            nAG, pAG->m_Seat.SeatNo.C_Str(), nChn);
        }
      }
    }
  }
}
//讀數字內線座席配置
void ReadDTSeatParam(char *filename)
{
	CH pzTemp[16], txtline[128];
	CStringX ArrString[8], ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER];
  int porttype, port, chindex, SeatNum, groupnum;
  int SeatType, SeatGroupNo, GroupNo, Level, WorkerNo;
  int nAG, nChn;

  SeatNum = GetPrivateProfileInt("DTSEAT","SeatNum", 0, filename);
  porttype = GetPrivateProfileInt("DTSEAT","ChType", 11, filename);
	if (SeatNum > 0)
	{
    for (port = 0; port < SeatNum; port ++)
		{
			sprintf(pzTemp, "Seat[%d]", port);
			GetPrivateProfileString("DTSEAT", pzTemp, "", txtline, 120, filename);
			sprintf(pzTemp, "SeatCH[%d]", port);
      chindex = GetPrivateProfileInt("DTSEAT",pzTemp, 9999, filename);
      ArrString[5] = "";
      ArrString[6] = "";
      ArrString[7] = "";
      if (SplitTxtLine(txtline, 8, ArrString) >= 5)
      {
        SeatType = atoi(ArrString[1].C_Str());
        SeatGroupNo = atoi(ArrString[2].C_Str());
        WorkerNo = atoi(ArrString[5].C_Str());
        nChn = pBoxchn->Get_ChnNo_By_ChStyle(porttype, chindex);

        if (pBoxchn->isnChnAvail(nChn) && pAgentMng->Alloc_nAG(nAG))
        {
          switch (SeatType)
          {
          case SEATTYPE_DT_TEL:
            if (ArrString[5].GetLength() > 0)
            {
              pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerParam(WorkerNo, ArrString[6].C_Str(), 1);
              pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[6].C_Str(), 1);
              groupnum = SplitString(ArrString[3].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString1);
              SplitString(ArrString[4].C_Str(), ';', MAX_GROUP_NUM_FORWORKER, ArrString2);
              for (int i=0; i<groupnum; i++)
              {
                GroupNo = atoi(ArrString1[i].C_Str());
                if (ArrString2[i].GetLength() > 0)
                  Level = atoi(ArrString2[i].C_Str());
                else
                  Level = 1;
                pAgentMng->m_Agent[nAG]->m_Worker.SetOldWorkerGroupNo(GroupNo, Level);
                pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
              }
            }
            pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[7]);

            pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
            pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
            pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[0], 0, "", nChn, port);
            pBoxchn->pChn_str[nChn].nAG = nAG;
            break;
          }
        }
      }
		}
  }
}
//讀外線坐席參數
void ReadRemoteSeatParam(char *filename)
{
	CH pzTemp[16], txtline[128];
	CStringX ArrString[10];
  int port, SeatNum;
  int SeatType, SeatGroupNo, GroupNo, Level, WorkerNo, RouteNo;
  int nAG;

  SeatNum = GetPrivateProfileInt("EXTSEAT","SeatNum", 0, filename);
	if (SeatNum > 0)
	{
    for (port = 0; port < SeatNum; port ++)
		{
			sprintf(pzTemp, "Seat[%d]", port);
			GetPrivateProfileString("EXTSEAT", pzTemp, "", txtline, 120, filename);
      ArrString[7] = "";
      ArrString[8] = "";
      ArrString[9] = "";
      if (SplitTxtLine(txtline, 10, ArrString) >= 7)
      {
        SeatType = atoi(ArrString[1].C_Str());
        WorkerNo = atoi(ArrString[2].C_Str());
        RouteNo = atoi(ArrString[3].C_Str());
        
        SeatGroupNo = atoi(ArrString[4].C_Str());
        GroupNo = atoi(ArrString[5].C_Str());
        Level = atoi(ArrString[6].C_Str());
        
        if (ArrString[2].GetLength() > 0 && pAgentMng->Alloc_nAG(nAG))
        {
          switch (SeatType)
          {
          case SEATTYPE_RT_TEL:
            if (ArrString[7].GetLength() > 0)
            {
              pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[8].C_Str(), 1);
              pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
            }
            pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[9]);

            pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
            pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
            pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[2], RouteNo, "", 0xFFFF, port);
            break;
          }
        }
      }
		}
  }
}
int SetOneRemoteSeatParam(int nSeatType, int nPortNo, const char *pszSeatData, int &nAG, char *pszError)
{
  CStringX ArrString[10];
  int nChn;
  int SeatType, SeatGroupNo, GroupNo, Level, WorkerNo, RouteNo;
  
  nAG = pAgentMng->GetnAGByPort(nSeatType, nPortNo);
  if (!pAgentMng->isnAGAvail(nAG))
  {
    if (!pAgentMng->Alloc_nAG(nAG))
    {
      strcpy(pszError, "number of seat is overflow");
      return 1;
    }
  }
  nChn = pAgentMng->m_Agent[nAG]->m_Seat.nChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    strcpy(pszError, "the seat had bangding");
    return 2;
  }
  if (pAG->isTcpLinked())
  {
    strcpy(pszError, "had login");
    return 3;
  }
  if (!pAG->isIdelForSrv())
  {
    strcpy(pszError, "the seat is not idel");
    return 4;
  }
  pAG->m_Worker.Init();
  pAG->m_Seat.Init();

  if (SplitTxtLine(pszSeatData, 10, ArrString) >= 7)
  {
    SeatType = atoi(ArrString[1].C_Str());
    WorkerNo = atoi(ArrString[2].C_Str());
    RouteNo = atoi(ArrString[3].C_Str());
    
    SeatGroupNo = atoi(ArrString[4].C_Str());
    GroupNo = atoi(ArrString[5].C_Str());
    Level = atoi(ArrString[6].C_Str());
    
    if (ArrString[2].GetLength() > 0)
    {
      switch (SeatType)
      {
      case SEATTYPE_RT_TEL:
        if (ArrString[7].GetLength() > 0)
        {
          pAgentMng->m_Agent[nAG]->m_Worker.WorkerLogin(WorkerNo, ArrString[8].C_Str(), 1);
          pAgentMng->m_Agent[nAG]->m_Worker.SetWorkerGroupNo(GroupNo, Level);
        }
        pAgentMng->m_Agent[nAG]->m_Seat.SetBandIP(ArrString[9]);
        
        pAgentMng->m_Agent[nAG]->m_Seat.TcpUnLinked();
        pAgentMng->m_Agent[nAG]->m_Seat.SeatLogin();
        pAgentMng->m_Agent[nAG]->m_Seat.SetSeatParam(SeatType, SeatGroupNo, ArrString[0], ArrString[2], RouteNo, "", 0xFFFF, nPortNo);
        break;
      }
    }
    return 0;
  }
  return 5;
}
int ModifySeatNo(int nAG, const CStringX NewSeatNo)
{
  if (!pAgentMng->isnAGAvail(nAG))
  {
    return 1;
  }
  if (pAgentMng->istheSeatNoExist(NewSeatNo.C_Str()))
  {
    return 2;
  }
  pAgentMng->m_Agent[nAG]->m_Seat.SeatNo = NewSeatNo;
  return 0;
}
void SendAllLoginWorkerstatus()
{
  static bool bSended=false;
  if (bSended == true)
  {
    return;
  }
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isLogin())
    {
      pAG->ChangeStatus(AG_STATUS_LOGIN);
      MyTrace(3, "WriteWorkerLoginStatus nAG=%d SendAllLoginWorkerstatus()", pAG->nAG);
      WriteWorkerLoginStatus(pAG);
      pAG->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
    }
  }
  bSended = true;
}
void SendAllLogoutWorkerstatus()
{
  static bool bSended=false;
  if (bSended == true)
  {
    return;
  }
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isLogin())
    {
      WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
      WriteWorkerLogoutStatus(pAG);
    }
  }
  bSended = true;
}
//-----------------------------------------------------------------------------
void SendAGStateToAG1(int nAG, int nAG1)
{
  int nChn;
  char szTemp[1024], szWsStatus[1024], szLogTime[32];
  if (pAgentMng->isnAGAvail(nAG) && pAgentMng->isnAGAvail(nAG1))
  {
    if (pAG1->isLogin() == true && pAG1->isTcpLinked() == true && (nAG == nAG1 || pAG1->m_Worker.GetAGState == 1))
    {
      if (pAG->ExtnType == 0 || pAG1->ExtnType == 0)
        return;
      Set_IVR2AG_Header(AGMSG_ongetagentstatus, 0);
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 1, nAG);
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 2, pAG->GetSeatNo());
      if (pAG->GetClientId() == 0x06FF)
      {
        Set_IVR2AG_Item(AGMSG_ongetagentstatus, 3, "");
      }
      else
      {
        if (pAG1->GetClientId() & 0x8000)
        {
          Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 3, pAG->GetClientId()&0x7FFF);
        } 
        else
        {
          Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 3, pAG->GetClientId()&0x00FF);
        }
      }
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 4, pAG->GetSeatType());
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 5, pAG->GetSeatGroupNo());
      
      if (pAG->m_Seat.BandIP.GetLength() == 0)
      {
        if (pAG->m_Seat.SeatIP.GetLength() == 0)
        {
          Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, "");
        }
        else
        {
          sprintf(szTemp, "SeatIP=%s", pAG->m_Seat.SeatIP.C_Str());
          Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, szTemp);
        }
      } 
      else
      {
        if (pAG->m_Seat.SeatIP.GetLength() == 0)
        {
          sprintf(szTemp, "BandIP=%s;PhoneIP=%s", pAG->m_Seat.BandIP.C_Str(), pAG->m_Seat.IPPhoneBandIP.C_Str());
          Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, szTemp);
        }
        else
        {
          sprintf(szTemp, "BandIP=%s;SeatIP=%s;PhoneIP=%s", pAG->m_Seat.BandIP.C_Str(), pAG->m_Seat.SeatIP.C_Str(), pAG->m_Seat.IPPhoneBandIP.C_Str());
          Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, szTemp);
        }
      }
      
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 7, pAG->GetWorkerNo());
      if (pAG->GetWorkerNo() == 0)
      {
        if (pAG->m_Worker.LogoutTime == 0)
        {
          Set_IVR2AG_Item(AGMSG_ongetagentstatus, 8, "");
        }
        else
        {
          Set_IVR2AG_Item(AGMSG_ongetagentstatus, 8, time_t2str(pAG->m_Worker.LogoutTime));
        }
      } 
      else
      {
        Set_IVR2AG_Item(AGMSG_ongetagentstatus, 8, time_t2str(pAG->m_Worker.LoginTime));
      }
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 9, pAG->GetWorkerName());
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 10, pAgentMng->GetGroupNoList(nAG)); //pAG->m_Worker.GetGroupNoList());
      if (pAG->DelayState == 0 || pAG->svState > 0)
      {
        Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 11, pAG->svState);
      } 
      else if (pAG->DelayState == 1)
      {
        Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 11, AG_SV_ACW);
      }
      else
      {
        Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 11, AG_SV_DELAY);
      }
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 12, pAG->m_Worker.DisturbId);
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 13, pAG->m_Worker.LeaveId);
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 14, pAG->m_Worker.WorkerCallCountParam.CallInAnsCount);
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 15, pAG->m_Worker.WorkerCallCountParam.CallInNoAnsCount);
      
      memset(szTemp, 0, 128);
      if (pAG->m_Worker.DisturbId == 3)
      {
        if (pAG->m_Seat.SeatLangID == 1)
          sprintf(szTemp, "外出值班電話:%s", pAG->m_Worker.DutyPhone.C_Str());
        else if (pAG->m_Seat.SeatLangID == 2)
          sprintf(szTemp, "���|�G痁?杠:%s", pAG->m_Worker.DutyPhone.C_Str());
        else
          sprintf(szTemp, "GoOutDutyTel:%s", pAG->m_Worker.DutyPhone.C_Str());
      }
      else
      {
        sprintf(szTemp, "%s", pAG->m_Worker.LeaveReason.C_Str());
      }
      if (pAG1->LockId == 1)
      {
        strcat(szTemp, "[臨時鎖定]");
      }
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 16, szTemp);
      Set_IVR2AG_Tail();
    
      SendMsg2AG(pAG1->GetClientId(), AGMSG_ongetagentstatus, IVRSndMsg.GetBuf());
    }
    if (WebSocketLogId == true && strlen(pAG1->m_Seat.WSClientId) > 0 && (pIVRCfg->isOnlySendMySeatStatus == false || pAG1->m_Worker.WorkerGrade > 2))
    {
      memset(szWsStatus, 0, 1024);

      strcat(szWsStatus, "cmd=onagentstatus;seatno="); //2015-07-04
      strcat(szWsStatus, pAG->GetSeatNo().C_Str()); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04

      strcat(szWsStatus, "seatip="); //2015-07-04
      strcat(szWsStatus, pAG->m_Seat.SeatIP.C_Str()); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04

      strcat(szWsStatus, "workerno="); //2015-07-04
      strcat(szWsStatus, pAG->GetWorkerNoStr()); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04

      if (pAG->GetWorkerNo() == 0)
      {
        if (pAG->m_Worker.LogoutTime == 0)
        {
          strcat(szWsStatus, "logtime="); //2015-07-04
          strcat(szWsStatus, ""); //2015-07-04
          strcat(szWsStatus, ";"); //2015-07-04
        }
        else
        {
          strcpy(szLogTime, time_t2str(pAG->m_Worker.LogoutTime));
          strcat(szWsStatus, "logtime="); //2015-07-04
          strcat(szWsStatus, szLogTime); //2015-07-04
          strcat(szWsStatus, ";"); //2015-07-04
        }
      }
      else
      {
        strcpy(szLogTime, time_t2str(pAG->m_Worker.LoginTime));
        strcat(szWsStatus, "logtime="); //2015-07-04
        strcat(szWsStatus, szLogTime); //2015-07-04
        strcat(szWsStatus, ";"); //2015-07-04
      }

      strcat(szWsStatus, "workername="); //2015-07-04
      strcat(szWsStatus, pAG->m_Worker.UnicodeWorkerName); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04

      strcat(szWsStatus, "groupno="); //2015-07-04
      strcat(szWsStatus, pAG->m_Worker.GetGroupNoList()); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04

      if (pAG->DelayState == 0 || pAG->svState > 0)
      {
        if (pAG->m_Worker.DisturbId == 2 && (pAG->svState == 0 || pAG->svState == AG_SV_ACW)) //2016-03-31
          sprintf(szTemp, "svstate=%d;", AG_SV_WAIT_RETURN); //2016-03-31
        else
          sprintf(szTemp, "svstate=%d;", pAG->svState); //2015-07-04
        strcat(szWsStatus, szTemp); //2015-07-04
      }
      else if (pAG->DelayState == 1)
      {
        if (pAG->m_Worker.DisturbId == 2) //2016-03-31
          sprintf(szTemp, "svstate=%d;", AG_SV_WAIT_RETURN); //2016-03-31
        else
          sprintf(szTemp, "svstate=%d;", AG_SV_ACW); //2015-07-04
        strcat(szWsStatus, szTemp); //2015-07-04
      }
      else
      {
        sprintf(szTemp, "svstate=%d;", AG_SV_DELAY); //2015-07-04
        strcat(szWsStatus, szTemp); //2015-07-04
      }

      if (pAG->m_Worker.DisturbId == 0 && pAG->m_Worker.LeaveId == 0)
      {
        sprintf(szTemp, "disturb=%d;leave=%d;reason=;", 0, 0); //2015-07-04
        strcat(szWsStatus, szTemp); //2015-07-04
      }
      else
      {
        if (pAG->m_Worker.DisturbId > 0)
        {
          sprintf(szTemp, "disturb=%d;leave=%d;reason=;", 1, 0); //2015-07-04
          strcat(szWsStatus, szTemp); //2015-07-04
        }
        else
        {
          sprintf(szTemp, "disturb=%d;leave=%d;reason=%s;", 0, pAG->m_Worker.LeaveId, pAG->m_Worker.UnicodeLeaveReason); //2015-07-04
          strcat(szWsStatus, szTemp); //2015-07-04
        }
      }

      nChn = pAG->GetSeatnChn();
      if (pBoxchn->isnChnAvail(nChn))
      {
        if (pChn->CallInOut == 1)
        {
          sprintf(szTemp, "callerno=%s;direct=%d;", pChn->CallerNo, 1); //2015-07-04
          strcat(szWsStatus, szTemp); //2015-07-04
        }
        else if (pChn->CallInOut == 2)
        {
          if (g_nSwitchType > 0 || pChn->lgChnType == 1)
            sprintf(szTemp, "callerno=%s;direct=%d;", pChn->CalledNo, 2); //2015-07-04
          else
          {
            sprintf(szTemp, "callerno=%s;direct=%d;", pChn->CallerNo, 1); //2015-08-13
          }
          strcat(szWsStatus, szTemp); //2015-07-04
        }
      }
      strcpy(szTemp, "wsclientid="); //2015-07-04
      strcat(szTemp, pAG1->GetSeatNo().C_Str()); //2015-07-04
      strcat(szTemp, ";"); //2015-07-04
      strcat(szTemp, szWsStatus); //2015-07-04
      SendMsg2WS(WebSocketClientId, 0, szTemp, false); //2016-01-02
    }
  }
}
void SendAGSelfState(int nAG)
{
  SendAGStateToAG1(nAG, nAG);
}
void SendAllAGStateToOne(int nAG)
{
  for (int nAG1 = 0; nAG1 < pAgentMng->MaxUseNum; nAG1 ++)
  {
    SendAGStateToAG1(nAG1, nAG);
  }
}
void SendOneAGStateToAll(int nAG1)
{
  //wsclientid=201;cmd=onagentstatus;seatno=201; workerno=話務員工號; workername=話務員姓名; groupno=登錄的話務員組號; seatip=坐席IP;logtime=登錄時間;svstate=電話狀態;disturb=示忙示閑;leave=在席離席
  char szTemp[1024], szLogTime[32], sqls[1024], szWsParam[1024], szWsStatus[1024]; //2015-07-04
  int nChn;

  if (pAG1->ExtnType == 0)
    return;

  memset(sqls, 0, 1024);
  memset(szWsParam, 0, 1024);
  memset(szWsStatus, 0, 1024); //2015-07-04
  strcpy(sqls, "exec sp_UpdateSeatState ");
  strcpy(szWsParam, "<AgentState>");

  Set_IVR2AG_Header(AGMSG_ongetagentstatus, 0);
  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 1, nAG1);
  Set_IVR2AG_Item(AGMSG_ongetagentstatus, 2, pAG1->GetSeatNo());
  
  sprintf(szTemp, "'%s',", pAG1->GetSeatNo().C_Str());
  strcat(sqls, szTemp);
  sprintf(szTemp, "<SeatNo>%s</SeatNo>", pAG1->GetSeatNo().C_Str());
  strcat(szWsParam, szTemp);

  strcat(szWsStatus, "cmd=onagentstatus;seatno="); //2015-07-04
  strcat(szWsStatus, pAG1->GetSeatNo().C_Str()); //2015-07-04
  strcat(szWsStatus, ";"); //2015-07-04
  
  if (pAG1->GetClientId() == 0x06FF)
  {
    Set_IVR2AG_Item(AGMSG_ongetagentstatus, 3, "");
    strcat(sqls, "'',");
    strcat(szWsParam, "<ClientId></ClientId>");
  }
  else
  {
    if (pAG1->GetClientId() & 0x8000)
    {
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 3, pAG1->GetClientId()&0x7FFF);
      sprintf(szTemp, "'%d',", pAG1->GetClientId()&0x7FFF);
      strcat(sqls, szTemp);
      sprintf(szTemp, "<ClientId>%d</ClientId>", pAG1->GetClientId()&0x7FFF);
      strcat(szWsParam, szTemp);
    } 
    else
    {
      Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 3, pAG1->GetClientId()&0x00FF);
      sprintf(szTemp, "'%d',", pAG1->GetClientId()&0x00FF);
      strcat(sqls, szTemp);
      sprintf(szTemp, "<ClientId>%d</ClientId>", pAG1->GetClientId()&0x00FF);
      strcat(szWsParam, szTemp);
    }
  }
  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 4, pAG1->GetSeatType());
  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 5, pAG1->GetSeatGroupNo());
  
  if (pAG1->m_Seat.BandIP.GetLength() == 0)
  {
    if (pAG1->m_Seat.SeatIP.GetLength() == 0)
    {
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, "");
      strcat(sqls, "'',");
      strcat(szWsParam, "<SeatIP></SeatIP>");
    }
    else
    {
      sprintf(szTemp, "SeatIP=%s", pAG1->m_Seat.SeatIP.C_Str());
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, szTemp);
      sprintf(szTemp, "'%s',", pAG1->m_Seat.SeatIP.C_Str());
      strcat(sqls, szTemp);
      sprintf(szTemp, "<SeatIP>%s</SeatIP>", pAG1->m_Seat.SeatIP.C_Str());
      strcat(szWsParam, szTemp);
    }
  } 
  else
  {
    if (pAG1->m_Seat.SeatIP.GetLength() == 0)
    {
      sprintf(szTemp, "BandIP=%s;PhoneIP=%s", pAG1->m_Seat.BandIP.C_Str(), pAG1->m_Seat.IPPhoneBandIP.C_Str());
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, szTemp);
      strcat(sqls, "'',");
      strcat(szWsParam, "<SeatIP></SeatIP>");
    }
    else
    {
      sprintf(szTemp, "BandIP=%s;SeatIP=%s;PhoneIP=%s", pAG1->m_Seat.BandIP.C_Str(), pAG1->m_Seat.SeatIP.C_Str(), pAG1->m_Seat.IPPhoneBandIP.C_Str());
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 6, szTemp);
      sprintf(szTemp, "'%s',", pAG1->m_Seat.SeatIP.C_Str());
      strcat(sqls, szTemp);
      sprintf(szTemp, "<SeatIP>%s</SeatIP>", pAG1->m_Seat.SeatIP.C_Str());
      strcat(szWsParam, szTemp);
    }
  }

  strcat(szWsStatus, "seatip="); //2015-07-04
  strcat(szWsStatus, pAG1->m_Seat.SeatIP.C_Str()); //2015-07-04
  strcat(szWsStatus, ";"); //2015-07-04

  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 7, pAG1->GetWorkerNo());
  sprintf(szTemp, "'%d',", pAG1->GetWorkerNo());
  strcat(sqls, szTemp);
  sprintf(szTemp, "<WorkerNo>%d</WorkerNo>", pAG1->GetWorkerNo());
  strcat(szWsParam, szTemp);

  strcat(szWsStatus, "workerno="); //2015-07-04
  strcat(szWsStatus, pAG1->GetWorkerNoStr()); //2015-07-04
  strcat(szWsStatus, ";"); //2015-07-04
  
  if (pAG1->GetWorkerNo() == 0)
  {
    if (pAG1->m_Worker.LogoutTime == 0)
    {
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 8, "");
      strcat(sqls, "'',");
      strcat(szWsParam, "<LoginTime></LoginTime>");

      strcat(szWsStatus, "logtime="); //2015-07-04
      strcat(szWsStatus, ""); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04
    }
    else
    {
      strcpy(szLogTime, time_t2str(pAG1->m_Worker.LogoutTime));
      Set_IVR2AG_Item(AGMSG_ongetagentstatus, 8, szLogTime);
      sprintf(szTemp, "'%s',", szLogTime);
      strcat(sqls, szTemp);
      sprintf(szTemp, "<LoginTime>%s</LoginTime>", szLogTime);
      strcat(szWsParam, szTemp);

      strcat(szWsStatus, "logtime="); //2015-07-04
      strcat(szWsStatus, szLogTime); //2015-07-04
      strcat(szWsStatus, ";"); //2015-07-04
    }
  } 
  else
  {
    strcpy(szLogTime, time_t2str(pAG1->m_Worker.LoginTime));
    Set_IVR2AG_Item(AGMSG_ongetagentstatus, 8, szLogTime);
    sprintf(szTemp, "'%s',", szLogTime);
    strcat(sqls, szTemp);
    sprintf(szTemp, "<LoginTime>%s</LoginTime>", szLogTime);
    strcat(szWsParam, szTemp);
  
    strcat(szWsStatus, "logtime="); //2015-07-04
    strcat(szWsStatus, szLogTime); //2015-07-04
    strcat(szWsStatus, ";"); //2015-07-04
  }

  Set_IVR2AG_Item(AGMSG_ongetagentstatus, 9, pAG1->GetWorkerName());
  sprintf(szTemp, "'%s',", pAG1->GetWorkerName().C_Str());
  strcat(sqls, szTemp);
  sprintf(szTemp, "<WorkerName>%s</WorkerName>", pAG1->GetWorkerName().C_Str());
  strcat(szWsParam, szTemp);

  strcat(szWsStatus, "workername="); //2015-07-04
  strcat(szWsStatus, pAG1->m_Worker.UnicodeWorkerName); //2015-07-04
  strcat(szWsStatus, ";"); //2015-07-04
  
  Set_IVR2AG_Item(AGMSG_ongetagentstatus, 10, pAgentMng->GetGroupNoList(nAG1)); //pAG1->m_Worker.GetGroupNoList());
  sprintf(szTemp, "'%s',", pAG1->m_Worker.GetGroupNoList());
  strcat(sqls, szTemp);
  sprintf(szTemp, "<GroupNo>%s</GroupNo>", pAG1->m_Worker.GetGroupNoList());
  strcat(szWsParam, szTemp);
  
  strcat(szWsStatus, "groupno="); //2015-07-04
  strcat(szWsStatus, pAG1->m_Worker.GetGroupNoList()); //2015-07-04
  strcat(szWsStatus, ";"); //2015-07-04

  if (pAG1->DelayState == 0 || pAG1->svState > 0)
  {
    Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 11, pAG1->svState);
    sprintf(szTemp, "%d,", pAG1->svState);
    strcat(sqls, szTemp);
    sprintf(szTemp, "<SvState>%d</SvState>", pAG1->svState);
    strcat(szWsParam, szTemp);

    if (pAG1->m_Worker.DisturbId == 2 && (pAG1->svState == 0 || pAG1->svState == AG_SV_ACW)) //2016-03-31
      sprintf(szTemp, "svstate=%d;", AG_SV_WAIT_RETURN); //2016-03-31
    else
      sprintf(szTemp, "svstate=%d;", pAG1->svState); //2015-07-04
    strcat(szWsStatus, szTemp); //2015-07-04
  } 
  else if (pAG1->DelayState == 1)
  {
    Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 11, AG_SV_ACW);
    sprintf(szTemp, "%d,", AG_SV_ACW);
    strcat(sqls, szTemp);
    sprintf(szTemp, "<SvState>%d</SvState>", AG_SV_ACW);
    strcat(szWsParam, szTemp);

    if (pAG1->m_Worker.DisturbId == 2) //2016-03-31
      sprintf(szTemp, "svstate=%d;", AG_SV_WAIT_RETURN); //2016-03-31
    else
      sprintf(szTemp, "svstate=%d;", AG_SV_ACW); //2015-07-04
    strcat(szWsStatus, szTemp); //2015-07-04
  }
  else
  {
    Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 11, AG_SV_DELAY);
    sprintf(szTemp, "%d,", AG_SV_DELAY);
    strcat(sqls, szTemp);
    sprintf(szTemp, "<SvState>%d</SvState>", AG_SV_DELAY);
    strcat(szWsParam, szTemp);

    sprintf(szTemp, "svstate=%d;", AG_SV_DELAY); //2015-07-04
    strcat(szWsStatus, szTemp); //2015-07-04
  }
  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 12, pAG1->m_Worker.DisturbId);
  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 13, pAG1->m_Worker.LeaveId);
  if (pAG1->m_Worker.DisturbId == 0 && pAG1->m_Worker.LeaveId == 0)
  {
    sprintf(szTemp, "%d,", 0);
    strcat(sqls, szTemp);
    sprintf(szTemp, "<FreeType>%d</FreeType>", 0);
    strcat(szWsParam, szTemp);

    sprintf(szTemp, "%d,", 0);
    strcat(sqls, szTemp);
    sprintf(szTemp, "<LeaveType>%d</LeaveType>", 0);
    strcat(szWsParam, szTemp);

    sprintf(szTemp, "disturb=%d;leave=%d;reason=;", 0, 0); //2015-07-04
    strcat(szWsStatus, szTemp); //2015-07-04
  }
  else 
  {
    if (pAG1->m_Worker.DisturbId > 0)
    {
      sprintf(szTemp, "%d,", 1);
      strcat(sqls, szTemp);
      sprintf(szTemp, "<FreeType>%d</FreeType>", 1);
      strcat(szWsParam, szTemp);
	  
	    sprintf(szTemp, "%d,", 0);
	    strcat(sqls, szTemp);
	    sprintf(szTemp, "<LeaveType>%d</LeaveType>", 0);
	    strcat(szWsParam, szTemp);

      sprintf(szTemp, "disturb=%d;leave=%d;reason=;", pAG1->m_Worker.DisturbId, 0); //2016-03-31
      strcat(szWsStatus, szTemp); //2015-07-04
    }
    else
    {
      sprintf(szTemp, "%d,", 2);
      strcat(sqls, szTemp);
      sprintf(szTemp, "<FreeType>%d</FreeType>", 2);
      strcat(szWsParam, szTemp);
	  
	    sprintf(szTemp, "%d,", pAG1->m_Worker.LeaveId);
	    strcat(sqls, szTemp);
	    sprintf(szTemp, "<LeaveType>%d</LeaveType>", pAG1->m_Worker.LeaveId);
	    strcat(szWsParam, szTemp);

      sprintf(szTemp, "disturb=%d;leave=%d;reason=%s;", 0, pAG1->m_Worker.LeaveId, pAG1->m_Worker.UnicodeLeaveReason); //2015-07-04
      strcat(szWsStatus, szTemp); //2015-07-04
    }
  }
  sprintf(szTemp, "'%s',", pAG1->StateChangeTime);
  strcat(sqls, szTemp);
  sprintf(szTemp, "<ChangeTime>%s</ChangeTime>", pAG1->StateChangeTime);
  strcat(szWsParam, szTemp);
  nChn = pAG1->GetSeatnChn();
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->CallInOut == 1)
    {
      sprintf(szTemp, "'%s',1,'%s'", pChn->CallerNo, pAG1->m_Seat.IVRPhone);
      strcat(sqls, szTemp);
      sprintf(szTemp, "<CallPhone>%s</CallPhone><CallDirect>1</CallDirect><IVRPhone>%s</IVRPhone></AgentState>", pChn->CallerNo, pAG1->m_Seat.IVRPhone);
      strcat(szWsParam, szTemp);

      sprintf(szTemp, "callerno=%s;direct=%d;", pChn->CallerNo, 1); //2015-07-04
      strcat(szWsStatus, szTemp); //2015-07-04
    } 
    else if (pChn->CallInOut == 2)
    {
      if (g_nSwitchType > 0 || pChn->lgChnType == 1)
      {
        sprintf(szTemp, "'%s',2,''", pChn->CalledNo);
        strcat(sqls, szTemp);
        sprintf(szTemp, "<CallPhone>%s</CallPhone><CallDirect>2</CallDirect><IVRPhone></IVRPhone></AgentState>", pChn->CalledNo);
        strcat(szWsParam, szTemp);
        
        sprintf(szTemp, "callerno=%s;direct=%d;", pChn->CalledNo, 2); //2015-07-04
        strcat(szWsStatus, szTemp); //2015-07-04
      }
      else
      {
        sprintf(szTemp, "'%s',1,''", pChn->CallerNo);
        strcat(sqls, szTemp);
        sprintf(szTemp, "<CallPhone>%s</CallPhone><CallDirect>1</CallDirect><IVRPhone></IVRPhone></AgentState>", pChn->CallerNo);
        strcat(szWsParam, szTemp);
        
        sprintf(szTemp, "callerno=%s;direct=%d;", pChn->CallerNo, 1); //2015-07-04
        strcat(szWsStatus, szTemp); //2015-07-04
      }
    }
    else
    {
      strcat(sqls, "'',0,''");
      strcat(szWsParam, "<CallPhone></CallPhone><CallDirect>0</CallDirect><IVRPhone></IVRPhone></AgentState>");
    }
  }
  else
  {
    strcat(sqls, "'',0,''");
    strcat(szWsParam, "<CallPhone></CallPhone><CallDirect>0</CallDirect><IVRPhone></IVRPhone></AgentState>");
  }

  if (pIVRCfg->isUpdateSeatStateToDB == true)
    SendExecSql2DB(sqls);
  if (pIVRCfg->isSendSeatStateToGW == true && pTcpLink->IsGWConnected())
    SendQueryWebService2GW(szWsParam);

  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 14, pAG1->m_Worker.WorkerCallCountParam.CallInAnsCount);
  Set_IVR2AG_IntItem(AGMSG_ongetagentstatus, 15, pAG1->m_Worker.WorkerCallCountParam.CallInNoAnsCount);
  
  memset(szTemp, 0, 128);
  if (pAG1->m_Worker.DisturbId == 3)
  {
    if (pAG1->m_Seat.SeatLangID == 1)
      sprintf(szTemp, "外出值班電話:%s", pAG1->m_Worker.DutyPhone.C_Str());
    else if (pAG1->m_Seat.SeatLangID == 2)
      sprintf(szTemp, "���|�G痁?杠:%s", pAG1->m_Worker.DutyPhone.C_Str());
    else
      sprintf(szTemp, "GoOutDutyTel:%s", pAG1->m_Worker.DutyPhone.C_Str());
  }
  else
  {
    sprintf(szTemp, "%s", pAG1->m_Worker.LeaveReason.C_Str());
  }
  if (pAG1->LockId == 1)
  {
    strcat(szTemp, "(Locked)");
  }
  Set_IVR2AG_Item(AGMSG_ongetagentstatus, 16, szTemp);
  
  nChn = pAG1->m_Seat.nChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 17, pChn->lgChnType);
    Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 18, pChn->lgChnNo);
  }
  else
  {
    Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 17, 0);
    Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 18, 0);
  }
  Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 19, pAG1->m_Worker.AccountNo);
  Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 20, pAG1->m_Worker.DepartmentNo);

  nChn = pAG1->GetSeatnChn();
  memset(szTemp, 0, 128);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pAG1->svState != 0)
    {
      if (pChn->CallInOut == 1)
      {
        sprintf(szTemp, "%d,%s,%s,%d,", pChn->CallInOut, pChn->CallerNo, time_t2str(pChn->CallTime), pAG1->m_Worker.WorkerCallCountParam.CurStatusStartTime);
      }
      else if (pChn->CallInOut == 2)
      {
        sprintf(szTemp, "%d,%s,%s,%d,", pChn->CallInOut, pChn->CalledNo, time_t2str(pChn->CallTime), pAG1->m_Worker.WorkerCallCountParam.CurStatusStartTime);
      }
      else
      {
        sprintf(szTemp, "0,,,%d,", pAG1->m_Worker.WorkerCallCountParam.CurStatusStartTime);
      }
    }
    else
    {
      sprintf(szTemp, "0,,,%d,", pAG1->m_Worker.WorkerCallCountParam.CurStatusStartTime);
    }
  }
  else
  {
    sprintf(szTemp, "0,,,%d,", pAG1->m_Worker.WorkerCallCountParam.CurStatusStartTime);
  }
  Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 21, szTemp);
  
  Set_IVR2AG_Tail();

  if (pIVRCfg->isOnlySendMySeatStatus == false) //2015-10-04
  {
    if (WebSocketLogId == true)
    {
      strcpy(szTemp, "wsclientid=allclient;");
      strcat(szTemp, szWsStatus);
      SendMsg2WS(WebSocketClientId, 2, szTemp, false);
    }
  }

  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isLogin() == true && pAG->isTcpLinked() == true && (pAG->m_Worker.GetAGState == 1 || nAG == nAG1))
      SendMsg2AG(pAG->GetClientId(), AGMSG_ongetagentstatus, IVRSndMsg.GetBuf());
    
    if (pAG->isLogin() == true &&  //2015-07-04
      pIVRCfg->isOnlySendMySeatStatus == true && ((pAG->m_Worker.GetAGState == 1 && pAG->m_Worker.WorkerGrade > 2) ||
      nAG == nAG1))
    {
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        strcpy(szTemp, "wsclientid="); //2015-07-04
        strcat(szTemp, pAG->GetSeatNo().C_Str()); //2015-07-04
        strcat(szTemp, ";"); //2015-07-04
        strcat(szTemp, szWsStatus); //2015-07-04
        SendMsg2WS(WebSocketClientId, 0, szTemp, false); //2016-01-02
      }
    }
  }

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAgStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongetagentstatus, IVRSndMsg.GetBuf());
    }
  }
}
void DispAgentStatus(int nAG)
{
  SendOneAGStateToAll(nAG);
  Send_AGState_To_REC(nAG);
}
//發送話務員登錄信息 nState: 21-話務員登錄 22-話務員退出
void SendWorkerLoginStateToDesCTI(int nAG, int nState)
{
  if (g_nMultiRunMode == 0)
    return;
  if (isMyselfInActive() == false)
    return;
  if (GetCTIHALinkFlag() == false)
    return;
  
  char szMsgBuf[512];
  sprintf(szMsgBuf, "<onaglogindata agno='%d' seatno='%s' workerno='%d' workername='%s' groupno='%s' groupname='%s' dutyno='%d' svstate='%d' disturbid='%d' leaveid='%d' reason='%s' callmsg=''/>",
    nAG, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo(), pAG->GetWorkerName().C_Str(), pAG->GetWorkerNoStr(), "", pAG->m_Worker.DutyNo, nState, pAG->m_Worker.DisturbId, pAG->m_Worker.LeaveId, pAG->m_Worker.LeaveReason.C_Str());
  if (g_nHostType == 0)
  {
    pTCPHALink->SendMessage2CLIENT(StandbyIVRClientId, (MSGTYPE_HAIVR<<8) | HAMSG_onaglogindata, szMsgBuf);
    //MyTrace(2, "0x%08x %s", StandbyIVRClientId, szMsgBuf);
  } 
  else
  {
    pTCPHALink->SendMessage2Master(HAMSG_onaglogindata, szMsgBuf);
    //MyTrace(2, "0x%08x %s", pTCPHALink->MasterIVRServer.Serverid, szMsgBuf);
  }
}
void SendAllAGLoginStateToDesCTI()
{
  if (g_nMultiRunMode == 0)
    return;
  if (GetCTIHALinkFlag() == false)
    return;
  if (isMyselfInActive() == false)
    return;
  
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->m_Worker.isLogin())
    {
      SendWorkerLoginStateToDesCTI(nAG, 21);
    }
    else
    {
      SendWorkerLoginStateToDesCTI(nAG, 22);
    }
  }
}
//發送通道狀態消息
void SendChStateToAgent(int nChn, int nAG)
{
  Set_IVR2AG_Header(AGMSG_ongetchnstatus, 0);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 1, nChn);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 2, pChn->ChStyle);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 3, pChn->ChStyleIndex);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 4, pChn->lgChnType);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 5, pChn->lgChnNo);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 6, pChn->hwState);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 7, pChn->lnState);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 8, pChn->ssState);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 9, pChn->CallInOut);
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 10, pChn->CallerNo);
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 11, pChn->CalledNo);
  //呼叫時間
  if (pChn->CallTime == 0)
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 12, "");
  }
  else
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 12, time_t2str(pChn->CallTime));
  }
  //應答時間
  if (pChn->AnsTime == 0)
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 13, "");
  }
  else
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 13, time_t2str(pChn->AnsTime));
  }
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 14, pChn->FlwState);
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 15, pChn->DeviceID);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_ongetchnstatus, IVRSndMsg.GetBuf());
}
void SendAllCHStateToOne(int nAG)
{
  int nChn;
  
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (pAG->isLogin() == true && pAG->isTcpLinked() == true && pAG->m_Worker.GetChState == 1)
    {
      for (nChn = 0; nChn < pBoxchn->GetMaxChnNum(); nChn ++)
      {
        SendChStateToAgent(nChn, nAG);
      }
    }
  }
}
void SendOneCHStateToAll(int nChn)
{
  int nAG;
  char szTemp[128];
  
  Set_IVR2AG_Header(AGMSG_ongetchnstatus, 0);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 1, nChn);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 2, pChn->ChStyle);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 3, pChn->ChStyleIndex);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 4, pChn->lgChnType);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 5, pChn->lgChnNo);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 6, pChn->hwState);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 7, pChn->lnState);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 8, pChn->ssState);
  Set_IVR2AG_IntItem(AGMSG_ongetchnstatus, 9, pChn->CallInOut);
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 10, pChn->CallerNo);
  //2016-11-22
  if (pChn->OrgCalledNo.GetLength() > 0)
  {
    sprintf(szTemp, "%s,%s", pChn->CalledNo, pChn->OrgCalledNo.C_Str());
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 11, szTemp);
  }
  else
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 11, pChn->CalledNo);
  //呼叫時間
  if (pChn->CallTime == 0)
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 12, "");
  }
  else
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 12, time_t2str(pChn->CallTime));
  }
  //應答時間
  if (pChn->AnsTime == 0)
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 13, "");
  }
  else
  {
    Set_IVR2AG_Item(AGMSG_ongetchnstatus, 13, time_t2str(pChn->AnsTime));
  }
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 14, pChn->FlwState);
  Set_IVR2AG_Item(AGMSG_ongetchnstatus, 15, pChn->DeviceID);
  Set_IVR2AG_Tail();

  for (nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isLogin() == true && pAG->isTcpLinked() == true && pAG->m_Worker.GetChState == 1)
    {
      SendMsg2AG(pAG->GetClientId(), AGMSG_ongetchnstatus, IVRSndMsg.GetBuf());
    }
  }
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetChnStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongetchnstatus, IVRSndMsg.GetBuf());
    }
  }
}

int GetQueueInfo(CStringX &queueinfo, int &waitacdcount, int &waitanscount)
{
  char pszTemp[256], pszTemp1[32], pszTemp2[32];
  US nAcd1=0, Next, nAG, nChn;
  int groupno,queuenum=0, nLen;
  time_t tQueue=0;
  
  //MyTrace(3, "Start GetQueueInfo");
  memset(pszTemp, 0, 256);
  //queueinfo.Empty();
  waitacdcount = 0;
  waitanscount = 0;
  Next = pACDQueue->pACD_str[nAcd1].Next;
  while ( Next < pACDQueue->MaxAcdNum )
  {
    nAG = pACDQueue->pACD_str[Next].Calleds[0].nAG;
    if (pAgentMng->isnAGAvail(nAG))
    {
      strcpy(pszTemp1, pAG->GetSeatNo().C_Str());
    } 
    else
    {
      memset(pszTemp1, 0, 32);
    }
    if (pACDQueue->pACD_str[Next].ACDRule.AcdedGroupNo > 0)
    {
      groupno = pACDQueue->pACD_str[Next].ACDRule.AcdedGroupNo;
    } 
    else
    {
      groupno = pACDQueue->pACD_str[Next].ACDRule.GroupNo[0];
    }
    strcpy(pszTemp2, pACDQueue->pACD_str[Next].Session.CallerNo.C_Str());
    nChn = pACDQueue->pACD_str[Next].Session.nChn;
    if (pBoxchn->isnChnAvail(nChn))
    {
      tQueue = pChn->IVRInTranAGTime;
      if (strlen(pszTemp2) == 0)
      {
        strcpy(pszTemp2, pChn->CustPhone); //2015-12-14
      }
    } 
    if (tQueue == 0)
    {
      tQueue = pACDQueue->pACD_str[Next].ACDRule.QueueTime;
    }
    nLen = (int)(time(0) - tQueue);
    sprintf(pszTemp,"%d,%s,%s,%d,%s,%d,%s,%d;", 
      Next, pszTemp2, 
      time_t2HourMinstr(pACDQueue->pACD_str[Next].ACDRule.QueueTime),
      pACDQueue->pACD_str[Next].acdState,
      pszTemp1, groupno,
      time_t2HourMinstr1(tQueue), nLen);
    queueinfo += pszTemp;
    
    if (pACDQueue->pACD_str[Next].acdState == 0 || pACDQueue->pACD_str[Next].acdState == 3)
    {
      waitacdcount ++;
    }
    else if (pACDQueue->pACD_str[Next].acdState == 2 || pACDQueue->pACD_str[Next].acdState == 1)
    {
      waitanscount ++;
    }
    
    queuenum++;
    
    if (queueinfo.GetLength() >= 512)
    {
      break;
    }
    Next = pACDQueue->pACD_str[Next].Next;
  }
  pACDQueue->CallerList = queueinfo;
  //MyTrace(3, "End GetQueueInfo");
  return queuenum;
}
//發送ACD分配簡單隊列信息
void SendACDQueueInfo(int nAG)
{
  int queunum, waitacdcount, waitanscount;
  CStringX queueinfo;

  if (pIVRCfg->isOnlySendACDQueueToLeader == true && pAG->m_Worker.WorkerGrade < 3)
    return;
  if (pAG->isLogin() == true && pAG->isTcpLinked() == true && pAG->m_Worker.GetWaitQueue == 1)
  {
    queunum = GetQueueInfo(queueinfo, waitacdcount, waitanscount);
    Set_IVR2AG_Header(AGMSG_ongetqueueinfo, pAG->GetClientId()<<16);
    Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 1, queunum);
    Set_IVR2AG_Item(AGMSG_ongetqueueinfo, 2, queueinfo);
    Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 3, waitacdcount);
    Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 4, waitanscount);
    Set_IVR2AG_Tail();
    SendMsg2AG(pAG->GetClientId(), AGMSG_ongetqueueinfo, IVRSndMsg.GetBuf());
  }
  if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
  {
    char szTemp[2048];
    sprintf(szTemp, "wsclientid=%s;cmd=queueinfo;queuenum=%d;waitacdnum=%d;waitansnum=%d;queueinfo=%s'", 
      pAG->m_Seat.WSClientId, queunum, waitacdcount, waitanscount, queueinfo.C_Str());
    SendMsg2WS(WebSocketClientId, 2, szTemp, false);
  }
}
void SendACDQueueInfoToAll()
{
  int queunum, waitacdcount, waitanscount;
  CStringX queueinfo;

  queunum = GetQueueInfo(queueinfo, waitacdcount, waitanscount);
  Set_IVR2AG_Header(AGMSG_ongetqueueinfo, 0);
  Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 1, queunum);
  Set_IVR2AG_Item(AGMSG_ongetqueueinfo, 2, queueinfo);
  Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 3, waitacdcount);
  Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 4, waitanscount);
  Set_IVR2AG_Tail();

  if (pIVRCfg->isUpdateACDQueueToDB == true)
  {
    char sqls[1024];

    sprintf(sqls, "exec sp_UpdateACDQueue '0','%d','%d','%d','%s'", queunum, waitacdcount, waitanscount, queueinfo.C_Str());
    SendExecSql2DB(sqls);
  }

  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isLogin() == true && pAG->isTcpLinked() == true && pAG->m_Worker.GetWaitQueue == 1 && (pIVRCfg->isOnlySendACDQueueToLeader == false || pAG->m_Worker.WorkerGrade > 2))
    {
      SendMsg2AG(pAG->GetClientId(), AGMSG_ongetqueueinfo, IVRSndMsg.GetBuf());
    }
  }
  
  if (WebSocketLogId == true)
  {
    char szTemp[2048];
    sprintf(szTemp, "wsclientid=allclient;cmd=queueinfo;queuenum=%d;waitacdnum=%d;waitansnum=%d;queueinfo=%s'", queunum, waitacdcount, waitanscount, queueinfo.C_Str());
    SendMsg2WS(WebSocketClientId, 2, szTemp, false);
  }

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAcdStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongetqueueinfo, IVRSndMsg.GetBuf());
    }
  }
  Send_ACDState_To_REC(queunum, waitacdcount, waitanscount, queueinfo.C_Str());
}

void SendACDQueueInfoToAll(int queunum, CStringX queueinfo)
{
  Set_IVR2AG_Header(AGMSG_ongetqueueinfo, 0);
  Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 1, queunum);
  Set_IVR2AG_Item(AGMSG_ongetqueueinfo, 2, queueinfo);
  Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 3, queunum);
  Set_IVR2AG_IntItem(AGMSG_ongetqueueinfo, 4, 0);
  Set_IVR2AG_Tail();
  
  if (pIVRCfg->isUpdateACDQueueToDB == true)
  {
    char sqls[1024];
    
    sprintf(sqls, "exec sp_UpdateACDQueue '0','%d','%d','%d','%s'", queunum, queunum, 0, queueinfo.C_Str());
    SendExecSql2DB(sqls);
  }
  
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isLogin() == true && pAG->isTcpLinked() == true && pAG->m_Worker.GetWaitQueue == 1 && (pIVRCfg->isOnlySendACDQueueToLeader == false || pAG->m_Worker.WorkerGrade > 2))
    {
      SendMsg2AG(pAG->GetClientId(), AGMSG_ongetqueueinfo, IVRSndMsg.GetBuf());
    }
  }
  if (WebSocketLogId == true)
  {
    char szTemp[2048];
    sprintf(szTemp, "wsclientid=allclient;cmd=queueinfo;queuenum=%d;waitacdnum=%d;waitansnum=%d;queueinfo=%s'", queunum, queunum, 0, queueinfo.C_Str());
    SendMsg2WS(WebSocketClientId, 2, szTemp, false);
  }
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAcdStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongetqueueinfo, IVRSndMsg.GetBuf());
    }
  }
  Send_ACDState_To_REC(queunum, queunum, 0, queueinfo.C_Str());
}

//設置并顯示坐席服務狀態
void SetAgentsvState(CAgent *pAgent, int svstate)
{
  if (pAgent == NULL)
    return;
  pAgent->SetAgentsvState(svstate);
  DispAgentStatus(pAgent->nAG);
}
void SetAgentsvState(int nAG, int svstate)
{
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  pAG->SetAgentsvState(svstate);
  DispAgentStatus(nAG);
}

//根據工號取坐席,返回值nResult：0-表示沒有該工號 1-表示有該工號且處于空閑狀態 2-表示有該工號但處于非空閑狀態
CAgent *GetAgentbyWorkerNo(int WorkerNo, int &nResult)
{
  if (WorkerNo > 0)
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (pAgentMng->m_Agent[nAG]->m_Worker.isWantWorkerNo(WorkerNo))
      {
        if (pAgentMng->m_Agent[nAG]->isIdelForACD())
          nResult = 1;
        else
          nResult = 2;
        return pAgentMng->m_Agent[nAG];
      }
    }
  }
  nResult = 0;
  return NULL;
}

//根據坐席號取坐席,返回值nResult：0-表示沒有該坐席號 1-表示有該坐席號且處于空閑狀態 2-表示有該坐席號但處于非空閑狀態
CAgent *GetAgentbySeatNo(const CStringX SeatNo, int &nResult)
{
  if (SeatNo.Compare("0") != 0)
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (pAgentMng->m_Agent[nAG]->isWantSeatNo(SeatNo.C_Str()))
      {
        if (pAgentMng->m_Agent[nAG]->isIdelForACD())
          nResult = 1;
        else
          nResult = 2;
        return pAgentMng->m_Agent[nAG];
      }
    }
  }
  nResult = 0;
  return NULL;
}
CAgent *GetAgentbyBandIP(const CStringX BandIP, int &nResult)
{
  if (CheckIPv4Address(BandIP.C_Str()) == 0)
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (pAgentMng->m_Agent[nAG]->isWantBandIP(BandIP.C_Str()))
      {
        if (pAgentMng->m_Agent[nAG]->isIdelForACD())
          nResult = 1;
        else
          nResult = 2;
        return pAgentMng->m_Agent[nAG];
      }
    }
  }
  nResult = 0;
  return NULL;
}
//根據遠端坐席外線號碼取坐席,返回值nResult：0-表示沒有該座席 1-表示有該座席且處于空閑狀態 2-表示有該座席但處于非空閑狀態
CAgent *GetAgentbyPhone(const CStringX PhoneNo, int &nResult)
{
  if (PhoneNo.GetLength() > 0)
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (pAgentMng->m_Agent[nAG]->isWantPhoneNo(PhoneNo.C_Str()))
      {
        if (pAgentMng->m_Agent[nAG]->isIdelForACD())
          nResult = 1;
        else
          nResult = 2;
        return pAgentMng->m_Agent[nAG];
      }
    }
  }
  nResult = 0;
  return NULL;
}
//根據工號或座席號取坐席,返回值nResult：0-表示沒有該座席 1-表示有該座席且處于空閑狀態 2-表示有該座席但處于非空閑狀態
CAgent *GetAgentbyWorkerNoSeatNo(const CStringX SeatNo, int WorkerNo, int acdrule, int &nResult, bool bOnlyPC)
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (bOnlyPC == true)
    {
      if (pAgentMng->m_Agent[nAG]->GetClientId() == 0x06FF)
        continue;
    }
    if (pAgentMng->m_Agent[nAG]->m_Seat.isWantSeatNo(SeatNo.C_Str()) && SeatNo.Compare("0") != 0)
    {
      if ((acdrule == 0 && pAgentMng->m_Agent[nAG]->m_Seat.isIdelForACD()) //呼叫指定坐席號時,acdrule=0時不判斷話務員是否可分配電話
        || (acdrule > 0 && pAgentMng->m_Agent[nAG]->isIdelForACD())) //呼叫指定坐席號時,acdrule=1時判斷話務員是否可分配電話
        nResult = 3; //表示呼叫指定的坐席號
      else
        nResult = 2;
      return pAgentMng->m_Agent[nAG];
    }
    else if (WorkerNo > 0 && pAgentMng->m_Agent[nAG]->m_Worker.isWantWorkerNo(WorkerNo))
    {
      MyTrace(3, "GetAgentbyWorkerNoSeatNo nAG=%d DisturbId=%d LeaveId=%d svState=%d DelayState=%d", pAG->m_Worker.DisturbId, pAG->m_Worker.LeaveId, pAG->svState, pAG->DelayState); //2016-06-17
      if (pAgentMng->m_Agent[nAG]->isIdelForACD())
        nResult = 1;
      else
      {
        if (acdrule == 7 && pAgentMng->m_Agent[nAG]->m_Worker.DisturbId == 2)
        {
          nResult = 1;
          //pAgentMng->m_Agent[nAG]->m_Worker.DisturbId = 0; //2016-06-17 這里不應該清0，不然如果IVR返回轉坐席不應答，后面就轉不進來了
        }
        else
        {
          nResult = 2;
        }
      }
      return pAgentMng->m_Agent[nAG];
    }
  }
  nResult = 0;
  return NULL;
}
int GetnAGbyWorkerNoSeatNo(const CStringX SeatNo, int WorkerNo)
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAgentMng->m_Agent[nAG]->m_Seat.isWantSeatNo(SeatNo.C_Str()) && SeatNo.Compare("0") != 0)
    {
      return nAG;
    }
    else if (WorkerNo > 0 && pAgentMng->m_Agent[nAG]->m_Worker.isWantWorkerNo(WorkerNo))
    {
      return nAG;
    }
  }
  return 0xFFFF;
}
//取代接的坐席
int GetnAGForPickup(const char *seatno, int workerno, int groupno, int selectmode)
{
  int nAG;
  
  if (strlen(seatno) > 0 && strcmp(seatno, "0") != 0)
  {
    nAG = pAgentMng->GetnAGBySeatNo(seatno);
    if (pAgentMng->isnAGAvail(nAG) && pAgentMng->m_Agent[nAG]->isThesvState(AG_SV_INRING))
      return nAG;
    else
      return 0xFFFF;
  }
  else if (workerno != 0)
  {
    nAG = pAgentMng->GetnAGByWorkerNo(workerno);
    if (pAgentMng->isnAGAvail(nAG) && pAgentMng->m_Agent[nAG]->isThesvState(AG_SV_INRING))
      return nAG;
    else
      return 0xFFFF;
  }
  else if (groupno != 0)
  {
    for (nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (!pAgentMng->m_Agent[nAG]->isThesvState(AG_SV_INRING))
        continue;
      for (int i=0; i<MAX_GROUP_NUM_FORWORKER; i++)
      {
        if (pAgentMng->m_Agent[nAG]->m_Worker.GroupNo[i] == groupno)
          return nAG;
      }
    }
    return 0xFFFF;
  }
  else
  {
    for (nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (pAgentMng->m_Agent[nAG]->isThesvState(AG_SV_INRING))
        return nAG;
    }
  }
  return 0xFFFF;
}

void ResetAllAG()
{
  int nAG;
  
  for (nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
    
    pAG->delaytimer = 0;
    if (pAG->svState == AG_SV_CONN || pAG->svState == AG_SV_HOLD || pAG->svState == AG_SV_IMCHAT || pAG->svState == AG_SV_PROC_EMAIL || pAG->svState == AG_SV_PROC_FAX)
      pAG->DelayState = 1;
    pAG->SetAgentsvState(AG_SV_IDEL);
    pAG->ClearSrvLine();
    pAG->TrannChn = 0xFFFF;
    
    SendSeatHangon(pAG->nAG);
    DispAgentStatus(pAG->nAG);
  }
}
void LogoutAllAG()
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->m_Worker.WorkerCallCountParam.CurStatus != AG_STATUS_LOGOUT)
    {
      WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
      WriteWorkerLogoutStatus(pAG);
    }
    if (pAG->m_Worker.oldWorkerNo == 0)
    {
      pAG->m_Worker.Logout();
      pAG->m_Worker.ClearState();
    }
    else
    {
      pAG->m_Worker.OldWorkerLogin();
    }
    pAG->m_Worker.OldDisturbId = 0;
    pAG->m_Worker.OldLeaveId = 0;
    DispAgentStatus(nAG);
  }
}
//---------------------------ACD-----------------------------------------------
//---------------------------ACD-----------------------------------------------
//根據規則取空閑坐席,返回值：NULL-表示沒有符合規則的空閑座席 not NULL-表示返回了符合規則的空閑座席
CAgent *GetIdelAgentbyRule(CACDRule *ACDRule, int waittimer, int ringtimer, bool bOnlyPC)
{
  static short last_nAG;
  int seattype, seatgroupno, groupno, levelrule;
  int grpno, gIndex, i, j, nLevel=0, anscount=0x0FFFFFFF;
  short leastnAG=-1;
  bool findid;
  time_t tmIdelTime=time(0); //2016-02-12

  seattype = ACDRule->SeatType;
  seatgroupno = ACDRule->SeatGroupNo;
  levelrule = ACDRule->LevelRule;

  MyTrace(3, "GetIdelAgentbyRule seattype=%d seatgroupno=%d levelrule=%d waittimer=%d ringtimer=%d", 
    seattype, seatgroupno, levelrule, waittimer, ringtimer);
  for (grpno = 0; grpno < MAX_GROUP_NUM_FORWORKER; grpno ++)
  {
    if (ACDRule->GroupNo[grpno] >= MAX_AG_GROUP)
      break; //groupno = 0;  //edit 2010-10-24 沒有組號了不應該繼續往下分配
    else
      groupno = ACDRule->GroupNo[grpno];

    if (ACDRule->Level[grpno] >= MAX_AG_LEVEL)
      nLevel = 0;
    else
      nLevel = ACDRule->Level[grpno];

    MyTrace(3, "GetIdelAgentbyRule ACDRule->GroupNo[%d]=%d groupno=%d ACDRule->NowLevel[%d]=%d level=%d", 
      grpno, ACDRule->GroupNo[grpno], groupno, grpno, ACDRule->NowLevel[grpno], nLevel);
    if (levelrule == 4) //4-從Level級別開始向高級別分配
    {
		  for (nLevel = ACDRule->NowLevel[grpno]; nLevel < MAX_AG_LEVEL; nLevel++)
		  {
        switch (ACDRule->AcdRule)
        {
        case 0: //循環振鈴順序分配(每次從上次振鈴的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAllocLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                //if (pAgentMng->m_Agent[last_nAG])
                {
                  if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                    && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                  { //2015-12-07
                    findid=false;
                    for (j = 0; j < MAX_AG_NUM; j ++)
                    {
                      if (ACDRule->AcdednAGList[j] == last_nAG)
                      {
                        findid=true;
                        break;
                      }
                    }
                    if (findid == true)
                      continue;
                    leastnAG = last_nAG;
                    MyTrace(3, "GetIdelAgentbyRule last_nAG=%d", last_nAG);
                    break;
                  }
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              pAgentMng->lastAllocnAG[groupno] = last_nAG;
              pAgentMng->lastAllocLevelGroupIndexnAG[groupno][nLevel][gIndex-1] = last_nAG;
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[last_nAG];
            }
          }
          break;
        case 1: //固定振鈴順序分配(每次從第1個開始振鈴）
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = -1;
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                //if (pAgentMng->m_Agent[last_nAG])
                {
                  if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                    && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                  {
                    ACDRule->AcdedGroupNo = groupno;
                    ACDRule->AcdedGroupIndex = gIndex-1;
                    ACDRule->AcdedLevel = nLevel;
                    return pAgentMng->m_Agent[last_nAG];
                  }
                }
              }
            }
          }
          break;
        case 2: //循環應答順序分配(每次從上次應答的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAnsLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                //if (pAgentMng->m_Agent[last_nAG])
                {
                  //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d gIndex=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d", 
                  //  last_nAG, gIndex, seattype, seatgroupno, groupno, nLevel, levelrule);
              
                  if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                    && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                  {
                    findid=false;
                    for (j = 0; j < MAX_AG_NUM; j ++)
                    {
                      if (ACDRule->AcdednAGList[j] == last_nAG)
                      {
                        findid=true;
                        break;
                      }
                    }
                    if (findid == true)
                      continue;
                    leastnAG = last_nAG;
                    MyTrace(3, "GetIdelAgentbyRule last_nAG=%d", last_nAG);
                    break;
                  }
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[last_nAG];
            }
          }
          break;
        case 3: //優先分配應答次數最少的座席 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatAnsCount=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount());

                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=3 last_nAG=%d", last_nAG);

                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatAnsCount=%d", 
                  //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount());

                  anscount = pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 4: //優先分配服務時長最少的座席 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatSrvTimeLen=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen());
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=4 last_nAG=%d", 
                  //  last_nAG);
              
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=4 last_nAG=%d finded", 
                  //  last_nAG);
                  anscount = pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 5: //優先分配應答次數最少的話務員 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerAnsCount=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount());
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=5 last_nAG=%d", 
                  //  last_nAG);
              
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=5 last_nAG=%d finded", 
                  //  last_nAG);
                  anscount = pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 6: //優先分配服務時長最少的話務員 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerSrvTimeLen=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen());
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d", 
                  //  last_nAG);
              
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d finded", 
                  //  last_nAG);
                  anscount = pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 8: //閑置時間最長的優先 //2016-02-12
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerSrvTimeLen=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen());
                
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->m_Worker.IdelTime <= tmIdelTime)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d", 
                  //  last_nAG);
                  
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d finded", 
                  //  last_nAG);
                  tmIdelTime = pAgentMng->m_Agent[last_nAG]->m_Worker.IdelTime;
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        }
		  }
    }
    else if (levelrule == 5) //5-從Level級別開始向低級別分配
    {
      for (nLevel = ACDRule->NowLevel[grpno]; nLevel >= 1; nLevel--)
      {
        switch (ACDRule->AcdRule)
        {
        case 0: //循環振鈴順序分配(每次從上次振鈴的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAllocLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                //if (pAgentMng->m_Agent[last_nAG])
                {
                  if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                    && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                  { //2015-12-07
                    findid=false;
                    for (j = 0; j < MAX_AG_NUM; j ++)
                    {
                      if (ACDRule->AcdednAGList[j] == last_nAG)
                      {
                        findid=true;
                        break;
                      }
                    }
                    if (findid == true)
                      continue;
                    leastnAG = last_nAG;
                    MyTrace(3, "GetIdelAgentbyRule last_nAG=%d", last_nAG);
                    break;
                  }
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              pAgentMng->lastAllocnAG[groupno] = last_nAG;
              pAgentMng->lastAllocLevelGroupIndexnAG[groupno][nLevel][gIndex-1] = last_nAG;
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[last_nAG];
            }
          }
          break;
        case 1: //固定振鈴順序分配(每次從第1個開始振鈴）
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = -1;
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                //if (pAgentMng->m_Agent[last_nAG])
                {
                  if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                    && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                  {
                    ACDRule->AcdedGroupNo = groupno;
                    ACDRule->AcdedGroupIndex = gIndex-1;
                    ACDRule->AcdedLevel = nLevel;
                    return pAgentMng->m_Agent[last_nAG];
                  }
                }
              }
            }
          }
          break;
        case 2: //循環應答順序分配(每次從上次應答的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAnsLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                //if (pAgentMng->m_Agent[last_nAG])
                {
                  //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d gIndex=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d", 
                  //  last_nAG, gIndex, seattype, seatgroupno, groupno, nLevel, levelrule);
              
                  if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                    && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                  {
                    findid=false;
                    for (j = 0; j < MAX_AG_NUM; j ++)
                    {
                      if (ACDRule->AcdednAGList[j] == last_nAG)
                      {
                        findid=true;
                        break;
                      }
                    }
                    if (findid == true)
                      continue;
                    leastnAG = last_nAG;
                    MyTrace(3, "GetIdelAgentbyRule last_nAG=%d", last_nAG);
                    break;
                  }
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[last_nAG];
            }
          }
          break;
        case 3: //優先分配應答次數最少的座席 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatAnsCount=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount());

                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=3 last_nAG=%d", last_nAG);

                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatAnsCount=%d", 
                  //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount());

                  anscount = pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 4: //優先分配服務時長最少的座席 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatSrvTimeLen=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen());
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=4 last_nAG=%d", 
                  //  last_nAG);
              
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=4 last_nAG=%d finded", 
                  //  last_nAG);
                  anscount = pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 5: //優先分配應答次數最少的話務員 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerAnsCount=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount());
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=5 last_nAG=%d", 
                  //  last_nAG);
              
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=5 last_nAG=%d finded", 
                  //  last_nAG);
                  anscount = pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 6: //優先分配服務時長最少的話務員 //edit 2008-05-20 add
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerSrvTimeLen=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen());
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen() < anscount)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d", 
                  //  last_nAG);
              
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d finded", 
                  //  last_nAG);
                  anscount = pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen();
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        case 8: //閑置時間最長的優先 //2016-02-12
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerSrvTimeLen=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen());
                
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->m_Worker.IdelTime <= tmIdelTime)
                {
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d", 
                  //  last_nAG);
                  
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d finded", 
                  //  last_nAG);
                  tmIdelTime = pAgentMng->m_Agent[last_nAG]->m_Worker.IdelTime;
                  leastnAG = last_nAG;
                }
              }
              if (leastnAG >= 0)
                break;
            }
            if (leastnAG >= 0)
            {
              for (i = 0; i < MAX_AG_NUM; i ++)
              {
                if (ACDRule->AcdednAGList[i] < 0)
                {
                  ACDRule->AcdednAGList[i] = leastnAG;
                  break;
                }
              }
              ACDRule->AcdedGroupNo = groupno;
              ACDRule->AcdedGroupIndex = gIndex-1;
              ACDRule->AcdedLevel = nLevel;
              return pAgentMng->m_Agent[leastnAG];
            }
          }
          break;
        }
      }
    }
    else
    {
      switch (ACDRule->AcdRule)
      {
      case 0: //循環振鈴順序分配(每次從上次振鈴的下一個座席開始)
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            last_nAG = pAgentMng->lastAllocGroupIndexnAG[groupno][gIndex-1];
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              if (++last_nAG >= pAgentMng->MaxUseNum)
                last_nAG = 0;
              //if (pAgentMng->m_Agent[last_nAG])
              {
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
                { //2015-12-07
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  leastnAG = last_nAG;
                  MyTrace(3, "GetIdelAgentbyRule last_nAG=%d", last_nAG);
                  break;
                }
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            pAgentMng->lastAllocnAG[groupno] = last_nAG;
            pAgentMng->lastAllocGroupIndexnAG[groupno][gIndex-1] = last_nAG;
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[last_nAG];
          }
        }
        break;
      case 1: //固定振鈴順序分配(每次從第1個開始振鈴）
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            last_nAG = -1;
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              if (++last_nAG >= pAgentMng->MaxUseNum)
                last_nAG = 0;
              //if (pAgentMng->m_Agent[last_nAG])
              {
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
                {
                  ACDRule->AcdedGroupNo = groupno;
                  ACDRule->AcdedGroupIndex = gIndex-1;
                  return pAgentMng->m_Agent[last_nAG];
                }
              }
            }
          }
        }
        break;
      case 2: //循環應答順序分配(每次從上次應答的下一個座席開始)
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            last_nAG = pAgentMng->lastAnsGroupIndexnAG[groupno][gIndex-1];
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              if (++last_nAG >= pAgentMng->MaxUseNum)
                last_nAG = 0;
              //if (pAgentMng->m_Agent[last_nAG])
              {
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d gIndex=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d", 
                //  last_nAG, gIndex, seattype, seatgroupno, groupno, nLevel, levelrule);
              
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
                {
                  findid=false;
                  for (j = 0; j < MAX_AG_NUM; j ++)
                  {
                    if (ACDRule->AcdednAGList[j] == last_nAG)
                    {
                      findid=true;
                      break;
                    }
                  }
                  if (findid == true)
                    continue;
                  leastnAG = last_nAG;
                  MyTrace(3, "GetIdelAgentbyRule last_nAG=%d", last_nAG);
                  break;
                }
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[last_nAG];
          }
        }
        break;
      case 3: //優先分配應答次數最少的座席 //edit 2008-05-20 add
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
              //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatAnsCount=%d", 
              //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount());

              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount() < anscount)
              {
                //MyTrace(3, "GetIdelAgentbyRule ruleid=3 last_nAG=%d", last_nAG);

                findid=false;
                for (j = 0; j < MAX_AG_NUM; j ++)
                {
                  if (ACDRule->AcdednAGList[j] == last_nAG)
                  {
                    findid=true;
                    break;
                  }
                }
                if (findid == true)
                  continue;
                //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatAnsCount=%d", 
                //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount());

                anscount = pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount();
                leastnAG = last_nAG;
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[leastnAG];
          }
        }
        break;
      case 4: //優先分配服務時長最少的座席 //edit 2008-05-20 add
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
              //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetSeatSrvTimeLen=%d", 
              //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen());
            
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen() < anscount)
              {
                //MyTrace(3, "GetIdelAgentbyRule ruleid=4 last_nAG=%d", 
                //  last_nAG);
              
                findid=false;
                for (j = 0; j < MAX_AG_NUM; j ++)
                {
                  if (ACDRule->AcdednAGList[j] == last_nAG)
                  {
                    findid=true;
                    break;
                  }
                }
                if (findid == true)
                  continue;
                //MyTrace(3, "GetIdelAgentbyRule ruleid=4 last_nAG=%d finded", 
                //  last_nAG);
                anscount = pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen();
                leastnAG = last_nAG;
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[leastnAG];
          }
        }
        break;
      case 5: //優先分配應答次數最少的話務員 //edit 2008-05-20 add
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
              //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerAnsCount=%d", 
              //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount());
            
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount() < anscount)
              {
                //MyTrace(3, "GetIdelAgentbyRule ruleid=5 last_nAG=%d", 
                //  last_nAG);
              
                findid=false;
                for (j = 0; j < MAX_AG_NUM; j ++)
                {
                  if (ACDRule->AcdednAGList[j] == last_nAG)
                  {
                    findid=true;
                    break;
                  }
                }
                if (findid == true)
                  continue;
                //MyTrace(3, "GetIdelAgentbyRule ruleid=5 last_nAG=%d finded", 
                //  last_nAG);
                anscount = pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount();
                leastnAG = last_nAG;
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[leastnAG];
          }
        }
        break;
      case 6: //優先分配服務時長最少的話務員 //edit 2008-05-20 add
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
              //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerSrvTimeLen=%d", 
              //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen());
            
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen() < anscount)
              {
                //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d", 
                //  last_nAG);
              
                findid=false;
                for (j = 0; j < MAX_AG_NUM; j ++)
                {
                  if (ACDRule->AcdednAGList[j] == last_nAG)
                  {
                    findid=true;
                    break;
                  }
                }
                if (findid == true)
                  continue;
                //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d finded", 
                //  last_nAG);
                anscount = pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen();
                leastnAG = last_nAG;
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[leastnAG];
          }
        }
        break;
      case 8: //閑置時間最長的優先 //2016-02-12
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
              //MyTrace(3, "GetIdelAgentbyRule last_nAG=%d seattype=%d seatgroupno=%d groupno=%d nLevel=%d levelrule=%d anscount=%d GetWorkerSrvTimeLen=%d", 
              //  last_nAG, seattype, seatgroupno, groupno, nLevel, levelrule, anscount, pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen());
              
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->m_Worker.IdelTime <= tmIdelTime)
              {
                //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d", 
                //  last_nAG);
                
                findid=false;
                for (j = 0; j < MAX_AG_NUM; j ++)
                {
                  if (ACDRule->AcdednAGList[j] == last_nAG)
                  {
                    findid=true;
                    break;
                  }
                }
                if (findid == true)
                  continue;
                //MyTrace(3, "GetIdelAgentbyRule ruleid=6 last_nAG=%d finded", 
                //  last_nAG);
                tmIdelTime = pAgentMng->m_Agent[last_nAG]->m_Worker.IdelTime;
                leastnAG = last_nAG;
              }
            }
            if (leastnAG >= 0)
              break;
          }
          if (leastnAG >= 0)
          {
            for (i = 0; i < MAX_AG_NUM; i ++)
            {
              if (ACDRule->AcdednAGList[i] < 0)
              {
                ACDRule->AcdednAGList[i] = leastnAG;
                break;
              }
            }
            ACDRule->AcdedGroupNo = groupno;
            ACDRule->AcdedGroupIndex = gIndex-1;
            return pAgentMng->m_Agent[leastnAG];
          }
        }
        break;
      }
    }
  }
  return NULL;
}
void MySwap(short *AGlist, int low, int high )
{
  MyTrace(3, "MySwap low=%d high=%d", low, high);
  int temp = AGlist[low];
  AGlist[low] = AGlist[high];
  AGlist[high] = temp;
}
bool MyLitter(const int low, int high )
{
  //MyTrace(3, "MyLitter low=%d high=%d", low, high);
  if(low < high)
    return true;
  else
    return false;
}
//2016-02-12 對坐席排序：sortType排序類型（1-優先分配應答次數最少的座席 2-優先分配服務時長最少的座席 3-優先分配應答次數最少的話務員 4-優先分配服務時長最少的話務員 5-閑置時間最長的優先）
void SortAGList(short *AGlist, int n, int sortType)
{
  int i,j;
  char szTemp[1024], szTemp1[16];
  memset(szTemp, 0, 1024);
  strcpy(szTemp, "AGlist=");
  for (i=0; i<n; i++)
  {
    if (AGlist[i] < 0)
      break;
    sprintf(szTemp1, "%d,", AGlist[i]);
    strcat(szTemp, szTemp1);
  }
  MyTrace(3, "SortAGList Num=%d sortType=%d %s", n, sortType, szTemp);

  for ( i = 0; i < n-1; i++ )
  {
    for ( j = n-1; j > i; j-- )
    {
      switch (sortType)
      {
      case 1:
        if( MyLitter( pAgentMng->m_Agent[AGlist[j]]->GetSeatAnsCount(), pAgentMng->m_Agent[AGlist[j-1]]->GetSeatAnsCount() ) )
          MySwap( AGlist, j, j-1 );
        break;
      case 2:
        if( MyLitter( pAgentMng->m_Agent[AGlist[j]]->GetSeatSrvTimeLen(), pAgentMng->m_Agent[AGlist[j-1]]->GetSeatSrvTimeLen() ) )
          MySwap( AGlist, j, j-1 );
        break;
      case 3:
        if( MyLitter( pAgentMng->m_Agent[AGlist[j]]->GetWorkerAnsCount(), pAgentMng->m_Agent[AGlist[j-1]]->GetWorkerAnsCount() ) )
          MySwap( AGlist, j, j-1 );
        break;
      case 4:
        if( MyLitter( pAgentMng->m_Agent[AGlist[j]]->GetWorkerSrvTimeLen(), pAgentMng->m_Agent[AGlist[j-1]]->GetWorkerSrvTimeLen() ) )
          MySwap( AGlist, j, j-1 );
        break;
      case 5:
        MyTrace(3, "i=%d j=%d pAgentMng->m_Agent[AGlist[j]]->m_Worker.IdelTime=%ld pAgentMng->m_Agent[AGlist[j-1]]->m_Worker.IdelTime=%ld", 
          i, j, pAgentMng->m_Agent[AGlist[j]]->m_Worker.IdelTime, pAgentMng->m_Agent[AGlist[j-1]]->m_Worker.IdelTime);

        if( MyLitter( pAgentMng->m_Agent[AGlist[j]]->m_Worker.IdelTime, pAgentMng->m_Agent[AGlist[j-1]]->m_Worker.IdelTime ) )
          MySwap( AGlist, j, j-1 );
        break;
      }
    }
  }
}
int GetAgentListByRule(CACDRule *ACDRule, int nChn)
{
  static short last_nAG;
  int seattype, seatgroupno, groupno, levelrule;
  int grpno, gIndex, i, k=0, nLevel=0, anscount=0x0FFFFFFF;
  short leastnAG=-1;
  bool bOnlyPC=false;
  time_t tmIdelTime=time(0); //2016-02-12

  MyTrace(3, "GetAgentListByRule nChn=%d", nChn);

  seattype = ACDRule->SeatType;
  seatgroupno = ACDRule->SeatGroupNo;
  levelrule = ACDRule->LevelRule;

  for (grpno = 0; grpno < MAX_GROUP_NUM_FORWORKER; grpno ++)
  {
    if (ACDRule->GroupNo[grpno] >= MAX_AG_GROUP)
      break;//沒有組號了不應該繼續往下分配
    else
      groupno = ACDRule->GroupNo[grpno];

    if (ACDRule->Level[grpno] >= MAX_AG_LEVEL)
      nLevel = 0;
    else
      nLevel = ACDRule->Level[grpno];

    if (levelrule == 4) //4-從Level級別開始向高級別分配
    {
		  for (nLevel = ACDRule->NowLevel[grpno]; nLevel < MAX_AG_LEVEL; nLevel++)
		  {
        switch (ACDRule->AcdRule)
        {
        case 0: //循環振鈴順序分配(每次從上次振鈴的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAllocLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
                	pChn->GetAgentList[k] = last_nAG;
                	k++;
                	if (k >= MAX_AG_NUM)
                		return k;
                }
              }
            }
          }
          break;
        case 1: //固定振鈴順序分配(每次從第1個開始振鈴）
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = -1;
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
              		pChn->GetAgentList[k] = last_nAG;
              		k++;
              		if (k >= MAX_AG_NUM)
              			return k;
                }
              }
            }
          }
          break;
        case 2: //循環應答順序分配(每次從上次應答的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAnsLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
              		pChn->GetAgentList[k] = last_nAG;
              		k++;
              		if (k >= MAX_AG_NUM)
              			return k;
                }
              }
            }
          }
          break;
        case 3: //優先分配應答次數最少的座席 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 1);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 1);
          }
          break;
        case 4: //優先分配服務時長最少的座席 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 2);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 2);
          }
          break;
        case 5: //優先分配應答次數最少的話務員 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 3);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 3);
          }
          break;
        case 6: //優先分配服務時長最少的話務員 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 4);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 4);
          }
          break;
        case 8: //閑置時間最長的優先 //2016-02-12
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 5);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 5);
          }
          break;
        }
		  }
    }
    else if (levelrule == 5) //5-從Level級別開始向低級別分配
    {
      for (nLevel = ACDRule->NowLevel[grpno]; nLevel >= 1; nLevel--)
      {
        switch (ACDRule->AcdRule)
        {
        case 0: //循環振鈴順序分配(每次從上次振鈴的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAllocLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                    return k;
                }
              }
            }
          }
          break;
        case 1: //固定振鈴順序分配(每次從第1個開始振鈴）
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = -1;
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                    return k;
                }
              }
            }
          }
          break;
        case 2: //循環應答順序分配(每次從上次應答的下一個座席開始)
          {
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              last_nAG = pAgentMng->lastAnsLevelGroupIndexnAG[groupno][nLevel][gIndex-1];
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                if (++last_nAG >= pAgentMng->MaxUseNum)
                  last_nAG = 0;
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                    return k;
                }
              }
            }
          }
          break;
        case 3: //優先分配應答次數最少的座席 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;

                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 1);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 1);
          }
          break;
        case 4: //優先分配服務時長最少的座席 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 2);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 2);
          }
          break;
        case 5: //優先分配應答次數最少的話務員 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;

                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 3);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 3);
          }
          break;
        case 6: //優先分配服務時長最少的話務員 //edit 2008-05-20 add
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
            
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC)
                  && pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen() < anscount)
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 4);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 4);
          }
          break;
        case 8: //閑置時間最長的優先 //2016-02-12
          {
            bool bFlag=false;
            for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
            {
              for (i = 0; i < pAgentMng->MaxUseNum; i ++)
              {
                last_nAG = i;
                
                if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                  && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, 1, gIndex, bOnlyPC))
                {
                  bFlag = true;
                  pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 5);
                    return k;
                  }
                }
              }
            }
            if (bFlag == true)
              SortAGList(pChn->GetAgentList, k, 5);
          }
          break;
        }
      }
    }
    else
    {
      switch (ACDRule->AcdRule)
      {
      case 0: //循環振鈴順序分配(每次從上次振鈴的下一個座席開始)
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            last_nAG = pAgentMng->lastAllocGroupIndexnAG[groupno][gIndex-1];
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              if (++last_nAG >= pAgentMng->MaxUseNum)
                last_nAG = 0;
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
              {
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                  return k;
              }
            }
          }
        }
        break;
      case 1: //固定振鈴順序分配(每次從第1個開始振鈴）
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            last_nAG = -1;
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              if (++last_nAG >= pAgentMng->MaxUseNum)
                last_nAG = 0;
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
              {
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                  return k;
              }
            }
          }
        }
        break;
      case 2: //循環應答順序分配(每次從上次應答的下一個座席開始)
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            last_nAG = pAgentMng->lastAnsGroupIndexnAG[groupno][gIndex-1];
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              if (++last_nAG >= pAgentMng->MaxUseNum)
                last_nAG = 0;
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
              {
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                  return k;
              }
            }
          }
        }
        break;
      case 3: //優先分配應答次數最少的座席 //edit 2008-05-20 add
        {
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;

              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetSeatAnsCount() < anscount)
              {
              		pChn->GetAgentList[k] = last_nAG;
                  k++;
                  if (k >= MAX_AG_NUM)
                  {
                    SortAGList(pChn->GetAgentList, k, 1);
                    return k;
                  }
              }
            }
          }
          SortAGList(pChn->GetAgentList, k, 1);
        }
        break;
      case 4: //優先分配服務時長最少的座席 //edit 2008-05-20 add
        {
          bool bFlag=false;
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
            
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetSeatSrvTimeLen() < anscount)
              {
                bFlag = true;
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                {
                  SortAGList(pChn->GetAgentList, k, 2);
                  return k;
                }
              }
            }
          }
          if (bFlag == true)
            SortAGList(pChn->GetAgentList, k, 2);
        }
        break;
      case 5: //優先分配應答次數最少的話務員 //edit 2008-05-20 add
        {
          bool bFlag=false;
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
            
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetWorkerAnsCount() < anscount)
              {
                bFlag = true;
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                {
                  SortAGList(pChn->GetAgentList, k, 3);
                  return k;
                }
              }
            }
          }
          if (bFlag == true)
            SortAGList(pChn->GetAgentList, k, 3);
        }
        break;
      case 6: //優先分配服務時長最少的話務員 //edit 2008-05-20 add
        {
          bool bFlag=false;
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
            
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC)
                && pAgentMng->m_Agent[last_nAG]->GetWorkerSrvTimeLen() < anscount)
              {
                bFlag = true;
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                {
                  SortAGList(pChn->GetAgentList, k, 4);
                  return k;
                }
              }
            }
          }
          if (bFlag == true)
            SortAGList(pChn->GetAgentList, k, 4);
        }
        break;
      case 8: //閑置時間最長的優先 //2016-02-12
        {
          bool bFlag=false;
          for (gIndex=1; gIndex<=MAX_GROUP_NUM_FORWORKER; gIndex++)
          {
            for (i = 0; i < pAgentMng->MaxUseNum; i ++)
            {
              last_nAG = i;
              
              if (pAgentMng->m_Agent[last_nAG]->isIdelForACD(seatgroupno) 
                && pAgentMng->m_Agent[last_nAG]->isWantAgent(seattype, seatgroupno, groupno, nLevel, levelrule, gIndex, bOnlyPC))
              {
                bFlag = true;
              	pChn->GetAgentList[k] = last_nAG;
                k++;
                if (k >= MAX_AG_NUM)
                {
                  SortAGList(pChn->GetAgentList, k, 5);
                  return k;
                }
              }
            }
          }
          if (bFlag == true)
            SortAGList(pChn->GetAgentList, k, 5);
        }
        break;
      }
    }
  }
  return k;
}
//取空閑的坐席自動呼出
CAgent *GetIdelAgentForCallOut(CACDRule &ACDRule)
{
  CAgent *pAgent=NULL;
  int nResult, nGetAgentResult;
  
  if (ACDRule.SeatNo.Compare("0") != 0 || ACDRule.WorkerNo > 0)
  {
    //分配制定的坐席號或工號
    pAgent = GetAgentbyWorkerNoSeatNo(ACDRule.SeatNo, ACDRule.WorkerNo, 0, nResult);
    if (nResult == 0) //0-表示沒有該座席
    {
      nGetAgentResult = 0;
    }
    else if (nResult == 1) //1-表示有該座席且處于空閑狀態
    {
      nGetAgentResult = 1;
    }
    else if (nResult == 3) //3-表示呼叫指定的座席且處于空閑狀態
    {
      nGetAgentResult = 1;
    }
    else //2-表示有該座席但處于非空閑狀態
    {
      nGetAgentResult = 0;
    }
    //累計分配次數
    ACDRule.AcdedCount++;
    if (ACDRule.AcdedCount == 1)
    {
      if (pAgent)
      {
        int nGroupNo = pAgent->GetGroupNo();
        if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
        {
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
          SendOneGroupStatusMeteCountB(nGroupNo);
          SendSystemStatusMeteCount();
        }
      }

      pAgentMng->AllCallCountParam.ACDCount++;
      SendSystemStatusMeteCount();
    }
  }
  else
  {
    //按規則分配坐席
    ACDRule.AcdedCount++;
    if (ACDRule.AcdedCount == 1)
    {
      int nGroupNo = ACDRule.GroupNo[0];
      if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
      {
        pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
        SendOneGroupStatusMeteCountB(nGroupNo);
        SendSystemStatusMeteCount();
      }

      pAgentMng->AllCallCountParam.ACDCount++;
      SendSystemStatusMeteCount();
    }
    pAgent = GetIdelAgentbyRule(&(ACDRule), 0, 0);
    if (pAgent != NULL) //1-表示有該座席且處于空閑狀態
    {
      nGetAgentResult = 1;
    }
    else
    {
      nGetAgentResult = 0;
    }
  }
  
  if (nGetAgentResult == 1) //有符合規則的空閑座席
  {
    return pAgent;
  }

  return NULL;
}
//根據通道號取坐席,返回值: NULL-該通道沒有綁定坐席 not NULL-該通道綁定坐席
CAgent *GetAgentBynChn(int nChn)
{
  int nAG;

  if (!pBoxchn->isnChnAvail(nChn))
    return NULL;
  nAG = pChn->nAG;
  if (pAgentMng->isnAGAvail(nAG))
  {
    return pAgentMng->m_Agent[nAG];
  }
  return NULL;
}
//根據通道取綁定的坐席類型
short GetSeatTypeBynChn(int nChn)
{
  CAgent *pAgent=GetAgentBynChn(nChn);
  if (pAgent != NULL)
  {
    return pAgent->GetSeatType();
  }
  return 0; //無坐席
}

//取坐席對應的坐席電話通道狀態
short GetAgentChState(CAgent *pAgent)
{
  US nChn;

  nChn = pAgent->GetSeatnChn();
  if (pBoxchn->isnChnAvail(nChn))
    return pChn->ssState;
  else
    return CHN_SV_IDEL;
}
short GetAgentChState(int nAG)
{
  US nChn;

  if (pAgentMng->isnAGAvail(nAG))
  {
    nChn = pAG->GetSeatnChn();
    if (pBoxchn->isnChnAvail(nChn))
      return pChn->ssState;
  }
  return CHN_SV_IDEL;
}
//該工號是否登錄
bool IsTheWorkerLogin(int WorkerNo)
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAgentMng->m_Agent[nAG]->m_Worker.isWantWorkerNo(WorkerNo) && pAgentMng->m_Agent[nAG]->m_Worker.isLogin())
      return true;
  }
  return false;
}

void AgentTimerAdd(int nTCount)
{
  int nAG;
  bool bSendState;

  for (nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    bSendState = false;
    pAG->timer += nTCount;
    pAG->delaytimer += nTCount;

    if (pAG->LockId > 0)
    {
      if (pAG->timer > 120)
      {
        pAG->LockId = 0;
        bSendState = true;
      }
    }
    if (pAG->m_Worker.DisturbId == 2)
    {
      if (pAG->timer > pIVRCfg->MaxWaitIVRReturnTimelen)
      {
        pAG->m_Worker.DisturbId = 0;
        pAG->SendTrnIVRHangonFlag = 0; //2016-07-19
        //2016-02-12
        if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=onhangon;seatno=%s;result=0;",
            pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str());
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
        bSendState = true;
      }
    }
    //2016-07-04
    if (pAG->m_Worker.PreDisturbId == 2)
    {
      if (pAG->timer > pIVRCfg->MaxWaitIVRReturnTimelen)
      {
        pAG->m_Worker.PreDisturbId = 0;
      }
    }
    if (pAG->svState == AG_SV_ACDLOCK)
    {
      if (pAG->timer > 15)
      {
        int nChn = pAG->GetSeatnChn();
        if (pBoxchn->isnChnAvail(nChn))
        {
          pChn->CalloutId = 0;
        }
        pAG->SetAgentsvState(AG_SV_IDEL); //2016-02-12
        MyTrace(3, "AgentTimerProc nAG=%d AG_SV_ACDLOCK to AG_SV_IDEL", nAG);
        bSendState = true;
      }
    }
    if (pAG->DelayState == 1)
    {
      //話后處理
      if (pAG->delaytimer >= pAG->m_Worker.DelayTime)
      {
        DBUpdateCallCDR_ACWTime(pAG->nAG);
        if (pAG->m_Worker.DisturbId == 1) //2013-08-08
        {
          WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
          WriteSeatStatus(pAG, AG_STATUS_BUSY, 0);
        }
        else if (pAG->m_Worker.LeaveId > 0)
        {
          WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
          WriteSeatStatus(pAG, AG_STATUS_LEAVE, 0);
        }
        else
        {
          WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
        }
        pAG->DelayState = 0; //2015-12-14 這個要放在 WriteSeatStatus 前執行，不然ACW計數不會-1
        
        memset(pAG->CdrSerialNo, 0, MAX_CHAR64_LEN);
        memset(pAG->RecordFileName, 0, MAX_CHAR128_LEN);
        memset(pAG->ExtRecordFileName, 0, MAX_CHAR128_LEN);
        bSendState = true;
        //2016-10-21增加判斷話務員是否登錄了 pAgent->isLogin()
        if (pAG->isLogin() && pIVRCfg->isSetReadyStateonAgentSetDisturb > 0 && pAG->m_Worker.DisturbId == 0  && pAG->m_Worker.LeaveId == 0)
        {
          Send_SWTMSG_setagentstatus(pAG->GetSeatnChn(), pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_READY);
        }
        AgentStatusCount();
        SendOneGroupStatusMeteCount(pAG->nAG);
        SendSystemStatusMeteCount();
      }
    } 
    else if (pAG->DelayState == 2)
    {
      //延時
      if (pAG->delaytimer >= 3)
      {
        if (pAG->m_Worker.DisturbId == 1) //2013-08-08
        {
          WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
          WriteSeatStatus(pAG, AG_STATUS_BUSY, 0);
        }
        else if (pAG->m_Worker.LeaveId > 0)
        {
          WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
          WriteSeatStatus(pAG, AG_STATUS_LEAVE, 0);
        }
        else
        {
          WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
        }
        
        pAG->DelayState = 0;
        memset(pAG->CdrSerialNo, 0, MAX_CHAR64_LEN);
        memset(pAG->RecordFileName, 0, MAX_CHAR128_LEN);
        memset(pAG->ExtRecordFileName, 0, MAX_CHAR128_LEN);
        bSendState = true;

        AgentStatusCount();
        SendOneGroupStatusMeteCount(pAG->nAG);
        SendSystemStatusMeteCount();
      }
    }
    if (bSendState == true)
    {
      DispAgentStatus(nAG);
    }
  }
}
//清楚當前相同的登錄賬號
void ClearTheSameAccountNo(const char *accountno)
{
  char szSection[32];
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAgentMng->m_Agent[nAG]->m_Worker.AccountNo.Compare(accountno) == 0)
    {
      pAgentMng->m_Agent[nAG]->m_Worker.AccountNo.Empty();
      sprintf(szSection, "Seat[%d].AccountNo", nAG);
      WritePrivateProfileString("SEAT", szSection, "", g_szServiceINIFileName);
      DispAgentStatus(nAG);
    }
  }
}
void SaveWorkerAccountNo(int nAG, const char *accountno)
{
  char szSection[32];
  pAgentMng->m_Agent[nAG]->m_Worker.AccountNo = accountno;
  sprintf(szSection, "Seat[%d].AccountNo", nAG);
  WritePrivateProfileString("SEAT", szSection, accountno, g_szServiceINIFileName);
}
//---------------------------------------------------------------------------
//處理坐席消息
//---------------------------------------------------------------------------
//發送消息到坐席
void SendMsg2AG(US ClientId, US MsgId, const CH *msg)
{
  pTcpAgLink->SendMessage2CLIENT(ClientId, (MSGTYPE_AGIVR<<8) | MsgId, msg);
  if (MsgId != AGMSG_ongetqueueinfo && MsgId != AGMSG_ongetagentstatus && MsgId != AGMSG_ongetchnstatus && MsgId != AGMSG_ongetqueueinfoEx)
    MyTrace(2, "0x%04x %s", ClientId, msg);
}
void SendMsg2AG(US ClientId, US MsgId, const CStringX &msg)
{
	SendMsg2AG(ClientId, MsgId, msg.C_Str());
}
void SendMsg2WS(US ClientId, US MsgId, const CH *msg, bool bSaveId)
{
  if (ClientId == 0)
    return;
  CH szEncode[4096];
  memset(szEncode, 0, 4096);
  if (g_nURIEncodeDecode == 0)
  {
    pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRWEB<<8) | MsgId, msg);
    if (bSaveId == true)
      MyTrace(2, "0x%04x %s", ClientId, msg);
  }
  else if (g_nURIEncodeDecode == 1)
  {
    if (URLEncode(msg, szEncode, 4096) == FALSE)
      strcpy(szEncode, msg);
    pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRWEB<<8) | MsgId, szEncode);
    if (bSaveId == true)
      MyTrace(2, "0x%04x %s", ClientId, szEncode);
  }
  else if (g_nURIEncodeDecode == 2)
  {
    if (ConvAnsiStrToUniStr(msg, szEncode) == 0)
      strcpy(szEncode, msg);
    pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRWEB<<8) | MsgId, szEncode);
    if (bSaveId == true)
      MyTrace(2, "0x%04x %s", ClientId, szEncode);
  }
  else if (g_nURIEncodeDecode == 3)
  {
    Base64Encode((CH *)msg, (UC *)szEncode);
    pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRWEB<<8) | MsgId, szEncode);
    if (bSaveId == true)
      MyTrace(2, "0x%04x %s", ClientId, szEncode);
  }
  else
  {
    pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRWEB<<8) | MsgId, msg);
    if (bSaveId == true)
      MyTrace(2, "0x%04x %s", ClientId, msg);
  }
}
//分析坐席來的消息
int Proc_Msg_From_AG(CXMLRcvMsg &AGMsg)
{
	unsigned short MsgId=AGMsg.GetMsgId();

  if(MsgId >= MAX_AGIVRMSG_NUM)	//ag-->ivr
  {
    //指令編號超出范圍
    MyTrace(0, "AGmsgid = %d is out of range\n", MsgId);
    return 1;
  }

  if(0!=AGMsg.ParseSndMsgWithCheck(pFlwRule->AGIVRMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(0, "parse agent msg error: MsgId = %d MsgBuf = %s\n", MsgId, AGMsg.GetBuf().C_Str());
    return 2;
  }
  return 0;
}
//取AG消息規定頭
void Get_AGMSG_HeaderValue(CXMLRcvMsg &AGMsg)
{
  ACDIdn = atol(AGMsg.GetAttrValue(0).C_Str());
  AGClientId = (US)(ACDIdn>>16);
  nAGn = atoi(AGMsg.GetAttrValue(1).C_Str());
}
//設置發送到AG的消息頭
void Set_IVR2AG_Header(int MsgId)
{
  IVRSndMsg.GetBuf().Format("<%s acdid='%lu'", pFlwRule->IVRAGMsgRule[MsgId].MsgNameEng, ACDIdn);
}
void Set_IVR2AG_Header(int MsgId, UL acdid)
{
  IVRSndMsg.GetBuf().Format("<%s acdid='%lu'", pFlwRule->IVRAGMsgRule[MsgId].MsgNameEng, acdid);
}
//增加發送到AG的消息屬性
void Set_IVR2AG_Item(int MsgId, int AttrId, const char *value)
{
  IVRSndMsg.AddItemToBuf(pFlwRule->IVRAGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2AG_Item(int MsgId, int AttrId, const CStringX &value)
{
  IVRSndMsg.AddItemToBuf(pFlwRule->IVRAGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2AG_IntItem(int MsgId, int AttrId, int value)
{
  IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRAGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2AG_LongItem(int MsgId, int AttrId, long value)
{
  IVRSndMsg.AddLongItemToBuf(pFlwRule->IVRAGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2AG_Tail()
{
  IVRSndMsg.AddTailToBuf();
}
//-----------------------------------------------------------------------------
//發送通用結果消息
void SendCommonResult(int onMsgId, int result, const char *error)
{
	Set_IVR2AG_Header(onMsgId);
  Set_IVR2AG_IntItem(onMsgId, 1, result);
  Set_IVR2AG_Item(onMsgId, 2, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(AGClientId, onMsgId, IVRSndMsg.GetBuf());
}
void SendCommonResult(int nAG, int onMsgId, int result, const char *error)
{
  if (pAG->GetClientId() == 0x06FF)
    return;
  Set_IVR2AG_Header(onMsgId, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(onMsgId, 1, result);
  Set_IVR2AG_Item(onMsgId, 2, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), onMsgId, IVRSndMsg.GetBuf());
}
//發送坐席登錄結果
void SendSeatLoginResult(int nAG, const CStringX SeatNo, int SeatType, int ChnType, int ChIndex, int result, const char *error)
{
	Set_IVR2AG_Header(AGMSG_onseatlogin);
  Set_IVR2AG_IntItem(AGMSG_onseatlogin, 1, nAG);
  Set_IVR2AG_Item(AGMSG_onseatlogin, 2, SeatNo);
  Set_IVR2AG_IntItem(AGMSG_onseatlogin, 3, SeatType);
  Set_IVR2AG_IntItem(AGMSG_onseatlogin, 4, ChnType);
  Set_IVR2AG_IntItem(AGMSG_onseatlogin, 5, ChIndex);
  Set_IVR2AG_IntItem(AGMSG_onseatlogin, 6, result);
  Set_IVR2AG_Item(AGMSG_onseatlogin, 7, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(AGClientId, AGMSG_onseatlogin, IVRSndMsg.GetBuf());
}
//發送數據庫登錄信息
void SendGetDBParam(int nAG)
{
  char szTemp[128];

  Set_IVR2AG_Header(AGMSG_ongetdbparam);
  Set_IVR2AG_IntItem(AGMSG_ongetdbparam, 1, nAG);
  
  if (g_nINIFileType == 0)
  {
    GetPrivateProfileString("CONFIGURE", "DBTYPE", "MYSQL", szTemp, 128, g_szDBINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 2, szTemp);
    GetPrivateProfileString("CONFIGURE", "DBSERVER", "127.0.0.1", szTemp, 128, g_szDBINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 3, szTemp);
    GetPrivateProfileString("CONFIGURE", "DATABASE", "callcenterdb", szTemp, 128, g_szDBINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 4, szTemp);
    GetPrivateProfileString("CONFIGURE", "USERID", "root", szTemp, 128, g_szDBINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 5, szTemp);
    GetPrivateProfileString("CONFIGURE", "PASSWORD", "", szTemp, 128, g_szDBINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 6, szTemp);
  }
  else
  {
    GetPrivateProfileString("DATABASE", "DBTYPE", "MYSQL", szTemp, 128, g_szUnimeINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 2, szTemp);
    GetPrivateProfileString("DATABASE", "DBSERVER", "127.0.0.1", szTemp, 128, g_szUnimeINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 3, szTemp);
    GetPrivateProfileString("DATABASE", "DATABASE", "callcenterdb", szTemp, 128, g_szUnimeINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 4, szTemp);
    GetPrivateProfileString("DATABASE", "USERID", "root", szTemp, 128, g_szUnimeINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 5, szTemp);
    GetPrivateProfileString("DATABASE", "PASSWORD", "", szTemp, 128, g_szUnimeINIFileName);
    Set_IVR2AG_Item(AGMSG_ongetdbparam, 6, szTemp);
  }
  
  Set_IVR2AG_Tail();
  SendMsg2AG(AGClientId, AGMSG_ongetdbparam, IVRSndMsg.GetBuf());
}
//發送數據庫登錄信息
void SendGetAuthKey(int nAG)
{
  Set_IVR2AG_Header(AGMSG_ongetauthkey);
  Set_IVR2AG_IntItem(AGMSG_ongetauthkey, 1, nAG);
  Set_IVR2AG_Item(AGMSG_ongetauthkey, 2, AuthKey);
  Set_IVR2AG_Tail();
  SendMsg2AG(AGClientId, AGMSG_ongetauthkey, IVRSndMsg.GetBuf());
}
//發送話務員登錄結果
void SendWorkerLoginResult(const CStringX inform, int result, const char *error)
{
	Set_IVR2AG_Header(AGMSG_onworkerlogin);
  Set_IVR2AG_Item(AGMSG_onworkerlogin, 1, inform);
  Set_IVR2AG_IntItem(AGMSG_onworkerlogin, 2, result);
  Set_IVR2AG_Item(AGMSG_onworkerlogin, 3, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(AGClientId, AGMSG_onworkerlogin, IVRSndMsg.GetBuf());
}

//發送停止呼入分配坐席消息
void SendStopACDCallIn(int nAG)
{
	if (pAG->GetClientId() != 0x06FF)
  {
    Set_IVR2AG_Header(AGMSG_onstopacdcallin, pAG->GetClientId()<<16);
    Set_IVR2AG_IntItem(AGMSG_onstopacdcallin, 1, 0);
    Set_IVR2AG_Item(AGMSG_onstopacdcallin, 2, "");
    Set_IVR2AG_Tail();
    SendMsg2AG(pAG->GetClientId(), AGMSG_onstopacdcallin, IVRSndMsg.GetBuf());
  }
  //2015-10-13 add
  if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
  {
    sprintf(SendMsgBuf, "wsclientid=%s;cmd=stopacdcallin;seatno=%s;result=%d;", 
      pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), 0, "");
    SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
  }
}
//發送應答呼入電話的結果
void SendAnswerCallResult(int nAG)
{
	if (pAG->GetClientId() != 0x06FF)
  {
    Set_IVR2AG_Header(AGMSG_onanswercall, pAG->GetClientId()<<16);
    Set_IVR2AG_IntItem(AGMSG_onanswercall, 1, 0);
    Set_IVR2AG_Item(AGMSG_onanswercall, 2, "");
    Set_IVR2AG_Tail();
    SendMsg2AG(pAG->GetClientId(), AGMSG_onanswercall, IVRSndMsg.GetBuf());
  }
  if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
  {
    sprintf(SendMsgBuf, "wsclientid=%s;cmd=onanswercall;seatno=%s;result=0;param=",
      pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str());
    SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
  }
}
//發送坐席話機消息
void SendSeatHangon(int nAG)
{
	if (pAG->GetClientId() != 0x06FF)
  {
    Set_IVR2AG_Header(AGMSG_onhangon, pAG->GetClientId()<<16);
    Set_IVR2AG_IntItem(AGMSG_onhangon, 1, 0);
    Set_IVR2AG_Tail();
    SendMsg2AG(pAG->GetClientId(), AGMSG_onhangon, IVRSndMsg.GetBuf());
  }
  if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
  {
    //2016-02-12 坐席處于“等待IVR返回”狀態時發送的掛機結果不同
    if (pAG->m_Worker.DisturbId == 2 && pAG->SendTrnIVRHangonFlag == 1) //2016-07-19
    {
      sprintf(SendMsgBuf, "wsclientid=%s;cmd=onhangon;seatno=%s;result=2;",
      pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str());
      pAG->SendTrnIVRHangonFlag = 0; //2016-07-19
    }
    else
    {
      sprintf(SendMsgBuf, "wsclientid=%s;cmd=onhangon;seatno=%s;result=0;",
      pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str());
    }
    SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
  }
}
//發送坐席呼叫失敗結果消息
void SendSeatMakeCallFailResult(int nAG, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_onmakecall, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_onmakecall, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_onmakecall, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_onmakecall, 3, 0);
  Set_IVR2AG_Item(AGMSG_onmakecall, 4, "");
  Set_IVR2AG_Item(AGMSG_onmakecall, 5, "");
  Set_IVR2AG_Item(AGMSG_onmakecall, 6, param);
  Set_IVR2AG_IntItem(AGMSG_onmakecall, 7, result);
  Set_IVR2AG_Item(AGMSG_onmakecall, 8, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onmakecall, IVRSndMsg.GetBuf());
}
//發送轉接失敗結果消息
void SendSeatTranCallFailResult(int nAG, const char*calledno, const char*param, int result, const char *error)
{
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  if (pAG->GetClientId() == 0x06FF)
    return;
  Set_IVR2AG_Header(AGMSG_ontrancall, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_ontrancall, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_ontrancall, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_ontrancall, 3, 0);
  Set_IVR2AG_IntItem(AGMSG_ontrancall, 4, 0);
  Set_IVR2AG_IntItem(AGMSG_ontrancall, 5, 0);
  Set_IVR2AG_Item(AGMSG_ontrancall, 6, "");
  Set_IVR2AG_Item(AGMSG_ontrancall, 7, calledno);
  Set_IVR2AG_Item(AGMSG_ontrancall, 8, param);
  Set_IVR2AG_IntItem(AGMSG_ontrancall, 9, result);
  Set_IVR2AG_Item(AGMSG_ontrancall, 10, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_ontrancall, IVRSndMsg.GetBuf());
}
//發送穿梭通話失敗結果消息
void SendSeatJoinConfFailResult(int nAG, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_onjoinconf, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_onjoinconf, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_onjoinconf, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_onjoinconf, 3, 0);
  Set_IVR2AG_IntItem(AGMSG_onjoinconf, 4, 0);
  Set_IVR2AG_IntItem(AGMSG_onjoinconf, 5, result);
  Set_IVR2AG_Item(AGMSG_onjoinconf, 6, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onjoinconf, IVRSndMsg.GetBuf());
}
//發送將來話轉接到ivr結果消息
void SendSeatTranIVRFailResult(int nAG, int returnflag, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_ontranivr, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_ontranivr, 1, returnflag);
  Set_IVR2AG_Item(AGMSG_ontranivr, 2, param);
  Set_IVR2AG_IntItem(AGMSG_ontranivr, 3, result);
  Set_IVR2AG_Item(AGMSG_ontranivr, 4, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_ontranivr, IVRSndMsg.GetBuf());
}
//發送坐席代接失敗結果消息
void SendSeatPickupFailResult(int nAG, const char*param, int result, const char *error)
{
  if (pAG->GetClientId() == 0x06FF) //2015-12-14
    return; 
  Set_IVR2AG_Header(AGMSG_onpickup, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 3, 0);
  Set_IVR2AG_Item(AGMSG_onpickup, 4, "");
  Set_IVR2AG_Item(AGMSG_onpickup, 5, "");
  Set_IVR2AG_Item(AGMSG_onpickup, 6, "");
  Set_IVR2AG_Item(AGMSG_onpickup, 7, "");
  Set_IVR2AG_IntItem(AGMSG_onpickup, 8, 0);
  Set_IVR2AG_Item(AGMSG_onpickup, 9, param);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 10, result);
  Set_IVR2AG_Item(AGMSG_onpickup, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onpickup, IVRSndMsg.GetBuf());
}
void SendSeatPickupResult(int nAG, int nOut, int point, int result, const char *error)
{
  int nChn, orgchntype, orgchnno;

  nChn = pOut->Session.nChn;
  pBoxchn->GetLgChBynChn(nChn, orgchntype, orgchnno);
  
  Set_IVR2AG_Header(AGMSG_onpickup, pAG->GetClientId()<<16);
  Set_IVR2AG_LongItem(AGMSG_onpickup, 1, pOut->Session.SessionNo);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 2, orgchntype);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 3, orgchnno);
  Set_IVR2AG_Item(AGMSG_onpickup, 4, pOut->CallerNo);
  Set_IVR2AG_Item(AGMSG_onpickup, 5, pOut->Calleds[point].CalledNo);
  Set_IVR2AG_Item(AGMSG_onpickup, 6, "");
  Set_IVR2AG_Item(AGMSG_onpickup, 7, "");
  Set_IVR2AG_IntItem(AGMSG_onpickup, 8, pOut->Session.FuncNo);
  Set_IVR2AG_Item(AGMSG_onpickup, 9, pOut->Session.Param);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 10, result);
  Set_IVR2AG_Item(AGMSG_onpickup, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onpickup, IVRSndMsg.GetBuf());
}
void SendSeatPickupResult(int nAG, int nAcd, int result, const char *error)
{
  int nChn, orgchntype, orgchnno;
  
  nChn = pAcd->Session.nChn;
  pBoxchn->GetLgChBynChn(nChn, orgchntype, orgchnno);
  
  Set_IVR2AG_Header(AGMSG_onpickup, pAG->GetClientId()<<16);
  Set_IVR2AG_LongItem(AGMSG_onpickup, 1, pAcd->Session.SessionNo);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 2, orgchntype);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 3, orgchnno);
  Set_IVR2AG_Item(AGMSG_onpickup, 4, pAcd->Session.CallerNo);
  Set_IVR2AG_Item(AGMSG_onpickup, 5, pAcd->Session.CalledNo);
  Set_IVR2AG_Item(AGMSG_onpickup, 6, "");
  Set_IVR2AG_Item(AGMSG_onpickup, 7, "");
  Set_IVR2AG_IntItem(AGMSG_onpickup, 8, pAcd->Session.FuncNo);
  Set_IVR2AG_Item(AGMSG_onpickup, 9, pAcd->Session.Param);
  Set_IVR2AG_IntItem(AGMSG_onpickup, 10, result);
  Set_IVR2AG_Item(AGMSG_onpickup, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onpickup, IVRSndMsg.GetBuf());
}
//發送坐席代接ACD分配隊列電話失敗結果消息
void SendSeatPickupACDFailResult(int nAG, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_onpickupqueuecall, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 3, 0);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 4, "");
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 5, "");
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 6, "");
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 7, "");
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 8, 0);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 9, param);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 10, result);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onpickupqueuecall, IVRSndMsg.GetBuf());
}
void SendSeatPickupACDResult(int nAG, int nAcd, int result, const char *error)
{
  int nChn, orgchntype, orgchnno;
  
  nChn = pAcd->Session.nChn;
  pBoxchn->GetLgChBynChn(nChn, orgchntype, orgchnno);
  
  Set_IVR2AG_Header(AGMSG_onpickupqueuecall, pAG->GetClientId()<<16);
  Set_IVR2AG_LongItem(AGMSG_onpickupqueuecall, 1, pAcd->Session.SessionNo);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 2, orgchntype);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 3, orgchnno);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 4, pAcd->Session.CallerNo);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 5, pAcd->Session.CalledNo);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 6, "");
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 7, "");
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 8, pAcd->Session.FuncNo);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 9, pAcd->Session.Param);
  Set_IVR2AG_IntItem(AGMSG_onpickupqueuecall, 10, result);
  Set_IVR2AG_Item(AGMSG_onpickupqueuecall, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onpickupqueuecall, IVRSndMsg.GetBuf());
}
//發送坐席接管電話失敗結果消息
void SendSeatTakeOverFailResult(int nAG, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_ontakeover, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 3, 0);
  Set_IVR2AG_Item(AGMSG_ontakeover, 4, "");
  Set_IVR2AG_Item(AGMSG_ontakeover, 5, "");
  Set_IVR2AG_Item(AGMSG_ontakeover, 6, "");
  Set_IVR2AG_Item(AGMSG_ontakeover, 7, "");
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 8, 0);
  Set_IVR2AG_Item(AGMSG_ontakeover, 9, param);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 10, result);
  Set_IVR2AG_Item(AGMSG_ontakeover, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_ontakeover, IVRSndMsg.GetBuf());
}
void SendSeatTakeOverResult(int nAG, int nChn, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_ontakeover, pAG->GetClientId()<<16);
  Set_IVR2AG_LongItem(AGMSG_ontakeover, 1, pChn->SessionNo);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 2, pChn->lgChnType);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 3, pChn->lgChnNo);
  Set_IVR2AG_Item(AGMSG_ontakeover, 4, pChn->CallerNo);
  Set_IVR2AG_Item(AGMSG_ontakeover, 5, pChn->CalledNo);
  Set_IVR2AG_Item(AGMSG_ontakeover, 6, pChn->OrgCallerNo);
  Set_IVR2AG_Item(AGMSG_ontakeover, 7, pChn->OrgCalledNo);
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 8, 0);
  Set_IVR2AG_Item(AGMSG_ontakeover, 9, "");
  Set_IVR2AG_IntItem(AGMSG_ontakeover, 10, result);
  Set_IVR2AG_Item(AGMSG_ontakeover, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_ontakeover, IVRSndMsg.GetBuf());
}
//發送監聽失敗結果消息
void SendSeatListenFailResult(int nAG, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_onlisten, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 3, 0);
  Set_IVR2AG_Item(AGMSG_onlisten, 4, "");
  Set_IVR2AG_Item(AGMSG_onlisten, 5, "");
  Set_IVR2AG_Item(AGMSG_onlisten, 6, "");
  Set_IVR2AG_Item(AGMSG_onlisten, 7, "");
  Set_IVR2AG_IntItem(AGMSG_onlisten, 8, 0);
  Set_IVR2AG_Item(AGMSG_onlisten, 9, param);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 10, result);
  Set_IVR2AG_Item(AGMSG_onlisten, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onlisten, IVRSndMsg.GetBuf());
}
void SendSeatListenResult(int nAG, int nChn, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_onlisten, pAG->GetClientId()<<16);
  Set_IVR2AG_LongItem(AGMSG_onlisten, 1, pChn->SessionNo);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 2, pChn->lgChnType);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 3, pChn->lgChnNo);
  Set_IVR2AG_Item(AGMSG_onlisten, 4, pChn->CallerNo);
  Set_IVR2AG_Item(AGMSG_onlisten, 5, pChn->CalledNo);
  Set_IVR2AG_Item(AGMSG_onlisten, 6, pChn->OrgCallerNo);
  Set_IVR2AG_Item(AGMSG_onlisten, 7, pChn->OrgCalledNo);
  Set_IVR2AG_IntItem(AGMSG_onlisten, 8, 0);
  Set_IVR2AG_Item(AGMSG_onlisten, 9, "");
  Set_IVR2AG_IntItem(AGMSG_onlisten, 10, result);
  Set_IVR2AG_Item(AGMSG_onlisten, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onlisten, IVRSndMsg.GetBuf());
}
//發送強插失敗結果消息
void SendSeatBreakinFailResult(int nAG, const char*param, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_oninsert, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 1, 0);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 2, 0);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 3, 0);
  Set_IVR2AG_Item(AGMSG_oninsert, 4, "");
  Set_IVR2AG_Item(AGMSG_oninsert, 5, "");
  Set_IVR2AG_Item(AGMSG_oninsert, 6, "");
  Set_IVR2AG_Item(AGMSG_oninsert, 7, "");
  Set_IVR2AG_IntItem(AGMSG_oninsert, 8, 0);
  Set_IVR2AG_Item(AGMSG_oninsert, 9, param);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 10, result);
  Set_IVR2AG_Item(AGMSG_oninsert, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_oninsert, IVRSndMsg.GetBuf());
}
void SendSeatBreakinResult(int nAG, int nChn, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_oninsert, pAG->GetClientId()<<16);
  Set_IVR2AG_LongItem(AGMSG_oninsert, 1, pChn->SessionNo);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 2, pChn->lgChnType);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 3, pChn->lgChnNo);
  Set_IVR2AG_Item(AGMSG_oninsert, 4, pChn->CallerNo);
  Set_IVR2AG_Item(AGMSG_oninsert, 5, pChn->CalledNo);
  Set_IVR2AG_Item(AGMSG_oninsert, 6, pChn->OrgCallerNo);
  Set_IVR2AG_Item(AGMSG_oninsert, 7, pChn->OrgCalledNo);
  Set_IVR2AG_IntItem(AGMSG_oninsert, 8, 0);
  Set_IVR2AG_Item(AGMSG_oninsert, 9, "");
  Set_IVR2AG_IntItem(AGMSG_oninsert, 10, result);
  Set_IVR2AG_Item(AGMSG_oninsert, 11, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_oninsert, IVRSndMsg.GetBuf());
}
//發送文字信息
void SendSeatMessage(int nAG, int nAG1, const char *ansimsg, const char *utf8msg)
{
  if (pAG->isLogin() == false)
    return;
  if (pAG->isTcpLinked() == false)
  {
    if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
    {
      sprintf(SendMsgBuf, "wsclientid=%s;cmd=recvmesssage;seatno=%s;sendseatno=%s;message=%s;",
        pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), pAG1->GetSeatNo().C_Str(), utf8msg);
      SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
    }
    return;
  }
  Set_IVR2AG_Header(AGMSG_onsendmessage, pAG->GetClientId()<<16);
  Set_IVR2AG_Item(AGMSG_onsendmessage, 1, pAG1->GetSeatNo());
  Set_IVR2AG_IntItem(AGMSG_onsendmessage, 2, pAG1->GetWorkerNo());
  Set_IVR2AG_Item(AGMSG_onsendmessage, 3, pAG1->GetWorkerName());
  Set_IVR2AG_Item(AGMSG_onsendmessage, 4, ansimsg);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_onsendmessage, IVRSndMsg.GetBuf());
}
//發送創建會議結果消息
void SendSeatCreateConfResult(int nAG, int confno, int result, const char *error)
{
  Set_IVR2AG_Header(AGMSG_oncreateconf, pAG->GetClientId()<<16);
  Set_IVR2AG_IntItem(AGMSG_oncreateconf, 1, confno);
  Set_IVR2AG_IntItem(AGMSG_oncreateconf, 2, result);
  Set_IVR2AG_Item(AGMSG_oncreateconf, 3, error);
  Set_IVR2AG_Tail();
  SendMsg2AG(pAG->GetClientId(), AGMSG_oncreateconf, IVRSndMsg.GetBuf());
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//電腦坐席登錄
int Proc_AGMSG_seatlogin(CXMLRcvMsg &AGMsg) 
{
  int nAG1, nChn, nResult, SeatLangID;
  CAgent *pAgent=NULL;

  SendRunStatusToClient(AGMsg.GetClientId(), 0x3031);

  SeatLangID = atoi(AGMsg.GetAttrValue(7).C_Str()); //坐席端語言類型
  if (SeatLangID == 0)
  {
    SeatLangID = 1;
  }

  if (pAgentMng->isTheClientIdLogin(AGClientId) == true)
  {
    if (SeatLangID == 1 || SeatLangID == 0)
      SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "該客戶端編號已登錄");
    else if (SeatLangID == 2)
      SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "贛��?狠絪腹��祆厚");
    else
      SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "the clientid had login");
    return 1;
  }
  if (AGMsg.GetAttrValue(2).Compare("0") == 0)
  {
    pAgent = GetAgentbyBandIP(AGMsg.GetAttrValue(4), nResult);
    if (nResult == 0)
    {
      if (SeatLangID == 1 || SeatLangID == 0)
        SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "該IP未綁定坐席");
      else if (SeatLangID == 2)
        SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "贛IP�H��?u﹚");
      else
        SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "the ip have not banding");
      return 1;
    }
  }
  else
  {
    pAgent = GetAgentbySeatNo(AGMsg.GetAttrValue(2), nResult);
    if (nResult == 0)
    {
      if (SeatLangID == 1 || SeatLangID == 0)
        SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "該坐席號不存在");
      else if (SeatLangID == 2)
        SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "贛??腹?祆厚");
      else
        SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "the seatno is not exist");
      return 1;
    }
  }
  if (pAgent->isTcpLinked())
  {
    if (SeatLangID == 1 || SeatLangID == 0)
      SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "該坐席已有電腦登錄");
    else if (SeatLangID == 2)
      SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "贛??��Τ?福祆厚");
    else
      SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "the seatno had login");
    return 1;
  }
  if (pAgent->GetSeatType() == SEATTYPE_RT_TEL)
  {
    if (AGMsg.GetAttrValue(3).GetLength() > 1)
    {
      nAG1 = pAgentMng->GetnAGByPhoneNo(AGMsg.GetAttrValue(3).C_Str());
      if (pAgentMng->isnAGAvail(nAG1))
      {
        if (nAG1 != pAgent->nAG)
        {
          if (SeatLangID == 1 || SeatLangID == 0)
            SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "該外線號碼已有電腦登錄");
          else if (SeatLangID == 2)
            SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "贛�蘊D��祆厚");
          else
            SendSeatLoginResult(0xFFFF, "", 0, 0, 0, OnFail, "the External line had login");
          return 1;
        }
      }
      pAgent->m_Seat.PhoneNo = AGMsg.GetAttrValue(3);
    }
    //chtype = 0;
    //chindex = 0;
  }
  else
  {
    nChn = pAgent->GetSeatnChn();
    //chtype = pChn->ChType;
    //chindex = pChn->ChIndex;
  }
  pAgentMng->SetClientId(pAgent->nAG, AGClientId);
  pAgent->m_Seat.TcpLinked(AGClientId);
  pAgent->m_Seat.SetSeatIP(AGMsg.GetAttrValue(4));
  if (1 == atoi(AGMsg.GetAttrValue(5).C_Str()))
  {
    pAgent->m_Seat.ClearWork();
  }
  pAgent->m_Seat.GroupNo = atoi(AGMsg.GetAttrValue(6).C_Str()); //坐席組號
  pAgent->m_Seat.SeatLangID = SeatLangID;
  
  if (pBoxchn->isnChnAvail(nChn))
    SendSeatLoginResult(pAgent->nAG, pAgent->GetSeatNo(), pAgent->GetSeatType(), pChn->lgChnType, pChn->lgChnNo, OnSuccess, "");
  else
    SendSeatLoginResult(pAgent->nAG, pAgent->GetSeatNo(), pAgent->GetSeatType(), 0, 0, OnSuccess, "");
  
  DispAgentStatus(pAgent->nAG);
  SendGetAuthKey(pAgent->nAG);
  SendGetDBParam(pAgent->nAG);
  return 0;
}
//話務員登錄
int Proc_AGMSG_workerlogin(CXMLRcvMsg &AGMsg) 
{
  int nAG1, workerno, groupno=0, workergrade=0, level=0, groupnum, dutyno;
  CStringX ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER];

  if (!pAGn->m_Seat.isLogin())
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendWorkerLoginResult("", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendWorkerLoginResult("", OnFail, "贛??腹?祆厚");
    else
      SendWorkerLoginResult("", OnFail, "the seatno have not login");
    return 1;
  }
  workerno = atoi(AGMsg.GetAttrValue(2).C_Str()); //話務員工號
  dutyno = atoi(AGMsg.GetAttrValue(10).C_Str());
  nAG1 = pAgentMng->GetnAGByWorkerNo(workerno);
  if (pAgentMng->isnAGAvail(nAG1))
  {
    if (nAG1 != nAGn)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendWorkerLoginResult("", OnFail, "該工號已登錄");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendWorkerLoginResult("", OnFail, "贛�籪���祆厚");
      else
        SendWorkerLoginResult("", OnFail, "the worker had login");
      return 1;
    }
  }
  if (1 == atoi(AGMsg.GetAttrValue(8).C_Str()))
  {
    pAGn->m_Worker.ClearWork();
  }
  else if (pIVRCfg->ClearCallCountType == 3 && workerno != pAGn->m_Worker.PreWorkerNo)
  {
    pAGn->m_Worker.ClearWork();
  }
  else if (pIVRCfg->ClearCallCountType == 4 && dutyno != pAGn->m_Worker.PreDutyNo)
  {
    pAGn->m_Worker.ClearWork();
  }
  if (workerno == pAGn->m_Worker.PreWorkerNo)
    WriteWorkerLogoutRecord(pAGn);

  pAGn->m_Worker.ClearWorkerParam();
  workergrade = atoi(AGMsg.GetAttrValue(6).C_Str()); //話務員類別
  pAGn->m_Worker.WorkerLogin(workerno, AGMsg.GetAttrValue(3).C_Str(), workergrade);
  UTF8_ANSI_Convert(AGMsg.GetAttrValue(3).C_Str(), pAGn->m_Worker.UnicodeWorkerName, CP_ACP, CP_UTF8);
  
  //ClearTheSameAccountNo(AGMsg.GetAttrValue(13).C_Str());
  //SaveWorkerAccountNo(nAGn, AGMsg.GetAttrValue(13).C_Str());
  pAGn->m_Worker.DepartmentNo = AGMsg.GetAttrValue(14);

  groupnum = SplitTxtLine(AGMsg.GetAttrValue(5).C_Str(), MAX_GROUP_NUM_FORWORKER, ArrString1);
  SplitTxtLine(AGMsg.GetAttrValue(7).C_Str(), MAX_GROUP_NUM_FORWORKER, ArrString2);
  for (int i=0; i<groupnum; i++)
  {
    groupno = atoi(ArrString1[i].C_Str());
    if (ArrString2[i].GetLength() > 0)
      level = atoi(ArrString2[i].C_Str());
    else
      level = 1;
    if (groupno > 0 && groupno < MAX_AG_GROUP)
    {
      pAGn->m_Worker.SetWorkerGroupNo(groupno, level);
      pAgentMng->WorkerGroup[groupno].state = 1;
    }
  }

  pAGn->m_Worker.WorkerCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
  pAGn->m_Worker.WorkerCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
  pAGn->m_Worker.WorkerCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
  pAGn->m_Worker.WorkerCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長
  
  pAGn->m_Seat.SeatCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
  pAGn->m_Seat.SeatCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
  pAGn->m_Seat.SeatCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
  pAGn->m_Seat.SeatCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長

  pAGn->m_Worker.DutyNo = dutyno;
  if (AGMsg.GetAttrValue(11).GetLength() == 0)
  {
    pAGn->m_Worker.AutoRecordId = 1;
  }
  else
  {
    pAGn->m_Worker.AutoRecordId = atoi(AGMsg.GetAttrValue(11).C_Str());
  }
  pAGn->m_Worker.CallDirection = atoi(AGMsg.GetAttrValue(12).C_Str());

  if (pAGn->m_Seat.SeatLangID == 1)
    SendWorkerLoginResult("歡迎登錄", OnSuccess, "");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendWorkerLoginResult("?�鬲瑹p", OnSuccess, "");
  else
    SendWorkerLoginResult("wellcome login", OnSuccess, "");

  pAGn->ChangeStatus(AG_STATUS_LOGIN);
  MyTrace(3, "WriteWorkerLoginStatus nAG=%d Proc_AGMSG_workerlogin()", pAGn->nAG);
  WriteWorkerLoginStatus(pAGn);

  if (1 == atoi(AGMsg.GetAttrValue(9).C_Str()))
  {
    pAGn->m_Worker.DisturbId = 1;
    pAGn->m_Worker.LeaveId = 0;
  } 
  else
  {
    pAGn->m_Worker.DisturbId = pAGn->m_Worker.OldDisturbId;
    pAGn->m_Worker.LeaveId = pAGn->m_Worker.OldLeaveId;
  }
  SendCommonResult(AGMSG_ondisturb, pAGn->m_Worker.DisturbId, "");
  SendCommonResult(AGMSG_onleval, pAGn->m_Worker.LeaveId, "");
  if (pAGn->m_Worker.DisturbId == 0 && pAGn->m_Worker.LeaveId == 0)
  {
    pAGn->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
  }
  else
  {
    pAGn->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_BUSY);
  }
  int nChn = pAGn->GetSeatnChn();

  if (pIVRCfg->AgentLogOnOffMode == 1)
  {
    Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), AGMsg.GetAttrValue(2).C_Str(), "", "", AG_AM_LOG_IN);
  }
  else
  {
    if (pIVRCfg->isAgentLoginSwitch == true && pAGn->m_Seat.LoginSwitchID == 1)
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), AGMsg.GetAttrValue(2).C_Str(), pAGn->m_Seat.LoginSwitchGroupID, pAGn->m_Seat.LoginSwitchPSW, AG_AM_LOG_IN);
    }
    if (pIVRCfg->isAgentLoginACDSplit == true)
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), AGMsg.GetAttrValue(2).C_Str(), AGMsg.GetAttrValue(5).C_Str(), "", AG_AM_LOG_IN_GROUP);
      strcpy(pAGn->m_Seat.LoginSwitchGroupID, AGMsg.GetAttrValue(5).C_Str());
    }
    if (pIVRCfg->isSetREADYonAgentLogin == true) //2014-10-15
    {
      if (pAGn->m_Worker.DisturbId == 0)
        Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetSeatNo().C_Str(), "", "", AG_AM_READY);
      else
        Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetSeatNo().C_Str(), "", "", AG_AM_NOT_READY);
    }
  }

  pAGn->m_Worker.DelayTime = g_nDefaultACWTimeLen;

  pAGn->m_Worker.PreWorkerNo = workerno;
  pAGn->m_Worker.PreGroupNo = pAGn->GetGroupNo();
  memcpy(pAGn->m_Worker.PreGroupNoList, pAGn->m_Worker.GroupNo, sizeof(pAGn->m_Worker.GroupNo));
  pAGn->m_Worker.PreDutyNo = dutyno;

  strcpy(pAGn->StateChangeTime, MyGetNow());
  pAGn->m_Worker.IdelTime = time(0);

  //WriteSeatStatus(pAGn, AG_STATUS_LOGIN, 0);
  AgentStatusCount();
  SendOneGroupStatusMeteCount(pAGn->nAG);
  SendSystemStatusMeteCount();
  //2016-07-04 恢復以前的免打擾狀態值
  if (pAGn->m_Worker.PreDisturbId == 2)
  {
    pAGn->m_Worker.DisturbId = pAGn->m_Worker.PreDisturbId;
    pAGn->m_Worker.PreDisturbId = 0;
  }

  DispAgentStatus(nAGn);
  return 0;
}
//話務員退出
int Proc_AGMSG_workerlogout(CXMLRcvMsg &AGMsg) 
{
  int nQue;
  int nGroupNo;
  if (pAGn->isLogin() == false)
  {
	  if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onworkerlogout, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onworkerlogout, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onworkerlogout, OnFail, "the seat had not login");
    return 1;
  }
  nQue = pAGn->nQue;
  if (pCalloutQue->isnQueAvail(nQue) && pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
  }
  if (pAGn->m_Worker.DisturbId == 3)
  {
    SendCommonResult(AGMSG_onworkerlogout, OnSuccess, "");
    return 0;
  }
  if (pAGn->m_Worker.LeaveId > 0)
    WriteSeatStatus(pAGn, AG_STATUS_LOGOUT, pAGn->m_Worker.LeaveId);
  else
    WriteSeatStatus(pAGn, AG_STATUS_LOGOUT, 0);
  WriteWorkerLogoutStatus(pAGn);
  int nChn = pAGn->GetSeatnChn();

  if (pIVRCfg->AgentLogOnOffMode == 1)
  {
    Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_LOG_OUT);
  }
  else
  {
    if (pIVRCfg->isAgentLoginSwitch == true && pAGn->m_Seat.LoginSwitchID == 1)
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), pAGn->m_Seat.LoginSwitchGroupID, pAGn->m_Seat.LoginSwitchPSW, AG_AM_LOG_OUT);
    }
    if (pIVRCfg->isAgentLoginACDSplit == true)
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), pAGn->m_Seat.LoginSwitchGroupID, "", AG_AM_LOG_OUT_GROUP);
      memset(pAGn->m_Seat.LoginSwitchGroupID, 0, 32);
    }
    if (pIVRCfg->isSetNOTREADYonAgentLogout == true) //2014-10-15
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetSeatNo().C_Str(), "", "", AG_AM_NOT_READY);
    }
  }

  //2016-07-04 保存以前的免打擾狀態值
  if (pAGn->m_Worker.DisturbId == 2)
  {
    pAGn->m_Worker.PreDisturbId = pAGn->m_Worker.DisturbId;
    pAGn->timer = 0;
  }

  nGroupNo = pAGn->GetGroupNo();
  if (pAGn->m_Worker.oldWorkerNo == 0)
  {
    pAGn->m_Worker.Logout();
    pAGn->m_Worker.ClearState();
  }
  else
  {
    pAGn->m_Worker.OldWorkerLogin();
  }
  pAGn->m_Worker.OldDisturbId = 0;
  pAGn->m_Worker.OldLeaveId = 0;

  SendCommonResult(AGMSG_onworkerlogout, OnSuccess, "");
  DispAgentStatus(nAGn);

  //WriteSeatStatus(pAGn, AG_STATUS_LOGOUT, 0);
  AgentStatusCount();
  SendOneGroupStatusMeteCount(pAGn->nAG);
  for (int i=1; i<MAX_GROUP_NUM_FORWORKER; i++)
    SendOneGroupStatusMeteCountB(pAGn->m_Worker.PreGroupNoList[i]);
  SendSystemStatusMeteCount();

  return 0;
}
//免打擾示忙閑設置
int Proc_AGMSG_disturb(CXMLRcvMsg &AGMsg) 
{
  int flag;

  if (!pAGn->isLogin())
    return 1;
  if (pAGn->isIdelForSrv() == false)
  {
    return 1;
  }
  flag = atoi(AGMsg.GetAttrValue(2).C_Str()); //1-免打擾,示忙 0-可打擾,示閑
  if (pAGn->m_Worker.DisturbId == 3)
    SendCommonResult(AGMSG_onsetgoouttel, 0, "");

  int nChn = pAGn->GetSeatnChn();

  if (flag == 0)
  {
    //2013-08-01 add
    pAGn->m_Worker.SetDisturb(flag); //2015-12-14 這個要放在 WriteSeatStatus 前執行，不然ACW計數不會-1
    if (pAGn->m_Worker.LeaveId > 0)
      WriteSeatStatus(pAGn, AG_STATUS_IDEL, pAGn->m_Worker.LeaveId);
    else
      WriteSeatStatus(pAGn, AG_STATUS_IDEL, 0);
    
    SendCommonResult(AGMSG_ondisturb, flag, "");
    pAGn->m_Worker.SetLeave(0,"");
    if (pAGn->m_Worker.WorkerCallCountParam.BusyCount > 0)
      pAGn->m_Worker.WorkerCallCountParam.BusyTimeAvgLen = pAGn->m_Worker.WorkerCallCountParam.BusyTimeSumLen/pAGn->m_Worker.WorkerCallCountParam.BusyCount;
    if ((pIVRCfg->isAgentLoginSwitch == true && pAGn->m_Seat.LoginSwitchID == 1) || pIVRCfg->isSetReadyStateonAgentSetDisturb > 0)
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_READY);
    }
  }
  else
  {
    if (pAGn->m_Worker.LeaveId > 0)
    {
      return 1; //小休時不可以示忙 //2013-08-01 add
    }
    pAGn->m_Worker.SetDisturb(flag);
    SendCommonResult(AGMSG_ondisturb, flag, "");
    pAGn->m_Worker.SetLeave(0,"");
    pAGn->m_Worker.WorkerCallCountParam.BusyCount++;
    WriteSeatStatus(pAGn, AG_STATUS_BUSY, 0);
    if ((pIVRCfg->isAgentLoginSwitch == true && pAGn->m_Seat.LoginSwitchID == 1) || pIVRCfg->isSetReadyStateonAgentSetDisturb > 0)
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
    }
  }
  SendCommonResult(AGMSG_onleval, 0, "");
  DispAgentStatus(nAGn);

  AgentStatusCount();
  SendOneGroupStatusMeteCount(pAGn->nAG);
  SendSystemStatusMeteCount();
  return 0;
}
//離席/在席設置
int Proc_AGMSG_leval(CXMLRcvMsg &AGMsg) 
{
  int flag;

  if (!pAGn->isLogin())
    return 1;
  if (pAGn->isIdelForSrv() == false)
  {
    return 1;
  }
  if (pAGn->m_Worker.DisturbId == 1)
  {
    return 1; //示忙時不可以離席/在席設置 //2013-08-01 add
  }
  int nChn = pAGn->GetSeatnChn();
  flag = atoi(AGMsg.GetAttrValue(2).C_Str()); //0-在席 >1-離席
  if (flag == 0)
  {
    if (pAGn->m_Worker.LeaveId == 0)
    {
      return 1; //已經是在席時不能再按
    }
    WriteSeatStatus(pAGn, AG_STATUS_IDEL, pAGn->m_Worker.LeaveId);
    if (pAGn->m_Worker.WorkerCallCountParam.LeaveCount > 0)
      pAGn->m_Worker.WorkerCallCountParam.LeaveTimeAvgLen = pAGn->m_Worker.WorkerCallCountParam.LeaveTimeSumLen/pAGn->m_Worker.WorkerCallCountParam.LeaveCount;
    
    if (pIVRCfg->AgentLogOnOffMode == 0)
    {
      if ((pIVRCfg->isAgentLoginSwitch == true && pAGn->m_Seat.LoginSwitchID == 1) || pIVRCfg->isSetReadyStateonAgentSetDisturb > 0)
      {
        Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_READY);
      }
    }
    else
    {
      Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_READY);
    }
  }
  else
  {
    pAGn->m_Worker.WorkerCallCountParam.LeaveCount++;
    if (pAGn->m_Worker.LeaveId > 0)
      WriteSeatStatus(pAGn, AG_STATUS_LEAVE, pAGn->m_Worker.LeaveId); //2013-08-01 add
    else
      WriteSeatStatus(pAGn, AG_STATUS_LEAVE, 0);
    
    if (pIVRCfg->AgentLogOnOffMode == 0)
    {
      if ((pIVRCfg->isAgentLoginSwitch == true && pAGn->m_Seat.LoginSwitchID == 1) || pIVRCfg->isSetReadyStateonAgentSetDisturb > 0)
      {
        Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
      }
    }
    else
    {
      if (flag == 1)
      {
        Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_WORK_NOT_READY);
      }
      else
      {
        Send_SWTMSG_setagentstatus(nChn, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
      }
    }
  }
  pAGn->m_Worker.SetLeave(flag,AGMsg.GetAttrValue(3));
  UTF8_ANSI_Convert(AGMsg.GetAttrValue(3).C_Str(), pAGn->m_Worker.UnicodeLeaveReason, CP_ACP, CP_UTF8);
  SendCommonResult(AGMSG_onleval, flag, "");
  DispAgentStatus(nAGn);

  AgentStatusCount();
  SendOneGroupStatusMeteCount(pAGn->nAG);
  SendSystemStatusMeteCount();
  return 0;
}
//設置呼叫轉移
int Proc_AGMSG_setforward(CXMLRcvMsg &AGMsg)
{
  int forwardtype;

  if (!pAGn->isLogin())
    return 1;
  int nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    forwardtype = atoi(AGMsg.GetAttrValue(2).C_Str()); //呼叫轉移類型: 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移
    switch (forwardtype)
    {
    case 1:
      pAGn->m_Seat.ForwardNo = AGMsg.GetAttrValue(3);
      break;
    case 2:
      pAGn->m_Seat.BusyForwardNo = AGMsg.GetAttrValue(3);
      break;
    case 3:
      pAGn->m_Seat.NoAnsForwardNo = AGMsg.GetAttrValue(3);
      break;
    }
    Send_SWTMSG_settranphone(nChn, pChn->DeviceID, forwardtype, AGMsg.GetAttrValue(3).C_Str());
    SendCommonResult(AGMSG_onsetforward, OnSuccess, "");
  }
  return 0;
}
//取消呼叫轉移
int Proc_AGMSG_cancelforward(CXMLRcvMsg &AGMsg)
{
  int forwardtype;

  if (!pAGn->isLogin())
    return 1;
  forwardtype = atoi(AGMsg.GetAttrValue(2).C_Str()); //呼叫轉移類型: 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移
  switch (forwardtype)
  {
  case 0:
    pAGn->m_Seat.ForwardNo = "";
    pAGn->m_Seat.BusyForwardNo = "";
    pAGn->m_Seat.NoAnsForwardNo = "";
    break;
  case 1:
    pAGn->m_Seat.ForwardNo = "";
    break;
  case 2:
    pAGn->m_Seat.BusyForwardNo = "";
    break;
  case 3:
    pAGn->m_Seat.NoAnsForwardNo = "";
    break;
  }
  SendCommonResult(AGMSG_oncancelforward, OnSuccess, "");
  return 0;
}
//應答來話
int Proc_AGMSG_answercall(CXMLRcvMsg &AGMsg)
{
  int nChn, nAG, flag, nQue, chState, nOut, nAcd;

  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onanswercall, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onanswercall, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onanswercall, OnFail, "the seat had not login");
    return 1;
  }
  if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
  {
    return 1;
  }
  flag = atoi(AGMsg.GetAttrValue(2).C_Str()); //0-電腦坐席收到呼入正在振鈴 1-應答來話 2-拒絕來話
  pAGn->AcceptSerialNo = AGMsg.GetAttrValue(3);
  pAGn->AcceptParam = AGMsg.GetAttrValue(4);

  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nQue = pAGn->nQue;
    if (pCalloutQue->isnQueAvail(nQue))
    {
      if (pQue->OrgCallType == 2)
      {
        nAcd = pQue->nAcd;
        if (pAcd->Session.MediaType > 0)
        {
          switch (flag)
          {
          case 0:
            pQue->CalloutResult = CALLOUT_RING;
            break;
          case 1:
            pQue->CalloutResult = CALLOUT_CONN;
            break;
          case 2:
            pQue->CalloutResult = CALLOUT_REFUSE;
            break;
          }
          return 0;
        }
      }
    }
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onanswercall, OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onanswercall, OnFail, "贛????﹚?杠?笵");
      else
        SendCommonResult(AGMSG_onanswercall, OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (flag == 1)
    {
      Send_SWTMSG_answercall(nChn, pChn->DeviceID, pChn->ConnID);
    }
    return 0;
  }
  nQue = pAGn->nQue;
  if (!pCalloutQue->isnQueAvail(nQue))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onanswercall, OnFail, "沒有電話呼入");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onanswercall, OnFail, "⊿Τ?杠㊣��");
    else
      SendCommonResult(AGMSG_onanswercall, OnFail, "have not callin");
    return 1;
  }

  if (pQue->OrgCallType == 2)
  {
    nAcd = pQue->nAcd;
    if (pAcd->Session.MediaType > 0)
    {
      switch (flag)
      {
      case 0:
        pQue->CalloutResult = CALLOUT_RING;
        break;
      case 1:
        pQue->CalloutResult = CALLOUT_CONN;
        break;
      case 2:
        pQue->CalloutResult = CALLOUT_REFUSE;
        break;
      }
      return 0;
    }
  }

  switch (flag)
  {
  case 0: //0-電腦坐席收到呼入正在振鈴
    if (g_nCardType == 9)
    {
      pQue->CalloutResult = CALLOUT_RING;
      if (pQue->OrgCallType == 1)
      {
        nOut = pQue->nOut;
        pOut->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
      }
      else
      {
        nAcd = pQue->nAcd;
        pAcd->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
      }
      nAG = pQue->nAG;
      if (pAgentMng->isnAGAvail(nAG))
      {
        SetAgentsvState(nAG, AG_SV_INRING);
      }
      nChn = pAGn->GetSeatnChn();
      pChn->ssState = CHN_SNG_OT_RING; //呼出振鈴
      DispChnStatus(nChn);
      break;
    }
    if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
    {
      chState = GetAgentChState(pAGn);
      if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
        pQue->CalloutResult = CALLOUT_RING;
    }
    else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
    {
      chState = GetAgentChState(pAGn);
      if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
        pQue->CalloutResult = CALLOUT_RING;
    }
    break;
  case 1: //1-應答來話
    if (g_nCardType == 9)
    {
      pQue->CalloutResult = CALLOUT_CONN;
      nChn = pAGn->GetSeatnChn();
      pChn->ssState = CHN_SNG_OT_TALK; //呼出振鈴
      pChn->CalloutId = 0;
      pChn->nQue = 0xFFFF;
      pChn->CanPlayOrRecId = 1; //允許開始放錄音
      pChn->AnsTime = time(0);
      DispChnStatus(nChn);
      break;
    }
    chState = GetAgentChState(pAGn);
    if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
    {
//       if (g_nCardType == 5) //FREESWITCH就直接發應答 2013-09-25
//       {
//         nChn = pAGn->GetSeatnChn();
//         SendACK(nChn, 1);
//         break;
//       }
      if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
      {
        pQue->CalloutResult = CALLOUT_CONN; //應答成功結果在發送呼叫坐席應答結果的地方發送
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendCommonResult(AGMSG_onanswercall, 2, "該坐席電話還未摘機,請先摘機然后點擊應答按鈕");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendCommonResult(AGMSG_onanswercall, 2, "贛???杠臨?篕訣,叫�g篕訣礛�A翴闌萊氮�V?");
        else
          SendCommonResult(AGMSG_onanswercall, 2, "");
      }
    }
    else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
    {
      if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
      {
        pQue->CalloutResult = CALLOUT_CONN; //應答成功結果在發送呼叫坐席應答結果的地方發送
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendCommonResult(AGMSG_onanswercall, 2, "請摘起坐席話機應答來話");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendCommonResult(AGMSG_onanswercall, 2, "叫篕癬??杠訣萊氮ㄓ杠");
        else
          SendCommonResult(AGMSG_onanswercall, 2, "");
      }
    }
//     else
//     {
//       if (chState != CHN_SNG_OT_TALK)
//       {
//         if (pAGn->m_Seat.SeatLangID == 1)
//           SendCommonResult(AGMSG_onanswercall, 2, "請摘起坐席話機應答來話");
//         else if (pAGn->m_Seat.SeatLangID == 2)
//           SendCommonResult(AGMSG_onanswercall, 2, "叫篕癬??杠訣萊氮ㄓ杠");
//         else
//           SendCommonResult(AGMSG_onanswercall, 2, "");
//       }
//     }
    break;
  case 2: //2-拒絕來話
    pQue->CalloutResult = CALLOUT_REFUSE;
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onanswercall, 3, "成功拒絕來話");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onanswercall, 3, "Θ��?蕩ㄓ杠");
    else
      SendCommonResult(AGMSG_onanswercall, 3, "");
    break;
  }
  return 0;
}
//發起呼叫
int Proc_AGMSG_makecall(CXMLRcvMsg &AGMsg)
{
  int nChn, chState, calledtype, nAG, workerno, len;
  UL SessionNo;
  char szCalled[32];//, szTemp[32];

  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛???祆厚");
    else
      SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
    return 1;
  }
  if (AGMsg.GetAttrValue(4).GetLength() == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "未輸入被叫號碼");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "?塊�迍��兢≠�");
    else
      SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛????﹚?杠?笵");
      else
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "the seat have not bangding channel");
      return 1;
    }
    //被叫類別: 0-外線呼入號碼(外線有效) 1-呼出的外線號碼 2-呼叫的內線坐席號 3-呼叫的坐席工號
    calledtype = atoi(AGMsg.GetAttrValue(5).C_Str());
    if (calledtype == 3)
    {
      workerno = atoi(AGMsg.GetAttrValue(4).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛�籪�?�Z�I");
        else
          SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛�籪�?�Z�I");
        else
          SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
        return 1;
      }
      else
      {
        if (pAG->isIdelForSrv() == false)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該工號正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛�籪�?Γ");
          else
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
          return 1;
        }
        Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, AGMsg.GetAttrValue(2).C_Str(), pAG->m_Seat.SeatNo.C_Str(), atoi(AGMsg.GetAttrValue(8).C_Str()), AGMsg.GetAttrValue(10).C_Str(), 0);
        return 0;
      }
    }
    else if (calledtype == 2)
    {
      nAG = pAgentMng->GetnAGBySeatNo(AGMsg.GetAttrValue(4).C_Str());
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (pAG->isIdelForSrv() == false)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該坐席正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛???Γ");
          else
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
          return 1;
        }
      }
    }
    char szDialParam[256];
    memset(szDialParam, 0, 256);
    if (strncmp(AGMsg.GetAttrValue(6).C_Str(), "DIALOUT:", 8) == 0)
      strcpy(szDialParam, AGMsg.GetAttrValue(6).C_Str());
    else
      strcpy(szDialParam, AGMsg.GetAttrValue(10).C_Str());

    GetDialOutId(szDialParam, nChn);

    /*len = strlen(pIVRCfg->LocaAreaCode);
    if (pIVRCfg->ProcLocaAreaCodeId == 1 && len > 0)
    {
      memset(szCalled, 0, 32);
      strncpy(szCalled, AGMsg.GetAttrValue(4).C_Str(), 31);
      ClearCharInCalledNo(szCalled);
      strcpy(szTemp, szCalled);
      if (strncmp(szCalled, pIVRCfg->LocaAreaCode, len) == 0)
      {
        memset(szTemp, 0, 32);
        strncpy(szTemp, &szCalled[len], 20);
      }
      Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, AGMsg.GetAttrValue(2).C_Str(), szTemp, atoi(AGMsg.GetAttrValue(8).C_Str()), szDialParam, calledtype);
    }
    else
    {*/
      memset(szCalled, 0, 32);
      strncpy(szCalled, AGMsg.GetAttrValue(4).C_Str(), 31);
      ClearCharInCalledNo(szCalled);
      Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, AGMsg.GetAttrValue(2).C_Str(), szCalled, atoi(AGMsg.GetAttrValue(8).C_Str()), szDialParam, calledtype);
    //}

    return 0;
  }

  //-------------------------板卡版本-----------------------------------------------
  nChn = pAGn->GetSeatnChn();
  if (g_nCardType == 9)
  {
    pChn->timer = 0;
    pChn->ssState = CHN_SNG_IDLE;
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInId = 1; //有呼叫進入
    pChn->CallInOut = CALL_OUT;

    strncpy(pChn->CallerNo, AGMsg.GetAttrValue(2).C_Str(), MAX_TELECODE_LEN-1);
    strncpy(pChn->CalledNo, AGMsg.GetAttrValue(4).C_Str(), MAX_TELECODE_LEN-1);
    len = strlen(pIVRCfg->DialOutPreCode);
    if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
    {
      strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
    }
    else
    {
      strcpy(pChn->CustPhone, pChn->CalledNo);
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
    }
    pChn->OrgCallerNo = AGMsg.GetAttrValue(6);
    pChn->OrgCalledNo = AGMsg.GetAttrValue(7);
    DispChnStatus(nChn);
    return 0;
  }
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_TEL) || pAGn->isWantSeatType(SEATTYPE_DT_TEL))
  {
    //電話坐席
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛????﹚?杠?笵");
      else
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "the seat have not bangding channel");
      return 1;
    }

    if (chState == CHN_SNG_IDLE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "請先摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "叫�g篕訣");
      else
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "please hangoff");
      return 1;
      /*strncpy(pChn->CallerNo, AGMsg.GetAttrValue(2).C_Str(), MAX_TELECODE_LEN-1);
      strncpy(pChn->CalledNo, AGMsg.GetAttrValue(4).C_Str(), MAX_TELECODE_LEN-1);
      len = strlen(pIVRCfg->DialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      pChn->CallInOut = CALL_OUT;
      pChn->OrgCallerNo = pAGn->GetPhoneNo();
      pChn->OrgCalledNo = AGMsg.GetAttrValue(7);
      pChn->GenerateCdrSerialNo();
      SendCallinEvent(nChn, 1, atoi(AGMsg.GetAttrValue(5).C_Str()), AGMsg.GetAttrValue(10).C_Str(), 1);
      DBInsertCallCDR(nChn, 2, 0);
      DispChnStatus(nChn);
      return 0;*/
    }
    else if (chState == CHN_SNG_IN_ARRIVE || chState == CHN_SNG_IN_WAIT)
    {
      //坐席通道處于撥號狀態
      //被叫類別: 0-外線呼入號碼(外線有效) 1-呼出的外線號碼 2-呼叫的內線坐席號 3-呼叫的坐席工號
      calledtype = atoi(AGMsg.GetAttrValue(5).C_Str());
      if (calledtype == 3)
      {
        workerno = atoi(AGMsg.GetAttrValue(4).C_Str());
        if (workerno == 0)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該工號不存在");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛�籪�?�Z�I");
          else
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
          return 1;
        }
        nAG = pAgentMng->GetnAGByWorkerNo(workerno);
        if (!pAgentMng->isnAGAvail(nAG))
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該工號不存在");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛�籪�?�Z�I");
          else
            SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
          return 1;
        }
        else
        {
          if (pAG->isIdelForSrv() == false)
          {
            if (pAGn->m_Seat.SeatLangID == 1)
              SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該工號正忙");
            else if (pAGn->m_Seat.SeatLangID == 2)
              SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛�籪�?Γ");
            else
              SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
            return 1;
          }
          strncpy(pChn->CalledNo, pAG->m_Seat.SeatNo.C_Str(), MAX_TELECODE_LEN-1);
        }
      }
      else
      {
        strncpy(pChn->CalledNo, AGMsg.GetAttrValue(4).C_Str(), MAX_TELECODE_LEN-1);
      }
      
      len = strlen(pIVRCfg->DialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      pChn->OrgCallerNo = AGMsg.GetAttrValue(6);
      pChn->OrgCalledNo = AGMsg.GetAttrValue(7);
      pChn->CallOutRouteNo = atoi(AGMsg.GetAttrValue(8).C_Str());
      pChn->CallData = AGMsg.GetAttrValue(10);
      DispChnStatus(nChn);
      return 0;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_DT_PC))
  {
    //電腦坐席
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "贛????﹚?杠?笵");
      else
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "the seat have not bangding channel");
      return 1;
    }

    if (pAGn->isIdelForSrv() == false)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "????杠?Γ");
      else
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
      return 1;
    }
    
    if (chState == CHN_SNG_IDLE)
    {
      strncpy(pChn->CalledNo, AGMsg.GetAttrValue(4).C_Str(), MAX_TELECODE_LEN-1);
      len = strlen(pIVRCfg->DialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      pChn->CallInOut = CALL_OUT;
      pChn->CallOutRouteNo = atoi(AGMsg.GetAttrValue(8).C_Str());
      pChn->OrgCallerNo = pAGn->GetPhoneNo();
      pChn->OrgCalledNo = AGMsg.GetAttrValue(7);
      pChn->GenerateCdrSerialNo();
      SendCallinEvent(nChn, 1, atoi(AGMsg.GetAttrValue(5).C_Str()), AGMsg.GetAttrValue(10).C_Str(), 1);
      DBInsertCallCDR(nChn, 1, 0);
      DBUpdateCallCDR_AnsWorkerNo(nChn, nAGn);
      strcpy(pAGn->CdrSerialNo, pChn->CdrSerialNo); //2012-08-30 for sqcw
      DispChnStatus(nChn);
      return 0;
    }
    else if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
    {
      strncpy(pChn->CalledNo, AGMsg.GetAttrValue(4).C_Str(), MAX_TELECODE_LEN-1);
      len = strlen(pIVRCfg->DialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      pChn->CallInOut = CALL_OUT; //2016-11-11
      pChn->OrgCallerNo = AGMsg.GetAttrValue(6);
      pChn->OrgCalledNo = AGMsg.GetAttrValue(7);
      pChn->GenerateCdrSerialNo();
      SendCallinEvent(nChn, 1, atoi(AGMsg.GetAttrValue(5).C_Str()), AGMsg.GetAttrValue(10).C_Str());
      DBInsertCallCDR(nChn, 1, 0); //2016-11-11這里應該要插入話單的
      DispChnStatus(nChn);
      SetAgentsvState(nAGn, AG_SV_OUTSEIZE);
      return 0;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    //遠端坐席
    if (pAGn->isIdelForSrv() == false)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "????杠?Γ");
      else
        SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
      return 1;
    }
    if (chState == CHN_SNG_IDLE)
    {
      sprintf(SendMsgBuf, "<oncallin chantype='%d' channo='%d' callerno='%s' callertype='0' calledno='%s' calledtype='%d' orgcallerno='%s' orgcalledno='%s' inrouteno='%d' chhwtype='%d' chhwindex='%d' seattype='%d' seatno='%s' workerno='%d' param='%s' groupno='%d' bandagfirst='1' answerid='0' cdrserialno='' recdrootpath='' recdfilename=''/>", 
        0, 0, AGMsg.GetAttrValue(2).C_Str(), AGMsg.GetAttrValue(4).C_Str(), atoi(AGMsg.GetAttrValue(5).C_Str()),
        pAGn->GetPhoneNo().C_Str(), AGMsg.GetAttrValue(7).C_Str(), 
        0, 0, 0, pAGn->GetSeatType(), pAGn->GetSeatNo().C_Str(), 
        pAGn->GetWorkerNo(), AGMsg.GetAttrValue(10).C_Str(), pAGn->GetGroupNo());
      SessionNo = ((1 << 16) & 0x00FF0000) | 0x02000000;
      SendMsg2XML(MSG_oncallin, SessionNo, SendMsgBuf);
      return 0;
    }
    else if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
    {
      strncpy(pChn->CalledNo, AGMsg.GetAttrValue(4).C_Str(), MAX_TELECODE_LEN-1);
      len = strlen(pIVRCfg->DialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      pChn->OrgCallerNo = pAGn->GetPhoneNo();
      pChn->OrgCalledNo = AGMsg.GetAttrValue(7);
      pChn->GenerateCdrSerialNo();
      SendCallinEvent(nChn, 1, atoi(AGMsg.GetAttrValue(5).C_Str()), AGMsg.GetAttrValue(10).C_Str());
      DispChnStatus(nChn);
      SetAgentsvState(nAGn, AG_SV_OUTSEIZE);
      return 0;
    }
  }
  
  if (pAGn->m_Seat.SeatLangID == 1)
    SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "本坐席電話正忙");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "????杠?Γ");
  else
    SendSeatMakeCallFailResult(nAGn, AGMsg.GetAttrValue(10).C_Str(), OnOutFail, "");
  return 1;
}
//坐席掛機
int Proc_AGMSG_hangon(CXMLRcvMsg &AGMsg)
{
  int nChn;

  if (!pAGn->isLogin())
    return 1;
  nChn = pAGn->GetSeatnChn();
  MyTrace(3, "Proc_AGMSG_hangon nChn=%d nAG=%d svState=%d DelayState=%d DelayTime=%d",
    nChn, pAGn->nAG, pAGn->GetsvState(), pAGn->DelayState, pAGn->m_Worker.DelayTime);
  if (pAGn->isThesvState(AG_SV_IDEL) == true)
  {
    Hangup(nChn); //2015-04-16防止狀態不對時掛不了電話
    return 0;
  }
  Release_Chn(nChn, 1, 1);
  return 0;
}
//快速轉接電話
int Proc_AGMSG_blindtrancall(CXMLRcvMsg &AGMsg)
{
  int i, nChn, nChn1, phonetype, nAG, workerno;
  int nOut, calledtype;
  char CalledNo[MAX_TELECODE_LEN], TranParam[256];

  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛???祆厚");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    memset(TranParam, 0, 256);
    if (AGMsg.GetAttrValue(4).GetLength() > 0)
    {
      sprintf(TranParam, "$S_DialParam=%s;", AGMsg.GetAttrValue(4).C_Str());
    }
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????﹚?杠?笵");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "the seat have not bangding channel");
      return 1;
    }
    nAG = 0xFFFF;
    phonetype = atoi(AGMsg.GetAttrValue(2).C_Str());
    if (phonetype == 3)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
      else
      {
        if (pAG->isIdelForSrv() == false)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
          return 1;
        }
        pChn->TranIVRId = 0;

        //2015-11-25 針對語音中繼的修改
        pAG->m_Seat.nCallInnChn = pAGn->m_Seat.nCallInnChn;
        pAGn->m_Seat.nTranAGnChn = pAG->GetSeatnChn();
        strcpy(pAG->m_Seat.CustPhone, pChn->CustPhone); 
        MyTrace(3, "Proc_AGMSG_blindtrancall1 nChn=%d CallerNo=%s CalledNo=%s CustPhone=%s Set nAG=%d CustPhone=%s",
          nChn, pChn->CallerNo, pChn->CalledNo, pChn->CustPhone, pAG->nAG, pAG->m_Seat.CustPhone);
        
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, pAG->m_Seat.SeatNo.C_Str(), 2, TranParam, phonetype);
        pAGn->TranDesnAG = nAG;
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "正在轉接");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "?�I鑼鋇");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "transfering...");
        return 0;
      }
    }
    else if (phonetype == 2)
    {
      nAG = pAgentMng->GetnAGBySeatNo(AGMsg.GetAttrValue(3).C_Str());
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (pAG->isIdelForSrv() == false)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的坐席正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y???Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
          return 1;
        }
        //2015-11-25 針對語音中繼的修改
        pAG->m_Seat.nCallInnChn = pAGn->m_Seat.nCallInnChn;
        pAGn->m_Seat.nTranAGnChn = pAG->GetSeatnChn();
        strcpy(pAG->m_Seat.CustPhone, pChn->CustPhone); 
        MyTrace(3, "Proc_AGMSG_blindtrancall2 nChn=%d CallerNo=%s CalledNo=%s CustPhone=%s Set nAG=%d CustPhone=%s",
          nChn, pChn->CallerNo, pChn->CalledNo, pChn->CustPhone, pAG->nAG, pAG->m_Seat.CustPhone);
      }
    }

    pChn->TranIVRId = 0;
    Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, AGMsg.GetAttrValue(3).C_Str(), 2, TranParam, phonetype);
    pAGn->TranDesnAG = nAG;
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "正在轉接");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "?�I鑼鋇");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "transfering...");
    return 0;
  }
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "IVR仿真測試環境不允許轉接電話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "IVR?痷代剛吏掛???鑼鋇?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "can not transfer");
    return 1;
  }

  if (pAGn->CanTransferId == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未處于可轉接電話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????���紜r鋇?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }

  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????﹚?杠?笵");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "the seat have not bangding channel");
    return 1;
  }

  if (AGMsg.GetAttrValue(3).GetLength() == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "未輸入轉接號碼");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "?塊�芢r鋇腹絏");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }
  if (pAGn->svState != AG_SV_CONN && pAGn->svState != AG_SV_HOLD)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未處于通話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????��?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }

  //--------板卡版本------------------------------------------------------
  if (pIVRCfg->ControlCallModal == 1)
  {
    if (pChn->TranStatus != 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席正處于轉接狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????�牓r鋇�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }
    nChn1 = pChn->LinkChn[0];
    if (!pBoxchn->isnChnAvail(nChn1))
    {
      nChn1 = pChn->HoldingnChn;
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席無可轉接的電話");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛???�紜r鋇�Y?杠");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
    }
    if (pChn1->ssState != CHN_SNG_IN_TALK && pChn1->ssState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "被轉接的電話未處于通話狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "砆鑼鋇�Y?杠??��?杠�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }
    if (pChn1->TranStatus != 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "被轉接的電話已處于轉接狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "砆鑼鋇�Y?杠��?�牓r鋇�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }

    nAG = 0xFFFF;
    //取被轉接的號碼及號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
    calledtype = atoi(AGMsg.GetAttrValue(2).C_Str());
    memset(CalledNo, 0, MAX_TELECODE_LEN);
    if (calledtype == 2)
    {
      strncpy(CalledNo, AGMsg.GetAttrValue(3).C_Str(), MAX_TELECODE_LEN-1);
    }
    else if (calledtype == 3)
    {
      //轉接的是工號,則查詢登錄的坐席號
      nAG = pAgentMng->GetnAGByWorkerNo(atoi(AGMsg.GetAttrValue(3).C_Str()));
      if (pAgentMng->isnAGAvail(nAG))
      {
        calledtype = 2;
        strcpy(CalledNo, pAG->m_Seat.SeatNo.C_Str());
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
    }
    else if (calledtype == 4)
    {
      CACDRule ACDRule;
      ACDRule.TranCallFailCallBack = 0;
      ACDRule.AcdedCount = 0;
      ACDRule.SeatType = 0;
      ACDRule.SeatNo = "0";
      ACDRule.WorkerNo = 0;
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
      {
        ACDRule.GroupNo[i] = -1;
        ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = 0;
      }
      ACDRule.GroupNo[0] = (UC)atoi(AGMsg.GetAttrValue(3).C_Str());
      ACDRule.Level[0] = 0;
      ACDRule.NowLevel[0] = 0;
      ACDRule.AcdedGroupNo = 0;
      ACDRule.AcdRule = 2;
      ACDRule.LevelRule = 0;
      ACDRule.CallMode = 0;
      ACDRule.SeatGroupNo = 0;
      ACDRule.WaitTimeLen = 0;
      ACDRule.RingTimeLen = 0;
      ACDRule.BusyWaitId = 0;
      ACDRule.Priority = 0;
      
      ACDRule.QueueTime = time(0);
      ACDRule.StartRingTime = time(0);
      for (i = 0; i < MAX_AG_NUM; i ++)
      {
        ACDRule.AcdednAGList[i] = -1;
      }
      CAgent *pAgent3=GetIdelAgentForCallOut(ACDRule);
      if (pAgent3 != NULL)
      {
        nAG = pAgent3->nAG;
        strcpy(CalledNo, pAgent3->GetSeatNo().C_Str());
      }
      else
      {
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "all busy");
        return 1;
      }
      calledtype = 2;
    }
    else
    {
      calledtype = 1;
      strncpy(CalledNo, AGMsg.GetAttrValue(3).C_Str(), MAX_TELECODE_LEN-1);
    }
    if (calledtype == 2)
    {
      //如果轉接的是坐席,判斷是否空閑
      nAG = pAgentMng->GetnAGBySeatNo(CalledNo);
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (!pAG->isIdelForSrv())
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的坐席正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y???Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
          return 1;
        }
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的坐席分機號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y???訣腹?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
    }

    nOut = pCallbuf->Get_Idle_nOut();
    if (!pCallbuf->isnOutAvail(nOut))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接緩沖區溢出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇絯≧跋犯�|");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }
    pAGn->TranDesnAG = nAG;
    //設置呼出數據
    pOut->state = 1;
    pOut->timer = 0;
    pOut->CallStep = 0;
    
    if (calledtype == 1)
      pOut->RingTime = 45;
    else
      pOut->RingTime = 30;
    pOut->IntervalTime = 0;
    
    pOut->Session.SessionNo = 0;
    pOut->Session.CmdAddr = 0;
    pOut->Session.nChn = nChn;
    pOut->Session.DialSerialNo = GetCallOutSerialNo(nChn);
    pOut->Session.SerialNo = 1;
    pOut->Session.CmdId = 3; //坐席轉接
    pOut->Session.VxmlId = 1;
    pOut->Session.FuncNo = 0;
    pOut->Session.CallerNo = pChn->CustPhone;
    pOut->Session.CalledNo = CalledNo;
    pOut->Session.GenerateCdrSerialNo(pChn->lgChnType, pChn->lgChnNo);
    char szTemp[256];
    sprintf(szTemp, "0`%s`%s`%s", pOut->Session.CdrSerialNo, pOut->Session.RecordFileName, pIVRCfg->SeatRecPath.C_Str());
    pOut->Session.Param = szTemp;
    
    if (calledtype == 1)
    {
      pOut->CallerNo = pIVRCfg->CenterCode;
    } 
    else
    {
      if (pChn->CallInOut == 1)
        pOut->CallerNo = pChn->CallerNo;
      else
        pOut->CallerNo = pChn->CalledNo;
    }
    pOut->CallerType = 0;
    pOut->CallType = 0;
    pOut->CallSeatType = 1;
    pOut->CancelId = 0;
    
    pOut->CallMode = 0;
    pOut->CallTimes = 1;
    pOut->CalledTimes = 0;
    
    pOut->CalledNum = 1;
    strcpy(pOut->Calleds[0].CalledNo, CalledNo);
    pOut->Calleds[0].CalledType = calledtype;
    pOut->Calleds[0].pQue_str = NULL;
    pOut->Calleds[0].CalloutResult = 0;
    pOut->Calleds[0].OutnChn = 0xFFFF;
    pOut->RouteNo[0] = 1;
    
    pOut->StartPoint = 0;
    pOut->CallPoint = 0;
    pOut->CallTime = 0;
    
    pOut->OutnChn = 0xFFFF;

    pOut->TranMode = 1; //快速盲轉
    pChn->nOut = nOut;
    pOut->TranCallFailReturn = 0;
    pOut->nAG = 0xFFFF;
    
    //斷開與正在的通話,并對其放等待音
    pChn->TranStatus = 3;
    pChn->TranControlnChn = nChn1;
    pChn1->TranStatus = 1;
    pChn1->TranControlnChn = nChn;
    
    pOut->WaitVocFile = pIVRCfg->WaitVocFile;
    if (pChn->LinkType[0] != 0)
    {
      UnRouterTalk(nChn, nChn1);
      pChn->LinkType[0] = 0;
      pChn->LinkChn[0] = 0xFFFF;
      pChn->HoldingnChn = nChn1;
      pChn1->LinkType[0] = 0;
      pChn1->LinkChn[0] = 0xFFFF;
      pChn1->HoldingnChn = nChn;
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      PlayFile(nChn1, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      
      WriteSeatStatus(pAGn, AG_STATUS_HOLD, 0);
      pAGn->SetAgentsvState(AG_SV_HOLD);
      SendCommonResult(AGMSG_onhold, OnSuccess, "hold");
      DispAgentStatus(nAGn);
    }
    else
    {
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
    }

    return 0;
  }
  
  sprintf(SendMsgBuf, "<onagblindtrancall sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' phonetype='%s' tranphone='%s' tranparam='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    AGMsg.GetAttrValue(2).C_Str(),
    AGMsg.GetAttrValue(3).C_Str(),
    AGMsg.GetAttrValue(4).C_Str());
  SendMsg2XML(MSG_onagblindtrancall, pChn->SessionNo, SendMsgBuf);
  if (pAGn->m_Seat.SeatLangID == 1)
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "正在轉接");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "?�I鑼鋇");
  else
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "transfering...");
  pAGn->IncTranCallCount();
  return 0;
}
//協商轉接電話
int Proc_AGMSG_consulttrancall(CXMLRcvMsg &AGMsg)
{
  int i, nChn, nChn1, phonetype, nAG, workerno;
  int nOut, calledtype;
  char CalledNo[MAX_TELECODE_LEN], TranParam[256];
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛???祆厚");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    memset(TranParam, 0, 256);
    if (AGMsg.GetAttrValue(4).GetLength() > 0)
    {
      sprintf(TranParam, "$S_DialParam=%s;", AGMsg.GetAttrValue(4).C_Str());
    }
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????﹚?杠?笵");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "the seat have not bangding channel");
      return 1;
    }
    nAG = 0xFFFF;
    phonetype = atoi(AGMsg.GetAttrValue(2).C_Str());
    if (phonetype == 3)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
      else
      {
        if (pAG->isIdelForSrv() == false)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
          return 1;
        }
        pChn->TranIVRId = 0;

        //2015-11-25 針對語音中繼的修改
        pAG->m_Seat.nCallInnChn = pAGn->m_Seat.nCallInnChn;
        pAGn->m_Seat.nTranAGnChn = pAG->GetSeatnChn();
        strcpy(pAG->m_Seat.CustPhone, pChn->CustPhone); 
        MyTrace(3, "Proc_AGMSG_consulttrancall1 nChn=%d CallerNo=%s CalledNo=%s CustPhone=%s Set nAG=%d CustPhone=%s",
          nChn, pChn->CallerNo, pChn->CalledNo, pChn->CustPhone, pAG->nAG, pAG->m_Seat.CustPhone);

        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, pAG->m_Seat.SeatNo.C_Str(), 1, TranParam, phonetype);
        pAGn->TranDesnAG = nAG;
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "正在轉接");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "?�I鑼鋇");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "transfering...");
        return 0;
      }
    }
    else if (phonetype == 2)
    {
      nAG = pAgentMng->GetnAGBySeatNo(AGMsg.GetAttrValue(3).C_Str());
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (pAG->isIdelForSrv() == false)
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的坐席正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y???Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
          return 1;
        }
        //2015-11-25 針對語音中繼的修改
        pAG->m_Seat.nCallInnChn = pAGn->m_Seat.nCallInnChn;
        pAGn->m_Seat.nTranAGnChn = pAG->GetSeatnChn();
        strcpy(pAG->m_Seat.CustPhone, pChn->CustPhone); 
        MyTrace(3, "Proc_AGMSG_consulttrancall2 nChn=%d CallerNo=%s CalledNo=%s CustPhone=%s Set nAG=%d CustPhone=%s",
          nChn, pChn->CallerNo, pChn->CalledNo, pChn->CustPhone, pAG->nAG, pAG->m_Seat.CustPhone);
      }
    }
    else if (phonetype == 4)
    {
      CACDRule ACDRule;
      ACDRule.TranCallFailCallBack = 0;
      ACDRule.AcdedCount = 0;
      ACDRule.SeatType = 0;
      ACDRule.SeatNo = "0";
      ACDRule.WorkerNo = 0;
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
      {
        ACDRule.GroupNo[i] = -1;
        ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = 0;
      }
      ACDRule.GroupNo[0] = (UC)atoi(AGMsg.GetAttrValue(3).C_Str());
      ACDRule.Level[0] = 0;
      ACDRule.NowLevel[0] = 0;
      ACDRule.AcdedGroupNo = 0;
      ACDRule.AcdRule = 2;
      ACDRule.LevelRule = 0;
      ACDRule.CallMode = 0;
      ACDRule.SeatGroupNo = 0;
      ACDRule.WaitTimeLen = 0;
      ACDRule.RingTimeLen = 0;
      ACDRule.BusyWaitId = 0;
      ACDRule.Priority = 0;
      
      ACDRule.QueueTime = time(0);
      ACDRule.StartRingTime = time(0);
      for (i = 0; i < MAX_AG_NUM; i ++)
      {
        ACDRule.AcdednAGList[i] = -1;
      }
      CAgent *pAgent3=GetIdelAgentForCallOut(ACDRule);
      if (pAgent3 != NULL)
      {
        nAG = pAgent3->nAG;
        strcpy(CalledNo, pAgent3->GetSeatNo().C_Str());
      }
      else
      {
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "all busy");
        return 1;
      }
      calledtype = 2;
    }

    pChn->TranIVRId = 0;
    Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, AGMsg.GetAttrValue(3).C_Str(), 1, TranParam, phonetype);
    pAGn->TranDesnAG = nAG;
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "正在轉接");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "?�I鑼鋇");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "transfering...");
    return 0;
  }
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "IVR仿真測試環境不允許轉接電話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "IVR?痷代剛吏掛???鑼鋇?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "can not transfer");
    return 1;
  }
  
  if (pAGn->CanTransferId == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未處于可轉接電話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????���紜r鋇?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????﹚?杠?笵");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "the seat have not bangding channel");
    return 1;
  }
  if (AGMsg.GetAttrValue(3).GetLength() == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "未輸入轉接號碼");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "?塊�芢r鋇腹絏");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }
  if (pAGn->svState != AG_SV_CONN && pAGn->svState != AG_SV_HOLD)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席未處于通話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????��?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
    return 1;
  }

  //--------板卡版本------------------------------------------------------
  if (pIVRCfg->ControlCallModal == 1)
  {
    if (pChn->TranStatus != 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席正處于轉接狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛????�牓r鋇�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }
    nChn1 = pChn->LinkChn[0];
    if (!pBoxchn->isnChnAvail(nChn1))
    {
      nChn1 = pChn->HoldingnChn;
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "該坐席無可轉接的電話");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "贛???�紜r鋇�Y?杠");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
    }
    if (pChn1->ssState != CHN_SNG_IN_TALK && pChn1->ssState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "被轉接的電話未處于通話狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "砆鑼鋇�Y?杠??��?杠�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }
    if (pChn1->TranStatus != 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "被轉接的電話已處于轉接狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "砆鑼鋇�Y?杠��?�牓r鋇�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }

    nAG = 0xFFFF;
    //取被轉接的號碼及號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
    calledtype = atoi(AGMsg.GetAttrValue(2).C_Str());
    memset(CalledNo, 0, MAX_TELECODE_LEN);
    if (calledtype == 2)
    {
      strncpy(CalledNo, AGMsg.GetAttrValue(3).C_Str(), MAX_TELECODE_LEN-1);
    }
    else if (calledtype == 3)
    {
      //轉接的是工號,則查詢登錄的坐席號
      nAG = pAgentMng->GetnAGByWorkerNo(atoi(AGMsg.GetAttrValue(3).C_Str()));
      if (pAgentMng->isnAGAvail(nAG))
      {
        calledtype = 2;
        strcpy(CalledNo, pAG->m_Seat.SeatNo.C_Str());
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
    }
    else if (calledtype == 4)
    {
      CACDRule ACDRule;
      ACDRule.TranCallFailCallBack = 0;
      ACDRule.AcdedCount = 0;
      ACDRule.SeatType = 0;
      ACDRule.SeatNo = "0";
      ACDRule.WorkerNo = 0;
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
      {
        ACDRule.GroupNo[i] = -1;
        ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = 0;
      }
      ACDRule.GroupNo[0] = (UC)atoi(AGMsg.GetAttrValue(3).C_Str());
      ACDRule.Level[0] = 0;
      ACDRule.NowLevel[0] = 0;
      ACDRule.AcdedGroupNo = 0;
      ACDRule.AcdRule = 2;
      ACDRule.LevelRule = 0;
      ACDRule.CallMode = 0;
      ACDRule.SeatGroupNo = 0;
      ACDRule.WaitTimeLen = 0;
      ACDRule.RingTimeLen = 0;
      ACDRule.BusyWaitId = 0;
      ACDRule.Priority = 0;
      
      ACDRule.QueueTime = time(0);
      ACDRule.StartRingTime = time(0);
      for (i = 0; i < MAX_AG_NUM; i ++)
      {
        ACDRule.AcdednAGList[i] = -1;
      }
      CAgent *pAgent3=GetIdelAgentForCallOut(ACDRule);
      if (pAgent3 != NULL)
      {
        nAG = pAgent3->nAG;
        strcpy(CalledNo, pAgent3->GetSeatNo().C_Str());
      }
      else
      {
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "all busy");
        return 1;
      }
      calledtype = 2;
    }
    else
    {
      calledtype = 1;
      strncpy(CalledNo, AGMsg.GetAttrValue(3).C_Str(), MAX_TELECODE_LEN-1);
    }
    if (calledtype == 2)
    {
      //如果轉接的是坐席,判斷是否空閑
      nAG = pAgentMng->GetnAGBySeatNo(CalledNo);
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (!pAG->isIdelForSrv())
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的坐席正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y???Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
          return 1;
        }
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接的坐席分機號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇�Y???訣腹?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
        return 1;
      }
    }

    nOut = pCallbuf->Get_Idle_nOut();
    if (!pCallbuf->isnOutAvail(nOut))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "轉接緩沖區溢出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "鑼鋇絯≧跋犯�|");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "");
      return 1;
    }

    pAGn->TranDesnAG = nAG;
    //設置呼出數據
    pOut->state = 1;
    pOut->timer = 0;
    pOut->CallStep = 0;
    
    if (calledtype == 1)
      pOut->RingTime = 45;
    else
      pOut->RingTime = 30;
    pOut->IntervalTime = 0;
    
    pOut->Session.SessionNo = 0;
    pOut->Session.CmdAddr = 0;
    pOut->Session.nChn = nChn;
    pOut->Session.DialSerialNo = GetCallOutSerialNo(nChn);
    pOut->Session.SerialNo = 1;
    pOut->Session.CmdId = 3; //坐席轉接
    pOut->Session.VxmlId = 1;
    pOut->Session.FuncNo = 0;
    pOut->Session.CallerNo = pChn->CustPhone;
    pOut->Session.CalledNo = CalledNo;
    pOut->Session.GenerateCdrSerialNo(pChn->lgChnType, pChn->lgChnNo);
    char szTemp[256];
    sprintf(szTemp, "0`%s`%s`%s", pOut->Session.CdrSerialNo, pOut->Session.RecordFileName, pIVRCfg->SeatRecPath.C_Str());
    pOut->Session.Param = szTemp;
    
    if (calledtype == 1)
    {
      pOut->CallerNo = pIVRCfg->CenterCode;
    } 
    else
    {
      if (pChn->CallInOut == 1)
        pOut->CallerNo = pChn->CallerNo;
      else
        pOut->CallerNo = pChn->CalledNo;
    }
    pOut->CallerType = 0;
    pOut->CallType = 0;
    pOut->CallSeatType = 1;
    pOut->CancelId = 0;
    
    pOut->CallMode = 0;
    pOut->CallTimes = 1;
    pOut->CalledTimes = 0;
    
    pOut->CalledNum = 1;
    strcpy(pOut->Calleds[0].CalledNo, CalledNo);
    pOut->Calleds[0].CalledType = calledtype;
    pOut->Calleds[0].pQue_str = NULL;
    pOut->Calleds[0].CalloutResult = 0;
    pOut->Calleds[0].OutnChn = 0xFFFF;
    pOut->RouteNo[0] = 1;
    
    pOut->StartPoint = 0;
    pOut->CallPoint = 0;
    pOut->CallTime = 0;
    
    pOut->OutnChn = 0xFFFF;

    pOut->TranMode = 2; //協商轉接
    pChn->nOut = nOut;
    pOut->TranCallFailReturn = 0;
    pOut->nAG = 0xFFFF;
    
    //斷開與正在的通話,并對其放等待音
    pChn->TranStatus = 3;
    pChn->TranControlnChn = nChn1;
    pChn->HoldingnChn = 0xFFFF;
    pChn1->TranStatus = 1;
    pChn1->TranControlnChn = nChn;
    pChn1->HoldingnChn = 0xFFFF;
    
    pOut->WaitVocFile = pIVRCfg->WaitVocFile;
    if (pChn->LinkType[0] != 0)
    {
      UnRouterTalk(nChn, nChn1);
      pChn->LinkType[0] = 0;
      pChn->LinkChn[0] = 0xFFFF;
      pChn1->LinkType[0] = 0;
      pChn1->LinkChn[0] = 0xFFFF;
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      PlayFile(nChn1, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      
      WriteSeatStatus(pAGn, AG_STATUS_HOLD, 0);
      pAGn->SetAgentsvState(AG_SV_HOLD);
      SendCommonResult(AGMSG_onhold, OnSuccess, "hold");
      DispAgentStatus(nAGn);
    }
    else
    {
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
    }

    return 0;
  }

  sprintf(SendMsgBuf, "<onagconsulttrancall sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' phonetype='%s' tranphone='%s' tranparam='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    AGMsg.GetAttrValue(2).C_Str(),
    AGMsg.GetAttrValue(3).C_Str(),
    AGMsg.GetAttrValue(4).C_Str());
  SendMsg2XML(MSG_onagconsulttrancall, pChn->SessionNo, SendMsgBuf);
  if (pAGn->m_Seat.SeatLangID == 1)
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "正在轉接");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "?�I鑼鋇");
  else
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutCalling, "transfering...");
  pAGn->IncTranCallCount();
  return 0;
}
//會議轉接電話
int Proc_AGMSG_conftrancall(CXMLRcvMsg &AGMsg)
{
  int nChn, nChn1, phonetype, nAG, workerno;
  int nOut, calledtype;
  char CalledNo[MAX_TELECODE_LEN];
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "贛???祆厚");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "the seat had not login");
    return 1;
  }
  
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "贛????﹚?杠?笵");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "the seat have not bangding channel");
      return 1;
    }
    phonetype = atoi(AGMsg.GetAttrValue(2).C_Str());
    if (phonetype == 0)
    {
      //表示直接將正在的通話1方及保持的1方加入三方會議通話
      Send_SWTMSG_threeconf(nChn, pChn->DeviceID, 0, 0);
      return 0;
    }
    if (phonetype == 3)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
        return 1;
      }
      else
      {
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, pAG->m_Seat.SeatNo.C_Str(), 3, "", phonetype);
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "正在轉接");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "?�I鑼鋇");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "transfering...");
        return 0;
      }
    }
    Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, AGMsg.GetAttrValue(3).C_Str(), 3, "", phonetype);
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "正在轉接");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "?�I鑼鋇");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "transfering...");
    return 0;
  }
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "IVR仿真測試環境不允許轉接電話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "IVR?痷代剛吏掛???鑼鋇?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(4).C_Str(), OnOutFail, "can not transfer");
    return 1;
  }
  
  if (pAGn->CanTransferId == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "該坐席未處于可轉接電話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "贛????���紜r鋇?杠�K?");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
    return 1;
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "贛????﹚?杠?笵");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "the seat have not bangding channel");
    return 1;
  }
  if (AGMsg.GetAttrValue(3).GetLength() == 0)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "未輸入轉接號碼");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "?塊�芢r鋇腹絏");
    else
      SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
    return 1;
  }

  //--------板卡版本------------------------------------------------------
  if (pIVRCfg->ControlCallModal == 1)
  {
    if (pChn->TranStatus != 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "該坐席正處于轉接狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "贛????�牓r鋇�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
      return 1;
    }
    nChn1 = pChn->LinkChn[0];
    if (!pBoxchn->isnChnAvail(nChn1))
    {
      nChn1 = pChn->HoldingnChn;
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "該坐席無可轉接的電話");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "贛???�紜r鋇�Y?杠");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
        return 1;
      }
    }
    if (pChn1->ssState != CHN_SNG_IN_TALK && pChn1->ssState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "被轉接的電話未處于通話狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "砆鑼鋇�Y?杠??��?杠�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
      return 1;
    }
    if (pChn1->TranStatus != 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "被轉接的電話已處于轉接狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "砆鑼鋇�Y?杠��?�牓r鋇�K?");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
      return 1;
    }

    //取被轉接的號碼及號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
    calledtype = atoi(AGMsg.GetAttrValue(2).C_Str());
    memset(CalledNo, 0, MAX_TELECODE_LEN);
    if (calledtype == 2)
    {
      strncpy(CalledNo, AGMsg.GetAttrValue(3).C_Str(), MAX_TELECODE_LEN-1);
    }
    else if (calledtype == 3)
    {
      //轉接的是工號,則查詢登錄的坐席號
      nAG = pAgentMng->GetnAGByWorkerNo(atoi(AGMsg.GetAttrValue(3).C_Str()));
      if (pAgentMng->isnAGAvail(nAG))
      {
        calledtype = 2;
        strcpy(CalledNo, pAG->m_Seat.SeatNo.C_Str());
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "轉接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "鑼鋇�Y�籪�?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
        return 1;
      }
    }
    else
    {
      calledtype = 1;
      strncpy(CalledNo, AGMsg.GetAttrValue(3).C_Str(), MAX_TELECODE_LEN-1);
    }
    if (calledtype == 2)
    {
      //如果轉接的是坐席,判斷是否空閑
      nAG = pAgentMng->GetnAGBySeatNo(CalledNo);
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (!pAG->isIdelForSrv())
        {
          if (pAGn->m_Seat.SeatLangID == 1)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "轉接的坐席正忙");
          else if (pAGn->m_Seat.SeatLangID == 2)
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "鑼鋇�Y???Γ");
          else
            SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
          return 1;
        }
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "轉接的坐席分機號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "鑼鋇�Y???訣腹?�Z�I");
        else
          SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
        return 1;
      }
    }

    nOut = pCallbuf->Get_Idle_nOut();
    if (!pCallbuf->isnOutAvail(nOut))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "轉接緩沖區溢出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "鑼鋇絯≧跋犯�|");
      else
        SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutFail, "");
      return 1;
    }
    //設置呼出數據
    pOut->state = 1;
    pOut->timer = 0;
    pOut->CallStep = 0;
    
    if (calledtype == 1)
      pOut->RingTime = 45;
    else
      pOut->RingTime = 30;
    pOut->IntervalTime = 0;
    
    pOut->Session.SessionNo = 0;
    pOut->Session.CmdAddr = 0;
    pOut->Session.nChn = nChn;
    pOut->Session.DialSerialNo = GetCallOutSerialNo(nChn);
    pOut->Session.SerialNo = 1;
    pOut->Session.CmdId = 3; //坐席轉接
    pOut->Session.VxmlId = 1;
    pOut->Session.FuncNo = 0;
    pOut->Session.CallerNo = pChn->CustPhone;
    pOut->Session.CalledNo = CalledNo;
    pOut->Session.GenerateCdrSerialNo(pChn->lgChnType, pChn->lgChnNo);
    char szTemp[256];
    sprintf(szTemp, "0`%s`%s`%s", pOut->Session.CdrSerialNo, pOut->Session.RecordFileName, pIVRCfg->SeatRecPath.C_Str());
    pOut->Session.Param = szTemp;
    
    if (calledtype == 1)
    {
      pOut->CallerNo = pIVRCfg->CenterCode;
    } 
    else
    {
      if (pChn->CallInOut == 1)
        pOut->CallerNo = pChn->CallerNo;
      else
        pOut->CallerNo = pChn->CalledNo;
    }
    pOut->CallerType = 0;
    pOut->CallType = 0;
    pOut->CallSeatType = 1;
    pOut->CancelId = 0;
    
    pOut->CallMode = 0;
    pOut->CallTimes = 1;
    pOut->CalledTimes = 0;
    
    pOut->CalledNum = 1;
    strcpy(pOut->Calleds[0].CalledNo, CalledNo);
    pOut->Calleds[0].CalledType = calledtype;
    pOut->Calleds[0].pQue_str = NULL;
    pOut->Calleds[0].CalloutResult = 0;
    pOut->Calleds[0].OutnChn = 0xFFFF;
    pOut->RouteNo[0] = 1;
    
    pOut->StartPoint = 0;
    pOut->CallPoint = 0;
    pOut->CallTime = 0;
    
    pOut->OutnChn = 0xFFFF;

    pOut->TranMode = 3; //會議轉接
    pChn->nOut = nOut;
    pOut->TranCallFailReturn = 0;
    pOut->nAG = 0xFFFF;
    
    //斷開與正在的通話,并對其放等待音
    pChn->TranStatus = 3;
    pChn->TranControlnChn = nChn1;
    pChn1->TranStatus = 1;
    pChn1->TranControlnChn = nChn;
    
    pOut->WaitVocFile = pIVRCfg->WaitVocFile;
    if (pChn->LinkType[0] != 0)
    {
      UnRouterTalk(nChn, nChn1);
      pChn->LinkType[0] = 0;
      pChn->LinkChn[0] = 0xFFFF;
      pChn->HoldingnChn = nChn1;
      pChn1->LinkType[0] = 0;
      pChn1->LinkChn[0] = 0xFFFF;
      pChn1->HoldingnChn = nChn;
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      PlayFile(nChn1, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      
      WriteSeatStatus(pAGn, AG_STATUS_HOLD, 0);
      pAGn->SetAgentsvState(AG_SV_HOLD);
      SendCommonResult(AGMSG_onhold, OnSuccess, "hold");
      DispAgentStatus(nAGn);
    }
    else
    {
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
    }

    return 0;
  }

  sprintf(SendMsgBuf, "<onagconftrancall sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' phonetype='%s' tranphone='%s' confno='%s' tranparam='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    AGMsg.GetAttrValue(2).C_Str(),
    AGMsg.GetAttrValue(3).C_Str(),
    AGMsg.GetAttrValue(4).C_Str(),
    AGMsg.GetAttrValue(5).C_Str());
  SendMsg2XML(MSG_onagconftrancall, pChn->SessionNo, SendMsgBuf);
  if (pAGn->m_Seat.SeatLangID == 1)
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "正在轉接");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "?�I鑼鋇");
  else
    SendSeatTranCallFailResult(nAGn, AGMsg.GetAttrValue(3).C_Str(), AGMsg.GetAttrValue(5).C_Str(), OnOutCalling, "transfering...");
  return 0;
}
//停止轉接
int Proc_AGMSG_stoptrancall(CXMLRcvMsg &AGMsg)
{
  int nChn, nChn1, nChn2, nOut, nResult;

  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onstoptrancall, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onstoptrancall, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onstoptrancall, OnFail, "the seat had not login");
  }
  if (g_nSwitchType > 0)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onstoptrancall, OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onstoptrancall, OnFail, "贛????﹚?杠?笵");
      else
        SendCommonResult(AGMSG_onstoptrancall, OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
    {
      StopTransfer(nChn, 0);
      Send_StopTransfer_To_REC(nChn, nAGn);
    }
    else
      Send_SWTMSG_stoptransfer(nChn, pChn->DeviceID, pChn->ConnID);
    return 0;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onstoptrancall, OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onstoptrancall, OnFail, "贛????﹚?杠?笵");
    else
      SendCommonResult(AGMSG_onstoptrancall, OnFail, "the seat have not bangding channel");
    return 1;
  }
  //--------板卡版本------------------------------------------------------
  if (pIVRCfg->ControlCallModal == 1)
  {
    nOut = pChn->nOut;
    if (pCallbuf->isnOutAvail(nOut))
    {
      pOut->CancelId = 2;
      nChn1 = pChn->TranControlnChn;
      if (pBoxchn->isnChnAvail(nChn1))
      {
        //是否已與轉接的目的通道交換了
        nChn2 = pChn->LinkChn[0];
        if ((pChn->LinkType[0] == 1 || pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7) && pBoxchn->isnChnAvail(nChn2))
        {
          UnRouterTalk(nChn, nChn2);
          pChn->LinkType[0] = 0;
          pChn->LinkChn[0] = 0xFFFF;
          pChn2->LinkType[0] = 0;
          pChn2->LinkChn[0] = 0xFFFF;
        }
        else
        {
          StopClearPlayDtmfBuf(nChn);
        }
        
        StopClearPlayDtmfBuf(nChn1);
        nResult = RouterTalk(nChn, nChn1);
        if (nResult == 0)
        {
          pChn->RecMixerFlag = 1;
          pChn->LinkType[0] = 1;
          pChn->LinkChn[0] = nChn1;
          pChn->HoldingnChn = 0xFFFF;
          
          pChn1->RecMixerFlag = 1;
          pChn1->LinkType[0] = 1;
          pChn1->LinkChn[0] = nChn;
          pChn1->HoldingnChn = 0xFFFF;
          
          WriteSeatStatus(pAGn, AG_STATUS_TALK, 0);
          pAGn->SetAgentsvState(AG_SV_CONN);
          DispAgentStatus(nAGn);
          SendCommonResult(AGMSG_onhold, OnSuccess, "");
        }
      }
      SendCommonResult(AGMSG_onstoptrancall, OnSuccess, "");
    }
    return 0;
  }

  //完全由流程控制模式
  sprintf(SendMsgBuf, "<onagstoptrancall sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo);
  SendMsg2XML(MSG_onagstoptrancall, pChn->SessionNo, SendMsgBuf);
  SendCommonResult(AGMSG_onstoptrancall, OnSuccess, "");
  return 0;
}
//指定通道會議發言
int Proc_AGMSG_confspeak(CXMLRcvMsg &AGMsg)
{
  int nChn;
  
  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  sprintf(SendMsgBuf, "<onagconfspeak sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' destchntype='%s' destchnno='%s' confno='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    AGMsg.GetAttrValue(2).C_Str(),
    AGMsg.GetAttrValue(3).C_Str(),
    AGMsg.GetAttrValue(4).C_Str());
  SendMsg2XML(MSG_onagconfspeak, pChn->SessionNo, SendMsgBuf);

  return 0;
}
//指定通道旁聽會議
int Proc_AGMSG_confaudit(CXMLRcvMsg &AGMsg)
{
  int nChn;
  
  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatJoinConfFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  sprintf(SendMsgBuf, "<onagconfaudit sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' destchntype='%s' destchnno='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    AGMsg.GetAttrValue(2).C_Str(),
    AGMsg.GetAttrValue(3).C_Str());
  SendMsg2XML(MSG_onagconfaudit, pChn->SessionNo, SendMsgBuf);
  
  return 0;
}
//轉接ivr流程
int Proc_AGMSG_tranivr(CXMLRcvMsg &AGMsg)
{
  int nChn, nChn1, tranivrid, returnid;
  char szTranData[256];
  
  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "贛???祆厚");
    else
      SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "the seat had not login");
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  if (pAGn->isThesvState(AG_SV_IDEL) == true)
  {
    if (g_nCardType == 9)
    {
      Release_Chn(nChn, 0, 0);
    }
    return 0;
  }
  if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
    return 0;
  else if (g_nSwitchType >0)
  {
    nChn1 = GetIVRChn(0, "");
    MyTrace(3, "Proc_AGMSG_tranivr GetIVRChn=%d svState=%d", nChn1, pAGn->svState);
    if (pAGn->svState == AG_SV_CONN && pBoxchn->isnChnAvail(nChn1))
    {
      tranivrid = atoi(AGMsg.GetAttrValue(2).C_Str()); //轉接的ivr標志,流程通過該標志約定來判斷跳轉到哪里
      returnid = atoi(AGMsg.GetAttrValue(3).C_Str()); //是否返回原服務坐席標志,如：要求客戶輸入密碼認證后,返回坐席繼續服務等業務
      
      if (tranivrid == 1)
      {
        //評分
        pChn->TranIVRId = 1;
        pChn1->TranIVRId = 2;
        pChn->nAcd = 0xFFFF;
        pChn1->nAcd = 0xFFFF;
        if (AGMsg.GetAttrValue(4).GetLength() == 0)
        {
          Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, pChn->CdrSerialNo, 0);
        }
        else
        {
          sprintf(szTranData, "%s`%s", AGMsg.GetAttrValue(4).C_Str(), pChn->CdrSerialNo);
          Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, szTranData, 0);
        }
      }
      else if (tranivrid == 2 || tranivrid == 3)
      {
        //接收驗證碼 $S_CheckCode、密碼驗證 $S_Password
        pChn->TranIVRId = 1;
        pChn1->TranIVRId = 7;
        pChn->nAcd = 0xFFFF;
        pChn1->nAcd = 0xFFFF;
        strcpy(pChn1->CdrSerialNo, pChn->CdrSerialNo);
        strcpy(pChn1->RecordFileName, pChn->RecordFileName);
        pAGn->m_Worker.DisturbId = 2; //設置等待IVR返回
        pAGn->SendTrnIVRHangonFlag = 1; //2016-07-19發送轉IVR掛機標志
        pAGn->timer = 0;
        SendOneAGStateToAll(nAGn);

        sprintf(szTranData, "%s`%s`%s`%s", AGMsg.GetAttrValue(4).C_Str(), pAGn->GetWorkerNoStr(), pChn->CdrSerialNo, pChn->RecordFileName);
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, szTranData, 0);
      }
      else if (tranivrid == 4 || tranivrid == 5)
      {
        //轉接收傳真、轉索取傳真
        pChn->TranIVRId = 1;
        pChn1->TranIVRId = 2;
        pChn->nAcd = 0xFFFF;
        pChn1->nAcd = 0xFFFF;
        
        sprintf(szTranData, "%s`%s`%s`%s", AGMsg.GetAttrValue(4).C_Str(), pAGn->GetWorkerNoStr(), pChn->CdrSerialNo, pChn->RecordFileName);
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, szTranData, 0);
      }
      else
      {
        //轉接ivr
        sprintf(SendMsgBuf, "<onagtranivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' tranivrid='%s' returnid='%s' tranparam='%s'/>", 
          pChn1->SessionNo, pChn1->CmdAddr, pChn1->lgChnType, pChn1->lgChnNo,
          AGMsg.GetAttrValue(2).C_Str(),
          AGMsg.GetAttrValue(3).C_Str(),
          AGMsg.GetAttrValue(4).C_Str());
        SendMsg2XML(MSG_onagtranivr, pChn1->SessionNo, SendMsgBuf);
      }
    }
    else
    {
      Release_Chn(nChn, 1, 1);
    }
    return 0;
  }

  if (pIVRCfg->ControlCallModal == 1)
  {
    nChn1 = pChn->LinkChn[0];
    if (pChn->TranStatus != 0 || !pBoxchn->isnChnAvail(nChn1))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "該坐席未處于通話狀態");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "贛????��?杠�K?");
      else
        SendSeatTranIVRFailResult(nAGn, 0, "", OnFail, "the seat is not in talking");
      return 1;
    }

    tranivrid = atoi(AGMsg.GetAttrValue(2).C_Str()); //轉接的ivr標志,流程通過該標志約定來判斷跳轉到哪里

    if (tranivrid == 1)
    {
      //評分
      Release_Chn(nChn, 1, 1);
    }
    else if (tranivrid == 2 || tranivrid == 3)
    {
      pAGn->m_Worker.DisturbId = 2; //設置等待IVR返回
      pAGn->SendTrnIVRHangonFlag = 1; //2016-07-19發送轉IVR掛機標志
      pAGn->timer = 0;
      SendOneAGStateToAll(nAGn);
      //接收驗證碼 $S_CheckCode、密碼驗證 $S_Password
      sprintf(SendMsgBuf, "<onagtranivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' tranivrid='%s' returnid='%s' tranparam='%s'/>", 
        pChn1->SessionNo, pChn1->CmdAddr, pChn1->lgChnType, pChn1->lgChnNo,
        AGMsg.GetAttrValue(2).C_Str(),
        AGMsg.GetAttrValue(3).C_Str(),
        AGMsg.GetAttrValue(4).C_Str());
      SendMsg2XML(MSG_onagtranivr, pChn1->SessionNo, SendMsgBuf);
    }
    else if (tranivrid == 4 || tranivrid == 5)
    {
      //轉接收傳真、轉索取傳真
      sprintf(SendMsgBuf, "<onagtranivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' tranivrid='%s' returnid='%s' tranparam='%s'/>", 
        pChn1->SessionNo, pChn1->CmdAddr, pChn1->lgChnType, pChn1->lgChnNo,
        AGMsg.GetAttrValue(2).C_Str(),
        AGMsg.GetAttrValue(3).C_Str(),
        AGMsg.GetAttrValue(4).C_Str());
      SendMsg2XML(MSG_onagtranivr, pChn1->SessionNo, SendMsgBuf);
    }
    else
    {
      //轉接ivr
      sprintf(SendMsgBuf, "<onagtranivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' tranivrid='%s' returnid='%s' tranparam='%s'/>", 
        pChn1->SessionNo, pChn1->CmdAddr, pChn1->lgChnType, pChn1->lgChnNo,
        AGMsg.GetAttrValue(2).C_Str(),
        AGMsg.GetAttrValue(3).C_Str(),
        AGMsg.GetAttrValue(4).C_Str());
      SendMsg2XML(MSG_onagtranivr, pChn1->SessionNo, SendMsgBuf);
    }
    return 0;
  }
  sprintf(SendMsgBuf, "<onagtranivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' tranivrid='%s' returnid='%s' tranparam='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    AGMsg.GetAttrValue(2).C_Str(),
    AGMsg.GetAttrValue(3).C_Str(),
    AGMsg.GetAttrValue(4).C_Str());
  SendMsg2XML(MSG_onagtranivr, pChn->SessionNo, SendMsgBuf);

  return 0;
}
//發送DTMF按鍵
int Proc_AGMSG_senddtmf(CXMLRcvMsg &AGMsg)
{
  int nChn;

  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onsenddtmf, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onsenddtmf, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onsenddtmf, OnFail, "the seat had not login");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onsenddtmf, OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onsenddtmf, OnFail, "贛????﹚?杠?笵");
      else
        SendCommonResult(AGMSG_onsenddtmf, OnFail, "the seat have not bangding channel");
      return 1;
    }
    Send_SWTMSG_senddtmf(nChn, pChn->DeviceID, pChn->ConnID, AGMsg.GetAttrValue(2).C_Str());
    return 0;
  }
  return 0;
}
//播放語音文件
int Proc_AGMSG_play(CXMLRcvMsg &AGMsg)
{
  CStringX filepath;
  int nChn, StartPos, chState;
  long size;

  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onplay, OnPlayError, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onplay, OnPlayError, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onplay, OnPlayError, "the seat had not login");
    return 1;
  }
  if (g_nCardType == 9)
  {
    return 0;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onplay, OnPlayError, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onplay, OnPlayError, "贛????﹚?杠?笵");
    else
      SendCommonResult(AGMSG_onplay, OnPlayError, "the seat have not bangding channel");
    return 1;
  }
  if (pAGn->svState != AG_SV_PLAY)
  {
    if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onplay, OnPlayError, "該狀態不能放音");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onplay, OnPlayError, "贛�K??�~�I��");
      else
        SendCommonResult(AGMSG_onplay, OnPlayError, "");
      return 1;
    }
    else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL) && pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onplay, OnPlayError, "該狀態不能放音");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onplay, OnPlayError, "贛�K??�~�I��");
      else
        SendCommonResult(AGMSG_onplay, OnPlayError, "");
      return 1;
    }
    chState = GetAgentChState(pAGn);
    if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
    {
      if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendCommonResult(AGMSG_onplay, OnPlayError, "該坐席電話還未摘機,請先摘機然后放音");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendCommonResult(AGMSG_onplay, OnPlayError, "贛???杠臨?篕訣,叫�g篕訣礛�A�I��");
        else
          SendCommonResult(AGMSG_onplay, OnPlayError, "");
        return 1;
      }
    }
    else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
    {
      if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendCommonResult(AGMSG_onplay, OnPlayError, "該坐席未綁定電話通道");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendCommonResult(AGMSG_onplay, OnPlayError, "贛????﹚?杠?笵");
        else
          SendCommonResult(AGMSG_onplay, OnPlayError, "");
        return 1;
      }
    }
    else
    {
      if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendCommonResult(AGMSG_onplay, OnPlayError, "該坐席電話還未摘機,請先摘機然后放音");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendCommonResult(AGMSG_onplay, OnPlayError, "贛???杠臨?篕訣,叫�g篕訣礛�A�I��");
        else
          SendCommonResult(AGMSG_onplay, OnPlayError, "");
        return 1;
      }
      SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
      pChn->hwState = CHN_HW_VALID;
      pChn->lnState = CHN_LN_SEIZE; //占用
      pChn->CallInOut = CALL_IN;
      pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
      pChn->CanPlayOrRecId = 1;
      pChn->CallTime = time(0);
      pChn->AnsTime = time(0);
      DispChnStatus(nChn);
    }
  }

  StartPos = atoi(AGMsg.GetAttrValue(3).C_Str());
  StopClearPlayDtmfBuf(nChn);
  
  filepath = AddRootPath(pIVRCfg->SeatRecPath, AGMsg.GetAttrValue(2));
  size = MyCheckFileSize(filepath.C_Str());
  size = size * StartPos / 100;
  if (AddSingleFileToPlayBuf(nChn, filepath.C_Str(), size, 0, ErrMsg) != 0)
  {
    SendCommonResult(AGMSG_onplay, OnPlayError, ErrMsg);
    
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  if (SetDTMFPlayRule(nChn, 0, 0, ErrMsg) != 0)
  {
    SendCommonResult(AGMSG_onplay, OnPlayError, ErrMsg);
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  pChn->curPlayData.PlayState = 1;
  pChn->PlayedTimer = 0;
 	if (Start_ChnPlay(nChn) != 0)
  {
    StopClearPlayDtmfBuf(nChn);
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onplay, OnPlayError, "啟動放音失敗");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onplay, OnPlayError, "幣笆�I��?毖");
    else
      SendCommonResult(AGMSG_onplay, OnPlayError, "");
    return 1;
  }
  pChn->curPlayData.CanPlayId = 1;
  pAGn->SetAgentsvState(AG_SV_PLAY); //2016-02-12
  DispAgentStatus(nAGn);
  if (pAGn->m_Seat.SeatLangID == 1)
    SendCommonResult(AGMSG_onplay, OnPlaying, "正在放音");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendCommonResult(AGMSG_onplay, OnPlaying, "?�I�I��");
  else
    SendCommonResult(AGMSG_onplay, OnPlaying, "");

  return 0;
}
//停止放音
int Proc_AGMSG_stopplay(CXMLRcvMsg &AGMsg)
{
  int nChn;

  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onstopplay, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onstopplay, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onstopplay, OnFail, "the seat had not login");
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onstopplay, OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onstopplay, OnFail, "贛????﹚?杠?笵");
    else
      SendCommonResult(AGMSG_onstopplay, OnFail, "the seat have not bangding channel");
    return 1;
  }
  StopClearPlayDtmfBuf(nChn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    pAGn->SetAgentsvState(AG_SV_IDEL);
    DispAgentStatus(nAGn);
  }
  SendCommonResult(AGMSG_onstopplay, OnSuccess, "");
  return 0;
}
//代接指定的電話
int Proc_AGMSG_pickup(CXMLRcvMsg &AGMsg)
{
  int nChn, nAG1, nChn1, nQue, nOut, nAcd, chState, workerno, nAG;
  CAgent *pAgent1=NULL;
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (AGMsg.GetAttrValue(2).GetLength() == 0
      && AGMsg.GetAttrValue(3).Compare("0") == 0
      && AGMsg.GetAttrValue(4).Compare("0") == 0)
    {
      Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, "GRP");
      return 0;
    }
    if (AGMsg.GetAttrValue(2).GetLength() > 0 && AGMsg.GetAttrValue(2).Compare("0") != 0)
    {
      //2015-11-25 針對語音中繼的修改,傳遞參數
      nAG = pAgentMng->GetnAGBySeatNo(AGMsg.GetAttrValue(2).C_Str());
      if (pAgentMng->isnAGAvail(nAG))
      {
        pAGn->m_Seat.ACDByVocTrkId = pAG->m_Seat.ACDByVocTrkId;
        pAGn->m_Seat.nVocTrknChn = pAG->m_Seat.nVocTrknChn;
        strcpy(pAGn->m_Seat.CustPhone, pAG->m_Seat.CustPhone);
        nQue = pAG->nQue;
        if (pCalloutQue->isnQueAvail(nQue))
        {
          if (pQue->OrgCallType == 2)
          {
            nAcd = pQue->nAcd;
            pQue->nAG = nAGn;
            pAcd->Calleds[pQue->CallPoint].nAG = nAGn;
          }
        }
      }

      Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, AGMsg.GetAttrValue(2).C_Str());
      return 0;
    }
    if (AGMsg.GetAttrValue(3).GetLength() > 0 && AGMsg.GetAttrValue(3).Compare("0") != 0)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatPickupFailResult(nAGn, "", OnFail, "代接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇�Y�籪�?�Z�I");
        else
          SendSeatPickupFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatPickupFailResult(nAGn, "", OnFail, "代接的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇�Y�籪�?�Z�I");
        else
          SendSeatPickupFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      else
      {
        //2015-11-25 針對語音中繼的修改,傳遞參數
        pAGn->m_Seat.ACDByVocTrkId = pAG->m_Seat.ACDByVocTrkId;
        pAGn->m_Seat.nVocTrknChn = pAG->m_Seat.nVocTrknChn;
        strcpy(pAGn->m_Seat.CustPhone, pAG->m_Seat.CustPhone);
        nQue = pAG->nQue;
        if (pCalloutQue->isnQueAvail(nQue))
        {
          if (pQue->OrgCallType == 2)
          {
            nAcd = pQue->nAcd;
            pQue->nAG = nAGn;
            pAcd->Calleds[pQue->CallPoint].nAG = nAGn;
          }
        }

        Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG->m_Seat.SeatNo.C_Str());
        return 0;
      }
      return 0;
    }
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "代接號碼不正確");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇腹絏???");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "仿真測試環境不支持代接功能");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "?痷代剛吏掛??�Y�r鋇�ｕ~");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  //板卡版本
  if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席正忙");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "贛???Γ");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL))
  {
    if (pAGn->svState == AG_SV_IDEL)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "本坐席電話還未摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "????杠臨?篕訣");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    else if (pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "????杠?Γ");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后代接");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A�r鋇");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席綁定的電話通道未通");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛???﹚�Y?杠?笵??");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else
  {
    if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后代接");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A�r鋇");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_IN;
    pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
    pChn->CanPlayOrRecId = 1;
    pChn->CallTime = time(0);
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
  nAG1 = GetnAGForPickup(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()), atoi(AGMsg.GetAttrValue(3).C_Str()), 0);
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "沒有可代接的坐席");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "⊿Τ���r鋇�Y??");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  nQue = pAG1->nQue;
  if (!pCalloutQue->isnQueAvail(nQue))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "代接的坐席沒有電話呼入");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇�Y??⊿Τ?杠㊣��");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  //取消以前的呼叫
  nOut = pQue->nOut;
  nAcd = pQue->nAcd;
  nChn1 = pQue->OutnChn;
  CancelCallout(nChn1);
  pAG1->nQue = 0xFFFF;
  if (pAG1->isWantSeatType(SEATTYPE_RT_TEL))
  {
    pChn1->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
    pAG1->m_Seat.nChn = 0xFFFF;
  }
  SendStopACDCallIn(nAG1);
  WriteSeatStatus(pAG1, AG_STATUS_IDEL, 2); //代接標志
  SetAgentsvState(nAG1, AG_SV_IDEL);
  pQue->state = 0;
  //發送新的呼叫結果給被代接通道
  if (pQue->OrgCallType == 1)
  {
    //取消被代接通道的呼出
    for (int j=0; j<pOut->CalledNum; j++)
    {
      if (pQue->CallPoint==j) continue;
      if (pOut->Calleds[j].pQue_str != NULL)
        pOut->Calleds[j].pQue_str->CancelId = 1;
      if (pOut->Calleds[j].CalloutResult == CALLOUT_CONN)
      {
        pOut->Calleds[j].CalloutResult = 0;
        Release_Chn(pOut->Calleds[j].OutnChn, 1, 0);
      }
    }
    pOut->state = 0;
    
    SendCalloutResult(nOut, pQue->CallPoint, OnOutAnswer, "", nChn);
    pAgentMng->lastAnsnAG[0] = nAGn;
    pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][0] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][pAGn->GetLevel()] = nAGn;
    pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
    pAGn->m_Worker.AcdedGroupNo = 0;
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);

    SetAgentsvState(nAGn, AG_SV_CONN);
    SendSeatPickupResult(nAGn, nOut, pQue->CallPoint, OnSuccess, "");
  }
  else
  {
    //取消被代接通道的呼叫坐席
    for (int j=0; j<pAcd->CalledNum; j++)
    {
      if (pQue->CallPoint==j) continue;
      if (pAcd->Calleds[j].pQue_str != NULL)
        pAcd->Calleds[j].pQue_str->CancelId = 1;
      pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
      if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
      {
        pAcd->Calleds[j].CalloutResult = 0;
        Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
      }
      SetAgentsvState(pAgent1, AG_SV_IDEL);
    }
    
    pAGn->CanTransferId = 1;
    pAGn->SetSrvLine(pAcd->Session.nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
    SendCallseatResult(nAcd, pAGn, 0, OnACDAnswer, "");
    pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
    pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
    pAGn->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);
    
    if (pIVRCfg->ControlCallModal == 0)
    {
      pAGn->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
    }
    else
    {
      nChn = pAcd->Session.nChn;
      if (pChn->lgChnType == 2)
      {
        DBUpdateCallCDR_CallInOut(nChn, 3);
      }
      //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
      strcpy(pAGn->CdrSerialNo, pChn->CdrSerialNo);
      strcpy(pAGn->RecordFileName, pChn->RecordFileName);
      DBUpdateCallCDR_SeatAnsTime(pAGn->nAG, pAcd->ACDRule.AcdedGroupNo);
      if (pAGn->m_Worker.AutoRecordId == 1 && pAGn->m_Seat.AutoRecordId == 1)
      {
        char szTemp[256], pszError[128];
        sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAGn->RecordFileName);
        CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
        
        if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
        {
          if (SetChnRecordRule(pAGn->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
          {
            DBUpdateCallCDR_RecdFile(pAGn->nAG);
          }
        }
      }
    }
    DelFromQueue(nAcd);
    
    SetAgentsvState(nAGn, AG_SV_CONN);
    SendSeatPickupResult(nAGn, nAcd, OnSuccess, "");
  }

  return 0;
}
//搶接ACD隊列里的呼叫
int Proc_AGMSG_pickupqueuecall(CXMLRcvMsg &AGMsg)
{
  int nChn, nAcd, nAG1, nChn1, chState;
  CAgent *pAgent1=NULL;
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
      return 1;
    }
    nAcd = atoi(AGMsg.GetAttrValue(2).C_Str());
    if (nAcd == 0)
    {
      nAcd = pAcd->Next; //沒有定制的排隊序號就取最前1個
    }
    if (!pACDQueue->isnAcdAvail(nAcd))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "指定的ACD隊列號無呼叫");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "�\﹚�YACD釘�曏�?㊣��");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    MyTrace(3, "Proc_AGMSG_pickupqueuecall nAGn=%d InnChn=%d nAcd=%d AllocnAG=%d acdState=%d", 
      nAGn, pAcd->Session.nChn, nAcd, pAcd->Calleds[0].nAG, pAcd->acdState);
    //判斷是否已經分配了坐席或還是在等待分配隊列里
    if (pAcd->acdState == 2)
    {
      //已經振鈴,等待應答結果
      nAG1 = pAcd->Calleds[0].nAG;
      if (pAgentMng->isnAGAvail(nAG1))
      {
        Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG1->m_Seat.SeatNo.C_Str());
        return 0;
      }
    }
    else if (pAcd->acdState == 0 || pAcd->acdState == 3)
    {
      //等待分配狀態
      SetAcdedSeatNo(nAcd, pAGn);
      nChn1 = pAcd->Session.nChn;
      if (SetACDCalloutQue(nAcd, 0, true) == 0)
      {
        pAcd->timer = 0;
        pAcd->ringtimer = 0;
        pAcd->acdState = 1; //1-等待呼叫結果
        if (pAcd->acdState == 0)
          ChCount.AcdCallSuccCount ++;
        pAcd->ACDRule.StartRingTime = time(0);
        SendACDQueueInfoToAll();
        if (pAcd->ACDRule.AcdRule != 7)
        {
          DBUpdateCallCDR_WaitTime(nChn1);
          DBUpdateCallCDR_ACDTime(nChn1, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
        }
        return 0;
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatPickupACDFailResult(nAGn, "", OnFail, "本坐席電話正忙，代接隊列電話失敗");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatPickupACDFailResult(nAGn, "", OnFail, "????杠?Γ���r鋇釘��?杠?毖");
        else
          SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
        return 1;
      }
    }
    
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "該來電剛分配到了坐席，但還未振鈴，代接隊列電話失敗");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛ㄓ?�g??����??���切{?�絡c���r鋇釘��?杠?毖");
    else
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
    return 1;
  }

  //板卡版本
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "仿真測試環境不支持代接功能");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "?痷代剛吏掛??�Y�r鋇�ｕ~");
    else
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席正忙");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛???Γ");
    else
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL))
  {
    if (pAGn->svState == AG_SV_IDEL)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "本坐席電話還未摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "????杠臨?篕訣");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    else if (pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "????杠?Γ");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席綁定的電話通道未通");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛???﹚�Y?杠?笵??");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else
  {
    if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_IN;
    pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
    pChn->CanPlayOrRecId = 1;
    pChn->CallTime = time(0);
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
  nAcd = atoi(AGMsg.GetAttrValue(2).C_Str());
  if (nAcd == 0)
  {
    nAcd = pAcd->Next;
  }
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "指定的ACD隊列號無呼叫");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "�\﹚�YACD釘�曏�?㊣��");
    else
      SendSeatPickupACDFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  //取消被代接通道的呼叫坐席
  for (int j=0; j<pAcd->CalledNum; j++)
  {
    if (pAcd->Calleds[j].pQue_str != NULL)
      pAcd->Calleds[j].pQue_str->CancelId = 1;
    pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
    if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
    {
      pAcd->Calleds[j].CalloutResult = 0;
      Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
    }
    SetAgentsvState(pAgent1, AG_SV_IDEL);
    if (pAgent1)
    {
      WriteSeatStatus(pAgent1, AG_STATUS_IDEL, 2);
    }
  }
  
  pAGn->CanTransferId = 1;
  pAGn->SetSrvLine(pAcd->Session.nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
  SendCallseatResult(nAcd, pAGn, 0, OnACDAnswer, "");
  pAgentMng->lastAnsnAG[0] = nAGn;
  pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
  pAgentMng->lastAnsLevelnAG[0][0] = nAGn;
  pAgentMng->lastAnsLevelnAG[0][pAGn->GetLevel()] = nAGn;
  pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
  pAGn->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
  WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);
  if (pIVRCfg->ControlCallModal == 0)
  {
    pAGn->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
  }
  else
  {
    nChn = pAcd->Session.nChn;
    if (pChn->lgChnType == 2)
    {
      DBUpdateCallCDR_CallInOut(nChn, 3);
    }
    //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
    strcpy(pAGn->CdrSerialNo, pChn->CdrSerialNo);
    strcpy(pAGn->RecordFileName, pChn->RecordFileName);
    DBUpdateCallCDR_SeatAnsTime(pAGn->nAG, pAcd->ACDRule.AcdedGroupNo);
    if (pAGn->m_Worker.AutoRecordId == 1 && pAGn->m_Seat.AutoRecordId == 1)
    {
      char szTemp[256], pszError[128];
      sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAGn->RecordFileName);
      CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
      
      if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
      {
        if (SetChnRecordRule(pAGn->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
        {
          DBUpdateCallCDR_RecdFile(pAGn->nAG);
        }
      }
    }
  }
  DelFromQueue(nAcd);
  
  SetAgentsvState(nAGn, AG_SV_CONN);
  SendSeatPickupACDResult(nAGn, nAcd, OnSuccess, "");
  return 0;
}
//接管其他坐席的電話
int Proc_AGMSG_takeover(CXMLRcvMsg &AGMsg)
{
  int nChn, nAG1, nChn1, nChn2, chState, workerno;
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  if (g_nCardType == 9)
    return 0;
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
      else
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (AGMsg.GetAttrValue(2).GetLength() > 0 && AGMsg.GetAttrValue(2).Compare("0") != 0)
    {
      Send_SWTMSG_takeovercall(nChn, pChn->DeviceID, pChn->ConnID, 0, AGMsg.GetAttrValue(2).C_Str());
      return 0;
    }
    if (AGMsg.GetAttrValue(3).GetLength() > 0 && AGMsg.GetAttrValue(3).Compare("0") != 0)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTakeOverFailResult(nAGn, "", OnFail, "接管的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTakeOverFailResult(nAGn, "", OnFail, "鋇恨�Y�籪�?�Z�I");
        else
          SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      nAG1 = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG1))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatTakeOverFailResult(nAGn, "", OnFail, "接管的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatTakeOverFailResult(nAGn, "", OnFail, "鋇恨�Y�籪�?�Z�I");
        else
          SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      else
      {
        Send_SWTMSG_takeovercall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG1->m_Seat.SeatNo.C_Str());
        return 0;
      }
      return 0;
    }
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "接管號碼不正確");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "鋇恨腹絏???");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  
  if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席正忙");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛???Γ");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL))
  {
    if (pAGn->svState == AG_SV_IDEL)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "本坐席電話還未摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "????杠臨?篕訣");
      else
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    else if (pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "????杠?Γ");
      else
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席綁定的電話通道未通");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛???﹚�Y?杠?笵??");
      else
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else
  {
    if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_IN;
    pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
    pChn->CanPlayOrRecId = 1;
    pChn->CallTime = time(0);
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "被接管的坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "砆鋇恨�Y???�籪�?�Z�I");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  if (!pAG1->isThesvState(AG_SV_CONN))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "被接管的坐席或工號不處于通話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "砆鋇恨�Y???�籪�??��?杠�K?");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  nChn1 = pAG1->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "被接管的坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "砆鋇恨�Y????﹚?杠?笵");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  nChn2 = pAG1->GetSrvnChn(); //被接管的通道
  if (!pBoxchn->isnChnAvail(nChn2))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "被接管的坐席未與其他通道通話");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "砆鋇恨�Y???籔ㄤ�p?笵?杠");
    else
      SendSeatTakeOverFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }

  UnRouterTalk(nChn1, nChn2);
  pChn1->LinkType[0] = 0;
  pChn1->LinkChn[0] = 0xFFFF;
  pChn2->LinkType[0] = 0;
  pChn2->LinkChn[0] = 0xFFFF;
  //由流程腳本去處理
  //Release_Chn(nChn1, 1, 1);

  SendSeatTakeOverResult(nAGn, nChn2, OnSuccess, "");
  
  sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' seatno='%s' workerno='%d' groupno='%d' queueno='0' serialno='0' param='' errorbuf=''/>", 
    pAG1->GetSrvSessionNo(), pAG1->GetSrvCmdAddr(), pChn2->lgChnType, pChn2->lgChnNo, OnACDTakeover, 
    pChn->lgChnType, pChn->lgChnNo, pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNo(), pAGn->GetGroupNo());
  SendMsg2XML(MSG_oncallseat, pAG1->GetSrvSessionNo(), SendMsgBuf);

  return 0;
}
//監聽
int Proc_AGMSG_listen(CXMLRcvMsg &AGMsg)
{
  int nChn, nAG1, nChn1, nChn2, chState, i, j, workerno, nAG;
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "仿真測試環境不支持監聽功能");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "?痷代剛吏掛??�Y菏鈕�ｕ~");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatListenFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatListenFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
      else
        SendSeatListenFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (AGMsg.GetAttrValue(2).GetLength() > 0 && AGMsg.GetAttrValue(2).Compare("0") != 0)
    {
      Send_SWTMSG_listencall(nChn, pChn->DeviceID, pChn->ConnID, 0, AGMsg.GetAttrValue(2).C_Str());
      return 0;
    }
    if (AGMsg.GetAttrValue(3).GetLength() > 0 && AGMsg.GetAttrValue(3).Compare("0") != 0)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatListenFailResult(nAGn, "", OnFail, "被監聽的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatListenFailResult(nAGn, "", OnFail, "砆菏鈕�Y�籪�?�Z�I");
        else
          SendSeatListenFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatListenFailResult(nAGn, "", OnFail, "被監聽的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatListenFailResult(nAGn, "", OnFail, "砆菏鈕�Y�籪�?�Z�I");
        else
          SendSeatListenFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      else
      {
        Send_SWTMSG_listencall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG->m_Seat.SeatNo.C_Str());
        return 0;
      }
      return 0;
    }
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "被監聽號碼不正確");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "砆菏鈕腹絏???");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  
  if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "該坐席正忙");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "贛???Γ");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL))
  {
    if (pAGn->svState == AG_SV_IDEL)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatListenFailResult(nAGn, "", OnFail, "本坐席電話還未摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatListenFailResult(nAGn, "", OnFail, "????杠臨?篕訣");
      else
        SendSeatListenFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    else if (pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatListenFailResult(nAGn, "", OnFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatListenFailResult(nAGn, "", OnFail, "????杠?Γ");
      else
        SendSeatListenFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatListenFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatListenFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatListenFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatListenFailResult(nAGn, "", OnFail, "該坐席綁定的電話通道未通");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatListenFailResult(nAGn, "", OnFail, "贛???﹚�Y?杠?笵??");
      else
        SendSeatListenFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else
  {
    if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatListenFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatListenFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatListenFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_IN;
    pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
    pChn->CanPlayOrRecId = 1;
    pChn->CallTime = time(0);
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "被監聽的坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "砆菏鈕�Y???�籪�?�Z�I");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  if (!pAG1->isThesvState(AG_SV_CONN))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "被監聽的坐席或工號不處于通話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "砆菏鈕�Y???�籪�??��?杠�K?");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  nChn1 = pAG1->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatListenFailResult(nAGn, "", OnFail, "被監聽的坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatListenFailResult(nAGn, "", OnFail, "砆菏鈕�Y????﹚?杠?笵");
    else
      SendSeatListenFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  for (i = 2; i <= MAX_LINK_CHN_NUM; i ++)
  {
    if (pChn1->LinkType[i] == 0)
    {
      break;
    }
  }
  if (i < MAX_LINK_CHN_NUM)
  {
    if (RouterMonitor(nChn, nChn1, 1) == 0)
    {
      nChn2 = pChn1->LinkChn[0];
      if (pBoxchn->isnChnAvail(nChn2))
      {
        for (j = 2; j <= MAX_LINK_CHN_NUM; j ++)
        {
          if (pChn2->LinkType[j] == 0)
          {
            break;
          }
        }
        //2012-06-20
        if (j < MAX_LINK_CHN_NUM)
        {
          if (RouterMonitor(nChn, nChn2, 1) == 0)
          {
            pChn2->LinkType[j] = 3;
            pChn2->LinkChn[j] = nChn;
            pChn->LinkType[1] = 2;
            pChn->LinkChn[1] = nChn2;
          }
        }
        SendSeatListenResult(nAGn, nChn2, OnSuccess, "");
      }
      else
      {
        SendSeatListenFailResult(nAGn, "", OnSuccess, "");
      }
      pChn->LinkType[0] = 2;
      pChn->LinkChn[0] = nChn1;
      pChn1->LinkType[i] = 3;
      pChn1->LinkChn[i] = nChn;
      SetAgentsvState(nAGn, AG_SV_CONN);
      return 0;
    }
  }
  
  SendSeatListenFailResult(nAGn, "", OnFail, "");
  Release_Chn(nChn, 1, 0);
  
  return 0;
}
//強插通話
int Proc_AGMSG_insert(CXMLRcvMsg &AGMsg)
{
  int nChn, nAG1, nChn1, nChn2, chState, nCfc, workerno, nAG;
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }
  if (g_nCardType == 9)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "仿真測試環境不支持強插功能");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "?痷代剛吏掛??�Y?礎�ｕ~");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (AGMsg.GetAttrValue(2).GetLength() > 0 && AGMsg.GetAttrValue(2).Compare("0") != 0)
    {
      Send_SWTMSG_breakincall(nChn, pChn->DeviceID, pChn->ConnID, 0, AGMsg.GetAttrValue(2).C_Str());
      return 0;
    }
    if (AGMsg.GetAttrValue(3).GetLength() > 0 && AGMsg.GetAttrValue(3).Compare("0") != 0)
    {
      workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
      if (workerno == 0)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatBreakinFailResult(nAGn, "", OnFail, "被強插的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatBreakinFailResult(nAGn, "", OnFail, "砆?礎�Y�籪�?�Z�I");
        else
          SendSeatBreakinFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      nAG = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG))
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendSeatBreakinFailResult(nAGn, "", OnFail, "被強插的工號不存在");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendSeatBreakinFailResult(nAGn, "", OnFail, "砆?礎�Y�籪�?�Z�I");
        else
          SendSeatBreakinFailResult(nAGn, "", OnFail, "");
        return 1;
      }
      else
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          Send_SWTMSG_breakincall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG->m_Seat.SeatNo.C_Str());
        return 0;
      }
      return 0;
    }
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "被強插號碼不正確");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "砆?礎腹絏???");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  
  if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席正忙");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "贛???Γ");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL))
  {
    if (pAGn->svState == AG_SV_IDEL)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "本坐席電話還未摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "????杠臨?篕訣");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    else if (pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "????杠?Γ");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席綁定的電話通道未通");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "贛???﹚�Y?杠?笵??");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else
  {
    if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后呼出");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A㊣�|");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_IN;
    pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
    pChn->CanPlayOrRecId = 1;
    pChn->CallTime = time(0);
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "被強插的坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "砆?礎�Y???�籪�?�Z�I");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  if (!pAG1->isThesvState(AG_SV_CONN))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "被強插的坐席或工號不處于通話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "砆?礎�Y???�籪�??��?杠�K?");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  nChn1 = pAG1->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "被強插的坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "砆?礎�Y????﹚?杠?笵");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  if (pChn1->JoinType > 0 || pChn1->CfcNo < MAX_CONF_NUM)
  {
    nCfc = pChn1->CfcNo;
    if (pCfc->Talkeds >= pCfc->MaxTalkers)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "強插失敗1");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatBreakinFailResult(nAGn, "", OnFail, "?礎?毖1");
      else
        SendSeatBreakinFailResult(nAGn, "", OnFail, "");
      Release_Chn(nChn, 1, 0);
      return 1;
    }
    if (JoinConf(nCfc, nChn, 100) == 0)
    {
      pChn->CfcNo = nCfc;
      pChn->JoinTime = time(0);
      pCfc->Talkers[pCfc->Talkeds] = nChn;
      pChn->JoinType = talker;
      pCfc->Talkeds ++;
      //theApp.m_MyConfStatusDialog->ProcDispMenber(nCfc, nChn, talker);
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
      SetAgentsvState(nAGn, AG_SV_CONN);
      SendSeatListenResult(nAGn, nChn1, OnSuccess, "");
      return 0;
    }
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "強插失敗2");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatBreakinFailResult(nAGn, "", OnFail, "?礎?毖2");
    else
      SendSeatBreakinFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  if (pChn1->LinkType[0] == 1 || pChn1->LinkType[0] == 6 || pChn1->LinkType[0] == 7)
  {
    nChn2 = pChn1->LinkChn[0];
    if (pBoxchn->isnChnAvail(nChn2))
    {
      nCfc = CreateThreeConf(nChn);
      if (nCfc > 0)
      {
        UnRouterTalk(nChn1, nChn2);
        pChn1->LinkType[0] = 0;
        pChn1->LinkChn[0] = 0xFFFF;
        pChn2->LinkType[0] = 0;
        pChn2->LinkChn[0] = 0xFFFF;
        
        StopClearPlayDtmfRecBuf(nChn);
        StopClearPlayDtmfRecBuf(nChn1);
        StopClearPlayDtmfRecBuf(nChn2);
        if (JoinThreeConf(nCfc, nChn, nChn1, nChn2) == 0)
        {
          SetAgentsvState(nAGn, AG_SV_CONN);
          SendSeatBreakinResult(nAGn, nChn1, OnSuccess, "");
          return 0;
        }
      }
    }
  }
  if (pAGn->m_Seat.SeatLangID == 1)
    SendSeatBreakinFailResult(nAGn, "", OnFail, "強插失敗3");
  else if (pAGn->m_Seat.SeatLangID == 2)
    SendSeatBreakinFailResult(nAGn, "", OnFail, "?礎?毖3");
  else
    SendSeatBreakinFailResult(nAGn, "", OnFail, "");
  Release_Chn(nChn, 1, 0);
  return 0;
}
//保持/取消保持
int Proc_AGMSG_hold(CXMLRcvMsg &AGMsg)
{
  int nChn, nChn1, nChn2, nResult;
  
  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onhold, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onhold, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onhold, OnFail, "the seat had not login");
    return 1;
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onhold, OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onhold, OnFail, "贛????﹚?杠?笵");
      else
        SendCommonResult(AGMSG_onhold, OnFail, "the seat have not bangding channel");
      return 1;
    }
    if (atoi(AGMsg.GetAttrValue(4).C_Str()) == 1)
    {
      Send_SWTMSG_holdcall(nChn, pChn->DeviceID, pChn->ConnID);
    }
    else
    {
      Send_SWTMSG_unholdcall(nChn, pChn->DeviceID, pChn->ConnID);
    }
    return 0;
  }

  //-----------------板卡版本-----------------------------------------
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onhold, OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onhold, OnFail, "贛????﹚?杠?笵");
    else
      SendCommonResult(AGMSG_onhold, OnFail, "the seat have not bangding channel");
    return 1;
  }
  if (pAGn->svState != AG_SV_CONN && pAGn->svState != AG_SV_HOLD)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onhold, OnFail, "該坐席未處于通話狀態");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onhold, OnFail, "贛????��?杠�K?");
    else
      SendCommonResult(AGMSG_onhold, OnFail, "");
    return 1;
  }

  if (pIVRCfg->ControlCallModal == 1)
  {
    if (pChn->TranStatus == 0)
    {
      //正常2方通話情況
      if (atoi(AGMsg.GetAttrValue(4).C_Str()) == 1)
      {
        //保持操作
        nChn1 = pChn->LinkChn[0];
        if ((pChn->LinkType[0] == 1 || pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7) && pBoxchn->isnChnAvail(nChn1))
        {
          UnRouterTalk(nChn, nChn1);
          pChn->LinkType[0] = 0;
          pChn->LinkChn[0] = 0xFFFF;
          pChn->HoldingnChn = nChn1;
          pChn1->LinkType[0] = 0;
          pChn1->LinkChn[0] = 0xFFFF;
          pChn1->HoldingnChn = nChn;
          if (SetDTMFPlayRule(nChn1, 0, MAX_PLAYRULE_BUF, ErrMsg) == 0)
            PlayFile(nChn1, pIVRCfg->WaitVocFile, 0, MAX_PLAYRULE_BUF); //循環放等待音
          
          WriteSeatStatus(pAGn, AG_STATUS_HOLD, 0);
          pAGn->SetAgentsvState(AG_SV_HOLD);
          DispAgentStatus(nAGn);
          SendCommonResult(AGMSG_onhold, OnSuccess, "hold");
        }
      }
      else
      {
        //取消保持
        nChn1 = pChn->HoldingnChn;
        if (pChn->LinkType[0] == 0 && pBoxchn->isnChnAvail(nChn1))
        {
          StopClearPlayDtmfBuf(nChn1);
          nResult = RouterTalk(nChn, nChn1);
          if (nResult == 0)
          {
            pChn->RecMixerFlag = 1;
            pChn->LinkType[0] = 1;
            pChn->LinkChn[0] = nChn1;
            pChn->HoldingnChn = 0xFFFF;
            
            pChn1->RecMixerFlag = 1;
            pChn1->LinkType[0] = 1;
            pChn1->LinkChn[0] = nChn;
            pChn1->HoldingnChn = 0xFFFF;
            
            WriteSeatStatus(pAGn, AG_STATUS_TALK, 0);
            pAGn->SetAgentsvState(AG_SV_CONN);
            DispAgentStatus(nAGn);
            SendCommonResult(AGMSG_onhold, OnSuccess, "unhold");
          }
        }
      }
    } 
    else if (pChn->TranStatus == 5)
    {
      nChn1 = pChn->TranControlnChn; //被轉接的通道
      nChn2 = pChn->TranDestionnChn; //轉接的目的通道
      if (pBoxchn->isnChnAvail(nChn1) && pBoxchn->isnChnAvail(nChn2))
      {
        if (nChn1 == pChn->LinkChn[0])
        {
          //正在與被轉接通道通話,則先斷開然后與轉接的目的通道通話
          UnRouterTalk(nChn, nChn1);
          pChn->LinkType[0] = 0;
          pChn->LinkChn[0] = 0xFFFF;
          pChn1->LinkType[0] = 0;
          pChn1->LinkChn[0] = 0xFFFF;
          PlayFile(nChn1, pIVRCfg->WaitVocFile, 0, MAX_PLAYRULE_BUF); //循環放等待音
          
          StopClearPlayDtmfBuf(nChn2);
          nResult = RouterTalk(nChn, nChn2);
          if (nResult == 0)
          {
            pChn->RecMixerFlag = 1;
            pChn->LinkType[0] = 1;
            pChn->LinkChn[0] = nChn2;
            
            pChn2->RecMixerFlag = 1;
            pChn2->LinkType[0] = 1;
            pChn2->LinkChn[0] = nChn;
          }
          
          WriteSeatStatus(pAGn, AG_STATUS_CONSULT, 0);
          pAGn->SetAgentsvState(AG_SV_HOLD);
          DispAgentStatus(nAGn);
          SendCommonResult(AGMSG_onhold, OnSuccess, "hold");
        }
        else if (nChn2 == pChn->LinkChn[0])
        {
          //正在與轉接的目的通道通話,則先斷開然后與被轉接通道通話
          UnRouterTalk(nChn, nChn2);
          pChn->LinkType[0] = 0;
          pChn->LinkChn[0] = 0xFFFF;
          pChn2->LinkType[0] = 0;
          pChn2->LinkChn[0] = 0xFFFF;
          PlayFile(nChn2, pIVRCfg->WaitVocFile, 0, MAX_PLAYRULE_BUF); //循環放等待音
          
          StopClearPlayDtmfBuf(nChn1);
          nResult = RouterTalk(nChn, nChn1);
          if (nResult == 0)
          {
            pChn->RecMixerFlag = 1;
            pChn->LinkType[0] = 1;
            pChn->LinkChn[0] = nChn1;
            
            pChn1->RecMixerFlag = 1;
            pChn1->LinkType[0] = 1;
            pChn1->LinkChn[0] = nChn;
          }
          
          WriteSeatStatus(pAGn, AG_STATUS_TALK, 0);
          pAGn->SetAgentsvState(AG_SV_CONN);
          DispAgentStatus(nAGn);
          SendCommonResult(AGMSG_onhold, OnSuccess, "unhold");
        }
      }
    }
    return 0;
  }

  sprintf(SendMsgBuf, "<onaghold sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' flag='%d' waitvoc='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    atoi(AGMsg.GetAttrValue(4).C_Str()), AGMsg.GetAttrValue(5).C_Str());
  SendMsg2XML(MSG_onaghold, pChn->SessionNo, SendMsgBuf);

  if (atoi(AGMsg.GetAttrValue(4).C_Str()) == 1)
  {
    WriteSeatStatus(pAGn, AG_STATUS_HOLD, 0);

    pAGn->SetAgentsvState(AG_SV_HOLD);
    DispAgentStatus(nAGn);
  }
  else
  {
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0);

    pAGn->SetAgentsvState(AG_SV_CONN);
    DispAgentStatus(nAGn);
  }

  SendCommonResult(AGMSG_onhold, OnSuccess, "");
  return 0;
}
//靜音/取消靜音
int Proc_AGMSG_mute(CXMLRcvMsg &AGMsg)
{
  int nChn;
  
  if (pAGn->isLogin() == false) 
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onmute, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onmute, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onmute, OnFail, "the seat had not login");
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onmute, OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onmute, OnFail, "贛????﹚?杠?笵");
    else
      SendCommonResult(AGMSG_onmute, OnFail, "the seat have not bangding channel");
    return 1;
  }
  sprintf(SendMsgBuf, "<onagmute sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' flag='%d'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
    atoi(AGMsg.GetAttrValue(4).C_Str()));
  SendMsg2XML(MSG_onaghold, pChn->SessionNo, SendMsgBuf);
  
  SendCommonResult(AGMSG_onmute, OnSuccess, "");
  return 0;
}
//釋放一方通話,
int Proc_AGMSG_releasecall(CXMLRcvMsg &AGMsg)
{
  return 0;
}

//強拆其他坐席
int Proc_AGMSG_forceclear(CXMLRcvMsg &AGMsg)
{
  int nAG1, nChn1;
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforceclear, OnFail, "該強拆的坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforceclear, OnFail, "贛??�Y???�籪�?�Z�I");
    else
      SendCommonResult(AGMSG_onforceclear, OnFail, "");
    return 1;
  }
  nChn1 = pAG1->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforceclear, OnFail, "該強拆的坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforceclear, OnFail, "贛??�Y????﹚?杠?笵");
    else
      SendCommonResult(AGMSG_onforceclear, OnFail, "");
    return 1;
  }
  Release_Chn(nChn1, 1, 1);
  SendCommonResult(AGMSG_onforceclear, OnSuccess, "");
  return 0;
}
//強制將其他話務員退出
int Proc_AGMSG_forcelogout(CXMLRcvMsg &AGMsg)
{
  int nAG1, nQue;
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforcelogout, OnFail, "該坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforcelogout, OnFail, "贛???�籪�?�Z�I");
    else
      SendCommonResult(AGMSG_onforcelogout, OnFail, "");
    return 1;
  }
  nQue = pAG1->nQue;
  if (pCalloutQue->isnQueAvail(nQue) && pAG1->isWantSeatType(SEATTYPE_AG_PC))
  {
    pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
  }
  if (pAG1->m_Worker.oldWorkerNo == 0)
  {
    pAG1->m_Worker.Logout();
    pAG1->m_Worker.ClearState();
  }
  else
  {
    pAG1->m_Worker.OldWorkerLogin();
  }
  DispAgentStatus(nAG1);
  SendCommonResult(AGMSG_onforcelogout, OnSuccess, "");
  return 0;
}
//強制其他坐席免打擾
int Proc_AGMSG_forcedisturb(CXMLRcvMsg &AGMsg)
{
  int nAG1;
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforcedisturb, OnFail, "該坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforcedisturb, OnFail, "贛???�籪�?�Z�I");
    else
      SendCommonResult(AGMSG_onforcedisturb, OnFail, "");
    return 1;
  }
  if (pAG1->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforcedisturb, OnFail, "該坐席或工號未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforcedisturb, OnFail, "贛???�籪�?祆魁");
    else
      SendCommonResult(AGMSG_onforcedisturb, OnFail, "");
    return 1;
  }
  pAG1->m_Worker.SetDisturb(1);
  DispAgentStatus(nAG1);
  SendCommonResult(AGMSG_onforcedisturb, OnSuccess, "");
  return 0;
}
//強制其他坐席空閑
int Proc_AGMSG_forceready(CXMLRcvMsg &AGMsg)
{
  int nAG1;
  nAG1 = GetnAGbyWorkerNoSeatNo(AGMsg.GetAttrValue(2).C_Str(), atoi(AGMsg.GetAttrValue(3).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforceready, OnFail, "該坐席或工號不存在");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforceready, OnFail, "贛???�籪�?�Z�I");
    else
      SendCommonResult(AGMSG_onforceready, OnFail, "");
    return 1;
  }
  if (pAG1->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onforceready, OnFail, "該坐席或工號未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onforceready, OnFail, "贛???�籪�?祆魁");
    else
      SendCommonResult(AGMSG_onforceready, OnFail, "");
    return 1;
  }
  pAG1->m_Worker.SetDisturb(0);
  pAG1->m_Worker.SetLeave(0, "Force Ready");
  DispAgentStatus(nAG1);
  SendCommonResult(AGMSG_onforceready, OnSuccess, "");
  return 0;
}

//取ACD分配隊列信息
int Proc_AGMSG_getqueueinfo(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
  {
    return 1;
  }
  pAGn->m_Worker.GetWaitQueue = (UC)atoi(AGMsg.GetAttrValue(2).C_Str()); 
  if (pAGn->m_Worker.GetWaitQueue == 1)
  {
    SendACDQueueInfo(nAGn);
  }
  return 0;
}
//取所有坐席的狀態信息
int Proc_AGMSG_getagentstatus(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
  {
    return 1;
  }
  pAGn->m_Worker.GetAGState = (UC)atoi(AGMsg.GetAttrValue(2).C_Str()); 
  if (pAGn->m_Worker.GetAGState == 1)
  {
    if (pIVRCfg->isOnlySendMySeatStatus == false || pAGn->m_Worker.WorkerGrade > 2)
      SendAllAGStateToOne(nAGn);
    else
      SendAGSelfState(nAGn);
  }
  return 0;
}
//取所有通道的狀態信息
int Proc_AGMSG_getchnstatus(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
  {
    return 1;
  }
  pAGn->m_Worker.GetChState = (UC)atoi(AGMsg.GetAttrValue(2).C_Str()); 
  if (pAGn->m_Worker.GetChState == 1)
    SendAllCHStateToOne(nAGn);
  return 0;
}

//發送消息到流程
int Proc_AGMSG_sendmsgtoflw(CXMLRcvMsg &AGMsg)
{
  int nChn, chntype, chnno, OnlineId;
  
  if (pAGn->isLogin() == false)
  {
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return 1;
  }
  chntype = atoi(AGMsg.GetAttrValue(2).C_Str());
  chnno = atoi(AGMsg.GetAttrValue(3).C_Str());
  if (chntype != 0 && chnno != 0)
  {
    OnlineId = pBoxchn->Get_ChnNo_By_LgChn(chntype, chnno, nChn);
    if (OnlineId != 0)
    {
      return 1;
    }
  }
  else
  {
    if (pChn->LinkType[0] == 1 || pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7)
    {
      nChn = pChn->LinkChn[0];
    }
  }
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (atoi(AGMsg.GetAttrValue(4).C_Str()) == 207) //2015-05-10
    {
      if (atoi(AGMsg.GetAttrValue(5).C_Str()) == 1)
      {
        char szCallerID[32];
        memset(szCallerID, 0, 32);
        strncpy(szCallerID, GetParamStrValue("CallerNo=", AGMsg.GetAttrValue(6).C_Str()), 31);
        SendQueryCustomer2Webservice(szCallerID, "", pIVRCfg->QueryCustomerWebserviceAddr, pIVRCfg->QueryCustomerWebserviceParam);
      }
    }
    else
    {
      sprintf(SendMsgBuf, "<onagflwmsg sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' eventtype='%s' msgtype='%s' msg='%s'/>", 
        pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo,
        AGMsg.GetAttrValue(4).C_Str(),
        AGMsg.GetAttrValue(5).C_Str(),
        AGMsg.GetAttrValue(6).C_Str());
      SendMsg2XML(MSG_onagflwmsg, pChn->SessionNo, SendMsgBuf);
    }
  }
  return 0;
}
//發送傳真
int Proc_AGMSG_sendfax(CXMLRcvMsg &AGMsg)
{
  return 0;
}
//接收傳真,通話后接收傳真
int Proc_AGMSG_recvfax(CXMLRcvMsg &AGMsg)
{
  return 0;
}
//發送文字信息
int Proc_AGMSG_sendmessage(CXMLRcvMsg &AGMsg)
{
  int workerno, groupno, nResult;
  char szAnsiMsg[1024], szUTF8Msg[1024];
  CAgent *pAgent=NULL;

  if (!pAGn->isLogin())
  {
    return 1;
  }
  memset(szAnsiMsg, 0, 1024);
  memset(szUTF8Msg, 0, 1024);

  strncpy(szAnsiMsg, AGMsg.GetAttrValue(5).C_Str(), 511);
  UTF8_ANSI_Convert(szAnsiMsg, szUTF8Msg, CP_ACP, CP_UTF8);

  if (AGMsg.GetAttrValue(2).Compare("0") != 0) //指定坐席
  {
    pAgent = GetAgentbySeatNo(AGMsg.GetAttrValue(2), nResult);
    if (pAgent != NULL)
    {
      SendSeatMessage(pAgent->nAG, nAGn, szAnsiMsg, szUTF8Msg);
    }
  }
  workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
  if (workerno != 0) //指定工號
  {
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
    if (pAgent != NULL)
    {
      SendSeatMessage(pAgent->nAG, nAGn, szAnsiMsg, szUTF8Msg);
    }
  }
  groupno = atoi(AGMsg.GetAttrValue(4).C_Str());
  if (groupno != 0) //指定組號
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (groupno == pAgentMng->m_Agent[nAG]->GetGroupNo())
      {
        SendSeatMessage(nAG, nAGn, szAnsiMsg, szUTF8Msg);
      }
    }
  }
  if (AGMsg.GetAttrValue(2).Compare("0") == 0 && workerno == 0 && groupno == 0)
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (nAG == nAGn)
        continue;
      SendSeatMessage(nAG, nAGn, szAnsiMsg, szUTF8Msg);
    }
  }
  return 0;
}
//發送短信
int Proc_AGMSG_sendsms(CXMLRcvMsg &AGMsg)
{
  return 0;
}
//創建會議
int Proc_AGMSG_createconf(CXMLRcvMsg &AGMsg)
{
  int nChn, nResult, nCfc;
  int cfcno, presiders, talkers, listeners, totaltalks, autofree;

  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatCreateConfResult(nAGn, 0, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatCreateConfResult(nAGn, 0, OnFail, "贛???祆厚");
    else
      SendSeatCreateConfResult(nAGn, 0, OnFail, "the seat had not login");
    return 1;
  }
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatCreateConfResult(nAGn, 0, OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatCreateConfResult(nAGn, 0, OnFail, "贛????﹚?杠?笵");
    else
      SendSeatCreateConfResult(nAGn, 0, OnFail, "the seat have not bangding channel");
    return 1;
  }

  cfcno = atoi(AGMsg.GetAttrValue(2).C_Str());
  presiders = atoi(AGMsg.GetAttrValue(4).C_Str());
  talkers = atoi(AGMsg.GetAttrValue(5).C_Str());
  listeners = atoi(AGMsg.GetAttrValue(6).C_Str());
  autofree = atoi(AGMsg.GetAttrValue(9).C_Str());

  if (cfcno == 0)
  {
    nCfc = pBoxconf->Get_Idle_nCfc();
    if (!pBoxconf->isnCfcAvail(nCfc))
    {
      SendSeatCreateConfResult(nAGn, 0, OnFail, "not conf resource");
      return 1;
    }
  }
  else if (!pBoxconf->isnCfcAvail(cfcno))
  {
    SendSeatCreateConfResult(nAGn, 0, OnFail, "confno is out of range");
    return 1;
  }
  else
  {
    nCfc = cfcno;
    if (pCfc->state != 0)
    {
      SendSeatCreateConfResult(nAGn, 0, OnFail, "the conf had created");
      return 1;
    }
  }
  if (presiders > MAX_PRESIDER_NUM)
  {
    presiders = MAX_PRESIDER_NUM;
  }
  if (talkers > MAX_TALKERS_NUM)
  {
    talkers = MAX_TALKERS_NUM;
  }
  totaltalks = presiders + talkers;
  
  nResult = CreateConf(nCfc, nChn, totaltalks, listeners, 0);
  if (nResult >= 0)
  {
    pCfc->state = 1;
    pCfc->timer = 0;
    
    pCfc->UseId = 1;
    
    pCfc->Name = AGMsg.GetAttrValue(3);
    pCfc->Owner = pAGn->GetWorkerName();
    pCfc->Password = AGMsg.GetAttrValue(7);
    
    pCfc->MaxPresiders = presiders;
    pCfc->MaxTalkers = talkers;
    pCfc->MaxListeners = listeners;
    pCfc->FreeId = autofree;
    pCfc->AutoRecordId = 0;
    
    pCfc->CreateId = 1;
    if (nResult == nCfc)
    {
      pCfc->CreateId = 3;
      SendSeatCreateConfResult(nAGn, nCfc, OnSuccess, "");
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    }
    return 0;
  }
  else
  {
    SendSeatCreateConfResult(nAGn, 0, OnFail, "create conf fail");
    return 1;
  }
}
//釋放會議
int Proc_AGMSG_destroyconf(CXMLRcvMsg &AGMsg)
{
  int nCfc;
  
  nCfc = atoi(AGMsg.GetAttrValue(2).C_Str());
  if (!pBoxconf->isnCfcAvail(nCfc))
  {
    SendCommonResult(AGMSG_ondestroyconf, OnFail, "confno is out of range");
    return 1;
  }
  if (pCfc->state == 0 || pCfc->CreateId != 3)
  {
    SendCommonResult(AGMSG_ondestroyconf, OnFail, "the conf is not open");
    return 1;
  }
  
  if (DestroyConf(nCfc, 0, 0) == nCfc)
  {
    DelAllConfMember(nCfc, 1);
    pBoxconf->InitConf(nCfc);
    SendCommonResult(AGMSG_ondestroyconf, OnSuccess, "");
    //theApp.m_MyConfStatusDialog->ClearConf(nCfc);
    //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    return 0;
  }
  SendCommonResult(AGMSG_ondestroyconf, OnFail, "destroy conf fail");
  return 0;
}
//設置事后處理時長(秒)
int Proc_AGMSG_setacwtimer(CXMLRcvMsg &AGMsg) 
{
  int len;
  
  if (!pAGn->isLogin())
    return 1;
  len = atoi(AGMsg.GetAttrValue(2).C_Str()); //事后處理延時時長
  if (len < 4)
  {
    pAGn->m_Worker.DelayTime = g_nDefaultACWTimeLen;
  }
  else
  {
    pAGn->m_Worker.DelayTime = len;
  }
  SendCommonResult(AGMSG_onsetacwtimer, OnSuccess, "");
  DispAgentStatus(nAGn);
  return 0;
}
//設置事后處理完成
int Proc_AGMSG_setacwend(CXMLRcvMsg &AGMsg) 
{
  if (!pAGn->isLogin())
    return 1;
  //int nChn = pAGn->GetSeatnChn();
  if (pAGn->svState == AG_SV_IDEL)
  {
    if (pAGn->DelayState == 1)
    {
      if (pIVRCfg->isSetReadyStateonAgentSetDisturb > 0)
      {
        Send_SWTMSG_setagentstatus(pAGn->GetSeatnChn(), pAGn->GetSeatNo().C_Str(), pAGn->GetWorkerNoStr(), "", "", AG_AM_READY);
      }
      WriteSeatStatus(pAGn, AG_STATUS_IDEL, 0);
      DBUpdateCallCDR_ACWTime(nAGn);
    }
    pAGn->DelayState = 0;
    AgentStatusCount(); //2015-12-14
    SendOneGroupStatusMeteCount(pAGn->nAG);
    SendSystemStatusMeteCount();
    DispAgentStatus(nAGn);
  }
  return 0;
}
//穿梭保持通話
int Proc_AGMSG_swaphold(CXMLRcvMsg &AGMsg) 
{
  if (!pAGn->isLogin())
    return 1;
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    int nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onhold, OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onhold, OnFail, "贛????﹚?杠?笵");
      else
        SendCommonResult(AGMSG_onhold, OnFail, "the seat have not bangding channel");
      return 1;
    }
    Send_SWTMSG_swapcall(nChn, pChn->DeviceID, pChn->ConnID);
  }
  return 0;
}
//設置技能組名稱
int Proc_AGMSG_setgroupname(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
    return 1;

  char group[32];
  int groupno = atoi(AGMsg.GetAttrValue(2).C_Str());
  if (groupno < MAX_AG_GROUP)
  {
    pAgentMng->WorkerGroup[groupno].GroupName = AGMsg.GetAttrValue(3);
    sprintf(group, "GroupName[%d]", groupno);
    WritePrivateProfileString("GROUPNAME", group, AGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
  }
  return 0;
}
//開始錄音
int Proc_AGMSG_startrecord(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
    return 1;
  char errorbuf[128];
  CStringX FilePath;

  pAGn->m_Worker.AutoRecordId = 1;

  int nChn = pAGn->GetSeatnChn();
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (AGMsg.GetAttrValue(2).GetLength() > 4)
    {
      FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, AGMsg.GetAttrValue(2).C_Str());
    }
    else
    {
      FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, pChn->RecordFileName);
    }
    
    if (CreateFullPathIfNotExist(FilePath.C_Str()) !=0)
    {
      return 1;
    }
    SetChnRecordRule(nChn, 0, FilePath.C_Str(), "", 0, atoi(AGMsg.GetAttrValue(3).C_Str()), 1, errorbuf, 1);
  }
  return 0;
}
//停止錄音
int Proc_AGMSG_stoprecord(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
    return 1;
  if (atoi(AGMsg.GetAttrValue(2).C_Str()) == 0)
  {
    pAGn->m_Worker.AutoRecordId = 0;
  }
  int nChn = pAGn->GetSeatnChn();
  if (pBoxchn->isnChnAvail(nChn))
  {
    StopRecord(nChn);
    ResetRecBuf(nChn);
  }
  return 0;
}
//設置呼叫方向(0-雙向服務 1-呼入 2-呼出)
int Proc_AGMSG_setcalldirection(CXMLRcvMsg &AGMsg)
{
  if (!pAGn->isLogin())
    return 1;
  pAGn->m_Worker.CallDirection = atoi(AGMsg.GetAttrValue(2).C_Str());
  return 0;
}
//代接電話擴展指令
int Proc_AGMSG_pickupex(CXMLRcvMsg &AGMsg)
{
  int nChn, nAG1, nChn1, nQue, nOut, nAcd, chState;
  CAgent *pAgent1=NULL;

  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "贛???祆厚");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "the seat had not login");
    return 1;
  }

  int nPickupType = atoi(AGMsg.GetAttrValue(2).C_Str());
  
  //PickupType 1-坐席分機號，2-話務員工號，3-話務員組號，4-話務員登錄賬號，5-話務員登錄姓名
  switch (nPickupType)
  {
  case 1:
  	nAG1 = pAgentMng->GetnAGBySeatNo(AGMsg.GetAttrValue(3).C_Str());
    break;
  case 2:
    nAG1 = pAgentMng->GetnAGByWorkerNo(atoi(AGMsg.GetAttrValue(3).C_Str()));
    break;
  case 3:
    nAG1 = 0xFFFF;
    break;
  case 4:
    nAG1 = pAgentMng->GetnAGByAccountNo(AGMsg.GetAttrValue(3).C_Str());
    break;
  case 5:
    nAG1 = pAgentMng->GetnAGByWorkerName(AGMsg.GetAttrValue(3).C_Str());
    break;
  default:
    nAG1 = 0xFFFF;
    break;
  }
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "代接目的號碼不正確");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇?�Y腹絏???");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    nChn = pAGn->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
      return 1;
    }
    Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG1->m_Seat.SeatNo.C_Str());
    return 0;
  }

  //板卡版本
  if ((pAGn->isWantSeatType(SEATTYPE_AG_PC) || pAGn->isWantSeatType(SEATTYPE_RT_TEL)) && pAGn->isIdelForSrv() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席正忙");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "贛???Γ");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    return 1;
  }
  else if (pAGn->isWantSeatType(SEATTYPE_AG_TEL))
  {
    if (pAGn->svState == AG_SV_IDEL)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "本坐席電話還未摘機");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "????杠臨?篕訣");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    else if (pAGn->svState != AG_SV_INSEIZE)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "本坐席電話正忙");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "????杠?Γ");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  
  nChn = pAGn->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席未綁定電話通道");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "贛????﹚?杠?笵");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "the seat have not bangding channel");
    return 1;
  }
  
  chState = GetAgentChState(pAGn);
  if (pAGn->isWantSeatType(SEATTYPE_AG_PC))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后代接");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A�r鋇");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else if (pAGn->isWantSeatType(SEATTYPE_RT_TEL))
  {
    if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席綁定的電話通道未通");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛???﹚�Y?杠?笵??");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
  }
  else
  {
    if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendSeatPickupFailResult(nAGn, "", OnFail, "該坐席電話還未摘機,請先摘機然后代接");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendSeatPickupFailResult(nAGn, "", OnFail, "贛???杠臨?篕訣,叫�g篕訣礛�A�r鋇");
      else
        SendSeatPickupFailResult(nAGn, "", OnFail, "");
      return 1;
    }
    SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
    pChn->hwState = CHN_HW_VALID;
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_IN;
    pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
    pChn->CanPlayOrRecId = 1;
    pChn->CallTime = time(0);
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }

  if (pAgentMng->m_Agent[nAG1]->isThesvState(AG_SV_INRING))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "代接的坐席沒有電話呼入");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇�Y??⊿Τ?杠㊣��");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  nQue = pAG1->nQue;
  if (!pCalloutQue->isnQueAvail(nQue))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendSeatPickupFailResult(nAGn, "", OnFail, "代接的坐席沒有電話呼入");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendSeatPickupFailResult(nAGn, "", OnFail, "�r鋇�Y??⊿Τ?杠㊣��");
    else
      SendSeatPickupFailResult(nAGn, "", OnFail, "");
    Release_Chn(nChn, 1, 0);
    return 1;
  }
  //取消以前的呼叫
  nOut = pQue->nOut;
  nAcd = pQue->nAcd;
  nChn1 = pQue->OutnChn;
  CancelCallout(nChn1);
  pAG1->nQue = 0xFFFF;
  if (pAG1->isWantSeatType(SEATTYPE_RT_TEL))
  {
    pChn1->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
    pAG1->m_Seat.nChn = 0xFFFF;
  }
  SendStopACDCallIn(nAG1);
  WriteSeatStatus(pAG1, AG_STATUS_IDEL, 2); //代接標志
  SetAgentsvState(nAG1, AG_SV_IDEL);
  pQue->state = 0;
  //發送新的呼叫結果給被代接通道
  if (pQue->OrgCallType == 1)
  {
    //取消被代接通道的呼出
    for (int j=0; j<pOut->CalledNum; j++)
    {
      if (pQue->CallPoint==j) continue;
      if (pOut->Calleds[j].pQue_str != NULL)
        pOut->Calleds[j].pQue_str->CancelId = 1;
      if (pOut->Calleds[j].CalloutResult == CALLOUT_CONN)
      {
        pOut->Calleds[j].CalloutResult = 0;
        Release_Chn(pOut->Calleds[j].OutnChn, 1, 0);
      }
    }
    pOut->state = 0;
    
    SendCalloutResult(nOut, pQue->CallPoint, OnOutAnswer, "", nChn);
    pAgentMng->lastAnsnAG[0] = nAGn;
    pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][0] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][pAGn->GetLevel()] = nAGn;
    pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
    pAGn->m_Worker.AcdedGroupNo = 0;
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);

    SetAgentsvState(nAGn, AG_SV_CONN);
    SendSeatPickupResult(nAGn, nOut, pQue->CallPoint, OnSuccess, "");
  }
  else
  {
    //取消被代接通道的呼叫坐席
    for (int j=0; j<pAcd->CalledNum; j++)
    {
      if (pQue->CallPoint==j) continue;
      if (pAcd->Calleds[j].pQue_str != NULL)
        pAcd->Calleds[j].pQue_str->CancelId = 1;
      pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
      if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
      {
        pAcd->Calleds[j].CalloutResult = 0;
        Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
      }
      SetAgentsvState(pAgent1, AG_SV_IDEL);
    }
    
    pAGn->CanTransferId = 1;
    pAGn->SetSrvLine(pAcd->Session.nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
    SendCallseatResult(nAcd, pAGn, 0, OnACDAnswer, "");
    pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
    pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
    pAGn->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);
    
    if (pIVRCfg->ControlCallModal == 0)
    {
      pAGn->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
    }
    else
    {
      nChn = pAcd->Session.nChn;
      if (pChn->lgChnType == 2)
      {
        DBUpdateCallCDR_CallInOut(nChn, 3);
      }
      //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
      strcpy(pAGn->CdrSerialNo, pChn->CdrSerialNo);
      strcpy(pAGn->RecordFileName, pChn->RecordFileName);
      DBUpdateCallCDR_SeatAnsTime(pAGn->nAG, pAcd->ACDRule.AcdedGroupNo);
      if (pAGn->m_Worker.AutoRecordId == 1 && pAGn->m_Seat.AutoRecordId == 1)
      {
        char szTemp[256], pszError[128];
        sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAGn->RecordFileName);
        CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
        
        if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
        {
          if (SetChnRecordRule(pAGn->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
          {
            DBUpdateCallCDR_RecdFile(pAGn->nAG);
          }
        }
      }
    }
    DelFromQueue(nAcd);
    
    SetAgentsvState(nAGn, AG_SV_CONN);
    SendSeatPickupResult(nAGn, nAcd, OnSuccess, "");
  }

  return 0;
}
//接管其他坐席的電話
int Proc_AGMSG_redirectcall(CXMLRcvMsg &AGMsg)
{
  int nAG, nAG1, workerno, nQue, nChn, nAcd;
  char szSourDeviceID[64], szDestDeviceID[64];
  
  if (pAGn->isLogin() == false)
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onredirectcall, OnFail, "該坐席未登錄");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onredirectcall, OnFail, "贛???祆厚");
    else
      SendCommonResult(AGMSG_onredirectcall, OnFail, "the seat had not login");
    return 1;
  }

  memset(szSourDeviceID, 0, 64);
  memset(szDestDeviceID, 0, 64);
  //源頭分機
  if (AGMsg.GetAttrValue(2).GetLength() > 0 && AGMsg.GetAttrValue(2).Compare("0") != 0)
  {
    strncpy(szSourDeviceID, AGMsg.GetAttrValue(2).C_Str(), 63);
  }
  else if (AGMsg.GetAttrValue(3).GetLength() > 0 && AGMsg.GetAttrValue(3).Compare("0") != 0)
  {
    workerno = atoi(AGMsg.GetAttrValue(3).C_Str());
    if (workerno == 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "源工號不存在");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "方�籪�?�Z�I");
      else
        SendCommonResult(AGMSG_onredirectcall, OnFail, "");
      return 1;
    }
    nAG = pAgentMng->GetnAGByWorkerNo(workerno);
    if (!pAgentMng->isnAGAvail(nAG))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "源工號不存在");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "方�籪�?�Z�I");
      else
        SendCommonResult(AGMSG_onredirectcall, OnFail, "");
      return 1;
    }
    else
    {
      strncpy(szSourDeviceID, pAG->m_Seat.SeatNo.C_Str(), 63);
    }
  }
  //目的分機
  if (AGMsg.GetAttrValue(4).GetLength() > 0 && AGMsg.GetAttrValue(4).Compare("0") != 0)
  {
    strncpy(szDestDeviceID, AGMsg.GetAttrValue(4).C_Str(), 63);
  }
  else if (AGMsg.GetAttrValue(5).GetLength() > 0 && AGMsg.GetAttrValue(5).Compare("0") != 0)
  {
    workerno = atoi(AGMsg.GetAttrValue(5).C_Str());
    if (workerno == 0)
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "目的工號不存在");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "?�Y�籪�?�Z�I");
      else
        SendCommonResult(AGMSG_onredirectcall, OnFail, "");
      return 1;
    }
    nAG1 = pAgentMng->GetnAGByWorkerNo(workerno);
    if (!pAgentMng->isnAGAvail(nAG1))
    {
      if (pAGn->m_Seat.SeatLangID == 1)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "目的工號不存在");
      else if (pAGn->m_Seat.SeatLangID == 2)
        SendCommonResult(AGMSG_onredirectcall, OnFail, "?�Y�籪�?�Z�I");
      else
        SendCommonResult(AGMSG_onredirectcall, OnFail, "");
      return 1;
    }
    else
    {
      if (pAG1->svState != AG_SV_IDEL)
      {
        if (pAGn->m_Seat.SeatLangID == 1)
          SendCommonResult(AGMSG_onredirectcall, OnFail, "目的分機正忙");
        else if (pAGn->m_Seat.SeatLangID == 2)
          SendCommonResult(AGMSG_onredirectcall, OnFail, "?�Y?訣?Γ");
        else
          SendCommonResult(AGMSG_onredirectcall, OnFail, "");
        return 1;
      }
      strncpy(szDestDeviceID, pAG1->m_Seat.SeatNo.C_Str(), 63);
    }
  }
  if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
  {
    Send_SWTMSG_redirectcall(0, szSourDeviceID, 0, 0, szDestDeviceID);
    return 0;
  }
  
  //板卡版本
  nQue = pAG->nQue;
  if (!pCalloutQue->isnQueAvail(nQue))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onredirectcall, OnFail, "源分機沒有電話呼入");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onredirectcall, OnFail, "方?訣⊿Τ?杠㊣��");
    else
      SendCommonResult(AGMSG_onredirectcall, OnFail, "");
    return 1;
  }
  //取消以前的呼叫
  nAcd = pQue->nAcd;
  nChn = pQue->OutnChn;
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    if (pAGn->m_Seat.SeatLangID == 1)
      SendCommonResult(AGMSG_onredirectcall, OnFail, "源分機沒有電話呼入");
    else if (pAGn->m_Seat.SeatLangID == 2)
      SendCommonResult(AGMSG_onredirectcall, OnFail, "方?訣⊿Τ?杠㊣��");
    else
      SendCommonResult(AGMSG_onredirectcall, OnFail, "");
    return 1;
  }

  //取消源頭坐席的呼叫
  pAG->nQue = 0xFFFF;
  if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
  {
    pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
    pAG->m_Seat.nChn = 0xFFFF;
  }
  CancelCallout(nChn);
  SendStopACDCallIn(nAG);
  WriteSeatStatus(pAG, AG_STATUS_IDEL, 0); //代接標志
  SetAgentsvState(nAG, AG_SV_IDEL);
  pQue->state = 0;

  //呼叫新的坐席
  pAcd->acdState = 0;
  pAcd->waittimer = 0;
  pAcd->ringtimer = 0;

  pAcd->ACDRule.SeatNo = szDestDeviceID;
  pAcd->ACDRule.QueueTime = time(0);
  pAcd->ACDRule.StartRingTime = time(0);

  return 0;
}

//設置外出值班電話
int Proc_AGMSG_setgoouttel(CXMLRcvMsg &AGMsg)
{
  int flag;
  
  if (!pAGn->isLogin())
    return 1;
  flag = atoi(AGMsg.GetAttrValue(2).C_Str()); //0-取消 1-設置
  pAGn->m_Worker.SetLeave(0,"");
  pAGn->m_Worker.SetGoOutDutyTel(flag, AGMsg.GetAttrValue(3));
  SendCommonResult(AGMSG_onsetgoouttel, flag, AGMsg.GetAttrValue(3).C_Str());
  DispAgentStatus(nAGn);
  return 0;
}

//查詢IVR運行狀態
int Proc_AGMSG_queryivrstatus(CXMLRcvMsg &AGMsg)
{
  SendRunStatusToClient(AGMsg.GetClientId(), 0x3031);
  return 0;
}

//-----------------------------------------------------------------------------
//自定義一個函數指針類型
typedef int (*Proc_AgentMSG)(CXMLRcvMsg &AGMsg);
Proc_AgentMSG proc_agentmsg[]=
{
  Proc_AGMSG_seatlogin, //電腦坐席登錄
  Proc_AGMSG_workerlogin, //話務員登錄
  Proc_AGMSG_workerlogout, //話務員退出
  
  Proc_AGMSG_disturb, //免打擾設置
  Proc_AGMSG_leval, //離席/在席設置
  Proc_AGMSG_setforward, //設置呼叫轉移
  Proc_AGMSG_cancelforward, //取消呼叫轉移
  
  Proc_AGMSG_answercall, //應答來話
  Proc_AGMSG_makecall, //發起呼叫
  Proc_AGMSG_hangon, //坐席掛機
  
  Proc_AGMSG_blindtrancall, //快速轉接電話
  Proc_AGMSG_consulttrancall, //協商轉接電話
  Proc_AGMSG_conftrancall, //會議轉接電話
  Proc_AGMSG_stoptrancall, //停止轉接
  Proc_AGMSG_confspeak, //指定通道會議發言
  Proc_AGMSG_confaudit, //指定通道旁聽會議
  
  Proc_AGMSG_tranivr, //轉接ivr流程
  
  Proc_AGMSG_senddtmf, //發送DTMF按鍵
  Proc_AGMSG_play, //播放語音文件
  Proc_AGMSG_stopplay, //停止放音

  Proc_AGMSG_pickup, //代接,代接指定的電話
  Proc_AGMSG_pickupqueuecall, //搶接ACD隊列里的呼叫
  Proc_AGMSG_takeover, //接管其他坐席的電話
  Proc_AGMSG_listen, //監聽
  Proc_AGMSG_insert, //強插通話

  Proc_AGMSG_hold, //保持/取消保持
  Proc_AGMSG_mute, //靜音/取消靜音
  Proc_AGMSG_releasecall, //釋放一方通話,
  
  Proc_AGMSG_forceclear, //強拆其他坐席
  Proc_AGMSG_forcelogout, //強制將其他話務員退出
  Proc_AGMSG_forcedisturb, //強制其他坐席免打擾
  Proc_AGMSG_forceready, //強制其他坐席空閑
  
  Proc_AGMSG_getqueueinfo, //取ACD分配隊列信息
  Proc_AGMSG_getagentstatus, //取所有坐席的狀態信息
  Proc_AGMSG_getchnstatus, //取所有通道的狀態信息

  Proc_AGMSG_sendmsgtoflw, //發送消息到流程
  Proc_AGMSG_sendfax, //發送傳真
  Proc_AGMSG_recvfax, //接收傳真,通話后接收傳真
  Proc_AGMSG_sendmessage, //發送文字信息
  Proc_AGMSG_sendsms, //發送短信

  Proc_AGMSG_createconf, //創建會議
  Proc_AGMSG_destroyconf, //釋放會議
  
  Proc_AGMSG_setacwtimer, //設置事后處理時長(秒)
  Proc_AGMSG_setacwend, //設置事后處理完成

  Proc_AGMSG_swaphold, //穿梭保持通話
  
  Proc_AGMSG_setgroupname,	//設置技能組名稱
  Proc_AGMSG_startrecord,	//開始錄音
  Proc_AGMSG_stoprecord,	//停止錄音
  Proc_AGMSG_setcalldirection,	//設置呼叫方向(0-雙向服務 1-呼入 2-呼出)

  Proc_AGMSG_pickupex,	//代接電話擴展指令
  Proc_AGMSG_redirectcall,	//電話重新定向振鈴

  Proc_AGMSG_setgoouttel, //設置外出值班電話

  Proc_AGMSG_queryivrstatus, //查詢IVR運行狀態
};
//處理AGENT消息
void ProcAGMsg(CXMLRcvMsg &AGMsg)
{
  Get_AGMSG_HeaderValue(AGMsg);
  if (pAgentMng->isnAGAvail(nAGn) || AGMsg.GetMsgId() == 0)
    proc_agentmsg[AGMsg.GetMsgId()](AGMsg);
}
//-----------------------------------------------------------------------------
void ProcWebConnected(const char *pszDialMsg)
{
  char szWSClientId[64];
  char szResultMsg[512];
  int nAG;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  if (strlen(szWSClientId) == 0)
    return;
  
  nAG = pAgentMng->GetnAGBySeatNo(szWSClientId);
  if (pAgentMng->isnAGAvail(nAG))
  {
    sprintf(szResultMsg, "wsclientid=%s;cmd=connected", szWSClientId);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    SendRunStatusToWSClient(szWSClientId);
  }
}

void ProcWebDisconnected(const char *pszDialMsg)
{
  char szWSClientId[64];
  int nAG, nQue;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  if (strlen(szWSClientId) == 0)
    return;
  
  nAG = pAgentMng->GetnAGByWSClientId(szWSClientId);
  if (pAgentMng->isnAGAvail(nAG))
  {
    memset(pAG->m_Seat.WSClientId, 0, 64);
    
    if (pAG->GetClientId() != 0x06FF)
      return;
    
    nQue = pAG->nQue;
    if (pCalloutQue->isnQueAvail(nQue) && pAG->isWantSeatType(SEATTYPE_AG_PC))
    {
      pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
    }
    /*
    if (pAG->m_Worker.DisturbId != 3)
    {
      if (pAG->m_Worker.WorkerCallCountParam.CurStatus != AG_STATUS_LOGOUT)
      {
        WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
        WriteWorkerLogoutStatus(pAG);
      }
      if (pAG->m_Worker.oldWorkerNo == 0)
      {
        pAG->m_Worker.Logout();
        pAG->m_Worker.ClearState();
      }
      else
      {
        pAG->m_Worker.OldWorkerLogin();
      }
    }
    DispAgentStatus(nAG);*/
    //2015-12-14
    if (pAG->m_Worker.DisturbId == 3)
    {
      SendCommonResult(AGMSG_onworkerlogout, OnSuccess, "");
      return;
    }
    if (pAG->m_Worker.LeaveId > 0)
      WriteSeatStatus(pAG, AG_STATUS_LOGOUT, pAG->m_Worker.LeaveId);
    else
      WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
    WriteWorkerLogoutStatus(pAG);
    if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1)
    {
      Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_LOG_OUT);
    }

    //2016-07-04 保存以前的免打擾狀態值
    if (pAG->m_Worker.DisturbId == 2)
    {
      pAG->m_Worker.PreDisturbId = pAG->m_Worker.DisturbId;
      pAG->timer = 0;
    }

    if (pAG->m_Worker.oldWorkerNo == 0)
    {
      pAG->m_Worker.Logout();
      pAG->m_Worker.ClearState();
    }
    else
    {
      pAG->m_Worker.OldWorkerLogin();
    }
    pAG->m_Worker.OldDisturbId = 0;
    pAG->m_Worker.OldLeaveId = 0;
    AgentStatusCount(); //2015-12-14
    SendOneGroupStatusMeteCount(pAG->nAG);
    for (int i=1; i<MAX_GROUP_NUM_FORWORKER; i++)
      SendOneGroupStatusMeteCountB(pAG->m_Worker.PreGroupNoList[i]);
    SendSystemStatusMeteCount();

    DispAgentStatus(nAG);
  }
}


//處理web話務員注冊消息: wsclientid=web客戶端id一般為分機號;cmd=login;seatno=分機號;workerno=工號;workername=姓名;groupno=組號;seatip=坐席ip;state=登錄初始狀態（0-示閑，1-示忙）
//處理web話務員注冊結果: wsclientid=web客戶端id一般為分機號;cmd=loginresult;seatno=分機號;workerno=工號;workername=姓名;groupno=組號;seatip=坐席ip;result=注冊結果(0-成功,1-失敗)
int ProcWebLogin(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szSeatIP[32];
  char szWorkerNo[32];
  char szPassword[64];
  char szWorkerName[64], szAnsWorkerName[64];
  char szGroupNo[64], szSkillLevel[64];
  char szGroupName[256], szAnsGroupName[256];
  char szResultMsg[512];
  int  nAG, nWorkerNo, nState, nGrade, nAcwTimer, nGetAGState, nAuthType;
  CStringX ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER], ArrString3[MAX_GROUP_NUM_FORWORKER];

  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    nAuthType = GetParamIntValue("authtype=", pszDialMsg);
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nWorkerNo = GetParamIntValue("workerno=", pszDialMsg);
    memset(szPassword, 0, 64);
    strncpy(szPassword, GetParamStrValue("password=", pszDialMsg), 63);

    if (nAuthType == 1)
    {
      //通過IVR后臺認證話務員資料
      if (pIVRCfg->isCheckPasswordWebLogin == true)
        sprintf(szResultMsg, "select * from tbworker where workerno='%s' and Password='%s'", szWorkerNo, szPassword);
      else
        sprintf(szResultMsg, "select * from tbworker where workerno='%s'", szWorkerNo);
      
      if (pSQLSfifo->Write(szSeatNo, szWorkerNo, szResultMsg, pszDialMsg, 1) == 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=loginresult;seatno=%s;result=1;getagstate=0;reason=query buffer is overflow", szWSClientId, szSeatNo);
        SendMsg2WS(WebSocketClientId, 1, szResultMsg);
      }
      return 0;
    }
    else if (nAuthType == 3)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=loginresult;seatno=%s;result=1;getagstate=0;reason=the workerno is not exist or the password is invalid", szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 1, szResultMsg);
      return 0;
    }
    
    memset(szWorkerName, 0, 64);
    strncpy(szWorkerName, GetParamStrValue("workername=", pszDialMsg), 63);
    memset(szGroupNo, 0, 64);
    strncpy(szGroupNo, GetParamStrValue("groupno=", pszDialMsg), 63);
    memset(szSkillLevel, 0, 64);
    strncpy(szSkillLevel, GetParamStrValue("skilllevel=", pszDialMsg), 63);
    memset(szGroupName, 0, 256);
    strncpy(szGroupName, GetParamStrValue("groupname=", pszDialMsg), 255);
    memset(szSeatIP, 0, 32);
    strncpy(szSeatIP, GetParamStrValue("seatip=", pszDialMsg), 31);
    nState = GetParamIntValue("busystate=", pszDialMsg);
    nGrade = GetParamIntValue("grade=", pszDialMsg);
    nAcwTimer = GetParamIntValue("acwtimer=", pszDialMsg);
    nGetAGState = GetParamIntValue("getagstate=", pszDialMsg);

    if (nWorkerNo == pAG->m_Worker.PreWorkerNo)
      WriteWorkerLogoutRecord(pAG);
    
    pAG->m_Worker.ClearWorkerParam();

    UTF8_ANSI_Convert(szWorkerName, szAnsWorkerName, CP_UTF8, CP_ACP);
    pAG->m_Worker.WorkerLogin(nWorkerNo, szAnsWorkerName, nGrade);

    strcpy( pAG->m_Worker.UnicodeWorkerName, szWorkerName);
    pAG->m_Seat.SetSeatIP(szSeatIP);
    strcpy(pAG->m_Seat.WSClientId, szWSClientId);

    pAG->m_Worker.GetAGState = nGetAGState;

    UTF8_ANSI_Convert(szGroupName, szAnsGroupName, CP_UTF8, CP_ACP);
    int groupnum = SplitTxtLine(szGroupNo, MAX_GROUP_NUM_FORWORKER, ArrString1);
    SplitTxtLine(szAnsGroupName, MAX_GROUP_NUM_FORWORKER, ArrString2);
    SplitTxtLine(szSkillLevel, MAX_GROUP_NUM_FORWORKER, ArrString3);
    for (int i=0; i<groupnum; i++)
    {
      int groupno = atoi(ArrString1[i].C_Str());
      if (groupno > 0 && groupno < MAX_AG_GROUP)
      {
        if (ArrString3[i].GetLength() > 0)
          pAG->m_Worker.SetWorkerGroupNo(groupno, atoi(ArrString3[i].C_Str()));
        else
          pAG->m_Worker.SetWorkerGroupNo(groupno, 1);
        pAgentMng->WorkerGroup[groupno].state = 1;

        if (ArrString2[i].GetLength() > 0)
          pAgentMng->WorkerGroup[groupno].GroupName = ArrString2[i];
      }
    }

    pAG->m_Worker.WorkerCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
    pAG->m_Worker.WorkerCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
    pAG->m_Worker.WorkerCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長
    
    pAG->m_Seat.SeatCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
    pAG->m_Seat.SeatCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
    pAG->m_Seat.SeatCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
    pAG->m_Seat.SeatCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長

    pAG->ChangeStatus(AG_STATUS_LOGIN);
    WriteWorkerLoginStatus(pAG);

    if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginSwitch == true 
      && pAG->m_Seat.LoginSwitchID == 1)
    {
      Send_SWTMSG_setagentstatus(0, szSeatNo, szWorkerNo, pAG->m_Seat.LoginSwitchGroupID, pAG->m_Seat.LoginSwitchPSW, AG_AM_LOG_IN);
    }

    sprintf(szResultMsg, "wsclientid=%s;cmd=loginresult;seatno=%s;workerno=%s;workername=%s;groupno=%s;result=0;getagstate=%d;", 
      szWSClientId, szSeatNo, szWorkerNo, szWorkerName, szGroupNo, nGetAGState);
    SendMsg2WS(WebSocketClientId, 1, szResultMsg);

    //2015-07-04增加登錄時初始狀態
    if (nState == 1)
    {
      pAG->m_Worker.DisturbId = 1;
      pAG->m_Worker.LeaveId = 0;
    }
    sprintf(szResultMsg, "wsclientid=%s;cmd=disturbstate;seatno=%s;state=%d", 
      szWSClientId, szSeatNo, pAG->m_Worker.DisturbId);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    
    if (pAG->m_Worker.DisturbId == 0 && pAG->m_Worker.LeaveId == 0)
    {
      pAG->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
    }
    else
    {
      pAG->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_BUSY);
    }

    int nChn = pAG->GetSeatnChn();
    if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1)
    {
      Send_SWTMSG_setagentstatus(nChn, pAG->GetSeatNo().C_Str(), szWorkerNo, pAG->m_Seat.LoginSwitchGroupID, pAG->m_Seat.LoginSwitchPSW, AG_AM_LOG_IN);
    }
    if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginACDSplit == true)
    {
      Send_SWTMSG_setagentstatus(nChn, pAG->GetSeatNo().C_Str(), szWorkerNo, szGroupNo, "", AG_AM_LOG_IN_GROUP);
      strcpy(pAG->m_Seat.LoginSwitchGroupID, szGroupNo);
    }
    if (g_nSwitchType > 0 && pIVRCfg->isSetREADYonAgentLogin == true) //2014-10-15
    {
      if (pAG->m_Worker.DisturbId == 0)
        Send_SWTMSG_setagentstatus(nChn, pAG->GetSeatNo().C_Str(), pAG->GetSeatNo().C_Str(), "", "", AG_AM_READY);
      else
        Send_SWTMSG_setagentstatus(nChn, pAG->GetSeatNo().C_Str(), pAG->GetSeatNo().C_Str(), "", "", AG_AM_NOT_READY);
    }
    if (nAcwTimer > 0)
      pAG->m_Worker.DelayTime = nAcwTimer;
    else
      pAG->m_Worker.DelayTime = g_nDefaultACWTimeLen;
    strcpy(pAG->StateChangeTime, MyGetNow());
    pAG->m_Worker.IdelTime = time(0);

    //2015-12-14
    pAG->m_Worker.PreWorkerNo = nWorkerNo;
    pAG->m_Worker.PreGroupNo = pAG->GetGroupNo();
    memcpy(pAG->m_Worker.PreGroupNoList, pAG->m_Worker.GroupNo, sizeof(pAG->m_Worker.GroupNo));
    pAG->m_Worker.PreDutyNo = 0;

    AgentStatusCount();
    SendOneGroupStatusMeteCount(pAG->nAG);
    SendSystemStatusMeteCount();
    //end
    
    //2016-07-04 恢復以前的免打擾狀態值
    if (pAG->m_Worker.PreDisturbId == 2)
    {
      pAG->m_Worker.DisturbId = pAG->m_Worker.PreDisturbId;
      pAG->m_Worker.PreDisturbId = 0;
    }
    DispAgentStatus(nAG);

    if (pAG->m_Worker.GetAGState == 1 && (pIVRCfg->isOnlySendMySeatStatus == false || pAG->m_Worker.WorkerGrade > 2))
    {
      SendAllAGStateToOne(nAG);
    }

    return 0;
  }
  sprintf(szResultMsg, "wsclientid=%s;cmd=loginresult;seatno=%s;result=1;getagstate=0;reason=the seatno is not exist", szWSClientId, szSeatNo);
  SendMsg2WS(WebSocketClientId, 1, szResultMsg);

  return 1;
}
//處理web話務員外出值班消息: wsclientid=web客戶端id一般為分機號;cmd=legwork;seatno=分機號;phoneno=外出值班電話;
int ProcWebLegwork(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szPhoneNo[32];
  char szResultMsg[512];
  int nAG;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    memset(szPhoneNo, 0, 32);
    strncpy(szPhoneNo, GetParamStrValue("phoneno=", pszDialMsg), 31);
    strcpy(pAG->m_Seat.WSClientId, szWSClientId);
    if (strlen(szPhoneNo) > 0)
      pAG->m_Worker.SetGoOutDutyTel(1, szPhoneNo);
    else
      pAG->m_Worker.SetGoOutDutyTel(0, "");
    pAG->m_Worker.GetAGState = 0;

    sprintf(szResultMsg, "wsclientid=%s;cmd=legworkresult;seatno=%s;result=0", szWSClientId, szSeatNo);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    return 0;
  }
  sprintf(szResultMsg, "wsclientid=%s;cmd=legworkresult;seatno=%s;result=1", szWSClientId, szSeatNo);
  SendMsg2WS(WebSocketClientId, 0, szResultMsg);
  return 1;
}

//處理web話務員退出消息: cmd=logout,seatno=分機號
int ProcWebLogOut(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32], szWorkerNo[32];
  char szResultMsg[512];
  int nAG, nQue;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szSeatNo) == 0)
  {
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(szWorkerNo));
  }
  else
  {
    nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, szSeatNo);
  }
  
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, pAG->GetSeatNo().C_Str());

    nQue = pAG->nQue;
    if (pCalloutQue->isnQueAvail(nQue) && pAG->isWantSeatType(SEATTYPE_AG_PC))
    {
      pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
    }
    if (pAG->m_Worker.DisturbId == 3)
    {
      SendCommonResult(AGMSG_onworkerlogout, OnSuccess, "");
      return 0;
    }
    if (pAG->m_Worker.LeaveId > 0)
      WriteSeatStatus(pAG, AG_STATUS_LOGOUT, pAG->m_Worker.LeaveId);
    else
      WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
    WriteWorkerLogoutStatus(pAG);
    if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1)
    {
      Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_LOG_OUT);
    }

    //2016-07-04 保存以前的免打擾狀態值
    if (pAG->m_Worker.DisturbId == 2)
    {
      pAG->m_Worker.PreDisturbId = pAG->m_Worker.DisturbId;
      pAG->timer = 0;
    }
    
    if (pAG->m_Worker.oldWorkerNo == 0)
    {
      pAG->m_Worker.Logout();
      pAG->m_Worker.ClearState();
    }
    else
    {
      pAG->m_Worker.OldWorkerLogin();
    }
    pAG->m_Worker.OldDisturbId = 0;
    pAG->m_Worker.OldLeaveId = 0;
    AgentStatusCount(); //2015-12-14
    SendOneGroupStatusMeteCount(pAG->nAG);
    for (int i=1; i<MAX_GROUP_NUM_FORWORKER; i++)
      SendOneGroupStatusMeteCountB(pAG->m_Worker.PreGroupNoList[i]);
    SendSystemStatusMeteCount();
    DispAgentStatus(nAG);

    sprintf(szResultMsg, "wsclientid=%s;cmd=logoutresult;seatno=%s;result=0", szWSClientId, szSeatNo);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);

    return 0;
  }
  sprintf(szResultMsg, "wsclientid=%s;cmd=logoutresult;seatno=%s;result=1", szWSClientId, szSeatNo);
  SendMsg2WS(WebSocketClientId, 0, szResultMsg);

  return 1;
}

//處理web外呼消息: cmd=dialout;seatno=分機號;workerno=工號;calledno=被叫號;param=附帶參數(CRMCALLID:)
int ProcWebDialOut(const char *pszDialMsg, int SendResult)
{
  int nAG, nChn, len, calledtype=1;
  char szWSClientId[64];
  char szSeatNo[32], szWorkerNo[32], szCalledNo[32], szCalledType[32], szParam[256], szPreCode[32]; //2016-07-31 增加傳遞外呼字冠判斷
  char szResultMsg[512];

  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szSeatNo) == 0)
  {
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(szWorkerNo));
  }
  else
  {
    nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, szSeatNo);
  }

  if (pAgentMng->isnAGAvail(nAG))
  {
    if (pAG->isIdelForDialOut() == false)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=85", szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, pAG->GetSeatNo().C_Str());

    memset(szCalledNo, 0, 32);
    memset(szCalledType, 0, 32);
    memset(szPreCode, 0, 32);
    memset(szParam, 0, 256);
    strncpy(szCalledNo, GetParamStrValue("calledno=", pszDialMsg), 31);
    strncpy(szCalledType, GetParamStrValue("calledtype=", pszDialMsg), 31);
    strncpy(szPreCode, GetParamStrValue("precode=", pszDialMsg), 31); //2016-07-31 增加傳遞外呼字冠判斷
    ClearCharInCalledNo(szCalledNo); //2015-01-07 清除號碼里的非法字符
    strncpy(szParam, GetParamStrValue("param=", pszDialMsg), 255);
    if (strlen(szCalledType) > 0)
      calledtype = atoi(szCalledType);

    if (strlen(szCalledNo) == 0)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=85", szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=85", szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }
    if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
    {
      if (strlen(szCalledNo) < 5)
        Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, szSeatNo, szCalledNo, 0, szParam, 2);
      else
      {
        //2016-11-18 對呼出號碼重新進行了判斷
        if (strlen(szPreCode) > 0)
        {
          Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, szSeatNo, szCalledNo, 0, szParam, 0, szPreCode);
        }
        else
        {
          if (calledtype == 0)
            Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, szSeatNo, szCalledNo, 0, szParam, 0);
          else
            Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, szSeatNo, szCalledNo, 0, szParam, 1);
        }
      }
      sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;calledno=%s;result=80", 
        szWSClientId, szSeatNo, szCalledNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
    if (g_nCardType == 9)
    {
      pChn->timer = 0;
      pChn->ssState = CHN_SNG_IDLE;
      pChn->hwState = CHN_HW_VALID;
      pChn->lnState = CHN_LN_SEIZE; //占用
      pChn->CallInId = 1; //有呼叫進入
      pChn->CallInOut = CALL_OUT;

      strcpy(pChn->CallerNo, szSeatNo);
      strcpy(pChn->CalledNo, szCalledNo);
      len = strlen(pIVRCfg->DialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      DispChnStatus(nChn);
      sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;calledno=%s;result=80", 
        szWSClientId, szSeatNo, szCalledNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
    int chState = GetAgentChState(pAG);
    if (pAG->isWantSeatType(SEATTYPE_AG_TEL) || pAG->isWantSeatType(SEATTYPE_DT_TEL))
    {
      //電話坐席
      if (chState == CHN_SNG_IDLE)
      {
        if (pAG->m_Seat.SeatLangID == 1)
          SendSeatMakeCallFailResult(nAG, szParam, OnOutFail, "請先摘機");
        else if (pAG->m_Seat.SeatLangID == 2)
          SendSeatMakeCallFailResult(nAG, szParam, OnOutFail, "叫�g篕訣");
        else
          SendSeatMakeCallFailResult(nAG, szParam, OnOutFail, "please hangoff");
        sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=85", szWSClientId, szSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      else if (chState == CHN_SNG_IN_ARRIVE || chState == CHN_SNG_IN_WAIT)
      {
        //坐席通道處于撥號狀態
        //被叫類別: 0-外線呼入號碼(外線有效) 1-呼出的外線號碼 2-呼叫的內線坐席號 3-呼叫的坐席工號
        strcpy(pChn->CallerNo, szSeatNo);
        strcpy(pChn->CalledNo, szCalledNo);
        strcpy(pChn->CustPhone, szCalledNo);
        pChn->CallData = szParam;
        DispChnStatus(nChn);
        sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;calledno=%s;result=80", 
          szWSClientId, szSeatNo, szCalledNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 0;
      }
    }
    else if (pAG->isWantSeatType(SEATTYPE_AG_PC) || pAG->isWantSeatType(SEATTYPE_DT_PC))
    {
      //電腦坐席
      if (pAG->isIdelForSrv() == false)
      {
        if (pAG->m_Seat.SeatLangID == 1)
          SendSeatMakeCallFailResult(nAG, szParam, OnOutFail, "本坐席電話正忙");
        else if (pAG->m_Seat.SeatLangID == 2)
          SendSeatMakeCallFailResult(nAG, szParam, OnOutFail, "????杠?Γ");
        else
          SendSeatMakeCallFailResult(nAG, szParam, OnOutFail, "");
        sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=85", szWSClientId, szSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
  
      if (chState == CHN_SNG_IDLE)
      {
        strcpy(pChn->CallerNo, szSeatNo);
        strcpy(pChn->CalledNo, szCalledNo);
        strcpy(pChn->CustPhone, szCalledNo);
        pChn->CallData = szParam;

        pChn->CallInOut = CALL_OUT;
        pChn->CallOutRouteNo = 1;
        pChn->GenerateCdrSerialNo();
        SendCallinEvent(nChn, 1, 1, szParam, 1);
        DBInsertCallCDR(nChn, 1, 0);
        DBUpdateCallCDR_AnsWorkerNo(nChn, nAG);
        strcpy(pAG->CdrSerialNo, pChn->CdrSerialNo); //2012-08-30 for sqcw
        DispChnStatus(nChn);
        sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;calledno=%s;cdrserialno=%s;result=80", 
          szWSClientId, szSeatNo, szCalledNo, pChn->CdrSerialNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 0;
      }
      else if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
      {
        strcpy(pChn->CallerNo, szSeatNo);
        strcpy(pChn->CalledNo, szCalledNo);
        strcpy(pChn->CustPhone, szCalledNo);
        pChn->GenerateCdrSerialNo();
        SendCallinEvent(nChn, 1, 1, szParam);
        DispChnStatus(nChn);
        SetAgentsvState(nAG, AG_SV_OUTSEIZE);
        sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;calledno=%s;cdrserialno=%s;result=80", 
          szWSClientId, szSeatNo, szCalledNo, pChn->CdrSerialNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 0;
      }
    }
  }
  sprintf(szResultMsg, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=85", szWSClientId, szSeatNo);
  SendMsg2WS(WebSocketClientId, 0, szResultMsg);
  return 1;
}

//處理示忙/示閑消息
int ProcWebSetDisturb(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32], szWorkerNo[32];
  char szResultMsg[512];
  int nAG, flag;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szSeatNo) == 0)
  {
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(szWorkerNo));
  }
  else
  {
    nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, szSeatNo);
  }
  
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, pAG->GetSeatNo().C_Str());
    
    if (!pAG->isLogin())
      return 1;
    if (pAG->isIdelForSrv() == false)
    {
      return 1;
    }

    flag = GetParamIntValue("flag=", pszDialMsg);
    if (pAG->m_Worker.DisturbId == 3)
      SendCommonResult(nAG, AGMSG_onsetgoouttel, 0, "");
    
    if (flag == 0)
    {
      if (pAG->m_Worker.LeaveId > 0)
        WriteSeatStatus(pAG, AG_STATUS_IDEL, pAG->m_Worker.LeaveId);
      else
        WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
      
      sprintf(szResultMsg, "wsclientid=%s;cmd=disturbstate;seatno=%s;state=%d", 
        szWSClientId, szSeatNo, flag);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      
      pAG->m_Worker.SetDisturb(flag);
      pAG->m_Worker.SetLeave(0,"");
      if (pAG->m_Worker.WorkerCallCountParam.BusyCount > 0)
        pAG->m_Worker.WorkerCallCountParam.BusyTimeAvgLen = pAG->m_Worker.WorkerCallCountParam.BusyTimeSumLen/pAG->m_Worker.WorkerCallCountParam.BusyCount;
      if (g_nSwitchType > 0 && ((pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1) || pIVRCfg->isSetReadyStateonAgentSetDisturb > 0))
      {
        Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_READY);
      }
    }
    else
    {
      if (pAG->m_Worker.LeaveId > 0)
      {
        return 1; //小休時不可以示忙 //2013-08-01 add
      }
      pAG->m_Worker.SetDisturb(flag);
      sprintf(szResultMsg, "wsclientid=%s;cmd=disturbstate;seatno=%s;state=%d", 
        szWSClientId, szSeatNo, flag);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      pAG->m_Worker.SetLeave(0,"");
      pAG->m_Worker.WorkerCallCountParam.BusyCount++;
      WriteSeatStatus(pAG, AG_STATUS_BUSY, 0);
      if (g_nSwitchType > 0 && ((pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1) || pIVRCfg->isSetReadyStateonAgentSetDisturb > 0))
      {
        Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
      }
    }
//     sprintf(szResultMsg, "wsclientid=%s;cmd=levalstate;seatno=%s;state=%d;reason=", 
//       szWSClientId, szSeatNo, 0);
//     SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    AgentStatusCount(); //2015-12-14
    SendOneGroupStatusMeteCount(pAG->nAG);
    SendSystemStatusMeteCount();

    DispAgentStatus(nAG);
    return 0;
  }
  return 1;
}

//處理小休離席消息
int ProcWebSetLeave(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32], szWorkerNo[32];
  char szResultMsg[512];
  char szReason[64], szAnsReason[128];
  int nAG, flag;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szSeatNo) == 0)
  {
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(szWorkerNo));
  }
  else
  {
    nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, szSeatNo);
  }
  
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, pAG->GetSeatNo().C_Str());
    
    if (!pAG->isLogin())
      return 1;
    if (pAG->isIdelForSrv() == false)
    {
      return 1;
    }
    if (pAG->m_Worker.DisturbId == 1)
    {
      return 1; //示忙時不可以離席/在席設置 //2013-08-01 add
    }
    flag = GetParamIntValue("flag=", pszDialMsg);
    if (flag == 0)
    {
      if (pAG->m_Worker.LeaveId == 0)
      {
        return 1; //已經是在席時不能再按
      }
      WriteSeatStatus(pAG, AG_STATUS_IDEL, pAG->m_Worker.LeaveId);
      if (pAG->m_Worker.WorkerCallCountParam.LeaveCount > 0)
        pAG->m_Worker.WorkerCallCountParam.LeaveTimeAvgLen = pAG->m_Worker.WorkerCallCountParam.LeaveTimeSumLen/pAG->m_Worker.WorkerCallCountParam.LeaveCount;
      
      if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1)
      {
        Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_READY);
      }
    }
    else
    {
      pAG->m_Worker.WorkerCallCountParam.LeaveCount++;
      if (pAG->m_Worker.LeaveId > 0)
        WriteSeatStatus(pAG, AG_STATUS_LEAVE, pAG->m_Worker.LeaveId); //2013-08-01 add
      else
        WriteSeatStatus(pAG, AG_STATUS_LEAVE, 0);
      
      if (g_nSwitchType > 0 && pIVRCfg->isAgentLoginSwitch == true && pAG->m_Seat.LoginSwitchID == 1)
      {
        Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
      }
    }
    memset(szReason, 0, 64);
    strncpy(szReason, GetParamStrValue("reason=", pszDialMsg), 63);

//     if (UrlDecode(szReason, szAnsReason, strlen(szReason)) == TRUE)
//       pAG->m_Worker.SetLeave(flag, szAnsReason);
//     else
//       pAG->m_Worker.SetLeave(flag, szReason);
    UTF8_ANSI_Convert(szReason, szAnsReason, CP_UTF8, CP_ACP);
    pAG->m_Worker.SetLeave(flag, szAnsReason);
    strcpy( pAG->m_Worker.UnicodeLeaveReason, szReason);

    AgentStatusCount(); //2015-12-14
    SendOneGroupStatusMeteCount(pAG->nAG);
    SendSystemStatusMeteCount();

    sprintf(szResultMsg, "wsclientid=%s;cmd=leavestate;seatno=%s;state=%d;reason=%s", 
      szWSClientId, szSeatNo, flag, szReason);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    DispAgentStatus(nAG);
    return 0;
  }
  return 1;
}

//處理應答來話消息
int ProcWebAnswerCall(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32], szWorkerNo[32];
  char szResultMsg[512];
  int nAG, nChn, flag, nQue, chState;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szSeatNo) == 0)
  {
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(szWorkerNo));
  }
  else
  {
    nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, szSeatNo);
  }
  
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (strlen(szWSClientId) == 0)
      strcpy(szWSClientId, pAG->GetSeatNo().C_Str());

    if (pAG->isLogin() == false)
      return 1;
    
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    flag = GetParamIntValue("flag=", pszDialMsg);

    if (g_nSwitchType == 0)
    {
      nQue = pAG->nQue;
      if (!pCalloutQue->isnQueAvail(nQue))
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=onanswercall;seatno=%s;result=1", 
          szWSClientId, szSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      switch (flag)
      {
      case 0: //0-電腦坐席收到呼入正在振鈴
        if (pAG->isWantSeatType(SEATTYPE_AG_PC))
        {
          chState = GetAgentChState(pAG);
          if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
            pQue->CalloutResult = CALLOUT_RING;
        }
        else if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
        {
          chState = GetAgentChState(pAG);
          if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
            pQue->CalloutResult = CALLOUT_RING;
        }
        break;
      case 1: //1-應答來話
        chState = GetAgentChState(pAG);
        if (pAG->isWantSeatType(SEATTYPE_AG_PC))
        {
          if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
          {
            pQue->CalloutResult = CALLOUT_CONN; //應答成功結果在發送呼叫坐席應答結果的地方發送
          }
        }
        else if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
        {
          if (chState == CHN_SNG_IN_TALK || chState == CHN_SNG_OT_TALK)
          {
            pQue->CalloutResult = CALLOUT_CONN; //應答成功結果在發送呼叫坐席應答結果的地方發送
          }
        }
        break;
      case 2: //2-拒絕來話
        pQue->CalloutResult = CALLOUT_REFUSE;
        break;
      }
      return 0;
    }
    if (flag == 1)
    {
      Send_SWTMSG_answercall(nChn, pChn->DeviceID, pChn->ConnID);
    }
    else if (flag == 2) //2015-10-14 拒絕來話
    {
      int nAcd = pAG->nAcd;
      if (pACDQueue->isnAcdAvail(nAcd))
      {
        int nChn1 = pAcd->Session.nChn;
        if (pBoxchn->isnChnAvail(nChn1))
        {
          pAcd->Calleds[0].CalloutResult = CALLOUT_REFUSE;
          Send_SWTMSG_stoptransfer(nChn1, pChn1->DeviceID, pChn1->ConnID);
          return 0;
        }
      }
      sprintf(szResultMsg, "wsclientid=%s;cmd=onanswercall;seatno=%s;result=1", 
        szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    }
    return 0;
  }
  return 1;
}

//處理保持/取消保持消息
int ProcWebSetHold(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  int nAG, nChn, nChn1, nChn2, flag, nResult;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (pAG->isLogin() == false)
    {
      return 1;
    }
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      return 1;
    }
    flag = GetParamIntValue("flag=", pszDialMsg);
    if (g_nSwitchType == 0)
    {
      if (pAG->svState != AG_SV_CONN && pAG->svState != AG_SV_HOLD)
      {
        return 1;
      }
      if (pChn->TranStatus == 0)
      {
        //正常2方通話情況
        if (flag == 1)
        {
          //保持操作
          nChn1 = pChn->LinkChn[0];
          if ((pChn->LinkType[0] == 1 || pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7) && pBoxchn->isnChnAvail(nChn1))
          {
            UnRouterTalk(nChn, nChn1);
            pChn->LinkType[0] = 0;
            pChn->LinkChn[0] = 0xFFFF;
            pChn->HoldingnChn = nChn1;
            pChn1->LinkType[0] = 0;
            pChn1->LinkChn[0] = 0xFFFF;
            pChn1->HoldingnChn = nChn;
            if (SetDTMFPlayRule(nChn1, 0, MAX_PLAYRULE_BUF, ErrMsg) == 0)
              PlayFile(nChn1, pIVRCfg->WaitVocFile, 0, MAX_PLAYRULE_BUF); //循環放等待音
          
            WriteSeatStatus(pAG, AG_STATUS_HOLD, 0);
            pAG->SetAgentsvState(AG_SV_HOLD);
            DispAgentStatus(nAG);

            sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=1", 
              szWSClientId, szSeatNo);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
        }
        else
        {
          //取消保持
          nChn1 = pChn->HoldingnChn;
          if (pChn->LinkType[0] == 0 && pBoxchn->isnChnAvail(nChn1))
          {
            StopClearPlayDtmfBuf(nChn1);
            nResult = RouterTalk(nChn, nChn1);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn1;
              pChn->HoldingnChn = 0xFFFF;
            
              pChn1->RecMixerFlag = 1;
              pChn1->LinkType[0] = 1;
              pChn1->LinkChn[0] = nChn;
              pChn1->HoldingnChn = 0xFFFF;
            
              WriteSeatStatus(pAG, AG_STATUS_TALK, 0);
              pAG->SetAgentsvState(AG_SV_CONN);
              DispAgentStatus(nAG);
              
              sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=0", 
                szWSClientId, szSeatNo);
              SendMsg2WS(WebSocketClientId, 0, szResultMsg);
            }
          }
        }
      } 
      else if (pChn->TranStatus == 5)
      {
        nChn1 = pChn->TranControlnChn; //被轉接的通道
        nChn2 = pChn->TranDestionnChn; //轉接的目的通道
        if (pBoxchn->isnChnAvail(nChn1) && pBoxchn->isnChnAvail(nChn2))
        {
          if (nChn1 == pChn->LinkChn[0])
          {
            //正在與被轉接通道通話,則先斷開然后與轉接的目的通道通話
            UnRouterTalk(nChn, nChn1);
            pChn->LinkType[0] = 0;
            pChn->LinkChn[0] = 0xFFFF;
            pChn1->LinkType[0] = 0;
            pChn1->LinkChn[0] = 0xFFFF;
            PlayFile(nChn1, pIVRCfg->WaitVocFile, 0, MAX_PLAYRULE_BUF); //循環放等待音
          
            StopClearPlayDtmfBuf(nChn2);
            nResult = RouterTalk(nChn, nChn2);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn2;
            
              pChn2->RecMixerFlag = 1;
              pChn2->LinkType[0] = 1;
              pChn2->LinkChn[0] = nChn;
            }
          
            WriteSeatStatus(pAG, AG_STATUS_CONSULT, 0);
            pAG->SetAgentsvState(AG_SV_HOLD);
            DispAgentStatus(nAG);
            
            sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=1", 
              szWSClientId, szSeatNo);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
          else if (nChn2 == pChn->LinkChn[0])
          {
            //正在與轉接的目的通道通話,則先斷開然后與被轉接通道通話
            UnRouterTalk(nChn, nChn2);
            pChn->LinkType[0] = 0;
            pChn->LinkChn[0] = 0xFFFF;
            pChn2->LinkType[0] = 0;
            pChn2->LinkChn[0] = 0xFFFF;
            PlayFile(nChn2, pIVRCfg->WaitVocFile, 0, MAX_PLAYRULE_BUF); //循環放等待音
          
            StopClearPlayDtmfBuf(nChn1);
            nResult = RouterTalk(nChn, nChn1);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn1;
            
              pChn1->RecMixerFlag = 1;
              pChn1->LinkType[0] = 1;
              pChn1->LinkChn[0] = nChn;
            }
          
            WriteSeatStatus(pAG, AG_STATUS_TALK, 0);
            pAG->SetAgentsvState(AG_SV_CONN);
            DispAgentStatus(nAG);
            
            sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=0", 
              szWSClientId, szSeatNo);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
        }
      }
      return 0;
    }

    if (flag == 1)
    {
      Send_SWTMSG_holdcall(nChn, pChn->DeviceID, pChn->ConnID);
    } 
    else
    {
      Send_SWTMSG_unholdcall(nChn, pChn->DeviceID, pChn->ConnID);
    }
    return 0;
  }
  return 1;
}
//處理取消轉接再保持組合消息 2015-12-07
int ProcWebReHeld(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  //char szResultMsg[512];
  int nAG, nChn;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (pAG->isLogin() == false)
    {
      return 1;
    }
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
    {
      return 1;
    }
    if (g_nSwitchType == 0)
    {
      return 1;
    }
    
    Send_SWTMSG_sendswitchcmd(nChn, pChn->DeviceID, 7);
    //sprintf(szResultMsg, "wsclientid=%s;cmd=reheldresult;seatno=%s;result=9", 
    //  szWSClientId, szSeatNo);
    //SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    
    return 0;
  }
  return 1;
}

//處理掛機釋放消息
int ProcWebHangon(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32], szWorkerNo[32];
  char szResultMsg[512];
  int nAG, nChn;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szSeatNo) == 0)
  {
    memset(szWorkerNo, 0, 32);
    strncpy(szWorkerNo, GetParamStrValue("workerno=", pszDialMsg), 31);
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(szWorkerNo));
  }
  else
  {
    nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  }
  
  if (pAgentMng->isnAGAvail(nAG))
  {
    //if (!pAG->isLogin()) //2016-11-18 改為不判斷是否登錄都可以掛機
    //  return 1;
    nChn = pAG->GetSeatnChn();
    if (pAG->isThesvState(AG_SV_IDEL) == true)
    {
      Hangup(nChn); //2016-03-03 不管什么情況都發掛機給交換機，以免狀態不一致時不能掛機
      //2016-01-07
      sprintf(szResultMsg, "wsclientid=%s;cmd=onhangon;seatno=%s;result=0",
        szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }
    Release_Chn(nChn, 1, 1);
    return 0;
  }
  return 1;
}

//處理轉接電話消息
int ProcWebTransferCall(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char Phone[32];
  char TranParam[256];
  char szTranParam[256]; //2016-06-17
  char CalledNo[MAX_TELECODE_LEN];
  int i, nAG, nAG1, nChn, nChn1, trantype, phonetype, workerno;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  nAG1 = 0xFFFF;
  if (pAgentMng->isnAGAvail(nAG))
  {
    //if (!pAG->isLogin()) //2016-11-18 改為不判斷是否登錄都可以轉接
    //  return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    memset(TranParam, 0, 256);
    strncpy(TranParam, GetParamStrValue("tranparam=", pszDialMsg), 255);
    //2016-06-17傳遞轉接坐席時傳遞附加參數 $S_DialParam=...;
    if (strlen(TranParam) > 0)
    {
      sprintf(szTranParam, "$S_DialParam=%s;", TranParam);
    }
    //轉接的號碼
    memset(Phone, 0, 32);
    strncpy(Phone, GetParamStrValue("phone=", pszDialMsg), 31);
    //轉接類型 1-協商轉接 2-盲轉 3-轉接會議
    trantype = GetParamIntValue("trantype=", pszDialMsg);
    //轉接的號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
    phonetype = GetParamIntValue("phonetype=", pszDialMsg);

    if (g_nSwitchType == 0)
    {
      //板卡版本
      if (pChn->TranStatus != 0)
      {
        //該坐席正處于轉接狀態
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutFail, "seat is transfing");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      nChn1 = pChn->LinkChn[0];
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        nChn1 = pChn->HoldingnChn;
        if (!pBoxchn->isnChnAvail(nChn1))
        {
          //該坐席無可轉接的電話
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnOutFail, "seat cannot transfer");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      if (pChn1->ssState != CHN_SNG_IN_TALK && pChn1->ssState != CHN_SNG_OT_TALK)
      {
        //被轉接的電話未處于通話狀態
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutFail, "seat is not talking");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      if (pChn1->TranStatus != 0)
      {
        //被轉接的電話已處于轉接狀態
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutFail, "seat is transfing");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }

      //取被轉接的號碼及號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
      memset(CalledNo, 0, MAX_TELECODE_LEN);
      if (phonetype == 2)
      {
        strncpy(CalledNo, Phone, MAX_TELECODE_LEN-1);
      }
      else if (phonetype == 3)
      {
        //轉接的是工號,則查詢登錄的坐席號
        nAG1 = pAgentMng->GetnAGByWorkerNo(atoi(Phone));
        if (pAgentMng->isnAGAvail(nAG1))
        {
          phonetype = 2;
          strcpy(CalledNo, pAG1->m_Seat.SeatNo.C_Str());
        }
        else
        {
          //轉接的工號不存在
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnOutFail, "the workerno is not exist");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else if (phonetype == 4)
      {
        CACDRule ACDRule;
        ACDRule.TranCallFailCallBack = 0;
        ACDRule.AcdedCount = 0;
        ACDRule.SeatType = 0;
        ACDRule.SeatNo = "0";
        ACDRule.WorkerNo = 0;
        for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
        {
          ACDRule.GroupNo[i] = -1;
          ACDRule.Level[i] = 0;
          ACDRule.NowLevel[i] = 0;
        }
        ACDRule.GroupNo[0] = (UC)atoi(Phone);
        ACDRule.Level[0] = 0;
        ACDRule.NowLevel[0] = 0;
        ACDRule.AcdedGroupNo = 0;
        ACDRule.AcdRule = 2;
        ACDRule.LevelRule = 0;
        ACDRule.CallMode = 0;
        ACDRule.SeatGroupNo = 0;
        ACDRule.WaitTimeLen = 0;
        ACDRule.RingTimeLen = 0;
        ACDRule.BusyWaitId = 0;
        ACDRule.Priority = 0;
      
        ACDRule.QueueTime = time(0);
        ACDRule.StartRingTime = time(0);
        for (i = 0; i < MAX_AG_NUM; i ++)
        {
          ACDRule.AcdednAGList[i] = -1;
        }
        CAgent *pAgent3=GetIdelAgentForCallOut(ACDRule);
        if (pAgent3 != NULL)
        {
          nAG1 = pAgent3->nAG;
          strcpy(CalledNo, pAgent3->GetSeatNo().C_Str());
        }
        phonetype = 2;
      }
      else
      {
        phonetype = 1;
        strncpy(CalledNo, Phone, MAX_TELECODE_LEN-1);
      }
      if (phonetype == 2)
      {
        //如果轉接的是坐席,判斷是否空閑
        nAG1 = pAgentMng->GetnAGBySeatNo(CalledNo);
        if (pAgentMng->isnAGAvail(nAG1))
        {
          if (!pAG1->isIdelForSrv())
          {
            //轉接的坐席正忙
            sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
              szWSClientId, szSeatNo, OnOutBusy, "the dest seat is busy"); //2015-10-13 edit
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
            return 1;
          }
        }
        else
        {
          //轉接的坐席分機號不存在
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnOutUNN, "the seatno is not exist"); //2015-10-13 edit
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }

      int nOut = pCallbuf->Get_Idle_nOut();
      if (!pCallbuf->isnOutAvail(nOut))
      {
        //轉接緩沖區溢出
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutFail, "transfer buffer overflow");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      pAG->TranDesnAG = nAG1;
      //設置呼出數據
      pOut->state = 1;
      pOut->timer = 0;
      pOut->CallStep = 0;
    
      if (phonetype == 1)
        pOut->RingTime = 45;
      else
        pOut->RingTime = 30;
      pOut->IntervalTime = 0;
    
      pOut->Session.SessionNo = 0;
      pOut->Session.CmdAddr = 0;
      pOut->Session.nChn = nChn;
      pOut->Session.DialSerialNo = GetCallOutSerialNo(nChn);
      pOut->Session.SerialNo = 1;
      pOut->Session.CmdId = 3; //坐席轉接
      pOut->Session.VxmlId = 1;
      pOut->Session.FuncNo = 0;
      pOut->Session.CallerNo = pChn->CustPhone;
      pOut->Session.CalledNo = CalledNo;
      pOut->Session.GenerateCdrSerialNo(pChn->lgChnType, pChn->lgChnNo);
      char szTemp[256];
      sprintf(szTemp, "0`%s`%s`%s", pOut->Session.CdrSerialNo, pOut->Session.RecordFileName, pIVRCfg->SeatRecPath.C_Str());
      pOut->Session.Param = szTemp;
    
      if (phonetype == 1)
      {
        pOut->CallerNo = pIVRCfg->CenterCode;
      } 
      else
      {
        if (pChn->CallInOut == 1)
          pOut->CallerNo = pChn->CallerNo;
        else
          pOut->CallerNo = pChn->CalledNo;
      }
      pOut->CallerType = 0;
      pOut->CallType = 0;
      pOut->CallSeatType = 1;
      pOut->CancelId = 0;
    
      pOut->CallMode = 0;
      pOut->CallTimes = 1;
      pOut->CalledTimes = 0;
    
      pOut->CalledNum = 1;
      strcpy(pOut->Calleds[0].CalledNo, CalledNo);
      pOut->Calleds[0].CalledType = phonetype;
      pOut->Calleds[0].pQue_str = NULL;
      pOut->Calleds[0].CalloutResult = 0;
      pOut->Calleds[0].OutnChn = 0xFFFF;
      pOut->RouteNo[0] = 1;
    
      pOut->StartPoint = 0;
      pOut->CallPoint = 0;
      pOut->CallTime = 0;
    
      pOut->OutnChn = 0xFFFF;

      if (trantype == 2)
        pOut->TranMode = 1; //快速盲轉
      else if (trantype == 3)
        pOut->TranMode = 3; //會議轉接
      else
        pOut->TranMode = 2; //協商轉接

      pChn->nOut = nOut;
      pOut->TranCallFailReturn = 0;
      pOut->nAG = 0xFFFF;
    
      //斷開與正在的通話,并對其放等待音
      pChn->TranStatus = 3;
      pChn->TranControlnChn = nChn1;
      if (pOut->TranMode == 2)
        pChn->HoldingnChn = 0xFFFF;
      pChn1->TranStatus = 1;
      pChn1->TranControlnChn = nChn;
      if (pOut->TranMode == 2)
        pChn1->HoldingnChn = 0xFFFF;
    
      pOut->WaitVocFile = pIVRCfg->WaitVocFile;
      if (pChn->LinkType[0] != 0)
      {
        UnRouterTalk(nChn, nChn1);
        pChn->LinkType[0] = 0;
        pChn->LinkChn[0] = 0xFFFF;
        if (pOut->TranMode != 2)
          pChn->HoldingnChn = nChn1;
        pChn1->LinkType[0] = 0;
        pChn1->LinkChn[0] = 0xFFFF;
        if (pOut->TranMode != 2)
          pChn1->HoldingnChn = nChn;
        PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
        PlayFile(nChn1, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      
        WriteSeatStatus(pAG, AG_STATUS_HOLD, 0);
        pAG->SetAgentsvState(AG_SV_HOLD);
        sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=1", 
          szWSClientId, szSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);

        DispAgentStatus(nAG);
      }
      else
      {
        PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      }
      
      sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnOutCalling, "transfering...");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
    
    //交換機版本 取被轉接的號碼及號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
    if (phonetype == 3)
    {
      workerno = atoi(Phone);
      if (workerno == 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutUNN, "WorkerNo is null"); //2015-10-13 edit
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      nAG1 = pAgentMng->GetnAGByWorkerNo(workerno);
      if (!pAgentMng->isnAGAvail(nAG1))
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutUNN, "WorkerNo is not exist"); //2015-10-13 edit
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      if (pAG1->isIdelForSrv() == false)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutBusy, "The WorkerNo is busy"); //2015-10-13 edit
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      pChn->TranIVRId = 0;

      //2015-11-25 針對語音中繼的修改
      pAG1->m_Seat.nCallInnChn = pAG->m_Seat.nCallInnChn;
      pAG->m_Seat.nTranAGnChn = pAG1->GetSeatnChn();
      strcpy(pAG1->m_Seat.CustPhone, pChn->CustPhone); 
      MyTrace(3, "Proc_AGMSG_blindtrancall1 nChn=%d CallerNo=%s CalledNo=%s CustPhone=%s Set nAG=%d CustPhone=%s",
        nChn, pChn->CallerNo, pChn->CalledNo, pChn->CustPhone, pAG1->nAG, pAG1->m_Seat.CustPhone);

      Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, pAG1->m_Seat.SeatNo.C_Str(), trantype, szTranParam, phonetype);
      pAG->TranDesnAG = nAG1;
      
      sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnOutCalling, "transfering...");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
    else if (phonetype == 2)
    {
      nAG1 = pAgentMng->GetnAGBySeatNo(Phone);
      if (pAgentMng->isnAGAvail(nAG1))
      {
        if (pAG1->isIdelForSrv() == false)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnOutBusy, "The WorkerNo is busy"); //2015-10-13 edit
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else //2015-10-14 add
      {
        //轉接的坐席分機號不存在
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutUNN, "the seatno is not exist"); //2015-10-13 edit
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
    }
    else if (phonetype == 4)
    {
      CACDRule ACDRule;
      ACDRule.TranCallFailCallBack = 0;
      ACDRule.AcdedCount = 0;
      ACDRule.SeatType = 0;
      ACDRule.SeatNo = "0";
      ACDRule.WorkerNo = 0;
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
      {
        ACDRule.GroupNo[i] = -1;
        ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = 0;
      }
      ACDRule.GroupNo[0] = (UC)atoi(Phone);
      ACDRule.Level[0] = 0;
      ACDRule.NowLevel[0] = 0;
      ACDRule.AcdedGroupNo = 0;
      ACDRule.AcdRule = 2;
      ACDRule.LevelRule = 0;
      ACDRule.CallMode = 0;
      ACDRule.SeatGroupNo = 0;
      ACDRule.WaitTimeLen = 0;
      ACDRule.RingTimeLen = 0;
      ACDRule.BusyWaitId = 0;
      ACDRule.Priority = 0;
      
      ACDRule.QueueTime = time(0);
      ACDRule.StartRingTime = time(0);
      for (i = 0; i < MAX_AG_NUM; i ++)
      {
        ACDRule.AcdednAGList[i] = -1;
      }
      CAgent *pAgent3=GetIdelAgentForCallOut(ACDRule);
      if (pAgent3 != NULL)
      {
        nAG1 = pAgent3->nAG;
        strcpy(Phone, pAgent3->GetSeatNo().C_Str());
      }
      else
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnOutBusy, "all busy");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      phonetype = 2;
    }

    pChn->TranIVRId = 0;

    if (pAgentMng->isnAGAvail(nAG1))
    {
      //2015-11-25 針對語音中繼的修改
      pAG1->m_Seat.nCallInnChn = pAG->m_Seat.nCallInnChn;
      pAG->m_Seat.nTranAGnChn = pAG1->GetSeatnChn();
      strcpy(pAG1->m_Seat.CustPhone, pChn->CustPhone); 
      pAG->TranDesnAG = nAG1;
    }

    MyTrace(3, "Proc_AGMSG_blindtrancall1 nChn=%d CallerNo=%s CalledNo=%s CustPhone=%s Set nAG=%d CustPhone=%s",
      nChn, pChn->CallerNo, pChn->CalledNo, pChn->CustPhone, pAG1->nAG, pAG1->m_Seat.CustPhone);

    Send_SWTMSG_transfercall(nChn, pChn->DeviceID, 0, 0, Phone, trantype, szTranParam, phonetype);
    
    sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
      szWSClientId, szSeatNo, OnOutCalling, "transfering...");
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    return 0;
  }
  return 1;
}

//處理取消轉接電話消息
int ProcWebStopTransfer(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  int nAG, nAG1, nChn, nChn1, nChn2, nOut, nResult;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    //if (!pAG->isLogin()) //2016-11-18 改為不判斷是否登錄都可以取消轉接
    //  return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    if (g_nSwitchType == 0)
    {
      //板卡版本
      nOut = pChn->nOut;
      if (pCallbuf->isnOutAvail(nOut))
      {
        pOut->CancelId = 2;
        nChn1 = pChn->TranControlnChn;
        if (pBoxchn->isnChnAvail(nChn1))
        {
          //是否已與轉接的目的通道交換了
          nChn2 = pChn->LinkChn[0];
          if ((pChn->LinkType[0] == 1 || pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7) && pBoxchn->isnChnAvail(nChn2))
          {
            UnRouterTalk(nChn, nChn2);
            pChn->LinkType[0] = 0;
            pChn->LinkChn[0] = 0xFFFF;
            pChn2->LinkType[0] = 0;
            pChn2->LinkChn[0] = 0xFFFF;
          }
          else
          {
            StopClearPlayDtmfBuf(nChn);
          }
          
          StopClearPlayDtmfBuf(nChn1);
          nResult = RouterTalk(nChn, nChn1);
          if (nResult == 0)
          {
            pChn->RecMixerFlag = 1;
            pChn->LinkType[0] = 1;
            pChn->LinkChn[0] = nChn1;
            pChn->HoldingnChn = 0xFFFF;
            
            pChn1->RecMixerFlag = 1;
            pChn1->LinkType[0] = 1;
            pChn1->LinkChn[0] = nChn;
            pChn1->HoldingnChn = 0xFFFF;
            
            WriteSeatStatus(pAG, AG_STATUS_TALK, 0);
            pAG->SetAgentsvState(AG_SV_CONN);
            DispAgentStatus(nAGn);
            sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=0", 
              szWSClientId, szSeatNo);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
        }
        sprintf(szResultMsg, "wsclientid=%s;cmd=stoptransferresult;seatno=%s;result=0", 
          szWSClientId, szSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        
        nAG1 = pAG->TranDesnAG;
        if (pAgentMng->isnAGAvail(nAG1))
        {
          if (WebSocketLogId == true && strlen(pAG1->m_Seat.WSClientId) > 0)
          {
            sprintf(szResultMsg, "wsclientid=%s;cmd=stoptransfer;seatno=%s;result=%d;", 
              pAG1->m_Seat.WSClientId, pAG1->GetSeatNo().C_Str(), OnSuccess);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
          pAG->TranDesnAG = 0xFFFF;
        }
      }

      return 0;
    }

    //交換機版本
    Send_SWTMSG_stoptransfer(nChn, pChn->DeviceID, pChn->ConnID);
    sprintf(szResultMsg, "wsclientid=%s;cmd=stoptransferresult;seatno=%s;result=0", 
      szWSClientId, szSeatNo);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);

    nAG1 = pAG->TranDesnAG;
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (WebSocketLogId == true && strlen(pAG1->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=stoptransfer;seatno=%s;result=%d;", 
          pAG1->m_Seat.WSClientId, pAG1->GetSeatNo().C_Str(), OnSuccess);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      pAG->TranDesnAG = 0xFFFF;
    }
  }
  return 1;
}

//處理完成轉接電話消息
int ProcWebCompleteTransfer(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  int nAG, nAG1, nChn;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    //if (!pAG->isLogin()) //2016-11-18 改為不判斷是否登錄都可以完成轉接
    //  return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    if (g_nSwitchType == 0)
    {
      //板卡版本
      nAG1 = pAG->TranDesnAG;
      if (pAgentMng->isnAGAvail(nAG1))
      {
        if (WebSocketLogId == true && strlen(pAG1->m_Seat.WSClientId) > 0)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=completetransfer;seatno=%s;result=%d;", 
            pAG1->m_Seat.WSClientId, pAG1->GetSeatNo().C_Str(), OnSuccess);
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        }
        pAG->TranDesnAG = 0xFFFF;
      }

      Release_Chn(nChn, 1, 1);
      sprintf(szResultMsg, "wsclientid=%s;cmd=completetransferresult;seatno=%s;result=0", 
        szWSClientId, szSeatNo);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
    
    //交換機版本
    Send_SWTMSG_sendswitchcmd(pChn->ChIndex, pChn->DeviceID, 6);
    sprintf(szResultMsg, "wsclientid=%s;cmd=completetransferresult;seatno=%s;result=0", 
      szWSClientId, szSeatNo);
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    
    nAG1 = pAG->TranDesnAG;
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (WebSocketLogId == true && strlen(pAG1->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=completetransfer;seatno=%s;result=%d;", 
          pAG1->m_Seat.WSClientId, pAG1->GetSeatNo().C_Str(), OnSuccess);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      pAG->TranDesnAG = 0xFFFF;
    }
  }
  return 1;
}

//處理代接電話消息
int ProcWebPickupCall(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char pickupseatno[32];
  int nAG, nChn, nAG1, nChn1, nQue, nOut, nAcd, chState;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (!pAG->isLogin())
      return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    memset(pickupseatno, 0, 32);
    strncpy(pickupseatno, GetParamStrValue("pickupseatno=", pszDialMsg), 31);
    if (strlen(pickupseatno) == 0)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "pickupseatno is null");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }

    if (g_nSwitchType == 0)
    {
      //板卡版本
      if ((pAG->isWantSeatType(SEATTYPE_AG_PC) || pAG->isWantSeatType(SEATTYPE_RT_TEL)) && pAG->isIdelForSrv() == false)
      {
        //該坐席正忙
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "the seat is busy");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      else if (pAG->isWantSeatType(SEATTYPE_AG_TEL))
      {
        if (pAG->svState == AG_SV_IDEL)
        {
          //本坐席電話還未摘機
          sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat hangon");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        else if (pAG->svState != AG_SV_INSEIZE)
        {
          //本坐席電話正忙
          sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is busy");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
  
      nChn = pAG->GetSeatnChn();
      if (!pBoxchn->isnChnAvail(nChn))
      {
        //該坐席未綁定電話通道
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "the seat is not banding ch");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
  
      chState = GetAgentChState(pAG);
      if (pAG->isWantSeatType(SEATTYPE_AG_PC))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
          //該坐席電話還未摘機,請先摘機然后代接
          sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not hangoff");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
          //該坐席綁定的電話通道未通
          sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not talk");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else
      {
        if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
        {
          //該坐席電話還未摘機,請先摘機然后代接
          sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not hangoff");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
        pChn->hwState = CHN_HW_VALID;
        pChn->lnState = CHN_LN_SEIZE; //占用
        pChn->CallInOut = CALL_IN;
        pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
        pChn->CanPlayOrRecId = 1;
        pChn->CallTime = time(0);
        pChn->AnsTime = time(0);
        DispChnStatus(nChn);
      }
      nAG1 = GetnAGForPickup(pickupseatno, 0, 0, 0);
      if (!pAgentMng->isnAGAvail(nAG1))
      {
        //沒有可代接的坐席
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not seat for pickup");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      nQue = pAG1->nQue;
      if (!pCalloutQue->isnQueAvail(nQue))
      {
        //代接的坐席沒有電話呼入
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not call for pickup");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      //取消以前的呼叫
      nOut = pQue->nOut;
      nAcd = pQue->nAcd;
      nChn1 = pQue->OutnChn;
      CancelCallout(nChn1);
      pAG1->nQue = 0xFFFF;
      if (pAG1->isWantSeatType(SEATTYPE_RT_TEL))
      {
        pChn1->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
        pAG1->m_Seat.nChn = 0xFFFF;
      }
      SendStopACDCallIn(nAG1);
      WriteSeatStatus(pAG1, AG_STATUS_IDEL, 2); //代接標志
      SetAgentsvState(nAG1, AG_SV_IDEL);
      pQue->state = 0;
      //發送新的呼叫結果給被代接通道
      if (pQue->OrgCallType == 1)
      {
        //取消被代接通道的呼出
        for (int j=0; j<pOut->CalledNum; j++)
        {
          if (pQue->CallPoint==j) continue;
          if (pOut->Calleds[j].pQue_str != NULL)
            pOut->Calleds[j].pQue_str->CancelId = 1;
          if (pOut->Calleds[j].CalloutResult == CALLOUT_CONN)
          {
            pOut->Calleds[j].CalloutResult = 0;
            Release_Chn(pOut->Calleds[j].OutnChn, 1, 0);
          }
        }
        pOut->state = 0;
    
        SendCalloutResult(nOut, pQue->CallPoint, OnOutAnswer, "", nChn);
        pAgentMng->lastAnsnAG[0] = nAG;
        pAgentMng->lastAnsnAG[pAG->GetGroupNo()] = nAG;
        pAgentMng->lastAnsLevelnAG[0][0] = nAG;
        pAgentMng->lastAnsLevelnAG[0][pAG->GetLevel()] = nAG;
        pAgentMng->lastAnsLevelnAG[pAG->GetGroupNo()][pAG->GetLevel()] = nAG;
        pAG->m_Worker.AcdedGroupNo = 0;
        WriteSeatStatus(pAG, AG_STATUS_TALK, 0, 1, 2);

        SetAgentsvState(nAG, AG_SV_CONN);

        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;callerno=%s;calledno=%s;param=%s;result=%d;reason=", 
          szWSClientId, szSeatNo, pOut->CallerNo.C_Str(), pOut->Calleds[pQue->CallPoint].CalledNo, pOut->Session.Param.C_Str(), OnSuccess);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      else
      {
        //取消被代接通道的呼叫坐席
        for (int j=0; j<pAcd->CalledNum; j++)
        {
          if (pQue->CallPoint==j) continue;
          if (pAcd->Calleds[j].pQue_str != NULL)
            pAcd->Calleds[j].pQue_str->CancelId = 1;
          CAgent *pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
          if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
          {
            pAcd->Calleds[j].CalloutResult = 0;
            Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
          }
          SetAgentsvState(pAgent1, AG_SV_IDEL);
        }
    
        pAG->CanTransferId = 1;
        pAG->SetSrvLine(pAcd->Session.nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
        SendCallseatResult(nAcd, pAG, 0, OnACDAnswer, "");
        pAgentMng->lastAnsnAG[pAG->GetGroupNo()] = nAG;
        pAgentMng->lastAnsLevelnAG[pAG->GetGroupNo()][pAG->GetLevel()] = nAG;
        pAG->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
        WriteSeatStatus(pAG, AG_STATUS_TALK, 0, 1, 2);
    
        if (pIVRCfg->ControlCallModal == 0)
        {
          pAG->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
        }
        else
        {
          nChn = pAcd->Session.nChn;
          if (pChn->lgChnType == 2)
          {
            DBUpdateCallCDR_CallInOut(nChn, 3);
          }
          strcpy(pAG->CdrSerialNo, pChn->CdrSerialNo);
          strcpy(pAG->RecordFileName, pChn->RecordFileName);
          DBUpdateCallCDR_SeatAnsTime(pAG->nAG, pAcd->ACDRule.AcdedGroupNo);
          if (pAG->m_Worker.AutoRecordId == 1 && pAG->m_Seat.AutoRecordId == 1)
          {
            char szTemp[256], pszError[128];
            sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAG->RecordFileName);
            CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
        
            if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
            {
              if (SetChnRecordRule(pAG->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
              {
                DBUpdateCallCDR_RecdFile(pAG->nAG);
              }
            }
          }
        }
        DelFromQueue(nAcd);
    
        SetAgentsvState(nAG, AG_SV_CONN);

        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;callerno=%s;calledno=%s;param=%s;result=%d;reason=;", 
          szWSClientId, szSeatNo, pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(), pAcd->Session.Param.C_Str(), OnSuccess);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      return 0;
    }
    
    //交換機版本
    nAG1 = pAgentMng->GetnAGBySeatNo(pickupseatno); //2016-01-13 修改代接之前判斷是否有代接的電話
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (pAG1->isThesvState(AG_SV_INRING))
      {
        Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, pickupseatno);
      }
      else
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s;", 
          szWSClientId, szSeatNo, OnFail, "not call for pickup");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
    }
    else
    {
      //沒有可代接的坐席 2016-06-13
      if (pIVRCfg->isOnlyPickupCallExistSeatNo == true)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not seat for pickup");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      else
      {
        Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, pickupseatno);
      }
    }
    return 0;
  }
  return 1;
}

//處理監聽電話消息
int ProcWebListenCall(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char listenseatno[32];
  int nAG, nAG1, nChn, nChn1, nChn2, chState, i, j;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (!pAG->isLogin())
      return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    memset(listenseatno, 0, 32);
    strncpy(listenseatno, GetParamStrValue("listenseatno=", pszDialMsg), 31);
    if (strlen(listenseatno) == 0)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "listenseatno is null");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }

    if (g_nSwitchType == 0)
    {
      //板卡版本
      if ((pAG->isWantSeatType(SEATTYPE_AG_PC) || pAG->isWantSeatType(SEATTYPE_RT_TEL)) && pAG->isIdelForSrv() == false)
      {
  	    //該坐席正忙
        sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "the seat is busy");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      else if (pAG->isWantSeatType(SEATTYPE_AG_TEL))
      {
        if (pAG->svState == AG_SV_IDEL)
        {
    	    //本坐席電話還未摘機
          sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat hangon");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        else if (pAG->svState != AG_SV_INSEIZE)
        {
    	    //本坐席電話正忙
          sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is busy");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
  
      chState = GetAgentChState(pAG);
      if (pAG->isWantSeatType(SEATTYPE_AG_PC))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
    	    //該坐席電話還未摘機,請先摘機然后呼出
          sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not hangoff");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
    	    //該坐席綁定的電話通道未通
          sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not talk");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else
      {
        if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
        {
          //該坐席電話還未摘機,請先摘機然后呼出
          sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not hangoff");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
        pChn->hwState = CHN_HW_VALID;
        pChn->lnState = CHN_LN_SEIZE; //占用
        pChn->CallInOut = CALL_IN;
        pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
        pChn->CanPlayOrRecId = 1;
        pChn->CallTime = time(0);
        pChn->AnsTime = time(0);
        DispChnStatus(nChn);
      }
      nAG1 = GetnAGbyWorkerNoSeatNo(listenseatno, 0);
      if (!pAgentMng->isnAGAvail(nAG1))
      {
        //被監聽的坐席或工號不存在
        sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not seat for listen");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      if (!pAG1->isThesvState(AG_SV_CONN))
      {
        //被監聽的坐席或工號不處于通話狀態
        sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not call for listen");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      nChn1 = pAG1->GetSeatnChn();
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        //被監聽的坐席未綁定電話通道
        sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      for (i = 2; i <= MAX_LINK_CHN_NUM; i ++)
      {
        if (pChn1->LinkType[i] == 0)
        {
          break;
        }
      }
      if (i < MAX_LINK_CHN_NUM)
      {
        if (RouterMonitor(nChn, nChn1, 1) == 0)
        {
          nChn2 = pChn1->LinkChn[0];
          if (pBoxchn->isnChnAvail(nChn2))
          {
            for (j = 2; j <= MAX_LINK_CHN_NUM; j ++)
            {
              if (pChn2->LinkType[j] == 0)
              {
                break;
              }
            }
            //2012-06-20
            if (j < MAX_LINK_CHN_NUM)
            {
              if (RouterMonitor(nChn, nChn2, 1) == 0)
              {
                pChn2->LinkType[j] = 3;
                pChn2->LinkChn[j] = nChn;
                pChn->LinkType[1] = 2;
                pChn->LinkChn[1] = nChn2;
              }
            }
            sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;callerno=%s;result=%d;reason=", 
              szWSClientId, szSeatNo, pChn2->CallerNo, OnSuccess);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
          else
          {
            sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=", 
              szWSClientId, szSeatNo, OnSuccess);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
          pChn->LinkType[0] = 2;
          pChn->LinkChn[0] = nChn1;
          pChn1->LinkType[i] = 3;
          pChn1->LinkChn[i] = nChn;
          SetAgentsvState(nAG, AG_SV_CONN);
          return 0;
        }
      }
  
      sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);

      Release_Chn(nChn, 1, 0);

      return 0;
    }

    //交換機版本
    nAG1 = pAgentMng->GetnAGBySeatNo(listenseatno); //2016-01-13 修改監聽之前判斷是否可以監聽
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (pAG1->isThesvState(AG_SV_CONN))
      {
        Send_SWTMSG_listencall(nChn, pChn->DeviceID, pChn->ConnID, 0, listenseatno);
      }
      else
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=listenresult;seatno=%s;result=%d;reason=%s;", 
          szWSClientId, szSeatNo, OnFail, "not call for listen");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
    }
    else
    {
      Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, listenseatno);
    }
    return 0;
  }
  return 1;
}

//處理強插電話消息
int ProcWebInsertCall(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char insertseatno[32];
  int nAG, nChn, nAG1, nChn1, nChn2, chState, nCfc;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (!pAG->isLogin())
      return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;
    memset(insertseatno, 0, 32);
    strncpy(insertseatno, GetParamStrValue("insertseatno=", pszDialMsg), 31);
    if (strlen(insertseatno) == 0)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "insertseatno is null");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 1;
    }
    
    if (g_nSwitchType == 0)
    {
      //板卡版本
      if ((pAG->isWantSeatType(SEATTYPE_AG_PC) || pAG->isWantSeatType(SEATTYPE_RT_TEL)) && pAG->isIdelForSrv() == false)
      {
  	    //該坐席正忙
        sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "the seat is busy");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      else if (pAG->isWantSeatType(SEATTYPE_AG_TEL))
      {
        if (pAG->svState == AG_SV_IDEL)
        {
    	    //本坐席電話還未摘機
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat hangon");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        else if (pAG->svState != AG_SV_INSEIZE)
        {
    	    //本坐席電話正忙
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is busy");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
  
      chState = GetAgentChState(pAG);
      if (pAG->isWantSeatType(SEATTYPE_AG_PC))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
    	    //該坐席電話還未摘機,請先摘機然后呼出
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not hangoff");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
    	    //該坐席綁定的電話通道未通
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not talk");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else
      {
        if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
        {
          //該坐席電話還未摘機,請先摘機然后呼出
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "the seat is not hangoff");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
        pChn->hwState = CHN_HW_VALID;
        pChn->lnState = CHN_LN_SEIZE; //占用
        pChn->CallInOut = CALL_IN;
        pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
        pChn->CanPlayOrRecId = 1;
        pChn->CallTime = time(0);
        pChn->AnsTime = time(0);
        DispChnStatus(nChn);
      }
      nAG1 = GetnAGbyWorkerNoSeatNo(insertseatno, 0);
      if (!pAgentMng->isnAGAvail(nAG1))
      {
        //被監聽的坐席或工號不存在
        sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not seat for insert");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      if (!pAG1->isThesvState(AG_SV_CONN))
      {
        //被監聽的坐席或工號不處于通話狀態
        sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not call for insert");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      nChn1 = pAG1->GetSeatnChn();
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        //被監聽的坐席未綁定電話通道
        sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }

      if (pChn1->JoinType > 0 || pChn1->CfcNo < MAX_CONF_NUM)
      {
        nCfc = pChn1->CfcNo;
        if (pCfc->Talkeds >= pCfc->MaxTalkers)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
            szWSClientId, szSeatNo, OnFail, "fail1");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          Release_Chn(nChn, 1, 0);
          return 1;
        }
        if (JoinConf(nCfc, nChn, 100) == 0)
        {
          pChn->CfcNo = nCfc;
          pChn->JoinTime = time(0);
          pCfc->Talkers[pCfc->Talkeds] = nChn;
          pChn->JoinType = talker;
          pCfc->Talkeds ++;
          SetAgentsvState(nAGn, AG_SV_CONN);
          sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;callerno=%s;result=%d;reason=", 
            szWSClientId, szSeatNo, pChn1->CallerNo, OnSuccess);
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 0;
        }
        sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "fail2");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        Release_Chn(nChn, 1, 0);
        return 1;
      }
      if (pChn1->LinkType[0] == 1 || pChn1->LinkType[0] == 6 || pChn1->LinkType[0] == 7)
      {
        nChn2 = pChn1->LinkChn[0];
        if (pBoxchn->isnChnAvail(nChn2))
        {
          nCfc = CreateThreeConf(nChn);
          if (nCfc > 0)
          {
            UnRouterTalk(nChn1, nChn2);
            pChn1->LinkType[0] = 0;
            pChn1->LinkChn[0] = 0xFFFF;
            pChn2->LinkType[0] = 0;
            pChn2->LinkChn[0] = 0xFFFF;
            
            StopClearPlayDtmfRecBuf(nChn);
            StopClearPlayDtmfRecBuf(nChn1);
            StopClearPlayDtmfRecBuf(nChn2);
            if (JoinThreeConf(nCfc, nChn, nChn1, nChn2) == 0)
            {
              SetAgentsvState(nAGn, AG_SV_CONN);
              sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;callerno=%s;result=%d;reason=", 
                szWSClientId, szSeatNo, pChn1->CallerNo, OnSuccess);
              SendMsg2WS(WebSocketClientId, 0, szResultMsg);
              return 0;
            }
          }
        }
      }
      sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "fail3");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      Release_Chn(nChn, 1, 0);

      return 0;
    }
    
    //交換機版本
    
    //交換機版本
    nAG1 = pAgentMng->GetnAGBySeatNo(insertseatno); //2016-01-13 修改強插之前判斷是否可以強插
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (pAG1->isThesvState(AG_SV_CONN))
      {
        Send_SWTMSG_breakincall(nChn, pChn->DeviceID, pChn->ConnID, 0, insertseatno);
      }
      else
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=insertresult;seatno=%s;result=%d;reason=%s;", 
          szWSClientId, szSeatNo, OnFail, "not call for insert");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
    }
    else
    {
      Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, insertseatno);
    }
    return 0;
  }
  return 1;
}

//處理設定話后處理時長消息
int ProcWebSetACWTimer(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  //char szResultMsg[512];
  int nAG, len;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    len = GetParamIntValue("acwtimer=", pszDialMsg);
    pAG->m_Worker.DelayTime = len;
  }
  return 1;
}

//處理結束話后處理消息
int ProcWebSetACWEnd(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  //char szResultMsg[512];
  int nAG;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (!pAG->isLogin())
      return 1;
    if (pAG->svState == AG_SV_IDEL)
    {
      if (pAG->DelayState == 1)
      {
        WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
        DBUpdateCallCDR_ACWTime(nAG);
      }
      pAG->DelayState = 0; //2015-12-14
      AgentStatusCount(); //2015-12-14
      SendOneGroupStatusMeteCount(pAG->nAG);
      SendSystemStatusMeteCount();
      DispAgentStatus(nAG);
    }
  }
  return 1;
}

//處理強拆其他座席消息
int ProcWebForceClear(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char  forceseatno[32];
  int nAG, nAG1, nChn1;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    memset(forceseatno, 0, 32);
    strncpy(forceseatno, GetParamStrValue("forceseatno=", pszDialMsg), 31);
    nAG1 = pAgentMng->GetnAGBySeatNo(forceseatno);
    if (pAgentMng->isnAGAvail(nAG1))
    {
      nChn1 = pAG1->GetSeatnChn();
      if (pBoxchn->isnChnAvail(nChn1))
      {
        Release_Chn(nChn1, 1, 1);
        sprintf(szResultMsg, "wsclientid=%s;cmd=forceclearresult;seatno=%s;forceseatno=%s;result=%d;reason=", 
          szWSClientId, szSeatNo, pAG1->m_Seat.SeatNo.C_Str(), OnSuccess);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 0;
      }
    }
  }
  return 1;
}

//處理強制將其他座席登出消息
int ProcWebForceLogout(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char  forceseatno[32];
  int nAG, nAG1, nQue;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    memset(forceseatno, 0, 32);
    strncpy(forceseatno, GetParamStrValue("forceseatno=", pszDialMsg), 31);
    nAG1 = pAgentMng->GetnAGBySeatNo(forceseatno);
    if (pAgentMng->isnAGAvail(nAG1))
    {
      nQue = pAG1->nQue;
      if (pCalloutQue->isnQueAvail(nQue) && pAG1->isWantSeatType(SEATTYPE_AG_PC))
      {
        pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
      }
      if (pAG1->m_Worker.oldWorkerNo == 0)
      {
        pAG1->m_Worker.Logout();
        pAG1->m_Worker.ClearState();
      }
      else
      {
        pAG1->m_Worker.OldWorkerLogin();
      }
      DispAgentStatus(nAG1);
      sprintf(szResultMsg, "wsclientid=%s;cmd=forcelogoutresult;seatno=%s;forceseatno=%s;result=%d;reason=", 
        szWSClientId, szSeatNo, pAG1->m_Seat.SeatNo.C_Str(), OnSuccess);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
  }
  return 1;
}

//處理強制將其他坐席示閑消息
int ProcWebForceReady(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char  forceseatno[32];
  int nAG, nAG1;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    memset(forceseatno, 0, 32);
    strncpy(forceseatno, GetParamStrValue("forceseatno=", pszDialMsg), 31);
    nAG1 = pAgentMng->GetnAGBySeatNo(forceseatno);
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (pAG1->isLogin() == false)
      {
        return 1;
      }
      pAG1->m_Worker.SetDisturb(0);
      pAG1->m_Worker.SetLeave(0, "Force Ready");
      DispAgentStatus(nAG1);
      sprintf(szResultMsg, "wsclientid=%s;cmd=forcereadyresult;seatno=%s;forceseatno=%s;result=%d;reason=", 
        szWSClientId, szSeatNo, pAG1->m_Seat.SeatNo.C_Str(), OnSuccess);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
  }
  return 1;
}

//處理強制將其他坐席示忙消息
int ProcWebForceBusy(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  char forceseatno[32];
  int nAG, nAG1;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    memset(forceseatno, 0, 32);
    strncpy(forceseatno, GetParamStrValue("forceseatno=", pszDialMsg), 31);
    nAG1 = pAgentMng->GetnAGBySeatNo(forceseatno);
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (pAG1->isLogin() == false)
      {
        return 1;
      }
      pAG1->m_Worker.SetDisturb(1);
      pAG1->m_Worker.SetLeave(0, "");
      DispAgentStatus(nAG1);
      sprintf(szResultMsg, "wsclientid=%s;cmd=forcebusyresult;seatno=%s;forceseatno=%s;result=%d;reason=", 
        szWSClientId, szSeatNo, pAG1->m_Seat.SeatNo.C_Str(), OnSuccess);
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }
  }
  return 1;
}

//處理發送文字信息給其他坐席消息
int ProcWebSendMessage(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szAnsiMsg[1024], szUTF8Msg[1024];
  char destno[32];
  int nAG, nAG1, desttype, workerno, groupno, nResult;
  CAgent *pAgent=NULL;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (!pAG->isLogin())
      return 1;
    desttype = GetParamIntValue("desttype=", pszDialMsg);
    memset(destno, 0, 32);
    strncpy(destno, GetParamStrValue("destno=", pszDialMsg), 31);
    
    memset(szAnsiMsg, 0, 1024);
    memset(szUTF8Msg, 0, 1024);
    strncpy(szUTF8Msg, GetParamStrValue("message=", pszDialMsg), 250);
    UTF8_ANSI_Convert(szUTF8Msg, szAnsiMsg, CP_UTF8, CP_ACP);
    
    if (desttype == 1)
    {
      pAgent = GetAgentbySeatNo(destno, nResult);
      if (pAgent != NULL)
      {
        SendSeatMessage(pAgent->nAG, nAG, szAnsiMsg, szUTF8Msg);
      }
      workerno = atoi(destno);
      if (workerno > 0) //指定工號
      {
        pAgent = GetAgentbyWorkerNo(workerno, nResult);
        if (pAgent != NULL)
        {
          SendSeatMessage(pAgent->nAG, nAG, szAnsiMsg, szUTF8Msg);
        }
      }
      groupno = atoi(destno);
      if (groupno > 0) //指定組號
      {
        for (nAG1 = 0; nAG1 < pAgentMng->MaxUseNum; nAG1 ++)
        {
          if (groupno == pAgentMng->m_Agent[nAG1]->GetGroupNo())
          {
            SendSeatMessage(nAG1, nAG, szAnsiMsg, szUTF8Msg);
          }
        }
      }
      else
      {
        for (nAG1 = 0; nAG1 < pAgentMng->MaxUseNum; nAG1 ++)
        {
          if (nAG == nAG)
            continue;
          SendSeatMessage(nAG, nAG1, szAnsiMsg, szUTF8Msg);
        }
      }
    }
  }
  return 1;
}
//處理設置呼轉消息
int ProcWebSetForwarding(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  //char szResultMsg[512];
  char destno[32];
  int nAG, nChn, forwardtype, fwdonoff, nTranType=0;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    nChn = pAG->GetSeatnChn();
    if (pBoxchn->isnChnAvail(nChn))
    {
      //forwardtype 0-取消所有轉移 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移 5-遇忙和久叫不應轉移 //2016-07-18
      forwardtype = GetParamIntValue("forwardtype=", pszDialMsg);
      fwdonoff = GetParamIntValue("forwardonoff=", pszDialMsg);
      if (fwdonoff == 1)
      {
        nTranType = forwardtype;
      } 
      else
      {
        switch (forwardtype)
        {
        case 0:
          nTranType = 0;
          break;
        case 1:
          nTranType = 10;
          break;
        case 2:
          nTranType = 20;
          break;
        case 3:
          nTranType = 30;
          break;
        case 5:
          nTranType = 50;
          break;
        }
      }
      memset(destno, 0, 32);
      strncpy(destno, GetParamStrValue("forwardno=", pszDialMsg), 31);
      //nTranType 0-取消所有轉移 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移 4-同振綁定 10-取消無條件轉移 20-取消遇忙轉移 30-取消久叫不應轉移 50-取消遇忙和久叫不應轉移
      Send_SWTMSG_settranphone(nChn, pChn->DeviceID, nTranType, destno);
    }
  }
  return 1;
}
//處理通話后發送DTMF消息
int ProcWebSendDTMF(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  //char szResultMsg[512];
  char szDTMF[32];
  int nAG, nChn;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    nChn = pAG->GetSeatnChn();
    if (pBoxchn->isnChnAvail(nChn))
    {
//       if (pAG->svState != AG_SV_CONN)
//       {
//         return 1;
//       }
      if (g_nSwitchType > 0)
      {
        memset(szDTMF, 0, 32);
        strncpy(szDTMF, GetParamStrValue("dtmfs=", pszDialMsg), 31);
        Send_SWTMSG_senddtmf(nChn, pChn->DeviceID, pChn->ConnID, szDTMF);
      }
    }
  }
  return 1;
}
//處理坐席轉IVR消息
int ProcWebTransferIVR(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  //char szResultMsg[512];
  char tranparam[128];
  char tranivrno[32]; //2016-03-30 轉接的IVR號碼
  char szTranData[256];
  int nAG, nChn, nChn1, tranivrid, returnid, routeno;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  //MyTrace(3, "ProcWebTransferIVR szSeatNo=%s nAG=%d", szSeatNo, nAG);
  if (pAgentMng->isnAGAvail(nAG))
  {
    nChn = pAG->GetSeatnChn();
    if (pBoxchn->isnChnAvail(nChn))
    {
      tranivrid = GetParamIntValue("tranivrid=", pszDialMsg);
      returnid = GetParamIntValue("returnid=", pszDialMsg);
      routeno = GetParamIntValue("routeno=", pszDialMsg); //2016-04-21
      memset(tranparam, 0, 128);
      strncpy(tranparam, GetParamStrValue("tranparam=", pszDialMsg), 127);
      memset(tranivrno, 0, 32);
      strncpy(tranivrno, GetParamStrValue("tranivrno=", pszDialMsg), 31);
    
      if (g_nSwitchType == 0)
      {
        Release_Chn(nChn, 1, 1);
        return 1;
      }
      //MyTrace(3, "ProcWebTransferIVR tranivrid=%d pAG->svState=%d", tranivrid, pAG->svState);
      if (pAG->svState != AG_SV_CONN)
      {
        return 1;
      }

      nChn1 = GetIVRChn(routeno, ""); //2016-04-21 增加IVR路由判斷
      MyTrace(3, "ProcWebTransferIVR nChn1=%d", nChn1);
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        return 1;
      }

      if (tranivrid == 1)
      {
        //評分
        pChn->TranIVRId = 1;
        pChn1->TranIVRId = 2;
        pChn->nAcd = 0xFFFF;
        pChn1->nAcd = 0xFFFF;
        if (strlen(tranparam) == 0)
        {
          Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, pChn->CdrSerialNo, 0);
        }
        else
        {
          sprintf(szTranData, "%s`%s", tranparam, pChn->CdrSerialNo);
          Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, szTranData, 0);
        }
      }
      else if (tranivrid == 2 || tranivrid == 3)
      {
        //接收驗證碼 $S_CheckCode、密碼驗證 $S_Password
        pChn->TranIVRId = 1;
        pChn1->TranIVRId = 7;
        pChn->nAcd = 0xFFFF;
        pChn1->nAcd = 0xFFFF;
        strcpy(pChn1->CdrSerialNo, pChn->CdrSerialNo);
        strcpy(pChn1->RecordFileName, pChn->RecordFileName);
        pAG->m_Worker.DisturbId = 2; //設置等待IVR返回
        pAG->SendTrnIVRHangonFlag = 1; //2016-07-19發送轉IVR掛機標志
        pAG->timer = 0;
        SendOneAGStateToAll(nAG);
        
        //2016-03-30增加判斷是否有指定轉接的IVR群組號
        if (strlen(tranivrno) > 0)
          sprintf(szTranData, "$S_IVRNO=%s;%s`%s`%s`%s", tranivrno, tranparam, pAG->GetWorkerNoStr(), pChn->CdrSerialNo, pChn->RecordFileName);
        else
          sprintf(szTranData, "%s`%s`%s`%s", tranparam, pAG->GetWorkerNoStr(), pChn->CdrSerialNo, pChn->RecordFileName);
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, szTranData, 0);
      }
      else if (tranivrid == 4 || tranivrid == 5)
      {
        //轉接收傳真、轉索取傳真
        pChn->TranIVRId = 1;
        pChn1->TranIVRId = 2;
        pChn->nAcd = 0xFFFF;
        pChn1->nAcd = 0xFFFF;
        
        sprintf(szTranData, "%s`%s`%s`%s", tranparam, pAG->GetWorkerNoStr(), pChn->CdrSerialNo, pChn->RecordFileName);
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pChn1->DeviceID, 2, szTranData, 0);
      }
      else
      {
        //轉接ivr
        sprintf(SendMsgBuf, "<onagtranivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' tranivrid='%d' returnid='%d' tranparam='%s'/>", 
          pChn1->SessionNo, pChn1->CmdAddr, pChn1->lgChnType, pChn1->lgChnNo, tranivrid, returnid, tranparam);
        SendMsg2XML(MSG_onagtranivr, pChn1->SessionNo, SendMsgBuf);
      }
    }
  }
  return 1;
}

//處理代接ACD隊列電話消息 //2015-12-14
int ProcWebPickupACDCall(const char *pszDialMsg, int SendResult)
{
  char szWSClientId[64];
  char szSeatNo[32];
  char szResultMsg[512];
  int nAG, nChn, nAcd, nAcd1, nAG1, nChn1, chState;
  CAgent *pAgent1=NULL;
  
  memset(szWSClientId, 0, 64);
  strncpy(szWSClientId, GetParamStrValue("wsclientid=", pszDialMsg), 63);
  memset(szSeatNo, 0, 32);
  strncpy(szSeatNo, GetParamStrValue("seatno=", pszDialMsg), 31);
  if (strlen(szWSClientId) == 0)
    strcpy(szWSClientId, szSeatNo);
  
  nAG = pAgentMng->GetnAGBySeatNo(szSeatNo);
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (!pAG->isLogin())
      return 1;
    nChn = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn))
      return 1;

    nAcd1 = GetParamIntValue("acdno=", pszDialMsg);
    if (nAcd1 <= 0)
    {
      nAcd1 = 0;
      nAcd = pAcd1->Next;
    }
    else
    {
      nAcd = nAcd1;
    }
    if (!pACDQueue->isnAcdAvail(nAcd))
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "the acdno is invalid");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      Release_Chn(nChn, 1, 0);
      return 1;
    }
    if (pAcd->state == 0)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnFail, "the acdno is invalid");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      Release_Chn(nChn, 1, 0);
      return 1;
    }

    if (g_nSwitchType == 0)
    {
      if (g_nCardType == 9)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "not support");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      if ((pAG->isWantSeatType(SEATTYPE_AG_PC) || pAG->isWantSeatType(SEATTYPE_RT_TEL)) && pAG->isIdelForSrv() == false)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "the seat is busy");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
      else if (pAG->isWantSeatType(SEATTYPE_AG_TEL))
      {
        if (pAG->svState == AG_SV_IDEL)
        {
    	    sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
      	    szWSClientId, szSeatNo, OnFail, "the seat is hangon");
    	    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        else if (pAG->svState != AG_SV_INSEIZE)
        {
    	    sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
      	    szWSClientId, szSeatNo, OnFail, "the seat is busy");
    	    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
  
      chState = GetAgentChState(pAG);
      if (pAG->isWantSeatType(SEATTYPE_AG_PC))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
	  	    sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
	    	    szWSClientId, szSeatNo, OnFail, "please hangoff first");
	  	    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else if (pAG->isWantSeatType(SEATTYPE_RT_TEL))
      {
        if (chState != CHN_SNG_IN_TALK && chState != CHN_SNG_OT_TALK)
        {
	  	    sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
	    	    szWSClientId, szSeatNo, OnFail, "the seat is not in talk");
	  	    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
      }
      else
      {
        if (chState != CHN_SNG_IN_ARRIVE && chState != CHN_SNG_IN_WAIT)
        {
	  	    sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
	    	    szWSClientId, szSeatNo, OnFail, "please hangoff first");
	  	    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          return 1;
        }
        SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
        pChn->hwState = CHN_HW_VALID;
        pChn->lnState = CHN_LN_SEIZE; //占用
        pChn->CallInOut = CALL_IN;
        pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
        pChn->CanPlayOrRecId = 1;
        pChn->CallTime = time(0);
        pChn->AnsTime = time(0);
        DispChnStatus(nChn);
      }
      //取消被代接通道的呼叫坐席
      for (int j=0; j<pAcd->CalledNum; j++)
      {
        if (pAcd->Calleds[j].pQue_str != NULL)
          pAcd->Calleds[j].pQue_str->CancelId = 1;
        pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
        if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
        {
          pAcd->Calleds[j].CalloutResult = 0;
          Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
        }
        SetAgentsvState(pAgent1, AG_SV_IDEL);
        if (pAgent1)
        {
          WriteSeatStatus(pAgent1, AG_STATUS_IDEL, 2);
        }
      }
  
      pAG->CanTransferId = 1;
      pAG->SetSrvLine(pAcd->Session.nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
      SendCallseatResult(nAcd, pAG, 0, OnACDAnswer, "");
      pAgentMng->lastAnsnAG[0] = nAG;
      pAgentMng->lastAnsnAG[pAG->GetGroupNo()] = nAG;
      pAgentMng->lastAnsLevelnAG[0][0] = nAG;
      pAgentMng->lastAnsLevelnAG[0][pAG->GetLevel()] = nAG;
      pAgentMng->lastAnsLevelnAG[pAG->GetGroupNo()][pAG->GetLevel()] = nAG;
      pAG->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
      WriteSeatStatus(pAG, AG_STATUS_TALK, 0, 1, 2);
      if (pIVRCfg->ControlCallModal == 0)
      {
        pAG->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
      }
      else
      {
        nChn1 = pAcd->Session.nChn;
        if (pChn1->lgChnType == 2)
        {
          DBUpdateCallCDR_CallInOut(nChn1, 3);
        }
        //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
        strcpy(pAG->CdrSerialNo, pChn1->CdrSerialNo);
        strcpy(pAG->RecordFileName, pChn1->RecordFileName);
        DBUpdateCallCDR_SeatAnsTime(pAG->nAG, pAcd->ACDRule.AcdedGroupNo);
        if (pAG->m_Worker.AutoRecordId == 1 && pAG->m_Seat.AutoRecordId == 1)
        {
          char szTemp[256], pszError[128];
          sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAG->RecordFileName);
          CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
      
          if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
          {
            if (SetChnRecordRule(pAG->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
            {
              DBUpdateCallCDR_RecdFile(pAG->nAG);
            }
          }
        }
      }
      DelFromQueue(nAcd);
  
      SetAgentsvState(nAG, AG_SV_CONN);
      sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
        szWSClientId, szSeatNo, OnSuccess, "");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      return 0;
    }

    //交換機版本
    MyTrace(3, "ProcWebPickupACDCall nAG=%d InnChn=%d nAcd=%d AllocnAG=%d acdState=%d", 
      nAG, pAcd->Session.nChn, nAcd, pAcd->Calleds[0].nAG, pAcd->acdState);
    //判斷是否已經分配了坐席或還是在等待分配隊列里
    if (pAcd->acdState == 2)
    {
      //已經振鈴,等待應答結果
      nAG1 = pAcd->Calleds[0].nAG;
      if (pAgentMng->isnAGAvail(nAG1))
      {
        Send_SWTMSG_pickupcall(nChn, pChn->DeviceID, pChn->ConnID, 0, pAG1->m_Seat.SeatNo.C_Str());
        return 0;
      }
    }
    else if (pAcd->acdState == 0 || pAcd->acdState == 3)
    {
      //等待分配狀態
      SetAcdedSeatNo(nAcd, pAG);
      nChn1 = pAcd->Session.nChn;
      if (SetACDCalloutQue(nAcd, 0, true) == 0)
      {
        pAcd->timer = 0;
        pAcd->ringtimer = 0;
        pAcd->acdState = 1; //1-等待呼叫結果
        if (pAcd->acdState == 0)
          ChCount.AcdCallSuccCount ++;
        pAcd->ACDRule.StartRingTime = time(0);
        SendACDQueueInfoToAll();
        if (pAcd->ACDRule.AcdRule != 7)
        {
          DBUpdateCallCDR_WaitTime(nChn1);
          DBUpdateCallCDR_ACDTime(nChn1, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
        }
        return 0;
      }
      else
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
          szWSClientId, szSeatNo, OnFail, "the seat is busy");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        return 1;
      }
    }
    
    sprintf(szResultMsg, "wsclientid=%s;cmd=pickupacdresult;seatno=%s;result=%d;reason=%s", 
      szWSClientId, szSeatNo, OnFail, "the seat is busy");
    SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    return 0;
  }
  return 1;
}
//處理創建會議消息 2016-12-13
int ProcWebCreateConf(const char *pszDialMsg, UC WsTcpLinkNo)
{
  char sqls[2048], szguid[50], szcreater[32], szconfmember[256], szconfstrattime[32];
  int nconfid, nconftype, nnotifyid, nrecordid;
  
  memset(szguid, 0, 50);
  strncpy(szguid, GetParamStrValue("confguid=", pszDialMsg), 36);
  memset(szcreater, 0, 32);
  strncpy(szcreater, GetParamStrValue("creater=", pszDialMsg), 31);
  memset(szconfmember, 0, 256);
  strncpy(szconfmember, GetParamStrValue("confmember=", pszDialMsg), 255);
  memset(szconfstrattime, 0, 32);
  strncpy(szconfstrattime, GetParamStrValue("confstrattime=", pszDialMsg), 31);
  nconfid = GetParamIntValue("confno=", pszDialMsg);
  nconftype = GetParamIntValue("conftype=", pszDialMsg);
  nnotifyid = GetParamIntValue("notifyid=", pszDialMsg);
  nrecordid = GetParamIntValue("recordid=", pszDialMsg);
  
  if (strlen(szguid) == 0)
    strcpy(szguid, MyGetGUID());
  
  sprintf(sqls, "exec sp_CreateConf '%s','%s',%d,%d,'%s','%s',%d,%d,'%s'", 
    szguid, szcreater, nconfid, nconftype, szconfstrattime, "", nnotifyid, nrecordid, szconfmember);
  SendExecSql2DB(sqls);
  
  return 1;
}
//處理增加會議成員消息
int ProcWebJoinConf(const char *pszDialMsg, UC WsTcpLinkNo)
{
  char sqls[2048], szguid[50], szcreater[32], szconfmember[256];
  int nconfid;
  
  memset(szguid, 0, 50);
  strncpy(szguid, GetParamStrValue("confguid=", pszDialMsg), 36);
  memset(szcreater, 0, 32);
  strncpy(szcreater, GetParamStrValue("creater=", pszDialMsg), 31);
  memset(szconfmember, 0, 256);
  strncpy(szconfmember, GetParamStrValue("confmember=", pszDialMsg), 255);
  nconfid = GetParamIntValue("confno=", pszDialMsg);
  
  sprintf(sqls, "exec sp_JoinConf '%s','%s',%d,'%s'", 
    szguid, szcreater, nconfid, szconfmember);
  SendExecSql2DB(sqls);
  
  return 1;
}
//處刪除會議成員消息
int ProcWebUnJoinConf(const char *pszDialMsg, UC WsTcpLinkNo)
{
  char sqls[2048], szguid[50], szcreater[32], szconfmember[256];
  int nconfid;
  
  memset(szguid, 0, 50);
  strncpy(szguid, GetParamStrValue("confguid=", pszDialMsg), 36);
  memset(szcreater, 0, 32);
  strncpy(szcreater, GetParamStrValue("creater=", pszDialMsg), 31);
  memset(szconfmember, 0, 256);
  strncpy(szconfmember, GetParamStrValue("confmember=", pszDialMsg), 255);
  nconfid = GetParamIntValue("confno=", pszDialMsg);
  
  sprintf(sqls, "exec sp_UnJoinConf '%s','%s',%d,'%s'", 
    szguid, szcreater, nconfid, szconfmember);
  SendExecSql2DB(sqls);
  
  return 1;
}
//處理關閉會議消息
int ProcWebCloseConf(const char *pszDialMsg, UC WsTcpLinkNo)
{
  char sqls[1024], szguid[50], szcreater[32];
  int nconfid;
  
  memset(szguid, 0, 50);
  strncpy(szguid, GetParamStrValue("confguid=", pszDialMsg), 36);
  memset(szcreater, 0, 32);
  strncpy(szcreater, GetParamStrValue("creater=", pszDialMsg), 31);
  nconfid = GetParamIntValue("confno=", pszDialMsg);
  
  sprintf(sqls, "exec sp_CloseConf '%s','%s',%d", 
    szguid, szcreater, nconfid);
  SendExecSql2DB(sqls);
  
  return 1;
}

void ProcHTTPMsg(const char *pszDialMsg)
{
  char szCmd[32], szDialMsg[4096];

  memset(szDialMsg, 0, 4096);

  if (g_nURIEncodeDecode == 1)
  {
    if (UrlDecode(pszDialMsg, szDialMsg, strlen(pszDialMsg)) != TRUE)
      memcpy(szDialMsg, pszDialMsg, 4096);
  }
  else if (g_nURIEncodeDecode == 2)
  {
    if (ConvUniStrToAnsiStr(pszDialMsg, szDialMsg) == 0)
      memcpy(szDialMsg, pszDialMsg, 4096);
  }
  else if (g_nURIEncodeDecode == 3)
  {
    Base64Decode((UC *)szDialMsg, pszDialMsg);
  }
  else
  {
    memcpy(szDialMsg, pszDialMsg, 4096);
  }

  memset(szCmd, 0, 32);
  strncpy(szCmd, GetParamStrValue("cmd=", szDialMsg), 31);
  
  if (stricmp(szCmd, "login") == 0)
  {
    ProcWebLogin(szDialMsg);
  }
  else if (stricmp(szCmd, "logout") == 0)
  {
    ProcWebLogOut(szDialMsg);
  }
  else if (stricmp(szCmd, "dialout") == 0)
  {
    ProcWebDialOut(szDialMsg);
  }
  else if (stricmp(szCmd, "setdisturb") == 0)
  {
    ProcWebSetDisturb(szDialMsg);
  }
  else if (stricmp(szCmd, "setleave") == 0)
  {
    ProcWebSetLeave(szDialMsg);
  }
  else if (stricmp(szCmd, "answercall") == 0)
  {
    ProcWebAnswerCall(szDialMsg);
  }
  else if (stricmp(szCmd, "sethold") == 0)
  {
    ProcWebSetHold(szDialMsg);
  }
  else if (stricmp(szCmd, "reheld") == 0)
  {
    ProcWebReHeld(szDialMsg);
  }
  else if (stricmp(szCmd, "hangon") == 0)
  {
    ProcWebHangon(szDialMsg);
  }
  else if (stricmp(szCmd, "transfercall") == 0)
  {
    ProcWebTransferCall(szDialMsg);
  }
  else if (stricmp(szCmd, "stoptransfer") == 0)
  {
    ProcWebStopTransfer(szDialMsg);
  }
  else if (stricmp(szCmd, "completetransfer") == 0)
  {
    ProcWebCompleteTransfer(szDialMsg);
  }
  else if (stricmp(szCmd, "pickupcall") == 0)
  {
    ProcWebPickupCall(szDialMsg);
  }
  else if (stricmp(szCmd, "listencall") == 0)
  {
    ProcWebListenCall(szDialMsg);
  }
  else if (stricmp(szCmd, "insertcall") == 0)
  {
    ProcWebInsertCall(szDialMsg);
  }
  else if (stricmp(szCmd, "setacwtimer") == 0)
  {
    ProcWebSetACWTimer(szDialMsg);
  }
  else if (stricmp(szCmd, "setacwend") == 0)
  {
    ProcWebSetACWEnd(szDialMsg);
  }
  else if (stricmp(szCmd, "forceclear") == 0)
  {
    ProcWebForceClear(szDialMsg);
  }
  else if (stricmp(szCmd, "forcelogout") == 0)
  {
    ProcWebForceLogout(szDialMsg);
  }
  else if (stricmp(szCmd, "forceready") == 0)
  {
    ProcWebForceReady(szDialMsg);
  }
  else if (stricmp(szCmd, "forcebusy") == 0)
  {
    ProcWebForceBusy(szDialMsg);
  }
  else if (stricmp(szCmd, "sendmessage") == 0)
  {
    ProcWebSendMessage(szDialMsg);
  }
  else if (stricmp(szCmd, "setforwarding") == 0)
  {
    ProcWebSetForwarding(szDialMsg);
  }
  else if (stricmp(szCmd, "senddtmf") == 0)
  {
    ProcWebSendDTMF(szDialMsg);
  }
  else if (stricmp(szCmd, "transferivr") == 0)
  {
    ProcWebTransferIVR(szDialMsg);
  }
  else if (stricmp(szCmd, "pickupacdcall") == 0) //2015-12-14
  {
    ProcWebPickupACDCall(szDialMsg);
  }
  else if (stricmp(szCmd, "createconf") == 0) //2016-12-13
  {
    ProcWebCreateConf(szDialMsg);
  }
  else if (stricmp(szCmd, "joinconf") == 0) //2016-12-13
  {
    ProcWebJoinConf(szDialMsg);
  }
  else if (stricmp(szCmd, "unjoinconf") == 0) //2016-12-13
  {
    ProcWebUnJoinConf(szDialMsg);
  }
  else if (stricmp(szCmd, "closeconf") == 0) //2016-12-13
  {
    ProcWebCloseConf(szDialMsg);
  }
}
void ProcWEBSOCKETMsg(const char *pszDialMsg)
{
  char szCmd[32], szDialMsg[4096];
  
  memset(szDialMsg, 0, 4096);

  if (g_nURIEncodeDecode == 1)
  {
    if (UrlDecode(pszDialMsg, szDialMsg, strlen(pszDialMsg)) != TRUE)
      memcpy(szDialMsg, pszDialMsg, 4096);
  }
  else if (g_nURIEncodeDecode == 2)
  {
    if (ConvUniStrToAnsiStr(pszDialMsg, szDialMsg) == 0)
      memcpy(szDialMsg, pszDialMsg, 4096);
  }
  else if (g_nURIEncodeDecode == 3)
  {
    Base64Decode((UC *)szDialMsg, pszDialMsg);
  }
  else
  {
    memcpy(szDialMsg, pszDialMsg, 4096);
  }
  
  memset(szCmd, 0, 32);
  strncpy(szCmd, GetParamStrValue("cmd=", szDialMsg), 31);
  
  if (stricmp(szCmd, "connected") == 0)
  {
    ProcWebConnected(szDialMsg);
  }
  else if (stricmp(szCmd, "disconnected") == 0)
  {
    ProcWebDisconnected(szDialMsg);
  }
  else if (stricmp(szCmd, "login") == 0)
  {
    ProcWebLogin(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "legwork") == 0)
  {
    ProcWebLegwork(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "logout") == 0)
  {
    ProcWebLogOut(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "dialout") == 0)
  {
    ProcWebDialOut(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "setdisturb") == 0)
  {
    ProcWebSetDisturb(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "setleave") == 0)
  {
    ProcWebSetLeave(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "answercall") == 0)
  {
    ProcWebAnswerCall(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "sethold") == 0)
  {
    ProcWebSetHold(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "reheld") == 0)
  {
    ProcWebReHeld(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "hangon") == 0)
  {
    ProcWebHangon(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "transfercall") == 0)
  {
    ProcWebTransferCall(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "stoptransfer") == 0)
  {
    ProcWebStopTransfer(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "completetransfer") == 0)
  {
    ProcWebCompleteTransfer(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "pickupcall") == 0)
  {
    ProcWebPickupCall(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "listencall") == 0)
  {
    ProcWebListenCall(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "insertcall") == 0)
  {
    ProcWebInsertCall(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "setacwtimer") == 0)
  {
    ProcWebSetACWTimer(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "setacwend") == 0)
  {
    ProcWebSetACWEnd(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "forceclear") == 0)
  {
    ProcWebForceClear(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "forcelogout") == 0)
  {
    ProcWebForceLogout(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "forceready") == 0)
  {
    ProcWebForceReady(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "forcebusy") == 0)
  {
    ProcWebForceBusy(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "sendmessage") == 0)
  {
    ProcWebSendMessage(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "setforwarding") == 0)
  {
    ProcWebSetForwarding(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "senddtmf") == 0)
  {
    ProcWebSendDTMF(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "transferivr") == 0)
  {
    ProcWebTransferIVR(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "pickupacdcall") == 0) //2015-12-14
  {
    ProcWebPickupACDCall(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "createconf") == 0) //2016-12-13
  {
    ProcWebCreateConf(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "joinconf") == 0) //2016-12-13
  {
    ProcWebJoinConf(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "unjoinconf") == 0) //2016-12-13
  {
    ProcWebUnJoinConf(szDialMsg, 1);
  }
  else if (stricmp(szCmd, "closeconf") == 0) //2016-12-13
  {
    ProcWebCloseConf(szDialMsg, 1);
  }
}
//---------------處理ACD隊列---------------------------------------------------
//順序分配時將分配的坐席號設置到被叫號碼表
void SetAcdedSeatNo(int nAcd, CAgent *pAgent)
{
  pAcd->Calleds[0].nAG = pAgent->nAG;
  pAcd->CalledNum = 1;
}

//同時分配時將組內所有的坐席號設置到被叫號碼表
void SetGroupSeatNo(int nAcd)
{
  UC groupno, seatgroupno;
  int calledpoint = 0;

  groupno = pAcd->ACDRule.GroupNo[0];
  pAcd->ACDRule.AcdedGroupNo = groupno;
  seatgroupno = pAcd->ACDRule.SeatGroupNo;
  if (seatgroupno == 0 || groupno > 0)
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (groupno == pAG->GetGroupNo())
      {
        pAcd->Calleds[calledpoint].nAG = pAG->nAG;
        calledpoint ++;
        pAcd->CalledNum = calledpoint;
      }
    }
  }
  else
  {
    for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
    {
      if (seatgroupno == pAG->GetSeatGroupNo())
      {
        pAcd->Calleds[calledpoint].nAG = pAG->nAG;
        calledpoint ++;
        pAcd->CalledNum = calledpoint;
      }
    }
  }
}
//將需要呼叫的坐席設置到呼出隊列
int SetACDCalloutQue(int nAcd, int calledpoint, bool istheseatno)
{
  int ChIndex, nChn, nChn1, nQue, nAG=0xFFFF;
  UC ChType;
  int seatgroupno = pAcd->ACDRule.SeatGroupNo;

  nAG = pAcd->Calleds[calledpoint].nAG;

  if (pAcd->Session.MediaType > 0)
  {
    nQue = pCalloutQue->Get_Idle_nQue();
    if (pCalloutQue->isnQueAvail(nQue))
    {
      pQue->state = 1;
      pQue->timer = 0;
      pQue->RingTime = pAcd->ACDRule.RingTimeLen;
      pQue->CallerType = pAcd->Session.CallerType;
      pQue->CallType = pAcd->Session.CallType;
      pQue->CallerNo = pAcd->Session.CallerNo;
      strncpy(pQue->CalledNo, pAG->GetPhoneNo().C_Str(), MAX_TELECODE_LEN-1);
      pQue->OrgCalledNo = "";
      pQue->CalledNoPoint = 0;
      pQue->OutnChn = 0xFFFF;
      pQue->CancelId = 0;
      pQue->CalloutResult = 0;
      pQue->OrgCallType = 2; //2-分配坐席呼出
      pQue->nOut = 0xFFFF;
      pQue->nAcd = nAcd;
      pQue->nAG = nAG;
      
      pAG->nQue = nQue;
      SetAgentsvState(nAG, AG_SV_INRING);
      
      pQue->CallPoint = calledpoint;
      
      pAcd->Calleds[calledpoint].pQue_str = pQue;
      pAcd->Calleds[calledpoint].CalloutResult = CALLOUT_CALLING;
      
      return 0; //成功設置
    }
    else
    {
      return 2; //呼出緩沖溢出
    }
  }
  
  if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
  {
    char szCallData[1024];
    int lgchntype, lgchnno;

    //交換機方式時
    nChn = pAcd->Session.nChn; //發起轉接的通道號
    nChn1 = pAG->GetSeatnChn(); //轉接的目的通道號
    if (pBoxchn->isnChnAvail(nChn1))
    {
      if (pBoxchn->isTheChIdel(nChn1, pIVRCfg->isACDToAgentWhenChBlock) != 0)
      {
        return 1;
      }
      pChn1->CalloutId = 1; //通道呼出占用標志
      pAG->timer = 0;
      SetAgentsvState(nAG, AG_SV_ACDLOCK);
    }

    sprintf(szCallData, "$S_OrgCalledNo=%s;$S_CDRSerialNo=%s;$S_RecdFileName=%s;$S_DialParam=%s", pChn->CalledNo, pChn->CdrSerialNo, pChn->RecordFileName, pAcd->Session.Param.C_Str()); //2016-04-19增加傳遞原被叫號碼
    if (Transfer(nChn, 1, pAG->GetPhoneNo().C_Str(), szCallData) == 0)
    {
      pAG->TranAGId = 1; //設置呼叫到該坐席的是IVR轉接呼叫
      pAG->TrannChn = nChn;
      if (pBoxchn->isnChnAvail(nChn1))
      {
        pChn1->nAcd = nAcd;
        pChn1->CallData = pAcd->Session.Param;
      }

      if (pAG->GetClientId() != 0x06FF)
      {
        pBoxchn->GetLgChBynChn(pAcd->Session.nChn, lgchntype, lgchnno);
        sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%ld' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='%d' param='%s'/>", 
          pAG->GetClientId()<<16, pAcd->Session.SessionNo, lgchntype, lgchnno, pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(),
          pAcd->CallSeatType, pAcd->Session.FuncNo, pAcd->Session.Param.C_Str());
        SendMsg2AG(pAG->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
      }
      Send_Transfer_To_REC(nAcd, nAG);
      return 0;
    }
    else
    {
      if (pBoxchn->isnChnAvail(nChn1))
        pChn1->CalloutId = 0;
      return 2;
    }
  }
  else if (g_nSwitchType > 0)
  {
    char szCallData[1024];
    //交換機方式時
    nChn = pAcd->Session.nChn; //發起轉接的通道號
    
    if (pChn->VOC_TrunkId == 0) //2015-11-25 針對語音中繼的修改
    {
      nChn1 = pAG->GetSeatnChn(); //轉接的目的通道號
      if (pBoxchn->isnChnAvail(nChn1))
      {
        if (pBoxchn->isTheChIdel(nChn1, pIVRCfg->isACDToAgentWhenChBlock) != 0)
        {
          return 1;
        }
        if (pAG->m_Worker.DisturbId != 3)
          pChn1->CalloutId = 1; //通道呼出占用標志
        pAG->timer = 0;
        SetAgentsvState(nAG, AG_SV_ACDLOCK);
      }

      if (pBoxchn->isnChnAvail(nChn1))
      {
        pChn1->CallData = pAcd->Session.Param;
      }
      pAG->TranAGId = 1; //設置呼叫到該坐席的是IVR轉接呼叫
      pAG->TrannChn = nChn;
    
      strcpy(pAG->m_Seat.IVRPhone, pAcd->Session.CalledNo.C_Str());
    
      strcpy(pAG->m_Seat.CustPhone, pAcd->Session.CallerNo.C_Str()); //2014-01-13 針對IPO收不到主叫，而通過中繼監錄收主叫
    
      sprintf(szCallData, "$S_OrgCalledNo=%s;$S_CDRSerialNo=%s;$S_RecdFileName=%s;$S_DialParam=%s", pChn->CalledNo, pChn->CdrSerialNo, pChn->RecordFileName, pAcd->Session.Param.C_Str()); //2016-04-19增加傳遞原被叫號碼
      if (pAG->m_Worker.DisturbId == 3)
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pAG->GetPhoneNo().C_Str(), 1, szCallData, 1);
      else
        Send_SWTMSG_transfercall(nChn, pChn->DeviceID, pChn->ConnID, nChn1, pAG->GetPhoneNo().C_Str(), 1, szCallData, 0);
      pAG->nAcd = nAcd; //2015-10-14 用來拒接

      /* 2015-07-16 屏蔽不然會重復發送來電彈屏消息，因為后面的交換機呼叫事件也會彈屏
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s;param=%s",
          pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(), pAcd->Session.Param.C_Str());
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      */
      return 0;
    }
    else //2015-11-25 針對語音中繼的修改
    {
      //語音中繼發起的分配座席的處理
      int nChn2 = pBoxchn->Get_IdelChnNo_By_Route(pChn->RouteNo, ChType, ChIndex);
      if (!pBoxchn->isnChnAvail(nChn2) && pChn->OverflowRouteNo > 0)
      {
        nChn2 = pBoxchn->Get_IdelChnNo_By_Route(pChn->OverflowRouteNo, ChType, ChIndex);
      }
      if (!pBoxchn->isnChnAvail(nChn2))
      {
        return 1;
      }
      nQue = pCalloutQue->Get_Idle_nQue();
      if (!pCalloutQue->isnQueAvail(nQue))
      {
        return 1;
      }
      
      nChn1 = pAG->GetSeatnChn(); //轉接的目的座席通道號
      if (pBoxchn->isnChnAvail(nChn1))
      {
        pChn1->CalloutId = 1; //通道呼出占用標志
        pChn1->CallData = pAcd->Session.Param;
        pAG->TranAGId = 1; //設置呼叫到該坐席的是IVR轉接呼叫
        pAG->TrannChn = nChn;
      }
      //設置外呼隊列
      pQue->state = 1;
      pQue->timer = 0;
      pQue->RingTime = pAcd->ACDRule.RingTimeLen;
      pQue->CallerType = pAcd->Session.CallerType;
      pQue->CallType = pAcd->Session.CallType;
      pQue->CallerNo = pAcd->Session.CallerNo;
      strncpy(pQue->CalledNo, pAG->GetPhoneNo().C_Str(), MAX_TELECODE_LEN-1);
      pQue->OrgCalledNo = "";
      pQue->CalledNoPoint = 0;
      pQue->OutnChn = nChn2;
      pQue->CancelId = 0;
      pQue->CalloutResult = 0;
      pQue->OrgCallType = 2; //2-分配坐席呼出
      pQue->nOut = 0xFFFF;
      pQue->nAcd = nAcd;
      pQue->nAG = nAG;
      
      pAG->m_Seat.ACDByVocTrkId = 1; //2015-11-25 針對語音中繼的修改
      pAG->m_Seat.nVocTrknChn = nChn2; //2015-11-25 針對語音中繼的修改
      pAG->m_Seat.nCallInnChn = nChn; //外線通道號
      strcpy(pAG->m_Seat.CustPhone, pAcd->Session.CallerNo.C_Str()); //2015-11-25 針對語音中繼的修改，保留客戶號碼
      pAG->m_Seat.DialParam = pAcd->Session.Param;
      
      pAG->nQue = nQue;
      SetAgentsvState(nAG, AG_SV_INSEIZE);
      
      pQue->CallPoint = calledpoint;
      
      pAcd->Calleds[calledpoint].pQue_str = pQue;
      pAcd->Calleds[calledpoint].CalloutResult = CALLOUT_CALLING;
      
      pChn2->timer = 0;
      pChn2->CalloutId = 1; //通道呼出占用標志
      pChn2->nQue = nQue;
      strncpy(pChn2->CustPhone, pAcd->Session.CallerNo.C_Str(), MAX_TELECODE_LEN-1);
      
      return 0;
    }
  }

  //板卡方式時
  if (pAG->GetSeatType() == SEATTYPE_RT_TEL) //5-遠程座席
  {
    nChn1 = pAG->GetSeatnChn();
    if (!pBoxchn->isnChnAvail(nChn1))
    {
      //還沒有綁定到遠程坐席通道
      nChn1 = pBoxchn->Get_IdelChnNo_By_Route(pAG->GetRouteNo(), ChType, ChIndex);
    }
  }
  else
  {
    nChn1 = pAG->GetSeatnChn();
    if ((!pAG->isWantSeatType(SEATTYPE_AG_PC) && pBoxchn->isTheChIdel(nChn1, pIVRCfg->isACDToAgentWhenChBlock) != 0) 
      || (istheseatno == false && !pAG->isIdelForACD(seatgroupno)))
    {
      nChn1 = 0xFFFF;
    }
  }
  if (pBoxchn->isnChnAvail(nChn1))
  {
    nQue = pCalloutQue->Get_Idle_nQue();
    if (pCalloutQue->isnQueAvail(nQue))
    {
      pQue->state = 1;
      pQue->timer = 0;
      pQue->RingTime = pAcd->ACDRule.RingTimeLen;
      pQue->CallerType = pAcd->Session.CallerType;
      pQue->CallType = pAcd->Session.CallType;
      pQue->CallerNo = pAcd->Session.CallerNo;
      strncpy(pQue->CalledNo, pAG->GetPhoneNo().C_Str(), MAX_TELECODE_LEN-1);
      pQue->OrgCalledNo = "";
      pQue->CalledNoPoint = 0;
      pQue->OutnChn = nChn1;
      pQue->CancelId = 0;
      pQue->CalloutResult = 0;
      pQue->OrgCallType = 2; //2-分配坐席呼出
      pQue->nOut = 0xFFFF;
      pQue->nAcd = nAcd;
      pQue->nAG = nAG;
      
      if (pAG->GetSeatType() == SEATTYPE_RT_TEL) //5-遠程座席
      {
        pAG->m_Seat.nChn = nChn1; //綁定遠程坐席與呼出通道的關系
        pChn1->nAG = nAG;
      }
      pAG->nQue = nQue;
      SetAgentsvState(nAG, AG_SV_INSEIZE);
      
      pQue->CallPoint = calledpoint;

      pAcd->Calleds[calledpoint].pQue_str = pQue;
      pAcd->Calleds[calledpoint].CalloutResult = CALLOUT_CALLING;

      pChn1->timer = 0;
	    pChn1->CalloutId = 1; //通道呼出占用標志
	    pChn1->nQue = nQue;
      strncpy(pChn1->CustPhone, pAcd->Session.CallerNo.C_Str(), MAX_TELECODE_LEN-1);

      return 0; //成功設置
    }
    else
    {
      return 2; //呼出緩沖溢出
    }
  }
  return 1; //沒有中繼
}
void ClearCalloutIdBynAG(int nAG)
{
  if (!pAgentMng->isnAGAvail(nAG))
    return;
  memset(pAG->m_Seat.CustPhone, 0, MAX_TELECODE_LEN); //2014-01-13 針對IPO收不到主叫，而通過中繼監錄收主叫
  memset(pAG->m_Seat.IVRPhone, 0, MAX_TELECODE_LEN);

  pAG->m_Seat.ACDByVocTrkId = 0; //2015-11-25 針對語音中繼的修改,清除標志
  pAG->m_Seat.nVocTrknChn = 0xFFFF; //2015-11-25 針對語音中繼的修改
  pAG->m_Seat.nCallInnChn = 0xFFFF; //2015-11-25 針對語音中繼的修改
  pAG->m_Seat.nTranAGnChn = 0xFFFF; //2015-11-25 針對語音中繼的修改
  memset(pAG->m_Seat.CustPhone, 0, MAX_TELECODE_LEN); //2015-11-25 針對語音中繼的修改
  pAG->m_Seat.DialParam = "";
  
  int nChn = pAG->GetSeatnChn();
  if (pBoxchn->isnChnAvail(nChn))
  {
    pChn->CalloutId = 0;
  }
}
void DelFromQueue(int nAcd)
{
  pACDQueue->DeleACDQueue(nAcd);
  SendACDQueueInfoToAll();
}
//處理ACD分配記錄
void ProcACDData(int nAcd, int WaitCount)
{
  CAgent *pAgent=NULL, *pAgent1=NULL;
  char pszError[128];
  int i, j, nResult, nGetAgentResult, OnACDResult, nGetQueResult, CallingCount=0, FailCount=0;
  int onResult=OnACDFail;
  bool bTheSeatNo=false;
  int nChn, nChn1, nAG;

  nChn = pAcd->Session.nChn;
  if (!pBoxchn->isnChnAvail(nChn))
  {
    SendCallseatResult(nAcd, pAgent, WaitCount, OnACDFail, "nChn is not availability");
    DelFromQueue(nAcd);
    return;
  }
  switch (pAcd->acdState)
  {
    case 0: //初次等待分配坐席
    case 3: //重新等待分配坐席
      if (pAcd->ACDRule.CallMode == 3)
      {
        //取已登錄的坐席數
        sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='0' outchanno='%d' seatno='0' workerno='0' groupno='0' queueno='0' serialno='' param='' errorbuf=''/>", 
          pAcd->Session.SessionNo, pAcd->Session.CmdAddr, pChn->lgChnType, pChn->lgChnNo, OnACDGetSeat, pAgentMng->CountLoginAgent(pAcd->ACDRule.GroupNo));
        SendMsg2XML(MSG_oncallseat, pAcd->Session.SessionNo, SendMsgBuf);
        DelFromQueue(nAcd);
        break;
      }
      if (pAcd->StopId == 1)
      {
        DelFromQueue(nAcd);
        pChn->nAcd = 0xFFFF; //2014-01-12應該同時要清除
        ChCount.AcdCallGiveUpCount ++;
        ChCount.AcdCallTotalGiveUpTime = ChCount.AcdCallTotalGiveUpTime + time(0) - pAcd->ACDRule.QueueTime;
        if (ChCount.AcdCallGiveUpCount > 0)
        {
          ChCount.AcdCallAvgGiveUpTime = ChCount.AcdCallTotalGiveUpTime / ChCount.AcdCallGiveUpCount;
        }
        MyTrace(3, "WriteAbandonStatus for stop nAcd=%d nChn=%d", nAcd, nChn);
        WriteAbandonStatus(NULL, AG_STATUS_ABAND, pAcd->ACDRule.QueueTime, time(0));
        break;
      }
      //等待分配超時
      if (pAcd->waittimer > pAcd->ACDRule.WaitTimeLen)
      {
        SendCallseatResult(nAcd, pAgent, WaitCount, OnACDTimeOut, "time out1");
        DelFromQueue(nAcd);
        pChn->nAcd = 0xFFFF; //2014-01-12應該同時要清除
        if (pAcd->ACDRule.AcdedCount == 1)
        {
          ChCount.AcdCallFailCount ++;
          pAgentMng->AllCallCountParam.ACDFailCount++;
          SendSystemStatusMeteCount();
        }
        break;
      }
      if (pAcd->timer < 3) //分配失敗后間隔2秒
      {
        break;
      }
      //根據ACD規則分配坐席
      if (pAcd->ACDRule.CallMode == 0) //0-單個呼叫
      {
        if (pAcd->ACDRule.SeatNo.Compare("0") != 0 || pAcd->ACDRule.WorkerNo > 0)
        {
          //分配制定的坐席號或工號
          pAgent = GetAgentbyWorkerNoSeatNo(pAcd->ACDRule.SeatNo, pAcd->ACDRule.WorkerNo, pAcd->ACDRule.AcdRule, nResult);
          if (nResult == 0) //0-表示沒有該座席
          {
            OnACDResult = OnACDUNN;
            nGetAgentResult = 0;
            strcpy(pszError, "the workerno or seatno is unn");
            MyTrace(3, "nChn=%d GetAgentbyWorkerNoSeatNo nAcd=%d fail for unn", nChn, nAcd);
          }
          else if (nResult == 1) //1-表示有該座席且處于空閑狀態
          {
            OnACDResult = OnACDRing;
            nGetAgentResult = 1;
            pAgent->LockId = 0;
            pAcd->ACDRule.AcdedGroupNo = pAgent->GetGroupNo();
            pAgent->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
            MyTrace(3, "nChn=%d GetAgentbyWorkerNoSeatNo nAcd=%d nAG=%d", nChn, nAcd, pAgent->nAG);
          }
          else if (nResult == 3) //3-表示呼叫指定的座席且處于空閑狀態
          {
            OnACDResult = OnACDRing;
            nGetAgentResult = 1;
            pAgent->LockId = 0;
            bTheSeatNo = true; //用來區別呼叫的是指定的坐席，就不需要判斷是否為服務狀態了
            pAcd->ACDRule.AcdedGroupNo = pAgent->GetGroupNo();
            pAgent->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
            MyTrace(3, "nChn=%d GetAgentbyWorkerNoSeatNo nAcd=%d nAG=%d", nChn, nAcd, pAgent->nAG);
          }
          else //2-表示有該座席但處于非空閑狀態
          {
            OnACDResult = OnACDBusy;
            nGetAgentResult = 0;
            strcpy(pszError, "the workerno or seatno is busy");
            MyTrace(3, "nChn=%d GetAgentbyWorkerNoSeatNo nAcd=%d fail for busy", nChn, nAcd);
          }
          pAcd->ACDRule.AcdedCount++;
          if (pAcd->ACDRule.AcdedCount == 1)
          {
            if (pAgent)
            {
              int nGroupNo = pAgent->GetGroupNo();
              if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
              {
                pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
                SendOneGroupStatusMeteCountB(nGroupNo);
                SendSystemStatusMeteCount();
              }
            }

            pAgentMng->AllCallCountParam.ACDCount++;
            SendSystemStatusMeteCount();
          }
        }
        else
        {
          //按規則分配坐席
          pAcd->ACDRule.AcdedCount++;
          if (pAcd->ACDRule.AcdedCount == 1)
          {
            int nGroupNo = pAcd->ACDRule.GroupNo[0];
            if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
            {
              pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
              SendOneGroupStatusMeteCountB(nGroupNo);
              SendSystemStatusMeteCount();
            }

            pAgentMng->AllCallCountParam.ACDCount++;
            SendSystemStatusMeteCount();
          }
          if (pAcd->ACDRule.BusyWaitId <= 2) //2015-12-07
          {
            pAgent = GetIdelAgentbyRule(&(pAcd->ACDRule), pAcd->waittimer, pAcd->ringtimer);
          }
          else //2015-12-07
          {
            if (pChn->GetAgentAcdPoint < 0)
            {
              if (GetAgentListByRule(&pAcd->ACDRule, nChn) > 0)
              {
                char szTemp[1024], szTemp1[16];
                memset(szTemp, 0, 1024);
                strcpy(szTemp, "GetAgentListByRule=");
                for (i=0; i<MAX_AG_NUM; i++)
                {
                  if (pChn->GetAgentList[i] < 0)
                    break;
                  sprintf(szTemp1, "%d", pChn->GetAgentList[i]);
                  strcat(szTemp, szTemp1);
                }
                MyTrace(3, "%s", szTemp);
                pChn->GetAgentAcdPoint = 0;
                pAgent = pAgentMng->m_Agent[pChn->GetAgentList[pChn->GetAgentAcdPoint]];
                pChn->GetAgentAcdPoint = 1;
              }
              else
              {
                pAgent = NULL;
              }
            }
            else
            {
              if (pChn->GetAgentAcdPoint >= MAX_AG_NUM)
              {
                pChn->GetAgentAcdPoint = -1;
              }
              if (pChn->GetAgentList[pChn->GetAgentAcdPoint] < 0)
              {
                pChn->GetAgentAcdPoint = -1;
              }
              //2016-01-14 這里增加判斷以前取得坐席現在是否空閑
              if (pChn->GetAgentAcdPoint >= 0)
              {
                pAgent = pAgentMng->m_Agent[pChn->GetAgentList[pChn->GetAgentAcdPoint]];
                pChn->GetAgentAcdPoint++;
                if (pAgent->isIdelForACD(0) == false)
                {
                  pAgent = NULL;
                }
              }
              else
              {
                pAgent = NULL;
              }
            }
          }
          if (pAgent != NULL) //1-表示有該座席且處于空閑狀態
          {
            memcpy(pChn->AcdednAGList, pAcd->ACDRule.AcdednAGList, sizeof(pChn->AcdednAGList)); //2015-12-07
            OnACDResult = OnACDRing;
            nGetAgentResult = 1;
            pAgent->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
            MyTrace(3, "nChn=%d GetIdelAgentbyRule nAcd=%d nAG=%d", nChn, nAcd, pAgent->nAG);
          }
          else
          {
            OnACDResult = OnACDBusy;
            nGetAgentResult = 0;
            strcpy(pszError, "the workerno or seatno is busy");
            //MyTrace(3, "GetIdelAgentbyRule nAcd=%d fail",nAcd);
          }
        }
        
        if (nGetAgentResult == 0) //0-表示沒有符合規則的空閑座席
        {
          if (pAcd->ACDRule.BusyWaitId == 0) //且不等待
          {
            SendCallseatResult(nAcd, pAgent, WaitCount, OnACDResult, pszError);
            DelFromQueue(nAcd);
            pChn->nAcd = 0xFFFF; //2014-01-12應該同時要清除
            if (pAcd->ACDRule.AcdedCount == 1)
            {
              ChCount.AcdCallFailCount ++;
              pAgentMng->AllCallCountParam.ACDFailCount++;
              SendSystemStatusMeteCount();
            }
          }
          else
          {
            pAcd->timer = 0;
          }
          break;
        }
        SetAcdedSeatNo(nAcd, pAgent);
        //順呼處理
        nGetQueResult = SetACDCalloutQue(nAcd, 0, bTheSeatNo);
        if (nGetQueResult == 0)
        {
          CallingCount ++;
          pAcd->Calleds[0].CalloutResult = 0;
        }
        else if (nGetQueResult == 1)
          pAcd->Calleds[0].CalloutResult = CALLOUT_NOTRUNK;
        else
          pAcd->Calleds[0].CalloutResult = CALLOUT_FAIL;
      }
      else //1-同時呼叫指定話務員組號
      {
        //同呼處理
        pAcd->ACDRule.AcdedCount++;
        if (pAcd->ACDRule.AcdedCount == 1)
        {
          int nGroupNo = pAcd->ACDRule.GroupNo[0];
          if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
          {
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
            SendOneGroupStatusMeteCountB(nGroupNo);
            SendSystemStatusMeteCount();
          }

          pAgentMng->AllCallCountParam.ACDCount++;
          SendSystemStatusMeteCount();
        }
        for (i=0; i<pAcd->CalledNum; i++)
        {
          nGetQueResult = SetACDCalloutQue(nAcd, i, bTheSeatNo);
          if (nGetQueResult == 0)
          {
            CallingCount ++;
            pAcd->Calleds[i].CalloutResult = 0;
          }
          else if (nGetQueResult == 1)
            pAcd->Calleds[i].CalloutResult = CALLOUT_NOTRUNK;
          else
            pAcd->Calleds[i].CalloutResult = CALLOUT_FAIL;
        }
      }

      if (CallingCount == 0) //沒有空閑的坐席
      {
        MyTrace(3, "nChn=%d have not idel seat nAcd=%d", nChn, nAcd);
        if (pAcd->ACDRule.BusyWaitId == 0) //且不等待
        {
          SendCallseatResult(nAcd, pAgent, WaitCount, OnACDFail, "all busy");
          DelFromQueue(nAcd);
          pChn->nAcd = 0xFFFF; //2014-01-12應該同時要清除
          if (pAcd->ACDRule.AcdedCount == 1)
          {
            ChCount.AcdCallFailCount ++;
            pAgentMng->AllCallCountParam.ACDFailCount++;
            SendSystemStatusMeteCount();
          }
        }
        else
        {
          pAcd->timer = 0;
          DBUpdateCallCDR_WaitTime(nChn);
        }
        break;
      }
      
      pAcd->timer = 0;
      pAcd->ringtimer = 0;
      //MyTrace(3, "DispTimer nAcd=%d WaitTimeLen=%d RingTimeLen=%d waittimer=%d ringtimer=0",
      //  nAcd, pAcd->ACDRule.WaitTimeLen, pAcd->ACDRule.RingTimeLen, pAcd->waittimer);
      if (pAcd->acdState == 0)
        ChCount.AcdCallSuccCount ++;
      pAcd->acdState = 1; //1-等待呼叫結果
      pAcd->ACDRule.StartRingTime = time(0);
      SendACDQueueInfoToAll();
      if (pAcd->ACDRule.AcdRule != 7)
      {
        DBUpdateCallCDR_WaitTime(nChn);
        DBUpdateCallCDR_ACDTime(nChn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
        MyTrace(3, "GetAgent nChn=%d nAcd=%d CalloutResult=%d", nChn, nAcd, pAcd->Calleds[0].CalloutResult);
      }
      break;
    
    case 1: //等待呼叫結果
    case 2: //已經振鈴,等待應答結果
      if (pAcd->StopId == 1)
      {
        pChn->nAcd = 0xFFFF;
        for (i=0; i<pAcd->CalledNum; i++)
        {
          //取消呼出
          if (pAcd->Calleds[i].pQue_str != NULL)
          {
            pAcd->Calleds[i].pQue_str->CancelId = 1;
            nAG = pAcd->Calleds[i].nAG;
            if (g_nSwitchType == 0 && pAgentMng->isnAGAvail(nAG))
            {
              MyTrace(3, "WriteAbandonStatus 222 nAcd=%d nChn=%d nAG=%d", nAcd, nChn, nAG);
              WriteAbandonStatus(pAG, AG_STATUS_ABAND, pAcd->ACDRule.QueueTime, time(0));
            }
          }
          else
          {
            nAG = pAcd->Calleds[i].nAG;
            ClearCalloutIdBynAG(nAG);
            if (pAgentMng->isnAGAvail(nAG))
            {
              SendStopACDCallIn(nAG); //2015-10-14 add
              MyTrace(3, "WriteAbandonStatus 333 nAcd=%d nChn=%d nAG=%d", nAcd, nChn, nAG);
              WriteAbandonStatus(pAG, AG_STATUS_ABAND, pAcd->ACDRule.QueueTime, time(0));

              //2014-11-06解決外出值班時外線線掛機，坐席狀態不變空閑
              if (pAG->m_Worker.DisturbId == 3)
              {
                pAG->SetAgentsvState(AG_SV_IDEL);
                
                AgentStatusCount();
                SendOneGroupStatusMeteCount(pAG->nAG);
                SendSystemStatusMeteCount();
              }
              //2016-06-23解決排線到坐席后，呼叫坐席還沒回TAPI，外線就掛斷了，坐席狀態一直未AG_SV_ACDLOCK 的問題
              if (pAG->svState == AG_SV_ACDLOCK)
              {
                pAG->SetAgentsvState(AG_SV_IDEL);
                MyTrace(3, "set nAG=%d AG_SV_ACDLOCK to AG_SV_IDEL", nAG);
              }
              DispAgentStatus(pAG->nAG);
            }
            if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
            {
              StopTransfer(nChn, 0);
              Send_StopTransfer_To_REC(nChn, nAG);
            }
            else if (g_nSwitchType > 0)
            {
              //交換機取消轉接
              Send_SWTMSG_stoptransfer(nChn, pChn->DeviceID, pChn->ConnID);
            }
          }
          //釋放已應答的通道
          if (pAcd->Calleds[i].CalloutResult == CALLOUT_CONN)
          {
            pAgent = pAgentMng->m_Agent[pAcd->Calleds[i].nAG];
            Release_Chn(pAgent->GetSeatnChn(), 1, 0);
          }
        }
        DelFromQueue(nAcd);
        ChCount.AcdCallGiveUpCount ++;
        ChCount.AcdCallTotalGiveUpTime = ChCount.AcdCallTotalGiveUpTime + time(0) - pAcd->ACDRule.QueueTime;
        if (ChCount.AcdCallGiveUpCount > 0)
        {
          ChCount.AcdCallAvgGiveUpTime = ChCount.AcdCallTotalGiveUpTime / ChCount.AcdCallGiveUpCount;
        }
        break;
      }
      if (pAcd->waittimer > pAcd->ACDRule.WaitTimeLen && pAcd->ringtimer > pAcd->ACDRule.RingTimeLen)
      {
        pChn->nAcd = 0xFFFF;
        for (i=0; i<pAcd->CalledNum; i++)
        {
          //取消呼出
          if (pAcd->Calleds[i].pQue_str != NULL)
          {
            pAcd->Calleds[i].pQue_str->CancelId = 1;
            nAG = pAcd->Calleds[i].nAG;
            if (g_nSwitchType == 0 && pAgentMng->isnAGAvail(nAG))
            {
              WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
            }
          }
          else
          {
            ClearCalloutIdBynAG(pAcd->Calleds[i].nAG);
            if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
            {
              StopTransfer(nChn, 0);
              Send_StopTransfer_To_REC(nChn, pAcd->Calleds[i].nAG);
            }
            else if (g_nSwitchType > 0)
            {
              //交換機取消轉接
              Send_SWTMSG_stoptransfer(nChn, pChn->DeviceID, pChn->ConnID);
            }
          }
          //釋放已應答的通道
          if (pAcd->Calleds[i].CalloutResult == CALLOUT_CONN)
          {
            pAgent = pAgentMng->m_Agent[pAcd->Calleds[i].nAG];
            Release_Chn(pAgent->GetSeatnChn(), 1, 0);
          }
          pAgent = pAgentMng->m_Agent[pAcd->Calleds[i].nAG];
          pAgent->m_Worker.NoAnswerCount ++;
          SendStopACDCallIn(pAgent->nAG); //2015-10-14 add
          if (pAgent->m_Worker.DisturbId == 3) //2015-07-31解決外出值班無人應答超時坐席狀態不變空閑的問題
            SetAgentsvState(pAgent, AG_SV_IDEL);
          MyTrace(3, "set nAG=%d NoAnswerCount=%d for AutoSetAgentLeave", pAgent->nAG, pAgent->m_Worker.NoAnswerCount);
          if (pIVRCfg->AutoSetAgentLeaveNoAnsCount > 0 && pAgent->m_Worker.NoAnswerCount >= pIVRCfg->AutoSetAgentLeaveNoAnsCount && pAcd->ACDRule.SeatNo.Compare("0") == 0)
          {
            pAgent->m_Worker.SetLeave(9, "force leave"); //連續超過規定次數為應答強制離席
            DispAgentStatus(pAgent->nAG);
          }
        }
        pAgent = NULL;
        SendCallseatResult(nAcd, pAgent, WaitCount, OnACDTimeOut, "time out2");
        DelFromQueue(nAcd);
        break;
      }

      FailCount=0;
      for (i=0; i<pAcd->CalledNum; i++)
      {
        pAgent = pAgentMng->m_Agent[pAcd->Calleds[i].nAG];
        switch (pAcd->Calleds[i].CalloutResult)
        {
        case CALLOUT_RING: //2-正在振鈴
          //發送呼出振鈴結果
          if ((pAcd->CalledNum == 1 || pAcd->ACDRule.CallMode == 0) && pAcd->acdState == 1)
          {
            SendCallseatResult(nAcd, pAgent, WaitCount, OnACDRing, "");
            
            //if (pAcd->ACDRule.SeatNo.Compare("0") == 0) //當不是呼叫指定坐席號時累計話務量
            
            if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
            {
              WriteSeatStatus(pAgent, AG_STATUS_INRING, 0, 1, 0);
            }
            if (pIVRCfg->ControlCallModal == 0)
            {
              pAgent->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
            }
            else
            {
              pAgent->SetCDRSerialNo(pChn->CdrSerialNo);
              if (pAcd->ACDRule.AcdRule != 7)
              {
                DBUpdateCallCDR_RingTime(nChn);
                DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);
              }
            }
            SetAgentsvState(pAgent, AG_SV_INRING);
            pAcd->acdState = 2;
            SendACDQueueInfoToAll();
          }
          pAcd->Calleds[i].CalloutResult = 0;
          break;
        
        case CALLOUT_CONN: //3-用戶應答
          //發送呼出應答結果
          pChn->nAcd = 0xFFFF;
          if (pChn->lgChnType == CHANNEL_IVR && pChn->IncIVRInTranAGCountId == 1 && pChn->IncIVRInTranAGAnsCountId == 0)
          {
            CIVRCallCountParam *pIVRCallCountParam;
            if (gIVRCallCountParamMng.GetIvrNoType == 1)
            {
              pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGAnsCount(pChn->CalledNo, (int)(time(0)-pChn->IVRInTranAGTime));
            }
            else
            {
              pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGAnsCount(pChn->OrgCalledNo.C_Str(), (int)(time(0)-pChn->IVRInTranAGTime)); //2016-01-08
            }
            pChn->IncIVRInTranAGAnsCountId = 1;
            pChn->IncIVRInTranAGAbandonCountId = 1;
            SendIVRCallCountData(pIVRCallCountParam);
          }
          if (pAcd->ACDRule.AcdRule == 7 && pAgent->m_Worker.DisturbId == 2) //2016-04-15如果坐席應答了需要清除返回IVR狀態
            pAgent->m_Worker.DisturbId = 0;
          SendCallseatResult(nAcd, pAgent, 0, OnACDAnswer, "");
          SendAnswerCallResult(pAgent->nAG);
          pAgent->CanTransferId = 1;
          pAgent->m_Worker.NoAnswerCount = 0;
          pAgent->SetSrvLine(nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
          //if (pAcd->ACDRule.SeatNo.Compare("0") == 0) //當不是呼叫指定坐席號時累計話務量
          {
            //記錄最后應答的坐席序號
            pAgentMng->lastAnsnAG[0] = pAgent->nAG;
            
            if (pAcd->ACDRule.AcdedGroupNo > 0 && pAcd->ACDRule.AcdedGroupNo < MAX_AG_GROUP)
            {
              pAgentMng->lastAnsnAG[pAcd->ACDRule.AcdedGroupNo] = pAgent->nAG;
              pAgentMng->lastAnsGroupIndexnAG[pAcd->ACDRule.AcdedGroupNo][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
              MyTrace(3, "OnACDAnswer lastAnsnAG[%d]=%d svState=%d", pAcd->ACDRule.AcdedGroupNo, pAgent->nAG, pAgent->svState);
            }
            else
            {
              pAgentMng->lastAnsnAG[pAgent->GetGroupNo()] = pAgent->nAG;
              pAgentMng->lastAnsGroupIndexnAG[pAgent->GetGroupNo()][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
              MyTrace(3, "OnACDAnswer lastAnsnAG[%d]=%d", pAgent->GetGroupNo(), pAgent->nAG);
            }
            
            
            pAgentMng->lastAnsLevelnAG[0][0] = pAgent->nAG;
            pAgentMng->lastAnsLevelnAG[0][pAgent->GetLevel()] = pAgent->nAG;
            
            if (pAcd->ACDRule.AcdedGroupNo > 0 && pAcd->ACDRule.AcdedGroupNo < MAX_AG_GROUP)
            {
              pAgentMng->lastAnsLevelnAG[pAcd->ACDRule.AcdedGroupNo][pAgent->GetLevel()] = pAgent->nAG;
              pAgentMng->lastAnsLevelGroupIndexnAG[pAcd->ACDRule.AcdedGroupNo][pAcd->ACDRule.AcdedLevel][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
            }
            else
            {
              pAgentMng->lastAnsLevelnAG[pAgent->GetGroupNo()][pAgent->GetLevel()] = pAgent->nAG;
              pAgentMng->lastAnsLevelGroupIndexnAG[pAgent->GetGroupNo()][pAgent->GetLevel()][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
            }
            
            if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
            {
              WriteSeatStatus(pAgent, AG_STATUS_TALK, 1, 1, 1); //應答標志
            }
            if (pIVRCfg->ControlCallModal == 1)
            {
              if (pChn->lgChnType == 2)
              {
                DBUpdateCallCDR_CallInOut(nChn, 3);
              }
              if (g_nSwitchType > 0)
              {
                nChn1 = pAgent->GetSeatnChn();
                if (pBoxchn->isnChnAvail(nChn1))
                {
                  strcpy(pChn1->CdrSerialNo, pChn->CdrSerialNo);
                  strcpy(pChn1->RecordFileName, pChn->RecordFileName);
                }
                MyTrace(3, "nChn=%d nChn1=%d nAG=%d PickupId=%d CdrSerialNo=%s RecordFileName=%s",
                  nChn, nChn1, pAgent->nAG, pAgent->PickupId, pChn->CdrSerialNo, pChn->RecordFileName);
                if (pAgent->PickupId == 1)
                {
                  char szTemp[256];
                  strcpy(pAgent->RecordFileName, pChn->RecordFileName);
                  strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
                  
                  sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
                  CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
                  
                  if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
                  {
                    if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
                    {
                      DBUpdateCallCDR_RecdFile(pAgent->nAG);
                    }
                  }
                  pAgent->PickupId = 0;
                }
              }
              strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
              
              if (pAcd->ACDRule.AcdRule != 7)
                DBUpdateCallCDR_SeatAnsTime(pAgent->nAG, pAcd->ACDRule.AcdedGroupNo);
              
              if ((g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE) && pAgent->m_Worker.AutoRecordId == 1 && pAgent->m_Seat.AutoRecordId == 1)
              {
                char szTemp[256];

                strcpy(pAgent->RecordFileName, pChn->RecordFileName);

                sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
                CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
                
                if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
                {
                  if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
                  {
                    DBUpdateCallCDR_RecdFile(pAgent->nAG);
                  }
                }
              }
            }
          }
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          else
          {
            if (pAgent->svState != AG_SV_IDEL)
              SetAgentsvState(pAgent, AG_SV_CONN);
          }
          
          for (j=0; j<pAcd->CalledNum; j++)
          {
            if (i==j) continue;
            if (pAcd->Calleds[j].pQue_str != NULL)
              pAcd->Calleds[j].pQue_str->CancelId = 1;
            pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
            if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
            {
              pAcd->Calleds[j].CalloutResult = 0;
              Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
            }
            SetAgentsvState(pAgent1, AG_SV_IDEL);
          }
          if (pAcd->Session.FuncNo > 0) //功能號大于0時,需要調用制定流程
          {
            SendCallFlwEvent(2, nAcd, i);
          }
          pAcd->Calleds[i].CalloutResult = 0;
          DelFromQueue(nAcd);
          ChCount.AcdCallAnsCount ++;
          ChCount.AcdCallTotalAnsTime = ChCount.AcdCallTotalAnsTime + time(0) - pAcd->ACDRule.StartRingTime;
          ChCount.AcdCallTotalWaitTime = ChCount.AcdCallTotalAnsTime + time(0) - pAcd->ACDRule.QueueTime;
          if (ChCount.AcdCallAnsCount > 0)
          {
            ChCount.AcdCallAvgAnsTime = ChCount.AcdCallTotalAnsTime / ChCount.AcdCallAnsCount;
            ChCount.AcdCallAvgWaitTime = ChCount.AcdCallTotalWaitTime / ChCount.AcdCallAnsCount;
          }
          if (pAcd->ACDRule.TranCallFailCallBack == 1)
          {
            nChn1 = pAgent->GetSeatnChn();
            StopClearPlayDtmfBuf(nChn);
            nResult = RouterTalk(nChn, nChn1);
            if (nResult == 0)
            {
              pChn->RecMixerFlag = 1;
              pChn->LinkType[0] = 1;
              pChn->LinkChn[0] = nChn1;
              pChn->HoldingnChn = 0xFFFF;
              pChn1->RecMixerFlag = 1;
              pChn1->LinkType[0] = 1;
              pChn1->LinkChn[0] = nChn;
              pChn1->HoldingnChn = 0xFFFF;
            }
          }
          break;
        
        case CALLOUT_NOBODY: //6-無人應答
          onResult = OnACDTimeOut;
          strcpy(pszError, "CALLOUT_NOBODY");
          FailCount++;
          if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
          {
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          pAgent->m_Worker.NoAnswerCount ++;
          MyTrace(3, "set nAG=%d NoAnswerCount=%d for AutoSetAgentLeave", pAgent->nAG, pAgent->m_Worker.NoAnswerCount);
          if (pIVRCfg->AutoSetAgentLeaveNoAnsCount > 0 && pAgent->m_Worker.NoAnswerCount >= pIVRCfg->AutoSetAgentLeaveNoAnsCount && pAcd->ACDRule.SeatNo.Compare("0") == 0)
          {
            //2015-03-07
            if (pIVRCfg->isSetReadyStateonAgentSetDisturb > 0)
            {
              Send_SWTMSG_setagentstatus(nChn, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
            }
            pAgent->m_Worker.SetLeave(9, "force leave"); //連續超過規定次數為應答強制離席
            DispAgentStatus(pAgent->nAG);
          }
          
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          SendStopACDCallIn(pAgent->nAG); //2015-10-14 add
          break;
        
        case CALLOUT_SLB: //4-用戶市忙
        case CALLOUT_STB:	//5-用戶長忙
          onResult = OnACDBusy;
          strcpy(pszError, "CALLOUT_SLB or CALLOUT_STB");
          FailCount++;
          if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
          {
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          break;
        
        case CALLOUT_UNN:	//8-空號
          onResult = OnACDUNN;
          strcpy(pszError, "CALLOUT_UNN");
          FailCount++;
          if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
          {
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          break;
        
        case CALLOUT_NOTRUNK:	//7-無空閑出中繼
          onResult = OnACDFail;
          strcpy(pszError, "CALLOUT_NOTRUNK");
          FailCount++;
          if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
          {
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          break;
        
        case CALLOUT_FAIL: //9-失敗
        case CALLOUT_CNG:	//10-設備擁塞
        case CALLOUT_CGC:	//11-電路群擁塞
        case CALLOUT_ADI:	//12-地址不全
        case CALLOUT_SST:	//13-專用信號音
        case CALLOUT_OFF:	//14-用戶關機
        case CALLOUT_BAR:	//15-接入拒絕
          onResult = OnACDFail;
          strcpy(pszError, "CALLOUT_FAIL");
          FailCount++;
          if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
          {
            DBUpdateCallCDR_SeatAnsId(nChn, 9);
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          break;
        case CALLOUT_REFUSE: //16-坐席拒絕來電
          onResult = OnACDRefuse;
          strcpy(pszError, "CALLOUT_REFUSE");
          FailCount++;
          if (g_nSwitchType == 0 || g_nSwitchType == PORT_NOCTI_PBX_TYPE)
          {
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          if (pAgent->m_Worker.DisturbId == 3)
            SetAgentsvState(pAgent, AG_SV_IDEL);
          SendStopACDCallIn(pAgent->nAG); //2015-10-14 add
          break;
        }
      }
      if (pAcd->state == 1
        && ((pAcd->ACDRule.CallMode == 1 && FailCount >= pAcd->CalledNum) ||(pAcd->ACDRule.CallMode == 0 && FailCount == 1)))
      {
        ClearCalloutIdBynAG(pAcd->Calleds[0].nAG);
        if (pAcd->ACDRule.CallMode == 0 || pAcd->CalledNum == 1)
        {
          //順呼或只有1個被叫號碼時
          if (pAcd->waittimer < pAcd->ACDRule.WaitTimeLen  
            && (pAcd->ACDRule.BusyWaitId >= 1 || onResult == OnACDTimeOut || onResult == OnACDBusy || onResult == OnACDFail || onResult == OnACDRefuse))
          {
            //未到等待時長,且全忙時等待或超時無人應答、失敗,繼續返回分配
            pAcd->acdState = 3;
            pAcd->timer = 0;
            MyTrace(3, "set nAcd=%d acdState=3",nAcd);
            break;
          }
        }
        else
        {
          //同呼時
          if (pAcd->waittimer < pAcd->ACDRule.WaitTimeLen && pAcd->ACDRule.BusyWaitId >= 1) //2016-01-14這里改為大于0度等待超時
          {
            //未到等待時長,且全忙時等待,繼續返回分配
            pAcd->acdState = 3;
            pAcd->timer = 0;
            MyTrace(3, "set nAcd=%d acdState=3",nAcd);
            break;
          }
        }
        pChn->nAcd = 0xFFFF;
        SendCallseatResult(nAcd, pAgent, WaitCount, onResult, pszError);
        DelFromQueue(nAcd);
      }
      if (g_nSwitchType > 0 
        && pAcd->ringtimer > pAcd->ACDRule.RingTimeLen
        && pAcd->Calleds[0].CalloutResult == 0)
      {
        //每個坐席的振鈴超時
        pAcd->Calleds[0].CalloutResult = CALLOUT_NOBODY;
        if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
        {
          StopTransfer(nChn, 0);
          Send_StopTransfer_To_REC(nChn, pAcd->Calleds[0].nAG);
        }
        else
        {
          Send_SWTMSG_stoptransfer(nChn, pChn->DeviceID, pChn->ConnID);
        }
        
        MyTrace(3, "set nAcd=%d CalloutResult=CALLOUT_NOBODY for switch",nAcd);
      }
      break;
  }
}
void ProcIMACDData(int nAcd, int WaitCount)
{
  CAgent *pAgent=NULL;
  char pszError[128];
  int i, j, nResult, nGetAgentResult, OnACDResult, nGetQueResult, CallingCount=0, FailCount=0;
  int onResult=OnACDFail;
  bool bTheSeatNo=false;
  int nAG;

  switch (pAcd->acdState)
  {
    case 0: //初次等待分配坐席
    case 3: //重新等待分配坐席
      if (pAcd->StopId == 1)
      {
        DelFromQueue(nAcd);
        ChCount.AcdCallGiveUpCount ++;
        ChCount.AcdCallTotalGiveUpTime = ChCount.AcdCallTotalGiveUpTime + time(0) - pAcd->ACDRule.QueueTime;
        if (ChCount.AcdCallGiveUpCount > 0)
        {
          ChCount.AcdCallAvgGiveUpTime = ChCount.AcdCallTotalGiveUpTime / ChCount.AcdCallGiveUpCount;
        }
        MyTrace(3, "WriteAbandonStatus for stop nAcd=%d", nAcd);
        WriteAbandonStatus(NULL, AG_STATUS_ABAND, pAcd->ACDRule.QueueTime, time(0));
        break;
      }
      //等待分配超時
      if (pAcd->waittimer > pAcd->ACDRule.WaitTimeLen)
      {
        SendCallseatResult(nAcd, pAgent, WaitCount, OnACDTimeOut, "time out1");
        DelFromQueue(nAcd);
        if (pAcd->ACDRule.AcdedCount == 1)
        {
          ChCount.AcdCallFailCount ++;
          pAgentMng->AllCallCountParam.ACDFailCount++;
          SendSystemStatusMeteCount();
        }
        break;
      }
      if (pAcd->timer < 3) //分配失敗后間隔2秒
      {
        break;
      }
      //根據ACD規則分配坐席
      if (pAcd->ACDRule.SeatNo.Compare("0") != 0 || pAcd->ACDRule.WorkerNo > 0)
      {
        //分配制定的坐席號或工號
        pAgent = GetAgentbyWorkerNoSeatNo(pAcd->ACDRule.SeatNo, pAcd->ACDRule.WorkerNo, pAcd->ACDRule.AcdRule, nResult, true);
        if (nResult == 0) //0-表示沒有該座席
        {
          OnACDResult = OnACDUNN;
          nGetAgentResult = 0;
          strcpy(pszError, "the workerno or seatno is unn");
          MyTrace(3, "GetAgentbyWorkerNoSeatNo nAcd=%d fail for unn", nAcd);
        }
        else if (nResult == 1) //1-表示有該座席且處于空閑狀態
        {
          OnACDResult = OnACDRing;
          nGetAgentResult = 1;
          pAgent->LockId = 0;
          pAcd->ACDRule.AcdedGroupNo = pAgent->GetGroupNo();
          pAgent->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
          MyTrace(3, "GetAgentbyWorkerNoSeatNo nAcd=%d nAG=%d", nAcd, pAgent->nAG);
        }
        else if (nResult == 3) //3-表示呼叫指定的座席且處于空閑狀態
        {
          OnACDResult = OnACDRing;
          nGetAgentResult = 1;
          pAgent->LockId = 0;
          bTheSeatNo = true; //用來區別呼叫的是指定的坐席，就不需要判斷是否為服務狀態了
          pAcd->ACDRule.AcdedGroupNo = pAgent->GetGroupNo();
          pAgent->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
          MyTrace(3, "GetAgentbyWorkerNoSeatNo nAcd=%d nAG=%d", nAcd, pAgent->nAG);
        }
        else //2-表示有該座席但處于非空閑狀態
        {
          OnACDResult = OnACDBusy;
          nGetAgentResult = 0;
          strcpy(pszError, "the workerno or seatno is busy");
          MyTrace(3, "GetAgentbyWorkerNoSeatNo nAcd=%d fail for busy", nAcd);
        }
        pAcd->ACDRule.AcdedCount++;
        if (pAcd->ACDRule.AcdedCount == 1)
        {
          if (pAgent)
          {
            int nGroupNo = pAgent->GetGroupNo();
            if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
            {
              pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
              SendOneGroupStatusMeteCountB(nGroupNo);
              SendSystemStatusMeteCount();
            }
          }

          pAgentMng->AllCallCountParam.ACDCount++;
          SendSystemStatusMeteCount();
        }
      }
      else
      {
        //按規則分配坐席
        pAcd->ACDRule.AcdedCount++;
        if (pAcd->ACDRule.AcdedCount == 1)
        {
          int nGroupNo = pAcd->ACDRule.GroupNo[0];
          if (nGroupNo > 0 && nGroupNo < MAX_AG_GROUP)
          {
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount ++;
            SendOneGroupStatusMeteCountB(nGroupNo);
            SendSystemStatusMeteCount();
          }

          pAgentMng->AllCallCountParam.ACDCount++;
          SendSystemStatusMeteCount();
        }
        pAgent = GetIdelAgentbyRule(&(pAcd->ACDRule), pAcd->waittimer, pAcd->ringtimer, true);
        if (pAgent != NULL) //1-表示有該座席且處于空閑狀態
        {
          OnACDResult = OnACDRing;
          nGetAgentResult = 1;
          pAgent->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
          MyTrace(3, "GetIdelAgentbyRule nAcd=%d nAG=%d", nAcd, pAgent->nAG);
        }
        else
        {
          OnACDResult = OnACDBusy;
          nGetAgentResult = 0;
          strcpy(pszError, "the workerno or seatno is busy");
        }
      }
      
      if (nGetAgentResult == 0) //0-表示沒有符合規則的空閑座席
      {
        if (pAcd->ACDRule.BusyWaitId == 0) //且不等待
        {
          SendCallseatResult(nAcd, pAgent, WaitCount, OnACDResult, pszError);
          DelFromQueue(nAcd);
          if (pAcd->ACDRule.AcdedCount == 1)
          {
            ChCount.AcdCallFailCount ++;
            pAgentMng->AllCallCountParam.ACDFailCount++;
            SendSystemStatusMeteCount();
          }
        }
        else
        {
          pAcd->timer = 0;
        }
        break;
      }
      SetAcdedSeatNo(nAcd, pAgent);
      //順呼處理
      nGetQueResult = SetACDCalloutQue(nAcd, 0, bTheSeatNo);
      if (nGetQueResult == 0)
      {
        CallingCount ++;
        pAcd->Calleds[0].CalloutResult = 0;
      }
      else if (nGetQueResult == 1)
        pAcd->Calleds[0].CalloutResult = CALLOUT_NOTRUNK;
      else
        pAcd->Calleds[0].CalloutResult = CALLOUT_FAIL;

      if (CallingCount == 0) //沒有空閑的坐席
      {
        MyTrace(3, "have not idel seat nAcd=%d", nAcd);
        if (pAcd->ACDRule.BusyWaitId == 0) //且不等待
        {
          SendCallseatResult(nAcd, pAgent, WaitCount, OnACDFail, "all busy");
          DelFromQueue(nAcd);
          if (pAcd->ACDRule.AcdedCount == 1)
          {
            ChCount.AcdCallFailCount ++;
            pAgentMng->AllCallCountParam.ACDFailCount++;
            SendSystemStatusMeteCount();
          }
        }
        else
        {
          pAcd->timer = 0;
          //DBUpdateCallCDR_WaitTime(nChn);
        }
        break;
      }
      
      pAcd->timer = 0;
      pAcd->ringtimer = 0;
      if (pAcd->acdState == 0)
        ChCount.AcdCallSuccCount ++;
      pAcd->acdState = 1; //1-等待呼叫結果
      pAcd->ACDRule.StartRingTime = time(0);
      SendACDQueueInfoToAll();

      //DBUpdateCallCDR_WaitTime(nChn);
      //DBUpdateCallCDR_ACDTime(nChn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
      MyTrace(3, "GetAgent nAcd=%d CalloutResult=%d", nAcd, pAcd->Calleds[0].CalloutResult);

      break;
    
    case 1: //等待呼叫結果
    case 2: //已經振鈴,等待應答結果
      if (pAcd->StopId == 1)
      {
        for (i=0; i<pAcd->CalledNum; i++)
        {
          //取消呼出
          if (pAcd->Calleds[i].pQue_str != NULL)
          {
            pAcd->Calleds[i].pQue_str->CancelId = 1;
            nAG = pAcd->Calleds[i].nAG;
            if (pAgentMng->isnAGAvail(nAG))
            {
              MyTrace(3, "WriteAbandonStatus 222 nAcd=%d nAG=%d", nAcd, nAG);
              WriteAbandonStatus(pAG, AG_STATUS_ABAND, pAcd->ACDRule.QueueTime, time(0));
            }
          }
          else
          {
            nAG = pAcd->Calleds[i].nAG;
            ClearCalloutIdBynAG(nAG);
            if (pAgentMng->isnAGAvail(nAG))
            {
              SendStopACDCallIn(nAG); //2015-10-14 add
              MyTrace(3, "WriteAbandonStatus 333 nAcd=%d nAG=%d", nAcd, nAG);
              WriteAbandonStatus(pAG, AG_STATUS_ABAND, pAcd->ACDRule.QueueTime, time(0));
            }
          }
        }
        DelFromQueue(nAcd);
        ChCount.AcdCallGiveUpCount ++;
        ChCount.AcdCallTotalGiveUpTime = ChCount.AcdCallTotalGiveUpTime + time(0) - pAcd->ACDRule.QueueTime;
        if (ChCount.AcdCallGiveUpCount > 0)
        {
          ChCount.AcdCallAvgGiveUpTime = ChCount.AcdCallTotalGiveUpTime / ChCount.AcdCallGiveUpCount;
        }
        break;
      }
      if (pAcd->waittimer > pAcd->ACDRule.WaitTimeLen && pAcd->ringtimer > pAcd->ACDRule.RingTimeLen)
      {
        for (i=0; i<pAcd->CalledNum; i++)
        {
          //取消呼出
          if (pAcd->Calleds[i].pQue_str != NULL)
          {
            pAcd->Calleds[i].pQue_str->CancelId = 1;
            nAG = pAcd->Calleds[i].nAG;
            if (pAgentMng->isnAGAvail(nAG))
            {
              WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
            }
          }
          pAgent = pAgentMng->m_Agent[pAcd->Calleds[i].nAG];
          pAgent->m_Worker.NoAnswerCount ++;
          MyTrace(3, "set nAG=%d NoAnswerCount=%d for AutoSetAgentLeave", pAgent->nAG, pAgent->m_Worker.NoAnswerCount);
          if (pIVRCfg->AutoSetAgentLeaveNoAnsCount > 0 && pAgent->m_Worker.NoAnswerCount >= pIVRCfg->AutoSetAgentLeaveNoAnsCount && pAcd->ACDRule.SeatNo.Compare("0") == 0)
          {
            pAgent->m_Worker.SetLeave(9, "force leave"); //連續超過規定次數為應答強制離席
            DispAgentStatus(pAgent->nAG);
          }
        }
        pAgent = NULL;
        SendCallseatResult(nAcd, pAgent, WaitCount, OnACDTimeOut, "time out2");
        DelFromQueue(nAcd);
        break;
      }

      FailCount=0;
      for (i=0; i<pAcd->CalledNum; i++)
      {
        pAgent = pAgentMng->m_Agent[pAcd->Calleds[i].nAG];
        switch (pAcd->Calleds[i].CalloutResult)
        {
        case CALLOUT_RING: //2-正在振鈴
          //發送呼出振鈴結果
          if ((pAcd->CalledNum == 1 || pAcd->ACDRule.CallMode == 0) && pAcd->acdState == 1)
          {
            SendCallseatResult(nAcd, pAgent, WaitCount, OnACDRing, "");
            
            if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
            {
              WriteSeatStatus(pAgent, AG_STATUS_INRING, 0, 1, 0);
            }
            //pAgent->SetCDRSerialNo(pChn->CdrSerialNo);
            //DBUpdateCallCDR_RingTime(nChn);
            //DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);
            SetAgentsvState(pAgent, AG_SV_INRING);
            pAcd->acdState = 2;
            SendACDQueueInfoToAll();
          }
          pAcd->Calleds[i].CalloutResult = 0;
          break;
        
        case CALLOUT_CONN: //3-用戶應答
          SendCallseatResult(nAcd, pAgent, 0, OnACDAnswer, "");
          SendAnswerCallResult(pAgent->nAG);
          strcpy(pAgent->IMId, pAcd->Session.CRDSerialNo.C_Str());
          pAgent->CanTransferId = 1;
          pAgent->m_Worker.NoAnswerCount = 0;
          //pAgent->SetSrvLine(nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
          //if (pAcd->ACDRule.SeatNo.Compare("0") == 0) //當不是呼叫指定坐席號時累計話務量
          {
            //記錄最后應答的坐席序號
            pAgentMng->lastAnsnAG[0] = pAgent->nAG;
            
            if (pAcd->ACDRule.AcdedGroupNo > 0 && pAcd->ACDRule.AcdedGroupNo < MAX_AG_GROUP)
            {
              pAgentMng->lastAnsnAG[pAcd->ACDRule.AcdedGroupNo] = pAgent->nAG;
              pAgentMng->lastAnsGroupIndexnAG[pAcd->ACDRule.AcdedGroupNo][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
              MyTrace(3, "OnACDAnswer lastAnsnAG[%d]=%d svState=%d", pAcd->ACDRule.AcdedGroupNo, pAgent->nAG, pAgent->svState);
            }
            else
            {
              pAgentMng->lastAnsnAG[pAgent->GetGroupNo()] = pAgent->nAG;
              pAgentMng->lastAnsGroupIndexnAG[pAgent->GetGroupNo()][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
              MyTrace(3, "OnACDAnswer lastAnsnAG[%d]=%d", pAgent->GetGroupNo(), pAgent->nAG);
            }
            
            
            pAgentMng->lastAnsLevelnAG[0][0] = pAgent->nAG;
            pAgentMng->lastAnsLevelnAG[0][pAgent->GetLevel()] = pAgent->nAG;
            
            if (pAcd->ACDRule.AcdedGroupNo > 0 && pAcd->ACDRule.AcdedGroupNo < MAX_AG_GROUP)
            {
              pAgentMng->lastAnsLevelnAG[pAcd->ACDRule.AcdedGroupNo][pAgent->GetLevel()] = pAgent->nAG;
              pAgentMng->lastAnsLevelGroupIndexnAG[pAcd->ACDRule.AcdedGroupNo][pAcd->ACDRule.AcdedLevel][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
            }
            else
            {
              pAgentMng->lastAnsLevelnAG[pAgent->GetGroupNo()][pAgent->GetLevel()] = pAgent->nAG;
              pAgentMng->lastAnsLevelGroupIndexnAG[pAgent->GetGroupNo()][pAgent->GetLevel()][pAcd->ACDRule.AcdedGroupIndex] = pAgent->nAG;
            }
            
            WriteSeatStatus(pAgent, AG_STATUS_TALK, 1, 1, 1); //應答標志
          }
          if (pAgent->svState != AG_SV_IDEL)
          {
            if (pAcd->Session.MediaType == 1)
              SetAgentsvState(pAgent, AG_SV_IMCHAT);
            else if (pAcd->Session.MediaType == 2)
              SetAgentsvState(pAgent, AG_SV_PROC_EMAIL);
            else if (pAcd->Session.MediaType == 3)
              SetAgentsvState(pAgent, AG_SV_PROC_FAX);
            DispAgentStatus(pAgent->nAG);
          }
          
          for (j=0; j<pAcd->CalledNum; j++)
          {
            if (i==j) continue;
            if (pAcd->Calleds[j].pQue_str != NULL)
              pAcd->Calleds[j].pQue_str->CancelId = 1;
          }
          pAcd->Calleds[i].CalloutResult = 0;
          DelFromQueue(nAcd);
          ChCount.AcdCallAnsCount ++;
          ChCount.AcdCallTotalAnsTime = ChCount.AcdCallTotalAnsTime + time(0) - pAcd->ACDRule.StartRingTime;
          ChCount.AcdCallTotalWaitTime = ChCount.AcdCallTotalAnsTime + time(0) - pAcd->ACDRule.QueueTime;
          if (ChCount.AcdCallAnsCount > 0)
          {
            ChCount.AcdCallAvgAnsTime = ChCount.AcdCallTotalAnsTime / ChCount.AcdCallAnsCount;
            ChCount.AcdCallAvgWaitTime = ChCount.AcdCallTotalWaitTime / ChCount.AcdCallAnsCount;
          }
          break;
        
        case CALLOUT_NOBODY: //6-無人應答
          onResult = OnACDTimeOut;
          strcpy(pszError, "CALLOUT_NOBODY");
          FailCount++;
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          pAgent->m_Worker.NoAnswerCount ++;
          MyTrace(3, "set nAG=%d NoAnswerCount=%d for AutoSetAgentLeave", pAgent->nAG, pAgent->m_Worker.NoAnswerCount);
          if (pIVRCfg->AutoSetAgentLeaveNoAnsCount > 0 && pAgent->m_Worker.NoAnswerCount >= pIVRCfg->AutoSetAgentLeaveNoAnsCount && pAcd->ACDRule.SeatNo.Compare("0") == 0)
          {
            pAgent->m_Worker.SetLeave(9, "force leave"); //連續超過規定次數為應答強制離席
          }
          
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          SetAgentsvState(pAgent, AG_SV_IDEL);
          DispAgentStatus(pAgent->nAG);
          break;
        
        case CALLOUT_SLB: //4-用戶市忙
        case CALLOUT_STB:	//5-用戶長忙
          onResult = OnACDBusy;
          strcpy(pszError, "CALLOUT_SLB or CALLOUT_STB");
          FailCount++;
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          SetAgentsvState(pAgent, AG_SV_IDEL);
          DispAgentStatus(pAgent->nAG);
          break;
        
        case CALLOUT_UNN:	//8-空號
          onResult = OnACDUNN;
          strcpy(pszError, "CALLOUT_UNN");
          FailCount++;
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          SetAgentsvState(pAgent, AG_SV_IDEL);
          DispAgentStatus(pAgent->nAG);
          break;
        
        case CALLOUT_NOTRUNK:	//7-無空閑出中繼
          onResult = OnACDFail;
          strcpy(pszError, "CALLOUT_NOTRUNK");
          FailCount++;
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          SetAgentsvState(pAgent, AG_SV_IDEL);
          DispAgentStatus(pAgent->nAG);
          break;
        
        case CALLOUT_FAIL: //9-失敗
        case CALLOUT_CNG:	//10-設備擁塞
        case CALLOUT_CGC:	//11-電路群擁塞
        case CALLOUT_ADI:	//12-地址不全
        case CALLOUT_SST:	//13-專用信號音
        case CALLOUT_OFF:	//14-用戶關機
        case CALLOUT_BAR:	//15-接入拒絕
          onResult = OnACDFail;
          strcpy(pszError, "CALLOUT_FAIL");
          FailCount++;
          //DBUpdateCallCDR_SeatAnsId(nChn, 9);
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          SetAgentsvState(pAgent, AG_SV_IDEL);
          DispAgentStatus(pAgent->nAG);
          break;
        case CALLOUT_REFUSE: //16-坐席拒絕來電
          onResult = OnACDRefuse;
          strcpy(pszError, "CALLOUT_REFUSE");
          FailCount++;
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          if (pAcd->ACDRule.CallMode == 0)
            pAcd->Calleds[i].CalloutResult = 0;
          
          SetAgentsvState(pAgent, AG_SV_IDEL);
          DispAgentStatus(pAgent->nAG);
          break;
        }
      }
      if (pAcd->state == 1 && pAcd->ACDRule.CallMode == 0 && FailCount == 1)
      {
        ClearCalloutIdBynAG(pAcd->Calleds[0].nAG);
          //順呼或只有1個被叫號碼時
        if (pAcd->waittimer < pAcd->ACDRule.WaitTimeLen  
          && (pAcd->ACDRule.BusyWaitId == 1 || onResult == OnACDTimeOut || onResult == OnACDBusy || onResult == OnACDFail))
        {
          //未到等待時長,且全忙時等待或超時無人應答、失敗,繼續返回分配
          pAcd->acdState = 3;
          pAcd->timer = 0;
          MyTrace(3, "set nAcd=%d acdState=3",nAcd);
          break;
        }
        SendCallseatResult(nAcd, pAgent, WaitCount, onResult, pszError);
        DelFromQueue(nAcd);
      }
      if (pAcd->ringtimer > pAcd->ACDRule.RingTimeLen
        && pAcd->Calleds[0].CalloutResult == 0)
      {
        //每個坐席的振鈴超時
        pAcd->Calleds[0].CalloutResult = CALLOUT_NOBODY;
        
        MyTrace(3, "set nAcd=%d CalloutResult=CALLOUT_NOBODY for switch",nAcd);
      }
      break;
  }
}

//處理ACD隊列
void ProcAcdQueue()
{
  US nAcd=0, Next;
  int waitcount=0;

  Next = pAcd->Next;
  while (pACDQueue->isnAcdAvail(Next))
  {
    if (pNext->Session.MediaType == 0)
      ProcACDData(Next, waitcount);
    else
      ProcIMACDData(Next, waitcount);
    waitcount++;
    Next = pNext->Next;
  }
}

void GetACDQueue(int groupno, int &acdwaitcount, int &acdwaitans)
{
  US nAcd=0, Next;
  int waitcount=0;
  
  acdwaitcount = 0;
  acdwaitans = 0;

  Next = pAcd->Next;
  while (pACDQueue->isnAcdAvail(Next))
  {
    if (pNext->acdState == 0 || pNext->acdState == 3)
    {
      acdwaitcount ++;
    }
    else if (pNext->acdState == 2 || pNext->acdState == 1)
    {
      acdwaitans ++;
    }
    Next = pNext->Next;
  }
}

//設置坐席轉接呼叫失敗后重新返回坐席的分配參數
void SetTranCallFailReturnTypeACDData(int nChn, int nAG, int TranCallFailReturn)
{
  int nAcd = pACDQueue->Get_Idle_nAcd();
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    return;
  }
  if (!pAgentMng->isnAGAvail(nAG))
  {
    return;
  }

  pAcd->state = 1;
  pAcd->acdState = 0;
  pAcd->waittimer = 0;
  pAcd->ringtimer = 0;
  pAcd->StopId = 0;
  pAcd->Prior = 0xFFFF;
  pAcd->Next = 0xFFFF;

  pAcd->Session.MediaType = 0;
  pAcd->Session.SessionNo = 0;
  pAcd->Session.CmdAddr = 0;
  pAcd->Session.nChn = nChn;
  pAcd->Session.DialSerialNo = GetCallOutSerialNo(nChn);
  pAcd->Session.SerialNo = 0;
  pAcd->Session.CallerType = 0;
  pAcd->Session.CallType = 0;
  pAcd->Session.CallerNo = pChn->CallerNo;
  pAcd->Session.CalledNo = pChn->CalledNo;
  pAcd->Session.CmdId = 1; //callseat指令
  pAcd->Session.VxmlId = 1;
  pAcd->Session.FuncNo = 0;
  pAcd->Session.ServiceType = "";
  pAcd->Session.SrvType = 0;
  pAcd->Session.SrvSubType = 0;
  pAcd->Session.CRDSerialNo = "";
  pAcd->Session.Param = "";

  pAcd->ACDRule.TranCallFailCallBack = 1;
  pAcd->ACDRule.AcdedCount = 0;
  pAcd->ACDRule.SeatType = 0;
  
  for (int i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = 0xFF;
    pAcd->ACDRule.Level[i] = 0;
    pAcd->ACDRule.NowLevel[i] = 0;
  }
  if (TranCallFailReturn == 1)
    pAcd->ACDRule.SeatNo = pAG->GetSeatNo();
  else
    pAcd->ACDRule.GroupNo[i] = pAG->GetGroupNo();
  pAcd->ACDRule.WorkerNo = 0;
  
  pAcd->ACDRule.AcdedGroupNo = 0;
  pAcd->ACDRule.AcdRule = 2;
  pAcd->ACDRule.LevelRule = 0;
  pAcd->ACDRule.CallMode = 0;
  pAcd->ACDRule.SeatGroupNo = 0;
  pAcd->ACDRule.WaitTimeLen = 30;
  pAcd->ACDRule.RingTimeLen = 30;
  pAcd->ACDRule.BusyWaitId = 1;
  pAcd->ACDRule.Priority = 1;

  pAcd->ACDRule.QueueTime = time(0);
  pAcd->ACDRule.StartRingTime = time(0);
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    pAcd->ACDRule.AcdednAGList[i] = -1;
  }
  pAcd->CalledNum = 0;
  for (i = 0; i < MAX_CALLED_NUM; i ++)
  {
    pAcd->Calleds[i].nAG = 0xFFFF;
    pAcd->Calleds[i].pQue_str = NULL;
    pAcd->Calleds[i].CalloutResult = 0;
  }
  pAcd->CallSeatType = 0; //來話直接呼入分配
  pChn->nAcd = nAcd;

  ChCount.AcdCallCount ++;
  pACDQueue->JoinACDQueue(nAcd);
  SendACDQueueInfoToAll();
}

//-----------------------------------------------------------------------------
//坐席實時話務量統計
void AgentCallMeteCount()
{
  static int nClearFlag[24]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  static bool bClearId=false;
  
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  char szNowTime[5];

  switch (pIVRCfg->ClearCallCountType)
  {
  case 1: //每天0時清零
  case 3: //
  case 4: //
    if (local->tm_hour == 23 && local->tm_min >= 59)
    {
      bClearId = true;
    }
    if (local->tm_hour == 0 && bClearId == true)
    {
      if (pIVRCfg->isClearLastACDnAG0Hour == true)
        pAgentMng->InitLastnAG();
      gIVRCallCountParamMng.Reset();
      pAgentMng->ResetAllStatusCountData();
      SendSystemStatusMeteCount();
      SendAllSeatStatusMeteCount();
      SendAllGroupStatusMeteCount();
      SendAllIVRCallCountData();
      bClearId = false;
    }
    break;
  case 2: //每整點清零
    if (local->tm_min == 59 && local->tm_sec >= 0)
    {
      bClearId = true;
    }
    if (local->tm_min == 0 && bClearId == true)
    {
      gIVRCallCountParamMng.Reset();
      pAgentMng->ResetAllStatusCountData();
      SendSystemStatusMeteCount();
      SendAllSeatStatusMeteCount();
      SendAllGroupStatusMeteCount();
      SendAllIVRCallCountData();
      bClearId = false;
    }
    break;
  case 5: //按交接班時間清零
    if (pIVRCfg->ClearTimeListNum == 0)
    {
      break;
    }
    sprintf(szNowTime, "%02d:%02d", local->tm_hour, local->tm_min);
    for (int i=0; i<pIVRCfg->ClearTimeListNum-1; i++)
    {
      if (strcmp(szNowTime, pIVRCfg->ClearTimeList[i]) > 0 && strcmp(szNowTime, pIVRCfg->ClearTimeList[i+1]) < 0)
      {
        if (i == 0)
        {
          if (nClearFlag[pIVRCfg->ClearTimeListNum-1] == 1)
          {
            gIVRCallCountParamMng.Reset();
            pAgentMng->ResetAllStatusCountData();
            SendSystemStatusMeteCount();
            SendAllSeatStatusMeteCount();
            SendAllGroupStatusMeteCount();
            SendAllIVRCallCountData();
            nClearFlag[pIVRCfg->ClearTimeListNum-1] = 0;
          }
          nClearFlag[i] = 1;
        }
        else
        {
          if (nClearFlag[i-1] == 1)
          {
            gIVRCallCountParamMng.Reset();
            pAgentMng->ResetAllStatusCountData();
            SendSystemStatusMeteCount();
            SendAllSeatStatusMeteCount();
            SendAllGroupStatusMeteCount();
            SendAllIVRCallCountData();
            nClearFlag[i-1] = 0;
          }
          nClearFlag[i] = 1;
        }
      }
    }
    break;
  }
  
  
  if (pIVRCfg->isCountCall == false)
  {
    return;
  }
  static int tcount=0;
  //int nGroupNo;

  if ((tcount++)<1000)
  {
    return;
  }
  tcount = 0;
  ChCount.AgLoginCur = 0;
  ChCount.AgBusyCur = 0;
  //pAgentMng->ResetAllStatusCount();
  //計算當前在線數
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    //nGroupNo = pAG->GetGroupNo();
    if (pAG->isLogin())
    {
      ChCount.AgLoginCur ++;
      if (!pAG->isIdelForSrv())
        ChCount.AgBusyCur ++;
      //pAgentMng->AllCallCountParam.LoginWorkerCount++;
      //pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCount++;
    }
    else
    {
      //pAgentMng->AllCallCountParam.LogOutCount++;
    }
    //AgentStatusCount(nAG);
  }

  if (ChCount.AgBusyCur > ChCount.AgBusyMax)
    ChCount.AgBusyMax = ChCount.AgBusyCur;
  if (ChCount.AgBusyCur < ChCount.AgBusyMin || ChCount.AgBusyMin == 0)
    ChCount.AgBusyMin = ChCount.AgBusyCur;

  if (ChCount.AgLoginCur > ChCount.AgLoginMax)
    ChCount.AgLoginMax = ChCount.AgLoginCur;
  if (ChCount.AgLoginCur < ChCount.AgLoginMin || ChCount.AgLoginMin == 0)
    ChCount.AgLoginMin = ChCount.AgLoginCur;
  
  //計算坐席占用率
  int nCurBusyPer;
  if (ChCount.AgLoginCur > 0)
    nCurBusyPer = ChCount.AgBusyCur * 100 / ChCount.AgLoginCur;
  else
    nCurBusyPer = 100;
  if (nCurBusyPer == 0)
  {
    ChCount.AgBusy0 ++;
  } 
  else if (nCurBusyPer > 0 && nCurBusyPer <= 25)
  {
    ChCount.AgBusy0_25 ++;
  }
  else if (nCurBusyPer > 25 && nCurBusyPer <= 50)
  {
    ChCount.AgBusy25_50 ++;
  }
  else if (nCurBusyPer > 50 && nCurBusyPer <= 75)
  {
    ChCount.AgBusy50_75 ++;
  }
  else if (nCurBusyPer > 75 && nCurBusyPer <= 90)
  {
    ChCount.AgBusy75_90 ++;
  }
  else
  {
    ChCount.AgBusy90_100 ++;
  }
  ChCount.AgBusyTotal ++;
  //每半小時記錄及發送統計結果
  static bool bTotalId=false;
  char LogFile[256], sqls[512];
  FILE	*Ftrace;
  int nCountTimeId;
  
  if ((pIVRCfg->CountCycle == 2 && local->tm_min == 28) || local->tm_min == 58)
    bTotalId = true;
  if (((pIVRCfg->CountCycle == 2 && local->tm_min == 29) || local->tm_min == 59) && bTotalId == true)
  {
    if (pIVRCfg->CountCycle == 2)
    {
      nCountTimeId = local->tm_hour*2;
      if (local->tm_min == 59)
      {
        nCountTimeId ++;
      }
    }
    else
    {
      nCountTimeId = local->tm_hour;
    }
    //計算占用百分比
    if (ChCount.AgBusyTotal > 0)
    {
      ChCount.AgBusy0_25Per = ChCount.AgBusy0_25*100/ChCount.AgBusyTotal;
      ChCount.AgBusy25_50Per = ChCount.AgBusy25_50*100/ChCount.AgBusyTotal;
      ChCount.AgBusy50_75Per = ChCount.AgBusy50_75*100/ChCount.AgBusyTotal;
      ChCount.AgBusy75_90Per = ChCount.AgBusy75_90*100/ChCount.AgBusyTotal;
      ChCount.AgBusy90_100Per = ChCount.AgBusy90_100*100/ChCount.AgBusyTotal;
      ChCount.AgBusy0Per = 100-ChCount.AgBusy0_25Per-ChCount.AgBusy25_50Per-ChCount.AgBusy50_75Per-ChCount.AgBusy75_90Per-ChCount.AgBusy90_100Per;
    }
    //將實時統計數據寫到文本文件
    if (pIVRCfg->isWriteCountCallTxt == true)
    {
      sprintf(LogFile, "%s\\AG_%04d%02d.log", g_szCountPath, local->tm_year+1900, local->tm_mon+1);
      Ftrace = fopen(LogFile, "a+t");
      if (Ftrace != NULL)
      {
        fprintf(Ftrace, "%04d-%02d-%02d,%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
        local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
        nCountTimeId,
        
        ChCount.AgLoginMax,
        ChCount.AgLoginMin,
        ChCount.AgBusyMin,
        ChCount.AgBusyMax,
      
        ChCount.AgBusy0Per,
        ChCount.AgBusy0_25Per,
        ChCount.AgBusy25_50Per,
        ChCount.AgBusy50_75Per,
        ChCount.AgBusy75_90Per,
        ChCount.AgBusy90_100Per,
      
        ChCount.AcdCallCount,
        ChCount.AcdCallFailCount,
        ChCount.AcdCallSuccCount,
        ChCount.AcdCallGiveUpCount,
        ChCount.AcdCallAnsCount,
        ChCount.AcdCallAvgWaitTime,
        ChCount.AcdCallAvgAnsTime,
        ChCount.AcdCallAvgGiveUpTime);

        fclose(Ftrace);
        Ftrace = NULL;
      }
    }
    //發送實時統計數據到流程解析器寫數據表
    if (pIVRCfg->isWriteCountCallDB == true)
    {
      if (MyStrCmpNoCase(g_szDBType, "MYSQL") == 0)
      {
        sprintf(sqls, "insert into tbAgCount values (0,'%04d-%02d-%02d','%02d:%02d',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
          nCountTimeId,
          
          ChCount.AgLoginMax,
          ChCount.AgLoginMin,
          ChCount.AgBusyMin,
          ChCount.AgBusyMax,
          
          ChCount.AgBusy0Per,
          ChCount.AgBusy0_25Per,
          ChCount.AgBusy25_50Per,
          ChCount.AgBusy50_75Per,
          ChCount.AgBusy75_90Per,
          ChCount.AgBusy90_100Per,
          
          ChCount.AcdCallCount,
          ChCount.AcdCallFailCount,
          ChCount.AcdCallSuccCount,
          ChCount.AcdCallGiveUpCount,
          ChCount.AcdCallAnsCount,
          ChCount.AcdCallAvgWaitTime,
          ChCount.AcdCallAvgAnsTime,
          ChCount.AcdCallAvgGiveUpTime);
      }
      else
      {
        sprintf(sqls, "insert into tbAgCount values ('%04d-%02d-%02d','%02d:%02d',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
          local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
          nCountTimeId,
          
          ChCount.AgLoginMax,
          ChCount.AgLoginMin,
          ChCount.AgBusyMin,
          ChCount.AgBusyMax,
          
          ChCount.AgBusy0Per,
          ChCount.AgBusy0_25Per,
          ChCount.AgBusy25_50Per,
          ChCount.AgBusy50_75Per,
          ChCount.AgBusy75_90Per,
          ChCount.AgBusy90_100Per,
          
          ChCount.AcdCallCount,
          ChCount.AcdCallFailCount,
          ChCount.AcdCallSuccCount,
          ChCount.AcdCallGiveUpCount,
          ChCount.AcdCallAnsCount,
          ChCount.AcdCallAvgWaitTime,
          ChCount.AcdCallAvgAnsTime,
          ChCount.AcdCallAvgGiveUpTime);
      }

      sprintf(SendMsgBuf, "<execsql sessionid='%ld' cmdaddr='0' chantype='0' channo='0' sqls=\"%s\"/>", 
        0x02010000, sqls);
      SendMsg2XML(MSG_onexecsql, 0x02010000, SendMsgBuf);
    }
      
    //發送實時統計數據到LOG監視
    sprintf(sqls, "%d,%04d-%02d-%02d,%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
      nCountTimeId,
      local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
      
      ChCount.AgLoginMax,
      ChCount.AgLoginMin,
      ChCount.AgBusyMin,
      ChCount.AgBusyMax,
      
      ChCount.AgBusy0Per,
      ChCount.AgBusy0_25Per,
      ChCount.AgBusy25_50Per,
      ChCount.AgBusy50_75Per,
      ChCount.AgBusy75_90Per,
      ChCount.AgBusy90_100Per,
      
      ChCount.AcdCallCount,
      ChCount.AcdCallFailCount,
      ChCount.AcdCallSuccCount,
      ChCount.AcdCallGiveUpCount,
      ChCount.AcdCallAnsCount,
      ChCount.AcdCallAvgWaitTime,
      ChCount.AcdCallAvgAnsTime,
      ChCount.AcdCallAvgGiveUpTime);
    SendAgCountData(nCountTimeId, sqls);
    strcpy(ChCount.strAgData, sqls);

    TodayChCount[nCountTimeId].SetAgData(ChCount);
    ChCount.InitAgData();
    if (nCountTimeId == 47)
    {
      for (int i = 0; i < 48; i ++)
      {
        TodayChCount[i].InitAgData();
      }
    }
    bTotalId = false;
  }
}
void AgentStatusCount(int nAG)
{
  int nGroupNo = pAG->GetGroupNo();
  switch (pAG->m_Worker.WorkerCallCountParam.CurStatus)
  {
  case AG_STATUS_LOGOUT:	//未登錄
    //pAgentMng->AllCallCountParam.LogOutCount++;
    break;
  case AG_STATUS_LOGIN:	//登錄
    //pAgentMng->AllCallCountParam.LoginWorkerCount++;
    //pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCount++;
    break;
  case AG_STATUS_BUSY:	//示忙態
    pAgentMng->AllCallCountParam.BusyWorkerCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.BusyWorkerCount++;
    break;
  case AG_STATUS_IDEL:	//空閑態
    pAgentMng->AllCallCountParam.IdelWorkerCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCount++;
    break;
  case AG_STATUS_INRING:	//呼入振鈴
    pAgentMng->AllCallCountParam.CallInRingCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInRingCount++;

    pAgentMng->AllCallCountParam.CallUseCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;
    break;
  case AG_STATUS_OUTSEIZE:	//呼出占用
    pAgentMng->AllCallCountParam.CallOutRingCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutRingCount++;

    pAgentMng->AllCallCountParam.CallUseCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;
    break;
  case AG_STATUS_OUTRING:	//呼出振鈴
    pAgentMng->AllCallCountParam.CallOutRingCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutRingCount++;

    pAgentMng->AllCallCountParam.CallUseCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;
    break;
  case AG_STATUS_TALK:	//通話狀態
    if (pAG->m_Worker.WorkerCallCountParam.CallInOut == 1)
    {
      pAgentMng->AllCallCountParam.CallInTalkCount++;
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkCount++;
    } 
    else if (pAG->m_Worker.WorkerCallCountParam.CallInOut == 2)
    {
      pAgentMng->AllCallCountParam.CallOutTalkCount++;
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkCount++;
    }
    pAgentMng->AllCallCountParam.CallTalkCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkCount++;

    pAgentMng->AllCallCountParam.CallUseCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;
    break;
  case AG_STATUS_HOLD:	//保持狀態
    pAgentMng->AllCallCountParam.CallHoldCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHoldCount++;

    pAgentMng->AllCallCountParam.CallUseCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;
    break;
  case AG_STATUS_ACW: //話后處理
    pAgentMng->AllCallCountParam.CallACWCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWCount++;

    pAgentMng->AllCallCountParam.CallUseCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;
    break;
  case AG_STATUS_LEAVE: //離席小休態
    pAgentMng->AllCallCountParam.LeaveWorkerCount++;
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LeaveWorkerCount++;
    break;
  }
}
void AgentStatusCount()
{
  int nGroupNo, nGroupNo1, nChn;
  pAgentMng->ResetAllStatusCount();
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    //2015-12-14
    if (g_bLogSpecId == true)
    {
      MyTrace(3, "AgentStatusCount nAG=%d SeatNo=%s WorkerNo=%d GroupNo=%d SeatLoginId=%d WorkerLoginId=%d svState=%d DelayState=%d DisturbId=%d LeaveId=%d",
        nAG, pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo(), pAG->GetGroupNo(), pAG->m_Seat.LoginId, pAG->m_Worker.LoginId, pAG->svState, pAG->DelayState, pAG->m_Worker.DisturbId, pAG->m_Worker.LeaveId);
    }
    nGroupNo = pAG->GetGroupNo();
    if (pAG->isLogin())
    {
      //已登錄話務員數
      pAgentMng->AllCallCountParam.LoginWorkerCount++;
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCount++;

      //2015-12-14
      for (int j = 1; j < MAX_GROUP_NUM_FORWORKER; j++)
      {
        nGroupNo1 = pAG->m_Worker.GroupNo[j];
        if (nGroupNo1 > 0 && nGroupNo1 < MAX_AG_GROUP)
        {
          pAgentMng->WorkerGroup[nGroupNo1].GroupCallCountParam.LoginWorkerCountB++;
        }
      }

      nChn = pAG->GetSeatnChn();
      if (pAG->svState == 0)
      {
        if (pAG->DelayState == 0 && pAG->m_Worker.isDisturb() == true)
        {
          //示忙話務員數
          pAgentMng->AllCallCountParam.BusyWorkerCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.BusyWorkerCount++;
        } 
        else if (pAG->DelayState == 0 && pAG->m_Worker.isLeave() == true)
        {
          //離席話務員數
          pAgentMng->AllCallCountParam.LeaveWorkerCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LeaveWorkerCount++;
        }
        else
        {
          if (pAG->DelayState > 0)
          {
            //正在話后處理數
            pAgentMng->AllCallCountParam.CallACWCount++;
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWCount++;
          }
          else
          {
            //空閑話務員數
            pAgentMng->AllCallCountParam.IdelWorkerCount++;
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCount++;
            
            //2015-12-14
            for (int j = 1; j < MAX_GROUP_NUM_FORWORKER; j++)
            {
              nGroupNo1 = pAG->m_Worker.GroupNo[j];
              if (nGroupNo1 > 0 && nGroupNo1 < MAX_AG_GROUP)
              {
                pAgentMng->WorkerGroup[nGroupNo1].GroupCallCountParam.IdelWorkerCountB++;
              }
            }
          }
        }
      }
      else
      {
        //正在使用數
        pAgentMng->AllCallCountParam.CallUseCount++;
        pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount++;

        if (pAG->svState == AG_SV_CONN)
        {
          //正在通話數
          pAgentMng->AllCallCountParam.CallTalkCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkCount++;

          if (pChn->CallInOut == 1)
          {
            //呼入通話數
            pAgentMng->AllCallCountParam.CallInTalkCount++;
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkCount++;
          }
          else
          {
            //呼出通話數
            pAgentMng->AllCallCountParam.CallOutTalkCount++;
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkCount++;
          }
        }
        else if (pAG->svState == AG_SV_HOLD)
        {
          //正在保持數
          pAgentMng->AllCallCountParam.CallHoldCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHoldCount++;
        }
        else if (pAG->svState == AG_SV_ACW)
        {
          //正在話后處理數
          pAgentMng->AllCallCountParam.CallACWCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWCount++;
        }
        else if (pAG->svState == AG_SV_INSEIZE || pAG->svState == AG_SV_INRING)
        {
          //呼入振鈴數
          pAgentMng->AllCallCountParam.CallInRingCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInRingCount++;
        }
        else
        {
          //正在呼出數
          pAgentMng->AllCallCountParam.CallOutRingCount++;
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutRingCount++;
        }
      }
      if (pAG->m_Worker.isDisturb() == true)
      {
        pAgentMng->AllCallCountParam.SetBusyWorkerCount++;
        pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetBusyWorkerCount++;
      }
      else if (pAG->m_Worker.isLeave() == true)
      {
        //離席話務員數
        pAgentMng->AllCallCountParam.SetLeaveWorkerCount++;
        pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetLeaveWorkerCount++;
      }
    }
    else
    {
      //未登錄數
      pAgentMng->AllCallCountParam.LogOutCount++;
    }
    //AgentStatusCount(nAG);
  }
}

//發送即時統計數據
void SendNowAgentCallMeteCount()
{
  char sqls[512];
  int nCountTimeId;
  
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

  if (pIVRCfg->CountCycle == 2)
  {
    nCountTimeId = local->tm_hour*2;
    if (local->tm_min == 59)
    {
      nCountTimeId ++;
    }
  }
  else
  {
    nCountTimeId = local->tm_hour;
  }
  //計算占用百分比
  if (ChCount.AgBusyTotal > 0)
  {
    ChCount.AgBusy0_25Per = ChCount.AgBusy0_25*100/ChCount.AgBusyTotal;
    ChCount.AgBusy25_50Per = ChCount.AgBusy25_50*100/ChCount.AgBusyTotal;
    ChCount.AgBusy50_75Per = ChCount.AgBusy50_75*100/ChCount.AgBusyTotal;
    ChCount.AgBusy75_90Per = ChCount.AgBusy75_90*100/ChCount.AgBusyTotal;
    ChCount.AgBusy90_100Per = ChCount.AgBusy90_100*100/ChCount.AgBusyTotal;
    ChCount.AgBusy0Per = 100-ChCount.AgBusy0_25Per-ChCount.AgBusy25_50Per-ChCount.AgBusy50_75Per-ChCount.AgBusy75_90Per-ChCount.AgBusy90_100Per;
  }
  //發送實時統計數據到LOG監視
  sprintf(sqls, "%d,%04d-%02d-%02d,%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    nCountTimeId,
    local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min,
    
    ChCount.AgLoginMax,
    ChCount.AgLoginMin,
    ChCount.AgBusyMin,
    ChCount.AgBusyMax,
    
    ChCount.AgBusy0Per,
    ChCount.AgBusy0_25Per,
    ChCount.AgBusy25_50Per,
    ChCount.AgBusy50_75Per,
    ChCount.AgBusy75_90Per,
    ChCount.AgBusy90_100Per,
    
    ChCount.AcdCallCount,
    ChCount.AcdCallFailCount,
    ChCount.AcdCallSuccCount,
    ChCount.AcdCallGiveUpCount,
    ChCount.AcdCallAnsCount,
    ChCount.AcdCallAvgWaitTime,
    ChCount.AcdCallAvgAnsTime,
    ChCount.AcdCallAvgGiveUpTime);
  SendAgCountData(nCountTimeId, sqls);
}
//發送坐席狀態實時統計數據
void SendAllSeatStatusMeteCount()
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    SendOneSeatStatusMeteCount(nAG);
  }
}
void SendOneSeatStatusMeteCount(int nAG)
{
  char szStatusData[1024];
  Set_IVR2LOG_Header(LOGMSG_onseatstatusdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 1, nAG);
  Set_IVR2LOG_Item(LOGMSG_onseatstatusdata, 2, pAG->GetSeatNo());
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 3, pAG->GetWorkerNo());
  Set_IVR2LOG_Item(LOGMSG_onseatstatusdata, 4, pAG->GetWorkerName());
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 5, pAG->m_Worker.WorkerCallCountParam.CurStatus);
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 6, time(0)-pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    pAG->m_Worker.WorkerCallCountParam.LogoutTimeSumLen, //1 累計未登錄時長
    
    pAG->m_Worker.WorkerCallCountParam.LoginTimeSumLen, //2 累計登錄時長
    
    pAG->m_Worker.WorkerCallCountParam.BusyCount, // 累計示忙次數
    pAG->m_Worker.WorkerCallCountParam.BusyTimeSumLen, //3 累計未就緒(即示忙)時長
    pAG->m_Worker.WorkerCallCountParam.BusyTimeAvgLen, // 平均即示忙時長
    
    pAG->m_Worker.WorkerCallCountParam.LeaveCount, // 累計離席次數
    pAG->m_Worker.WorkerCallCountParam.LeaveTimeSumLen, //4 累計離席時長
    pAG->m_Worker.WorkerCallCountParam.LeaveTimeAvgLen, // 平均離席時長
    
    pAG->m_Worker.WorkerCallCountParam.IdelTimeSumLen, //5 累計等待來話(即示閑)時長
    
    pAG->m_Worker.WorkerCallCountParam.CallInCount, //6 來話分配總次數
    pAG->m_Worker.WorkerCallCountParam.CallInAnsCount, //7 來話應答總次數
    pAG->m_Worker.WorkerCallCountParam.CallInNoAnsCount, //8 來話未應答總次數
    pAG->m_Worker.WorkerCallCountParam.CallInAbandonCount, //9 來話放棄總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeSumLen, //10 呼入累計等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeMaxLen, //11 呼入最長等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeMinLen, //12 呼入最短等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeAvgLen, //13 呼入平均等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInWaitOverTimeCount, //14 呼入應答超時總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeSumLen, //15 呼入累計放棄前等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeMaxLen, //16 呼入放棄前最長等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeMinLen, //17 呼入放棄前最短等待時長
    pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeAvgLen, //18 呼入放棄前平均等待時長
    
    pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeSumLen, //19 呼入累計通話時長
    pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeMaxLen, //20 呼入最長通話時長
    pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeMinLen, //21 呼入最短通話時長
    pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeAvgLen, //22 呼入平均通話時長
    pAG->m_Worker.WorkerCallCountParam.CallInTalkOverTimeCount, //23 呼入通話超時總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallInACWTimeSumLen, //24 呼入累計話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInACWTimeMaxLen, //25 呼入最長話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInACWTimeMinLen, //26 呼入最短話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInACWTimeAvgLen, //27 呼入平均話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInACWOverTimeCount, //28 呼入話后處理超時總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeSumLen, //29 呼入累計話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeMaxLen, //30 呼入最長話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeMinLen, //31 呼入最短話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeAvgLen, //32 呼入平均話務處理時長
    
    pAG->m_Worker.WorkerCallCountParam.CallInAbandonRate, //33 來話放棄率
    pAG->m_Worker.WorkerCallCountParam.CallInAnswerRate, //34 來話接通率
    
    pAG->m_Worker.WorkerCallCountParam.CallOutCount, //35 呼出總次數
    pAG->m_Worker.WorkerCallCountParam.CallOutAnsCount, //36 呼出應答總次數
    pAG->m_Worker.WorkerCallCountParam.CallOutNoAnsCount, //37 呼出未應答總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeSumLen, //38 呼出累計等待時長
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeMaxLen, //39 呼出最長等待時長
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeMinLen, //40 呼出最短等待時長
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeAvgLen, //41 呼出平均等待時長
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitOverTimeCount, //42 呼出應答超時總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeSumLen, //43 呼出累計通話時長
    pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeMaxLen, //44 呼出最長通話時長
    pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeMinLen, //45 呼出最短通話時長
    pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeAvgLen, //46 呼出平均通話時長
    pAG->m_Worker.WorkerCallCountParam.CallOutTalkOverTimeCount, //47 呼出通話超時總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeSumLen, //48 呼出累計話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeMaxLen, //49 呼出最長話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeMinLen, //50 呼出最短話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeAvgLen, //51 呼出平均話后處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutACWOverTimeCount, //52 呼出話后處理超時總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeSumLen, //53 呼出累計話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeMaxLen, //54 呼出最長話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeMinLen, //55 呼出最短話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeAvgLen, //56 呼出平均話務處理時長
    
    pAG->m_Worker.WorkerCallCountParam.CallOutFailRate, //57 呼出失敗率
    pAG->m_Worker.WorkerCallCountParam.CallOutAnswerRate, //58 呼出接通率
    
    pAG->m_Worker.WorkerCallCountParam.CallTalkOverTimeCount, //59 通話超時總次數
    pAG->m_Worker.WorkerCallCountParam.CallTalkTimeSumLen, //60 累計通話總時長
    pAG->m_Worker.WorkerCallCountParam.CallAnsCount, //61 累計通話總次數
    
    pAG->m_Worker.WorkerCallCountParam.CallACWOverTimeCount, //62 話后處理超時總次數
    pAG->m_Worker.WorkerCallCountParam.CallACWTimeSumLen, //63 累計話后處理總時長
    
    pAG->m_Worker.WorkerCallCountParam.CallHandleTimeSumLen, //64 累計話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallHandleTimeMaxLen, //65 最長話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallHandleTimeMinLen, //66 最短話務處理時長
    pAG->m_Worker.WorkerCallCountParam.CallHandleTimeAvgLen, //67 平均話務處理時長
    
    pAG->m_Worker.WorkerCallCountParam.HoldCallCount, //68 保持總次數
    pAG->m_Worker.WorkerCallCountParam.HoldCallTimeSumLen, //69 累計保持時長
    pAG->m_Worker.WorkerCallCountParam.HoldCallTimeMaxLen, //70 最長保持時長
    pAG->m_Worker.WorkerCallCountParam.HoldCallTimeMinLen, //71 最短保持時長
    pAG->m_Worker.WorkerCallCountParam.HoldCallTimeAvgLen, //72 平均保持時長
    
    pAG->m_Worker.WorkerCallCountParam.TranCallCount //73 轉接總次數
    );
  Set_IVR2LOG_Item(LOGMSG_onseatstatusdata, 7, szStatusData);
  Set_IVR2LOG_Tail();
  
  if (pIVRCfg->isUpdateAgentSumDataToDB == true)
  {
    char sqls[1024];
    int svState;
    if (pAG->DelayState == 0 || pAG->svState > 0)
    {
      svState = pAG->svState;
    } 
    else if (pAG->DelayState == 1)
    {
      svState = AG_SV_ACW;
    }
    else
    {
      svState = AG_SV_DELAY;
    }

    sprintf(sqls, "exec sp_UpdateAgentSumData '%s','%d','%s','%d','%s'", 
      pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo(), pAG->GetWorkerName().C_Str(), svState, szStatusData);
    SendExecSql2DB(sqls);
  }

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_onseatstatusdata, IVRSndMsg.GetBuf());
    }
  }
}
//發送坐席狀態實時統計數據
void SendOneSeatStatusMeteCount(int nAG, int nLOG)
{
  char szStatusData[1024];
  Set_IVR2LOG_Header(LOGMSG_onseatstatusdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 1, nAG);
  Set_IVR2LOG_Item(LOGMSG_onseatstatusdata, 2, pAG->GetSeatNo());
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 3, pAG->GetWorkerNo());
  Set_IVR2LOG_Item(LOGMSG_onseatstatusdata, 4, pAG->GetWorkerName());
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 5, pAG->m_Worker.WorkerCallCountParam.CurStatus);
  Set_IVR2LOG_IntItem(LOGMSG_onseatstatusdata, 6, time(0)-pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
      pAG->m_Worker.WorkerCallCountParam.LogoutTimeSumLen, //1 累計未登錄時長
  
      pAG->m_Worker.WorkerCallCountParam.LoginTimeSumLen, //2 累計登錄時長
      
      pAG->m_Worker.WorkerCallCountParam.BusyCount, //3 累計示忙次數
      pAG->m_Worker.WorkerCallCountParam.BusyTimeSumLen, //4 累計未就緒(即示忙)時長
      pAG->m_Worker.WorkerCallCountParam.BusyTimeAvgLen, //5 平均即示忙時長

      pAG->m_Worker.WorkerCallCountParam.LeaveCount, //6 累計離席次數
      pAG->m_Worker.WorkerCallCountParam.LeaveTimeSumLen, //7 累計離席時長
      pAG->m_Worker.WorkerCallCountParam.LeaveTimeAvgLen, //8 平均離席時長
      
      pAG->m_Worker.WorkerCallCountParam.IdelTimeSumLen, //9 累計等待來話(即示閑)時長
  
      pAG->m_Worker.WorkerCallCountParam.CallInCount, //10 來話分配總次數
      pAG->m_Worker.WorkerCallCountParam.CallInAnsCount, //11 來話應答總次數
      pAG->m_Worker.WorkerCallCountParam.CallInNoAnsCount, //12 來話未應答總次數
      pAG->m_Worker.WorkerCallCountParam.CallInAbandonCount, //13 來話放棄總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeSumLen, //14 呼入累計等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeMaxLen, //15 呼入最長等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeMinLen, //16 呼入最短等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInWaitTimeAvgLen, //17 呼入平均等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInWaitOverTimeCount, //18 呼入應答超時總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeSumLen, //19 呼入累計放棄前等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeMaxLen, //20 呼入放棄前最長等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeMinLen, //21 呼入放棄前最短等待時長
      pAG->m_Worker.WorkerCallCountParam.CallInAbandonTimeAvgLen, //22 呼入放棄前平均等待時長
  
      pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeSumLen, //23 呼入累計通話時長
      pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeMaxLen, //24 呼入最長通話時長
      pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeMinLen, //25 呼入最短通話時長
      pAG->m_Worker.WorkerCallCountParam.CallInTalkTimeAvgLen, //26 呼入平均通話時長
      pAG->m_Worker.WorkerCallCountParam.CallInTalkOverTimeCount, //27 呼入通話超時總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallInACWTimeSumLen, //28 呼入累計話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInACWTimeMaxLen, //29 呼入最長話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInACWTimeMinLen, //30 呼入最短話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInACWTimeAvgLen, //31 呼入平均話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInACWOverTimeCount, //32 呼入話后處理超時總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeSumLen, //33 呼入累計話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeMaxLen, //34 呼入最長話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeMinLen, //35 呼入最短話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallInHandleTimeAvgLen, //36 呼入平均話務處理時長
  
      pAG->m_Worker.WorkerCallCountParam.CallInAbandonRate, //37 來話放棄率
      pAG->m_Worker.WorkerCallCountParam.CallInAnswerRate, //38 來話接通率
  
      pAG->m_Worker.WorkerCallCountParam.CallOutCount, //39 呼出總次數
      pAG->m_Worker.WorkerCallCountParam.CallOutAnsCount, //40 呼出應答總次數
      pAG->m_Worker.WorkerCallCountParam.CallOutNoAnsCount, //41 呼出未應答總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeSumLen, //42 呼出累計等待時長
      pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeMaxLen, //43 呼出最長等待時長
      pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeMinLen, //44 呼出最短等待時長
      pAG->m_Worker.WorkerCallCountParam.CallOutWaitTimeAvgLen, //45 呼出平均等待時長
      pAG->m_Worker.WorkerCallCountParam.CallOutWaitOverTimeCount, //46 呼出應答超時總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeSumLen, //47 呼出累計通話時長
      pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeMaxLen, //48 呼出最長通話時長
      pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeMinLen, //49 呼出最短通話時長
      pAG->m_Worker.WorkerCallCountParam.CallOutTalkTimeAvgLen, //50 呼出平均通話時長
      pAG->m_Worker.WorkerCallCountParam.CallOutTalkOverTimeCount, //51 呼出通話超時總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeSumLen, //52 呼出累計話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeMaxLen, //53 呼出最長話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeMinLen, //544 呼出最短話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutACWTimeAvgLen, //55 呼出平均話后處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutACWOverTimeCount, //56 呼出話后處理超時總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeSumLen, //57 呼出累計話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeMaxLen, //58 呼出最長話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeMinLen, //59 呼出最短話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallOutHandleTimeAvgLen, //60 呼出平均話務處理時長
  
      pAG->m_Worker.WorkerCallCountParam.CallOutFailRate, //61 呼出失敗率
      pAG->m_Worker.WorkerCallCountParam.CallOutAnswerRate, //62 呼出接通率
  
      pAG->m_Worker.WorkerCallCountParam.CallTalkOverTimeCount, //63 通話超時總次數
      pAG->m_Worker.WorkerCallCountParam.CallTalkTimeSumLen, //64 累計通話總時長
      pAG->m_Worker.WorkerCallCountParam.CallAnsCount, //65 累計通話總次數
  
      pAG->m_Worker.WorkerCallCountParam.CallACWOverTimeCount, //66 話后處理超時總次數
      pAG->m_Worker.WorkerCallCountParam.CallACWTimeSumLen, //67 累計話后處理總時長
  
      pAG->m_Worker.WorkerCallCountParam.CallHandleTimeSumLen, //68 累計話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallHandleTimeMaxLen, //69 最長話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallHandleTimeMinLen, //70 最短話務處理時長
      pAG->m_Worker.WorkerCallCountParam.CallHandleTimeAvgLen, //71 平均話務處理時長
  
      pAG->m_Worker.WorkerCallCountParam.HoldCallCount, //72 保持總次數
      pAG->m_Worker.WorkerCallCountParam.HoldCallTimeSumLen, //73 累計保持時長
      pAG->m_Worker.WorkerCallCountParam.HoldCallTimeMaxLen, //74 最長保持時長
      pAG->m_Worker.WorkerCallCountParam.HoldCallTimeMinLen, //75 最短保持時長
      pAG->m_Worker.WorkerCallCountParam.HoldCallTimeAvgLen, //76 平均保持時長
  
      pAG->m_Worker.WorkerCallCountParam.TranCallCount //77 轉接總次數
    );
  Set_IVR2LOG_Item(LOGMSG_onseatstatusdata, 7, szStatusData);
  Set_IVR2LOG_Tail();
  
  if (pIVRCfg->isUpdateAgentSumDataToDB == true)
  {
    char sqls[1024];
    int svState;
    if (pAG->DelayState == 0 || pAG->svState > 0)
    {
      svState = pAG->svState;
    } 
    else if (pAG->DelayState == 1)
    {
      svState = AG_SV_ACW;
    }
    else
    {
      svState = AG_SV_DELAY;
    }
    
    sprintf(sqls, "exec sp_UpdateAgentSumData '%s','%d','%s','%d','%s'", 
      pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo(), pAG->GetWorkerName().C_Str(), svState, szStatusData);
    SendExecSql2DB(sqls);
  }

  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
  {
    SendMsg2LOG(nLOG, LOGMSG_onseatstatusdata, IVRSndMsg.GetBuf());
  }
}
void SendAllSeatStatusMeteCount(int nLOG)
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    SendOneSeatStatusMeteCount(nAG, nLOG);
  }
}
/*
01 累計未登錄時長
02 累計登錄時長
03 累計示忙時長
04 累計離席時長
05 累計示閑時長
06 來話分配總次數
07 來話應答總次數
08 來話未應答總次數
09 來話放棄總次數
10 呼入累計等待時長

11 呼入最長等待時長
12 呼入最短等待時長
13 呼入平均等待時長
14 呼入應答超時總次數
15 呼入累計放棄前等待時長
16 呼入放棄前最長等待時長
17 呼入放棄前最短等待時長
18 呼入放棄前平均等待時長
19 呼入累計通話時長
20 呼入最長通話時長

21 呼入最短通話時長
22 呼入平均通話時長
23 呼入通話超時總次數
24 呼入累計話后處理時長
25 呼入最長話后處理時長
26 呼入最短話后處理時長
27 呼入平均話后處理時長
28 呼入話后處理超時總次數
29 呼入累計話務處理時長
30 呼入最長話務處理時長

31 呼入最短話務處理時長
32 呼入平均話務處理時長
33 來話放棄率
34 來話接通率
35 呼出總次數
36 呼出應答總次數
37 呼出未應答總次數
38 呼出累計等待時長
39 呼出最長等待時長
40 呼出最短等待時長

41 呼出平均等待時長
42 呼出應答超時總次數
43 呼出累計通話時長
44 呼出最長通話時長
45 呼出最短通話時長
46 呼出平均通話時長
47 呼出通話超時總次數
48 呼出累計話后處理時長
49 呼出最長話后處理時長
50 呼出最短話后處理時長

51 呼出平均話后處理時長
52 呼出話后處理超時總次數
53 呼出累計話務處理時長
54 呼出最長話務處理時長
55 呼出最短話務處理時長
56 呼出平均話務處理時長
57 呼出失敗率
58 呼出接通率
59 通話超時總次數
60 累計通話總時長

61 累計通話總次數
62 話后處理超時總次數
63 累計話后處理總時長
64 累計話務處理時長
65 最長話務處理時長
66 最短話務處理時長
67 平均話務處理時長
68 保持總次數
69 累計保持時長
70 最長保持時長

71 最短保持時長
72 平均保持時長
73 轉接總次數

01 ACD分配總次數
02 ACD分配失敗總次數

03 來話分配總次數
04 來話應答總次數
05 來話未應答總次數
06 來話放棄總次數
07 呼入累計等待時長
08 呼入最長等待時長
09 呼入最短等待時長
10 呼入平均等待時長
11 呼入應答超時總次數
12 呼入累計放棄前等待時長

13 呼入放棄前最長等待時長
14 呼入放棄前最短等待時長
15 呼入放棄前平均等待時長
16 呼入累計通話時長
17 呼入最長通話時長
18 呼入最短通話時長
19 呼入平均通話時長
20 呼入通話超時總次數
21 呼入累計話後處理時長
22 呼入最長話後處理時長

23 呼入最短話後處理時長
24 呼入平均話後處理時長
25 呼入話後處理超時總次數
26 呼入累計話務處理時長
27 呼入最長話務處理時長
28 呼入最短話務處理時長
29 呼入平均話務處理時長
30 來話放棄率
31 來話接通率

32 呼出總次數
33 呼出應答總次數
34 呼出未應答總次數
35 呼出累計等待時長
36 呼出最長等待時長
37 呼出最短等待時長
38 呼出平均等待時長
39 呼出應答超時總次數
40 呼出累計通話時長
41 呼出最長通話時長
42 呼出最短通話時長

43 呼出平均通話時長
44 呼出通話超時總次數
45 呼出累計話後處理時長
46 呼出最長話後處理時長
47 呼出最短話後處理時長
48 呼出平均話後處理時長
49 呼出話後處理超時總次數
50 呼出累計話務處理時長
51 呼出最長話務處理時長
52 呼出最短話務處理時長

53 呼出平均話務處理時長
54 呼出失敗率
55 呼出接通率
56 通話超時總次數
57 累計通話總時長
58 累計通話總次數
59 話後處理超時總次數
60 累計話後處理總時長
61 累計話務處理時長
62 最長話務處理時長

63 最短話務處理時長
64 平均話務處理時長
65 保持總次數
66 累計保持時長
67 最長保持時長
68 最短保持時長
69 平均保持時長
70 轉接總次數

*/
void SendAllGroupStatusMeteCount()
{
  for (int nGroupNo = 1; nGroupNo < MAX_AG_GROUP; nGroupNo ++)
  {
    if (pAgentMng->WorkerGroup[nGroupNo].state == 1)
    {
      for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
      {
        if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAgStatus == true)
        {
          SendOneGroupStatusMeteCount(nGroupNo, nLOG);
          SendSystemStatusMeteCount(nLOG);
        }
      }
    }
  }
}
void SendOneGroupStatusMeteCount(int nAG)
{
  int nGroupNo;
  char szStatusCount[256], szStatusData[1024];

  SendOneGroupStatusMeteCountB(pAG->m_Worker.PreGroupNo);

  for (int i=0; i<MAX_GROUP_NUM_FORWORKER; i++)
  {
    nGroupNo = pAG->m_Worker.GroupNo[i];
    if (nGroupNo <= 0 || nGroupNo >= MAX_AG_GROUP)
      continue;

    Set_IVR2LOG_Header(LOGMSG_ongroupstatusdata, 0);
    Set_IVR2LOG_IntItem(LOGMSG_ongroupstatusdata, 1, nGroupNo);
    Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 2, pAgentMng->WorkerGroup[nGroupNo].GroupName);
    sprintf(szStatusCount, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LogOutCount, //未登錄數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCount, //已登錄話務員數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCount, //示閑話務員數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.BusyWorkerCount, //示忙話務員數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LeaveWorkerCount, //離席話務員數
  
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInRingCount, //呼入振鈴數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkCount, //呼入通話數
  
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutRingCount, //正在呼出數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkCount, //呼出通話數
  
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkCount, //正在通話數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWCount, //正在話后處理數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHoldCount, //正在保持數

      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount, //正在使用數

      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetBusyWorkerCount, //設定示忙話務員數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetLeaveWorkerCount, //設定離席話務員數

      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCountB, //已登錄備用話務員數 2015-12-14
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCountB //備用示閑話務員數 2015-12-14
      );
    Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 3, szStatusCount);
  
    sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount, //ACD分配總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDFailCount, //ACD分配失敗總次數
    
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInCount, //1來話分配總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAnsCount, //2來話應答總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInNoAnsCount, //3來話未應答總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonCount, //4來話放棄總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeSumLen, //5呼入累計等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeMaxLen, //6呼入最長等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeMinLen, //7呼入最短等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeAvgLen, //8呼入平均等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitOverTimeCount, //9呼入應答超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeSumLen, //10呼入累計放棄前等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeMaxLen, //11呼入放棄前最長等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeMinLen, //12呼入放棄前最短等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeAvgLen, //13呼入放棄前平均等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeSumLen, //15呼入累計通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeMaxLen, //15呼入最長通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeMinLen, //呼入最短通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeAvgLen, //呼入平均通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkOverTimeCount, //呼入通話超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeSumLen, //呼入累計話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeMaxLen, //呼入最長話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeMinLen, //呼入最短話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeAvgLen, //呼入平均話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWOverTimeCount, //呼入話后處理超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeSumLen, //呼入累計話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeMaxLen, //呼入最長話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeMinLen, //呼入最短話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeAvgLen, //呼入平均話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonRate, //來話放棄率
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAnswerRate, //來話接通率
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutCount, //呼出總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutAnsCount, //呼出應答總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutNoAnsCount, //呼出未應答總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeSumLen, //呼出累計等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeMaxLen, //呼出最長等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeMinLen, //呼出最短等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeAvgLen, //呼出平均等待時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitOverTimeCount, //呼出應答超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeSumLen, //呼出累計通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeMaxLen, //呼出最長通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeMinLen, //呼出最短通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeAvgLen, //呼出平均通話時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkOverTimeCount, //呼出通話超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeSumLen, //呼出累計話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeMaxLen, //呼出最長話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeMinLen, //呼出最短話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeAvgLen, //呼出平均話后處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWOverTimeCount, //呼出話后處理超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeSumLen, //呼出累計話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeMaxLen, //呼出最長話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeMinLen, //呼出最短話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeAvgLen, //呼出平均話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutFailRate, //呼出失敗率
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutAnswerRate, //呼出接通率
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkOverTimeCount, //通話超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkTimeSumLen, //累計通話總時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallAnsCount, //累計通話總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWOverTimeCount, //話后處理超時總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWTimeSumLen, //累計話后處理總時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeSumLen, //累計話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeMaxLen, //最長話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeMinLen, //最短話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeAvgLen, //平均話務處理時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallCount, //保持總次數
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeSumLen, //累計保持時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeMaxLen, //最長保持時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeMinLen, //最短保持時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeAvgLen, //平均保持時長
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.TranCallCount //轉接總次數
      );
    Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 4, szStatusData);
    Set_IVR2LOG_Tail();
  
    if (pIVRCfg->isUpdateGroupSumDataToDB == true)
    {
      char sqls[1024];
    
      sprintf(sqls, "exec sp_UpdateGroupSumData '%d','%s','%s','%s'", 
        nGroupNo, pAgentMng->WorkerGroup[nGroupNo].GroupName.C_Str(), szStatusCount, szStatusData);
      SendExecSql2DB(sqls);
    }

    for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
    {
      if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
      {
        SendMsg2LOG(nLOG, LOGMSG_ongroupstatusdata, IVRSndMsg.GetBuf());
      }
    }
  }
}
void SendOneGroupStatusMeteCountB(int nGroupNo)
{
  char szStatusCount[256], szStatusData[1024];

  if (nGroupNo <= 0 || nGroupNo >= MAX_AG_GROUP)
    return;

  Set_IVR2LOG_Header(LOGMSG_ongroupstatusdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongroupstatusdata, 1, nGroupNo);
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 2, pAgentMng->WorkerGroup[nGroupNo].GroupName);
  sprintf(szStatusCount, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LogOutCount, //未登錄數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCount, //已登錄話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCount, //示閑話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.BusyWorkerCount, //示忙話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LeaveWorkerCount, //離席話務員數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInRingCount, //呼入振鈴數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkCount, //呼入通話數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutRingCount, //正在呼出數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkCount, //呼出通話數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkCount, //正在通話數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWCount, //正在話后處理數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHoldCount, //正在保持數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount, //正在使用數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetBusyWorkerCount, //設定示忙話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetLeaveWorkerCount, //設定離席話務員數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCountB, //已登錄備用話務員數 2015-12-14
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCountB //備用示閑話務員數 2015-12-14
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 3, szStatusCount);

  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
  
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount, //ACD分配總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDFailCount, //ACD分配失敗總次數
  
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInCount, //1來話分配總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAnsCount, //2來話應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInNoAnsCount, //3來話未應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonCount, //4來話放棄總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeSumLen, //5呼入累計等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeMaxLen, //6呼入最長等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeMinLen, //7呼入最短等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeAvgLen, //8呼入平均等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitOverTimeCount, //9呼入應答超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeSumLen, //10呼入累計放棄前等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeMaxLen, //11呼入放棄前最長等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeMinLen, //12呼入放棄前最短等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeAvgLen, //13呼入放棄前平均等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeSumLen, //15呼入累計通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeMaxLen, //15呼入最長通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeMinLen, //呼入最短通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeAvgLen, //呼入平均通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkOverTimeCount, //呼入通話超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeSumLen, //呼入累計話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeMaxLen, //呼入最長話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeMinLen, //呼入最短話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeAvgLen, //呼入平均話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWOverTimeCount, //呼入話后處理超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeSumLen, //呼入累計話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeMaxLen, //呼入最長話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeMinLen, //呼入最短話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeAvgLen, //呼入平均話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonRate, //來話放棄率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAnswerRate, //來話接通率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutCount, //呼出總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutAnsCount, //呼出應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutNoAnsCount, //呼出未應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeSumLen, //呼出累計等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeMaxLen, //呼出最長等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeMinLen, //呼出最短等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeAvgLen, //呼出平均等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitOverTimeCount, //呼出應答超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeSumLen, //呼出累計通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeMaxLen, //呼出最長通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeMinLen, //呼出最短通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeAvgLen, //呼出平均通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkOverTimeCount, //呼出通話超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeSumLen, //呼出累計話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeMaxLen, //呼出最長話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeMinLen, //呼出最短話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeAvgLen, //呼出平均話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWOverTimeCount, //呼出話后處理超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeSumLen, //呼出累計話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeMaxLen, //呼出最長話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeMinLen, //呼出最短話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeAvgLen, //呼出平均話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutFailRate, //呼出失敗率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutAnswerRate, //呼出接通率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkOverTimeCount, //通話超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkTimeSumLen, //累計通話總時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallAnsCount, //累計通話總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWOverTimeCount, //話后處理超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWTimeSumLen, //累計話后處理總時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeSumLen, //累計話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeMaxLen, //最長話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeMinLen, //最短話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeAvgLen, //平均話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallCount, //保持總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeSumLen, //累計保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeMaxLen, //最長保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeMinLen, //最短保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeAvgLen, //平均保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.TranCallCount //轉接總次數
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 4, szStatusData);
  Set_IVR2LOG_Tail();

  if (pIVRCfg->isUpdateGroupSumDataToDB == true)
  {
    char sqls[1024];
  
    sprintf(sqls, "exec sp_UpdateGroupSumData '%d','%s','%s','%s'", 
      nGroupNo, pAgentMng->WorkerGroup[nGroupNo].GroupName.C_Str(), szStatusCount, szStatusData);
    SendExecSql2DB(sqls);
  }

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongroupstatusdata, IVRSndMsg.GetBuf());
    }
  }
}
void SendOneGroupStatusMeteCount(int nGroupNo, int nLOG)
{
  if (nGroupNo <= 0 || nGroupNo >= MAX_AG_GROUP)
    return;
  char szStatusCount[256], szStatusData[1024];
  Set_IVR2LOG_Header(LOGMSG_ongroupstatusdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongroupstatusdata, 1, nGroupNo);
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 2, pAgentMng->WorkerGroup[nGroupNo].GroupName);
  sprintf(szStatusCount, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LogOutCount, //未登錄數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCount, //已登錄話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCount, //示閑話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.BusyWorkerCount, //示忙話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LeaveWorkerCount, //離席話務員數
    
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInRingCount, //呼入振鈴數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkCount, //呼入通話數
    
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutRingCount, //正在呼出數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkCount, //呼出通話數
    
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkCount, //正在通話數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWCount, //正在話后處理數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHoldCount, //正在保持數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallUseCount, //正在使用數
    
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetBusyWorkerCount, //設定示忙話務員數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.SetLeaveWorkerCount, //設定離席話務員數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.LoginWorkerCountB, //已登錄備用話務員數 2015-12-14
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IdelWorkerCountB //備用示閑話務員數 2015-12-14
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 3, szStatusCount);
  
  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDCount, //ACD分配總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.ACDFailCount, //ACD分配失敗總次數

    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInCount, //1來話分配總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAnsCount, //2來話應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInNoAnsCount, //3來話未應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonCount, //4來話放棄總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeSumLen, //5呼入累計等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeMaxLen, //6呼入最長等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeMinLen, //7呼入最短等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitTimeAvgLen, //8呼入平均等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInWaitOverTimeCount, //9呼入應答超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeSumLen, //10呼入累計放棄前等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeMaxLen, //11呼入放棄前最長等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeMinLen, //12呼入放棄前最短等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonTimeAvgLen, //13呼入放棄前平均等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeSumLen, //15呼入累計通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeMaxLen, //15呼入最長通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeMinLen, //呼入最短通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkTimeAvgLen, //呼入平均通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInTalkOverTimeCount, //呼入通話超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeSumLen, //呼入累計話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeMaxLen, //呼入最長話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeMinLen, //呼入最短話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWTimeAvgLen, //呼入平均話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInACWOverTimeCount, //呼入話后處理超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeSumLen, //呼入累計話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeMaxLen, //呼入最長話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeMinLen, //呼入最短話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInHandleTimeAvgLen, //呼入平均話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAbandonRate, //來話放棄率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallInAnswerRate, //來話接通率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutCount, //呼出總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutAnsCount, //呼出應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutNoAnsCount, //呼出未應答總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeSumLen, //呼出累計等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeMaxLen, //呼出最長等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeMinLen, //呼出最短等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitTimeAvgLen, //呼出平均等待時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutWaitOverTimeCount, //呼出應答超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeSumLen, //呼出累計通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeMaxLen, //呼出最長通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeMinLen, //呼出最短通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkTimeAvgLen, //呼出平均通話時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutTalkOverTimeCount, //呼出通話超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeSumLen, //呼出累計話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeMaxLen, //呼出最長話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeMinLen, //呼出最短話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWTimeAvgLen, //呼出平均話后處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutACWOverTimeCount, //呼出話后處理超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeSumLen, //呼出累計話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeMaxLen, //呼出最長話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeMinLen, //呼出最短話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutHandleTimeAvgLen, //呼出平均話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutFailRate, //呼出失敗率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallOutAnswerRate, //呼出接通率
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkOverTimeCount, //通話超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallTalkTimeSumLen, //累計通話總時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallAnsCount, //累計通話總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWOverTimeCount, //話后處理超時總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallACWTimeSumLen, //累計話后處理總時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeSumLen, //累計話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeMaxLen, //最長話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeMinLen, //最短話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.CallHandleTimeAvgLen, //平均話務處理時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallCount, //保持總次數
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeSumLen, //累計保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeMaxLen, //最長保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeMinLen, //最短保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.HoldCallTimeAvgLen, //平均保持時長
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.TranCallCount //轉接總次數
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 4, szStatusData);
  Set_IVR2LOG_Tail();

//   if (pIVRCfg->isUpdateGroupSumDataToDB == true)
//   {
//     char sqls[1024];
//     
//     sprintf(sqls, "exec sp_UpdateGroupSumData '%d','%s','%s','%s'", 
//       nGroupNo, pAgentMng->WorkerGroup[nGroupNo].GroupName.C_Str(), szStatusCount, szStatusData);
//     SendExecSql2DB(sqls);
//   }

  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
  {
    SendMsg2LOG(nLOG, LOGMSG_ongroupstatusdata, IVRSndMsg.GetBuf());
  }
}
void SendAllGroupStatusMeteCount(int nLOG)
{
  for (int nGroupNo = 1; nGroupNo < MAX_AG_GROUP; nGroupNo ++)
  {
    if (pAgentMng->WorkerGroup[nGroupNo].state == 1)
      SendOneGroupStatusMeteCount(nGroupNo, nLOG);
  }
}
void SendSystemStatusMeteCount()
{
  char szStatusCount[256], szStatusData[1024];
  Set_IVR2LOG_Header(LOGMSG_ongroupstatusdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongroupstatusdata, 1, 0);
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 2, "所有話務組");
  sprintf(szStatusCount, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    pAgentMng->AllCallCountParam.LogOutCount, //未登錄數
    pAgentMng->AllCallCountParam.LoginWorkerCount, //已登錄話務員數
    pAgentMng->AllCallCountParam.IdelWorkerCount, //示閑話務員數
    pAgentMng->AllCallCountParam.BusyWorkerCount, //示忙話務員數
    pAgentMng->AllCallCountParam.LeaveWorkerCount, //離席話務員數
    
    pAgentMng->AllCallCountParam.CallInRingCount, //呼入振鈴數
    pAgentMng->AllCallCountParam.CallInTalkCount, //呼入通話數
    
    pAgentMng->AllCallCountParam.CallOutRingCount, //正在呼出數
    pAgentMng->AllCallCountParam.CallOutTalkCount, //呼出通話數
    
    pAgentMng->AllCallCountParam.CallTalkCount, //正在通話數
    pAgentMng->AllCallCountParam.CallACWCount, //正在話后處理數
    pAgentMng->AllCallCountParam.CallHoldCount, //正在保持數

    pAgentMng->AllCallCountParam.CallUseCount, //正在使用數

    pAgentMng->AllCallCountParam.SetBusyWorkerCount, //設定示忙話務員數
    pAgentMng->AllCallCountParam.SetLeaveWorkerCount, //設定離席話務員數

    pAgentMng->AllCallCountParam.LoginWorkerCountB, //已登錄備用話務員數 2015-12-14
    pAgentMng->AllCallCountParam.IdelWorkerCountB, //備用示閑話務員數 2015-12-14

    pAgentMng->AllCallCountParam.IvrChnTotalCount, //IVR通道總數數
    pAgentMng->AllCallCountParam.IvrChnIdelCount, //IVR空閑數
    pAgentMng->AllCallCountParam.IvrChnBusyCount, //IVR占用數
    pAgentMng->AllCallCountParam.IvrChnBlockCount //IVR阻塞數
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 3, szStatusCount);
  
  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    
    pAgentMng->AllCallCountParam.ACDCount, //ACD分配總次數
    pAgentMng->AllCallCountParam.ACDFailCount, //ACD分配失敗總次數

    pAgentMng->AllCallCountParam.CallInCount, //1來話分配總次數
    pAgentMng->AllCallCountParam.CallInAnsCount, //2來話應答總次數
    pAgentMng->AllCallCountParam.CallInNoAnsCount, //3來話未應答總次數
    pAgentMng->AllCallCountParam.CallInAbandonCount, //4來話放棄總次數
    pAgentMng->AllCallCountParam.CallInWaitTimeSumLen, //5呼入累計等待時長
    pAgentMng->AllCallCountParam.CallInWaitTimeMaxLen, //6呼入最長等待時長
    pAgentMng->AllCallCountParam.CallInWaitTimeMinLen, //7呼入最短等待時長
    pAgentMng->AllCallCountParam.CallInWaitTimeAvgLen, //8呼入平均等待時長
    pAgentMng->AllCallCountParam.CallInWaitOverTimeCount, //9呼入應答超時總次數
    pAgentMng->AllCallCountParam.CallInAbandonTimeSumLen, //10呼入累計放棄前等待時長
    pAgentMng->AllCallCountParam.CallInAbandonTimeMaxLen, //11呼入放棄前最長等待時長
    pAgentMng->AllCallCountParam.CallInAbandonTimeMinLen, //12呼入放棄前最短等待時長
    pAgentMng->AllCallCountParam.CallInAbandonTimeAvgLen, //13呼入放棄前平均等待時長
    pAgentMng->AllCallCountParam.CallInTalkTimeSumLen, //15呼入累計通話時長
    pAgentMng->AllCallCountParam.CallInTalkTimeMaxLen, //15呼入最長通話時長
    pAgentMng->AllCallCountParam.CallInTalkTimeMinLen, //呼入最短通話時長
    pAgentMng->AllCallCountParam.CallInTalkTimeAvgLen, //呼入平均通話時長
    pAgentMng->AllCallCountParam.CallInTalkOverTimeCount, //呼入通話超時總次數
    pAgentMng->AllCallCountParam.CallInACWTimeSumLen, //呼入累計話后處理時長
    pAgentMng->AllCallCountParam.CallInACWTimeMaxLen, //呼入最長話后處理時長
    pAgentMng->AllCallCountParam.CallInACWTimeMinLen, //呼入最短話后處理時長
    pAgentMng->AllCallCountParam.CallInACWTimeAvgLen, //呼入平均話后處理時長
    pAgentMng->AllCallCountParam.CallInACWOverTimeCount, //呼入話后處理超時總次數
    pAgentMng->AllCallCountParam.CallInHandleTimeSumLen, //呼入累計話務處理時長
    pAgentMng->AllCallCountParam.CallInHandleTimeMaxLen, //呼入最長話務處理時長
    pAgentMng->AllCallCountParam.CallInHandleTimeMinLen, //呼入最短話務處理時長
    pAgentMng->AllCallCountParam.CallInHandleTimeAvgLen, //呼入平均話務處理時長
    pAgentMng->AllCallCountParam.CallInAbandonRate, //來話放棄率
    pAgentMng->AllCallCountParam.CallInAnswerRate, //來話接通率
    pAgentMng->AllCallCountParam.CallOutCount, //呼出總次數
    pAgentMng->AllCallCountParam.CallOutAnsCount, //呼出應答總次數
    pAgentMng->AllCallCountParam.CallOutNoAnsCount, //呼出未應答總次數
    pAgentMng->AllCallCountParam.CallOutWaitTimeSumLen, //呼出累計等待時長

    pAgentMng->AllCallCountParam.CallOutWaitTimeMaxLen, //呼出最長等待時長
    pAgentMng->AllCallCountParam.CallOutWaitTimeMinLen, //呼出最短等待時長
    pAgentMng->AllCallCountParam.CallOutWaitTimeAvgLen, //呼出平均等待時長
    pAgentMng->AllCallCountParam.CallOutWaitOverTimeCount, //呼出應答超時總次數
    pAgentMng->AllCallCountParam.CallOutTalkTimeSumLen, //呼出累計通話時長
    pAgentMng->AllCallCountParam.CallOutTalkTimeMaxLen, //呼出最長通話時長
    pAgentMng->AllCallCountParam.CallOutTalkTimeMinLen, //呼出最短通話時長
    pAgentMng->AllCallCountParam.CallOutTalkTimeAvgLen, //呼出平均通話時長
    pAgentMng->AllCallCountParam.CallOutTalkOverTimeCount, //呼出通話超時總次數
    pAgentMng->AllCallCountParam.CallOutACWTimeSumLen, //呼出累計話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWTimeMaxLen, //呼出最長話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWTimeMinLen, //呼出最短話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWTimeAvgLen, //呼出平均話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWOverTimeCount, //呼出話后處理超時總次數
    pAgentMng->AllCallCountParam.CallOutHandleTimeSumLen, //呼出累計話務處理時長
    pAgentMng->AllCallCountParam.CallOutHandleTimeMaxLen, //呼出最長話務處理時長
    pAgentMng->AllCallCountParam.CallOutHandleTimeMinLen, //呼出最短話務處理時長
    pAgentMng->AllCallCountParam.CallOutHandleTimeAvgLen, //呼出平均話務處理時長
    pAgentMng->AllCallCountParam.CallOutFailRate, //呼出失敗率
    pAgentMng->AllCallCountParam.CallOutAnswerRate, //呼出接通率
    pAgentMng->AllCallCountParam.CallTalkOverTimeCount, //通話超時總次數
    pAgentMng->AllCallCountParam.CallTalkTimeSumLen, //累計通話總時長
    pAgentMng->AllCallCountParam.CallAnsCount, //累計通話總次數
    pAgentMng->AllCallCountParam.CallACWOverTimeCount, //話后處理超時總次數
    pAgentMng->AllCallCountParam.CallACWTimeSumLen, //累計話后處理總時長
    pAgentMng->AllCallCountParam.CallHandleTimeSumLen, //累計話務處理時長
    pAgentMng->AllCallCountParam.CallHandleTimeMaxLen, //最長話務處理時長
    pAgentMng->AllCallCountParam.CallHandleTimeMinLen, //最短話務處理時長
    pAgentMng->AllCallCountParam.CallHandleTimeAvgLen, //平均話務處理時長
    pAgentMng->AllCallCountParam.HoldCallCount, //保持總次數
    pAgentMng->AllCallCountParam.HoldCallTimeSumLen, //累計保持時長
    pAgentMng->AllCallCountParam.HoldCallTimeMaxLen, //最長保持時長
    pAgentMng->AllCallCountParam.HoldCallTimeMinLen, //最短保持時長
    pAgentMng->AllCallCountParam.HoldCallTimeAvgLen, //平均保持時長
    pAgentMng->AllCallCountParam.TranCallCount //轉接總次數
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 4, szStatusData);
  Set_IVR2LOG_Tail();
  
  if (pIVRCfg->isUpdateGroupSumDataToDB == true)
  {
    char sqls[1024];
    
    sprintf(sqls, "exec sp_UpdateGroupSumData '%d','%s','%s','%s'", 
      0, "All Group", szStatusCount, szStatusData);
    SendExecSql2DB(sqls);
  }

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongroupstatusdata, IVRSndMsg.GetBuf());
    }
  }
}
void SendSystemStatusMeteCount(int nLOG)
{
  char szStatusData[1024];
  Set_IVR2LOG_Header(LOGMSG_ongroupstatusdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongroupstatusdata, 1, 0);
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 2, "所有話務組");
  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    pAgentMng->AllCallCountParam.LogOutCount, //未登錄數
    pAgentMng->AllCallCountParam.LoginWorkerCount, //已登錄數
    pAgentMng->AllCallCountParam.IdelWorkerCount, //示閑數
    pAgentMng->AllCallCountParam.BusyWorkerCount, //示忙數
    pAgentMng->AllCallCountParam.LeaveWorkerCount, //離席數
    
    pAgentMng->AllCallCountParam.CallInRingCount, //呼入振鈴數
    pAgentMng->AllCallCountParam.CallInTalkCount, //呼入通話數
    
    pAgentMng->AllCallCountParam.CallOutRingCount, //正在呼出數
    pAgentMng->AllCallCountParam.CallOutTalkCount, //呼出通話數
    
    pAgentMng->AllCallCountParam.CallTalkCount, //正在通話數
    pAgentMng->AllCallCountParam.CallACWCount, //話后處理數
    pAgentMng->AllCallCountParam.CallHoldCount, //保持數

    pAgentMng->AllCallCountParam.CallUseCount, //正在使用數

    pAgentMng->AllCallCountParam.SetBusyWorkerCount, //設定示忙話務員數
    pAgentMng->AllCallCountParam.SetLeaveWorkerCount, //設定離席話務員數
    
    pAgentMng->AllCallCountParam.LoginWorkerCountB, //已登錄備用話務員數 2015-12-14
    pAgentMng->AllCallCountParam.IdelWorkerCountB, //備用示閑話務員數 2015-12-14

    pAgentMng->AllCallCountParam.IvrChnTotalCount, //IVR通道總數數
    pAgentMng->AllCallCountParam.IvrChnIdelCount, //IVR空閑數
    pAgentMng->AllCallCountParam.IvrChnBusyCount, //IVR占用數
    pAgentMng->AllCallCountParam.IvrChnBlockCount //IVR阻塞數
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 3, szStatusData);
  
  sprintf(szStatusData, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
    
    pAgentMng->AllCallCountParam.ACDCount, //ACD分配總次數
    pAgentMng->AllCallCountParam.ACDFailCount, //ACD分配失敗總次數

    pAgentMng->AllCallCountParam.CallInCount, //1來話分配總次數
    pAgentMng->AllCallCountParam.CallInAnsCount, //2來話應答總次數
    pAgentMng->AllCallCountParam.CallInNoAnsCount, //3來話未應答總次數
    pAgentMng->AllCallCountParam.CallInAbandonCount, //4來話放棄總次數
    pAgentMng->AllCallCountParam.CallInWaitTimeSumLen, //5呼入累計等待時長
    pAgentMng->AllCallCountParam.CallInWaitTimeMaxLen, //6呼入最長等待時長
    pAgentMng->AllCallCountParam.CallInWaitTimeMinLen, //7呼入最短等待時長
    pAgentMng->AllCallCountParam.CallInWaitTimeAvgLen, //8呼入平均等待時長
    pAgentMng->AllCallCountParam.CallInWaitOverTimeCount, //9呼入應答超時總次數
    pAgentMng->AllCallCountParam.CallInAbandonTimeSumLen, //10呼入累計放棄前等待時長
    pAgentMng->AllCallCountParam.CallInAbandonTimeMaxLen, //11呼入放棄前最長等待時長
    pAgentMng->AllCallCountParam.CallInAbandonTimeMinLen, //12呼入放棄前最短等待時長
    pAgentMng->AllCallCountParam.CallInAbandonTimeAvgLen, //13呼入放棄前平均等待時長
    pAgentMng->AllCallCountParam.CallInTalkTimeSumLen, //15呼入累計通話時長
    pAgentMng->AllCallCountParam.CallInTalkTimeMaxLen, //15呼入最長通話時長
    pAgentMng->AllCallCountParam.CallInTalkTimeMinLen, //呼入最短通話時長
    pAgentMng->AllCallCountParam.CallInTalkTimeAvgLen, //呼入平均通話時長
    pAgentMng->AllCallCountParam.CallInTalkOverTimeCount, //呼入通話超時總次數
    pAgentMng->AllCallCountParam.CallInACWTimeSumLen, //呼入累計話后處理時長
    pAgentMng->AllCallCountParam.CallInACWTimeMaxLen, //呼入最長話后處理時長
    pAgentMng->AllCallCountParam.CallInACWTimeMinLen, //呼入最短話后處理時長
    pAgentMng->AllCallCountParam.CallInACWTimeAvgLen, //呼入平均話后處理時長
    pAgentMng->AllCallCountParam.CallInACWOverTimeCount, //呼入話后處理超時總次數
    pAgentMng->AllCallCountParam.CallInHandleTimeSumLen, //呼入累計話務處理時長
    pAgentMng->AllCallCountParam.CallInHandleTimeMaxLen, //呼入最長話務處理時長
    pAgentMng->AllCallCountParam.CallInHandleTimeMinLen, //呼入最短話務處理時長
    pAgentMng->AllCallCountParam.CallInHandleTimeAvgLen, //呼入平均話務處理時長
    pAgentMng->AllCallCountParam.CallInAbandonRate, //來話放棄率
    pAgentMng->AllCallCountParam.CallInAnswerRate, //來話接通率
    pAgentMng->AllCallCountParam.CallOutCount, //呼出總次數
    pAgentMng->AllCallCountParam.CallOutAnsCount, //呼出應答總次數
    pAgentMng->AllCallCountParam.CallOutNoAnsCount, //呼出未應答總次數
    pAgentMng->AllCallCountParam.CallOutWaitTimeSumLen, //呼出累計等待時長
    pAgentMng->AllCallCountParam.CallOutWaitTimeMaxLen, //呼出最長等待時長
    pAgentMng->AllCallCountParam.CallOutWaitTimeMinLen, //呼出最短等待時長
    pAgentMng->AllCallCountParam.CallOutWaitTimeAvgLen, //呼出平均等待時長
    pAgentMng->AllCallCountParam.CallOutWaitOverTimeCount, //呼出應答超時總次數
    pAgentMng->AllCallCountParam.CallOutTalkTimeSumLen, //呼出累計通話時長
    pAgentMng->AllCallCountParam.CallOutTalkTimeMaxLen, //呼出最長通話時長
    pAgentMng->AllCallCountParam.CallOutTalkTimeMinLen, //呼出最短通話時長
    pAgentMng->AllCallCountParam.CallOutTalkTimeAvgLen, //呼出平均通話時長
    pAgentMng->AllCallCountParam.CallOutTalkOverTimeCount, //呼出通話超時總次數
    pAgentMng->AllCallCountParam.CallOutACWTimeSumLen, //呼出累計話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWTimeMaxLen, //呼出最長話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWTimeMinLen, //呼出最短話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWTimeAvgLen, //呼出平均話后處理時長
    pAgentMng->AllCallCountParam.CallOutACWOverTimeCount, //呼出話后處理超時總次數
    pAgentMng->AllCallCountParam.CallOutHandleTimeSumLen, //呼出累計話務處理時長
    pAgentMng->AllCallCountParam.CallOutHandleTimeMaxLen, //呼出最長話務處理時長
    pAgentMng->AllCallCountParam.CallOutHandleTimeMinLen, //呼出最短話務處理時長
    pAgentMng->AllCallCountParam.CallOutHandleTimeAvgLen, //呼出平均話務處理時長
    pAgentMng->AllCallCountParam.CallOutFailRate, //呼出失敗率
    pAgentMng->AllCallCountParam.CallOutAnswerRate, //呼出接通率
    pAgentMng->AllCallCountParam.CallTalkOverTimeCount, //通話超時總次數
    pAgentMng->AllCallCountParam.CallTalkTimeSumLen, //累計通話總時長
    pAgentMng->AllCallCountParam.CallAnsCount, //累計通話總次數
    pAgentMng->AllCallCountParam.CallACWOverTimeCount, //話后處理超時總次數
    pAgentMng->AllCallCountParam.CallACWTimeSumLen, //累計話后處理總時長
    pAgentMng->AllCallCountParam.CallHandleTimeSumLen, //累計話務處理時長
    pAgentMng->AllCallCountParam.CallHandleTimeMaxLen, //最長話務處理時長
    pAgentMng->AllCallCountParam.CallHandleTimeMinLen, //最短話務處理時長
    pAgentMng->AllCallCountParam.CallHandleTimeAvgLen, //平均話務處理時長
    pAgentMng->AllCallCountParam.HoldCallCount, //保持總次數
    pAgentMng->AllCallCountParam.HoldCallTimeSumLen, //累計保持時長
    pAgentMng->AllCallCountParam.HoldCallTimeMaxLen, //最長保持時長
    pAgentMng->AllCallCountParam.HoldCallTimeMinLen, //最短保持時長
    pAgentMng->AllCallCountParam.HoldCallTimeAvgLen, //平均保持時長
    pAgentMng->AllCallCountParam.TranCallCount //轉接總次數
    );
  Set_IVR2LOG_Item(LOGMSG_ongroupstatusdata, 4, szStatusData);
  Set_IVR2LOG_Tail();
  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].LoginLevel != 6 && LogerStatusSet[nLOG].bGetAgStatus == true)
  {
    SendMsg2LOG(nLOG, LOGMSG_ongroupstatusdata, IVRSndMsg.GetBuf());
  }
}
//-----------------------------------------------------------------------------
void WriteWorkerLoginStatus(CAgent *pAgent)
{
  char sqls[512], szStartTime[25];

  if (pIVRCfg->isWriteSeatStatus == false || pAgent == NULL)
    return;

  if (pAgent->ExtnType == 0)
    return;

  strcpy(szStartTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.LoginTime));
  
  sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
    pAgent->m_Worker.WorkerCallCountParam.LoginGID, 
    pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),AG_STATUS_LOGIN,pAgent->m_Worker.DutyNo,
    pAgent->m_Worker.WorkerCallCountParam.LoginTime,szStartTime,
    pAgent->m_Worker.WorkerCallCountParam.LoginTime,szStartTime,
    0,"",0);
  SendExecSql2DB(sqls);
}
void WriteWorkerLogoutStatus(CAgent *pAgent)
{
  char sqls[512], szStartTime[25];
  
  if (pIVRCfg->isWriteSeatStatus == false || pAgent == NULL)
    return;
  if (pAgent->m_Worker.WorkerCallCountParam.LoginTime == 0)
  {
    return;
  }
  
  time_t tm = time(0);
  
  if (pAgent->ExtnType == 1)
  {
    sprintf(sqls, "update tbseatstatus set EndTime=%d,EndTimeLoca='%s',Duration=%d where GID='%s'",
      tm,time_t2str(tm),tm-pAgent->m_Worker.WorkerCallCountParam.LoginTime,pAgent->m_Worker.WorkerCallCountParam.LoginGID);
    SendExecSql2DB(sqls);
  }

  pAgent->m_Worker.WorkerCallCountParam.LoginTime = 0;

  if (pAgent->ExtnType == 1 && pAgent->GetWorkerNo() > 0 && pAgent->m_Worker.WorkerCallCountParam.LogoutTime > 0)
  {
    strcpy(szStartTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.LogoutTime));
    sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
      pAgent->m_Worker.WorkerCallCountParam.LogoutGID, 
      pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),AG_STATUS_LOGOUT,pAgent->m_Worker.DutyNo,
      pAgent->m_Worker.WorkerCallCountParam.LogoutTime,szStartTime,
      pAgent->m_Worker.WorkerCallCountParam.LogoutTime,szStartTime,
      0,"",0);
    SendExecSql2DB(sqls);
  }
}
void WriteWorkerLogoutRecord(CAgent *pAgent)
{
  char sqls[512];
  int t;

  if (pIVRCfg->isWriteSeatStatus == false || pAgent == NULL)
    return;

  if (pAgent->m_Worker.WorkerCallCountParam.LogoutTime == 0)
  {
    pAgent->m_Worker.ClearWork();
    return;
  }
  
  time_t tm = time(0);
  t = tm-pAgent->m_Worker.WorkerCallCountParam.LogoutTime;
  if (t > 3600*pIVRCfg->MinChangeDutyLen)
  {
    return;
  }
  
  if (pAgent->ExtnType == 1)
  {
    sprintf(sqls, "update tbseatstatus set EndTime=%d,EndTimeLoca='%s',Duration=%d where GID='%s'",
      tm,time_t2str(tm),t,pAgent->m_Worker.WorkerCallCountParam.LogoutGID);
    pAgent->m_Worker.WorkerCallCountParam.LogoutTime = 0;
    SendExecSql2DB(sqls);
  }
  
  pAgent->m_Worker.WorkerCallCountParam.OldStatus = 0;
  pAgent->m_Worker.WorkerCallCountParam.OldStatusStartTime = tm;
  pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration = 0;
  pAgent->m_Worker.WorkerCallCountParam.CurStatus = 0;
  pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime = tm;
  pAgent->m_Worker.WorkerCallCountParam.CurStatusEndTime = tm;
}
void WriteWorkerStatus(CAgent *pAgent, int nStatus, time_t tStartTime, time_t tEndTime)
{
  char sqls[512], szStartTime[25], szEndTime[25];

  strcpy(szStartTime, time_t2str(tStartTime));
  strcpy(szEndTime, time_t2str(tEndTime));
  
  if (pIVRCfg->isWriteSeatStatus == false || pAgent == NULL)
  {
    sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','0','0',%d,0,%d,'%s',%d,'%s',%d,'%s',%d)",
      MyGetGUID(), nStatus,tStartTime,szStartTime,tEndTime,szEndTime,tEndTime-tStartTime,"",0);
    SendExecSql2DB(sqls);
  }
  else
  {
    if (pAgent->ExtnType == 1)
    {
      sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
        MyGetGUID(), pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),nStatus,pAgent->m_Worker.DutyNo,
        tStartTime,szStartTime,tEndTime,szEndTime,tEndTime-tStartTime,pAgent->m_Worker.WorkerCallCountParam.CRDSerialNo,0);
      SendExecSql2DB(sqls);
    }
  }
}
//當Status=4時(nFlag:0-未應答 1-應答)
void WriteWorkerStatus(CAgent *pAgent, char *cdrserialno, int nFlag)
{
  char sqls[512], szStartTime[25], szEndTime[25];

  if (pIVRCfg->isWriteSeatStatus == false || pAgent == NULL)
    return;
  if (pAgent->ExtnType == 0)
    return;
  if (pAgent->GetWorkerNo() == 0 && pIVRCfg->isWriteSeatStatusOnLogout == false) //2016-03-17 不登錄也寫狀態
  {
    return;
  }

  strcpy(szStartTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.OldStatusStartTime));
  strcpy(szEndTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime));

  sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
    MyGetGUID(), pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),pAgent->m_Worker.WorkerCallCountParam.OldStatus,pAgent->m_Worker.DutyNo,
    pAgent->m_Worker.WorkerCallCountParam.OldStatusStartTime,szStartTime,
    pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime,szEndTime,
    pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration,
    cdrserialno,nFlag);
  SendExecSql2DB(sqls);
}
void WriteWorkerStatus(CAgent *pAgent, int nFlag)
{
  char sqls[512], szStartTime[25], szEndTime[25];
  char szCRDSerialNo[MAX_CHAR64_LEN];
  int nChn;
  
  if (pIVRCfg->isWriteSeatStatus == false || pAgent == NULL)
    return;
  if (pAgent->GetWorkerNo() == 0 && pIVRCfg->isWriteSeatStatusOnLogout == false) //2016-03-17 不登錄也寫狀態
  {
    return;
  }
  memset(szCRDSerialNo, 0, MAX_CHAR64_LEN);
  if (pAgent->m_Worker.WorkerCallCountParam.OldStatus == 0 && pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration == 0)
  {
    if (pAgent->m_Worker.WorkerCallCountParam.CurStatus == AG_STATUS_IDEL)
      memset(pAgent->m_Worker.WorkerCallCountParam.CRDSerialNo, 0, MAX_CHAR64_LEN);
    return;
  }
  
  if (strlen(pAgent->m_Worker.WorkerCallCountParam.CRDSerialNo) == 0)
  {
    if (strlen(pAgent->CdrSerialNo) > 0)
    {
      strcpy(szCRDSerialNo, pAgent->CdrSerialNo);
    } 
    else
    {
      nChn = pAgent->GetSeatnChn();
      if (pBoxchn->isnChnAvail(nChn))
      {
        if (strlen(pChn->CdrSerialNo) > 0)
        {
          strcpy(szCRDSerialNo, pAgent->CdrSerialNo);
        }
        else
        {
          if (pAgent->m_Worker.WorkerCallCountParam.OldStatus == 4)
            return;
        }
      }
    }
  }
  else
  {
    strcpy(szCRDSerialNo, pAgent->m_Worker.WorkerCallCountParam.CRDSerialNo);
  }
  strcpy(szStartTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.OldStatusStartTime));
  strcpy(szEndTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime));
  
  if (pAgent->ExtnType == 1)
  {
    sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
      MyGetGUID(), pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),pAgent->m_Worker.WorkerCallCountParam.OldStatus,pAgent->m_Worker.DutyNo,
      pAgent->m_Worker.WorkerCallCountParam.OldStatusStartTime,szStartTime,
      pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime,szEndTime,
      pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration,
      szCRDSerialNo,nFlag);
    SendExecSql2DB(sqls);
  }
  if (pAgent->m_Worker.WorkerCallCountParam.CurStatus == AG_STATUS_ACW)
  {
    if (pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen > 0)
    {
      strcpy(szStartTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.StartTalkTime));
      if (pAgent->ExtnType == 1)
      {
        sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
          MyGetGUID(), pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),AG_STATUS_CONNECT,pAgent->m_Worker.DutyNo,
          pAgent->m_Worker.WorkerCallCountParam.StartTalkTime,szStartTime,
          pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime,szEndTime,
          pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen,
          szCRDSerialNo,nFlag);
        SendExecSql2DB(sqls);
      }
      pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen = 0;
    }
  } 
  else if (pAgent->m_Worker.WorkerCallCountParam.CurStatus == AG_STATUS_IDEL)
  {
    if (pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen > 0)
    {
      strcpy(szStartTime, time_t2str(pAgent->m_Worker.WorkerCallCountParam.StartTalkTime));
      if (pAgent->ExtnType == 1)
      {
        sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','%d','%s',%d,%d,%d,'%s',%d,'%s',%d,'%s',%d)",
          MyGetGUID(), pAgent->GetWorkerNo(),pAgent->GetSeatNo().C_Str(),AG_STATUS_HANDLECALL,pAgent->m_Worker.DutyNo,
          pAgent->m_Worker.WorkerCallCountParam.StartTalkTime,szStartTime,
          pAgent->m_Worker.WorkerCallCountParam.CurStatusStartTime,szEndTime,
          pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen,
          szCRDSerialNo,nFlag);
        SendExecSql2DB(sqls);
      }
      pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen = 0;
    }
    pAgent->m_Worker.WorkerCallCountParam.StartTalkTime = 0;
  }
  if (pAgent->m_Worker.WorkerCallCountParam.CurStatus == AG_STATUS_IDEL)
    memset(pAgent->m_Worker.WorkerCallCountParam.CRDSerialNo, 0, MAX_CHAR64_LEN);
}
void WriteWorkerStatus(int nAG, char *cdrserialno, int nFlag)
{
  if (pIVRCfg->isWriteSeatStatus == false || !pAgentMng->isnAGAvail(nAG))
    return;
  WriteWorkerStatus(pAG, cdrserialno, nFlag);
}
void WriteChnIVRStatus(int nChn, int nStatus)
{
  char sqls[512], szStartTime[25], szEndTime[25];
  
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (pIVRCfg->isWriteSeatStatus == false || pChn->IVRStartTime == 0)
  {
    return;
  }
  time_t nCurTime = time(0);
  strcpy(szStartTime, time_t2str(pChn->IVRStartTime));
  strcpy(szEndTime, time_t2str(nCurTime));
  
  sprintf(sqls, "insert into tbseatstatus (Gid,WorkerNo,SeatNo,Status,DutyNo,StartTime,StartTimeLoca,EndTime,EndTimeLoca,Duration,SerialNo,Flag) values ('%s','0','0',%d,0,%d,'%s',%d,'%s',%d,'%s',%d)",
      MyGetGUID(), nStatus,pChn->IVRStartTime,szStartTime,nCurTime,szEndTime,nCurTime-pChn->IVRStartTime,pChn->CdrSerialNo,0);
  SendExecSql2DB(sqls);
  pChn->IVRStartTime = 0;
}
//先累計話務組話務時長
void WriteSeatStatus(CAgent *pAgent, int nStatus, int nFlag)
{
  strcpy(pAgent->StateChangeTime, MyGetNow());
  if (pIVRCfg->isWriteSeatStatus == false)
  {
    return;
  }
  int nGroupNo = pAgent->GetAcdedGroupNo();
  int nOldStatus = pAgent->ChangeStatus(nStatus);
  
  if (nGroupNo < MAX_AG_GROUP && pAgent->ExternalId == 1 && pIVRCfg->isSumCallExtnToExtn == false) //2015-12-26
  {
    switch (nOldStatus)
    {
    case AG_STATUS_INRING:
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallInWaitTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
      pAgentMng->AllCallCountParam.AddCallInWaitTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
      break;
    case AG_STATUS_OUTSEIZE:
    case AG_STATUS_OUTRING:
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallOutWaitTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
      pAgentMng->AllCallCountParam.AddCallOutWaitTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
      break;
    case AG_STATUS_HOLD:
      {
        if (pAgent->m_Worker.WorkerCallCountParam.CallInOut == 2)
        {
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallOutHoldTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
          pAgentMng->AllCallCountParam.AddCallOutHoldTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
        } 
        else
        {
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallInHoldTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
          pAgentMng->AllCallCountParam.AddCallInHoldTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
        }
      }
      break;
    case AG_STATUS_ACW:
      {
        if (pAgent->m_Worker.WorkerCallCountParam.CallInOut == 2)
        {
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallOutACWTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
          pAgentMng->AllCallCountParam.AddCallOutACWTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
        } 
        else
        {
          pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallInACWTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
          pAgentMng->AllCallCountParam.AddCallInACWTimeLen(pAgent->m_Worker.WorkerCallCountParam.OldStatusDuration);
        }
      }
      break;
    }
    switch (nStatus)
    {
    case AG_STATUS_ACW:
      {
        if (pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen > 0)
        {
          if (pAgent->m_Worker.WorkerCallCountParam.CallInOut == 2)
          {
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallOutTalkTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen);
            pAgentMng->AllCallCountParam.AddCallOutTalkTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen);
          } 
          else
          {
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallInTalkTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen);
            pAgentMng->AllCallCountParam.AddCallInTalkTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallConnectedTimeLen);
          }
        }
      }
      break;
    case AG_STATUS_IDEL:
      {
        if (pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen > 0)
        {
          if (pAgent->m_Worker.WorkerCallCountParam.CallInOut == 2)
          {
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallOutHandleTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen);
            pAgentMng->AllCallCountParam.AddCallOutHandleTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen);
          } 
          else
          {
            pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.AddCallInHandleTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen);
            pAgentMng->AllCallCountParam.AddCallInHandleTimeLen(pAgent->m_Worker.WorkerCallCountParam.CallHandleTimeLen);
          }
        }
      }
      break;
    }
  }
  WriteWorkerStatus(pAgent, nFlag);
  AgentStatusCount();
  SendSystemStatusMeteCount();
  SendOneSeatStatusMeteCount(pAgent->nAG);
  SendOneGroupStatusMeteCount(pAgent->nAG);
}
//初次呼叫需要計數呼叫次數的用次函數,參數說明(nInOutFlag: 1-呼入,2-呼出)(nAnsFlag: 0-振鈴 1-應答 2-未振鈴直接應答)
void WriteSeatStatus(CAgent *pAgent, int nStatus, int nFlag, int nInOutFlag, int nAnsFlag)
{
  if (pIVRCfg->isWriteSeatStatus == false)
  {
    return;
  }

  //2015-12-26 外線標志(用來判斷內線之間呼叫不累計呼叫次數)
  if (pIVRCfg->isSumCallExtnToExtn == false && pAgent->ExternalId == 0)
  {
    WriteSeatStatus(pAgent, nStatus, nFlag);
    return;
  }

  int nGroupNo = pAgent->GetAcdedGroupNo();
  if (nInOutFlag == 1) //呼入
  {
    if (nAnsFlag == 0) //振鈴
    {
      pAgent->IncCallInCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallInCount();
      pAgentMng->AllCallCountParam.IncCallInCount();
    }
    else if (nAnsFlag == 1) //應答
    {
      pAgent->IncCallInAnsCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallInAnsCount();
      pAgentMng->AllCallCountParam.IncCallInAnsCount();
    }
    else if (nAnsFlag == 2) //振鈴及應答
    {
      pAgent->IncCallInCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallInCount();
      pAgentMng->AllCallCountParam.IncCallInCount();
      pAgent->IncCallInAnsCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallInAnsCount();
      pAgentMng->AllCallCountParam.IncCallInAnsCount();
    }
  }
  else if (nInOutFlag == 2) //呼出
  {
    if (nAnsFlag == 0)
    {
      pAgent->IncCallOutCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallOutCount();
      pAgentMng->AllCallCountParam.IncCallOutCount();
    }
    else if (nAnsFlag == 1)
    {
      pAgent->IncCallOutAnsCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallOutAnsCount();
      pAgentMng->AllCallCountParam.IncCallOutAnsCount();
    }
    else if (nAnsFlag == 2)
    {
      pAgent->IncCallOutCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallOutCount();
      pAgentMng->AllCallCountParam.IncCallOutCount();
      pAgent->IncCallOutAnsCount();
      pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallOutAnsCount();
      pAgentMng->AllCallCountParam.IncCallOutAnsCount();
    }
  }
  WriteSeatStatus(pAgent, nStatus, nFlag);
}

void WriteAbandonStatus(CAgent *pAgent, int nStatus, time_t tStartTime, time_t tEndTime)
{
  if (pIVRCfg->isWriteSeatStatus == false)
  {
    return;
  }
  int nTimeLen = tEndTime - tStartTime;
  if (pAgent == NULL)
  {
    WriteWorkerStatus(NULL, nStatus, tStartTime, tEndTime);
    pAgentMng->AllCallCountParam.IncCallInCount();
    pAgentMng->AllCallCountParam.IncCallInAbandonCount(nTimeLen);
    SendSystemStatusMeteCount();
  } 
  else
  {
    pAgent->IncCallInAbandonCount();
    WriteWorkerStatus(pAgent, nStatus, tStartTime, tEndTime);
    //WriteSeatStatus(pAgent, nStatus, 0);
    pAgentMng->AllCallCountParam.IncCallInAbandonCount(nTimeLen);
    int nGroupNo = pAgent->GetAcdedGroupNo();
    pAgentMng->WorkerGroup[nGroupNo].GroupCallCountParam.IncCallInAbandonCount(nTimeLen);
    AgentStatusCount();
    SendSystemStatusMeteCount();
    SendOneSeatStatusMeteCount(pAgent->nAG);
    SendOneGroupStatusMeteCount(pAgent->nAG);
  }
}
//-----------------------------------------------------------------------------
void ProcIMCallInMsg(CXMLMsg &IMMsg)
{
  CStringX ArrGroupNo[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrLevel[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrString[8];
  int nAcd, i, num;

  nAcd = pACDQueue->Get_Idle_nAcd();
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    SendIMACDResult((UL)(atol(IMMsg.GetAttrValue(0).C_Str())), IMMsg.GetAttrValue(8).C_Str(), OnACDFail);
    return;
  }

  pAcd->state = 1;
  pAcd->acdState = 0;
  pAcd->waittimer = 0;
  pAcd->ringtimer = 0;
  pAcd->StopId = 0;
  pAcd->Prior = 0xFFFF;
  pAcd->Next = 0xFFFF;

  pAcd->Session.MediaType = 1;
  pAcd->Session.SessionNo = (UL)(atol(IMMsg.GetAttrValue(0).C_Str()));
  pAcd->Session.CmdAddr = 0;
  pAcd->Session.nChn = 0xFFFF;
  pAcd->Session.DialSerialNo = pAcd->Session.SessionNo;
  pAcd->Session.SerialNo = 0;
  pAcd->Session.CallerType = 0;
  pAcd->Session.CallType = 0;
  pAcd->Session.CallerNo = IMMsg.GetAttrValue(1);
  pAcd->Session.CalledNo = IMMsg.GetAttrValue(2);
  pAcd->Session.CmdId = 1; //callseat指令
  pAcd->Session.VxmlId = 0;
  pAcd->Session.FuncNo = 0;
  pAcd->Session.ServiceType = IMMsg.GetAttrValue(7);
  num = SplitString(IMMsg.GetAttrValue(7).C_Str(), ';', 2, ArrString);
  //MyTrace(3, "num=%d str1=%s str2=%s", num, ArrString[0].C_Str(), ArrString[1].C_Str());
  if (num == 1)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = 0;
    //DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else if (num == 2)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = atoi(ArrString[1].C_Str());
    //DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else
  {
    pAcd->Session.SrvType = 0;
    pAcd->Session.SrvSubType = 0;
  }
  pAcd->Session.CRDSerialNo = IMMsg.GetAttrValue(8);
  pAcd->Session.Param = IMMsg.GetAttrValue(11);

  pAcd->ACDRule.TranCallFailCallBack = 0;
  pAcd->ACDRule.AcdedCount = 0;
  pAcd->ACDRule.SeatType = 0;
  pAcd->ACDRule.SeatNo = IMMsg.GetAttrValue(3);
  pAcd->ACDRule.WorkerNo = atoi(IMMsg.GetAttrValue(4).C_Str());
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = 0xFF;
    pAcd->ACDRule.Level[i] = 0;
    pAcd->ACDRule.NowLevel[i] = 0;
  }
  num = SplitTxtLine(IMMsg.GetAttrValue(6).C_Str(), MAX_GROUP_NUM_FORWORKER, ArrGroupNo);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER && i < num; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = atoi(ArrGroupNo[i].C_Str());
    if (pAcd->ACDRule.GroupNo[i] >= MAX_AG_GROUP)
      pAcd->ACDRule.GroupNo[i] = 0;
    pAcd->ACDRule.NowLevel[i] = pAcd->ACDRule.Level[i];
  }
  pAcd->ACDRule.AcdedGroupNo = 0;
  pAcd->ACDRule.AcdedGroupIndex = 0;
  pAcd->ACDRule.AcdedLevel = 0;
  pAcd->ACDRule.AcdRule = (UC)atoi(IMMsg.GetAttrValue(5).C_Str());
  pAcd->ACDRule.LevelRule = 0;
  pAcd->ACDRule.CallMode = 0;
  pAcd->ACDRule.SeatGroupNo = 0;
  pAcd->ACDRule.WaitTimeLen = atoi(IMMsg.GetAttrValue(9).C_Str());
  pAcd->ACDRule.RingTimeLen = atoi(IMMsg.GetAttrValue(10).C_Str());
  pAcd->ACDRule.BusyWaitId = 0;
  pAcd->ACDRule.Priority = 1;

  pAcd->ACDRule.QueueTime = time(0);
  pAcd->ACDRule.StartRingTime = time(0);
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    pAcd->ACDRule.AcdednAGList[i] = -1;
  }
  pAcd->CalledNum = 0;
  for (i = 0; i < MAX_CALLED_NUM; i ++)
  {
    pAcd->Calleds[i].nAG = 0xFFFF;
    pAcd->Calleds[i].pQue_str = NULL;
    pAcd->Calleds[i].CalloutResult = 0;
  }
  if (pAcd->ACDRule.CallMode == 1)
  {
    //設置同呼同組的坐席號碼
    SetGroupSeatNo(nAcd);
  }
  pAcd->CallSeatType = 9; //IM呼入分配

  ChCount.AcdCallCount ++;
  pACDQueue->JoinACDQueue(nAcd);
  SendACDQueueInfoToAll();
  SendIMACDResult((UL)(atol(IMMsg.GetAttrValue(0).C_Str())), IMMsg.GetAttrValue(8).C_Str(), OnACDWait);
}
void ProcIMCancelMsg(CXMLMsg &IMMsg)
{
  int nAcd = pACDQueue->Get_nAcd_By_DialSerialNo(atol(IMMsg.GetAttrValue(0).C_Str()));
  if (pACDQueue->isnAcdAvail(nAcd))
  {
    pAcd->StopId = 1;
  }
  else
  {
    int nAG = pAgentMng->GetnAGByIMId(IMMsg.GetAttrValue(1).C_Str());
    if (pAgentMng->isnAGAvail(nAG))
    {
      int nChn = pAG->GetSeatnChn();
      Release_Chn(nChn, 1, 1);
    }
  }
}
void ProcEMCallInMsg(CXMLMsg &EMMsg)
{
  CStringX ArrGroupNo[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrLevel[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrString[8];
  int nAcd, i, num;

  nAcd = pACDQueue->Get_Idle_nAcd();
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    SendEMACDResult((UL)(atol(EMMsg.GetAttrValue(0).C_Str())), EMMsg.GetAttrValue(8).C_Str(), OnACDFail);
    return;
  }

  pAcd->state = 1;
  pAcd->acdState = 0;
  pAcd->waittimer = 0;
  pAcd->ringtimer = 0;
  pAcd->StopId = 0;
  pAcd->Prior = 0xFFFF;
  pAcd->Next = 0xFFFF;

  pAcd->Session.MediaType = 2;
  pAcd->Session.SessionNo = (UL)(atol(EMMsg.GetAttrValue(0).C_Str()));
  pAcd->Session.CmdAddr = 0;
  pAcd->Session.nChn = 0xFFFF;
  pAcd->Session.DialSerialNo = pAcd->Session.SessionNo;
  pAcd->Session.SerialNo = 0;
  pAcd->Session.CallerType = 0;
  pAcd->Session.CallType = 0;
  pAcd->Session.CallerNo = GetEmailAddr(EMMsg.GetAttrValue(1).C_Str());
  pAcd->Session.CalledNo = EMMsg.GetAttrValue(2);
  pAcd->Session.CmdId = 1; //callseat指令
  pAcd->Session.VxmlId = 0;
  pAcd->Session.FuncNo = 0;
  pAcd->Session.ServiceType = EMMsg.GetAttrValue(7);
  num = SplitString(EMMsg.GetAttrValue(7).C_Str(), ';', 2, ArrString);
  //MyTrace(3, "num=%d str1=%s str2=%s", num, ArrString[0].C_Str(), ArrString[1].C_Str());
  if (num == 1)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = 0;
    //DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else if (num == 2)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = atoi(ArrString[1].C_Str());
    //DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else
  {
    pAcd->Session.SrvType = 0;
    pAcd->Session.SrvSubType = 0;
  }
  pAcd->Session.CRDSerialNo = EMMsg.GetAttrValue(8);
  pAcd->Session.Param = EMMsg.GetAttrValue(11);

  pAcd->ACDRule.TranCallFailCallBack = 0;
  pAcd->ACDRule.AcdedCount = 0;
  pAcd->ACDRule.SeatType = 0;
  pAcd->ACDRule.SeatNo = EMMsg.GetAttrValue(3);
  pAcd->ACDRule.WorkerNo = atoi(EMMsg.GetAttrValue(4).C_Str());
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = 0xFF;
    pAcd->ACDRule.Level[i] = 0;
    pAcd->ACDRule.NowLevel[i] = 0;
  }
  num = SplitTxtLine(EMMsg.GetAttrValue(6).C_Str(), MAX_GROUP_NUM_FORWORKER, ArrGroupNo);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER && i < num; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = atoi(ArrGroupNo[i].C_Str());
    if (pAcd->ACDRule.GroupNo[i] >= MAX_AG_GROUP)
      pAcd->ACDRule.GroupNo[i] = 0;
    pAcd->ACDRule.NowLevel[i] = pAcd->ACDRule.Level[i];
  }
  pAcd->ACDRule.AcdedGroupNo = 0;
  pAcd->ACDRule.AcdedGroupIndex = 0;
  pAcd->ACDRule.AcdedLevel = 0;
  pAcd->ACDRule.AcdRule = (UC)atoi(EMMsg.GetAttrValue(5).C_Str());
  pAcd->ACDRule.LevelRule = 0;
  pAcd->ACDRule.CallMode = 0;
  pAcd->ACDRule.SeatGroupNo = 0;
  pAcd->ACDRule.WaitTimeLen = atoi(EMMsg.GetAttrValue(9).C_Str());
  pAcd->ACDRule.RingTimeLen = atoi(EMMsg.GetAttrValue(10).C_Str());
  pAcd->ACDRule.BusyWaitId = 0;
  pAcd->ACDRule.Priority = 1;

  pAcd->ACDRule.QueueTime = time(0);
  pAcd->ACDRule.StartRingTime = time(0);
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    pAcd->ACDRule.AcdednAGList[i] = -1;
  }
  pAcd->CalledNum = 0;
  for (i = 0; i < MAX_CALLED_NUM; i ++)
  {
    pAcd->Calleds[i].nAG = 0xFFFF;
    pAcd->Calleds[i].pQue_str = NULL;
    pAcd->Calleds[i].CalloutResult = 0;
  }
  if (pAcd->ACDRule.CallMode == 1)
  {
    //設置同呼同組的坐席號碼
    SetGroupSeatNo(nAcd);
  }
  pAcd->CallSeatType = 10; //EMAIL呼入分配

  ChCount.AcdCallCount ++;
  pACDQueue->JoinACDQueue(nAcd);
  SendACDQueueInfoToAll();
  SendEMACDResult((UL)(atol(EMMsg.GetAttrValue(0).C_Str())), EMMsg.GetAttrValue(8).C_Str(), OnACDWait);
}
void ProcEMCancelMsg(CXMLMsg &EMMsg)
{
  int nAcd = pACDQueue->Get_nAcd_By_DialSerialNo(atol(EMMsg.GetAttrValue(0).C_Str()));
  if (pACDQueue->isnAcdAvail(nAcd))
  {
    pAcd->StopId = 1;
  }
  else
  {
    int nAG = pAgentMng->GetnAGByIMId(EMMsg.GetAttrValue(1).C_Str());
    if (pAgentMng->isnAGAvail(nAG))
    {
      int nChn = pAG->GetSeatnChn();
      Release_Chn(nChn, 1, 1);
    }
  }
}
void ProcFAXCallInMsg(CXMLMsg &FAXMsg)
{
  CStringX ArrGroupNo[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrLevel[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrString[8];
  int nAcd, i, num;

  nAcd = pACDQueue->Get_Idle_nAcd();
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    SendFAXACDResult((UL)(atol(FAXMsg.GetAttrValue(0).C_Str())), FAXMsg.GetAttrValue(8).C_Str(), OnACDFail);
    return;
  }

  pAcd->state = 1;
  pAcd->acdState = 0;
  pAcd->waittimer = 0;
  pAcd->ringtimer = 0;
  pAcd->StopId = 0;
  pAcd->Prior = 0xFFFF;
  pAcd->Next = 0xFFFF;

  pAcd->Session.MediaType = 3;
  pAcd->Session.SessionNo = (UL)(atol(FAXMsg.GetAttrValue(0).C_Str()));
  pAcd->Session.CmdAddr = 0;
  pAcd->Session.nChn = 0xFFFF;
  pAcd->Session.DialSerialNo = pAcd->Session.SessionNo;
  pAcd->Session.SerialNo = 0;
  pAcd->Session.CallerType = 0;
  pAcd->Session.CallType = 0;
  pAcd->Session.CallerNo = GetEmailAddr(FAXMsg.GetAttrValue(1).C_Str());
  pAcd->Session.CalledNo = FAXMsg.GetAttrValue(2);
  pAcd->Session.CmdId = 1; //callseat指令
  pAcd->Session.VxmlId = 0;
  pAcd->Session.FuncNo = 0;
  pAcd->Session.ServiceType = FAXMsg.GetAttrValue(7);
  num = SplitString(FAXMsg.GetAttrValue(7).C_Str(), ';', 2, ArrString);
  //MyTrace(3, "num=%d str1=%s str2=%s", num, ArrString[0].C_Str(), ArrString[1].C_Str());
  if (num == 1)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = 0;
    //DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else if (num == 2)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = atoi(ArrString[1].C_Str());
    //DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else
  {
    pAcd->Session.SrvType = 0;
    pAcd->Session.SrvSubType = 0;
  }
  pAcd->Session.CRDSerialNo = FAXMsg.GetAttrValue(8);
  pAcd->Session.Param = FAXMsg.GetAttrValue(11);

  pAcd->ACDRule.TranCallFailCallBack = 0;
  pAcd->ACDRule.AcdedCount = 0;
  pAcd->ACDRule.SeatType = 0;
  pAcd->ACDRule.SeatNo = FAXMsg.GetAttrValue(3);
  pAcd->ACDRule.WorkerNo = atoi(FAXMsg.GetAttrValue(4).C_Str());
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = 0xFF;
    pAcd->ACDRule.Level[i] = 0;
    pAcd->ACDRule.NowLevel[i] = 0;
  }
  num = SplitTxtLine(FAXMsg.GetAttrValue(6).C_Str(), MAX_GROUP_NUM_FORWORKER, ArrGroupNo);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER && i < num; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = atoi(ArrGroupNo[i].C_Str());
    if (pAcd->ACDRule.GroupNo[i] >= MAX_AG_GROUP)
      pAcd->ACDRule.GroupNo[i] = 0;
    pAcd->ACDRule.NowLevel[i] = pAcd->ACDRule.Level[i];
  }
  pAcd->ACDRule.AcdedGroupNo = 0;
  pAcd->ACDRule.AcdedGroupIndex = 0;
  pAcd->ACDRule.AcdedLevel = 0;
  pAcd->ACDRule.AcdRule = (UC)atoi(FAXMsg.GetAttrValue(5).C_Str());
  pAcd->ACDRule.LevelRule = 0;
  pAcd->ACDRule.CallMode = 0;
  pAcd->ACDRule.SeatGroupNo = 0;
  pAcd->ACDRule.WaitTimeLen = atoi(FAXMsg.GetAttrValue(9).C_Str());
  pAcd->ACDRule.RingTimeLen = atoi(FAXMsg.GetAttrValue(10).C_Str());
  pAcd->ACDRule.BusyWaitId = 0;
  pAcd->ACDRule.Priority = 1;

  pAcd->ACDRule.QueueTime = time(0);
  pAcd->ACDRule.StartRingTime = time(0);
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    pAcd->ACDRule.AcdednAGList[i] = -1;
  }
  pAcd->CalledNum = 0;
  for (i = 0; i < MAX_CALLED_NUM; i ++)
  {
    pAcd->Calleds[i].nAG = 0xFFFF;
    pAcd->Calleds[i].pQue_str = NULL;
    pAcd->Calleds[i].CalloutResult = 0;
  }
  if (pAcd->ACDRule.CallMode == 1)
  {
    //設置同呼同組的坐席號碼
    SetGroupSeatNo(nAcd);
  }
  pAcd->CallSeatType = 11; //FAX呼入分配

  ChCount.AcdCallCount ++;
  pACDQueue->JoinACDQueue(nAcd);
  SendACDQueueInfoToAll();
  SendFAXACDResult((UL)(atol(FAXMsg.GetAttrValue(0).C_Str())), FAXMsg.GetAttrValue(8).C_Str(), OnACDWait);
}
void ProcFAXCancelMsg(CXMLMsg &FAXMsg)
{
  int nAcd = pACDQueue->Get_nAcd_By_DialSerialNo(atol(FAXMsg.GetAttrValue(0).C_Str()));
  if (pACDQueue->isnAcdAvail(nAcd))
  {
    pAcd->StopId = 1;
  }
  else
  {
    int nAG = pAgentMng->GetnAGByIMId(FAXMsg.GetAttrValue(1).C_Str());
    if (pAgentMng->isnAGAvail(nAG))
    {
      int nChn = pAG->GetSeatnChn();
      Release_Chn(nChn, 1, 1);
    }
  }
}
int  ProcExtnTransferCall(int nChnSrc, const char *pszSeatNo, int nCalledType, int nTranType, const char *pszCalledNo)
{
  char szResultMsg[512];
  char CalledNo[MAX_TELECODE_LEN];
  int i, nAG, nAG1, nChn, nChn1;

  nAG = pAgentMng->GetnAGBySeatNo(pszSeatNo);
  nAG1 = 0xFFFF;

  MyTrace(3, "ProcExtnTransferCall nChn=%d SeatNo=%s CalledType=%d TranType=%d CalledNo=%s",
    nChnSrc, pszSeatNo, nCalledType, nTranType, pszCalledNo);

  if (!pAgentMng->isnAGAvail(nAG))
    return 1;
  
  nChn = pAG->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
    return 1;
  
  if (g_nSwitchType == 0)
  {
    //板卡版本
    if (pChn->TranStatus != 0)
    {
      //該坐席正處于轉接狀態
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
          pszSeatNo, pszSeatNo, OnOutFail, "seat is transfing");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      return 1;
    }
    nChn1 = pChn->LinkChn[0];
    if (!pBoxchn->isnChnAvail(nChn1))
      nChn1 = nChnSrc;
    if (!pBoxchn->isnChnAvail(nChn1))
    {
      nChn1 = pChn->HoldingnChn;
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        //該坐席無可轉接的電話
        if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
            pszSeatNo, pszSeatNo, OnOutFail, "seat cannot transfer");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        }
        return 1;
      }
    }
    if (pChn1->ssState != CHN_SNG_IN_TALK && pChn1->ssState != CHN_SNG_OT_TALK)
    {
      //被轉接的電話未處于通話狀態
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
          pszSeatNo, pszSeatNo, OnOutFail, "seat is not talking");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      return 1;
    }
    if (pChn1->TranStatus != 0)
    {
      //被轉接的電話已處于轉接狀態
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
          pszSeatNo, pszSeatNo, OnOutFail, "seat is transfing");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      return 1;
    }

    //取被轉接的號碼及號碼類型 1-外線號碼 2-坐席分機號 3-話務員工號
    memset(CalledNo, 0, MAX_TELECODE_LEN);
    if (nCalledType == 2)
    {
      strncpy(CalledNo, pszCalledNo, MAX_TELECODE_LEN-1);
    }
    else if (nCalledType == 3)
    {
      //轉接的是工號,則查詢登錄的坐席號
      nAG1 = pAgentMng->GetnAGByWorkerNo(atoi(pszCalledNo));
      if (pAgentMng->isnAGAvail(nAG1))
      {
        nCalledType = 2;
        strcpy(CalledNo, pAG1->m_Seat.SeatNo.C_Str());
      }
      else
      {
        //轉接的工號不存在
        if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
            pszSeatNo, pszSeatNo, OnOutFail, "the workerno is not exist");
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        }
        return 1;
      }
    }
    else if (nCalledType == 4)
    {
      CACDRule ACDRule;
      ACDRule.TranCallFailCallBack = 0;
      ACDRule.AcdedCount = 0;
      ACDRule.SeatType = 0;
      ACDRule.SeatNo = "0";
      ACDRule.WorkerNo = 0;
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
      {
        ACDRule.GroupNo[i] = -1;
        ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = 0;
      }
      ACDRule.GroupNo[0] = (UC)atoi(pszCalledNo);
      ACDRule.Level[0] = 0;
      ACDRule.NowLevel[0] = 0;
      ACDRule.AcdedGroupNo = 0;
      ACDRule.AcdRule = 2;
      ACDRule.LevelRule = 0;
      ACDRule.CallMode = 0;
      ACDRule.SeatGroupNo = 0;
      ACDRule.WaitTimeLen = 0;
      ACDRule.RingTimeLen = 0;
      ACDRule.BusyWaitId = 0;
      ACDRule.Priority = 0;
    
      ACDRule.QueueTime = time(0);
      ACDRule.StartRingTime = time(0);
      for (i = 0; i < MAX_AG_NUM; i ++)
      {
        ACDRule.AcdednAGList[i] = -1;
      }
      CAgent *pAgent3=GetIdelAgentForCallOut(ACDRule);
      if (pAgent3 != NULL)
      {
        nAG1 = pAgent3->nAG;
        strcpy(CalledNo, pAgent3->GetSeatNo().C_Str());
      }
      nCalledType = 2;
    }
    else
    {
      nCalledType = 1;
      strncpy(CalledNo, pszCalledNo, MAX_TELECODE_LEN-1);
    }
    if (nCalledType == 2)
    {
      //如果轉接的是坐席,判斷是否空閑
      nAG1 = pAgentMng->GetnAGBySeatNo(CalledNo);
      if (pAgentMng->isnAGAvail(nAG1))
      {
        if (!pAG1->isIdelForSrv())
        {
          //轉接的坐席正忙
          if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
          {
            sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
              pszSeatNo, pszSeatNo, OnOutBusy, "the dest seat is busy"); //2015-10-13 edit
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
          return 1;
        }
      }
      else
      {
        //轉接的坐席分機號不存在
        if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
            pszSeatNo, pszSeatNo, OnOutUNN, "the seatno is not exist"); //2015-10-13 edit
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        }
        return 1;
      }
    }

    int nOut = pCallbuf->Get_Idle_nOut();
    if (!pCallbuf->isnOutAvail(nOut))
    {
      //轉接緩沖區溢出
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s", 
          pszSeatNo, pszSeatNo, OnOutFail, "transfer buffer overflow");
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      return 1;
    }
    pAG->TranDesnAG = nAG1;
    //設置呼出數據
    pOut->state = 1;
    pOut->timer = 0;
    pOut->CallStep = 0;
  
    if (nCalledType == 1)
      pOut->RingTime = 45;
    else
      pOut->RingTime = 30;
    pOut->IntervalTime = 0;
  
    pOut->Session.SessionNo = 0;
    pOut->Session.CmdAddr = 0;
    pOut->Session.nChn = nChn;
    pOut->Session.DialSerialNo = GetCallOutSerialNo(nChn);
    pOut->Session.SerialNo = 1;
    pOut->Session.CmdId = 3; //坐席轉接
    pOut->Session.VxmlId = 1;
    pOut->Session.FuncNo = 0;
    pOut->Session.CallerNo = pChn->CustPhone;
    pOut->Session.CalledNo = CalledNo;
    pOut->Session.GenerateCdrSerialNo(pChn->lgChnType, pChn->lgChnNo);
    char szTemp[256];
    sprintf(szTemp, "0`%s`%s`%s", pOut->Session.CdrSerialNo, pOut->Session.RecordFileName, pIVRCfg->SeatRecPath.C_Str());
    pOut->Session.Param = szTemp;
  
    if (nCalledType == 1)
    {
      pOut->CallerNo = pIVRCfg->CenterCode;
    } 
    else
    {
      if (pChn->CallInOut == 1)
        pOut->CallerNo = pChn->CallerNo;
      else
        pOut->CallerNo = pChn->CalledNo;
    }
    pOut->CallerType = 0;
    pOut->CallType = 0;
    pOut->CallSeatType = 1;
    pOut->CancelId = 0;
  
    pOut->CallMode = 0;
    pOut->CallTimes = 1;
    pOut->CalledTimes = 0;
  
    pOut->CalledNum = 1;
    strcpy(pOut->Calleds[0].CalledNo, CalledNo);
    pOut->Calleds[0].CalledType = nCalledType;
    pOut->Calleds[0].pQue_str = NULL;
    pOut->Calleds[0].CalloutResult = 0;
    pOut->Calleds[0].OutnChn = 0xFFFF;
    pOut->RouteNo[0] = 1;
  
    pOut->StartPoint = 0;
    pOut->CallPoint = 0;
    pOut->CallTime = 0;
  
    pOut->OutnChn = 0xFFFF;

    if (nTranType == 2)
      pOut->TranMode = 1; //快速盲轉
    else if (nTranType == 3)
      pOut->TranMode = 3; //會議轉接
    else
      pOut->TranMode = 2; //協商轉接

    pChn->nOut = nOut;
    pOut->TranCallFailReturn = 0;
    pOut->nAG = 0xFFFF;
  
    //斷開與正在的通話,并對其放等待音
    pChn->TranStatus = 3;
    pChn->TranControlnChn = nChn1;
    if (pOut->TranMode == 2)
      pChn->HoldingnChn = 0xFFFF;
    pChn1->TranStatus = 1;
    pChn1->TranControlnChn = nChn;
    if (pOut->TranMode == 2)
      pChn1->HoldingnChn = 0xFFFF;
  
    pOut->WaitVocFile = pIVRCfg->WaitVocFile;
    if (pChn->LinkType[0] != 0)
    {
      UnRouterTalk(nChn, nChn1);
      pChn->LinkType[0] = 0;
      pChn->LinkChn[0] = 0xFFFF;
      if (pOut->TranMode != 2)
        pChn->HoldingnChn = nChn1;
      pChn1->LinkType[0] = 0;
      pChn1->LinkChn[0] = 0xFFFF;
      if (pOut->TranMode != 2)
        pChn1->HoldingnChn = nChn;
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
      PlayFile(nChn1, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
    
      WriteSeatStatus(pAG, AG_STATUS_HOLD, 0);
      pAG->SetAgentsvState(AG_SV_HOLD);

      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=1", 
          pszSeatNo, pszSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }

      DispAgentStatus(nAG);
    }
    else
    {
      PlayFile(nChn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
    }
    
    if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
    {
      sprintf(szResultMsg, "wsclientid=%s;cmd=transferresult;seatno=%s;result=%d;reason=%s;", 
        pszSeatNo, pszSeatNo, OnOutCalling, "transfering...");
      SendMsg2WS(WebSocketClientId, 0, szResultMsg);
    }
    return 0;
  }
  return 1;
}
int  ProcExtnStopTransfer(const char *pszSeatNo)
{
  char szResultMsg[512];
  int nAG, nAG1, nChn, nChn1, nChn2, nOut, nResult;

  nAG = pAgentMng->GetnAGBySeatNo(pszSeatNo);
  nAG1 = 0xFFFF;
  
  if (!pAgentMng->isnAGAvail(nAG))
    return 1;
  
  nChn = pAG->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
    return 1;
  
  if (g_nSwitchType == 0)
  {
    //板卡版本
    nOut = pChn->nOut;
    if (pCallbuf->isnOutAvail(nOut))
    {
      pOut->CancelId = 2;
      nChn1 = pChn->TranControlnChn;
      if (pBoxchn->isnChnAvail(nChn1))
      {
        //是否已與轉接的目的通道交換了
        nChn2 = pChn->LinkChn[0];
        if ((pChn->LinkType[0] == 1 || pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7) && pBoxchn->isnChnAvail(nChn2))
        {
          UnRouterTalk(nChn, nChn2);
          pChn->LinkType[0] = 0;
          pChn->LinkChn[0] = 0xFFFF;
          pChn2->LinkType[0] = 0;
          pChn2->LinkChn[0] = 0xFFFF;
        }
        else
        {
          StopClearPlayDtmfBuf(nChn);
        }
        
        StopClearPlayDtmfBuf(nChn1);
        nResult = RouterTalk(nChn, nChn1);
        if (nResult == 0)
        {
          pChn->RecMixerFlag = 1;
          pChn->LinkType[0] = 1;
          pChn->LinkChn[0] = nChn1;
          pChn->HoldingnChn = 0xFFFF;
          
          pChn1->RecMixerFlag = 1;
          pChn1->LinkType[0] = 1;
          pChn1->LinkChn[0] = nChn;
          pChn1->HoldingnChn = 0xFFFF;
          
          WriteSeatStatus(pAG, AG_STATUS_TALK, 0);
          pAG->SetAgentsvState(AG_SV_CONN);
          DispAgentStatus(nAGn);
          if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
          {
            sprintf(szResultMsg, "wsclientid=%s;cmd=holdstate;seatno=%s;state=0;", 
              pszSeatNo, pszSeatNo);
            SendMsg2WS(WebSocketClientId, 0, szResultMsg);
          }
        }
      }
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(szResultMsg, "wsclientid=%s;cmd=stoptransferresult;seatno=%s;result=0;", 
          pszSeatNo, pszSeatNo);
        SendMsg2WS(WebSocketClientId, 0, szResultMsg);
      }
      
      nAG1 = pAG->TranDesnAG;
      if (pAgentMng->isnAGAvail(nAG1))
      {
        if (WebSocketLogId == true && strlen(pAG1->m_Seat.WSClientId) > 0)
        {
          sprintf(szResultMsg, "wsclientid=%s;cmd=stoptransfer;seatno=%s;result=%d;", 
            pAG1->m_Seat.WSClientId, pAG1->GetSeatNo().C_Str(), OnSuccess);
          SendMsg2WS(WebSocketClientId, 0, szResultMsg);
        }
        pAG->TranDesnAG = 0xFFFF;
      }
    }
    
    return 0;
  }
  return 1;
}
