//---------------------------------------------------------------------------
/*
會議類定義
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "boxconf.h"

//---------------------------------------------------------------------------
CBoxconf::CBoxconf()
{
	isInit = false;
  MaxCfcNum = 0;
	pCfc_str = NULL;
}

CBoxconf::~CBoxconf()
{
  if (pCfc_str != NULL)
  {
		delete []pCfc_str;
  	pCfc_str = NULL;
  }
}

int CBoxconf::Init(int TotalConf)
{
	if (TotalConf <= 0) return 1;
  if (isInit == true) return 1;

  MaxCfcNum = TotalConf;
	pCfc_str = new VXML_CONF_STRUCT[MaxCfcNum];
  if ( pCfc_str == NULL ) return 0x01;
  isInit = true;
  for (int i = 0; i < MaxCfcNum; i ++)
  {
  	InitConf(i);
  }
  return 0;
}
void CBoxconf::ClearPlayData(US nCfc)
{
  pCfc_str[nCfc].curPlayState = 0;
	//初始化放音緩沖
	pCfc_str[nCfc].curPlayData.state = 0;

  pCfc_str[nCfc].curPlayData.CanPlayId = 0;
	pCfc_str[nCfc].curPlayData.PlayState = 0;
	pCfc_str[nCfc].curPlayData.PlayTypeId = 0;
	pCfc_str[nCfc].curPlayData.StartPos = 0;
	pCfc_str[nCfc].curPlayData.Length = -1;
	pCfc_str[nCfc].curPlayData.DataBufNum = 0;
	for (int i = 0; i < MAX_PLAY_INDEX_NUM; i ++)
	{
		pCfc_str[nCfc].curPlayData.PlayIndex[i].PlayType = 0;
		pCfc_str[nCfc].curPlayData.PlayIndex[i].PlayFormat = 0;
		pCfc_str[nCfc].curPlayData.PlayIndex[i].PlayFile.Empty();
		pCfc_str[nCfc].curPlayData.PlayIndex[i].PlayIndex = 0xFFFF;
	}
	pCfc_str[nCfc].curPlayData.PlayPoint = 0;
	pCfc_str[nCfc].curPlayData.PlayedTime = 0;
	pCfc_str[nCfc].curPlayData.PlayedLen = 0;
	pCfc_str[nCfc].curPlayData.PlayEndReleaseId = 0;
}
void CBoxconf::ClearRecData(US nCfc)
{
  pCfc_str[nCfc].curRecState = 0;
	pCfc_str[nCfc].RecTimer = 0;
	//初始化錄音緩沖
	pCfc_str[nCfc].curRecData.state = 0;
	pCfc_str[nCfc].curRecData.SessionNo = 0;
	pCfc_str[nCfc].curRecData.CmdAddr = 0;
	pCfc_str[nCfc].curRecData.lgChnType = 0;
	pCfc_str[nCfc].curRecData.lgChnNo = 0;
	pCfc_str[nCfc].curRecData.CanRecId = 0;
	pCfc_str[nCfc].curRecData.RecState = 0;
	pCfc_str[nCfc].curRecData.RecTypeId = 0;
	pCfc_str[nCfc].curRecData.Format = 0;
	pCfc_str[nCfc].curRecData.RecordFile.Empty();
	pCfc_str[nCfc].curRecData.TermChar.Empty();
	pCfc_str[nCfc].curRecData.StartPos = 0;
	pCfc_str[nCfc].curRecData.MaxTime = 0;
	pCfc_str[nCfc].curRecData.RecedLen = 0;
	pCfc_str[nCfc].curRecData.RecedByte = 0;
}
void CBoxconf::ClearPlayRule(US nCfc)
{
	pCfc_str[nCfc].PlayPauseTimer = 0;
	//初始化當前放音規則
	pCfc_str[nCfc].curPlayRule.state = 0;
	pCfc_str[nCfc].curPlayRule.ruleid = 0;
	pCfc_str[nCfc].curPlayRule.format = 0;
	pCfc_str[nCfc].curPlayRule.repeat = 0;
	pCfc_str[nCfc].curPlayRule.duration = 0;
	pCfc_str[nCfc].curPlayRule.broadcast = 0;
	pCfc_str[nCfc].curPlayRule.volume = 0;
	pCfc_str[nCfc].curPlayRule.speech = 0;
	pCfc_str[nCfc].curPlayRule.voice = 0;
	pCfc_str[nCfc].curPlayRule.pause = 0;
	pCfc_str[nCfc].curPlayRule.error = 0;
}
void CBoxconf::InitConf(US nCfc)
{
	int i;

  if (isInit == false || nCfc >= MaxCfcNum) return;

	pCfc_str[nCfc].state = 0;
	pCfc_str[nCfc].timer = 0;

	pCfc_str[nCfc].GroupNo = 0xFFFF;
	pCfc_str[nCfc].RoomNo = 0xFFFF;

	pCfc_str[nCfc].CreateId = 0;
	pCfc_str[nCfc].UseId = 0;

	pCfc_str[nCfc].ChType = 0;
	pCfc_str[nCfc].ChIndex = 0;
	pCfc_str[nCfc].CreateChnExitId = 0;

	pCfc_str[nCfc].Name.Empty();
	pCfc_str[nCfc].Owner.Empty();
	pCfc_str[nCfc].Password.Empty();

  pCfc_str[nCfc].MaxPresiders = 0;
  pCfc_str[nCfc].MaxTalkers = 0;
	pCfc_str[nCfc].MaxListeners = 0;
	pCfc_str[nCfc].FreeId = 0;
  pCfc_str[nCfc].AutoRecordId = 0;
	
  pCfc_str[nCfc].Presideds = 0;
  pCfc_str[nCfc].Talkeds = 0;
	pCfc_str[nCfc].Listeneds = 0;
  pCfc_str[nCfc].KnockTone = 0;

  for (i = 0; i < MAX_PRESIDER_NUM; i ++)
    pCfc_str[nCfc].Presiders[i] = 0xFFFF;
  for (i = 0; i < MAX_TALKERS_NUM; i ++)
    pCfc_str[nCfc].Talkers[i] = 0xFFFF;
  for (i = 0; i < MAX_LISTENERS_NUM; i ++)
  {
  	pCfc_str[nCfc].Listeners[i] = 0xFFFF;
  	pCfc_str[nCfc].EmptyEvent[i] = 0;
  }

	//初始化放音緩沖
  ClearPlayData(nCfc);
	//初始化錄音緩沖
  ClearRecData(nCfc);
	//初始化當前放音規則
  ClearPlayRule(nCfc);

  pCfc_str[nCfc].curPlayState = 0;
	pCfc_str[nCfc].curRecState = 0;
}
void CBoxconf::ResetConf(US nCfc)
{
	int i;

  if (isInit == false || nCfc >= MaxCfcNum) return;

	pCfc_str[nCfc].state = 0;
	pCfc_str[nCfc].timer = 0;

  pCfc_str[nCfc].Presideds = 0;
  pCfc_str[nCfc].Talkeds = 0;
	pCfc_str[nCfc].Listeneds = 0;

  for (i = 0; i < MAX_PRESIDER_NUM; i ++)
    pCfc_str[nCfc].Presiders[i] = 0xFFFF;
  for (i = 0; i < MAX_TALKERS_NUM; i ++)
    pCfc_str[nCfc].Talkers[i] = 0xFFFF;
  for (i = 0; i < MAX_LISTENERS_NUM; i ++)
  {
  	pCfc_str[nCfc].Listeners[i] = 0xFFFF;
    pCfc_str[nCfc].EmptyEvent[i] = 0;
  }

	//初始化放音緩沖
  ClearPlayData(nCfc);
	//初始化錄音緩沖
  ClearRecData(nCfc);
	//初始化當前放音規則
  ClearPlayRule(nCfc);

  pCfc_str[nCfc].curPlayState = 0;
	pCfc_str[nCfc].curRecState = 0;
}
void CBoxconf::TimerAdd(int nTCount)
{
  for (int nCfc = 0; nCfc < MaxCfcNum; nCfc ++)
  {
    pCfc_str[nCfc].timer ++;
    pCfc_str[nCfc].PlayPauseTimer ++;
    pCfc_str[nCfc].PlayedTimer ++;
    pCfc_str[nCfc].RecTimer ++;
  }
}
int CBoxconf::Get_Idle_nCfc() //取空閑的會議室號
{
  int nCfc;

  for (nCfc = 1; nCfc < MaxCfcNum; nCfc ++)
  {
    if (pCfc_str[nCfc].state == 0)
      return nCfc;
  }
  return 0xFFFF;
}
//讀會議配置文件
void CBoxconf::Read_ConfIniFile(char *filename)
{
	int nCfc, ConfNum;

  ConfNum = GetPrivateProfileInt("CONF","ConfNum", 0, filename);
	if (ConfNum > 0)
	{
    for (nCfc = 0; nCfc <= ConfNum && nCfc < MaxCfcNum; nCfc ++)
		{
			Read_OneConfIniFile(filename, nCfc);
		}
  }
}
void CBoxconf::Read_OneConfIniFile(char *filename, int nCfc)
{
	CH pzTemp[16], txtline[128];
  CStringX ArrString[6];

	if (nCfc < MaxCfcNum)
	{
		sprintf(pzTemp, "Conf[%d]", nCfc);
		GetPrivateProfileString("CONF", pzTemp, "", txtline, 120, filename);
    if (SplitTxtLine(txtline, 6, ArrString) >= 5)
    {
      pCfc_str[nCfc].state = 1;
      pCfc_str[nCfc].Name = ArrString[0];
      pCfc_str[nCfc].MaxPresiders = (UC)atoi(ArrString[1].C_Str());
      pCfc_str[nCfc].MaxTalkers = (UC)atoi(ArrString[2].C_Str());
      if (pCfc_str[nCfc].MaxPresiders > MAX_PRESIDER_NUM)
      {
        pCfc_str[nCfc].MaxPresiders = MAX_PRESIDER_NUM;
      }
      if (pCfc_str[nCfc].MaxTalkers == 0)
      {
        pCfc_str[nCfc].MaxTalkers = 2;
      }
      else if (pCfc_str[nCfc].MaxTalkers > MAX_TALKERS_NUM)
      {
        pCfc_str[nCfc].MaxTalkers = MAX_TALKERS_NUM;
      }
      pCfc_str[nCfc].MaxListeners = (UC)atoi(ArrString[3].C_Str());
      pCfc_str[nCfc].AutoRecordId = (UC)atoi(ArrString[4].C_Str());
      pCfc_str[nCfc].Password = ArrString[5];
		  
      //MyTrace(3, "會議名稱 = %s 最大主持人數 = %d 最大加入人數 = %d 最大旁聽人數 = %d 自動錄音 = %d 加入密碼 = %s", 
      //  pCfc_str[nCfc].Name.C_Str(), pCfc_str[nCfc].MaxPresiders, pCfc_str[nCfc].MaxTalkers, pCfc_str[nCfc].MaxListeners, pCfc_str[nCfc].AutoRecordId, pCfc_str[nCfc].Password.C_Str());
    }
	}
}
//創建系統會議室
int CBoxconf::CreateAllSysConf(int ncfc) 
{
  for (int nCfc = ncfc; nCfc < MaxCfcNum; nCfc ++)
  {
    if (MyCreateOneConf(nCfc) == 0)
      break;
  }
  return 0;
}

int CBoxconf::MyCreateOneConf(int nCfc)
{
  int nResult, ToTalTalkers;

  if (nCfc >= MaxCfcNum) return -1;
  
  if (pCfc_str[nCfc].state == 1 && pCfc_str[nCfc].CreateId == 0)
  {
    ToTalTalkers = pCfc_str[nCfc].MaxPresiders + pCfc_str[nCfc].MaxTalkers;

    pCfc_str[nCfc].CreateId = 1;
    pCfc_str[nCfc].UseId = 1;
  
    pCfc_str[nCfc].FreeId = 0;

    nResult = CreateConf(nCfc, 0, ToTalTalkers, pCfc_str[nCfc].MaxListeners, 1);
    if (nResult == nCfc)
    {
      pCfc_str[nCfc].CreateId = 3;
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
      return nCfc;
    }
    return nResult;
  }
  return -1;
}
