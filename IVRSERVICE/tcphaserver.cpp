// TCPServer.cpp: implementation of the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#include "stdafx.h"
#include "tcphaserver.h"
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

CTCPHALink::CTCPHALink()
{
  isLinked = false;
	IsOpen = false;
  isIVRInit = false;
	hdll = NULL;
  memset(m_szAppPath, 0, MAX_PATH_LEN);
  strcpy(m_szDllFile, "tmhaserver.dll");
  IVRServerId = 0x0001;
  IVRServerPort = 5225;
  IsStandbyIVRServerConnected = false;
}

CTCPHALink::~CTCPHALink()
{
  ReleaseDll();
}

bool CTCPHALink::LoadDll()
{
  if (strlen(m_szAppPath) > 0)
    sprintf(m_szDllFile, "%s\\tmhaserver.dll", m_szAppPath);
  else
    strcpy(m_szDllFile, "tmhaserver.dll");
	hdll = LoadLibrary(m_szDllFile);
	if ( hdll != NULL )
	{
    AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(  !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MyTrace(0, "GetProcAddress tmhaserver.dll eror");
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MyTrace(0, "LoadLibrary tmhaserver.dll eror");
		return false;
	}
}

void CTCPHALink::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}

bool CTCPHALink::ReadIni(char *filename)
{
  m_nMultiRunMode = GetPrivateProfileInt("CONFIGURE", "MultiRunMode", 0, filename);
  m_nHostType = GetPrivateProfileInt("CONFIGURE", "HostType", 0, filename);

  isIVRInit = false;
  IVRServerId = CombineID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "IVRServerId", 1, filename));
  IVRServerPort = GetPrivateProfileInt("IVRSERVER", "HAServerPort", 5223, filename);

  //對方主機
  MasterIVRServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "MASTERIVRServerIP", "127.0.0.1", MasterIVRServer.ServerIP, 30, filename);
  MasterIVRServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "HAServerPort", 5223, filename);
  MasterIVRServer.Serverid = CombineID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "MASTERIVRServerID", 1, filename));

  return true;
}    

bool CTCPHALink::ReadWebSetupIni(char *filename)
{
  m_nMultiRunMode = GetPrivateProfileInt("CONFIGURE", "MultiRunMode", 0, filename);
  m_nHostType = GetPrivateProfileInt("CONFIGURE", "HostType", 0, filename);

  isIVRInit = false;
  IVRServerId = CombineID(NODE_IVR,GetPrivateProfileInt("TCPLINK", "IVRServerID", 1, filename));
  IVRServerPort = GetPrivateProfileInt("TCPLINK", "HAServerPort", 5223, filename);

  MasterIVRServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "MASTERIVRServerIP", "127.0.0.1", MasterIVRServer.ServerIP, 30, filename);
  MasterIVRServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "HAServerPort", 5223, filename);
  MasterIVRServer.Serverid = CombineID(NODE_DB,GetPrivateProfileInt("IVRSERVER", "MASTERIVRServerID", 1, filename));

  return true;
}    

int CTCPHALink::SendMessage(US ServerId, US MsgId, int MsgLen, const CH *MsgBuf)
{
  int SendedLen;
  
  SendedLen = AppSendData(ServerId, MsgId, (UC *)MsgBuf, MsgLen);
  if (SendedLen != MsgLen)
    MyTrace(0, "Send msg to HA error ServerId=0x%04x MsgId=0x%04x MsgBuf=%s", ServerId, MsgId, MsgBuf);
  return SendedLen;
}
//IVR發送消息到客戶端
int CTCPHALink::SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf)
{
	if (Clientid == 0) return 0;
	int SendedLen, MsgLen;
  MsgLen = strlen(MsgBuf);
  SendedLen = AppSendData(Clientid, MsgId, (UC *)MsgBuf , MsgLen);
  if (SendedLen != MsgLen)
  {
    MyTrace(0, "Send msg to HA error Clientid=0x%04x MsgId=0x%04x MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPHALink::SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len)
{
	if (Clientid == 0) return 0;
	int SendedLen;
  SendedLen = AppSendData(Clientid, MsgId, MsgBuf , len);
  if (SendedLen != len)
  {
    MyTrace(0, "IVR send msg error Clientid=%04x MsgId=%d MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPHALink::SendMessage2Master(US MsgId, const CH *MsgBuf)
{
  return SendMessage(MasterIVRServer.Serverid, (MSGTYPE_HAIVR<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPHALink::InitIVRServer()
{
	if (m_nMultiRunMode == 0)
		return 0;
  if (m_nHostType != 0)
    return 0;
    
  if (isIVRInit == true)
    return 0;
  if (IsOpen == true)
  {
  	//服務端
    AppInitS(IVRServerId, IVRServerPort, OnIVRLogin_A, OnIVRClose_A, OnIVRReceiveData_A);
    MyTrace(9, "Init HAServer ServerId=0x%04x ServerPort=%d", 
      IVRServerId, IVRServerPort);
    isIVRInit = true;
    return 0;
  }
  else
  {
    return 1;
  }
}
int CTCPHALink::ConnectMasterIVRServer()
{
	if (m_nMultiRunMode == 0)
		return 0;
  if ((MasterIVRServer.Serverid&0xFF) == 0) 
    return 0;
  if (m_nHostType == 0) 
    return 0;
  if (IsOpen == true)
  {
    AppInit(MasterIVRServer.Serverid,
      IVRServerId,
      MasterIVRServer.ServerIP,
      MasterIVRServer.ServerPort,
      OnIVRLogin_B,
      OnIVRClose_B,
      OnIVRReceiveData_B);
    MyTrace(9, "Connect MasterHAServer LocaClientId=0x%04x ServerId=0x%04x ServerPort=%d ServerIP=%s", 
      IVRServerId, MasterIVRServer.Serverid, MasterIVRServer.ServerPort, MasterIVRServer.ServerIP);
    return 0;
  }
  else
  {
    //加載動態連接庫失敗
    return 1;
  }
}
int CTCPHALink::InitTCPLink()
{
  int nResult = 0;
	if (m_nMultiRunMode == 0)
		return 0;
  if (LoadDll() == false)
    return 1;
  if (InitIVRServer() != 0)
    nResult = 2;
  if (ConnectMasterIVRServer() != 0)
    nResult = 3;
  return nResult;
}
