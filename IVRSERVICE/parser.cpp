//---------------------------------------------------------------------------
/*
流程解析類定義
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "parser.h"

//---------------------------------------------------------------------------
CParser::CParser()
{
  pAcc_str = NULL;
}

CParser::~CParser()
{
  if (pAcc_str != NULL)
  {
    delete []pAcc_str;
    pAcc_str = NULL;
  }
}
//初始化
int CParser::Init(void)
{
	int nAcc;
	
	memset(VxmlLogId, 0, MAX_NODE_XML);
	pAcc_str = new VXML_PRECODE_STRUCT[MAX_ACCESSCODES_NUM];
	if (pAcc_str == NULL) return 1;
	for (nAcc = 0; nAcc < MAX_ACCESSCODES_NUM; nAcc++)
	{
		pAcc->state = 0;
		memset(pAcc->code, 0, 20);
		pAcc->minlen = 0xFF;
		pAcc->maxlen = 0xFF;
		pAcc->NodeXMLs = 0;
		for (int i = 0; i < MAX_NODE_XML; i ++)
      pAcc->NodeXML[i] = 0xFFFF;
		pAcc->AssignPoint = 0;
		pAcc->prior = 0xFFFF;
		pAcc->next = 0xFFFF;
	}
	nAcc = 0;
	pAcc->state = 1;
	pAcc->prior = 0;
	pAcc->next = 0xFFFF;
	return 0;
}
//增加給定節點的接入號碼
bool CParser::AddPreCode(int XmlNo, char *PreCode, int MinLen, int MaxLen)
{
  int nAcc, j;
	int p = 0, prior = 0, next = pAcc_str[0].next;

	if (MinLen >= MAX_TELECODE_LEN)
    MinLen = MAX_TELECODE_LEN - 1;
	if (MaxLen >= MAX_TELECODE_LEN)
    MaxLen = MAX_TELECODE_LEN - 1;
  if (strlen(PreCode) == 0) return false;
	for (nAcc = 1; nAcc < MAX_ACCESSCODES_NUM; nAcc ++)
	{
		if (pAcc->state == 0 && p == 0)
    {
			p = nAcc;
    }
		if (strcmp(pAcc->code, PreCode) == 0)
		{
			//接入碼已經存在
			for(j = 0; j < pAcc->NodeXMLs && j < MAX_NODE_XML; j ++)
			{
				if (pAcc->NodeXML[j] == XmlNo)
					return true;
			}
			if (j < MAX_NODE_XML)
			{
				pAcc->NodeXML[j] = XmlNo;
        pAcc->NodeXMLs ++;
			}
			else
			{
				return false;
			}
			return true;
		}
	}
	nAcc = p;
	if (nAcc < MAX_ACCESSCODES_NUM)
	{
		while (next < MAX_ACCESSCODES_NUM)
		{
			//先按號碼長度從大到小
			prior = pAccNext->prior;
      if (strlen(PreCode) >= strlen((char *)pAccNext->code))
			{
				if (strlen(PreCode) == strlen((char *)pAccNext->code) && strcmp(PreCode,(char *)pAccNext->code)==0) 
					return false;
				pAccNext->prior = nAcc;
        pAccPrior->next = nAcc;

				pAcc->state = 1;
				strncpy((char *)pAcc->code, PreCode, 20);
				pAcc->minlen = MinLen;
				pAcc->maxlen = MaxLen;
		    pAcc->NodeXMLs = 1;
		    pAcc->NodeXML[0] = XmlNo;
		    pAcc->AssignPoint = 0;
				pAcc->prior = prior;
				pAcc->next = next;
				return true;
			}
			prior = next;
			next = pAccPrior->next;
		}
		pAcc->state = 1;
		strncpy((char *)pAcc->code, PreCode, 20);
		pAcc->minlen = MinLen;
		pAcc->maxlen = MaxLen;
		pAcc->NodeXMLs = 1;
		pAcc->NodeXML[0] = XmlNo;
		pAcc->AssignPoint = 0;
		pAccPrior->next = nAcc;
		pAcc->prior = prior;
		pAcc->next = 0xFFFF;
		return true;
	}
	return false;
}
//刪除給定節點的接入號碼
bool CParser::DelPreCode(int XmlNo, char *PreCode, int MinLen, int MaxLen)
{
  int nAcc, i, j = 0;
	int prior = 0, next;

	if (strlen(PreCode) == 0) return false;
	for (nAcc = 1; nAcc < MAX_ACCESSCODES_NUM; nAcc ++)
	{
		if (pAcc->state == 0 ) continue;
    if (strcmp(pAcc->code, PreCode) == 0)
		{
			//接入碼已經存在
			for(j = 0; j < pAcc->NodeXMLs && j < MAX_NODE_XML; j ++)
			{
				if (pAcc->NodeXML[j] == XmlNo)
					break;
			}
			if (j < pAcc->NodeXMLs)
			{
				j ++;
				for (;  j < MAX_NODE_XML; j ++)
				{
					pAcc->NodeXML[j-1] = pAcc->NodeXML[j];
				}
				if (pAcc->NodeXMLs == 1)
        {
          prior = pAcc->prior;  //2008-12-12
          next = pAcc->next;
          pAccPrior->next = next;
          pAccNext->prior = prior;

		      pAcc->state = 0;
		      memset(pAcc->code, 0, 20);
		      pAcc->minlen = 0xFF;
		      pAcc->maxlen = 0xFF;
		      pAcc->NodeXMLs = 0;
		      for (i = 0; i < MAX_NODE_XML; i ++)
            pAcc->NodeXML[i] = 0xFFFF;
		      pAcc->AssignPoint = 0;
		      pAcc->prior = 0xFFFF;
		      pAcc->next = 0xFFFF;
        }
        else if (pAcc->NodeXMLs > 0)
        {
					pAcc->NodeXMLs --;
        }
			}
			return true;
		}
	}
	return false;
}
//解析器退出刪除相關的接入號碼
bool CParser::DelPreCodeForVxmlLogout(int XmlNo)
{
  int nownext, i, j;
	int prior = 0, next = pAcc_str[0].next;

	while (next < MAX_ACCESSCODES_NUM)
	{
    if (pAccNext->state == 0 ) continue;
    nownext = pAccNext->next;
    for(j = 0; j < pAccNext->NodeXMLs && j < MAX_NODE_XML; j ++)
		{
			if (pAccNext->NodeXML[j] == XmlNo)
				break;
		}
		if (j < pAccNext->NodeXMLs)
		{
			if (pAccNext->NodeXMLs == 1)
      {
		    pAccNext->state = 0;
		    memset(pAccNext->code, 0, 20);
		    pAccNext->minlen = 0xFF;
		    pAccNext->maxlen = 0xFF;
		    pAccNext->NodeXMLs = 0;
		    for (i = 0; i < MAX_NODE_XML; i ++)
          pAccNext->NodeXML[i] = 0xFFFF;
		    pAccNext->AssignPoint = 0;
        prior = pAccNext->prior;
		    pAccNext->prior = 0xFFFF;
		    pAccNext->next = 0xFFFF;

        next = nownext;
        pAccPrior->next = nownext;
        if (next  < MAX_ACCESSCODES_NUM)
          pAccNext->prior = prior;
      }
      else if (pAccNext->NodeXMLs > 1)
      {
			  j ++;
			  for (;  j < MAX_NODE_XML; j ++)
			  {
				  pAccNext->NodeXML[j-1] = pAccNext->NodeXML[j];
			  }
				pAccNext->NodeXMLs --;
        prior = next;
      }
		}
    next = nownext;
	}
	return false;
}
//判斷被叫號碼是否收全,并返回最終的號碼
int CParser::CheckCalledACM(char *CalledNo, int &VxmlId)
{
  int AssignPoint, CheckId = 2, calledlen, codelen;
  int i, prior = 0, next = pAcc_str[0].next;

  VxmlId = 0xFF;
  calledlen = strlen(CalledNo);
  if (calledlen == 0)
    return 1;
  if (next >= MAX_ACCESSCODES_NUM) //收碼設置為空時，可接收任意長度號碼
  {
    VxmlId = 1;
    return CheckId;
  }
  while (next < MAX_ACCESSCODES_NUM)
  {
    codelen = strlen(pAcc_str[next].code);

    if (calledlen < codelen) //edit 2008-06-22 <= 改為 <
    {
      if (strncmp(pAcc_str[next].code, CalledNo, calledlen) == 0)
      {
        CheckId = 1;
      }
    }
    else
    {
      if (strncmp(CalledNo, pAcc_str[next].code, codelen) == 0)
      {
        if (CalledNo[calledlen-1] == '#')
        { //號碼已匹配并且收到#
          //CalledNo[calledlen-1] = 0;
          for (i = 0; i < MAX_NODE_XML; i ++)
          {
            AssignPoint = (pAcc_str[next].AssignPoint+i)%MAX_NODE_XML;
            if (pAcc_str[next].NodeXML[AssignPoint] > 0 
              && pAcc_str[next].NodeXML[AssignPoint] < MAX_NODE_XML)
            {
              VxmlId = (UC)pAcc_str[next].NodeXML[AssignPoint];
              break;
            }
          }
          AssignPoint ++;
          pAcc_str[next].AssignPoint = AssignPoint % MAX_NODE_XML;
          if (VxmlId > MAX_NODE_XML)
            VxmlId = 1;
          return 0;
        }
        
        CheckId = 3;
        VxmlId = 1;
        for (i = 0; i < MAX_NODE_XML; i ++)
        {
          AssignPoint = (pAcc_str[next].AssignPoint+i)%MAX_NODE_XML;
          if (pAcc_str[next].NodeXML[AssignPoint] > 0 
            && pAcc_str[next].NodeXML[AssignPoint] < MAX_NODE_XML)
          {
            VxmlId = (UC)pAcc_str[next].NodeXML[AssignPoint];
            break;
          }
        }
      }
    }
    if (strncmp(CalledNo, pAcc_str[next].code, codelen) == 0 
      && calledlen >= pAcc_str[next].minlen)
    {
      CalledNo[pAcc_str[next].maxlen] = 0;
      for (i = 0; i < MAX_NODE_XML; i ++)
      {
        AssignPoint = (pAcc_str[next].AssignPoint+i)%MAX_NODE_XML;
        if (pAcc_str[next].NodeXML[AssignPoint] > 0 
          && pAcc_str[next].NodeXML[AssignPoint] < MAX_NODE_XML)
        {
          VxmlId = (UC)pAcc_str[next].NodeXML[AssignPoint];
          break;
        }
      }
      AssignPoint ++;
      pAcc_str[next].AssignPoint = AssignPoint % MAX_NODE_XML;
      if (VxmlId > MAX_NODE_XML)
        VxmlId = 1;
      return 0;
    }
    prior = next;
    next = pAcc_str[prior].next;
  }
  return CheckId;
}
//判斷內線坐席摘機后是否直接進入流程，流程接入碼設置為：A,1,1,*
int CParser::CheckSeatDirectIn(int &VxmlId)
{
  int prior = 0, next = pAcc_str[0].next;
  while (next < MAX_ACCESSCODES_NUM)
  {
    if (stricmp(pAcc_str[next].code, "A") == 0)
    {
      VxmlId = (UC)pAcc_str[next].NodeXML[0];
      if (VxmlId < MAX_NODE_XML)
        return 1;
    }
    prior = next;
    next = pAcc_str[prior].next;
  }
  return 0;
}
