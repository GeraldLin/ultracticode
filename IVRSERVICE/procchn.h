//---------------------------------------------------------------------------
#ifndef ProcchnH
#define ProcchnH
//---------------------------------------------------------------------------
void DispChnStatus(int nChn);

//計時器
void TimerAdd();
//通道事物處理
void ProcessChn();

void SumIVRChnOnlines();

//處理通道呼叫事物
void ProcessChnCall(int nChn);
//處理通道放音事務
void ProcessChnPlay(int nChn);
//處理通道收碼事務
void ProcessChnDTMF(int nChn);
//處理通道錄音事務
void ProcessChnRec(int nChn);

//會議事務處理
void ProcessConf();
//處理會議放音事務
void SendConfPlayResult(int nCfc, int result);
void ProcessConfPlay();
//處理會議錄音事務
void ProcessConfRec();

//產生呼出序列號
UL GetCallOutSerialNo(int nChn);
//處理呼出
void ProcCalloutQue();
//設置呼出隊列數據
int SetCalloutQue(int nOut, int calledpoint);
//發送呼出結果
void SendCallResult(int nOut, int calledpoint, int result, const char *error);
//處理呼出緩沖
void ProcCalloutBuf();
//解析器退出時，清除相關通道狀態
void ResetAllChnForParserLogout(UC NodeId);
//
void ResetAllChnForRECLogout();

//中繼實時話務量統計
void TrkCallMeteCount();
//發送即時統計數據
void SendNowTrkCallMeteCount();
//發送即時路由空閑中繼數據
void SendRouteIdelTrkNum(bool bFirstId=false);

//發送詳細話單號
void SendCdrSerialNo(int nChn);
void SendCdrSerialNo(int nChn, const char *CdrSerialNo);
void SendCdrSerialNo(int nChn, int nChn1);
void SendCdrSerialNo(const char *SerialNo, int nChnType, int nChnNo, const char *CdrSerialNo);
//插入初始詳細通話記錄
void DBInsertCallCDR(int nChn, int nAnsId, int nCallType, const char *TranWorkerNo="");
void DBUpdateCallCDR_AnsId(int nChn, int nAnsId);
//更新主被叫號碼
void DBUpdateCallCDR_Phone(int nChn);
//更新呼叫方向(1-呼入 2-呼出 3-內部)
void DBUpdateCallCDR_CallInOut(int nChn, int nInOut);
//更新呼入系統應答時間
void DBUpdateCallCDR_AnsTime(int nChn);
//更新等待分配坐席時間
void DBUpdateCallCDR_WaitTime(int nChn);
//更新分配坐席時間
void DBUpdateCallCDR_ACDTime(int nChn, int nSrvType, int nSrvSubType);
void DBUpdateCallCDR_SrvType(int nChn, int nSrvType, int nSrvSubType);
//添加分配過的工號
void DBUpdateCallCDR_AnsWorkerNo(int nChn, int nAG);
void DBUpdateCallCDR_WorkerNo(int nChn, int nAG);
void DBUpdateCallCDR_WorkerNoGroupNo(int nChn, int nAG);
void DBUpdateCallCDR_WorkerNo(int nChn, const char *SeatNo, const char *WorkerNo);
void DBUpdateCallCDR_WorkerNo(int nChn, const char *SeatNo, const char *WorkerNo, int nSeatAnsId);
//更新坐席振鈴時間
void DBUpdateCallCDR_RingTime(int nChn);
//更新坐席應答時間
void DBUpdateCallCDR_SeatAnsTime(int nChn);
void DBUpdateCallCDR_SeatAnsTime(int nChn, const char *SeatNo, const char *WorkerNo);
void DBUpdateCallCDR_SeatAnsTime(int nAG, int AcdGroupNo);
void DBUpdateCallCDR_SeatAnsId(int nChn, int seatansid);
//更新錄音文件名
void DBUpdateCallCDR_RecdFile(int nAG);
void DBUpdateCallCDR_RecdFile(int nAG, const char *rootpath, const char *filename, const char *callid);
void DBUpdateCallCDR_RecdFile(const char *serialno, const char *rootpath, const char *filename, const char *callid);
void DBUpdateCallCDR_ChnRecdFile(int nChn);
//更新坐席掛機時間
void DBUpdateCallCDR_SeatRelTime(int nAG);
void DBUpdateCallCDR_ChnSeatRelTime(int nChn);
//更新外線掛機時間時間
void DBUpdateCallCDR_RelTime(int nChn);
void DBUpdateCallCDR_RelTimeByRecCh(int nChn);
void DBUpdateCallCDR_RelReason(int nChn, int nRelReason);
void DBUpdateCallCDR_UpdateFlag(int nChn); //修改話單完成更新標志
//更新坐席話后處理完成時間
void DBUpdateCallCDR_ACWTime(int nAG);
void DBUpdateCallCDR_DialOutID(int nChn, const char *dialid);
void DBUpdateCallCDR_CRMCallID(int nChn, const char *callid);

void DBInsertIVRDTMFTrace(int nChn, int flag, int nAG);

void DBInsertCallTrace(int nChn, const char *status);
//---------------------------------------------------------------------------
//修改人工服務呼出結果
bool GetDialOutId(const char *dialparam, int nChn);
void InsertDialOutListRecord(const char *diallistid, const char *calledno, const char *workerno);
void UpdateDialOutTime(const char *dialid, const char *diallistid);
void UpdateDialOutSerialNo(const char *dialid, const char *diallistid, const char *serialno);
void UpdateDialOutRingTime(const char *dialid, const char *diallistid);
void UpdateDialOutFailTime(const char *dialid, const char *diallistid, int dialreault=85);
void UpdateDialOutAnsTime(const char *dialid, const char *diallistid);
void UpdateDialOutHangonTime(const char *dialid, const char *diallistid);
bool GetCRMCallId(const char *dialparam, int nChn);

//---------------------------------------------------------------------------
void SendIVRCallCountData(CIVRCallCountParam *pIVRCallCountParam);
void SendAllIVRCallCountData();

#endif
