//---------------------------------------------------------------------------
#include "stdafx.h"
#include "mixervoc.h"
//---------------------------------------------------------------------------
#include "extern.h"

//---------------------------------------------------------------------------
//讀內存索引放音文件對照表
void Load_MemPlayIndex(const char *filename, const char *vocpath)
{
	UI IndexNo;
	FILE *fp1;
  CStringX ArrString[3];
  CStringX FileName;
  //CH FileName[256];
  CH txtline[128];
	
  fp1 = fopen(filename, "r");
  if (fp1 != NULL)
  {
    while (!feof(fp1))
    {
      if (fgets(txtline, 128, fp1) > 0)
      {
        if (SplitTxtLine(txtline, 3, ArrString) >= 3)
        {
          IndexNo = atoi(ArrString[0].C_Str());
          FileName.Format("%s/%s", vocpath, ArrString[2].C_Str());
          if (g_VOCInLinux != 0 || MyCheckFileExist(FileName.C_Str()) == 1) //2013-09-30
            ADDPLAYINDEX(IndexNo, FileName.C_Str(), "");
        }
      }
    }
    fclose(fp1);
  }
  else
  {
  	printf("Read MemIndex File = %s fail\n", filename);
  }
}
//-----------------------語音合成函數----------------------------------------
//增加內存放音索引
int AddMemIndexToPlayBuf(int nChn, int MemIndex, char *ErrMsg)
{
	ErrMsg[0] = 0;
  int DataBufNum = pChn->curPlayData.DataBufNum;
	if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		strcpy(ErrMsg, "play buffer is overflow");
    return 1;
	}
  pChn->curPlayData.PlayIndex[DataBufNum].PlayType = PLAYTYPE_index;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayFile.Empty();
	pChn->curPlayData.PlayIndex[DataBufNum].PlayIndex = MemIndex;
	pChn->curPlayData.DataBufNum ++;
  return 0;
}

//-----------------------合成普通話語音函數----------------------------------------
//規范日期時間字符串
int FormatDateTimeStr(const CH *pszInDTStr, CH *pszOutDTStr, int &nMGYear)
{
  int len, i, j, t, yyyy, mm, dd, hh, mi, ss, w;
  char c, szTemp[22];

  nMGYear = 0;
  pszOutDTStr[0] = '\0';
  len = strlen(pszInDTStr);
  if (len == 1)
  {
    if (pszInDTStr[0] >= '0' && pszInDTStr[0] <= '6')
    {
      sprintf(pszOutDTStr, "YYYY-MM-DD_HH:MI:SS_%c", pszInDTStr[0]);
      return 0;
    }
    else
    {
      return 1;
    }
  }
  else if (len == 0 || len > 21)
  {
    return 1;
  }

  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

  j = 0;
  t = 0;
  memset(szTemp, 0, 22);
  for (i=0; i<=len; i++)
  {
    c = pszInDTStr[i];

    switch (t)
    {
    case 0: //年與月之間的分隔符 / - .
      if (c == '\0' || c == '/' || c == '-' || c == '.')
      {
        if (j >= 1 && j <= 4)
        {
          yyyy = atoi(szTemp);
          if (yyyy >= 1 && yyyy <= 12)
          {
            //直接是月開始
            strcat(pszOutDTStr, "XXXX-");
            sprintf(szTemp, "%02d-", yyyy);
            strcat(pszOutDTStr, szTemp);
            mm = yyyy;
            yyyy = local->tm_year+1900;
            t++;
          }
          else if (yyyy > 0 && yyyy < 999)
          {
            //民國年
            yyyy = yyyy+1911;
            sprintf(szTemp, "%04d-", yyyy);
            strcat(pszOutDTStr, szTemp);
            nMGYear = 1;
          }
          else if (yyyy > 0 && yyyy < 9999)
          {
            sprintf(szTemp, "%04d-", yyyy);
            strcat(pszOutDTStr, szTemp);
          }
          else
          {
            yyyy = local->tm_year+1900;
            strcat(pszOutDTStr, "XXXX-");
          }
        }
        else
        {
          yyyy = local->tm_year+1900;
          strcat(pszOutDTStr, "XXXX-");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else if (c == '_' || c == ' ')
      {
        //直接是日開始
        dd = atoi(szTemp);
        if (dd >= 1 && dd <= 31)
        {
          sprintf(szTemp, "XXXX-XX-%02d_", dd);
          strcat(pszOutDTStr, szTemp);
        }
        else
        {
          strcat(pszOutDTStr, "XXXX-XX-XX_");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t = 3;
      }
      else if (c == ':')
      {
        //直接是時開始
        if (Check_Int_Str(szTemp, hh) == 0)
        {
          if (hh >= 0 && hh <= 23)
          {
            sprintf(szTemp, "XXXX-XX-XX_%02d:", hh);
            strcat(pszOutDTStr, szTemp);
          }
          else
          {
            strcat(pszOutDTStr, "XXXX-XX-XX_XX:");
          }
        }
        else
        {
          strcat(pszOutDTStr, "XXXX-XX-XX_XX:");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t = 4;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 1: //月與日之間的分隔 / - .
      if (c == '\0' || c == '/' || c == '-' || c == '.')
      {
        if (j == 1 || j == 2)
        {
          mm = atoi(szTemp);
          if (mm >= 1 && mm <= 12)
          {
            sprintf(szTemp, "%02d-", mm);
            strcat(pszOutDTStr, szTemp);
          }
          else
          {
            mm = local->tm_mon+1;
            strcat(pszOutDTStr, "XX-");
          }
        }
        else
        {
          mm = local->tm_mon+1;
          strcat(pszOutDTStr, "XX-");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 2: //日與時之間的分隔 _ 空格
      if (c == '\0' || c == '_' || c == ' ')
      {
        if (j == 1 || j == 2)
        {
          dd = atoi(szTemp);
          if (mm == 1 || mm == 3 || mm == 5 || mm == 7 || mm == 8 || mm == 10 || mm == 12)
          {
            //大月
            if (dd >= 1 && dd <= 31)
            {
              sprintf(szTemp, "%02d_", dd);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX_");
            }
          }
          else if (mm == 4 || mm == 6 || mm == 9 || mm == 11)
          {
            //小月
            if (dd >= 1 && dd <= 30)
            {
              sprintf(szTemp, "%02d_", dd);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX_");
            }
          }
          else if (mm == 2)
          {
            //二月
            if (IsLeapYear(yyyy) == true)
            {
              //閏年
              if (dd >= 1 && dd <= 29)
              {
                sprintf(szTemp, "%02d_", dd);
                strcat(pszOutDTStr, szTemp);
              }
              else
              {
                strcat(pszOutDTStr, "XX_");
              }
            }
            else
            {
              //平年
              if (dd >= 1 && dd <= 28)
              {
                sprintf(szTemp, "%02d_", dd);
                strcat(pszOutDTStr, szTemp);
              }
              else
              {
                strcat(pszOutDTStr, "XX_");
              }
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX_");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 3: //時與分之間的分隔 ：
      if (c == '\0' || c == ':')
      {
        if (j == 1 || j == 2)
        {
          if (Check_Int_Str(szTemp, hh) == 0)
          {
            if (hh >= 0 && hh <= 23)
            {
              sprintf(szTemp, "%02d:", hh);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX:");
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX:");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 4: //分與秒之間的分隔 ：
      if (c == '\0' || c == ':')
      {
        if (j == 1 || j == 2)
        {
          if (Check_Int_Str(szTemp, mi) == 0)
          {
            if (mi >= 0 && mi <= 59)
            {
              sprintf(szTemp, "%02d:", mi);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX:");
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX:");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 5: //秒與周之間的分隔 _ 空格
      if (c == '\0' || c == '_')
      {
        if (j == 1 || j == 2)
        {
          if (Check_Int_Str(szTemp, ss) == 0)
          {
            if (ss >= 0 && ss <= 59)
            {
              sprintf(szTemp, "%02d_", ss);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX_");
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX_");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 6: //星期
      if (c == '\0')
      {
        if (j == 1)
        {
          if (Check_Int_Str(szTemp, w) == 0)
          {
            if (w >= 0 && w <= 6)
            {
              sprintf(szTemp, "%d", w);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "X");
            }
          }
          else
          {
            strcat(pszOutDTStr, "X");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    }
  }
  switch (t)
  {
  case 1:
    strcat(pszOutDTStr, "MM-DD_HH:MI:SS_W");
    break;
  case 2:
    strcat(pszOutDTStr, "DD_HH:MI:SS_W");
    break;
  case 3:
    strcat(pszOutDTStr, "HH:MI:SS_W");
    break;
  case 4:
    strcat(pszOutDTStr, "MI:SS_W");
    break;
  case 5:
    strcat(pszOutDTStr, "SS_W");
    break;
  case 6:
    strcat(pszOutDTStr, "W");
    break;
  }
  return 0;
}
//將日期時間合成普通話語音
int MixDateTime2CHN(const CH *pszTime, int nChn, CH *pszErrMsg, int nMGYear, int nAMPM)
{
	//TimeFormat有效格式：YYYY-MM-DD_HH:MI:SS_W 不需要合成的用X代替
	int i, len, iTemp, nFormat, nYear;
	CH szTemp[20];

	len = strlen(pszTime);
	if (len != 21)
	{
		sprintf(pszErrMsg, "%s datetime string is error", pszTime);
		return 1;
	}
	
  nFormat = 0;
	//分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
		if (nMGYear == 0)
    {
      for (i = 0; i < 4; i ++)
      {
        iTemp = szTemp[i] - '0';
        iTemp = 0 + iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
    }
    else
    {
      nYear = atoi(szTemp);
      if (nYear > 1911)
      {
        nYear = nYear - 1911;
      }
      AddMemIndexToPlayBuf(nChn, 240, pszErrMsg); //民國
      MixNumber2CHN(nYear, szTemp, nChn, pszErrMsg);
    }
		iTemp = 32; //年
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	else
	{
		if (strcmpi(szTemp, "YYYY") != 0 && strcmpi(szTemp, "XXXX"))
		{
			sprintf(pszErrMsg, "%s year is error", pszTime);
			//return 1;
		}
	}
	//分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 1 && iTemp <= 12)
		{
			iTemp = 39 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s month is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "MM") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s month is error", pszTime);
			//return 1;
		}
	}
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 1 && iTemp <= 31)
		{
			iTemp = 51 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s day is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "DD") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s day is error", pszTime);
			//return 1;
		}
	}
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 23)
		{
      if (nAMPM == 0)
      {
        //按24小時制播報
			  iTemp = 83 + iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else
      {
        //按凌晨(0點-6點)，早上(6點-8點)，上午(8點-12點)，中午(12點-13點)，下午(13點-19點)，晚上(19點-23點)
        if (iTemp >= 0 && iTemp < 6)
        {
          AddMemIndexToPlayBuf(nChn, 241, pszErrMsg); //凌晨
          iTemp = 83 + iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (iTemp >= 6 && iTemp < 8)
        {
          AddMemIndexToPlayBuf(nChn, 242, pszErrMsg); //早上
          iTemp = 83 + iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (iTemp >= 8 && iTemp < 12)
        {
          AddMemIndexToPlayBuf(nChn, 243, pszErrMsg); //上午
          iTemp = 83 + iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (iTemp >= 12 && iTemp < 13)
        {
          AddMemIndexToPlayBuf(nChn, 244, pszErrMsg); //中午
          iTemp = 83 + iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (iTemp >= 13 && iTemp < 19)
        {
          AddMemIndexToPlayBuf(nChn, 245, pszErrMsg); //下午
          iTemp = 83 + iTemp - 12;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (iTemp >= 19 && iTemp < 23)
        {
          AddMemIndexToPlayBuf(nChn, 246, pszErrMsg); //晚上
          iTemp = 83 + iTemp - 12;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else
        {
          return 1;
        }
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s hour is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "HH") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s hour is error", pszTime);
			//return 1;
		}
	}
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 59)
		{
			iTemp = 107 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s minute is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "MI") != 0 && strcmpi(szTemp, "XX") != 0 && strcmpi(szTemp, "MM") != 0)
		{
			sprintf(pszErrMsg, "%s minute is error", pszTime);
			//return 1;
		}
	}
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 59)
		{
			iTemp = 167 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s second is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "SS") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s second is error", pszTime);
			//return 1;
		}
	}
	//分析星期
	memset(szTemp, 0, 20);
	szTemp[0] = pszTime[20];
	if (szTemp[0] >= '0' && szTemp[0] <= '6')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 6)
		{
			iTemp = 227 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s weekday is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "W") != 0 && strcmpi(szTemp, "X") != 0)
		{
			sprintf(pszErrMsg, "%s weekday is error", pszTime);
			//return 1;
		}
	}
	return 0;
}
const US MoneyIndex[] = 
	{ 26,12,13,14,15,12,13,14,19,
	  12,13,14,15,12,13,14,19,
	  12,13,14,15,12,13,14 };
//將金額合成普通話
int MixMoney2CHN(DF fMoney, int nChn, CH *pszErrMsg)
{
	double fraction, integer;
	long lMoney;
	int iTemp, len, jiao, fen, nFormat;
	int n, lastzero=0;
	int hasvalue=0;			// 億、萬進位前有數值標記
	char szMoney[30];

  if (fMoney == 0)
  {
    if (AddMemIndexToPlayBuf(nChn, 23, pszErrMsg) != 0)
    {
      return 1;
    }
    if (AddMemIndexToPlayBuf(nChn, 26, pszErrMsg) != 0)
    {
      return 1;
    }
    return 0;
  }
	nFormat = 0;
	//判斷正負
	if(fMoney<0) 
	{
		fMoney = -fMoney;
		iTemp = 25; //負
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	fraction = modf(fMoney+0.005, &integer); // 四舍五入到分
	
	//處理整數
	lMoney = (long)integer;
	if (lMoney > 0)
	{
		//ltoa(lMoney, szMoney, 10);
		sprintf(szMoney, "%ld", lMoney);
		len = strlen(szMoney);
		if (len > 24)
		{
			sprintf(pszErrMsg, "money is too big");
			return 1;
		}
		for(int i = len; i > 0; i--)
		{
			n = szMoney[len-i] - '0';

			if( n!=0 ) 
			{
				if( lastzero ) 
				{
					//RMBStr += HanDigiStr[0]; // 若干零后若跟非零值，只顯示一個零
											// 除了億萬前的零不帶到后面
					iTemp = 23; //零
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
				}
				//if( !( n==1 && (i%4)==1 && (lastzero || i==len-1) ) )// 如十進位前有零也不發壹音用此行
				//if( !( n==1 && (i%4)==1 && i==len-1 ) ) // 十進位處于第一位不發壹音
					//RMBStr += HanDigiStr[n];
					iTemp = 0 + n;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }

				//RMBStr += HanDiviStr[i];			// 非零值后加進位，個位為空
					iTemp = MoneyIndex[i-1];
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
				
				hasvalue = 1;                                   // 置萬進位前有值標記
			}
			else 
			{
				if( ((i-1)%8)==0 || (((i-1)%8)==4 && hasvalue) ) 	// 億萬之間必須有非零值方顯示萬
				{
					//RMBStr += HanDiviStr[i];		// “億”或“萬”
					iTemp = MoneyIndex[i-1];
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
				}
			}
			if( (i-1)%8==0 ) hasvalue = 0;  			// 萬進位前有值標記逢億復位
			lastzero = (n==0) && ((i-1)%4!=0);                  // 億萬前有零后不加零，如：拾萬貳仟
		}
	}
    //else
    //{
	//	nPlayIndex[nIndexNum] = 0;
	//	nIndexNum ++;
	//	strcat(pszPlayStr, "CDG0,");
    //}
	
	//處理小數位
	jiao = int(fraction*10);
	fen = int(fraction*100)-jiao*10;
	if( jiao == 0 && fen == 0 && lMoney != 0) 
	{
		iTemp = 27; //整
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	else 
	{
		//角
		if(jiao > 0)
		{
			iTemp = 0 + jiao;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }

			iTemp = 28; //角
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		//分
		iTemp = 0 + fen;
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
		iTemp = 29; //分
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	return 0;
}
//索引
const US NumberIndex[] = 
	{ 24,12,13,14,15,12,13,14,19,
	  12,13,14,15,12,13,14,19,
	  12,13,14,15,12,13,14 };
//將數值合成普通話
int MixNumber2CHN(DF fNumber, const CH *pszNumber, int nChn, CH *pszErrMsg)
{
	double fraction;
	long lNumber;
	int i, len, iTemp, nFormat; //dec, sign, 
	int n, lastzero=0;
	int hasvalue=0;			// 億、萬進位前有數值標記
	char cTemp, szNumber[30];
  char *szFration;

	nFormat = 0;
	//判斷正負
	if(fNumber<0) 
	{
		fNumber = -fNumber;
		iTemp = 25; //負
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	
	//處理整數
	lNumber = (long)fNumber;
	if (lNumber > 0)
	{
		//ltoa(lNumber, szNumber, 10);
		sprintf(szNumber, "%ld", lNumber);
		len = strlen(szNumber);
		if (len > 24)
		{
			sprintf(pszErrMsg, "value is too big");
			return 1;
		}
		for(int i = len; i > 0; i--)
		{
			n = szNumber[len-i] - '0';

			if( n!=0 ) 
			{
				if( lastzero ) 
				{
					//RMBStr += HanDigiStr[0]; // 若干零后若跟非零值，只顯示一個零
											// 除了億萬前的零不帶到后面
					iTemp = 0; //數字0
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
				}
				//if( !( n==1 && (i%4)==1 && (lastzero || i==len-1) ) )// 如十進位前有零也不發壹音用此行
				//if( !( n==1 && (i%4)==1 && i==len-1 ) ) // 十進位處于第一位不發壹音
					//RMBStr += HanDigiStr[n];
					iTemp = 0 + n;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }

				//RMBStr += HanDiviStr[i];			// 非零值后加進位，個位為空
        if (i > 1)
        {
					iTemp = NumberIndex[i-1];
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
				
				hasvalue = 1;                                   // 置萬進位前有值標記
			}
			else 
			{
				if( ((i-1)%8)==0 || (((i-1)%8)==4 && hasvalue) ) 	// 億萬之間必須有非零值方顯示萬
				{
					//RMBStr += HanDiviStr[i];		// “億”或“萬”
					if (i > 1)
          {
            iTemp = NumberIndex[i-1];
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
          }
				}
			}
			if( (i-1)%8==0 ) hasvalue = 0;  			// 萬進位前有值標記逢億復位
			lastzero = (n==0) && ((i-1)%4!=0);                  // 億萬前有零后不加零，如：拾萬貳仟
		}
	}
    else
    {
		  iTemp = 0; 
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
    }

	//處理小數位
	fraction = fNumber - lNumber;
	if (fraction > 0)
	{
		iTemp = 24; //點
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }

    szFration = strchr(pszNumber, '.'); //ecvt(fraction, 8, &dec, &sign);
    if (szFration == NULL)
      return 0;
    szFration++;
    MyTrace(3, "MixNumber2CHN fNumber=%f fraction=%f szFration=%s", fNumber, fraction, szFration);
    
    len = strlen(szFration);
    for (i = len-1; i >= 0; i --)
    {
      if (szFration[i] == '0')
      {
        szFration[i] = '\0';
      }
      else
      {
        break;
      }
    }
    len = strlen(szFration);
    for (i = 0; i < len; i ++)
    {
      cTemp = szFration[i];
		  if (cTemp >= '0' && cTemp <= '9')
		  {
			  iTemp = cTemp - '0';
			  iTemp = 0 + iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		  }
    }
	}
	
	return 0;
}
//將數字串合成普通話
int MixDigits2CHN(const CH *pszDigits, int nChn, CH *pszErrMsg)
{
	int i, len, iTemp, nFormat;
	CH cTemp;

	nFormat = 0;
	len = strlen(pszDigits);
	for (i = 0; i < len; i ++)
	{
		cTemp = pszDigits[i];
		if (cTemp >= '0' && cTemp <= '9')
		{
			if (cTemp == '1' && pIVRCfg->isDigitOneSayYao == true)
			{
				iTemp = 10;
			}
			else
			{
				iTemp = cTemp - '0';
			}
			iTemp = 0 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
	}
	return 0;
}

//-----------------------合成普通話語音函數----------------------------------------
//將日期時間合成廣東話
int MixDateTime2GDH(const CH *pszTime, int nChn, CH *pszErrMsg)
{
	//TimeFormat有效格式：YYYY-MM-DD_HH:MI:SS_W 不需要合成的用X代替
	int i, len, iTemp, nFormat;
	CH szTemp[20];

	len = strlen(pszTime);
	if (len != 21)
	{
		sprintf(pszErrMsg, "%s datetime string is error", pszTime);
		return 1;
	}
	
  nFormat = 0;
	//分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
		for (i = 0; i < 4; i ++)
		{
			iTemp = szTemp[i] - '0';
			iTemp = 0 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		iTemp = 32; //年
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	else
	{
		if (strcmpi(szTemp, "YYYY") != 0 && strcmpi(szTemp, "XXXX"))
		{
			sprintf(pszErrMsg, "%s year is error", pszTime);
			//return 1;
		}
	}
	//分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 1 && iTemp <= 12)
		{
			iTemp = 39 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s month is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "MM") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s month is error", pszTime);
			//return 1;
		}
	}
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 1 && iTemp <= 31)
		{
			iTemp = 51 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s day is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "DD") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s day is error", pszTime);
			//return 1;
		}
	}
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 23)
		{
			iTemp = 83 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s hour is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "HH") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s hour is error", pszTime);
			//return 1;
		}
	}
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 59)
		{
			iTemp = 107 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s minute is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "MI") != 0 && strcmpi(szTemp, "XX") != 0 && strcmpi(szTemp, "MM") != 0)
		{
			sprintf(pszErrMsg, "%s minute is error", pszTime);
			//return 1;
		}
	}
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 59)
		{
			iTemp = 167 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s second is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "SS") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s second is error", pszTime);
			//return 1;
		}
	}
	//分析星期
	memset(szTemp, 0, 20);
	szTemp[0] = pszTime[20];
	if (szTemp[0] >= '0' && szTemp[0] <= '6')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 6)
		{
			iTemp = 227 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s weekday is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "W") != 0 && strcmpi(szTemp, "X") != 0)
		{
			sprintf(pszErrMsg, "%s weekday is error", pszTime);
			//return 1;
		}
	}
	return 0;
}
//將金額合成廣東話
int MixMoney2GDH(DF fMoney, int nChn, CH *pszErrMsg)
{
	double fraction, integer;
	long lMoney;
	int iTemp, len, jiao, fen, nFormat;
	int n, lastzero=0;
	int hasvalue=0;			// 億、萬進位前有數值標記
	char szMoney[30];

	nFormat = 0;
	//判斷正負
	if(fMoney<0) 
	{
		fMoney = -fMoney;
		iTemp = 25; //負
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	fraction = modf(fMoney+0.005, &integer); // 四舍五入到分
	
	//處理整數
	lMoney = (long)integer;
	if (lMoney > 0)
	{
		//ltoa(lMoney, szMoney, 10);
		sprintf(szMoney, "%ld", lMoney);
		len = strlen(szMoney);
		if (len > 24)
		{
			sprintf(pszErrMsg, "money is too big");
			return 1;
		}
		for(int i = len; i > 0; i--)
		{
			n = szMoney[len-i] - '0';

			if( n!=0 ) 
			{
				if( lastzero ) 
				{
					//RMBStr += HanDigiStr[0]; // 若干零后若跟非零值，只顯示一個零
											// 除了億萬前的零不帶到后面
					iTemp = 23; //零
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }
				}
				//if( !( n==1 && (i%4)==1 && (lastzero || i==len-1) ) )// 如十進位前有零也不發壹音用此行
				//if( !( n==1 && (i%4)==1 && i==len-1 ) ) // 十進位處于第一位不發壹音
					//RMBStr += HanDigiStr[n];
					iTemp = 0 + n;
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }

				//RMBStr += HanDiviStr[i];			// 非零值后加進位，個位為空
					iTemp = MoneyIndex[i-1];
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }
				
				hasvalue = 1;                                   // 置萬進位前有值標記
			}
			else 
			{
				if( ((i-1)%8)==0 || (((i-1)%8)==4 && hasvalue) ) 	// 億萬之間必須有非零值方顯示萬
				{
					//RMBStr += HanDiviStr[i];		// “億”或“萬”
					iTemp = MoneyIndex[i-1];
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }
				}
			}
			if( (i-1)%8==0 ) hasvalue = 0;  			// 萬進位前有值標記逢億復位
			lastzero = (n==0) && ((i-1)%4!=0);                  // 億萬前有零后不加零，如：拾萬貳仟
		}
	}
    //else
    //{
	//	nPlayIndex[nIndexNum] = 0;
	//	nIndexNum ++;
	//	strcat(pszPlayStr, "CDG0,");
    //}
	
	//處理小數位
	jiao = int(fraction*10);
	fen = int(fraction*100)-jiao*10;
	if( jiao == 0 && fen == 0 && lMoney != 0) 
	{
		iTemp = 27; //整
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	else 
	{
		//角
		if(jiao > 0)
		{
			iTemp = 0 + jiao;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }

			iTemp = 28; //角
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		//分
		iTemp = 0 + fen;
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }
		iTemp = 29; //分
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	return 0;
}
//將數值合成廣東話
int MixNumber2GDH(DF fNumber, int nChn, CH *pszErrMsg)
{
	double fraction;
	long lNumber;
	int i, len, dec, sign, iTemp, nFormat;
	int n, lastzero=0;
	int hasvalue=0;			// 億、萬進位前有數值標記
	char cTemp, szNumber[30];
    char *szFration;

	nFormat = 0;
	//判斷正負
	if(fNumber<0) 
	{
		fNumber = -fNumber;
		iTemp = 25; //負
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	
	//處理整數
	lNumber = (long)fNumber;
	if (lNumber > 0)
	{
		//ltoa(lNumber, szNumber, 10);
		sprintf(szNumber, "%ld", lNumber);
		len = strlen(szNumber);
		if (len > 24)
		{
			sprintf(pszErrMsg, "value is too big");
			return 1;
		}
		for(int i = len; i > 0; i--)
		{
			n = szNumber[len-i] - '0';

			if( n!=0 ) 
			{
				if( lastzero ) 
				{
					//RMBStr += HanDigiStr[0]; // 若干零后若跟非零值，只顯示一個零
											// 除了億萬前的零不帶到后面
					iTemp = 0; //數字0
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }
				}
				//if( !( n==1 && (i%4)==1 && (lastzero || i==len-1) ) )// 如十進位前有零也不發壹音用此行
				//if( !( n==1 && (i%4)==1 && i==len-1 ) ) // 十進位處于第一位不發壹音
					//RMBStr += HanDigiStr[n];
					iTemp = 0 + n;
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }

				//RMBStr += HanDiviStr[i];			// 非零值后加進位，個位為空
        if (i > 1)
        {
					iTemp = NumberIndex[i-1];
          if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
          {
            return 1;
          }
        }
				
				hasvalue = 1;                                   // 置萬進位前有值標記
			}
			else 
			{
				if( ((i-1)%8)==0 || (((i-1)%8)==4 && hasvalue) ) 	// 億萬之間必須有非零值方顯示萬
				{
					//RMBStr += HanDiviStr[i];		// “億”或“萬”
					if (i > 1)
          {
            iTemp = NumberIndex[i-1];
            if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
            {
              return 1;
            }
          }
				}
			}
			if( (i-1)%8==0 ) hasvalue = 0;  			// 萬進位前有值標記逢億復位
			lastzero = (n==0) && ((i-1)%4!=0);                  // 億萬前有零后不加零，如：拾萬貳仟
		}
	}
    else
    {
		  iTemp = 0; 
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
    }

	//處理小數位
	fraction = fNumber - lNumber;
	if (fraction > 0)
	{
		iTemp = 24; //點
    if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
    {
      return 1;
    }

        szFration = fcvt(fraction, 8, &dec, &sign);
        len = strlen(szFration);
        for (i = len-1; i >= 0; i --)
        {
            if (szFration[i] == '0')
            {
                szFration[i] = '\0';
            }
            else
            {
                break;
            }
        }
        len = strlen(szFration);
        for (i = 0; i < len; i ++)
        {
            cTemp = szFration[i];
		    if (cTemp >= '0' && cTemp <= '9')
		    {
				iTemp = cTemp - '0';
				iTemp = 0 + iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
        {
          return 1;
        }
		    }
        }
	}
	
	return 0;
}
//將數字串合成廣東話
int MixDigits2GDH(const CH *pszDigits, int nChn, CH *pszErrMsg)
{
	int i, len, iTemp, nFormat;
	CH cTemp;

	nFormat = 0;
	len = strlen(pszDigits);
	for (i = 0; i < len; i ++)
	{
		cTemp = pszDigits[i];
		if (cTemp >= '0' && cTemp <= '9')
		{
			if (cTemp == '1')
			{
				iTemp = 10;
			}
			else
			{
				iTemp = cTemp - '0';
			}
			iTemp = 0 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp+500, pszErrMsg) != 0)
      {
        return 1;
      }
		}
	}
	return 0;
}

//--------------------合成英文-------------------------------------------------
//將日期時間合成英文
int MixDateTime2ENG(const CH *pszTime, int nChn, CH *pszErrMsg)
{
	//TimeFormat有效格式：YYYY-MM-DD_HH:MI:SS_W 不需要合成的用X代替
	int len, iTemp, nFormat;
	CH szTemp[20];

	len = strlen(pszTime);
	if (len != 21)
	{
		sprintf(pszErrMsg, "%s datetime string is error", pszTime);
		return 1;
	}
	
  nFormat = 0;
	//分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 1 && iTemp <= 12)
		{
			iTemp = 419 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s month is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "MM") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s month is error", pszTime);
			//return 1;
		}
	}
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 1 && iTemp <= 31)
		{
      if (szTemp[0]=='0' || szTemp[0]=='1')
      {
		    //幾日或十幾日
        iTemp = (szTemp[1]-'0')+(szTemp[0]-'0')*10;
        iTemp = 358+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else if (szTemp[0]=='2')
      {
        //20幾日
        if (szTemp[1]=='0')
        {
          //20
          if (AddMemIndexToPlayBuf(nChn, 378, pszErrMsg) != 0)
          {
            return 1;
          }
        }
		    else
        {
          //20幾
          if (AddMemIndexToPlayBuf(nChn, 347, pszErrMsg) != 0)
          {
            return 1;
          }
          iTemp = szTemp[1] - '0';
          iTemp = 358+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
      }
      else if (szTemp[0]=='3')
      {
        //30幾日
        if (szTemp[1]=='0')
        {
          //30
          if (AddMemIndexToPlayBuf(nChn, 379, pszErrMsg) != 0)
          {
            return 1;
          }
        }
		    else
        {
          //30幾
          if (AddMemIndexToPlayBuf(nChn, 348, pszErrMsg) != 0)
          {
            return 1;
          }
          iTemp = szTemp[1] - '0';
          iTemp = 358+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s day is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "DD") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s day is error", pszTime);
			//return 1;
		}
	}
	//分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
		if (szTemp[1] == '0')
    {
			if (szTemp[0] > '0')
      {
        //年前2位
        iTemp = szTemp[0] - '0';
			  iTemp = 327 + iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
        //thousand
        if (AddMemIndexToPlayBuf(nChn, 356, pszErrMsg) != 0)
        {
          return 1;
        }
        //年后2位
        if (szTemp[2] != '0' || szTemp[3] != '0')
        {
          //and
          if (AddMemIndexToPlayBuf(nChn, 391, pszErrMsg) != 0)
          {
            return 1;
          }
          if (szTemp[2]>'1')
          {
		        iTemp = szTemp[2] - '0';
            iTemp = 345+iTemp;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
		        if (szTemp[3]>'0')
            {
              iTemp = szTemp[3] - '0';
              iTemp = 327+iTemp;
              if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
              {
                return 1;
              }
            }
          }
          else
          {
		        iTemp = (szTemp[3]-'0')+(szTemp[2]-'0')*10;
            iTemp = 327+iTemp;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
          }
        }
      }
    }
    else
    {
      //年前2位
      if (szTemp[0]>'1')
      {
		    iTemp = szTemp[0] - '0';
        iTemp = 345+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		    if (szTemp[1]>'0')
        {
          iTemp = szTemp[1] - '0';
          iTemp = 327+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
      }
      else
      {
		    iTemp = (szTemp[1]-'0')+(szTemp[0]-'0')*10;
        iTemp = 327+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      //年后2位
      if (szTemp[2] == '0' && szTemp[3] == '0')
      {
        //hundred
        if (AddMemIndexToPlayBuf(nChn, 355, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else
      {
        if (szTemp[2]=='0')
        {
          //o
          if (AddMemIndexToPlayBuf(nChn, 390, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (szTemp[2]=='1')
        {
		      iTemp = (szTemp[3]-'0')+10;
          iTemp = 327+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
        else if (szTemp[2]>'1')
        {
		      iTemp = szTemp[2] - '0';
          iTemp = 345+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
		      if (szTemp[3]>'0')
          {
            iTemp = szTemp[3] - '0';
            iTemp = 327+iTemp;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
          }
        }
      }
    }
	}
	else
	{
		if (strcmpi(szTemp, "YYYY") != 0 && strcmpi(szTemp, "XXXX"))
		{
			sprintf(pszErrMsg, "%s year is error", pszTime);
			//return 1;
		}
	}
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 23)
		{
      if (szTemp[0]>'1')
      {
		    iTemp = szTemp[0] - '0';
        iTemp = 345+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		    if (szTemp[1]>'0')
        {
          iTemp = szTemp[1] - '0';
          iTemp = 327+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
      }
      else
      {
		    iTemp = (szTemp[1]-'0')+(szTemp[0]-'0')*10;
        iTemp = 327+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s hour is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "HH") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s hour is error", pszTime);
			//return 1;
		}
	}
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 59)
		{
      if (szTemp[0]=='0')
      {
        //o
        if (AddMemIndexToPlayBuf(nChn, 390, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else if (szTemp[0]=='1')
      {
		    iTemp = (szTemp[1]-'0')+10;
        iTemp = 327+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else if (szTemp[0]>'1')
      {
		    iTemp = szTemp[0] - '0';
        iTemp = 345+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		    if (szTemp[1]>'0')
        {
          iTemp = szTemp[1] - '0';
          iTemp = 327+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s minute is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "MI") != 0 && strcmpi(szTemp, "XX") != 0 && strcmpi(szTemp, "MM") != 0)
		{
			sprintf(pszErrMsg, "%s minute is error", pszTime);
			//return 1;
		}
	}
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &pszTime[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 59)
		{
      if (szTemp[0]=='0')
      {
        //o
        if (AddMemIndexToPlayBuf(nChn, 390, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else if (szTemp[0]=='1')
      {
		    iTemp = (szTemp[1]-'0')+10;
        iTemp = 327+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else if (szTemp[0]>'1')
      {
		    iTemp = szTemp[0] - '0';
        iTemp = 345+iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		    if (szTemp[1]>'0')
        {
          iTemp = szTemp[1] - '0';
          iTemp = 327+iTemp;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
        }
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s second is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "SS") != 0 && strcmpi(szTemp, "XX") != 0)
		{
			sprintf(pszErrMsg, "%s second is error", pszTime);
			//return 1;
		}
	}
	//分析星期
	memset(szTemp, 0, 20);
	szTemp[0] = pszTime[20];
	if (szTemp[0] >= '0' && szTemp[0] <= '6')
	{
		iTemp = atoi(szTemp);
		if (iTemp >= 0 && iTemp <= 6)
		{
			iTemp = 432 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else
		{
			sprintf(pszErrMsg, "%s weekday is error", pszTime);
			return 1;
		}
	}
	else
	{
		if (strcmpi(szTemp, "W") != 0 && strcmpi(szTemp, "X") != 0)
		{
			sprintf(pszErrMsg, "%s weekday is error", pszTime);
			//return 1;
		}
	}
	return 0;
}
//將金額合成英文
int MixMoney2ENG(DF fMoney, int nChn, CH *pszErrMsg)
{
	double fraction;
	long lNumber;
	int i, len, iTemp, jiao, fen, nFormat;
	int n, h=0, lastzero=0;
	int hasvalue=0;			// 億、萬進位前有數值標記
	char szNumber[30];
  //char *szFration;

	nFormat = 0;
	//判斷正負
	if(fMoney<0) 
	{
		fMoney = -fMoney;
		iTemp = 408; //負
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	
	//處理整數
	lNumber = (long)fMoney;
	if (lNumber > 0)
	{
		//ltoa(lNumber, szNumber, 10);
		sprintf(szNumber, "%ld", lNumber);
		len = strlen(szNumber);
		if (len > 12)
		{
			sprintf(pszErrMsg, "value is too big");
			return 1;
		}
		for(i = len; i > 0; i--)
		{
			n = szNumber[len-i] - '0';

			if( n!=0 ) 
			{
        if (i%3 == 0)
        {
          //hundred
		      iTemp = 327+n;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
          if (AddMemIndexToPlayBuf(nChn, 355, pszErrMsg) != 0)
          {
            return 1;
          }
          lastzero=1;
        }
        else if (i%3 == 2)
        {
          if (n==1)
          {
            h=10;
          }
          else
          {
		        iTemp = 345+n;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          lastzero=1;
        }
        else if (i%3 == 1)
        {
		      iTemp = 327+n+h;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
          h=0;

          lastzero=1;
        }

        if (i == 10)
        {
          if (h>0)
          {
		        iTemp = 327+h;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          if (lastzero!=0)
          {
            //billion
            if (AddMemIndexToPlayBuf(nChn, 358, pszErrMsg) != 0)
            {
              return 1;
            }
          }
          lastzero=0;
        }
        else if (i == 7)
        {
          if (h>0)
          {
		        iTemp = 327+h;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          if (lastzero!=0)
          {
            //million
            if (AddMemIndexToPlayBuf(nChn, 357, pszErrMsg) != 0)
            {
              return 1;
            }
          }
          lastzero=0;
        }
        else if (i == 4)
        {
          if (h>0)
          {
		        iTemp = 327+h;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          if (lastzero!=0)
          {
            //thousand
            if (AddMemIndexToPlayBuf(nChn, 356, pszErrMsg) != 0)
            {
              return 1;
            }
          }
          lastzero=0;
        }
			}
		}
	}
  else
  {
		iTemp = 327; //zero
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
  }
  if (lNumber<=1)
  {
    //dollar
    if (AddMemIndexToPlayBuf(nChn, 409, pszErrMsg) != 0)
    {
      return 1;
    }
  }
  else
  {
    //dollars
    if (AddMemIndexToPlayBuf(nChn, 393, pszErrMsg) != 0)
    {
      return 1;
    }
  }

	//處理小數位
	fraction = fMoney - lNumber;
	if (fraction > 0)
	{
	  jiao = int(fraction*10);
	  fen = int(fraction*100)-jiao*10;
	  if (jiao > 0 || fen > 0)
	  {
      if (jiao>1)
      {
		    iTemp = 345+jiao;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		    iTemp = 327+fen;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else
      {
		    iTemp = 327+fen+jiao*10;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      if (jiao==0 && fen==1)
      {
        //cent
        if (AddMemIndexToPlayBuf(nChn, 410, pszErrMsg) != 0)
        {
          return 1;
        }
      }
      else
      {
        //cents
        if (AddMemIndexToPlayBuf(nChn, 394, pszErrMsg) != 0)
        {
          return 1;
        }
      }
	  }
  }
	return 0;
}
//將數值合成英文
int MixNumber2ENG(DF fNumber, int nChn, CH *pszErrMsg)
{
	double fraction;
	long lNumber;
	int i, len, dec, sign, iTemp, nFormat;
	int n, h=0, lastzero=0;
	int hasvalue=0;			// 億、萬進位前有數值標記
	char cTemp, szNumber[30];
  char *szFration;

	nFormat = 0;
	//判斷正負
	if(fNumber<0) 
	{
		fNumber = -fNumber;
		iTemp = 408; //負
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
	}
	
	//處理整數
	lNumber = (long)fNumber;
	if (lNumber > 0)
	{
		//ltoa(lNumber, szNumber, 10);
		sprintf(szNumber, "%ld", lNumber);
		len = strlen(szNumber);
		if (len > 12)
		{
			sprintf(pszErrMsg, "value is too big");
			return 1;
		}
		for(int i = len; i > 0; i--)
		{
			n = szNumber[len-i] - '0';

			if( n!=0 ) 
			{
        if (i%3 == 0)
        {
          //hundred
		      iTemp = 327+n;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
          if (AddMemIndexToPlayBuf(nChn, 355, pszErrMsg) != 0)
          {
            return 1;
          }
          lastzero=1;
        }
        else if (i%3 == 2)
        {
          if (n==1)
          {
            h=10;
          }
          else
          {
		        iTemp = 345+n;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          lastzero=1;
        }
        else if (i%3 == 1)
        {
		      iTemp = 327+n+h;
          if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
          {
            return 1;
          }
          h=0;

          lastzero=1;
        }

        if (i == 10)
        {
          if (h>0)
          {
		        iTemp = 327+h;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          if (lastzero!=0)
          {
            //billion
            if (AddMemIndexToPlayBuf(nChn, 358, pszErrMsg) != 0)
            {
              return 1;
            }
          }
          lastzero=0;
        }
        else if (i == 7)
        {
          if (h>0)
          {
		        iTemp = 327+h;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          if (lastzero!=0)
          {
            //million
            if (AddMemIndexToPlayBuf(nChn, 357, pszErrMsg) != 0)
            {
              return 1;
            }
          }
          lastzero=0;
        }
        else if (i == 4)
        {
          if (h>0)
          {
		        iTemp = 327+h;
            if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
            {
              return 1;
            }
            h=0;
          }
          if (lastzero!=0)
          {
            //thousand
            if (AddMemIndexToPlayBuf(nChn, 356, pszErrMsg) != 0)
            {
              return 1;
            }
          }
          lastzero=0;
        }
			}
		}
	}
  else
  {
		iTemp = 327; //zero
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }
  }

	//處理小數位
	fraction = fNumber - lNumber;
	if (fraction > 0)
	{
		iTemp = 392; //point
    if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
    {
      return 1;
    }

    szFration = fcvt(fraction, 8, &dec, &sign);
    len = strlen(szFration);
    for (i = len-1; i >= 0; i --)
    {
      if (szFration[i] == '0')
      {
          szFration[i] = '\0';
      }
      else
      {
          break;
      }
    }
    len = strlen(szFration);
    for (i = 0; i < len; i ++)
    {
      cTemp = szFration[i];
		  if (cTemp >= '0' && cTemp <= '9')
		  {
		    iTemp = cTemp - '0';
		    iTemp = 327 + iTemp;
        if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
        {
          return 1;
        }
		  }
    }
	}
	
	return 0;
}
//將數字串合成英文
int MixDigits2ENG(const CH *pszDigits, int nChn, CH *pszErrMsg)
{
	int i, len, iTemp, nFormat;
	CH cTemp;

	nFormat = 0;
	len = strlen(pszDigits);
	for (i = 0; i < len; i ++)
	{
		cTemp = pszDigits[i];
		if (cTemp >= '0' && cTemp <= '9')
		{
			iTemp = cTemp - '0';
			iTemp = 327 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
	}
	return 0;
}
//合成英文字母串
int MixChars(const CH *pszChars, int nChn, CH *pszErrMsg)
{
	int i, len, iTemp, nFormat;
	CH cTemp;

	nFormat = 0;
	len = strlen(pszChars);
	for (i = 0; i < len; i ++)
	{
		cTemp = pszChars[i];
		cTemp = toupper(cTemp);
		if (cTemp >= '0' && cTemp <= '9')
		{
			if (cTemp == '1')
			{
				iTemp = 10;
			}
			else
			{
				iTemp = cTemp - '0';
			}
			iTemp = 0 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
		else if (cTemp >= 'A' && cTemp <= 'Z')
		{
			iTemp = cTemp - 'A';
			iTemp = 300 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
		}
    else if (cTemp >= 'a' && cTemp <= 'z')
    {
      iTemp = cTemp - 'a';
      iTemp = 300 + iTemp;
      if (AddMemIndexToPlayBuf(nChn, iTemp, pszErrMsg) != 0)
      {
        return 1;
      }
    }
	}
	return 0;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//增加單文件放音到放音緩沖區
int AddSingleFileToPlayBuf(int nChn, const CH *FileName, int StartPos, int Length, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	pChn->curPlayData.state = 1;
	pChn->curPlayData.PlayTypeId = 1;
	pChn->curPlayData.StartPos = StartPos;
	pChn->curPlayData.Length = Length;
	pChn->curPlayData.DataBufNum = 1;
	pChn->curPlayData.PlayIndex[0].PlayType = PLAYTYPE_file;
  pChn->curPlayData.PlayIndex[0].PlayFile = FileName;
	pChn->curPlayData.PlayIndex[0].PlayIndex = 0xFFFF;
	pChn->curPlayData.PlayPoint = 0;
	pChn->curPlayData.PlayedTime = 0;
	pChn->curPlayData.PlayedLen = 0;
  return 0;
}
//增加放音文件到緩沖區
int AddFileToPlayBuf(int nChn, const CH *FileName, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
  if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	pChn->curPlayData.state = 1;
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayType = PLAYTYPE_file;
  pChn->curPlayData.PlayIndex[DataBufNum].PlayFile = FileName;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayIndex = 0xFFFF;
	pChn->curPlayData.DataBufNum ++;
	return 0;
}

//-----------------------------------------------------------------------------
int AddTTSStrToPlayBuf(int nChn, const CH *TTSStr, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
  if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	pChn->curPlayData.state = 1;
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayType = PLAYTYPE_ttsstr;
  pChn->curPlayData.PlayIndex[DataBufNum].PlayFile = TTSStr;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayIndex = 0xFFFF;
	pChn->curPlayData.DataBufNum ++;
	return 0;
}
int AddTTSFileToPlayBuf(int nChn, const CH *FileName, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
  if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	pChn->curPlayData.state = 1;
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayType = PLAYTYPE_ttsfile;
  pChn->curPlayData.PlayIndex[DataBufNum].PlayFile = FileName;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayIndex = 0xFFFF;
	pChn->curPlayData.DataBufNum ++;
	return 0;
}
int AddDTMFStrToPlayBuf(int nChn, const CH *DTMFStr, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
  if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	pChn->curPlayData.state = 1;
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayType = PLAYTYPE_dtmf;
  pChn->curPlayData.PlayIndex[DataBufNum].PlayFile = DTMFStr;
	pChn->curPlayData.PlayIndex[DataBufNum].PlayIndex = 0xFFFF;
	pChn->curPlayData.DataBufNum ++;
	return 0;
}
//增加日期合成串到放音緩沖區
int AddDateTimeToPlayBuf(int SpeechType, int nChn, const CH *TimeStr, CH *ErrMsg, int nMGYear, int nAMPM)
{
	//TimeFormat有效格式：YYYY-MM-DDTHH:MI:SS_W 不需要合成的用對應的字母代替
	int nResult;
  ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
	if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
  MyTrace(3, "AddDateTimeToPlayBuf nChn=%d TimeStr=%s SpeechType=%d",
    nChn, TimeStr, SpeechType);
	switch (SpeechType)
  {
  case LANGUAGE_ENG:
    nResult = MixDateTime2ENG(TimeStr, nChn, ErrMsg);
    break;
  case LANGUAGE_CHN:
    nResult = MixDateTime2CHN(TimeStr, nChn, ErrMsg, nMGYear, nAMPM);
    break;
  case LANGUAGE_LOC:
    nResult = MixDateTime2GDH(TimeStr, nChn, ErrMsg);
    break;
  default:
    nResult = MixDateTime2ENG(TimeStr, nChn, ErrMsg);
    break;
  }
	if (nResult != 0)
	{
		return 1;
	}
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.state = 1;
	return 0;
}
//增加金額合成串到放音緩沖區
int AddMoneyToPlayBuf(int SpeechType, int nChn, double Money, CH *ErrMsg)
{
	int nResult;
  ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
	if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	switch (SpeechType)
  {
  case LANGUAGE_ENG:
    nResult = MixMoney2ENG(Money, nChn, ErrMsg);
    break;
  case LANGUAGE_CHN:
    nResult = MixMoney2CHN(Money, nChn, ErrMsg);
    break;
  case LANGUAGE_LOC:
    nResult = MixMoney2GDH(Money, nChn, ErrMsg);
    break;
  default:
    nResult = MixMoney2ENG(Money, nChn, ErrMsg);
    break;
  }
	if (nResult != 0)
	{
		return 1;
	}
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.state = 1;
	return 0;
}
//增加數值合成串到放音緩沖區
int AddNumberToPlayBuf(int SpeechType, int nChn, double Number, const CH *pszNumber, CH *ErrMsg)
{
	int nResult;
  ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
	if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	switch (SpeechType)
  {
  case LANGUAGE_ENG:
    nResult = MixNumber2ENG(Number, nChn, ErrMsg);
    break;
  case LANGUAGE_CHN:
    nResult = MixNumber2CHN(Number, pszNumber, nChn, ErrMsg);
    break;
  case LANGUAGE_LOC:
    nResult = MixNumber2GDH(Number, nChn, ErrMsg);
    break;
  default:
    nResult = MixNumber2ENG(Number, nChn, ErrMsg);
    break;
  }
	if (nResult != 0)
	{
		return 1;
	}
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.state = 1;
	return 0;
}
//增加數字合成串到放音緩沖區
int AddDigitsToPlayBuf(int SpeechType, int nChn, const CH *Dgits, CH *ErrMsg)
{
	int nResult;
  ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
	if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	switch (SpeechType)
  {
  case LANGUAGE_ENG:
    nResult = MixDigits2ENG(Dgits, nChn, ErrMsg);
    break;
  case LANGUAGE_CHN:
    nResult = MixDigits2CHN(Dgits, nChn, ErrMsg);
    break;
  case LANGUAGE_LOC:
    nResult = MixDigits2GDH(Dgits, nChn, ErrMsg);
    break;
  default:
    nResult = MixDigits2ENG(Dgits, nChn, ErrMsg);
    break;
  }
  
  if (nResult != 0)
	{
		return 1;
	}
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.state = 1;
	return 0;
}
//增加字母合成串到放音緩沖區
int AddCharsToPlayBuf(int nChn, const CH *Chars, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	int DataBufNum = pChn->curPlayData.DataBufNum;
	if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play bufer is overflow");
		return 1;
	}
	if (MixChars(Chars, nChn, ErrMsg) != 0)
	{
		return 1;
	}
	pChn->curPlayData.PlayTypeId = 2;
	pChn->curPlayData.state = 1;
	return 0;
}
//增加信號音到放音緩沖區
int AddToneToPlayBuf(int nChn, int ToneType, int DtmfRuleId, int Repeat, int ReleaseId, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	if (DtmfRuleId >= MAX_DTMFRULE_BUF) 
	{
		sprintf(ErrMsg, "DtmfRule=%d is out of range", DtmfRuleId);
		return 1;
	}
	if (pChn->DtmfRule[DtmfRuleId].state == 0)
	{
		sprintf(ErrMsg, "DtmfRule=%d not set", DtmfRuleId);
		return 1;
	}

  pChn->DtmfPauseTimer = 0;
	pChn->PlayPauseTimer = 0;

  //設置按鍵停止放音標志
  pChn->curDtmfRuleId = DtmfRuleId;
	pChn->RecvDtmfId = 0;
	memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
	//清除收碼緩沖
  memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);

  pChn->curPlayState = 0;
  pChn->curPlayRuleId = 0;
  pChn->PlayRule[0].state = 1;
	pChn->PlayRule[0].repeat = Repeat;

	pChn->curPlayData.state = 1;
	pChn->curPlayData.PlayTypeId = 3;
	pChn->curPlayData.StartPos = 0;
	pChn->curPlayData.Length = 0;
	pChn->curPlayData.DataBufNum = 1;
	pChn->curPlayData.PlayIndex[0].PlayType = PLAYTYPE_tone;
	pChn->curPlayData.PlayIndex[0].PlayIndex = ToneType;
	pChn->curPlayData.PlayPoint = 0;
	pChn->curPlayData.PlayedTime = 0;
	pChn->curPlayData.PlayedLen = 0;
	pChn->curPlayData.PlayEndReleaseId = ReleaseId;

  return 0;
}

int AddMixToPlayBuf(int SpeechType, int nChn, int mixtype, const CH *content, CH *ErrMsg, int nMGYear, int nAMPM)
{
	double Number;
	int nResult = 0;

  MyTrace(3, "AddMixToPlayBuf nChn=%d SpeechType=%d mixtype=%d content=%s nMGYear=%d",
    nChn, SpeechType, mixtype, content, nMGYear);
  ErrMsg[0] = 0;
  if (strlen(content) > 0)
	{
		switch (mixtype)
		{
	  case 1: //file
      if (AddFileToPlayBuf(nChn, content, ErrMsg) != 0)
		  {
			  nResult = 1;
		  }
		  break;
	  case 2: //datetime
		  if (AddDateTimeToPlayBuf(SpeechType, nChn, content, ErrMsg, nMGYear, nAMPM) != 0)
		  {
  		  nResult = 1;
		  }
		  break;
	  case 3: //money
		  if (Check_Float_Str(content, Number) != 0)
		  {
  		  nResult = 1;
        sprintf(ErrMsg, "money: %s is illegal", content);
			  break;
		  }
		  if (AddMoneyToPlayBuf(SpeechType, nChn, Number, ErrMsg) != 0)
		  {
  		  nResult = 1;
		  }
		  break;
	  case 4: //number
		  if (Check_Float_Str(content, Number) != 0)
		  {
  		  nResult = 1;
        sprintf(ErrMsg, "number: %s is illegal", content);
			  break;
		  }
		  if (AddNumberToPlayBuf(SpeechType, nChn, Number, content, ErrMsg) != 0)
		  {
        nResult = 1;
		  }
		  break;
	  case 5: //digits
		  if (AddDigitsToPlayBuf(SpeechType, nChn, content, ErrMsg) != 0)
		  {
  		  nResult = 1;
		  }
		  break;
	  case 6: //chars
		  if (AddCharsToPlayBuf(nChn, content, ErrMsg) != 0)
		  {
  		  nResult = 1;
		  }
		  break;
		}
	}
  return nResult;
}
//-----------------------------------------------------------------------------
//清除當前收碼規則
void ResetDtmfRule(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
	pChn->DtmfPauseTimer = 0;
	pChn->RecvDtmfId = 0;
  pChn->curDtmfRuleId = 0xFFFF;
  pChn->PbxFirstDigitId = 0;
}
//清除當前放音規則
void ResetPlayRule(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
	pChn->curSpeech = LANGUAGE_CHN;
  pChn->PlayPauseTimer = 0;
  pChn->curPlayRuleId = 0xFFFF;
}
//清除放音緩沖數據
void ResetPlayBuf(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
  pChn->curPlayState = 0;
  //初始化放音緩沖
	pChn->PlayPauseTimer = 0;

  pBoxchn->ClearPlayData(nChn);
}
//清除錄音緩沖數據
void ResetRecBuf(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
  pChn->curRecState = 0;
	//初始化錄音緩沖
	pChn->RecTimer = 0;
	pChn->RecvDtmfId = 0;
	memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);

	pBoxchn->ClearRecData(nChn);
}
//停止正在進行的放音并清除放音沖區
void Stop_Play_Clear_Buf(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
	if (pChn->state == 0) return;

	//停止放音
  StopPlay(nChn);
  //pChn->Volume = 8;
	//pChn->PlayBusFlag = 0;
	pChn->svState = CHN_SV_IDEL;

	ResetPlayRule(nChn);
	ResetPlayBuf(nChn);
}
//停止正在進行的錄音并清除錄音沖區
void Stop_Rec_Clear_Buf(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
	if (pChn->state == 0) return;

	StopRecord(nChn);
	//pChn->RecAGCFlag = 0;
  //pChn->RecMixerFlag = 0;
	pChn->svState = CHN_SV_IDEL;

	ResetRecBuf(nChn);
}
//停止收碼清除收碼緩沖區及收碼規則
void Stop_Dtmf_Clear_Buf(int nChn)
{
	//if (!pBoxchn->isnChnAvail(nChn)) return;
	if (pChn->state == 0) return;

  //清除收碼緩沖
	memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);

  ResetDtmfRule(nChn);
}
//停止以前的放音收碼及錄音
int StopClearPlayDtmfRecBuf(int nChn)
{
	//停止正在進行的放音
	Stop_Play_Clear_Buf(nChn);
	//停止收碼
	Stop_Dtmf_Clear_Buf(nChn);
	//停止正在進行的錄音
	Stop_Rec_Clear_Buf(nChn);
  return 0;
}
int StopClearPlayDtmfBuf(int nChn)
{
	//停止正在進行的放音
	Stop_Play_Clear_Buf(nChn);
	//停止收碼
	Stop_Dtmf_Clear_Buf(nChn);
  return 0;
}
int StopClearRecDtmfBuf(int nChn)
{
	//停止收碼
	Stop_Dtmf_Clear_Buf(nChn);
	//先停止正在的錄音
	Stop_Rec_Clear_Buf(nChn);
  return 0;
}
void SetcurPlayRuleData(int nChn, int PlayRuleId)
{
	pChn->curPlayState = 0;
  if (pChn->PlayBusFlag == 1)
    SetPlayBus(nChn, 1);
  pChn->curPlayRuleId = PlayRuleId;
  pChn->curSpeech = pChn->PlayRule[PlayRuleId].speech;
}
void SetcurDtmfRuleData(int nChn, int DtmfRuleId)
{
  pChn->curDtmfRuleId = DtmfRuleId;
  memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);
	memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);

	pChn->RecvDtmfId = 0;
}
//設置當前收碼放音規則
int SetDTMFPlayRule(int nChn, int DtmfRuleId, int PlayRuleId, CH *ErrMsg)
{
	int dtmfid, playid;
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	if (pChn->CanPlayOrRecId == 0)
	{
		sprintf(ErrMsg, "chn=%d not in can play or rec state", nChn);
		return 1;
	}
	dtmfid = DtmfRuleId;
  playid = PlayRuleId;
  if (dtmfid >= MAX_DTMFRULE_BUF) 
	{
		sprintf(ErrMsg, "DtmfRule=%d is out of range", DtmfRuleId);
    dtmfid = 0;
		//return 1;
	}
	if (pChn->DtmfRule[dtmfid].state == 0)
	{
		sprintf(ErrMsg, "DtmfRule=%d not set", DtmfRuleId);
		dtmfid = 0;
    //return 1;
	}
	if (playid > MAX_PLAYRULE_BUF) 
	{
		sprintf(ErrMsg, "PlayRule=%d is out of range", PlayRuleId);
		playid = 0;
    //return 1;
	}
	if (pChn->PlayRule[playid].state == 0)
	{
		sprintf(ErrMsg, "PlayRule=%d not set", PlayRuleId);
		playid = 0;
    //return 1;
	}
	pChn->DtmfPauseTimer = 0;
	pChn->PlayPauseTimer = 0;

  SetcurDtmfRuleData(nChn, dtmfid);
	SetcurPlayRuleData(nChn, playid);
	return 0;
}
//設置當前放音規則
int SetPlayRule(int nChn, int PlayRuleId, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	if (PlayRuleId >= MAX_PLAYRULE_BUF) 
	{
		sprintf(ErrMsg, "PlayRule=%d is out of range", PlayRuleId);
		return 1;
	}
	if (pChn->PlayRule[PlayRuleId].state == 0)
	{
		sprintf(ErrMsg, "PlayRule=%d not set", PlayRuleId);
		return 1;
	}
	SetcurPlayRuleData(nChn, PlayRuleId);
	return 0;
}
//設置當前收碼規則
int SetDTMFRule(int nChn, int DtmfRuleId, CH *ErrMsg)
{
	//int Volume = 0;
	ErrMsg[0] = 0;
  //if (!pBoxchn->isnChnAvail(nChn)) return 1;
	if (DtmfRuleId >= MAX_DTMFRULE_BUF) 
	{
		sprintf(ErrMsg, "DtmfRule=%d is out of range", DtmfRuleId);
		return 1;
	}
	if (pChn->DtmfRule[DtmfRuleId].state == 0)
	{
		sprintf(ErrMsg, "DtmfRule=%d not set", DtmfRuleId);
		return 1;
	}
  SetcurDtmfRuleData(nChn, DtmfRuleId);

  SetcurPlaySessionParam(nChn, MSG_ongetdtmf, "ongetdtmf");

  return 0;
}

//設置當前錄音收碼規則
int SetChnRecordRule(int nChn, int BeepId, const CH *FileName, 
                    const CH *TermChar, int Format, int MaxTime, int WriteMode, char *errorbuf, int mixterid)
{
  int nResult=1;

  if (g_nSwitchType != 13 && g_nSwitchMode != 0)
  {
    //TRACE("nChn=%d pChn->cVopChn=%d\n", nChn, pChn->cVopChn);
    if (pChn->cVopChn == NULL)
    {
      int nChn1 = pChn->nSwtPortID;
      if (pBoxchn->isnChnAvail(nChn1))
      {
        if (pChn1->cVopChn == NULL)
        {
          strcpy(errorbuf, "link port have not banding vopchn error");
          return nResult;
        }

        if (pChn1->cVopChn->m_nVopChnType != 6 && pChn1->cVopChn->m_nVopChnType != 7)
        {
          strcpy(errorbuf, "link port banding vopchn type error");
          return nResult;
        }
        
        //本代碼主要對連接的中繼監錄通道錄音
        //存儲錄音參數
        pChn1->RecTimer = 0;
        
        pChn1->curRecState = 0;
        pChn1->curRecData.state = 0;
        pChn1->curRecData.RecState = 1;
        pChn1->curRecData.RecTypeId = 1;
        pChn1->curRecData.BeepId = BeepId;
        pChn1->curRecData.Format = Format;
        pChn1->curRecData.RecordFile = FileName;
        pChn1->curRecData.TermChar = TermChar;
        if (WriteMode == 0)
        {
          pChn1->curRecData.StartPos = 0; //覆蓋錄音
        }
        else
        {
          pChn1->curRecData.StartPos = -1; //追加錄音
        }
        pChn1->curRecData.MaxTime = MaxTime; //為0時表示不限制時間
        pChn1->curRecData.RecedLen = 0;
        pChn1->curRecData.CanRecId = 1;
        
        memset(pChn1->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
        //清除收碼緩沖
        memset(pChn1->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);
        
        if (mixterid == 1)
          SetRecdParam(nChn, 1, pChn->RecAGCFlag);
        else
          SetRecdParam(nChn, pChn1->RecMixerFlag, pChn->RecAGCFlag);

        pChn->RecTimer = 0;
        
        pChn->curRecState = 0;
        pChn->curRecData.state = 1;
        pChn->curRecData.RecState = 0;
        pChn->curRecData.RecTypeId = 1;
        pChn->curRecData.BeepId = BeepId;
        pChn->curRecData.Format = Format;
        pChn->curRecData.RecordFile = FileName;
        pChn->curRecData.TermChar = TermChar;
        if (WriteMode == 0)
        {
          pChn->curRecData.StartPos = 0; //覆蓋錄音
        }
        else
        {
          pChn->curRecData.StartPos = -1; //追加錄音
        }
        pChn->curRecData.MaxTime = MaxTime; //為0時表示不限制時間
        pChn->curRecData.RecedLen = 0;
        pChn->curRecData.CanRecId = 1;
        
        memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
        //清除收碼緩沖
        memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);
        
        //是否需要放錄音提示音“嘀”
        if (BeepId == 1)
        {
          //發送文件錄音命令消息
          if (RecordFile(nChn1) == 0)
          {
            SendChnRecResult(nChn, OnRecording, 0, 0);
            pChn->curRecData.RecState = 1;
            pChn1->nSwtPortID = nChn;
            nResult = 0;
          }
          else
          {
            SendChnRecResult(nChn, OnRecordError, 0, 0);
            StopClearRecDtmfBuf(nChn1);
            StopClearRecDtmfBuf(nChn);
          }
        }
        else
        {
          //發送文件錄音命令消息
          if (RecordFile(nChn1) == 0)
          {
            //返回正在錄音消息
            SendChnRecResult(nChn, OnRecording, 0, 0);
            pChn->curRecData.RecState = 2;
            pChn1->nSwtPortID = nChn;
            nResult = 0;
          }
          else
          {
            SendChnRecResult(nChn, OnRecordError, 0, 0);
            StopClearRecDtmfBuf(nChn1);
            StopClearRecDtmfBuf(nChn);
          }
        }
        memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
        return nResult;
      }
      strcpy(errorbuf, "have not banding vopchn error");
      return 1;
    }
  }

	//存儲錄音參數
	pChn->RecTimer = 0;
	
  pChn->curRecState = 0;
  pChn->curRecData.state = 1;
	pChn->curRecData.RecState = 0;
	pChn->curRecData.RecTypeId = 1;
	pChn->curRecData.BeepId = BeepId;
	pChn->curRecData.Format = Format;
	pChn->curRecData.RecordFile = FileName;
	pChn->curRecData.TermChar = TermChar;
	if (WriteMode == 0)
	{
		pChn->curRecData.StartPos = 0; //覆蓋錄音
	}
	else
	{
		pChn->curRecData.StartPos = -1; //追加錄音
	}
	pChn->curRecData.MaxTime = MaxTime; //為0時表示不限制時間
	pChn->curRecData.RecedLen = 0;
	pChn->curRecData.CanRecId = 1;

  memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
	//清除收碼緩沖
  memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);

  if (mixterid == 1)
    SetRecdParam(nChn, 1, pChn->RecAGCFlag);
  else
    SetRecdParam(nChn, pChn->RecMixerFlag, pChn->RecAGCFlag);

  //是否需要放錄音提示音“嘀”
  if (BeepId == 1)
	{
    //發送文件錄音命令消息
    if (RecordFile(nChn) == 0)
    {
      SendChnRecResult(nChn, OnRecording, 0, 0);
      pChn->curRecData.RecState = 1;
      nResult = 0;
    }
    else
    {
      SendChnRecResult(nChn, OnRecordError, 0, 0);
      StopClearRecDtmfBuf(nChn);
    }
	}
	else
	{
    //發送文件錄音命令消息
    if (RecordFile(nChn) == 0)
    {
		  //返回正在錄音消息
      SendChnRecResult(nChn, OnRecording, 0, 0);
      pChn->curRecData.RecState = 2;
      nResult = 0;
    }
    else
    {
      SendChnRecResult(nChn, OnRecordError, 0, 0);
      StopClearRecDtmfBuf(nChn);
    }
	}
  memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
	return nResult;
}
int SetChnMonitorRecordRule(int nChn, const CH *FileName, int Format, int MaxTime, int WriteMode)
{
  int nResult=1;

  //存儲錄音參數
  pChn->RecTimer = 0;
  
  pChn->curRecState = 0;
  pChn->curRecData.state = 1;
  pChn->curRecData.RecState = 0;
  pChn->curRecData.RecTypeId = 1;
  pChn->curRecData.BeepId = 0;
  pChn->curRecData.Format = Format;
  pChn->curRecData.RecordFile = FileName;
  pChn->curRecData.TermChar = "";
  if (WriteMode == 0)
  {
    pChn->curRecData.StartPos = 0; //覆蓋錄音
  }
  else
  {
    pChn->curRecData.StartPos = -1; //追加錄音
  }
  pChn->curRecData.MaxTime = MaxTime; //為0時表示不限制時間
  pChn->curRecData.RecedLen = 0;
  pChn->curRecData.CanRecId = 1;
  
  memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
  //清除收碼緩沖
  memset(pChn->RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);
  
  SetRecdParam(nChn, 1, pChn->RecAGCFlag);
  //發送文件錄音命令消息
  if (RecordFile(nChn) == 0)
  {
		  //返回正在錄音消息
    SendChnRecResult(nChn, OnRecording, 0, 0);
    pChn->curRecData.RecState = 2;
    nResult = 0;
  }
  else
  {
    SendChnRecResult(nChn, OnRecordError, 0, 0);
    StopClearRecDtmfBuf(nChn);
  }

  memset(pChn->curRecvDtmf, 0, MAX_DTMFBUF_LEN);
  return nResult;
}
//對通道啟動放音
int Start_ChnPlay(int nChn)
{
  if (g_nSwitchMode == 0 || g_nSwitchType == 13)
  {
    return Start_VopChnPlay(nChn);
  }
  else if (pChn->cVopChn)
  {
    if (pChn->SeizeVopChnState == 1 && (pChn->cVopChn->m_nVopChnType == 0 || pChn->cVopChn->m_nVopChnType == 1 || pChn->cVopChn->m_nVopChnType == 8))
    {
      return Start_VopChnPlay(nChn);
    }
  }
  SendChnPlayResult(nChn, OnPlayError, "have not banding vopchn error");
  return 1;
}
//對通道啟動放音
int Start_VopChnPlay(int nChn)
{
  CStringX error;
  int index, result = 0;

  pChn->curPlayState = 0;
  pChn->PlayedTimer = 0;
	if (pChn->curPlayData.DataBufNum == 0)
  {
    error = "no voice for play";
		result = 1;
  }
  else
  {
    SetRPparam(nChn, 0);
    if (pChn->curPlayData.PlayTypeId == 1)
	  {
		  //單文件放音
      if (PlaySingleFile(nChn) != 0)
      {
        error = "play file error";
        result = 1;
      }
	  }
	  else if (pChn->curPlayData.PlayTypeId == 2)
	  {
		  //組合放音
      for (int i = 0; i < pChn->curPlayData.DataBufNum; i ++)
      {
        switch (pChn->curPlayData.PlayIndex[i].PlayType)
        {
        case PLAYTYPE_file: //文件放音
          if (PlayFile(nChn, i) != 0)
          {
			      error = "play file error";
            result = 1;
          }
          break;

        case PLAYTYPE_index: //內存索引放音
          if (PlayIndex(nChn, i) != 0) 
          {
			      error = "play index voc error";
            result = 1;
          }
          break;

        case PLAYTYPE_ttsfile: //TTS文件
          if (PlayTTSFile(nChn, i) != 0)
          {
			      error = "plat ttsfile error";
            result = 1;
          }
          break;

        case PLAYTYPE_ttsstr: //TTS字符串
          if (PlayTTS(nChn, i) != 0)
          {
			      error = "play tts string error";
            result = 1;
          }
          break;
        case PLAYTYPE_dtmf: //dtmf串
          if (SendDTMFStr(nChn) != 0)
          {
			      error = "send DTMF error";
            result = 1;
          }
          break;
        default:
			    error = "play type is error";
          result = 1;
          break;
        }
      }
	  }
	  else if (pChn->curPlayData.PlayTypeId == 3)
	  {
		  //信號音索引號是從901開始
      index = pChn->curPlayData.PlayIndex[0].PlayIndex + 900;
      if (PlayTone(nChn, index, 1) != 0)
      {
			  error = "play index voc error";
        result = 1;
      }
	  }
    else if (pChn->curPlayData.PlayTypeId == 4) //TTS語音流
    {
      if (pChn->curPlayData.CurBufId == 0 || pChn->curPlayData.CurBufId == 1)
      {
        if (pChn->curPlayData.StreamLen[0] > 0)
        {
          if (PlayMemory(nChn, pChn->curPlayData.TTSBuf[0], pChn->curPlayData.StreamLen[0]) != 0)
          {
			      error = "play vioce stream error0";
            result = 1;
            SendChnPlayResult(nChn, OnPlayError, error.C_Str());
          }
          else
          {
            pChn->curPlayData.CurBufId = 1;
          }
        }
        else
        {
          pChn->curPlayData.CurBufId = 0;
        }
      }
      else
      {
        if (pChn->curPlayData.StreamLen[1] > 0)
        {
          if (PlayMemory(nChn, pChn->curPlayData.TTSBuf[1], pChn->curPlayData.StreamLen[1]) != 0)
          {
			      error = "play vioce stream error1";
            result = 1;
            SendChnPlayResult(nChn, OnPlayError, error.C_Str());
          }
        }
        else
        {
          pChn->curPlayData.CurBufId = 0;
        }
      }
    }
  }
  if (result == 1)
  {
		SendChnPlayResult(nChn, OnPlayError, error.C_Str());
    if (pChn->PlayRule[pChn->curPlayRuleId].error == 1) //放音錯誤時是否要釋放該通道
    {
      Release_Chn(nChn, 1, 1);
    }
    else
    {
      StopClearPlayDtmfBuf(nChn);
    }
    return 1;
  }
	return 0;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

//增加放音文件到會議放音緩沖區
int AddFileToConfPlayBuf(int nCfc, const CH *FileName, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
	int DataBufNum = pCfc->curPlayData.DataBufNum;
  if (DataBufNum >= MAX_PLAY_INDEX_NUM)
	{
		sprintf(ErrMsg, "play buffer is overflow");
		return 1;
	}
	pCfc->curPlayData.state = 1;
	pCfc->curPlayData.PlayTypeId = 2;
	pCfc->curPlayData.PlayIndex[DataBufNum].PlayType = PLAYTYPE_file;
	pCfc->curPlayData.PlayIndex[DataBufNum].PlayFile = FileName;
	pCfc->curPlayData.PlayIndex[DataBufNum].PlayIndex = 0xFFFF;
	pCfc->curPlayData.DataBufNum ++;
	return 0;
}

//設置會議放音規則
int SetConfPlayRule(int nCfc, int nChn, int PlayRuleId, CH *ErrMsg)
{
	ErrMsg[0] = 0;
  //if (!pBoxconf->isnCfcAvail(nCfc)) return 1;

	pCfc->PlayPauseTimer = 0;
  pCfc->curPlayState = 0;

  if (pBoxchn->isnChnAvail(nChn))
  {
    if (PlayRuleId < MAX_PLAYRULE_BUF) 
    {
      if (pChn->PlayRule[PlayRuleId].state == 1)
      {
        pCfc->curPlayRule.state = 1;
	      pCfc->curPlayRule.ruleid = PlayRuleId;
	      pCfc->curPlayRule.format = pChn->PlayRule[PlayRuleId].format;
	      pCfc->curPlayRule.repeat = pChn->PlayRule[PlayRuleId].repeat;
	      pCfc->curPlayRule.duration = pChn->PlayRule[PlayRuleId].duration;
	      pCfc->curPlayRule.broadcast = pChn->PlayRule[PlayRuleId].broadcast;
	      pCfc->curPlayRule.volume = pChn->PlayRule[PlayRuleId].volume;
	      pCfc->curPlayRule.speech = pChn->PlayRule[PlayRuleId].speech;
	      pCfc->curPlayRule.voice = pChn->PlayRule[PlayRuleId].voice;
	      pCfc->curPlayRule.pause = pChn->PlayRule[PlayRuleId].pause;
	      pCfc->curPlayRule.error = pChn->PlayRule[PlayRuleId].error;
        return 0;
      }
    }
  }
  //設置為默認的放音規則
  pCfc->curPlayRule.state = 1;
	pCfc->curPlayRule.ruleid = 0;
	pCfc->curPlayRule.format = 0;
	pCfc->curPlayRule.repeat = 1;
	pCfc->curPlayRule.duration = 0;
	pCfc->curPlayRule.broadcast = 0;
	pCfc->curPlayRule.volume = 0;
	pCfc->curPlayRule.speech = 2;
	pCfc->curPlayRule.voice = 1;
	pCfc->curPlayRule.pause = 0;
	pCfc->curPlayRule.error = 0;

	return 0;
}
//停止正在進行的會議放音并清除放音沖區
void Stop_Play_Clear_ConfBuf(int nCfc)
{
	//if (!pBoxconf->isnCfcAvail(nCfc)) return;
	if (pCfc->state == 0) return;

	//停止放音
  ConfStopPlay(nCfc);

  pBoxconf->ClearPlayRule(nCfc);

  //初始化放音緩沖
  pBoxconf->ClearPlayData(nCfc);
}
//設置會議錄音
int SetConfRecordRule(int nCfc, const CH *FileName, int Format, int MaxTime, int WriteMode)
{
  //if (!pBoxconf->isnCfcAvail(nCfc)) return 1;

  //存儲錄音參數
	pCfc->RecTimer = 0;
	
  pCfc->curRecState = 0;
  pCfc->curRecData.state = 1;
	pCfc->curRecData.RecState = 0;
	pCfc->curRecData.RecTypeId = 1;
	pCfc->curRecData.BeepId = 0;
	pCfc->curRecData.Format = Format;
	pCfc->curRecData.RecordFile = FileName;
	pCfc->curRecData.TermChar.Empty();
	if (WriteMode == 1)
	{
		pCfc->curRecData.StartPos = 0;
	}
	else
	{
		pCfc->curRecData.StartPos = -1;
	}
	pCfc->curRecData.MaxTime = MaxTime;
	pCfc->curRecData.RecedLen = 0;
	pCfc->curRecData.CanRecId = 1;

  //發送文件監錄命令消息
  ConfRecordFile(nCfc);

  ////返回正在監錄消息
  SendConfRecResult(nCfc, OnRecording, 0, 0);
  pCfc->curRecData.RecState = 2;
  return 0;
}
//停止正在進行的會議錄音并清除錄音沖區
void Stop_Rec_Clear_ConfBuf(int nCfc)
{
	//if (!pBoxconf->isnCfcAvail(nCfc)) return;
	if (pCfc->state == 0) return;

	ConfStopRecord(nCfc);
  pBoxconf->ClearRecData(nCfc);
}
//對會議啟動放音
int Start_ConfPlay(int nCfc)
{
	//if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
	if (pCfc->state == 0 || pCfc->CreateId != 3) return 1;
  
  for (int i = 0; i < pCfc->curPlayData.DataBufNum; i ++)
  {
    if (pCfc->curPlayData.PlayIndex[i].PlayType == PLAYTYPE_file)
    {
      if (ConfPlayFile(nCfc, i) == 0)
      {
			  //清除放音緩沖區
			  Stop_Play_Clear_ConfBuf(nCfc);
        return 1;
      }
    }
    else
    {
			//清除放音緩沖區
			Stop_Play_Clear_ConfBuf(nCfc);
    }
  }
  return 0;
}

int GetIVRChn(int nRouteNo, const CH *Extension)
{
  static int n=0;
  int nChn;
  if (nRouteNo == 0)
  {
    for (int i = 0; i < pBoxchn->TotalIvrNum; i ++)
    {
      n = (n+1)%pBoxchn->TotalIvrNum;

      if (pBoxchn->pIvr_str[n].state == 1)
      {
        nChn = pBoxchn->pIvr_str[n].ChnNo;
        if (pChn->cVopChn != NULL && pChn->hwState == 0 && pChn->lnState == 0 && pChn->ssState == 0)
        {
          if (pChn->cVopChn->m_nLnState == 0 && (pChn->cVopChn->m_nVopChnType == 0 || pChn->cVopChn->m_nVopChnType == 1)) //2015-12-28 如果IVR線路阻塞就不占用
            return nChn;
        }
      }
    }
  }
  else //2016-04-21 增加IVR路由判斷
  {
    int last_RouteIndex = pBoxchn->Routes[nRouteNo].LastIndex;
    
    for (int i = 0; i < pBoxchn->Routes[nRouteNo].TrkTotalNum && i < MAX_ROUTE_TRUNK_NUM; i ++)
		{
      last_RouteIndex = (last_RouteIndex + 1) % pBoxchn->Routes[nRouteNo].TrkTotalNum; //MAX_ROUTE_TRUNK_NUM;
      pBoxchn->Routes[nRouteNo].LastIndex = last_RouteIndex;
      int nIvr = pBoxchn->Routes[nRouteNo].TrkNoList[last_RouteIndex];
      if (nIvr >= 0 && nIvr < pBoxchn->TotalIvrNum)
      {
        if (pBoxchn->pIvr_str[nIvr].state == 1)
        {
          nChn = pBoxchn->pIvr_str[nIvr].ChnNo;
          if (pBoxchn->isnChnAvail(nChn))
          {
            if (pChn->cVopChn != NULL && pChn->hwState == 0 && pChn->lnState == 0 && pChn->ssState == 0)
            {
              if (pChn->cVopChn->m_nLnState == 0 && (pChn->cVopChn->m_nVopChnType == 0 || pChn->cVopChn->m_nVopChnType == 1)) //2015-12-28 如果IVR線路阻塞就不占用
                return nChn;
            }
          }
        }
      }
		}
  }
  return -1;
}
