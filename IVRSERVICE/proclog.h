//---------------------------------------------------------------------------
#ifndef ProclogH
#define ProclogH
//---------------------------------------------------------------------------
void SendMsg2LOG(int nLOG, US MsgId, const CH *msg);
void SendMsg2LOG(int nLOG, US MsgId, const CStringX &msg);
int Proc_Msg_From_LOG(CXMLRcvMsg &LOGMsg);
void Get_LOGMSG_HeaderValue(CXMLRcvMsg &LOGMsg);

void Set_IVR2LOG_Header(int MsgId);
void Set_IVR2LOG_Header(int MsgId, int nLOG);
void Set_IVR2LOG_Item(int MsgId, int AttrId, const char *value);
void Set_IVR2LOG_Item(int MsgId, int AttrId, const CStringX &value);
void Set_IVR2LOG_IntItem(int MsgId, int AttrId, int value);
void Set_IVR2LOG_LongItem(int MsgId, int AttrId, long value);
void Set_IVR2LOG_BoolItem(int MsgId, int AttrId, bool value);
void Set_IVR2LOG_Tail();

void SendCommonResultToLOG(int nLOG, int onMsgId, int result, const char *error);
void SendMonitorONOFFStatus(int nLOG);

void SendINIParamToLOG();
void SendTrkCountData(int nCountTimeId, const char *pszCountData);
void SendAgCountData(int nCountTimeId, const char *pszCountData);

void SendAllAGStateToLOG(int nLOG);
void SendAllCHStateToLOG(int nLOG);
void SendVopStateToAll(int nVopNo);
void SendVopStateToAll(int nVopNo, int nVopChnNo);
void SendVopStateToLOG(int nVopNo, int nVopChnNo, int nLOG);
void SendAllVOPStateToLOG(int nLOG);

void SendVOPStateToOppIVR(int nVopNo, int nVopChnNo);
void SendaLLVOPStateToOppIVR();

void SendQueryTrunkGroupToLOG(int nLOG, const char *trunkgroup, int idletrunks, int usedtrunks, int blocktrunks);
void SendQueryTrunkGroupToAllLOG(const char *trunkgroup, int idletrunks, int usedtrunks, int blocktrunks);

void SendHARunStatusToLOG(int nLOG, int nHostType, int nActiveId, const char *pszSwitchTime, const char *pszReason);
void SendHARunStatusToAllLog(int nHostType, int nActiveId, const char *pszSwitchTime, const char *pszReason);

int Proc_LOGMSG_login(CXMLRcvMsg &LOGMsg);	  //登錄
int Proc_LOGMSG_logout(CXMLRcvMsg &LOGMsg);	  //登出

int Proc_LOGMSG_getqueueinfo(CXMLRcvMsg &LOGMsg);	  //取ACD分配隊列信息
int Proc_LOGMSG_getagentstatus(CXMLRcvMsg &LOGMsg);	  //取所有坐席的狀態信息
int Proc_LOGMSG_getchnstatus(CXMLRcvMsg &LOGMsg);	  //取所有通道的狀態信息
int Proc_LOGMSG_getivrtracemsg(CXMLRcvMsg &LOGMsg);	  //取ivr日志跟蹤信息
int Proc_LOGMSG_getloadedflws(CXMLRcvMsg &LOGMsg);	  //取已加載的流程信息
int Proc_LOGMSG_getflwtracemsg(CXMLRcvMsg &LOGMsg);	  //取flw日志跟蹤信息

int Proc_LOGMSG_releasechn(CXMLRcvMsg &LOGMsg);	  //強行釋放通道
int Proc_LOGMSG_forceclear(CXMLRcvMsg &LOGMsg);	  //強拆其他坐席的電話
int Proc_LOGMSG_forcelogout(CXMLRcvMsg &LOGMsg);	  //強制將其他話務員退出
int Proc_LOGMSG_forcedisturb(CXMLRcvMsg &LOGMsg);	  //強制其他坐席免打擾
int Proc_LOGMSG_forceready(CXMLRcvMsg &LOGMsg);	//強制其他坐席空閑

int Proc_LOGMSG_modifyroutedata(CXMLRcvMsg &LOGMsg);	//修改路由參數
int Proc_LOGMSG_modifyseatno(CXMLRcvMsg &LOGMsg);	//修改坐席分機號
int Proc_LOGMSG_modifyextline(CXMLRcvMsg &LOGMsg);	//修改模擬外線接入號碼參數

int Proc_LOGMSG_loadflw(CXMLRcvMsg &LOGMsg);	//加載業務流程
int Proc_LOGMSG_addaccesscode(CXMLRcvMsg &LOGMsg);	//添加接入號碼
int Proc_LOGMSG_delaccesscode(CXMLRcvMsg &LOGMsg);	//刪除接入號碼

int Proc_LOGMSG_setivrsavemsgid(CXMLRcvMsg &LOGMsg);	//設置IVR服務自動保存日志標志
int Proc_LOGMSG_setflwsavemsgid(CXMLRcvMsg &LOGMsg);	//設置FLW服務自動保存日志標志

int Proc_LOGMSG_modifyiniparam(CXMLRcvMsg &LOGMsg);	//修改INI配置文件參數
int Proc_LOGMSG_modifyseatdata(CXMLRcvMsg &LOGMsg);	//修改坐席參數

int Proc_LOGMSG_gettrkcallcount(CXMLRcvMsg &LOGMsg);	//取中繼實時話務統計數據
int Proc_LOGMSG_getagcallcount(CXMLRcvMsg &LOGMsg);	//取坐席實時話務統計數據

int Proc_LOGMSG_autoflwonoff(CXMLRcvMsg &LOGMsg);	//

int Proc_LOGMSG_modifypassword(CXMLRcvMsg &LOGMsg);	//修改登錄密碼

int Proc_LOGMSG_modifyctilinkdata(CXMLRcvMsg &LOGMsg);	//修改CTILINK參數

void ProcLOGMsg(CXMLRcvMsg &LOGMsg); //處理監控發來的消息

void CheckLOGTraceMsgId();

//---------------------------------------------------------------------------
//備機發送狀態到主機
void SendMsgToMasterIVR(int nMasterStatus, int nStandbyStatus, const char *pszReason);
//主機發送狀態到備機
void SendMsgToStandbyIVR(int nMasterStatus, int nStandbyStatus, const char *pszReason);
//發送運行切換指令到各客戶端節點
void SendSwitchoverToAllClient(int nReason, const char *pszReason);
//發送運行狀態到某客戶端
void SendRunStatusToClient(unsigned long usClientId, int nMsgId);
//發送運行狀態到websocket客戶端
void SendRunStatusToWSClient(const char *pszSeatNo);
//---------------------------------------------------------------------------
#endif
