//---------------------------------------------------------------------------
/*
坐席類定義
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "boxag.h"
#include "boxchn.h"

extern CBoxchn    *pBoxchn; //通道資源
//---------------------------------------------------------------------------
CAgentMng::CAgentMng()
{
  int i, j, k;
  MaxUseNum = 0;
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    m_Agent[i] = NULL;
    m_ClientId[i] = 0;
  }

  for (i = 0; i < MAX_AG_GROUP; i ++)
  {
    lastAllocnAG[i] = -1;
    lastAnsnAG[i] = -1;
    for (j = 0; j < MAX_GROUP_NUM_FORWORKER; j ++)
    {
      lastAllocGroupIndexnAG[i][j] = -1;
      lastAnsGroupIndexnAG[i][j] = -1;
    }
    for (j = 0; j < MAX_AG_LEVEL; j ++)
    {
      lastAllocLevelnAG[i][j] = -1;
      lastAnsLevelnAG[i][j] = -1;
      for (k = 0; k < MAX_GROUP_NUM_FORWORKER; k ++)
      {
        lastAllocLevelGroupIndexnAG[i][j][k] = -1;
        lastAnsLevelGroupIndexnAG[i][j][k] = -1;
      }
    }
  }

  AgLoginCount = 0;
  AgLogoutCount = 0;
  AgIdelCount = 0;
  AgBusyCount = 0;
  AgLeavelCount = 0;
  AgTalkCount = 0;
}

CAgentMng::~CAgentMng()
{
  for (int nAG = 0; nAG < MAX_AG_NUM; nAG ++)
  {
    Free_nAG(nAG);
  }
}

void CAgentMng::InitLastnAG()
{
  int i, j, k;
  for (i = 0; i < MAX_AG_GROUP; i ++)
  {
    lastAllocnAG[i] = -1;
    lastAnsnAG[i] = -1;
    for (j = 0; j < MAX_GROUP_NUM_FORWORKER; j ++)
    {
      lastAllocGroupIndexnAG[i][j] = -1;
      lastAnsGroupIndexnAG[i][j] = -1;
    }
    for (j = 0; j < MAX_AG_LEVEL; j ++)
    {
      lastAllocLevelnAG[i][j] = -1;
      lastAnsLevelnAG[i][j] = -1;
      for (k = 0; k < MAX_GROUP_NUM_FORWORKER; k ++)
      {
        lastAllocLevelGroupIndexnAG[i][j][k] = -1;
        lastAnsLevelGroupIndexnAG[i][j][k] = -1;
      }
    }
  }
}

bool CAgentMng::Alloc_nAG(int &nAG)
{
  for (int i = 0; i < MAX_AG_NUM; i ++)
  {
    if (m_Agent[i] == NULL)
    {
      m_Agent[i] = new CAgent;
      m_Agent[i]->nAG = i;
      nAG = i;
      MaxUseNum++;
      return true;
    }
  }
  return false;
}
void CAgentMng::Free_nAG(int nAG)
{
  if (m_Agent[nAG] != NULL)
  {
    delete m_Agent[nAG];
    m_Agent[nAG] = NULL;
  }
}
bool CAgentMng::isnAGAvail(int nAG)
{
  return (nAG >= 0 && nAG < MaxUseNum) ? true : false;
}

void CAgentMng::SetClientId(int nAG, US clientid)
{
  m_ClientId[nAG] = clientid;
}

void CAgentMng::ClearClientId(int nAG)
{
  m_ClientId[nAG] = 0;
  m_Agent[nAG]->m_Seat.SetSeatIP("");
  m_Agent[nAG]->m_Seat.TcpUnLinked();
}

int CAgentMng::GetnAGByClientId(US clientid)
{
  int nAG;

  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_ClientId[nAG] == clientid)
      return nAG;
  }
  return 0xFFFF;
}

bool CAgentMng::isTheClientIdLogin(US clientid)
{
  int nAG;

  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_ClientId[nAG] == clientid)
      return true;
  }
  return false;
}

int CAgentMng::GetnAGByWorkerNo(int workerno)
{
  int nAG;

  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->GetWorkerNo() == workerno)
      return nAG;
  }
  return 0xFFFF;
}
bool CAgentMng::istheWorkerNoExist(int workerno)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->GetWorkerNo() == workerno)
      return true;
  }
  return false;
}
int CAgentMng::GetnAGBySeatNo(const char *seatno)
{
  int nAG;

  if (strlen(seatno) == 0) //2016-11-23
    return 0xFFFF;
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->isWantSeatNo(seatno))
      return nAG;
  }
  return 0xFFFF;
}
int CAgentMng::GetnAGByWSClientId(const char *wsclientid)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (strcmp(m_Agent[nAG]->m_Seat.WSClientId, wsclientid) == 0)
      return nAG;
  }
  return 0xFFFF;
}
int CAgentMng::GetnAGByDutyNo(const char *dutyno)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (strcmp(m_Agent[nAG]->m_Worker.DutyPhone.C_Str(), dutyno) == 0)
      return nAG;
  }
  return 0xFFFF;
}
int CAgentMng::GetnAGByIMId(const char *imid)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (strcmp(m_Agent[nAG]->IMId, imid) == 0)
      return nAG;
  }
  return 0xFFFF;
}
bool CAgentMng::istheSeatNoExist(const char *seatno)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->isWantSeatNo(seatno))
      return true;
  }
  return false;
}
bool CAgentMng::istheBandIPExist(const char *bandip)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->isWantBandIP(bandip))
      return true;
  }
  return false;
}
int CAgentMng::GetnAGByPhoneNo(const char *phoneno)
{
  int nAG;

  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->isWantPhoneNo(phoneno))
      return nAG;
  }
  return 0xFFFF;
}
int CAgentMng::GetnAGByPort(int porttype, int portno)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    switch (porttype)
    {
    case SEATTYPE_AG_TEL:
    case SEATTYPE_AG_PC:
      if ((m_Agent[nAG]->m_Seat.SeatType == SEATTYPE_AG_TEL || m_Agent[nAG]->m_Seat.SeatType == SEATTYPE_AG_PC) 
        && m_Agent[nAG]->m_Seat.PortNo == portno)
        return nAG;
      break;
    case SEATTYPE_DT_TEL:
    case SEATTYPE_DT_PC:
      if ((m_Agent[nAG]->m_Seat.SeatType == SEATTYPE_DT_TEL || m_Agent[nAG]->m_Seat.SeatType == SEATTYPE_DT_PC) 
        && m_Agent[nAG]->m_Seat.PortNo == portno)
        return nAG;
      break;
    case SEATTYPE_RT_TEL:
    case SEATTYPE_RT_VOIP:
      if ((m_Agent[nAG]->m_Seat.SeatType == SEATTYPE_RT_TEL || m_Agent[nAG]->m_Seat.SeatType == SEATTYPE_RT_VOIP)
        && m_Agent[nAG]->m_Seat.PortNo == portno)
        return nAG;
      break;
    }
  }
  return 0xFFFF;
}
char *CAgentMng::GetGroupNoList(int nAG)
{
  char szTemp[32];
  int groupno;
  static char szGroupNoList[256];

  if (!isnAGAvail(nAG))
    return "";
  memset(szGroupNoList, 0, 256);
  for (int i=0; i<MAX_GROUP_NUM_FORWORKER; i++)
  {
    groupno = m_Agent[nAG]->m_Worker.GroupNo[i];
    if (groupno > 0 && groupno < MAX_AG_GROUP)
    {
      sprintf(szTemp, "[%d-%s;Level=%d]", groupno, WorkerGroup[groupno].GroupName.C_Str(),
        m_Agent[nAG]->m_Worker.Level[i]);
      if ((strlen(szGroupNoList) + strlen(szTemp)) < 256)
        strcat(szGroupNoList, szTemp);
    }
  }
  //2015-06-14
  if (m_Agent[nAG]->m_Worker.WorkerGrade == 1 || m_Agent[nAG]->m_Worker.WorkerGrade == 2)
    strcat(szGroupNoList, "[Operator]");
  else if (m_Agent[nAG]->m_Worker.WorkerGrade == 9)
    strcat(szGroupNoList, "[Admin]");
  else if (m_Agent[nAG]->m_Worker.WorkerGrade >= 3)
    strcat(szGroupNoList, "[Leader]");
  return szGroupNoList;
}
void CAgentMng::ResetAllStatusCountData()
{
  AllCallCountParam.Reset();
  AllCallCountParam.ResetStatusCount();
  for (int i=0; i<MAX_AG_GROUP; i++)
  {
    WorkerGroup[i].GroupCallCountParam.Reset();
    WorkerGroup[i].GroupCallCountParam.ResetStatusCount();
  }
  for (int nAG=0; nAG<MaxUseNum; nAG++)
  {
    m_Agent[nAG]->m_Worker.WorkerCallCountParam.Reset();
  }
}
void CAgentMng::ResetAllStatusCount()
{
  AllCallCountParam.ResetStatusCount();
  for (int i=0; i<MAX_AG_GROUP; i++)
  {
    WorkerGroup[i].GroupCallCountParam.ResetStatusCount();
  }
}
int CAgentMng::GetnAGByAccountNo(const char *accountno)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->m_Worker.AccountNo.Compare(accountno) == 0)
      return nAG;
  }
  return 0xFFFF;
}
int CAgentMng::GetnAGByWorkerName(const char *workername)
{
  int nAG;
  
  for (nAG = 0; nAG < MaxUseNum; nAG ++)
  {
    if (m_Agent[nAG]->m_Worker.WorkerName.Compare(workername) == 0)
      return nAG;
  }
  return 0xFFFF;
}
int CAgentMng::CountLoginAgentByGroupNo(UC groupno)
{
  int count=0;
  for (int nAG = 0; nAG < MAX_AG_NUM; nAG ++)
  {
    if (m_Agent[nAG])
    {
      if (m_Agent[nAG]->m_Worker.isWantGroup(groupno, 0, 0) && m_Agent[nAG]->m_Worker.isLogin())
        count ++;
    }
  }
  return count;
}
int CAgentMng::CountIdleAgentByGroupNo(UC groupno)
{
  int count=0;
  for (int nAG = 0; nAG < MAX_AG_NUM; nAG ++)
  {
    if (m_Agent[nAG])
    {
      if (m_Agent[nAG]->m_Worker.isWantGroup(groupno, 0, 0) && m_Agent[nAG]->m_Worker.isLogin() && m_Agent[nAG]->isIdelForACD())
        count ++;
    }
  }
  return count;
}
//2016-05-10增加取坐席狀態統計數時通話數
int CAgentMng::CountTalkAgentByGroupNo(UC groupno)
{
  int count=0;
  for (int nAG = 0; nAG < MAX_AG_NUM; nAG ++)
  {
    if (m_Agent[nAG])
    {
      if (m_Agent[nAG]->m_Worker.isWantGroup(groupno, 0, 0) && 
        (m_Agent[nAG]->svState == AG_SV_CONN || m_Agent[nAG]->svState == AG_SV_OUTSEIZE || m_Agent[nAG]->svState == AG_SV_OUTRING || m_Agent[nAG]->svState == AG_SV_ACW))
        count ++;
    }
  }
  return count;
}
int CAgentMng::CountLoginAgent(UC *pGroupNoList)
{
  int groupcount=0;
  int totallogin=0, logincount=0;
  for (int i=0; i<MAX_GROUP_NUM_FORWORKER; i++)
  {
    if (pGroupNoList[i] > 0)
    {
      logincount = CountLoginAgentByGroupNo(pGroupNoList[i]);
      totallogin += logincount;
      groupcount++;
    }
  }
  if (groupcount == 0)
    totallogin = CountLoginAgentByGroupNo(0);
  return totallogin;
}
void CAgentMng::CountLoginIdleAgent(UC *pGroupNoList, int &logins, int &idles, int &talks)
{
  int groupcount=0;
  int logincount=0, idlecount=0, talkcount=0;
  logins = 0;
  idles = 0;
  talks = 0;
  for (int i=0; i<MAX_GROUP_NUM_FORWORKER; i++)
  {
    if (pGroupNoList[i] > 0 && pGroupNoList[i] < MAX_AG_GROUP)
    {
      logincount = CountLoginAgentByGroupNo(pGroupNoList[i]);
      logins += logincount;
      idlecount = CountIdleAgentByGroupNo(pGroupNoList[i]);
      idles += idlecount;
      talkcount = CountTalkAgentByGroupNo(pGroupNoList[i]);
      talks += talkcount;
      groupcount++;
    }
  }
  if (groupcount == 0)
  {
    logins = CountLoginAgentByGroupNo(0);
    idles = CountIdleAgentByGroupNo(0);
    talks = CountTalkAgentByGroupNo(0);
  }
}
//-----------------------------------------------------------------------------
void CAgentCallCountParam::Reset()
{
  LogoutTimeSumLen = 0;
  LoginTimeSumLen = 0; //累計登錄時長
  BusyCount = 0;
  BusyTimeSumLen = 0; //累計未就緒(即示忙)時長
  BusyTimeAvgLen = 0;
  LeaveCount = 0;
  LeaveTimeSumLen = 0; //累計離席時長
  LeaveTimeAvgLen = 0;
  IdelTimeSumLen = 0; //累計等待來話(即示閑)時長
  
  CallInCount = 0; //來話分配總次數
  CallInAnsCount = 0; //來話應答總次數
  CallInNoAnsCount = 0; //來話未應答總次數
  CallInAbandonCount = 0; //來話放棄總次數
  
  CallInWaitTimeSumLen = 0; //呼入累計等待時長
  CallInWaitTimeMaxLen = 0; //呼入最長等待時長
  CallInWaitTimeMinLen = 0; //呼入最短等待時長
  CallInWaitTimeAvgLen = 0; //呼入平均等待時長
  CallInWaitOverTimeCount = 0; //呼入應答超時總次數
  
  CallInAbandonTimeSumLen = 0; //呼出累計放棄前等待時長
  CallInAbandonTimeMaxLen = 0; //呼出放棄前最長等待時長
  CallInAbandonTimeMinLen = 0; //呼出放棄前最短等待時長
  CallInAbandonTimeAvgLen = 0; //呼出放棄前平均等待時長

  CallInTalkTimeLen = 0; //呼入通話時長
  CallInTalkTimeSumLen = 0; //呼入累計通話時長
  CallInTalkTimeMaxLen = 0; //呼入最長通話時長
  CallInTalkTimeMinLen = 0; //呼入最短通話時長
  CallInTalkTimeAvgLen = 0; //呼入平均通話時長
  CallInTalkOverTimeCount = 0;
  
  CallInACWTimeLen = 0; //呼入話后處理時長
  CallInACWTimeSumLen = 0; //呼入累計話后處理時長
  CallInACWTimeMaxLen = 0; //呼入最長話后處理時長
  CallInACWTimeMinLen = 0; //呼入最短話后處理時長
  CallInACWTimeAvgLen = 0; //呼入平均話后處理時長
  CallInACWOverTimeCount = 0;
  
  CallInHandleTimeLen = 0; //呼入話務處理時長
  CallInHandleTimeSumLen = 0; //呼入累計話務處理時長
  CallInHandleTimeMaxLen = 0; //呼入最長話務處理時長
  CallInHandleTimeMinLen = 0; //呼入最短話務處理時長
  CallInHandleTimeAvgLen = 0; //呼入平均話務處理時長
  
  CallInAbandonRate = 0; //來話放棄率
  CallInAnswerRate = 0; //來話接通率
  
  CallOutCount = 0; //呼出總次數
  CallOutAnsCount = 0; //呼出應答總次數
  CallOutNoAnsCount = 0; //呼出未應答總次數
  
  CallOutWaitTimeLen = 0;
  CallOutWaitTimeSumLen = 0; //呼出累計等待時長
  CallOutWaitTimeMaxLen = 0; //呼出最長等待時長
  CallOutWaitTimeMinLen = 0; //呼出最短等待時長
  CallOutWaitTimeAvgLen = 0; //呼出平均等待時長
  CallOutWaitOverTimeCount = 0; //呼出應答超時總次數
  
  CallOutTalkTimeLen = 0; //呼出通話時長
  CallOutTalkTimeSumLen = 0; //呼出累計通話時長
  CallOutTalkTimeMaxLen = 0; //呼出最長通話時長
  CallOutTalkTimeMinLen = 0; //呼出最短通話時長
  CallOutTalkTimeAvgLen = 0; //呼出平均通話時長
  CallOutTalkOverTimeCount = 0;
  
  CallOutACWTimeLen = 0; //呼出話后處理時長
  CallOutACWTimeSumLen = 0; //呼出累計話后處理時長
  CallOutACWTimeMaxLen = 0; //呼出最長話后處理時長
  CallOutACWTimeMinLen = 0; //呼出最短話后處理時長
  CallOutACWTimeAvgLen = 0; //呼出平均話后處理時長
  CallOutACWOverTimeCount = 0;
  
  CallOutHandleTimeLen = 0; //呼出話務處理時長
  CallOutHandleTimeSumLen = 0; //呼出累計話務處理時長
  CallOutHandleTimeMaxLen = 0; //呼出最長話務處理時長
  CallOutHandleTimeMinLen = 0; //呼出最短話務處理時長
  CallOutHandleTimeAvgLen = 0; //呼出平均話務處理時長
  
  CallOutFailRate = 0; //呼出失敗率
  CallOutAnswerRate = 0; //呼出接通率
  
  CallTalkOverTimeCount = 0; //通話超時總次數
  CallTalkTimeSumLen = 0; //累計通話總時長
  CallAnsCount = 0;
  
  CallACWOverTimeCount = 0;
  CallACWTimeSumLen = 0;
  
  CallHandleTimeSumLen = 0; //累計話務處理時長
  CallHandleTimeMaxLen = 0; //最長話務處理時長
  CallHandleTimeMinLen = 0; //最短話務處理時長
  CallHandleTimeAvgLen = 0; //平均話務處理時長
  
  HoldCallCount = 0; //保持總次數
  HoldCallTimeSumLen = 0; //累計保持時長
  HoldCallTimeMaxLen = 0; //最長保持時長
  HoldCallTimeMinLen = 0; //最短保持時長
  HoldCallTimeAvgLen = 0; //平均保持時長
  
  TranCallCount = 0; //轉接總次數
  
  CallInOut = 0; //呼入呼出標志
  RingID = 0;
  
  CurStatus = 0; //當前狀態
  OldStatus = 0;
  OldStatusStartTime = time(0);
  OldStatusDuration = 0;
  
  LoginTime = 0; //登錄時間
  MyGetGUID(LoginGID);
  memset(CRDSerialNo, 0, MAX_CHAR64_LEN);
  
  LogoutTime = 0;
  MyGetGUID(LogoutGID);

  StartTalkTime = 0;
  CallConnectedTimeLen = 0;
  CallHandleTimeLen = 0;
  
  CurStatusStartTime = time(0); //當前狀態開始時間
  CurStatusEndTime = time(0); //當前狀態結束時間
}
void CAgentCallCountParam::Init()
{
  CallInWaitOverTimeLen = 0; //呼入超時應答時長
  CallOutWaitOverTimeLen = 0; //呼出超時應答時長
  CallTalkOverTimeLen = 0; //通話超時時長
  CallACWOverTimeLen = 0; //話后處理超時時長

  LogoutTimeSumLen = 0;

  LoginTimeSumLen = 0; //累計登錄時長
  BusyCount = 0;
  BusyTimeSumLen = 0; //累計未就緒(即示忙)時長
  BusyTimeAvgLen = 0;
  LeaveCount = 0;
  LeaveTimeSumLen = 0; //累計離席時長
  LeaveTimeAvgLen = 0;
  IdelTimeSumLen = 0; //累計等待來話(即示閑)時長
  
  CallInCount = 0; //來話分配總次數
  CallInAnsCount = 0; //來話應答總次數
  CallInNoAnsCount = 0; //來話未應答總次數
  CallInAbandonCount = 0; //來話放棄總次數
  
  CallInWaitTimeSumLen = 0; //呼入累計等待時長
  CallInWaitTimeMaxLen = 0; //呼入最長等待時長
  CallInWaitTimeMinLen = 0; //呼入最短等待時長
  CallInWaitTimeAvgLen = 0; //呼入平均等待時長
  CallInWaitOverTimeCount = 0; //呼入應答超時總次數
  
  CallInAbandonTimeSumLen = 0; //呼出累計放棄前等待時長
  CallInAbandonTimeMaxLen = 0; //呼出放棄前最長等待時長
  CallInAbandonTimeMinLen = 0; //呼出放棄前最短等待時長
  CallInAbandonTimeAvgLen = 0; //呼出放棄前平均等待時長

  CallInTalkTimeLen = 0; //呼入通話時長
  CallInTalkTimeSumLen = 0; //呼入累計通話時長
  CallInTalkTimeMaxLen = 0; //呼入最長通話時長
  CallInTalkTimeMinLen = 0; //呼入最短通話時長
  CallInTalkTimeAvgLen = 0; //呼入平均通話時長
  CallInTalkOverTimeCount = 0;
  
  CallInACWTimeLen = 0; //呼入話后處理時長
  CallInACWTimeSumLen = 0; //呼入累計話后處理時長
  CallInACWTimeMaxLen = 0; //呼入最長話后處理時長
  CallInACWTimeMinLen = 0; //呼入最短話后處理時長
  CallInACWTimeAvgLen = 0; //呼入平均話后處理時長
  CallInACWOverTimeCount = 0;
  
  CallInHandleTimeLen = 0; //呼入話務處理時長
  CallInHandleTimeSumLen = 0; //呼入累計話務處理時長
  CallInHandleTimeMaxLen = 0; //呼入最長話務處理時長
  CallInHandleTimeMinLen = 0; //呼入最短話務處理時長
  CallInHandleTimeAvgLen = 0; //呼入平均話務處理時長
  
  CallInAbandonRate = 0; //來話放棄率
  CallInAnswerRate = 0; //來話接通率
  
  CallOutCount = 0; //呼出總次數
  CallOutAnsCount = 0; //呼出應答總次數
  CallOutNoAnsCount = 0; //呼出未應答總次數
  
  CallOutWaitTimeLen = 0;
  CallOutWaitTimeSumLen = 0; //呼出累計等待時長
  CallOutWaitTimeMaxLen = 0; //呼出最長等待時長
  CallOutWaitTimeMinLen = 0; //呼出最短等待時長
  CallOutWaitTimeAvgLen = 0; //呼出平均等待時長
  CallOutWaitOverTimeCount = 0; //呼出應答超時總次數
  
  CallOutTalkTimeLen = 0; //呼出通話時長
  CallOutTalkTimeSumLen = 0; //呼出累計通話時長
  CallOutTalkTimeMaxLen = 0; //呼出最長通話時長
  CallOutTalkTimeMinLen = 0; //呼出最短通話時長
  CallOutTalkTimeAvgLen = 0; //呼出平均通話時長
  CallOutTalkOverTimeCount = 0;
  
  CallOutACWTimeLen = 0; //呼出話后處理時長
  CallOutACWTimeSumLen = 0; //呼出累計話后處理時長
  CallOutACWTimeMaxLen = 0; //呼出最長話后處理時長
  CallOutACWTimeMinLen = 0; //呼出最短話后處理時長
  CallOutACWTimeAvgLen = 0; //呼出平均話后處理時長
  CallOutACWOverTimeCount = 0;
  
  CallOutHandleTimeLen = 0; //呼出話務處理時長
  CallOutHandleTimeSumLen = 0; //呼出累計話務處理時長
  CallOutHandleTimeMaxLen = 0; //呼出最長話務處理時長
  CallOutHandleTimeMinLen = 0; //呼出最短話務處理時長
  CallOutHandleTimeAvgLen = 0; //呼出平均話務處理時長
  
  CallOutFailRate = 0; //呼出失敗率
  CallOutAnswerRate = 0; //呼出接通率
  
  CallTalkOverTimeCount = 0; //通話超時總次數
  CallTalkTimeSumLen = 0; //累計通話總時長
  CallAnsCount = 0;
  
  CallACWOverTimeCount = 0;
  CallACWTimeSumLen = 0;
  
  CallHandleTimeSumLen = 0; //累計話務處理時長
  CallHandleTimeMaxLen = 0; //最長話務處理時長
  CallHandleTimeMinLen = 0; //最短話務處理時長
  CallHandleTimeAvgLen = 0; //平均話務處理時長
  
  HoldCallCount = 0; //保持總次數
  HoldCallTimeSumLen = 0; //累計保持時長
  HoldCallTimeMaxLen = 0; //最長保持時長
  HoldCallTimeMinLen = 0; //最短保持時長
  HoldCallTimeAvgLen = 0; //平均保持時長
  
  TranCallCount = 0; //轉接總次數
  
  CallInOut = 0; //呼入呼出標志
  RingID = 0;
  
  CurStatus = 0; //當前狀態
  OldStatus = 0;
  OldStatusStartTime = time(0);
  OldStatusDuration = 0;
  
  LoginTime = 0; //登錄時間
  MyGetGUID(LoginGID);
  memset(CRDSerialNo, 0, MAX_CHAR64_LEN);

  LogoutTime = 0;
  MyGetGUID(LogoutGID);

  StartTalkTime = 0;
  CallConnectedTimeLen = 0;
  CallHandleTimeLen = 0;
  
  CurStatusStartTime = time(0); //當前狀態開始時間
  CurStatusEndTime = time(0); //當前狀態結束時間
}
CAgentCallCountParam::CAgentCallCountParam()
{
  Init();
}
CAgentCallCountParam::~CAgentCallCountParam()
{

}
void CAgentCallCountParam::SetCDRSerialNo(const char *pszSerialNo)
{
  strncpy(CRDSerialNo, pszSerialNo, MAX_CHAR64_LEN);
}
int CAgentCallCountParam::ChangeStatus(int nStatus)
{
  //累計之前狀態時長
  CurStatusEndTime = time(0);
  int nTimeLen = CurStatusEndTime - CurStatusStartTime;
  switch (CurStatus)
  {
  case AG_STATUS_LOGOUT: //未登錄
    LogoutTimeSumLen += nTimeLen;
    break;
  case AG_STATUS_LOGIN: //登錄
    LoginTimeSumLen += nTimeLen;
    break;
  case AG_STATUS_BUSY: //示忙態
    BusyTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    break;
  case AG_STATUS_IDEL: //空閑態
    IdelTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    if (RingID == 1)
    {
      CallInNoAnsCount = CallInCount-CallInAnsCount;
    } 
    else if (RingID == 2)
    {
      CallOutNoAnsCount = CallOutCount-CallOutAnsCount;
    }
    RingID = 0;
    break;
  case AG_STATUS_INRING: //呼入振鈴
    CallInOut = 1;
    RingID = 1;
    CallInWaitTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    if (CallInWaitTimeMaxLen == 0 || CallInWaitTimeMaxLen < nTimeLen)
    {
      CallInWaitTimeMaxLen = nTimeLen;
    }
    if (CallInWaitTimeMinLen == 0 || CallInWaitTimeMinLen > nTimeLen)
    {
      CallInWaitTimeMinLen = nTimeLen;
    }
    if (CallInCount > 0)
    {
      CallInWaitTimeAvgLen = CallInWaitTimeSumLen/CallInCount;
    }
    if (nTimeLen > CallInWaitOverTimeLen && CallInWaitOverTimeLen > 0)
    {
      CallInWaitOverTimeCount++;
    }
    break;
  case AG_STATUS_OUTSEIZE: //呼出占用
    CallOutWaitTimeLen += nTimeLen;
    CallOutWaitTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    break;
  case AG_STATUS_OUTRING: //呼出振鈴
    CallOutWaitTimeLen += nTimeLen;
    CallInOut = 2;
    RingID = 2;
    CallOutWaitTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    if (CallOutWaitTimeMaxLen == 0 || CallOutWaitTimeMaxLen < CallOutWaitTimeLen)
    {
      CallOutWaitTimeMaxLen = CallOutWaitTimeLen;
    }
    if (CallOutWaitTimeMinLen == 0 || CallOutWaitTimeMinLen > CallOutWaitTimeLen)
    {
      CallOutWaitTimeMinLen = CallOutWaitTimeLen;
    }
    if (CallOutCount > 0)
    {
      CallOutWaitTimeAvgLen = CallOutWaitTimeSumLen/CallOutCount;
    }
    if (nTimeLen > CallOutWaitOverTimeLen && CallOutWaitOverTimeLen > 0)
    {
      CallOutWaitOverTimeCount++;
    }
    break;
  case AG_STATUS_HOLD: //保持狀態
    if (StartTalkTime != 0)
    {
      CallConnectedTimeLen += nTimeLen;
      CallHandleTimeLen += nTimeLen;
    }
    
    HoldCallTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    HoldCallCount++;
    if (HoldCallTimeMaxLen == 0 || HoldCallTimeMaxLen < nTimeLen)
    {
      HoldCallTimeMaxLen = nTimeLen;
    }
    if (HoldCallTimeMinLen == 0 || HoldCallTimeMinLen > nTimeLen)
    {
      HoldCallTimeMinLen = nTimeLen;
    }
    if (HoldCallCount > 0)
    {
      HoldCallTimeAvgLen = HoldCallTimeSumLen/HoldCallCount;
    }
    
    CallTalkTimeSumLen += nTimeLen;
    CallHandleTimeSumLen += nTimeLen;
    if (CallInOut == 1)
    {
      CallInTalkTimeSumLen += nTimeLen;
      CallInHandleTimeSumLen += nTimeLen;
    }
    else
    {
      CallOutTalkTimeSumLen += nTimeLen;
      CallOutHandleTimeSumLen += nTimeLen;
    }
    break;
  case AG_STATUS_TALK: //通話狀態
    if (StartTalkTime == 0)
    {
      StartTalkTime = CurStatusStartTime;
      CallConnectedTimeLen = 0;
      CallHandleTimeLen = 0;
    }
    CallConnectedTimeLen += nTimeLen;
    CallHandleTimeLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;

    RingID = 0;
    CallOutWaitTimeLen = 0;

    CallTalkTimeSumLen += nTimeLen;
    CallHandleTimeSumLen += nTimeLen;
    if (CallInOut == 1)
    {
      CallInTalkTimeSumLen += nTimeLen;
      CallInHandleTimeSumLen += nTimeLen;
    }
    else
    {
      CallOutTalkTimeSumLen += nTimeLen;
      CallOutHandleTimeSumLen += nTimeLen;
    }
    break;
  case AG_STATUS_ACW: //話后處理
    if (CallHandleTimeLen > 0)
    {
      CallHandleTimeLen += nTimeLen;
    }
    LoginTimeSumLen += nTimeLen;

    CallHandleTimeSumLen += nTimeLen;
    if (CallInOut == 1)
    {
      CallInHandleTimeSumLen += nTimeLen;
      CallInACWTimeSumLen += nTimeLen;
      if (CallInACWTimeMaxLen == 0 || CallInACWTimeMaxLen < nTimeLen)
      {
        CallInACWTimeMaxLen = nTimeLen;
      }
      if (CallInACWTimeMinLen == 0 || CallInACWTimeMinLen > nTimeLen)
      {
        CallInACWTimeMinLen = nTimeLen;
      }
      if (CallInAnsCount > 0)
      {
        CallInACWTimeAvgLen = CallInACWTimeSumLen/CallInAnsCount;
      }
      if (nTimeLen > CallACWOverTimeLen && CallACWOverTimeLen > 0)
      {
        CallInACWOverTimeCount++;
        CallACWOverTimeCount++;
      }
    }
    else
    {
      CallOutHandleTimeSumLen += nTimeLen;
      CallOutACWTimeSumLen += nTimeLen;
      if (CallOutACWTimeMaxLen == 0 || CallOutACWTimeMaxLen < nTimeLen)
      {
        CallOutACWTimeMaxLen = nTimeLen;
      }
      if (CallOutACWTimeMinLen == 0 || CallOutACWTimeMinLen > nTimeLen)
      {
        CallOutACWTimeMinLen = nTimeLen;
      }
      if (CallOutAnsCount > 0)
      {
        CallOutACWTimeAvgLen = CallOutACWTimeSumLen/CallOutAnsCount;
      }
      if (nTimeLen > CallACWOverTimeLen && CallACWOverTimeLen > 0)
      {
        CallOutACWOverTimeCount++;
        CallACWOverTimeCount++;
      }
    }
    break;
  case AG_STATUS_LEAVE: //離席小休態
    LeaveTimeSumLen += nTimeLen;
    LoginTimeSumLen += nTimeLen;
    break;
  case AG_STATUS_CONSULT: //咨詢狀態
    if (StartTalkTime != 0)
    {
      CallConnectedTimeLen += nTimeLen;
      CallHandleTimeLen += nTimeLen;
    }
    
    LoginTimeSumLen += nTimeLen;
    
    CallTalkTimeSumLen += nTimeLen;
    CallHandleTimeSumLen += nTimeLen;
    if (CallInOut == 1)
    {
      CallInTalkTimeSumLen += nTimeLen;
      CallInHandleTimeSumLen += nTimeLen;
    }
    else
    {
      CallOutTalkTimeSumLen += nTimeLen;
      CallOutHandleTimeSumLen += nTimeLen;
    }
    break;
  case AG_STATUS_CONF: //會議狀態
    if (StartTalkTime != 0)
    {
      CallConnectedTimeLen += nTimeLen;
      CallHandleTimeLen += nTimeLen;
    }
    
    LoginTimeSumLen += nTimeLen;
    
    CallTalkTimeSumLen += nTimeLen;
    CallHandleTimeSumLen += nTimeLen;
    if (CallInOut == 1)
    {
      CallInTalkTimeSumLen += nTimeLen;
      CallInHandleTimeSumLen += nTimeLen;
    }
    else
    {
      CallOutTalkTimeSumLen += nTimeLen;
      CallOutHandleTimeSumLen += nTimeLen;
    }
    break;
  }

  if (nStatus == AG_STATUS_ACW)
  {
    if (CallConnectedTimeLen > 0)
    {
      if (CallInOut == 1)
      {
        if (CallInTalkTimeMaxLen == 0 || CallInTalkTimeMaxLen < CallConnectedTimeLen)
        {
          CallInTalkTimeMaxLen = CallConnectedTimeLen;
        }
        if (CallInTalkTimeMinLen == 0 || CallInTalkTimeMinLen > CallConnectedTimeLen)
        {
          CallInTalkTimeMinLen = CallConnectedTimeLen;
        }
        if (CallInAnsCount > 0)
        {
          CallInTalkTimeAvgLen = CallInTalkTimeSumLen/CallInAnsCount;
        }
        if (CallConnectedTimeLen > CallTalkOverTimeLen && CallTalkOverTimeLen > 0)
        {
          CallInTalkOverTimeCount++;
          CallTalkOverTimeCount++;
        }
      }
      else
      {
        if (CallOutTalkTimeMaxLen == 0 || CallOutTalkTimeMaxLen < CallConnectedTimeLen)
        {
          CallOutTalkTimeMaxLen = CallConnectedTimeLen;
        }
        if (CallOutTalkTimeMinLen == 0 || CallOutTalkTimeMinLen > CallConnectedTimeLen)
        {
          CallOutTalkTimeMinLen = CallConnectedTimeLen;
        }
        if (CallOutAnsCount > 0)
        {
          CallOutTalkTimeAvgLen = CallOutTalkTimeSumLen/CallOutAnsCount;
        }
        if (CallConnectedTimeLen > CallTalkOverTimeLen && CallTalkOverTimeLen > 0)
        {
          CallInTalkOverTimeCount++;
          CallTalkOverTimeCount++;
        }
      }
    }
  }
  else if (nStatus == AG_STATUS_IDEL)
  {
    if (CallHandleTimeLen > 0)
    {
      if (CallHandleTimeMaxLen == 0 || CallHandleTimeMaxLen < CallHandleTimeLen)
      {
        CallHandleTimeMaxLen = CallHandleTimeLen;
      }
      if (CallHandleTimeMinLen == 0 || CallHandleTimeMinLen > CallHandleTimeLen)
      {
        CallHandleTimeMinLen = CallHandleTimeLen;
      }
      if ((CallInAnsCount+CallOutAnsCount) > 0)
      {
        CallHandleTimeAvgLen = CallHandleTimeSumLen/(CallInAnsCount+CallOutAnsCount);
      }

      if (CallInOut == 1)
      {
        if (CallInHandleTimeMaxLen == 0 || CallInHandleTimeMaxLen < CallHandleTimeLen)
        {
          CallInHandleTimeMaxLen = CallHandleTimeLen;
        }
        if (CallInHandleTimeMinLen == 0 || CallInHandleTimeMinLen > CallHandleTimeLen)
        {
          CallInHandleTimeMinLen = CallHandleTimeLen;
        }
        if (CallInAnsCount > 0)
        {
          CallInHandleTimeAvgLen = CallInHandleTimeSumLen/CallInAnsCount;
        }
      }
      else
      {
        if (CallOutHandleTimeMaxLen == 0 || CallOutHandleTimeMaxLen < CallHandleTimeLen)
        {
          CallOutHandleTimeMaxLen = CallHandleTimeLen;
        }
        if (CallOutHandleTimeMinLen == 0 || CallOutHandleTimeMinLen > CallHandleTimeLen)
        {
          CallOutHandleTimeMinLen = CallHandleTimeLen;
        }
        if (CallOutAnsCount > 0)
        {
          CallOutHandleTimeAvgLen = CallOutHandleTimeSumLen/CallOutAnsCount;
        }
      }
    }

    if (RingID == 1)
    {
      CallInNoAnsCount = CallInCount-CallInAnsCount;
      if (CallInCount > 0)
        CallInAnswerRate = CallInAnsCount*100/CallInCount;
    } 
    else if (RingID == 2)
    {
      CallOutNoAnsCount = CallOutCount-CallOutAnsCount;
      if (CallOutCount > 0)
        CallOutAnswerRate = CallOutAnsCount*100/CallOutCount;
    }
    RingID = 0;
    CallOutWaitTimeLen = 0;
    if (BusyCount > 0)
      BusyTimeAvgLen = BusyTimeSumLen/BusyCount;
    if (LeaveCount > 0)
      LeaveTimeAvgLen = LeaveTimeSumLen/LeaveCount;
  }
  //設置之前狀態
  OldStatusStartTime = CurStatusStartTime;
  OldStatusDuration = nTimeLen;
  OldStatus = CurStatus;

  //設置當前狀態
  CurStatusStartTime = CurStatusEndTime;
  CurStatus = nStatus;

  if (CurStatus == AG_STATUS_LOGIN)
  {
    MyGetGUID(LoginGID);
    LoginTime = CurStatusStartTime;
  }
  else if (CurStatus == AG_STATUS_LOGOUT)
  {
    MyGetGUID(LogoutGID);
    LogoutTime = CurStatusStartTime;
  }
  return OldStatus;
}
//累計來話次數
void CAgentCallCountParam::IncCallInCount()
{
  CallInCount++;
}
//累計放棄來話次數
void CAgentCallCountParam::IncCallInAbandonCount()
{
  CallInAbandonCount++;
  if (CallInCount < CallInAbandonCount)
  {
    CallInCount = CallInAbandonCount;
  }
  if (CallInCount > 0)
  {
    CallInAbandonRate = CallInAbandonCount*100/CallInCount;
  }
  
  CurStatusEndTime = time(0);
  int nTimeLen = CurStatusEndTime - CurStatusStartTime;
  CallInAbandonTimeSumLen += nTimeLen;
  if (CallInAbandonTimeMaxLen == 0 || CallInAbandonTimeMaxLen < nTimeLen)
  {
    CallInAbandonTimeMaxLen = nTimeLen;
  }
  if (CallInAbandonTimeMinLen == 0 || CallInAbandonTimeMinLen > nTimeLen)
  {
    CallInAbandonTimeMinLen = nTimeLen;
  }
  if (CallInAbandonCount > 0)
  {
    CallInAbandonTimeAvgLen = CallInAbandonTimeSumLen/CallInAbandonCount;
  }
  CallInNoAnsCount = CallInCount-CallInAnsCount;
  //2014-11-23修改實時話務統計數據
  if (CallInCount > 0)
  {
    CallInAnswerRate = CallInAnsCount*100/CallInCount;
  }
}
//累計來話應答次數
void CAgentCallCountParam::IncCallInAnsCount()
{
  CallInAnsCount++;
  CallAnsCount++;
  if (CallInCount < CallInAnsCount)
  {
    CallInCount = CallInAnsCount;
  }
  if (CallInCount > 0)
  {
    CallInAnswerRate = CallInAnsCount*100/CallInCount;
  }
  //2014-11-23修改實時話務統計數據
  if (CallInCount > 0)
  {
    CallInAbandonRate = CallInAbandonCount*100/CallInCount;
  }
}
//累計呼出次數
void CAgentCallCountParam::IncCallOutCount()
{
  CallOutCount++;
}
//累計呼出應答次數
void CAgentCallCountParam::IncCallOutAnsCount()
{
  CallOutAnsCount++;
  CallAnsCount++;
  if (CallOutCount < CallOutAnsCount)
  {
    CallOutCount = CallOutAnsCount;
  }
  if (CallOutCount > 0)
  {
    CallOutAnswerRate = CallOutAnsCount*100/CallOutCount;
    CallOutFailRate = 100 - CallOutAnswerRate;
  }
}
//累計轉接次數
void CAgentCallCountParam::IncTranCallCount()
{
  TranCallCount++;
}
//-----------------------------------------------------------------------------

//話務組
void CGroupCallCountParam::Reset()
{
  ACDCount = 0;
  ACDFailCount = 0;

  CallInCount = 0; //來話分配總次數
  CallInAnsCount = 0; //來話應答總次數
  CallInNoAnsCount = 0; //來話未應答總次數
  CallInAbandonCount = 0; //來話放棄總次數
  
  CallInWaitTimeSumLen = 0; //呼入累計等待時長
  CallInWaitTimeMaxLen = 0; //呼入最長等待時長
  CallInWaitTimeMinLen = 0; //呼入最短等待時長
  CallInWaitTimeAvgLen = 0; //呼入平均等待時長
  CallInWaitOverTimeCount = 0; //呼入應答超時總次數
  
  CallInAbandonTimeSumLen = 0; //呼出累計放棄前等待時長
  CallInAbandonTimeMaxLen = 0; //呼出放棄前最長等待時長
  CallInAbandonTimeMinLen = 0; //呼出放棄前最短等待時長
  CallInAbandonTimeAvgLen = 0; //呼出放棄前平均等待時長

  CallInTalkTimeLen = 0; //呼入通話時長
  CallInTalkTimeSumLen = 0; //呼入累計通話時長
  CallInTalkTimeMaxLen = 0; //呼入最長通話時長
  CallInTalkTimeMinLen = 0; //呼入最短通話時長
  CallInTalkTimeAvgLen = 0; //呼入平均通話時長
  CallInTalkOverTimeCount = 0;
  
  CallInACWTimeLen = 0; //呼入話后處理時長
  CallInACWTimeSumLen = 0; //呼入累計話后處理時長
  CallInACWTimeMaxLen = 0; //呼入最長話后處理時長
  CallInACWTimeMinLen = 0; //呼入最短話后處理時長
  CallInACWTimeAvgLen = 0; //呼入平均話后處理時長
  CallInACWOverTimeCount = 0;
  
  CallInHandleTimeLen = 0; //呼入話務處理時長
  CallInHandleTimeSumLen = 0; //呼入累計話務處理時長
  CallInHandleTimeMaxLen = 0; //呼入最長話務處理時長
  CallInHandleTimeMinLen = 0; //呼入最短話務處理時長
  CallInHandleTimeAvgLen = 0; //呼入平均話務處理時長
  
  CallInAbandonRate = 0; //來話放棄率
  CallInAnswerRate = 0; //來話接通率
  
  CallOutCount = 0; //呼出總次數
  CallOutAnsCount = 0; //呼出應答總次數
  CallOutNoAnsCount = 0; //呼出未應答總次數
  
  CallOutWaitTimeSumLen = 0; //呼出累計等待時長
  CallOutWaitTimeMaxLen = 0; //呼出最長等待時長
  CallOutWaitTimeMinLen = 0; //呼出最短等待時長
  CallOutWaitTimeAvgLen = 0; //呼出平均等待時長
  CallOutWaitOverTimeCount = 0; //呼出應答超時總次數
  
  CallOutTalkTimeLen = 0; //呼出通話時長
  CallOutTalkTimeSumLen = 0; //呼出累計通話時長
  CallOutTalkTimeMaxLen = 0; //呼出最長通話時長
  CallOutTalkTimeMinLen = 0; //呼出最短通話時長
  CallOutTalkTimeAvgLen = 0; //呼出平均通話時長
  CallOutTalkOverTimeCount = 0;
  
  CallOutACWTimeLen = 0; //呼出話后處理時長
  CallOutACWTimeSumLen = 0; //呼出累計話后處理時長
  CallOutACWTimeMaxLen = 0; //呼出最長話后處理時長
  CallOutACWTimeMinLen = 0; //呼出最短話后處理時長
  CallOutACWTimeAvgLen = 0; //呼出平均話后處理時長
  CallOutACWOverTimeCount = 0;
  
  CallOutHandleTimeLen = 0; //呼出話務處理時長
  CallOutHandleTimeSumLen = 0; //呼出累計話務處理時長
  CallOutHandleTimeMaxLen = 0; //呼出最長話務處理時長
  CallOutHandleTimeMinLen = 0; //呼出最短話務處理時長
  CallOutHandleTimeAvgLen = 0; //呼出平均話務處理時長
  
  CallOutFailRate = 0; //呼出失敗率
  CallOutAnswerRate = 0; //呼出接通率
  
  CallTalkOverTimeCount = 0; //通話超時總次數
  CallTalkTimeSumLen = 0; //累計通話總時長
  CallAnsCount = 0;

  CallACWOverTimeCount = 0;
  CallACWTimeSumLen = 0;
  
  CallHandleTimeSumLen = 0; //累計話務處理時長
  CallHandleTimeMaxLen = 0; //最長話務處理時長
  CallHandleTimeMinLen = 0; //最短話務處理時長
  CallHandleTimeAvgLen = 0; //平均話務處理時長
  
  HoldCallCount = 0; //保持總次數
  HoldCallTimeSumLen = 0; //累計保持時長
  HoldCallTimeMaxLen = 0; //最長保持時長
  HoldCallTimeMinLen = 0; //最短保持時長
  HoldCallTimeAvgLen = 0; //平均保持時長
  
  TranCallCount = 0; //轉接總次數
  
  LogOutCount=0;
  LoginWorkerCount = 0; //已登錄話務員數
  IdelWorkerCount = 0; //示閑話務員數
  BusyWorkerCount = 0; //示忙話務員數
  LeaveWorkerCount = 0; //離席話務員數
  SetBusyWorkerCount = 0; //設定示忙話務員數
  SetLeaveWorkerCount = 0; //設定離席話務員數
  
  CallInRingCount = 0; //呼入振鈴數
  CallInTalkCount = 0; //呼入通話數
  
  CallOutRingCount = 0; //正在呼出數
  CallOutTalkCount = 0; //呼出通話數
  
  CallTalkCount = 0; //正在通話數
  CallACWCount = 0; //正在話后處理數
  CallHoldCount = 0; //正在保持數

  CallUseCount = 0; //正在使用數
  
  IvrChnTotalCount = 0; //IVR通道總數數
  IvrChnBusyCount = 0; //IVR占用數
  IvrChnIdelCount = 0; //IVR空閑數
  IvrChnBlockCount = 0; //IVR阻塞數

  LoginWorkerCountB = 0; //已登錄備用話務員數
  IdelWorkerCountB = 0; //備用示閑話務員數
}
void CGroupCallCountParam::Init()
{
  CallInWaitOverTimeLen = 0; //呼入超時應答時長
  CallOutWaitOverTimeLen = 0; //呼出超時應答時長
  CallTalkOverTimeLen = 0; //通話超時時長
  CallACWOverTimeLen = 0; //話后處理超時時長

  ACDCount = 0;
  ACDFailCount = 0;
  
  CallInCount = 0; //來話分配總次數
  CallInAnsCount = 0; //來話應答總次數
  CallInNoAnsCount = 0; //來話未應答總次數
  CallInAbandonCount = 0; //來話放棄總次數
  
  CallInWaitTimeSumLen = 0; //呼入累計等待時長
  CallInWaitTimeMaxLen = 0; //呼入最長等待時長
  CallInWaitTimeMinLen = 0; //呼入最短等待時長
  CallInWaitTimeAvgLen = 0; //呼入平均等待時長
  CallInWaitOverTimeCount = 0; //呼入應答超時總次數
  
  CallInAbandonTimeSumLen = 0; //呼出累計放棄前等待時長
  CallInAbandonTimeMaxLen = 0; //呼出放棄前最長等待時長
  CallInAbandonTimeMinLen = 0; //呼出放棄前最短等待時長
  CallInAbandonTimeAvgLen = 0; //呼出放棄前平均等待時長
  
  CallInTalkTimeLen = 0; //呼入通話時長
  CallInTalkTimeSumLen = 0; //呼入累計通話時長
  CallInTalkTimeMaxLen = 0; //呼入最長通話時長
  CallInTalkTimeMinLen = 0; //呼入最短通話時長
  CallInTalkTimeAvgLen = 0; //呼入平均通話時長
  CallInTalkOverTimeCount = 0;
  
  CallInACWTimeLen = 0; //呼入話后處理時長
  CallInACWTimeSumLen = 0; //呼入累計話后處理時長
  CallInACWTimeMaxLen = 0; //呼入最長話后處理時長
  CallInACWTimeMinLen = 0; //呼入最短話后處理時長
  CallInACWTimeAvgLen = 0; //呼入平均話后處理時長
  CallInACWOverTimeCount = 0;
  
  CallInHandleTimeSumLen = 0; //呼入累計話務處理時長
  CallInHandleTimeMaxLen = 0; //呼入最長話務處理時長
  CallInHandleTimeMinLen = 0; //呼入最短話務處理時長
  CallInHandleTimeAvgLen = 0; //呼入平均話務處理時長
  
  CallInAbandonRate = 0; //來話放棄率
  CallInAnswerRate = 0; //來話接通率
  
  CallOutCount = 0; //呼出總次數
  CallOutAnsCount = 0; //呼出應答總次數
  CallOutNoAnsCount = 0; //呼出未應答總次數
  
  CallOutWaitTimeSumLen = 0; //呼出累計等待時長
  CallOutWaitTimeMaxLen = 0; //呼出最長等待時長
  CallOutWaitTimeMinLen = 0; //呼出最短等待時長
  CallOutWaitTimeAvgLen = 0; //呼出平均等待時長
  CallOutWaitOverTimeCount = 0; //呼出應答超時總次數
  
  CallOutTalkTimeLen = 0; //呼出通話時長
  CallOutTalkTimeSumLen = 0; //呼出累計通話時長
  CallOutTalkTimeMaxLen = 0; //呼出最長通話時長
  CallOutTalkTimeMinLen = 0; //呼出最短通話時長
  CallOutTalkTimeAvgLen = 0; //呼出平均通話時長
  CallOutTalkOverTimeCount = 0;
  
  CallOutACWTimeLen = 0; //呼出話后處理時長
  CallOutACWTimeSumLen = 0; //呼出累計話后處理時長
  CallOutACWTimeMaxLen = 0; //呼出最長話后處理時長
  CallOutACWTimeMinLen = 0; //呼出最短話后處理時長
  CallOutACWTimeAvgLen = 0; //呼出平均話后處理時長
  CallOutACWOverTimeCount = 0;
  
  CallOutHandleTimeLen = 0; //呼出話務處理時長
  CallOutHandleTimeSumLen = 0; //呼出累計話務處理時長
  CallOutHandleTimeMaxLen = 0; //呼出最長話務處理時長
  CallOutHandleTimeMinLen = 0; //呼出最短話務處理時長
  CallOutHandleTimeAvgLen = 0; //呼出平均話務處理時長
  
  CallOutFailRate = 0; //呼出失敗率
  CallOutAnswerRate = 0; //呼出接通率
  
  CallTalkOverTimeCount = 0; //通話超時總次數
  CallTalkTimeSumLen = 0; //累計通話總時長
  CallAnsCount = 0;

  CallACWOverTimeCount = 0;
  CallACWTimeSumLen = 0;
  
  CallHandleTimeSumLen = 0; //累計話務處理時長
  CallHandleTimeMaxLen = 0; //最長話務處理時長
  CallHandleTimeMinLen = 0; //最短話務處理時長
  CallHandleTimeAvgLen = 0; //平均話務處理時長
  
  HoldCallCount = 0; //保持總次數
  HoldCallTimeSumLen = 0; //累計保持時長
  HoldCallTimeMaxLen = 0; //最長保持時長
  HoldCallTimeMinLen = 0; //最短保持時長
  HoldCallTimeAvgLen = 0; //平均保持時長
  
  TranCallCount = 0; //轉接總次數
  
  LogOutCount=0;
  LoginWorkerCount = 0; //已登錄話務員數
  IdelWorkerCount = 0; //示閑話務員數
  BusyWorkerCount = 0; //示忙話務員數
  LeaveWorkerCount = 0; //離席話務員數
  SetBusyWorkerCount = 0; //設定示忙話務員數
  SetLeaveWorkerCount = 0; //設定離席話務員數
  
  CallInRingCount = 0; //呼入振鈴數
  CallInTalkCount = 0; //呼入通話數
  
  CallOutRingCount = 0; //正在呼出數
  CallOutTalkCount = 0; //呼出通話數
  
  CallTalkCount = 0; //正在通話數
  CallACWCount = 0; //正在話后處理數
  CallHoldCount = 0; //正在保持數

  CallUseCount = 0; //正在使用數
  
  IvrChnTotalCount = 0; //IVR通道總數數
  IvrChnBusyCount = 0; //IVR占用數
  IvrChnIdelCount = 0; //IVR空閑數
  IvrChnBlockCount = 0; //IVR阻塞數
  
  LoginWorkerCountB = 0; //已登錄備用話務員數
  IdelWorkerCountB = 0; //備用示閑話務員數
}
CGroupCallCountParam::CGroupCallCountParam()
{
  Init();
}
CGroupCallCountParam::~CGroupCallCountParam()
{

}
void CGroupCallCountParam::ResetStatusCount()
{
  LogOutCount=0;
  LoginWorkerCount = 0; //已登錄話務員數
  IdelWorkerCount = 0; //示閑話務員數
  BusyWorkerCount = 0; //示忙話務員數
  LeaveWorkerCount = 0; //離席話務員數
  SetBusyWorkerCount = 0; //設定示忙話務員數
  SetLeaveWorkerCount = 0; //設定離席話務員數
  
  CallInRingCount = 0; //呼入振鈴數
  CallInTalkCount = 0; //呼入通話數
  
  CallOutRingCount = 0; //正在呼出數
  CallOutTalkCount = 0; //呼出通話數
  
  CallTalkCount = 0; //正在通話數
  CallACWCount = 0; //正在話后處理數
  CallHoldCount = 0; //正在保持數

  CallUseCount = 0; //正在使用數
  
  LoginWorkerCountB = 0; //已登錄備用話務員數
  IdelWorkerCountB = 0; //備用示閑話務員數
}
//累計來話次數
void CGroupCallCountParam::IncCallInCount()
{
  CallInCount++;
  //ACDCount++;
}
//累計放棄來話次數
void CGroupCallCountParam::IncCallInAbandonCount(int nTimeLen)
{
  CallInAbandonCount++;
  if (CallInCount < CallInAbandonCount)
  {
    CallInCount = CallInAbandonCount;
  }
  if (CallInCount > 0)
  {
    CallInAbandonRate = CallInAbandonCount*100/CallInCount;
  }
  CallInAbandonTimeSumLen += nTimeLen;
  if (CallInAbandonTimeMaxLen == 0 || CallInAbandonTimeMaxLen < nTimeLen)
  {
    CallInAbandonTimeMaxLen = nTimeLen;
  }
  if (CallInAbandonTimeMinLen == 0 || CallInAbandonTimeMinLen > nTimeLen)
  {
    CallInAbandonTimeMinLen = nTimeLen;
  }
  if (CallInAbandonCount > 0)
  {
    CallInAbandonTimeAvgLen = CallInAbandonTimeSumLen/CallInAbandonCount;
  }
  CallInNoAnsCount = CallInCount-CallInAnsCount;
  //2014-11-23修改實時話務統計數據
  if (CallInCount > 0)
  {
    CallInAnswerRate = CallInAnsCount*100/CallInCount;
  }
}
//累計來話應答次數
void CGroupCallCountParam::IncCallInAnsCount()
{
  CallInAnsCount++;
  CallAnsCount++;
  if (CallInCount < CallInAnsCount)
  {
    CallInCount = CallInAnsCount;
  }
  if (CallInCount > 0)
  {
    CallInAnswerRate = CallInAnsCount*100/CallInCount;
  }
  CallInNoAnsCount = CallInCount-CallInAnsCount;
  //2014-11-23修改實時話務統計數據
  if (CallInCount > 0)
  {
    CallInAbandonRate = CallInAbandonCount*100/CallInCount;
  }
}
//累計呼出次數
void CGroupCallCountParam::IncCallOutCount()
{
  CallOutCount++;
}
//累計呼出應答次數
void CGroupCallCountParam::IncCallOutAnsCount()
{
  CallOutAnsCount++;
  CallAnsCount++;
  if (CallOutCount < CallOutAnsCount)
  {
    CallOutCount = CallOutAnsCount;
  }
  if (CallOutCount > 0)
  {
    CallOutAnswerRate = CallOutAnsCount*100/CallOutCount;
    CallOutFailRate = 100 - CallOutAnswerRate;
  }
}
//累計轉接次數
void CGroupCallCountParam::IncTranCallCount()
{
  TranCallCount++;
}
//累計來話振鈴時長
void CGroupCallCountParam::AddCallInWaitTimeLen(int nTimeLen)
{
  CallInWaitTimeSumLen += nTimeLen;
  if (CallInWaitTimeMaxLen == 0 || CallInWaitTimeMaxLen < nTimeLen)
  {
    CallInWaitTimeMaxLen = nTimeLen;
  }
  if (CallInWaitTimeMinLen == 0 || CallInWaitTimeMinLen > nTimeLen)
  {
    CallInWaitTimeMinLen = nTimeLen;
  }
  if (CallInCount > 0)
  {
    CallInWaitTimeAvgLen = CallInWaitTimeSumLen/CallInCount;
  }
  if (nTimeLen > CallInWaitOverTimeLen && CallInWaitOverTimeLen > 0)
  {
    CallInWaitOverTimeCount++;
  }
}
//累計來話通話時長
void CGroupCallCountParam::AddCallInTalkTimeLen(int nTimeLen)
{
  CallInTalkTimeSumLen += nTimeLen;
  if (CallInTalkTimeMaxLen == 0 || CallInTalkTimeMaxLen < nTimeLen)
  {
    CallInTalkTimeMaxLen = nTimeLen;
  }
  if (CallInTalkTimeMinLen == 0 || CallInTalkTimeMinLen > nTimeLen)
  {
    CallInTalkTimeMinLen = nTimeLen;
  }
  if (CallInAnsCount > 0)
  {
    CallInTalkTimeAvgLen = CallInTalkTimeSumLen/CallInAnsCount;
  }
  if (nTimeLen > CallTalkOverTimeLen && CallTalkOverTimeLen > 0)
  {
    CallInTalkOverTimeCount++;
    CallTalkOverTimeCount++;
  }

  CallTalkTimeSumLen += nTimeLen;
}
//累計來話保持時長
void CGroupCallCountParam::AddCallInHoldTimeLen(int nTimeLen)
{
  HoldCallTimeSumLen += nTimeLen;
  HoldCallCount++;
  if (HoldCallTimeMaxLen == 0 || HoldCallTimeMaxLen < nTimeLen)
  {
    HoldCallTimeMaxLen = nTimeLen;
  }
  if (HoldCallTimeMinLen == 0 || HoldCallTimeMinLen > nTimeLen)
  {
    HoldCallTimeMinLen = nTimeLen;
  }
  if (HoldCallCount > 0)
  {
    HoldCallTimeAvgLen = HoldCallTimeSumLen/HoldCallCount;
  }
}
//累計來話話后處理時長
void CGroupCallCountParam::AddCallInACWTimeLen(int nTimeLen)
{
  CallInACWTimeSumLen += nTimeLen;
  if (CallInACWTimeMaxLen == 0 || CallInACWTimeMaxLen < nTimeLen)
  {
    CallInACWTimeMaxLen = nTimeLen;
  }
  if (CallInACWTimeMinLen == 0 || CallInACWTimeMinLen > nTimeLen)
  {
    CallInACWTimeMinLen = nTimeLen;
  }
  if (CallInAnsCount > 0)
  {
    CallInACWTimeAvgLen = CallInACWTimeSumLen/CallInAnsCount;
  }
  if (nTimeLen > CallACWOverTimeLen && CallACWOverTimeLen > 0)
  {
    CallInACWOverTimeCount++;
    CallACWOverTimeCount++;
  }

  CallACWTimeSumLen += nTimeLen;
}
//累計來話話務處理時長
void CGroupCallCountParam::AddCallInHandleTimeLen(int nTimeLen)
{
  CallInHandleTimeSumLen += nTimeLen;
  if (CallInHandleTimeMaxLen == 0 || CallInHandleTimeMaxLen < nTimeLen)
  {
    CallInHandleTimeMaxLen = nTimeLen;
  }
  if (CallInHandleTimeMinLen == 0 || CallInHandleTimeMinLen > nTimeLen)
  {
    CallInHandleTimeMinLen = nTimeLen;
  }
  if (CallInAnsCount > 0)
  {
    CallInHandleTimeAvgLen = CallInHandleTimeSumLen/CallInAnsCount;
  }
  
  CallHandleTimeSumLen += nTimeLen;
  if (CallHandleTimeMaxLen == 0 || CallHandleTimeMaxLen < nTimeLen)
  {
    CallHandleTimeMaxLen = nTimeLen;
  }
  if (CallHandleTimeMinLen == 0 || CallHandleTimeMinLen > nTimeLen)
  {
    CallHandleTimeMinLen = nTimeLen;
  }
  if ((CallInAnsCount+CallOutAnsCount) > 0)
  {
    CallHandleTimeAvgLen = CallHandleTimeSumLen/(CallInAnsCount+CallOutAnsCount);
  }
}


//累計呼出振鈴時長
void CGroupCallCountParam::AddCallOutWaitTimeLen(int nTimeLen)
{
  CallOutWaitTimeSumLen += nTimeLen;
  if (CallOutWaitTimeMaxLen == 0 || CallOutWaitTimeMaxLen < nTimeLen)
  {
    CallOutWaitTimeMaxLen = nTimeLen;
  }
  if (CallOutWaitTimeMinLen == 0 || CallOutWaitTimeMinLen > nTimeLen)
  {
    CallOutWaitTimeMinLen = nTimeLen;
  }
  if (CallOutCount > 0)
  {
    CallOutWaitTimeAvgLen = CallOutWaitTimeSumLen/CallOutCount;
  }
  if (nTimeLen > CallOutWaitOverTimeLen && CallOutWaitOverTimeLen > 0)
  {
    CallOutWaitOverTimeCount++;
  }
}
//累計呼出通話時長
void CGroupCallCountParam::AddCallOutTalkTimeLen(int nTimeLen)
{
  CallOutTalkTimeSumLen += nTimeLen;
  if (CallOutTalkTimeMaxLen == 0 || CallOutTalkTimeMaxLen < nTimeLen)
  {
    CallOutTalkTimeMaxLen = nTimeLen;
  }
  if (CallOutTalkTimeMinLen == 0 || CallOutTalkTimeMinLen > nTimeLen)
  {
    CallOutTalkTimeMinLen = nTimeLen;
  }
  if (CallOutAnsCount > 0)
  {
    CallOutTalkTimeAvgLen = CallOutTalkTimeSumLen/CallOutAnsCount;
  }
  if (nTimeLen > CallTalkOverTimeLen && CallTalkOverTimeLen > 0)
  {
    CallOutTalkOverTimeCount++;
    CallTalkOverTimeCount++;
  }
  
  CallTalkTimeSumLen += nTimeLen;
}
//累計呼出保持時長
void CGroupCallCountParam::AddCallOutHoldTimeLen(int nTimeLen)
{
  HoldCallTimeSumLen += nTimeLen;
  HoldCallCount++;
  if (HoldCallTimeMaxLen == 0 || HoldCallTimeMaxLen < nTimeLen)
  {
    HoldCallTimeMaxLen = nTimeLen;
  }
  if (HoldCallTimeMinLen == 0 || HoldCallTimeMinLen > nTimeLen)
  {
    HoldCallTimeMinLen = nTimeLen;
  }
  if (HoldCallCount > 0)
  {
    HoldCallTimeAvgLen = HoldCallTimeSumLen/HoldCallCount;
  }
}
//累計呼出話后處理時長
void CGroupCallCountParam::AddCallOutACWTimeLen(int nTimeLen)
{
  CallOutACWTimeSumLen += nTimeLen;
  if (CallOutACWTimeMaxLen == 0 || CallOutACWTimeMaxLen < nTimeLen)
  {
    CallOutACWTimeMaxLen = nTimeLen;
  }
  if (CallOutACWTimeMinLen == 0 || CallOutACWTimeMinLen > nTimeLen)
  {
    CallOutACWTimeMinLen = nTimeLen;
  }
  if (CallOutAnsCount > 0)
  {
    CallOutACWTimeAvgLen = CallOutACWTimeSumLen/CallOutAnsCount;
  }
  if (nTimeLen > CallACWOverTimeLen && CallACWOverTimeLen > 0)
  {
    CallOutACWOverTimeCount++;
    CallACWOverTimeCount++;
  }
  
  CallACWTimeSumLen += nTimeLen;
}
//累計呼出話務處理時長
void CGroupCallCountParam::AddCallOutHandleTimeLen(int nTimeLen)
{
  CallOutHandleTimeSumLen += nTimeLen;
  if (CallOutHandleTimeMaxLen == 0 || CallOutHandleTimeMaxLen < nTimeLen)
  {
    CallOutHandleTimeMaxLen = nTimeLen;
  }
  if (CallOutHandleTimeMinLen == 0 || CallOutHandleTimeMinLen > nTimeLen)
  {
    CallOutHandleTimeMinLen = nTimeLen;
  }
  if (CallOutAnsCount > 0)
  {
    CallOutHandleTimeAvgLen = CallOutHandleTimeSumLen/CallOutAnsCount;
  }
  
  CallHandleTimeSumLen += nTimeLen;
  if (CallHandleTimeMaxLen == 0 || CallHandleTimeMaxLen < nTimeLen)
  {
    CallHandleTimeMaxLen = nTimeLen;
  }
  if (CallHandleTimeMinLen == 0 || CallHandleTimeMinLen > nTimeLen)
  {
    CallHandleTimeMinLen = nTimeLen;
  }
  if ((CallInAnsCount+CallOutAnsCount) > 0)
  {
    CallHandleTimeAvgLen = CallHandleTimeSumLen/(CallInAnsCount+CallOutAnsCount);
  }
}
