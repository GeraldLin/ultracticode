//////////////////////////////////////////////////////////////////////////
//
// odsLog.h 
//
//		有關程序日志（使用數據庫日志和NT系統日志兩種方式）的操作
//
//
//////////////////////////////////////////////////////////////////////////

#if !defined(_ODS_20_LOG_H_)
#define _ODS_20_LOG_H_

//-------------------------------------------------------------------
// odsAddToSystemLog			- 在 NT 的日志表中增加一條信息
//
// 參數：
//		strSourceName			- 事件源名稱
//		strMsg					- 信息內容
//		wType					- 類型，可以為以下幾種
//								- EVENTLOG_ERROR_TYPE
//								- EVENTLOG_WARNING_TYPE
//								- EVENTLOG_INFORMATION_TYPE
//								- EVENTLOG_AUDIT_SUCCESS
//								- EVENTLOG_AUDIT_FAILURE
//-------------------------------------------------------------------
void odsAddToSystemLog( LPCTSTR strSourceName, LPCTSTR strMsg, WORD wType );
void odsAddToSystemLog( LPCTSTR strSourceName, WORD wType, LPCTSTR lpszMsgFormat, ... );

#endif // _ODS_20_LOG_H_