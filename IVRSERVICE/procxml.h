//---------------------------------------------------------------------------
#ifndef ProcxmlH
#define ProcxmlH

//#include "rmsgfifo.h"
//---------------------------------------------------------------------------
void SendMsg2XML(int MsgId, UL SessionId, CH *msg);
void SendExecSql2XML(CH *sqls);
void SendExecSql2DB(CH *sqls);
void SendDumpSql2DB(CH *querysqls, CH *insertsqls);
void SendQueryCustomer2Webservice(const CH *callerno, const CH *calledno, const CH *weberviceaddr, const CH *weberviceparam);
void SendQueryWebService2GW(CH *params);
void SendIMACDResult(UL SessionNo, const char *IMSerialNo, int nResult);
void SendIMACDResult(UL SessionNo, const char *IMSerialNo, int nResult, const CH *seatno, int workerno, int groupno);
void SendEMACDResult(UL SessionNo, const char *EMSerialNo, int nResult);
void SendEMACDResult(UL SessionNo, const char *EMSerialNo, int nResult, const CH *seatno, int workerno, int groupno);
void SendFAXACDResult(UL SessionNo, const char *EMSerialNo, int nResult);
void SendFAXACDResult(UL SessionNo, const char *EMSerialNo, int nResult, const CH *seatno, int workerno, int groupno);

void ClearCharInCalledNo(char *pszCalledNo);

void GetXMLHeader(VXML_CMD_ATTRIBUTE_STRUCT *AttrData, VXML_XML_HEADER_STRUCT *XMLHeader);
void SetSessionCmdAttr(int nChn, UL sessionid, UL cmdattr);
void SetcurPlaySessionParam(int nChn, int MsgId, const char *MsgName);

void Proc_MSG_setaccessno_Result(UL sessionid, int settype, const CH *accessno, int result, const CH *error);
void Proc_MSG_common_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void Proc_MSG_play_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *dtmf, const CH *error);
void Proc_MSG_record_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void Proc_MSG_recordcfc_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int cfcno, const CH *error);
void Proc_MSG_callout_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void Proc_MSG_transfer_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void Proc_MSG_callseat_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void Proc_MSG_checkfile_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, UL size, const CH *error);
void Proc_MSG_getfilenum_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int filenums, const CH *error);
void Proc_MSG_getfilename_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int serialno, int filenums, const CH *filename, const CH *error);
void Proc_MSG_createcfc_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int cfcno, const CH *error);
void Proc_MSG_createcfc_Result(int nChn, int result, int cfcno, const CH *error);
void Proc_MSG_checktone_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int tonetype, const CH *error);
void Proc_MSG_pickupcall_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *callerno, const CH *calledno, int chntype, int chnno, const CH *seatno, int workerno, const CH *error);
void Proc_MSG_getacdqueue_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int waitacd, int waitans, const CH *error);

int Check_common_MSG(VXML_XML_HEADER_STRUCT *XMLHeader);
int Check_play_MSG(VXML_XML_HEADER_STRUCT *XMLHeader);

//發送應答結果
void SendAnswerResult(int nChn, int result, const char *error);

void SendCallinEvent(int nChn, int VxmlId, int calledtype, const char *param, int bandagfirst=0);
void SendSamEvent(int nChn, const char *Sam, const char *callerno, const char *orgcallerno, const char *orgcalledno);
void SendSeatDialDTMFEvent(int nChn, const char *dialdtmf);
void SendChnPlayResult(int nChn, int result, const char *error);
void SendChnTTSResult(int nChn, int result, const char *ttsfile, const char *error);
void TransferOneDTMF(int nChn, int result, char dtmf);
void SendChnRecResult(int nChn, int result, long reclen, long filesize);
void SendConfRecResult(int nCfc, int result, long reclen, long filesize);

//發送傳真結果
void Proc_MSG_Sendfax_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void SendSendfaxResult(int nChn, int result, LPCTSTR ErrorMsg, short SendPages=0, long SendBytes=0, long SendSpeed=0);
//接收傳真結果
void Proc_MSG_Recvfax_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error);
void SendRecvfaxResult(int nChn, int result, LPCTSTR ErrorMsg, LPCTSTR FaxCSID="", LPCTSTR BarCode="", short RecvPages=0, long RecvBytes=0, long RecvSpeed=0);

void SendLinkChnEvent(int nChn, int result, const char *error);
void SendJoinConfResult(int nChn, int result, const char *error);
void SendChnHangonEvent(int nChn);
void SendSendSwitchCMDResult(int nChn, int result);
void SendCalloutResult(int nOut, int point, int result, const char *error, int nChn2=0xFFFF);
void SendCalloutResult1(int nChn, int nChn1, int result, const char *error);
void SendTransferResult(int nOut, int point, int result, const char *error);
void SendTransferResult1(int nChn, int nChn1, int result, const char *error);
void SendCallseatResult(int nAcd, CAgent *pAgent, int queueno, int result, const char *error);
void SendCallseatResult1(int nChn, int result, const char *error);
void SendDialoutResult(int nChn, int result);
void SendCallFlwEvent(int calltype, int num, int point);
void SendCheckToneResult(int nChn, int result, int tonetype, const char *error);
void SendSendFSKResult(int nChn, int result, const char *error);
void SendRecvdFSKResult(int nChn, short FSKCMD, const char *FSKData, short FSKLen, int result, const char *error);
void SendTransferIVRResult(int nChn, int result, const char *error);
void SendCallConnectedEvent(int nChn, int result);
//-----------------------------------------------------------------------------
int Proc_MSG_login(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_setaccessno(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_answer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendsignal(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_hangon(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_dtmfrule(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_getdtmf(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playrule(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_asrrule(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_setvolume(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_setplaypos(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playfiles(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playcompose(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_multiplay(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_ttsstring(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_ttsfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_senddtmf(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendfsk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playcfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playtone(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_recordfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_recordcfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_stop(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_callout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_cancelcallout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendsam(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_transfer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_callseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_stopcallseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_createcfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_destroycfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_joincfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_unjoincfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_link(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_talkwith(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_listenfrom(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_inserttalk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_threetalk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_stoptalk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendfax(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_recvfax(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_checkpath(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_createpath(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_deletepath(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_getfilenum(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_getfilename(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_renname(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_copyfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_deletefile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_checkfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_clearmixer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_addfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_adddatetime(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_addmoney(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_addnumber(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_adddigits(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_addchars(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playmixer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_addttsstr(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_addttsfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_dialout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_flash(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_setvcparam(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_pause(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_replay(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_fastplay(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_checktone(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_workerlogin(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_workerlogout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_agtrancallresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_agjoinconfresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_agtranivrresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendagentmsg(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_pickupcall(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_getacdqueue(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_setflwstate(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_swapcallresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_recvfsk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_transferivr(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_releaseivr(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playswitchvoice(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendswitchcmd(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_dial(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_appendfaxfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_bandagentchn(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_sendcalltoagent(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_play(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_workerloginex(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_workerlogoutex(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_getseatlogindata(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_getidleseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_unlockseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_updateservicescore(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_updatecdrfield(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_updatesysparam(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_setforwarding(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_callfunction(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
int Proc_MSG_playex(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);

void ProcXMLMsg(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);

//---------------------------------------------------------------------------
#endif
