//---------------------------------------------------------------------------
#include "stdafx.h"
#include "prococx.h"
#include "extern.h"

//---------------------------------------------------------------------------
short MySetBit(short nValue, short nBit, short nBitValue)
{
  return nValue | (nBitValue<<nBit);
}
//--------------------放、錄音操作-------------------------------------------
//動態加載索引放音用的語音文件
int AddVoiceIndex(int index, const char *filename)
{
	return 0;
}
bool GetVopChnBynChn(int nChn, short &VopNo, short &VopChnNo)
{
  if (pChn->cVopChn == NULL || pChn->SeizeVopChnState != 1)
  {
    MyTrace(3, "GetVopChnBynChn nChn=%d fail",
      nChn);
    return false;
  }
  VopNo = pChn->cVopChn->m_nVopNo;
  VopChnNo = pChn->cVopChn->m_nChIndex;
  return true;
}
//坐席發送文件放音消息
int AgentPlayFile(int nChn, const CStringX filename, long startpos)
{
	short PlayParam=0;
  CStringX FilePath;
	
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
	
  FilePath = AddRootPath(pIVRCfg->SeatRecPath, filename);
	MyTrace(3, "AgentPlayFile ChType=%d ChIndex=%d; Filename=%s;",
    pChn->ChType, pChn->ChIndex,
    FilePath.C_Str());

  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);
  return POSTPLAYFILE(pChn->ChType, pChn->ChIndex, FilePath.C_Str(), 
    startpos, 0, PlayParam, "");
}
//發送單個文件放音消息
int PlaySingleFile(int nChn)
{
	int nPlayResult;
  short PlayParam=0, IsDtmfStop, VopNo, VopChnNo;
  CStringX FilePath;
  char DtmfStopSet[128];
	
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
	//按鍵屬性
  if (pChn->curDtmfRuleId < MAX_DTMFRULE_BUF)
  {
    sprintf(DtmfStopSet, "%s%s", pChn->DtmfRule[pChn->curDtmfRuleId].digits.C_Str(), 
      pChn->DtmfRule[pChn->curDtmfRuleId].termdigits.C_Str());
	  IsDtmfStop = pChn->DtmfRule[pChn->curDtmfRuleId].breakid;
  }
  else
  {
    DtmfStopSet[0]=0;
    IsDtmfStop = 0;
  }
  //2008-08-25 edit add
  if (pChn->curPlayRuleId < MAX_PLAYRULE_BUF)
    PlayParam = pChn->PlayRule[pChn->curPlayRuleId].format;

  FilePath = AddRootPath(pIVRCfg->PlayFilePath, pChn->curPlayData.PlayIndex[0].PlayFile);
	MyTrace(3, "PlaySingleFile ChType=%d ChIndex=%d; Filename=%s; DtmfStopSet=%s;",
    pChn->ChType, pChn->ChIndex, FilePath.C_Str(), DtmfStopSet);

  PlayParam = MySetBit(PlayParam, 4, IsDtmfStop);
  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);

  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendPlayFile2TestPhone(nChn, FilePath.C_Str(), 
        pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
    }
    else
    {
      return POSTPLAYFILE(pChn->ChType, pChn->ChIndex, FilePath.C_Str(), 
        pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    Send_SWTMSG_playfile(nChn, pChn->DeviceID, pChn->ConnID, FilePath.C_Str(), PlayParam, DtmfStopSet);
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      {
        nPlayResult = POSTPLAYFILE(0, VopChnNo, FilePath.C_Str(), 
          pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
      }
      else
      {
        nPlayResult = SendPlayFile2VOP(nChn, VopNo, VopChnNo,FilePath.C_Str(), 
          pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
      }
      if (nPlayResult == 0)
      {
        pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 1); //正在放音
        SendVopStateToAll(VopNo, VopChnNo);
        SendVOPStateToOppIVR(VopNo, VopChnNo);
      }
      return nPlayResult;
    }
  }
  return 2;
}
int PlayFileLoop(int nChn, char *filename)
{
  CStringX FilePath;
  short PlayParam=0;
  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);
  PlayParam = MySetBit(PlayParam, 8, 1);
  FilePath = AddRootPath(pIVRCfg->PlayFilePath, filename);
  return POSTPLAYFILE(pChn->ChType, pChn->ChIndex, FilePath.C_Str(), 0, 0, PlayParam, "");
}
void GetPlayFirstLast(int nChn, int PlayIndex, short &isfirst, short &islast)
{
  if (PlayIndex == 0)
  {
    if (pChn->curPlayData.DataBufNum == 1)
    {
      isfirst = 1;
      islast = 1;
    }
    else if (pChn->curPlayData.DataBufNum > 1)
    {
      isfirst = 1;
      islast = 0;
    }
  }
  else
  {
    if (PlayIndex == pChn->curPlayData.DataBufNum - 1)
    {
      isfirst = 0;
      islast = 1;
    }
    else
    {
      isfirst = 0;
      islast = 0;
    }
  }
}
//發送文件放音消息
int PlayFile(int nChn, int PlayIndex)
{
	int nPlayResult;
  short PlayParam=0, IsDtmfStop, VopNo, VopChnNo;
  CStringX FilePath;
	short isfirst, islast;
  char DtmfStopSet[128];
	
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  //按鍵屬性
  if (pChn->curDtmfRuleId < MAX_DTMFRULE_BUF)
  {
    sprintf(DtmfStopSet, "%s%s", pChn->DtmfRule[pChn->curDtmfRuleId].digits.C_Str(), 
      pChn->DtmfRule[pChn->curDtmfRuleId].termdigits.C_Str());
	  IsDtmfStop = pChn->DtmfRule[pChn->curDtmfRuleId].breakid;
  }
  else
  {
    DtmfStopSet[0]=0;
    IsDtmfStop = 0;
  }
  //2008-08-25 edit add
  if (pChn->curPlayRuleId < MAX_PLAYRULE_BUF)
    PlayParam = pChn->PlayRule[pChn->curPlayRuleId].format;

  GetPlayFirstLast(nChn, PlayIndex, isfirst, islast);
	
  FilePath = AddRootPath(pIVRCfg->PlayFilePath, pChn->curPlayData.PlayIndex[PlayIndex].PlayFile);
	MyTrace(3, "PlayFile ChType=%d ChIndex=%d; Filename=%s; DtmfStopSet=%s; isfirst=%d; islast=%d",
    pChn->ChType, pChn->ChIndex, FilePath.C_Str(), DtmfStopSet, isfirst, islast);

  PlayParam = MySetBit(PlayParam, 4, IsDtmfStop);
  PlayParam = MySetBit(PlayParam, 6, isfirst);
  PlayParam = MySetBit(PlayParam, 7, islast);
  
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendPlayFile2TestPhone(nChn, FilePath.C_Str(), 
        pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
    }
    else
    {
      return POSTPLAYFILE(pChn->ChType, pChn->ChIndex, FilePath.C_Str(), 
        0, 0, PlayParam, DtmfStopSet);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    Send_SWTMSG_playfile(nChn, pChn->DeviceID, pChn->ConnID, FilePath.C_Str(), PlayParam, DtmfStopSet);
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      {
        nPlayResult = POSTPLAYFILE(0, VopChnNo, FilePath.C_Str(), 
          pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
      }
      else
      {
        nPlayResult = SendPlayFile2VOP(nChn, VopNo, VopChnNo,FilePath.C_Str(), 
          pChn->curPlayData.StartPos, pChn->curPlayData.Length, PlayParam, DtmfStopSet);
      }
      if (nPlayResult == 0)
      {
        pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 1); //正在放音
        SendVopStateToAll(VopNo, VopChnNo);
        SendVOPStateToOppIVR(VopNo, VopChnNo);
      }
      return nPlayResult;
    }
  }
  return 2;
}
//通用放音
int PlayFile(int nChn, const char *szFileName, int nDtmfRuleId, int nPlayRuleId)
{
  StopClearPlayDtmfBuf(nChn);
  
  CStringX filepath = AddRootPath(pIVRCfg->PlayFilePath, szFileName);
  if (AddSingleFileToPlayBuf(nChn, filepath.C_Str(), 0, 0, ErrMsg) != 0)
  {
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  if (SetDTMFPlayRule(nChn, nDtmfRuleId, nPlayRuleId, ErrMsg) != 0)
  {
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  pChn->curPlayData.PlayState = 1;
  pChn->PlayedTimer = 0;
 	if (Start_ChnPlay(nChn) != 0)
  {
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  pChn->curPlayData.CmdAddr = 0;
  pChn->curPlayData.OnMsgId = MSG_onplayfile;
  pChn->curPlayData.MsgName = "onplayfile";
  pChn->curPlayData.CanPlayId = 1;
  return 0;
}
//發送內存索引放音消息
int PlayIndex(int nChn, int PlayIndex)
{
	int nPlayResult;
  short PlayParam=0, IsDtmfStop, VopNo, VopChnNo;
	short isfirst, islast;
  char DtmfStopSet[128];
	
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  //按鍵屬性
  if (pChn->curDtmfRuleId < MAX_DTMFRULE_BUF)
  {
    sprintf(DtmfStopSet, "%s%s", pChn->DtmfRule[pChn->curDtmfRuleId].digits.C_Str(), 
      pChn->DtmfRule[pChn->curDtmfRuleId].termdigits.C_Str());
	  IsDtmfStop = pChn->DtmfRule[pChn->curDtmfRuleId].breakid;
  }
  else
  {
    DtmfStopSet[0]=0;
    IsDtmfStop = 0;
  }

  GetPlayFirstLast(nChn, PlayIndex, isfirst, islast);
	
	MyTrace(3, "PlayIndex ChType=%d ChIndex=%d; VocIndex=%d; DtmfStopSet=%s; isfirst=%d; islast=%d",
    pChn->ChType, pChn->ChIndex, pChn->curPlayData.PlayIndex[PlayIndex].PlayIndex,
    DtmfStopSet, isfirst, islast);

  PlayParam = MySetBit(PlayParam, 4, IsDtmfStop);
  PlayParam = MySetBit(PlayParam, 6, isfirst);
  PlayParam = MySetBit(PlayParam, 7, islast);
  
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendPlayIndex2TestPhone(nChn, 
        pChn->curPlayData.PlayIndex[PlayIndex].PlayIndex, PlayParam, DtmfStopSet);
    }
    else
    {
      return POSTPLAYINDEX(pChn->ChType, pChn->ChIndex, 
        pChn->curPlayData.PlayIndex[PlayIndex].PlayIndex, PlayParam, DtmfStopSet);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    Send_SWTMSG_playindex(nChn, pChn->DeviceID, pChn->ConnID, pChn->curPlayData.PlayIndex[PlayIndex].PlayIndex, PlayParam, DtmfStopSet);
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      {
        nPlayResult = POSTPLAYINDEX(0, VopChnNo, 
          pChn->curPlayData.PlayIndex[PlayIndex].PlayIndex, PlayParam, DtmfStopSet);
      }
      else
      {
        nPlayResult = SendPlayIndex2VOP(nChn, VopNo, VopChnNo,
          pChn->curPlayData.PlayIndex[PlayIndex].PlayIndex, PlayParam, DtmfStopSet);
      }
      if (nPlayResult == 0)
      {
        pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 1); //正在放音
        SendVopStateToAll(VopNo, VopChnNo);
        SendVOPStateToOppIVR(VopNo, VopChnNo);
      }
      return nPlayResult;
    }
  }
  return 2;
}
//放信號音
int PlayTone(int nChn, int index, int isloop)
{
	short PlayParam=0;
	
	if (!pBoxchn->isnChnAvail(nChn)) return 1;

  if (g_nSwitchMode == 0)
  {
		MyTrace(3, "PlayTone ChType=%d ChIndex=%d; VocIndex=%d; IsLoop=%d;",
	    pChn->ChType, pChn->ChIndex, index, isloop);
	  
    if (pChn->ChStyle == CH_A_EXTN && pChn->ssState == CHN_SNG_IN_RING)
    {
      SendACK(nChn, 1);
      pChn->ssState = CHN_SNG_IN_TALK;
      DispChnStatus(nChn);
    }

    pChn->curPlayData.PlayState = 1;
	  
	  PlayParam = MySetBit(PlayParam, 6, 1);
	  PlayParam = MySetBit(PlayParam, 7, 1);
	  PlayParam = MySetBit(PlayParam, 8, isloop);
    if (g_nCardType == 9)
    {
      return SendPlayIndex2TestPhone(nChn, index, PlayParam, "");
    }
    else
    {
	    return POSTPLAYINDEX(pChn->ChType, pChn->ChIndex, index, PlayParam, "");
    }
	}
	return 0;
}
//發送內置TTS放音消息
int PlayTTS(int nChn, int PlayIndex)
{
	short PlayParam=0, IsDtmfStop, VopNo, VopChnNo;
	short isfirst, islast;
  char DtmfStopSet[128];
	
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  //按鍵屬性
  if (pChn->curDtmfRuleId < MAX_DTMFRULE_BUF)
  {
    sprintf(DtmfStopSet, "%s%s", pChn->DtmfRule[pChn->curDtmfRuleId].digits.C_Str(), 
      pChn->DtmfRule[pChn->curDtmfRuleId].termdigits.C_Str());
	  IsDtmfStop = pChn->DtmfRule[pChn->curDtmfRuleId].breakid;
  }
  else
  {
    DtmfStopSet[0]=0;
    IsDtmfStop = 0;
  }

  GetPlayFirstLast(nChn, PlayIndex, isfirst, islast);
	
	MyTrace(3, "PlayTTS ChType=%d ChIndex=%d; String=%s; DtmfStopSet=%s; isfirst=%d; islast=%d",
    pChn->ChType, pChn->ChIndex, pChn->curPlayData.PlayIndex[PlayIndex].PlayFile.C_Str(),
    DtmfStopSet, isfirst, islast);

  PlayParam = MySetBit(PlayParam, 6, isfirst);
  PlayParam = MySetBit(PlayParam, 7, islast);
  
  if (g_nSwitchMode == 0)
  {
    return POSTPLAYTTSSTR(pChn->ChType, pChn->ChIndex, 
      pChn->curPlayData.PlayIndex[PlayIndex].PlayFile.C_Str(), PlayParam, DtmfStopSet);
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
        return POSTPLAYTTSSTR(0, VopChnNo, 
          pChn->curPlayData.PlayIndex[PlayIndex].PlayFile.C_Str(), PlayParam, DtmfStopSet);
    }
  }
  return 2;
}
//發送文本文件放音消息
int PlayTTSFile(int nChn, int PlayIndex)
{
	return 0;
}
//發送內存放音消息
int PlayMemory(int nChn, unsigned char *pmem, unsigned long length)
{
	return 1;
}
//發送文件錄音消息
int RecordFile(int nChn)
{
  int nRecdResult;
  short RecdParam=0, IsDtmfStop, VopNo, VopChnNo;
  CStringX FilePath;
  char DtmfStopSet[128];
	
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  //按鍵屬性
	
	if (pChn->curRecData.TermChar.GetLength() > 0)
  {
		IsDtmfStop = 1;
    strcpy(DtmfStopSet, pChn->curRecData.TermChar.C_Str());
  }
	else
  {
		DtmfStopSet[0] = 0;
    IsDtmfStop = 0;
  }

	//if (pChn->curRecData.BeepId == 1)
		//PlayTone(nChn, 900, 0);
	
  FilePath = AddRootPath(pIVRCfg->PlayFilePath, pChn->curRecData.RecordFile);

  RecdParam = pChn->curRecData.Format;
  RecdParam = MySetBit(RecdParam, 4, IsDtmfStop);
  RecdParam = MySetBit(RecdParam, 6, pChn->curRecData.BeepId);
  RecdParam = MySetBit(RecdParam, 8, (pChn->curRecData.StartPos==0)?0:1);
  RecdParam = MySetBit(RecdParam, 9, 1);

  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "RecordFile ChType=%d ChIndex=%d; FileName=%s; MaxTime=%ld; DtmfStopSet=%s;",
      pChn->ChType, pChn->ChIndex, FilePath.C_Str(), pChn->curRecData.MaxTime, DtmfStopSet);
    if (g_nCardType == 9)
    {
      return SendRecordFile2TestPhone(nChn, FilePath.C_Str(),
        pChn->curRecData.StartPos, pChn->curRecData.MaxTime, RecdParam, DtmfStopSet);
    }
    else
    {
      return POSTRECORDFILE(pChn->ChType, pChn->ChIndex, FilePath.C_Str(),
        pChn->curRecData.StartPos, pChn->curRecData.MaxTime, RecdParam, DtmfStopSet);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    Send_SWTMSG_recordfile(nChn, pChn->DeviceID, pChn->ConnID, FilePath.C_Str(), pChn->curRecData.MaxTime, RecdParam, DtmfStopSet);
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (pVopGroup->m_pVops[VopNo].m_pVopChns[VopChnNo].GetlnState() != 0)
      {
        return 3; //阻塞
      }
      if (VopNo == 0)
      {
        nRecdResult = POSTRECORDFILE(0, VopChnNo, FilePath.C_Str(),
          pChn->curRecData.StartPos, pChn->curRecData.MaxTime, RecdParam, DtmfStopSet);
      }
      else
      {
        nRecdResult = SendRecordFile2VOP(nChn, VopNo, VopChnNo, FilePath.C_Str(),
          pChn->curRecData.StartPos, pChn->curRecData.MaxTime, RecdParam, DtmfStopSet);
      }
      if (nRecdResult == 0)
      {
        MyTrace(3, "RecordFile ChType=%d ChIndex=%d; FileName=%s; MaxTime=%ld; DtmfStopSet=%s;",
          pChn->ChType, pChn->ChIndex, FilePath.C_Str(), pChn->curRecData.MaxTime, DtmfStopSet);
        pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 2); //正在錄音
        SendVopStateToAll(VopNo, VopChnNo);
        SendVOPStateToOppIVR(VopNo, VopChnNo);
      }
      return nRecdResult;
    }
  }
  return 2;
}
//發送停止放音消息
int StopPlay(int nChn)
{
	short VopNo, VopChnNo;
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  if (pChn->curPlayData.WaitTTSResult == 1)
  {
    StopTTS(nChn);
    pChn->curPlayData.WaitTTSResult = 0;
  }
  if (pChn->curPlayData.PlayState == 0) return 0;
  MyTrace(3, "StopPlay nChn=%d;ChType=%d;ChIndex=%d", 
    nChn, pChn->ChType, pChn->ChIndex);

  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendStopPlay2TestPhone(nChn);
    }
    else
    {
      return POSTSTOPPLAY(pChn->ChType, pChn->ChIndex, 0);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    Send_SWTMSG_stopplay(nChn, pChn->DeviceID, pChn->ConnID, 0);
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 0);
      SendVopStateToAll(VopNo, VopChnNo);
      SendVOPStateToOppIVR(VopNo, VopChnNo);
      if (VopNo == 0)
      {
        return POSTSTOPPLAY(0, VopChnNo, 0);
      }
      else
      {
        return SendStopPlay2VOP(nChn, VopNo, VopChnNo);
      }
    }
  }
  return 2;
}
//暫停放音
int PausePlay(int nChn)
{
	short VopNo, VopChnNo;
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  if (pChn->curPlayData.PlayState == 0) return 0;
  
  if (g_nSwitchMode == 0)
  {
    return POSTSTOPPLAY(pChn->ChType, pChn->ChIndex, 1);
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
        return POSTSTOPPLAY(0, VopChnNo, 1);
    }
  }
  return 2;
}
//恢復暫停放音
int ReStartPlay(int nChn)
{
	short VopNo, VopChnNo;
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  if (pChn->curPlayData.PlayState == 0) return 0;
  
  if (g_nSwitchMode == 0)
  {
    return POSTSTOPPLAY(pChn->ChType, pChn->ChIndex, 2);
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      return POSTSTOPPLAY(0, VopChnNo, 2);
    }
  }
  return 2;
}
//發送停止錄音消息
int StopRecord(int nChn)
{
	short VopNo, VopChnNo;
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "StopRecord nChn=%d;ChType=%d;ChIndex=%d", 
    nChn, pChn->ChType, pChn->ChIndex);
  //if (pChn->curRecData.RecState == 0) return 0;
  
  if (g_nSwitchMode == 0)
  {
	  if (g_nCardType == 9)
    {
      return SendStopRecord2TestPhone(nChn);
    }
    else
    {
      return POSTSTOPRECORD(pChn->ChType, pChn->ChIndex, 0);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    Send_SWTMSG_stoprecord(nChn, pChn->DeviceID, pChn->ConnID, 0);
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 0);
      SendVopStateToAll(VopNo, VopChnNo);
      SendVOPStateToOppIVR(VopNo, VopChnNo);
      if (VopNo == 0)
      {
        return POSTSTOPRECORD(0, VopChnNo, 0);
      }
      else
      {
        return SendStopRecord2VOP(nChn, VopNo, VopChnNo);
      }
    }
  }
  return 2;
}
//設置錄放音動態參數
int SetRPparam(int nChn, short SetParam)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  if (g_nSwitchMode == 0)
  {
    return POSTSETRPPARAM(pChn->ChType, pChn->ChIndex, SetParam, "");
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 0;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      return POSTSETRPPARAM(0, VopChnNo, SetParam, "");
    }
  }
  return 2;
}
//向前快進
int FastFwdPlay(int nChn)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  if (g_nSwitchMode == 0)
  {
    return POSTSETRPPARAM(pChn->ChType, pChn->ChIndex, 8, "");
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      return POSTSETRPPARAM(0, VopChnNo, 8, "");
    }
  }
  return 2;
}
//向后快進
int FastBwdPlay(int nChn)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  if (g_nSwitchMode == 0)
  {
    return POSTSETRPPARAM(pChn->ChType, pChn->ChIndex, 24, "");
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      return POSTSETRPPARAM(0, VopChnNo, 24, "");
    }
  }
  return 2;
}
//設置放音音量
int SetPlayVolume(int nChn, int volume)
{
  short SetParam = 0;

  SetParam = MySetBit(SetParam, 1, 1);
  SetParam = SetParam | ((((short)volume)<<12)&0xF000);
  return SetRPparam(nChn, (short)SetParam);
}
//設置放音上總線(即放音時對方也能聽到)
int SetPlayBus(int nChn, short nBusFlag)
{
  short SetParam = 0;

  SetParam = MySetBit(SetParam, 0, 1);
  SetParam = MySetBit(SetParam, 2, nBusFlag);
  return SetRPparam(nChn, (short)SetParam);
}
//設置錄音參數
int SetRecdParam(int nChn, short nMixFlag, short nAGCFlag)
{
  short SetParam = 0;

  SetParam = MySetBit(SetParam, 6, 1);
  SetParam = MySetBit(SetParam, 8, nMixFlag);
  SetParam = MySetBit(SetParam, 9, nAGCFlag);
  return SetRPparam(nChn, (short)SetParam);
}
//發送FSK串消息
int SendFSKStr(int nChn, short FskHeader, short FskCMD, const char *FskData, short FskDataLen, short CRCType)
{
	short VopNo, VopChnNo;
	if (!pBoxchn->isnChnAvail(nChn)) return 1;

  MyTrace(3, "SendFSKStr ChType=%d;ChIndex=%d;FskHeader=0x%02x;FskCMD=0x%02x;FskData=%s;FskDataLen=%d",
    pChn->ChType, pChn->ChIndex, FskHeader, FskCMD, FskData, FskDataLen);

  if (g_nSwitchMode == 0)
  {
    return POSTSENDFSK(pChn->ChType, pChn->ChIndex, 0, FskHeader, FskCMD, (long)FskData, FskDataLen, CRCType, 0, "");
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      return POSTSENDFSK(0, VopChnNo, 0, FskHeader, FskCMD, (long)FskData, FskDataLen, CRCType, 0, "");
    }
  }
  return 2;
}
//開始接收FSK
int StartRecvFSKStr(int nChn, short FskHeader, short CRCType)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  MyTrace(3, "StartRecvFSKStr ChType=%d;ChIndex=%d;FskHeader=0x%02x;",
    pChn->ChType, pChn->ChIndex, FskHeader);
  
  if (g_nSwitchMode == 0)
  {
    return POSTSTARTRECVFSK(pChn->ChType, pChn->ChIndex, 0, FskHeader, CRCType, 0, "");
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      return POSTSTARTRECVFSK(0, VopChnNo, 0, FskHeader, CRCType, 0, "");
    }
  }
  return 2;
}
//發送DTMF串消息
int SendDTMFStr(int nChn)
{
	short VopNo, VopChnNo;
	short PlayParam=0;

  if (!pBoxchn->isnChnAvail(nChn)) return 1;
	
	MyTrace(3, "SendDTMFStr ChType=%d;ChIndex=%d;dtmf=%s;",
    pChn->ChType, pChn->ChIndex, pChn->curPlayData.PlayIndex[0].PlayFile.C_Str());

  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);
  
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendSendDTMF2TestPhone(nChn, pChn->curPlayData.PlayIndex[0].PlayFile.C_Str());
    else
      return POSTSENDDTMF(pChn->ChType, pChn->ChIndex, 
      pChn->curPlayData.PlayIndex[0].PlayFile.C_Str(), PlayParam, "");
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      {
        return POSTSENDDTMF(0, VopChnNo, 
          pChn->curPlayData.PlayIndex[0].PlayFile.C_Str(), PlayParam, "");
      }
      else
      {
        return SendSendDTMF2VOP(nChn, VopNo, VopChnNo,
          pChn->curPlayData.PlayIndex[0].PlayFile.C_Str(), PlayParam, "");
      }
    }
  }
  return 2;
}
int SendDTMFStr(int nChn, const char *dtmfs)
{
  short VopNo, VopChnNo;
  short PlayParam=0;
  
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  MyTrace(3, "SendDTMFStr ChType=%d;ChIndex=%d;dtmf=%s;",
    pChn->ChType, pChn->ChIndex, dtmfs);
  
  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);
  
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendSendDTMF2TestPhone(nChn, dtmfs);
    else
      return POSTSENDDTMF(pChn->ChType, pChn->ChIndex, 
      dtmfs, PlayParam, "");
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 1;
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      {
        return POSTSENDDTMF(0, VopChnNo, dtmfs, PlayParam, "");
      }
      else
      {
        return SendSendDTMF2VOP(nChn, VopNo, VopChnNo, dtmfs, PlayParam, "");
      }
    }
  }
  return 2;
}
//-----------------------會議放音、錄音----------------------------------------
//判斷會議放音序號
void ConfGetPlayFirstLast(int nCfc, int PlayIndex, short &isfirst, short &islast)
{
  if (PlayIndex == 0)
  {
    if (pCfc->curPlayData.DataBufNum == 1)
    {
      isfirst = 1;
      islast = 1;
    }
    else if (pCfc->curPlayData.DataBufNum > 1)
    {
      isfirst = 1;
      islast = 0;
    }
  }
  else
  {
    if (PlayIndex == pCfc->curPlayData.DataBufNum - 1)
    {
      isfirst = 0;
      islast = 1;
    }
    else
    {
      isfirst = 0;
      islast = 0;
    }
  }
}
//發送會議文件放音消息
int ConfPlayFile(int nCfc, int PlayIndex)
{
	short PlayParam=0;
  CStringX FilePath;
	short isfirst, islast;
	
	if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
  GetPlayFirstLast(nCfc, PlayIndex, isfirst, islast);
	
  FilePath = AddRootPath(pIVRCfg->PlayFilePath, pCfc->curPlayData.PlayIndex[PlayIndex].PlayFile);
	MyTrace(3, "ConfPlayFile ConfNo=%d; FileName=%s; isfirst=%d; islast=%d",
    nCfc, FilePath.C_Str(), isfirst, islast);

  PlayParam = MySetBit(PlayParam, 6, isfirst);
  PlayParam = MySetBit(PlayParam, 7, islast);
  return CONFPLAYFILE(nCfc, FilePath.C_Str(), 0, 0, PlayParam, "");
}
//發送會議內存索引放音消息
int ConfPlayIndex(int nCfc, int PlayIndex)
{
  short PlayParam=0;
  short isfirst, islast;
	
  if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
  GetPlayFirstLast(nCfc, PlayIndex, isfirst, islast);
	
	MyTrace(3, "ConfPlayIndex ConfNo=%d; VocIndex=%d; isfirst=%d; islast=%d",
    nCfc, pCfc->curPlayData.PlayIndex[PlayIndex].PlayIndex, isfirst, islast);

  PlayParam = MySetBit(PlayParam, 6, isfirst);
  PlayParam = MySetBit(PlayParam, 7, islast);
	return CONFPLAYINDEX(nCfc, pCfc->curPlayData.PlayIndex[PlayIndex].PlayIndex,
    PlayParam, "");
}
//播放會議提示音
int ConfPlayKnockTone(int nCfc, int KnockTone)
{
	short PlayParam=0;

  MyTrace(3, "ConfPlayKnockTone ConfNo=%d; VocIndex=%d;",
    nCfc, KnockTone);

  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);
	return CONFPLAYINDEX(nCfc, KnockTone, PlayParam, "");
}
//會議放信號音
int ConfPlayTone(int nCfc, int index, int isloop)
{
	short PlayParam=0;

  MyTrace(3, "ConfPlayTone ConfNo=%d; index=%d;",
    nCfc, index);

  PlayParam = MySetBit(PlayParam, 6, 1);
  PlayParam = MySetBit(PlayParam, 7, 1);
	return CONFPLAYINDEX(nCfc, index, PlayParam, "");
}
//發送會議內置TTS放音消息
int ConfPlayTTS(int nCfc, int PlayIndex)
{
	return 1;
}
//發送會議文本文件放音消息
int ConfPlayTTSFile(int nCfc, int PlayIndex)
{
	return 1;
}
//發送會議文件錄音消息
int ConfRecordFile(int nCfc)
{
  short RecdParam=0;
  
  if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
  RecdParam = pCfc->curRecData.Format;
  RecdParam = MySetBit(RecdParam, 4, 0);
  RecdParam = MySetBit(RecdParam, 6, 0);
  RecdParam = MySetBit(RecdParam, 8, (pCfc->curRecData.StartPos==0)?0:1);
  RecdParam = MySetBit(RecdParam, 9, 1); //錄音時間單位為秒
  
  return CONFRECORDFILE(nCfc, pCfc->curRecData.RecordFile.C_Str(),
    pCfc->curRecData.StartPos, pCfc->curRecData.MaxTime, RecdParam, "");
}
//發送會議停止放音消息
int ConfStopPlay(int nCfc)
{
  MyTrace(3, "ConfStopPlay nCfc=%d", nCfc);
  return CONFSTOPPLAY(nCfc, 0);
}
//發送會議停止錄音消息
int ConfStopRecord(int nCfc)
{
  MyTrace(3, "ConfStopRecord nCfc=%d", nCfc);
  return CONFSTOPRECORD(nCfc, 0);
}
//設置會議錄放音動態參數
int SetConfRPparam(int nCfc, int RecWithPlayVoice, int PlayVol)
{
  return 1;
}
//------------------呼叫操作-------------------------------------------------
//發送ACM消息
int SendACM(int nChn, int AcmPara)
{
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "SendACM nChn=%d;ChType=%d;ChIndex=%d;AcmPara=%d", 
    nChn, pChn->ChType, pChn->ChIndex, AcmPara);
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendACM2TestPhone(nChn);
    }
    else
    {
      //如果返回值為9表示該指令已提交程序，需要等待后續實際的執行結果（0-成功 1-失敗）
      return POSTACM(pChn->ChType, pChn->ChIndex, AcmPara);
    }
  }
  else
  {
    return 0;
  }
}
//發送ACK消息
int SendACK(int nChn, int TollId)
{
	short VopNo, VopChnNo;

  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "SendACK nChn=%d;ChType=%d;ChIndex=%d;TollId=%d", 
    nChn, pChn->ChType, pChn->ChIndex, TollId);
  if (pChn->lgChnType == 1)
  {
    ChCount.TrkInAnsCount ++;
  }
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendACK2TestPhone(nChn);
    }
    else
    {
      //如果返回值為9表示該指令已提交程序，需要等待后續實際的執行結果（0-成功 1-失敗）
      return POSTACK(pChn->ChType, pChn->ChIndex, TollId);
    }
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
        return POSTACK(0, VopChnNo, TollId);
    }
    return 1;
  }
}
//發送釋放通道消息
int Release(int nChn, int Reason)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "Release nChn=%d;ChType=%d;ChIndex=%d;Reason=%d", 
    nChn, pChn->ChType, pChn->ChIndex, Reason);
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
    {
      return SendRelease2TestPhone(nChn);
    }
    else
    {
      return POSTRELEASE(pChn->ChType, pChn->ChIndex, Reason);
    }
  }
  else if (pVopGroup == NULL && g_nSwitchType == 13) //edit 2015-11-26 for freeswitch
  {
    return 0;
  }
  else
  {
    if (pChn->VOC_TrunkId == 0)
    {
      Send_SWTMSG_hangup(pChn->ChIndex, pChn->DeviceID);
    }
    else
    {
      if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
      {
        if (VopNo == 0)
          POSTRELEASE(0, VopChnNo, Reason);
      }
    }
    return 0;
  }
}
int Hangup(int nChn)
{
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "Hangup nChn=%d;ChType=%d;ChIndex=%d;", 
    nChn, pChn->ChType, pChn->ChIndex);
  if (g_nSwitchMode != 0)
  {
    Send_SWTMSG_hangup(pChn->ChIndex, pChn->DeviceID);
  }
  return 0;
}
int HangonIVR(int nChn)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "Hangup nChn=%d;ChType=%d;ChIndex=%d;", 
    nChn, pChn->ChType, pChn->ChIndex);
  if (g_nSwitchMode != 0)
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
        POSTRELEASE(0, VopChnNo, 0);
    }
  }
  return 0;
}
//發送呼出消息 2013-09-30增加傳遞呼出路由
int Callout(int nChn, int RouteNo, int CallPara, const CH *caller, const CH *called, const CH *orgcalled, short routeno)
{
	short nResult, i;
  char newcalled[128];
  char newcaller[128];
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  memset(newcalled, 0, 128);
  strcpy(newcaller, caller);
  if (g_nSwitchMode == 0)
  {
    if (pChn->ChStyle == CH_A_EXTN)
    {
      if (pChn->ChStyleIndex < 64)
      {
        if (strlen(called) > 0)
        {
          if (called[0] == '0')
          {
            sprintf(newcalled, "%s%s%s", pIVRCfg->PreCode[pChn->ChStyleIndex], pIVRCfg->IPPreCode[pChn->ChStyleIndex], called);
          }
          else
          {
            if (pIVRCfg->PBXExtrnList[pChn->ChStyleIndex].ExtrnPreNum == 0)
            {
              sprintf(newcalled, "%s%s", pIVRCfg->PreCode[pChn->ChStyleIndex], called);
            }
            else
            {
              for (i=0; i<pIVRCfg->PBXExtrnList[pChn->ChStyleIndex].ExtrnPreNum; i++)
              {
                if (strncmp(called, pIVRCfg->PBXExtrnList[pChn->ChStyleIndex].PBXExtrn[i].ExtrnPreCode, strlen(pIVRCfg->PBXExtrnList[pChn->ChStyleIndex].PBXExtrn[i].ExtrnPreCode)) == 0
                  && (int)strlen(called) == pIVRCfg->PBXExtrnList[pChn->ChStyleIndex].PBXExtrn[i].ExtrnLen)
                {
                  break;
                }
              }
              if (i>=pIVRCfg->PBXExtrnList[pChn->ChStyleIndex].ExtrnPreNum)
              {
                sprintf(newcalled, "%s%s", pIVRCfg->PreCode[pChn->ChStyleIndex], called);
              }
              else
              {
                strncpy(newcalled, called, MAX_TELECODE_LEN-1);
              }
            }
          }
        }
        else
        {
          newcalled[0] = 0;
        }
      }
      else
      {
        strncpy(newcalled, called, MAX_TELECODE_LEN-1);
      }
    }
    else if (pChn->ChStyle == CH_S_VOIP || pChn->ChStyle == CH_D_VOIP)
    {
      if (RouteNo >= 0 && RouteNo < MAX_ROUTE_NUM)
      {
        if (pBoxchn->Routes[RouteNo].SIPRegMode == 0 && strlen(pBoxchn->Routes[RouteNo].CallerSIPProxyIP) > 0)
        {
          sprintf(newcaller, "sip:%s@%s", caller, pBoxchn->Routes[RouteNo].CallerSIPProxyIP);
        }

        if (pBoxchn->Routes[RouteNo].SIPRegMode == 0 && strlen(pBoxchn->Routes[RouteNo].CalledSIPProxyIP) > 0)
        {
          sprintf(newcalled, "sip:%s@%s", called, pBoxchn->Routes[RouteNo].CalledSIPProxyIP);
        }
        else
        {
          strncpy(newcalled, called, MAX_TELECODE_LEN-1);
        }
      }
      else
      {
        strncpy(newcalled, called, MAX_TELECODE_LEN-1);
      }
    }
    else
    {
      if (RouteNo == 0 || RouteNo >= MAX_ROUTE_NUM)
      {
        strncpy(newcalled, called, MAX_TELECODE_LEN-1);
      }
      else
      {
        if (called[0] == '0')
          sprintf(newcalled, "%s%s", pBoxchn->Routes[RouteNo].IPPreCode, called);
        else
          strncpy(newcalled, called, MAX_TELECODE_LEN-1);
      }
    }
    MyTrace(3, "Callout nChn=%d;ChType=%d;ChIndex=%d;CallPara=%d;Caller=%s;Called=%s;OrgCaller=%s", 
      nChn, pChn->ChType, pChn->ChIndex, CallPara, newcaller, newcalled, orgcalled);
    
    pChn->lnState = CHN_LN_SEIZE; //占用
    pChn->CallInOut = CALL_OUT; //呼叫方向
    pChn->ssState = CHN_SNG_OT_WAIT;
    strncpy(pChn->CallerNo, caller, MAX_TELECODE_LEN-1);
    strncpy(pChn->CalledNo, called, MAX_TELECODE_LEN-1);
    pChn->CallTime = time(0);
    DispChnStatus(nChn);
    
    if (pChn->lgChnType == CHANNEL_TRUNK)
    {
      pBoxchn->pIdelTrkChnQueue->TrkNoDelFromQueue(pChn->lgChnNo);
    }
    if (g_nCardType == 9)
      nResult = SendCallOut2TestPhone(nChn, newcaller, newcalled);
    else
      nResult = POSTCALLOUT(pChn->ChType, pChn->ChIndex, CallPara, ((routeno<<10)&0xFC00)|(nChn&0x3FF), newcaller, newcalled, orgcalled);
    
    if (pChn->lgChnType == 1)
    {
      ChCount.TrkOutCount ++;
      ChCount.TrkCallCount ++;
    }
    return nResult;
  }
  else
  {
    short VopNo, VopChnNo;

    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo != 0)
        return 1;

      if (strlen(caller) == 0 && strlen(called) == 0)
      {
        MyTrace(3, "Callout nChn=%d;VopChnNo=%d;CallPara=%d;Caller=%s;Called=%s;OrgCaller=%s", 
          nChn, VopChnNo, CallPara, caller, newcalled, orgcalled);
        return POSTCALLOUT(0, VopChnNo, CallPara, nChn, "", "", "");
      }

      AutoAddCallOutPreCode(called, newcalled, 0);
      
      MyTrace(3, "Callout nChn=%d;VopChnNo=%d;CallPara=%d;Caller=%s;Called=%s;OrgCaller=%s", 
        nChn, VopChnNo, CallPara, caller, newcalled, orgcalled);
      
      pChn->lnState = CHN_LN_SEIZE; //占用
      pChn->CallInOut = CALL_OUT; //呼叫方向
      pChn->ssState = CHN_SNG_OT_WAIT;
      strncpy(pChn->CallerNo, caller, MAX_TELECODE_LEN-1);
      strncpy(pChn->CalledNo, called, MAX_TELECODE_LEN-1);
      pChn->CallTime = time(0);
      DispChnStatus(nChn);
      
      if (pChn->lgChnType == CHANNEL_TRUNK)
      {
        pBoxchn->pIdelTrkChnQueue->TrkNoDelFromQueue(pChn->lgChnNo);
      }
      nResult = POSTCALLOUT(0, VopChnNo, CallPara, nChn, caller, newcalled, orgcalled);
      if (pChn->lgChnType == 1)
      {
        ChCount.TrkOutCount ++;
        ChCount.TrkCallCount ++;
      }
      return nResult;
    }
  }
  return 1;
}
//自動判斷交換機呼出號碼是否加撥字冠
void AutoAddCallOutPreCode(const char *called, char *newcalled, int nTranType)
{
  int i;
  char szCalled[64], szTemp[64];
  memset(szTemp, 0, 64);

  memset(szCalled, 0, 64);
  
  if (strlen(called) > 5 && called[0] == '9' 
    && ((pIVRCfg->isAutoAddDailOutPreCode == true && nTranType == 0) 
       || (pIVRCfg->isAutoAddTranOutPreCode == true && nTranType == 1))
     )
  {
    strcpy(szCalled, &called[1]);
  }
  else
  {
    strcpy(szCalled, called);
  }

  if (strlen(szCalled) > 0)
  {
    if (szCalled[0] == '0')
    {
      if (pIVRCfg->isAutoAddDailOutIPCode == true && strlen(pIVRCfg->SWTIPPreCode) > 0)
        sprintf(szTemp, "%s%s%s", pIVRCfg->SWTPreCode, pIVRCfg->SWTIPPreCode, szCalled);
      else
        sprintf(szTemp, "%s%s", pIVRCfg->SWTPreCode, szCalled);
    }
    else
    {
      if (pIVRCfg->SWTPBXExtrnList.ExtrnPreNum == 0)
      {
        sprintf(szTemp, "%s%s", pIVRCfg->SWTPreCode, szCalled);
      }
      else
      {
        for (i=0; i<pIVRCfg->SWTPBXExtrnList.ExtrnPreNum; i++)
        {
          if (strncmp(szCalled, pIVRCfg->SWTPBXExtrnList.PBXExtrn[i].ExtrnPreCode, strlen(pIVRCfg->SWTPBXExtrnList.PBXExtrn[i].ExtrnPreCode)) == 0
            && (int)strlen(szCalled) == pIVRCfg->SWTPBXExtrnList.PBXExtrn[i].ExtrnLen)
          {
            break;
          }
        }
        if (i>=pIVRCfg->SWTPBXExtrnList.ExtrnPreNum)
        {
          sprintf(szTemp, "%s%s", pIVRCfg->SWTPreCode, szCalled);
        }
        else
        {
          sprintf(szTemp, "%s", szCalled);
        }
      }
    }

    if (((pIVRCfg->isAutoAddDailOutPreCode == true && nTranType == 0) 
      || (pIVRCfg->isAutoAddTranOutPreCode == true && nTranType == 1)) 
      && strlen(pIVRCfg->SWTDialOutPreCode) > 0)
    {
      sprintf(newcalled, "%s%s", pIVRCfg->SWTDialOutPreCode, szTemp);
    }
    else
    {
      strcpy(newcalled, szTemp);
    }
  }
  else
  {
    newcalled[0] = 0;
  }
}
//發送后續地址消息
int Callinfo(int nChn, const CH *called)
{
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  if ((strlen(pChn->CalledNo) + strlen(called)) < MAX_TELECODE_LEN)
  {
    strcat(pChn->CalledNo, called);
    DispChnStatus(nChn);
  }
  MyTrace(3, "Callinfo nChn=%d;ChType=%d;ChIndex=%d;Called=%s", 
    nChn, pChn->ChType, pChn->ChIndex, called);
  if (g_nSwitchMode == 0)
  {
    return POSTSAM(pChn->ChType, pChn->ChIndex, 0, nChn, "", called);
  }
  else
  {
    return 0;
  }
}
//發送取消呼出消息
int CancelCallout(int nChn, int nChn1)
{
	int nChn2;
  short VopNo, VopChnNo;
  
	if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "CancelCallout nChn=%d;ChType=%d;ChIndex=%d", 
    nChn, pChn->ChType, pChn->ChIndex);
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendStopCallout2TestPhone(nChn);
    else
      return POSTCANCELCALLOUT(pChn->ChType, pChn->ChIndex, 0, nChn);
  }
  else
  {
    if (pBoxchn->isnChnAvail(nChn1))
    {
      if (pChn1->cVopChn)
      {
        nChn2 = pChn1->cVopChn->m_nBoxChn;
        if (pBoxchn->isnChnAvail(nChn2))
        {
          Send_SWTMSG_stoptransfer(nChn2, pChn2->DeviceID, pChn1->ConnID);
        }
        return 0;
      }
    }
    else
    {
      if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
      {
        if (VopNo == 0)
        {
          return POSTCANCELCALLOUT(0, VopChnNo, 0, VopChnNo);
        }
      }
    }
  }
  return 1;
}
//發送模擬外線拍叉簧消息
int Flash(int nChn)
{
  short VopNo, VopChnNo;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  MyTrace(3, "Flash nChn=%d;ChType=%d;ChIndex=%d", 
    nChn, pChn->ChType, pChn->ChIndex);
  if (g_nSwitchMode == 0)
  {
    return POSTFLASH(pChn->ChType, pChn->ChIndex, 0);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
      {
        return POSTFLASH(0, VopChnNo, 0);
      }
    }
  }
  return 1;
}
//轉接電話（拍叉簧方式）
int Transfer(int nChn, short TranType, LPCTSTR Called, const char *TranData)
{
  short VopNo, VopChnNo;
  
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "Transfer nChn=%d;ChType=%d;ChIndex=%d;TranType=%d;Called=%s;TranData=%s", 
    nChn, pChn->ChType, pChn->ChIndex, TranType, Called, TranData);
  if (g_nSwitchMode == 0)
  {
    return POSTTRANSFER(pChn->ChType, pChn->ChIndex, TranType, Called, TranData);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
        return POSTTRANSFER(0, VopChnNo, TranType, Called, TranData);
    }
    return 1;
  }
}
//停止轉接電話（拍叉簧方式）(Reason: 0-正常停止轉接 1-因為檢測到摘機而停止轉接)
int StopTransfer(int nChn, short Reason)
{
  short VopNo, VopChnNo;
  
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  MyTrace(3, "StopTransfer nChn=%d;ChType=%d;ChIndex=%d;Reason=%d", 
    nChn, pChn->ChType, pChn->ChIndex, Reason);
  if (g_nSwitchMode == 0)
  {
    return POSTSTOPTRANSFER(pChn->ChType, pChn->ChIndex, Reason);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (VopNo == 0)
        return POSTSTOPTRANSFER(0, VopChnNo, Reason);
    }
    return 1;
  }
}

//------------------交換操作-------------------------------------------------
//通道交換函數
int RouterTalk(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;
  
  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "RouterTalk nChn1=%d;ChType1=%d;ChIndex1=%d <-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
      nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
    return POSTTALK(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 0);
  }
  else if (pChn1->VOC_TrunkId == 1 && pChn2->VOC_TrunkId == 1) //2015-11-25 針對語音中繼的修改
  {
    short VopNo1, VopChnNo1;
    short VopNo2, VopChnNo2;
    if (GetVopChnBynChn(nChn1, VopNo1, VopChnNo1) && GetVopChnBynChn(nChn2, VopNo2, VopChnNo2))
    {
      if (VopNo1 == 0 && VopNo2 == 0)
      {
        MyTrace(3, "RouterTalk nChn1=%d;ChType1=%d;ChIndex1=%d <-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
          nChn1, VopNo1, VopChnNo1, nChn2, VopNo2, VopChnNo2);
        return POSTTALK(VopNo1, VopChnNo1, VopNo2, VopChnNo2, 0);
      }
    }
  }
  return 0;
}
int UnRouterTalk(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "RouterDisConn nChn1=%d;ChType1=%d;ChIndex1=%d <-X-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
      nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
    return POSTDISCON(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 0);
  }
  else if (pChn1->VOC_TrunkId == 1 && pChn2->VOC_TrunkId == 1) //2015-11-25 針對語音中繼的修改
  {
    short VopNo1, VopChnNo1;
    short VopNo2, VopChnNo2;
    if (GetVopChnBynChn(nChn1, VopNo1, VopChnNo1) && GetVopChnBynChn(nChn2, VopNo2, VopChnNo2))
    {
      if (VopNo1 == 0 && VopNo2 == 0)
      {
        MyTrace(3, "RouterDisConn nChn1=%d;ChType1=%d;ChIndex1=%d <-X-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
          nChn1, VopNo1, VopChnNo1, nChn2, VopNo2, VopChnNo2);
        return POSTDISCON(VopNo1, VopChnNo1, VopNo2, VopChnNo2, 0);
      }
    }
  }
  return 0;
}
int RouterMonitor(int nChn1, int nChn2, int nVolume)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "RouterMonitor nChn1=%d;ChType1=%d;ChIndex1=%d <- nChn2=%d;ChType2=%d;ChIndex2=%d", 
      nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
    return POSTMONITOR(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, nVolume);
  }
  else if (pChn1->VOC_TrunkId == 1 && pChn2->VOC_TrunkId == 1) //2015-11-25 針對語音中繼的修改
  {
    short VopNo1, VopChnNo1;
    short VopNo2, VopChnNo2;
    if (GetVopChnBynChn(nChn1, VopNo1, VopChnNo1) && GetVopChnBynChn(nChn2, VopNo2, VopChnNo2))
    {
      if (VopNo1 == 0 && VopNo2 == 0)
      {
        MyTrace(3, "RouterMonitor nChn1=%d;ChType1=%d;ChIndex1=%d <- nChn2=%d;ChType2=%d;ChIndex2=%d", 
          nChn1, VopNo1, VopChnNo1, nChn2, VopNo2, VopChnNo2);
        return POSTMONITOR(VopNo1, VopChnNo1, VopNo2, VopChnNo2, nVolume);
      }
    }
  }
  return 0;
}
int UnRouterMonitor(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "RouterDisConn nChn1=%d;ChType1=%d;ChIndex1=%d <-X- nChn2=%d;ChType2=%d;ChIndex2=%d", 
      nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
    return POSTDISCON(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 1);
  }
  else if (pChn1->VOC_TrunkId == 1 && pChn2->VOC_TrunkId == 1) //2015-11-25 針對語音中繼的修改
  {
    short VopNo1, VopChnNo1;
    short VopNo2, VopChnNo2;
    if (GetVopChnBynChn(nChn1, VopNo1, VopChnNo1) && GetVopChnBynChn(nChn2, VopNo2, VopChnNo2))
    {
      if (VopNo1 == 0 && VopNo2 == 0)
      {
        MyTrace(3, "RouterDisConn nChn1=%d;ChType1=%d;ChIndex1=%d <-X- nChn2=%d;ChType2=%d;ChIndex2=%d", 
          nChn1, VopNo1, VopChnNo1, nChn2, VopNo2, VopChnNo2);
        return POSTDISCON(VopNo1, VopChnNo1, VopNo2, VopChnNo2, 1);
      }
    }
  }
  return 0;
}
int RouterCbm(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "RouterCbm nChn1=%d;ChType1=%d;ChIndex1=%d <-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
      nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
    return POSTTALK(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 2);
  }
  else if (pChn1->VOC_TrunkId == 1 && pChn2->VOC_TrunkId == 1) //2015-11-25 針對語音中繼的修改
  {
    short VopNo1, VopChnNo1;
    short VopNo2, VopChnNo2;
    if (GetVopChnBynChn(nChn1, VopNo1, VopChnNo1) && GetVopChnBynChn(nChn2, VopNo2, VopChnNo2))
    {
      if (VopNo1 == 0 && VopNo2 == 0)
      {
        MyTrace(3, "RouterCbm nChn1=%d;ChType1=%d;ChIndex1=%d <-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
          nChn1, VopNo1, VopChnNo1, nChn2, VopNo2, VopChnNo2);
        return POSTTALK(VopNo1, VopChnNo1, VopNo2, VopChnNo2, 2);
      }
    }
  }
  return 0;
}
int UnRouterCbm(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  if (g_nSwitchMode == 0)
  {
    MyTrace(3, "UnRouterCbm nChn1=%d;ChType1=%d;ChIndex1=%d <-X-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
      nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
    return POSTDISCON(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 2);
  }
  else if (pChn1->VOC_TrunkId == 1 && pChn2->VOC_TrunkId == 1) //2015-11-25 針對語音中繼的修改
  {
    short VopNo1, VopChnNo1;
    short VopNo2, VopChnNo2;
    if (GetVopChnBynChn(nChn1, VopNo1, VopChnNo1) && GetVopChnBynChn(nChn2, VopNo2, VopChnNo2))
    {
      if (VopNo1 == 0 && VopNo2 == 0)
      {
        MyTrace(3, "UnRouterCbm nChn1=%d;ChType1=%d;ChIndex1=%d <-X-> nChn2=%d;ChType2=%d;ChIndex2=%d", 
          nChn1, VopNo1, VopChnNo1, nChn2, VopNo2, VopChnNo2);
        return POSTDISCON(VopNo1, VopChnNo1, VopNo2, VopChnNo2, 2);
      }
    }
  }
  return 0;
}
int RouterInVC(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  MyTrace(3, "RouterInVC nChn1=%d;ChType1=%d;ChIndex1=%d <<--> nChn2=%d;ChType2=%d;ChIndex2=%d", 
    nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
  return POSTTALK(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 3);
}
int RouterOutVC(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  MyTrace(3, "RouterOutVC nChn1=%d;ChType1=%d;ChIndex1=%d <-->> nChn2=%d;ChType2=%d;ChIndex2=%d", 
    nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
  return POSTTALK(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 4);
}
int RouterIOVC(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  MyTrace(3, "RouterIOVC nChn1=%d;ChType1=%d;ChIndex1=%d <<-->> nChn2=%d;ChType2=%d;ChIndex2=%d", 
    nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
  return POSTTALK(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 5);
}
int UnRouterVC(int nChn1, int nChn2)
{
  if (!pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) return 1;

  MyTrace(3, "UnRouterVC nChn1=%d;ChType1=%d;ChIndex1=%d <<-X->> nChn2=%d;ChType2=%d;ChIndex2=%d", 
    nChn1, pChn1->ChType, pChn1->ChIndex, nChn2, pChn2->ChType, pChn2->ChIndex);
  return POSTDISCON(pChn1->ChType, pChn1->ChIndex, pChn2->ChType, pChn2->ChIndex, 5);
}
int SetVCParam(int nChn, int vcmode, int vcparam, int vcmix, int noisemode, int noiseparam)
{
  US param, dataspec;

  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  param = vcparam & 0x0F;
  if (vcmode == 1)
    param |= 16;
  param = param | (vcmix << 5);
  
  dataspec = noiseparam;
  dataspec = dataspec | (noisemode << 6);
  
  MyTrace(3, "SetVCParam nChn=%d;ChType=%d;ChIndex=%d VCParam=0x%x NoiseParam=0x%x", 
    nChn, pChn->ChType, pChn->ChIndex, param, dataspec);
  param = param | ((dataspec<<8)&0xFF00);
  return POSTSETVCPARAM(pChn->ChType, pChn->ChIndex, param, 0);
}
//傳真收發函數
int SendFAX(int nChn, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode)
{
  short VopNo, VopChnNo;
  int nResult;

  if (AuthMaxFax == 0)
    return 1;
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendSendFax2TestPhone(nChn, FileName);
    else
      return POSTSENDFAX(pChn->ChType, pChn->ChIndex, 0, FileName, FaxCSID, Resolution, TotalPages, PageList, FaxHeader, FaxFooter, BarCode);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (pVopGroup->m_pVops[VopNo].m_pVopChns[VopChnNo].GetlnState() != 0)
      {
        MyTrace(3, "SendFAX nChn=%d VopNo=%d VopChnNo=%d block", nChn, VopNo, VopChnNo);
        return 3; //阻塞
      }
      if (VopNo == 0)
      {
        nResult = POSTSENDFAX(0, VopChnNo, 0, FileName, FaxCSID, Resolution, TotalPages, PageList, FaxHeader, FaxFooter, BarCode);
        MyTrace(3, "SendFAX POSTSENDFAX nChn=%d nResult=%d", nChn, nResult);
      }
      else
      {
        nResult = SendSendFax2VOP(nChn, VopNo, VopChnNo, FileName, FaxCSID, 0, "");
        MyTrace(3, "SendFAX SendSendFax2VOP nChn=%d nResult=%d", nChn, nResult);
      }
      if (nResult == 0)
      {
        pVopGroup->m_pVops[VopNo].SetsvState(VopChnNo, 3); //正在發送傳真
        SendVopStateToAll(VopNo, VopChnNo);
        SendVOPStateToOppIVR(VopNo, VopChnNo);
      }
      return nResult;
    }
    else
    {
      MyTrace(3, "SendFAX GetVopChnBynChn fail nChn=%d", nChn);
    }
  }
  return 2;
}
int AppendFAX(int nChn, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode)
{
  short VopNo, VopChnNo;
  int nResult;
  if (AuthMaxFax == 0)
    return 1;
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendAppendFax2TestPhone(nChn, FileName);
    else
      return POSTAPPENDFAX(pChn->ChType, pChn->ChIndex, 0, FileName, FaxCSID, Resolution, TotalPages, PageList, FaxHeader, FaxFooter, BarCode);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (pVopGroup->m_pVops[VopNo].m_pVopChns[VopChnNo].GetlnState() != 0)
      {
        return 3; //阻塞
      }
      if (VopNo == 0)
      {
        nResult = POSTAPPENDFAX(0, VopChnNo, 0, FileName, FaxCSID, Resolution, TotalPages, PageList, FaxHeader, FaxFooter, BarCode);
      }
      else
      {
        nResult = SendAppendFax2VOP(nChn, VopNo, VopChnNo, FileName, FaxCSID, 0, "");
      }
      return nResult;
    }
    else
    {
      MyTrace(3, "AppendFAX GetVopChnBynChn fail nChn=%d", nChn);
    }
  }
  return 2;
}
int RecvFAX(int nChn, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR BarCode)
{
  short VopNo, VopChnNo;
  int nResult;
  if (AuthMaxFax == 0)
    return 1;
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendRecvFax2TestPhone(nChn, FileName);
    else
      return POSTRECVFAX(pChn->ChType, pChn->ChIndex, 0, FileName, FaxCSID, BarCode);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (pVopGroup->m_pVops[VopNo].m_pVopChns[VopChnNo].GetlnState() != 0)
      {
        return 3; //阻塞
      }
      if (VopNo == 0)
      {
        nResult = POSTRECVFAX(0, VopChnNo, 0, FileName, FaxCSID, BarCode);
      }
      else
      {
        nResult = SendRecvFax2VOP(nChn, VopNo, VopChnNo, FileName, FaxCSID, 0, "");
      }
      return nResult;
    }
  }
  return 2;
}
int StopFAX(int nChn)
{
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  short VopNo, VopChnNo;
  int nResult;
  if (g_nSwitchMode == 0)
  {
    if (g_nCardType == 9)
      return SendStopFax2TestPhone(nChn);
    else
      return POSTSTOPFAX(pChn->ChType, pChn->ChIndex, 0, 0);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (pVopGroup->m_pVops[VopNo].m_pVopChns[VopChnNo].GetlnState() != 0)
      {
        return 3; //阻塞
      }
      if (VopNo == 0)
      {
        nResult = POSTSTOPFAX(0, VopChnNo, 0, 0);
      }
      else
      {
        nResult = 1;
      }
      return nResult;
    }
  }
  return 2;
}
int CheckTone(int nChn, int tonetype, const char *toneparam)
{
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  
  short VopNo, VopChnNo;
  int nResult;
  if (g_nSwitchMode == 0)
  {
    return POSTCHECKTONE(pChn->ChType, pChn->ChIndex, tonetype, toneparam);
  }
  else
  {
    if (GetVopChnBynChn(nChn, VopNo, VopChnNo))
    {
      if (pVopGroup->m_pVops[VopNo].m_pVopChns[VopChnNo].GetlnState() != 0)
      {
        return 3; //阻塞
      }
      if (VopNo == 0)
      {
        nResult = POSTCHECKTONE(0, VopChnNo, tonetype, toneparam);
      }
      else
      {
        nResult = 1;
      }
      return nResult;
    }
  }
  return 2;
}
//------------------會議操作-------------------------------------------------
//創建會議
int CreateConf(int ConfNo, int nChn, int Talkers, int Listeners, int SysConfId)
{
  int result;
  result = CONFCREATE(pChn->ChType, pChn->ChIndex, ConfNo, Talkers, Listeners, SysConfId);
  MyTrace(3, "CreateConf ConfNo=%d;Talkers=%d;Listeners=%d result=%d", 
    ConfNo, Talkers, Listeners, result);
  return result;
}
//刪除會議
int DestroyConf(int ConfNo, int nChn, int DestroyId)
{
  int result;
  result = CONFDESTROY(pChn->ChType, pChn->ChIndex, ConfNo, DestroyId);
  MyTrace(3, "DestroyConf ConfNo=%d;DestroyId=%d result=%d", ConfNo, DestroyId, result);
  return result;
}
//參加會議
int JoinConf(int ConfNo, int nChn, int TalkVol)
{
  int result;
  result = CONFJOIN(pChn->ChType, pChn->ChIndex, ConfNo, 0);
  MyTrace(3, "JoinConf ConfNo=%d;nChn=%d;ChType=%d;ChIndex=%d;TalkVol=%d result=%d", 
    ConfNo, nChn, pChn->ChType, pChn->ChIndex, TalkVol, result);
  return result;
}
//退出會議
int UnjoinConf(int ConfNo, int nChn, int IgnoreConfNo)
{
  int result;
  result = CONFUNJOIN(pChn->ChType, pChn->ChIndex, ConfNo, 0);
  MyTrace(3, "UnjoinConf ConfNo=%d;nChn=%d;ChType=%d;ChIndex=%d;IgnoreConfNo=%d result=%d", 
    ConfNo, nChn, pChn->ChType, pChn->ChIndex, IgnoreConfNo, result);
  return result;
}
//旁聽會議
int ListenConf(int ConfNo, int nChn)
{
  int result;
  result = CONFLISTEN(pChn->ChType, pChn->ChIndex, ConfNo, 0);
  MyTrace(3, "ListenConf ConfNo=%d;nChn=%d;ChType=%d;ChIndex=%d result=%d", 
    ConfNo, nChn, pChn->ChType, pChn->ChIndex, result);
  return result;
}
//退出旁聽會議
int UnListenConf(int ConfNo, int nChn, int IgnoreConfNo)
{
  int result;
  result = CONFUNLISTEN(pChn->ChType, pChn->ChIndex, ConfNo, 0);
  MyTrace(3, "UnListenConf ConfNo=%d;nChn=%d;ChType=%d;ChIndex=%d;IgnoreConfNo=%d result=%d", 
    ConfNo, nChn, pChn->ChType, pChn->ChIndex, IgnoreConfNo, result);
  return result;
}
//創建三方會議(返回值：0表示創建失敗 >0表示返回的會議號)
int CreateThreeConf(int nChn)
{
  int nCfc;
  nCfc = pBoxconf->Get_Idle_nCfc();
  if (!pBoxconf->isnCfcAvail(nCfc))
		return 0;
  if (pCfc->state != 0)
    return 0;
  if (CreateConf(nCfc, nChn, 4, 0, 0) != nCfc)
  {
    return 0;
  }

  pCfc->state = 1;
  pCfc->timer = 0;
  
  pCfc->CreateId = 3;
  pCfc->UseId = 1;
  
  pCfc->Name = "三方會議";
  pCfc->Owner = "";
  pCfc->Password = "";

  pCfc->MaxPresiders = 0;
  pCfc->MaxTalkers = 5;
  pCfc->MaxListeners = 0;
  pCfc->FreeId = 2; //當會議室只有2人時退出并自動雙向連接通話
  pCfc->AutoRecordId = 0;
  //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
  MyTrace(3, "Create Three Conf success confno=%d", nCfc);
  return nCfc;
}
//加入三方會議
int JoinThreeConf(int nCfc, int nChn, int nChn1, int nChn2)
{
  if (!pBoxconf->isnCfcAvail(nCfc))
		return 1;
  if (!pBoxchn->isnChnAvail(nChn) || !pBoxchn->isnChnAvail(nChn1) || !pBoxchn->isnChnAvail(nChn2)) 
    return 1;
  if (JoinConf(nCfc, nChn, 100) == nCfc)
  {
    pChn->CfcNo = nCfc;
    pChn->JoinTime = time(0);
    pCfc->Talkers[pCfc->Talkeds] = nChn;
    pChn->JoinType = talker;
    pCfc->Talkeds ++;
  }
  else
  {
    return 1;
  }
  if (JoinConf(nCfc, nChn1, 100) == nCfc)
  {
    pChn1->CfcNo = nCfc;
    pChn1->JoinTime = time(0);
    pCfc->Talkers[pCfc->Talkeds] = nChn1;
    pChn1->JoinType = talker;
    pCfc->Talkeds ++;
  }
  else
  {
    return 1;
  }
  if (JoinConf(nCfc, nChn2, 100) == nCfc)
  {
    pChn2->CfcNo = nCfc;
    pChn2->JoinTime = time(0);
    pCfc->Talkers[pCfc->Talkeds] = nChn2;
    pChn2->JoinType = talker;
    pCfc->Talkeds ++;
  }
  else
  {
    return 1;
  }
  MyTrace(3, "JoinThreeConf success confno=%d nChn=%d nChn1=%d nChn2=%d", 
    nCfc, pCfc->Talkers[0], pCfc->Talkers[1], pCfc->Talkers[2]);
  return 0;
}
//-----------------------------------------------------------------------------
//刪除會議成員數據
int DelConfMember(int nCfc, int nChn, int &Jointype)
{
  int i, j;

  if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
  if (!pBoxchn->isnChnAvail(nChn)) return 1;
  //是否為主持人
  for (i = 0; i < MAX_PRESIDER_NUM; i ++)
  {
    if (pCfc->Presiders[i] == nChn)
    {
      for (j = i; j < MAX_PRESIDER_NUM - 1; j ++)
      {
        pCfc->Presiders[j] = pCfc->Presiders[j+1];
      }
      pCfc->Presiders[MAX_PRESIDER_NUM-1] = 0xFFFF;
      if (pCfc->Presideds > 0)
        pCfc->Presideds --;
      pChn->CfcNo = 0xFFFF;
      pChn->JoinType = 0;
      Jointype = presider;
      pChn->JoinTime = time(0);
      return 0;
    }
  }
  //是否為參加人
  for (i = 0; i < MAX_TALKERS_NUM; i ++)
  {
    if (pCfc->Talkers[i] == nChn)
    {
      for (j = i; j < MAX_TALKERS_NUM - 1; j ++)
      {
        pCfc->Talkers[j] = pCfc->Talkers[j+1];
      }
      pCfc->Talkers[MAX_TALKERS_NUM-1] = 0xFFFF;
      if (pCfc->Talkeds > 0)
        pCfc->Talkeds --;
      pChn->CfcNo = 0xFFFF;
      pChn->JoinType = 0;
      Jointype = talker;
      pChn->JoinTime= 0;
      return 0;
    }
  }
  //是否為旁聽人
  for (i = 0; i < MAX_LISTENERS_NUM; i ++)
  {
    if (pCfc->Listeners[i] == nChn)
    {
      for (j = i; j < MAX_LISTENERS_NUM - 1; j ++)
      {
        pCfc->Listeners[j] = pCfc->Listeners[j+1];
        pCfc->EmptyEvent[j] = pCfc->EmptyEvent[j+1];
      }
      pCfc->Listeners[MAX_LISTENERS_NUM-1] = 0xFFFF;
      pCfc->EmptyEvent[MAX_LISTENERS_NUM-1] = 0;
      if (pCfc->Listeneds > 0)
        pCfc->Listeneds --;
      pChn->CfcNo = 0xFFFF;
      pChn->JoinType = 0;
      Jointype = listener;
      pChn->JoinTime = time(0);
      return 0;
    }
  }
  return 1;
}
//刪除會議成員數據
int DelAllConfMember(int nCfc, int destroyid)
{
  int nChn;
  int i;

  //if (!pBoxconf->isnCfcAvail(nCfc)) return 1;
  //是否為主持人
  for (i = 0; i < MAX_PRESIDER_NUM; i ++)
  {
    nChn = pCfc->Presiders[i];
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->CfcNo = 0xFFFF;
      pChn->JoinType = 0;
      pChn->JoinTime = 0;
      if (destroyid == 1) //是否需要釋放該通道
        Release_Chn(nChn, 1, 1);
    }
  }
  //是否為參加人
  for (i = 0; i < MAX_TALKERS_NUM; i ++)
  {
    nChn = pCfc->Talkers[i];
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->CfcNo = 0xFFFF;
      pChn->JoinType = 0;
      pChn->JoinTime = 0;
      if (destroyid == 1) //是否需要釋放該通道
        Release_Chn(nChn, 1, 1);
    }
  }
  //是否為旁聽人
  for (i = 0; i < MAX_LISTENERS_NUM; i ++)
  {
    nChn = pCfc->Listeners[i];
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->CfcNo = 0xFFFF;
      pChn->JoinType = 0;
      pChn->JoinTime = 0;
      if (destroyid == 1) //是否需要釋放該通道
        Release_Chn(nChn, 1, 1);
    }
  }
  return 1;
}
void ClearTransferLink(int nChn)
{
  int nChn1, nChn2, nChn3, nAG, nAG1, nOut, nResult;

  if (!pBoxchn->isnChnAvail(nChn)) return;
  //處理與轉接相關的操作
  if (pChn->TranStatus == 0)
  {
    //未處于轉接狀態
    nChn1 = pChn->HoldingnChn;
    if (pBoxchn->isnChnAvail(nChn1))
    {
      pChn->HoldingnChn = 0xFFFF;
      pChn1->HoldingnChn = 0xFFFF;
      if (pChn1->SessionNo == 0)
        Release_Chn(nChn1, 1, 1);
      else
        SendLinkChnEvent(nChn1, OnLinkHangon, "");
    }
  }
  else if (pChn->TranStatus == 1)
  {
    //先掛機的是:被轉接方
    nChn1 = pChn->TranControlnChn; //控制轉接方通道
    if (pBoxchn->isnChnAvail(nChn1))
    {
      nChn2 = pChn1->TranDestionnChn; //轉接目的方通道

      pChn1->TranStatus = 0;
      pChn1->TranControlnChn = 0xFFFF;
      pChn1->TranDestionnChn = 0xFFFF;
      
      Release_Chn(nChn1, 1, 1);

      if (pBoxchn->isnChnAvail(nChn2))
      {
        pChn2->TranStatus = 0;
        pChn2->TranControlnChn = 0xFFFF;
        Release_Chn(nChn2, 1, 1);
      }
    }
    pChn->TranStatus = 0;
    pChn->TranControlnChn = 0xFFFF;
  }
  else if (pChn->TranStatus == 2)
  {
    //先掛機的是:轉接目的方
    nChn1 = pChn->TranControlnChn; //控制轉接方通道
    if (pBoxchn->isnChnAvail(nChn1))
    {
      nChn2 = pChn1->TranControlnChn; //被轉接方通道
      nChn3 = pChn1->LinkChn[0]; //接控制方正在通話的通道
      
      if (pBoxchn->isnChnAvail(nChn2))
      {
        if (nChn2 == nChn3)
        {
          //如果轉接控制方正在與被轉接方通話,則清除轉接相關標志
          pChn1->TranStatus = 0;
          pChn1->TranControlnChn = 0xFFFF;
          pChn1->TranDestionnChn = 0xFFFF;
          
          pChn2->TranStatus = 0;
          pChn2->TranControlnChn = 0xFFFF;
          pChn2->TranDestionnChn = 0xFFFF;
        }
        else if (nChn == nChn3)
        {
          pChn1->TranStatus = 0;
          pChn1->TranControlnChn = 0xFFFF;
          pChn1->TranDestionnChn = 0xFFFF;
          
          pChn2->TranStatus = 0;
          pChn2->TranControlnChn = 0xFFFF;
          pChn2->TranDestionnChn = 0xFFFF;

          //如果轉接控制方正在與轉接的目的方通話,則先斷開通話然后與被轉接方通話,清除轉接相關標志
          UnRouterTalk(nChn1, nChn);
          pChn->LinkType[0] = 0;
          pChn->LinkChn[0] = 0xFFFF;
          pChn1->LinkType[0] = 0;
          pChn1->LinkChn[0] = 0xFFFF;

          StopClearPlayDtmfBuf(nChn2);
          nResult = RouterTalk(nChn1, nChn2);
          if (nResult == 0)
          {
            pChn1->RecMixerFlag = 1;
            pChn1->LinkType[0] = 1;
            pChn1->LinkChn[0] = nChn2;
            pChn1->HoldingnChn = 0xFFFF;
            
            pChn2->RecMixerFlag = 1;
            pChn2->LinkType[0] = 1;
            pChn2->LinkChn[0] = nChn1;
            pChn2->HoldingnChn = 0xFFFF;
            
            nAG = pChn1->nAG;
            if (pAgentMng->isnAGAvail(nAG))
            {
              WriteSeatStatus(pAG, AG_STATUS_TALK, 0);
              pAG->SetAgentsvState(AG_SV_CONN);
              SendCommonResult(nAG, AGMSG_onhold, OnSuccess, "unhold");
              DispAgentStatus(nAG);
            }
          }
        }
      }
    }
    pChn->TranStatus = 0;
  }
  else if (pChn->TranStatus == 3)
  {
    //先掛機的是:控制轉接方,未收到振鈴就先掛機了
    nOut = pChn->nOut;
    nChn1 = pChn->TranControlnChn; //被轉接方通道

    pOut->Session.SessionNo = 0;
    pOut->Session.CmdAddr = 0;
    pOut->Session.nChn = nChn1;

    pOut->TranCallFailReturn = pIVRCfg->TranCallFailReturnType;
    pOut->nAG = pChn->nAG;
    
    pChn1->TranStatus = 0;
    pChn1->nOut = nOut;
    pChn1->TranControlnChn = 0xFFFF;
    pChn1->HoldingnChn = 0xFFFF;

    pChn->TranStatus = 0;
    pChn->nOut = 0xFFFF;
    pChn->TranControlnChn = 0xFFFF;
    pChn->HoldingnChn = 0xFFFF;
    Release_Chn(nChn, 1, 1);

    DBUpdateCallCDR_RelReason(nChn1, 2);
    DBUpdateCallCDR_RelTime(nChn1);
    //插入新的話單
    if (strlen(pOut->Session.CdrSerialNo) > 0)
    {
      strcpy(pChn1->CdrSerialNo, pOut->Session.CdrSerialNo);
      strcpy(pChn1->RecordFileName, pOut->Session.RecordFileName);
    }
    else
    {
      pChn1->GenerateCdrSerialNo();
    }
    
    nAG1 = pChn->nAG;
    if (pAgentMng->isnAGAvail(nAG1))
    {
      DBInsertCallCDR(nChn1, 1, 3, pAG1->GetWorkerNoStr());
    }
    else
    {
      DBInsertCallCDR(nChn1, 1, 3);
    }
    DBUpdateCallCDR_ACDTime(nChn1, 0, 0);
    if (pOut->Calleds[0].CalledType == 1)
    {
      //轉接的是外線
      DBUpdateCallCDR_WorkerNo(nChn1, pOut->Calleds[0].CalledNo, pOut->Calleds[0].CalledNo);
    }
    else
    {
      //轉接的是坐席
      DBUpdateCallCDR_WorkerNo(nChn1, pChn1->nAG);
    }
    SendCdrSerialNo(nChn1);
  }
  else if (pChn->TranStatus == 4)
  {
    //先掛機的是:控制轉接方,收到振鈴就掛機了
    nOut = pChn->nOut;
    nChn1 = pChn->TranControlnChn; //被轉接方通道
    nChn2 = pOut->Calleds[0].OutnChn;
    
    pOut->Session.SessionNo = 0;
    pOut->Session.CmdAddr = 0;
    pOut->Session.nChn = nChn1;
    
    pOut->TranCallFailReturn = pIVRCfg->TranCallFailReturnType;
    pOut->nAG = pChn->nAG;
    
    pChn1->TranStatus = 0;
    pChn1->nOut = nOut;
    pChn1->TranControlnChn = 0xFFFF;
    pChn1->HoldingnChn = 0xFFFF;
    
    pChn->TranStatus = 0;
    pChn->nOut = 0xFFFF;
    pChn->TranControlnChn = 0xFFFF;
    pChn->HoldingnChn = 0xFFFF;

    if (pOut->Calleds[0].CalledType == 1)
    {
      //是轉接的外線,則先要斷開通道連接
      UnRouterTalk(nChn, nChn2);
      pChn->LinkType[0] = 0;
      pChn->LinkChn[0] = 0xFFFF;
      pChn2->LinkType[0] = 0;
      pChn2->LinkChn[0] = 0xFFFF;
    }
    Release_Chn(nChn, 1, 1);

    DBUpdateCallCDR_RelReason(nChn1, 2);
    DBUpdateCallCDR_RelTime(nChn1);
    //插入新的話單
    if (strlen(pOut->Session.CdrSerialNo) > 0)
    {
      strcpy(pChn1->CdrSerialNo, pOut->Session.CdrSerialNo);
      strcpy(pChn1->RecordFileName, pOut->Session.RecordFileName);
    }
    else
    {
      pChn1->GenerateCdrSerialNo();
    }
    nAG1 = pChn->nAG;
    if (pAgentMng->isnAGAvail(nAG1))
    {
      DBInsertCallCDR(nChn1, 1, 3, pAG1->GetWorkerNoStr());
    }
    else
    {
      DBInsertCallCDR(nChn1, 1, 3);
    }
    DBUpdateCallCDR_ACDTime(nChn1, 0, 0);
    if (pOut->Calleds[0].CalledType == 1)
    {
      //轉接的是外線
      DBUpdateCallCDR_WorkerNo(nChn1, pOut->Calleds[0].CalledNo, pOut->Calleds[0].CalledNo);
    }
    else
    {
      //轉接的是坐席
      DBUpdateCallCDR_WorkerNo(nChn1, pChn1->nAG);
    }
    nAG = pChn2->nAG;
    if (pAgentMng->isnAGAvail(nAG))
    {
      pAG->SetCDRSerialNo(pChn1->CdrSerialNo);
      WriteSeatStatus(pAG, AG_STATUS_INRING, 0, 1, 0);
      pAG->SetAgentsvState(AG_SV_INRING);
      DispAgentStatus(nAG);
    }
    SendCdrSerialNo(nChn1);
  }
  else if (pChn->TranStatus == 5)
  {
    //先掛機的是:控制轉接方,轉接成功,已與目的方通話
    pChn->TranStatus = 0;
    nChn1 = pChn->TranControlnChn; //被轉接方通道
    nChn2 = pChn->TranDestionnChn; //轉接目的通道
    nChn3 = pChn->LinkChn[0]; //接控制方正在通話的通道
    if (pBoxchn->isnChnAvail(nChn1) && pBoxchn->isnChnAvail(nChn2))
    {
      if (nChn3 == nChn1)
      {
        //如果轉接控制方正在與被轉接方通話,則先斷開通話,然后將被轉接方與轉接目的方通話,清除轉接相關標志,轉接成功,需要寫新的通話記錄
        UnRouterTalk(nChn, nChn1);
        pChn->LinkType[0] = 0;
        pChn->LinkChn[0] = 0xFFFF;
        pChn1->LinkType[0] = 0;
        pChn1->LinkChn[0] = 0xFFFF;

        StopClearPlayDtmfBuf(nChn2);
        nResult = RouterTalk(nChn1, nChn2);
        if (nResult == 0)
        {
          pChn1->RecMixerFlag = 1;
          pChn1->LinkType[0] = 1;
          pChn1->LinkChn[0] = nChn2;
          pChn1->HoldingnChn = 0xFFFF;
          
          pChn2->RecMixerFlag = 1;
          pChn2->LinkType[0] = 1;
          pChn2->LinkChn[0] = nChn1;
          pChn2->HoldingnChn = 0xFFFF;
        }
      } 
      else if (nChn3 == nChn2)
      {
        //如果轉接控制方正在與轉接目的方通話,則先斷開通話,然后將被轉接方與轉接目的方通話,清除轉接相關標志,轉接成功,需要寫新的通話記錄
        UnRouterTalk(nChn, nChn2);
        pChn->LinkType[0] = 0;
        pChn->LinkChn[0] = 0xFFFF;
        pChn2->LinkType[0] = 0;
        pChn2->LinkChn[0] = 0xFFFF;
        
        StopClearPlayDtmfBuf(nChn1);
        nResult = RouterTalk(nChn1, nChn2);
        if (nResult == 0)
        {
          pChn1->RecMixerFlag = 1;
          pChn1->LinkType[0] = 1;
          pChn1->LinkChn[0] = nChn2;
          pChn1->HoldingnChn = 0xFFFF;
          
          pChn2->RecMixerFlag = 1;
          pChn2->LinkType[0] = 1;
          pChn2->LinkChn[0] = nChn1;
          pChn2->HoldingnChn = 0xFFFF;
        }
      }
      pChn1->TranStatus = 0;
      pChn1->TranControlnChn = 0xFFFF;
      pChn2->TranStatus = 0;
      pChn2->TranControlnChn = 0xFFFF;
      
      Release_Chn(nChn, 1, 1);
      
      DBUpdateCallCDR_RelReason(nChn1, 2);
      DBUpdateCallCDR_RelTime(nChn1);
      //插入新的話單
      MyTrace(3, "ClearTransferLink nChn=%d nChn1=%d nChn2=%d TranCdrSerialNo=%s TranRecordFileName=%s",
        nChn, nChn1, nChn2, pChn->TranCdrSerialNo, pChn->TranRecordFileName);
      if (strlen(pChn2->TranCdrSerialNo) > 0)
      {
        strcpy(pChn1->CdrSerialNo, pChn2->TranCdrSerialNo);
        strcpy(pChn1->RecordFileName, pChn2->TranRecordFileName);
      }
      else
      {
        pChn1->GenerateCdrSerialNo();
      }
      nAG1 = pChn->nAG;
      if (pAgentMng->isnAGAvail(nAG1))
      {
        DBInsertCallCDR(nChn1, 1, 3, pAG1->GetWorkerNoStr());
      }
      else
      {
        DBInsertCallCDR(nChn1, 1, 3);
      }
      DBUpdateCallCDR_ACDTime(nChn1, 0, 0);
      nAG = pChn2->nAG;
      if (pAgentMng->isnAGAvail(nAG))
      {
        //轉接的是坐席
        pAG->SetCDRSerialNo(pChn1->CdrSerialNo);
        strcpy(pAG->CdrSerialNo, pChn1->CdrSerialNo);
        DBUpdateCallCDR_WorkerNo(nChn1, nAG);
        DBUpdateCallCDR_SeatAnsTime(nAG, pAG->GetAcdedGroupNo());
        
        if (g_nSwitchMode == 0 && pAG->m_Worker.AutoRecordId == 1 && pAG->m_Seat.AutoRecordId == 1)
        {
          char szTemp[256], pszError[128];
          
          //pChn1->GenerateRecordFileName(); //del by zgj 2011-07-22
          strcpy(pAG->RecordFileName, pChn1->RecordFileName);
          
          sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAG->RecordFileName);
          CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
          
          if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
          {
            if (SetChnRecordRule(pAG->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
            {
              DBUpdateCallCDR_RecdFile(pAG->nAG);
            }
          }
        }
        WriteSeatStatus(pAG, AG_STATUS_TALK, 1, 1, 2);
        pAG->SetAgentsvState(AG_SV_CONN);
        DispAgentStatus(nAG);
      }
      else
      {
        //轉接的是外線
        DBUpdateCallCDR_WorkerNo(nChn1, pChn2->CalledNo, pChn2->CalledNo);
        DBUpdateCallCDR_SeatAnsTime(nChn1, pChn2->CalledNo, pChn2->CalledNo);
        pChn2->TranOutForAgent = 1;
        strcpy(pChn2->CdrSerialNo, pChn1->CdrSerialNo);
        if (g_nSwitchMode == 0 && pIVRCfg->isRecordTranOutCall == true)
        {
          char szTemp[256], pszError[128];
          
          sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pChn1->RecordFileName);
          CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
          
          if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
          {
            if (SetChnRecordRule(nChn2, 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
            {
              DBUpdateCallCDR_ChnRecdFile(nChn1);
            }
          }
        }
      }
      SendCdrSerialNo(nChn1);
    }
  }
}
//退出已加入的會議、斷開已經交換的通道(RelId=1 時表示由于本系統掛機)
void UnLink_All_By_ChnNo(int nChn, int RelId)
{
	int JoinType;
	int nChn1, nChn2, nCfc; 
	int i, nResult;

	if (!pBoxchn->isnChnAvail(nChn)) return;
	if (pChn->state == 0) return;
	if (pBoxconf->isnCfcAvail(pChn->CfcNo) && pChn->JoinType != 0)
  {
    nCfc = pChn->CfcNo;
    DelConfMember(nCfc, nChn, JoinType);
    //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    //theApp.m_MyConfStatusDialog->ProcDispMenber(nCfc, nChn, 0);
    if (JoinType == listener)
    {
      UnListenConf(nCfc, nChn, 1);
    }
    else
    {
      if ((pCfc->Presideds+pCfc->Talkeds) == 0 && pCfc->AutoRecordId == 1 
        && pCfc->curRecData.state == 1 && pCfc->curRecData.RecState != 0)
      {
        Stop_Rec_Clear_ConfBuf(nCfc);
      }
      UnjoinConf(nCfc, nChn, 1);
      if (pCfc->Presideds + pCfc->Talkeds == 1)
      {
        ConfPlayKnockTone(nCfc, 911); //放會議只有1人提示音
        if (pCfc->Presideds == 1)
        {
          nChn1 = pCfc->Presiders[0];
          //發送會議室由多人邊1人消息
          SendJoinConfResult(nChn1, OnCfcMany2One, "");
        }
        else
        {
          nChn1 = pCfc->Talkers[0];
          //發送會議室由多人邊1人消息
          SendJoinConfResult(nChn1, OnCfcMany2One, "");
        }
      }
      //發送聊天室有空位事件
      if (pCfc->Presideds+pCfc->Talkeds == pCfc->MaxPresiders+pCfc->MaxTalkers - 1)
      {
        for (int i = 0; i < MAX_LISTENERS_NUM; i ++)
        {
          if (pCfc->EmptyEvent[i] == 1 && pBoxchn->isnChnAvail(pCfc->Listeners[i]))
          {
		        nChn1 = pCfc->Listeners[i];
            //發送會議室有空位消息
            SendJoinConfResult(nChn1, OnCfcBusy2Idle, "");
            break;
          }
        }
      }
    }
    MyTrace(3, "confno=%d nChn=%d nChn1=%d nChn2=%d", 
      nCfc, pCfc->Talkers[0], pCfc->Talkers[1], pCfc->Talkers[2]);
    if (pCfc->Presideds+pCfc->Talkeds+pCfc->Listeneds == 0 && pCfc->FreeId == 1)
    {
      //會議室無人并且需要自動刪除會議
      if (DestroyConf(nCfc, 0, 1) == nCfc)
      {
        pBoxconf->InitConf(nCfc);
      }
    }
    else if (pCfc->Talkeds == 2 && pCfc->FreeId == 2)
    {
      //會議室只有2人并且需要自動刪除會議,并雙向通道交換
      nChn1 = pCfc->Talkers[0];
      nChn2 = pCfc->Talkers[1];
      if (pBoxchn->isnChnAvail(nChn1) && pBoxchn->isnChnAvail(nChn2))
      {
        DelConfMember(nCfc, nChn1, JoinType);
        UnjoinConf(nCfc, nChn1, 1);
        DelConfMember(nCfc, nChn2, JoinType);
        UnjoinConf(nCfc, nChn2, 1);
        if (DestroyConf(nCfc, 0, 1) == nCfc)
        {
          pBoxconf->InitConf(nCfc);
        }
        nResult = RouterTalk(nChn1, nChn2);
        if (nResult == 0)
        {
          pChn1->RecMixerFlag = 1;
          pChn1->LinkType[0] = 1;
          pChn1->LinkChn[0] = nChn2;
          pChn2->RecMixerFlag = 1;
          pChn2->LinkType[0] = 1;
          pChn2->LinkChn[0] = nChn1;
        }
      }
    }
  }

  if (pChn->LinkType[0] == 1) //雙向通話
	{
		//結束雙向通話
		nChn1 = pChn->LinkChn[0];
		if (pBoxchn->isnChnAvail(nChn1))
		{
			UnRouterTalk(nChn, nChn1);
			pChn->LinkType[0] = 0;
			pChn->LinkChn[0] = 0xFFFF;
			pChn1->LinkType[0] = 0;
			pChn1->LinkChn[0] = 0xFFFF;

		  if (RelId == 1)
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnLinkHangon, "");
      }
      else
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnUnLink, "");
      }
		}
	}
  else if (pChn->LinkType[0] == 6 || pChn->LinkType[0] == 7) //彩話
  {
		nChn1 = pChn->LinkChn[0];
		if (pBoxchn->isnChnAvail(nChn1))
		{
			UnRouterCbm(nChn, nChn1);
      if (pChn->PlayBusFlag == 1)
      {
        SetPlayBus(nChn, 0);
        pChn->PlayBusFlag = 0;
      }
      pChn->LinkType[0] = 0;
			pChn->LinkChn[0] = 0xFFFF;
      if (pChn1->PlayBusFlag == 1)
      {
        SetPlayBus(nChn1, 0);
        pChn1->PlayBusFlag = 0;
      }
			pChn1->LinkType[0] = 0;
			pChn1->LinkChn[0] = 0xFFFF;

      if (RelId == 1)
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnLinkHangon, "");
      }
      else
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnUnLink, "");
      }
		}
  }
	else if (pChn->LinkType[0] == 2) //監聽對方
	{
		//結束監聽對方
		nChn1 = pChn->LinkChn[0];
		if (pBoxchn->isnChnAvail(nChn1))
		{
      UnRouterMonitor(nChn, nChn1);
      pChn->LinkType[0] = 0;
	    pChn->LinkChn[0] = 0xFFFF;
      for (i = 2; i < MAX_LINK_CHN_NUM; i ++)
      {
        if (pChn1->LinkChn[i] == nChn)
        {
          pChn1->LinkType[i] = 0;
	        pChn1->LinkChn[i] = 0xFFFF;
        }
      }
		}
    nChn1 = pChn->LinkChn[1];
    if (pBoxchn->isnChnAvail(nChn1))
    {
      UnRouterMonitor(nChn, nChn1);
      pChn->LinkType[1] = 0;
      pChn->LinkChn[1] = 0xFFFF;
      for (i = 2; i < MAX_LINK_CHN_NUM; i ++)
      {
        if (pChn1->LinkChn[i] == nChn)
        {
          pChn1->LinkType[i] = 0;
          pChn1->LinkChn[i] = 0xFFFF;
        }
      }
    }
	}
  else if (pChn->LinkType[0] == 8 || pChn->LinkType[0] == 9) //傳真
  {
		pChn->LinkType[0] = 0;
		pChn->LinkChn[0] = 0xFFFF;
    StopFAX(nChn);
  }
  else if (pChn->LinkType[0] == 10 || pChn->LinkType[0] == 11) //變聲
  {
		nChn1 = pChn->LinkChn[0];
		if (pBoxchn->isnChnAvail(nChn1))
		{
			UnRouterVC(nChn, nChn1);
      pChn->LinkType[0] = 0;
			pChn->LinkChn[0] = 0xFFFF;
			pChn1->LinkType[0] = 0;
			pChn1->LinkChn[0] = 0xFFFF;

      if (RelId == 1)
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnLinkHangon, "");
      }
      else
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnUnLink, "");
      }
		}
  }
  if (pChn->LinkType[1] == 2) //監聽另外一方
  {
    //結束監聽對方
    nChn1 = pChn->LinkChn[1];
    if (pBoxchn->isnChnAvail(nChn1))
    {
      UnRouterMonitor(nChn, nChn1);
      pChn->LinkType[1] = 0;
      pChn->LinkChn[1] = 0xFFFF;
      for (i = 2; i < MAX_LINK_CHN_NUM; i ++)
      {
        if (pChn1->LinkChn[i] == nChn)
        {
          pChn1->LinkType[i] = 0;
          pChn1->LinkChn[i] = 0xFFFF;
        }
      }
    }
  }
  //被對方監聽
  for (i = 2; i < MAX_LINK_CHN_NUM; i ++)
  {
    if (pChn->LinkType[i] == 3 && pBoxchn->isnChnAvail(pChn->LinkChn[i]))
    {
      nChn1 = pChn->LinkChn[i];
      UnRouterMonitor(nChn1, nChn);
      pChn1->LinkType[0] = 0;
	    pChn1->LinkChn[0] = 0xFFFF;
      pChn->LinkType[i] = 0;
      pChn->LinkChn[i] = 0xFFFF;
      if (RelId == 1)
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnLinkHangon, "");
      }
      else
      {
        if (pChn1->SessionNo == 0)
          Release_Chn(nChn1, 1, 1);
        else
          SendLinkChnEvent(nChn1, OnUnLink, "");
      }
    }
  }
}
//釋放通道處理SendRelId:是否向底層通道發送釋放指令(0-否,1-是),SendMsgId是否發送釋放事件給流程解析器(0-否,1-是)
void Release_Chn(int nChn, int SendRelId, int SendMsgId)
{
	CAgent *pAgent=NULL;
  CIVRCallCountParam *pIVRCallCountParam=NULL;

  if (!pBoxchn->isnChnAvail(nChn)) return;

  if (pChn->lgChnType == CHANNEL_IVR && pChn->ssState != 0)
  {
    if (pChn->IncIVRInTranAGCountId == 1 && pChn->IncIVRInTranAGAbandonCountId == 0)
    {
      if (pChn->CallInOut == 1)
      {
        if (gIVRCallCountParamMng.GetIvrNoType == 1)
        {
          MyTrace(3, "IncIVRInTranAGAbandonCount(%s,%d)", pChn->CalledNo, (int)(time(0)-pChn->IVRInTranAGTime));
          pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGAbandonCount(pChn->CalledNo, (int)(time(0)-pChn->IVRInTranAGTime));
        }
        else
        {
          MyTrace(3, "IncIVRInTranAGAbandonCount(%s,%d)", pChn->OrgCalledNo.C_Str(), (int)(time(0)-pChn->IVRInTranAGTime));
          pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGAbandonCount(pChn->OrgCalledNo.C_Str(), (int)(time(0)-pChn->IVRInTranAGTime));
        }
      }
      else if (pChn->CallInOut == 2)
      {
        MyTrace(3, "IncIVRInTranAGAbandonCount(%s,%d)", pChn->CallerNo, (int)(time(0)-pChn->IVRInTranAGTime));
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGAbandonCount(pChn->CallerNo, (int)(time(0)-pChn->IVRInTranAGTime));
      }
      pChn->IncIVRInTranAGAbandonCountId = 1;
      SendIVRCallCountData(pIVRCallCountParam);
    }
    else
    {
      if (pChn->CallInOut == 1)
      {
        if (gIVRCallCountParamMng.GetIvrNoType == 1)
          pIVRCallCountParam = gIVRCallCountParamMng.GetIVRCallCountParam(pChn->CalledNo);
        else
          pIVRCallCountParam = gIVRCallCountParamMng.GetIVRCallCountParam(pChn->OrgCalledNo.C_Str());
      }
      else if (pChn->CallInOut == 2)
      {
        pIVRCallCountParam = gIVRCallCountParamMng.GetIVRCallCountParam(pChn->CallerNo);
      }
    }
  }

  if (pChn->VOC_TrunkId == 0 && (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE))
  {
    //交換機版本處理
    int nChn1 = pChn->SwtLinkChn[0];
    if (pBoxchn->isnChnAvail(nChn1))
    {
      if (pChn1->lgChnType == CHANNEL_TRUNK)
      {
        DBUpdateCallCDR_UpdateFlag(nChn1);
        pBoxchn->ResetChn(nChn1);
        DispChnStatus(nChn1);
      }
      else
      {
        pChn1->LinkType[0] = 0;
        pChn1->SwtLinkChn[0] = 0xFFFF;
      }
    }

    pAgent = GetAgentBynChn(nChn);
    if (pChn->lgChnType == CHANNEL_SEAT || pChn->lgChnType == CHANNEL_IVR || pChn->lgChnType == CHANNEL_REC)
    {
      CVopChn *pVopChn = pChn->cVopChn;
      if (pVopChn)
      {
        pVopChn->SetssState(0);
        StopClearPlayDtmfRecBuf(nChn);
        pVopChn->SetsvState(0);
        //memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
        //memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
        SendVopStateToAll(pVopChn->m_nVopNo, pVopChn->m_nVopChnNo);
        SendVOPStateToOppIVR(pVopChn->m_nVopNo, pVopChn->m_nVopChnNo);
      }
      else
      {
        int nChn1 = pChn->nSwtPortID;
        if (pBoxchn->isnChnAvail(nChn1))
        {
          if (pChn1->cVopChn)
          {
            if (pChn1->cVopChn->m_nVopChnType == 6 || pChn1->cVopChn->m_nVopChnType == 7)
            {
              if (pChn1->cVopChn->m_nVopChnType == 6) //2016-10-08 如果是數字中繼監錄就不在這里停止錄音了，在底層dll里檢測到掛機后停止錄音
                StopClearPlayDtmfRecBuf(nChn1);
              pChn1->cVopChn->SetsvState(0);
              //memset(pChn1->cVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
              //memset(pChn1->cVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
              SendVopStateToAll(pChn1->cVopChn->m_nVopNo, pChn1->cVopChn->m_nVopChnNo);
              SendVOPStateToOppIVR(pChn1->cVopChn->m_nVopNo, pChn1->cVopChn->m_nVopChnNo);
            }
          }
        }
      }
    }
    else if (pChn->lgChnType == CHANNEL_TRUNK)
    {
      if (pChn->cVopChn)
      {
        if (pChn->cVopChn->m_nVopChnType == 6 || pChn->cVopChn->m_nVopChnType == 7)
        {
          StopClearPlayDtmfRecBuf(nChn);
          //memset(pChn->cVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
          //memset(pChn->cVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
          pChn->cVopChn->SetsvState(0);
          SendVopStateToAll(pChn->cVopChn->m_nVopNo, pChn->cVopChn->m_nVopChnNo);
          SendVOPStateToOppIVR(pChn->cVopChn->m_nVopNo, pChn->cVopChn->m_nVopChnNo);
        }
      }
    }
    //將該通道拆線
    if (pChn->ssState != CHN_SV_IDEL)
    {
      if (SendRelId == 1)
      {
        Hangup(nChn);
      }
      if (SendMsgId == 1)
      {
        SendChnHangonEvent(nChn);
      }
    }
    if (pAgent != NULL)
    {
      memset(pAgent->RecordFileName, 0, MAX_CHAR128_LEN);
      memset(pAgent->m_Seat.IVRPhone, 0, MAX_TELECODE_LEN);
      pAgent->delaytimer = 0;
      //2016-10-21增加判斷話務員是否登錄了 pAgent->isLogin()
      if (pAgent->isLogin() && pAgent->m_Worker.DelayTime > 0 && (pAgent->svState == AG_SV_CONN || pAgent->svState == AG_SV_HOLD || pAgent->svState == AG_SV_IMCHAT || pAgent->svState == AG_SV_PROC_EMAIL || pAgent->svState == AG_SV_PROC_FAX))
      {
        if (pIVRCfg->AgentLogOnOffMode == 0)
        {
          if (pIVRCfg->isSetReadyStateonAgentSetDisturb == 1)
          {
            Send_SWTMSG_setagentstatus(nChn, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNoStr(), "", "", AG_AM_NOT_READY);
          }
        }
        else
        {
          Send_SWTMSG_setagentstatus(nChn, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNoStr(), "", "", AG_AM_WORK_NOT_READY);
        }
        DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
        pAgent->DelayState = 1;
        WriteSeatStatus(pAgent, AG_STATUS_ACW, 0);
        MyTrace(3, "Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d",
          nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime);
      }
      else
      {
        if (pAgent->DelayState != 1) //2014-05-19 增加判斷，防止在主動掛機評價時化后處理狀態無效
        {
          DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
          DBUpdateCallCDR_ACWTime(pAgent->nAG);
          pAgent->DelayState = 2;
        }
      }
      pAgent->PickupId = 0;
      pAgent->TrannChn = 0xFFFF;
      pAgent->nAcd = 0xFFFF; //2015-10-14 用來拒接
      pAgent->SetAgentsvState(AG_SV_IDEL);
      memset(pAgent->ExtRecordServer, 0, MAX_CHAR128_LEN);
      memset(pAgent->ExtRecordFileName, 0, MAX_CHAR128_LEN);
      memset(pAgent->ExtRecordCallID, 0, MAX_CHAR128_LEN);
      pAgent->ClearSrvLine();
      
      if (SendRelId == 0)
        SendSeatHangon(pAgent->nAG);
      DispAgentStatus(pAgent->nAG);

      if (pAgent->m_Worker.TelLogInOutFlag == 1)
      {
        MyTrace(3, "Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
          nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
        pAgent->DelayState = 0;
        pAgent->m_Worker.TelLogInOutFlag = 0;
        pAgent->ChangeStatus(AG_STATUS_LOGIN);
        MyTrace(3, "WriteWorkerLoginStatus nAG=%d Proc_SWTMSG_onagentloggedon1()", pAgent->nAG);
        WriteWorkerLoginStatus(pAgent);
        //WriteSeatStatus(pAgent, AG_STATUS_LOGIN, 0);
      }
      else if (pAgent->m_Worker.TelLogInOutFlag == 2)
      {
        MyTrace(3, "Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
          nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
        pAgent->DelayState = 0;
        pAgent->m_Worker.TelLogInOutFlag = 0;
        WriteSeatStatus(pAgent, AG_STATUS_LOGOUT, 0);
        AgentStatusCount();
        SendOneGroupStatusMeteCount(pAgent->nAG);
        for (int i=1; i<MAX_GROUP_NUM_FORWORKER; i++)
          SendOneGroupStatusMeteCountB(pAgent->m_Worker.PreGroupNoList[i]);
        SendSystemStatusMeteCount();
      }
      if (pAgent->m_Worker.TelLogInOutFlag == 3)
      {
        MyTrace(3, "Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
          nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
        pAgent->DelayState = 0;
        pAgent->m_Worker.TelLogInOutFlag = 0;
        pAgent->ChangeStatus(AG_STATUS_LOGIN);
        MyTrace(3, "WriteWorkerLoginStatus nAG=%d Proc_SWTMSG_onagentloggedon2()", pAgent->nAG);
        WriteWorkerLoginStatus(pAgent);
        //WriteSeatStatus(pAgent, AG_STATUS_LOGIN, 0);
        pAgent->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_BUSY);
      }
      else
      {
        AgentStatusCount();
        SendOneGroupStatusMeteCount(pAgent->nAG);
        SendSystemStatusMeteCount();
      }
      //2015-11-25 針對語音中繼的修改
      Flash(pAgent->m_Seat.nVocTrknChn);
      SendCdrSerialNo(pAgent->m_Seat.nCallInnChn, pAgent->m_Seat.nTranAGnChn);
      ClearCalloutIdBynAG(pAgent->nAG);

      pAgent->ExternalId = 1; //2015-12-26
    }

    //復位通道數據
    if (pIVRCfg->isHangonAutoCancelCall == true)
    {
      int nOut = pChn->nOut;
      if (pCallbuf->isnOutAvail(nOut))
        pOut->CancelId = 2;
      int nAcd = pChn->nAcd;
      if (pCallbuf->isnOutAvail(nAcd))
        pAcd->StopId = 1;
    }
    WriteChnIVRStatus(nChn, AG_STATUS_IVR);
    DBUpdateCallCDR_RelTime(nChn);

    if (pChn->ssState == CHN_SNG_OT_TALK || pChn->ssState == CHN_SNG_IN_TALK)
      UpdateDialOutHangonTime(pChn->ManDialOutId, pChn->ManDialOutListId);
    else if (pChn->ssState == CHN_SNG_OT_RING || pChn->ssState == CHN_SNG_IN_ARRIVE)
      UpdateDialOutFailTime(pChn->ManDialOutId, pChn->ManDialOutListId, 83);
    else
      UpdateDialOutFailTime(pChn->ManDialOutId, pChn->ManDialOutListId, 85);

    pBoxchn->ResetChn(nChn);
    DispChnStatus(nChn);
    
    if (pChn->lgChnType == CHANNEL_TRUNK)
    {
      pBoxchn->pIdelTrkChnQueue->TrkNoJoinInQueueTail(pChn->lgChnNo);
    }
    if (pChn->lgChnType == CHANNEL_IVR)
    {
      SumIVRChnOnlines();
      SendIVRCallCountData(pIVRCallCountParam);
    }
    return;
  }

  //============================板卡版本掛機處理================================================
  StopClearPlayDtmfRecBuf(nChn);
  //清除轉接相關交換連接
  ClearTransferLink(nChn);
	//斷開已連接的通道
	UnLink_All_By_ChnNo(nChn, 1);
  pAgent = GetAgentBynChn(nChn);
	//將該通道拆線
	if (pChn->ssState != CHN_SV_IDEL)
	{
		if (SendRelId == 1)
    {
      if (pAgent == NULL)
      {
        Release(nChn, pChn->HangonReason); //2016-08-11
      }
      else 
      {
        if (!pAgent->isWantSeatType(SEATTYPE_AG_PC) && !pAgent->isWantSeatType(SEATTYPE_RT_TEL))
          Release(nChn, 0);
        //else //純電腦坐席或遠程坐席不主動釋放該通道
      }
    }
    if (SendMsgId == 1)
    {
      SendChnHangonEvent(nChn);
    }
	}
  if (pAgent != NULL)
  {
    MyTrace(3, "Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
      nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);

    pAgent->delaytimer = 0;
    if (pAgent->svState == AG_SV_CONN || pAgent->svState == AG_SV_HOLD || pAgent->svState == AG_SV_IMCHAT || pAgent->svState == AG_SV_PROC_EMAIL || pAgent->svState == AG_SV_PROC_FAX)
    {
      DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
      pAgent->DelayState = 1;
      WriteSeatStatus(pAgent, AG_STATUS_ACW, 0);
    }
    else
    {
      DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
      DBUpdateCallCDR_ACWTime(pAgent->nAG);
      WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
    }
    pAgent->SetAgentsvState(AG_SV_IDEL);
    pAgent->ClearSrvLine();
    
    if (SendRelId == 0 && pAgent->isWantSeatType(SEATTYPE_RT_TEL))
    {
      pChn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
      pAgent->m_Seat.nChn = 0xFFFF;
    }

    if (g_nCardType != 9 && SendRelId == 1 && pAgent->isWantSeatType(SEATTYPE_AG_PC))
    {
      PlayTone(nChn, 903, 0); //純電腦坐席或遠程坐席釋放時放短暫忙音
      SendSeatHangon(pAgent->nAG);
      DispAgentStatus(pAgent->nAG);
      return;
    }
    SendSeatHangon(pAgent->nAG);
    DispAgentStatus(pAgent->nAG);
  }

  //復位通道數據
  if (pIVRCfg->isHangonAutoCancelCall == true)
  {
    int nOut = pChn->nOut;
    if (pCallbuf->isnOutAvail(nOut))
      pOut->CancelId = 2;
    int nAcd = pChn->nAcd;
    if (pCallbuf->isnOutAvail(nAcd))
      pAcd->StopId = 1;
  }
  WriteChnIVRStatus(nChn, AG_STATUS_IVR);
  if (pChn->TranOutForAgent == 1)
    DBUpdateCallCDR_ChnSeatRelTime(nChn);
  DBUpdateCallCDR_RelTime(nChn);
  int lnState = pChn->lnState;

  //2015-11-25 針對語音中繼的修改
  int nAG2 = pChn->VocTrknAG;
  if (pAgentMng->isnAGAvail(nAG2))
  {
    int nChn2 = pAG2->GetSeatnChn();
    Hangup(nChn2);
  }  

  pBoxchn->ResetChn(nChn);
  if (g_nSwitchType == 0 && SendRelId == 1 && pChn->ChStyle == CH_A_SEAT && GetSeatTypeBynChn(nChn) != SEATTYPE_AG_PC)
  {
    MyTrace(3, "Release_Chn nChn=%d lnState=%d", nChn, lnState);
    if (g_nCardType != 9 && lnState == CHN_LN_SEIZE) //2013-04-20 針對前面的狀態已空閑，后面收到關聯的通道釋放的消息就不要置等待釋放狀態了
    {
      pChn->lnState = CHN_LN_WAIT_REL;
      if (pAgent != NULL)
      {
        SetAgentsvState(pAgent, AG_SV_WAIT_REL);
      }
    }
  }
  if (pChn->lgChnType == CHANNEL_TRUNK)
  {
    pBoxchn->pIdelTrkChnQueue->TrkNoJoinInQueueTail(pChn->lgChnNo);
  }

  //2015-09-06
  if (pAgent != NULL && pChn->lnState != CHN_LN_WAIT_REL)
  {
    if (pAgent->m_Worker.TelLogInOutFlag == 1)
    {
      MyTrace(3, "-Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
        nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
      pAgent->DelayState = 0;
      pAgent->m_Worker.TelLogInOutFlag = 0;
      
      pAgent->ChangeStatus(AG_STATUS_LOGIN);
      pAgent->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
      WriteWorkerLoginStatus(pAgent);
      
      strcpy(pAgent->StateChangeTime, MyGetNow());
      AgentStatusCount();
      SendOneGroupStatusMeteCount(pAgent->nAG);
      SendSystemStatusMeteCount();
      
      DispAgentStatus(pAgent->nAG);
    }
    else if (pAgent->m_Worker.TelLogInOutFlag == 2)
    {
      MyTrace(3, "--Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
        nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
      pAgent->DelayState = 0;
      pAgent->m_Worker.TelLogInOutFlag = 0;

      WriteSeatStatus(pAgent, AG_STATUS_LOGOUT, 0);
      WriteWorkerLogoutStatus(pAgent);

      int nGroupNo = pAgent->GetGroupNo();
      if (pAgent->m_Worker.oldWorkerNo == 0)
      {
        pAgent->m_Worker.Logout();
        pAgent->m_Worker.ClearState();
      }
      else
      {
        pAgent->m_Worker.OldWorkerLogin();
      }
      pAgent->m_Worker.OldDisturbId = 0;
      pAgent->m_Worker.OldLeaveId = 0;

      DispAgentStatus(pAgent->nAG);

      AgentStatusCount();
      SendOneGroupStatusMeteCount(pAgent->nAG);
      SendSystemStatusMeteCount();
    }
  }

  DispChnStatus(nChn);
}
