//---------------------------------------------------------------------------
#include "stdafx.h"
#include "proctts.h"
#include "extern.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/************************************************
函數名：ProcTTSMsg
函數功能描述：處理TTS代理服務器來的消息
算法簡要說明：無
訪問的數據表：無
輸入參數：
    msgno：消息類型（格式參考：..\E:\Box\include\vmsg\ivrtts.h）
    msgbuf：消息內容（格式參考：..\E:\Box\include\vmsg\ivrtts.h）
輸出參數：無
返回值：無

修改記錄：
	  修改時間：2007-12-25
	  修改內容：初次編寫
	  修改作者：zgj
***************************************************/
void ProcTTSMsg(US msgtype, const UC * msgbuf)
{
  VXML_ONTTSSTREAM_STRUCT *pOnTTSStream;
  VXML_ONTTSSFILE_STRUCT  *pOnTTSFile;
  VXML_ONGETTTSSAUTHDATA_STRUCT  *pOnGetTTSAuthData;

  VXML_TTSAUTHDATA_STRUCT str_TTSAuthData;

  int DtmfRuleId, PlayRuleId, nChn;

  switch (msgtype&0xFF)
  {
  case TTSMSG_onttsstring: //返回合成的語音流
    pOnTTSStream = (VXML_ONTTSSTREAM_STRUCT *)msgbuf;
	  XMLHeader.MsgId = MSG_ttsstring;
	  XMLHeader.SessionId = pOnTTSStream->SessionId;
	  XMLHeader.CmdAddr = pOnTTSStream->CmdAddr;
	  XMLHeader.ChnType = pOnTTSStream->ChnType;
	  XMLHeader.ChnNo = pOnTTSStream->ChnNo;
	  XMLHeader.ErrorId = 0;
	  XMLHeader.OnlineId = pBoxchn->Get_ChnNo_By_LgChn(XMLHeader.ChnType, XMLHeader.ChnNo, XMLHeader.nChn);
    
    PlayRuleId = pOnTTSStream->PlayRuleId;

    XMLHeader.OnMsgId = MSG_onttsstring;
	  strcpy(XMLHeader.OnMsgName, "onttsstring");

    if (Check_play_MSG(&XMLHeader) != 0)
      break;
	  nChn = XMLHeader.nChn;
    if (pOnTTSStream->Result == 2)
    {
      Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "TTS fail");
      if (PlayRuleId > 0)
        StopClearPlayDtmfBuf(nChn);
      break;
    }
    if (pOnTTSStream->StreamLen == 0)
      break;
    pChn->curPlayData.StreamIndex = pOnTTSStream->VocIndex;
    pChn->curPlayData.StreamIndex ++;
    if (pChn->curPlayData.CurBufId == 0 || pChn->curPlayData.CurBufId == 2)
    {
      memcpy(pChn->curPlayData.TTSBuf[0], &(pOnTTSStream->VocStream), MAX_TTS_STREAM_LEN);
      pChn->curPlayData.TTSResult[0] = pOnTTSStream->Result + 1;
      pChn->curPlayData.StreamLen[0] = pOnTTSStream->StreamLen;
    }
    else
    {
      memcpy(pChn->curPlayData.TTSBuf[1], &(pOnTTSStream->VocStream), MAX_TTS_STREAM_LEN);
      pChn->curPlayData.TTSResult[1] = pOnTTSStream->Result + 1;
      pChn->curPlayData.StreamLen[1] = pOnTTSStream->StreamLen;
    }
    
    if (pChn->curPlayData.CurBufId == 0)
    {
      Start_ChnPlay(nChn);
      if (pOnTTSStream->Result == 0)
        GetNextTTSStream(nChn);
    }
    break;

  case TTSMSG_onttsfile: //返回合成的語音文件
    pOnTTSFile = (VXML_ONTTSSFILE_STRUCT *)msgbuf;
	  XMLHeader.MsgId = MSG_ttsfile;
	  XMLHeader.SessionId = pOnTTSFile->SessionId;
	  XMLHeader.CmdAddr = pOnTTSFile->CmdAddr;
	  XMLHeader.ChnType = pOnTTSFile->ChnType;
	  XMLHeader.ChnNo = pOnTTSFile->ChnNo;
	  XMLHeader.ErrorId = 0;
	  XMLHeader.OnlineId = pBoxchn->Get_ChnNo_By_LgChn(XMLHeader.ChnType, XMLHeader.ChnNo, XMLHeader.nChn);

    DtmfRuleId = pOnTTSFile->DtmfRuleId;
    PlayRuleId = pOnTTSFile->PlayRuleId;
	  
    XMLHeader.OnMsgId = MSG_onttsfile;
	  strcpy(XMLHeader.OnMsgName, "onttsfile");

    MyTrace(1, "TTSRESULT SessionId=%ld CmdAddr=%ld ChnType=%d ChnNo=%d DtmfRuleId=%d PlayRuleId=%d Result=%d VocFile=%s", 
      XMLHeader.SessionId, XMLHeader.CmdAddr, XMLHeader.ChnType, XMLHeader.ChnNo, DtmfRuleId, PlayRuleId, pOnTTSFile->Result, pOnTTSFile->VocFile);

    if (XMLHeader.ChnType == 0 && XMLHeader.ChnNo == 0)
    {
      if (pOnTTSFile->Result == 1)
      {
        sprintf(SendMsgBuf, "<onttsfile sessionid='%lu' cmdaddr='%lu' chantype='%d' channo='%d' result='%d' dtmfbuf='' asrbuf='' ttsbuf='%s' errorbuf=''/>", 
          XMLHeader.SessionId, XMLHeader.CmdAddr, XMLHeader.ChnType, XMLHeader.ChnNo, OnTTSEnd, pOnTTSFile->VocFile);
        SendMsg2XML(MSG_onttsfile, XMLHeader.SessionId, SendMsgBuf);
      }
      else
      {
        sprintf(SendMsgBuf, "<onttsfile sessionid='%lu' cmdaddr='%lu' chantype='%d' channo='%d' result='%d' dtmfbuf='' asrbuf='' ttsbuf='' errorbuf=''/>", 
          XMLHeader.SessionId, XMLHeader.CmdAddr, XMLHeader.ChnType, XMLHeader.ChnNo, OnTTSError);
        SendMsg2XML(MSG_onttsfile, XMLHeader.SessionId, SendMsgBuf);
      }
      break;
    }
    if (Check_play_MSG(&XMLHeader) != 0)
      break;
	  nChn = XMLHeader.nChn;
    if (pChn->curPlayData.WaitTTSResult == 0)
      break;
    if (pOnTTSFile->Result == 2)
    {
      Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "TTS fail");
      if (PlayRuleId > 0)
        StopClearPlayDtmfBuf(nChn);
      break;
    }
    if (PlayRuleId == 0)
    {
      sprintf(SendMsgBuf, "<onttsfile sessionid='%lu' cmdaddr='%lu' chantype='%d' channo='%d' result='%d' dtmfbuf='' asrbuf='' ttsbuf='%s' errorbuf=''/>", 
        XMLHeader.SessionId, XMLHeader.CmdAddr, XMLHeader.ChnType, XMLHeader.ChnNo, OnTTSEnd, pOnTTSFile->VocFile);
      SendMsg2XML(MSG_onttsfile, XMLHeader.SessionId, SendMsgBuf);
      break;
    }
    AddFileToPlayBuf(nChn, pOnTTSFile->VocFile, ErrMsg);
	  pChn->curPlayData.PlayState = 1;
	  pChn->PlayedTimer = 0;
	  if (Start_ChnPlay(nChn) != 0)
	  {
      Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "start play error");
      StopClearPlayDtmfBuf(nChn);
		  break;
	  }
	  pChn->curPlayData.CanPlayId = 1;
    pChn->curPlayData.WaitTTSResult = 0;
    break;
  case TTSMSG_ongetauthdata:
    pOnGetTTSAuthData = (VXML_ONGETTTSSAUTHDATA_STRUCT *)msgbuf;
    MyTrace(1, "Get TTSLicence=%ld %s", pOnGetTTSAuthData->TTSLicence, pOnGetTTSAuthData->TTSAuthData);
    str_TTSAuthData.SessionId = 0;
    str_TTSAuthData.TTSLineNum = AuthMaxTTS;
    memset(str_TTSAuthData.ttsAuthData, 0, 128);
    pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsauthdata,
      (UC *)&str_TTSAuthData, sizeof(VXML_TTSAUTHDATA_STRUCT));
    break;
  }
}
/************************************************
函數名：StopTTS
函數功能描述：停止TTS合成
算法簡要說明：無
訪問的數據表：無
輸入參數：
    nChn：通道號
輸出參數：無
返回值：無

修改記錄：
	  修改時間：2007-12-25
	  修改內容：初次編寫
	  修改作者：zgj
***************************************************/
void StopTTS(int nChn)
{
  if (TTSLogId == false) return;
  VXML_TTSSTOP_STRUCT stru_TTSStop;
  
  stru_TTSStop.SessionId = pChn->SessionNo;
  stru_TTSStop.CmdAddr = pChn->CmdAddr;
  stru_TTSStop.ChnType = pChn->lgChnType;
  stru_TTSStop.ChnNo = pChn->lgChnNo;
  pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsstop,
    (UC *)&stru_TTSStop, sizeof(VXML_TTSSTOP_STRUCT));
}
/************************************************
函數名：GetNextTTSStream
函數功能描述：取下一語音流
算法簡要說明：無
訪問的數據表：無
輸入參數：
    nChn：通道號
輸出參數：無
返回值：無

修改記錄：
	  修改時間：2007-12-25
	  修改內容：初次編寫
	  修改作者：zgj
***************************************************/
void GetNextTTSStream(int nChn)
{
  if (TTSLogId == false) return;
  VXML_TTSNEXTSTREAM_STRUCT stru_TTSNextStream;
  
  stru_TTSNextStream.SessionId = pChn->SessionNo;
  stru_TTSNextStream.CmdAddr = pChn->CmdAddr;
  stru_TTSNextStream.ChnType = pChn->lgChnType;
  stru_TTSNextStream.ChnNo = pChn->lgChnNo;
  stru_TTSNextStream.VocIndex = pChn->curPlayData.StreamIndex;
  
  pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsnextstream,
    (UC *)&stru_TTSNextStream, sizeof(VXML_TTSNEXTSTREAM_STRUCT));
}
