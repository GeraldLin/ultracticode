#ifndef __MAIN_H__
#define __MAIN_H__

//---------------------------------------------------------------------------
//所有的類指針
CMemvoc   *pMemvoc=NULL; //內存合成放音文件索引
CIVRCfg   *pIVRCfg=NULL; //IVR配置
CTCPLink  *pTcpLink=NULL; //通信消息類
CTCPAgServer  *pTcpAgLink=NULL; //ag通信消息類
CTCPClient *pDBClient=NULL; //DB通信消息類
CTCPHALink *pTCPHALink=NULL; //主備方式通信消息類
CFlwRule  *pFlwRule=NULL; //流程規則類
CMsgfifo  *RMsgFifo=NULL; //XML消息緩沖隊列
CParser   *pParser=NULL; //流程解析器
CBoxchn   *pBoxchn=NULL; //通道
CBoxconf  *pBoxconf=NULL; //會議
CCallout  *pCallbuf=NULL; //呼出緩沖
CCalloutQue *pCalloutQue=NULL; //呼出隊列
CAgentMng *pAgentMng=NULL; //坐席管理類
CACDQueue *pACDQueue=NULL; //ACD隊列分配
CVopGroup *pVopGroup=NULL; //語音機資源
CSwitchPortList *pSwitchPortList=NULL; //交換機資源
CLogInOutFifo *pLogInOutFifo=NULL;
CSQLSfifo *pSQLSfifo=NULL; //2015-10-20 增加sql隊列

CACDSplitMng gACDSplitMng;
CIVRDeptMng  gIVRDeptMng;

CIVRCallCountParamMng gIVRCallCountParamMng;

static CXMLRcvMsg LogRcvMsg; //保存接收到的通訊消息，靜態變量防止頻繁分配內存
static CXMLRcvMsg IVRRcvMsg;
static CXMLMsg    SWTRcvMsg;
CXMLMsg RecvDBMsg;
CXMLMsg IVRSndMsg;
CXMLMsg VOPSendMsg;

//---------------------------------------------------------------------------
short g_DestIVRServerId=0;
short g_MasterIVRServerId;
short g_IVRServerId;
char g_szLocaIVRIP[32]; //IVR ip
char g_szAdapterName[64]; //網絡標志
short g_nMultiRunMode=0; //雙機熱備方式
short g_nHostType=0; //本機類型：0-主機 1-備機
short g_nMasterStatus=1; //主機運行狀態：0-待機，1-運行
short g_nStandbyStatus=0; //備機運行狀態：0-待機，1-運行
char g_szSwitchTime[32]; //切換的時間
unsigned short StandbyIVRClientId=0; //備用及的客戶端編號

short g_nLanDownHAMode=0; //與CTI服務之間的網絡斷開后的處理方式：0-不切換，1-斷開后設定時長切換
short g_nLanDownCheckTimeLen=60; //網絡斷開后檢測時長秒
short g_nCTILINKDownSwitchOverId=0; //雙機熱備方式時，CTI鏈路故障是否切換 2016-10-05
short g_nDBDownSwitchOverId=0; //雙機熱備方式時，數據庫故障是否切換 2016-10-05

CCriticalSection g_cDownCriticalSection;
CCriticalSection g_cHALinkCriticalSection;
CCriticalSection g_cMonLinkCriticalSection;

volatile bool g_bCTIHAServiceDown=false; //CTI備用機（或主用機）服務斷開標志
time_t g_nCTIHAServiceDownTimer=0; //CTI備用機（或主用機）服務斷開時間點

volatile bool g_bFLWServiceDown=false; //FLW服務斷開標志
time_t g_nFLWServiceDownTimer=0; //FLW服務斷開時間點

volatile bool g_bSWTServiceDown=false; //SWT服務斷開標志
time_t g_nSWTServiceDownTimer=0; //SWT服務斷開時間點

volatile bool g_bCTILinkDown=false; //CTI鏈路斷開標志
time_t g_nCTILinkDownTimer=0; //CTI鏈路斷開時間點

volatile bool g_bWSServiceDown=false; //WebSocket服務斷開標志
time_t g_nWSServiceDownTimer=0; //WebSocket服務斷開時間點

volatile bool g_bDBServiceDown=false; //DB服務斷開標志
time_t g_nDBServiceDownTimer=0; //DB服務斷開時間點

volatile bool g_bDBConnDown=false; //連接DB失敗標志
time_t g_nDBConnDownTimer=0; //連接DB失敗時間點

volatile bool g_bIVRChBlock=false; //IVR通道全部阻塞標志
time_t g_nIVRChBlockTimer=0; //IVR通道全部阻塞時間點
//---------------------------------------------------------------------------
bool g_bRunId = false; //運行標志
bool g_bAlarmId = false; //系統告警標志 true-有告警 false-無告警
bool g_bAlartId = false; //告警音開啟標志

bool g_bLogId = false; //監控開關
bool g_bSaveId = false; //保存消息
bool g_bCommId = false; //顯示通信消息
bool g_bDebugId = false; //顯示調試消息
bool g_bDrvId = false; //記錄板卡驅動消息
bool g_bSQLId = false; //記錄SQL消息
bool g_bLogSpecId = false; //記錄詳細消息

bool g_bServiceDebugId = false;

short g_nSwitchMode=0; //是否為交換機接入方式 0-語音卡運行方式 >=1-是交換機運行方式
short g_nSwitchType = 0; //連接的交換機類型：0-板卡模式 1-AVAYA-IPO系列 2-AVAYA-S系列 3-Alcatel OXO系列 
                         //4-Alcatel OXE系列 5-simens系列 6-Panasonic系列 7-NEC系列 8-HUAWEI 9-START-NET 
                         //10-普通PBX(無CTI接口) 11-中聯 12-CISCO 13-FREESWITCH 99-演示平臺

short g_nCTILinkStatus=0;

short g_nCardType = 0; //本服務內置語音卡類型：0-未安裝語音卡 1-杭州三匯 2-DIVA 3-NMS 4-DONJIN 5-FREESWITCH 9-演示聲卡
short g_VOCInLinux = 0; //語音文件是否在linux系統上 2013-09-30
bool g_bVoiceCardInit = false;
bool g_bChnInit = false;
short g_nUSBInitId = 0; //USB盒子初始化狀態:0-未初始化 1-已連接 2-已斷開 3-檢測到斷開

#define pChn  ((pBoxchn->pChn_str) + nChn)
#define pChnn ((pBoxchn->pChn_str) + nChnn)
#define pChn1 ((pBoxchn->pChn_str) + nChn1)
#define pChn2 ((pBoxchn->pChn_str) + nChn2)

#define pAG  (pAgentMng->m_Agent[nAG])
#define pAGn (pAgentMng->m_Agent[nAGn])
#define pAG1 (pAgentMng->m_Agent[nAG1])
#define pAG2 (pAgentMng->m_Agent[nAG2])

#define pTrk ((pBoxchn->pTrk_str) + nTrk)
#define pCsc ((pBoxchn->pCsc_str) + nCsc)

#define pCfc ((pBoxconf->pCfc_str) + nCfc)

#define pOut ((pCallbuf->pOut_str) + nOut)

#define pQue ((pCalloutQue->pQue_str) + nQue)

#define pAcd ((pACDQueue->pACD_str) + nAcd)
#define pAcd1 ((pACDQueue->pACD_str) + nAcd1)
#define pPrior ((pACDQueue->pACD_str) + Prior)
#define pNext ((pACDQueue->pACD_str) + Next)

//int nChnn; //當前通道號
char SendMsgBuf[MAX_MSG_BUF_LEN]; //通訊消息發送緩沖區
VXML_XML_HEADER_STRUCT XMLHeader; //XML發送來的消息頭信息
char ErrMsg[MAX_MSG_BUF_LEN]; //錯誤消息緩沖區

int nLOGn=0;
unsigned short AGClientId; //當前坐席TCPLINK編號
unsigned long ACDIdn; //當前ACD序列號
CStringX AGErrMsg;

bool TTSLogId=false;
unsigned short TTSClientId;
bool CTILinkLogId=false;
unsigned short CTILinkClientId;
bool SWTConnectedId=false;

bool TestPhoneLogId=false;
unsigned short TestPhoneClientId;

bool RECSystemLogId=false; //外部錄音系統
unsigned short RECSystemClientId;

bool WebSocketLogId=false;
unsigned short WebSocketClientId;

bool COMServiceLogId=false;
unsigned short COMServiceClientId;

int g_nINIFileType=0; //INI配置文件：0-默認的舊的格式 2-新的web配置格式

char g_szUnimeINIFileName[MAX_PATH_FILE_LEN];
char g_szServiceINIFileName[MAX_PATH_FILE_LEN]; //配置文件
char g_szDBINIFileName[MAX_PATH_FILE_LEN]; //db配置文件
char g_szCTILINKINIFileName[MAX_PATH_FILE_LEN]; //CTILINK配置文件
char g_szMemVocINIFileName[MAX_PATH_FILE_LEN]; //內存放音語音文件所引配置文件

char g_szRootPath[MAX_PATH_LEN]; //平臺安裝根路徑
char g_szAppPath[MAX_PATH_LEN]; //執行文件路徑
char g_szLogPath[MAX_PATH_LEN]; //日志文件路徑
char g_szCountPath[MAX_PATH_LEN]; //統計文件路徑

char g_szDBType[32];
int  g_nDBType=6;
     /*DB_NONE		    0
       DB_SQLSERVER		1
       DB_ORACLE			2
       DB_SYBASE			3
       DB_ACCESS		  4
       DB_ODBC				5
       DB_MYSQL			  6
       DB_SQLITE3		  7
     */
char g_szSQLNowFunc[16];

bool AuthPassId=false;
int AuthMaxTrunk=2;
int AuthMaxSeat=2;
int AuthMaxFax=0;
int AuthMaxSms=0;
int AuthMaxIVR=2;
int AuthMaxRECD=2;
int AuthMaxTTS=0;
int AuthCardType=1;
int AuthSwitchType=1;
char AuthEndDate[20];
char AuthKey[256];
char g_szFirstDate[20];
int g_dwRegRunDays=0; //已運行天數
char g_szLastDate[20]; //累計后的日期

int g_nCallInCount=0;
CChCount ChCount; //當前時段話務統計數據
CChCount TodayChCount[48]; //當天話務統計數據

bool g_nSendTraceMsgToLOG = false;
CLogerStatusSet LogerStatusSet[MAX_LOGNODE_NUM];

int g_nWriteRegRunDog=0;
DWORD g_dwRegServerState=0; //告警bit定義：bit0-FLW,bit1-SWT,bit2-DB,bit3-VOP,bit4-CTILINK,bit5-DB CONECT,bit6-WS,bit7-ALLIVR,bit8-IVR ch,bit9-REC ch //2015-11-16
DWORD g_DestCTIRunState=0; //雙機熱備時，對方主機的運行狀態

CWebServiceParam g_WebServiceParam;

int g_nDefaultACWTimeLen=3;

int g_LangID=1; //語言類型 0-默認 1-簡體中文 2-繁體中文 3-美國英文
/*
0x0404   Chinese   (Taiwan)
0x0804   Chinese   (PRC)
0x0c04   Chinese   (Hong   Kong   SAR,   PRC)
0x1004   Chinese   (Singapore)

0x0409   English   (United   States)
0x0809   English   (United   Kingdom)
*/

UL g_SetForwardingSessionNo=0; //會話序列號
UL g_SetForwardingCmdAddr=0; //當前指令行地址

CCriticalSection g_cTraceCriticalSection;
volatile int g_nExistThread=0;
CWinThread*	g_hIVRClientThrdProc=NULL;
CWinThread*	g_hAGServerThrdProc=NULL;
CWinThread*	g_hDBClientThrdProc=NULL;
CWinThread*	g_hHAServerThrdProc=NULL;

bool g_bQueryLocaMobileNo=false; //是否需要判斷本地手機號碼
int  g_nURIEncodeDecode=false; //是否需要對HTTP、websocket傳遞的參數編碼和解碼:0-無編碼解碼，1-URL方式，2-unicode轉義方式,3-base64 //2015-06-23
int  g_nParserAccessCodeType; //判斷接入號碼類型：1-通過被叫號碼判斷流程，2-通過原被叫號碼判斷流程 2016-03-31

int g_WebServerPort=0;
int g_WebServerTimeOut=10;
CHTTPServer g_WebServer;
CHTTPfifo   g_HTTPfifo;
CWinThread*	g_hHTTPThrdProc=NULL;
CWinThread*	g_hDbThrdProc=NULL;

char g_szAlarmSqliteDB[MAX_PATH_FILE_LEN]; //告警數據庫文件名

int g_nWebServiceSerialNo=1;

char g_szDBServer[32], g_szDBName[32], g_szDBUser[32], g_szDBPwd[64];
char g_szConnectionString[256];
int g_nConnectDbState=0; //0-未連接數據庫，1-連接成功，2-連接失敗
//---------------------------------------------------------------------------

#endif
