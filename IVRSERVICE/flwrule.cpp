//---------------------------------------------------------------------------
#include "stdafx.h"
#include "flwrule.h"


#include "../include/rule/intrule.cpp"
#include "../include/rule/charrule.cpp"
#include "../include/rule/strrule.cpp"
#include "../include/rule/intmacro.cpp"
#include "../include/rule/strmacro.cpp"

#include "../include/msgcpp/dbxml.cpp"
#include "../include/msgcpp/cfgsrv.cpp"
#include "../include/msgcpp/xmlivr.cpp"
#include "../include/msgcpp/ivrxml.cpp"
#include "../include/msgcpp/agivr.cpp"
#include "../include/msgcpp/ivrag.cpp"
#include "../include/msgcpp/logivr.cpp"
#include "../include/msgcpp/ivrlog.cpp"
#include "../include/msgcpp/swtivr.cpp"
#include "../include/msgcpp/ivrswt.cpp"
#include "../include/msgcpp/vopivr.cpp"
#include "../include/msgcpp/ivrvop.cpp"
//---------------------------------------------------------------------------
CFlwRule::CFlwRule()
{
	IntRange=IntRangeArray;
	CharRange=CharRangeArray;
	StrRange=StrRangeArray;
	MacroIntRange=MacroIntRangeArray;
	MacroStrRange=MacroStrRangeArray;
	//IVRXMLMsgRule=IVRXMLMsgRuleArray;
	XMLIVRMsgRule=XMLIVRMsgRuleArray;
  AGIVRMsgRule=AGIVRMsgRuleArray;
  IVRAGMsgRule=IVRAGMsgRuleArray;
	LOGIVRMsgRule=LOGIVRMsgRuleArray;
  IVRLOGMsgRule=IVRLOGMsgRuleArray;
  SWTIVRMsgRule=SWTIVRMsgRuleArray;
  IVRSWTMsgRule=IVRSWTMsgRuleArray;
  VOPIVRMsgRule=VOPIVRMsgRuleArray;
  IVRVOPMsgRule=IVRVOPMsgRuleArray;
  S_TO_MMsgRule=S_TO_MMsgRuleArray;
  M_TO_SMsgRule=M_TO_SMsgRuleArray;
  DBXMLMsgRule=DBXMLMsgRuleArray;
}
CFlwRule::~CFlwRule()
{
}
