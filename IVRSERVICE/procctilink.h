//---------------------------------------------------------------------------
#ifndef ProcCTILINKH
#define ProcCTILINKH
//---------------------------------------------------------------------------

class CCTILinkClient
{
public:
  UC LoginID;
  US ClientID;

  US nChn;
  CH DeviceID[MAX_TELECODE_LEN]; //設備編號
  UC CTILinkStatus;

public:
  void InitData()
  {
    nChn = 0xFFFF;
    ClientID = 0;
    memset(DeviceID, 0, MAX_TELECODE_LEN); //設備編號
    
    LoginID = 0;
    CTILinkStatus = 0;
  }
  CCTILinkClient()
  {
    InitData();
  }
  virtual ~CCTILinkClient(){}
};
class CCTILinkClientList
{
public:
  CCTILinkClient CTILinkClient[256];

public:
  CCTILinkClient *AllocCTILinkClientByClientID(US clientid)
  {
    int i;
    for (i=0; i<256; i++)
    {
      if (CTILinkClient[i].ClientID == clientid)
      {
        return &CTILinkClient[i];
      }
    }
    for (i=0; i<256; i++)
    {
      if (CTILinkClient[i].ClientID == 0)
      {
        return &CTILinkClient[i];
      }
    }
    return NULL;
  }
  CCTILinkClient *GetCTILinkClientByClientID(US clientid)
  {
    for (int i=0; i<256; i++)
    {
      if (CTILinkClient[i].ClientID == clientid)
      {
        return &CTILinkClient[i];
      }
    }
    return NULL;
  }
  CCTILinkClient *GetCTILinkClientByDeviceID(const char *deviceid)
  {
    for (int i=0; i<256; i++)
    {
      if (strcmp(CTILinkClient[i].DeviceID, deviceid) == 0)
      {
        return &CTILinkClient[i];
      }
    }
    return NULL;
  }
};

void SendMsg2SWT(US ClientId, US MsgId, const CH *msg);
void SendMsg2SWT(US ClientId, US MsgId, const CStringX &msg);
int Proc_Msg_From_SWT(CXMLMsg &SWTMsg);

void Set_IVR2SWT_Header(int MsgId);
void Set_IVR2SWT_Header(int MsgId, UL nSwitchID);
void Set_IVR2SWT_Item(int MsgId, int AttrId, const char *value);
void Set_IVR2SWT_Item(int MsgId, int AttrId, const CStringX &value);
void Set_IVR2SWT_Item(int MsgId, int AttrId, int value);
void Set_IVR2SWT_Item(int MsgId, int AttrId, long value);
void Set_IVR2SWT_Item(int MsgId, int AttrId, bool value);
void Set_IVR2SWT_Tail();

void ReadSwitchPortFlwCode(short nPortID);

void BandVopChnByCustPhone(int nChn);

//連接CTILinkService
void Send_SWTMSG_connectlink();

//設置坐席狀態
void Send_SWTMSG_setagentstatus(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, LPCTSTR pszPassword, short nStatus);
//設置轉移號碼 nTranType: 0-取消所有轉移 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移 4-同振綁定
void Send_SWTMSG_settranphone(short nPortID, const char *pszDeviceID, short nTranType, const char *pszTranPhone);
//設置留言指示燈
void Send_SWTMSG_setlampstatus(short nPortID, const char *pszDeviceID, short nStatus);
//應答來話
void Send_SWTMSG_answercall(short nPortID, const char *pszDeviceID, long nConnID);
//拒絕來話
void Send_SWTMSG_refusecall(short nPortID, const char *pszDeviceID, long nConnID);
//發起呼叫 //2016-07-31 增加傳遞外呼字冠判斷
void Send_SWTMSG_makecall(short nPortID, const char *pszDeviceID, long nCallID, const char *pszCallerNo, const char *pszCalledNo, short nRouteNo, const char *pszCallData, int nCalledType, const char *pszPreCode="");
//將來中繼來話綁定到分機
void Send_SWTMSG_bandcall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID);
//將來中繼來話綁定到提示語音
void Send_SWTMSG_bandvoice(short nPortID, const char *pszDeviceID, long nConnID, short nVoiceID);
//保持電話
void Send_SWTMSG_holdcall(short nPortID, const char *pszDeviceID, long nConnID);
//取消保持電話
void Send_SWTMSG_unholdcall(short nPortID, const char *pszDeviceID, long nConnID);
//轉接電話
void Send_SWTMSG_transfercall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID, short nTranType, const char *pszCallData, int nCalledType);
//停止轉接電話
void Send_SWTMSG_stoptransfer(short nPortID, const char *pszDeviceID, long nConnID);
//穿梭通話
void Send_SWTMSG_swapcall(short nPortID, const char *pszDeviceID, long nConnID);
//代接電話
void Send_SWTMSG_pickupcall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID);
//強插通話
void Send_SWTMSG_breakincall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID);
//監聽電話
void Send_SWTMSG_listencall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID);
//強拆電話
void Send_SWTMSG_removecall(short nPortID, const char *pszDeviceID, long nConnID, short nDesPortID, const char *pszDesDeviceID);
//三方通話
void Send_SWTMSG_threeconf(short nPortID, const char *pszDeviceID, long nHeldConnID, long nActConnID);
//釋放呼叫
void Send_SWTMSG_releasecall(short nPortID, const char *pszDeviceID, long nConnID);
//分機掛機
void Send_SWTMSG_hangup(short nPortID, const char *pszDeviceID);
//將電話保持到話務臺隊列
void Send_SWTMSG_putacdqueuecall(short nPortID, const char *pszDeviceID, long nConnID, short nQueueNo);
//從話務臺隊列提取電話
void Send_SWTMSG_getacdqueuecall(short nPortID, const char *pszDeviceID, long nConnID, short nQueueNo);
//發送交換機操作指令
void Send_SWTMSG_sendswitchcmd(short nPortID, const char *pszDeviceID, short nCmdID);
//發送DTMF指令
void Send_SWTMSG_senddtmf(short nPortID, const char *pszDeviceID, long nConnID, const char *pszDTMFs);
//接管其他坐席的電話
void Send_SWTMSG_takeovercall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
//電話重新定向振鈴
void Send_SWTMSG_redirectcall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
//加入會議
void Send_SWTMSG_addtoconf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, long nDesConnID, long nConfID);
//退出會議
void Send_SWTMSG_removefromconf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, long nDesConnID, long nConfID);
//發送VOC狀態到交換機
void Send_SWTMSG_sendivrstatus(short nVopNo, short nStatus);
//發送啟動錄音
void Send_SWTMSG_recordfile(short nPortID, const char *pszDeviceID, long nConnID, const char *pszFileName, long nRecdLength, short nRecdParam, LPCTSTR pszRule);
//發送停止錄音
void Send_SWTMSG_stoprecord(short nPortID, const char *pszDeviceID, long nConnID, short nStopFlag);
//文件放音
void Send_SWTMSG_playfile(short nPortID, const char *pszDeviceID, long nConnID, const char *pszFileName, short nPlayParam, LPCTSTR pszRule);
//索引放音
void Send_SWTMSG_playindex(short nPortID, const char *pszDeviceID, long nConnID, short nIndex, short nPlayParam, LPCTSTR pszRule);
//停止放音
void Send_SWTMSG_stopplay(short nPortID, const char *pszDeviceID, long nConnID, short nStopFlag);

int Proc_SWTMSG_onconnectlink(CXMLMsg &SWTMsg);	//連接CTILinkService結果
int Proc_SWTMSG_onlinkstatus(CXMLMsg &SWTMsg); //返回CTILink接口與交換機的連接狀態
int Proc_SWTMSG_ongetdevnnum(CXMLMsg &SWTMsg);	//返回交換機設備配置數量
int Proc_SWTMSG_ongetdevparam(CXMLMsg &SWTMsg);	//返回交換機設備的配置參數
int Proc_SWTMSG_onportstatus(CXMLMsg &SWTMsg);	//端口狀態事件
int Proc_SWTMSG_onagentstatus(CXMLMsg &SWTMsg);	//坐席狀態事件
int Proc_SWTMSG_onroomstatus(CXMLMsg &SWTMsg);	//房間狀態事件
int Proc_SWTMSG_onlampevent(CXMLMsg &SWTMsg);	//留言燈狀態事件
int Proc_SWTMSG_onwakeevent(CXMLMsg &SWTMsg);	//叫醒狀態事件
int Proc_SWTMSG_onconsoleevent(CXMLMsg &SWTMsg);	//話務臺隊列事件
int Proc_SWTMSG_onbillevent(CXMLMsg &SWTMsg); //收到呼叫話單事件
int Proc_SWTMSG_oncallevent(CXMLMsg &SWTMsg); //呼叫事件
int Proc_SWTMSG_oncallsam(CXMLMsg &SWTMsg); //收到后續地址事件
int Proc_SWTMSG_oncallacm(CXMLMsg &SWTMsg); //號碼收全事件
int Proc_SWTMSG_oncallring(CXMLMsg &SWTMsg); //收到振鈴事件
int Proc_SWTMSG_oncallfail(CXMLMsg &SWTMsg); //收到呼叫失敗事件
int Proc_SWTMSG_oncallack(CXMLMsg &SWTMsg); //收到應答事件
int Proc_SWTMSG_oncallrelease(CXMLMsg &SWTMsg); //收到釋放事件
int Proc_SWTMSG_onanswercall(CXMLMsg &SWTMsg);	//應答來話結果
int Proc_SWTMSG_onrefusecall(CXMLMsg &SWTMsg);	//拒絕來話結果
int Proc_SWTMSG_onmakecall(CXMLMsg &SWTMsg);	//發起呼叫結果
int Proc_SWTMSG_onbandcall(CXMLMsg &SWTMsg);	//將來中繼來話綁定到分機結果
int Proc_SWTMSG_onbandvoice(CXMLMsg &SWTMsg);	//將來中繼來話綁定到提示語音結果
int Proc_SWTMSG_onholdcall(CXMLMsg &SWTMsg);	//保持電話結果
int Proc_SWTMSG_onunholdcall(CXMLMsg &SWTMsg);	//取消保持電話結果
int Proc_SWTMSG_ontransfercall(CXMLMsg &SWTMsg);	//轉接電話結果
int Proc_SWTMSG_onstoptransfercall(CXMLMsg &SWTMsg);	//停止轉接電話結果
int Proc_SWTMSG_onswapcall(CXMLMsg &SWTMsg);	//穿梭通話結果
int Proc_SWTMSG_onpickupcall(CXMLMsg &SWTMsg);	//代接電話結果
int Proc_SWTMSG_onbreakincall(CXMLMsg &SWTMsg);	//強插通話結果
int Proc_SWTMSG_onlistencall(CXMLMsg &SWTMsg);	//監聽電話結果
int Proc_SWTMSG_onremovecall(CXMLMsg &SWTMsg);	//強拆電話結果
int Proc_SWTMSG_onthreeconf(CXMLMsg &SWTMsg);	//三方通話結果
int Proc_SWTMSG_onconsolehold(CXMLMsg &SWTMsg);	//將電話保持到話務臺隊列結果
int Proc_SWTMSG_onconsolepickup(CXMLMsg &SWTMsg);	//從話務臺隊列提取電話結果
int Proc_SWTMSG_onsendswitchcmd(CXMLMsg &SWTMsg);	//發送交換機操作指令結果
int Proc_SWTMSG_oncallconnected(CXMLMsg &SWTMsg);	//收到呼叫通話連接事件
int Proc_SWTMSG_onrecordresult(CXMLMsg &SWTMsg);	//錄音結果 v3.2
int Proc_SWTMSG_onplayresult(CXMLMsg &SWTMsg);	//放音結果 v3.2
int Proc_SWTMSG_onrecvdtmf(CXMLMsg &SWTMsg);	//收碼結果 v3.2
int Proc_SWTMSG_onqueryacdsplit(CXMLMsg &SWTMsg);	//查詢ACD話務組統計狀態結果 v3.3
int Proc_SWTMSG_onagentloggedon(CXMLMsg &SWTMsg);	//agent登錄交換機事件 v3.3
int Proc_SWTMSG_onagentloggedoff(CXMLMsg &SWTMsg);	//agent登出交換機事件 v3.3
int Proc_SWTMSG_onsettranphone(CXMLMsg &SWTMsg);	//設置轉移號碼結果

void ProcSWTMsg(CXMLMsg &SWTMsg); //處理交換機發來的消息

//---------------------------------------------------------------------------
#endif
