// TCPClient.cpp: implementation of the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "tcpdbserver.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

CTCPClient::CTCPClient()
{
  IsOpen = false;
	hdll = NULL;
  memset(m_szAppPath, 0, MAX_PATH_LEN);
  strcpy(m_szDllFile, "tmdbserver.dll");
  ClientId = 0x0101;
}

CTCPClient::~CTCPClient()
{
  ReleaseDll();
}

bool CTCPClient::LoadDll()
{
  if (strlen(m_szAppPath) > 0)
    sprintf(m_szDllFile, "%s\\tmdbserver.dll", m_szAppPath);
  else
    strcpy(m_szDllFile, "tmdbserver.dll");
	hdll = LoadLibrary(m_szDllFile);
	if ( hdll != NULL )
	{
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(  !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MyTrace(0, "GetProcAddress tmtcpserverdb.dll eror");
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MyTrace(0, "LoadLibrary tmtcpserverdb.dll eror");
		return false;
	}
}

void CTCPClient::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}
//組合對象編碼
unsigned short CTCPClient::CombineID( unsigned char ObjectType, unsigned char ObjectNo )
{
  US o_i1, o_i2;
  o_i1 = ( ObjectType << 8 ) & 0xFF00;
  o_i2 = ObjectNo & 0x00FF;
  return (o_i1 | o_i2);
}
//組合消息類型及消息編號
unsigned short CTCPClient::CombineMessageID(unsigned char MsgType, unsigned char MsgNo)
{
  US m_i1, m_i2;
  m_i1 = ( MsgType << 8 ) & 0xFF00;
  m_i2 = MsgNo & 0x00FF;
  return (m_i1 | m_i2);
}
//分解對象標識及編號
void CTCPClient::GetClientID(unsigned short Msg_wParamHi, unsigned char &ClientType, unsigned char &ClientNo)
{
  UC C_T;
  UC C_N;
  C_T = (UC)(( Msg_wParamHi >> 8 ) & 0xFF);
  C_N = Msg_wParamHi & 0xFF;
  ClientType = C_T;
  ClientNo = C_N;
}
//分解消息類型及消息編號
void CTCPClient::GetMsgID(unsigned short Msg_wParamLo, unsigned char &MsgType, unsigned char &MsgNo)
{
  UC M_T, M_N;
  M_T = ( Msg_wParamLo >> 8 ) & 0x00FF;
  M_N = Msg_wParamLo & 0x00FF;
  MsgType = M_T;
  MsgNo = M_N;
}
//發送消息
int CTCPClient::SendMessage(unsigned short ServerId, unsigned short MsgId, int MsgLen, const unsigned char *MsgBuf)
{
	int SendedLen;

  SendedLen = AppSendData(ServerId, (MSGTYPE_DBXML<<8) | MsgId, MsgBuf, MsgLen);
  if (SendedLen != MsgLen)
    MyTrace(0, "Send Msg to DB Error ServerId=%04x MsgId=%04x MsgBuf=%s\n", ServerId, MsgId, MsgBuf);
	return SendedLen;
}
int CTCPClient::DBSendMessage(unsigned short MsgId, const char *MsgBuf)
{
  return SendMessage(DBServer.Serverid, MsgId, strlen(MsgBuf), (unsigned char *)MsgBuf);
}
int CTCPClient::IMSendMessage(unsigned short MsgId, const char *MsgBuf)
{
  return SendMessage(IMServer.Serverid, MsgId, strlen(MsgBuf), (unsigned char *)MsgBuf);
}
bool CTCPClient::ReadIni(const char *filename)
{
  ClientId = CombineID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "IVRServerId", 1, filename));

  DBServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "DBServerIP", "127.0.0.1", DBServer.ServerIP, 30, filename);
  DBServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "DBServerPort", 5228, filename);
  DBServer.Serverid = CombineID(NODE_DB,GetPrivateProfileInt("IVRSERVER", "DBServerID", 1, filename));
  SessionId = DBServer.Serverid;
  
  IMServer.IsConnected=false;
  GetPrivateProfileString("IMSERVER", "IMServerIP", "127.0.0.1", IMServer.ServerIP, 30, filename);
  IMServer.ServerPort = GetPrivateProfileInt("IMSERVER", "IMServerPort", 5004, filename);
  IMServer.Serverid = CombineID(NODE_DB,GetPrivateProfileInt("IMSERVER", "IMServerID", 0, filename));
  SessionId = IMServer.Serverid;

  return true;
}    
bool CTCPClient::ReadWebSetupIni(const char *filename)
{
  ClientId = CombineID(NODE_IVR, GetPrivateProfileInt("TCPLINK", "IVRServerId", 1, filename));
  SessionId = ClientId;
  
  DBServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "DBServerIP", "127.0.0.1", DBServer.ServerIP, 30, filename);
  DBServer.ServerPort = GetPrivateProfileInt("TCPLINK", "DBServerPort", 5228, filename);
  DBServer.Serverid = CombineID(NODE_DB,GetPrivateProfileInt("TCPLINK", "DBServerID", 1, filename));
  
  return true;
}    
int CTCPClient::ConnectDBServer()
{
  if (IsOpen == true)
  {
    AppInit(DBServer.Serverid,
            ClientId,
            DBServer.ServerIP,
            DBServer.ServerPort,
            OnDBLogin,
            OnDBClose,
            OnDBReceiveData);
    MyTrace(0, "ConnectDBServer LocaClientId=0x%04x ServerIP=%s ServerPort=%d Serverid=0x%04x",
      ClientId, DBServer.ServerIP, DBServer.ServerPort, DBServer.Serverid);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPClient::ConnectIMServer()
{
  if (IsOpen == true)
  {
    if (IMServer.Serverid&0xFF > 0)
    {
    AppInit(IMServer.Serverid,
      ClientId,
      IMServer.ServerIP,
      IMServer.ServerPort,
      OnIMLogin,
      OnIMClose,
      OnIMReceiveData);
    MyTrace(0, "ConnectIMServer LocaClientId=0x%04x ServerIP=%s ServerPort=%d Serverid=0x%04x",
      ClientId, IMServer.ServerIP, IMServer.ServerPort, IMServer.Serverid);
    }
    return 0;
  }
  else
  {
    //加載動態連接庫失敗
    return 1;
  }
}
int CTCPClient::ConnectServer()
{
  int nResult = 0;

  if (LoadDll() == false)
    return 1;
  if (ConnectDBServer() != 0)
    nResult = 2;
  ConnectIMServer();
  return nResult;
}