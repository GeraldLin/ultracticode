// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__40095FE4_5F70_11D4_B3A2_0080C88FC26A__INCLUDED_)
#define AFX_STDAFX_H__40095FE4_5F70_11D4_B3A2_0080C88FC26A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#pragma warning(disable: 4786)

// 為在服務中使用對話框需要的定義
#define _WIN32_WINNT			0X4000

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxmt.h>
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


// TODO: reference additional headers your program requires here

#include <winsvc.h>
#include <afxdb.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <direct.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <winsock2.h>
#include <iphlpapi.h>

#include "typedef.h"
#include "../include/comm/macrodef.h"

#include "../include/msgh/tlinkid.h"  
#include "../include/msgh/vmsgrule.h" 
#include "../include/msgh/ivrxml.h"
#include "../include/msgh/ivrag.h"
#include "../include/msgh/ivrtts.h"
#include "../include/msgh/ivrlog.h"
#include "../include/msgh/ivrswt.h"
#include "../include/msgh/ivrvop.h"
#include "../include/msgh/dbxml.h"
#include "../include/msgh/cfgsrv.h"
#include "../include/msgh/ivrcom.h"

#include "../include/comm/DEFMSG.H"   //定義參數意義

#include "cstring.h"
#include "parxml.h"

#include "../include/comm/rulestru.h"

#include "memvoc.h"
#include "ivrcfg.h"
#include "funccast.h"
#include "funcstr.h"
#include "functime.h"
#include "funcfile.h"

#include "flwrule.h"
#include "tcplink.h"
#include "tcpagserver.h"
#include "tcpdbserver.h"
#include "tcphaserver.h"
#include "sqlsfifo.h"
#include "rmsgfifo.h"
#include "loginoutfifo.h"
#include "VopChn.h"

#include "parser.h"

#include "boxchn.h"
#include "boxconf.h"
#include "boxag.h"
#include "callout.h"

#include "mixervoc.h"
#include "prococx.h"
#include "procchn.h"
#include "procag.h"
#include "proclog.h"
#include "proctts.h"
#include "procxml.h"
#include "procctilink.h"
#include "procvop.h"
#include "../comm/base64.h"

//#include "ivrdrv.h"
#include "../comm/ivrdll.h"

#include "HTTPServer.h"

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__40095FE4_5F70_11D4_B3A2_0080C88FC26A__INCLUDED_)
