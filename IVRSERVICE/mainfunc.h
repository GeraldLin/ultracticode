//---------------------------------------------------------------------------
#ifndef MainfuncH
#define MainfuncH
//---------------------------------------------------------------------------

void WriteAlarmList(int nAlarmCode, int nAlarmLevel, int nAlarmState, LPCTSTR lpszFormat, ...);

int GetAdapterNameByIP(const char *ivrip);
int CheckLocaNetCable(const char *adaptername);

int WriteRegData(struct HKEY__ *ReRootKey, TCHAR *ReSubKey, TCHAR *ReValueName, LPBYTE ReSetContent_S);
int WriteRegData(struct HKEY__ *ReRootKey, TCHAR *ReSubKey, TCHAR *ReValueName, DWORD ReSetWalue);
int WriteRegRunTimerDog();
int WriteRegServiceRunState();
int WriteRegRunDays();

//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-內部調試信息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

bool isMyselfInActive();
bool GetCTIHALinkFlag();

//處理登錄退出消息隊列
void CheckLogInOutFifo();
//檢查接收消息隊列
void CheckMsgFifo();

void ProcSqlFiFo();

void SendLogCFGMsg();
void ProcCFGMsg(unsigned short MsgId, const char *msg);

//-------------------IVR通信事件處理-------------------------------------------
void OnIVRLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function
void OnIVRClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
//讀授權端口數據
void ReadAuthData();
//讀總的服務的配置文件（返回0表示成功）
int ReadSERVICEIniFile();
//初始化用到的類資源（返回0表示成功）
int InitResourceMem();
//初始化語音卡驅動DLL（返回0表示成功）
int InitVoiceCard();
//初始化普通交換機
int InitNoCTISwitchData();
//連接數據庫
int ConnectDB();
//是否為本地手機
int IsLocaMobileNo(LPCTSTR PhoneNo, char *pszResult);
//運行前初始化所有（返回0表示成功）
int InitRun(void);
//釋放資源
void ReleaseAllResouce();
//主循環處理
void APPLoop();

//封裝的板卡驅動事件回調函數
void OnFireCALLIN(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
void OnFireRECVSAM(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called);
void OnFireRECVACM(short ChType, short ChIndex, short AcmParam);
void OnFireRECVACK(short ChType, short ChIndex, short AckParam);
void OnFireRELEASE(short ChType, short ChIndex, short ReleaseType, short ReleaseReason);
void OnFirePLAYRESULT(short ChType, short ChIndex, short Result);
void OnFireRECORDRESULT(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen);
void OnFireRECVDTMF(short ChType, short ChIndex, LPCTSTR Dtmfs);
void OnFireCONFPLAYRESULT(short ConfNo, short Result);
void OnFireCONFRECORDRESULT(short ConfNo, short Result, long RecdSize, long RecdLen);
void OnFireSENDFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, short Param);
void OnFireRECVFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, short Param);
void OnFireFLASH(short ChType, short ChIndex);
void OnFireLINEALARM(short ChType, short ChIndex, short AlarmType, short AlarmParam);
void OnFireRECORDMEMRESULT(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize);
void OnFireCONFCREATERESULT(short ChType, short ChIndex, short ConfNo, short CreateId, short Result);
void OnFireCONFDESTROYRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCHECKTONERESULT(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result);
void OnFireSENDFSKRESULT(short ChType, short ChIndex, short FskChIndex, short Result, short Param);
void OnFireRECVFSK(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param);
void OnFireFAXSTATE(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FaxState);
void OnFireTRANSFERRESULT(short ChType, short ChIndex, short Result, LPCTSTR Reason);
void OnFireSTOPTRANSFERRESULT(short ChType, short ChIndex, short Result);

//設置所有事件回調函數
void SetAllCallBackFunc();

//封裝的板卡驅動事件回調函數(內置語音機版本)
void OnFireCALLINForVOP(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
void OnFireRECVSAMForVOP(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called);
void OnFireRECVACMForVOP(short ChType, short ChIndex, short AcmParam);
void OnFireRECVACKForVOP(short ChType, short ChIndex, short AckParam);
void OnFireRELEASEForVOP(short ChType, short ChIndex, short ReleaseType, short ReleaseReason);
void OnFirePLAYRESULTForVOP(short ChType, short ChIndex, short Result);
void OnFireRECORDRESULTForVOP(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen);
void OnFireRECVDTMFForVOP(short ChType, short ChIndex, LPCTSTR Dtmfs);
void OnFireCONFPLAYRESULTForVOP(short ConfNo, short Result);
void OnFireCONFRECORDRESULTForVOP(short ConfNo, short Result, long RecdSize, long RecdLen);
void OnFireSENDFAXRESULTForVOP(short ChType, short ChIndex, short FaxChIndex, short Result, short Param);
void OnFireRECVFAXRESULTForVOP(short ChType, short ChIndex, short FaxChIndex, short Result, short Param);
void OnFireFLASHForVOP(short ChType, short ChIndex);
void OnFireLINEALARMForVOP(short ChType, short ChIndex, short AlarmType, short AlarmParam);
void OnFireRECORDMEMRESULTForVOP(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize);
void OnFireCONFCREATERESULTForVOP(short ChType, short ChIndex, short ConfNo, short CreateId, short Result);
void OnFireCONFDESTROYRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFJOINRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFLISTENRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNJOINRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNLISTENRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCHECKTONERESULTForVOP(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result);
void OnFireSENDFSKRESULTForVOP(short ChType, short ChIndex, short FskChIndex, short Result, short Param);
void OnFireRECVFSKForVOP(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param);
void OnFireFAXSTATEForVOP(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FaxState);
void OnFireTRANSFERRESULTForVOP(short ChType, short ChIndex, short Result, LPCTSTR Reason);
void OnFireSTOPTRANSFERRESULTForVOP(short ChType, short ChIndex, short Result);

//設置所有事件回調函數
void SetAllCallBackFuncForVOP();
//---------------------------------------------------------------------------
#endif
