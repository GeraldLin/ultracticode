//---------------------------------------------------------------------------
#ifndef ProcvopH
#define ProcvopH

//---------------------------------------------------------------------------
//外置語音機
void SendMsg2VOP(US ClientId, US MsgId, const CH *msg);
void SendMsg2VOP(US ClientId, US MsgId, const CStringX &msg);
void SendMsg2IPREC(US ClientId, US MsgId, const CH *msg);

int Proc_Msg_From_VOP(CXMLRcvMsg &AGMsg);
void Get_VOPMSG_HeaderValue(CXMLRcvMsg &VOPMsg);

int Proc_VOPMSG_onlogin(CXMLRcvMsg &VOPMsg);	//語音節點資源注冊
int Proc_VOPMSG_onlinealarm(CXMLRcvMsg &VOPMsg);	//收到線路告警消息
int Proc_VOPMSG_onplayresult(CXMLRcvMsg &VOPMsg);	//放音結果消息
int Proc_VOPMSG_onrecordresult(CXMLRcvMsg &VOPMsg);	//錄音結果消息
int Proc_VOPMSG_onrecvdtmf(CXMLRcvMsg &VOPMsg);	//收到DTMF消息
int Proc_VOPMSG_onsendfaxresult(CXMLRcvMsg &VOPMsg);	//發送傳真結果消息
int Proc_VOPMSG_onrecvfaxresult(CXMLRcvMsg &VOPMsg);	//接收傳真結果消息
int Proc_VOPMSG_onrelease(CXMLRcvMsg &VOPMsg);	//通道釋放消息
int Proc_VOPMSG_oncallin(CXMLRcvMsg &VOPMsg); //電話呼入消息
int Proc_VOPMSG_onrecvsam(CXMLRcvMsg &VOPMsg); //收到電話呼入后續號碼
int Proc_VOPMSG_onsendacm(CXMLRcvMsg &VOPMsg); //發送ACM結果
int Proc_VOPMSG_onsendack(CXMLRcvMsg &VOPMsg); //發送ACK結果
int Proc_VOPMSG_onrecvacm(CXMLRcvMsg &VOPMsg); //呼出收到ACM
int Proc_VOPMSG_onrecvack(CXMLRcvMsg &VOPMsg); //呼出收到ACK
int Proc_VOPMSG_ontransfer(CXMLRcvMsg &VOPMsg); //拍叉簧轉接電話結果
int Proc_VOPMSG_onivrstatus(CXMLRcvMsg &VOPMsg); //語音機將一方的IVR服務狀態發給另一方

void ProcVOPMsg(CXMLRcvMsg &VOPMsg);

void Add2VOPMsgHeader(US MsgId, UL sessionid, int vopno, int vopchnno, int chntype, int chnno);
void Add2VOPStrItem(US MsgId, US AttrId, const char *value);
void Add2VOPIntItem(US MsgId, US AttrId, int value);
void Add2VOPLongItem(US MsgId, US AttrId, long value);
void Add2VOPMsgTail();

//發送文件放音到語音機
int SendPlayFile2VOP(int nChn, int vopno, int vopchnno, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
//發送內存索引放音到語音機
int SendPlayIndex2VOP(int nChn, int vopno, int vopchnno, short VocIndex, short PlayParam, LPCTSTR PlayRule);
//發送停止放音
int SendStopPlay2VOP(int nChn, int vopno, int vopchnno);
//發送文件錄音到語音機
int SendRecordFile2VOP(int nChn, int vopno, int vopchnno, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
//發送停止錄音
int SendStopRecord2VOP(int nChn, int vopno, int vopchnno);
//發送DTMF到語音機
int SendSendDTMF2VOP(int nChn, int vopno, int vopchnno, LPCTSTR DTMFStr, short PlayParam, LPCTSTR PlayRule);
//發送釋放通道
int SendRelease2VOP(int nChn, int vopno, int vopchnno);
//發送傳真到語音機
int SendSendFax2VOP(int nChn, int vopno, int vopchnno, LPCTSTR faxfilename, LPCTSTR faxcsid, int param, LPCTSTR rule);
//追加發送傳真到語音機
int SendAppendFax2VOP(int nChn, int vopno, int vopchnno, LPCTSTR faxfilename, LPCTSTR faxcsid, int param, LPCTSTR rule);
//接收傳真到語音機
int SendRecvFax2VOP(int nChn, int vopno, int vopchnno, LPCTSTR faxfilename, LPCTSTR faxcsid, int param, LPCTSTR rule);
//停止收發傳真到語音機
int SendStopFax2VOP(int nChn, int vopno, int vopchnno);

//發送文件放音到測試話機
int SendPlayFile2TestPhone(int nChn, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
//發送內存索引放音到測試話機
int SendPlayIndex2TestPhone(int nChn, short VocIndex, short PlayParam, LPCTSTR PlayRule);
//發送停止放音測試話機
int SendStopPlay2TestPhone(int nChn);
//發送文件錄音到測試話機
int SendRecordFile2TestPhone(int nChn, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
//發送停止錄音測試話機
int SendStopRecord2TestPhone(int nChn);
//發送釋放通道測試話機
int SendRelease2TestPhone(int nChn);
//發送ACM到測試話機
int SendACM2TestPhone(int nChn);
//發送ACK到測試話機
int SendACK2TestPhone(int nChn);
//發送呼出到測試話機
int SendCallOut2TestPhone(int nChn, const char *caller, const char *called);
//發送取消呼出到測試話機
int SendStopCallout2TestPhone(int nChn);
//發送傳真到測試話機
int SendSendFax2TestPhone(int nChn, const char *filename);
//追加發送傳真到測試話機
int SendAppendFax2TestPhone(int nChn, const char *filename);
//接收傳真到測試話機
int SendRecvFax2TestPhone(int nChn, const char *filename);
//停止收發傳真到測試話機
int SendStopFax2TestPhone(int nChn);
//發送DTMF到測試話機
int SendSendDTMF2TestPhone(int nChn, const char *dtmfs);

//第三方錄音系統
void Set_IVR2REC_Header(UC MsgType, char *MsgName, char *ExtnID);
void Add_IVR2REC_Item(char *ParamName, char *ParamValue);
void Add_IVR2REC_Item(char *ParamName, int ParamValue);
void SendMsg2REC(US ClientId, US MsgId, const CH *msg);

void Send_Logon_Result_To_REC(const char *ExtnID, int nResult);
void Send_Transfer_To_REC(int nChn, int nAG);
void Send_StopTransfer_To_REC(int nAcd, int nAG);
void Send_ACDState_To_REC(int ACDNum, int WaitACDNum, int WaitANSNum, const char *CallerList);
void Send_IVRState_To_REC(int IdelNum, int BusyNum, int BlockNum);
void Send_AGState_To_REC(int LogonNum, int LogoffNum, int IdelNum, int BusyNum, int TalkNum, int NoReadyNum);
void Send_AGState_To_REC(int nAG);
void Send_ReqCallID_To_REC(int nAG);
void Send_ReqCallID_To_REC1(int nChn);

void Proc_Msg_From_REC(UC MsgType, char *msgbuf);
void Proc_Msg_From_IPREC(UC MsgType, char *msgbuf);

void SendMsg2COM(US ClientId, US MsgId, const CH *msg);
void Proc_Msg_From_COM(const char *pszComMsg);
//---------------------------------------------------------------------------
#endif
