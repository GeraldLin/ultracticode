//---------------------------------------------------------------------------
#include "stdafx.h"
#include "callout.h"

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
//---------------------------------------------------------------------------
void CSession::Init()
{
  memset(CdrSerialNo, 0, MAX_CHAR64_LEN);
  memset(RecordFileName, 0, MAX_CHAR128_LEN);
}
void CSession::GenerateCdrSerialNo(int lgChnType, int lgChnNo)
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  sprintf(CdrSerialNo, "%04d%02d%02d%02d%02d%02d%d%03d", 
    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
    local->tm_hour, local->tm_min, local->tm_sec,
    lgChnType, lgChnNo);
  
  char szCallerNo[MAX_TELECODE_LEN], szCalledNo[MAX_TELECODE_LEN];
  int i, j, len;
  
  memset(szCallerNo, 0, MAX_TELECODE_LEN);
  memset(szCalledNo, 0, MAX_TELECODE_LEN);
  
  len = strlen(CallerNo.C_Str());
  j = 0;
  for (i = 0; i < len; i ++)
  {
    if (CallerNo.C_Str()[i] >= '0' && CallerNo.C_Str()[i] <= '9')
    {
      szCallerNo[j] = CallerNo.C_Str()[i];
      j ++;
    }
  }
  
  len = strlen(CalledNo.C_Str());
  j = 0;
  for (i = 0; i < len; i ++)
  {
    if (CalledNo.C_Str()[i] >= '0' && CalledNo.C_Str()[i] <= '9')
    {
      szCalledNo[j] = CalledNo.C_Str()[i];
      j ++;
    }
  }
  
  sprintf(RecordFileName, "%04d%02d%02d/%02d%02d%02d_%s_%s_%d%03d.wav", 
    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
    local->tm_hour, local->tm_min, local->tm_sec,
    szCallerNo, szCalledNo,
    lgChnType, lgChnNo);
}

CCalloutQue::CCalloutQue()
{
  isInit = false;
  pQue_str = NULL;
}

CCalloutQue::~CCalloutQue()
{
	if (pQue_str != NULL)
	{
		delete []pQue_str;
  	pQue_str = NULL;

    MaxQueNum = 0;
  }
}

int CCalloutQue::Init(int maxquenum)
{
	if (maxquenum <= 0) return 1;
  if (isInit == true) return 1;

  MaxQueNum = maxquenum;

  pQue_str = new CCalloutRecd[maxquenum];
  if ( pQue_str == NULL ) return 0x01;

  isInit = true;
  return 0;
}

void CCalloutQue::InitQue(int nQue)
{
  if (nQue >= MaxQueNum) return;
  pQue_str[nQue].Init();
}

int CCalloutQue::Get_Idle_nQue()
{
  static int last_nQue = -1;

  if (isInit == false) return 0xFFFF;

  for (int nQue = 0; nQue < MaxQueNum; nQue ++)
  {
    last_nQue = (last_nQue + 1) % MaxQueNum;
    if (pQue_str[last_nQue].state == 0)
    {
      MyTrace(3, "Get_Idle_nQue=%d",last_nQue);
      return last_nQue;
    }
  }
  MyTrace(3, "Get Idel nQue fail");
  return 0xFFFF;
}

void CCalloutQue::TimerAdd(int nTCount)
{
  for (int nQue = 0; nQue < MaxQueNum; nQue ++)
    pQue_str[nQue].timer ++;
}


//-----------------------------------------------------------------------------
CACDCalled::CACDCalled()
{
  nAG = 0xFFFF;
  pQue_str = NULL;
  CalloutResult = 0;
}
CCallout::CCallout()
{
  isInit = false;
  pOut_str = NULL;
}

CCallout::~CCallout()
{
	if (pOut_str != NULL)
	{
		delete []pOut_str;
  	pOut_str = NULL;

    MaxOutNum = 0;
  }
}

int CCallout::Init(int maxoutnum)
{
	if (maxoutnum <= 0) return 1;
  if (isInit == true) return 1;

  MaxOutNum = maxoutnum;

  pOut_str = new CCalloutBuf[maxoutnum];
  if ( pOut_str == NULL ) return 0x01;

  isInit = true;
	for (int i = 0; i < MaxOutNum; i ++)
	{
		InitOut(i);
	}
  return 0;
}

void CCallout::InitOut(int nOut)
{
	if (isInit == false || nOut >= MaxOutNum) return;

	pOut_str[nOut].state = 0;
}

void CCallout::TimerAdd(int nTCount)
{
  for (int nOut = 0; nOut < MaxOutNum; nOut ++)
  {
    pOut_str[nOut].timer ++;
  }
}
int CCallout::Get_Idle_nOut()
{
  static int last_nOut = -1;

  if (isInit == false) return 0xFFFF;

  for (int nOut = 0; nOut < MaxOutNum; nOut ++)
  {
    last_nOut = (last_nOut + 1) % MaxOutNum;
    if (pOut_str[last_nOut].state == 0)
    {
      pOut_str[last_nOut].Session.Init();
      MyTrace(3, "Get_Idle_nOut=%d",last_nOut);
      return last_nOut;
    }
  }
  MyTrace(3, "Get Idel nOut fail");
  return 0xFFFF;
}
int CCallout::Get_nOut_By_DialSerialNo(UL DialSerialNo)
{
  int nOut;

  if (isInit == false) return 0xFFFF;

  for (nOut = 0; nOut < MaxOutNum; nOut ++)
  {
    if (pOut_str[nOut].state == 1 && pOut_str[nOut].Session.DialSerialNo == DialSerialNo)
      return nOut;
  }
  return 0xFFFF;
}

//---------------------------------------------------------------------------------

CACDQueue::CACDQueue()
{
  isInit = false;
  pACD_str = NULL;
  
  AcdCount = 0; //ACD钉羆单计
  WaitAcdCount = 0; //单だ皌秪Г畊计
  WaitAnsCount = 0; //だ皌秪Г畊单莱氮计

  CallerList.Empty();
}

CACDQueue::~CACDQueue()
{
	if (pACD_str != NULL)
	{
		delete []pACD_str;
  	pACD_str = NULL;

    MaxAcdNum = 0;
  }
}

void CACDQueue::InitAcd(int nAcd)
{
	if (isInit == false || nAcd >= MaxAcdNum) return;

	pACD_str[nAcd].state = 0;
	pACD_str[nAcd].Prior = 0xFFFF;
	pACD_str[nAcd].Next = 0xFFFF;
}

int CACDQueue::Init(int maxacdnum)
{
	if (maxacdnum <= 0) return 1;
  if (isInit == true) return 1;

  MaxAcdNum = maxacdnum;

  pACD_str = new CACDdata[maxacdnum];
  if ( pACD_str == NULL ) return 0x01;

  isInit = true;
	for (int i = 0; i < MaxAcdNum; i ++)
	{
		InitAcd(i);
	}
	pACD_str[0].state = 1;
	pACD_str[0].Prior = 0xFFFF;
	pACD_str[0].Next = 0xFFFF;
  return 0;
}

void CACDQueue::TimerAdd(int nTCount)
{
  for (int nAcd = 0; nAcd < MaxAcdNum; nAcd ++)
  {
    pACD_str[nAcd].timer ++;
    pACD_str[nAcd].waittimer ++;
    pACD_str[nAcd].ringtimer ++;
  }
}

int CACDQueue::Get_Idle_nAcd()
{
  static int last_nAcd = -1;

  if (isInit == false) return 0xFFFF;

  for (int nAcd = 0; nAcd < MaxAcdNum; nAcd ++)
  {
    last_nAcd = (last_nAcd + 1) % MaxAcdNum;
    if (pACD_str[last_nAcd].state == 0)
    {
      MyTrace(3, "Get_Idle_nAcd=%d",last_nAcd);
      return last_nAcd;
    }
  }
  MyTrace(3, "Get Idel nAcd fail");
  return 0xFFFF;
}

void CACDQueue::JoinACDQueue(int nAcd)
{
  US nAcd1=0, Prior=0, Next;
  int queueno=0;

  Next = pACD_str[nAcd1].Next;
  while ( Next < MaxAcdNum )
  {
      if ( pACD_str[Next].ACDRule.Priority < pACD_str[nAcd].ACDRule.Priority )
      {
          pACD_str[Next].Prior = nAcd;
          pACD_str[Prior].Next = nAcd;
          pACD_str[nAcd].Prior = Prior;
          pACD_str[nAcd].Next = Next;
          MyTrace(3, "JoinACDQueue nAcd=%d QueueNo=%d",nAcd, queueno);
          return;
      }
      queueno++;
      Prior = Next;
      Next = pACD_str[Next].Next;
  }
  pACD_str[Prior].Next = nAcd;
  pACD_str[nAcd].Prior = Prior;
  pACD_str[nAcd].Next = 0xFFFF;
  MyTrace(3, "JoinACDQueue nAcd=%d QueueNo=%d",nAcd, queueno);
  
  return;
}

void CACDQueue::DeleACDQueue(int nAcd)
{
  US Prior, Next;

  Prior = pACD_str[nAcd].Prior;
  Next = pACD_str[nAcd].Next;
  
  pACD_str[Prior].Next = Next;
  if ( Next < MaxAcdNum ) 
    pACD_str[Next].Prior = Prior;

  pACD_str[nAcd].state = 0;
}

int CACDQueue::Get_nAcd_By_DialSerialNo(UL DialSerialNo)
{
  int nAcd;

  if (isInit == false) return 0xFFFF;

  for (nAcd = 1; nAcd < MaxAcdNum; nAcd ++)
  {
    if (pACD_str[nAcd].state == 1 && pACD_str[nAcd].Session.DialSerialNo == DialSerialNo)
      return nAcd;
  }
  return 0xFFFF;
}

