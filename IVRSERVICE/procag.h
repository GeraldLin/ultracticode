//---------------------------------------------------------------------------
#ifndef ProcagH
#define ProcagH

//---------------------------------------------------------------------------
void ReadWorkerGroupName();
void ReadLocaSeatParam(char *filename, int maxseatnum); //讀內線分機電話配置
int SetOneLocaSeatParam(int nSeatType, int nPortNo, const char *pszSeatData, int &nAG, char *pszError);
void SetLocalSeatChn();
void ReadDTSeatParam(char *filename); //讀數字內線座席配置
void ReadRemoteSeatParam(char *filename); //讀外線坐席參數
int SetOneRemoteSeatParam(int nSeatType, int nPortNo, const char *pszSeatData, int &nAG, char *pszError);
int ModifySeatNo(int nAG, const CStringX NewSeatNo);
void SendAllLoginWorkerstatus();
void SendAllLogoutWorkerstatus();

void DispAgentStatus(int nAG);

//發送坐席狀態
void SendAGSelfState(int nAG);
void SendAllAGStateToOne(int nAG);
void SendOneAGStateToAll(int nAG1);

void SendWorkerLoginStateToDesCTI(int nAG, int nState);
void SendAllAGLoginStateToDesCTI();

//發送通道狀態
void SendAllCHStateToOne(int nAG);
void SendOneCHStateToAll(int nChn);

void SendOneCHStateToOppIVR(int nChn);
void SendAllCHStateToOppIVR();

//取ACD排隊信息
int GetQueueInfo(CStringX &queueinfo, int &waitacdcount, int &waitanscount);
//發送ACD分配簡單隊列信息
void SendACDQueueInfo(int nAG);
void SendACDQueueInfoToAll();
void SendACDQueueInfoToAll(int queunum, CStringX queueinfo);

//設置并顯示坐席服務狀態
void SetAgentsvState(CAgent *pAgent, int svstate);
void SetAgentsvState(int nAG, int svstate);

//根據工號取坐席,返回值nResult：0-表示沒有該工號 1-表示有該工號且處于空閑狀態 2-表示有該工號但處于非空閑狀態
CAgent *GetAgentbyWorkerNo(int WorkerNo, int &nResult);
//根據坐席號取坐席,返回值nResult：0-表示沒有該坐席號 1-表示有該坐席號且處于空閑狀態 2-表示有該坐席號但處于非空閑狀態
CAgent *GetAgentbySeatNo(const CStringX SeatNo, int &nResult);
//根據遠端坐席外線號碼取坐席,返回值nResult：0-表示沒有該座席 1-表示有該座席且處于空閑狀態 2-表示有該座席但處于非空閑狀態
CAgent *GetAgentbyPhone(const CStringX PhoneNo, int &nResult);
//根據工號或座席號取坐席,返回值nResult：0-表示沒有該座席 1-表示有該座席且處于空閑狀態 2-表示有該座席但處于非空閑狀態
CAgent *GetAgentbyWorkerNoSeatNo(const CStringX  SeatNo, int WorkerNo, int acdrule, int &nResult, bool bOnlyPC=false);
int GetnAGbyWorkerNoSeatNo(const CStringX SeatNo, int WorkerNo);
//根據規則取空閑坐席,返回值：NULL-表示沒有符合規則的空閑座席 not NULL-表示返回了符合規則的空閑座席
CAgent *GetIdelAgentbyRule(CACDRule *ACDRule, int waittimer, int ringtimer, bool bOnlyPC=false);
int GetAgentListByRule(CACDRule *ACDRule, int nChn); //2015-12-07
//取空閑的坐席自動呼出
CAgent *GetIdelAgentForCallOut(CACDRule &ACDRule);
//取代接的坐席
int GetnAGForPickup(const char *seatno, int workerno, int groupno, int selectmode);
//發送坐席代接失敗結果消息
void SendSeatPickupResult(int nAG, int nOut, int point, int result, const char *error);
void SendSeatPickupResult(int nAG, int nAcd, int result, const char *error);

//根據通道號取坐席
CAgent *GetAgentBynChn(int nChn);
//根據通道取綁定的坐席類型
short GetSeatTypeBynChn(int nChn);

//取坐席對應的坐席電話通道狀態
short GetAgentChState(CAgent *pAgent);
short GetAgentChState(int nAG);
//該工號是否登錄
bool IsTheWorkerLogin(int WorkerNo);
//清楚當前相同的登錄賬號
void ClearTheSameAccountNo(const char *accountno);
void SaveWorkerAccountNo(int nAG, const char *accountno);

void AgentTimerAdd(int nTCount);
void ResetAllAG();
void LogoutAllAG();
//-----------------------------------------------------------------------------
void SendMsg2AG(US ClientId, US MsgId, const CH *msg);
void SendMsg2AG(US ClientId, US MsgId, const CStringX &msg);
void SendMsg2WS(US ClientId, US MsgId, const CH *msg, bool bSaveId=true);

int Proc_Msg_From_AG(CXMLRcvMsg &AGMsg);
void Get_AGMSG_HeaderValue(CXMLRcvMsg &AGMsg);

void Set_IVR2AG_Header(int MsgId);
void Set_IVR2AG_Header(int MsgId, UL acdid);
void Set_IVR2AG_Item(int MsgId, int AttrId, const char *value);
void Set_IVR2AG_Item(int MsgId, int AttrId, const CStringX &value);
void Set_IVR2AG_IntItem(int MsgId, int AttrId, int value);
void Set_IVR2AG_LongItem(int MsgId, int AttrId, long value);
void Set_IVR2AG_Tail();

void SendCommonResult(int nAG, int onMsgId, int result, const char *error);
//發送坐席呼叫失敗結果消息
void SendSeatMakeCallFailResult(int nAG, const char*param, int result, const char *error);
//發送轉接失敗結果消息
void SendSeatTranCallFailResult(int nAG, const char*calledno, const char*param, int result, const char *error);
//發送穿梭通話失敗結果消息
void SendSeatJoinConfFailResult(int nAG, const char*param, int result, const char *error);
//發送將來話轉接到ivr結果消息
void SendSeatTranIVRFailResult(int nAG, int returnflag, const char*param, int result, const char *error);
//發送坐席代接失敗結果消息
void SendSeatPickupFailResult(int nAG, const char*param, int result, const char *error);
//發送坐席接管電話失敗結果消息
void SendSeatTakeOverFailResult(int nAG, const char*param, int result, const char *error);
//發送監聽失敗結果消息
void SendSeatListenFailResult(int nAG, const char*param, int result, const char *error);
//發送強插失敗結果消息
void SendSeatBreakinFailResult(int nAG, const char*param, int result, const char *error);

//發送停止呼入分配坐席消息
void SendStopACDCallIn(int nAG);
//發送應答呼入電話的結果
void SendAnswerCallResult(int nAG);
//發送坐席話機消息
void SendSeatHangon(int nAG);

//-----------------------------------------------------------------------------
int Proc_AGMSG_seatlogin(CXMLRcvMsg &AGMsg); //電腦坐席登錄
int Proc_AGMSG_workerlogin(CXMLRcvMsg &AGMsg); //話務員登錄
int Proc_AGMSG_workerlogout(CXMLRcvMsg &AGMsg); //話務員退出

int Proc_AGMSG_disturb(CXMLRcvMsg &AGMsg); //免打擾設置
int Proc_AGMSG_leval(CXMLRcvMsg &AGMsg); //離席/在席設置
int Proc_AGMSG_setforward(CXMLRcvMsg &AGMsg); //設置呼叫轉移
int Proc_AGMSG_cancelforward(CXMLRcvMsg &AGMsg); //取消呼叫轉移

int Proc_AGMSG_answercall(CXMLRcvMsg &AGMsg); //應答來話
int Proc_AGMSG_makecall(CXMLRcvMsg &AGMsg); //發起呼叫
int Proc_AGMSG_hangon(CXMLRcvMsg &AGMsg); //坐席掛機

int Proc_AGMSG_blindtrancall(CXMLRcvMsg &AGMsg); //快速轉接電話
int Proc_AGMSG_consulttrancall(CXMLRcvMsg &AGMsg); //協商轉接電話
int Proc_AGMSG_conftrancall(CXMLRcvMsg &AGMsg); //會議轉接電話
int Proc_AGMSG_stoptrancall(CXMLRcvMsg &AGMsg); //停止轉接
int Proc_AGMSG_confspeak(CXMLRcvMsg &AGMsg); //指定通道會議發言
int Proc_AGMSG_confaudit(CXMLRcvMsg &AGMsg); //指定通道旁聽會議

int Proc_AGMSG_tranivr(CXMLRcvMsg &AGMsg); //轉接ivr流程

int Proc_AGMSG_senddtmf(CXMLRcvMsg &AGMsg); //發送DTMF按鍵
int Proc_AGMSG_play(CXMLRcvMsg &AGMsg); //播放語音文件
int Proc_AGMSG_stopplay(CXMLRcvMsg &AGMsg); //停止放音

int Proc_AGMSG_pickup(CXMLRcvMsg &AGMsg); //代接,代接指定的電話
int Proc_AGMSG_pickupqueuecall(CXMLRcvMsg &AGMsg); //搶接ACD隊列里的呼叫
int Proc_AGMSG_takeover(CXMLRcvMsg &AGMsg); //接管其他坐席的電話
int Proc_AGMSG_listen(CXMLRcvMsg &AGMsg); //監聽
int Proc_AGMSG_insert(CXMLRcvMsg &AGMsg); //強插通話

int Proc_AGMSG_hold(CXMLRcvMsg &AGMsg); //保持/取消保持
int Proc_AGMSG_mute(CXMLRcvMsg &AGMsg); //靜音/取消靜音
int Proc_AGMSG_releasecall(CXMLRcvMsg &AGMsg); //釋放一方通話,

int Proc_AGMSG_forceclear(CXMLRcvMsg &AGMsg); //強拆其他坐席
int Proc_AGMSG_forcelogout(CXMLRcvMsg &AGMsg); //強制將其他話務員退出
int Proc_AGMSG_forcedisturb(CXMLRcvMsg &AGMsg); //強制其他坐席免打擾
int Proc_AGMSG_forceready(CXMLRcvMsg &AGMsg); //強制其他坐席空閑

int Proc_AGMSG_getqueueinfo(CXMLRcvMsg &AGMsg); //取ACD分配隊列信息
int Proc_AGMSG_getagentstatus(CXMLRcvMsg &AGMsg); //取所有坐席的狀態信息
int Proc_AGMSG_getchnstatus(CXMLRcvMsg &AGMsg); //取所有通道的狀態信息

int Proc_AGMSG_sendmsgtoflw(CXMLRcvMsg &AGMsg); //發送消息到流程
int Proc_AGMSG_sendfax(CXMLRcvMsg &AGMsg); //發送傳真
int Proc_AGMSG_recvfax(CXMLRcvMsg &AGMsg); //接收傳真,通話后接收傳真
int Proc_AGMSG_sendmessage(CXMLRcvMsg &AGMsg); //發送文字信息
int Proc_AGMSG_sendsms(CXMLRcvMsg &AGMsg); //發送短信

int Proc_AGMSG_createconf(CXMLRcvMsg &AGMsg); //創建會議
int Proc_AGMSG_destroyconf(CXMLRcvMsg &AGMsg); //釋放會議

int Proc_AGMSG_setacwtimer(CXMLRcvMsg &AGMsg); //設置事后處理時長(秒)
int Proc_AGMSG_setacwend(CXMLRcvMsg &AGMsg); //設置事后處理完成

int Proc_AGMSG_swaphold(CXMLRcvMsg &AGMsg); //穿梭保持通話

int Proc_AGMSG_setgroupname(CXMLRcvMsg &AGMsg); //設置技能組名稱

int Proc_AGMSG_startrecord(CXMLRcvMsg &AGMsg); //開始錄音
int Proc_AGMSG_stoprecord(CXMLRcvMsg &AGMsg); //停止錄音

int Proc_AGMSG_setcalldirection(CXMLRcvMsg &AGMsg); //設置呼叫方向(0-雙向服務 1-呼入 2-呼出)

int Proc_AGMSG_pickupex(CXMLRcvMsg &AGMsg); //代接電話擴展指令
int Proc_AGMSG_redirectcall(CXMLRcvMsg &AGMsg); //電話重新定向振鈴

int Proc_AGMSG_setgoouttel(CXMLRcvMsg &AGMsg); //設置外出值班電話

int Proc_AGMSG_queryivrstatus(CXMLRcvMsg &AGMsg);	//查詢IVR運行狀態

void ProcAGMsg(CXMLRcvMsg &AGMsg); //處理坐席發來的消息

void ProcWebConnected(const char *pszDialMsg);
void ProcWebDisconnected(const char *pszDialMsg);

int ProcWebLogin(const char *pszDialMsg, int SendResult=0); //處理web話務員注冊消息
int ProcWebLegwork(const char *pszDialMsg, int SendResult=0); //處理web話務員外出值班消息
int ProcWebLogOut(const char *pszDialMsg, int SendResult=0); //處理web話務員退出消息
int ProcWebDialOut(const char *pszDialMsg, int SendResult=0); //處理web外呼消息

int ProcWebSetDisturb(const char *pszDialMsg, int SendResult=0); //處理示忙/示閑消息
int ProcWebSetLeave(const char *pszDialMsg, int SendResult=0); //處理小休離席消息
int ProcWebAnswerCall(const char *pszDialMsg, int SendResult=0); //處理應答來話消息
int ProcWebSetHold(const char *pszDialMsg, int SendResult=0); //處理保持/取消保持消息
int ProcWebReHeld(const char *pszDialMsg, int SendResult=0); //處理取消轉接再保持組合消息 2015-12-07
int ProcWebHangon(const char *pszDialMsg, int SendResult=0); //處理掛機釋放消息
int ProcWebTransferCall(const char *pszDialMsg, int SendResult=0); //處理轉接電話消息
int ProcWebStopTransfer(const char *pszDialMsg, int SendResult=0); //處理取消轉接電話消息
int ProcWebCompleteTransfer(const char *pszDialMsg, int SendResult=0); //處理完成轉接電話消息
int ProcWebPickupCall(const char *pszDialMsg, int SendResult=0); //處理代接電話消息
int ProcWebListenCall(const char *pszDialMsg, int SendResult=0); //處理監聽電話消息
int ProcWebInsertCall(const char *pszDialMsg, int SendResult=0); //處理強插電話消息
int ProcWebSetACWTimer(const char *pszDialMsg, int SendResult=0); //處理設定話后處理時長消息
int ProcWebSetACWEnd(const char *pszDialMsg, int SendResult=0); //處理結束話后處理消息
int ProcWebForceClear(const char *pszDialMsg, int SendResult=0); //處理強拆其他座席消息
int ProcWebForceLogout(const char *pszDialMsg, int SendResult=0); //處理強制將其他座席登出消息
int ProcWebForceReady(const char *pszDialMsg, int SendResult=0); //處理強制將其他坐席示閑消息
int ProcWebForceBusy(const char *pszDialMsg, int SendResult=0); //處理強制將其他坐席示忙消息
int ProcWebSendMessage(const char *pszDialMsg, int SendResult=0); //處理發送文字信息給其他坐席消息
int ProcWebSetForwarding(const char *pszDialMsg, int SendResult=0); //處理設置呼轉消息
int ProcWebSendDTMF(const char *pszDialMsg, int SendResult=0); //處理通話后發送DTMF消息
int ProcWebTransferIVR(const char *pszDialMsg, int SendResult=0); //處理坐席轉IVR消息
int ProcWebPickupACDCall(const char *pszDialMsg, int SendResult=0); //處理代接ACD隊列電話消息 //2015-12-14

//增加HTTP會議接口 2016-12-13
int ProcWebCreateConf(const char *pszDialMsg, UC WsTcpLinkNo=0); //處理創建會議消息
int ProcWebJoinConf(const char *pszDialMsg, UC WsTcpLinkNo=0); //處理增加會議成員消息
int ProcWebUnJoinConf(const char *pszDialMsg, UC WsTcpLinkNo=0); //處刪除會議成員消息
int ProcWebCloseConf(const char *pszDialMsg, UC WsTcpLinkNo=0); //處理關閉會議消息

void ProcHTTPMsg(const char *pszDialMsg); //處理web http消息
void ProcWEBSOCKETMsg(const char *pszDialMsg); //處理web socket消息
void ProcIMCallInMsg(CXMLMsg &IMMsg);
void ProcIMCancelMsg(CXMLMsg &IMMsg);
void ProcEMCallInMsg(CXMLMsg &EMMsg);
void ProcEMCancelMsg(CXMLMsg &EMMsg);
void ProcFAXCallInMsg(CXMLMsg &FAXMsg);
void ProcFAXCancelMsg(CXMLMsg &EFAXMsg);

//-----------------------------------------------------------------------------
//設置坐席轉接呼叫失敗后重新返回坐席的分配參數
void SetTranCallFailReturnTypeACDData(int nChn, int nAG, int TranCallFailReturn);
//將組內所有的坐席號設置到被叫號碼表
void SetGroupSeatNo(int nAcd);
//將需要呼叫的坐席號碼設置到呼出隊列
void SetAcdedSeatNo(int nAcd, CAgent *pAgent);
int SetACDCalloutQue(int nAcd, int calledpoint, bool istheseatno);
void ClearCalloutIdBynAG(int nAG);
void DelFromQueue(int nAcd);
//處理ACD分配記錄
void ProcACDData(int nAcd, int WaitCount);
void ProcIMACDData(int nAcd, int WaitCount);
//處理ACD隊列
void ProcAcdQueue();
//取等待隊列數量
void GetACDQueue(int groupno, int &acdwaitcount, int &acdwaitans);
//坐席實時話務量統計
void AgentCallMeteCount();
void AgentStatusCount(int nAG);
void AgentStatusCount();
//發送即時統計數據
void SendNowAgentCallMeteCount();
//發送坐席狀態實時統計數據
void SendAllSeatStatusMeteCount();
void SendOneSeatStatusMeteCount(int nAG);
void SendOneSeatStatusMeteCount(int nAG, int nLOG);
void SendAllSeatStatusMeteCount(int nLOG);

void SendAllGroupStatusMeteCount();
void SendOneGroupStatusMeteCount(int nAG);
void SendOneGroupStatusMeteCountB(int nGroupNo);
void SendOneGroupStatusMeteCount(int nGroupNo, int nLOG);
void SendAllGroupStatusMeteCount(int nLOG);

void SendSystemStatusMeteCount();
void SendSystemStatusMeteCount(int nLOG);
//---------------------------------------------------------------------------
void WriteWorkerLoginStatus(CAgent *pAgent);
void WriteWorkerLogoutStatus(CAgent *pAgent);
void WriteWorkerLogoutRecord(CAgent *pAgent);
void WriteWorkerStatus(CAgent *pAgent, int nStatus, time_t tStartTime, time_t tEndTime);
void WriteWorkerStatus(CAgent *pAgent, char *cdrserialno, int nFlag);
void WriteWorkerStatus(CAgent *pAgent, int nFlag);
void WriteWorkerStatus(int nAG, char *cdrserialno, int nFlag);
void WriteChnIVRStatus(int nChn, int nStatus);

void WriteSeatStatus(CAgent *pAgent, int nStatus, int nFlag);
void WriteSeatStatus(CAgent *pAgent, int nStatus, int nFlag, int nInOutFlag, int nAnsFlag);

void WriteAbandonStatus(CAgent *pAgent, int nStatus, time_t tStartTime, time_t tEndTime);

int  ProcExtnTransferCall(int nChnSrc, const char *pszSeatNo, int nCalledType, int nTranType, const char *pszCalledNo);
int  ProcExtnStopTransfer(const char *pszSeatNo);
//---------------------------------------------------------------------------
#endif
