//---------------------------------------------------------------------------
#ifndef MixervocH
#define MixervocH
//---------------------------------------------------------------------------
//加載內存索引文件
void Load_MemPlayIndex(const char *filename, const char *vocpath);
//增加內存放音索引
int AddMemIndexToPlayBuf(int nChn, int MemIndex, char *ErrMsg);
//規范日期時間字符串
int FormatDateTimeStr(const CH *pszInDTStr, CH *pszOutDTStr, int &nMGYear);
//將日期時間合成普通話語音
int MixDateTime2CHN(const CH *pszTime, int nChn, CH *pszErrMsg, int nMGYear=0, int nAMPM=0);
//將金額合成普通話
int MixMoney2CHN(DF fMoney, int nChn, CH *pszErrMsg);
//將數值合成普通話
int MixNumber2CHN(DF fNumber, const CH *pszNumber, int nChn, CH *pszErrMsg);
//將數字串合成普通話
int MixDigits2CHN(const CH *pszDigits, int nChn, CH *pszErrMsg);

//將日期時間合成廣東話
int MixDateTime2GDH(const CH *pszTime, int nChn, CH *pszErrMsg);
//將金額合成廣東話
int MixMoney2GDH(DF fMoney, int nChn, CH *pszErrMsg);
//將數值合成廣東話
int MixNumber2GDH(DF fNumber, int nChn, CH *pszErrMsg);
//將數字串合成廣東話
int MixDigits2GDH(const CH *pszDigits, int nChn, CH *pszErrMsg);

//將日期時間合成英文
int MixDateTime2ENG(const CH *pszTime, int nChn, CH *pszErrMsg);
//將金額合成英文
int MixMoney2ENG(DF fMoney, int nChn, CH *pszErrMsg);
//將數值合成英文
int MixNumber2ENG(DF fNumber, int nChn, CH *pszErrMsg);
//將數字串合成英文
int MixDigits2ENG(const CH *pszDigits, int nChn, CH *pszErrMsg);
//合成英文字母串
int MixChars(const CH *pszChars, int nChn, CH *pszErrMsg);

int AddSingleFileToPlayBuf(int nChn, const CH *FileName, int StartPos, int Length, CH *ErrMsg);
int AddFileToPlayBuf(int nChn, const CH *FileName, CH *ErrMsg);
int AddTTSStrToPlayBuf(int nChn, const CH *TTSStr, CH *ErrMsg);
int AddTTSFileToPlayBuf(int nChn, const CH *FileName, CH *ErrMsg);
int AddDTMFStrToPlayBuf(int nChn, const CH *DTMFStr, CH *ErrMsg);

int AddDateTimeToPlayBuf(int SpeechType, int nChn, const CH *TimeStr, CH *ErrMsg, int nMGYear=0, int nAMPM=0);
int AddMoneyToPlayBuf(int SpeechType, int nChn, double Money, CH *ErrMsg);
int AddNumberToPlayBuf(int SpeechType, int nChn, double Number, const CH *pszNumber, CH *ErrMsg);
int AddDigitsToPlayBuf(int SpeechType, int nChn, const CH *Digits, CH *ErrMsg);

int AddCharsToPlayBuf(int nChn, const CH *Chars, CH *ErrMsg);
int AddToneToPlayBuf(int nChn, int ToneType, int DtmfRuleId, int Repeat, int ReleaseId, CH *ErrMsg);
int AddMixToPlayBuf(int SpeechType, int nChn, int mixtype, const CH *content, CH *ErrMsg, int nMGYear=0, int nAMPM=0);

void ResetDtmfRule(int nChn);
void ResetPlayRule(int nChn);
void ResetPlayBuf(int nChn);
void ResetRecBuf(int nChn);
void Stop_Play_Clear_Buf(int nChn);
void Stop_Rec_Clear_Buf(int nChn);
void Stop_Dtmf_Clear_Buf(int nChn);
int Start_ChnPlay(int nChn);
int Start_VopChnPlay(int nChn);

int StopClearPlayDtmfRecBuf(int nChn);
int StopClearPlayDtmfBuf(int nChn);
int StopClearRecDtmfBuf(int nChn);

void SetcurPlayRuleData(int nChn, int PlayRuleId);
void SetcurDtmfRuleData(int nChn, int DtmfRuleId);
int SetDTMFPlayRule(int nChn, int DtmfRuleId, int PlayRuleId, CH *ErrMsg);
int SetPlayRule(int nChn, int PlayRuleId, CH *ErrMsg);
int SetDTMFRule(int nChn, int DtmfRuleId, CH *ErrMsg);
int SetChnRecordRule(int nChn, int BeepId, const CH *FileName, const CH *TermChar, int Format, int MaxTime, int WriteMode, char *errorbuf, int mixterid=0);
int SetChnMonitorRecordRule(int nChn, const CH *FileName, int Format, int MaxTime, int WriteMode);

int SetConfRecordRule(int nCfc, const CH *FileName, int Format, int MaxTime, int WriteMode);

int AddFileToConfPlayBuf(int nCfc, const CH *FileName, CH *ErrMsg);
void Stop_Play_Clear_ConfBuf(int nCfc);
void Stop_Rec_Clear_ConfBuf(int nCfc);
int Start_ConfPlay(int nCfc);
int SetConfPlayRule(int nCfc, int nChn, int PlayRuleId, CH *ErrMsg);

int GetIVRChn(int nRouteNo, const CH *Extension);
//---------------------------------------------------------------------------
#endif
