//---------------------------------------------------------------------------
#ifndef PrococxH
#define PrococxH
//---------------------------------------------------------------------------
short MySetBit(short nValue, short nBit, short nBitValue);
//--------------------放、錄音操作-------------------------------------------
bool GetVopChnBynChn(int nChn, short &VopNo, short &VopChnNo);
//動態加載索引放音用的語音文件
int AddVoiceIndex(int index, const char *filename);
//坐席發送文件放音消息
int AgentPlayFile(int nChn, const CStringX filename, long startpos);
//發送單個文件放音消息
int PlaySingleFile(int nChn);
int PlayFileLoop(int nChn, char *filename);
//判斷放音序號
void GetPlayFirstLast(int nChn, short &isfirst, short &islast);
//發送文件放音消息
int PlayFile(int nChn, int PlayIndex);
//文件放音
int PlayFile(int nChn, const char *szFileName, int nDtmfRuleId, int nPlayRuleId);
//發送內存索引放音消息
int PlayIndex(int nChn, int PlayIndex);
//放信號音
int PlayTone(int nChn, int index, int isloop);
//發送內存索引放音消息
int PlayTTS(int nChn, int PlayIndex);
//發送文本文件放音消息
int PlayTTSFile(int nChn, int PlayIndex);
//發送內存放音消息
int PlayMemory(int nChn, unsigned char *pmem, unsigned long length);
//發送文件錄音消息
int RecordFile(int nChn);
//發送停止放音消息
int StopPlay(int nChn);
//暫停放音
int PausePlay(int nChn);
//恢復暫停放音
int ReStartPlay(int nChn);
//發送停止錄音消息
int StopRecord(int nChn);
//設置錄放音動態參數
int SetRPparam(int nChn, short SetParam);
//向前快進
int FastFwdPlay(int nChn);
//向后快進
int FastBwdPlay(int nChn);
//設置放音音量
int SetPlayVolume(int nChn, int volume);
//設置放音上總線(即放音時對方也能聽到)
int SetPlayBus(int nChn, short nBusFlag);
//設置錄音參數
int SetRecdParam(int nChn, short nMixFlag, short nAGCFlag);
//設置變聲參數
int SetVCParam(int nChn, int vcmode, int vcparam, int vcmix, int noisemode, int noiseparam);
//發送FSK串消息
int SendFSKStr(int nChn, short FskHeader, short FskCMD, const char *FskData, short FskDataLen, short CRCType);
//開始接收FSK
int StartRecvFSKStr(int nChn, short FskHeader, short CRCType);
//發送DTMF串消息
int SendDTMFStr(int nChn);
int SendDTMFStr(int nChn, const char *dtmfs);

//判斷會議放音序號
void ConfGetPlayFirstLast(int nCfc, int PlayIndex, short &isfirst, short &islast);
//發送會議文件放音消息
int ConfPlayFile(int nCfc, int PlayIndex);
//播放會議提示音
int ConfPlayKnockTone(int nCfc, int KnockTone);
//發送會議內存索引放音消息
int ConfPlayIndex(int nCfc, int PlayIndex);
//會議放信號音
int ConfPlayTone(int nCfc, int index, int isloop);
//發送會議內存索引放音消息
int ConfPlayTTS(int nCfc, int PlayIndex);
//發送會議文本文件放音消息
int ConfPlayTTSFile(int nCfc, int PlayIndex);
//發送會議文件錄音消息
int ConfRecordFile(int nCfc);
//發送會議停止放音消息
int ConfStopPlay(int nCfc);
//發送會議停止錄音消息
int ConfStopRecord(int nCfc);
//設置會議錄放音動態參數
int SetConfRPparam(int nCfc, int RecWithPlayVoice, int PlayVol);

//------------------呼叫操作-------------------------------------------------
//發送ACM消息
int SendACM(int nChn, int AcmPara);
//發送ACK消息
int SendACK(int nChn, int TollId);
//發送釋放通道消息
int Release(int nChn, int Reason);
//掛機
int Hangup(int nChn);
int HangonIVR(int nChn);
//發送呼出消息
int Callout(int nChn, int RouteNo, int CallPara, const char *caller, const char *called, const char *orgcalled, short routeno=0);
//發送后續地址消息
int Callinfo(int nChn, const char *called);
//發送取消呼出消息
int CancelCallout(int nChn, int nChn1=0xFFFF);
//發送模擬外線拍叉簧消息
int Flash(int nChn);
//自動判斷交換機呼出號碼是否加撥字冠
void AutoAddCallOutPreCode(const char *called, char *newcalled, int nTranType);
//轉接電話（拍叉簧方式）
int Transfer(int nChn, short TranType, LPCTSTR Called, const char *TranData);
//停止轉接電話（拍叉簧方式）
int StopTransfer(int nChn, short Reason);
//------------------交換操作-------------------------------------------------
//通道交換函數
int RouterTalk(int nChn1, int nChn2);
int UnRouterTalk(int nChn1, int nChn2);
int RouterMonitor(int nChn1, int nChn2, int nVolume=0);
int UnRouterMonitor(int nChn1, int nChn2);
int RouterCbm(int nChn1, int nChn2);
int UnRouterCbm(int nChn1, int nChn2);
int RouterInVC(int nChn1, int nChn2);
int RouterOutVC(int nChn1, int nChn2);
int RouterIOVC(int nChn1, int nChn2);
int UnRouterVC(int nChn1, int nChn2);

//------------------傳真操作-------------------------------------------------
int SendFAX(int nChn, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
int AppendFAX(int nChn, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
int RecvFAX(int nChn, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR BarCode);
int StopFAX(int nChn);
int CheckTone(int nChn, int tonetype, const char *toneparam);

//------------------會議操作-------------------------------------------------
//創建會議
int CreateConf(int ConfNo, int nChn, int Talkers, int Listeners, int SysConfId);
//刪除會議
int DestroyConf(int ConfNo, int nChn, int DestroyId);
//參加會議
int JoinConf(int ConfNo, int nChn, int TalkVol);
//退出會議
int UnjoinConf(int ConfNo, int nChn, int IgnoreConfNo);
//旁聽會議
int ListenConf(int ConfNo, int nChn);
//退出旁聽會議
int UnListenConf(int ConfNo, int nChn, int IgnoreConfNo);
//創建三方會議(返回值：0表示創建失敗 >0表示返回的會議號)
int CreateThreeConf(int nChn);
//加入三方會議
int JoinThreeConf(int nCfc, int nChn, int nChn1, int nChn2);

//刪除會議成員數據
int DelConfMember(int nCfc, int nChn, int &Jointype);
//刪除會議成員數據
int DelAllConfMember(int nCfc, int destroyid);

void ClearTransferLink(int nChn);
//退出已加入的會議、斷開已經交換的通道
void UnLink_All_By_ChnNo(int nChn, int RelId);
//釋放通道處理
void Release_Chn(int nChn, int SendRelId, int SendMsgId);

//---------------------------------------------------------------------------
#endif
