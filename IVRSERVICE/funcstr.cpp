//---------------------------------------------------------------------------
#include "stdafx.h"
#include "funcstr.h"
//---------------------------------------------------------------------------
//---------------才﹃ㄧ计--------------------------------------------------
//才﹃
US MyStrLen( const CH *vString )
{
    static US result;
    result = strlen( vString );
    return result;
}
//奔才﹃ㄢ繷
CH *MyTrim( const CH *vString )
{
		static char temp[MAX_VAR_DATA_LEN];
    int i,j;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	if(!isspace(vString[i]))
    		break;
    for(j=len-1;j>=0;j--)
    	if(!isspace(vString[j]))
    		break;
    len=j+1-i;		
    if(len>=0)
    {
    	memcpy(temp,vString+i,len);
    	temp[len]=0;
    }
    else
    	temp[0]=0;	
    return temp;			
}
//奔才﹃オ
CH *MyLeftTrim( const CH *vString )
{
		static char temp[MAX_VAR_DATA_LEN];
    int i;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	if(!isspace(vString[i]))
    		break;
    memcpy(temp,vString+i,len-i);
    temp[len-i]=0;
    return temp;			
}
//奔才﹃娩
CH *MyRightTrim( const CH *vString )
{
		static char temp[MAX_VAR_DATA_LEN];
    int i;
    int len=strlen(vString);
    for(i=len-1;i>=0;i--)
    	if(!isspace(vString[i]))
    		break;
    memcpy(temp,vString,i+1);
    temp[i+1]=0;
    	
    return temp;			
}
//眖オ才﹃
CH *MyLeft( const CH *vString, const US vLenght )
{
		static char temp[MAX_VAR_DATA_LEN];
		strcpy(temp,vString);
		temp[vLenght]=0;
		return temp;
}
//眖才﹃
CH *MyRight( const CH *vString, const US vLenght )
{
		static char temp[MAX_VAR_DATA_LEN];
	  US len=strlen(vString);
    US mlen=vLenght>len?len:vLenght;
	  	
    memcpy(temp,vString+(len-mlen),mlen);
    temp[mlen]=0;
    	
    return temp;			
}
//才﹃
CH *MySubString( const CH *vString, const US vStart, const US vLenght )
{
		static char temp[MAX_VAR_DATA_LEN];
	  US len=strlen(vString);
	  US mstart=vStart>=len?len-1:vStart;
    US mlen=vLenght>(len-mstart)?(len-mstart):vLenght;
	  	
    memcpy(temp,vString+mstart,mlen);
    temp[mlen]=0;
    	
    return temp;			
}
//才﹃
CH *MyStrAdd( const CH *vString1, const CH *vString2 )
{
		static char temp[MAX_VAR_DATA_LEN];
	  	
    strcpy(temp,vString1);
    strcat(temp,vString2);
    
    return temp;		
}
//才﹃糶
CH *MyLower( const CH *vString )
{
    static char temp[MAX_VAR_DATA_LEN];
 		int i;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	temp[i]=tolower(vString[i]);
    temp[len]=0;	
   	return temp;
}
//才﹃糶
CH *MyUpper( const CH *vString )
{
	 static char temp[MAX_VAR_DATA_LEN];
 		int i;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	temp[i]=toupper(vString[i]);
    temp[len]=0;	
    return temp;
}
//才﹃竚
US MyStrPos( const CH *vString1, const CH *vString2 )
{
    char *result;
    US nPos;
    result = strstr(vString1, vString2);
		if(result==0)
			return 0xffff;
    nPos = result-vString1;
    return nPos;
}
//ゑ耕才﹃
UC MyStrCmp( const CH *vString1, const CH *vString2 )
{
    UC result;
    if ( strcmp(vString1,vString2) == 0 )
    {
        result = 0;
    }
    else
    {
        result = 1;
    }
    return result;
}
//┛菠糶ゑ耕才﹃
UC MyStrCmpNoCase( const CH *vString1, const CH *vString2 )
{
  UC result;
#ifdef WIN32 //win32吏挂
  if ( strcmpi(vString1,vString2) == 0 )
#else //LINUX吏挂
    if ( strcasecmp(vString1,vString2) == 0 )
#endif
    {
      result = 0;
    }
    else
    {
      result = 1;
    }
    return result;
}
//だ瞒ゅセ︽(,)
int SplitTxtLine(const char *txtline, int maxnum, CStringX *arrstring)
{
  int len, i = 0, k = 0, num = 0;
  char ch, ch1, ch2 = 0, strtemp[64];

  memset(strtemp, 0, 64);
  len = strlen(txtline);
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    ch1 = ch2;
    ch2 = ch;
    if (ch1 == '/' && ch2 == '/')
    {
      //称猔夹в  
      if (k > 0)
      {
        strtemp[k] = '\0';
        arrstring[num] = strtemp;
        num ++;
      }
      break;
    }
    if (ch == ',' || ch == ';'   || ch == ' ' || ch == '\n' || ch == '\t' || ch == 0)
    {
      //丁筳夹в
      if (k == 0 && (ch != ',' && ch != ';'))
        continue;
      strtemp[k] = '\0';
      arrstring[num] = strtemp;  
      num ++;
      k = 0;
      if (num >= maxnum)
        break;
    }
    else
    {
      if (k < 64)
      {
        strtemp[k] = ch;
        k ++;
      }
    }
  }
  return num;
}
//沮だ瞒才だ澄才﹃
int SplitString(const char *txtline, const char splitchar, int maxnum, CStringX *arrstring)
{
  int len, i = 0, k = 0, num = 0;
  char ch, strtemp[64];

  memset(strtemp, 0, 64);
  len = strlen(txtline);
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    if (ch == splitchar || ch == 0)
    {
      //丁筳夹в
      strtemp[k] = '\0';
      arrstring[num] = strtemp;  
      num ++;
      k = 0;
      if (num >= maxnum)
        break;
    }
    else
    {
      if (k < 64)
      {
        strtemp[k] = ch;
        k ++;
      }
    }
  }
  return num;
}
//浪琩code 琌で皌Rule(珹硄皌才)
bool IsStrMatch(const CStringX &Rule,const CStringX &code)
{
	unsigned int pos1=0;
	unsigned int pos2=0;
	unsigned int len=code.GetLength();
	while((pos1<Rule.GetLength()) && (pos2<len))
	{
		if(Rule[pos1]!=code[pos2])
		{
			if(Rule[pos1]=='*') 
			{
				pos2++;
				continue;	
			}
			else if(Rule[pos1]!='?') 
				return false;				
		}	
		pos1++;
		pos2++;
	}
	if(pos2==len)
		if(pos1==Rule.GetLength()) 
			return true;
		else if(Rule[pos1]=='*')
			return true;	
	
	return false;	
}

int CheckIPv4Address(const char *pszIPAddr)
{
  //浪琩
  int nIPlen=strlen(pszIPAddr);
  
  if(nIPlen < 7 || nIPlen > 15)
  {
    return 1;
  }
  //浪琩翴だ筳才计
  int i, nDotCount=0;
  for(i=0;i<nIPlen;i++)
  {
    if (pszIPAddr[i] != '.' && (pszIPAddr[i] < '0' || pszIPAddr[i] > '9'))
      return 2;
    if(pszIPAddr[i]=='.')
      nDotCount++;
  }
  
  if(nDotCount != 3)
  {
    return 2;
  }
  char strIp[4][20];
  for(i=0;i<4;i++)
    memset(strIp[i],0,20);
  sscanf(pszIPAddr, "%20[^.].%20[^.].%20[^.].%20[^.]", strIp[0],strIp[1],strIp[2],strIp[3]);
  
  for(i=0;i<4;i++)
  {
    if (strlen(strIp[i]) == 0)
      return 3;
    if(atoi(strIp[i]) > 255 || atoi(strIp[i]) < 0)
    {
      return 4;
    }
  }
  return 0;
}
char *MyGetGUID()
{
  static char szGUID[MAX_CHAR64_LEN];
  
  memset(szGUID, 0, MAX_CHAR64_LEN);
  GUID guid = GUID_NULL;
  ::CoCreateGuid(&guid);
  if (guid == GUID_NULL)
    return "";
  sprintf(szGUID, "%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
    guid.Data1, guid.Data2, guid.Data3,
    guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
    guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);
  return szGUID;
}
void MyGetGUID(char *pszGUID)
{
  GUID guid = GUID_NULL;
  ::CoCreateGuid(&guid);
  if (guid == GUID_NULL)
  {
    strcpy(pszGUID, "00000000-0000-0000-0000-000000000000");
    return;
  }
  sprintf(pszGUID, "%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
    guid.Data1, guid.Data2, guid.Data3,
    guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
    guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);
}
//沮把计把计
char *GetParamByName(const char *paramstr, const char *paramname, const char *defaultval, int maxlen)
{
  static char param[256];
  char *pos1, *pos2, ParamName[128];
  short l, m, len;
  
  if (maxlen > 255)
  {
    m = 255;
  }
  else
  {
    m = maxlen;
  }
  memset(param, 0, 256);
  sprintf(ParamName, "%s=", paramname);
  l = strlen(ParamName);
  pos1 = strstr(paramstr, ParamName);
  if (pos1 == NULL)
  {
    strcpy(param, defaultval);
    return param;
  }
  pos1 +=l;
  pos2 = strstr(pos1, ";");
  if (pos2 == NULL)
  {
    strncpy(param, pos1, m);
  }
  else
  {
    len = pos2 - pos1;
    if (len > m)
    {
      len = m;
    }
    strncpy(param, pos1, len);
  }
  return param;
}

char *Mystristr( const char *vString1, const char *vString2 )
{
  return strstr(MyLower(vString1), MyLower(vString2));
}

char *GetParamStrValue(LPCTSTR ParamName, LPCTSTR MsgBuffer) 
{
	// TODO: Add your dispatch handler code here
  static char retvalue[256];
  char *pos1, *pos2, paramname[128], msgbuffer[2048];
  short l, len;
  
  memset(paramname, 0, 128);
  memset(msgbuffer, 0, 2048);

  l = strlen(MsgBuffer);
  if (l == 0)
  {
    return "";
  }
  strncpy(msgbuffer, MsgBuffer, (l>=2048)?2047:l);

  l = strlen(ParamName);
  if (l == 0)
  {
    return "";
  }
  strncpy(paramname, ParamName, (l>=127)?126:l);
  if (paramname[l-1] != '=')
  {
    paramname[l] = '=';
    paramname[l+1] = '\0';
  }
  
  pos1 = strstr(msgbuffer, paramname);
  if (pos1 == NULL)
  {
    return "";
  }
  pos1 +=l;
  pos2 = strchr(pos1, '`');
  if (pos2 == NULL)
    pos2 = strchr(pos1, '&');
  if (pos2 == NULL)
    pos2 = strchr(pos1, ';');
  if (pos2 == NULL)
    pos2 = strchr(pos1, '\0');
  memset(retvalue, 0, 256);
  if (pos2 == NULL)
  {
    return "";
  }
  else
  {
    len = pos2 - pos1;
    if (len >= 256)
    {
      len = 255;
    }
    strncpy(retvalue, pos1, len);
  }

	return retvalue;
}

char *GetParamStrValue1(LPCTSTR ParamName, LPCTSTR MsgBuffer) 
{
  // TODO: Add your dispatch handler code here
  static char retvalue[256];
  char *pos1, *pos2, paramname[128], msgbuffer[2048];
  short l, len;
  
  memset(paramname, 0, 128);
  memset(msgbuffer, 0, 2048);
  
  l = strlen(MsgBuffer);
  if (l == 0)
  {
    return "";
  }
  strncpy(msgbuffer, MsgBuffer, (l>=2048)?2047:l);
  
  l = strlen(ParamName);
  if (l == 0)
  {
    return "";
  }
  strncpy(paramname, ParamName, (l>=127)?126:l);
  if (paramname[l-1] != '=')
  {
    paramname[l] = '=';
    paramname[l+1] = '\0';
  }
  
  pos1 = strstr(msgbuffer, paramname);
  if (pos1 == NULL)
  {
    return "";
  }
  pos1 +=l;
  pos2 = strchr(pos1, '`');
  if (pos2 == NULL)
    pos2 = strchr(pos1, '&');
  if (pos2 == NULL)
    pos2 = strchr(pos1, ';');
  if (pos2 == NULL)
    pos2 = strchr(pos1, '\0');
  memset(retvalue, 0, 256);
  if (pos2 == NULL)
  {
    return "";
  }
  else
  {
    len = pos2 - pos1;
    if (len >= 256)
    {
      len = 255;
    }
    strncpy(retvalue, pos1, len);
  }
  
  return retvalue;
}

int GetParamIntValue(LPCTSTR ParamName, LPCTSTR MsgBuffer) 
{
	// TODO: Add your dispatch handler code here
  int lResult;
  char *pos1, *pos2, retvalue[32], paramname[128], msgbuffer[2048];
  short l, len;
  
  memset(paramname, 0, 128);
  memset(msgbuffer, 0, 2048);
  
  l = strlen(MsgBuffer);
  if (l == 0)
  {
    return 0;
  }
  strncpy(msgbuffer, MsgBuffer, (l>=2048)?2047:l);
  
  l = strlen(ParamName);
  if (l == 0)
  {
    return 0;
  }
  strncpy(paramname, ParamName, (l>=127)?126:l);
  if (paramname[l-1] != '=')
  {
    paramname[l] = '=';
    paramname[l+1] = '\0';
  }
  
  pos1 = strstr(msgbuffer, paramname);
  if (pos1 == NULL)
  {
    return 0;
  }
  pos1 +=l;
  pos2 = strchr(pos1, '`');
  if (pos2 == NULL)
    pos2 = strchr(pos1, '&');
  if (pos2 == NULL)
    pos2 = strchr(pos1, ';');
  if (pos2 == NULL)
    pos2 = strchr(pos1, ',');
  if (pos2 == NULL)
    pos2 = strchr(pos1, '\0');
  memset(retvalue, 0, 32);
  if (pos2 == NULL)
  {
    return 0;
  }
  else
  {
    len = pos2 - pos1;
    if (len >= 32)
    {
      len = 31;
    }
    strncpy(retvalue, pos1, len);
  }
  lResult = atoi(retvalue);

	return lResult;
}

//沮把计把计
bool GetParamStrValue(LPCTSTR ParamName, LPCTSTR MsgBuffer, char *pszParamValue)
{
  char *pos1, *pos2, retvalue[256], paramname[128], msgbuffer[2048];
  short l, len;
  
  memset(paramname, 0, 128);
  memset(msgbuffer, 0, 2048);
  
  l = strlen(MsgBuffer);
  if (l == 0)
  {
    pszParamValue[0] = '\0';
    return false;
  }
  strncpy(msgbuffer, MsgBuffer, (l>=2048)?2047:l);
  
  l = strlen(ParamName);
  if (l == 0)
  {
    pszParamValue[0] = '\0';
    return false;
  }
  strncpy(paramname, ParamName, (l>=127)?126:l);
  if (paramname[l-1] != '=')
  {
    paramname[l] = '=';
    paramname[l+1] = '\0';
  }
  
  pos1 = strstr(msgbuffer, paramname);
  if (pos1 == NULL)
  {
    pszParamValue[0] = '\0';
    return false;
  }
  pos1 +=l;
  pos2 = strstr(pos1, "`");
  if (pos2 == NULL)
    pos2 = strchr(pos1, '&');
  if (pos2 == NULL)
    pos2 = strstr(pos1, ";");
  memset(retvalue, 0, 256);
  if (pos2 == NULL)
  {
    pszParamValue[0] = '\0';
    return false;
  }
  else
  {
    len = pos2 - pos1;
    if (len >= 256)
    {
      len = 255;
    }
    strncpy(retvalue, pos1, len);
  }
  strcpy(pszParamValue, retvalue);
  
  return true;
}

bool GetParamIntValue(LPCTSTR ParamName, LPCTSTR MsgBuffer, long &lParamValue)
{
  char *pos1, *pos2, retvalue[32], paramname[128], msgbuffer[2048];
  short l, len;
  
  memset(paramname, 0, 128);
  memset(msgbuffer, 0, 2048);
  
  l = strlen(MsgBuffer);
  if (l == 0)
  {
    lParamValue = 0;
    return false;
  }
  strncpy(msgbuffer, MsgBuffer, (l>=2048)?2047:l);
  
  l = strlen(ParamName);
  if (l == 0)
  {
    lParamValue = 0;
    return false;
  }
  strncpy(paramname, ParamName, (l>=127)?126:l);
  if (paramname[l-1] != '=')
  {
    paramname[l] = '=';
    paramname[l+1] = '\0';
  }
  
  pos1 = strstr(msgbuffer, paramname);
  if (pos1 == NULL)
  {
    lParamValue = 0;
    return false;
  }
  pos1 +=l;
  pos2 = strstr(pos1, "`");
  if (pos2 == NULL)
    pos2 = strchr(pos1, '&');
  if (pos2 == NULL)
    pos2 = strstr(pos1, ";");
  memset(retvalue, 0, 32);
  if (pos2 == NULL)
  {
    lParamValue = 0;
    return false;
  }
  else
  {
    len = pos2 - pos1;
    if (len >= 32)
    {
      len = 31;
    }
    strncpy(retvalue, pos1, len);
  }
  lParamValue = atol(retvalue);
  
  return true;
}
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult)
{
  int i=0, nTemp, nLen, nOldLen, nNewLen, nCopyedLen=0;
  bool bStartReplace=false;
  char *pszStart, *pszFinded;
  
  memset(pszResult, 0, nOutputMaxLen);

  nOldLen = strlen(pszOldStr);
  nNewLen = strlen(pszNewStr);
  if (nOldLen == 0)
  {
    memcpy(pszResult, pszSource, nOutputMaxLen-1);
  }
  else
  {
    pszStart = (char *)pszSource;
    
    do 
    {
      pszFinded = strstr(pszStart, pszOldStr);
      if (pszFinded)
      {
        //т惠璶蠢传才﹃
        if (i < nStartIndex)
        {
          //⊿Τ惠璶传竚ま玥钡ī赣琿才
          nLen = (int)(pszFinded-pszStart)+nOldLen;
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strncat(pszResult, pszStart, nLen);
            pszStart = pszStart+nLen;
            nCopyedLen = nTemp;
          }
          else
          {
            //禬筁﹚玥挡
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
            break;
          }
        }
        else
        {
          //惠璶传竚ま玥ī赣琿玡ぃで皌才﹃發穝蠢传才
          nLen = (int)(pszFinded-pszStart);
          if (nLen > 0)
          {
            //ī玡ぃで皌才﹃
            nTemp = nCopyedLen + nLen;
            if (nTemp < nOutputMaxLen)
            {
              strncat(pszResult, pszStart, nLen);
              pszStart = pszStart+nLen+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszStart, nLen);
              break;
            }
            //發穝蠢传才
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
          else
          {
            //玡⊿Τぃで皌才﹃钡發穝蠢传才
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              pszStart = pszStart+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
        }
        i++;
        if (i>=nReplaceMaxNum)
        {
          //竒蠢传惠璶计ヘ
          nLen = strlen(pszStart);
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strcat(pszResult, pszStart);
            nCopyedLen = nTemp;
          }
          else
          {
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
          }
          break;
        }
      } 
      else
      {
        //ゼт惠璶蠢传才﹃
        nLen = strlen(pszStart);
        nTemp = nCopyedLen + nLen;
        if (nTemp < nOutputMaxLen)
        {
          strcat(pszResult, pszStart);
          nCopyedLen = nTemp;
        }
        else
        {
          nLen = nOutputMaxLen-nCopyedLen-1;
          strncat(pszResult, pszStart, nLen);
        }
        break;
      }
    } while (true);
  }
}
char *GetSIPPhoneCode(const char *SipUri)
{
  static char phonecode[32];
  char *p1, *p2;
  int len;
  
  p1 = strstr(SipUri, "sip:");
  if (p1 == NULL)
    return "";
  
  p2 = strstr(SipUri, "@");
  if (p2 == NULL)
    return "";
  
  if (p1 >= p2)
    return "";
  
  p1 += 4;
  len = (int)(p2-p1);
  if (len <= 0)
    return "";
  memset(phonecode, 0, 32);
  strncpy(phonecode, p1, len);
  return phonecode;
}
char *GetEmailAddr(const char *EmailUri)
{
  static char emailaddr[64];
  char *p1, *p2;
  int len;
  
  memset(emailaddr, 0, 64);
  p1 = strstr(EmailUri, "<");
  if (p1 == NULL)
    p1 = (char *)EmailUri;
  else
    p1++;
  
  p2 = strstr(p1, ">");
  if (p2 == NULL)
  {
    strncpy(emailaddr, p1, 63);
  }
  else
  {
    len = (int)(p2-p1);
    strncpy(emailaddr, p1, len);
  }
  return emailaddr;
}
//-----------------------------------------------------------------------------
int AnsiToUnicode(const char *ansiStr, wchar_t *outUnicode)
{
  int len=0;
  len = strlen(ansiStr);
  if (len == 0)
    return 0;
  int unicodeLen=::MultiByteToWideChar(CP_ACP, 0, ansiStr, -1, NULL, 0);
  memset(outUnicode, 0, (unicodeLen+1)*sizeof(wchar_t));
  ::MultiByteToWideChar(CP_ACP, 0, ansiStr, -1, (LPWSTR)outUnicode, unicodeLen);
  return unicodeLen;
}
int ConvUnicode2UniStr(WCHAR *pWchar, char *szUnicodeString, int iBuffSize)
{
  WCHAR c;
  int j=0;
  char szTemp[16];
  for (int i=0; i<iBuffSize; i++)
  {
    c = pWchar[i];
    if (c == 0 || (c >= ' ' && c <= '~'))
    {
      szUnicodeString[j] = (char)c;
      j++;
      szUnicodeString[j] = '\0';
    }
    else
    {
      sprintf(szTemp, "\\u%x", c);
      strcat(szUnicodeString, szTemp);
      j += strlen(szTemp);
    }
  }
  return j;
}
int ConvAnsiStrToUniStr(const char *ansiStr, char *ansiUniStr)
{
  WCHAR pwbBuffer[4096];
  memset(pwbBuffer, 0, sizeof(pwbBuffer));
  int len1,len2;
  
  len1 = AnsiToUnicode(ansiStr, pwbBuffer);
  if (len1 > 0)
  {
    len2 = ConvUnicode2UniStr(pwbBuffer, ansiUniStr, len1);
    if (len2 > 0)
    {
      return len2;
    }
  }
  return 0;
}
//UNICODE才﹃锣传UNICODE计沮
//锣传UNICODE计沮  
int ConvUniStr2Unicode(LPCSTR szUnicodeString, WCHAR *pWchar)
{
  UINT wChar = -1;
  bool bU=false;
  int l=0;
  int len = strlen(szUnicodeString);
  for (int i=0; i<len; i++)
  {
    if (szUnicodeString[i] == '\\')
    {
      if (bU == true)
      {
        pWchar[l] = '\\';
        l++;
      }
      else
      {
        bU = true;
      }
    }
    else if (szUnicodeString[i] == 'u')
    {
      if (bU == true)
      {
        i++;
        int nR = sscanf(&szUnicodeString[i], "%4X", &wChar);
        if (nR > 0)
        {
          pWchar[l] = wChar;
          l++;
          i+=3;
        }
        else
        {
          pWchar[l] = '\\';
          l++;
          pWchar[l] = szUnicodeString[i];
          l++;
        }
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else if (szUnicodeString[i] == '/')
    {
      if (bU == true)
      {
        pWchar[l] = '/';
        l++;
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else if (szUnicodeString[i] == 'n')
    {
      if (bU == true)
      {
        pWchar[l] = '\r';
        l++;
        pWchar[l] = '\n';
        l++;
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else
    {
      if (bU == true)
      {
        pWchar[l] = '\\';
        l++;
        bU = false;
      }
      pWchar[l] = szUnicodeString[i];
      l++;
    }
  }
  pWchar[l] = 0;
  return l;
}
//**************************************
// unicode才﹃锣ansi才﹃
// 0Θ0ア毖
//**************************************
int ustr_astr( WCHAR *unicodestr, char *ansistr )
{
    int result = 0;
    try
    {
        int needlen = WideCharToMultiByte( CP_ACP, 0, unicodestr, -1, NULL, 0, NULL, NULL );
        if( needlen < 0 )
        {
            return needlen;
        }
 
        result = WideCharToMultiByte( CP_ACP, 0, unicodestr, -1, ansistr, needlen + 1, NULL, NULL );
        if( result < 0 )
        {
            return result;
        }
        return strlen( ansistr );
    }
    catch( ... )
    {

    }
    return result;
}
int ConvUniStrToAnsiStr(const char *ansiUniStr, char *ansiStr)
{
  WCHAR pwbBuffer[4096];
  memset(pwbBuffer, 0, sizeof(pwbBuffer));
  int len1,len2;
  
  len1 = ConvUniStr2Unicode(ansiUniStr, pwbBuffer);
  if (len1 > 0)
  {
    len2 = ustr_astr(pwbBuffer, ansiStr);
    if (len2 > 0)
    {
      return len2;
    }
  }
  return 0;
}
//---------------------------------------------------------------------------
int URLEncode(LPCTSTR pszUrl, LPTSTR pszEncode, int nEncodeLen)
{  
    if( pszUrl == NULL )  
        return 0;  
  
    if( pszEncode == NULL || nEncodeLen == 0)  
        return 0;  
  
    //??宵  
    int nLength = 0;  
    WCHAR* pWString = NULL;  
    TCHAR* pString = NULL;  
  
    //昰?擸?擮擸???UTF-8??  
    nLength = MultiByteToWideChar(CP_ACP, 0, pszUrl, -1, NULL, 0);  
  
    //??Unicode鷇?  
    pWString = new WCHAR[nLength];  
      
    //昰??Unicode  
    MultiByteToWideChar(CP_ACP, 0, pszUrl, -1, pWString, nLength);  
  
    //??UTF-8鷇?  
    nLength = WideCharToMultiByte(CP_UTF8, 0, pWString, -1, NULL, 0, NULL, NULL);  
    pString = new TCHAR[nLength];  
  
    //Unicode?曊UTF-8  
    nLength = WideCharToMultiByte(CP_UTF8, 0, pWString, -1, pString, nLength, NULL, NULL);  
  
    static char hex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    int dd = nEncodeLen / sizeof(TCHAR);
    memset(pszEncode, 0, dd);
  
    for( int i = 0; i < nLength-1; i++ )  
    {  
        unsigned char c = pString[i];  
        if( c > 0x20 && c < 0x7f )    // ?擸?擸?  
        {  
            *pszEncode++ = c;  
        }  
        else if( c == 0x20 )        // 摚斴鷇  
        {  
            *pszEncode++ = '+';  
        }  
        else                        // ???  
        {  
            *pszEncode++ = '%';  
            *pszEncode++ = hex[c / 16];  
            *pszEncode++ = hex[c % 16];  
        }  
    }  
  
    //?甁?擹  
    delete pWString;  
    delete pString;  
  
    return nLength;  
}
BOOL UrlDecode(const char* szSrc, char* pBuf, int cbBufLen)
{
    if(szSrc == NULL || pBuf == NULL || cbBufLen <= 0)
        return FALSE;

    size_t len_ascii = strlen(szSrc);
    if(len_ascii == 0)
    {
        pBuf[0] = 0;
        return TRUE;
    }
    
    char *pUTF8 = (char*)malloc(len_ascii + 1);
    if(pUTF8 == NULL)
        return FALSE;

    int cbDest = 0; //摗
    unsigned char *pSrc = (unsigned char*)szSrc;
    unsigned char *pDest = (unsigned char*)pUTF8;
    while(*pSrc)
    {
        if(*pSrc == '%')
        {
            *pDest = 0;
            //魔晉
            if(pSrc[1] >= 'A' && pSrc[1] <= 'F')
                *pDest += (pSrc[1] - 'A' + 10) * 0x10;
            else if(pSrc[1] >= 'a' && pSrc[1] <= 'f')
                *pDest += (pSrc[1] - 'a' + 10) * 0x10;
            else
                *pDest += (pSrc[1] - '0') * 0x10;

            //敩晉
            if(pSrc[2] >= 'A' && pSrc[2] <= 'F')
                *pDest += (pSrc[2] - 'A' + 10);
            else if(pSrc[2] >= 'a' && pSrc[2] <= 'f')
                *pDest += (pSrc[2] - 'a' + 10);
            else
                *pDest += (pSrc[2] - '0');

            pSrc += 3;
        }
        else if(*pSrc == '+')
        {
            *pDest = ' ';
            ++pSrc;
        }
        else
        {
            *pDest = *pSrc;
            ++pSrc;
        }
        ++pDest;
        ++cbDest;
    }
    //null-terminator
    *pDest = '\0';
    ++cbDest;

    int cchWideChar = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)pUTF8, cbDest, NULL, 0);
    LPWSTR pUnicode = (LPWSTR)malloc(cchWideChar * sizeof(WCHAR));
    if(pUnicode == NULL)
    {
        free(pUTF8);
        return FALSE;
    }
    MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)pUTF8, cbDest, pUnicode, cchWideChar);
    WideCharToMultiByte(CP_ACP, 0, pUnicode, cchWideChar, pBuf, cbBufLen, NULL, NULL);
    free(pUTF8);
    free(pUnicode);
    return TRUE;
}
/*Convert(strA_in,strB_out,CP_UTF8,CP_ACP)//UTF8锣传ANSI
Convert(strA_out,strB_in,CP_ACP,CP_UTF8)//ANSI锣传UTF8
*/
void UTF8_ANSI_Convert(const char* strIn,char* strOut, int sourceCodepage, int targetCodepage)
{
  //int len=lstrlen(strIn);
  int unicodeLen=MultiByteToWideChar(sourceCodepage,0,strIn,-1,NULL,0);
  wchar_t* pUnicode;
  pUnicode=new wchar_t[unicodeLen+1];
  memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t));
  MultiByteToWideChar(sourceCodepage,0,strIn,-1,(LPWSTR)pUnicode,unicodeLen);
  BYTE * pTargetData = NULL;
  int targetLen=WideCharToMultiByte(targetCodepage,0,(LPWSTR)pUnicode,-1,(char *)pTargetData,0,NULL,NULL);
  pTargetData=new BYTE[targetLen+1];
  memset(pTargetData,0,targetLen+1);
  WideCharToMultiByte(targetCodepage,0,(LPWSTR)pUnicode,-1,(char *)pTargetData,targetLen,NULL,NULL);
  lstrcpy(strOut,(char*)pTargetData);
  //delete pUnicode;
  delete pTargetData;
}
//---------------------------------------------------------------------------
