//---------------------------------------------------------------------------
/*
內存放音索引類定義
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "memvoc.h"

//---------------------------------------------------------------------------
CMemvoc::CMemvoc()
{
	Index = new VXML_MEM_PLAY_LIST_STRUCT[MAX_MEM_PLAY_LIST_NUM];
}

CMemvoc::~CMemvoc()
{
	if (Index != NULL)
	{
		delete []Index;
		Index = NULL;
	}
}
//讀內存索引放音文件對照表
void CMemvoc::Read_MemPlayIndex(const char *pszMemVocINIFileName, const char *pszMemVocPath)
{
	UI IndexNo;
	FILE *fp1;
  CStringX ArrString[3];
  CH txtline[128];
	
	//初始化
	for (int i = 0; i < MAX_MEM_PLAY_LIST_NUM; i ++)
	{
		Index[i].state = 0;
		Index[i].IndexNo = 0xFFFF;
		Index[i].FileName.Empty();
	}

  fp1 = fopen(pszMemVocINIFileName, "r");
  if (fp1 != NULL)
  {
    while (!feof(fp1))
    {
      if (fgets(txtline, 128, fp1) > 0)
      {
        if (SplitTxtLine(txtline, 3, ArrString) >= 3)
        {
          IndexNo = atoi(ArrString[0].C_Str());
			    if (IndexNo < MAX_MEM_PLAY_LIST_NUM)
			    {
				    Index[IndexNo].state = 1;
				    Index[IndexNo].IndexNo = IndexNo;
            Index[IndexNo].FileName.Format("%s/%s", pszMemVocPath, ArrString[2].C_Str());
			    }
        }
      }
    }
    fclose(fp1);
  }
  else
  {
  	printf("Read MemIndex File = %s fail\n", pszMemVocINIFileName);
  }
}

