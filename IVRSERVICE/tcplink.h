// TCPServer.h: interface for the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#ifndef __TCPLINK__H__
#define __TCPLINK__H__
//---------------------------------------------------------------------------
//#include "stdafx.h"
//#include "../comm/include/tlinko.h"

struct TServer
{
	char  ServerIP[32];
	short ServerPort;
	unsigned short Serverid;
  bool  IsConnected;
};

class CTCPLink
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnLOGLogin,
                          ONAPPCLOSE OnLOGClose,
                          ONAPPRECEIVEDATA OnLOGReceiveData
                          );
  typedef void (*APPINITS)(unsigned short server_id,
                          unsigned short server_port,
                          ONAPPLOGIN OnIVRLogin,
                          ONAPPCLOSE OnIVRClose,
                          ONAPPRECEIVEDATA OnIVRReceiveData
                          );
public:
  TServer  MasterIVRServer;		//主用IVR服務
  TServer  CFGServer;		//參數配置服務
  TServer  CRMServer;		//媒體網關服務
  TServer  GWServer;		//外部數據網關服務

  short m_nMultiRunMode; //雙機熱備方式
  short m_nHostType; //本機類型：0-主機 1-備機

	void ReleaseDll();
public:
	CTCPLink();
	virtual ~CTCPLink();
  unsigned short IVRServerId; //本端ID標號
  int IVRServerPort;
  
  unsigned short GetMasterIVRServerId()
  {return MasterIVRServer.Serverid;}
  void SetMasterIVRLinked() {MasterIVRServer.IsConnected=true;}
  void SetMasterIVRUnlink() {MasterIVRServer.IsConnected=false;}
  bool IsMasterIVRConnected()
  {return MasterIVRServer.IsConnected;}
  
  unsigned short GetCRMServerId()
  {return CRMServer.Serverid;}
  void SetCRMLinked() {CRMServer.IsConnected=true;}
  void SetCRMUnlink() {CRMServer.IsConnected=false;}
  bool IsCRMConnected()
  {return CRMServer.IsConnected;}
  
  unsigned short GetGWServerId()
  {return GWServer.Serverid;}
  void SetGWLinked() {GWServer.IsConnected=true;}
  void SetGWUnlink() {GWServer.IsConnected=false;}
  bool IsGWConnected()
  {return GWServer.IsConnected;}

  unsigned short GetCFGServerId()
  {return CFGServer.Serverid;}
  void SetCFGLinked() {CFGServer.IsConnected=true;}
  void SetCFGUnlink() {CFGServer.IsConnected=false;}
  bool IsCFGConnected()
  {return CFGServer.IsConnected;}

  char DllFile[64];
  bool LoadDll();
  bool IsOpen; //dll is opend
  bool isIVRInit; //服務器初始化標志
	
  //申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;
  APPINITS        AppInitS;

public:
  int InitIVRServer();
  int SendMessage(US ServerId, US MsgId, int MsgLen, const CH *MsgBuf);
  int SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf);
  int SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len);

  int SendMessage2Master(US MsgId, const CH *MsgBuf);

  int CRMSendMessage(US MsgId, const CH *MsgBuf);
  int CRMSendMessage(US MsgId, const CStringX &MsgBuf);

  int GWSendMessage(US MsgId, const CH *MsgBuf);
  int GWSendMessage(US MsgId, const CStringX &MsgBuf);

  int CFGSendMessage(US MsgId, const CH *MsgBuf);
  int CFGSendMessage(US MsgId, const CStringX &MsgBuf);

  bool ReadIni(char *filename);
  bool ReadWebSetupIni(char *filename);

  int ConnectCRMServer();
  int ConnectGWServer();

  int InitTCPLink();

public:
	//組合消息類型及消息編號
  US CombineID(UC IdType, UC IdNo)
  {
    US m_i1, m_i2;
    m_i1 = ( IdType << 8 ) & 0xFF00;
    m_i2 = IdNo & 0x00FF;
    return (m_i1 | m_i2);
  }
  //分解對象標識及編號
  void DisCombineID(US ID, UC &IdType, UC &IdNo)
  {
    IdType = ( ID >> 8 ) & 0x00FF;
    IdNo = ID & 0x00FF;
  }

};

extern void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len); //need define in main.cpp
extern void OnIVRLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnIVRReceiveData_A(unsigned short remoteid,unsigned short msgtype,
                             const unsigned char *buf,
                             unsigned long len); //need define in main.cpp
extern void OnIVRLogin_A(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose_A(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnCRMReceiveData(unsigned short remoteid,unsigned short msgtype,
                             const unsigned char *buf,
                             unsigned long len); //need define in main.cpp
extern void OnCRMLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnCRMClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnGWReceiveData(unsigned short remoteid,unsigned short msgtype,
                            const unsigned char *buf,
                            unsigned long len); //need define in main.cpp
extern void OnGWLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnGWClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp
//---------------------------------------------------------------------------
#endif // !defined(AFX_TCPSERVER_H__A15B7B15_5824_444E_9B1E_A087BAA5B4CC__INCLUDED_)
