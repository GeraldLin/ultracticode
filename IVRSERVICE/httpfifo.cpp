//---------------------------------------------------------------------------
/*
接收消息先進先出隊列
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "httpfifo.h"

//---------------------------------------------------------------------------
unsigned short CHTTPfifo::Write(const char *MsgBuf, unsigned short MsgLen)         
{
  if (MsgLen == 0)
    return 0;
  m_cCriticalSection.Lock();
	if(IsFull()) //full
  {
    m_cCriticalSection.Unlock();
		return 0;
  }

  if (MsgLen >= 4096)
    pMsgs[Tail].MsgLen = 4095;
  else
    pMsgs[Tail].MsgLen = MsgLen;
  memset(pMsgs[Tail].MsgBuf, 0, 4096);
  memcpy(pMsgs[Tail].MsgBuf, MsgBuf, pMsgs[Tail].MsgLen);
  pMsgs[Tail].MsgBuf[pMsgs[Tail].MsgLen] = 0;

	if(++Tail >= Len)
    Tail=0;

  m_cCriticalSection.Unlock();
	return 1;
}

unsigned short CHTTPfifo::Read(char *MsgBuf, unsigned short &MsgLen)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
    return 0;//Buf Null
  }
	
  MsgLen = pMsgs[Head].MsgLen;
  memcpy(MsgBuf, pMsgs[Head].MsgBuf, MsgLen);
  MsgBuf[MsgLen] = 0;

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}
