//---------------------------------------------------------------------------
/*
接收消息先進先出隊列
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "sqlsfifo.h"

//---------------------------------------------------------------------------
unsigned short CSQLSfifo::Write(const char *SeatNo, const char *WorkerNo, const char *SqlsBuf, const char *MsgBuf, unsigned short MsgType)         
{
  m_cCriticalSection.Lock();
	if(IsFull()) //full
  {
    m_cCriticalSection.Unlock();
		return 0;
  }

  pSqls[Tail].MsgType = MsgType;
  strcpy(pSqls[Tail].SeatNo, SeatNo);
  strcpy(pSqls[Tail].WorkerNo, WorkerNo);
  strcpy(pSqls[Tail].SqlsBuf, SqlsBuf);
  memset(pSqls[Tail].MsgBuf, 0, 1024);
  strncpy(pSqls[Tail].MsgBuf, MsgBuf, 1023);

	if(++Tail >= Len)
    Tail=0;

  m_cCriticalSection.Unlock();
	return 1;
}

unsigned short CSQLSfifo::Read(char *SeatNo, char *WorkerNo, char *SqlsBuf, char *MsgBuf, unsigned short &MsgType)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
    return 0;//Buf Null
  }
	
  MsgType = pSqls[Head].MsgType;
  strcpy(SeatNo, pSqls[Head].SeatNo);
  strcpy(WorkerNo, pSqls[Head].WorkerNo);
  strcpy(SqlsBuf, pSqls[Head].SqlsBuf);
  strcpy(MsgBuf, pSqls[Head].MsgBuf);

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}
