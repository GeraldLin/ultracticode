//---------------------------------------------------------------------------
#ifndef __BOXCHN_H_
#define __BOXCHN_H_
//---------------------------------------------------------------------------
#include "boxag.h"
//---------------------------------------------------------------------------
#define MAXCHGROUPNUM		32		//最大通道組數
#define MAXTRANCHNNUM		5		//最大轉接通道數

#define MAXCALLCOUNTONAUTHFAIL	100	//授權失敗后運行最大呼叫次數

//路由中繼
typedef struct
{
  int LastIndex; //最后占用的索引
  int TrkTotalNum; //該路由的中繼線總數
  int OldTrkBusyNum; //重新計數前該路由占用中繼線總數
  int TrkBusyNum; //該路由占用中繼線總數
  UC E1TrkSelMode; //數字中繼優先選擇模式：0-按順序循環選擇 1-先選奇數電路，全忙時再選偶數電路 2-先選偶數電路，全忙時再選奇數電路 3-按順序固定選擇,前面條中繼忙后才選下一條中繼 4-按固定逆序選空閑的電路 5-選擇空閑時間最長電路
  US TrkNoList[MAX_ROUTE_TRUNK_NUM]; //中繼序號
  CH IPPreCode[20]; //呼出長途時加的IP字冠
  UC VOCTrunkId; //是否為語音中繼路由
  UC SIPRegMode; //SIP注冊方式：0-點對點，1-注冊
  CH CallerSIPProxyIP[32]; //外呼主叫送的SIP服務器的IP
  CH CalledSIPProxyIP[32]; //外呼被叫送的SIP服務器的IP
  US OverflowRouteNo; //2015-11-25 溢出的路由號 0-表示無

}VXML_ROUTERTRK_STRUCT;

//DTMF按鍵規則結構定義
typedef struct
{
	UC state;		//數據狀態 0: 無 1：有
	UC ruleid; //收碼規則編號(整數常量)
  UC breakid; //按鍵中斷放音標志(true|false)，缺省為true
  UC transtype; //轉發方式,收全后發送，收一位發一位(true|false)，缺省為false
  UC minlength; //收碼最短長度(整數常量、變量)
  UC maxlength; //收碼最大長度(整數常量、變量)
  UC timeout; //收碼間隔時間秒，缺省為5(整數常量、變量)
  UC termid; //收碼結果是否包含結束按鍵(true|false)，缺省為false
  UC firstdigitdelay; //第1位按鍵延時時長秒
  UC specpredigitsmaxlength[10]; //特殊字冠接收按鍵的最大長度，針對不是等位收碼的情況，如4開頭的是4位，其他開頭的是6位 2014-01-22
  CStringX specpredigits[10]; //特殊字冠
  CStringX digits; //允許接收按鍵(0123456789*#)(字符串常量、變量)
  CStringX termdigits; //收碼結束按鍵(0123456789*#)，缺省為空(字符串常量、變量)
  CStringX onebitdigits; //只收1位按鍵的例外情況,如總機提示按鍵:請撥3位分機號,查號請撥0,技術不請撥1
  CStringX pbxpredigits; //PBX分機號碼字冠，如果onebitdigits不在pbxpredigits里，則可以不等待延時世界返回收碼結果 2015-04-25
  CStringX ValidDTMF; //實際有效菜單按鍵 2015-06-29

}VXML_DTMFRULE_STRUCT;

//放音規則結構定義
typedef struct
{
	UC state;		//數據狀態 0: 無 1：有
  UC ruleid; //放音規則編號（整數常量）
  UC format; //文件格式, -1（使用硬件通道缺省的放音編碼格式），6（A-率），7（m-率），17（IMA ADPCM），49（GSM），85（MP3），23（VOX），-2（PCM16）。
  UC repeat; //重復放音次數，缺省為1(整數常量)
	US duration; //放音持續時長(秒) 為0時按repeat次數放音 >0按持續時長放音
  UC broadcast; //廣播放音標志(true|false)，缺省為false
  UC volume; //音量，缺省為0(正常音量) (可以是常量、變量)
  UC speech; //語音設置(english|chinese|localism) 缺省為chn(普通話)
              //english						1 //英語
              //chinese						2 //普通話
              //localism					3 //本地話
              //guangdong					4 //廣東話
              //minnan						5 //閩南話
              //french						6 //法語
              //portuguese				7 //葡萄牙語
              //arabic						8 //阿拉伯語
  UC voice; //聲音選擇(male|female|child) 缺省為female
  UC pause; //重復放音時停頓時間秒，缺省為0(可以是常量、變量)
  UC error; //放音出錯處理標志(true|false),true釋放該呼叫，false返回錯誤消息，缺省true
	
}VXML_PLAYRULE_STRUCT;

//ASR規則結構定義
typedef struct
{
	UC state;		//數據狀態 0: 無 1：有
  UC ruleid;		//ASR規則編號（整數常量）
	CStringX grammar;	//ASR語法規則文件

}VXML_ASRRULE_STRUCT;

//放音索引緩沖數據結構定義
typedef struct
{
	UC PlayType; //放音類型：0-沒有數據 1-文件放音 2-內存索引放音 3-TTS文件 4-TTS字符串 5-信號音
  UC PlayFormat; //語音格式：0-ALAW PCM 1-WAVE 2-CIRRUS ADPCM 3-G.723.1 4-Dialogic 6K 5-Dialogic 8K
  US PlayIndex; //或內存索引號或系統信號音類型(1-撥號音 2-回鈴音 3-忙音)(0xFFFF表示無索引號)
	CStringX PlayFile; //放音的文件名或TTS文件或TTS字符串
  US repeat; //重復放音次數，為0表示按統一放音規則次數，>0則播放設置的次數 2015-06-29 針對有些放音，如：第1句公司問候語放一遍，主菜單放3遍

}VXML_PLAYINDEX_STRUCT;

//放音緩沖數據結構定義
typedef struct
{
  UC state; //有效狀態：0 無；1 有

	UL SessionNo; //發起放音指令的會話序列號
	UL CmdAddr; //發起放音指令的放音的指令行地址
  UC lgChnType; //發起放音指令的業務通道類型: 1-中繼 2-坐席 3-會議 4-VOIP 5-錄音
	US lgChnNo; //發起放音指令的業務通道編號
  US OnMsgId; //返回放音結果的消息號
  CStringX MsgName; //[MAX_CMD_NAME_LEN]; //返回放音結果的消息名稱

	UC CanPlayId; //允許開始放音標志 0-不允許 1-允許
	UC PlayState; //放音狀態: 0-未放音 1-準備放音 2-正在放音 3-當次放音結束	4-放音正常結束 
	
	UC PlayTypeId; //放音標志 0-無 1-單文件放音 2-組合放音 3-信號音
	long StartPos; //單文件放音起始偏移量，=0表示從頭播放文件
	long Length;  //播放語音數據的長度(字節數), -1表示播放全部
    
	US DataBufNum; //放音索引個數
  VXML_PLAYINDEX_STRUCT PlayIndex[MAX_PLAY_INDEX_NUM];
	
  UC WaitTTSResult; //已提交TTS消息，正在等待TTS結果 1-是 0-否
  UC CurBufId; //當前正在播放的語音流緩沖序號0表示沒有播放或已播放完，1表示正在播放TTSBuf[0]，2表示正在播放TTSBuf[1]
  US StreamIndex; //返回的語音流索引號
  UC TTSResult[2]; //收到TTS合成結果：0-未收到結果 1-收到結果，但還沒有完成 2-收到結果且已完成
  UL StreamLen[2]; //實際的語音流長度
  UC TTSBuf[2][MAX_TTS_STREAM_LEN]; //語音流雙緩沖區

	US PlayPoint; //放音索引指針
	UC PlayedTime; //已循環放音次數
	UL PlayedLen; //已放音時長(秒)
  UC PlayEndReleaseId; //放音結束后釋放通道標志

}VXML_PLAY_DATA_BUF_STRUCT;

//錄音緩沖數據結構定義
typedef struct
{
  UC state; //有效狀態：0 無；1 有
  UC WaitRecdId; //等待連通語音通道后錄音標志（針對交換機版本） 0-不需要 1-需要

	UL SessionNo; //發起錄音指令的會話序列號
	UL CmdAddr; //發起錄音指令的當前指令行地址
  UC lgChnType; //發起錄音指令的業務通道類型: 1-中繼 2-坐席 3-會議 4-VOIP 5-錄音
	US lgChnNo; //發起錄音指令的業務通道編號

	UC CanRecId; //允許開始錄音標志 0-不允許 1-允許
	UC RecState; //錄音狀態: 0-未錄音 1-正在放BEEP音 2-已啟動錄音 3-正在錄音 4-按鍵結束放音 5-錄音正常結束 

	UC RecTypeId; //錄音標志 0-無 1-文件錄音 2-內存錄音
	UC BeepId; //錄音前嘀聲提示標志
	UC Format; //錄音文件格式:0-ALAW PCM 1-WAVE 2-CIRRUS ADPCM 3-G.723.1 4-Dialogic 6K 5-Dialogic 8K
	CStringX TermChar; //[MAX_DTMFRULE_LEN]; //停止錄音按鍵
	CStringX RecordFile; //錄音文件名
	long StartPos; //當文件存在時,錄音起始偏移量，=0表示從頭錄音 -1表示從文件尾開始錄音
	UL MaxTime;  //最大錄音時長(秒)
	
	UL RecedLen; //已錄音時長(秒)
	UL RecedByte; //已錄音大小(字節)

}VXML_RECORD_DATA_BUF_STRUCT;

//---------------------業務通道類型映射表結構----------------------------------
//業務中繼數據
typedef struct
{
	US state; //數據狀態 0: 無 1：有
	US ChnNo; //對應的通道號

}VXML_TRUNK_STRUCT;

//業務坐席數據
typedef struct
{
	US state; //數據狀態 0: 無 1：有
	US ChnNo; //對應的通道號

	US SeatType; //坐席類型(0表示任意類型)
	CStringX SeatNo; //坐席號
	CStringX Psw; //坐席密碼

}VXML_SEAT_STRUCT;

//業務會議通道數據
typedef struct
{
  US state; //數據狀態 0: 無 1：有
  US ChnNo; //對應的通道號
  
}VXML_CFC_STRUCT;

//業務VOIP通道數據
typedef struct
{
  US state; //數據狀態 0: 無 1：有
  US ChnNo; //對應的通道號
  
}VXML_VOIP_STRUCT;

//業務錄音通道數據
typedef struct
{
	US state; //數據狀態 0: 無 1：有
	US ChnNo; //對應的通道號

}VXML_REC_STRUCT;

//業務傳真通道數據
typedef struct
{
  US state; //數據狀態 0: 無 1：有
  US ChnNo; //對應的通道號
  
}VXML_FAX_STRUCT;

//業務IVR通道數據
typedef struct
{
  US state; //數據狀態 0: 無 1：有
  US ChnNo; //對應的通道號
  CVopChn *cVopChn; //對應的VOP語音通道
  
}VXML_IVR_STRUCT;
//---------------------業務通道類型映射表結構----------------------------------
//邏輯通道號與通道序號對應關系
class CCIdx
{
public:
  US TotalChnNum; //該類型的通道總數
  US StartIndex;
  US *pnChns; //對應的通道序號指針

public:
	//初始化 chnnum-表示該類型的通道總數
  CCIdx(int chnnum, int startindex=0)
	{
		if(chnnum > 0)
		{
			TotalChnNum = chnnum;
      StartIndex = startindex;
      pnChns = new US[chnnum];
		}
    else
    {
      pnChns = NULL;
      TotalChnNum = 0;
      StartIndex = 0;
    }
	} 
	virtual ~CCIdx()
  {
    if (pnChns != NULL)
    {
      delete []pnChns;
      pnChns = NULL;
    }
  }
  
  //設置通道序號
  void SetnChn(int chindex, int nchn)
  {
    if (pnChns == NULL || chindex < 0 || chindex >= TotalChnNum)
      return;
    pnChns[chindex] = nchn;
  }
  //取通道序號
  int GetnChn(int chindex) 
  {
    if (pnChns == NULL || chindex < 0 || chindex >= TotalChnNum)
      return 0xFFFF;
    return pnChns[chindex];
  }
};

class CIdelTrkChnQueue;
//-----------------------------------------------------------------------------
//通道數據結構定義
class CChn
{
public:
	UC state; //數據狀態 0: 無 1：有
	UL timer; //定時器
	UL PlayPauseTimer; //放音間隔定時器
	UL PlayedTimer; //已放音時長定時器
	UL DtmfPauseTimer; //收碼間隔定時器
	UL RecTimer; //已錄音時長定時器
	UL CalloutTimer; //呼出定時器
  UL WaitTTSTimer; //等待TTS合成定時器
  UL WaitDialTimer; //等待內線撥號定時器

  US CTILinkClientId;
  CH DeviceID[MAX_TELECODE_LEN]; //設備編號
  UL ConnID; //關聯的交換機呼叫ID

  UC ChStyle; //通道信令類型
  US ChStyleIndex; //按信令排列的通道序號

  UC ChType; //按硬件端口分類定義類型: 0-PORT_SANHUI_TYPE(三匯板卡端口類型); 1-PORT_AVAYA_IPO_TYPE; 2-PORT_AVAYA_SXXXX_TYPE;3-PORT_ALCATEL_OXE_TYPE; 10-PORT_NOCTI_PBX_TYPE(無CTI接口的交換機端口類型)
	US ChIndex; //硬件邏輯通道號

  UC lgChnType; //業務通道類型: 1-中繼 2-坐席 3-IVR 4-錄音 5-傳真 6-會議 7-VOIP
	US lgChnNo; //業務通道編號
  
  US nAG; //對應的坐席編號，當該通道為坐席電話時
  US RouteNo; //對應的路由號
  US CallOutRouteNo; //呼出的路由號
  US OverflowRouteNo; //2015-11-25 溢出的路由號 0-表示無

	UL SessionNo; //會話序列號
	UL CmdAddr; //當前指令行地址
  UL FaxCmdAddr; //收發傳真指令行地址
  UL CheckToneCmdAddr; //收發傳真指令行地址

	UC hwState; //硬件狀態:	1: 未初始化	0: 可用	2: 閉塞	3: 關閉
	UC lnState; //線路狀態: 0-空閑 1-占用 2-收到線路釋放 3-發送釋放后等待回應
	UC ssState; //信令狀態: 參考宏定義
	UC svState; //業務狀態: 參考宏定義
  UC swState; //交換機端口狀態：0-空閑 1-摘機 2-振鈴 3-通話 4-忙音
	
	US CfcNo; //參加的會議室號
	UC JoinType; //參加方式: 0-未加入 1-主持人 2-發言 3-旁聽 4-對會議放音 5-強插加入會議 6-三方通話加入會議
  time_t JoinTime; //加入會議時間
	
	UC CallInOut; //呼叫方向：1-呼入 2-呼出

  UC CallInId; //呼入標志: 0-空閑 1-呼入振鈴

	UC CalloutId; //呼出占用標志 0-無呼出 1-正在呼出 2-取消呼出
	US nQue; //本通道呼出時的呼出隊列緩沖序號: 0xFFFF表示無呼出
  US nOut; //本通道會話發起呼出的呼出序號
  US nAcd; //占用的ACD緩沖序號
  UL DialSessionNo; //撥號器呼出序列號

  CH CustPhone[MAX_TELECODE_LEN]; //關聯的客戶電話號碼
  CH CallerNo[MAX_TELECODE_LEN]; //主叫號碼
  CH CalledNo[MAX_TELECODE_LEN]; //被叫號碼
  CStringX OrgCallerNo; //原被叫號碼
  CStringX OrgCalledNo; //原被叫號碼
  UC CalledPoint; //已向應用層發送號碼個數

  time_t CallTime; //呼叫時間
  time_t AnsTime; //應答時間
  time_t RelTime; //釋放時間

	US LinkChn[MAX_LINK_CHN_NUM]; //交換的通道號B 0xFFFF表示無交換通道
	UC LinkType[MAX_LINK_CHN_NUM]; //交換的連接方式：0-未交換 1-雙向交換(A<->B) 2-監聽單向交換(A<-B) 3-被監聽單向交換(A->B) 4-強插通話 5-三方通話 6-彩話主通道 7-彩話副通道 8-發送傳真 9-接收傳真 10-變聲通話去話不變聲 11-變聲通話去話變聲
									/*
									語音卡版本交換的連接: LinkChn[0]固定存儲雙向通話、監聽對方、強插通話、三方通話時對方的通道號，LinkChn[1]固定存儲監聽對方、強插通話、三方通話另一通道號，從LinkChn[2]開始作為被監聽對方的通道號
									*/

	UC curPlayState; //當前放音狀態
					/*
					0: 未放音
					1: 放音正在進行。
					2: 放音正常結束。
					3: 放音操作因檢測到DTMF 按鍵字符而結束。
					4: 放音操作因檢測到bargein而結束。
					5: 放音操作因檢測到對端掛機而停止。
					6: 放音操作被應用程序停止
					7: 放音操作被暫停
					8: 放音操作因下總線而停止（只針對數字卡）
					9: 放音操作因網絡中斷而停止
          10: 放音分配資源失敗
					*/
	UC curRecState; //當前錄音狀態
					/*
					0: 未錄音
					1: 錄音正在進行 
					2: 錄音操作被應用程序停止
					3: 錄音操作因檢測到DTMF 按鍵字符而結束
					4: 錄音操作因檢測到對端掛機而結束
					5: 錄音操作自然結束（到達指定的字節長度或時間）
					6: 文件錄音操作被暫停
					7: 錄音操作因寫文件失敗而停止
					8: 放音操作因網絡中斷而停止
          10: 放音分配資源失敗
					*/

	int Volume; //放音音量
	UC RecMixerFlag; //錄音混音器開關
	UC PlayBusFlag; //放音上總線標志
	UC RecAGCFlag; //錄音自動增益控制開關
	UC CanPlayOrRecId; //可以開始放音或錄音標志

	VXML_PLAY_DATA_BUF_STRUCT curPlayData;
	VXML_RECORD_DATA_BUF_STRUCT curRecData;

	UC RecvDtmfId; //是否收到第1位按鍵標志 0-未按鍵 1-按鍵
  UC RecvDialDtmfId; //內線撥號按鍵完成標志
  CH RecvDialDtmf[MAX_DTMFBUF_LEN]; //接收的內線撥號的按鍵 2010-06-03
  CH curRecvDtmf[MAX_DTMFBUF_LEN]; //當前規則原始接收的DTMF按鍵
	CH RecvDtmfBuf[MAX_DTMFBUF_LEN]; //有效的DTMF緩沖區
  CH CallInDtmfBuf[MAX_DTMFBUF_LEN]; //呼入時的DTMF緩沖區 2015-07-04
	//CStringX RecvAsrBuf; //[MAX_ASRBUF_LEN]; //ASR緩沖區

  UC curSpeech; //當前選擇的語言
  US curPlayRuleId; //當前放音規則
	US curDtmfRuleId; //當前收碼DTMF規則 
  UC PbxFirstDigitId; //自動總機菜單收到第1位例外按鍵標志
	//US curAsrRuleId; //當前語音識別規則

	VXML_DTMFRULE_STRUCT DtmfRule[MAX_DTMFRULE_BUF]; //流程發送來的收碼規則 
	VXML_PLAYRULE_STRUCT PlayRule[MAX_PLAYRULE_BUF+1]; //流程發送來的流程放音規則
	//VXML_ASRRULE_STRUCT AsrRule[MAX_ASRRULE_BUF]; ///流程發送來的流程ASR規則

  UC RingCount; //模擬外線來電振鈴累計次數
  CStringX FlwState; //業務流程傳遞過來的狀態信息
  
  US SwtLinkChn[MAX_LINK_CHN_NUM]; //交換的通道號B 0xFFFF表示無交換通道
  UC SwtLinkType[MAX_LINK_CHN_NUM]; //
  /* 
  交換機版本交換的連接方式：0-未占用 1-正在呼叫該端口 2-呼叫該端口失敗 3-已成功連通該端口 4-該端口處于轉接其他端口狀態
  */
  
  CVopChn *cVopChn; //綁定的資源通道類
  UC SeizeVopChnState; //綁定的資源通道狀態標志: 0-未綁定 1-已經綁定
  
  short nSwtPortID; //主連接的交換機的邏輯端口號
  short nCallType; //呼叫類型(0-普通呼叫 1-直接呼入 2-直接呼出 3-轉移呼入 4-代接呼入 5-強插監聽呼出)
  short nAnswerId; //已應答標志(1-是 0-否)

  CStringX CallData; //轉接呼叫傳遞的附加參數
  
  CH CustomNo[MAX_CHAR64_LEN]; //客戶編號
  CH CdrSerialNo[MAX_CHAR64_LEN]; //話單編號
  CH RecordFileName[MAX_CHAR128_LEN]; //錄音文件名
  UC DBUpdateCallCDR_RingTimeId;

  time_t IVRStartTime; //進入IVR時間
  UC TranIVRId; //交換機版本轉接標志 0-不是轉IVR，而是坐席轉接電話 1-轉接的源頭通道標志 2-轉接的目的通道標志 3-主通道IVR通道通過流程transfer轉接呼出標志 4-主通道IVR通道通過流程callout轉接呼出 5-自動流程callout直接呼出 6-自動流程callseat直接呼出 7-轉接的目的通道進行密碼驗證 9-板卡版本拍叉轉接
  UL TranSessionNo; //會話序列號
  UL TranCmdAddr; //該會話分配坐席的指令行地址
  US TranAG; //通過transfer指令轉接的目的坐席
  US TranWaitTime; //等待轉接時長
  
  US HoldingnChn; //關聯的主通話通道,即最初通話的2方
  UC TranStatus; //轉接身份標志:0-未轉接,1-被轉接方,2-目的轉接方,3-轉接方(正在轉接),4-轉接方(轉接振鈴),5-轉接方(轉接應答)
  US TranControlnChn; //TranStatus=0或3時表示為被轉接方的通道 TranStatus=1時表示為控制轉接的通道 TranStatus=2時表示為控制轉接的通道
  US TranDestionnChn; //轉接的目的通道
  UC TranOutForAgent; //坐席轉接外線后，外線作為坐席服務標志

  UL SerialNo;

  UC WritedCallCDR_RelTimeId; //已經寫了通話釋放時間標志
  UC WritedCallCDR_WaitTimeId; //已經寫了通話釋放時間標志
  
  UC IncIVRInTranAGCountId; //已經累加IVR轉坐席次數標志
  UC IncIVRInTranAGAnsCountId; //已經累加IVR轉坐席應答次數標志
  UC IncIVRInTranAGAbandonCountId; //已經累加IVR轉坐席失敗或放棄次數標志
  time_t IVRInTranAGTime; //IVR開始轉接坐席的時間點
  
  UC VOC_TrunkId; //語音中繼標志
  UC VOC_TrunkId_ex; //臨時作為語音中繼標志
  UC VOC_TrunkRecvCallEvent; //語音中繼已收到呼入事件標志

  CH ManDialOutId[32];
  CH ManDialOutListId[64];
  CH TranCdrSerialNo[MAX_CHAR64_LEN]; //轉接的話單編號
  CH TranRecordFileName[MAX_CHAR128_LEN]; //轉接的錄音文件名

  CH CRMCallId[64]; //crm外呼關聯呼叫標示號
  
  UC ExtRecordReqFlag; //請求第3放錄音系統錄音id標志 0-不需要請求，1-請求狀態，2-已請求成功  //2015-05-30

  CH TranUUI[MAX_CHAR128_LEN]; //轉接的UUI數據 2015-10-24
  CH CallUCID[MAX_CHAR128_LEN]; //呼叫唯一標識代碼 2015-10-24
  CH TrunkGroupNumber[32]; //2016-11-22增加保存AVAYA中繼群組

  US VocTrknAG; //2015-11-25 針對語音中繼的修改增加的參數：語音中繼呼出綁定的座席

  //2015-12-07記錄已分配過的坐席邏輯編號
  short AcdednAGList[MAX_AG_NUM]; //已經分配過的座席序號（初始值置為-1，防止分配后該座席未應答后反復分配）
  short GetAgentList[MAX_AG_NUM]; //找到符合ACD規則的坐席序號列表
  short GetAgentAcdPoint;
  
  UL SetForwardingSessionNo; //設置呼叫轉移會話序列號 2016-07-19
  UL SetForwardingCmdAddr; //設置呼叫轉移當前指令行地址 2016-07-19

  US HangonReason; //2016-08-11增加掛機原因

  int SWTACDTimeLen; //2017-01-08增加交換機硬排隊在轉到分機前已等待的時長

public:
  void GenerateCdrSerialNo();
  void GenerateRecordFileName();
  void GenerateSerialNo();
};

//---------------------------------------------------------------------------
class CBoxchn
{
private:
	bool isInit;
	US lastnChn;
	US lastnTrk;
	US lastnCsc;
	US lastnRec;
  
public:
	CBoxchn();
	virtual ~CBoxchn();

  void ReleaseAll();
	
	UC isDTUseTs32; //是否列出數字中繼的時隙0和16
	UC MaxRouteNum; //最大路由數
	VXML_ROUTERTRK_STRUCT Routes[MAX_ROUTE_NUM]; //路由號對應的通道號
  UC E1TrkSelMode; //數字中繼優先選擇模式：0-按順序選擇 1-先選奇數電路，全忙時再選偶數電路 2-先選偶數電路，全忙時再選奇數電路
  
	CChn		          *pChn_str; //通道數據結構表
	VXML_TRUNK_STRUCT	*pTrk_str; //中繼數據結構表
	VXML_SEAT_STRUCT	*pCsc_str; //坐席數據結構表
  VXML_CFC_STRUCT	  *pCfc_str; //會議數據結構表
  VXML_VOIP_STRUCT	*pVoip_str; //VOIP數據結構表
	VXML_REC_STRUCT		*pRec_str; //錄音數據結構表
  VXML_FAX_STRUCT		*pFax_str; //FAX數據結構表
  VXML_IVR_STRUCT		*pIvr_str; //IVR數據結構表
  
  CCIdx *pIdx[MAXCHGROUPNUM]; //通道邏輯類型與通道序號對應關系指針
  CIdelTrkChnQueue *pIdelTrkChnQueue;

public:
  int MaxChnNum; //最大總通道數
  int MaxTrkNum; //最大中繼總數
  int MaxCscNum; //最大坐席通道總數
  int MaxRecNum; //最大錄音通道總數
  int MaxIvrNum; //最大IVR通道總數
  int MaxFaxNum; //最大FAX通道總數
  int MaxFxoNum; //最大模擬外線通道總數

  int TotalTrkNum; //實際中繼通道總數
  int TotalSeatNum; //實際坐席通道總數
  int TotalIvrNum; //實際IVR通道總數
  int TotalRecNum;

  int TrkChnBusyCount; //中繼占用數
  int TrkChnIdelCount; //中繼空閑數
  int TrkChnBlockCount; //中繼阻塞數
  int TrkChnInCount; //中繼呼入數
  int TrkChnOutCount; //中繼呼出數

  int IvrChnBusyCount; //IVR占用數
  int IvrChnIdelCount; //IVR空閑數
  int IvrChnBlockCount; //IVR阻塞數

  int Init(int TotalChn );
  bool isInited()
  {
    return isInit;
  }
  void SetChData(int nChn, int chstyle, int chstyleindex, int chtype, int chindex, int lgchtype, int lgchindex);
  void SetChDeviceID(int nChn, const char *deviceid);
  
  //初始化通道邏輯類型與通道序號對應關系指針 chtype-邏輯通道類型 chnnum-該類型的通道總數
  int InitChnIdx(int chtype, int chnnum);
  //設置硬件邏輯通道類型數據
  void SetChtypeData(int chtype, int chindex, int nchn);
  //設置中繼通道
  void SetTrunkCh(int lgchindex, int nchn);
  //設置坐席通道
  void SetSeatCh(int lgchindex, int nchn);
  void SetSeatCh(int lgchindex, int nchn, const char *seatno, int seattype);
  //設置錄音通道
  void SetRecCh(int lgchindex, int nchn);
  void SetIvrCh(int lgchindex, int nchn, CVopChn *pVopChn);
  void SetFaxCh(int lgchindex, int nchn);
  
	void InitTrunk(int nTrk);
	void InitSeat(int nCsc);
  void InitRec(int nRec);
  void InitIvr(int nIvr);
  void InitFax(int nFax);

  void ClearDtmfRule(int nChn, int DtmfRuleId);
  void ClearPlayRule(int nChn, int PlayRuleId);
  void ClearPlayData(int nChn);
  void ClearAllDtmfPlayRule(int nChn);
  void ClearRecData(int nChn);

  int SetTrkCh(int maxchnum, int ChType);

	void InitChn(int nChn);
	void ResetChn(int nChn);
  void ResetAllChn();

  void TimerAdd(int nTCount);

  int GetMaxChnNum()
  {return MaxChnNum;}
  bool isnChnAvail(int nChn)
  {
    return (nChn >= 0) && (nChn < MaxChnNum) ? true : false;
  }
  bool isnTrkAvail(int nTrk)
  {
    return nTrk < MaxTrkNum ? true : false;
  }
  bool isnCscAvail(int nCsc)
  {
    return nCsc < MaxCscNum ? true : false;
  }
  bool isnRecAvail(int nRec)
  {
    return nRec < MaxRecNum ? true : false;
  }
  //根據業務通道類型、通道號取實際通道號
  int Get_nChn_By_LgChn(int chntype, int chnno);
  int Get_ChnNo_By_LgChn(int chntype, int chnno, int &nChn);
  //根據語音卡邏輯通道類型、通道號取實際通道號
  int Get_ChnNo_By_CardChn(int chtype, int chindex);
  int Get_ChnNo_By_ChStyle(int chstyle, int chstyleindex);
  //取通道類型、通道號
  bool Get_lgChn(int nChn, int &lgchntype, int &lgchnno);
  //讀內線端口號碼配置
  void Read_SeatNoIniFile(char *filename);
  //讀路由數據
  void Read_Router(char *filename);
  int SetOneRoute(int routeno, int selmode, const char *pszChnList, const char *pszIPPreCode);
  //根據坐席號取通道號
  int Get_IdelChnNo_By_SeatNo(const char *seatno);
  int Get_ChnNo_By_DeviceID(const char *deviceid);
  //取空閑的IVR中繼通道
  int Get_IdelIVRChnNo();
  //根據路由號取空閑的中繼通道
  int Get_IdelChnNo_By_Route(int Route, UC &ChType, int &ChIndex);
  //根據通道號取業務通道類型及通道號
  int GetLgChBynChn(int nChn, int &lgchtype, int &lgchnno);
  int Get_IdelChnNo_By_LgChn(int lgchntype, int lgchnno, UC &ChType, int &ChIndex);
  int isTheChIdel(int nChn, bool bACDToAgentWhenChBlock);
  //放音緩沖是否溢出
  bool isPlayBufOver(int nChn)
  {
    return pChn_str[nChn].curPlayData.DataBufNum < MAX_PLAY_INDEX_NUM ? false : true;
  }
  void GenerateCdrSerialNo(int nChn);
  void GenerateRecordFileName(int nChn);

  void ClearnSwtPortID(int nPortID);

  bool IsAllTrkChnAct();
};

class CIdelTrkChn
{
public:
  short state;
  
  short TrkNo; //中繼通道序號
  
  unsigned short Prior; //對應的前一序號, 0表示第1個
  unsigned short Next; //對應的下一序號, 0xFFFF表示結束
  
public:
  void InitData()
  {
    state = 0;
    TrkNo = -1;
    
    Prior = 0xFFFF;
    Next = 0xFFFF;
  }
  CIdelTrkChn()
  {
    InitData();
  }
  virtual ~CIdelTrkChn()
  {
  }
};
//空閑中繼記錄隊列
class CIdelTrkChnQueue
{
private:
  bool isInit;
  
public:
  short MaxQueNum;
  CIdelTrkChn *m_pIdelTrkNo;
  
public:
  CIdelTrkChnQueue()
  {
    isInit = false;
    MaxQueNum = 0;
    m_pIdelTrkNo = NULL;
  }
  virtual ~CIdelTrkChnQueue()
  {
    if (m_pIdelTrkNo != NULL)
    {
      delete []m_pIdelTrkNo;
      m_pIdelTrkNo = NULL;
    }
  }
  void InitQueueData(int nQue)
  {
    if (isInit == false || nQue >= MaxQueNum) return;
    m_pIdelTrkNo[nQue].InitData();
  }
  bool InitQueue(int maxquenum)
  {
    if (maxquenum <= 0) return false;
    if (isInit == true) return false;
    
    m_pIdelTrkNo = new CIdelTrkChn[maxquenum];
    if (m_pIdelTrkNo == NULL) return false;
    
    isInit = true;
    MaxQueNum = maxquenum;
    for (int i = 0; i < MaxQueNum; i ++)
    {
      InitQueueData(i);
    }
    m_pIdelTrkNo[0].state = 1;
    return true;
  }
  int GetIdelnOue()
  {
    static int nQue=-1;
    
    for (int i=0; i < MaxQueNum; i ++)
    {
      nQue = (nQue+1)%MaxQueNum;
      if (m_pIdelTrkNo[nQue].state == 0)
      {
        return nQue;
      }
    }
    return -1;
  }
  
  bool GetIdelnOue(int &nque)
  {
    static int nQue=-1;
    
    for (int i=0; i < MaxQueNum; i ++)
    {
      nQue = (nQue+1)%MaxQueNum;
      if (m_pIdelTrkNo[nQue].state == 0)
      {
        nque = nQue;
        return true;
      }
    }
    return false;
  }
  
  bool isnQueAvail(int nQue)
  {
    if (nQue < MaxQueNum && nQue >= 0)
      return true;
    else
      return false;
  }
  void JoinInQueueHead(int nQue)
  {
    unsigned short nFirstQry=0, Next;
    
    Next = m_pIdelTrkNo[nFirstQry].Next;
    if (Next >= MaxQueNum)
    {
      //隊列中沒有記錄
      m_pIdelTrkNo[nFirstQry].Prior = nQue;
      m_pIdelTrkNo[nFirstQry].Next = nQue;
      m_pIdelTrkNo[nQue].Prior = 0;
      m_pIdelTrkNo[nQue].Next = 0xFFFF;
    }
    else
    {
      //隊列中有記錄
      m_pIdelTrkNo[nFirstQry].Next = nQue;
      m_pIdelTrkNo[Next].Prior = nQue;
      m_pIdelTrkNo[nQue].Prior = 0;
      m_pIdelTrkNo[nQue].Next = Next;
    }
  }
  
  void JoinInQueueTail(int nQue)
  {
    unsigned short nFirstQry=0, Prior=0;
    
    Prior = m_pIdelTrkNo[nFirstQry].Prior;
    if (Prior >= MaxQueNum)
    {
      //隊列中沒有記錄
      m_pIdelTrkNo[nFirstQry].Prior = nQue;
      m_pIdelTrkNo[nFirstQry].Next = nQue;
      m_pIdelTrkNo[nQue].Prior = 0;
      m_pIdelTrkNo[nQue].Next = 0xFFFF;
    }
    else
    {
      //隊列中有記錄
      m_pIdelTrkNo[nFirstQry].Prior = nQue;
      m_pIdelTrkNo[Prior].Next = nQue;
      m_pIdelTrkNo[nQue].Prior = Prior;
      m_pIdelTrkNo[nQue].Next = 0xFFFF;
    }
  }
  
  void DelFromQueue(int nQue)
  {
    unsigned short Prior, Next;
    
    Prior = m_pIdelTrkNo[nQue].Prior;
    Next = m_pIdelTrkNo[nQue].Next;
    
    m_pIdelTrkNo[Prior].Next = Next;
    if ( Next < MaxQueNum ) 
      m_pIdelTrkNo[Next].Prior = Prior;
    else
      m_pIdelTrkNo[0].Prior = Prior;
    
    InitQueueData(nQue);
  }
  bool TrkNoJoinInQueueTail(int nTrkNo)
  {
    int nQue;
    if (GetIdelnOue(nQue))
    {
      m_pIdelTrkNo[nQue].state = 1;
      m_pIdelTrkNo[nQue].TrkNo = nTrkNo;
      JoinInQueueTail(nQue);
    }
    return false;
  }
  void TrkNoDelFromQueue(int nTrkNo)
  {
    int nQue;
    for (nQue = 1; nQue < MaxQueNum; nQue ++)
    {
      if (m_pIdelTrkNo[nQue].state == 0 && m_pIdelTrkNo[nQue].TrkNo == nTrkNo)
      {
        DelFromQueue(nQue);
      }
    }
  }
};


//IVR話務統計參數
class CIVRCallCountParam
{
public:
  short state;
  char IVRNo[20];
  char IVRName[64];

  int IVRInCount; //IVR呼入總次數
  int IVRInAnsCount; //IVR應答總次數
  int IVRInTranAGCount; //IVR轉座席總次數
  int IVRInTranAGAnsCount; //IVR轉座席後應答總次數
  int IVRInTranAGAbandonCount; //IVR轉座席失敗或放棄總次數

  int IVRInAnswerRate; //IVR轉座席接通率
  
  int IVRInWaitTimeSumLen; //IVR轉座席累計等待時長
  int IVRInWaitTimeMaxLen; //IVR轉座席最大等待時長
  int IVRInWaitTimeMinLen; //IVR轉座席最小等待時長
  int IVRInWaitTimeAvgLen; //IVR轉座席平均等待時長

  //2015-10-22 add 針對不同IVR群組實時占用數的修改
  int IvrChnBusyCount; //本IVR群組占用數
  int IvrChnInBusyCount; //本IVR群組呼入占用數
  int IvrChnOutBusyCount; //本IVR群組呼出占用數

public:
  void Reset()
  {
    IVRInCount = 0; //IVR呼入總次數
    IVRInAnsCount = 0; //IVR應答總次數
    IVRInTranAGCount = 0; //IVR轉座席總次數
    IVRInTranAGAnsCount = 0; //IVR轉座席後應答總次數
    IVRInTranAGAbandonCount = 0; //IVR轉座席失敗或放棄總次數

    IVRInAnswerRate = 0; //IVR轉座席接通率
    
    IVRInWaitTimeSumLen = 0; //IVR轉座席累計等待時長
    IVRInWaitTimeMaxLen = 0; //IVR轉座席最大等待時長
    IVRInWaitTimeMinLen = 0; //IVR轉座席最小等待時長
    IVRInWaitTimeAvgLen = 0; //IVR轉座席平均等待時長

    IvrChnBusyCount = 0;
    IvrChnInBusyCount = 0;
    IvrChnOutBusyCount = 0;
  }
  void Init()
  {
    state = 0;
    memset(IVRNo, 0, 20);
    memset(IVRName, 0, 64);
    Reset();
  }

  void InitOnlines()
  {
    IvrChnBusyCount = 0;
    IvrChnInBusyCount = 0;
    IvrChnOutBusyCount = 0;
  }
  
  CIVRCallCountParam()
  {
    Init();
  }
  virtual ~CIVRCallCountParam(){}
};

#define MAXIVRGROUPNUM		64		//最大IVR群組數

//IVR話務統計參數管理
class CIVRCallCountParamMng
{
public:
  short GetIvrNoType; //呼入時取IVR群組號的方式：1-從被叫號碼取，2-從原被叫號碼取 2015-11-22
  short MaxIvrNum;
  CIVRCallCountParam IVRCallCountParam[MAXIVRGROUPNUM];

public:
  void Reset()
  {
    for (int i=0; i<MAXIVRGROUPNUM; i++)
    {
      IVRCallCountParam[i].Reset();
    }
  }
  void Init()
  {
    GetIvrNoType = 1;
    MaxIvrNum = 1;
    for (int i=0; i<MAXIVRGROUPNUM; i++)
    {
      IVRCallCountParam[i].Init();
    }
    IVRCallCountParam[0].state = 1;
    strcpy(IVRCallCountParam[0].IVRNo, "0");
  }
  void InitAllOnlines()
  {
    for (int i=0; i<MAXIVRGROUPNUM; i++)
    {
      IVRCallCountParam[i].InitOnlines();
    }
  }
  CIVRCallCountParamMng()
  {
    Init();
  }
  CIVRCallCountParam *GetIVRCallCountParam(const char *ivrno)
  {
    int i;
    if (strlen(ivrno) == 0)
    {
      return NULL;
    }
    for (i=0; i<MAXIVRGROUPNUM; i++)
    {
      if (strcmp(ivrno, IVRCallCountParam[i].IVRNo) == 0)
      {
        return &IVRCallCountParam[i];
      }
    }
    for (i=0; i<MAXIVRGROUPNUM; i++)
    {
      if (IVRCallCountParam[i].state == 0)
      {
        MaxIvrNum ++;
        IVRCallCountParam[i].state = 1;
        strcpy(IVRCallCountParam[i].IVRNo, ivrno);
        return &IVRCallCountParam[i];
      }
    }
    return NULL;
  }
  CIVRCallCountParam *IncIVRInOnlines(const char *ivrno)
  {
    IVRCallCountParam[0].IvrChnBusyCount ++;
    IVRCallCountParam[0].IvrChnInBusyCount ++;
    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IvrChnBusyCount ++;
      pIVRCallCountParam->IvrChnInBusyCount ++;
    }
    return pIVRCallCountParam;
  }
  CIVRCallCountParam *IncIVROutOnlines(const char *ivrno)
  {
    IVRCallCountParam[0].IvrChnBusyCount ++;
    IVRCallCountParam[0].IvrChnOutBusyCount ++;
    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IvrChnBusyCount ++;
      pIVRCallCountParam->IvrChnOutBusyCount ++;
    }
    return pIVRCallCountParam;
  }
  CIVRCallCountParam *IncIVRInCount(const char *ivrno)
  {
    IVRCallCountParam[0].IVRInCount ++;
    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IVRInCount ++;
    }
    return pIVRCallCountParam;
  }
  CIVRCallCountParam *IncIVRInAnsCount(const char *ivrno)
  {
    IVRCallCountParam[0].IVRInAnsCount ++;
    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IVRInAnsCount ++;
    }
    return pIVRCallCountParam;
  }
  CIVRCallCountParam *IncIVRInTranAGCount(const char *ivrno)
  {
    IVRCallCountParam[0].IVRInTranAGCount ++;
    if (IVRCallCountParam[0].IVRInTranAGCount > 0)
      IVRCallCountParam[0].IVRInAnswerRate = IVRCallCountParam[0].IVRInTranAGAnsCount*100/IVRCallCountParam[0].IVRInTranAGCount;

    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IVRInTranAGCount ++;
      if (pIVRCallCountParam->IVRInTranAGCount > 0)
        pIVRCallCountParam->IVRInAnswerRate = pIVRCallCountParam->IVRInTranAGAnsCount*100/pIVRCallCountParam->IVRInTranAGCount;
    }
    return pIVRCallCountParam;
  }
  CIVRCallCountParam *IncIVRInTranAGAnsCount(const char *ivrno, int timelen)
  {
    IVRCallCountParam[0].IVRInTranAGAnsCount ++;
    
    IVRCallCountParam[0].IVRInWaitTimeSumLen += timelen;
    if (IVRCallCountParam[0].IVRInWaitTimeMaxLen == 0  || timelen > IVRCallCountParam[0].IVRInWaitTimeMaxLen)
      IVRCallCountParam[0].IVRInWaitTimeMaxLen = timelen;
    if (IVRCallCountParam[0].IVRInWaitTimeMinLen == 0  || timelen < IVRCallCountParam[0].IVRInWaitTimeMinLen)
      IVRCallCountParam[0].IVRInWaitTimeMinLen = timelen;
    if (IVRCallCountParam[0].IVRInTranAGCount > 0)
      IVRCallCountParam[0].IVRInWaitTimeAvgLen = IVRCallCountParam[0].IVRInWaitTimeSumLen/IVRCallCountParam[0].IVRInTranAGCount;

    if (IVRCallCountParam[0].IVRInTranAGCount > 0)
      IVRCallCountParam[0].IVRInAnswerRate = IVRCallCountParam[0].IVRInTranAGAnsCount*100/IVRCallCountParam[0].IVRInTranAGCount;

    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IVRInTranAGAnsCount ++;
      
      pIVRCallCountParam->IVRInWaitTimeSumLen += timelen;
      if (pIVRCallCountParam->IVRInWaitTimeMaxLen == 0  || timelen > pIVRCallCountParam->IVRInWaitTimeMaxLen)
        pIVRCallCountParam->IVRInWaitTimeMaxLen = timelen;
      if (pIVRCallCountParam->IVRInWaitTimeMinLen == 0  || timelen < pIVRCallCountParam->IVRInWaitTimeMinLen)
        pIVRCallCountParam->IVRInWaitTimeMinLen = timelen;
      if (pIVRCallCountParam->IVRInTranAGCount > 0)
        pIVRCallCountParam->IVRInWaitTimeAvgLen = pIVRCallCountParam->IVRInWaitTimeSumLen/pIVRCallCountParam->IVRInTranAGCount;

      if (pIVRCallCountParam->IVRInTranAGCount > 0)
        pIVRCallCountParam->IVRInAnswerRate = pIVRCallCountParam->IVRInTranAGAnsCount*100/pIVRCallCountParam->IVRInTranAGCount;
    }
    return pIVRCallCountParam;
  }
  CIVRCallCountParam *IncIVRInTranAGAbandonCount(const char *ivrno, int timelen)
  {
    IVRCallCountParam[0].IVRInTranAGAbandonCount ++;

    IVRCallCountParam[0].IVRInWaitTimeSumLen += timelen;
    if (IVRCallCountParam[0].IVRInWaitTimeMaxLen == 0  || timelen > IVRCallCountParam[0].IVRInWaitTimeMaxLen)
      IVRCallCountParam[0].IVRInWaitTimeMaxLen = timelen;
    if (IVRCallCountParam[0].IVRInWaitTimeMinLen == 0  || timelen < IVRCallCountParam[0].IVRInWaitTimeMinLen)
      IVRCallCountParam[0].IVRInWaitTimeMinLen = timelen;
    if (IVRCallCountParam[0].IVRInTranAGCount > 0)
      IVRCallCountParam[0].IVRInWaitTimeAvgLen = IVRCallCountParam[0].IVRInWaitTimeSumLen/IVRCallCountParam[0].IVRInTranAGCount;

    if (IVRCallCountParam[0].IVRInTranAGCount > 0)
      IVRCallCountParam[0].IVRInAnswerRate = IVRCallCountParam[0].IVRInTranAGAnsCount*100/IVRCallCountParam[0].IVRInTranAGCount;
    
    CIVRCallCountParam *pIVRCallCountParam = GetIVRCallCountParam(ivrno);
    if (pIVRCallCountParam)
    {
      pIVRCallCountParam->IVRInTranAGAbandonCount ++;

      pIVRCallCountParam->IVRInWaitTimeSumLen += timelen;
      if (pIVRCallCountParam->IVRInWaitTimeMaxLen == 0  || timelen > pIVRCallCountParam->IVRInWaitTimeMaxLen)
        pIVRCallCountParam->IVRInWaitTimeMaxLen = timelen;
      if (pIVRCallCountParam->IVRInWaitTimeMinLen == 0  || timelen < pIVRCallCountParam->IVRInWaitTimeMinLen)
        pIVRCallCountParam->IVRInWaitTimeMinLen = timelen;
      if (pIVRCallCountParam->IVRInTranAGCount > 0)
        pIVRCallCountParam->IVRInWaitTimeAvgLen = pIVRCallCountParam->IVRInWaitTimeSumLen/pIVRCallCountParam->IVRInTranAGCount;

      if (pIVRCallCountParam->IVRInTranAGCount > 0)
        pIVRCallCountParam->IVRInAnswerRate = pIVRCallCountParam->IVRInTranAGAnsCount*100/pIVRCallCountParam->IVRInTranAGCount;
    }
    return pIVRCallCountParam;
  }
};

//---------------------------------------------------------------------------
#endif
