//---------------------------------------------------------------------------
/*
接收消息先進先出隊列
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "loginoutfifo.h"

//---------------------------------------------------------------------------
unsigned short CLogInOutFifo::Write(US ServerId, US ClientId, UC NodeType, US NodeId, UC LoginOut)
{
  m_cCriticalSection.Lock();
	if(IsFull()) //full
  {
    m_cCriticalSection.Unlock();
		return 0;
  }

  pMsgs[Tail].ServerId = ServerId;
  pMsgs[Tail].ClientId = ClientId;
  pMsgs[Tail].NodeType = NodeType;
  pMsgs[Tail].NodeId = NodeId;
  pMsgs[Tail].LoginOut = LoginOut;

	if(++Tail >= Len)
    Tail=0;
  m_cCriticalSection.Unlock();
	return 1;
}

unsigned short CLogInOutFifo::Read(US &ServerId, US &ClientId, UC &NodeType, US &NodeId, UC &LoginOut)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
	  return 0;//Buf Null
  }
	
	ServerId = pMsgs[Head].ServerId;
	ClientId = pMsgs[Head].ClientId;
  NodeType = pMsgs[Head].NodeType;
  NodeId = pMsgs[Head].NodeId;
  LoginOut = pMsgs[Head].LoginOut;

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}
unsigned short CLogInOutFifo::Read(VXML_LGOINOUT_STRUCT *LogInOutMsg)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
	  return 0;//Buf Null
  }
	
	LogInOutMsg->ServerId = pMsgs[Head].ServerId;
	LogInOutMsg->ClientId = pMsgs[Head].ClientId;
  LogInOutMsg->NodeType = pMsgs[Head].NodeType;
  LogInOutMsg->NodeId = pMsgs[Head].NodeId;
  LogInOutMsg->LoginOut = pMsgs[Head].LoginOut;

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}
