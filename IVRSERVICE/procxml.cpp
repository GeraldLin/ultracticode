//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procxml.h"
#include "extern.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void SendMsg2XML(int MsgId, UL SessionId, CH *msg)
{
	US ClientId;
	if (SessionId == 0) return;
  ClientId = (US)(SessionId >> 16);
	pTcpLink->SendMessage2CLIENT(ClientId, MsgId, msg);
  if (MsgId==0 || MsgId==MSG_ongetidleseat) 
    return;
  if (MsgId==MSG_onexecsql && pIVRCfg->isDispSQLMsg == false)
    return;
  MyTrace(2, "0x%04x %s", ClientId, msg);
}
void SendExecSql2XML(CH *sqls)
{
  sprintf(SendMsgBuf, "<execsql sessionid='%ld' cmdaddr='0' chantype='0' channo='0' sqls=\"%s\"/>", 
    0x02010000, sqls);
  SendMsg2XML(MSG_onexecsql, 0x02010000, SendMsgBuf);
  MyTrace(4, "0x%04x %s", 0x0201, SendMsgBuf);
}
void SendExecSql2DB(CH *sqls)
{
  sprintf(SendMsgBuf, "<execsql sessionid='%ld' cmdaddr='0' chantype='0' channo='0' sqls=\"%s\"/>", 
    (pDBClient->ClientId<<16), sqls);
  pDBClient->DBSendMessage(MSG_execsql, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetDBServerId(), SendMsgBuf);
}
void SendDumpSql2DB(CH *querysqls, CH *insertsqls)
{
  sprintf(SendMsgBuf, "<dbrecorddump sessionid='%ld' cmdaddr='0' chantype='0' channo='0' sourdbid='%d' querysqls=\"%s\" destdbid='%d' insertsqls=\"%s\"/>", 
    (pTcpLink->IVRServerId<<16), pIVRCfg->SourDBId, querysqls, pIVRCfg->DestDBId, insertsqls);
  pTcpLink->CRMSendMessage(MSG_dbrecorddump, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pTcpLink->GetCRMServerId(), SendMsgBuf);
}
void SendQueryCustomer2Webservice(const CH *callerno, const CH *calledno, const CH *weberviceaddr, const CH *weberviceparam)
{
  sprintf(SendMsgBuf, "<querycustomer sessionid='%ld' cmdaddr='0' chantype='0' channo='0' callerno='%s' calledno='%s' webserviceaddr='%s' param='%s'/>", 
    (pTcpLink->IVRServerId<<16)|g_nWebServiceSerialNo, callerno, calledno, weberviceaddr, weberviceparam);
  pTcpLink->GWSendMessage(MSG_querycustomer, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pTcpLink->GetGWServerId(), SendMsgBuf);
  g_nWebServiceSerialNo++;
  if (g_nWebServiceSerialNo >= 512)
  {
    g_nWebServiceSerialNo = 0;
  }
}
void SendQueryWebService2GW(CH *params)
{
  sprintf(SendMsgBuf, "<querywebservice sessionid='%ld' cmdaddr='0' chantype='0' channo='0' wsdladdress='%s' soapaction='%s' startenvelope='%s' startbody='%s' interfacestring='%s' paramcount='%d' paramstring='%s%s'/>", 
    (pTcpLink->IVRServerId<<16)|g_nWebServiceSerialNo, 
    g_WebServiceParam.WS_wsdladdress, g_WebServiceParam.WS_soapaction,
    g_WebServiceParam.WS_startenvelope, g_WebServiceParam.WS_startbody,
    g_WebServiceParam.WS_interface, g_WebServiceParam.WS_paramcount,
    g_WebServiceParam.WS_paramstring[0], params);
  pTcpLink->GWSendMessage(MSG_querywebservice, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pTcpLink->GetGWServerId(), SendMsgBuf);
  g_nWebServiceSerialNo++;
  if (g_nWebServiceSerialNo >= 512)
  {
    g_nWebServiceSerialNo = 0;
  }
}
void SendIMACDResult(UL SessionNo, const char *IMSerialNo, int nResult)
{
  sprintf(SendMsgBuf, "<imcallinresult sessionid='%ld' cmdaddr='0' chantype='0' channo='0' result='%d' seatno='0' workerno='0' groupno='0' queueno='0' imserialno='%s' param='' errorbuf=''/>", 
    SessionNo, nResult, IMSerialNo);
  pDBClient->IMSendMessage(MSG_imcallinresult, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetIMServerId(), SendMsgBuf);
}
void SendIMACDResult(UL SessionNo, const char *IMSerialNo, int nResult, const CH *seatno, int workerno, int groupno)
{
  sprintf(SendMsgBuf, "<imcallinresult sessionid='%ld' cmdaddr='0' chantype='0' channo='0' result='%d' seatno='%s' workerno='%d' groupno='%d' queueno='0' imserialno='%s' param='' errorbuf=''/>", 
    SessionNo, nResult, seatno, workerno, groupno, IMSerialNo);
  pDBClient->IMSendMessage(MSG_imcallinresult, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetIMServerId(), SendMsgBuf);
}

void SendEMACDResult(UL SessionNo, const char *EMSerialNo, int nResult)
{
  sprintf(SendMsgBuf, "<emcallinresult sessionid='%ld' cmdaddr='0' chantype='0' channo='0' result='%d' seatno='0' workerno='0' groupno='0' queueno='0' emserialno='%s' param='' errorbuf=''/>", 
    SessionNo, nResult, EMSerialNo);
  pDBClient->IMSendMessage(MSG_emcallinresult, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetIMServerId(), SendMsgBuf);
}
void SendEMACDResult(UL SessionNo, const char *EMSerialNo, int nResult, const CH *seatno, int workerno, int groupno)
{
  sprintf(SendMsgBuf, "<emcallinresult sessionid='%ld' cmdaddr='0' chantype='0' channo='0' result='%d' seatno='%s' workerno='%d' groupno='%d' queueno='0' emserialno='%s' param='' errorbuf=''/>", 
    SessionNo, nResult, seatno, workerno, groupno, EMSerialNo);
  pDBClient->IMSendMessage(MSG_emcallinresult, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetIMServerId(), SendMsgBuf);
}

void SendFAXACDResult(UL SessionNo, const char *EMSerialNo, int nResult)
{
  sprintf(SendMsgBuf, "<faxcallinresult sessionid='%ld' cmdaddr='0' chantype='0' channo='0' result='%d' seatno='0' workerno='0' groupno='0' queueno='0' fxserialno='%s' param='' errorbuf=''/>", 
    SessionNo, nResult, EMSerialNo);
  pDBClient->IMSendMessage(MSG_faxcallinresult, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetIMServerId(), SendMsgBuf);
}
void SendFAXACDResult(UL SessionNo, const char *EMSerialNo, int nResult, const CH *seatno, int workerno, int groupno)
{
  sprintf(SendMsgBuf, "<faxcallinresult sessionid='%ld' cmdaddr='0' chantype='0' channo='0' result='%d' seatno='%s' workerno='%d' groupno='%d' queueno='0' fxserialno='%s' param='' errorbuf=''/>", 
    SessionNo, nResult, seatno, workerno, groupno, EMSerialNo);
  pDBClient->IMSendMessage(MSG_faxcallinresult, SendMsgBuf);
  MyTrace(4, "0x%04x %s", pDBClient->GetIMServerId(), SendMsgBuf);
}
//-----------------------------------------------------------------------------
//取消息頭并分析消息的合法性
void GetXMLHeader(VXML_CMD_ATTRIBUTE_STRUCT *AttrData, VXML_XML_HEADER_STRUCT *XMLHeader)
{
	XMLHeader->MsgId = AttrData->MsgId;
	XMLHeader->SessionId = atol(AttrData->AttrValue[0]);
	XMLHeader->CmdAddr = atol(AttrData->AttrValue[1]);
	XMLHeader->ChnType = atoi(AttrData->AttrValue[2]);
	XMLHeader->ChnNo = atoi(AttrData->AttrValue[3]);
	XMLHeader->ErrorId = 0; //RMsgFifo->Parse_Msg(pFlwRule, pFlwRule->XMLIVRMsgRule, AttrData);
	XMLHeader->OnlineId = pBoxchn->Get_ChnNo_By_LgChn(XMLHeader->ChnType, XMLHeader->ChnNo, XMLHeader->nChn);
}
//設置當前會話序列號及指令地址
void SetSessionCmdAttr(int nChn, UL sessionid, UL cmdattr)
{
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  //pChn->SessionNo = sessionid;
  pChn->CmdAddr = cmdattr;
}
//設置當前放音會話參數
void SetcurPlaySessionParam(int nChn, int MsgId, const char *MsgName)
{
	pChn->curPlayData.CmdAddr = pChn->CmdAddr;
  pChn->curPlayData.OnMsgId = MsgId;
  pChn->curPlayData.MsgName = MsgName;
}
//發送設置接入號碼消息處理結果
void Proc_MSG_setaccessno_Result(UL sessionid, int settype, const CH *accessno, int result, const CH *error)
{
  sprintf(SendMsgBuf, "<onsetaccessno sessionid='%ld' settype='%d' accessno='%s' result='%d' errorbuf='%s'/>", 
    sessionid, settype, accessno, result, error);
  SendMsg2XML(MSG_onsetaccessno, sessionid, SendMsgBuf);
}
//發送通用消息處理結果
void Proc_MSG_common_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送放音消息處理結果
void Proc_MSG_play_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *dtmf, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' dtmfbuf='%s' asrbuf='' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, dtmf, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送錄音消息處理結果
void Proc_MSG_record_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' recordedlen='0' recordedsize='0' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送會議錄音錄音消息處理結果
void Proc_MSG_recordcfc_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int cfcno, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' cfcno='%d' recordedlen='0' recordedsize='0' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, cfcno, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送呼出消息處理結果
void Proc_MSG_callout_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='0' outchanno='0' calledno='' funcno='0' point='0' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
void Proc_MSG_transfer_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='0' outchanno='0' calledno='' funcno='0' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
void Proc_MSG_callseat_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='0' outchanno='0' seatno='0' workerno='0' groupno='0' queueno='0' serialno='' param='' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送檢查文件消息處理結果
void Proc_MSG_checkfile_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, UL size, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' size='%ld' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, size, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送取文件數消息處理結果
void Proc_MSG_getfilenum_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int filenums, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' filenums='%d' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, filenums, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送取文件名消息處理結果
void Proc_MSG_getfilename_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int serialno, int filenums, const CH *filename, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' serialno='%d' filenums='%d' filename='%s' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, serialno, filenums, filename, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送創建會議消息處理結果
void Proc_MSG_createcfc_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int cfcno, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' cfcno='%d' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, cfcno, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
void Proc_MSG_createcfc_Result(int nChn, int result, int cfcno, const CH *error)
{
	sprintf(SendMsgBuf, "<oncreatecfc sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' cfcno='%d' errorbuf='%s'/>", 
		pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, cfcno, error);
	SendMsg2XML(MSG_oncreatecfc, pChn->SessionNo, SendMsgBuf);
}
//發送檢查文件消息處理結果
void Proc_MSG_checktone_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int tonetype, const CH *error)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' tonetype='%d' errorbuf='%s'/>", 
		XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, tonetype, error);
	SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送代接電話處理結果
void Proc_MSG_pickupcall_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *callerno, const CH *calledno, int chntype, int chnno, const CH *seatno, int workerno, const CH *error)
{
  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' callerno='%s' calledno='%s' chntype='%d' chnno='%d' seatno='%s' workerno='%d' errorbuf='%s'/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, callerno, calledno, chntype, chnno, seatno, workerno, error);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
void Proc_MSG_pickupcall_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int nOut, int point, const CH *seatno, int workerno)
{
  int nChn, orgchntype, orgchnno;
  
  nChn = pOut->Session.nChn;
  pBoxchn->GetLgChBynChn(nChn, orgchntype, orgchnno);

  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' callerno='%s' calledno='%s' chntype='%d' chnno='%d' seatno='%s' workerno='%d' errorbuf=''/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, pOut->CallerNo.C_Str(), pOut->Calleds[point].CalledNo, orgchntype, orgchnno, seatno, workerno);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
void Proc_MSG_pickupcall_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int nAcd, const CH *seatno, int workerno)
{
  int nChn, orgchntype, orgchnno;
  
  nChn = pAcd->Session.nChn;
  pBoxchn->GetLgChBynChn(nChn, orgchntype, orgchnno);

  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' callerno='%s' calledno='%s' chntype='%d' chnno='%d' seatno='%s' workerno='%d' errorbuf=''/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, pAcd->Session.CallerNo.C_Str(), pAcd->Session.CalledNo.C_Str(), orgchntype, orgchnno, seatno, workerno);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送取ACD隊列信息處理結果
void Proc_MSG_getacdqueue_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, int waitacd, int waitans, const CH *error)
{
  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' waitacd='%d' waitans='%d' errorbuf='%s'/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, waitacd, waitans, error);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//發送綁定坐席通道結果
void Proc_MSG_bandagentchn_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int nChn, int result, const CH *error)
{
  CAgent *pAgent=NULL;
  if (!pBoxchn->isnChnAvail(nChn))
  {
    sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' chhwtype='0' chhwindex='0' seattype='0' seatno='' workerno='0' groupno='0' result='%d' errorbuf='%s'/>", 
      XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
    SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
  }
  else
  {
    pAgent = GetAgentBynChn(nChn);
    if (pAgent == NULL)
    {
      sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' chhwtype='0' chhwindex='0' seattype='0' seatno='' workerno='0' groupno='0' result='%d' errorbuf='%s'/>", 
        XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
      SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
    }
    else
    {
      sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' chhwtype='%d' chhwindex='%d' seattype='%d' seatno='%s' workerno='%d' groupno='%d' result='%d' errorbuf='%s'/>", 
        XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, 
        pChn->ChStyle, pChn->ChStyleIndex, pAgent->GetSeatType(),
        pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAgent->GetGroupNo(),
        result, error);
      SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
    }
  }
}
//檢查通用消息
int Check_common_MSG(VXML_XML_HEADER_STRUCT *XMLHeader)
{
	if (XMLHeader->ErrorId != 0)
	{
		Proc_MSG_common_Result(XMLHeader, OnFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader->OnlineId != 0)
	{
		Proc_MSG_common_Result(XMLHeader, OnFail, "chn is error or not online");
		return 1;
	}
  return 0;
}
//檢查放音消息
int Check_play_MSG(VXML_XML_HEADER_STRUCT *XMLHeader)
{
	if (XMLHeader->ErrorId != 0)
	{
		Proc_MSG_play_Result(XMLHeader, OnPlayError, "", "recv msg param error");
		return 1;
	}
	if (XMLHeader->OnlineId != 0)
	{
		Proc_MSG_play_Result(XMLHeader, OnPlayError, "", "chn is error or not online");
		return 1;
	}
  return 0;
}
//清理主叫號碼里的非數字字符
void ClearCharInCallerNo(int nChn)
{
  char callerno[MAX_TELECODE_LEN];
  int i, j=0, len;
  
  memset(callerno, 0, MAX_TELECODE_LEN);
  len = strlen(pChn->CallerNo);
  for (i = 0; i < len; i ++)
  {
    if (pChn->CallerNo[i] >= '0' && pChn->CallerNo[i] <= '9')
    {
      callerno[j] = pChn->CallerNo[i];
      j ++;
    }
  }
  strncpy(pChn->CallerNo, callerno, 20);
}
void ClearCharInCalledNo(char *pszCalledNo)
{
  char calledno[MAX_TELECODE_LEN];
  int i, j=0, len;
  
  memset(calledno, 0, MAX_TELECODE_LEN);
  len = strlen(pszCalledNo);
  for (i = 0; i < len && i < MAX_TELECODE_LEN; i ++)
  {
    if ((pszCalledNo[i] >= '0' && pszCalledNo[i] <= '9') || pszCalledNo[i] == '*' || pszCalledNo[i] == '#') //2015-01-07 清除號碼里的非法字符
    {
      calledno[j] = pszCalledNo[i];
      j ++;
    }
  }
  strcpy(pszCalledNo, calledno);
}
//發送電話呼入事件
void SendCallinEvent(int nChn, int VxmlId, int calledtype, const char *param, int bandagfirst)
{
  UL SessionNo;
  int workerno=0, seattype=0, groupno=0;
  char seatno[32];

  if (AuthPassId == false && g_nCallInCount > MAXCALLCOUNTONAUTHFAIL)
  {
    MyTrace(0, "nChn=%d SendCallinEvent but authdata is error !!!", nChn);
    return;
  }
  if (pIVRCfg->isClearCallerNoChar == true)
    ClearCharInCallerNo(nChn);
  int nAG = pChn->nAG;
  strcpy(seatno, "0");
  if (pAgentMng->isnAGAvail(nAG))
  {
    workerno = pAG->GetWorkerNo();
    seattype = pAG->GetSeatType();
    strcpy(seatno, pAG->GetSeatNo().C_Str());
    groupno = pAG->GetGroupNo();
  }

  if (pIVRCfg->isMCITestFlag == true)
  {
    GetPrivateProfileString("CONFIGURE", "MCIMessage", "", pChn->CallInDtmfBuf, MAX_DTMFBUF_LEN-1, g_szServiceINIFileName);
  }

	sprintf(SendMsgBuf, "<oncallin chantype='%d' channo='%d' callerno='%s' callertype='0' calledno='%s' calledtype='%d' orgcallerno='%s' orgcalledno='%s' inrouteno='%d' chhwtype='%d' chhwindex='%d' seattype='%d' seatno='%s' workerno='%d' param='%s' groupno='%d' bandagfirst='%d' calltype='%d' answerid='%d' cdrserialno='%s' recdrootpath='%s' recdfilename='%s' dtmfbuf='%s'/>", 
    pChn->lgChnType, pChn->lgChnNo, pChn->CallerNo, pChn->CalledNo, calledtype,
    pChn->OrgCallerNo.C_Str(), pChn->OrgCalledNo.C_Str(), 
    pChn->lgChnType==2 ? pChn->CallOutRouteNo : pChn->RouteNo, pChn->ChStyle, pChn->ChStyleIndex, 
    seattype, seatno, workerno, param, groupno, bandagfirst, pChn->nCallType, 
    pChn->nAnswerId, pChn->CdrSerialNo, pIVRCfg->SeatRecPath.C_Str(), pChn->RecordFileName, pChn->CallInDtmfBuf);
	SessionNo = ((VxmlId << 16) & 0x00FF0000) | 0x02000000;
  pChn->SessionNo = SessionNo;
  memset(pChn->CallInDtmfBuf, 0, MAX_DTMFBUF_LEN);
  SendMsg2XML(MSG_oncallin, SessionNo, SendMsgBuf);
}
//發送后續號碼
void SendSamEvent(int nChn, const char *Sam, const char *callerno, const char *orgcallerno, const char *orgcalledno)
{
  sprintf(SendMsgBuf, "<onsendsam sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcalledno='%s' orgcallerno='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, callerno, Sam, orgcallerno, orgcalledno);
  SendMsg2XML(MSG_onsendsam, pChn->SessionNo, SendMsgBuf);
}
void SendSeatDialDTMFEvent(int nChn, const char *dialdtmf)
{
  sprintf(SendMsgBuf, "<ondialdtmf sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' dialdtmf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, dialdtmf);
  SendMsg2XML(MSG_ongetdialdtmf, pChn->SessionNo, SendMsgBuf);
}
//發送應答結果
void SendAnswerResult(int nChn, int result, const char *error)
{
  sprintf(SendMsgBuf, "<onanswer sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, error);
  SendMsg2XML(MSG_onanswer, pChn->SessionNo, SendMsgBuf);
}
//發送通道放音結果
void SendChnPlayResult(int nChn, int result, const char *error)
{
	if (pChn->curPlayData.OnMsgId != MSG_onttsstring && pChn->curPlayData.OnMsgId != MSG_onttsfile) //edit 2008-07-05 add
  {
    sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' dtmfbuf='%s' asrbuf='' errorbuf='%s'/>", 
      pChn->curPlayData.MsgName.C_Str(), pChn->SessionNo, pChn->curPlayData.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, pChn->RecvDtmfBuf, error);
  }
  else
  {
    sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' dtmfbuf='%s' asrbuf='' ttsbuf='' errorbuf='%s'/>", 
      pChn->curPlayData.MsgName.C_Str(), pChn->SessionNo, pChn->curPlayData.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, pChn->RecvDtmfBuf, error);
  }
	SendMsg2XML(pChn->curPlayData.OnMsgId, pChn->SessionNo, SendMsgBuf);
}
//發送通道TTS合成結果 
void SendChnTTSResult(int nChn, int result, const char *ttsfile, const char *error) //edit 2008-07-05 add
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' dtmfbuf='%s' asrbuf='' ttsbuf='%s' errorbuf='%s'/>", 
    pChn->curPlayData.MsgName.C_Str(), pChn->SessionNo, pChn->curPlayData.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, pChn->RecvDtmfBuf, ttsfile, error);
	SendMsg2XML(pChn->curPlayData.OnMsgId, pChn->SessionNo, SendMsgBuf);
}
//傳送單個DTMF碼（針對overload發送方式）
void TransferOneDTMF(int nChn, int result, char dtmf)
{
	sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' dtmfbuf='%c' asrbuf='' errorbuf=''/>", 
    pChn->curPlayData.MsgName.C_Str(), pChn->SessionNo, pChn->curPlayData.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, dtmf);
	SendMsg2XML(pChn->curPlayData.OnMsgId, pChn->SessionNo, SendMsgBuf);
}
//發送通道錄音結果
void SendChnRecResult(int nChn, int result, long reclen, long filesize)
{
	sprintf(SendMsgBuf, "<onrecordfile sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' recordedlen='%ld' recordedsize='%ld' errorbuf=''/>", 
    pChn->curRecData.SessionNo, pChn->curRecData.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, reclen, filesize);
	SendMsg2XML(MSG_onrecordfile, pChn->curRecData.SessionNo, SendMsgBuf);
}
//發送會議錄音結果
void SendConfRecResult(int nCfc, int result, long reclen, long filesize)
{
	sprintf(SendMsgBuf, "<onrecordcfc sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' recordedlen='%ld' recordedsize='%ld' errorbuf=''/>", 
    pCfc->curRecData.SessionNo, pCfc->curRecData.CmdAddr, pCfc->curRecData.lgChnType, pCfc->curRecData.lgChnNo, result, reclen, filesize);
	SendMsg2XML(MSG_onrecordcfc, pCfc->curRecData.SessionNo, SendMsgBuf);
}
//發送傳真結果
void Proc_MSG_Sendfax_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' sendpages='0' sendbytes='0' sendspeed='0' errorbuf='%s'/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}

void SendSendfaxResult(int nChn, int result, LPCTSTR ErrorMsg, short SendPages, long SendBytes, long SendSpeed)
{
  sprintf(SendMsgBuf, "<onsendfax sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' sendpages='%d' sendbytes='%ld' sendspeed='%ld' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->FaxCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, SendPages, SendBytes, SendSpeed, ErrorMsg);
  SendMsg2XML(MSG_onsendfax, pChn->SessionNo, SendMsgBuf);
}

//接收傳真結果
void Proc_MSG_Recvfax_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const CH *error)
{
  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' faxcsid='' barcode='' recvpages='0' recvbytes='0' recvspeed='0' errorbuf='%s'/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, result, error);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}

//取一空閑坐席并臨時鎖定結果
void Proc_MSG_ongetidleseat_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const char *seatno, int workerno, int logins, int idles, const CH *error)
{
  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' seatno='%s' workerno='%d' logins='%d' idels='%d' result='%d' errorbuf='%s'/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, seatno, workerno, logins, idles, result, error);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}

void Proc_MSG_ongetidleseat_Result(VXML_XML_HEADER_STRUCT *XMLHeader, int result, const char *seatno, int workerno, int logins, const char *idles, const CH *error)
{
  sprintf(SendMsgBuf, "<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' seatno='%s' workerno='%d' logins='%d' idels='%s' result='%d' errorbuf='%s'/>", 
    XMLHeader->OnMsgName, XMLHeader->SessionId, XMLHeader->CmdAddr, XMLHeader->ChnType, XMLHeader->ChnNo, seatno, workerno, logins, idles, result, error);
  SendMsg2XML(XMLHeader->OnMsgId, XMLHeader->SessionId, SendMsgBuf);
}
//-----------------------------------------------------------------------------
void SendRecvfaxResult(int nChn, int result, LPCTSTR ErrorMsg, LPCTSTR FaxCSID, LPCTSTR BarCode, short RecvPages, long RecvBytes, long RecvSpeed)
{
  sprintf(SendMsgBuf, "<onrecvfax sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' faxcsid='%s' barcode='%s' recvpages='%d' recvbytes='%ld' recvspeed='%ld' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->FaxCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, FaxCSID, BarCode, RecvPages, RecvBytes, RecvSpeed, ErrorMsg);
  SendMsg2XML(MSG_onrecvfax, pChn->SessionNo, SendMsgBuf);
}
//發送通道關聯事件
void SendLinkChnEvent(int nChn, int result, const char *error)
{
	sprintf(SendMsgBuf, "<onlinkevent sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, error);
	SendMsg2XML(MSG_onlinkevent, pChn->SessionNo, SendMsgBuf);
}
//發送加入會議結果事件
void SendJoinConfResult(int nChn, int result, const char *error)
{
  if (!pBoxchn->isnChnAvail(nChn)) return;
  sprintf(SendMsgBuf, "<onjoincfc sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, error);
	SendMsg2XML(MSG_onjoincfc, pChn->SessionNo, SendMsgBuf);
}
//發送通道掛機事件
void SendChnHangonEvent(int nChn)
{
	if (!pBoxchn->isnChnAvail(nChn)) return;
  sprintf(SendMsgBuf, "<onhangon sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='0' errorbuf=''/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo);
	SendMsg2XML(MSG_onhangon, pChn->SessionNo, SendMsgBuf);
}
//發送交換機操作指令結果
void SendSendSwitchCMDResult(int nChn, int result)
{
  sprintf(SendMsgBuf, "<onsendswitchcmd sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf=''/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result);
  SendMsg2XML(MSG_onsendswitchcmd, pChn->SessionNo, SendMsgBuf);
}
void SendCallConnectedEvent(int nChn, int result)
{
  sprintf(SendMsgBuf, "<oncallconnected sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf=''/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result);
  SendMsg2XML(MSG_oncallconnected, pChn->SessionNo, SendMsgBuf);
}
//發送流程呼出結果事件
void SendCalloutResult(int nOut, int point, int result, const char *error, int nChn2)
{
  CAgent *pAgent=NULL;
  int nChn, orgchntype, orgchnno;
  int nChn1, outchntype, outchnno;
  char temp[32];

  nChn = pOut->Session.nChn;
  if (pBoxchn->isnChnAvail(nChn2))
    nChn1 = nChn2;
  else
    nChn1 = pOut->Calleds[point].OutnChn;
  pBoxchn->GetLgChBynChn(nChn, orgchntype, orgchnno);
  pBoxchn->GetLgChBynChn(nChn1, outchntype, outchnno);

  if (pBoxchn->isnChnAvail(nChn))
  {
    //給呼出方發送坐席呼出結果
    pAgent = GetAgentBynChn(nChn);
    if (pAgent != NULL)
    {
      if (pAgent->isTcpLinked())
      {
        sprintf(SendMsgBuf, "<onmakecall acdid='%lu' sessionno='%d' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
          pAgent->GetClientId()<<16, pOut->Session.SessionNo, outchntype, outchnno, pOut->CallerNo.C_Str(), pOut->Calleds[point].CalledNo, pOut->Session.Param.C_Str(), result, error);
	      SendMsg2AG(pAgent->GetClientId(), AGMSG_onmakecall, SendMsgBuf);
      }
      if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=%d;callerno=%s;calledno=%s;cdrserialno=%s;param=%s",
          pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), result, pOut->CallerNo.C_Str(), pOut->Calleds[point].CalledNo, pChn->CdrSerialNo, pOut->Session.Param.C_Str());
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
      if (result == OnOutAnswer)
      {
        pAgent->CanTransferId = 1;
        WriteSeatStatus(pAgent, AG_STATUS_TALK, 0, 2, 1);
        pAgent->SetSrvLine(nChn, pOut->Session.SessionNo, pOut->Session.CmdAddr);
        SetAgentsvState(pAgent, AG_SV_CONN);
        DispAgentStatus(pAgent->nAG);
      }
      else if (result == OnOutRing)
      {
        WriteSeatStatus(pAgent, AG_STATUS_OUTRING, 0);
        SetAgentsvState(pAgent, AG_SV_OUTRING);
        DispAgentStatus(pAgent->nAG);
      }
    }
  }

  if (pBoxchn->isnChnAvail(nChn1) && result == OnOutAnswer)
  {
    //給呼入方發送坐席應答結果
    pAgent = GetAgentBynChn(nChn1);
    if (pAgent != NULL)
    {
      if (pAgent->isTcpLinked())
      {
        sprintf(SendMsgBuf, "<onanswercall acdid='%lu' result='0' errorbuf=''/>", 
          pAgent->GetClientId()<<16);
	      SendMsg2AG(pAgent->GetClientId(), AGMSG_onanswercall, SendMsgBuf);
      }
    }
  }

  sprintf(temp, "%d,%d,%d", point, pOut->Calleds[point].CurPoint, pOut->Calleds[point].CurRouteNo);
  sprintf(SendMsgBuf, "<oncallout sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' calledno='%s' funcno='%d' point='%s' errorbuf='%s'/>", 
    pOut->Session.SessionNo, pOut->Session.CmdAddr, orgchntype, orgchnno, result, outchntype, outchnno, pOut->Calleds[point].CalledNo, pOut->Session.FuncNo, temp, error);
	SendMsg2XML(MSG_oncallout, pOut->Session.SessionNo, SendMsgBuf);
}
void SendCalloutResult1(int nChn, int nChn1, int result, const char *error)
{
  int outchntype, outchnno;
  
  pBoxchn->GetLgChBynChn(nChn1, outchntype, outchnno);
  
  sprintf(SendMsgBuf, "<oncallout sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' calledno='%s' funcno='0' point='0' errorbuf='%s'/>", 
    pChn->TranSessionNo, pChn->TranCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, outchntype, outchnno, pChn->CalledNo, error);
  SendMsg2XML(MSG_oncallout, pChn->TranSessionNo, SendMsgBuf);
}
//發送流程轉接呼出結果事件
void SendTransferResult(int nOut, int point, int result, const char *error)
{
  int orgchntype, orgchnno;
  int outchntype, outchnno;

  pBoxchn->GetLgChBynChn(pOut->Session.nChn, orgchntype, orgchnno);
  pBoxchn->GetLgChBynChn(pOut->Calleds[point].OutnChn, outchntype, outchnno);

  sprintf(SendMsgBuf, "<ontransfer sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' errorbuf='%s'/>", 
    pOut->Session.SessionNo, pOut->Session.CmdAddr, orgchntype, orgchnno, result, outchntype, outchnno, error);
	SendMsg2XML(MSG_ontransfer, pOut->Session.SessionNo, SendMsgBuf);
}
void SendTransferResult1(int nChn, int nChn1, int result, const char *error)
{
  int outchntype, outchnno;
  
  pBoxchn->GetLgChBynChn(nChn1, outchntype, outchnno);

  sprintf(SendMsgBuf, "<ontransfer sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' errorbuf='%s'/>", 
    pChn->TranSessionNo, pChn->TranCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, outchntype, outchnno, error);
  SendMsg2XML(MSG_ontransfer, pChn->TranSessionNo, SendMsgBuf);
}
//發送呼叫坐席結果
void SendCallseatResult(int nAcd, CAgent *pAgent, int queueno, int result, const char *error)
{
  int nChn, outchntype, outchnno, nAG;

  if (pAcd->Session.MediaType == 1)
  {
    if (pAgent == NULL)
    {
      SendIMACDResult(pAcd->Session.SessionNo, pAcd->Session.CRDSerialNo.C_Str(), result);
    }
    else
    {
      SendIMACDResult(pAcd->Session.SessionNo, pAcd->Session.CRDSerialNo.C_Str(), result, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAcd->ACDRule.AcdedGroupNo);
    }
    return;
  }
  else if (pAcd->Session.MediaType == 2)
  {
    if (pAgent == NULL)
    {
      SendEMACDResult(pAcd->Session.SessionNo, pAcd->Session.CRDSerialNo.C_Str(), result);
    }
    else
    {
      SendEMACDResult(pAcd->Session.SessionNo, pAcd->Session.CRDSerialNo.C_Str(), result, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAcd->ACDRule.AcdedGroupNo);
    }
    return;
  }
  else if (pAcd->Session.MediaType == 3)
  {
    if (pAgent == NULL)
    {
      SendFAXACDResult(pAcd->Session.SessionNo, pAcd->Session.CRDSerialNo.C_Str(), result);
    }
    else
    {
      SendFAXACDResult(pAcd->Session.SessionNo, pAcd->Session.CRDSerialNo.C_Str(), result, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAcd->ACDRule.AcdedGroupNo);
    }
    return;
  }
  nChn = pAcd->Session.nChn;
  if (pAgent == NULL)
  {
    if (pBoxchn->isnChnAvail(nChn))
    {
      sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='0' outchanno='0' seatno='0' workerno='0' groupno='0' queueno='%d' serialno='' param='' errorbuf='%s'/>", 
        pAcd->Session.SessionNo, pAcd->Session.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, queueno, error);
    }
    else
    {
      sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='0' channo='0' result='%d' outchantype='0' outchanno='0' seatno='0' workerno='0' groupno='0' queueno='%d' serialno='' param='' errorbuf='%s'/>", 
        pAcd->Session.SessionNo, pAcd->Session.CmdAddr, result, queueno, error);
    }
  }
  else
  {
    pBoxchn->GetLgChBynChn(pAgent->m_Seat.nChn, outchntype, outchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      nAG = pChn->nAG;
      if (result == OnACDRing && pAgentMng->isnAGAvail(pChn->nAG))
      {
        sprintf(SendMsgBuf, "<onmakecall acdid='%lu' sessionno='%d' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
          pAG->GetClientId()<<16, pAcd->Session.SessionNo, outchntype, outchnno, pChn->CallerNo, pChn->CustPhone, pAcd->Session.Param.C_Str(), OnOutRing, error);
        SendMsg2AG(pAG->GetClientId(), AGMSG_onmakecall, SendMsgBuf);

        SetAgentsvState(pChn->nAG, AG_SV_OUTRING);
        DispAgentStatus(pChn->nAG);
      }
      else if (result == OnACDAnswer && pAgentMng->isnAGAvail(pChn->nAG))
      {
        sprintf(SendMsgBuf, "<onmakecall acdid='%lu' sessionno='%d' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
          pAG->GetClientId()<<16, pAcd->Session.SessionNo, outchntype, outchnno, pChn->CallerNo, pChn->CustPhone, pAcd->Session.Param.C_Str(), OnOutAnswer, error);
        SendMsg2AG(pAG->GetClientId(), AGMSG_onmakecall, SendMsgBuf);

        SetAgentsvState(pChn->nAG, AG_SV_CONN);
        DispAgentStatus(pChn->nAG);
      }
      if (pAgent->m_Seat.ACDByVocTrkId == 0)
      {
        sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' seatno='%s' workerno='%d' groupno='%d' queueno='%d' serialno='%s' param='%s' errorbuf='%s'/>", 
          pAcd->Session.SessionNo, pAcd->Session.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, outchntype, outchnno, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAcd->ACDRule.AcdedGroupNo, queueno, pAgent->AcceptSerialNo.C_Str(), pAgent->AcceptParam.C_Str(), error);
      }
      else
      {
        //2015-11-25 針對語音中繼的修改
        pBoxchn->GetLgChBynChn(pAgent->m_Seat.nVocTrknChn, outchntype, outchnno);
        sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' seatno='%s' workerno='%d' groupno='%d' queueno='%d' serialno='%s' param='%s' errorbuf='%s'/>", 
          pAcd->Session.SessionNo, pAcd->Session.CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, outchntype, outchnno, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAcd->ACDRule.AcdedGroupNo, queueno, pAgent->AcceptSerialNo.C_Str(), pAgent->AcceptParam.C_Str(), error);
        
        if (result == OnACDAnswer)
        {
          pAgent->m_Seat.ACDByVocTrkId = 0; //2015-11-25 針對語音中繼的修改,清除標志
          pAgent->m_Seat.nVocTrknChn = 0xFFFF; //2015-11-25 針對語音中繼的修改
          //memset(pAgent->m_Seat.CustPhone, 0, MAX_TELECODE_LEN); //2015-11-25 針對語音中繼的修改
        }
      }
    }
    else
    {
      sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='0' channo='0' result='%d' outchantype='%d' outchanno='%d' seatno='%s' workerno='%d' groupno='%d' queueno='%d' serialno='%s' param='%s' errorbuf='%s'/>", 
        pAcd->Session.SessionNo, pAcd->Session.CmdAddr, result, outchntype, outchnno, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAcd->ACDRule.AcdedGroupNo, queueno, pAgent->AcceptSerialNo.C_Str(), pAgent->AcceptParam.C_Str(), error);
    }
  }
	SendMsg2XML(MSG_oncallseat, pAcd->Session.SessionNo, SendMsgBuf);
}
void SendCallseatResult1(int nChn, int result, const char *error)
{
  int outchntype, outchnno;
  CAgent *pAgent;
  
  if (pBoxchn->isnChnAvail(nChn))
  {
    pBoxchn->GetLgChBynChn(nChn, outchntype, outchnno);
    pAgent = GetAgentBynChn(nChn);
    if (pAgent == NULL)
    {
      sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' seatno='%s' workerno='%d' groupno='%d' queueno='0' serialno='' param='' errorbuf='%s'/>", 
        pChn->TranSessionNo, pChn->TranCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, outchntype, outchnno, "0", 0, 0, error);
    }
    else
    {
      sprintf(SendMsgBuf, "<oncallseat sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' outchantype='%d' outchanno='%d' seatno='%s' workerno='%d' groupno='%d' queueno='0' serialno='' param='' errorbuf='%s'/>", 
        pChn->TranSessionNo, pChn->TranCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, outchntype, outchnno, pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(), pAgent->GetGroupNo(), error);
    }
    
    SendMsg2XML(MSG_oncallseat, pChn->TranSessionNo, SendMsgBuf);
  }
}
//發送撥號器呼出結果
void SendDialoutResult(int nChn, int result)
{
  sprintf(SendMsgBuf, "<ondialout sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf=''/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result);
  SendMsg2XML(MSG_ondialout, pChn->SessionNo, SendMsgBuf);
}
//發送調用指定流程事件
void SendCallFlwEvent(int calltype, int num, int point)
{
  UL SessionNo;
  CAgent *pAgent=NULL;
  int nChn1, nOut, nAcd, nAG;
  int workerno=0, seattype=0;
  char seatno[32];

  if (calltype == 1)
  {
    nOut = num;
    nChn1 = pOut->Calleds[point].OutnChn;
    if (pBoxchn->isnChnAvail(nChn1))
    {

      nAG = pChn1->nAG;
      strcpy(seatno, "0");
      if (pAgentMng->isnAGAvail(nAG))
      {
        workerno = pAG->GetWorkerNo();
        seattype = pAG->GetSeatType();
        strcpy(seatno, pAG->GetSeatNo().C_Str());
      }

      sprintf(SendMsgBuf, "<oncallflw funcno='%d' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcalledno='%s' orgcallerno='%s' inrouteno='0' chhwtype='%d' chhwindex='%d' seattype='%d' seatno='%s' workerno='%d' serialno='%ld' param='%s'/>", 
        pOut->Session.FuncNo, pChn1->lgChnType, pChn1->lgChnNo, 
        pOut->CallerNo.C_Str(), pOut->Calleds[point].CalledNo, 
        pChn1->OrgCalledNo.C_Str(), pChn1->OrgCallerNo.C_Str(), 
        pChn1->ChStyle, pChn1->ChStyleIndex, 
        seattype, seatno, workerno,
        pOut->Session.DialSerialNo, pOut->Session.Param.C_Str());
	    SessionNo = 0x02000000 | ((pOut->Session.VxmlId << 16) & 0x00FF0000);
      SendMsg2XML(MSG_oncallflw, SessionNo, SendMsgBuf);
    }
  }
  else
  {
    nAcd = num;
    pAgent = pAgentMng->m_Agent[pAcd->Calleds[point].nAG];

    nChn1 = pAgent->GetSeatnChn();
    if (pBoxchn->isnChnAvail(nChn1))
    {
      sprintf(SendMsgBuf, "<oncallflw funcno='%d' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcalledno='%s' orgcallerno='%s' inrouteno='0' chhwtype='%d' chhwindex='%d' seattype='%d' seatno='%s' workerno='%d' serialno='%ld' param='%s'/>", 
        pAcd->Session.FuncNo, pChn1->lgChnType, pChn1->lgChnNo, 
        pAcd->Session.CallerNo.C_Str(), pAgent->GetPhoneNo().C_Str(), 
        pChn1->OrgCalledNo.C_Str(), pChn1->OrgCallerNo.C_Str(), 
        pChn1->ChStyle, pChn1->ChStyleIndex, 
        pAgent->GetSeatType(), pAgent->GetSeatNo().C_Str(), pAgent->GetWorkerNo(),
        pAcd->Session.DialSerialNo, pAcd->Session.Param.C_Str());
	    SessionNo = 0x02000000 | ((pAcd->Session.VxmlId << 16) & 0x00FF0000);
      SendMsg2XML(MSG_oncallflw, SessionNo, SendMsgBuf);
    }
  }
}
//發送檢測信號音結果
void SendCheckToneResult(int nChn, int result, int tonetype, const char *error)
{
  sprintf(SendMsgBuf, "<onchecktone sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' tonetype='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CheckToneCmdAddr, pChn->lgChnType, pChn->lgChnNo, result, tonetype, error);
	SendMsg2XML(MSG_onchecktone, pChn->SessionNo, SendMsgBuf);
}
//檢查序列號是否與當前通道設置的序列是否相同 //Edit 2007-08-09 add
bool CheckTheSameSessionId(VXML_XML_HEADER_STRUCT *XMLHeader, int nChn, int msgtype)
{
  if (!pBoxchn->isnChnAvail(nChn)) return true;
  if (pChn->SessionNo != XMLHeader->SessionId)
  {
    char szTemp[256];
    sprintf(szTemp, "Pre SessionNo=%ld Now Recv SessionNo=%ld is not same", pChn->SessionNo, XMLHeader->SessionId);
    switch (msgtype)
    {
      case 1: //通用事件
        Proc_MSG_common_Result(XMLHeader, OnFail, szTemp);
        break;
      case 3: //放音收碼事件
        Proc_MSG_play_Result(XMLHeader, OnPlayError, "", szTemp);
        break;
      case 5: //會議錄音事件
        Proc_MSG_recordcfc_Result(XMLHeader, OnRecordError, 0, szTemp);
        break;
      case 6: //錄音事件
        Proc_MSG_record_Result(XMLHeader, OnRecordError, szTemp);
        break;
      case 7: //呼出事件
        Proc_MSG_callout_Result(XMLHeader, OnOutFail, szTemp);
        break;
      case 8: //坐席分配事件
        Proc_MSG_callseat_Result(XMLHeader, OnACDFail, szTemp);
        break;
      case 9: //接收傳真事件
        Proc_MSG_Recvfax_Result(XMLHeader, OnrFaxFail, szTemp);
        break;
      case 10: //發送傳真事件
        Proc_MSG_Sendfax_Result(XMLHeader, OnsFaxFail, szTemp);
        break;
      case 11: //檢測信號音事件
        Proc_MSG_checktone_Result(XMLHeader, OnFail, 0, szTemp);
        break;
    }
    return false;
  }
  return true;
}
void SendSendFSKResult(int nChn, int result, const char *error)
{
  if (!pBoxchn->isnChnAvail(nChn)) return;
  sprintf(SendMsgBuf, "<onsendfsk sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, error);
  SendMsg2XML(MSG_onsendfsk, pChn->SessionNo, SendMsgBuf);
}
void SendRecvdFSKResult(int nChn, short FSKCMD, const char *FSKData, short FSKLen, int result, const char *error)
{
  if (!pBoxchn->isnChnAvail(nChn)) return;
  sprintf(SendMsgBuf, "<onrecvfsk sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' fskcmd='%d' fskdata='%s' datalen='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, FSKCMD, FSKData, FSKLen, error);
  SendMsg2XML(MSG_onrecvfsk, pChn->SessionNo, SendMsgBuf);
}
void SendTransferIVRResult(int nChn, int result, const char *error)
{
  sprintf(SendMsgBuf, "<ontransferivr sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, error);
  SendMsg2XML(MSG_ontransferivr, pChn->SessionNo, SendMsgBuf);
}
void SendgetseatlogindataResult(int nChn, int nAG, int result, const char *error)
{
  if (pAgentMng->isnAGAvail(nAG))
    sprintf(SendMsgBuf, "<ongetseatlogindata sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' seatno='%s' workerno='%d' workername='%s' groupno='%d' disturbid='%d' leaveid='%d' dutyno='%d' accountno='%s' logtime='%s' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, 
    pAG->GetSeatNo().C_Str(), pAG->GetWorkerNo(), pAG->GetWorkerName().C_Str(), 
    pAG->GetGroupNo(), pAG->m_Worker.DisturbId, pAG->m_Worker.LeaveId,
    pAG->m_Worker.DutyNo, pAG->m_Worker.AccountNo.C_Str(), time_t2str(pAG->m_Worker.LoginTime), 
    result, error);
  else
    sprintf(SendMsgBuf, "<ongetseatlogindata sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' seatno='0' workerno='0' workername='' groupno='0' disturbid='0' leaveid='0' dutyno='' accountno='' logtime='' result='%d' errorbuf='%s'/>", 
    pChn->SessionNo, pChn->CmdAddr, pChn->lgChnType, pChn->lgChnNo, result, error);

  SendMsg2XML(MSG_ongetseatlogindata, pChn->SessionNo, SendMsgBuf);
}
//---------------------XML消息處理---------------------------------------------
int nChnn; //當前處理消息的通道序號

int Proc_MSG_login(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	UL sessionid;
	int nodetype, nodeno, logid;

  SendAllLoginWorkerstatus();
	sessionid = atol(AttrData->AttrValue[0]);
	
	if (RMsgFifo->Parse_Msg(pFlwRule, pFlwRule->XMLIVRMsgRule, AttrData) != 0)
	{
		sprintf(SendMsgBuf, "<onlogin sessionid='%ld' nodetype='0' nodeno='0' maxtrunk='2' maxseat='2' cardtype='0' result='1' errorbuf='recv msg param error'/>", 
			sessionid);
		SendMsg2XML(MSG_onlogin, sessionid, SendMsgBuf);
    return 1;
	}
	
	nodetype = atoi(AttrData->AttrValue[1]);
	nodeno = atoi(AttrData->AttrValue[2]);
  logid = atoi(AttrData->AttrValue[5]);

	if (logid == 1)
  {
    if (nodeno < MAX_NODE_XML)
      pParser->SetLogin(nodeno);
  }
  else
  {
    if (nodeno < MAX_NODE_XML)
    {
      pParser->SetLogout(nodeno);
      pParser->DelPreCodeForVxmlLogout(nodeno);
      //SendPreCodesEvent();
    }
  }
  sprintf(SendMsgBuf, "<onlogin sessionid='%ld' nodetype='%d' nodeno='%d' maxtrunk='%d' maxseat='%d' cardtype='%d' result='0' errorbuf='' switchtype='%d' dbtype='%s'/>", 
    sessionid, nodetype, nodeno, AuthMaxTrunk, AuthMaxSeat, AuthCardType, g_nSwitchType, g_szDBType);
  SendMsg2XML(MSG_onlogin, sessionid, SendMsgBuf);
  SendRouteIdelTrkNum(true);
  
  sprintf(SendMsgBuf, "<onnodestate sessionid='%ld' nodetype='%d' nodeno='%d' state='%d'/>", 
    sessionid, 15, 1, ((RECSystemLogId==true)?1:0));
  SendMsg2XML(MSG_onnodestate, sessionid, SendMsgBuf);

  return 0;
}
int Proc_MSG_setaccessno(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	UL sessionid;
	int settype, minlen, maxlen;
  short VxmlNo;

	sessionid = atol(AttrData->AttrValue[0]);
  VxmlNo = (short)((sessionid >> 16) & 0x000000FF);
	if (RMsgFifo->Parse_Msg(pFlwRule, pFlwRule->XMLIVRMsgRule, AttrData) != 0)
	{
    Proc_MSG_setaccessno_Result(sessionid, 0, "", OnFail, "recv msg param error");
		return 1;
	}

	settype = atoi(AttrData->AttrValue[1]);
	minlen = atoi(AttrData->AttrValue[3]);
	maxlen = atoi(AttrData->AttrValue[4]);
	if (settype == 1)
	{
		if (pParser->AddPreCode(VxmlNo, AttrData->AttrValue[2], minlen, maxlen) == true)
		{
      Proc_MSG_setaccessno_Result(sessionid, settype, AttrData->AttrValue[2], OnSuccess, "");
		}
		else
		{
      Proc_MSG_setaccessno_Result(sessionid, settype, AttrData->AttrValue[2], OnFail, "add access code error");
		}
	}
	else
	{
		if (pParser->DelPreCode(VxmlNo, AttrData->AttrValue[2], minlen, maxlen) == true)
		{
      Proc_MSG_setaccessno_Result(sessionid, settype, AttrData->AttrValue[2], OnSuccess, "");
		}
		else
		{
      Proc_MSG_setaccessno_Result(sessionid, settype, AttrData->AttrValue[2], OnFail, "del access code error");
		}
	}
  //theApp.m_MyParVxmlDialog->DispAllAccCode();
	return 0;
}
int Proc_MSG_answer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int answertype, nAG;
	
  if (!pBoxchn->isnChnAvail(nChnn)) return 1;

	XMLHeader.OnMsgId = MSG_onanswer;
	strcpy(XMLHeader.OnMsgName, "onanswer");

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  //Edit 2007-08-09 add
  answertype = atoi(AttrData->AttrValue[4]);
  if (answertype == 9)
  {
    pChnn->SessionNo = XMLHeader.SessionId;
    return 0;
  }

  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (pChnn->ssState == CHN_SNG_IN_TALK 
    || pChnn->ssState == CHN_SNG_OT_TALK)
  {
	  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
		Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    nAG = pChnn->nAG;
//     if (pAgentMng->isnAGAvail(nAG)) //edit 2010-12-29 del
//     {
//       SetAgentsvState(nAG, AG_SV_CONN);
//     }
    return 0;
  }

  if (pChnn->ssState != CHN_SNG_IN_RING)
	{
		//Proc_MSG_common_Result(&XMLHeader, OnFail, "chn not in ringing");
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
		return 0;
	}
  int nResult = SendACK(nChnn, answertype);
	if (nResult == 9)
	{
    return 0;
	}
  else if (nResult > 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "answer fail");
    return 1;
  }
	Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  pChnn->CanPlayOrRecId = 1;
  pChnn->AnsTime = time(0);
  pChnn->ssState = CHN_SNG_IN_TALK;
  pChnn->IVRStartTime = time(0);
  //DBUpdateCallCDR_AnsTime(nChnn); //2015-08-17針對O2O去掉了
  DispChnStatus(nChnn);
  
//   nAG = pChnn->nAG;
//   if (pAgentMng->isnAGAvail(nAG)) //edit 2010-12-29 del
//   {
//     SetAgentsvState(nAG, AG_SV_CONN);
//   }
  
	return 0;
}
int Proc_MSG_sendsignal(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int signal;
	
	XMLHeader.OnMsgId = MSG_onsendsignal;
	strcpy(XMLHeader.OnMsgName, "onsendsignal");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

// 	if (pChnn->ssState != CHN_SNG_IN_WAIT) //2016-08-11增加掛機原因
// 	{
// 		Proc_MSG_common_Result(&XMLHeader, OnFail, "chn not in ringing");
// 		return 1;
// 	}
	signal = atoi(AttrData->AttrValue[5]);
	//if (pChnn->ChType == CHTYPE_DTNO1 || pChnn->ChType == CHTYPE_DTNO7 
  //  || pChnn->ChType == CHTYPE_DTNO7ISUP || pChnn->ChType == CHTYPE_DTDSS1)
  if (pChnn->ChStyle == CH_E_SS1 || pChnn->ChStyle == CH_E_TUP 
    || pChnn->ChStyle == CH_E_ISDN_U || pChnn->ChStyle == CH_E_ISDN_N 
    || pChnn->ChStyle == CH_E_ISUP || pChnn->ChStyle == CH_S_VOIP
    || pChnn->ChStyle == CH_D_VOIP)
	{
    //2016-08-11增加掛機原因
    if (signal == ACM)
    {
      if (SendACM(nChnn, signal) != 0)
      {
        Proc_MSG_common_Result(&XMLHeader, OnFail, "send signal fail");
        return 1;
      }
    }
    else if (signal == REL)
    {
      pChnn->HangonReason = atoi(AttrData->AttrValue[6]);
    }
	}
  else
  {
		Proc_MSG_common_Result(&XMLHeader, OnSuccess, "the chntype not support");
    return 1;
  }
	Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  pChnn->ssState = CHN_SNG_IN_RING;
  
  return 0;
}
int Proc_MSG_hangon(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	CAgent *pAgent=NULL;

  XMLHeader.OnMsgId = MSG_onrecvevent;
	strcpy(XMLHeader.OnMsgName, "onrecvevent");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		//Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or not online");
    MyTrace(1, "chn=%d is error or not online", nChnn);
		return 1;
	}

  if (!pBoxchn->isnChnAvail(nChnn)) 
  {
    MyTrace(1, "chn=%d is invalid", nChnn);
    return 1;
  }

  if (pChnn->VOC_TrunkId == 0 && (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE))
  {
    Release_Chn(nChnn, 1, 0);
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    return 0;
  }
  if (pChnn->ssState == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    return 0;
  }

	StopClearPlayDtmfRecBuf(nChnn);

  //斷開已連接的通道
  ClearTransferLink(nChnn);
	UnLink_All_By_ChnNo(nChnn, 1);
  pAgent = GetAgentBynChn(nChnn);
	
  if (pAgent == NULL)
  {
    if (Release(nChnn, pChnn->HangonReason) != 0) //2016-08-11
	  {
		  Proc_MSG_common_Result(&XMLHeader, OnFail, "hangon fail");
		  return 1;
	  }
  }
  else 
  {
    if (!pAgent->isWantSeatType(SEATTYPE_AG_PC) && !pAgent->isWantSeatType(SEATTYPE_RT_TEL))
    {
      if (Release(nChnn, 0) != 0)
	    {
		    Proc_MSG_common_Result(&XMLHeader, OnFail, "hangon fail");
		    return 1;
	    }
    }
  }
	Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	
  if (pAgent != NULL)
  {
    pAgent->delaytimer = 0;
    if (pAgent->svState == AG_SV_CONN || pAgent->svState == AG_SV_HOLD)
    {
      DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
      pAgent->DelayState = 1;
      WriteSeatStatus(pAgent, AG_STATUS_ACW, 0);
    }
    else
    {
      DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
      DBUpdateCallCDR_ACWTime(pAgent->nAG);
      WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
    }
    pAgent->SetAgentsvState(AG_SV_IDEL);
    pAgent->ClearSrvLine();
    
    //if (pAgent->isWantSeatType(SEATTYPE_RT_TEL))
    //{
    //  pChnn->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
    //  pAgent->m_Seat.nChn = 0xFFFF;
    //}

    if (g_nCardType != 9 && pAgent->isWantSeatType(SEATTYPE_AG_PC) || pAgent->isWantSeatType(SEATTYPE_RT_TEL))
    {
      PlayTone(nChnn, 903, 0); //純電腦坐席釋放時放短暫忙音
      SendSeatHangon(pAgent->nAG);
      DispAgentStatus(pAgent->nAG);
      return 0;
    }
    SendSeatHangon(pAgent->nAG);
    DispAgentStatus(pAgent->nAG);
    /*
    //2015-06-14
    if (pAgent->m_Worker.TelLogInOutFlag == 1)
    {
      MyTrace(3, "-Proc_MSG_hangon nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
        nChnn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
      pAgent->DelayState = 0;
      pAgent->m_Worker.TelLogInOutFlag = 0;
      pAgent->ChangeStatus(AG_STATUS_LOGIN);
      WriteWorkerLoginStatus(pAgent);
      WriteSeatStatus(pAgent, AG_STATUS_LOGIN, 0);
    }
    else if (pAgent->m_Worker.TelLogInOutFlag == 2)
    {
      MyTrace(3, "--Proc_MSG_hangon nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
        nChnn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
      pAgent->DelayState = 0;
      pAgent->m_Worker.TelLogInOutFlag = 0;
      WriteSeatStatus(pAgent, AG_STATUS_LOGOUT, 0);
      AgentStatusCount();
      SendOneGroupStatusMeteCount(pAgent->nAG);
      SendSystemStatusMeteCount();
    }
    else
    {
      AgentStatusCount();
      SendOneGroupStatusMeteCount(pAgent->nAG);
      SendSystemStatusMeteCount();
    }*/
  }

  DBUpdateCallCDR_RelTime(nChnn);
  int lnState = pChnn->lnState;
  pBoxchn->ResetChn(nChnn);
  if (g_nSwitchType == 0 && pChnn->ChStyle == CH_A_SEAT && GetSeatTypeBynChn(nChnn) != SEATTYPE_AG_PC)
  {
    if (g_nCardType != 9 && lnState == CHN_LN_SEIZE)
    {
      pChnn->lnState = CHN_LN_WAIT_REL;
      if (pAgent != NULL)
      {
        SetAgentsvState(pAgent, AG_SV_WAIT_REL);
      }
    }
  }
  DispChnStatus(nChnn);
  
  return 0;
}
int Proc_MSG_dtmfrule(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int ruleid, num;
  CStringX ArrString[3];
	
	XMLHeader.OnMsgId = MSG_ondtmfrule;
	strcpy(XMLHeader.OnMsgName, "ondtmfrule");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 0) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	ruleid = atoi(AttrData->AttrValue[4]);
	if (ruleid >= MAX_DTMFRULE_BUF)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "dtmfrule out of range");
		return 1;
	}
	pChnn->DtmfRule[ruleid].state = 1;
	pChnn->DtmfRule[ruleid].ruleid = ruleid;
	pChnn->DtmfRule[ruleid].digits.Empty();
	pChnn->DtmfRule[ruleid].digits = AttrData->AttrValue[5];
	pChnn->DtmfRule[ruleid].breakid = atoi(AttrData->AttrValue[6]);
	pChnn->DtmfRule[ruleid].transtype = atoi(AttrData->AttrValue[7]);
	pChnn->DtmfRule[ruleid].minlength = atoi(AttrData->AttrValue[8]);
	pChnn->DtmfRule[ruleid].maxlength = atoi(AttrData->AttrValue[9]);
	pChnn->DtmfRule[ruleid].timeout = atoi(AttrData->AttrValue[10]);
  pChnn->DtmfRule[ruleid].firstdigitdelay = 0;
	
  pChnn->DtmfRule[ruleid].termdigits.Empty();
  pChnn->DtmfRule[ruleid].onebitdigits.Empty();
  pChnn->DtmfRule[ruleid].pbxpredigits.Empty();

  num = SplitString(AttrData->AttrValue[11], ';', 3, ArrString);
  if (num == 1)
  {
    pChnn->DtmfRule[ruleid].termdigits = ArrString[0];
  }
  else if (num == 2)
  {
    pChnn->DtmfRule[ruleid].termdigits = ArrString[0];
    pChnn->DtmfRule[ruleid].onebitdigits = ArrString[1];
  } 
  else if (num == 3)
  {
    pChnn->DtmfRule[ruleid].termdigits = ArrString[0];
    pChnn->DtmfRule[ruleid].onebitdigits = ArrString[1];
    pChnn->DtmfRule[ruleid].pbxpredigits = ArrString[2];
    pChnn->DtmfRule[ruleid].firstdigitdelay = 3;
  } 
	
  pChnn->DtmfRule[ruleid].termid = atoi(AttrData->AttrValue[12]);
	return 0;
}
int Proc_MSG_getdtmf(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int dtmfruleid;
	
	XMLHeader.OnMsgId = MSG_ongetdtmf;
	strcpy(XMLHeader.OnMsgName, "ongetdtmf");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 0) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	dtmfruleid = atoi(AttrData->AttrValue[4]);
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	if (SetDTMFRule(nChnn, dtmfruleid, ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		ResetDtmfRule(nChnn);
    return 1;
	}
	return 0;
}
int Proc_MSG_playrule(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int ruleid, nFormat;
	
	XMLHeader.OnMsgId = MSG_onplayrule;
	strcpy(XMLHeader.OnMsgName, "onplayrule");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 0) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	ruleid = atoi(AttrData->AttrValue[4]);
	if (ruleid >= MAX_DTMFRULE_BUF)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "playrule out of range");
		return 1;
	}
	pChnn->PlayRule[ruleid].state = 1;
	pChnn->PlayRule[ruleid].ruleid = ruleid;

	nFormat = atoi(AttrData->AttrValue[5]);
	pChnn->PlayRule[ruleid].format = nFormat;
	pChnn->PlayRule[ruleid].repeat = atoi(AttrData->AttrValue[6]);
	pChnn->PlayRule[ruleid].duration = atoi(AttrData->AttrValue[7]);
	pChnn->PlayRule[ruleid].broadcast = atoi(AttrData->AttrValue[8]);
	pChnn->PlayRule[ruleid].volume = atoi(AttrData->AttrValue[9]);
	pChnn->PlayRule[ruleid].speech = atoi(AttrData->AttrValue[10]);
	pChnn->PlayRule[ruleid].voice = atoi(AttrData->AttrValue[11]);
	pChnn->PlayRule[ruleid].pause = atoi(AttrData->AttrValue[12]);
	pChnn->PlayRule[ruleid].error = atoi(AttrData->AttrValue[13]);
  pChnn->curSpeech = pChnn->PlayRule[ruleid].speech;
	return 0;
}
int Proc_MSG_asrrule(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	/*int ruleid;
	
	XMLHeader.OnMsgId = MSG_onasrrule;
	strcpy(XMLHeader.OnMsgName, "onasrrule");

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	ruleid = atoi(AttrData->AttrValue[4]);
	if (ruleid >= MAX_DTMFRULE_BUF)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "語音識別規則號超出范圍");
		return 1;
	}
	pChnn->AsrRule[ruleid].state = 1;
	pChnn->AsrRule[ruleid].ruleid = ruleid;
	pChnn->AsrRule[ruleid].grammar = AttrData->AttrValue[5];*/
	return 0;
}
int Proc_MSG_setvolume(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int volume;
  int SetvolumeId=1;
	
	XMLHeader.OnMsgId = MSG_onsetvolume;
	strcpy(XMLHeader.OnMsgName, "onsetvolume");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	if (strcmp(AttrData->AttrValue[4], "0") == 0)
  {
    volume = 8;
  }
  else if (strcmp(AttrData->AttrValue[4], "-") == 0)
  {
    if (pChnn->Volume > 0 )
      volume = pChnn->Volume - 1;
    else
      volume = 0;
  }
  else if (strcmp(AttrData->AttrValue[4], "+") == 0)
  {
    if (pChnn->Volume < 15 )
      volume = pChnn->Volume + 1;
    else
      volume = 15;
  }
  else
  {
    volume = atoi(AttrData->AttrValue[4]);
    if (volume > 15 || volume < 0)
      volume = 8;
  }
  //設置放音音量
  if (SetPlayVolume(nChnn, volume) == 0)
    pChnn->Volume = volume;

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
//-------------------放音操作類消息----------------------------------------------
int Proc_MSG_playfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	CStringX filepath;
  int DtmfRuleId;
	int PlayRuleId;
	int AsrRuleId;
	int StartPos;
  long size;
	int Length;
	
	XMLHeader.OnMsgId = MSG_onplayfile;
	strcpy(XMLHeader.OnMsgName, "onplayfile");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;


	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }
  DtmfRuleId = atoi(AttrData->AttrValue[4]);
	PlayRuleId = atoi(AttrData->AttrValue[5]);
	AsrRuleId = atoi(AttrData->AttrValue[6]);
	StartPos = atoi(AttrData->AttrValue[8]);
	Length = atoi(AttrData->AttrValue[9]);

	StopClearPlayDtmfBuf(nChnn);

	SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onplayfile, "onplayfile");

  filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[7]);
  if (MyCheckFileExist(filepath.C_Str()) == 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "vocfile not exist");
		return 1;
  }
	size = MyCheckFileSize(filepath.C_Str());
  size = size * StartPos / 100;
  if (AddSingleFileToPlayBuf(nChnn, filepath.C_Str(), size, Length, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);

	  StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
	if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
	pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
 	if (Start_ChnPlay(nChnn) != 0)
	{
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_setplaypos(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int pos;
	
	XMLHeader.OnMsgId = MSG_onsetplaypos;
	strcpy(XMLHeader.OnMsgName, "onsetplaypos");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	pos = atoi(AttrData->AttrValue[4]);

	SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_playfiles(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, PlayRuleId, AsrRuleId;
	
	
	XMLHeader.OnMsgId = MSG_onplayfiles;
	strcpy(XMLHeader.OnMsgName, "onplayfiles");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }
	DtmfRuleId = atoi(AttrData->AttrValue[4]);
	PlayRuleId = atoi(AttrData->AttrValue[5]);
	AsrRuleId = atoi(AttrData->AttrValue[6]);

  StopClearPlayDtmfBuf(nChnn);

	if (strlen(AttrData->AttrValue[7]) == 0 
    && strlen(AttrData->AttrValue[8]) == 0 
    && strlen(AttrData->AttrValue[9]) == 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "filename is null");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onplayfiles, "onplayfiles");

	//設置收碼放音規則
	if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}

  if (strlen(AttrData->AttrValue[7]) > 0)
	{
		AddFileToPlayBuf(nChnn, AttrData->AttrValue[7], ErrMsg);
	}
	
	if (strlen(AttrData->AttrValue[8]) > 0)
	{
		AddFileToPlayBuf(nChnn, AttrData->AttrValue[8], ErrMsg);
	}
	
	if (strlen(AttrData->AttrValue[9]) > 0)
	{
		AddFileToPlayBuf(nChnn, AttrData->AttrValue[9], ErrMsg);
	}

	pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
	if (Start_ChnPlay(nChnn) != 0)
	{
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_playcompose(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, PlayRuleId, AsrRuleId, comptype;
	int nFormat, nSpeech, nResult = 0, nMGYear=0, nAMPMHour=0, nMGYYYY, nMG;
  char szParamStr[MAX_VAR_DATA_LEN], szTemp[256], szTemp1[256];
	
	XMLHeader.OnMsgId = MSG_onplaycompose;
	strcpy(XMLHeader.OnMsgName, "onplaycompose");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }
	DtmfRuleId = atoi(AttrData->AttrValue[4]);
	PlayRuleId = atoi(AttrData->AttrValue[5]);
	AsrRuleId = atoi(AttrData->AttrValue[6]);
	comptype = atoi(AttrData->AttrValue[7]);

  StopClearPlayDtmfBuf(nChnn);

	if (strlen(AttrData->AttrValue[8]) == 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "no data for play");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onplaycompose, "onplaycompose");

	//設置收碼放音規則
	if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
	nFormat = pChnn->PlayRule[PlayRuleId].format;
  nSpeech = pChnn->PlayRule[PlayRuleId].speech;

  if (comptype == 2)
  {
    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strncpy(szTemp, AttrData->AttrValue[8], 21);
    strcpy(szTemp, MyUpper(szTemp));

    if (strncmp(szTemp, "DATETIME=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][9]);
      FormatDateTimeStr(szTemp, szParamStr, nMGYear);
    }
    else if (strncmp(szTemp, "DATE=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][11]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 21)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][11], 10);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
    {
      nMGYear = 1; //臺灣民國讀法

      memset(szTemp, 0, 256);
      memset(szTemp1, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[8][10], 20);
      int len = strlen(szTemp);
      int k=0, id=0, l=0, y=0, m=0, d=0;
      char c;
      bool bErr=false;
      if (len >= 6 && len <= 9)
      {
        for (int j=0; j<=len; j++)
        {
          c = szTemp[j];
          if (id == 0)
          {
            //年
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              y++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (y == 2)
              {
                szTemp1[3] = '-';
                szTemp1[2] = szTemp1[1];
                szTemp1[1] = szTemp1[0];
                szTemp1[0] = '0';
                k = 4;
                id = 1;
              }
              else if (y == 3)
              {
                szTemp1[k] = '-';
                k++;
                id = 1;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 1)
          {
            //月
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              m++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (m == 1)
              {
                szTemp1[6] = '-';
                szTemp1[5] = szTemp1[4];
                szTemp1[4] = '0';
                k = 7;
                id = 2;
              }
              else if (m == 2)
              {
                szTemp1[k] = '-';
                k++;
                id = 2;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 2)
          {
            //日
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              d++;
            }
            else if (c == '\0')
            {
              if (d == 1)
              {
                szTemp1[8] = szTemp1[7];
                szTemp1[7] = '0';
                k = 9;
                id = 3;
              }
              else if (d == 2)
              {
                k++;
                id = 3;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
        }
      }

      if (strlen(szTemp1) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, szTemp1, 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strncat(szTemp, &szTemp1[3], 6);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 15)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][8], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMM=", 6) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 11)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][6], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MG=", 3) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][3], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-MM-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
      else if (strlen(szTemp) > 6)
      {
        FormatDateTimeStr(&AttrData->AttrValue[8][3], szParamStr, nMGYear);
        nMGYear = 1;
      }
    }
    else if (strncmp(szTemp, "MMDD=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[8][5], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][7], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "TIME=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "AMPM=", 5) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HHMISS=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMISS=", 7) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "HHMI=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMI=", 5) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 5);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][12]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 11);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][9], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "_");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, "YYYY-");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, ":SS_W");
      }
    }
    else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 8);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 8);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "DD_HH=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 5);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 2);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "W=", 2) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][2]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
      strncat(szParamStr, &szTemp1[20], 1);
    }
    else if (strncmp(szTemp, "HH:MI=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "AP:MI=", 6) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MI:SS=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 5);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HH=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "AP=", 3) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "MI=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 2);
      strcat(szParamStr, ":SS_W");
    }
    else
    {
      FormatDateTimeStr(AttrData->AttrValue[8], szParamStr, nMGYear);
    }
    nResult = AddMixToPlayBuf(nSpeech, nChnn, comptype, szParamStr, ErrMsg, nMGYear, nAMPMHour);
  }
  else
  {
    nResult = AddMixToPlayBuf(nSpeech, nChnn, comptype, AttrData->AttrValue[8], ErrMsg);
  }
	if (nResult == 1)
  {
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
  }
	pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
	if (Start_ChnPlay(nChnn) != 0)
	{
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_multiplay(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, PlayRuleId, AsrRuleId, mixtype1, mixtype2, mixtype3;
	int nSpeech, nResult = 0, nMGYear=0, nAMPMHour=0, nMGYYYY, nMG;
  char szParamStr[MAX_VAR_DATA_LEN], szTemp[256], szTemp1[256];
	
	XMLHeader.OnMsgId = MSG_onmultiplay;
	strcpy(XMLHeader.OnMsgName, "onmultiplay");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }
	DtmfRuleId = atoi(AttrData->AttrValue[4]);
	PlayRuleId = atoi(AttrData->AttrValue[5]);
	AsrRuleId = atoi(AttrData->AttrValue[6]);
	mixtype1 = atoi(AttrData->AttrValue[7]);
	mixtype2 = atoi(AttrData->AttrValue[9]);
	mixtype3 = atoi(AttrData->AttrValue[11]);

  StopClearPlayDtmfBuf(nChnn);

	if (strlen(AttrData->AttrValue[8]) == 0 
    && strlen(AttrData->AttrValue[10]) == 0 
    && strlen(AttrData->AttrValue[12]) == 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "no data for play");
		return 1;
	}

	SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onmultiplay, "onmultiplay");

	//設置收碼放音規則
	if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
  nSpeech = pChnn->PlayRule[PlayRuleId].speech;
  //合成串一
  if (mixtype1 == 2)
  {
    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strncpy(szTemp, AttrData->AttrValue[8], 21);
    strcpy(szTemp, MyUpper(szTemp));

    if (strncmp(szTemp, "DATETIME=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][9]);
      FormatDateTimeStr(szTemp, szParamStr, nMGYear);
    }
    else if (strncmp(szTemp, "DATE=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][11]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 21)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][11], 10);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
    {
      nMGYear = 1; //臺灣民國讀法

      memset(szTemp, 0, 256);
      memset(szTemp1, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[8][10], 20);
      int len = strlen(szTemp);
      int k=0, id=0, l=0, y=0, m=0, d=0;
      char c;
      bool bErr=false;
      if (len >= 6 && len <= 9)
      {
        for (int j=0; j<=len; j++)
        {
          c = szTemp[j];
          if (id == 0)
          {
            //年
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              y++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (y == 2)
              {
                szTemp1[3] = '-';
                szTemp1[2] = szTemp1[1];
                szTemp1[1] = szTemp1[0];
                szTemp1[0] = '0';
                k = 4;
                id = 1;
              }
              else if (y == 3)
              {
                szTemp1[k] = '-';
                k++;
                id = 1;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 1)
          {
            //月
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              m++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (m == 1)
              {
                szTemp1[6] = '-';
                szTemp1[5] = szTemp1[4];
                szTemp1[4] = '0';
                k = 7;
                id = 2;
              }
              else if (m == 2)
              {
                szTemp1[k] = '-';
                k++;
                id = 2;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 2)
          {
            //日
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              d++;
            }
            else if (c == '\0')
            {
              if (d == 1)
              {
                szTemp1[8] = szTemp1[7];
                szTemp1[7] = '0';
                k = 9;
                id = 3;
              }
              else if (d == 2)
              {
                k++;
                id = 3;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
        }
      }

      if (strlen(szTemp1) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, szTemp1, 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strncat(szTemp, &szTemp1[3], 6);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 15)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][8], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMM=", 6) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 11)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][6], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MG=", 3) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][3], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-MM-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
      else if (strlen(szTemp) > 6)
      {
        FormatDateTimeStr(&AttrData->AttrValue[8][3], szParamStr, nMGYear);
        nMGYear = 1;
      }
    }
    else if (strncmp(szTemp, "MMDD=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[8][5], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][7], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "TIME=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "AMPM=", 5) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HHMISS=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMISS=", 7) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "HHMI=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMI=", 5) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 5);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][12]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 11);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[8][9], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[8][11], 2);
        strcat(szTemp, "_");
        strncat(szTemp, &AttrData->AttrValue[8][13], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[8][15], 2);
        
        strcpy(szParamStr, "YYYY-");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, ":SS_W");
      }
    }
    else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 8);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 8);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "DD_HH=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 5);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 2);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "W=", 2) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][2]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
      strncat(szParamStr, &szTemp1[20], 1);
    }
    else if (strncmp(szTemp, "HH:MI=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "AP:MI=", 6) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MI:SS=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 5);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HH=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "AP=", 3) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "MI=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[8][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 2);
      strcat(szParamStr, ":SS_W");
    }
    else
    {
      FormatDateTimeStr(AttrData->AttrValue[8], szParamStr, nMGYear);
    }
    nResult = AddMixToPlayBuf(nSpeech, nChnn, mixtype1, szParamStr, ErrMsg, nMGYear, nAMPMHour);
  }
  else
  {
    nResult = AddMixToPlayBuf(nSpeech, nChnn, mixtype1, AttrData->AttrValue[8], ErrMsg);
  }
  
  //合成串二
  if (mixtype2 == 2)
  {
    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strncpy(szTemp, AttrData->AttrValue[10], 21);
    strcpy(szTemp, MyUpper(szTemp));

    if (strncmp(szTemp, "DATETIME=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][9]);
      FormatDateTimeStr(szTemp, szParamStr, nMGYear);
    }
    else if (strncmp(szTemp, "DATE=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][11]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 21)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][11], 10);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
    {
      nMGYear = 1; //臺灣民國讀法

      memset(szTemp, 0, 256);
      memset(szTemp1, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[10][10], 20);
      int len = strlen(szTemp);
      int k=0, id=0, l=0, y=0, m=0, d=0;
      char c;
      bool bErr=false;
      if (len >= 6 && len <= 9)
      {
        for (int j=0; j<=len; j++)
        {
          c = szTemp[j];
          if (id == 0)
          {
            //年
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              y++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (y == 2)
              {
                szTemp1[3] = '-';
                szTemp1[2] = szTemp1[1];
                szTemp1[1] = szTemp1[0];
                szTemp1[0] = '0';
                k = 4;
                id = 1;
              }
              else if (y == 3)
              {
                szTemp1[k] = '-';
                k++;
                id = 1;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 1)
          {
            //月
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              m++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (m == 1)
              {
                szTemp1[6] = '-';
                szTemp1[5] = szTemp1[4];
                szTemp1[4] = '0';
                k = 7;
                id = 2;
              }
              else if (m == 2)
              {
                szTemp1[k] = '-';
                k++;
                id = 2;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 2)
          {
            //日
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              d++;
            }
            else if (c == '\0')
            {
              if (d == 1)
              {
                szTemp1[8] = szTemp1[7];
                szTemp1[7] = '0';
                k = 9;
                id = 3;
              }
              else if (d == 2)
              {
                k++;
                id = 3;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
        }
      }

      if (strlen(szTemp1) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, szTemp1, 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strncat(szTemp, &szTemp1[3], 6);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 15)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][8], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][13], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMM=", 6) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 11)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][6], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MG=", 3) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][3], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-MM-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
      else if (strlen(szTemp) > 6)
      {
        FormatDateTimeStr(&AttrData->AttrValue[10][3], szParamStr, nMGYear);
        nMGYear = 1;
      }
    }
    else if (strncmp(szTemp, "MMDD=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[10][5], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][7], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[10][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "TIME=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "AMPM=", 5) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HHMISS=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMISS=", 7) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "HHMI=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMI=", 5) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 5);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][12]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 11);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[10][9], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[10][11], 2);
        strcat(szTemp, "_");
        strncat(szTemp, &AttrData->AttrValue[10][13], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[10][15], 2);
        
        strcpy(szParamStr, "YYYY-");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, ":SS_W");
      }
    }
    else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 8);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 8);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "DD_HH=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 5);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 2);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "W=", 2) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][2]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
      strncat(szParamStr, &szTemp1[20], 1);
    }
    else if (strncmp(szTemp, "HH:MI=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "AP:MI=", 6) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MI:SS=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 5);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HH=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "AP=", 3) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "MI=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[10][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 2);
      strcat(szParamStr, ":SS_W");
    }
    else
    {
      FormatDateTimeStr(AttrData->AttrValue[10], szParamStr, nMGYear);
    }
    nResult = AddMixToPlayBuf(nSpeech, nChnn, mixtype2, szParamStr, ErrMsg, nMGYear);
  }
  else
  {
    nResult = AddMixToPlayBuf(nSpeech, nChnn, mixtype2, AttrData->AttrValue[10], ErrMsg);
  }
  //合成串三
  if (mixtype3 == 2)
  {
    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strncpy(szTemp, AttrData->AttrValue[10], 21);
    strcpy(szTemp, MyUpper(szTemp));

    if (strncmp(szTemp, "DATETIME=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][9]);
      FormatDateTimeStr(szTemp, szParamStr, nMGYear);
    }
    else if (strncmp(szTemp, "DATE=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][11]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 21)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][11], 10);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
    {
      nMGYear = 1; //臺灣民國讀法

      memset(szTemp, 0, 256);
      memset(szTemp1, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[12][10], 20);
      int len = strlen(szTemp);
      int k=0, id=0, l=0, y=0, m=0, d=0;
      char c;
      bool bErr=false;
      if (len >= 6 && len <= 9)
      {
        for (int j=0; j<=len; j++)
        {
          c = szTemp[j];
          if (id == 0)
          {
            //年
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              y++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (y == 2)
              {
                szTemp1[3] = '-';
                szTemp1[2] = szTemp1[1];
                szTemp1[1] = szTemp1[0];
                szTemp1[0] = '0';
                k = 4;
                id = 1;
              }
              else if (y == 3)
              {
                szTemp1[k] = '-';
                k++;
                id = 1;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 1)
          {
            //月
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              m++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (m == 1)
              {
                szTemp1[6] = '-';
                szTemp1[5] = szTemp1[4];
                szTemp1[4] = '0';
                k = 7;
                id = 2;
              }
              else if (m == 2)
              {
                szTemp1[k] = '-';
                k++;
                id = 2;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 2)
          {
            //日
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              d++;
            }
            else if (c == '\0')
            {
              if (d == 1)
              {
                szTemp1[8] = szTemp1[7];
                szTemp1[7] = '0';
                k = 9;
                id = 3;
              }
              else if (d == 2)
              {
                k++;
                id = 3;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
        }
      }

      if (strlen(szTemp1) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, szTemp1, 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strncat(szTemp, &szTemp1[3], 6);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 15)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][8], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][13], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMM=", 6) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 11)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][6], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MG=", 3) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][3], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-MM-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
      else if (strlen(szTemp) > 6)
      {
        FormatDateTimeStr(&AttrData->AttrValue[12][3], szParamStr, nMGYear);
        nMGYear = 1;
      }
    }
    else if (strncmp(szTemp, "MMDD=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[12][5], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][7], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[12][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "TIME=", 5) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "AMPM=", 5) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HHMISS=", 7) == 0)
    {
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMISS=", 7) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "HHMI=", 5) == 0)
    {
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMI=", 5) == 0)
    {
      nAMPMHour = 1;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 5);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][12]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 11);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
    {
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[12][9], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[12][11], 2);
        strcat(szTemp, "_");
        strncat(szTemp, &AttrData->AttrValue[12][13], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[12][15], 2);
        
        strcpy(szParamStr, "YYYY-");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, ":SS_W");
      }
    }
    else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 8);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 8);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "DD_HH=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 5);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 2);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "W=", 2) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][2]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
      strncat(szParamStr, &szTemp1[20], 1);
    }
    else if (strncmp(szTemp, "HH:MI=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "AP:MI=", 6) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MI:SS=", 6) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 5);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HH=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "AP=", 3) == 0)
    {
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "MI=", 3) == 0)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[12][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 2);
      strcat(szParamStr, ":SS_W");
    }
    else
    {
      FormatDateTimeStr(AttrData->AttrValue[12], szParamStr, nMGYear);
    }
    nResult = AddMixToPlayBuf(nSpeech, nChnn, mixtype3, szParamStr, ErrMsg, nMGYear);
  }
  else
  {
    nResult = AddMixToPlayBuf(nSpeech, nChnn, mixtype3, AttrData->AttrValue[12], ErrMsg);
  }

  if (nResult == 1)
  {
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
  }
	pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
	if (Start_ChnPlay(nChnn) != 0)
	{
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_ttsstring(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, PlayRuleId, AsrRuleId;
  VXML_TTSSTRING_STRUCT stru_TTSString;
	
	XMLHeader.OnMsgId = MSG_onttsstring;
	strcpy(XMLHeader.OnMsgName, "onttsstring");

  if (AuthMaxTTS == 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "tts authdata is error");
    return 1;
  }
  if (!pBoxchn->isnChnAvail(nChnn))
  {
    if (strlen(AttrData->AttrValue[12]) > 0)
    {
      if (TTSLogId == false)
      {
        //TTS代理服務器未登錄
        Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "TTSProxy not login");
        return 1;
      }
      stru_TTSString.ttsStream = 1; //返回語音流方式: 1-文件 2-數據流
      
      stru_TTSString.SessionId = XMLHeader.SessionId;
      stru_TTSString.CmdAddr = XMLHeader.CmdAddr;
      stru_TTSString.ChnType = 0;
      stru_TTSString.ChnNo = 0;
      
      stru_TTSString.DtmfRuleId = 0;
      stru_TTSString.PlayRuleId = 0;
      stru_TTSString.AsrRuleId = 0;
      
      strcpy(stru_TTSString.ttsType, AttrData->AttrValue[7]);
      strcpy(stru_TTSString.ttsGrammar, AttrData->AttrValue[8]);
      strcpy(stru_TTSString.ttsParam, AttrData->AttrValue[9]);
      
      stru_TTSString.ttsFormat = atoi(AttrData->AttrValue[10]);
      
      CStringX filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[11]);
      strcpy(stru_TTSString.ttsFileName, filepath.C_Str());
      
      strcpy(stru_TTSString.ttsString, AttrData->AttrValue[12]);
      
      pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsstring,
        (UC *)&stru_TTSString, sizeof(VXML_TTSSTRING_STRUCT));
      return 0;
    }

    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "string is null");
    return 1;
  }
  
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

  if (strlen(AttrData->AttrValue[12]) == 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "string is null");
    return 1;
  }

  if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "the chn is talkwith or in conf");
    return 1;
  }
  
  DtmfRuleId = atoi(AttrData->AttrValue[4]);
  PlayRuleId = atoi(AttrData->AttrValue[5]);
  AsrRuleId = atoi(AttrData->AttrValue[6]);

  if (PlayRuleId > 0)
  {
    StopClearPlayDtmfBuf(nChnn);
    
    SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
    SetcurPlaySessionParam(nChnn, MSG_onttsstring, "onttsstring");
    
    if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
    {
      Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
      StopClearPlayDtmfBuf(nChnn);
      return 1;
    }
  }

  if (TTSLogId == false)
  {
		//TTS代理服務器未登錄
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "TTSProxy not login");
    if (PlayRuleId > 0)
      StopClearPlayDtmfBuf(nChnn);
		return 1;
  }
	pChnn->curPlayData.WaitTTSResult = 1;
  pChnn->WaitTTSTimer = 0;
  stru_TTSString.ttsStream = 1; //返回語音流方式: 1-文件 2-數據流

  stru_TTSString.SessionId = XMLHeader.SessionId;
  stru_TTSString.CmdAddr = XMLHeader.CmdAddr;
  stru_TTSString.ChnType = XMLHeader.ChnType;
  stru_TTSString.ChnNo = XMLHeader.ChnNo;
  
  stru_TTSString.DtmfRuleId = DtmfRuleId;
  stru_TTSString.PlayRuleId = PlayRuleId;
  stru_TTSString.AsrRuleId = AsrRuleId;
  
  strcpy(stru_TTSString.ttsType, AttrData->AttrValue[7]);
  strcpy(stru_TTSString.ttsGrammar, AttrData->AttrValue[8]);
  strcpy(stru_TTSString.ttsParam, AttrData->AttrValue[9]);

  stru_TTSString.ttsFormat = atoi(AttrData->AttrValue[10]);
  
  CStringX filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[11]);
  strcpy(stru_TTSString.ttsFileName, filepath.C_Str());
  
  strcpy(stru_TTSString.ttsString, AttrData->AttrValue[12]);

  pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsstring,
    (UC *)&stru_TTSString, sizeof(VXML_TTSSTRING_STRUCT));
  return 0;
}
int Proc_MSG_ttsfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, PlayRuleId, AsrRuleId;
  VXML_TTSFILE_STRUCT stru_TTSFile;

  XMLHeader.OnMsgId = MSG_onttsfile;
	strcpy(XMLHeader.OnMsgName, "onttsfile");

  if (AuthMaxTTS == 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "tts authdata is error");
    return 1;
  }
  if (!pBoxchn->isnChnAvail(nChnn))
  {
    if (strlen(AttrData->AttrValue[12]) > 0)
    {
      if (TTSLogId == false)
      {
        //TTS代理服務器未登錄
        Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "TTSProxy not login");
        return 1;
      }
      stru_TTSFile.ttsStream = 1; //返回語音流方式: 1-文件 2-數據流
      
      stru_TTSFile.SessionId = XMLHeader.SessionId;
      stru_TTSFile.CmdAddr = XMLHeader.CmdAddr;
      stru_TTSFile.ChnType = 0;
      stru_TTSFile.ChnNo = 0;
      
      stru_TTSFile.DtmfRuleId = 0;
      stru_TTSFile.PlayRuleId = 0; //0-表示是合成到語音文件不直接播放
      stru_TTSFile.AsrRuleId = 0;
      
      strcpy(stru_TTSFile.ttsType, AttrData->AttrValue[7]);
      strcpy(stru_TTSFile.ttsGrammar, AttrData->AttrValue[8]);
      strcpy(stru_TTSFile.ttsParam, AttrData->AttrValue[9]);
      
      stru_TTSFile.ttsFormat = atoi(AttrData->AttrValue[10]);
      
      CStringX filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[11]);
      strcpy(stru_TTSFile.ttsFileName, filepath.C_Str());
      
      filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[12]);
      strcpy(stru_TTSFile.FileName, filepath.C_Str());
      
      pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsfile,
        (UC *)&stru_TTSFile, sizeof(VXML_TTSFILE_STRUCT));
      return 0;
    }
    
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "filename is null");
    return 1;
  }

  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "the chn is talkwith or in conf");
    return 1;
  }
	DtmfRuleId = atoi(AttrData->AttrValue[4]);
	PlayRuleId = atoi(AttrData->AttrValue[5]);
	AsrRuleId = atoi(AttrData->AttrValue[6]);

  if (DtmfRuleId > 0 && PlayRuleId > 0)
  {
    StopClearPlayDtmfBuf(nChnn);
    
    SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
    SetcurPlaySessionParam(nChnn, MSG_onttsfile, "onttsfile");
    
    if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
    {
      Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", ErrMsg);
      StopClearPlayDtmfBuf(nChnn);
      return 1;
    }
  }
  
  if (TTSLogId == false)
  {
		Proc_MSG_play_Result(&XMLHeader, OnTTSError, "", "TTSProxy not login");
    if (PlayRuleId > 0)
      StopClearPlayDtmfBuf(nChnn);
		return 1;
  }
	pChnn->curPlayData.WaitTTSResult = 1;
  pChnn->WaitTTSTimer = 0;
  stru_TTSFile.ttsStream = 1; //返回語音流方式: 1-文件 2-數據流

  stru_TTSFile.SessionId = XMLHeader.SessionId;
  stru_TTSFile.CmdAddr = XMLHeader.CmdAddr;
  stru_TTSFile.ChnType = XMLHeader.ChnType;
  stru_TTSFile.ChnNo = XMLHeader.ChnNo;

  stru_TTSFile.DtmfRuleId = DtmfRuleId;
  stru_TTSFile.PlayRuleId = PlayRuleId; //0-表示是合成到語音文件不直接播放
  stru_TTSFile.AsrRuleId = AsrRuleId;
  
  strcpy(stru_TTSFile.ttsType, AttrData->AttrValue[7]);
  strcpy(stru_TTSFile.ttsGrammar, AttrData->AttrValue[8]);
  strcpy(stru_TTSFile.ttsParam, AttrData->AttrValue[9]);
  
  stru_TTSFile.ttsFormat = atoi(AttrData->AttrValue[10]);
  
  CStringX filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[11]);
  strcpy(stru_TTSFile.ttsFileName, filepath.C_Str());
  
  filepath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[12]);
  strcpy(stru_TTSFile.FileName, filepath.C_Str());

  pTcpLink->SendMessage2CLIENT(TTSClientId, (MSGTYPE_IVRTTS<<8)|TTSMSG_ttsfile,
    (UC *)&stru_TTSFile, sizeof(VXML_TTSFILE_STRUCT));
  return 0;
}
int Proc_MSG_senddtmf(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  XMLHeader.OnMsgId = MSG_onsenddtmf;
	strcpy(XMLHeader.OnMsgName, "onsenddtmf");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;
  
  StopClearPlayDtmfBuf(nChnn);

	SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onsenddtmf, "onsenddtmf");

	if (AddDTMFStrToPlayBuf(nChnn, AttrData->AttrValue[6], ErrMsg) != 0)
	{
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
	if (SetDTMFPlayRule(nChnn, 0, 0, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "set dtmfrule fail");
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
	pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
	if (Start_ChnPlay(nChnn) != 0)
	{
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_sendfsk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int FskHeader, FskCmd, FskDataLen, CRCType;

  XMLHeader.OnMsgId = MSG_onsendfsk;
	strcpy(XMLHeader.OnMsgName, "onsendfsk");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  StopClearPlayDtmfBuf(nChnn);
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  FskHeader = atoi(AttrData->AttrValue[4]);
  FskCmd = atoi(AttrData->AttrValue[5]);
  FskDataLen = atoi(AttrData->AttrValue[7]);
  CRCType = atoi(AttrData->AttrValue[8]);

  if (FskDataLen == 0)
  {
    FskDataLen = strlen(AttrData->AttrValue[6]);
  }
  if (SendFSKStr(nChnn, FskHeader, FskCmd, AttrData->AttrValue[6], FskDataLen, CRCType) != 0)
  {
    SendSendFSKResult(nChnn, OnFail, "send fsk fail");
  }
  return 0;
}
int Proc_MSG_playcfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int PlayRuleId, nCfc;
	
	XMLHeader.OnMsgId = MSG_onplaycfc;
	strcpy(XMLHeader.OnMsgName, "onplaycfc");

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	PlayRuleId = atoi(AttrData->AttrValue[4]);
	nCfc = atoi(AttrData->AttrValue[5]);

  if (!pBoxconf->isnCfcAvail(nCfc))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "confno is out of range");
    return 1;
  }
  if (pCfc->state == 0 || pCfc->CreateId != 3 || pCfc->UseId != 1)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the conf is not open");
    return 1;
  }

	if (strlen(AttrData->AttrValue[6]) == 0 
    && strlen(AttrData->AttrValue[7]) == 0 
    && strlen(AttrData->AttrValue[8]) == 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "filename is null");
		return 1;
	}

	//設置收碼放音規則
  if (SetConfPlayRule(nCfc, nChnn, PlayRuleId, ErrMsg) != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "set conf playrule fail");
  }

  if (strlen(AttrData->AttrValue[6]) > 0)
	{
    AddFileToConfPlayBuf(nCfc, AttrData->AttrValue[6], ErrMsg);
	}
	
	if (strlen(AttrData->AttrValue[7]) > 0)
	{
		AddFileToConfPlayBuf(nCfc, AttrData->AttrValue[7], ErrMsg);
	}
	
	if (strlen(AttrData->AttrValue[8]) > 0)
	{
		AddFileToConfPlayBuf(nCfc, AttrData->AttrValue[8], ErrMsg);
	}

  pCfc->curPlayData.PlayState = 1;
	pCfc->PlayedTimer = 0;
	if (Start_ConfPlay(nCfc) != 0)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "start confplay fail");
		//清除放音緩沖區
		Stop_Play_Clear_ConfBuf(nCfc);
		return 1;
	}
	pCfc->curPlayData.CanPlayId = 1;
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	
	return 0;
}
int Proc_MSG_playtone(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, tonetype, repeat, release;
	
	XMLHeader.OnMsgId = MSG_onplaytone;
	strcpy(XMLHeader.OnMsgName, "onplaytone");
  //if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
  //  return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }
	DtmfRuleId = atoi(AttrData->AttrValue[4]);
	tonetype = atoi(AttrData->AttrValue[5]);
	repeat = atoi(AttrData->AttrValue[6]);
	release = atoi(AttrData->AttrValue[7]);

  StopClearPlayDtmfBuf(nChnn);

	SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onplaytone, "onplaytone");

	//設置收碼放音規則
	if (AddToneToPlayBuf(nChnn, tonetype, DtmfRuleId, repeat, release, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}
	
  pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
	if (Start_ChnPlay(nChnn) != 0)
	{
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_recordfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  CStringX FilePath;
	int beep, maxtime, format, writemode;
  char errorbuf[128];
		
	XMLHeader.OnMsgId = MSG_onrecordfile;
	strcpy(XMLHeader.OnMsgName, "onrecordfile");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 6) == false)
    return 1;

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_record_Result(&XMLHeader, OnRecordError, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_record_Result(&XMLHeader, OnRecordError, "chn is error or not online");
		return 1;
	}
	beep = atoi(AttrData->AttrValue[5]);
	maxtime = atoi(AttrData->AttrValue[6]);
	format = atoi(AttrData->AttrValue[8]);
	writemode = atoi(AttrData->AttrValue[9]);

	SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearRecDtmfBuf(nChnn);

	if (strlen(AttrData->AttrValue[4]) == 0)
	{
		Proc_MSG_record_Result(&XMLHeader, OnRecordError, "record filename is null");
		return 1;
	}
  FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, AttrData->AttrValue[4]);
  
  if (CreateFullPathIfNotExist(FilePath.C_Str()) !=0)
  {
    Proc_MSG_record_Result(&XMLHeader, OnRecordError, "create path fail");
    return 1;
  }

	pChnn->curRecData.SessionNo = XMLHeader.SessionId;
	pChnn->curRecData.CmdAddr = XMLHeader.CmdAddr;
  pChnn->curRecData.lgChnType = pChnn->lgChnType;
  pChnn->curRecData.lgChnNo = pChnn->lgChnNo;
	//設置錄音規則
	if ((pChnn->curPlayData.PlayState != 0 || maxtime == 0) && beep == 0)
  {
    if (SetRPparam(nChnn, 0) == 0)
      pChnn->RecMixerFlag = 1;
  }
  strcpy(errorbuf, "set record rule fail");
  if (SetChnRecordRule(nChnn, beep, FilePath.C_Str(), 
    AttrData->AttrValue[7], format, maxtime, writemode, errorbuf) != 0)
	{
		Proc_MSG_record_Result(&XMLHeader, OnRecordError, errorbuf);
    StopClearRecDtmfBuf(nChnn);
		return 1;
	}
	return 0;
}
int Proc_MSG_recordcfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	CStringX FilePath;
  int nCfc, maxtime, format, writemode;

	XMLHeader.OnMsgId = MSG_onrecordcfc;
	strcpy(XMLHeader.OnMsgName, "onrecordcfc");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 5) == false)
    return 1;

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_recordcfc_Result(&XMLHeader, OnRecordError, 0, "recv msg param error");
		return 1;
	}
  nCfc = atoi(AttrData->AttrValue[4]);
  maxtime = atoi(AttrData->AttrValue[6]);
  format = atoi(AttrData->AttrValue[7]);
  writemode = atoi(AttrData->AttrValue[8]);
  if (!pBoxconf->isnCfcAvail(nCfc))
  {
		Proc_MSG_recordcfc_Result(&XMLHeader, OnRecordError, 0, "the confno is out of range");
		return 1;
  }
  pCfc->curRecData.SessionNo = 0;
	pCfc->curRecData.CmdAddr = 0;
  pCfc->curRecData.lgChnType = 0;
  pCfc->curRecData.lgChnNo = 0;
  
  FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, AttrData->AttrValue[5]);
  
  if (CreateFullPathIfNotExist(FilePath.C_Str()) !=0)
  {
    Proc_MSG_recordcfc_Result(&XMLHeader, OnRecordError, 0, "create path fail");
    return 1;
  }

  //設置錄音規則
	if (SetConfRecordRule(nCfc, FilePath.C_Str(), format, maxtime, writemode) != 0)
	{
		Stop_Rec_Clear_ConfBuf(nCfc);
    Proc_MSG_recordcfc_Result(&XMLHeader, OnRecordError, 0, "conf record fail");
    return 1;
	}
  Proc_MSG_recordcfc_Result(&XMLHeader, OnRecording, 0, "");
  return 0;
}
int Proc_MSG_stop(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onstop;
	strcpy(XMLHeader.OnMsgName, "onstop");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
	
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearPlayDtmfRecBuf(nChnn);

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  
	return 0;
}
//-------------------呼叫操作類消息----------------------------------------------
int Proc_MSG_callout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int i, serialno, callertype, calltype, waittime, routeno, vxmlid, funcno, times, outchntype, outchnno, callednum, callmode, callpoint, callinterval, calledtype;
	int nOut, ChIndex, RouteNum, OutnChn=0xFFFF;
  UC ChType;
  UL dialserialno;
  CStringX ArrString[MAX_CALLED_NUM];
  CStringX RouteString[MAX_CALLED_NUM];
	
	XMLHeader.OnMsgId = MSG_oncallout;
	strcpy(XMLHeader.OnMsgName, "oncallout");

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_callout_Result(&XMLHeader, OnOutFail, "recv msg param error");
		return 1;
	}

  RouteNum = SplitTxtLine(AttrData->AttrValue[10], MAX_CALLED_NUM, RouteString);
  if (RouteNum == 0)
  {
    RouteNum = 1;
    routeno = 0;
    RouteString[0] = "0";
  }
  else
  {
    routeno = atoi(RouteString[0].C_Str());
  }
  //根據路由查詢呼出中繼
  if (routeno > pBoxchn->MaxRouteNum)
  {
    routeno = 0;
  }
  
	if (pBoxchn->isnChnAvail(nChnn))
  {
    //已經通了，再轉接
    SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
    if (g_nSwitchMode > 0 && pBoxchn->Routes[routeno].VOCTrunkId == 0)
    {
      pChnn->TranIVRId = 4;
      pChnn->TranSessionNo = XMLHeader.SessionId;
      pChnn->TranCmdAddr = XMLHeader.CmdAddr;
      Send_SWTMSG_transfercall(nChnn, pChnn->DeviceID, 0, 0, AttrData->AttrValue[8], 1, "", 1);
      Proc_MSG_callout_Result(&XMLHeader, OnOutCalling, "");
      return 0;
    }
  }
  else
  {
    //流程自動發起呼出方式
    if (g_nSwitchMode > 0 && (g_nSwitchMode == 13 || pBoxchn->Routes[routeno].VOCTrunkId == 0))
    {
      int nChn;
      if (g_nSwitchMode == 13)
        nChn = pBoxchn->Get_IdelIVRChnNo();
      else
        nChn = GetIVRChn(routeno, ""); //2016-04-21 增加IVR路由判斷
      if (pBoxchn->isnChnAvail(nChn))
      {
        pChn->lnState = CHN_LN_SEIZE;
        pChn->TranIVRId = 5;
        pChn->TranSessionNo = XMLHeader.SessionId;
        pChn->TranCmdAddr = XMLHeader.CmdAddr;
        pChn->SessionNo = XMLHeader.SessionId;
        pChn->CmdAddr = XMLHeader.CmdAddr;

        //2015-12-28 增加等待計時器清0
        pChn->CalloutTimer = 0;
        pChn->TranWaitTime = atoi(AttrData->AttrValue[9]);
        MyTrace(3, "Proc_MSG_callout TranIVRId=5 TranWaitTime=%d", pChn->TranWaitTime);
        
        if (pIVRCfg->isHangoffOnIVRCallout == true) //2015-10-14 
        {
          strcpy(pChn->CallerNo, AttrData->AttrValue[7]);
          strcpy(pChn->CalledNo, AttrData->AttrValue[8]);
          pChn->CallData = AttrData->AttrValue[15];
          Callout(nChn, 0, 0, "", "", "");
        }
        else
        {
          Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, AttrData->AttrValue[7], AttrData->AttrValue[8], atoi(AttrData->AttrValue[10]), AttrData->AttrValue[15], 1);
        }

        Proc_MSG_callout_Result(&XMLHeader, OnOutCalling, "");
        DispChnStatus(nChn);
      }
      else
      {
        Proc_MSG_callout_Result(&XMLHeader, OnOutFail, "no idel ivr chn");
      }
      return 0;
    }
  }

	serialno = atoi(AttrData->AttrValue[4]);
	callertype = atoi(AttrData->AttrValue[5]);
  calltype = atoi(AttrData->AttrValue[6]);
	waittime = atoi(AttrData->AttrValue[9]);
  
  vxmlid = atoi(AttrData->AttrValue[11]);
	funcno = atoi(AttrData->AttrValue[12]);
	times = atoi(AttrData->AttrValue[13]);
  if (times <= 0)
    times = 1;
	dialserialno = atol(AttrData->AttrValue[14]);
  outchntype = atoi(AttrData->AttrValue[16]);
  outchnno = atoi(AttrData->AttrValue[17]);
  callednum = atoi(AttrData->AttrValue[18]);
  callmode = atoi(AttrData->AttrValue[18]);
  callpoint = atoi(AttrData->AttrValue[19]);
  callinterval = atoi(AttrData->AttrValue[20]);

	if (outchntype != 0 && outchnno >= 0)
  {
    OutnChn = pBoxchn->Get_IdelChnNo_By_LgChn(outchntype, outchnno, ChType, ChIndex);
    if (!pBoxchn->isnChnAvail(OutnChn) && (routeno == 0 || outchntype == 2))
    {
      Proc_MSG_callout_Result(&XMLHeader, OnOutBusy, "the specified chn is busy");
		  return 1;
    }
  }
	if (strlen(AttrData->AttrValue[7]) < 3)
	{
		strcpy(AttrData->AttrValue[7], pIVRCfg->CenterCode);
	}
	if (waittime < 4)
	{
		waittime = 4;
	}
  //該標志號是否已有呼出
  nOut = pCallbuf->Get_nOut_By_DialSerialNo(dialserialno);
  if (pCallbuf->isnOutAvail(nOut))
  {
    Proc_MSG_callout_Result(&XMLHeader, OnOutFail, "the callout serialno is calling");
		return 1;
  }
  nOut = pCallbuf->Get_Idle_nOut();
  if (!pCallbuf->isnOutAvail(nOut))
  {
    Proc_MSG_callout_Result(&XMLHeader, OnOutFail, "callout buf is overflow");
		return 1;
  }
	//設置呼出數據
  pOut->state = 1;
  pOut->timer = callinterval;
  pOut->CallStep = 0;

  pOut->RingTime = waittime;
  pOut->IntervalTime = callinterval;

  pOut->Session.SessionNo = XMLHeader.SessionId;
  pOut->Session.CmdAddr = XMLHeader.CmdAddr;
  pOut->Session.nChn = nChnn;
  pOut->Session.DialSerialNo = dialserialno;
  pOut->Session.SerialNo = serialno;
  pOut->Session.CmdId = 1;
  pOut->Session.VxmlId = vxmlid;
  pOut->Session.FuncNo = funcno;
  pOut->Session.Param = AttrData->AttrValue[15];
  if (pBoxchn->isnChnAvail(nChnn))
    pOut->Session.CallerNo = pChnn->CustPhone;
  else
    pOut->Session.CallerNo = AttrData->AttrValue[7];

	pOut->CallerNo = AttrData->AttrValue[7];
  pOut->CallerType = callertype;
  pOut->CallType = calltype;
  pOut->CallSeatType = 1;
  pOut->CancelId = 0;

	pOut->CallMode = callmode;
	pOut->CallTimes = times;
  pOut->CalledTimes = 0;
  pOut->StartPoint = callpoint;
	pOut->CallPoint = callpoint;
	pOut->CallTime = 0;
  
  pOut->TranMode = 0;
  pOut->TranCallFailReturn = 0;
  pOut->nAG = 0xFFFF;

	if (outchntype != 0 && outchnno >= 0)
  {
    pOut->OutnChn = pBoxchn->Get_IdelChnNo_By_LgChn(outchntype, outchnno, ChType, ChIndex);
  }
  else
  {
    pOut->OutnChn = 0xFFFF;
  }

  if (strlen(AttrData->AttrValue[8]) == 0)
  {
    pOut->CalledNum = 1;
    memset(pOut->Calleds[0].CalledNo, 0, MAX_TELECODE_LEN);
    pOut->Calleds[0].CalledType = 1;
    pOut->Calleds[0].pQue_str = NULL;
    pOut->Calleds[0].CalloutResult = 0;
    pOut->Calleds[0].OutnChn = 0xFFFF;
    pOut->Calleds[0].RouteNo = routeno;
  }
  else
  {
    pOut->CalledNum = SplitString(AttrData->AttrValue[8], ';', MAX_CALLED_NUM, ArrString);
    for (i=0; i<pOut->CalledNum; i++)
    {
      memset(pOut->Calleds[i].CalledNo, 0, MAX_TELECODE_LEN);
      strncpy(pOut->Calleds[i].CalledNo, ArrString[i].C_Str(), MAX_TELECODE_LEN-1);
      pOut->Calleds[i].CalledType = 1;
      pOut->Calleds[i].pQue_str = NULL;
      pOut->Calleds[i].CalloutResult = 0;
      pOut->Calleds[i].OutnChn = 0xFFFF;
      pOut->Calleds[i].RouteNo = routeno;
    }
    if (pOut->CalledNum > 1)
    {
      pOut->RingTime = pOut->RingTime/pOut->CalledNum;
      if (pOut->RingTime < 5)
        pOut->RingTime = 5;
    }
  }
  if (pOut->CallPoint >= pOut->CalledNum)
    pOut->CallPoint = 0;
  pOut->Session.CalledNo = pOut->Calleds[0].CalledNo;

  //edit 2007-10-18 add start
  if (pOut->CalledNum ==1 && pOut->CallMode == 0)
    pOut->CallTimes = times*RouteNum;
  
  pOut->RouteNum = RouteNum;
  for (i=0; i<RouteNum; i++)
  {
    routeno = atoi(RouteString[i].C_Str());
    pOut->RouteNo[i] = routeno;
  }
  //edit 2007-10-18 add end

  for (i=1; i<RouteNum && i<pOut->CalledNum; i++)
  {
    routeno = atoi(RouteString[i].C_Str());
    pOut->Calleds[i].RouteNo = routeno;
  }
  //取被叫類型 1-外線 2-內線
  callednum = SplitTxtLine(AttrData->AttrValue[21], MAX_CALLED_NUM, ArrString);
  if (callednum == 1)
  {
    calledtype = atoi(ArrString[0].C_Str());
    for (i=0; i<pOut->CalledNum; i++)
    {
      pOut->Calleds[i].CalledType = calledtype;
    }
  }
  else
  {
    for (i=0; i<callednum; i++)
    {
      calledtype = atoi(ArrString[i].C_Str());
      pOut->Calleds[i].CalledType = calledtype;
    }
  }
  if (pBoxchn->isnChnAvail(nChnn))
  {
    pChnn->nOut = nOut;
  }

  Proc_MSG_callout_Result(&XMLHeader, OnOutCalling, "");
  CAgent *pAgent = GetAgentBynChn(nChnn);
  if (pAgent != NULL)
  {
    pAgent->m_Worker.AcdedGroupNo = pAgent->GetGroupNo();
    WriteSeatStatus(pAgent, AG_STATUS_OUTSEIZE, 0, 2, 0);
    if (pAgent->isTcpLinked())
    {
      sprintf(SendMsgBuf, "<onmakecall acdid='%lu' sessionno='%ld' chantype='0' channo='0' callerno='' calledno='' param='%s' result='%d' errorbuf=''/>", 
        pAgent->GetClientId()<<16, pOut->Session.SessionNo, pOut->Session.Param.C_Str(), OnOutCalling);
	    SendMsg2AG(pAgent->GetClientId(), AGMSG_onmakecall, SendMsgBuf);
    }
  }
	return 0;
}
int Proc_MSG_cancelcallout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  UL dialserialno;
  int nOut;
    
	XMLHeader.OnMsgId = MSG_oncancelcallout;
	strcpy(XMLHeader.OnMsgName, "oncancelcallout");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

	if (pBoxchn->isnChnAvail(nChnn))
  {
    SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
    if (g_nSwitchType != 0 && g_nSwitchType != PORT_NOCTI_PBX_TYPE)
    {
      Send_SWTMSG_stoptransfer(nChnn, pChnn->DeviceID, pChnn->ConnID);
      Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
      return 0;
    }
  }
	
	dialserialno = atol(AttrData->AttrValue[4]);
  nOut = pCallbuf->Get_nOut_By_DialSerialNo(dialserialno);
  if (!pCallbuf->isnOutAvail(nOut))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the callout serialno is not callout");
		return 1;
  }
  pOut->CancelId = 2;

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_sendsam(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  UL dialserialno;
	US nOut;
	
  XMLHeader.OnMsgId = MSG_onsendsam;
	strcpy(XMLHeader.OnMsgName, "onsendsam");

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  dialserialno = atol(AttrData->AttrValue[5]);
  nOut = pCallbuf->Get_nOut_By_DialSerialNo(dialserialno);
  if (!pCallbuf->isnOutAvail(nOut))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "not callout before");
		return 1;
  }
	
	if (strlen(AttrData->AttrValue[4]) == 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "calledno is null");
    return 0;
	}
	
  if (strlen(pOut->Calleds[0].CalledNo) + strlen(AttrData->AttrValue[4]) < MAX_TELECODE_LEN)
  {
    strcat(pOut->Calleds[0].CalledNo, AttrData->AttrValue[4]);
    if (pOut->Calleds[0].pQue_str != NULL)
      strcat(pOut->Calleds[0].pQue_str->CalledNo, AttrData->AttrValue[4]);
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_transfer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int transmode, callertype, calledtype, calltype, waittime, routeno, outchntype, outchnno;
	int nOut, ChIndex, nAG, nChn1, sourcechntype, sourcechnno;
  char CalledNo[MAX_TELECODE_LEN], szTemp[128];
  UC ChType;
  UL dialserialno;
	
	XMLHeader.OnMsgId = MSG_ontransfer;
	strcpy(XMLHeader.OnMsgName, "ontransfer");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "chn is error or not online");
		return 1;
	}
  if (!pBoxchn->isnChnAvail(nChnn))
  {
    Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "nChn is error");
    return 1;
  }

  if (pChnn->TranStatus != 0)
  {
    Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "nChn is transfering");
    return 1;
  }
  transmode = atoi(AttrData->AttrValue[6]); //轉接類型:0-板卡版本拍叉轉接 1-協商轉接 2-盲轉 3-會議轉接 4-轉接外部路由 5-流程發起坐席轉接電話指令(2016-01-17) 6-流程發起坐席停止轉接電話指令(2016-01-17)
  
  //2016-01-17
  if (transmode == 5)
  {
    sourcechntype = atoi(AttrData->AttrValue[4]);
    sourcechnno = atoi(AttrData->AttrValue[5]);
    nChn1 = pBoxchn->Get_nChn_By_LgChn(sourcechntype, sourcechnno);
    if (ProcExtnTransferCall(nChn1, AttrData->AttrValue[10], atoi(AttrData->AttrValue[8]), atoi(AttrData->AttrValue[9]), AttrData->AttrValue[11]) == 0)
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutCalling, "tranfering...");
    } 
    else
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "");
    }
    return 0;
  }
  else if (transmode == 6)
  {
    if (ProcExtnStopTransfer(AttrData->AttrValue[10]) == 0)
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutCalling, "");
    } 
    else
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "");
    }
    return 0;
  }

  if (g_nSwitchMode == 0)
  {
    if (transmode == 0)
    {
      waittime = atoi(AttrData->AttrValue[12]);
      sprintf(szTemp, "WaitTime=%d`", waittime);
      //板卡版本拍叉轉接
      if (Transfer(nChnn, atoi(AttrData->AttrValue[9]), AttrData->AttrValue[11], szTemp) == 0) //2015-11-28傳送轉接方式 當tranmode=0時：1-協商轉接，2-快速轉接
      {
        pChnn->TranIVRId = 9;
        pChnn->TranSessionNo = XMLHeader.SessionId;
        pChnn->TranCmdAddr = XMLHeader.CmdAddr;
      }
      else
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "Flash Transfer fail");
      }
      return 0;
    }
    sourcechntype = atoi(AttrData->AttrValue[4]);
    sourcechnno = atoi(AttrData->AttrValue[5]);
    if (sourcechntype == 0 && sourcechnno == 0)
    {
      nChn1 = pChnn->LinkChn[0];
      if (pChnn->LinkType[0] != 1 || !pBoxchn->isnChnAvail(nChn1)) //未處于通話狀態,就不需要轉接
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "nChn is not talking");
        return 1;
      }
    } 
    else
    {
      nChn1 = pBoxchn->Get_nChn_By_LgChn(sourcechntype, sourcechnno);
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "source nChn is invalid");
        return 1;
      }
      if (pChnn->LinkType[0] != 0 && pChnn->LinkChn[0] != nChn1) //源通道與正在通話的通道不同,就不能轉接
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "set source chn but nChn is linking");
        return 1;
      }
      if (pChn1->ssState != CHN_SNG_IN_TALK && pChn1->ssState != CHN_SNG_OT_TALK)
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "source nChn is not talking");
        return 1;
      }
    }
    if (pChn1->TranStatus != 0)
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "source nChn is transfering");
      return 1;
    }
    if (pBoxchn->isnChnAvail(pChnn->TranDestionnChn))
    {
      //已經有轉接的目的通道
      return 1;
    }
  }

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  //轉接的號碼類型:1-外線號碼 2-坐席號(判斷是否存在) 3-話務員工號 4-分機號(不判斷是否存在)
  calledtype = atoi(AttrData->AttrValue[8]); 
  memset(CalledNo, 0, MAX_TELECODE_LEN);
  if (calledtype == 2)
  {
    strncpy(CalledNo, AttrData->AttrValue[11], MAX_TELECODE_LEN-1);
  }
  else if (calledtype == 3)
  {
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(AttrData->AttrValue[11]));
    if (pAgentMng->isnAGAvail(nAG))
    {
      calledtype = 2;
      strcpy(CalledNo, pAG->m_Seat.SeatNo.C_Str());
    }
    else
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "the workerno is not exist");
      return 1;
    }
  }
  else if (calledtype == 4) //2016-07-16 增加判斷如果是監視的坐席就判斷是否空閑
  {
    nAG = pAgentMng->GetnAGByWorkerNo(atoi(AttrData->AttrValue[11]));
    if (pAgentMng->isnAGAvail(nAG))
    {
      if (!pAG->isIdelForSrv())
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "the seat is busy");
        return 1;
      }
    }
    strncpy(CalledNo, AttrData->AttrValue[11], MAX_TELECODE_LEN-1);
  }
  else
  {
    strncpy(CalledNo, AttrData->AttrValue[11], MAX_TELECODE_LEN-1);
  }
  if (calledtype == 2)
  {
    nAG = pAgentMng->GetnAGBySeatNo(CalledNo);
    if (pAgentMng->isnAGAvail(nAG))
    {
      if (!pAG->isIdelForSrv())
      {
        Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "the seat is busy");
        return 1;
      }
    }
    else
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "the seatno is not exist");
      return 1;
    }
  }

	callertype = atoi(AttrData->AttrValue[7]);
	calltype = atoi(AttrData->AttrValue[9]);

	waittime = atoi(AttrData->AttrValue[12]);
	routeno = atoi(AttrData->AttrValue[13]);
  outchntype = atoi(AttrData->AttrValue[14]);
  outchnno = atoi(AttrData->AttrValue[15]);

  dialserialno = atol(AttrData->AttrValue[18]);

  if (g_nSwitchMode == PORT_NOCTI_PBX_TYPE)
  {
    if (Transfer(nChnn, 1, CalledNo, "") == 0)
    {
      pChnn->TranIVRId = 3;
      pChnn->TranSessionNo = XMLHeader.SessionId;
      pChnn->TranCmdAddr = XMLHeader.CmdAddr;
    }
    else
    {
      Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "Flash Transfer fail");
    }
    return 0;
  }
  else if (g_nSwitchMode > 0)
  {
    char szCallData[1024];

    pChnn->TranIVRId = 3;
    if (calledtype == 2 || calledtype == 4)
      pChnn->TranAG = pAgentMng->GetnAGBySeatNo(CalledNo);
    else
      pChnn->TranAG = 0xFFFF;
    pChnn->CalloutTimer = 0;
    pChnn->TranWaitTime = waittime;
    pChnn->TranSessionNo = XMLHeader.SessionId;
    pChnn->TranCmdAddr = XMLHeader.CmdAddr;

    DBUpdateCallCDR_ACDTime(nChnn, 0, 0);
    DBUpdateCallCDR_WaitTime(nChnn);
    nAG = pChnn->TranAG;
    if (pAgentMng->isnAGAvail(nAG))
    {
      sprintf(szCallData, "$S_OrgCalledNo=%s;$S_CDRSerialNo=%s;$S_RecdFileName=%s;$S_DialParam=", pChnn->CalledNo, pChnn->CdrSerialNo, pChnn->RecordFileName); //2016-04-19增加傳遞原被叫號碼
      pAG->TranAGId = 2;
      if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
      {
        sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s",
          pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), AttrData->AttrValue[10], CalledNo);
        SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
      }
    }
    else
    {
      memset(szCallData, 0, 1024);
      DBUpdateCallCDR_WorkerNo(nChnn, CalledNo, "");
      nAG = pAgentMng->GetnAGByDutyNo(CalledNo);
      if (pAgentMng->isnAGAvail(nAG))
      {
        if (WebSocketLogId == true && strlen(pAG->m_Seat.WSClientId) > 0)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s",
            pAG->m_Seat.WSClientId, pAG->GetSeatNo().C_Str(), AttrData->AttrValue[10], CalledNo);
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
      }
    }
    //2016-03-20 轉接類型不能為0，如果是0在ctilink就會直接返回失敗
    if (transmode == 0)
      transmode = 1;

    Send_SWTMSG_transfercall(nChnn, pChnn->DeviceID, 0, 0, CalledNo, transmode, szCallData, calledtype);
    return 0;
  }

	//根據路由查詢呼出中繼
	if (routeno > pBoxchn->MaxRouteNum)
	{
		routeno = 0;
	}
	if (strlen(AttrData->AttrValue[10]) < 3)
	{
		strcpy(AttrData->AttrValue[10], pIVRCfg->CenterCode);
	}
	if (waittime < 4)
	{
		waittime = 4;
	}
  //該標志號是否已有呼出
  nOut = pCallbuf->Get_nOut_By_DialSerialNo(dialserialno);
  if (pCallbuf->isnOutAvail(nOut))
  {
    Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "the callout serialno is calling");
		return 1;
  }
  nOut = pCallbuf->Get_Idle_nOut();
  if (!pCallbuf->isnOutAvail(nOut))
  {
    Proc_MSG_transfer_Result(&XMLHeader, OnOutFail, "callout buf is overflow");
		return 1;
  }
	//設置呼出數據
  pOut->state = 1;
  pOut->timer = 0;
  pOut->CallStep = 0;
  
  pOut->RingTime = waittime;
  pOut->IntervalTime = 0;
  
  pOut->Session.SessionNo = XMLHeader.SessionId;
  pOut->Session.CmdAddr = XMLHeader.CmdAddr;
  pOut->Session.nChn = nChnn;
  pOut->Session.DialSerialNo = dialserialno;
  pOut->Session.SerialNo = 1;
  pOut->Session.CmdId = 2;
  pOut->Session.VxmlId = 1;
  pOut->Session.FuncNo = 0;
  pOut->Session.Param = AttrData->AttrValue[17];
  pOut->Session.CallerNo = pChnn->CustPhone;
  pOut->Session.CalledNo = CalledNo;
  
  pOut->CallerNo = AttrData->AttrValue[10];
  pOut->CallerType = callertype;
  pOut->CallType = calltype;
  pOut->CallSeatType = 1;
  pOut->CancelId = 0;
  
  pOut->CallMode = 0;
  pOut->CallTimes = 1;
  pOut->CalledTimes = 0;
  
  pOut->CalledNum = 1;
  strcpy(pOut->Calleds[0].CalledNo, CalledNo);
  pOut->Calleds[0].CalledType = calledtype;
  pOut->Calleds[0].pQue_str = NULL;
  pOut->Calleds[0].CalloutResult = 0;
  pOut->Calleds[0].OutnChn = 0xFFFF;
  pOut->RouteNo[0] = routeno;
  
  pOut->StartPoint = 0;
  pOut->CallPoint = 0;
  pOut->CallTime = 0;

  pOut->TranMode = transmode;
  pOut->TranCallFailReturn = 0;
  pOut->nAG = 0xFFFF;
  
  if (outchntype != 0 && outchnno >= 0)
  {
    pOut->OutnChn = pBoxchn->Get_IdelChnNo_By_LgChn(outchntype, outchnno, ChType, ChIndex);
  }
  else
  {
    pOut->OutnChn = 0xFFFF;
  }
  
  //斷開與正在的通話,并對其放等待音
  pChnn->TranStatus = 3;
  pChnn->TranControlnChn = nChn1;
  pChn1->TranStatus = 1;
  pChn1->TranControlnChn = nChnn;

  if (sourcechntype == 0 && sourcechnno == 0)
  {
    UnRouterTalk(nChnn, nChn1);
    pChnn->LinkType[0] = 0;
    pChnn->LinkChn[0] = 0xFFFF;
    pChn1->LinkType[0] = 0;
    pChn1->LinkChn[0] = 0xFFFF;
  }

  if (strlen(AttrData->AttrValue[16]) > 0)
  {
    pOut->WaitVocFile = AttrData->AttrValue[16];
  }
  else
  {
    pOut->WaitVocFile = pIVRCfg->WaitVocFile;
  }
  if (pBoxchn->isnChnAvail(nChnn))
  {
    pChnn->nOut = nOut;
  }

  PlayFile(nChnn, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);
  PlayFile(nChn1, pOut->WaitVocFile.C_Str(), 0, MAX_PLAYRULE_BUF);

  Proc_MSG_transfer_Result(&XMLHeader, OnOutCalling, "");
	return 0;
}
int Proc_MSG_callseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  CStringX ArrGroupNo[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrLevel[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrString[8];
  int nAcd, i, num;

	XMLHeader.OnMsgId = MSG_oncallseat;
	strcpy(XMLHeader.OnMsgName, "oncallseat");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_callseat_Result(&XMLHeader, OnACDFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
	  nChnn = 0xFFFF;
	}
  else
  {
    nChnn = XMLHeader.nChn;
    SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
    if (pIVRCfg->ControlCallModal == 0)
      strncpy(pChnn->CdrSerialNo, AttrData->AttrValue[23], MAX_CHAR64_LEN-1);
  }
  MyTrace(3, "Proc_MSG_callseat nChnn=%d OnlineId=%d", nChnn, XMLHeader.OnlineId);

  if (!pBoxchn->isnChnAvail(nChnn))
  {
    if (g_nSwitchMode > 0)
    {
      //交換機版本流程自動呼出分配坐席
      CACDRule ACDRule;
      ACDRule.TranCallFailCallBack = 0;
      ACDRule.AcdedCount = 0;
      ACDRule.SeatType = (UC)(atoi(AttrData->AttrValue[8]));
      ACDRule.SeatNo = AttrData->AttrValue[6];
      ACDRule.WorkerNo = atoi(AttrData->AttrValue[7]);
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
      {
        ACDRule.GroupNo[i] = 0xFF;
        ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = 0;
      }
      num = SplitTxtLine(AttrData->AttrValue[11], MAX_GROUP_NUM_FORWORKER, ArrGroupNo);
      SplitTxtLine(AttrData->AttrValue[14], MAX_GROUP_NUM_FORWORKER, ArrLevel);
      for (i = 0; i < MAX_GROUP_NUM_FORWORKER && i < num; i ++)
      {
        ACDRule.GroupNo[i] = atoi(ArrGroupNo[i].C_Str());
        if (ACDRule.GroupNo[i] >= MAX_AG_GROUP)
          ACDRule.GroupNo[i] = 0;
        ACDRule.Level[i] = atoi(ArrLevel[i].C_Str());
        if (ACDRule.Level[i] >= MAX_AG_LEVEL)
          ACDRule.Level[i] = 0;
        ACDRule.NowLevel[i] = ACDRule.Level[i];
      }
      ACDRule.AcdedGroupNo = 0;
      ACDRule.AcdedGroupIndex = 0;
      ACDRule.AcdedLevel = 0;
      ACDRule.AcdRule = (UC)atoi(AttrData->AttrValue[9]);
      ACDRule.LevelRule = (UC)atoi(AttrData->AttrValue[15]);
      ACDRule.CallMode = (UC)atoi(AttrData->AttrValue[10]);
      ACDRule.SeatGroupNo = (UC)atoi(AttrData->AttrValue[13]);
      ACDRule.WaitTimeLen = atoi(AttrData->AttrValue[16]);
      ACDRule.RingTimeLen = atoi(AttrData->AttrValue[17]);
      ACDRule.BusyWaitId = (UC)atoi(AttrData->AttrValue[20]);
      ACDRule.Priority = (UC)atoi(AttrData->AttrValue[21]);
      
      ACDRule.QueueTime = time(0);
      ACDRule.StartRingTime = time(0);
      for (i = 0; i < MAX_AG_NUM; i ++)
      {
        ACDRule.AcdednAGList[i] = -1;
      }
      CAgent *pAgent=GetIdelAgentForCallOut(ACDRule);
      if (pAgent == NULL)
      {
        Proc_MSG_callseat_Result(&XMLHeader, OnACDOUTSRV, "GetIdelAgentForCallOut fail");
        return 1;
      }
      int nChn = pAgent->m_Seat.nChn;
      if (!pBoxchn->isnChnAvail(nChn))
      {
        Proc_MSG_callseat_Result(&XMLHeader, OnACDOUTSRV, "the seat noy bangding chn fail");
        return 1;
      }
      pChn->lnState = CHN_LN_SEIZE;
      pChn->TranIVRId = 6;
      pChn->TranSessionNo = XMLHeader.SessionId;
      pChn->TranCmdAddr = XMLHeader.CmdAddr;
      pChn->SessionNo = XMLHeader.SessionId;
      pChn->CmdAddr = XMLHeader.CmdAddr;

      SetAgentsvState(pAgent->nAG, AG_SV_INSEIZE);
      Send_SWTMSG_makecall(nChn, pChn->DeviceID, 0, AttrData->AttrValue[4], AttrData->AttrValue[5], 0, AttrData->AttrValue[24], 1);
      Proc_MSG_callseat_Result(&XMLHeader, OnACDWait, "");
      DispChnStatus(nChn);
      return 0;
    }
  }
  //------------------------------------------------------
  DBUpdateCallCDR_AnsId(nChnn, 1);
  DBUpdateCallCDR_WaitTime(nChnn);

  nAcd = pACDQueue->Get_Idle_nAcd();
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    Proc_MSG_callseat_Result(&XMLHeader, OnACDFail, "acd queue buf overflow");
    return 1;
  }

  pAcd->state = 1;
  pAcd->acdState = 0;
  pAcd->waittimer = 0;
  pAcd->ringtimer = 0;
  pAcd->StopId = 0;
  pAcd->Prior = 0xFFFF;
  pAcd->Next = 0xFFFF;

  pAcd->Session.MediaType = 0;
  pAcd->Session.SessionNo = XMLHeader.SessionId;
  pAcd->Session.CmdAddr = XMLHeader.CmdAddr;
  pAcd->Session.nChn = nChnn;
  pAcd->Session.DialSerialNo = (UL)(atol(AttrData->AttrValue[25]));
  pAcd->Session.SerialNo = 0;
  pAcd->Session.CallerType = (UC)(atoi(AttrData->AttrValue[12]));
  pAcd->Session.CallType = 0;
  pAcd->Session.CallerNo = AttrData->AttrValue[4];
  pAcd->Session.CalledNo = AttrData->AttrValue[5];
  pAcd->Session.CmdId = 1; //callseat指令
  pAcd->Session.VxmlId = (UC)(atoi(AttrData->AttrValue[18]));
  pAcd->Session.FuncNo = (UC)(atoi(AttrData->AttrValue[19]));
  pAcd->Session.ServiceType = AttrData->AttrValue[22];
  num = SplitString(AttrData->AttrValue[22], ';', 2, ArrString);
  //MyTrace(3, "num=%d str1=%s str2=%s", num, ArrString[0].C_Str(), ArrString[1].C_Str());
  if (num == 1)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = 0;
    DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else if (num == 2)
  {
    pAcd->Session.SrvType = atoi(ArrString[0].C_Str());
    pAcd->Session.SrvSubType = atoi(ArrString[1].C_Str());
    DBUpdateCallCDR_SrvType(nChnn, pAcd->Session.SrvType, pAcd->Session.SrvSubType);
  }
  else
  {
    pAcd->Session.SrvType = 0;
    pAcd->Session.SrvSubType = 0;
  }
  pAcd->Session.CRDSerialNo = AttrData->AttrValue[23];
  pAcd->Session.Param = AttrData->AttrValue[24];

  pAcd->ACDRule.TranCallFailCallBack = 0;
  pAcd->ACDRule.AcdedCount = 0;
  pAcd->ACDRule.SeatType = (UC)(atoi(AttrData->AttrValue[8]));
  pAcd->ACDRule.SeatNo = AttrData->AttrValue[6];
  pAcd->ACDRule.WorkerNo = atoi(AttrData->AttrValue[7]);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = 0xFF;
    pAcd->ACDRule.Level[i] = 0;
    pAcd->ACDRule.NowLevel[i] = 0;
  }
  num = SplitTxtLine(AttrData->AttrValue[11], MAX_GROUP_NUM_FORWORKER, ArrGroupNo);
  SplitTxtLine(AttrData->AttrValue[14], MAX_GROUP_NUM_FORWORKER, ArrLevel);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER && i < num; i ++)
  {
    pAcd->ACDRule.GroupNo[i] = atoi(ArrGroupNo[i].C_Str());
    if (pAcd->ACDRule.GroupNo[i] >= MAX_AG_GROUP)
      pAcd->ACDRule.GroupNo[i] = 0;
    pAcd->ACDRule.Level[i] = atoi(ArrLevel[i].C_Str());
    if (pAcd->ACDRule.Level[i] >= MAX_AG_LEVEL)
      pAcd->ACDRule.Level[i] = 0;
    pAcd->ACDRule.NowLevel[i] = pAcd->ACDRule.Level[i];
  }
  pAcd->ACDRule.AcdedGroupNo = 0;
  pAcd->ACDRule.AcdedGroupIndex = 0;
  pAcd->ACDRule.AcdedLevel = 0;
  pAcd->ACDRule.AcdRule = (UC)atoi(AttrData->AttrValue[9]);
  pAcd->ACDRule.LevelRule = (UC)atoi(AttrData->AttrValue[15]);
  pAcd->ACDRule.CallMode = (UC)atoi(AttrData->AttrValue[10]);
  pAcd->ACDRule.SeatGroupNo = (UC)atoi(AttrData->AttrValue[13]);
  pAcd->ACDRule.WaitTimeLen = atoi(AttrData->AttrValue[16]);
  pAcd->ACDRule.RingTimeLen = atoi(AttrData->AttrValue[17]);
  pAcd->ACDRule.BusyWaitId = (UC)atoi(AttrData->AttrValue[20]);
  pAcd->ACDRule.Priority = (UC)atoi(AttrData->AttrValue[21]);

  pAcd->ACDRule.QueueTime = time(0);
  pAcd->ACDRule.StartRingTime = time(0);
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    pAcd->ACDRule.AcdednAGList[i] = -1;
  }
  pAcd->CalledNum = 0;
  for (i = 0; i < MAX_CALLED_NUM; i ++)
  {
    pAcd->Calleds[i].nAG = 0xFFFF;
    pAcd->Calleds[i].pQue_str = NULL;
    pAcd->Calleds[i].CalloutResult = 0;
  }
  if (pAcd->ACDRule.CallMode == 1)
  {
    //設置同呼同組的坐席號碼
    SetGroupSeatNo(nAcd);
  }
  pAcd->CallSeatType = 0; //來話直接呼入分配
  
  if (pBoxchn->isnChnAvail(nChnn))
  {
    //2015-2-07
    pChnn->nAcd = nAcd; 
    if (pAcd->ACDRule.BusyWaitId < 2)
    {
      for (i=0; i<MAX_AG_NUM; i++)
      {
        pChnn->GetAgentList[i] = -1;
      }
      pChnn->GetAgentAcdPoint = -1;
    }
    else if (pAcd->ACDRule.BusyWaitId == 2)
    {
      for (i=0; i<MAX_AG_NUM; i++)
      {
        pChnn->GetAgentList[i] = -1;
      }
      pChnn->GetAgentAcdPoint = -1;
      memcpy(pChnn->AcdednAGList, pAcd->ACDRule.AcdednAGList, sizeof(pChnn->AcdednAGList));
    }
    else if (pAcd->ACDRule.BusyWaitId == 3 && pChnn->GetAgentAcdPoint < 0)
    {
      if (GetAgentListByRule(&pAcd->ACDRule, nChnn) > 0)
      {
        char szTemp[1024], szTemp1[16];
        memset(szTemp, 0, 1024);
        strcpy(szTemp, "GetAgentListByRule=");
        for (i=0; i<MAX_AG_NUM; i++)
        {
          if (pChnn->GetAgentList[i] < 0)
            break;
          sprintf(szTemp1, "%d", pChnn->GetAgentList[i]);
          strcat(szTemp, szTemp1);
        }
        MyTrace(3, "%s", szTemp);
        pChnn->GetAgentAcdPoint = 0;
      }
    }

    if (pChnn->lgChnType == CHANNEL_IVR && pChnn->IncIVRInTranAGCountId == 0)
    {
      CIVRCallCountParam *pIVRCallCountParam;
      if (gIVRCallCountParamMng.GetIvrNoType == 1)
      {
        MyTrace(3, "IncIVRInTranAGCount(%s)", pChnn->CalledNo);
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGCount(pChnn->CalledNo);
      }
      else
      {
        MyTrace(3, "IncIVRInTranAGCount(%s)", pChnn->OrgCalledNo.C_Str());
        pIVRCallCountParam = gIVRCallCountParamMng.IncIVRInTranAGCount(pChnn->OrgCalledNo.C_Str()); //2016-01-08
      }
      pChnn->IncIVRInTranAGCountId = 1;
      pChnn->IVRInTranAGTime = time(0);
      SendIVRCallCountData(pIVRCallCountParam);
    }
  }

  ChCount.AcdCallCount ++;
  pACDQueue->JoinACDQueue(nAcd);
  SendACDQueueInfoToAll();
  Proc_MSG_callseat_Result(&XMLHeader, OnACDWait, "");
  WriteChnIVRStatus(nChnn, AG_STATUS_IVR);
  return 0;
}
int Proc_MSG_stopcallseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAcd;

	XMLHeader.OnMsgId = MSG_onstopcallseat;
	strcpy(XMLHeader.OnMsgName, "onstopcallseat");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}
  nAcd = pACDQueue->Get_nAcd_By_DialSerialNo(atol(AttrData->AttrValue[4]));
  if (pACDQueue->isnAcdAvail(nAcd))
    pAcd->StopId = 1;

	if (XMLHeader.OnlineId != 0)
	{
		return 0;
	}
	
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  if (!pACDQueue->isnAcdAvail(nAcd))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "not callseat before");
		return 1;
  }
  
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
//-------------------會議操作類消息----------------------------------------------
int Proc_MSG_createcfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int nResult, nCfc;
  int cfcno, presiders, talkers, listeners, totaltalks, autorecord, autofree;
	
	XMLHeader.OnMsgId = MSG_oncreatecfc;
	strcpy(XMLHeader.OnMsgName, "oncreatecfc");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

    if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_createcfc_Result(&XMLHeader, OnFail, 0, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_createcfc_Result(&XMLHeader, OnFail, 0, "chn is error or not online");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	cfcno = atoi(AttrData->AttrValue[4]);
  presiders = atoi(AttrData->AttrValue[7]);
	talkers = atoi(AttrData->AttrValue[8]);
	listeners = atoi(AttrData->AttrValue[9]);
	autorecord = atoi(AttrData->AttrValue[11]);
  autofree = atoi(AttrData->AttrValue[12]);

  if (cfcno == 0)
  {
    nCfc = pBoxconf->Get_Idle_nCfc();
    if (!pBoxconf->isnCfcAvail(nCfc))
    {
      Proc_MSG_createcfc_Result(&XMLHeader, OnFail, 0, "not conf resource");
		  return 1;
    }
  }
  else if (!pBoxconf->isnCfcAvail(cfcno))
  {
    Proc_MSG_createcfc_Result(&XMLHeader, OnFail, 0, "confno is out of range");
		return 1;
  }
  else
  {
    nCfc = cfcno;
    if (pCfc->state != 0)
    {
      Proc_MSG_createcfc_Result(&XMLHeader, OnFail, 0, "the conf had created");
		  return 1;
    }
  }
  if (pCfc->CreateId == 3) //2016-12-07
  {
    Proc_MSG_createcfc_Result(&XMLHeader, OnSuccess, nCfc, "");
    return 0;
  }

  if (presiders > MAX_PRESIDER_NUM)
  {
    presiders = MAX_PRESIDER_NUM;
  }
  if (talkers > MAX_TALKERS_NUM)
  {
    talkers = MAX_TALKERS_NUM;
  }
  totaltalks = presiders + talkers;

  nResult = CreateConf(nCfc, nChnn, totaltalks, listeners, 0);
  if (nResult >= 0)
  {
    pCfc->state = 1;
    pCfc->timer = 0;
  
    pCfc->UseId = 1;
  
    pCfc->Name = AttrData->AttrValue[5];
    pCfc->Owner = AttrData->AttrValue[6];
    pCfc->Password = AttrData->AttrValue[10];

    pCfc->MaxPresiders = presiders;
    pCfc->MaxTalkers = talkers;
    pCfc->MaxListeners = listeners;
    pCfc->FreeId = autofree;
    pCfc->AutoRecordId = autorecord;

    pCfc->CreateId = 1;
    if (nResult == nCfc)
    {
      pCfc->CreateId = 3;
      Proc_MSG_createcfc_Result(&XMLHeader, OnSuccess, nCfc, "");
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    }
    return 0;
  }
  else
  {
    Proc_MSG_createcfc_Result(&XMLHeader, OnFail, 0, "create conf fail");
    return 1;
  }
}
int Proc_MSG_destroycfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int nCfc, destroyid;
	
	XMLHeader.OnMsgId = MSG_ondestroycfc;
	strcpy(XMLHeader.OnMsgName, "ondestroycfc");
  //if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
  //  return 1;

  //if (Check_common_MSG(&XMLHeader) != 0)
  //  return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  nCfc = atoi(AttrData->AttrValue[4]);
  destroyid = atoi(AttrData->AttrValue[5]);
  if (!pBoxconf->isnCfcAvail(nCfc))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "confno is out of range");
		return 1;
  }
  if (pCfc->state == 0 || pCfc->CreateId != 3)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the conf is not open");
		return 1;
  }

  if (DestroyConf(nCfc, 0, 0) == nCfc)
  {
    DelAllConfMember(nCfc, destroyid);
    pBoxconf->InitConf(nCfc);
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    //theApp.m_MyConfStatusDialog->ClearConf(nCfc);
    //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    return 0;
  }
  Proc_MSG_common_Result(&XMLHeader, OnFail, "destroy conf fail");
  return 1;
}
int Proc_MSG_joincfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nCfc, jointype, knocktone, emptyevent;
  CH filename[MAX_PATH_FILE_LEN];
	
	XMLHeader.OnMsgId = MSG_onjoincfc;
	strcpy(XMLHeader.OnMsgName, "onjoincfc");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnCfcJoinFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnCfcJoinFail, "chn is error or not online");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	
	nCfc = atoi(AttrData->AttrValue[4]);
	jointype = atoi(AttrData->AttrValue[5]);
  knocktone = atoi(AttrData->AttrValue[6]);
  emptyevent = atoi(AttrData->AttrValue[7]);

  if (!pBoxconf->isnCfcAvail(nCfc))
  {
    Proc_MSG_common_Result(&XMLHeader, OnCfcNotOpen, "confno is out of range");
		return 1;
  }
  if (pCfc->state == 0 || pCfc->CreateId != 3 || pCfc->UseId != 1)
  {
    Proc_MSG_common_Result(&XMLHeader, OnCfcNotOpen, "the conf is not open");
		return 1;
  }
  StopClearPlayDtmfBuf(nChnn);
  if (knocktone == 0)
  {
    pCfc->KnockTone = 0;
  }
  else
  {
    pCfc->KnockTone = (UC)knocktone;
  }
  if (jointype == presider || jointype == talker)
  {
    if (jointype == presider && pCfc->Presideds >= pCfc->MaxPresiders)
    {
      Proc_MSG_common_Result(&XMLHeader, OnCfcJoinFail, "presider is overflow");
		  return 1;
    }
    if (jointype == talker && pCfc->Talkeds >= pCfc->MaxTalkers)
    {
      Proc_MSG_common_Result(&XMLHeader, OnCfcJoinFail, "talker is overflow");
		  return 1;
    }
    if (JoinConf(nCfc, nChnn, 100) == nCfc)
    {
      pChnn->CfcNo = nCfc;
      pChnn->JoinTime = time(0);
      if (jointype == presider)
      {
        pCfc->Presiders[pCfc->Presideds] = nChnn;
        pChnn->JoinType = presider;
        pCfc->Presideds ++;
      }
      else
      {
        pCfc->Talkers[pCfc->Talkeds] = nChnn;
        pChnn->JoinType = talker;
        pCfc->Talkeds ++;
      }
		  //判斷播放加入會議提示音
      if (pCfc->Presideds + pCfc->Talkeds == 1)
      {
        Proc_MSG_common_Result(&XMLHeader, OnCfcFirst, "");
        if (pCfc->curPlayData.state == 0)
        {
          if (pCfc->KnockTone > 0)
            ConfPlayKnockTone(nCfc, 911); //放會議只有1人提示音
        }
      }
      else
      {
        Proc_MSG_common_Result(&XMLHeader, OnCfcJoin, "");
        if (pCfc->curPlayData.state == 0)
        {
          if (pCfc->KnockTone > 0)
          {
            ConfPlayKnockTone(nCfc, 920+pCfc->KnockTone); //放敲門聲
            pCfc->KnockTone = 0;
          }
        }
      }
      if (pCfc->AutoRecordId == 1 && pCfc->curRecData.state == 0 && pCfc->curRecData.RecState == 0)
      {
	      //先停止正在的錄音
	      //Stop_Rec_Clear_ConfBuf(nCfc);

        sprintf(filename, "CRec_%d_%s.voc", nCfc, MyGetNowStr());
        pCfc->curRecData.SessionNo = 0;
	      pCfc->curRecData.CmdAddr = 0;
        pCfc->curRecData.lgChnType = 0;
        pCfc->curRecData.lgChnNo = 0;
        //設置錄音規則
	      if (SetConfRecordRule(nCfc, filename, 0, 0, 1) != 0)
	      {
		      Stop_Rec_Clear_ConfBuf(nCfc);
	      }
      }
      //theApp.m_MyConfStatusDialog->ProcDispMenber(nCfc, nChnn, jointype);
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnCfcJoinFail, "join conf fail");
    }
  }
  else if (jointype == listener)
  {
    if (pCfc->Listeneds >= pCfc->MaxListeners)
    {
      Proc_MSG_common_Result(&XMLHeader, OnCfcListenFail, "listener is overflow");
		  return 1;
    }
    if (ListenConf(nCfc, nChnn) == nCfc)
    {
      pChnn->CfcNo = nCfc;
      pChnn->JoinType = listener;
      pChnn->JoinTime = time(0);
      pCfc->Listeners[pCfc->Listeneds] = nChnn;
      pCfc->EmptyEvent[pCfc->Listeneds] = (UC)emptyevent;
      pCfc->Listeneds ++;
      Proc_MSG_common_Result(&XMLHeader, OnCfcListen, "");
      if (pCfc->curPlayData.state == 0)
      {
        if (pCfc->KnockTone > 0)
        {
          ConfPlayKnockTone(nCfc, 920+pCfc->KnockTone); //放敲門聲
          pCfc->KnockTone = 0;
        }
      }
      //theApp.m_MyConfStatusDialog->ProcDispMenber(nCfc, nChnn, jointype);
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnCfcListenFail, "listen fail");
    }
  }
  else
  {
    Proc_MSG_common_Result(&XMLHeader, OnCfcJoinFail, "jointype is error");
		return 1;
  }
  
  return 0;
}
int Proc_MSG_unjoincfc(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nChn1;
  int nCfc, JoinType;
	
	XMLHeader.OnMsgId = MSG_onunjoincfc;
	strcpy(XMLHeader.OnMsgName, "onunjoincfc");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	
	nCfc = atoi(AttrData->AttrValue[4]);
  if (!pBoxconf->isnCfcAvail(nCfc))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "confno is out of range");
		return 1;
  }
  DelConfMember(nCfc, nChnn, JoinType);
  //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
  //theApp.m_MyConfStatusDialog->ProcDispMenber(nCfc, nChnn, 0);
  if (JoinType == listener)
  {
    if (UnListenConf(nCfc, nChnn, 0) == nCfc)
    {
      Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "confno is out of range");
    }
  }
  else
  {
    if ((pCfc->Presideds+pCfc->Talkeds) == 0 && pCfc->AutoRecordId == 1 && pCfc->curRecData.state == 1 && pCfc->curRecData.RecState != 0)
    {
      Stop_Rec_Clear_ConfBuf(nCfc);
    }
    if (UnjoinConf(nCfc, nChnn, 0) == nCfc)
    {
      Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
      if (pCfc->Presideds + pCfc->Talkeds == 1)
      {
        ConfPlayKnockTone(nCfc, 911); //放會議只有1人提示音
        if (pCfc->Presideds == 1)
        {
          nChn1 = pCfc->Presiders[0];
          //發送會議室由多人邊1人消息
          SendJoinConfResult(nChn1, OnCfcMany2One, "");
        }
        else
        {
          nChn1 = pCfc->Talkers[0];
          //發送會議室由多人邊1人消息
          SendJoinConfResult(nChn1, OnCfcMany2One, "");
        }
      }
      //發送聊天室有空位事件
      if (pCfc->Presideds+pCfc->Talkeds == pCfc->MaxPresiders+pCfc->MaxTalkers - 1)
      {
        for (int i = 0; i < MAX_LISTENERS_NUM; i ++)
        {
          if (pCfc->EmptyEvent[i] == 1 && pBoxchn->isnChnAvail(pCfc->Listeners[i]))
          {
		        nChn1 = pCfc->Listeners[i];
            //發送會議室有空位消息
            SendJoinConfResult(nChn1, OnCfcBusy2Idle, "");
            break;
          }
        }
      }
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "unjoin conf fail");
    }
  }
  if (pCfc->Presideds+pCfc->Talkeds+pCfc->Listeneds == 0 && pCfc->FreeId == 1)
  {
    //會議室無人并且需要自動刪除會議
    if (DestroyConf(nCfc, 0, 1) == nCfc)
    {
      //printf("DestroyConf end\n");
      pBoxconf->InitConf(nCfc);
      //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
      //theApp.m_MyConfStatusDialog->ClearConf(nCfc);
    }
  }
	return 0;
}
//-------------------通道交換類消息----------------------------------------------
int Proc_MSG_link(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int i, nChn1, OnlineId;
  int linkchantype, linkchanno, linkmode, lResult = 0;
  
	XMLHeader.OnMsgId = MSG_onlink;
	strcpy(XMLHeader.OnMsgName, "onlink");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	linkchantype = atoi(AttrData->AttrValue[4]);
	linkchanno = atoi(AttrData->AttrValue[5]);
  linkmode = atoi(AttrData->AttrValue[6]);
  OnlineId = pBoxchn->Get_ChnNo_By_LgChn(linkchantype, linkchanno, nChn1);
	if (OnlineId == 1)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn2 in over range");
		return 1;
	}
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	//判斷是否已加入會議
	if (pChnn->JoinType > 0 || pChnn->CfcNo < MAX_CONF_NUM)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn1 in conf");
		return 1;
	}
  //如果雙方通道已交換則直接返回成功
  if (linkmode == 1 && pChnn->LinkChn[0] == nChn1 && pChn1->LinkChn[0] == nChnn)
  {
    RouterTalk(nChnn, nChn1);
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "relink");
    return 0;
  }
  if (linkmode != disconn)
  {
	  //判斷ChnNo是否已交換
	  if (linkmode != connto && (pChnn->LinkType[0] != 0 || pChnn->LinkType[1] != 0))
	  {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn1 is link with other chn");
		  return 1;
	  }
	  //判斷ChnNo1是否已交換
	  if (linkmode != connfrom && (pChn1->LinkType[0] != 0 || pChn1->LinkType[1] != 0))
	  {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn2 is link with other chn");
		  return 1;
	  }
  }

	switch (linkmode)
  {
  case disconn:
    if ((pChnn->LinkType[0] == 1) && pChnn->LinkChn[0] == nChn1)
      //&& (pChn1->LinkType[0] == 1) && pChn1->LinkChn[0] == nChnn) //edit 2007-09-08 del
    {
      //nChnn <-> nChn1 通話
      lResult = UnRouterTalk(nChnn, nChn1);
      pChnn->RecMixerFlag = 0;
      pChnn->LinkType[0] = 0;
	    pChnn->LinkChn[0] = 0xFFFF;
      pChn1->RecMixerFlag = 0;
	    pChn1->LinkType[0] = 0;
	    pChn1->LinkChn[0] = 0xFFFF;
    }
    else if (pChnn->LinkType[0] == connfrom && pChnn->LinkChn[0] == nChn1)
    {
      //nChnn <- nChn1 監聽
      lResult = UnRouterMonitor(nChnn, nChn1);
      pChnn->LinkType[0] = 0;
	    pChnn->LinkChn[0] = 0xFFFF;
      for (i = 2; i < MAX_LINK_CHN_NUM; i ++)
      {
        if (pChnn->LinkChn[i] == nChnn)
        {
          pChnn->LinkType[i] = 0;
	        pChnn->LinkChn[i] = 0xFFFF;
        }
      }
    }
    else if (pChn1->LinkType[0] == connfrom && pChn1->LinkChn[0] == nChnn)
    {
      //nChnn -> nChn1 被監聽
      lResult = UnRouterMonitor(nChn1, nChnn);
	    pChn1->LinkType[0] = 0;
	    pChn1->LinkChn[0] = 0xFFFF;
      for (i = 2; i < MAX_LINK_CHN_NUM; i ++)
      {
        if (pChn1->LinkChn[i] == nChn1)
        {
          pChn1->LinkType[i] = 0;
	        pChn1->LinkChn[i] = 0xFFFF;
        }
      }
    }
    else if (pChnn->LinkType[0] == 6 || pChnn->LinkType[0] == 7 
      || pChn1->LinkType[0] == 6 || pChn1->LinkType[0] == 7)
    {
      lResult = UnRouterCbm(nChnn, nChn1);
      if (pChnn->PlayBusFlag == 1)
      {
        SetPlayBus(nChnn, 0);
        pChnn->PlayBusFlag = 0;
      }
      pChnn->LinkType[0] = 0;
	    pChnn->LinkChn[0] = 0xFFFF;
      if (pChn1->PlayBusFlag == 1)
      {
        SetPlayBus(nChn1, 0);
        pChn1->PlayBusFlag = 0;
      }
	    pChn1->LinkType[0] = 0;
	    pChn1->LinkChn[0] = 0xFFFF;
    }
    else if (pChnn->LinkType[0] == 10 || pChnn->LinkType[0] == 11
      || pChn1->LinkType[0] == 10 || pChn1->LinkType[0] == 11)
    {
      lResult = UnRouterVC(nChnn, nChn1);
      pChnn->LinkType[0] = 0;
	    pChnn->LinkChn[0] = 0xFFFF;
	    pChn1->LinkType[0] = 0;
	    pChn1->LinkChn[0] = 0xFFFF;
    }
    else
    {
      lResult = 1;
    }
    break;
  case connboth:
    StopClearPlayDtmfBuf(nChnn);
    StopClearPlayDtmfBuf(nChn1);
    lResult = RouterTalk(nChnn, nChn1);
	  if (lResult == 0)
    {
      pChnn->RecMixerFlag = 1;
      pChnn->LinkType[0] = 1;
	    pChnn->LinkChn[0] = nChn1;
      pChn1->RecMixerFlag = 1;
	    pChn1->LinkType[0] = 1;
	    pChn1->LinkChn[0] = nChnn;
    }
    break;
  case connfrom:
    StopClearPlayDtmfRecBuf(nChnn);
    for (i = 2; i <= MAX_LINK_CHN_NUM; i ++)
	  {
		  if (pChn1->LinkType[i] == 0)
		  {
			  break;
		  }
	  }
	  if (i >= MAX_LINK_CHN_NUM)
	  {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "listen chn num is overflow");
		  return 1;
	  }
    lResult = RouterMonitor(nChnn, nChn1);
	  if (lResult == 0)
    {
	    pChnn->LinkType[0] = 2;
	    pChnn->LinkChn[0] = nChn1;
	    pChn1->LinkType[i] = 3;
	    pChn1->LinkChn[i] = nChnn;
    }
    break;
  case connto:
    for (i = 2; i <= MAX_LINK_CHN_NUM; i ++)
	  {
		  if (pChnn->LinkType[i] == 0)
		  {
			  break;
		  }
	  }
	  if (i >= MAX_LINK_CHN_NUM)
	  {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "listen chn num is out of range");
		  return 1;
	  }
    StopClearPlayDtmfRecBuf(nChn1);
    lResult = RouterMonitor(nChn1, nChnn);
	  if (lResult == 0)
    {
	    pChn1->LinkType[0] = 2;
	    pChn1->LinkChn[0] = nChnn;
	    pChnn->LinkType[i] = 3;
	    pChnn->LinkChn[i] = nChn1;
    }
    break;
  case conncbm:
    StopClearPlayDtmfBuf(nChnn);
    StopClearPlayDtmfBuf(nChn1);
    lResult = RouterCbm(nChnn, nChn1);
	  if (lResult == 0)
    {
      pChnn->RecMixerFlag = 1;
      pChnn->PlayBusFlag = 1;
      pChnn->LinkType[0] = 6;
	    pChnn->LinkChn[0] = nChn1;
      
      pChn1->RecMixerFlag = 1;
      pChn1->PlayBusFlag = 1;
	    pChn1->LinkType[0] = 7;
	    pChn1->LinkChn[0] = nChnn;
    }
    break;
  case connvcfrom: //來話變聲
    StopClearPlayDtmfBuf(nChnn);
    StopClearPlayDtmfBuf(nChn1);
    lResult = RouterInVC(nChnn, nChn1);
	  if (lResult == 0)
    {
      pChnn->LinkType[0] = 10;
	    pChnn->LinkChn[0] = nChn1;
	    pChn1->LinkType[0] = 11;
	    pChn1->LinkChn[0] = nChnn;
    }
    break;
  case connvcto: //去話變聲
    StopClearPlayDtmfBuf(nChnn);
    StopClearPlayDtmfBuf(nChn1);
    lResult = RouterOutVC(nChnn, nChn1);
	  if (lResult == 0)
    {
      pChnn->LinkType[0] = 11;
	    pChnn->LinkChn[0] = nChn1;
	    pChn1->LinkType[0] = 10;
	    pChn1->LinkChn[0] = nChnn;
    }
    break;
  case connvcboth: //雙向變聲
    StopClearPlayDtmfBuf(nChnn);
    StopClearPlayDtmfBuf(nChn1);
    lResult = RouterIOVC(nChnn, nChn1);
	  if (lResult == 0)
    {
      pChnn->LinkType[0] = 11;
	    pChnn->LinkChn[0] = nChn1;
	    pChn1->LinkType[0] = 11;
	    pChn1->LinkChn[0] = nChnn;
    }
    break;

  default:
    break;
  }
	
  if (lResult != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "chn link fail");
		return 1;
	}
	
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_talkwith(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int nChn1, OnlineId;
  int linkchantype, linkchanno;
  
	XMLHeader.OnMsgId = MSG_ontalkwith;
	strcpy(XMLHeader.OnMsgName, "ontalkwith");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	linkchantype = atoi(AttrData->AttrValue[4]);
	linkchanno = atoi(AttrData->AttrValue[5]);
  OnlineId = pBoxchn->Get_ChnNo_By_LgChn(linkchantype, linkchanno, nChn1);
	if (OnlineId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or not online");
		return 1;
	}
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearPlayDtmfBuf(nChnn);
  StopClearPlayDtmfBuf(nChn1);
	
	//判斷是否已加入會議
	if (pChnn->JoinType > 0 || pChnn->CfcNo < MAX_CONF_NUM)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn in conf");
		return 1;
	}
  if (pChnn->LinkType[0] == 1 && pChnn->LinkChn[0] == nChn1 && pChn1->LinkType[0] == 1 && pChn1->LinkChn[0] == nChnn)
  {
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    return 0;
  }
	//判斷ChnNo是否已交換
	if (pChnn->LinkType[0] != 0 || pChnn->LinkType[1] != 0)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn1 is link with other chn");
		return 1;
	}
	//判斷ChnNo1是否已交換
	if (pChn1->LinkType[0] != 0 || pChn1->LinkType[1] != 0)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn2 is link with other chn");
		return 1;
	}

	if (RouterTalk(nChnn, nChn1) != 0)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "RouterTalk fail");
		return 1;
	}
  pChnn->PlayBusFlag = 1;
	pChnn->LinkType[0] = 1;
	pChnn->LinkChn[0] = nChn1;
  //pChn1->PlayBusFlag = 1;
	pChn1->LinkType[0] = 1;
	pChn1->LinkChn[0] = nChnn;
	
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_listenfrom(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int i, nChn1, OnlineId;
  int linkchantype, linkchanno;
	
	XMLHeader.OnMsgId = MSG_onlistenfrom;
	strcpy(XMLHeader.OnMsgName, "onlistenfrom");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	linkchantype = atoi(AttrData->AttrValue[4]);
	linkchanno = atoi(AttrData->AttrValue[5]);
  OnlineId = pBoxchn->Get_ChnNo_By_LgChn(linkchantype, linkchanno, nChn1);
	if (OnlineId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or not online");
		return 1;
	}
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearPlayDtmfBuf(nChnn);

	//判斷是否已加入會議
	if (pChnn->JoinType > 0 || pChnn->CfcNo < MAX_CONF_NUM)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn in conf");
		return 1;
	}
	//判斷ChnNo是否已交換
	if (pChnn->LinkType[0] != 0 || pChnn->LinkType[1] != 0)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn is link with other chn");
		return 1;
	}
	//判斷ChnNo1是否已交換
	if (pChn1->LinkType[0] == 2)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the chn is listening other chn");
		return 1;
	}

	for (i = 2; i <= MAX_LINK_CHN_NUM; i ++)
	{
		if (pChn1->LinkType[i] == 0)
		{
			break;
		}
	}
	if (i >= MAX_LINK_CHN_NUM)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "listen chn num is out of range");
		return 1;
	}

  if (RouterMonitor(nChnn, nChn1) != 0)
	{
    Proc_MSG_common_Result(&XMLHeader, OnFail, "RouterMonitor fail");
		return 1;
	}
	pChnn->LinkType[0] = 2;
	pChnn->LinkChn[0] = nChn1;
	pChn1->LinkType[i] = 3;
	pChn1->LinkChn[i] = nChnn;
	
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_inserttalk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int nCfc, nChn1, nChn2=0xFFFF, OnlineId;
  int linkchantype1, linkchanno1;

  XMLHeader.OnMsgId = MSG_oninserttalk;
	strcpy(XMLHeader.OnMsgName, "oninserttalk");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  linkchantype1 = atoi(AttrData->AttrValue[4]);
	linkchanno1 = atoi(AttrData->AttrValue[5]);
  OnlineId = pBoxchn->Get_ChnNo_By_LgChn(linkchantype1, linkchanno1, nChn1);
	if (OnlineId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "dest chn is error or not online");
		return 1;
	}
 
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  if (pChn1->LinkType[0] == 1)
  {
    nChn2 = pChn1->LinkChn[0];
  }
  if (!pBoxchn->isnChnAvail(nChn2))
  {
	  if (RouterTalk(nChnn, nChn1) != 0)
	  {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "RouterTalk fail");
		  return 1;
	  }
	  pChnn->LinkType[0] = 1;
	  pChnn->LinkChn[0] = nChn1;
	  pChn1->LinkType[0] = 1;
	  pChn1->LinkChn[0] = nChnn;
  }
  else
  {
	  nCfc = CreateThreeConf(nChnn);
    if (nCfc == 0)
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "create three conf fail");
      return 1;
    }
    if (JoinThreeConf(nCfc, nChnn, nChn1, nChn2) != 0)
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "join three conf fail");
      return 1;
    }
  }

	Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_threetalk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int nCfc, nChn1, nChn2, OnlineId;
  int linkchantype1, linkchanno1, linkchantype2, linkchanno2;

  XMLHeader.OnMsgId = MSG_onthreetalk;
	strcpy(XMLHeader.OnMsgName, "onthreetalk");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  linkchantype1 = atoi(AttrData->AttrValue[4]);
	linkchanno1 = atoi(AttrData->AttrValue[5]);
  OnlineId = pBoxchn->Get_ChnNo_By_LgChn(linkchantype1, linkchanno1, nChn1);
	if (OnlineId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "chn1 is error or not online");
		return 1;
	}
  linkchantype2 = atoi(AttrData->AttrValue[6]);
	linkchanno2 = atoi(AttrData->AttrValue[7]);
  OnlineId = pBoxchn->Get_ChnNo_By_LgChn(linkchantype2, linkchanno2, nChn2);
	if (OnlineId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "chn2 is error or not online");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	
	nCfc = CreateThreeConf(nChnn);
  if (nCfc == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "create three conf fail");
    return 1;
  }
  StopClearPlayDtmfBuf(nChnn);
  StopClearPlayDtmfBuf(nChn1);
  StopClearPlayDtmfBuf(nChn2);
  if (JoinThreeConf(nCfc, nChnn, nChn1, nChn2) != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "join three conf fail");
    return 1;
  }
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_stoptalk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onstoptalk;
	strcpy(XMLHeader.OnMsgName, "onstoptalk");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  UnLink_All_By_ChnNo(nChnn, 0);
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
//-------------------傳真操作類消息----------------------------------------------
int Proc_MSG_sendfax(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int result;
  XMLHeader.OnMsgId = MSG_onsendfax;
	strcpy(XMLHeader.OnMsgName, "onsendfax");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_Sendfax_Result(&XMLHeader, OnsFaxFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_Sendfax_Result(&XMLHeader, OnsFaxFail, "chn is error or not online");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearPlayDtmfRecBuf(nChnn);

  result = SendFAX(nChnn, 
    AttrData->AttrValue[11], //filename
    AttrData->AttrValue[4], //faxcsid
    AttrData->AttrValue[5], //resolution
    atoi(AttrData->AttrValue[6]), //totalpages
    AttrData->AttrValue[7], //pagelist
    AttrData->AttrValue[8], //faxheader
    AttrData->AttrValue[9], //faxfooter
    AttrData->AttrValue[10] //barcode
    );
  if (result == 9)
  {
    pChnn->FaxCmdAddr = XMLHeader.CmdAddr;
    pChnn->LinkType[0] = 8; //發送傳真
    return 0;
  }
  else if (result > 0)
  {
    Proc_MSG_Sendfax_Result(&XMLHeader, OnsFaxFail, "send fax fail");
    return 1;
  }
  pChnn->FaxCmdAddr = XMLHeader.CmdAddr;
  pChnn->LinkType[0] = 8; //發送傳真
  Proc_MSG_Sendfax_Result(&XMLHeader, OnsFaxing, "");
  return 0;
}
int Proc_MSG_recvfax(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  CStringX FilePath;
  
	XMLHeader.OnMsgId = MSG_onrecvfax;
	strcpy(XMLHeader.OnMsgName, "onrecvfax");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 9) == false)
    return 1;

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_Recvfax_Result(&XMLHeader, OnrFaxFail, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_Recvfax_Result(&XMLHeader, OnrFaxFail, "chn is error or not online");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearPlayDtmfRecBuf(nChnn);
  
  FilePath = AddRootPath_str(pIVRCfg->RecvFaxPath, AttrData->AttrValue[6]);
  
  if (CreateFullPathIfNotExist(FilePath.C_Str()) !=0)
  {
    Proc_MSG_Recvfax_Result(&XMLHeader, OnrFaxFail, "create path fail");
    return 1;
  }
  int nResult = RecvFAX(nChnn, AttrData->AttrValue[6], AttrData->AttrValue[4], AttrData->AttrValue[5]);
  if (nResult == 9)
  {
    pChnn->FaxCmdAddr = XMLHeader.CmdAddr;
    pChnn->LinkType[0] = 9; //接收傳真
    return 0;
  }
  else if (nResult > 0)
  {
    Proc_MSG_Recvfax_Result(&XMLHeader, OnrFaxFail, "recv fax fail");
    return 1;
  }
  pChnn->FaxCmdAddr = XMLHeader.CmdAddr;
  pChnn->LinkType[0] = 9; //接收傳真
  Proc_MSG_Recvfax_Result(&XMLHeader, OnrFaxing, "");
	return 0;
}
//-------------------文件操作類消息----------------------------------------------
int Proc_MSG_checkpath(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_oncheckpath;
	strcpy(XMLHeader.OnMsgName, "oncheckpath");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
  if (MyCheckPath(FilePath.C_Str()) == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "path not exist");
    return 1;
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_createpath(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_oncreatepath;
	strcpy(XMLHeader.OnMsgName, "oncreatepath");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
  
  //if (CreateFullPathIfNotExist(FilePath.C_Str()) !=0)
  CreateAllDirectories((CString)FilePath.C_Str());
  if(GetFileAttributes((CString)FilePath.C_Str())==-1)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "create path fail");
    return 1;
  }
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_deletepath(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_ondeletepath;
	strcpy(XMLHeader.OnMsgName, "ondeletepath");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
  //if (remove(FilePath.C_Str()) != 0)
  if (DeleteDir((CString)FilePath.C_Str()) != 0) //徹底刪除該路徑
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "delete path fail");
    return 1;
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_getfilenum(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int FileNum;
	
	XMLHeader.OnMsgId = MSG_ongetfilenum;
	strcpy(XMLHeader.OnMsgName, "ongetfilenum");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_getfilenum_Result(&XMLHeader, OnFail, 0, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
	FileNum = MyGetFileNum(FilePath.C_Str(), AttrData->AttrValue[5]);
  Proc_MSG_getfilenum_Result(&XMLHeader, OnSuccess, FileNum, "");

	return 0;
}
int Proc_MSG_getfilename(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int FileNum, MaxFileNum;
	VXML_GETFILEATTR_STRUCT FileNames[MAX_GET_FILE_NUM];
	
	XMLHeader.OnMsgId = MSG_ongetfilename;
	strcpy(XMLHeader.OnMsgName, "ongetfilename");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_getfilename_Result(&XMLHeader, OnFail, 0, 0, "", "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	
	MaxFileNum = atoi(AttrData->AttrValue[6]);

	if (MaxFileNum > MAX_GET_FILE_NUM)
	{
		MaxFileNum = MAX_GET_FILE_NUM;
	}
	
	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
	FileNum = MyGetFileName(FilePath.C_Str(), AttrData->AttrValue[5], MAX_GET_FILE_NUM, FileNames);
	for (int i = 0; i < FileNum; i ++)
	{
    Proc_MSG_getfilename_Result(&XMLHeader, OnSuccess, i, FileNum, FileNames[i].FileName, "");
	}
	XMLHeader.OnMsgId = MSG_ongetfilenum;
	strcpy(XMLHeader.OnMsgName, "ongetfilenum");
  Proc_MSG_getfilenum_Result(&XMLHeader, OnSuccess, FileNum, "");
	return 0;
}
int Proc_MSG_renname(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onrenname;
	strcpy(XMLHeader.OnMsgName, "onrenname");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath1, FilePath2;
  FilePath1 = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
  FilePath2 = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[5]);
  if (rename(FilePath1.C_Str(), FilePath2.C_Str()) != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "file rename fail");
    return 1;
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_copyfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	CH *pDest, Path[MAX_VAR_DATA_LEN];
	CStringX FilePath1, FilePath2;
	int nResult;

  XMLHeader.OnMsgId = MSG_oncopyfile;
	strcpy(XMLHeader.OnMsgName, "oncopyfile");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  FilePath1 = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
  FilePath2 = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[5]);

  pDest = strrchr(FilePath2.C_Str(), '/');
  if (pDest == NULL)
  {
    pDest = strrchr(FilePath2.C_Str(), '\\');
  }
	if (pDest != NULL)
  {
    nResult = pDest - FilePath2.C_Str();
    if (nResult > 0)
    {
		  memset(Path, 0, MAX_VAR_DATA_LEN);
		  strncpy(Path, FilePath2.C_Str(), nResult);

		  if (MyCheckPath(Path) == 0)
      if (MyCreatePath(Path) != 0)
		  {
			  //printf("MyCreatePath Path=%s\n", Path);
        Proc_MSG_common_Result(&XMLHeader, OnFail, "create dest path error");
			  return 1;
		  }
    }
	}
  if (MyCopyFile(FilePath1.C_Str(), FilePath2.C_Str()) == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "copy file fail");
    return 1;
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_deletefile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_ondeletefile;
	strcpy(XMLHeader.OnMsgName, "ondeletefile");

	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
  DeleteFiles((CString)(FilePath.C_Str()));

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_checkfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	VXML_GETFILEATTR_STRUCT FileAttr;
	
	XMLHeader.OnMsgId = MSG_oncheckfile;
	strcpy(XMLHeader.OnMsgName, "oncheckfile");
	if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_checkfile_Result(&XMLHeader, OnFail, 0, "recv msg param error");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	CStringX FilePath;
  FilePath = AddRootPath_str(pIVRCfg->PlayFilePath, AttrData->AttrValue[4]);
	if (MyCheckFileAttr(FilePath.C_Str(), &FileAttr) == 0)
	{
    Proc_MSG_checkfile_Result(&XMLHeader, OnFail, 0, "check file not exist");
		return 1;
	}
  Proc_MSG_checkfile_Result(&XMLHeader, OnSuccess, FileAttr.FileSize, "");
	return 0;
}
int Proc_MSG_clearmixer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onclearmixer;
	strcpy(XMLHeader.OnMsgName, "onclearmixer");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  StopClearPlayDtmfBuf(nChnn);
	
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_addfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onaddfile;
	strcpy(XMLHeader.OnMsgName, "onaddfile");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	if (strlen(AttrData->AttrValue[4]) > 0)
	{
		if (AddFileToPlayBuf(nChnn, AttrData->AttrValue[4], ErrMsg) != 0)
		{
			Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
			return 1;
		}
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_adddatetime(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	char szParamStr[MAX_VAR_DATA_LEN], szTemp[256], szTemp1[256];
  int mixtype, nMGYear=0, nAMPMHour=0, nMGYYYY, nMG;

  XMLHeader.OnMsgId = MSG_onadddatetime;
	strcpy(XMLHeader.OnMsgName, "onadddatetime");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  memset(szTemp, 0, 256);
  memset(szTemp1, 0, 256);
  memset(szParamStr, 0, MAX_VAR_DATA_LEN);
  strncpy(szTemp, AttrData->AttrValue[4], 21);
  strcpy(szTemp, MyUpper(szTemp));

  if (strncmp(szTemp, "DATETIME=", 9) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][9]);
    FormatDateTimeStr(szTemp, szParamStr, nMGYear);
  }
  else if (strncmp(szTemp, "DATE=", 5) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][5]);
    FormatDateTimeStr(szTemp, szTemp1, nMGYear);
    
    strncpy(szParamStr, szTemp1, 10);
    strcat(szParamStr, "_HH:MI:SS_W");
  }
  else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][11]);
    FormatDateTimeStr(szTemp, szTemp1, nMGYear);
    
    strncpy(szParamStr, szTemp1, 10);
    strcat(szParamStr, "_HH:MI:SS_W");
  }
  else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 17)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][9], 4);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][13], 2);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][15], 2);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    if (strlen(szTemp) == 21)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][11], 10);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    if (strlen(szTemp) == 17)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][9], 4);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][13], 2);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][15], 2);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;

    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    strncpy(szTemp, &AttrData->AttrValue[4][10], 20);
    int len = strlen(szTemp);
    int k=0, id=0, l=0, y=0, m=0, d=0;
    char c;
    bool bErr=false;
    if (len >= 6 && len <= 9)
    {
      for (int j=0; j<=len; j++)
      {
        c = szTemp[j];
        if (id == 0)
        {
          //年
          if (c >= '0' && c <= '9')
          {
            szTemp1[k] = c;
            k++;
            y++;
          }
          else if (c == '\0' || c == '/' || c == '-' || c == '.')
          {
            if (y == 2)
            {
              szTemp1[3] = '-';
              szTemp1[2] = szTemp1[1];
              szTemp1[1] = szTemp1[0];
              szTemp1[0] = '0';
              k = 4;
              id = 1;
            }
            else if (y == 3)
            {
              szTemp1[k] = '-';
              k++;
              id = 1;
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else
          {
            bErr = true;
            break;
          }
        }
        else if (id == 1)
        {
          //月
          if (c >= '0' && c <= '9')
          {
            szTemp1[k] = c;
            k++;
            m++;
          }
          else if (c == '\0' || c == '/' || c == '-' || c == '.')
          {
            if (m == 1)
            {
              szTemp1[6] = '-';
              szTemp1[5] = szTemp1[4];
              szTemp1[4] = '0';
              k = 7;
              id = 2;
            }
            else if (m == 2)
            {
              szTemp1[k] = '-';
              k++;
              id = 2;
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else
          {
            bErr = true;
            break;
          }
        }
        else if (id == 2)
        {
          //日
          if (c >= '0' && c <= '9')
          {
            szTemp1[k] = c;
            k++;
            d++;
          }
          else if (c == '\0')
          {
            if (d == 1)
            {
              szTemp1[8] = szTemp1[7];
              szTemp1[7] = '0';
              k = 9;
              id = 3;
            }
            else if (d == 2)
            {
              k++;
              id = 3;
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else
          {
            bErr = true;
            break;
          }
        }
      }
    }

    if (strlen(szTemp1) == 9)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, szTemp1, 3);
      nMGYYYY = atoi(szTemp);
      sprintf(szTemp, "%d", nMGYYYY+1911);
      strncat(szTemp, &szTemp1[3], 6);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    if (strlen(szTemp) == 15)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][8], 3);
      nMGYYYY = atoi(szTemp);
      sprintf(szTemp, "%d", nMGYYYY+1911);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][13], 2);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 13)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][7], 4);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      strcat(szTemp, "-DD");
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    if (strlen(szTemp) == 13)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][7], 4);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      strcat(szTemp, "-DD");
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "YYYMM=", 6) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    if (strlen(szTemp) == 11)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][6], 3);
      nMGYYYY = atoi(szTemp);
      sprintf(szTemp, "%d", nMGYYYY+1911);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      strcat(szTemp, "-DD");
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "MG=", 3) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][3], 3);
      nMGYYYY = atoi(szTemp);
      sprintf(szTemp, "%d", nMGYYYY+1911);
      strcat(szTemp, "-MM-DD");
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strlen(szTemp) > 6)
    {
      FormatDateTimeStr(&AttrData->AttrValue[4][3], szParamStr, nMGYear);
      nMGYear = 1;
    }
  }
  else if (strncmp(szTemp, "MMDD=", 5) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 9)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, "YYYY-");
      strncat(szTemp, &AttrData->AttrValue[4][5], 2);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][7], 2);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 17)
    {
      memset(szTemp, 0, 256);
      strcpy(szTemp, "YYYY-");
      strncat(szTemp, &AttrData->AttrValue[4][13], 2);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][15], 2);
      
      strcpy(szParamStr, szTemp);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
  }
  else if (strncmp(szTemp, "TIME=", 5) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][5]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_");
    strncat(szParamStr, &szTemp1[11], 8);
    strcat(szParamStr, "_W");
  }
  else if (strncmp(szTemp, "AMPM=", 5) == 0)
  {
    mixtype = 2;
    nAMPMHour = 1;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][5]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_");
    strncat(szParamStr, &szTemp1[11], 8);
    strcat(szParamStr, "_W");
  }
  else if (strncmp(szTemp, "HHMISS=", 7) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 13)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][7], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][9], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strcat(szParamStr, szTemp);
      strcat(szParamStr, "_W");
    }
  }
  else if (strncmp(szTemp, "APMISS=", 7) == 0)
  {
    mixtype = 2;
    nAMPMHour = 1;
    if (strlen(szTemp) == 13)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][7], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][9], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strcat(szParamStr, szTemp);
      strcat(szParamStr, "_W");
    }
  }
  else if (strncmp(szTemp, "HHMI=", 5) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 9)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][5], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][7], 2);
      strcat(szTemp, ":SS");
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strcat(szParamStr, szTemp);
      strcat(szParamStr, "_W");
    }
  }
  else if (strncmp(szTemp, "APMI=", 5) == 0)
  {
    mixtype = 2;
    nAMPMHour = 1;
    if (strlen(szTemp) == 9)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][5], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][7], 2);
      strcat(szTemp, ":SS");
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strcat(szParamStr, szTemp);
      strcat(szParamStr, "_W");
    }
  }
  else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][8]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strncpy(szParamStr, szTemp1, 7);
    strcat(szParamStr, "-DD_HH:MI:SS_W");
  }
  else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
  {
    nMGYear = 1; //臺灣民國讀法
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][8]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strncpy(szParamStr, szTemp1, 7);
    strcat(szParamStr, "-DD_HH:MI:SS_W");
  }
  else if (strncmp(szTemp, "MM-DD=", 6) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][6]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);

    strcpy(szParamStr, "YYYY-");
    strncat(szParamStr, &szTemp1[5], 5);
    strcat(szParamStr, "_HH:MI:SS_W");
  }
  else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][12]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-");
    strncat(szParamStr, &szTemp1[5], 11);
    strcat(szParamStr, ":SS_W");
  }
  else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
  {
    mixtype = 2;
    if (strlen(szTemp) == 17)
    {
      memset(szTemp, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[4][9], 2);
      strcat(szTemp, "-");
      strncat(szTemp, &AttrData->AttrValue[4][11], 2);
      strcat(szTemp, "_");
      strncat(szTemp, &AttrData->AttrValue[4][13], 2);
      strcat(szTemp, ":");
      strncat(szTemp, &AttrData->AttrValue[4][15], 2);
      
      strcpy(szParamStr, "YYYY-");
      strcat(szParamStr, szTemp);
      strcat(szParamStr, ":SS_W");
    }
  }
  else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][9]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-");
    strncat(szParamStr, &szTemp1[5], 8);
    strcat(szParamStr, ":MI:SS_W");
  }
  else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][9]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-");
    strncat(szParamStr, &szTemp1[8], 8);
    strcat(szParamStr, ":SS_W");
  }
  else if (strncmp(szTemp, "DD_HH=", 6) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][6]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-");
    strncat(szParamStr, &szTemp1[8], 5);
    strcat(szParamStr, ":MI:SS_W");
  }
  else if (strncmp(szTemp, "DD=", 3) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][3]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);

    strcpy(szParamStr, "YYYY-MM-");
    strncat(szParamStr, &szTemp1[8], 2);
    strcat(szParamStr, "_HH:MI:SS_W");
  }
  else if (strncmp(szTemp, "W=", 2) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][2]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
    strncat(szParamStr, &szTemp1[20], 1);
  }
  else if (strncmp(szTemp, "HH:MI=", 6) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][6]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_");
    strncat(szParamStr, &szTemp1[11], 5);
    strcat(szParamStr, ":SS_W");
  }
  else if (strncmp(szTemp, "AP:MI=", 6) == 0)
  {
    mixtype = 2;
    nAMPMHour = 1;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][6]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_");
    strncat(szParamStr, &szTemp1[11], 5);
    strcat(szParamStr, ":SS_W");
  }
  else if (strncmp(szTemp, "MI:SS=", 6) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][6]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);

    strcpy(szParamStr, "YYYY-MM-DD_HH:");
    strncat(szParamStr, &szTemp1[14], 5);
    strcat(szParamStr, "_W");
  }
  else if (strncmp(szTemp, "HH=", 3) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][3]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);

    strcpy(szParamStr, "YYYY-MM-DD_");
    strncat(szParamStr, &szTemp1[11], 2);
    strcat(szParamStr, ":MI:SS_W");
  }
  else if (strncmp(szTemp, "AP=", 3) == 0)
  {
    mixtype = 2;
    nAMPMHour = 1;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][3]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_");
    strncat(szParamStr, &szTemp1[11], 2);
    strcat(szParamStr, ":MI:SS_W");
  }
  else if (strncmp(szTemp, "MI=", 3) == 0)
  {
    mixtype = 2;
    memset(szTemp, 0, 256);
    strcpy(szTemp, &AttrData->AttrValue[4][3]);
    FormatDateTimeStr(szTemp, szTemp1, nMG);
    
    strcpy(szParamStr, "YYYY-MM-DD_HH:");
    strncat(szParamStr, &szTemp1[14], 2);
    strcat(szParamStr, ":SS_W");
  }
  else
  {
    FormatDateTimeStr(AttrData->AttrValue[4], szParamStr, nMGYear);
  }

  if (AddDateTimeToPlayBuf(pChnn->curSpeech, nChnn, szParamStr, ErrMsg, nMGYear, nAMPMHour) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_addmoney(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	double Money;
	
	XMLHeader.OnMsgId = MSG_onaddmoney;
	strcpy(XMLHeader.OnMsgName, "onaddmoney");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	if (Check_Float_Str(AttrData->AttrValue[4], Money) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "money error");
		return 1;
	}
	if (AddMoneyToPlayBuf(pChnn->curSpeech, nChnn, Money, ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_addnumber(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	double Number;
	
	XMLHeader.OnMsgId = MSG_onaddnumber;
	strcpy(XMLHeader.OnMsgName, "onaddnumber");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	if (Check_Float_Str(AttrData->AttrValue[4], Number) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, "numeric error");
		return 1;
	}
	if (AddNumberToPlayBuf(pChnn->curSpeech, nChnn, Number, AttrData->AttrValue[4], ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_adddigits(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onadddigits;
	strcpy(XMLHeader.OnMsgName, "onadddigits");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	if (AddDigitsToPlayBuf(pChnn->curSpeech, nChnn, AttrData->AttrValue[4], ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_addchars(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onaddchars;
	strcpy(XMLHeader.OnMsgName, "onaddchars");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	if (AddCharsToPlayBuf(nChnn, AttrData->AttrValue[4], ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_playmixer(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int DtmfRuleId, PlayRuleId, AsrRuleId;
	
	XMLHeader.OnMsgId = MSG_onplaymixer;
	strcpy(XMLHeader.OnMsgName, "onplaymixer");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;

  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;

	if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }
 
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChnn, MSG_onplaymixer, "onplaymixer");

	DtmfRuleId = atoi(AttrData->AttrValue[4]);
	PlayRuleId = atoi(AttrData->AttrValue[5]);
	AsrRuleId = atoi(AttrData->AttrValue[6]);
	//設置收碼放音規則
	if (SetDTMFPlayRule(nChnn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
	{
		Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChnn);
		return 1;
	}

	pChnn->curPlayData.PlayState = 1;
	pChnn->PlayedTimer = 0;
	if (Start_ChnPlay(nChnn) != 0)
	{
    //StopClearPlayDtmfBuf(nChnn);
    //Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "啟動放音失敗");
		return 1;
	}
	pChnn->curPlayData.CanPlayId = 1;
	
	return 0;
}
int Proc_MSG_addttsstr(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	
	XMLHeader.OnMsgId = MSG_onaddttsstr;
	strcpy(XMLHeader.OnMsgName, "onaddttsstr");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;


  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	if (AddTTSStrToPlayBuf(nChnn, AttrData->AttrValue[4], ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_addttsfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onaddttsfile;
	strcpy(XMLHeader.OnMsgName, "onaddttsfile");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	if (AddTTSFileToPlayBuf(nChnn, AttrData->AttrValue[4], ErrMsg) != 0)
	{
		Proc_MSG_common_Result(&XMLHeader, OnFail, ErrMsg);
		return 1;
	}
	//Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_dialout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	return 0;
}
int Proc_MSG_flash(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onflash;
	strcpy(XMLHeader.OnMsgName, "onflash");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  if (Flash(nChnn) != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "flash fail");
    return 1;
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_setvcparam(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int vcmode, vcparam, vcmix, noisemode, noiseparam;

  XMLHeader.OnMsgId = MSG_onsetvcparam;
	strcpy(XMLHeader.OnMsgName, "onsetvcparam");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

	vcmode = atoi(AttrData->AttrValue[4]);
	vcparam = atoi(AttrData->AttrValue[5]);
	vcmix = atoi(AttrData->AttrValue[6]);
	noisemode = atoi(AttrData->AttrValue[7]);
	noiseparam = atoi(AttrData->AttrValue[8]);

  if (SetVCParam(nChnn, vcmode, vcparam, vcmix, noisemode, noiseparam) != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "set vc fail");
    return 1;
  }

  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
	return 0;
}
int Proc_MSG_pause(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onpause;
	strcpy(XMLHeader.OnMsgName, "onpause");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  //暫停放音
  if (PausePlay(nChnn) == 0)
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  else
    Proc_MSG_common_Result(&XMLHeader, OnFail, "");

  return 0;
}
int Proc_MSG_replay(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	XMLHeader.OnMsgId = MSG_onreplay;
	strcpy(XMLHeader.OnMsgName, "onreplay");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  //重新放音操作
  if (ReStartPlay(nChnn) == 0)
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  else
    Proc_MSG_common_Result(&XMLHeader, OnFail, "");

	return 0;
}
int Proc_MSG_fastplay(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int result, direction, length;

  XMLHeader.OnMsgId = MSG_onfastplay;
	strcpy(XMLHeader.OnMsgName, "onfastplay");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
	
  direction = atoi(AttrData->AttrValue[4]);
	length = atoi(AttrData->AttrValue[5]);

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  //快速放音操作
  if (direction == 0)
    result = FastFwdPlay(nChnn);
  else if (direction == 1)
    result = FastBwdPlay(nChnn);

  if (result == 0)
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  else
    Proc_MSG_common_Result(&XMLHeader, OnFail, "");

	return 0;
}
int Proc_MSG_checktone(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int tonetype;

	XMLHeader.OnMsgId = MSG_onchecktone;
	strcpy(XMLHeader.OnMsgName, "onchecktone");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 11) == false)
    return 1;

  if (XMLHeader.ErrorId != 0)
	{
		Proc_MSG_checktone_Result(&XMLHeader, OnFail, 0, "recv msg param error");
		return 1;
	}
	if (XMLHeader.OnlineId != 0)
	{
		Proc_MSG_checktone_Result(&XMLHeader, OnFail, 0, "chn is error or not online");
		return 1;
	}

  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
	
  //1-撥號音，2-回鈴音，3-忙音，4-應答音，5-F1傳真信號音(發送傳真的信號音)，6-F2傳真信號音(等待接收傳真的信號音)
  tonetype = atoi(AttrData->AttrValue[4]);

  pChnn->CheckToneCmdAddr = XMLHeader.CmdAddr;
  if (CheckTone(nChnn, tonetype, AttrData->AttrValue[5]) != 0)
  {
    Proc_MSG_checktone_Result(&XMLHeader, OnFail, 0, "detect tone fail");
  }
  if (tonetype == 0)
  {
    Proc_MSG_checktone_Result(&XMLHeader, OnSuccess, 0, "");
  }
	return 0;
}
//話務員登錄
int Proc_MSG_workerlogin(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
	int workerno, groupno, workergrade, level, groupnum, nAG;
  CStringX ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER];

	XMLHeader.OnMsgId = MSG_onworkerlogin;
	strcpy(XMLHeader.OnMsgName, "onworkerlogin");

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  if (!pAgentMng->isnAGAvail(nAG))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "SeatNo not Login before");
    return 0;
  }
  workerno = atoi(AttrData->AttrValue[5]); //話務員工號
  if (workerno == 0)
  {
    if (pAG->m_Worker.LoginIniWorker() == 0)
    {
      Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
      DispAgentStatus(pAG->nAG);

      pAG->m_Worker.TelLogInOutFlag = 1;

//       pAG->ChangeStatus(AG_STATUS_LOGIN);
//       WriteWorkerLoginStatus(pAG);
//       WriteSeatStatus(pAG, AG_STATUS_LOGIN, 0);
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "ini workerno is not set");
    }
    return 0;
  }
  if (IsTheWorkerLogin(workerno))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the workerno had login");
    return 1;
  }
  workergrade = atoi(AttrData->AttrValue[9]); //話務員類別
  pAG->m_Worker.WorkerLogin(workerno, AttrData->AttrValue[6], workergrade);
  
  groupnum = SplitTxtLine(AttrData->AttrValue[8], MAX_GROUP_NUM_FORWORKER, ArrString1);
  SplitTxtLine(AttrData->AttrValue[10], MAX_GROUP_NUM_FORWORKER, ArrString2);
  for (int i=0; i<groupnum; i++)
  {
    groupno = atoi(ArrString1[i].C_Str());
    level = atoi(ArrString2[i].C_Str());
    pAG->m_Worker.SetWorkerGroupNo(groupno, level);
  }
  
  pAG->m_Worker.PreWorkerNo = workerno;
  pAG->m_Worker.PreGroupNo = pAG->GetGroupNo();
  memcpy(pAG->m_Worker.PreGroupNoList, pAG->m_Worker.GroupNo, sizeof(pAG->m_Worker.GroupNo));
  pAG->m_Worker.PreDutyNo = 0;

  DispAgentStatus(pAG->nAG);
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");

  pAG->m_Worker.TelLogInOutFlag = 1;

//   pAG->ChangeStatus(AG_STATUS_LOGIN);
//   WriteWorkerLoginStatus(pAG);
//   WriteSeatStatus(pAG, AG_STATUS_LOGIN, 0);
  return 0;
}
//話務員退出
int Proc_MSG_workerlogout(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAG;

	XMLHeader.OnMsgId = MSG_onworkerlogout;
	strcpy(XMLHeader.OnMsgName, "onworkerlogout");

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

	nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  if (!pAgentMng->isnAGAvail(nAG))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "SeatNo not Login before");
    return 0;
  }

//   int nGroupNo = pAG->GetGroupNo();
//   pAG->m_Worker.Logout();
//   pAG->m_Worker.ClearState();
// 
//   DispAgentStatus(pAG->nAG);
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");

  pAG->m_Worker.TelLogInOutFlag = 2;
  
//   WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
//   AgentStatusCount();
//   SendOneGroupStatusMeteCount(pAG->nAG);

  return 0;
}
//向坐席發送轉接呼叫結果
int Proc_MSG_agtrancallresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int workerno, nResult;
  CStringX SeatNo; 
  CAgent *pAgent=NULL;

  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;

  SeatNo = AttrData->AttrValue[4];
  workerno = atoi(AttrData->AttrValue[5]);

  if (SeatNo.Compare("0") == 0 && workerno == 0)
  {
    pAgent = GetAgentBynChn(nChnn);
  }
  else if (SeatNo.Compare("0") != 0)
  {
    pAgent = GetAgentbySeatNo(SeatNo, nResult);
  }
  else if (workerno != 0)
  {
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
  }
  
  if (pAgent != NULL)
  {
    if (pAgent->isTcpLinked())
    {
      sprintf(SendMsgBuf, "<ontrancall acdid='%lu' trantype='%s' sessionno='%s' chantype='%s' channo='%s' cfcno='%s' callerno='%s' calledno='%s' param='%s' result='%s' errorbuf='%s'/>", 
        pAgent->GetClientId()<<16,
        AttrData->AttrValue[6],
        AttrData->AttrValue[7],
        AttrData->AttrValue[8],
        AttrData->AttrValue[9],
        AttrData->AttrValue[10],
        AttrData->AttrValue[11],
        AttrData->AttrValue[12],
        AttrData->AttrValue[13],
        AttrData->AttrValue[14],
        AttrData->AttrValue[15]);
      SendMsg2AG(pAgent->GetClientId(), AGMSG_ontrancall, SendMsgBuf);
    }
  }

  return 0;
}
//坐席與服務的通道穿梭通話結果
int Proc_MSG_agjoinconfresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int workerno, nResult;
  CStringX SeatNo; 
  CAgent *pAgent=NULL;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SeatNo = AttrData->AttrValue[4];
  workerno = atoi(AttrData->AttrValue[5]);
  
  if (SeatNo.Compare("0") == 0 && workerno == 0)
  {
    pAgent = GetAgentBynChn(nChnn);
  }
  else if (SeatNo.Compare("0") != 0)
  {
    pAgent = GetAgentbySeatNo(SeatNo, nResult);
  }
  else if (workerno != 0)
  {
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
  }
  
  if (pAgent != NULL)
  {
    if (pAgent->isTcpLinked())
    {
      sprintf(SendMsgBuf, "<onjoinconf acdid='%lu' destchntype='%s' destchnno='%s' confno='%s' jointype='%s' result='%s' errorbuf='%s'/>", 
        pAgent->GetClientId()<<16,
        AttrData->AttrValue[6],
        AttrData->AttrValue[7],
        AttrData->AttrValue[8],
        AttrData->AttrValue[9],
        AttrData->AttrValue[10],
        AttrData->AttrValue[11]);
      SendMsg2AG(pAgent->GetClientId(), AGMSG_onjoinconf, SendMsgBuf);
    }
  }
  
  return 0;
}
//坐席將來話轉接到ivr流程結果
int Proc_MSG_agtranivrresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int workerno, nResult;
  CStringX SeatNo; 
  CAgent *pAgent=NULL;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SeatNo = AttrData->AttrValue[4];
  workerno = atoi(AttrData->AttrValue[5]);
  
  if (SeatNo.Compare("0") == 0 && workerno == 0)
  {
    pAgent = GetAgentBynChn(nChnn);
  }
  else if (SeatNo.Compare("0") != 0)
  {
    pAgent = GetAgentbySeatNo(SeatNo, nResult);
  }
  else if (workerno != 0)
  {
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
  }
  
  if (pAgent != NULL)
  {
    if (pAgent->isTcpLinked())
    {
      sprintf(SendMsgBuf, "<ontranivr acdid='%lu' returnflag='%s' param='%s' result='%s' errorbuf='%s'/>", 
        pAgent->GetClientId()<<16,
        AttrData->AttrValue[6],
        AttrData->AttrValue[7],
        AttrData->AttrValue[8],
        AttrData->AttrValue[9]);
      SendMsg2AG(pAgent->GetClientId(), AGMSG_ontranivr, SendMsgBuf);
    }
  }
  
  return 0;
}
//發送消息到坐席
int Proc_MSG_sendagentmsg(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int workerno, nResult;
  CStringX SeatNo; 
  CAgent *pAgent=NULL;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SeatNo = AttrData->AttrValue[4];
  workerno = atoi(AttrData->AttrValue[5]);
  
  pAgent = NULL;
  if (SeatNo.Compare("0") == 0 && workerno == 0)
  {
    pAgent = GetAgentBynChn(nChnn);
  }
  else if (SeatNo.Compare("0") != 0)
  {
    pAgent = GetAgentbySeatNo(SeatNo, nResult);
  }
  else if (workerno != 0)
  {
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
  }
  
  if (pAgent != NULL)
  {
    if (pAgent->isLogin() == true && pAgent->isTcpLinked() == true)
    {
      sprintf(SendMsgBuf, "<onrecvmsgfromflw acdid='%lu' msgtype='%s' item0='%s' item1='%s' item2='%s' item3='%s'/>", 
        pAgent->GetClientId()<<16,
        AttrData->AttrValue[6],
        AttrData->AttrValue[7],
        AttrData->AttrValue[8],
        AttrData->AttrValue[9],
        AttrData->AttrValue[10]);
      SendMsg2AG(pAgent->GetClientId(), AGMSG_onrecvmsgfromflw, SendMsgBuf);
    }
  }
  return 0;
}
//代接電話
int Proc_MSG_pickupcall(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int groupno, workerno, selectmode;
  CStringX seatno;
  int nAGn, nAG1, nChn1, nQue, nOut, nAcd;
  CAgent *pAgent=NULL, *pAgent1=NULL;
  
  XMLHeader.OnMsgId = MSG_onpickupcall;
  strcpy(XMLHeader.OnMsgName, "onpickupcall");
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_pickupcall_Result(&XMLHeader, OnFail, "", "", 0, 0, "", 0, "param is error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_pickupcall_Result(&XMLHeader, OnFail, "", "", 0, 0, "", 0, "chn is error or is not online");
    return 1;
  }
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  pAgent = GetAgentBynChn(nChnn);
  if (pAgent == NULL)
  {
    Proc_MSG_pickupcall_Result(&XMLHeader, OnFail, "", "", 0, 0, "", 0, "the seat have not login");
    return 0;
  }
  nAGn = pAgent->nAG;

  groupno = atoi(AttrData->AttrValue[4]);
  seatno = AttrData->AttrValue[5];
  workerno = atoi(AttrData->AttrValue[6]);
  selectmode = atoi(AttrData->AttrValue[7]);

  nAG1 = GetnAGForPickup(seatno.C_Str(), workerno, groupno, selectmode);
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    Proc_MSG_pickupcall_Result(&XMLHeader, OnFail, "", "", 0, 0, "", 0, "have not seat for pickup");
    return 1;
  }
  nQue = pAG1->nQue;
  if (!pCalloutQue->isnQueAvail(nQue))
  {
    Proc_MSG_pickupcall_Result(&XMLHeader, OnFail, "", "", 0, 0, "", 0, "have not call in");
    return 1;
  }
  //取消以前的呼叫
  nOut = pQue->nOut;
  nAcd = pQue->nAcd;
  nChn1 = pQue->OutnChn;
  CancelCallout(nChn1);
  pAG1->nQue = 0xFFFF;
  if (pAG1->isWantSeatType(SEATTYPE_RT_TEL))
  {
    pChn1->nAG = 0xFFFF; //清除遠程坐席與通道的綁定關系
    pAG1->m_Seat.nChn = 0xFFFF;
  }
  SendStopACDCallIn(nAG1);
  WriteSeatStatus(pAG1, AG_STATUS_IDEL, 2);
  SetAgentsvState(nAG1, AG_SV_IDEL);
  pQue->state = 0;
  //發送新的呼叫結果給被代接通道
  if (pQue->OrgCallType == 1)
  {
    //取消被代接通道的呼出
    for (int j=0; j<pOut->CalledNum; j++)
    {
      if (pQue->CallPoint==j) continue;
      if (pOut->Calleds[j].pQue_str != NULL)
        pOut->Calleds[j].pQue_str->CancelId = 1;
      if (pOut->Calleds[j].CalloutResult == CALLOUT_CONN)
      {
        pOut->Calleds[j].CalloutResult = 0;
        Release_Chn(pOut->Calleds[j].OutnChn, 1, 0);
      }
    }
    pOut->state = 0;
    
    SendCalloutResult(nOut, pQue->CallPoint, OnOutAnswer, "", nChnn);
    pAgentMng->lastAnsnAG[0] = nAGn;
    pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][0] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][pAGn->GetLevel()] = nAGn;
    pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
    pAGn->m_Worker.AcdedGroupNo = 0;
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);
    
    SetAgentsvState(nAGn, AG_SV_CONN);
    SendSeatPickupResult(nAGn, nOut, pQue->CallPoint, OnSuccess, "");

    Proc_MSG_pickupcall_Result(&XMLHeader, OnSuccess, nOut, pQue->CallPoint, pAG1->GetSeatNo().C_Str(), pAG1->GetWorkerNo());
  }
  else
  {
    //取消被代接通道的呼叫坐席
    for (int j=0; j<pAcd->CalledNum; j++)
    {
      if (pQue->CallPoint==j) continue;
      if (pAcd->Calleds[j].pQue_str != NULL)
        pAcd->Calleds[j].pQue_str->CancelId = 1;
      pAgent1 = pAgentMng->m_Agent[pAcd->Calleds[j].nAG];
      if (pAcd->Calleds[j].CalloutResult == CALLOUT_CONN)
      {
        pAcd->Calleds[j].CalloutResult = 0;
        Release_Chn(pAgent1->GetSeatnChn(), 1, 0);
      }
      SetAgentsvState(pAgent1, AG_SV_IDEL);
    }
    
    pAGn->CanTransferId = 1;
    pAGn->SetSrvLine(pAcd->Session.nChn, pAcd->Session.SessionNo, pAcd->Session.CmdAddr);
    SendCallseatResult(nAcd, pAGn, 0, OnACDAnswer, "");
    pAgentMng->lastAnsnAG[0] = nAGn;
    pAgentMng->lastAnsnAG[pAGn->GetGroupNo()] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][0] = nAGn;
    pAgentMng->lastAnsLevelnAG[0][pAGn->GetLevel()] = nAGn;
    pAgentMng->lastAnsLevelnAG[pAGn->GetGroupNo()][pAGn->GetLevel()] = nAGn;
    pAGn->m_Worker.AcdedGroupNo = pAcd->ACDRule.AcdedGroupNo;
    WriteSeatStatus(pAGn, AG_STATUS_TALK, 0, 1, 2);

    if (pIVRCfg->ControlCallModal == 0)
    {
      pAGn->SetCDRSerialNo(pAcd->Session.CRDSerialNo.C_Str());
    }
    else
    {
      int nChn = pAcd->Session.nChn;
      if (pChn->lgChnType == 2)
      {
        DBUpdateCallCDR_CallInOut(nChn, 3);
      }
      //pChn->GenerateRecordFileName(); //del by zgj 2011-07-22
      strcpy(pAGn->CdrSerialNo, pChn->CdrSerialNo);
      strcpy(pAGn->RecordFileName, pChn->RecordFileName);
      DBUpdateCallCDR_SeatAnsTime(pAGn->nAG, pAcd->ACDRule.AcdedGroupNo);
      if (pAGn->m_Worker.AutoRecordId == 1 && pAGn->m_Seat.AutoRecordId == 1)
      {
        char szTemp[256], pszError[128];
        sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAGn->RecordFileName);
        CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
        
        if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
        {
          if (SetChnRecordRule(pAGn->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
          {
            DBUpdateCallCDR_RecdFile(pAGn->nAG);
          }
        }
      }
    }
    DelFromQueue(nAcd);
    
    SetAgentsvState(nAGn, AG_SV_CONN);
    SendSeatPickupResult(nAGn, nAcd, OnSuccess, "");

    Proc_MSG_pickupcall_Result(&XMLHeader, OnSuccess, nAcd, pAG1->GetSeatNo().C_Str(), pAG1->GetWorkerNo());
  }
  return 0;
}
//取ACD隊列信息結果
int Proc_MSG_getacdqueue(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int groupno, waitacd, waitans;
  CStringX ArrString[MAX_GROUP_NUM_FORWORKER];
  
  XMLHeader.OnMsgId = MSG_ongetacdqueue;
  strcpy(XMLHeader.OnMsgName, "ongetacdqueue");
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_getacdqueue_Result(&XMLHeader, OnFail, 0, 0, "param is error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_getacdqueue_Result(&XMLHeader, OnFail, 0, 0, "chn is error or is not online");
    return 1;
  }
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  SplitString(AttrData->AttrValue[4], ';', MAX_GROUP_NUM_FORWORKER, ArrString);
  groupno = atoi(ArrString[0].C_Str());
  GetACDQueue(groupno, waitacd, waitans);
  
  Proc_MSG_getacdqueue_Result(&XMLHeader, OnSuccess, waitacd, waitans, "");
  return 0;
}
//設置通道流程業務狀態信息
int Proc_MSG_setflwstate(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  CAgent *pAgent=NULL;
  if (XMLHeader.ErrorId != 0)
  {
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    return 1;
  }
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  pChnn->FlwState = AttrData->AttrValue[4];
  pAgent = GetAgentBynChn(nChnn);
  if (pAgent != NULL)
  {
    pAgent->FlwState = AttrData->AttrValue[4];
  }
  DispChnStatus(nChnn);
  return 0;
}
int Proc_MSG_swapcallresult(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  UL dialserialno, sessionid1;
  int nOut, nAcd, nChn1, chntype1, chnno1;
  
  dialserialno = atol(AttrData->AttrValue[4]);
  sessionid1 = atol(AttrData->AttrValue[5]);
  chntype1 = atoi(AttrData->AttrValue[6]);
  chnno1 = atoi(AttrData->AttrValue[7]);
  nChn1 = pBoxchn->Get_nChn_By_LgChn(chntype1, chnno1);

  nOut = pCallbuf->Get_nOut_By_DialSerialNo(dialserialno);
  if (pCallbuf->isnOutAvail(nOut))
  {
    pOut->Session.SessionNo = sessionid1;
    pOut->Session.nChn = nChn1;
  }
  nAcd = pACDQueue->Get_nAcd_By_DialSerialNo(dialserialno);
  if (pACDQueue->isnAcdAvail(nAcd))
  {
    pAcd->Session.SessionNo = sessionid1;
    pAcd->Session.nChn = nChn1;
  }
  return 0;
}
//接收FSK數據
int Proc_MSG_recvfsk(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int FskHeader, CRCType;
  
  XMLHeader.OnMsgId = MSG_onrecvfsk;
  strcpy(XMLHeader.OnMsgName, "onrecvfsk");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  StopClearPlayDtmfBuf(nChnn);
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  FskHeader = atoi(AttrData->AttrValue[4]);
  CRCType = atoi(AttrData->AttrValue[5]);
  
  if (StartRecvFSKStr(nChnn, FskHeader, CRCType) != 0)
  {
    SendRecvdFSKResult(nChnn, 0, "", 0, OnFail, "recv fsk fail");
  }
  return 0;
}
int Proc_MSG_transferivr(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int vopno, nChn;
  
  XMLHeader.OnMsgId = MSG_ontransferivr;
  strcpy(XMLHeader.OnMsgName, "ontransferivr");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  if (g_nSwitchMode == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  }
  else
  {
    vopno = atoi(AttrData->AttrValue[4]);
    nChn = GetIVRChn(vopno, AttrData->AttrValue[5]);

    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->lnState = CHN_LN_SEIZE;
      Send_SWTMSG_transfercall(nChnn, pChnn->DeviceID, pChnn->ConnID, nChn, pChn->DeviceID, 2, AttrData->AttrValue[6], 0);
      DispChnStatus(nChn);
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "");
    }
  }
  return 0;
}
int Proc_MSG_releaseivr(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  short nVopNo, nVopChnNo;

  XMLHeader.OnMsgId = MSG_onreleaseivr;
  strcpy(XMLHeader.OnMsgName, "onreleaseivr");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  if (g_nSwitchMode == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  }
  else
  {
    if (GetVopChnBynChn(nChnn, nVopNo, nVopChnNo) == true)
    {
      if (nVopNo == 0)
      {
        POSTRELEASE(0, nVopChnNo, 0);
      }
      else
      {
        SendRelease2VOP(nChnn, nVopNo, nVopChnNo);
      }
      pChnn->cVopChn->ReleaseVopChn();
      pChnn->SeizeVopChnState = 0;
      Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
    }
    else
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "not allocated ivr defore");
    }
  }
  return 0;
}
int Proc_MSG_playswitchvoice(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int voiceid;

  XMLHeader.OnMsgId = MSG_onplayswitchvoice;
  strcpy(XMLHeader.OnMsgName, "onplayswitchvoice");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  if (g_nSwitchMode == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "");
  }
  else
  {
    voiceid = atoi(AttrData->AttrValue[4]);
    Send_SWTMSG_bandvoice(pChnn->ChIndex, pChnn->DeviceID, pChnn->ConnID, voiceid);
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  }
  return 0;
}
int Proc_MSG_sendswitchcmd(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nChn1, cmdid;
  CVopChn *pVopChn=NULL;
  int workerno, nResult;
  CStringX SeatNo; 
  CAgent *pAgent=NULL;
  
  XMLHeader.OnMsgId = MSG_onsendswitchcmd;
  strcpy(XMLHeader.OnMsgName, "onsendswitchcmd");
  
  cmdid = atoi(AttrData->AttrValue[4]);
  if (cmdid == 100 || cmdid == 101)
  {
    workerno = atoi(AttrData->AttrValue[5]);
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
    if (pAgent)
    {
      if (cmdid == 100)
      {
        //2016-04-15 只有在DisturbId == 2時發送onhangon
        if (pAgent->m_Worker.DisturbId == 2 && WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
        {
          sprintf(SendMsgBuf, "wsclientid=%s;cmd=onhangon;seatno=%s;result=0;",
            pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str());
          SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
        }
        if (pAgent->m_Worker.DisturbId == 2) //2016-04-15
        {
          pAgent->m_Worker.DisturbId = 0;
          pAgent->SendTrnIVRHangonFlag = 0; //2016-07-19
        }
      }
      else
      {
        pAgent->m_Worker.DisturbId = 2;
      }
      SendOneAGStateToAll(pAgent->nAG);
    }
    return 0;
  }

  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);

  if (g_nSwitchMode == 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  }
  else if (g_nSwitchMode == 13)
  {
    Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  }
  else
  {
    pVopChn = pChnn->cVopChn;
    if (pVopChn == NULL)
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "");
      return 1;
    }
    else
    {
      nChn1 = pVopChn->m_nBoxChn;
      if (!pBoxchn->isnChnAvail(nChn1))
      {
        Proc_MSG_common_Result(&XMLHeader, OnFail, "");
        return 1;
      }
    }
    //cmdid: 1-拍叉簧 2-保持 3-三方會議 4-切換通話 5-掛斷 6-完成轉接
    Send_SWTMSG_sendswitchcmd(pChn1->ChIndex, pChn1->DeviceID, cmdid);
    
    //Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  }
  return 0;
}
//發送撥打電話命令
int Proc_MSG_dial(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  XMLHeader.OnMsgId = MSG_ondial;
  strcpy(XMLHeader.OnMsgName, "ondial");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  Proc_MSG_common_Result(&XMLHeader, OnOutFail, "");
  return 0;
}
int Proc_MSG_appendfaxfile(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int result;
  
  XMLHeader.OnMsgId = MSG_onappendfaxfile;
  strcpy(XMLHeader.OnMsgName, "onappendfaxfile");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or not online");
    return 1;
  }
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  result = AppendFAX(nChnn, 
    AttrData->AttrValue[11], //filename
    AttrData->AttrValue[4], //faxcsid
    AttrData->AttrValue[5], //resolution
    atoi(AttrData->AttrValue[6]), //totalpages
    AttrData->AttrValue[7], //pagelist
    AttrData->AttrValue[8], //faxheader
    AttrData->AttrValue[9], //faxfooter
    AttrData->AttrValue[10] //barcode
    );
  
  if (result != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "append faxfile fail");
    return 1;
  }
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_bandagentchn(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAG, nChn;

  XMLHeader.OnMsgId = MSG_onbandagentchn;
  strcpy(XMLHeader.OnMsgName, "onbandagentchn");
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_bandagentchn_Result(&XMLHeader, 0xFFFF, OnFail, "param is error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_bandagentchn_Result(&XMLHeader, 0xFFFF, OnFail, "chn is error or is not online");
    return 1;
  }
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  if (strlen(AttrData->AttrValue[4]) > 0)
  {
    nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  }
  else if (strlen(AttrData->AttrValue[5]) > 0)
  {
    nAG = pAgentMng->GetnAGByPhoneNo(AttrData->AttrValue[5]);
  }
  else
  {
    Proc_MSG_bandagentchn_Result(&XMLHeader, 0xFFFF, OnFail, ""); //未設置坐席參數坐席號或綁定的外線號碼
    return 1;
  }
  if (!pAgentMng->isnAGAvail(nAG))
  {
    Proc_MSG_bandagentchn_Result(&XMLHeader, 0xFFFF, OnFail, ""); //未找到對應的坐席
    return 1;
  }
  if (pAG->GetSeatType() != SEATTYPE_RT_TEL)
  {
    Proc_MSG_bandagentchn_Result(&XMLHeader, 0xFFFF, OnFail, ""); //只能綁定遠端類型的坐席
    return 1;
  }
  nChn = pBoxchn->Get_nChn_By_LgChn(atoi(AttrData->AttrValue[6]), atoi(AttrData->AttrValue[7]));
  if (!pBoxchn->isnChnAvail(nChn))
  {
    Proc_MSG_bandagentchn_Result(&XMLHeader, 0xFFFF, OnFail, ""); //綁定的通道非法
    return 1;
  }
  pAG->m_Seat.nChn = nChn; //綁定遠程坐席與通道的關系
  pChn->nAG = nAG;
  SetAgentsvState(nAG, atoi(AttrData->AttrValue[8]));
  Proc_MSG_bandagentchn_Result(&XMLHeader, nChn, OnSuccess, "");

  return 0;
}
int Proc_MSG_sendcalltoagent(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int workerno, nResult, callinout, calltype;
  CStringX SeatNo; 
  CAgent *pAgent=NULL;
  char CallerNo[MAX_TELECODE_LEN], CalledNo[MAX_TELECODE_LEN];
  
  XMLHeader.OnMsgId = MSG_onsendcalltoagent;
  strcpy(XMLHeader.OnMsgName, "onsendcalltoagent");
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "param is error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or is not online");
    return 1;
  }
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  if (pIVRCfg->isSendACDCallToAgent == true)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "");
    return 1;
  }
  
  SeatNo = AttrData->AttrValue[4];
  workerno = atoi(AttrData->AttrValue[5]);
  callinout = atoi(AttrData->AttrValue[6]);
  calltype = atoi(AttrData->AttrValue[7]);
  
  if (SeatNo.Compare("0") == 0 && workerno == 0)
  {
    pAgent = GetAgentBynChn(nChnn);
  }
  else if (SeatNo.Compare("0") != 0)
  {
    pAgent = GetAgentbySeatNo(SeatNo, nResult);
  }
  else if (workerno != 0)
  {
    pAgent = GetAgentbyWorkerNo(workerno, nResult);
  }
  
  if (pAgent != NULL)
  {
    if (pAgent->isLogin() == true && pAgent->isTcpLinked() == true)
    {
      memset(CallerNo, 0, MAX_TELECODE_LEN);
      memset(CalledNo, 0, MAX_TELECODE_LEN);
      if (strlen(AttrData->AttrValue[8]) == 0)
      {
        strncpy(CallerNo, pChnn->CallerNo, MAX_TELECODE_LEN-1);
      } 
      else
      {
        strncpy(CallerNo, AttrData->AttrValue[8], MAX_TELECODE_LEN-1);
      }
      if (strlen(AttrData->AttrValue[9]) == 0)
      {
        strncpy(CalledNo, pChnn->CalledNo, MAX_TELECODE_LEN-1);
      } 
      else
      {
        strncpy(CalledNo, AttrData->AttrValue[9], MAX_TELECODE_LEN-1);
      }
      if (callinout == 1)
      {
        sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%ld' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='0' param='%s'/>", 
          pAgent->GetClientId()<<16, 0, pChnn->lgChnType, pChnn->lgChnNo, CallerNo, CalledNo, calltype, AttrData->AttrValue[10]);
        SendMsg2AG(pAgent->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
      }
      else
      {
        sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%ld' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='0' param='%s'/>", 
          pAgent->GetClientId()<<16, 0, pChnn->lgChnType, pChnn->lgChnNo, CalledNo, CallerNo, calltype, AttrData->AttrValue[10]);
        SendMsg2AG(pAgent->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
      }
      
      Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
      return 0;
    }
  }
  Proc_MSG_common_Result(&XMLHeader, OnFail, "have not login");

  return 0;
}
int Proc_MSG_Play(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int DtmfRuleId, PlayRuleId, mixtype, nSpeech, nFormat;
  int nChn, chntype, chnno, nResult = 0, nMGYear=0, nAMPMHour=0, nMGYYYY, nMG;
  char szParamStr[MAX_VAR_DATA_LEN], szTemp[256], szTemp1[256];
  CStringX ArrString1[10], ArrString2[2];
  
  XMLHeader.OnMsgId = MSG_onplay;
  strcpy(XMLHeader.OnMsgName, "onplay");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;
  
  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;
  
  if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }

  chntype = atoi(AttrData->AttrValue[7]);
  chnno = atoi(AttrData->AttrValue[8]);
  if (chntype == 0 && chnno == 0)
  {
    nChn = nChnn;
  }
  else
  {
    nChn = pBoxchn->Get_nChn_By_LgChn(chntype, chnno);
    if (!pBoxchn->isnChnAvail(nChn))
    {
      Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "dest nChn is invalid");
      return 1;
    }
    if (pChn->ssState != CHN_SNG_IN_TALK && pChn->ssState != CHN_SNG_OT_TALK)
    {
      Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "dest nChn can not play");
      return 1;
    }
  }
  
  if (strlen(AttrData->AttrValue[9]) == 0 
    && strlen(AttrData->AttrValue[10]) == 0 
    && strlen(AttrData->AttrValue[11]) == 0 
    && strlen(AttrData->AttrValue[12]) == 0 
    && strlen(AttrData->AttrValue[13]) == 0 
    && strlen(AttrData->AttrValue[14]) == 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "no data for play");
    return 1;
  }

  StopClearPlayDtmfBuf(nChn);
  
  if (nChn == nChnn)
  {
    SetSessionCmdAttr(nChn, XMLHeader.SessionId, XMLHeader.CmdAddr);
    SetcurPlaySessionParam(nChn, MSG_onplay, "onplay");
  }
  else
  {
    SetSessionCmdAttr(nChn, pChn->SessionNo, XMLHeader.CmdAddr&0xFFFF0000);
    SetcurPlaySessionParam(nChn, MSG_onplay, "onplay");
  }
  if (Check_Int_Str(AttrData->AttrValue[4], DtmfRuleId) == 0)
  {
    if (DtmfRuleId >= MAX_DTMFRULE_BUF)
      DtmfRuleId = 0;
    if (pChn->DtmfRule[DtmfRuleId].state == 0)
      DtmfRuleId = 0;
  } 
  else
  {
    DtmfRuleId = MAX_DTMFRULE_BUF-1;
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strcpy(szParamStr, MyUpper(AttrData->AttrValue[4]));

    pChn->DtmfRule[DtmfRuleId].state = 1;
    pChn->DtmfRule[DtmfRuleId].ruleid = DtmfRuleId;
    pChn->DtmfRule[DtmfRuleId].digits.Empty();
    pChn->DtmfRule[DtmfRuleId].digits = GetParamByName(szParamStr, "DIGITS", "0123456789*#");
    
    strcpy(szTemp, GetParamByName(szParamStr, "BREAKID", "TRUE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->DtmfRule[DtmfRuleId].breakid = 1;
    else
      pChn->DtmfRule[DtmfRuleId].breakid = 0;

    strcpy(szTemp, GetParamByName(szParamStr, "TRANSTYPE", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->DtmfRule[DtmfRuleId].transtype = 1;
    else
      pChn->DtmfRule[DtmfRuleId].transtype = 0;
    
    pChn->DtmfRule[DtmfRuleId].minlength = atoi(GetParamByName(szParamStr, "MINLENGTH", "1"));
    pChn->DtmfRule[DtmfRuleId].maxlength = atoi(GetParamByName(szParamStr, "MAXLENGTH", "1"));
    pChn->DtmfRule[DtmfRuleId].timeout = atoi(GetParamByName(szParamStr, "TIMEOUT", "0"));
    pChn->DtmfRule[DtmfRuleId].termdigits.Empty();
    pChn->DtmfRule[DtmfRuleId].termdigits = GetParamByName(szParamStr, "TERMDIGITS", "");
    pChn->DtmfRule[DtmfRuleId].onebitdigits.Empty();
    pChn->DtmfRule[DtmfRuleId].onebitdigits = GetParamByName(szParamStr, "ONEBITDIGITS", "");
    pChn->DtmfRule[DtmfRuleId].pbxpredigits.Empty();
    pChn->DtmfRule[DtmfRuleId].pbxpredigits = GetParamByName(szParamStr, "PBXPREDIGITS", "");
    pChn->DtmfRule[DtmfRuleId].firstdigitdelay = atoi(GetParamByName(szParamStr, "FIRSTDELAY", "0"));

    strcpy(szTemp, GetParamByName(szParamStr, "TERMID", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->DtmfRule[DtmfRuleId].termid = 1;
    else
      pChn->DtmfRule[DtmfRuleId].termid = 0;

    //特殊字冠接收按鍵的最大長度: specmaxlen=分機字冠1,最大長度1|分機字冠2,最大長度2|...
    strcpy(szTemp, GetParamByName(szParamStr, "SPECMAXLEN", ""));
    if (strlen(szTemp) > 0)
    {
      int b=0;
      int num1 = SplitString(szTemp, '|', 10, ArrString1);
      for (int a=0; a<num1; a++)
      {
        if (strlen(ArrString1[a].C_Str()) > 0)
        {
          int num2 = SplitString(ArrString1[a].C_Str(), ',', 2, ArrString2);
          if (num2 == 2)
          {
            if (strlen(ArrString2[0].C_Str()) > 0 && strlen(ArrString2[1].C_Str()) > 0)
            {
              pChn->DtmfRule[DtmfRuleId].specpredigitsmaxlength[b] = (UC)atoi(ArrString2[1].C_Str());
              if (pChn->DtmfRule[DtmfRuleId].specpredigitsmaxlength[b] > 0)
              {
                pChn->DtmfRule[DtmfRuleId].specpredigits[b] = ArrString2[0];
                b++;
              }
            }
          }
        }
      }
    }
  }

  if (Check_Int_Str(AttrData->AttrValue[5], PlayRuleId) == 0)
  {
    if (PlayRuleId >= MAX_PLAYRULE_BUF)
      PlayRuleId = 0;
    if (pChn->PlayRule[PlayRuleId].state == 0)
      PlayRuleId = 0;
  } 
  else
  {
    PlayRuleId = MAX_PLAYRULE_BUF-1;
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strcpy(szParamStr, MyUpper(AttrData->AttrValue[5]));

    pChn->PlayRule[PlayRuleId].state = 1;
    pChn->PlayRule[PlayRuleId].ruleid = PlayRuleId;
    
    strcpy(szTemp, GetParamByName(szParamStr, "FORMAT", "WAVE"));
    
    if (stricmp(szTemp, "pcma") == 0)
    {
      nFormat = AUDIO_FORMAT_pcma;
    } 
    else if (stricmp(szTemp, "pcmmu") == 0)
    {
      nFormat = AUDIO_FORMAT_pcmmu;
    }
    else if (stricmp(szTemp, "adpcm") == 0)
    {
      nFormat = AUDIO_FORMAT_adpcm;
    }
    else if (stricmp(szTemp, "wave") == 0)
    {
      nFormat = AUDIO_FORMAT_wave;
    }
    else if (stricmp(szTemp, "mp3") == 0)
    {
      nFormat = AUDIO_FORMAT_mp3;
    }
    else if (stricmp(szTemp, "gsm") == 0)
    {
      nFormat = AUDIO_FORMAT_gsm;
    }
    else if (stricmp(szTemp, "vox") == 0)
    {
      nFormat = AUDIO_FORMAT_vox;
    }
    else
    {
      nFormat = 0;
    }
    pChn->PlayRule[PlayRuleId].format = nFormat;
    
    pChn->PlayRule[PlayRuleId].repeat = atoi(GetParamByName(szParamStr, "REPEAT", "1"));
    pChn->PlayRule[PlayRuleId].duration = atoi(GetParamByName(szParamStr, "DURATION", "0"));
    
    strcpy(szTemp, GetParamByName(szParamStr, "BROADCAST", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->PlayRule[DtmfRuleId].broadcast = 1;
    else
      pChn->PlayRule[DtmfRuleId].broadcast = 0;
    
    pChn->PlayRule[PlayRuleId].volume = atoi(GetParamByName(szParamStr, "VOLUME", "0"));
    
    strcpy(szTemp, GetParamByName(szParamStr, "SPEECH", "chinese"));
    if (stricmp(szTemp, "english") == 0)
      pChn->PlayRule[PlayRuleId].speech = 1;
    else if (stricmp(szTemp, "chinese") == 0)
      pChn->PlayRule[PlayRuleId].speech = 2;
    else if (stricmp(szTemp, "localism") == 0)
      pChn->PlayRule[PlayRuleId].speech = 3;
    else if (stricmp(szTemp, "guangdong") == 0)
      pChn->PlayRule[PlayRuleId].speech = 4;
    else if (stricmp(szTemp, "minnan") == 0)
      pChn->PlayRule[PlayRuleId].speech = 5;
    else if (stricmp(szTemp, "french") == 0)
      pChn->PlayRule[PlayRuleId].speech = 6;
    else if (stricmp(szTemp, "portuguese") == 0)
      pChn->PlayRule[PlayRuleId].speech = 7;
    else if (stricmp(szTemp, "arabic") == 0)
      pChn->PlayRule[PlayRuleId].speech = 8;
    else
      pChn->PlayRule[PlayRuleId].speech = 2;

    strcpy(szTemp, GetParamByName(szParamStr, "VOICE", "female"));
    if (stricmp(szTemp, "female") == 0)
      pChn->PlayRule[PlayRuleId].voice = 1;
    else if (stricmp(szTemp, "male") == 0)
      pChn->PlayRule[PlayRuleId].voice = 2;
    else if (stricmp(szTemp, "child") == 0)
      pChn->PlayRule[PlayRuleId].voice = 3;
    else
      pChn->PlayRule[PlayRuleId].voice = 0;
    
    pChn->PlayRule[PlayRuleId].pause = atoi(GetParamByName(szParamStr, "PAUSE", "0"));
    
    strcpy(szTemp, GetParamByName(szParamStr, "BROADCAST", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->PlayRule[DtmfRuleId].error = 1;
    else
      pChn->PlayRule[DtmfRuleId].error = 0;
    
    pChn->curSpeech = pChnn->PlayRule[PlayRuleId].speech;
  }

  //設置收碼放音規則
  if (SetDTMFPlayRule(nChn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  nSpeech = pChn->PlayRule[PlayRuleId].speech;
  //合成串
  for (int i=0; i<6; i++)
  {
    nMGYear = 0;
    nAMPMHour = 0;
    if (strlen(AttrData->AttrValue[9+i]) == 0)
    {
      continue;
    }
    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strncpy(szTemp, AttrData->AttrValue[9+i], 21);
    strcpy(szTemp, MyUpper(szTemp));
    
    if (strncmp(szTemp, "FILE=", 5) == 0)
    {
      mixtype = 1;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][5]);
    }
    else if (strncmp(szTemp, "DATETIME=", 9) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][9]);
      FormatDateTimeStr(szTemp, szParamStr, nMGYear);
    }
    else if (strncmp(szTemp, "DATE=", 5) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][11]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 21)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][11], 10);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;

      memset(szTemp, 0, 256);
      memset(szTemp1, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[9+i][10], 20);
      int len = strlen(szTemp);
      int k=0, id=0, l=0, y=0, m=0, d=0;
      char c;
      bool bErr=false;
      if (len >= 6 && len <= 9)
      {
        for (int j=0; j<=len; j++)
        {
          c = szTemp[j];
          if (id == 0)
          {
            //年
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              y++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (y == 2)
              {
                szTemp1[3] = '-';
                szTemp1[2] = szTemp1[1];
                szTemp1[1] = szTemp1[0];
                szTemp1[0] = '0';
                k = 4;
                id = 1;
              }
              else if (y == 3)
              {
                szTemp1[k] = '-';
                k++;
                id = 1;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 1)
          {
            //月
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              m++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (m == 1)
              {
                szTemp1[6] = '-';
                szTemp1[5] = szTemp1[4];
                szTemp1[4] = '0';
                k = 7;
                id = 2;
              }
              else if (m == 2)
              {
                szTemp1[k] = '-';
                k++;
                id = 2;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 2)
          {
            //日
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              d++;
            }
            else if (c == '\0')
            {
              if (d == 1)
              {
                szTemp1[8] = szTemp1[7];
                szTemp1[7] = '0';
                k = 9;
                id = 3;
              }
              else if (d == 2)
              {
                k++;
                id = 3;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
        }
      }

      if (strlen(szTemp1) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, szTemp1, 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strncat(szTemp, &szTemp1[3], 6);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 15)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][8], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMM=", 6) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 11)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][6], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MG=", 3) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][3], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-MM-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MMDD=", 5) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[9+i][5], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][7], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "TIME=", 5) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "AMPM=", 5) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HHMISS=", 7) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMISS=", 7) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "HHMI=", 5) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMI=", 5) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 5);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][12]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 11);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][9], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "_");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, "YYYY-");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, ":SS_W");
      }
    }
    else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 8);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 8);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "DD_HH=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 5);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD=", 3) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 2);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "W=", 2) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][2]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
      strncat(szParamStr, &szTemp1[20], 1);
    }
    else if (strncmp(szTemp, "HH:MI=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "AP:MI=", 6) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MI:SS=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 5);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HH=", 3) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "AP=", 3) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "MI=", 3) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 2);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MONEY=", 6) == 0)
    {
      mixtype = 3;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][6]);
    }
    else if (strncmp(szTemp, "VALUE=", 6) == 0)
    {
      mixtype = 4;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][6]);
    }
    else if (strncmp(szTemp, "DIGIT=", 6) == 0)
    {
      mixtype = 5;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][6]);
    }
    else if (strncmp(szTemp, "CHAR=", 5) == 0)
    {
      mixtype = 6;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][5]);
    }
    else
    {
      mixtype = 1;
      strcpy(szParamStr, AttrData->AttrValue[9+i]);
    }
    
    nResult = AddMixToPlayBuf(nSpeech, nChn, mixtype, szParamStr, ErrMsg, nMGYear, nAMPMHour);
  }
  
  if (nResult == 1)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  pChn->curPlayData.PlayState = 1;
  pChn->PlayedTimer = 0;
  if (Start_ChnPlay(nChn) != 0)
  {
    return 1;
  }
  pChn->curPlayData.CanPlayId = 1;
  
  return 0;
}
//話務員登錄擴展指令
int Proc_MSG_workerloginex(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAG, nAG1, workerno, groupno=0, workergrade=0, level=0, groupnum, dutyno;
  CStringX ArrString1[MAX_GROUP_NUM_FORWORKER], ArrString2[MAX_GROUP_NUM_FORWORKER];
  
  XMLHeader.OnMsgId = MSG_onworkerlogin;
  strcpy(XMLHeader.OnMsgName, "onworkerlogin");
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  if (!pAgentMng->isnAGAvail(nAG))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "SeatNo not Login before");
    return 0;
  }
  
  workerno = atoi(AttrData->AttrValue[5]); //話務員工號
  dutyno = atoi(AttrData->AttrValue[13]);
  nAG1 = pAgentMng->GetnAGByWorkerNo(workerno);
  if (pAgentMng->isnAGAvail(nAG1))
  {
    if (nAG1 != nAG)
    {
      Proc_MSG_common_Result(&XMLHeader, OnFail, "the worker had login");
      return 1;
    }
  }
  if (1 == atoi(AttrData->AttrValue[11]))
  {
    pAG->m_Worker.ClearWork();
  }
  else if (pIVRCfg->ClearCallCountType == 3 && workerno != pAG->m_Worker.PreWorkerNo)
  {
    pAG->m_Worker.ClearWork();
  }
  else if (pIVRCfg->ClearCallCountType == 4 && dutyno != pAG->m_Worker.PreDutyNo)
  {
    pAG->m_Worker.ClearWork();
  }
  if (workerno == pAG->m_Worker.PreWorkerNo)
    WriteWorkerLogoutRecord(pAG);

  pAG->m_Worker.ClearWorkerParam();
  workergrade = atoi(AttrData->AttrValue[9]); //話務員類別
  pAG->m_Worker.WorkerLogin(workerno, AttrData->AttrValue[6], workergrade);
  
  ClearTheSameAccountNo(AttrData->AttrValue[16]);
  SaveWorkerAccountNo(nAG, AttrData->AttrValue[16]);
  pAG->m_Worker.DepartmentNo = AttrData->AttrValue[17];

  groupnum = SplitTxtLine(AttrData->AttrValue[8], MAX_GROUP_NUM_FORWORKER, ArrString1);
  SplitTxtLine(AttrData->AttrValue[10], MAX_GROUP_NUM_FORWORKER, ArrString2);
  for (int i=0; i<groupnum; i++)
  {
    groupno = atoi(ArrString1[i].C_Str());
    if (ArrString2[i].GetLength() > 0)
      level = atoi(ArrString2[i].C_Str());
    else
      level = 1;
    if (groupno > 0 && groupno < MAX_AG_GROUP)
    {
      pAG->m_Worker.SetWorkerGroupNo(groupno, level);
      pAgentMng->WorkerGroup[groupno].state = 1;
    }
  }

  pAG->m_Worker.WorkerCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
  pAG->m_Worker.WorkerCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
  pAG->m_Worker.WorkerCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
  pAG->m_Worker.WorkerCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長
  
  pAG->m_Seat.SeatCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
  pAG->m_Seat.SeatCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
  pAG->m_Seat.SeatCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
  pAG->m_Seat.SeatCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長

  pAG->m_Worker.DutyNo = dutyno;
  pAG->m_Worker.AutoRecordId = atoi(AttrData->AttrValue[14]);
  pAG->m_Worker.CallDirection = atoi(AttrData->AttrValue[15]);

//   pAG->ChangeStatus(AG_STATUS_LOGIN);
//   WriteWorkerLoginStatus(pAG);
//   WriteSeatStatus(pAG, AG_STATUS_LOGIN, 0);

  if (1 == atoi(AttrData->AttrValue[12]))
  {
    pAG->m_Worker.DisturbId = 1;
    pAG->m_Worker.LeaveId = 0;
  } 
  else
  {
    pAG->m_Worker.DisturbId = pAG->m_Worker.OldDisturbId;
    pAG->m_Worker.LeaveId = pAG->m_Worker.OldLeaveId;
  }
  if (pAG->m_Worker.DisturbId == 0 && pAG->m_Worker.LeaveId == 0)
  {
    pAG->m_Worker.TelLogInOutFlag = 1;
    //pAG->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
  }
  else
  {
    pAG->m_Worker.TelLogInOutFlag = 3;
    //pAG->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_BUSY);
  }
  if (pIVRCfg->isAgentLoginSwitch == true 
    && pAG->m_Seat.LoginSwitchID == 1)
  {
    Send_SWTMSG_setagentstatus(0, pAG->GetSeatNo().C_Str(), AttrData->AttrValue[5], pAG->m_Seat.LoginSwitchGroupID, pAG->m_Seat.LoginSwitchPSW, AG_AM_LOG_IN);
  }

  pAG->m_Worker.PreWorkerNo = workerno;
  pAG->m_Worker.PreGroupNo = pAG->GetGroupNo();
  memcpy(pAG->m_Worker.PreGroupNoList, pAG->m_Worker.GroupNo, sizeof(pAG->m_Worker.GroupNo));
  pAG->m_Worker.PreDutyNo = dutyno;

  DispAgentStatus(nAG);
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
//話務員退出擴展指令
int Proc_MSG_workerlogoutex(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAG=-1, nWorkerNo;
  
  XMLHeader.OnMsgId = MSG_onworkerlogout;
  strcpy(XMLHeader.OnMsgName, "onworkerlogout");
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  if (strlen(AttrData->AttrValue[4]) > 0 && strcmp(AttrData->AttrValue[4], "0") != 0)
  {
    nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  }
  else
  {
    nWorkerNo = atoi(AttrData->AttrValue[5]);
    if (nWorkerNo > 0)
    {
      nAG = pAgentMng->GetnAGByWorkerNo(nWorkerNo);
    }
  }
  if (!pAgentMng->isnAGAvail(nAG))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "SeatNo not Login before");
    return 0;
  }
  
  int nGroupNo = pAG->GetGroupNo();
  if (pAG->m_Worker.oldWorkerNo == 0)
  {
    pAG->m_Worker.Logout();
    pAG->m_Worker.ClearState();
  }
  else
  {
    pAG->m_Worker.OldWorkerLogin();
  }
  DispAgentStatus(pAG->nAG);
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");

//   WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
//   AgentStatusCount();
//   SendOneGroupStatusMeteCount(pAG->nAG);
//   SendSystemStatusMeteCount();
  pAG->m_Worker.TelLogInOutFlag = 2;

  return 0;
}
//取坐席登錄的話務員信息指令
int Proc_MSG_getseatlogindata(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAG;
  
  XMLHeader.OnMsgId = MSG_ongetseatlogindata;
  strcpy(XMLHeader.OnMsgName, "ongetseatlogindata");
  
  if (Check_common_MSG(&XMLHeader) != 0)
    return 1;
  
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  if (!pAgentMng->isnAGAvail(nAG))
  {
    SendgetseatlogindataResult(nChnn, nAG, OnFail, "SeatNo not Login before");
    return 0;
  }
  
  SendgetseatlogindataResult(nChnn, nAG, OnSuccess, "");
  return 0;
}
int Proc_MSG_getidleseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  CACDRule ACDRule;
  CStringX ArrGroupNo[MAX_GROUP_NUM_FORWORKER];
  CStringX ArrLevel[MAX_GROUP_NUM_FORWORKER];
  int i, num, nGroupNo, nWorkerNo, nLogins, nIdles, nTalks;
  char SeatNo[32], szTemp[128];
  
  XMLHeader.OnMsgId = MSG_ongetidleseat;
  strcpy(XMLHeader.OnMsgName, "ongetidleseat");
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_ongetidleseat_Result(&XMLHeader, OnFail, "0", 0, 0, 0, "recv msg param error");
    return 1;
  }

  nGroupNo = atoi(AttrData->AttrValue[8]);
  if (nGroupNo > 96)
  {
    CACDSplit *pACDSplit = gACDSplitMng.GetACDSplit(AttrData->AttrValue[8]);
    if (pACDSplit)
    {
      Proc_MSG_ongetidleseat_Result(&XMLHeader, OnSuccess, "0", 0, pACDSplit->m_nLoginAgents, pACDSplit->m_nAvailAgents, "");
    }
    else
    {
      Proc_MSG_ongetidleseat_Result(&XMLHeader, OnFail, "0", 0, 0, 0, "the acd group is not exist");
    }
    return 0;
  }

  ACDRule.TranCallFailCallBack = 0;
  ACDRule.AcdedCount = 0;
  ACDRule.SeatType = (UC)(atoi(AttrData->AttrValue[6]));
  ACDRule.SeatNo = AttrData->AttrValue[4];
  ACDRule.WorkerNo = atoi(AttrData->AttrValue[5]);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER; i ++)
  {
    ACDRule.GroupNo[i] = 0xFF;
    ACDRule.Level[i] = 0;
    ACDRule.NowLevel[i] = 0;
  }
  num = SplitTxtLine(AttrData->AttrValue[8], MAX_GROUP_NUM_FORWORKER, ArrGroupNo);
  SplitTxtLine(AttrData->AttrValue[10], MAX_GROUP_NUM_FORWORKER, ArrLevel);
  for (i = 0; i < MAX_GROUP_NUM_FORWORKER && i < num; i ++)
  {
    ACDRule.GroupNo[i] = atoi(ArrGroupNo[i].C_Str());
    if (ACDRule.GroupNo[i] >= MAX_AG_GROUP)
      ACDRule.GroupNo[i] = 0;
    ACDRule.Level[i] = atoi(ArrLevel[i].C_Str());
    if (ACDRule.Level[i] > MAX_AG_LEVEL)
      ACDRule.Level[i] = 0;
    ACDRule.NowLevel[i] = ACDRule.Level[i];
  }
  ACDRule.AcdedGroupNo = 0;
  ACDRule.AcdedGroupIndex = 0;
  ACDRule.AcdedLevel = 0;
  ACDRule.AcdRule = (UC)atoi(AttrData->AttrValue[7]);
  ACDRule.LevelRule = (UC)atoi(AttrData->AttrValue[11]);
  ACDRule.CallMode = 0;
  ACDRule.SeatGroupNo = (UC)atoi(AttrData->AttrValue[9]);
  ACDRule.WaitTimeLen = 0;
  ACDRule.RingTimeLen = 0;
  ACDRule.BusyWaitId = 0;
  ACDRule.Priority = 0;
  
  ACDRule.QueueTime = time(0);
  ACDRule.StartRingTime = time(0);
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    ACDRule.AcdednAGList[i] = -1;
  }
  CAgent *pAgent=GetIdelAgentForCallOut(ACDRule);
  if (pAgent == NULL)
  {
    strcpy(SeatNo, "0");
    nWorkerNo = 0;
  }
  else
  {
    strcpy(SeatNo, pAgent->GetSeatNo().C_Str());
    nWorkerNo = pAgent->GetWorkerNo();
    if (atoi(AttrData->AttrValue[12]) == 1)
    {
      pAgent->LockId = 1;
      pAgent->timer = 0;
      DispAgentStatus(pAgent->nAG);
    }
  }

  pAgentMng->CountLoginIdleAgent(ACDRule.GroupNo, nLogins, nIdles, nTalks); //2016-05-10

  //2016-05-10
  if (pIVRCfg->GetIdleSeatCountType == 1)
  {
    Proc_MSG_ongetidleseat_Result(&XMLHeader, OnSuccess, SeatNo, nWorkerNo, nLogins, nIdles, "");
  }
  else
  {
    sprintf(szTemp, "%d,%d",nIdles, nTalks);
    Proc_MSG_ongetidleseat_Result(&XMLHeader, OnSuccess, SeatNo, nWorkerNo, nLogins, szTemp, "");
  }
  return 0;
}
int Proc_MSG_unlockseat(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nAG=-1, nWorkerNo;
  
  XMLHeader.OnMsgId = MSG_onunlockseat;
  strcpy(XMLHeader.OnMsgName, "onunlockseat");
  
  if (strlen(AttrData->AttrValue[4]) > 0 && strcmp(AttrData->AttrValue[4], "0") != 0)
  {
    nAG = pAgentMng->GetnAGBySeatNo(AttrData->AttrValue[4]);
  }
  else
  {
    nWorkerNo = atoi(AttrData->AttrValue[5]);
    if (nWorkerNo > 0)
    {
      nAG = pAgentMng->GetnAGByWorkerNo(nWorkerNo);
    }
  }
  if (!pAgentMng->isnAGAvail(nAG))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "SeatNo or WorkerNo is not exist");
    return 0;
  }
  pAG->LockId = 0;
  DispAgentStatus(pAG->nAG);
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_updateservicescore(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  XMLHeader.OnMsgId = MSG_onupdatesrvcore;
  strcpy(XMLHeader.OnMsgName, "onupdatesrvcore");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or not online");
    return 1;
  }
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_updatecdrfield(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  XMLHeader.OnMsgId = MSG_onupdatecdrfield;
  strcpy(XMLHeader.OnMsgName, "onupdatecdrfield");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or not online");
    return 1;
  }

  if (stricmp(AttrData->AttrValue[5], "CallerNo") == 0)
  {
    if (pIVRCfg->isQueryCustomerByWebservice == true && pTcpLink->IsGWConnected())
    {
      SendQueryCustomer2Webservice(AttrData->AttrValue[6], "", pIVRCfg->QueryCustomerWebserviceAddr, pIVRCfg->QueryCustomerWebserviceParam);
    }
  }
  
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_updatesysparam(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  XMLHeader.OnMsgId = MSG_onupdatesysparam;
  strcpy(XMLHeader.OnMsgName, "onupdatesysparam");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 1) == false)
    return 1;
  
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
    return 1;
  }
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
  return 0;
}
int Proc_MSG_setforwarding(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int nChn;
  int forwardtype, fwdonoff, nTranType=0;
  
  nChn = pBoxchn->Get_ChnNo_By_DeviceID(AttrData->AttrValue[4]);
  
  if (pBoxchn->isnChnAvail(nChn))
  {
    pChn->SetForwardingSessionNo = XMLHeader.SessionId; //設置呼叫轉移會話序列號 2016-07-19
    pChn->SetForwardingCmdAddr = XMLHeader.CmdAddr; //2016-07-19
  }
  else
  {
    g_SetForwardingSessionNo = XMLHeader.SessionId;
    g_SetForwardingCmdAddr = XMLHeader.CmdAddr;
  }
  
  forwardtype = atoi(AttrData->AttrValue[5]);
  fwdonoff = atoi(AttrData->AttrValue[6]);
  if (fwdonoff == 1)
  {
    nTranType = forwardtype;
  } 
  else
  {
    switch (forwardtype)
    {
    case 0:
      nTranType = 0;
      break;
    case 1:
      nTranType = 10;
      break;
    case 2:
      nTranType = 20;
      break;
    case 3:
      nTranType = 30;
      break;
    case 5:
      nTranType = 50;
      break;
    }
  }
  
  Send_SWTMSG_settranphone(nChn, AttrData->AttrValue[4], nTranType, AttrData->AttrValue[7]);
  
  return 0;
}
//特殊功能呼叫，如：監聽、強插 2015-06-14
int Proc_MSG_callfunction(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int i, j, calltype, workerno;
  CStringX seatno;
  int nAGn, nAG1, nChn1, nChn2;
  CAgent *pAgent=NULL, *pAgent1=NULL;
  
  XMLHeader.OnMsgId = MSG_oncallfunction;
  strcpy(XMLHeader.OnMsgName, "oncallfunction");
  if (XMLHeader.ErrorId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "recv msg param error");
    return 1;
  }
  if (XMLHeader.OnlineId != 0)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "chn is error or is not online");
    return 1;
  }
  nChnn = XMLHeader.nChn;
  SetSessionCmdAttr(nChnn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  
  pAgent = GetAgentBynChn(nChnn);
  if (pAgent == NULL)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the seat have not login");
    return 0;
  }
  nAGn = pAgent->nAG;
  
  calltype = atoi(AttrData->AttrValue[4]);
  seatno = AttrData->AttrValue[5];
  workerno = atoi(AttrData->AttrValue[6]);
  
  nAG1 = GetnAGbyWorkerNoSeatNo(seatno.C_Str(), workerno);
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "have not seat for call");
    return 1;
  }
  if (!pAG1->isThesvState(AG_SV_CONN))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the seat not talking");
    return 1;
  }
  nChn1 = pAG1->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn1))
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "the seat not banging ch");
    return 1;
  }
  if (pAGn->m_Worker.WorkerGrade < 3)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "permission denied");
    return 1;
  }
  if (calltype == 1)
  {
    for (i = 2; i <= MAX_LINK_CHN_NUM; i ++)
    {
      if (pChn1->LinkType[i] == 0)
      {
        break;
      }
    }
    if (i < MAX_LINK_CHN_NUM)
    {
      if (RouterMonitor(nChnn, nChn1, 1) == 0)
      {
        nChn2 = pChn1->LinkChn[0];
        if (pBoxchn->isnChnAvail(nChn2))
        {
          for (j = 2; j <= MAX_LINK_CHN_NUM; j ++)
          {
            if (pChn2->LinkType[j] == 0)
            {
              break;
            }
          }
          //2012-06-20
          if (j < MAX_LINK_CHN_NUM)
          {
            if (RouterMonitor(nChnn, nChn2, 1) == 0)
            {
              pChn2->LinkType[j] = 3;
              pChn2->LinkChn[j] = nChnn;
              pChnn->LinkType[1] = 2;
              pChnn->LinkChn[1] = nChn2;
            }
          }
          Proc_MSG_common_Result(&XMLHeader, OnSuccess, "");
        }
        else
        {
          SendSeatListenFailResult(nAGn, "", OnSuccess, "");
        }
        pChnn->LinkType[0] = 2;
        pChnn->LinkChn[0] = nChn1;
        pChn1->LinkType[i] = 3;
        pChn1->LinkChn[i] = nChnn;
        return 0;
      }
    }
    Proc_MSG_common_Result(&XMLHeader, OnFail, "listen fail");
    return 1;
  }
  else if (calltype == 2)
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "calltype is not valid");
    return 1;
  }
  else
  {
    Proc_MSG_common_Result(&XMLHeader, OnFail, "calltype is not valid");
    return 1;
  }
}
int Proc_MSG_Playex(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  int DtmfRuleId, PlayRuleId, mixtype, nSpeech, nFormat;
  int nChn, nResult = 0, nMGYear=0, nAMPMHour=0, nMGYYYY, nMG;
  char szParamStr[MAX_VAR_DATA_LEN], szTemp[256], szTemp1[256];
  CStringX ArrString1[10], ArrString2[2];
  
  XMLHeader.OnMsgId = MSG_onplayex;
  strcpy(XMLHeader.OnMsgName, "onplayex");
  if (CheckTheSameSessionId(&XMLHeader, nChnn, 3) == false)
    return 1;
  
  if (Check_play_MSG(&XMLHeader) != 0)
    return 1;
  
  if ((pChnn->LinkType[0] != 0 && pChnn->LinkType[0] != 6 && pChnn->LinkType[0] != 7)
    || (pBoxconf->isnCfcAvail(pChnn->CfcNo) || pChnn->JoinType != 0))
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "the chn is talkwith or in conf");
    return 1;
  }

  nChn = nChnn;
  
  if (strlen(AttrData->AttrValue[9]) == 0 
    && strlen(AttrData->AttrValue[10]) == 0 
    && strlen(AttrData->AttrValue[11]) == 0 
    && strlen(AttrData->AttrValue[12]) == 0 
    && strlen(AttrData->AttrValue[13]) == 0 
    && strlen(AttrData->AttrValue[14]) == 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", "no data for play");
    return 1;
  }

  StopClearPlayDtmfBuf(nChn);
  
  SetSessionCmdAttr(nChn, XMLHeader.SessionId, XMLHeader.CmdAddr);
  SetcurPlaySessionParam(nChn, MSG_onplayex, "onplayex");

  if (Check_Int_Str(AttrData->AttrValue[4], DtmfRuleId) == 0)
  {
    if (DtmfRuleId >= MAX_DTMFRULE_BUF)
      DtmfRuleId = 0;
    if (pChn->DtmfRule[DtmfRuleId].state == 0)
      DtmfRuleId = 0;
  } 
  else
  {
    DtmfRuleId = MAX_DTMFRULE_BUF-1;
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strcpy(szParamStr, MyUpper(AttrData->AttrValue[4]));

    pChn->DtmfRule[DtmfRuleId].state = 1;
    pChn->DtmfRule[DtmfRuleId].ruleid = DtmfRuleId;
    pChn->DtmfRule[DtmfRuleId].digits.Empty();
    pChn->DtmfRule[DtmfRuleId].digits = GetParamByName(szParamStr, "DIGITS", "0123456789*#");
    
    strcpy(szTemp, GetParamByName(szParamStr, "BREAKID", "TRUE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->DtmfRule[DtmfRuleId].breakid = 1;
    else
      pChn->DtmfRule[DtmfRuleId].breakid = 0;

    strcpy(szTemp, GetParamByName(szParamStr, "TRANSTYPE", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->DtmfRule[DtmfRuleId].transtype = 1;
    else
      pChn->DtmfRule[DtmfRuleId].transtype = 0;
    
    pChn->DtmfRule[DtmfRuleId].minlength = atoi(GetParamByName(szParamStr, "MINLENGTH", "1"));
    pChn->DtmfRule[DtmfRuleId].maxlength = atoi(GetParamByName(szParamStr, "MAXLENGTH", "1"));
    pChn->DtmfRule[DtmfRuleId].timeout = atoi(GetParamByName(szParamStr, "TIMEOUT", "0"));
    pChn->DtmfRule[DtmfRuleId].termdigits.Empty();
    pChn->DtmfRule[DtmfRuleId].termdigits = GetParamByName(szParamStr, "TERMDIGITS", "");
    pChn->DtmfRule[DtmfRuleId].onebitdigits.Empty();
    pChn->DtmfRule[DtmfRuleId].onebitdigits = GetParamByName(szParamStr, "ONEBITDIGITS", "");
    pChn->DtmfRule[DtmfRuleId].pbxpredigits.Empty();
    pChn->DtmfRule[DtmfRuleId].pbxpredigits = GetParamByName(szParamStr, "PBXPREDIGITS", "");
    pChn->DtmfRule[DtmfRuleId].firstdigitdelay = atoi(GetParamByName(szParamStr, "FIRSTDELAY", "0"));

    strcpy(szTemp, GetParamByName(szParamStr, "TERMID", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->DtmfRule[DtmfRuleId].termid = 1;
    else
      pChn->DtmfRule[DtmfRuleId].termid = 0;

    //特殊字冠接收按鍵的最大長度: specmaxlen=字冠1,最大收碼長度1|字冠2,最大收碼長度2|...
    strcpy(szTemp, GetParamByName(szParamStr, "SPECMAXLEN", ""));
    if (strlen(szTemp) > 0)
    {
      int b=0;
      int num1 = SplitString(szTemp, '|', 10, ArrString1);
      for (int a=0; a<num1; a++)
      {
        if (strlen(ArrString1[a].C_Str()) > 0)
        {
          int num2 = SplitString(ArrString1[a].C_Str(), ',', 2, ArrString2);
          if (num2 == 2)
          {
            if (strlen(ArrString2[0].C_Str()) > 0 && strlen(ArrString2[1].C_Str()) > 0)
            {
              pChn->DtmfRule[DtmfRuleId].specpredigitsmaxlength[b] = (UC)atoi(ArrString2[1].C_Str());
              if (pChn->DtmfRule[DtmfRuleId].specpredigitsmaxlength[b] > 0)
              {
                pChn->DtmfRule[DtmfRuleId].specpredigits[b] = ArrString2[0];
                b++;
              }
            }
          }
        }
      }
    }
  }

  if (Check_Int_Str(AttrData->AttrValue[5], PlayRuleId) == 0)
  {
    if (PlayRuleId >= MAX_PLAYRULE_BUF)
      PlayRuleId = 0;
    if (pChn->PlayRule[PlayRuleId].state == 0)
      PlayRuleId = 0;
  } 
  else
  {
    PlayRuleId = MAX_PLAYRULE_BUF-1;
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strcpy(szParamStr, MyUpper(AttrData->AttrValue[5]));

    pChn->PlayRule[PlayRuleId].state = 1;
    pChn->PlayRule[PlayRuleId].ruleid = PlayRuleId;
    
    strcpy(szTemp, GetParamByName(szParamStr, "FORMAT", "WAVE"));
    
    if (stricmp(szTemp, "pcma") == 0)
    {
      nFormat = AUDIO_FORMAT_pcma;
    } 
    else if (stricmp(szTemp, "pcmmu") == 0)
    {
      nFormat = AUDIO_FORMAT_pcmmu;
    }
    else if (stricmp(szTemp, "adpcm") == 0)
    {
      nFormat = AUDIO_FORMAT_adpcm;
    }
    else if (stricmp(szTemp, "wave") == 0)
    {
      nFormat = AUDIO_FORMAT_wave;
    }
    else if (stricmp(szTemp, "mp3") == 0)
    {
      nFormat = AUDIO_FORMAT_mp3;
    }
    else if (stricmp(szTemp, "gsm") == 0)
    {
      nFormat = AUDIO_FORMAT_gsm;
    }
    else if (stricmp(szTemp, "vox") == 0)
    {
      nFormat = AUDIO_FORMAT_vox;
    }
    else
    {
      nFormat = 0;
    }
    pChn->PlayRule[PlayRuleId].format = nFormat;
    
    pChn->PlayRule[PlayRuleId].repeat = atoi(GetParamByName(szParamStr, "REPEAT", "1"));
    pChn->PlayRule[PlayRuleId].duration = atoi(GetParamByName(szParamStr, "DURATION", "0"));
    
    strcpy(szTemp, GetParamByName(szParamStr, "BROADCAST", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->PlayRule[DtmfRuleId].broadcast = 1;
    else
      pChn->PlayRule[DtmfRuleId].broadcast = 0;
    
    pChn->PlayRule[PlayRuleId].volume = atoi(GetParamByName(szParamStr, "VOLUME", "0"));
    
    strcpy(szTemp, GetParamByName(szParamStr, "SPEECH", "chinese"));
    if (stricmp(szTemp, "english") == 0)
      pChn->PlayRule[PlayRuleId].speech = 1;
    else if (stricmp(szTemp, "chinese") == 0)
      pChn->PlayRule[PlayRuleId].speech = 2;
    else if (stricmp(szTemp, "localism") == 0)
      pChn->PlayRule[PlayRuleId].speech = 3;
    else if (stricmp(szTemp, "guangdong") == 0)
      pChn->PlayRule[PlayRuleId].speech = 4;
    else if (stricmp(szTemp, "minnan") == 0)
      pChn->PlayRule[PlayRuleId].speech = 5;
    else if (stricmp(szTemp, "french") == 0)
      pChn->PlayRule[PlayRuleId].speech = 6;
    else if (stricmp(szTemp, "portuguese") == 0)
      pChn->PlayRule[PlayRuleId].speech = 7;
    else if (stricmp(szTemp, "arabic") == 0)
      pChn->PlayRule[PlayRuleId].speech = 8;
    else
      pChn->PlayRule[PlayRuleId].speech = 2;

    strcpy(szTemp, GetParamByName(szParamStr, "VOICE", "female"));
    if (stricmp(szTemp, "female") == 0)
      pChn->PlayRule[PlayRuleId].voice = 1;
    else if (stricmp(szTemp, "male") == 0)
      pChn->PlayRule[PlayRuleId].voice = 2;
    else if (stricmp(szTemp, "child") == 0)
      pChn->PlayRule[PlayRuleId].voice = 3;
    else
      pChn->PlayRule[PlayRuleId].voice = 0;
    
    pChn->PlayRule[PlayRuleId].pause = atoi(GetParamByName(szParamStr, "PAUSE", "0"));
    
    strcpy(szTemp, GetParamByName(szParamStr, "BROADCAST", "FALSE"));
    if (stricmp(szTemp, "TRUE") == 0 || stricmp(szTemp, "1") == 0)
      pChn->PlayRule[DtmfRuleId].error = 1;
    else
      pChn->PlayRule[DtmfRuleId].error = 0;
    
    pChn->curSpeech = pChnn->PlayRule[PlayRuleId].speech;
  }

  //設置收碼放音規則
  if (SetDTMFPlayRule(nChn, DtmfRuleId, PlayRuleId, ErrMsg) != 0)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  nSpeech = pChn->PlayRule[PlayRuleId].speech;
  //合成串
  for (int i=0; i<4; i++)
  {
    nMGYear = 0;
    nAMPMHour = 0;
    if (strlen(AttrData->AttrValue[9+i]) == 0)
    {
      continue;
    }
    memset(szTemp, 0, 256);
    memset(szTemp1, 0, 256);
    memset(szParamStr, 0, MAX_VAR_DATA_LEN);
    strncpy(szTemp, AttrData->AttrValue[9+i], 21);
    strcpy(szTemp, MyUpper(szTemp));
    
    if (strncmp(szTemp, "FILE=", 5) == 0)
    {
      mixtype = 1;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][5]);
    }
    else if (strncmp(szTemp, "DATETIME=", 9) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][9]);
      FormatDateTimeStr(szTemp, szParamStr, nMGYear);
    }
    else if (strncmp(szTemp, "DATE=", 5) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYY-MM-DD=", 11) == 0 || strncmp(szTemp, "YYYY/MM/DD=", 11) == 0 || strncmp(szTemp, "YYYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 YYYY/MM/DD YYYY.MM.DD
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][11]);
      FormatDateTimeStr(szTemp, szTemp1, nMGYear);
      
      strncpy(szParamStr, szTemp1, 10);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYY-MM-DD=", 11) == 0 ||strncmp(szTemp, "MGYY/MM/DD=", 11) == 0 || strncmp(szTemp, "MGYY.MM.DD=", 11) == 0) //2015-06-20增加日期格式 MGYY/MM/DD MGYY.MM.DD
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 21)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][11], 10);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMMDD=", 9) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][9], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYY-MM-DD=", 10) == 0 || strncmp(szTemp, "YYY/MM/DD=", 10) == 0 || strncmp(szTemp, "YYY.MM.DD=", 10) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;

      memset(szTemp, 0, 256);
      memset(szTemp1, 0, 256);
      strncpy(szTemp, &AttrData->AttrValue[9+i][10], 20);
      int len = strlen(szTemp);
      int k=0, id=0, l=0, y=0, m=0, d=0;
      char c;
      bool bErr=false;
      if (len >= 6 && len <= 9)
      {
        for (int j=0; j<=len; j++)
        {
          c = szTemp[j];
          if (id == 0)
          {
            //年
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              y++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (y == 2)
              {
                szTemp1[3] = '-';
                szTemp1[2] = szTemp1[1];
                szTemp1[1] = szTemp1[0];
                szTemp1[0] = '0';
                k = 4;
                id = 1;
              }
              else if (y == 3)
              {
                szTemp1[k] = '-';
                k++;
                id = 1;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 1)
          {
            //月
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              m++;
            }
            else if (c == '\0' || c == '/' || c == '-' || c == '.')
            {
              if (m == 1)
              {
                szTemp1[6] = '-';
                szTemp1[5] = szTemp1[4];
                szTemp1[4] = '0';
                k = 7;
                id = 2;
              }
              else if (m == 2)
              {
                szTemp1[k] = '-';
                k++;
                id = 2;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
          else if (id == 2)
          {
            //日
            if (c >= '0' && c <= '9')
            {
              szTemp1[k] = c;
              k++;
              d++;
            }
            else if (c == '\0')
            {
              if (d == 1)
              {
                szTemp1[8] = szTemp1[7];
                szTemp1[7] = '0';
                k = 9;
                id = 3;
              }
              else if (d == 2)
              {
                k++;
                id = 3;
              }
              else
              {
                bErr = true;
                break;
              }
            }
            else
            {
              bErr = true;
              break;
            }
          }
        }
      }

      if (strlen(szTemp1) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, szTemp1, 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strncat(szTemp, &szTemp1[3], 6);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMMDD=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 15)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][8], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MGYYMM=", 7) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 4);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "YYYMM=", 6) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 11)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][6], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MG=", 3) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      if (strlen(szTemp) == 4 || strlen(szTemp) == 5 || strlen(szTemp) == 6)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][3], 3);
        nMGYYYY = atoi(szTemp);
        sprintf(szTemp, "%d", nMGYYYY+1911);
        strcat(szTemp, "-MM-DD");
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "MMDD=", 5) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[9+i][5], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][7], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "XXXXMMDD=", 9) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strcpy(szTemp, "YYYY-");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, szTemp);
        strcat(szParamStr, "_HH:MI:SS_W");
      }
    }
    else if (strncmp(szTemp, "TIME=", 5) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "AMPM=", 5) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][5]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 8);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HHMISS=", 7) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMISS=", 7) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      if (strlen(szTemp) == 13)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][9], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "HHMI=", 5) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "APMI=", 5) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      if (strlen(szTemp) == 9)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][5], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][7], 2);
        strcat(szTemp, ":SS");
        
        strcpy(szParamStr, "YYYY-MM-DD_");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, "_W");
      }
    }
    else if (strncmp(szTemp, "YYYY-MM=", 8) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MGYY-MM=", 8) == 0)
    {
      nMGYear = 1; //臺灣民國讀法
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][8]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strncpy(szParamStr, szTemp1, 7);
      strcat(szParamStr, "-DD_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 5);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "MM-DD_HH:MI=", 12) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][12]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 11);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
    {
      mixtype = 2;
      if (strlen(szTemp) == 17)
      {
        memset(szTemp, 0, 256);
        strncpy(szTemp, &AttrData->AttrValue[9+i][9], 2);
        strcat(szTemp, "-");
        strncat(szTemp, &AttrData->AttrValue[9+i][11], 2);
        strcat(szTemp, "_");
        strncat(szTemp, &AttrData->AttrValue[9+i][13], 2);
        strcat(szTemp, ":");
        strncat(szTemp, &AttrData->AttrValue[9+i][15], 2);
        
        strcpy(szParamStr, "YYYY-");
        strcat(szParamStr, szTemp);
        strcat(szParamStr, ":SS_W");
      }
    }
    else if (strncmp(szTemp, "MM-DD_HH=", 9) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-");
      strncat(szParamStr, &szTemp1[5], 8);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD_HH:MI=", 9) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][9]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 8);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "DD_HH=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 5);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "DD=", 3) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-");
      strncat(szParamStr, &szTemp1[8], 2);
      strcat(szParamStr, "_HH:MI:SS_W");
    }
    else if (strncmp(szTemp, "W=", 2) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][2]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:MI:SS_");
      strncat(szParamStr, &szTemp1[20], 1);
    }
    else if (strncmp(szTemp, "HH:MI=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "AP:MI=", 6) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 5);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MI:SS=", 6) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][6]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 5);
      strcat(szParamStr, "_W");
    }
    else if (strncmp(szTemp, "HH=", 3) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);

      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "AP=", 3) == 0)
    {
      mixtype = 2;
      nAMPMHour = 1;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_");
      strncat(szParamStr, &szTemp1[11], 2);
      strcat(szParamStr, ":MI:SS_W");
    }
    else if (strncmp(szTemp, "MI=", 3) == 0)
    {
      mixtype = 2;
      memset(szTemp, 0, 256);
      strcpy(szTemp, &AttrData->AttrValue[9+i][3]);
      FormatDateTimeStr(szTemp, szTemp1, nMG);
      
      strcpy(szParamStr, "YYYY-MM-DD_HH:");
      strncat(szParamStr, &szTemp1[14], 2);
      strcat(szParamStr, ":SS_W");
    }
    else if (strncmp(szTemp, "MONEY=", 6) == 0)
    {
      mixtype = 3;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][6]);
    }
    else if (strncmp(szTemp, "VALUE=", 6) == 0)
    {
      mixtype = 4;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][6]);
    }
    else if (strncmp(szTemp, "DIGIT=", 6) == 0)
    {
      mixtype = 5;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][6]);
    }
    else if (strncmp(szTemp, "CHAR=", 5) == 0)
    {
      mixtype = 6;
      strcpy(szParamStr, &AttrData->AttrValue[9+i][5]);
    }
    else
    {
      mixtype = 1;
      strcpy(szParamStr, AttrData->AttrValue[9+i]);
    }
    
    nResult = AddMixToPlayBuf(nSpeech, nChn, mixtype, szParamStr, ErrMsg, nMGYear, nAMPMHour);
  }
  
  if (nResult == 1)
  {
    Proc_MSG_play_Result(&XMLHeader, OnPlayError, "", ErrMsg);
    StopClearPlayDtmfBuf(nChn);
    return 1;
  }
  pChn->curPlayData.PlayState = 1;
  pChn->PlayedTimer = 0;
  if (Start_ChnPlay(nChn) != 0)
  {
    return 1;
  }
  pChn->curPlayData.CanPlayId = 1;
  
  return 0;
}
//-----------------------------------------------------------------------------
//定義一個函數指針類型
typedef int (*Proc_ParserMSG)(VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
Proc_ParserMSG proc_parsermsg[]=
{
  Proc_MSG_login,
  Proc_MSG_setaccessno,
  Proc_MSG_answer,
  Proc_MSG_sendsignal,
  Proc_MSG_hangon,
  Proc_MSG_dtmfrule,
  Proc_MSG_getdtmf,
  Proc_MSG_playrule,
  Proc_MSG_asrrule,
  Proc_MSG_setvolume,
  Proc_MSG_playfile,
  Proc_MSG_setplaypos,
  Proc_MSG_playfiles,
  Proc_MSG_playcompose,
  Proc_MSG_multiplay,
  Proc_MSG_ttsstring,
  Proc_MSG_ttsfile,
  Proc_MSG_senddtmf,
  Proc_MSG_sendfsk,
  Proc_MSG_playcfc,
  Proc_MSG_playtone,
  Proc_MSG_recordfile,
  Proc_MSG_recordcfc,
  Proc_MSG_stop,
  Proc_MSG_callout,
  Proc_MSG_cancelcallout,
  Proc_MSG_sendsam,
  Proc_MSG_transfer,
  Proc_MSG_callseat,
  Proc_MSG_stopcallseat,
  Proc_MSG_createcfc,
  Proc_MSG_destroycfc,
  Proc_MSG_joincfc,
  Proc_MSG_unjoincfc,
  Proc_MSG_link,
  Proc_MSG_talkwith,
  Proc_MSG_listenfrom,
  Proc_MSG_inserttalk,
  Proc_MSG_threetalk,
  Proc_MSG_stoptalk,
  Proc_MSG_sendfax,
  Proc_MSG_recvfax,
  Proc_MSG_checkpath,
  Proc_MSG_createpath,
  Proc_MSG_deletepath,
  Proc_MSG_getfilenum,
  Proc_MSG_getfilename,
  Proc_MSG_renname,
  Proc_MSG_copyfile,
  Proc_MSG_deletefile,
  Proc_MSG_checkfile,
  Proc_MSG_clearmixer,
  Proc_MSG_addfile,
  Proc_MSG_adddatetime,
  Proc_MSG_addmoney,
  Proc_MSG_addnumber,
  Proc_MSG_adddigits,
  Proc_MSG_addchars,
  Proc_MSG_playmixer,
  Proc_MSG_addttsstr,
  Proc_MSG_addttsfile,
  Proc_MSG_dialout,
  Proc_MSG_flash,
  Proc_MSG_setvcparam,
  Proc_MSG_pause,
  Proc_MSG_replay,
  Proc_MSG_fastplay,
  Proc_MSG_checktone,
  Proc_MSG_workerlogin,
  Proc_MSG_workerlogout,
  Proc_MSG_agtrancallresult,
  Proc_MSG_agjoinconfresult,
  Proc_MSG_agtranivrresult,
  Proc_MSG_sendagentmsg,
  Proc_MSG_pickupcall,
  Proc_MSG_getacdqueue,
  Proc_MSG_setflwstate,
  Proc_MSG_swapcallresult,
  Proc_MSG_recvfsk,
  Proc_MSG_transferivr,
  Proc_MSG_releaseivr,
  Proc_MSG_playswitchvoice,
  Proc_MSG_sendswitchcmd,
  Proc_MSG_dial,
  Proc_MSG_appendfaxfile,
  Proc_MSG_bandagentchn,
  Proc_MSG_sendcalltoagent,
  Proc_MSG_Play,
  Proc_MSG_workerloginex,
  Proc_MSG_workerlogoutex,
  Proc_MSG_getseatlogindata,
  Proc_MSG_getidleseat,
  Proc_MSG_unlockseat,
  Proc_MSG_updateservicescore,
  Proc_MSG_updatecdrfield,
  Proc_MSG_updatesysparam,
  Proc_MSG_setforwarding,
  Proc_MSG_callfunction,
  Proc_MSG_Playex,
};
//處理解析器XML消息
void ProcXMLMsg(VXML_CMD_ATTRIBUTE_STRUCT *AttrData)
{
  GetXMLHeader(AttrData, &XMLHeader);
  nChnn = XMLHeader.nChn;
  proc_parsermsg[AttrData->MsgId](AttrData);
}
