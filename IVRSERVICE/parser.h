//---------------------------------------------------------------------------
//流程解析類
//---------------------------------------------------------------------------
#ifndef __PARSER_H_
#define __PARSER_H_
//---------------------------------------------------------------------------
//#include ""
//---------------------------------------------------------------------------
#define pAcc (pAcc_str + nAcc)
#define pAccNext (pAcc_str + next)
#define pAccPrior (pAcc_str + prior)
//---------------------------------------------------------------------------

//接入字冠結構(采用隊列方式保存，增加字冠時，按字冠長度從大到小排)
typedef struct
{
	US state;		  //數據狀態 0: 無 1：有
	CH code[20];	//接入字冠
	US minlen;		//接入字冠最少長度
	US maxlen;		//接入字冠最大長度
	US NodeXMLs;	//登錄的流程解析節點數
	US NodeXML[MAX_NODE_XML];	//登錄的流程解析節點號(0xFF表示無, 0表示IVR自帶的節點號)
	US AssignPoint; //當前分配的指針
	US prior;		  //對應的前一序號
	US next;		  //對應的下一序號, 0xFFFF表示結束

}VXML_PRECODE_STRUCT;

class CParser
{
public:
	CParser();
	virtual ~CParser();
	
	US Clientid; //解析器節點編號

	UC VxmlLogId[MAX_NODE_XML]; //業務節點登錄標志(數組下標對應節點的編號 1-表示節點已登錄 0-表示節點未登錄)
	VXML_PRECODE_STRUCT *pAcc_str; //接入碼結構
	
public:
	//初始化
	int Init(void);
	//增加給定節點的接入號碼
	bool AddPreCode(int XmlNo, char *PreCode, int MinLen, int MaxLen);
	//刪除給定節點的接入號碼
	bool DelPreCode(int XmlNo, char *PreCode, int MinLen, int MaxLen);
  //解析器退出刪除相關的接入號碼
  bool DelPreCodeForVxmlLogout(int XmlNo);
  //判斷被叫號碼是否收全,并返回最終的號碼(返回值為0時表示與定義的接入號碼匹配，1表示匹配但未到最小匹配位數，2表示未找到匹配號碼)
  int CheckCalledACM(char *CalledNo, int &VxmlId);
  //判斷內線坐席摘機后是否直接進入流程，流程接入碼設置為：A,1,1,*
  int CheckSeatDirectIn(int &VxmlId);

  void SetLogin(int vxmlid)
  {
    if (vxmlid > MAX_NODE_XML) return;
    VxmlLogId[vxmlid] = 1;
  }
  void SetLogout(int vxmlid)
  {
    if (vxmlid > MAX_NODE_XML) return;
    VxmlLogId[vxmlid] = 0;
  }
  bool isLogin(int vxmlid)
  {
    if (vxmlid > MAX_NODE_XML) return false;
    return (VxmlLogId[vxmlid]==1)?true:false;
  }
};
//---------------------------------------------------------------------------
#endif
