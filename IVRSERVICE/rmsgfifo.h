//---------------------------------------------------------------------------
#ifndef __RMSGFIFO_H_
#define __RMSGFIFO_H_
//---------------------------------------------------------------------------
#include <afxmt.h>
#include "flwrule.h"
//---------------------------------------------------------------------------
//
typedef struct
{
	US ClientId; //節點編號
  US MsgId; //指令編號
  US MsgLen;
  CH MsgBuf[4096];

}VXML_RECV_MSG_STRUCT;

//消息屬性結構
typedef struct
{
	US MsgId; //指令編號
	CH CmdName[MAX_CMD_LABEL_LEN]; //指令名稱
	US AttrNums; //屬性個數
  CH AttrName[MAX_ATTRIBUTE_NUM][MAX_ATTRIBUTE_LEN]; //屬性名稱
  CH AttrValue[MAX_ATTRIBUTE_NUM][MAX_VAR_DATA_LEN]; //屬性值
	//CH Text[MAX_VAR_DATA_LEN]; //文本

}VXML_CMD_ATTRIBUTE_STRUCT;

//XML消息頭
typedef struct
{
  UL SessionId; //會話序列號
  UL CmdAddr; //指令地址
  US MsgId; //消息編號
  US ChnType; //通道類型
  US ChnNo; //通道號
  int nChn; //通道號
  UC OnlineId; //在線標志
  UC ErrorId; //消息包錯誤標志
  US OnMsgId;
  CH OnMsgName[MAX_USERNAME_LEN];
  
}VXML_XML_HEADER_STRUCT;

class CMsgfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_RECV_MSG_STRUCT      *pMsgs;
  
  CCriticalSection m_cCriticalSection;
  
public:
  CMsgfifo(unsigned short len)
  {
    Head=0;
    Tail=0;
    resv=0;
    Len=len;
    pMsgs=new VXML_RECV_MSG_STRUCT[Len];
  }
  
  ~CMsgfifo()
  {
    if (pMsgs != NULL)
    {
      delete []pMsgs;
      pMsgs = NULL;
    }
  }
  
  bool	IsFull()
  {
    unsigned short temp=Tail+1;
    return temp==Len?Head==0:temp==Head;
  }
  bool 	IsEmpty()
  {
    bool bResult;
    
    m_cCriticalSection.Lock();
    Head==Tail ? bResult=true : bResult=false;
    m_cCriticalSection.Unlock();
    
    return bResult;
  }
  VXML_RECV_MSG_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
    if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(US ClientId, US MsgId, const char *MsgBuf, US MsgLen);
  unsigned short  Read(US &ClientId, US &MsgId, char *MsgBuf, US &MsgLen);
  unsigned short  Read(VXML_RECV_MSG_STRUCT *RecvMsg);
  
  //解析XML字符串
  int ParseXMLstr(const VXML_SEND_MSG_CMD_RULE_STRUCT *MsgRule, short MsgId, CH *XMLstr, VXML_CMD_ATTRIBUTE_STRUCT *CmdAttr);
  //消息屬性參數值檢查
  int Parse_Msg_Attr_Value(CFlwRule *FlwRule, CH *ValueStr, UC VarType, UC MacroType, US MacroNo, CH *ReturnValueStr, CH *ErrMsg);
  //解析消息
  int Parse_Msg(CFlwRule *FlwRule, const VXML_SEND_MSG_CMD_RULE_STRUCT *MsgRule, VXML_CMD_ATTRIBUTE_STRUCT *AttrData);
};

//Parser接收到的消息的封裝類
class CXMLRcvMsg : public CXMLMsg
{
public:
		int ParseRevMsgWithCheck(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule);
    int ParseSndMsgWithCheck(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule);
}	;

//Parser發送的消息的封裝類
class CXMLSndMsg : public CXMLMsg
{
public:	
  void SetHeadToBuf(const char *msgname,int nChn); //在buf中設置頭部
}	;

//---------------------------------------------------------------------------
#endif
