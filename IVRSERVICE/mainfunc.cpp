//
//CTI/IVR后臺服務主處理函數部分
//

#include "stdafx.h"
#include "../sqlite3/sqlite3.h"
#include "odsLog.h"
#include "vardef.h"
#include "main.h"
#include "mainfunc.h"
#include "MyDiskInfo.h"
#include "md5proc.h"
#include "ado2.h"

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "../sqlite3/sqlite3.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#define CONSOLE_RUN_MODE
CADODatabase  *g_pDbConnect=NULL;
CADORecordset g_pRsQuery;

//-------------------通信線程處理-------------------------------------------
//ivr,ctilink通信處理線程
UINT IVRServerProc(LPVOID pParam)
{
  while (1)
  {
    if (g_nExistThread == 1)
    {
      //pTcpLink->AppCloseAll();
      break;
    }
    if(pTcpLink->IsOpen)
      pTcpLink->AppMainLoop();
    Sleep(10);
  }
  MyTrace(0, "Thread IVRServerProc termination");
  return 0;
}

//AG通信處理線程
UINT AGServerProc(LPVOID pParam)
{
  static int timercount=0;
  while (1)
  {
    if (g_nExistThread == 1)
    {
      pTcpAgLink->AppCloseAll();
      break;
    }
    if(pTcpAgLink->IsOpen)
    {
      pTcpAgLink->AppMainLoop();
      if (pIVRCfg->isWriteTimerDogLogId == true && timercount++ > 500)
      {
        timercount = 0;
        MyTrace(0, "AGServerProc Timer Action");
      }
    }
    Sleep(10);
  }
  MyTrace(0, "Thread AGServerProc termination");
  return 0;
}
//主被機通信處理線程
UINT HAServerProc(LPVOID pParam)
{
  while (1)
  {
    if (g_nExistThread == 1)
    {
      pTCPHALink->AppCloseAll();
      break;
    }
    if(pTCPHALink->IsOpen)
    {
      pTCPHALink->AppMainLoop();
    }
    Sleep(10);
  }
  MyTrace(0, "Thread HAServerProc termination");
  return 0;
}

//DB通信處理線程
UINT DBClientProc(LPVOID pParam)
{
  while (1)
  {
    if (g_nExistThread == 1)
    {
      pDBClient->AppCloseAll();
      break;
    }
    if(pDBClient->IsOpen)
      pDBClient->AppMainLoop();
    Sleep(10);
  }
  MyTrace(0, "Thread DBClientProc termination");
  return 0;
}

//http通信處理線程
UINT HTTPProc(LPVOID pParam)
{
  CH MsgBuf[4096], NewMsgBuf[4096], szCalledNo[32], szNewCalledNo[32];
  US MsgLen, Count;
  while (1)
  {
    if (g_nExistThread == 1)
    {
      break;
    }
    if (g_WebServerPort > 0 || g_bQueryLocaMobileNo == true)
    {
      Count = 0;
      do 
      {
        if (g_WebServer.Read(MsgBuf, MsgLen) != 0)
        {
          char szCmd[32];
          
          memset(szCmd, 0, 32);
          strncpy(szCmd, GetParamStrValue("cmd=", MsgBuf), 31);
          if (stricmp(szCmd, "dialout") == 0)
          {
            memset(szCalledNo, 0, 32);
            memset(szNewCalledNo, 0, 32);
            strncpy(szCalledNo, GetParamStrValue1("calledno=", MsgBuf), 31);
            if (strlen(szCalledNo) > 0)
            {
              int len = strlen(pIVRCfg->SWTDialOutPreCode);
              if (len > 0 && len < (int)strlen(szCalledNo) && strncmp(szCalledNo, pIVRCfg->SWTDialOutPreCode, len) == 0)
                strcpy(szCalledNo, &szCalledNo[len]);
              
              IsLocaMobileNo(szCalledNo, szNewCalledNo);
              
              MyStringReplace(MsgBuf, szCalledNo, szNewCalledNo, 0, 1, 4096, NewMsgBuf);
              g_HTTPfifo.Write(NewMsgBuf, MsgLen);
            }
            else
            {
              g_HTTPfifo.Write(MsgBuf, MsgLen);
            }
          }
          else
          {
            g_HTTPfifo.Write(MsgBuf, MsgLen);
          }
        }
        else
        {
          break;
        }
      } while (Count++ < 64);
    }
    Sleep(10);
  }
  MyTrace(0, "Thread HTTPProc termination");
  return 0;
}
//數據庫處理線程
UINT DbProc(LPVOID pParam)
{
  while (1)
  {
    if (g_nExistThread == 1)
    {
      break;
    }
    ProcSqlFiFo();
    
    Sleep(50);
  }
  MyTrace(0, "Thread DbProc termination");
  return 0;
}
//----------------------------------------------------------------------------
void ProcWorkerLoginSql(const char *SeatNo, const char *WorkerNo, const char *SqlsBuf, const char *MsgBuf)
{
  int nGrpNo=0, nAcwTimer=0, nGroupNo[16], nSkillLevel[16], nWorkerGrade;
  CString strWorkerName;
  char szSqls[256], szWorkerName[128], szGroupNo[128], szSkillLevel[128], szTemp[32], szMsgBuf[2048];

  try
  {
    g_pRsQuery.Open(SqlsBuf, CADORecordset::openQuery);
    if (g_pRsQuery.GetRecordCount() > 0)
    {
      memset(szGroupNo, 0, 128);
      memset(szSkillLevel, 0, 128);

      g_pRsQuery.GetFieldValue("WorkerName", strWorkerName);
      UTF8_ANSI_Convert((LPCTSTR)strWorkerName, szWorkerName, CP_ACP, CP_UTF8);

      g_pRsQuery.GetFieldValue("WorkerType", nWorkerGrade);
      
      g_pRsQuery.GetFieldValue("GroupNo", nGroupNo[0]);
      g_pRsQuery.GetFieldValue("SkillLevel", nSkillLevel[0]);
      if (nGroupNo[0] > 0)
      {
        nGrpNo = nGroupNo[0];
        sprintf(szTemp, "%d,", nGroupNo[0]);
        strcat(szGroupNo, szTemp);
        sprintf(szTemp, "%d,", nSkillLevel[0]);
        strcat(szSkillLevel, szTemp);
      }
      for (int i=1; i<16; i++)
      {
        sprintf(szTemp, "GroupNo%d", i);
        g_pRsQuery.GetFieldValue(szTemp, nGroupNo[i]);
        sprintf(szTemp, "SkillLevel%d", i);
        g_pRsQuery.GetFieldValue(szTemp, nSkillLevel[i]);
        if (nGroupNo[i] > 0)
        {
          if (nGrpNo == 0)
            nGrpNo = nGroupNo[i];
          sprintf(szTemp, "%d,", nGroupNo[i]);
          strcat(szGroupNo, szTemp);
          sprintf(szTemp, "%d,", nSkillLevel[i]);
          strcat(szSkillLevel, szTemp);
        }
      }
      g_pRsQuery.Close();
      sprintf(szSqls, "select acwtimer from tbworkergroup where groupno=%d", nGrpNo);
      g_pRsQuery.Open(szSqls, CADORecordset::openQuery);
      if (g_pRsQuery.GetRecordCount() > 0)
      {
        g_pRsQuery.GetFieldValue("acwtimer", nAcwTimer);
      }
      g_pRsQuery.Close();
      if (nAcwTimer == 0)
      {
        sprintf(szSqls, "select acwtimelen from tbsysset where id=1");
        g_pRsQuery.Open(szSqls, CADORecordset::openQuery);
        if (g_pRsQuery.GetRecordCount() > 0)
        {
          g_pRsQuery.GetFieldValue("acwtimelen", nAcwTimer);
        }
      }
      g_pRsQuery.Close();
      sprintf(szMsgBuf, "authtype=2;workername=%s;groupno=%s;skilllevel=%s;grade=%d;acwtimer=%d;%s",
        szWorkerName, szGroupNo, szSkillLevel, nWorkerGrade, nAcwTimer, MsgBuf);

      MyTrace(3, "%s", szMsgBuf);
      if (RMsgFifo->Write(0x0701, 0x0701, szMsgBuf, strlen(szMsgBuf)) == 0)
        MyTrace(0, "Recv Msg Fifo is overflow");
    }
    else
    {
      sprintf(szMsgBuf, "authtype=3;%s", MsgBuf);
      if (RMsgFifo->Write(0x0701, 0x0701, szMsgBuf, strlen(szMsgBuf)) == 0)
        MyTrace(0, "Recv Msg Fifo is overflow");
      g_pRsQuery.Close();
    }
  }
  catch (CADOException e)
  {
    if (strstr((LPCTSTR)e.GetErrorMessage(), "連接失敗") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "超時已過期") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "Missing Connection or ConnectionString") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "gone away") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "not connected to ORACLE") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "超出打開游標的最大數") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "一般性網絡錯誤") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "未連接到") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "硈??毖") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "?�Z�I??蕩�Z����") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "筄��?戳") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "ORA-03114") != NULL
      )
    {
      g_nConnectDbState = 2;
    }
    MyTrace(0, "ProcWorkerLoginSql fail %s", (LPCTSTR)e.GetErrorMessage());
    sprintf(szMsgBuf, "authtype=3;%s", MsgBuf);
    if (RMsgFifo->Write(0x0701, 0x0701, szMsgBuf, strlen(szMsgBuf)) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }

}
void ProcSqlFiFo()
{
  CH SeatNo[32], WorkerNo[32], SqlBuf[1024], MsgBuf[1024];
  US count=0, MsgType;
  
  if (g_nConnectDbState != 1)
  {
    ConnectDB();
  }
  if (g_nConnectDbState != 1)
    return;
  while (count < 64 && pSQLSfifo->Read(SeatNo, WorkerNo, SqlBuf, MsgBuf, MsgType) != 0)
  {
    try
    {
      MyTrace(4, "%s", SqlBuf);
      ProcWorkerLoginSql(SeatNo, WorkerNo, SqlBuf, MsgBuf);
    }
    catch (CADOException e)
    {
      MyTrace(1, "ExecSql fail %s", (LPCTSTR)e.GetErrorMessage());
      if (strstr((LPCTSTR)e.GetErrorMessage(), "連接失敗") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "超時已過期") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "Missing Connection or ConnectionString") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "gone away") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "not connected to ORACLE") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "超出打開游標的最大數") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "一般性網絡錯誤") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "未連接到") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "硈??毖") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "?�Z�I??蕩�Z����") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "筄��?戳") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "ORA-03114") != NULL
        )
      {
        g_nConnectDbState = 2;
        g_pDbConnect->Close();
      }
    }
    if (g_nExistThread == 1)
    {
      break;
    }
    
    count++;
  }
}

void WriteAlarmToDB(int nAlarmCode, int nAlarmLevel, int nAlarmState, const CH *pszAlarmMsg)
{
  sqlite3 *sqliteDB;
  sqlite3_stmt *statement;
  char sqls[1024], szGid[64];
  
  strcpy(szGid, MyGetGUID());
  
  sprintf(sqls, "insert into tbSysAlarmList (Gid,ServerId,AlarmCode,AlarmLevel,AlarmTime,AlarmMsg,AlarmState) values ('%s',%d,%d,%d,'%s','%s',%d)",
    szGid, g_IVRServerId, nAlarmCode, nAlarmLevel, MyGetNow(), pszAlarmMsg, nAlarmState);
  
  if (pIVRCfg->isWriteAlarmToDB == 1)
  {
    SendExecSql2DB(sqls);
  }
  else
  {
    if (sqlite3_open(g_szAlarmSqliteDB, &sqliteDB) == SQLITE_OK)
    {
      if ( sqlite3_prepare(sqliteDB, sqls, -1, &statement, 0 ) == SQLITE_OK )
      {
        int res = 0;
        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
          MyTrace(0, "WriteAlarmToDB error sqls=%s", sqls);
        }
        else
        {
          MyTrace(0, "WriteAlarmToDB success sqls=%s", sqls);
        }
      }
      sqlite3_close(sqliteDB);
    }
  }
}
void WriteAlarmList(int nAlarmCode, int nAlarmLevel, int nAlarmState, LPCTSTR lpszFormat, ...)
{
  if (pIVRCfg->isWriteAlarmToDB == 0)
  {
    return;
  }
  char buf[1024];
  va_list	ArgList;
  va_start(ArgList, lpszFormat);
  _vsnprintf(buf, 1024, lpszFormat, ArgList);
  va_end (ArgList);
  
  WriteAlarmToDB(nAlarmCode, nAlarmLevel, nAlarmState, buf);
}

//-----------------------------------------------------------------------------
int GetAdapterNameByIP(const char *ivrip)
{
  DWORD dwOutBufLen=0;
  PIP_ADAPTER_INFO pAdapterInfo=NULL,pAdapter=NULL;
  MIB_IFROW zSNMP;
  int iReturn;
  
  iReturn=GetAdaptersInfo(pAdapterInfo,&dwOutBufLen);
  if(iReturn != ERROR_BUFFER_OVERFLOW)
  {
    memset(g_szAdapterName, 0, 64);
    return 1;
  }
  
  pAdapterInfo =(PIP_ADAPTER_INFO) HeapAlloc(GetProcessHeap(), 0, dwOutBufLen);
  iReturn=GetAdaptersInfo(pAdapterInfo,&dwOutBufLen);
  // 可通過pAdapterInfo得到網卡名稱，ip地址，子網掩碼等信息
  // 此指針是一個列表，pc上所有網卡的枚舉列表
  if(iReturn != ERROR_SUCCESS)
  {
    HeapFree(GetProcessHeap(), 0, pAdapterInfo);
    memset(g_szAdapterName, 0, 64);
    return 1;
  }
  pAdapter=pAdapterInfo;
  
  //find if there is ppp adapter
  int k = 0;
  while(pAdapter!=NULL )
  {
    zSNMP.dwIndex = pAdapter->Index;
    iReturn=GetIfEntry(&zSNMP);
    if(iReturn!=NO_ERROR)
    {
      memset(g_szAdapterName, 0, 64);
      return 1;
    }
    
    if (strcmp(pAdapter->IpAddressList.IpAddress.String, ivrip) == 0)
    {
      strcpy(g_szAdapterName, pAdapter->AdapterName);
      MyTrace(0, "GetAdapterNameByIP IP=%s AdapterName=%s", g_szLocaIVRIP, g_szAdapterName);
      return 0;
    }
    pAdapter=pAdapter->Next;
  }
  
  memset(g_szAdapterName, 0, 64);
  return 1;
}
//返回0-表示本機網絡正常，1-本機網絡斷開
int CheckLocaNetCable(const char *adaptername)
{
  DWORD dwOutBufLen=0;
  PIP_ADAPTER_INFO pAdapterInfo=NULL,pAdapter=NULL;
  MIB_IFROW zSNMP;
  int iReturn;
  
  iReturn=GetAdaptersInfo(pAdapterInfo,&dwOutBufLen);
  if(iReturn != ERROR_BUFFER_OVERFLOW)
  {
    MyTrace(0, "IP=%s CheckLocaNetCable GetAdaptersInfo OVERFLOW", g_szLocaIVRIP);
    return 0;
  }
  
  pAdapterInfo =(PIP_ADAPTER_INFO) HeapAlloc(GetProcessHeap(), 0, dwOutBufLen);
  iReturn=GetAdaptersInfo(pAdapterInfo,&dwOutBufLen);
  // 可通過pAdapterInfo得到網卡名稱，ip地址，子網掩碼等信息
  // 此指針是一個列表，pc上所有網卡的枚舉列表
  if(iReturn != ERROR_SUCCESS)
  {
    HeapFree(GetProcessHeap(), 0, pAdapterInfo);
    MyTrace(0, "IP=%s CheckLocaNetCable GetAdaptersInfo error", g_szLocaIVRIP);
    return 0;
  }
  pAdapter=pAdapterInfo;
  
  //find if there is ppp adapter
  int k = 0;
  while(pAdapter!=NULL )
  {
    zSNMP.dwIndex = pAdapter->Index;
    iReturn=GetIfEntry(&zSNMP);
    if(iReturn!=NO_ERROR)
    {
      MyTrace(0, "IP=%s CheckLocaNetCable GetIfEntry error", g_szLocaIVRIP);
      return 0;
    }
    
    MyTrace(0, "dwOperStatus=%d AdapterName=%s", zSNMP.dwOperStatus, pAdapter->AdapterName);
    if ((0 == zSNMP.dwOperStatus || 2 == zSNMP.dwOperStatus)
      && strcmp(pAdapter->AdapterName, adaptername) == 0)
    {
      MyTrace(0, "IP=%s CheckLocaNetCable cut", g_szLocaIVRIP);
      return 1;
    }
    pAdapter=pAdapter->Next;
  }
  
  MyTrace(0, "IP=%s CheckLocaNetCable ok", g_szLocaIVRIP);
  return 0;
}

CString GetCPUID()
{
  CString CPUID;
  unsigned long s1,s2;
  unsigned char vendor_id[]="------------";
  char sel;
  sel='1';
  CString VernderID;
  CString MyCpuID,CPUID1,CPUID2;
  switch(sel)
  {
  case '1':
    __asm{
      xor eax,eax      //eax=0:取Vendor信息
        cpuid    //取cpu id指令，可在Ring3級使用
        mov dword ptr vendor_id,ebx
        mov dword ptr vendor_id[+4],edx
        mov dword ptr vendor_id[+8],ecx
    }
    VernderID.Format("%s-",vendor_id);
    __asm{
      mov eax,01h   //eax=1:取CPU序列號
        xor edx,edx
        cpuid
        mov s1,edx
        mov s2,eax
    }
    CPUID1.Format("%08X%08X",s1,s2);
    __asm{
      mov eax,03h
        xor ecx,ecx
        xor edx,edx
        cpuid
        mov s1,edx
        mov s2,ecx
    }
    CPUID2.Format("%08X%08X",s1,s2);
    break;
  case '2':
    {
      __asm{
        mov ecx,119h
          rdmsr
          or eax,00200000h
          wrmsr
      }
    }
    //AfxMessageBox("CPU id is disabled.");
    break;
  }
  MyCpuID = CPUID1+CPUID2;
  CPUID = MyCpuID;
  return CPUID;
}
int CreateRegData(struct HKEY__ *ReRootKey, TCHAR *ReSubKey, TCHAR *ReValueName, LPBYTE ReSetContent_S)
{
  HKEY hKey;
  int nResult=0;
  
  if(RegOpenKeyEx(ReRootKey,ReSubKey,0,KEY_WRITE,&hKey) != ERROR_SUCCESS)
  {
    if (RegCreateKey(ReRootKey, ReSubKey, &hKey) == ERROR_SUCCESS)
    {
      if(RegSetValueEx(hKey, ReValueName, NULL, REG_SZ, ReSetContent_S, CString(ReSetContent_S).GetLength()) != ERROR_SUCCESS)
      {
        nResult = 1;
      }
      RegCloseKey(hKey);
    }
    else
    {
      return 2;
    }
  }
  
  return nResult;
}

int WriteRegData(struct HKEY__ *ReRootKey, TCHAR *ReSubKey, TCHAR *ReValueName, LPBYTE ReSetContent_S)
{
  HKEY hKey;
  int nResult=0;
  
  if(RegOpenKeyEx(ReRootKey,ReSubKey,0,KEY_WRITE,&hKey) != ERROR_SUCCESS)
  {
    if (RegCreateKey(ReRootKey, ReSubKey, &hKey) == ERROR_SUCCESS)
    {
      if(RegSetValueEx(hKey, ReValueName, NULL, REG_SZ, ReSetContent_S, CString(ReSetContent_S).GetLength()) != ERROR_SUCCESS)
      {
        nResult = 1;
      }
      RegCloseKey(hKey);
    }
    else
    {
      return 2;
    }
  }
  else
  {
    if(RegSetValueEx(hKey, ReValueName, NULL, REG_SZ, ReSetContent_S, CString(ReSetContent_S).GetLength()) != ERROR_SUCCESS)
    {
      nResult = 1;
    }
    RegCloseKey(hKey);
  }
  
  return nResult;
}

int WriteRegData(struct HKEY__ *ReRootKey, TCHAR *ReSubKey, TCHAR *ReValueName, DWORD ReSetWalue)
{
  HKEY hKey;
  int nResult=0;
  
  if(RegOpenKeyEx(ReRootKey,ReSubKey,0,KEY_WRITE,&hKey) != ERROR_SUCCESS)
  {
    if (RegCreateKey(ReRootKey, ReSubKey, &hKey) == ERROR_SUCCESS)
    {
      if(RegSetValueEx(hKey, ReValueName, NULL, REG_DWORD, (LPBYTE)&ReSetWalue, sizeof(DWORD)) != ERROR_SUCCESS)
      {
        nResult = 1;
      }
      RegCloseKey(hKey);
    }
    else
    {
      return 2;
    }
  }
  else
  {
    if(RegSetValueEx(hKey, ReValueName, NULL, REG_DWORD, (LPBYTE)&ReSetWalue, sizeof(DWORD)) != ERROR_SUCCESS)
    {
      nResult = 1;
    }
    RegCloseKey(hKey);
  }
  
  return nResult;
}
int ShowContent (struct HKEY__*ReRootKey, TCHAR *ReSubKey, TCHAR *ReValueName, char *ReContent)
{
  HKEY hKey;
  DWORD dwType = REG_SZ;
  DWORD dwLength = 256;
  int nResult=0;	//操作結果：0==succeed

  ReContent[0] = '\0';
  if(RegOpenKeyEx(ReRootKey, ReSubKey, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
  {
    if(RegQueryValueEx(hKey, ReValueName, NULL, &dwType, (unsigned char *)ReContent, &dwLength) != ERROR_SUCCESS)
    {
      //AfxMessageBox("錯誤：無法查詢有關的注冊表信息");
      nResult = 1;
    }
    RegCloseKey(hKey);
  }
  else
  {
    //AfxMessageBox("錯誤：無法打開有關的hKEY");
    nResult = 2;
  }
	return nResult;
}

int WriteRegRunTimerDog()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  LPBYTE SetContent_S;		//字符串類型
  
  if (pIVRCfg->isWriteAlarmToDB == 1)
  {
    char sqls[128];
    sprintf(sqls, "update tbSysRunState set RunState=%d,MasterStatus=%d,StandbyStatus=%d,UpdateTime=getdate() where Id=1", 
      g_dwRegServerState, g_nMasterStatus, g_nStandbyStatus);
    SendExecSql2DB(sqls);
  }
  
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\UltraLog\\System\\DOG";  //欲打開注冊表值的地址
  ValueName = "LastNormalTime";               //欲設置值的名稱
  SetContent_S = LPBYTE(MyGetNowEx());   //值的內容
  return WriteRegData(RootKey, SubKey, ValueName, SetContent_S);
}

int WriteRegServiceRunState()
{
  if (g_nWriteRegRunDog == 1)
  {
    struct HKEY__*RootKey;		//注冊表主鍵名稱
    TCHAR *SubKey;			//欲打開注冊表項的地址
    TCHAR *ValueName;		//欲設置值的名稱
    
    RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
    SubKey = "SOFTWARE\\UltraLog\\System\\DOG";  //欲打開注冊表值的地址
    ValueName = "ServiceState";               //欲設置值的名稱
    WriteRegData(RootKey, SubKey, ValueName, g_dwRegServerState);
  }
  if (pIVRCfg->isWriteAlarmToDB == 1)
  {
    char sqls[128];
    sprintf(sqls, "update tbSysRunState set RunState=%d,MasterStatus=%d,StandbyStatus=%d,UpdateTime=getdate() where Id=1", 
      g_dwRegServerState, g_nMasterStatus, g_nStandbyStatus);
    SendExecSql2DB(sqls);
  }
  if (g_nMultiRunMode == 0)
    return 0;
  if (GetCTIHALinkFlag() == false)
    return 0;
  
  char szMsgBuf[512];
  sprintf(szMsgBuf, "<onsysrunstate sessionid='%ld' nodetype='%d' nodeno='%d' masterstatus='%d' standbystatus='%d' runstate='%d'/>",
    0, 0, g_nHostType==0?1:2, g_nMasterStatus, g_nStandbyStatus, g_dwRegServerState);
  MyTrace(2, "%s", szMsgBuf);
  
  if (g_nHostType == 0)
  {
    pTCPHALink->SendMessage2CLIENT(StandbyIVRClientId, (MSGTYPE_HAIVR<<8) | HAMSG_onsysrunstate, szMsgBuf);
  } 
  else
  {
    pTCPHALink->SendMessage2Master(HAMSG_onsysrunstate, szMsgBuf);
  }
  return 0;
}
int WriteRegAuthData()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  LPBYTE SetContent_S;		//字符串類型
  char szGUID[64];
  
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\Avaya\\Avaya AE Services TSAPI Client\\GUID";  //欲打開注冊表值的地址
  ValueName = "SerialNo";               //欲設置值的名稱
  MyGetGUID(szGUID);
  SetContent_S = LPBYTE(szGUID);   //值的內容
  return CreateRegData(RootKey, SubKey, ValueName, SetContent_S);
}

char *ReadRegAuthData()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  static char szAuthData[256];
  
  memset(szAuthData, 0, 256);
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\Avaya\\Avaya AE Services TSAPI Client\\GUID";  //欲打開注冊表值的地址
  ValueName = "SerialNo";               //欲設置值的名稱
  ShowContent(RootKey, SubKey, ValueName, szAuthData);
  return szAuthData;
}

int WriteRegFirstRunDate()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  LPBYTE SetContent_S;		//字符串類型
  char szGUID[256];
  
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\JuneySoft\\System\\INITDATA";  //欲打開注冊表值的地址
  ValueName = "FirstDate";               //欲設置值的名稱
  strcpy(szGUID, EncodePass(MyGetToday()));
  SetContent_S = LPBYTE(szGUID);   //值的內容
  return CreateRegData(RootKey, SubKey, ValueName, SetContent_S);
}

char *ReadRegFirstRunDate()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  static char szAuthData[256];
  static char szDay[256];
  
  memset(szAuthData, 0, 256);
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\JuneySoft\\System\\INITDATA";  //欲打開注冊表值的地址
  ValueName = "FirstDate";               //欲設置值的名稱
  ShowContent(RootKey, SubKey, ValueName, szAuthData);
  strcpy(szDay, DecodePass(szAuthData));
  return szDay;
}

int WriteRegRunDays()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  LPBYTE SetContent_S;		//字符串類型
  char szTemp[256];
  char szGUID[256];
  
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\JuneySoft\\System\\INITDATA";  //欲打開注冊表值的地址
  ValueName = "RunDays";               //欲設置值的名稱
  sprintf(szTemp, "%d", g_dwRegRunDays);
  strcpy(szGUID, EncodePass(szTemp));
  SetContent_S = LPBYTE(szGUID);   //值的內容
  return WriteRegData(RootKey, SubKey, ValueName, SetContent_S);
}

int ReadRegRunDays()
{
  struct HKEY__*RootKey;		//注冊表主鍵名稱
  TCHAR *SubKey;			//欲打開注冊表項的地址
  TCHAR *ValueName;		//欲設置值的名稱
  char szAuthData[256];
  char szRunDays[256];
  
  memset(szAuthData, 0, 256);
  RootKey = HKEY_LOCAL_MACHINE;     //注冊表主鍵名稱
  SubKey = "SOFTWARE\\JuneySoft\\System\\INITDATA";  //欲打開注冊表值的地址
  ValueName = "RunDays";               //欲設置值的名稱
  if (ShowContent(RootKey, SubKey, ValueName, szAuthData) != 0)
    return 0;
  strcpy(szRunDays, DecodePass(szAuthData));
  return atoi(szRunDays);
}

//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-內部調試信息 4-SQL消息 5-錯誤消息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...)
{
  g_cTraceCriticalSection.Lock();
  
  try
  {
    if (MsgType != 0 && MsgType != 9 && g_nSendTraceMsgToLOG == false && g_bServiceDebugId == false)
    {
      if (g_bLogId == false && g_bSaveId == false)
      {
        g_cTraceCriticalSection.Unlock();
        return;
      }
      if ((MsgType == 1 || MsgType == 2) && g_bCommId == false)
      {
        g_cTraceCriticalSection.Unlock();
        return;
      }
      if (MsgType == 3 && g_bDebugId == false)
      {
        g_cTraceCriticalSection.Unlock();
        return;
      }
      if (MsgType == 4 && g_bSQLId == false)
      {
        g_cTraceCriticalSection.Unlock();
        return;
      }
    }
  
    char buf[2048], msgtype[16];
    char LogFile[256];
    FILE	*Ftrace;	//跟蹤日志文件
    SYSTEMTIME sysTime;
  
    va_list		ArgList;
    va_start(ArgList, lpszFormat);
    _vsnprintf(buf, 2048, lpszFormat, ArgList);
    va_end (ArgList);
    CString dispbuf;
  
    switch (MsgType)
    {
    case 0:
      strcpy(msgtype, "ALRM: ");
      break;
    case 1:
      strcpy(msgtype, "RECV: ");
      break;
    case 2:
      strcpy(msgtype, "SEND: ");
      break;
    case 3:
      strcpy(msgtype, "DEBG: ");
      break;
    case 4:
      strcpy(msgtype, "SQLS: ");
      break;
    case 5:
      strcpy(msgtype, "ERR: ");
      break;
    case 6:
      strcpy(msgtype, "HTTP: ");
      break;
    default:
      strcpy(msgtype, "REST: ");
      break;
    }
    /*if (g_nSendTraceMsgToLOG == true || MsgType == 0)
    {
      sprintf(buf1, "%s >> %s%s", MyGetNow(), msgtype, buf);
      for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
      {
        if (LogerStatusSet[nLOG].nLoginId == 2 && (LogerStatusSet[nLOG].LoginLevel == 1 || LogerStatusSet[nLOG].LoginLevel == 2) 
          && (LogerStatusSet[nLOG].bGetIVRMsg == true || MsgType == 0))
          SendMsg2LOG(nLOG, LOGMSG_ongetivrtracemsg, buf1);
      }
    }*/
    if (g_bSaveId == true || MsgType == 0)
    {
      GetLocalTime(&sysTime);
    
      sprintf(LogFile, "%s\\IVR_%04d%02d%02d%02d%d.log", g_szLogPath, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute/10);
      Ftrace = fopen(LogFile, "a+t");
      if (Ftrace != NULL)
      {
        fprintf(Ftrace, "%s >> %s%s\n", MyGetNow(), msgtype, buf);
      }
      if (Ftrace != NULL)
      {
        fclose(Ftrace);
        Ftrace = NULL;
      }
    }
    if (g_bServiceDebugId == TRUE)
    {
      printf("%s >> %s%s\n", MyGetNow(), msgtype, buf);
      g_cTraceCriticalSection.Unlock();
      return;
    }
    #ifdef CONSOLE_RUN_MODE
    printf("%s >> %s%s\n", MyGetNow(), msgtype, buf);
    #endif
  }
  catch (...)
  {
  }
  
  g_cTraceCriticalSection.Unlock();
}
//--------------------------------------------------------------------------
bool GetCTIHAServiceDownFlag()
{
  bool bDownFlag;
  
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bCTIHAServiceDown;
  g_cDownCriticalSection.Unlock();
  
  return bDownFlag;
}
void SetCTIHAServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_nCTIHAServiceDownTimer = time(0);
  g_bCTIHAServiceDown = true;
  g_cDownCriticalSection.Unlock();
}
void ClearCTIHAServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_bCTIHAServiceDown = false;
  g_cDownCriticalSection.Unlock();
}
void SetCTIHALinkFlag(unsigned long serverid, unsigned long clientid)
{
  g_cHALinkCriticalSection.Lock();
  if (g_nHostType == 0)
  {
    //主機
    if (clientid == 0)
    {
      StandbyIVRClientId = 0;
      pTCPHALink->SetStandbyIVRUnlink();
    }
    else
    {
      g_DestIVRServerId = (short)(clientid & 0xFFFF);
      StandbyIVRClientId = clientid;
      pTCPHALink->SetStandbyIVRLinked();
      pTCPHALink->isLinked = true;
    }
  } 
  else
  {
    //備機
    if (clientid == 0)
    {
      pTCPHALink->SetMasterIVRUnlink();
    }
    else
    {
      g_DestIVRServerId = (short)(serverid & 0xFFFF);
      pTCPHALink->SetMasterIVRLinked();
      pTCPHALink->isLinked = true;
    }
  }
  g_cHALinkCriticalSection.Unlock();
}
bool GetCTIHALinkFlag()
{
  bool bLinked;

  if (g_nMultiRunMode == 0 || pTCPHALink == NULL)
  {
    return false;
  }

  g_cHALinkCriticalSection.Lock();
  if (g_nHostType == 0)
  {
    bLinked = pTCPHALink->IsStandbyIVRConnected();
  }
  else
  {
    bLinked = pTCPHALink->IsMasterIVRConnected();
  }
  g_cHALinkCriticalSection.Unlock();
  return bLinked;
}
//

bool GetFLWServiceDownFlag()
{
  bool bDownFlag;
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bFLWServiceDown;
  g_cDownCriticalSection.Unlock();
  return bDownFlag;
}
void SetFLWServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_nFLWServiceDownTimer = time(0);
  g_bFLWServiceDown = true;
  g_cDownCriticalSection.Unlock();
}
void ClearFLWServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_bFLWServiceDown = false;
  g_cDownCriticalSection.Unlock();
}
//

bool GetSWTServiceDownFlag()
{
  bool bDownFlag;
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bSWTServiceDown;
  g_cDownCriticalSection.Unlock();
  return bDownFlag;
}
void SetSWTServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_nSWTServiceDownTimer = time(0);
  g_bSWTServiceDown = true;
  g_cDownCriticalSection.Unlock();
}
void ClearSWTServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_bSWTServiceDown = false;
  g_cDownCriticalSection.Unlock();
}
//

bool GetWSServiceDownFlag()
{
  bool bDownFlag;
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bWSServiceDown;
  g_cDownCriticalSection.Unlock();
  return bDownFlag;
}
void SetWSServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_nWSServiceDownTimer = time(0);
  g_bWSServiceDown = true;
  g_cDownCriticalSection.Unlock();
}
void ClearWSServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_bWSServiceDown = false;
  g_cDownCriticalSection.Unlock();
}
//

bool GetDBServiceDownFlag()
{
  bool bDownFlag;
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bDBServiceDown;
  g_cDownCriticalSection.Unlock();
  return bDownFlag;
}
void SetDBServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_nDBServiceDownTimer = time(0);
  g_bDBServiceDown = true;
  g_cDownCriticalSection.Unlock();
}
void ClearDBServiceDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_bDBServiceDown = false;
  g_cDownCriticalSection.Unlock();
}
//2016-10-05增加DB連接失敗及成功設置
bool GetDBConnDownFlag()
{
  bool bDownFlag;
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bDBConnDown;
  g_cDownCriticalSection.Unlock();
  return bDownFlag;
}
void SetDBConnDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_nDBConnDownTimer = time(0);
  g_bDBConnDown = true;
  g_cDownCriticalSection.Unlock();
}
void ClearDBConnDownFlag()
{
  g_cDownCriticalSection.Lock();
  g_bDBConnDown = false;
  g_cDownCriticalSection.Unlock();
}
//---
bool GetIVRChBlockFlag()
{
  bool bDownFlag;
  g_cDownCriticalSection.Lock();
  bDownFlag = g_bIVRChBlock;
  g_cDownCriticalSection.Unlock();
  return bDownFlag;
}
void SetIVRChBlockFlag()
{
  g_cDownCriticalSection.Lock();
  g_nIVRChBlockTimer = time(0);
  g_bIVRChBlock = true;
  g_cDownCriticalSection.Unlock();
}
void ClearIVRChBlockFlag()
{
  g_cDownCriticalSection.Lock();
  g_bIVRChBlock = false;
  g_cDownCriticalSection.Unlock();
}
//-----------------------------------------------------------------------------
bool isMyselfInActive()
{
  if (g_nMultiRunMode == 0)
    return true;
//   MyTrace(3, "HostType=%d MasterStatus=%d StandbyStatus=%d",
//     g_nHostType, g_nMasterStatus, g_nStandbyStatus);
  if (g_nHostType == 0)
  {
    return g_nMasterStatus == 1 ? true : false;
  }
  else
  {
    return g_nStandbyStatus == 1 ? true : false;
  }
}
void CloseVoiceCard()
{
  if (pIVRCfg->VoiceCardInitState == 1)
  {
    MyTrace(3, "CloseVoiceCard");
    MAININIT(9); //關閉語音卡
    ClearIVRChBlockFlag();
    g_dwRegServerState = g_dwRegServerState & 0x7F;
    WriteRegServiceRunState();
  }
}
void OpenVoiceCard()
{
  if (pIVRCfg->VoiceCardInitState == 1)
  {
    MyTrace(3, "OpenVoiceCard");
    MAININIT(1); //重啟語音卡
  }
}
void StopVoiceCard()
{
  if (g_nMultiRunMode == 0 || pIVRCfg->VoiceCardInitState == 0)
    return;
  if (g_nHostType == 0)
  {
    if (g_nMasterStatus == 1)
    {
      return;
    }
  }
  else
  {
    if (g_nStandbyStatus == 1)
    {
      return;
    }
  }
  MyTrace(3, "StopVoiceCard");
  MAININIT(9); //關閉語音卡
}
//處理CTI服務器之間的連接故障
void ProcCTILanDown()
{
  if (g_nMultiRunMode == 0 || g_nLanDownHAMode == 0)
  {
    return;
  }
  if ((g_DestCTIRunState&0xFF) != 0)
  {
    //如果對方系統運行狀態有問題就不切換
    return;
  }

  if (GetCTIHAServiceDownFlag() == true)
  {
    if ((time(0) - g_nCTIHAServiceDownTimer) > g_nLanDownCheckTimeLen)
    {
      ClearCTIHAServiceDownFlag();
      if (g_nHostType == 0)
      {
        //本機是主機
        if (CheckLocaNetCable(g_szAdapterName) == 0)
        {
          //本機為主機，且本機網絡沒斷開，備機那邊有問題
          if (g_nMasterStatus == 0)
          {
            //切換到主機運行
            OpenVoiceCard();

            g_nMasterStatus = 1;
            g_nStandbyStatus = 0;
            //發送切換指令
            MyTrace(3, "MasterCTIServer Switchover to Active because StandbyCTIServer DisConnect!!!");
            SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandByCTIServer DisConnect");
            WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server IVRSERVER is interrupted and is switched to the main server.");
            //SendAllLoginWorkerstatus();
            //SendClearAllGroupCountToAllMON();
            //SendAllAGCountToAllMON(); //發送主機的座席組統計數據
          }
          else
          {
            MyTrace(3, "MasterCTIServer Had in Active StandbyCTIServer DisConnect!!!");
          }
        }
        else
        {
          //本機為主機，主機網絡斷開，維持原狀態不變
          MyTrace(3, "MasterCTIServer keep state for LAN DisConnect!!!");
        }
        strcpy(g_szSwitchTime, MyGetNow());
        SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
      }
      else
      {
        //本機是備機
        if (CheckLocaNetCable(g_szAdapterName) == 0)
        {
          //本機為備機，但本機網絡沒斷開
          if (g_nStandbyStatus == 0)
          {
            OpenVoiceCard();

            g_nMasterStatus = 0;
            g_nStandbyStatus = 1;
            //發送切換指令
            MyTrace(3, "StandbyCTIServer Switchover to Active because MasterCTIServer DisConnect!!!");
            SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterCTIServer DisConnect");
            WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server IVRSERVER is interrupted and is switched to the standby server.");
            //SendAllLoginWorkerstatus();
            //SendClearAllGroupCountToAllMON();
            //SendAllAGCountToAllMON(); //發送備機的座席組統計數據
          }
          else
          {
            MyTrace(3, "StandbyCTIServer Had in Active MasterCTIServer DisConnec!!!");
          }
        }
        else
        {
          //本機為備機，本機網絡已斷開，維持原狀態不變
          MyTrace(3, "StandbyCTIServer keep state for LAN DisConnect!!!");
        }
        strcpy(g_szSwitchTime, MyGetNow());
        SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
      }
    }
  }
  else if (GetFLWServiceDownFlag() == true) //流程服務故障
  {
    if (GetCTIHAServiceDownFlag() == false //CTI服務之間的通信沒有斷開過
      && GetCTIHALinkFlag() == true //CTI服務之間是連接狀態
      && (time(0) - g_nFLWServiceDownTimer) > g_nLanDownCheckTimeLen)
    {
      ClearFLWServiceDownFlag();
      if (g_nHostType == 0)
      {
        //本機為主機，切換到備機運行
        if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 0;
        g_nStandbyStatus = 1;
        //發送切換指令
        MyTrace(3, "StandbyCTIServer Switchover to Active because MasterFLW DisConnect!!!");
        SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because MasterFLW DisConnect");
        SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterFLW DisConnect");
        WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server FLWSERVER is interrupted and is switched to the standby server.");
      } 
      else
      {
        //本機為備機，切換到主機運行
        if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 1;
        g_nStandbyStatus = 0;
        //發送切換指令
        MyTrace(3, "MasterCTIServer Switchover to Active because StandbyFLW DisConnect!!!");
        SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because StandbyFLW DisConnect");
        SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyFLW DisConnect");
        WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server FLWSERVER is interrupted and is switched to the main server.");
      }
      strcpy(g_szSwitchTime, MyGetNow());
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
  else if (GetSWTServiceDownFlag() == true) //交換機CTILINK服務故障
  {
    if (GetCTIHAServiceDownFlag() == false //CTI服務之間的通信沒有斷開過
      && GetCTIHALinkFlag() == true //CTI服務之間是連接狀態
      && (time(0) - g_nSWTServiceDownTimer) > g_nLanDownCheckTimeLen)
    {
      ClearSWTServiceDownFlag();
      if (g_nHostType == 0)
      {
        //本機為主機，切換到備機運行
        if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 0;
        g_nStandbyStatus = 1;
        //發送切換指令
        MyTrace(3, "StandbyCTIServer Switchover to Active because MasterCTILINK DisConnect!!!");
        SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because MasterCTILINK DisConnect");
        SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterCTILINK DisConnect");
        WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server CTILINKSERVER is interrupted and is switched to the standby server.");
      } 
      else
      {
        //本機為備機，切換到主機運行
        if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 1;
        g_nStandbyStatus = 0;
        //發送切換指令
        MyTrace(3, "MasterCTIServer Switchover to Active because StandbyCTILINK DisConnect!!!");
        SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because StandbyCTILINK DisConnect");
        SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyCTILINK DisConnect");
        WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server CTILINKSERVER is interrupted and is switched to the main server.");
      }
      strcpy(g_szSwitchTime, MyGetNow());
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
  else if (GetWSServiceDownFlag() == true) //WebSocket服務故障
  {
    if (GetCTIHAServiceDownFlag() == false //CTI服務之間的通信沒有斷開過
      && GetCTIHALinkFlag() == true //CTI服務之間是連接狀態
      && (time(0) - g_nWSServiceDownTimer) > g_nLanDownCheckTimeLen)
    {
      ClearWSServiceDownFlag();
      if (g_nHostType == 0)
      {
        //本機為主機，切換到備機運行
        if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 0;
        g_nStandbyStatus = 1;
        //發送切換指令
        MyTrace(3, "StandbyCTIServer Switchover to Active because MasterWEBSCOKET DisConnect!!!");
        SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because MasterWEBSCOKET DisConnect");
        SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterWEBSCOKET DisConnect");
        WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server WEBSOCKER is interrupted and is switched to the standby server.");
      } 
      else
      {
        //本機為備機，切換到主機運行
        if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 1;
        g_nStandbyStatus = 0;
        //發送切換指令
        MyTrace(3, "MasterCTIServer Switchover to Active because StandbyWEBSCOKET DisConnect!!!");
        SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because StandbyWEBSCOKET DisConnect");
        SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyWEBSCOKET DisConnect");
        WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server WEBSCOKET is interrupted and is switched to the main server.");
      }
      strcpy(g_szSwitchTime, MyGetNow());
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
  else if (GetDBServiceDownFlag() == true) //DB數據庫服務故障
  {
    if (GetCTIHAServiceDownFlag() == false //CTI服務之間的通信沒有斷開過
      && GetCTIHALinkFlag() == true //CTI服務之間是連接狀態
      && (time(0) - g_nDBServiceDownTimer) > g_nLanDownCheckTimeLen)
    {
      ClearDBServiceDownFlag();
      if (g_nHostType == 0)
      {
        //本機為主機，切換到備機運行
        if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 0;
        g_nStandbyStatus = 1;
        //發送切換指令
        MyTrace(3, "StandbyCTIServer Switchover to Active because MasterDBServiceDown!!!");
        SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because MasterDBServiceDown");
        SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterDBServiceDown");
        WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server DBSERVER is interrupted and is switched to the standby server.");
      } 
      else
      {
        //本機為備機，切換到主機運行
        if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 1;
        g_nStandbyStatus = 0;
        //發送切換指令
        MyTrace(3, "MasterCTIServer Switchover to Active because StandbyDBServiceDown!!!");
        SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because StandbyDBService DisConnect");
        SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyDBService DisConnect");
        WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server DBSERVER is interrupted and is switched to the main server.");
      }
      strcpy(g_szSwitchTime, MyGetNow());
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
  else if (g_nDBDownSwitchOverId == 1 && GetDBConnDownFlag() == true) //DB數據庫連接故障 //2016-10-05
  {
    if (GetCTIHAServiceDownFlag() == false 
      && GetCTIHALinkFlag() == true //CTI服務之間是連接狀態
      && (time(0) - g_nDBConnDownTimer) > g_nLanDownCheckTimeLen)
    {
      //g_bDBConnDown = false;
      //g_nDBConnDownTimer = 0;
      ClearDBConnDownFlag(); //2016-10-05
      if (g_nHostType == 0)
      {
        //本機為主機，切換到備機運行
        if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 0;
        g_nStandbyStatus = 1;
        //發送切換指令
        MyTrace(3, "StandbyCTIServer Switchover to Active because MasterDBConnDown!!!");
        SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because MasterDBConn DisConnect");
        SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterDBConn DisConnect");
        WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server DBCONNECT is interrupted and is switched to the standby server.");
      } 
      else
      {
        //本機為備機，切換到主機運行
        if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 1;
        g_nStandbyStatus = 0;
        //發送切換指令
        MyTrace(3, "MasterCTIServer Switchover to Active because StandbyDBConnDown!!!");
        SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because StandbyDBConn DisConnect");
        SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyDBConn DisConnect");
        WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server DBCONNECT is interrupted and is switched to the main server.");
      }
      strcpy(g_szSwitchTime, MyGetNow());
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
  else if (GetIVRChBlockFlag() == true) //IVR所有通道阻塞
  {
    if (GetCTIHAServiceDownFlag() == false 
      && GetCTIHALinkFlag() == true //CTI服務之間是連接狀態
      && (time(0) - g_nIVRChBlockTimer) > g_nLanDownCheckTimeLen)
    {
      ClearIVRChBlockFlag();
      if (g_nHostType == 0)
      {
        //本機為主機，切換到備機運行
        if (g_nMasterStatus == 0 && g_nStandbyStatus == 1) //主機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 0;
        g_nStandbyStatus = 1;
        //發送切換指令
        MyTrace(3, "StandbyCTIServer Switchover to Active because Master AllIVRCH BLOCK!!!");
        SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Switchover to Active because Master AllIVRCH BLOCK");
        SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because AllIVRCH BLOCK");
        WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "The main server IVRCHBLOCK is interrupted and is switched to the standby server.");
      } 
      else
      {
        //本機為備機，切換到主機運行
        if (g_nMasterStatus == 1 && g_nStandbyStatus == 0) //備機已處于待機狀態
          return;
        CloseVoiceCard();
        //WriteAllCDR();
        SendAllLogoutWorkerstatus();
        //ResetAllChnForDown();
        g_nMasterStatus = 1;
        g_nStandbyStatus = 0;
        //發送切換指令
        MyTrace(3, "MasterCTIServer Switchover to Active because Standby AllIVRCH BLOCK!!!");
        SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "MasterCTIServer Switchover to Active because Standby AllIVRCH BLOCK");
        SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because Standby AllIVRCH BLOCK");
        WriteAlarmList(1016, ALARM_LEVEL_MAJOR, 1, "The standby server IVRCHBLOCK is interrupted and is switched to the main server.");
      }
      strcpy(g_szSwitchTime, MyGetNow());
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
}
void ProcHAMsg(UL remoteid, US msgtype, const char *buf)
{
  CXMLMsg RecvHAMsg;
  CStringX ArrString[64];
  UC MsgType, MsgId;

  pTCPHALink->DisCombineMsgID(msgtype, MsgType, MsgId);
  RecvHAMsg.SetMsgBufId(MsgId, buf);
  RecvHAMsg.SetClientId(remoteid);
//   if (g_nHostType == 0)
//     MyTrace(1, "MasterCTIServer recv msgno=0x%04x MsgType=%d MsgId=%d msg=%s",
//       msgtype, MsgType, MsgId, (char *)buf);
//   else
//     MyTrace(1, "StandbyCTIServer recv msgno=0x%04x MsgType=%d MsgId=%d msg=%s",
//     msgtype, MsgType, MsgId, (char *)buf);
  
  if (MsgType == MSGTYPE_HAIVR)
  {
    switch (MsgId)
    {
    case 0:
      {
        if (g_nHostType == 0)
        {
          if(0==RecvHAMsg.ParseSndMsg(pFlwRule->S_TO_MMsgRule[MsgId]))
          {
            //本機是主機，備機發來的消息
            int masterstatus = atoi(RecvHAMsg.GetAttrValue(3).C_Str());
            int standbystatus = atoi(RecvHAMsg.GetAttrValue(4).C_Str());
            MyTrace(3, "MasterCTIServer g_nMasterStatus=%d g_nStandbyStatus=%d masterstatus=%d standbystatus=%d", 
              g_nMasterStatus, g_nStandbyStatus, masterstatus, standbystatus);
            if (standbystatus == 1)
            {
              //備機處于運行狀態
              if (g_nMasterStatus == 1)
              {
                //主機也處于運行狀態，強制備機待機
                g_nStandbyStatus = 0;
                MyTrace(3, "Force Switchover StandbyCTIServer to DisActive because MasterCTIServer in Active!!!");
                SendMsgToStandbyIVR(g_nMasterStatus, 0, "Force Switchover StandbyCTIServer to DisActive because MasterCTIServer in Active");
                WriteRegServiceRunState();
                //SendClearAllGroupCountToAllMON();
                //SendAllAGCountToAllMON(); //發送主機的座席組統計數據
              }
              else
              {
                CloseVoiceCard();
                g_nMasterStatus = 0;
                g_nStandbyStatus = 1;
                MyTrace(3, "MasterCTIServer set in DisActive and StandbyCTIServer in Active!!!");
              }
            }
            else
            {
              //備機處于待機狀態
              g_nStandbyStatus = 0;
              if (g_nMasterStatus == 0)
              {
                //主機切換為運行狀態
                OpenVoiceCard();
                g_nMasterStatus = 1;
                MyTrace(3, "MasterCTIServer Switchover to Active because StandbyCTIServer disactive or breakdown!!!");
                SendSwitchoverToAllClient(1, "MasterCTIServer Switchover to Active because StandbyCTIServer disactive or breakdown");
                WriteRegServiceRunState();
                //SendAllLoginWorkerstatus();
                //SendClearAllGroupCountToAllMON();
                //SendAllAGCountToAllMON(); //發送主機的座席組統計數據
              } 
              //主機已為運行狀態
              SendMsgToStandbyIVR(g_nMasterStatus, g_nStandbyStatus, "Return MasterCTIServer status");
            }
            strcpy(g_szSwitchTime, MyGetNow());
            SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
          }
        }
        else
        {
          if(0==RecvHAMsg.ParseRevMsg(pFlwRule->M_TO_SMsgRule[MsgId]))
          {
            //本機是備機，主機發來的消息
            int masterstatus = atoi(RecvHAMsg.GetAttrValue(3).C_Str());
            int standbystatus = atoi(RecvHAMsg.GetAttrValue(4).C_Str());
            MyTrace(1, "StandbyCTIServer g_nMasterStatus=%d g_nStandbyStatus=%d masterstatus=%d standbystatus=%d", 
              g_nMasterStatus, g_nStandbyStatus, masterstatus, standbystatus);
            if (masterstatus == 1)
            {
              //主機處于運行狀態
              g_nMasterStatus = 1;
              if (g_nStandbyStatus == 1)
              {
                //備機也處于運行狀態，強制待機
                CloseVoiceCard();
                g_nStandbyStatus = 0;
                MyTrace(3, "Force Switchover StandbyCTIServer to DisActive because MasterCTIServer in Active!!!");
                SendSwitchoverToAllClient(0, "Force Switchover StandbyCTIServer to DisActive because MasterCTIServer in Active");
                WriteRegServiceRunState();
              }
            }
            else
            {
              //主機處于待機狀態
              g_nMasterStatus = 0;
              if (g_nStandbyStatus == 0)
              {
                //備機切換為運行狀態
                OpenVoiceCard();
                g_nStandbyStatus = 1;
                MyTrace(3, "StandbyCTIServer Switchover to Active because MasterCTIServer DisActive!!!");
                SendSwitchoverToAllClient(1, "StandbyCTIServer Switchover to Active because MasterCTIServer DisActive");
                WriteRegServiceRunState();
                //SendAllLoginWorkerstatus();
                //SendClearAllGroupCountToAllMON();
              } 
              else
              {
                //備機已為運行狀態
                OpenVoiceCard();
                SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "Return StandbyIVR status");
              }
            }
            strcpy(g_szSwitchTime, MyGetNow());
            SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
          }
        }
      }
      break;
    case 1:
      {
        if(0==RecvHAMsg.ParseRevMsg(pFlwRule->M_TO_SMsgRule[MsgId]))
        {
          /*int svState = atoi(RecvHAMsg.GetAttrValue(7).C_Str());
          CAgent *pAgent = pAgentMng->GetAgentBySeatNo(RecvHAMsg.GetAttrValue(1).C_Str());
          if (pAgent)
          {
            if (svState == 21)
            {
              int nWorkerNo = atoi(RecvHAMsg.GetAttrValue(2).C_Str());
              int nGroupNo = atoi(RecvHAMsg.GetAttrValue(4).C_Str());
              int nDutyNo = atoi(RecvHAMsg.GetAttrValue(6).C_Str());
              int nDisturbId = atoi(RecvHAMsg.GetAttrValue(8).C_Str());
              pAgent->m_Worker.DisturbId = (UC)nDisturbId;
              pAgent->m_Worker.DutyNo = (UC)nDutyNo;
              pAgent->m_Worker.DisturbReason = RecvHAMsg.GetAttrValue(9);
              pAgent->m_Worker.SetWorkerData(nWorkerNo, RecvHAMsg.GetAttrValue(3).C_Str(), 1);
              if (nGroupNo > 0)
              {
                strcpy(pAgent->m_Worker.SwtGroupID, RecvHAMsg.GetAttrValue(4).C_Str());
                pAgent->m_Worker.SetWorkerGroupNo(nGroupNo, 1);
                CWorkerGroup *pWorkerGroup = pAgentMng->WorkerGroupMng.AllocWorkerGroupByGroupNo(nGroupNo);
                if (pWorkerGroup)
                {
                  pWorkerGroup->state = 1;
                  pWorkerGroup->GroupNo = nGroupNo;
                  pAgent->m_Worker.GroupSaveId = pWorkerGroup->GroupSaveId;
                  pAgent->m_Worker.OldGroupSaveId = pWorkerGroup->GroupSaveId;
                  if (pIVRCfg->isAutoGroupQueueToAgent == true)
                    pAgent->m_Worker.GetGroupQueueGroupNo[0] = nGroupNo;
                  CreateGroupCountToDB(pWorkerGroup);
                }
              }
              pAgent->m_Worker.SetLoginState();
              DispAgentStatus(pAgent->nAG);
            }
            else if (svState == 22)
            {
              pAgent->m_Worker.Logout();
              pAgent->m_Worker.ClearState();
              DispAgentStatus(pAgent->nAG);
            }
            else
            {
              int nWorkerNo = atoi(RecvHAMsg.GetAttrValue(2).C_Str());
              int nGroupNo = atoi(RecvHAMsg.GetAttrValue(4).C_Str());
              int nDutyNo = atoi(RecvHAMsg.GetAttrValue(6).C_Str());
              int nDisturbId = atoi(RecvHAMsg.GetAttrValue(8).C_Str());
              pAgent->m_Worker.DisturbId = (UC)nDisturbId;
              pAgent->m_Worker.DutyNo = (UC)nDutyNo;
              pAgent->m_Worker.DisturbReason = RecvHAMsg.GetAttrValue(9);
              pAgent->m_Worker.SetWorkerData(nWorkerNo, RecvHAMsg.GetAttrValue(3).C_Str(), 1);
              if (nGroupNo > 0)
              {
                strcpy(pAgent->m_Worker.SwtGroupID, RecvHAMsg.GetAttrValue(4).C_Str());
                pAgent->m_Worker.SetWorkerGroupNo(nGroupNo, 1);
                CWorkerGroup *pWorkerGroup = pAgentMng->WorkerGroupMng.AllocWorkerGroupByGroupNo(nGroupNo);
                if (pWorkerGroup)
                {
                  pWorkerGroup->state = 1;
                  pWorkerGroup->GroupNo = nGroupNo;
                  pAgent->m_Worker.GroupSaveId = pWorkerGroup->GroupSaveId;
                  pAgent->m_Worker.OldGroupSaveId = pWorkerGroup->GroupSaveId;
                  if (pIVRCfg->isAutoGroupQueueToAgent == true)
                    pAgent->m_Worker.GetGroupQueueGroupNo[0] = nGroupNo;
                  CreateGroupCountToDB(pWorkerGroup);
                }
              }
              pAgent->svState = 0;//svState;
              DispAgentStatus(pAgent->nAG);
            }
          }*/
        }
      }
      break;
    case 2:
      {
        if(0==RecvHAMsg.ParseRevMsg(pFlwRule->M_TO_SMsgRule[MsgId]))
        {
          g_DestCTIRunState = atoi(RecvHAMsg.GetAttrValue(5).C_Str());
          MyTrace(0, "DestCTIRunState=%d", g_DestCTIRunState);
        }
      }
      break;
    }
  }
}
//-------------------通信消息處理-------------------------------------------
//處理登錄退出消息隊列
void CheckLogInOutFifo()
{
  US ServerId, ClientId, NodeId;
  US count = 0;
  UC NodeType, LogInOut;

  while(count++ < 256) //一次最多處理128條信息
  {
    if (pLogInOutFifo->Read(ServerId, ClientId, NodeType, NodeId, LogInOut) != 0)
    {
      switch (NodeType)
      {
      case NODE_IVR:
        {
          if (g_nMultiRunMode == 0)
          {
            break;
          }
          if (LogInOut == 1)
          {
            char szMsgBuf[512];
            sprintf(szMsgBuf, "<onsysrunstate sessionid='%ld' nodetype='%d' nodeno='%d' masterstatus='%d' standbystatus='%d' runstate='%d'/>",
              0, 0, g_nHostType==0?1:2, g_nMasterStatus, g_nStandbyStatus, g_dwRegServerState);
            MyTrace(2, "%s", szMsgBuf);
            
            //CTI服務器之間已連接,發送登錄的坐席到對方
            if (g_nHostType == 0)
            {
              //本機是主機
              pTCPHALink->SendMessage2CLIENT(StandbyIVRClientId, (MSGTYPE_HAIVR<<8) | HAMSG_onsysrunstate, szMsgBuf);
              
              SendAllAGLoginStateToDesCTI();
            }
            else
            {
              //本機是備機
              pTCPHALink->SendMessage2Master(HAMSG_onsysrunstate, szMsgBuf);
              
              SendAllAGLoginStateToDesCTI();
            }
          } 
        }
        break;
      case NODE_SWT:
        {
          if (LogInOut == 1)
          {
            if (g_nSwitchMode == 0 || g_nSwitchMode == 10)
            {
              CTILinkClientId = ClientId;
              CTILinkLogId = true;
              MyTrace(0, "CTILinkSERVICE[%d] Login", NodeId);
              return;
            }
            g_dwRegServerState = g_dwRegServerState & 0xFD;
            WriteRegServiceRunState();
            CTILinkClientId = ClientId;
            CTILinkLogId = true;
            MyTrace(0, "CTILinkSERVICE[%d] Login", NodeId);
            WriteAlarmList(1006, ALARM_LEVEL_MINOR, 0, "CTILINKService Connected");
          } 
          else
          {
            if (g_nSwitchMode == 0 || g_nSwitchMode == 10)
            {
              CTILinkClientId = 0;
              CTILinkLogId = false;
              MyTrace(0, "CTILinkSERVICE[%d] Logout", NodeId);
              return;
            }
            g_dwRegServerState = g_dwRegServerState | 0x02;
            WriteRegServiceRunState();
            MyTrace(0, "CTILinkSERVICE[%d] Logout", NodeId);
            CTILinkClientId = 0;
            CTILinkLogId = false;
            g_bChnInit = false;
            pBoxchn->ResetAllChn();
            ResetAllAG();
            WriteAlarmList(1005, ALARM_LEVEL_MAJOR, 1, "CTILINKService DisConnected");
          }
        }
        break;
      case NODE_XML:
        {
          if (LogInOut == 1)
          {
            if (NodeId < MAX_NODE_XML)
            {
              g_dwRegServerState = g_dwRegServerState & 0xFE;
              WriteRegServiceRunState();
              pParser->SetLogin(NodeId);
              g_bAlarmId = false;
              MyTrace(0, "ParseFlw[%d] Login", NodeId);
              WriteAlarmList(1010, ALARM_LEVEL_MINOR, 0, "FLWService Connected");
            }
          } 
          else
          {
            if (NodeId < MAX_NODE_XML)
            {
              g_dwRegServerState = g_dwRegServerState | 0x01;
              WriteRegServiceRunState();
              pParser->SetLogout(NodeId);
              pParser->DelPreCodeForVxmlLogout(NodeId);
              g_bAlarmId = true;
              ResetAllChnForParserLogout((UC)NodeId);
              MyTrace(0, "ParseFlw[%d] Logout", NodeId);
              WriteAlarmList(1009, ALARM_LEVEL_MAJOR, 1, "FLWService DisConnected");
            }
          }
        }
        break;
      case NODE_CSC:
        {
          int nAG, nQue, nGroupNo;
          if (LogInOut == 1)
          {
            MyTrace(0, "Seat ClientId[%d] Login", NodeId);
          } 
          else
          {
            if (pIVRCfg->isWorkerLogOutAfterunTcpLink == true)
            {
              nAG = pAgentMng->GetnAGByClientId(ClientId);
              if (pAgentMng->isnAGAvail(nAG))
              {
                nQue = pAG->nQue;
                if (pCalloutQue->isnQueAvail(nQue) && pAG->isWantSeatType(SEATTYPE_AG_PC))
                {
                  pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
                }
                pAgentMng->ClearClientId(nAG);
              
                if (pAG->svState == AG_SV_IMCHAT || pAG->svState == AG_SV_PROC_EMAIL || pAG->svState == AG_SV_PROC_FAX)
                {
                  pAG->SetAgentsvState(AG_SV_IDEL);
                }
                
                if (pAG->m_Worker.DisturbId != 3)
                {
                  if (pAG->m_Worker.WorkerCallCountParam.CurStatus != AG_STATUS_LOGOUT)
                  {
                    WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
                    WriteWorkerLogoutStatus(pAG);
                  }
                  nGroupNo = pAG->GetGroupNo();
                  if (pAG->m_Worker.oldWorkerNo == 0)
                  {
                    pAG->m_Worker.Logout();
                    pAG->m_Worker.ClearState();
                  }
                  else
                  {
                    pAG->m_Worker.OldWorkerLogin();
                  }
                  WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
                  AgentStatusCount();
                  SendOneGroupStatusMeteCount(pAG->nAG);
                  for (int i=1; i<MAX_GROUP_NUM_FORWORKER; i++)
                    SendOneGroupStatusMeteCountB(pAG->m_Worker.PreGroupNoList[i]);
                  SendSystemStatusMeteCount();
                }
                DispAgentStatus(nAG);
                MyTrace(0, "Seat ClientId[%d] Logout and Worker Logout", NodeId);
              }
            }
            else
            {
              nAG = pAgentMng->GetnAGByClientId(ClientId);
              if (pAgentMng->isnAGAvail(nAG))
              {
                pAgentMng->ClearClientId(nAG);
                DispAgentStatus(nAG);
              }
              MyTrace(0, "Seat ClientId[%d] Logout", NodeId);
            }
          }
        }
        break;
      case NODE_LOG:
        {
          if (LogInOut == 1)
          {
            for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
            {
              if (LogerStatusSet[nLOG].nLoginId == 0 && LogerStatusSet[nLOG].LogerClientId == 0)
              {
                LogerStatusSet[nLOG].nLoginId = 1;
                LogerStatusSet[nLOG].LogerClientId = (UC)NodeId;
                break;
              }
            }
          } 
          else
          {
            for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
            {
              if (LogerStatusSet[nLOG].LogerClientId == NodeId)
              {
                LogerStatusSet[nLOG].nLoginId = 0;
                LogerStatusSet[nLOG].LogerClientId = 0;
                LogerStatusSet[nLOG].LoginLevel = 0;
                CheckLOGTraceMsgId();
              }
            }
          }
        }
        break;
      case NODE_DB:
        {
          if (LogInOut == 1)
          {
            if (NodeId == 1)
            {
              g_dwRegServerState = g_dwRegServerState & 0xFB;
              WriteRegServiceRunState();
              pDBClient->SetDBLinked();
              MyTrace(0, "Connect DBserver success!!!");
              WriteAlarmList(1008, ALARM_LEVEL_MINOR, 0, "DBService Connected");
            }
            else if (NodeId == 2)
            {
              g_dwRegServerState = g_dwRegServerState & 0xFB;
              WriteRegServiceRunState();
              pTcpLink->SetGWLinked();
              MyTrace(0, "Connect GWserver success!!!");
              WriteAlarmList(1008, ALARM_LEVEL_MINOR, 0, "GWService Connected");
            }
            else if (NodeId == 3)
            {
              pTcpLink->SetCRMLinked();
              MyTrace(0, "Connect CRMserver success!!!");
              WriteAlarmList(1008, ALARM_LEVEL_MINOR, 0, "CRMService Connected");
            }
            else if (NodeId == 4)
            {
              pDBClient->SetIMLinked();
              MyTrace(0, "Connect IMserver success!!!");
              WriteAlarmList(1008, ALARM_LEVEL_MINOR, 0, "IMService Connected");
            }
          } 
          else
          {
            if (NodeId == 1)
            {
              g_dwRegServerState = g_dwRegServerState | 0x04;
              WriteRegServiceRunState();
              pDBClient->SetDBUnlink();
              MyTrace(0, "DisConnect DBserver!!!");
              WriteAlarmList(1007, ALARM_LEVEL_MAJOR, 1, "DBService DisConnected");
            }
            else if (NodeId == 2)
            {
              g_dwRegServerState = g_dwRegServerState | 0x04;
              WriteRegServiceRunState();
              pTcpLink->SetGWUnlink();
              MyTrace(0, "DisConnect GWserver!!!");
              WriteAlarmList(1007, ALARM_LEVEL_MAJOR, 1, "GWService DisConnected");
            }
            else if (NodeId == 3)
            {
              pTcpLink->SetCRMUnlink();
              MyTrace(0, "DisConnect CRMserver!!!");
              WriteAlarmList(1007, ALARM_LEVEL_MAJOR, 1, "CRMService DisConnected");
            }
            else if (NodeId == 4)
            {
              pDBClient->SetIMUnlink();
              MyTrace(0, "DisConnect IMserver!!!");
              WriteAlarmList(1007, ALARM_LEVEL_MAJOR, 1, "IMService DisConnected");
            }
          }
        }
        break;
      case NODE_VOP:
        {
          if (LogInOut == 1)
          {
            if (g_nSwitchMode == 0 || pVopGroup == NULL)
            {
              if (g_nCardType == 9)
              {
                TestPhoneClientId = ClientId;
                TestPhoneLogId = true;
                MyTrace(0, "TestPhone[%d] Login", NodeId);
              }
              return;
            }
            if (NodeId < pVopGroup->m_nVopNum)
            {
              pVopGroup->m_pVops[NodeId].m_usClientId = ClientId;
              pVopGroup->m_pVops[NodeId].m_nLoginState = 1;
            }
            g_dwRegServerState = g_dwRegServerState & 0xF7;
            WriteRegServiceRunState();
            MyTrace(0, "VOP[%d] Login", NodeId);
            WriteAlarmList(1018, ALARM_LEVEL_MINOR, 0, "VOPService[%d] Connected", NodeId);
          } 
          else
          {
            if (g_nSwitchMode == 0 || pVopGroup == NULL)
            {
              if (g_nCardType == 9)
              {
                TestPhoneClientId = 0;
                TestPhoneLogId = false;
                MyTrace(0, "TestPhone[%d] Logout", NodeId);
              }
              return;
            }
            if (NodeId < pVopGroup->m_nVopNum)
            {
              pVopGroup->m_pVops[NodeId].m_usClientId = 0;
              pVopGroup->m_pVops[NodeId].m_nLoginState = 2;
              pVopGroup->BlockVopChn(NodeId);
              SendVopStateToAll(NodeId);
              Send_SWTMSG_sendivrstatus(NodeId, 1);
            }
            g_dwRegServerState = g_dwRegServerState | 0x08;
            WriteRegServiceRunState();
            MyTrace(0, "VOP[%d] Logout", NodeId);
            WriteAlarmList(1017, ALARM_LEVEL_MAJOR, 1, "VOPService[%d] DisConnected", NodeId);
          }
        }
        break;
      case NODE_TTS:
        {
          if (LogInOut == 1)
          {
            TTSClientId = ClientId;
            TTSLogId = true;
            MyTrace(0, "TTSProxy[%d] Login", NodeId);
            WriteAlarmList(1012, ALARM_LEVEL_MINOR, 0, "TTSService Connected");
          }
          else
          {
            TTSClientId = ClientId;
            TTSLogId = false;
            MyTrace(0, "TTSProxy[%d] Logout", NodeId);
            WriteAlarmList(1011, ALARM_LEVEL_MAJOR, 1, "TTSService DisConnected");
          }
        }
        break;
      case NODE_IPREC:
        {
          if (LogInOut == 1)
          {
            if (NodeId < pVopGroup->m_nVopNum)
            {
              pVopGroup->m_pVops[NodeId].m_usClientId = ClientId;
              pVopGroup->m_pVops[NodeId].m_nLoginState = 1;
              pVopGroup->FreeVopChn(NodeId);
              SendVopStateToAll(NodeId);
            }
            MyTrace(0, "IPREC[%d] Login", NodeId);
            WriteAlarmList(1020, ALARM_LEVEL_MINOR, 1, "IPREC[%d] Connected", NodeId);
          }
          else
          {
            if (NodeId < pVopGroup->m_nVopNum)
            {
              pVopGroup->m_pVops[NodeId].m_usClientId = 0;
              pVopGroup->m_pVops[NodeId].m_nLoginState = 2;
              pVopGroup->BlockVopChn(NodeId);
              SendVopStateToAll(NodeId);
              Send_SWTMSG_sendivrstatus(NodeId, 1);
            }
            MyTrace(0, "IPREC[%d] Logout", NodeId);
            WriteAlarmList(1019, ALARM_LEVEL_MAJOR, 1, "IPREC[%d] DisConnected", NodeId);
          }
        }
        break;
      case NODE_REC:
        {
          if (LogInOut == 1)
          {
            RECSystemClientId = ClientId;
            RECSystemLogId = true;
            MyTrace(0, "RecordSystem[%d] Login", NodeId);
            
            Send_ACDState_To_REC(pACDQueue->AcdCount, pACDQueue->WaitAcdCount, pACDQueue->WaitAnsCount, pACDQueue->CallerList.C_Str());
            Send_IVRState_To_REC(pBoxchn->IvrChnIdelCount, pBoxchn->IvrChnBusyCount, pBoxchn->IvrChnBlockCount);
            Send_AGState_To_REC(pAgentMng->AgLoginCount, pAgentMng->AgLogoutCount, pAgentMng->AgIdelCount, pAgentMng->AgBusyCount, pAgentMng->AgTalkCount, pAgentMng->AgLeavelCount);
            
            if (pParser->VxmlLogId[1] == 1)
            {
              sprintf(SendMsgBuf, "<onnodestate sessionid='%ld' nodetype='%d' nodeno='%d' state='%d'/>", 
                0x02010000, 15, 1, 1);
              SendMsg2XML(MSG_onnodestate, 0x02010000, SendMsgBuf);
            }
          }
          else
          {
            RECSystemClientId = 0;
            RECSystemLogId = false;
            
            if (g_nSwitchMode == PORT_NOCTI_PBX_TYPE)
            {
              ResetAllChnForParserLogout(1);
              LogoutAllAG();
            }
            
            if (pParser->VxmlLogId[1] == 1)
            {
              sprintf(SendMsgBuf, "<onnodestate sessionid='%ld' nodetype='%d' nodeno='%d' state='%d'/>", 
                0x02010000, 15, 1, 0);
              SendMsg2XML(MSG_onnodestate, 0x02010000, SendMsgBuf);
            }
            
            MyTrace(0, "Record System[%d] Logout", NodeId);
          }
        }
        break;
      case NODE_WS:
        {
          if (NodeId != 1)
            break;
          if (LogInOut == 1)
          {
            g_dwRegServerState = g_dwRegServerState & 0xBF;
            WriteRegServiceRunState();
            WebSocketClientId = ClientId;
            WebSocketLogId = true;
            MyTrace(0, "WebSocket[%d] Login", NodeId);
            WriteAlarmList(1014, ALARM_LEVEL_MINOR, 0, "WebSocket Connected");
          }
          else
          {
            g_dwRegServerState = g_dwRegServerState | 0x40;
            WriteRegServiceRunState();
            WebSocketClientId = 0;
            WebSocketLogId = false;
            MyTrace(0, "WebSocket[%d] Logout", NodeId);
            WriteAlarmList(1013, ALARM_LEVEL_MAJOR, 1, "WebSocket DisConnected");
          }
        }
        break;
      case NODE_COM:
        {
          if (LogInOut == 1)
          {
            COMServiceClientId = ClientId;
            COMServiceLogId = true;
            MyTrace(0, "COM Client[%d] Login", NodeId);
            WriteAlarmList(1016, ALARM_LEVEL_MINOR, 0, "COMService Connected");
          }
          else
          {
            COMServiceClientId = 0;
            COMServiceLogId = false;
            MyTrace(0, "COM Client[%d] Logout", NodeId);
            WriteAlarmList(1015, ALARM_LEVEL_MAJOR, 1, "COMService DisConnected");
          }
        }
        break;
      }
    }
    else
    {
      break;
    }
  }
}
//檢查接收消息隊列
void CheckMsgFifo()
{
	US count = 0, ClientId, MsgNo, MsgId, MsgLen;
  CH MsgBuf[4096];
  VXML_CMD_ATTRIBUTE_STRUCT AttrData;

	while(count++ < 256) //一次最多處理128條信息
	{
    CheckLogInOutFifo();
    memset(MsgBuf, 0, 4096);
		if (RMsgFifo->Read(ClientId, MsgNo, MsgBuf, MsgLen) != 0)
    {
      MsgId = MsgNo & 0xFF;
      switch (MsgNo>>8)
      {
      case MSGTYPE_HAIVR:
        {
          ProcHAMsg(ClientId, MsgNo, MsgBuf);
        }
        break;
      case MSGTYPE_IVRXML: //流程解析器parser消息
        if (MsgId < MAX_XMLIVRMSG_NUM)
        {
          if (RMsgFifo->ParseXMLstr(pFlwRule->XMLIVRMsgRule, MsgId, MsgBuf, &AttrData) == 0)
          {
            ProcXMLMsg(&AttrData);
          }
        }
        break;
      case MSGTYPE_DBXML:
        if (MsgNo == 0x1013)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcHTTPMsg(RecvDBMsg.GetAttrValue(2).C_Str());
          }
        }
        else if (MsgId == MSG_onconnectdbfail)
        {
          //g_bDBConnDown = true;
          SetDBConnDownFlag(); //2016-10-05
          WriteAlarmList(1003, ALARM_LEVEL_MAJOR, 1, "Database DisConnected");
        }
        else if (MsgId == MSG_onconnectdbsucc)
        {
          //g_bDBConnDown = false;
          ClearDBConnDownFlag(); //2016-10-05
          WriteAlarmList(1004, ALARM_LEVEL_MINOR, 0, "Database Connected");
        }
        else if (MsgId == MSG_onimcallin)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcIMCallInMsg(RecvDBMsg);
          }
        }
        else if (MsgId == MSG_onimcancel)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcIMCancelMsg(RecvDBMsg);
          }
        }
        else if (MsgId == MSG_onemailcallin)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcEMCallInMsg(RecvDBMsg);
          }
        }
        else if (MsgId == MSG_onemcancel)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcEMCancelMsg(RecvDBMsg);
          }
        }
        else if (MsgId == MSG_onfaxcallin)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcFAXCallInMsg(RecvDBMsg);
          }
        }
        else if (MsgId == MSG_onfaxcancel)
        {
          RecvDBMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
          RecvDBMsg.SetClientId(ClientId);
          if(0==RecvDBMsg.ParseRevMsg(pFlwRule->DBXMLMsgRule[MsgId]))
          {
            ProcFAXCancelMsg(RecvDBMsg);
          }
        }
        break;
      case MSGTYPE_IVRVOP: //外置語音機消息
        if (MsgId < MAX_XMLIVRMSG_NUM)
        {
          IVRRcvMsg.SetMsgBufId(MsgNo&0xFF,(char *)MsgBuf);
          IVRRcvMsg.SetClientId(ClientId);
          if (Proc_Msg_From_VOP(IVRRcvMsg) == 0)
          {
            ProcVOPMsg(IVRRcvMsg);
          }
        }
        break;
      case MSGTYPE_AGIVR: //電腦坐席消息
        if (MsgId < MAX_AGIVRMSG_NUM)
        {
          IVRRcvMsg.SetMsgBufId(MsgNo&0xFF,(char *)MsgBuf);
          IVRRcvMsg.SetClientId(ClientId);
          if (Proc_Msg_From_AG(IVRRcvMsg) == 0)
          {
            ProcAGMsg(IVRRcvMsg);
          }
        }
        break;
      case MSGTYPE_IVRTTS:
        if (MsgId < MAX_TTSIVRMSG_NUM)
        {
          ProcTTSMsg(MsgNo, (unsigned char *)MsgBuf);
        }
        break;
      case MSGTYPE_IVRLOG:
        if (MsgId < MAX_LOGIVRMSG_NUM)
        {
          LogRcvMsg.SetMsgBufId(MsgNo&0xFF,(char *)MsgBuf);
          LogRcvMsg.SetClientId(ClientId);
          if (Proc_Msg_From_LOG(LogRcvMsg) == 0)
          {
            ProcLOGMsg(LogRcvMsg);
          }
        }
        break;
      case MSGTYPE_IVRSWT:
        if (MsgId < MAX_SWTIVRMSG_NUM)
        {
          SWTRcvMsg.SetMsgBufId(MsgNo&0xFF,(char *)MsgBuf);
          SWTRcvMsg.SetClientId(ClientId);
          if (Proc_Msg_From_SWT(SWTRcvMsg) == 0)
          {
            ProcSWTMsg(SWTRcvMsg);
          }
        }
        break;
      case MSGTYPE_IVRREC:
        Proc_Msg_From_REC(MsgNo&0xFF,(char *)MsgBuf);
        break;
      case MSGTYPE_IVRIPREC:
        Proc_Msg_From_IPREC(MsgNo&0xFF,(char *)MsgBuf);
        break;
      case MSGTYPE_IVRWEB:
        if (g_bQueryLocaMobileNo == false)
          ProcWEBSOCKETMsg((char *)MsgBuf);
        else
          g_WebServer.Write((char *)MsgBuf, MsgLen);
        break;
      case MSGTYPE_IVRCOM:
        Proc_Msg_From_COM((char *)MsgBuf);
        break;
      }
    }
    else
    {
      break;
    }
	}

  if (g_WebServerPort > 0 || g_bQueryLocaMobileNo == true)
  {
    count = 0;
    do 
    {
      if (g_bQueryLocaMobileNo == false)
      {
        if (g_WebServer.Read(MsgBuf, MsgLen) != 0)
        {
          MyTrace(6, "%s", MsgBuf);
          ProcHTTPMsg(MsgBuf);
        }
        else
        {
          break;
        }
      }
      else
      {
        if (g_HTTPfifo.Read(MsgBuf, MsgLen) != 0)
        {
          MyTrace(6, "%s", MsgBuf);
          ProcHTTPMsg(MsgBuf);
        }
        else
        {
          break;
        }
      }
    } while (count++ < 64);
  }
}		
//--------------------座席通信事件---------------------------------------------------------
void OnIVRLogin_AG(unsigned short serverid,unsigned short clientid)     //tcllinkc.dll callback function
{
  if (clientid & 0x8000)
  {
    pLogInOutFifo->Write(serverid, clientid, NODE_CSC, clientid&0x7FFF, 1);
  }
  else if (clientid & 0x0600)
  {
    pLogInOutFifo->Write(serverid, clientid, NODE_CSC, clientid&0x00FF, 1);
  }
}
void OnIVRReceiveData_AG(unsigned short remoteid,unsigned short msgtype,
                         const unsigned char *buf,
                         unsigned long len) //tcllinkc.dll callback function
{
 	UC MsgType=msgtype>>8;
  ((unsigned char *)buf)[len] = 0;
  
  if(MsgType == MSGTYPE_AGIVR)
  {
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
    {
      MyTrace(0, "Recv AGMsg Fifo is overflow");
    }
  }
  else
    MyTrace(0, "OnAGReceiveData other msgtype");
}
void OnIVRClose_AG(unsigned short serverid,unsigned short clientid)    //tcllinkc.dll callback function
{
  if (clientid & 0x8000)
  {
    pLogInOutFifo->Write(serverid, clientid, NODE_CSC, clientid&0x7FFF, 0);
  }
  else if (clientid & 0x0600)
  {
    pLogInOutFifo->Write(serverid, clientid, NODE_CSC, clientid&0x00FF, 0);
  }
}

//---------------本機為主機--------------------------------------------------------------
//備機消息(當本機為主機時)
void OnIVRReceiveData_A(unsigned short remoteid,unsigned short msgtype,
                        const unsigned char *buf,
                        unsigned long len) //need define in main.cpp
{
  if (((remoteid >> 24) & 0xFF) != NODE_IVR)
  {
    return;
  }
  
  ((unsigned char *)buf)[len] = 0;
  if (msgtype == 0x0900 || msgtype == 0x0901 || msgtype == 0x0902)
    MyTrace(1, "0x%08x %s", remoteid, (char *)buf);
  if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
  {
  		MyTrace(0, "Recv HAMsg Fifo is overflow");
      WriteAlarmList(1024, ALARM_LEVEL_MAJOR, 1, "Receive message buffer overflow");
  }
}

void OnIVRLogin_A(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  //本機是主機，與備機網絡連接成功
  if (((clientid >> 24) & 0xFF) != NODE_IVR)
  {
    return;
  }
  SetCTIHALinkFlag(serverid, clientid);
  ClearCTIHAServiceDownFlag();
  pLogInOutFifo->Write(serverid, clientid, (UC)(( clientid >> 24 ) & 0xFF), clientid&0x00FF, 1);
  MyTrace(0, "Connect StandbyCTIServer success serverid=0x%08x clientid=0x%08x", serverid, clientid);
}

void OnIVRClose_A(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  //本機是主機，與備機網絡連接斷開
  if (((clientid >> 24) & 0xFF) != NODE_IVR)
  {
    return;
  }
  SetCTIHALinkFlag(serverid, 0);
  SetCTIHAServiceDownFlag();
  MyTrace(0, "DisConnect StandbyCTIServer!!!");
}
//---------------本機為備用機--------------------------------------------------------------
//主機發來的消息(當本機為備用機時)
void OnIVRReceiveData_B(unsigned short remoteid,unsigned short msgtype,
                        const unsigned char *buf,
                        unsigned long len) //need define in main.cpp
{
  if (((remoteid >> 24) & 0xFF) != NODE_IVR)
  {
    return;
  }
  
  ((unsigned char *)buf)[len] = 0;
  if (msgtype == 0x0900 || msgtype == 0x0901 || msgtype == 0x0902)
    MyTrace(1, "0x%08x %s", remoteid, (char *)buf);
  if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
  {
  		MyTrace(0, "Recv HAMsg Fifo is overflow");
      WriteAlarmList(1024, ALARM_LEVEL_MAJOR, 1, "Receive message buffer overflow");
  }
}

void OnIVRLogin_B(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  //本機是備用機，與主機網絡連接成功
  if (((serverid >> 24) & 0xFF) != NODE_IVR)
  {
    return;
  }
  SetCTIHALinkFlag(serverid, clientid);
  ClearCTIHAServiceDownFlag();
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 24 ) & 0xFF), serverid&0x00FF, 1);
  MyTrace(0, "Connect MasterCTIServer success serverid=0x%08x clientid=0x%08x", serverid, clientid);
  SendMsgToMasterIVR(g_nMasterStatus, g_nStandbyStatus, "StandbyCTIServer Send CurState");
}

void OnIVRClose_B(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  //本機是備用機，與主機網絡連接斷開
  if (((serverid >> 24) & 0xFF) != NODE_IVR)
  {
    return;
  }
  SetCTIHALinkFlag(serverid, 0);
  SetCTIHAServiceDownFlag();
  MyTrace(0, "DisConnect MasterCTIServer!!!");
}
//-----------------------------------------------------------------------------
void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype,
                      const unsigned char *buf,
                      unsigned long len) //need define in main.cpp
{
  if (msgtype == 0x1013 //MSG_onwebhttprequest
    || msgtype == 0x1017 //MSG_onconnectdbfail
    || msgtype == 0x1018 //MSG_onconnectdbsucc
    )
  {
    ((unsigned char *)buf)[len] = 0;
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv DBMsg Fifo is overflow");
  }
}

void OnDBLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  ClearDBServiceDownFlag();
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 1);
}

void OnDBClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  SetDBServiceDownFlag();
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 0);
}

void OnCRMReceiveData(unsigned short remoteid,unsigned short msgtype,
                     const unsigned char *buf,
                     unsigned long len) //need define in main.cpp
{
}

void OnCRMLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 1);
}

void OnCRMClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 0);
}

//-----------------------------------------------------------------------------
void OnGWReceiveData(unsigned short remoteid,unsigned short msgtype,
                     const unsigned char *buf,
                     unsigned long len) //need define in main.cpp
{
  //((unsigned char *)buf)[len] = 0;
  //MyTrace(4, (char *)buf);
}

void OnGWLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 1);
}

void OnGWClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 0);
}
//-----------------------------------------------------------------------------
void OnIMReceiveData(unsigned short remoteid,unsigned short msgtype,
                     const unsigned char *buf,
                     unsigned long len) //need define in main.cpp
{
  if (msgtype == 0x101A //MSG_onimcallin
    || msgtype == 0x101B //MSG_onemailcallin
    || msgtype == 0x101C //MSG_onfaxcallin
    || msgtype == 0x101D //MSG_onimcancel
    || msgtype == 0x101E //MSG_onemcancel
    || msgtype == 0x101F //MSG_onfaxcancel
    )
  {
    ((unsigned char *)buf)[len] = 0;
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv IMMsg Fifo is overflow");
  }
}

void OnIMLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 1);
}

void OnIMClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  pLogInOutFifo->Write(serverid, clientid, (UC)(( serverid >> 8 ) & 0xFF), serverid&0x00FF, 0);
}

//-------------------IVR通信事件處理-------------------------------------------
void OnIVRLogin(unsigned short serverid,unsigned short clientid)     //tcllinkc.dll callback function
{
    if ((( clientid >> 8 ) & 0xFF) == NODE_SWT)
  {
    ClearSWTServiceDownFlag();
  }
  else if ((( clientid >> 8 ) & 0xFF) == NODE_XML)
  {
    ClearFLWServiceDownFlag();
  }
  else if ((( clientid >> 8 ) & 0xFF) == NODE_WS)
  {
    ClearWSServiceDownFlag();
  }
  pLogInOutFifo->Write(serverid, clientid, (UC)(( clientid >> 8 ) & 0xFF), clientid&0x00FF, 1);
}

void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //tcllinkc.dll callback function
{
 	UC msgid=msgtype>>8;
  ((unsigned char *)buf)[len] = 0;
  
  if(msgid == MSGTYPE_IVRXML || msgid == MSGTYPE_AGIVR || msgid == MSGTYPE_IVRLOG)
  {
    if (msgtype != MSG_getidleseat)
      MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
  	if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
  		MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRSWT)
  {
    if (g_nSwitchMode == 0 || g_nSwitchMode == 10)
    {
      if (msgtype == 0x0228) //SWTMSG_onqueryacdsplit
      {
        MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
        if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
          MyTrace(0, "Recv Msg Fifo is overflow");
      }
      return;
    }
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRVOP)
  {
    if (g_nSwitchMode == 0 && g_nCardType != 9)
      return;
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRIPREC)
  {
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRTTS)
  {
    //MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRREC)
  {
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRWEB && (remoteid&0xff) == 1)
  {
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else if(msgid == MSGTYPE_IVRCOM)
  {
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
    if (RMsgFifo->Write(remoteid, msgtype, (char *)buf, (US)len) == 0)
      MyTrace(0, "Recv Msg Fifo is overflow");
  }
  else
  {
  	MyTrace(0, "OnIVRReceiveData other msgtype");
  }
}

void OnIVRClose(unsigned short serverid,unsigned short clientid)    //tcllinkc.dll callback function
{
  if ((( clientid >> 8 ) & 0xFF) == NODE_SWT)
  {
    SetSWTServiceDownFlag();
  }
  else if ((( clientid >> 8 ) & 0xFF) == NODE_XML)
  {
    SetFLWServiceDownFlag();
  }
  else if ((( clientid >> 8 ) & 0xFF) == NODE_WS)
  {
    SetWSServiceDownFlag();
  }
	pLogInOutFifo->Write(serverid, clientid, (UC)(( clientid >> 8 ) & 0xFF), clientid&0x00FF, 0);
}
//-----------------------------------------------------------------------------
void ReadAuthData()
{
  char temp[256], authdata[256], TempData[256];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

  LANGID seatlangid = GetSystemDefaultLangID();
  if (seatlangid == 0x0804)
  {
    g_LangID = 1; //簡體中文
  } 
  else if (seatlangid == 0x0404 || seatlangid == 0x0c04 || seatlangid == 0x1004)
  {
    g_LangID = 2; //繁體中文
  }
  else
  {
    g_LangID = 3; //美國英文
  }

  sprintf(AuthEndDate, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
  GetPrivateProfileString("AUTHPSW", "SerialNo", "none", temp, 256, g_szServiceINIFileName);

  if (strncmp(temp, "@#$", 3) == 0)
  {
    strcpy(TempData, &temp[3]);
    memset(temp, 0, 256);
    
    int len = strlen(TempData);
    int n = len/2;
    int m = len%2;
    int i;
    for (i=0; i<n; i++)
    {
      temp[i] = TempData[i*2];
      temp[len-i-1] = TempData[i*2+1];
    }
    if (m == 1)
    {
      temp[n] = TempData[len-1];
    }
  }

  //授權碼格式: 硬件類型;中繼通道數;坐席通道數;有效截至日期;傳真通道數;短信通道數;IVR通道數;監錄通道數;

  if (strlen(temp) >= 78)
  {
    strcpy(authdata, &temp[32]);
    
    memset(AuthKey, 0, 256);
    strcpy(AuthKey, DecodePass(authdata));

    //MyTrace(0, "%s -> %s", authdata, temp);

    AuthCardType = AuthKey[0]-'0';
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[2], 4);
    AuthMaxTrunk = atoi(authdata);
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[7], 4);
    AuthMaxSeat = atoi(authdata);

    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[23], 4);
    AuthMaxFax = atoi(authdata);
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[28], 4);
    AuthMaxSms = atoi(authdata);
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[33], 4);
    AuthMaxIVR = atoi(authdata);
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[38], 4);
    AuthMaxRECD = atoi(authdata);
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[43], 2);
    AuthSwitchType = atoi(authdata);
    
    memset(authdata, 0, 256);
    strncpy(authdata, &AuthKey[46], 4);
    AuthMaxTTS = atoi(authdata);
    
    strncpy(AuthEndDate, &AuthKey[12], 10);

    MyTrace(0, "InitMsg: LangID=%d ReadAuthData AuthSwitchType=%d AuthCardType=%d AuthMaxTrunk=%d AuthMaxSeat=%d AuthMaxFax=%d AuthMaxSms=%d AuthMaxIVR=%d AuthMaxRECD=%d AuthMaxTTS=%d AuthEndDate=%s", 
      g_LangID, AuthSwitchType, AuthCardType, AuthMaxTrunk, AuthMaxSeat, AuthMaxFax, AuthMaxSms, AuthMaxIVR, AuthMaxRECD, AuthMaxTTS, AuthEndDate);
  }
  else
  {
    MyTrace(0, "InitMsg: LangID=%d Read AuthDate error", 
      g_LangID);
  }
}
//-----------------------------------------------------------------------------
//讀總的服務的配置文件（返回0表示成功）
int ReadSERVICEIniFile()
{
  //取程序所在路徑
  char szTemp[256];
  TCHAR szFull[_MAX_PATH];
  TCHAR szDrive[_MAX_DRIVE];
  TCHAR szDir[_MAX_DIR];
  GetModuleFileName(NULL, szFull, sizeof(szFull)/sizeof(TCHAR));
  _tsplitpath(szFull, szDrive, szDir, NULL, NULL);
  _tcscpy(szFull, szDrive);
  _tcscat(szFull, szDir);
  strcpy(g_szAppPath, szFull);

  memset(g_szRootPath, 0, MAX_PATH_FILE_LEN);
  strncpy(g_szRootPath, g_szAppPath, strlen(g_szAppPath)-12);
  
  sprintf(g_szLogPath, "%sAuthSerialNo.ini", g_szAppPath);
  DeleteFiles((CString)g_szLogPath);
  
  //IVR配置文件名
  sprintf(g_szUnimeINIFileName, "%s\\unimeconfig.ini", g_szRootPath);
  sprintf(g_szServiceINIFileName, "%sIVRSERVICE.ini", g_szAppPath);
  sprintf(g_szDBINIFileName, "%s\\DBSERVICE\\DBSERVICE.ini", g_szRootPath);
  sprintf(g_szCTILINKINIFileName, "%s\\CTILINKSERVICE\\CTILINKSERVICE.ini", g_szRootPath);

  GetPrivateProfileString("CONFIGURE","DBTYPE","MYSQL",g_szDBType,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","DBSERVER","127.0.0.1",g_szDBServer,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","DATABASE","callcenterdb",g_szDBName,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","USERID","root",g_szDBUser,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","PASSWORD","",szTemp,63,g_szDBINIFileName);
  strcpy(g_szDBPwd, DecodePass(szTemp));
  //strcpy(g_szDBPwd, szTemp);
  if (stricmp(g_szDBType, "SQLSERVER") == 0) //sql server
  {
    sprintf(g_szConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s", 
      g_szDBPwd, g_szDBUser, g_szDBName, g_szDBServer);
  }
  else if (stricmp(g_szDBType, "ORACLE") == 0) //oracle
  {
    sprintf(g_szConnectionString, "Provider=OraOLEDB.Oracle.1;Password=%s;Persist Security Info=True;User ID=%s;Data Source=%s", 
      g_szDBPwd, g_szDBUser, g_szDBServer );
  }
  else if (stricmp(g_szDBType, "SYBASE") == 0) //sybase
  {
    sprintf(g_szConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s", 
      g_szDBPwd, g_szDBUser, g_szDBName, g_szDBServer);
  }
  else if (stricmp(g_szDBType, "ACCESS") == 0) //access
  {
    sprintf(g_szConnectionString, "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%s", g_szDBName);
  }
  else if (stricmp(g_szDBType, "MYSQL") == 0) //mysql
  {
    sprintf(g_szConnectionString, "Driver={MySQL ODBC 5.1 Driver};server=%s;uid=%s;pwd=%s;database=%s;", 
      g_szDBServer, g_szDBUser, g_szDBPwd, g_szDBName);
  }
  else if (stricmp(g_szDBType, "ODBC") == 0) //odbc
  {
    sprintf(g_szConnectionString, "Data Source=%s;UID=%s;PWD=%s;", g_szDBName, g_szDBUser, g_szDBPwd);
  }
  else
  {
    sprintf(g_szConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s", 
      g_szDBPwd, g_szDBUser, g_szDBName, g_szDBServer);
  }

  if (MyCheckFileExist(g_szServiceINIFileName) == 0)
  {
    g_nINIFileType = 1;
  }
  else
  {
    g_nINIFileType = GetPrivateProfileInt("QUARKCALL","INIType", 0, g_szServiceINIFileName);
  }
  if (g_nINIFileType == 1)
  {
    sprintf(g_szServiceINIFileName, g_szUnimeINIFileName);
  }

  GetPrivateProfileString("LOG", "DBPath", "", szTemp, 127, g_szServiceINIFileName);
  if (strlen(szTemp)==0)
    sprintf(szTemp, "%s\\DB", g_szRootPath);
  sprintf(g_szAlarmSqliteDB, "%s\\ctialarm.db", szTemp);

  strcpy(g_szSwitchTime, MyGetNow());
  g_nMultiRunMode = GetPrivateProfileInt("CONFIGURE", "MultiRunMode", 0, g_szServiceINIFileName);
  g_nLanDownHAMode = GetPrivateProfileInt("CONFIGURE", "LanDownHAMode", 1, g_szServiceINIFileName);
  g_nLanDownCheckTimeLen = GetPrivateProfileInt("CONFIGURE", "LanDownCheckTimeLen", 60, g_szServiceINIFileName);
  g_nCTILINKDownSwitchOverId = GetPrivateProfileInt("CONFIGURE", "CTILINKDownSwitchOverId", 1, g_szServiceINIFileName);
  g_nDBDownSwitchOverId = GetPrivateProfileInt("CONFIGURE", "DBDownSwitchOverId", 1, g_szServiceINIFileName);
  if (g_nMultiRunMode == 0)
  {
    g_nMasterStatus = 1;
    g_nHostType = 0;
  }
  else
  {
    g_nHostType = GetPrivateProfileInt("CONFIGURE", "HostType", 0, g_szServiceINIFileName);
    g_nMasterStatus = GetPrivateProfileInt("CONFIGURE", "MasterInitStatus", 1, g_szServiceINIFileName);
  }
  
  g_nSwitchMode = GetPrivateProfileInt("CONFIGURE", "SwitchMode", 0, g_szServiceINIFileName);
  g_nSwitchType = g_nSwitchMode;
  
  g_IVRServerId = GetPrivateProfileInt("IVRSERVER", "IVRServerId", 1, g_szServiceINIFileName);
  if (g_nHostType == 0)
  {
    g_MasterIVRServerId = g_IVRServerId;
  }
  else
  {
    g_MasterIVRServerId = GetPrivateProfileInt("IVRSERVER", "MASTERIVRServerid", 1, g_szServiceINIFileName);
  }
  GetPrivateProfileString("CONFIGURE", "LocaIVRIP", "127.0.0.1", g_szLocaIVRIP, 32, g_szServiceINIFileName);
  g_nWriteRegRunDog = GetPrivateProfileInt("CONFIGURE", "WriteRegRunDog", 0, g_szServiceINIFileName);
  
  if (g_nINIFileType == 0)
  {
    GetPrivateProfileString("CONFIGURE", "DBTYPE", "MYSQL", g_szDBType, 32, g_szDBINIFileName);
    strcpy(szTemp, "LOG");
  }
  else
  {
    GetPrivateProfileString("DATABASE", "DBTYPE", "MYSQL", g_szDBType, 32, g_szUnimeINIFileName);
    strcpy(szTemp, "IVRLOG");
  }
  
  if (strcmpi(g_szDBType, "SQLSERVER") == 0)
  {
    g_nDBType = DB_SQLSERVER;
    strcpy(g_szSQLNowFunc, "GETDATE()");
  }
  else if (strcmpi(g_szDBType, "ORACLE") == 0)
  {
    g_nDBType = DB_ORACLE;
    strcpy(g_szSQLNowFunc, "sysdate");
  }
  else if (strcmpi(g_szDBType, "SYBASE") == 0)
  {
    g_nDBType = DB_SYBASE;
    strcpy(g_szSQLNowFunc, "GETDATE()");
  }
  else if (strcmpi(g_szDBType, "MYSQL") == 0)
  {
    g_nDBType = DB_MYSQL;
    strcpy(g_szSQLNowFunc, "NOW()");
  }
  else if (strcmpi(g_szDBType, "ACCESS") == 0)
  {
    g_nDBType = DB_ACCESS;
    strcpy(g_szSQLNowFunc, "GETDATE()");
  }
  else if (strcmpi(g_szDBType, "ODBC") == 0)
  {
    g_nDBType = DB_ODBC;
    strcpy(g_szSQLNowFunc, "GETDATE()");
  }
  else
  {
    g_nDBType = DB_NONE;
    memset(g_szSQLNowFunc, 0, 16);
  }

  if (MyCheckFileExist(g_szServiceINIFileName) == 0)
  {
    return 1;
  }

  WritePrivateProfileString("CONFIGURE", "OS", "WIN32", g_szServiceINIFileName);
  
  GetPrivateProfileString(szTemp, "LogPath", "", g_szLogPath, 128, g_szServiceINIFileName);
  if (strlen(g_szLogPath)==0)
    sprintf(g_szLogPath, "%smsg", g_szAppPath);
  CreateAllDirectories((CString)g_szLogPath);
  
  if (GetPrivateProfileInt("CONFIGURE","QueryLocaMobileNo", 0, g_szServiceINIFileName) == 1)
    g_bQueryLocaMobileNo = true;
  g_nURIEncodeDecode = GetPrivateProfileInt("CONFIGURE","URIEncodeDecode", 0, g_szServiceINIFileName);
  g_nParserAccessCodeType = GetPrivateProfileInt("CONFIGURE","ParserAccessCodeType", 1, g_szServiceINIFileName); //2016-03-31

  g_WebServerPort = GetPrivateProfileInt("IVRSERVER", "WebServerPort", 0, g_szServiceINIFileName);
  g_WebServerTimeOut = GetPrivateProfileInt("IVRSERVER", "WebServerTimeOut", 15, g_szServiceINIFileName);
  g_WebServer.SetLogFilePath(g_szAppPath);
  
  MyTrace(0, "-----------------------------------------------------------------");
  MyTrace(0, "InitMsg: Start InitRun");
  
  sprintf(g_szCountPath, "%scount", g_szAppPath);
  CreateAllDirectories((CString)g_szCountPath);
  
  //讀是否保存日志標志
  if (GetPrivateProfileInt(szTemp,"SaveId", 0, g_szServiceINIFileName) == 1)
  {
    g_bSaveId = true;
    g_bLogSpecId = GetPrivateProfileInt(szTemp,"SpecId", 0, g_szServiceINIFileName) == 1 ? true : false;
  }
  if (GetPrivateProfileInt(szTemp,"CommId", 0, g_szServiceINIFileName) == 1)
  {
    g_bCommId = true;
  }
  if (GetPrivateProfileInt(szTemp,"DebugId", 0, g_szServiceINIFileName) == 1)
  {
    g_bDebugId = true;
  }
  if (GetPrivateProfileInt(szTemp,"DrvId", 0, g_szServiceINIFileName) == 1)
  {
    g_bDrvId = true;
  }
  if (GetPrivateProfileInt(szTemp,"SQLId", 0, g_szServiceINIFileName) == 1)
  {
    g_bSQLId = true;
  }
  if (GetPrivateProfileInt(szTemp,"AlartId", 0, g_szServiceINIFileName) == 1)
  {
    g_bAlartId = true;
  }
  (g_bCommId==true || g_bDebugId==true || g_bSQLId==true) ? g_bSaveId = true : g_bSaveId = false;

  //內存索引配置文件名
  sprintf(g_szMemVocINIFileName, "%smemindex.ini", g_szAppPath);
  if (MyCheckFileExist(g_szMemVocINIFileName) == 0)
  {
  }

  WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "NONE", g_szServiceINIFileName);
  ReadAuthData(); //讀授權參數

  /*0-板卡模式 
    1-AVAYA-IPO系列收費CTI
    2-AVAYA-S系列
    3-Alcatel-OXO系列
    4-Alcatel-OXE系列
    5-simens系列
    6-Panasonic系列
    7-NEC系列
    8-HUAWEI
    9-START-NET
    10-普通PBX(無CTI接口)
    99-演示平臺
  */

  g_nSwitchMode = GetPrivateProfileInt("CONFIGURE", "SwitchMode", 0, g_szServiceINIFileName);
  g_nSwitchType = g_nSwitchMode;
  g_VOCInLinux = GetPrivateProfileInt("CONFIGURE", "VOCInLinux", 0, g_szServiceINIFileName); //2013-09-30

  int i;
  //讀webservice接口參數
  GetPrivateProfileString("WEBSERVICE", "wsdladdress", "", g_WebServiceParam.WS_wsdladdress, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICE", "soapaction", "", g_WebServiceParam.WS_soapaction, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICE", "startenvelope", "", g_WebServiceParam.WS_startenvelope, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICE", "startbody", "", g_WebServiceParam.WS_startbody, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICE", "interface", "", g_WebServiceParam.WS_interface, 255, g_szServiceINIFileName);
  g_WebServiceParam.WS_paramcount = GetPrivateProfileInt("WEBSERVICE", "paramcount", 0, g_szServiceINIFileName);
  for (i=0; i<g_WebServiceParam.WS_paramcount; i++)
  {
    sprintf(szTemp, "paramstring%d", i+1);
    GetPrivateProfileString("WEBSERVICE", szTemp, "", g_WebServiceParam.WS_paramstring[i], 255, g_szServiceINIFileName);
  }

  int nIVRNONum;
  char szIVRNO[32];
  short nDeptNo;
  nIVRNONum = GetPrivateProfileInt("IVRNODEPTNO", "IVRNONUM", 0, g_szServiceINIFileName);
  for (i=0; i<nIVRNONum; i++)
  {
    sprintf(szTemp, "IVRNO[%d]", i);
    GetPrivateProfileString("IVRNODEPTNO", szTemp, "", szIVRNO, 31, g_szServiceINIFileName);
    sprintf(szTemp, "DEPTNO[%d]", i);
    nDeptNo = GetPrivateProfileInt("IVRNODEPTNO", szTemp, 0, g_szServiceINIFileName);
    gIVRDeptMng.SetIVRDept(szIVRNO, nDeptNo);
  }

  //2015-10-22 針對不同IVR群組實時占用數的修改
  gIVRCallCountParamMng.GetIvrNoType = GetPrivateProfileInt("IVRGROUP", "GetIVRnoType", 1, g_szServiceINIFileName);
  nIVRNONum = GetPrivateProfileInt("IVRGROUP", "IVRNONUM", 0, g_szServiceINIFileName);
  for (i=0; i<nIVRNONum; i++)
  {
    sprintf(szTemp, "IVRNO[%d]", i);
    GetPrivateProfileString("IVRGROUP", szTemp, "", szIVRNO, 31, g_szServiceINIFileName);
    gIVRCallCountParamMng.GetIVRCallCountParam(szIVRNO);
  }

  g_nDefaultACWTimeLen = GetPrivateProfileInt("CONFIGURE", "DefaultACWTimeLen", 3, g_szServiceINIFileName);

  return 0;
}
//初始化用到的類資源（返回0表示成功）
int InitResourceMem()
{
  //讀IVR配置參數
  pMemvoc = new CMemvoc;

  pIVRCfg = new CIVRCfg;
  if (pIVRCfg == NULL) 
  {
    return 1;
  }
  else
  {
    strcpy(pIVRCfg->AppPath, g_szAppPath);
    strcpy(pIVRCfg->INIFileName, g_szServiceINIFileName);
    pIVRCfg->ReadIniFile();
  }
  //通信消息規則
  pFlwRule = new CFlwRule;
  if (pFlwRule == NULL) 
  {
    return 2;
  }
  //接收消息緩沖初始化	
  RMsgFifo = new CMsgfifo(MAX_MSG_QUEUE_NUM);
  if (RMsgFifo == NULL) 
  {
    return 3;
  }
  //流程解析器接入號
  pParser = new CParser;
  if (pParser == NULL) 
  {
    return 4;
  }
  else
  {
    pParser->Init();
  }
  //通道初始化	
  pBoxchn = new CBoxchn;
  if (pBoxchn == NULL) 
  {
    return 5;
  }

  //會議初始化	
  pBoxconf = new CBoxconf;
  if (pBoxconf == NULL) 
  {
    return 6;
  }
  //呼出緩沖初始化
  int MaxOutNum;
  pCallbuf = new CCallout;
  if (pCallbuf == NULL) 
  {
    return 7;
  }
  else
  {
    MaxOutNum = GetPrivateProfileInt("ROUTE","MaxOutBuf", 128, g_szServiceINIFileName);
    if (MaxOutNum < 128)
      MaxOutNum = 128;
    pCallbuf->Init(MaxOutNum);
  }
  //呼出隊列初始化
  int MaxQueNum;
  pCalloutQue = new CCalloutQue;
  if (pCalloutQue == NULL) 
  {
    return 8;
  }
  else
  {
    MaxQueNum = GetPrivateProfileInt("ROUTE","MaxQueNum", 128, g_szServiceINIFileName);
    if (MaxQueNum < 128)
      MaxQueNum = 128;
    pCalloutQue->Init(MaxQueNum);
  }
  //坐席類初始化
  pAgentMng = new CAgentMng;
  if (pAgentMng == NULL) 
  {
    return 9;
  }

  //ACD隊列初始化
  int MaxAcdNum;
  pACDQueue = new CACDQueue;
  if (pACDQueue == NULL) 
  {
    return 10;
  }
  else
  {
    MaxAcdNum = GetPrivateProfileInt("ROUTE","MaxAcdBuf", 128, g_szServiceINIFileName);
    if (MaxAcdNum < 64)
      MaxAcdNum = 64;
    pACDQueue->Init(MaxAcdNum);
  }
  
  pLogInOutFifo = new CLogInOutFifo(512);
  if (pLogInOutFifo == NULL) 
  {
    return 11;
  }

  pSQLSfifo = new CSQLSfifo(256);
  if (pSQLSfifo == NULL) 
  {
    return 12;
  }
  
  return 0;
}
//初始化語音卡驅動DLL（返回0表示成功）
int InitVoiceCard()
{
  //初始化語音卡,返回值：0-硬件初始化成功并通過授權驗證 1-硬件初始化成功但未通過授權驗證 2-未找到三匯卡的配置文件 3-初始化失敗，無可以通道 4-硬件初始化失敗 5-初始化pCard指針錯誤 9-重復初始化
  short InitId = MAININIT(0); //初始化DLL
  short USBType = GetPrivateProfileInt("BOARDMODEL","USB", 0, g_szServiceINIFileName);
  
  if (USBType == 0)
  {
    MyTrace(0, "MAININIT InitId=%d", InitId);
    if(InitId > 1)
    {
      MyTrace(0, "InitMsg: Voice Board Init fail!"); //初始化失敗
      return 1;	
    }
    else
    {
      SetWriteDebugId(g_bDrvId?1:0); //設置是否記錄底層日志
    
      //語音卡內存放音索引初始化	
      pMemvoc->Read_MemPlayIndex(g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());

      Load_MemPlayIndex(g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());
      MyTrace(0, "Load_MemPlayIndex MemVocINIFileName=%s MemIndexPath=%s", g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());

      g_nCardType = GetDeviceId(); //1-杭州三匯
      if(InitId == 1) //未授權時
      {
        AuthMaxTrunk = 2;
        AuthMaxSeat = 2;
        WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
      }
      else
      {
        if (AuthMaxTrunk >= GetTrunkNum())
          AuthMaxTrunk = GetTrunkNum();
        if (AuthMaxSeat >= GetSeatNum())
          AuthMaxSeat = GetSeatNum();
        AuthPassId = true;
        WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "OK", g_szServiceINIFileName);
      }
      //------------初始化通道數據--------------
      int i, MaxChnNum, MaxCfcNum, ChnNum, SeatNum=0;
      int nChn, chstyle, chstyleindex, chtype, chindex, lgchtype, lgchindex;
    
      MaxChnNum = GetChnNum();
      if (pBoxchn->Init(MaxChnNum) == 0)
      {
        for (i = 0; i < MAXCHGROUPNUM; i++)
        {
          ChnNum = GetChnNumByType(i);
          if (ChnNum > 0)
          {
            pBoxchn->InitChnIdx(i, ChnNum);
          }
        }
        pBoxchn->TotalTrkNum = 0;
        for (nChn = 0; nChn < MaxChnNum; nChn++)
        {
          chstyle = GetChStyle(nChn);
          chstyleindex = GetChStyleIndex(nChn);
          chtype = GetChType(nChn);
          chindex = GetChIndex(nChn);
          lgchtype = GetLgChType(nChn);
          lgchindex = GetLgChIndex(nChn);
        
          pBoxchn->SetChData(nChn, chstyle, chstyleindex, chtype, chindex, lgchtype, lgchindex);
          pBoxchn->SetChtypeData(chtype, chindex, nChn);
          if (lgchtype == CHANNEL_TRUNK)
          {
            pBoxchn->SetTrunkCh(lgchindex, nChn);
            if (chstyle == CH_A_EXTN)
            {
              pBoxchn->MaxFxoNum ++;
              strncpy(pChn->DeviceID, pIVRCfg->PSTNCode[chstyleindex], MAX_TELECODE_LEN-1);
              MyTrace(3, "SetChDeviceID nChn=%d DeviceID=%s", nChn, pChn->DeviceID);
            }
            pBoxchn->TotalTrkNum ++;
          }
          else if (lgchtype == CHANNEL_SEAT)
          {
            pBoxchn->SetSeatCh(lgchindex, nChn);
            SeatNum++;
            pBoxchn->TotalSeatNum++; //2017-01-14 實際坐席端口數
          }
          else if (lgchtype == CHANNEL_REC)
          {
            pBoxchn->SetRecCh(lgchindex, nChn);
          }
        }
        char szTemp[16];
        sprintf(szTemp, "%d", pBoxchn->MaxFxoNum);
        WritePrivateProfileString("PSTN", "PSTNNum", szTemp, g_szServiceINIFileName);

        //讀內線端口號碼配置
        pBoxchn->Read_SeatNoIniFile(g_szServiceINIFileName);
        //讀坐席配置參數
        WritePrivateProfileString("SEAT", "SeatNum", "0", g_szServiceINIFileName);
        ReadWorkerGroupName();
        ReadLocaSeatParam(g_szServiceINIFileName, SeatNum);
        ReadDTSeatParam(g_szServiceINIFileName);
        ReadRemoteSeatParam(g_szServiceINIFileName);
        //讀呼出路由數據
        pBoxchn->Read_Router(g_szServiceINIFileName);
        //設置最空閑中繼
        pBoxchn->pIdelTrkChnQueue = new CIdelTrkChnQueue;
        if (pBoxchn->pIdelTrkChnQueue != NULL)
        {
          pBoxchn->pIdelTrkChnQueue->InitQueue(MaxChnNum);
          for (i = 0; i < pBoxchn->TotalTrkNum; i++)
          {
            pBoxchn->pIdelTrkChnQueue->TrkNoJoinInQueueTail(i);
          }
        }
      }
      MyTrace(0, "Get Total ChnNum=%d CardType=%d", MaxChnNum, g_nCardType);
    
      //建立系統會議
      MaxCfcNum = GetConfNum();
      MyTrace(0, "Get Conf ChnNum=%d", MaxCfcNum);
    
      pBoxconf->Init(MaxCfcNum);
      pBoxconf->Read_ConfIniFile(g_szServiceINIFileName);
      pBoxconf->CreateAllSysConf(1);

      MyTrace(0, "AuthCardType=%d AuthMaxTrunk=%d AuthMaxSeat=%d AuthEndDate=%s", 
        AuthCardType, AuthMaxTrunk, AuthMaxSeat, AuthEndDate);
      g_bVoiceCardInit = true;
      g_bChnInit = true;
      return 0;
    }
  }
  else
  {
    if (g_bChnInit == false)
    {
      MyTrace(0, "MAININIT InitId=%d", InitId);
    }
    if(InitId > 1)
    {
      g_nUSBInitId = 2;
      return 0;	
    }
    else
    {
      MyTrace(0, "MAININIT InitId success");
      g_nUSBInitId = 1;
      if (g_bChnInit == true)
      {
        for (int nChn=0; nChn<pBoxchn->MaxChnNum; nChn++)
        {
          pChn->hwState = 0;
          pBoxchn->ResetChn(nChn);
          DispChnStatus(nChn);
        }
        Load_MemPlayIndex(g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());
        return 0;
      }

      SetWriteDebugId(g_bDrvId?1:0); //設置是否記錄底層日志
    
      //語音卡內存放音索引初始化	
      Load_MemPlayIndex(g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());
      MyTrace(0, "Load_MemPlayIndex MemVocINIFileName=%s MemIndexPath=%s", g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());

      g_nCardType = GetDeviceId(); //1-杭州三匯
      if(InitId == 1) //未授權時
      {
        AuthMaxTrunk = 2;
        AuthMaxSeat = 2;
        WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
      }
      else
      {
        if (AuthMaxTrunk >= GetTrunkNum())
          AuthMaxTrunk = GetTrunkNum();
        if (AuthMaxSeat >= GetSeatNum())
          AuthMaxSeat = GetSeatNum();
        AuthPassId = true;
        WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "OK", g_szServiceINIFileName);
      }
      //------------初始化通道數據--------------
      int i, MaxChnNum, MaxCfcNum, ChnNum, SeatNum=0;
      int nChn, chstyle, chstyleindex, chtype, chindex, lgchtype, lgchindex;
    
      MaxChnNum = GetChnNum();
      if (pBoxchn->Init(MaxChnNum) == 0)
      {
        for (i = 0; i < MAXCHGROUPNUM; i++)
        {
          ChnNum = GetChnNumByType(i);
          if (ChnNum > 0)
          {
            pBoxchn->InitChnIdx(i, ChnNum);
          }
        }
        pBoxchn->TotalTrkNum = 0;
        for (nChn = 0; nChn < MaxChnNum; nChn++)
        {
          chstyle = GetChStyle(nChn);
          chstyleindex = GetChStyleIndex(nChn);
          chtype = GetChType(nChn);
          chindex = GetChIndex(nChn);
          lgchtype = GetLgChType(nChn);
          lgchindex = GetLgChIndex(nChn);
        
          pBoxchn->SetChData(nChn, chstyle, chstyleindex, chtype, chindex, lgchtype, lgchindex);
          pBoxchn->SetChtypeData(chtype, chindex, nChn);
          if (lgchtype == CHANNEL_TRUNK)
          {
            pBoxchn->SetTrunkCh(lgchindex, nChn);
            if (chstyle == CH_A_EXTN)
            {
              pBoxchn->MaxFxoNum ++;
              strncpy(pChn->DeviceID, pIVRCfg->PSTNCode[chstyleindex], MAX_TELECODE_LEN-1);
              MyTrace(3, "SetChDeviceID nChn=%d DeviceID=%s", nChn, pChn->DeviceID);
            }
            pBoxchn->TotalTrkNum ++;
          }
          else if (lgchtype == CHANNEL_SEAT)
          {
            pBoxchn->SetSeatCh(lgchindex, nChn);
            SeatNum++;
          }
          else if (lgchtype == CHANNEL_REC)
          {
            pBoxchn->SetRecCh(lgchindex, nChn);
          }
        }
        //讀內線端口號碼配置
        pBoxchn->Read_SeatNoIniFile(g_szServiceINIFileName);
        //讀坐席配置參數
        ReadWorkerGroupName();
        ReadLocaSeatParam(g_szServiceINIFileName, SeatNum);
        ReadDTSeatParam(g_szServiceINIFileName);
        ReadRemoteSeatParam(g_szServiceINIFileName);
        //讀呼出路由數據
        pBoxchn->Read_Router(g_szServiceINIFileName);
        //設置最空閑中繼
        pBoxchn->pIdelTrkChnQueue = new CIdelTrkChnQueue;
        if (pBoxchn->pIdelTrkChnQueue != NULL)
        {
          pBoxchn->pIdelTrkChnQueue->InitQueue(MaxChnNum);
          for (i = 0; i < pBoxchn->TotalTrkNum; i++)
          {
            pBoxchn->pIdelTrkChnQueue->TrkNoJoinInQueueTail(i);
          }
        }
      }
      MyTrace(0, "Get Total ChnNum=%d CardType=%d", MaxChnNum, g_nCardType);
    
      //建立系統會議
      MaxCfcNum = GetConfNum();
      MyTrace(0, "Get Conf ChnNum=%d", MaxCfcNum);
    
      pBoxconf->Init(MaxCfcNum);
      pBoxconf->Read_ConfIniFile(g_szServiceINIFileName);
      pBoxconf->CreateAllSysConf(1);

      MyTrace(0, "AuthCardType=%d AuthMaxTrunk=%d AuthMaxSeat=%d AuthEndDate=%s", 
        AuthCardType, AuthMaxTrunk, AuthMaxSeat, AuthEndDate);
      g_bVoiceCardInit = true;
      g_bChnInit = true;
      return 0;
    }
  }
}
//-----------------------------------------------------------------------------
//初始化語音機
int InitVopResource()
{
  int nIVRCount=0, nRECDCount=0, nFAXCount=0, nVopIndex=0, nVopNum, nVopNo, nVopOpen, nVopChnNum, nVopChnNo, nLinkSwitchMode, nSwitchStartPortID, nPortID, nVopChnType;
  char szIniSection[32], szInikey[32], szDeviceID[MAX_TELECODE_LEN], szExtension[MAX_TELECODE_LEN], AuthMd5Psw[256], md5psw[256], szTemp[256], TempData[256];
  CStringX ArrString[8];
  CString strCPUId = GetCPUID();

  FILE	*Fkey;
  CMyDiskInfo MyDiskInfo;
  MyDiskInfo.GetDiskInfo();

  memset(szTemp, 0, 256);
  if (strlen(MyDiskInfo.szSerialNumber) > 0)
  {
    strcpy(szTemp, MyDiskInfo.szSerialNumber);
  }
  else
  {
    strcpy(szTemp, ReadRegAuthData());
    strcat(szTemp, (LPCSTR)strCPUId);
  }
  strcpy(md5psw, MDString(szTemp));

  GetPrivateProfileString("AUTHPSW", "SerialNo", "none", AuthMd5Psw, 256, g_szServiceINIFileName);

  if (strncmp(AuthMd5Psw, "@#$", 3) == 0)
  {
    strcpy(TempData, &AuthMd5Psw[3]);
    memset(AuthMd5Psw, 0, 256);
    
    int len = strlen(TempData);
    int n = len/2;
    int m = len%2;
    int i;
    for (i=0; i<n; i++)
    {
      AuthMd5Psw[i] = TempData[i*2];
      AuthMd5Psw[len-i-1] = TempData[i*2+1];
    }
    if (m == 1)
    {
      AuthMd5Psw[n] = TempData[len-1];
    }
  }

  nVopNum = GetPrivateProfileInt("VOP", "VopNum", 0, g_szServiceINIFileName);
  if (nVopNum == 0)
  {
    sprintf(szTemp, "%s\\AuthSerialNo.ini", g_szAppPath);
    Fkey = fopen(szTemp, "wt");
    if (Fkey != NULL)
    {
      fprintf(Fkey, "[AUTHPSW]\n");
      fprintf(Fkey, "Time=%s\n", MyGetNow());
      fprintf(Fkey, "BoardSerialNo=0\n");
      fprintf(Fkey, "AuthData=%s\n", md5psw);
      fclose(Fkey);
      Fkey = NULL;
    }

    WritePrivateProfileString("AUTHPSW", "AuthData", md5psw, g_szServiceINIFileName);
    sprintf(szTemp, "authkey%s", md5psw);
    strcpy(md5psw, MDString(szTemp));
    if (strncmp(AuthMd5Psw, md5psw, 32) == 0)
    {
      AuthPassId = true;
      WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "OK", g_szServiceINIFileName);
      MyTrace(0, "InitMsg: InitVopResource VopNum=0!");
    }
    else
    {
      WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
      MyTrace(0, "InitMsg: InitVopResource VopNum=0 AuthKey error!");
    }
    return 0;
  }
  //語音機組初始化	
  pVopGroup = new CVopGroup(nVopNum);
  if (pVopGroup == NULL) 
  {
    return 2;
  }
  for (nVopNo = 0; nVopNo < nVopNum; nVopNo++)
  {
    sprintf(szIniSection, "VOP(%d)", nVopNo);
    nVopOpen = GetPrivateProfileInt(szIniSection, "Open", 0, g_szServiceINIFileName);
    if (nVopOpen == 0)
    {
      continue;
    }
    pVopGroup->m_pVops[nVopNo].m_nVopType = GetPrivateProfileInt(szIniSection, "VopType", 0, g_szServiceINIFileName);
    nVopChnNum = GetPrivateProfileInt(szIniSection, "VopChnNum", 0, g_szServiceINIFileName);
    GetPrivateProfileString(szIniSection, "GroupExtension", "", szExtension, MAX_TELECODE_LEN, g_szServiceINIFileName);
    GetPrivateProfileString(szIniSection, "SwitchIVRhostId", "", pVopGroup->m_pVops[nVopNo].m_szIVRHostId, 63, g_szServiceINIFileName);
    if (nVopChnNum == 0)
    {
      //為設置語音通道數量
      continue;
    }
    pVopGroup->m_pVops[nVopNo].InitData(nVopChnNum);
    pVopGroup->m_pVops[nVopNo].SetGroupExtension(szExtension);
    pVopGroup->m_pVops[nVopNo].OpenVop();
    if (nVopNo == 0)
    {
      //初始化語音卡驅動DLL
      SetAllCallBackFuncForVOP(); //設置回調函數
      short InitId = MAININIT(0); //初始化DLL
      
      g_nCardType = GetDeviceId();
      MyTrace(0, "InitMsg: MAININIT InitId=%d", InitId);
      if(InitId == 0 || InitId == 1)
      {
        //設置是否記錄底層日志
        SetWriteDebugId(g_bDrvId?1:0); 
        //語音卡內存放音索引初始化	
        Load_MemPlayIndex(g_szMemVocINIFileName, pIVRCfg->MemIndexPath.C_Str());
        g_bVoiceCardInit = true;
        if (InitId == 0)
        {
          AuthPassId = true;
          WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "OK", g_szServiceINIFileName);
        }
        else
        {
          WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
        }
        StopVoiceCard();
      }
      else
      {
        return 1;
      }
      
      pVopGroup->m_pVops[0].SetLoginState(1);
    }
    nLinkSwitchMode = GetPrivateProfileInt(szIniSection, "LinkSwitchMode", 1, g_szServiceINIFileName);
    if (nLinkSwitchMode == 0)
    {
      //按設置的SwitchStartPortID起始端口號順序排列線序連接
      nSwitchStartPortID = GetPrivateProfileInt(szIniSection, "SwitchStartPortID", 0, g_szServiceINIFileName);
      for (nVopChnNo = 0; nVopChnNo < nVopChnNum; nVopChnNo++)
      {
        pVopGroup->m_pVops[nVopNo].SetSwtData(nVopChnNo, nVopNo, nSwitchStartPortID);
        sprintf(szInikey, "VopChn[%d].Type", nVopChnNo);
        nVopChnType = GetPrivateProfileInt(szIniSection, szInikey, 0, g_szServiceINIFileName);
        
        //對應的語音資源類型：0-IVR/FAX 1-IVR 2-FAX 3-模擬話機坐席監錄 4-數字話機坐席監錄 5-VOIP坐席監錄 6-模擬中繼監錄 7-數字中繼監錄
        if (nVopChnType == 0 || nVopChnType == 1)
        {
          nIVRCount++;
          if (nIVRCount > AuthMaxIVR)
          {
            MyTrace(0, "InitMsg: InitVopResource fail IVR AuthKey error");
            //return 3;
            AuthPassId = false;
            WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
          }
        }
        else if (nVopChnType == 2)
        {
          nFAXCount++;
          if (nFAXCount > AuthMaxFax)
          {
            MyTrace(0, "InitMsg: InitVopResource fail FAX AuthKey error");
            //return 4;
            AuthPassId = false;
            WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
          }
        }
        else
        {
          nRECDCount++;
          if (nRECDCount > AuthMaxRECD)
          {
            MyTrace(0, "InitMsg: InitVopResource fail RECD AuthKey error");
            //return 5;
            AuthPassId = false;
            WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
          }
        }
        
        pVopGroup->m_pVops[nVopNo].SetVopChnType(nVopChnNo, nVopChnType);
        pVopGroup->m_pVops[nVopNo].SetCardData(nVopChnNo, CH_A_EXTN, nVopChnNo);
        pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nIndex = nVopIndex;
        nVopIndex ++;
        nSwitchStartPortID++;
        MyTrace(3, "Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d LnState=%d",
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nIndex,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopNo,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopChnNo,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopChnType,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nChIndex,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSwtPortID,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState);
      }
    }
    else
    {
      //按制定的對應關系連接
      for (nVopChnNo = 0; nVopChnNo < nVopChnNum; nVopChnNo++)
      {
        //連接的交換機的邏輯通道號
        
        memset(szDeviceID, 0, MAX_TELECODE_LEN);
        if (nLinkSwitchMode == 1)
        {
          //按交換機的邏輯端口號設置
          sprintf(szInikey, "VopChn[%d].PortID", nVopChnNo);
          nPortID = GetPrivateProfileInt(szIniSection, szInikey, -1, g_szServiceINIFileName);
          //對應的語音資源類型：0-IVR/FAX 1-IVR 2-FAX 3-模擬話機坐席監錄 4-數字話機坐席監錄 5-VOIP坐席監錄 6-模擬中繼監錄 7-數字中繼監錄
          sprintf(szInikey, "VopChn[%d].Type", nVopChnNo);
          nVopChnType = GetPrivateProfileInt(szIniSection, szInikey, -1, g_szServiceINIFileName);
        }
        else
        {
          //按交換機的設備分機號設置
          sprintf(szInikey, "VopChn[%d].DeviceID", nVopChnNo);
          GetPrivateProfileString("SEAT", szInikey, "", szTemp, 120, g_szServiceINIFileName);
          ArrString[0] = "";
          ArrString[1] = "";
          if (SplitTxtLine(szTemp, 2, ArrString) >= 1)
          {
            strncpy(szDeviceID, ArrString[0].C_Str(), MAX_TELECODE_LEN-1);
          }
          nVopChnType = atoi(ArrString[1].C_Str());
          nPortID = -1;
        }
        
        pVopGroup->m_pVops[nVopNo].SetSwtData(nVopChnNo, nVopNo, nPortID);
        pVopGroup->m_pVops[nVopNo].SetSwtDeviceID(nVopChnNo, szDeviceID);
        pVopGroup->m_pVops[nVopNo].SetVopChnType(nVopChnNo, nVopChnType);
        pVopGroup->m_pVops[nVopNo].SetCardData(nVopChnNo, CH_A_EXTN, nVopChnNo, nVopChnType);
        
        pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nIndex = nVopIndex;
        if (nPortID >= 0 && nVopChnType >= 0)
        {
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nValid = 1;
          if (nVopNo > 0)
          {
            if (nVopChnType == 5)
              pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState = 0;
            else
              pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState = 1;
          }
          if (nVopChnType == 0 || nVopChnType == 1)
          {
            nIVRCount++;
            if (nIVRCount > AuthMaxIVR)
            {
              MyTrace(0, "InitMsg: InitVopResource fail IVR AuthKey error");
              //return 3;
              AuthPassId = false;
              WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
            }
          }
          else if (nVopChnType == 2)
          {
            nFAXCount++;
            if (nFAXCount > AuthMaxFax)
            {
              MyTrace(0, "InitMsg: InitVopResource fail FAX AuthKey error");
              //return 4;
              AuthPassId = false;
              WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
            }
          }
          else
          {
            nRECDCount++;
            if (nRECDCount > AuthMaxRECD)
            {
              MyTrace(0, "InitMsg: InitVopResource fail RECD AuthKey error");
              //return 5;
              AuthPassId = false;
              WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
            }
          }
        }
        else
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState = 2;
        nVopIndex ++;
        
        MyTrace(3, "Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d LnState=%d",
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nIndex,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopNo,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopChnNo,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopChnType,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nChIndex,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSwtPortID,
          pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState);
      }
    }
  }
  pVopGroup->m_nVopChnNum = nVopIndex;
  if (pVopGroup->m_pVops[0].m_nValid == 0)
  {
    sprintf(szTemp, "%s\\AuthSerialNo.ini", g_szAppPath);
    Fkey = fopen(szTemp, "wt");
    if (Fkey != NULL)
    {
      fprintf(Fkey, "[AUTHPSW]\n");
      fprintf(Fkey, "Time=%s\n", MyGetNow());
      fprintf(Fkey, "BoardSerialNo=0\n");
      fprintf(Fkey, "AuthData=%s\n", md5psw);
      fclose(Fkey);
      Fkey = NULL;
    }

    WritePrivateProfileString("AUTHPSW", "AuthData", md5psw, g_szServiceINIFileName);
    sprintf(szTemp, "authkey%s", md5psw);
    strcpy(md5psw, MDString(szTemp));
    if (strncmp(AuthMd5Psw, md5psw, 32) == 0 || strncmp(AuthMd5Psw, "d372e4383252be666aa2051ef43b7eeb", 32) == 0)
    {
      AuthPassId = true;
      WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "OK", g_szServiceINIFileName);
      MyTrace(0, "InitMsg: InitVopResource VopNum[0] not set!");
    }
    else
    {
      WritePrivateProfileString("AUTHPSW", "AUTHRESULT", "FAIL", g_szServiceINIFileName);
      MyTrace(0, "InitMsg: InitVopResource VopNum[0] not set AuthKey error!");
    }
  }
  return 0;
}
int InitNoCTISwitchData()
{
  int nPortNum;
  
  nPortNum = GetPrivateProfileInt("PORT","PORTNUM", 0, g_szCTILINKINIFileName);
  
  pSwitchPortList = new CSwitchPortList;
  if (pSwitchPortList)
  {
    pSwitchPortList->InitData(nPortNum);
    pSwitchPortList->ReadIniSetParam(g_szCTILINKINIFileName);

    if (pBoxchn->Init(nPortNum) == 0)
    {
      pBoxchn->InitChnIdx(g_nSwitchType, nPortNum);
      pBoxchn->TotalTrkNum = 0;
      pBoxchn->TotalSeatNum = 0;
      pBoxchn->TotalIvrNum = 0;
      pBoxchn->TotalRecNum = 0;
    }

    short nPortID;
    short nChStyle;
    short nChStyleIndex;
    short nChType;
    short nChIndex;
    short nLgChType;
    short nLgChIndex;

    for (nPortID = 0; nPortID < nPortNum; nPortID++)
    {
      nChStyle = pSwitchPortList->m_pSwitchPort[nPortID].m_nChStyle;
      nChStyleIndex =  pSwitchPortList->m_pSwitchPort[nPortID].m_nChStyleIndex;
      nChType =  pSwitchPortList->m_pSwitchPort[nPortID].m_nChType;
      nChIndex =  pSwitchPortList->m_pSwitchPort[nPortID].m_nChIndex;
      nLgChType =  pSwitchPortList->m_pSwitchPort[nPortID].m_nLgChType;
      nLgChIndex =  pSwitchPortList->m_pSwitchPort[nPortID].m_nLgChIndex;

      if (nChStyle > 0)
      {
        MyTrace(3, "DeviceID=%s nPortID=%d nChStyle=%d nChStyleIndex=%d nChType=%d nChIndex=%d nLgChType=%d nLgChIndex=%d FlwAccessCode=%s",
          pSwitchPortList->m_pSwitchPort[nPortID].m_szDeviceID, nPortID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex, pSwitchPortList->m_pSwitchPort[nPortID].m_szFlwAccessCode);
      }
  
      pBoxchn->SetChData(nPortID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex);
      pBoxchn->SetChtypeData(nChType, nChIndex, nPortID);
      pBoxchn->SetChDeviceID(nPortID, pSwitchPortList->m_pSwitchPort[nPortID].m_szDeviceID);

      pBoxchn->pChn_str[nPortID].CTILinkClientId = 0;

      //設置語音資源通道與交換機端口的對應關系
      if (pVopGroup)
        pVopGroup->SetDeviceID(nPortID, nPortID, pSwitchPortList->m_pSwitchPort[nPortID].m_szDeviceID);

      if (nLgChType == CHANNEL_TRUNK)
      {
        pBoxchn->SetTrunkCh(nLgChIndex, nPortID);
        pBoxchn->TotalTrkNum ++;

        if (pVopGroup)
          pBoxchn->pChn_str[nPortID].cVopChn = pVopGroup->GetVopChnByChnNo(nPortID);
    
        if (pBoxchn->pChn_str[nPortID].cVopChn != NULL)
        {
          pBoxchn->pChn_str[nPortID].SeizeVopChnState = 1;
          pBoxchn->pChn_str[nPortID].VOC_TrunkId = 1; //IVR語音中繼標志
          pBoxchn->pChn_str[nPortID].cVopChn->SeizeVopChn(nPortID);
          MyTrace(3, "nChn=%d banding VOPCHN Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d", 
            nPortID,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nIndex,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopNo,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnNo,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnType,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nChIndex,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nSwtPortID);
        }
        if (nChStyle == CH_A_EXTN && nPortID < MAX_CHANGE_EXT_NUM)
        {
          if (strlen(pIVRCfg->PSTNCode[nChStyleIndex]) == 0)
          {
            strncpy(pIVRCfg->PSTNCode[nChStyleIndex], pSwitchPortList->m_pSwitchPort[nPortID].m_szFlwAccessCode, 19);
          }
        }
      }
      else if (nLgChType == CHANNEL_SEAT)
      {
        pBoxchn->SetSeatCh(nLgChIndex, nPortID, pSwitchPortList->m_pSwitchPort[nPortID].m_szDeviceID, 1);
        pBoxchn->TotalSeatNum++;
    
        if (pVopGroup)
          pBoxchn->pChn_str[nPortID].cVopChn = pVopGroup->GetVopChnByChnNo(nPortID);
    
        if (pBoxchn->pChn_str[nPortID].cVopChn != NULL)
        {
          pBoxchn->pChn_str[nPortID].SeizeVopChnState = 1;
          pBoxchn->pChn_str[nPortID].cVopChn->SeizeVopChn(nPortID);
          MyTrace(3, "nChn=%d banding VOPCHN Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d", 
            nPortID,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nIndex,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopNo,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnNo,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnType,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nChIndex,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nSwtPortID);
        }
      }
      else if (nLgChType == CHANNEL_IVR)
      {
        if (pVopGroup)
        {
          pBoxchn->SetIvrCh(nLgChIndex, nPortID, pVopGroup->GetVopChnByChnNo(nPortID));
          pBoxchn->TotalIvrNum ++;
        }

        if (pVopGroup)
          pBoxchn->pChn_str[nPortID].cVopChn = pVopGroup->GetVopChnByChnNo(nPortID);
        if (pBoxchn->pChn_str[nPortID].cVopChn != NULL)
        {
          pBoxchn->pChn_str[nPortID].SeizeVopChnState = 1;
          pBoxchn->pChn_str[nPortID].VOC_TrunkId = 1; //IVR語音中繼標志
          pBoxchn->pChn_str[nPortID].cVopChn->SeizeVopChn(nPortID);
          MyTrace(3, "nChn=%d banding VOPCHN Index=%d VopNo=%d VopChnNo=%d VopChnType=%d CardChIndex=%d SwtPortID=%d", 
            nPortID,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nIndex,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopNo,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnNo,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nVopChnType,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nChIndex,
            pBoxchn->pChn_str[nPortID].cVopChn->m_nSwtPortID);
        }
        if (nChStyle == CH_A_EXTN && nChStyleIndex < MAX_CHANGE_EXT_NUM)
        {
          if (strlen(pIVRCfg->PSTNCode[nChStyleIndex]) == 0)
          {
            strncpy(pIVRCfg->PSTNCode[nChStyleIndex], pSwitchPortList->m_pSwitchPort[nPortID].m_szFlwAccessCode, 19);
          }
        }
      }
      else if (nLgChType == CHANNEL_REC)
      {
        pBoxchn->SetRecCh(nLgChIndex, nPortID);
        pBoxchn->TotalRecNum ++;
      }
    }
    if (pBoxchn->pIdelTrkChnQueue == NULL)
    {
      pBoxchn->pIdelTrkChnQueue = new CIdelTrkChnQueue;
      if (pBoxchn->pIdelTrkChnQueue != NULL)
      {
        pBoxchn->pIdelTrkChnQueue->InitQueue(pBoxchn->TotalTrkNum);
        for (int i = 0; i < pBoxchn->TotalTrkNum; i++)
        {
          pBoxchn->pIdelTrkChnQueue->TrkNoJoinInQueueTail(i);
        }
      }
    }
    SetLocalSeatChn();
    g_bChnInit = true;
  }
  return 0;
}
int ConnectDB()
{
  try
  {
    if (g_pDbConnect == NULL)
      g_pDbConnect = new CADODatabase;
    if (g_pDbConnect->Open(g_szConnectionString))
    {
      MyTrace(0, "ConnectDB success DBServer=%s DBName=%s DBUser=%s", g_szDBServer, g_szDBName, g_szDBUser);
      g_nConnectDbState = 1;
      g_pRsQuery = CADORecordset(g_pDbConnect);
      return 0;
    }
    else
    {
      MyTrace(0, "ConnectDB fail DBServer=%s DBName=%s DBUser=%s", g_szDBServer, g_szDBName, g_szDBUser);
      g_nConnectDbState = 2;
    }
  }
  catch (CADOException e)
  {
    MyTrace(0, "ConnectDB fail=%s DBServer=%s DBName=%s DBUser=%s", (LPCTSTR)e.GetErrorMessage(), g_szDBServer, g_szDBName, g_szDBUser);
    g_nConnectDbState = 2;
  }
  return 1;
}
//是否為本地手機: 0-不是，1-是，2-固話號碼，3-內部分機
int IsLocaMobileNo(LPCTSTR PhoneNo, char *pszResult)
{
  static int nCount=0;
  char szSql[256], szTemp[64];
  
  memset(szTemp, 0, 64);
  if (strlen(PhoneNo) == 11 && (strncmp(PhoneNo, "13", 2) == 0 || strncmp(PhoneNo, "14", 2) == 0 || strncmp(PhoneNo, "15", 2) == 0 || strncmp(PhoneNo, "18", 2) == 0 || strncmp(PhoneNo, "17", 2) == 0))
  {
    strcpy(szTemp, PhoneNo);
  }
  else if (strlen(PhoneNo) == 12 && (strncmp(PhoneNo, "013", 3) == 0 || strncmp(PhoneNo, "014", 3) == 0 || strncmp(PhoneNo, "015", 3) == 0 || strncmp(PhoneNo, "018", 3) == 0 || strncmp(PhoneNo, "017", 3) == 0))
  {
    strcpy(szTemp, &PhoneNo[1]);
  }
  else if (strlen(PhoneNo) < 5)
  {
    strcpy(pszResult, PhoneNo);
    MyTrace(1, "IsLocaMobileNo1 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
    return 3;
  }
  else
  {
    strcpy(pszResult, PhoneNo);
    MyTrace(1, "IsLocaMobileNo2 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
    return 2;
  }
  if (g_nConnectDbState != 1)
  {
    if (nCount++ < 300)
    {
      strcpy(pszResult, PhoneNo);
      MyTrace(1, "IsLocaMobileNo3 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
      return 0;
    }
  }
  nCount = 0;
  try
  {
    if (g_nConnectDbState != 1)
    {
      if (g_pDbConnect->Open(g_szConnectionString))
      {
        g_nConnectDbState = 1;
        g_pRsQuery = CADORecordset(g_pDbConnect);
        MyTrace(0, "ReOpen ADODB (%s) OK!!!", g_szConnectionString);
      }
      else
      {
        g_nConnectDbState = 2;
        MyTrace(0, "ReOpen ADODB (%s) FAIL!!!", g_szConnectionString);
        strcpy(pszResult, PhoneNo);
        MyTrace(1, "IsLocaMobileNo4 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
        return 0;
      }
    }
  }
  catch (...)
  {
    MyTrace(0, "ReOpen ADODB (%s) FAIL!!!", g_szConnectionString);
    strcpy(pszResult, PhoneNo);
    MyTrace(1, "IsLocaMobileNo5 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
    return 0;
  }

  if (g_nDBType == DB_SQLSERVER)
    sprintf(szSql, "select Id from tblocamobilelist where num=left('%s',Len(num))", szTemp);
  else if (g_nDBType == DB_MYSQL)
    sprintf(szSql, "select Id from tblocamobilelist where num=left('%s',Length(num))", szTemp);
  
  try
  {
    g_pRsQuery.Open(szSql, CADORecordset::openQuery);
    if (g_pRsQuery.GetRecordCount() > 0)
    {
      g_pRsQuery.Close();
      sprintf(pszResult, "%s%s", pIVRCfg->SWTPreCodeEx, szTemp); //2016-11-28
      MyTrace(1, "IsLocaMobileNo6 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
      return 0;
    }
    else
    {
      sprintf(pszResult, "%s0%s", pIVRCfg->SWTPreCodeEx, szTemp); //2016-11-28
      MyTrace(1, "NotLocaMobileNo7 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
    }
    g_pRsQuery.Close();
  }
  catch (CADOException e)
  {
    if (strstr((LPCTSTR)e.GetErrorMessage(), "連接失敗") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "超時已過期") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "Missing Connection or ConnectionString") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "gone away") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "not connected to ORACLE") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "超出打開游標的最大數") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "一般性網絡錯誤") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "未連接到") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "硈??毖") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "?�Z�I??蕩�Z����") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "筄��?戳") != NULL
      || strstr((LPCTSTR)e.GetErrorMessage(), "ORA-03114") != NULL
      )
    {
      g_nConnectDbState = 2;
    }
    MyTrace(0, "QueryRecvStateByUidl fail %s", (LPCTSTR)e.GetErrorMessage());
    g_pRsQuery.Close();
    sprintf(pszResult, "%s%s", pIVRCfg->SWTPreCodeEx, PhoneNo); //2016-11-28
    MyTrace(1, "IsLocaMobileNo8 PhoneNo=%s NewPhoneNo=%s", PhoneNo, pszResult);
    return 0;
  }
  
  return 1;
}
//-----------------------------------------------------------------------------
//運行前初始化所有（返回0表示成功）
int InitRun(void)
{
  ::CoInitialize(NULL);
  WriteRegFirstRunDate();
  if (ReadSERVICEIniFile() != 0)
  {
    return 2;
  }
  if (ConnectDB() == 0)
  {
    try
    {
      g_pDbConnect->Execute("exec sp_ClearSumData 1");
    }
    catch (CADOException e)
    {
    }
  }
  if (InitResourceMem() != 0)
  {
    return 3;
  }
  WriteRegAuthData();
  if (g_nSwitchMode == 0)
  {
    //初始化語音卡驅動DLL
    SetAllCallBackFunc(); //設置回調函數
    if (InitVoiceCard() != 0)
    {
      return 3;
    }
  }
  else
  {
    if (InitVopResource() != 0) //初始話語音機資源
    {
      MyTrace(0, "InitMsg: InitVopResource fail");
      return 4;
    }
    ReadWorkerGroupName();
    ReadLocaSeatParam(g_szServiceINIFileName, pBoxchn->TotalSeatNum);
    pBoxchn->Read_Router(g_szServiceINIFileName); //add by zgj 2011-11-30
    if (g_nSwitchType == PORT_NOCTI_PBX_TYPE)
    {
      //初始化無CTI接口的交換機端口
      InitNoCTISwitchData();
    }
  }
  //數據庫操作線程
  g_hDbThrdProc = AfxBeginThread(DbProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
  if (g_hDbThrdProc)
  {
    g_hDbThrdProc->m_bAutoDelete = true;
    g_hDbThrdProc->ResumeThread();
  }
  //通信動態庫
  pTcpLink = new CTCPLink;
  if (pTcpLink == NULL) 
  {
    return 5;
  }
  else
  {
    if (g_nINIFileType == 0)
      pTcpLink->ReadIni(g_szServiceINIFileName);
    else
      pTcpLink->ReadWebSetupIni(g_szServiceINIFileName);
    
    //初始化服務端
    if(pTcpLink->InitTCPLink() != 0)
    {
      return 6;
    }
    else
    {
      g_hIVRClientThrdProc = AfxBeginThread(IVRServerProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
      if (g_hIVRClientThrdProc)
      {
        g_hIVRClientThrdProc->m_bAutoDelete = true;
        g_hIVRClientThrdProc->ResumeThread();
      }
      else
      {
        return 6;
      }
    }
  }
  //DB通信動態庫
  pDBClient = new CTCPClient;
  if (pDBClient == NULL) 
  {
    return 14;
  }
  else
  {
    strcpy(pDBClient->m_szAppPath, g_szAppPath);
    if (g_nINIFileType == 0)
      pDBClient->ReadIni(g_szServiceINIFileName);
    else
      pDBClient->ReadWebSetupIni(g_szServiceINIFileName);
    
    //初始化服務端
    if(pDBClient->ConnectServer() != 0)
    {
      return 15;
    }
    else
    {
      g_hDBClientThrdProc = AfxBeginThread(DBClientProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
      if (g_hDBClientThrdProc)
      {
        g_hDBClientThrdProc->m_bAutoDelete = true;
        g_hDBClientThrdProc->ResumeThread();
      }
      else
      {
        return 16;
      }
    }
  }
  //AG通信動態庫
  pTcpAgLink = new CTCPAgServer;
  if (pTcpAgLink == NULL) 
  {
    return 7;
  }
  else
  {
    strcpy(pTcpAgLink->m_szAppPath, g_szAppPath);
    if (g_nINIFileType == 0)
      pTcpAgLink->ReadIni(g_szServiceINIFileName);
    else
      pTcpAgLink->ReadWebSetupIni(g_szServiceINIFileName);
    
    //初始化服務端
    if(pTcpAgLink->InitTCPLink() != 0)
    {
      return 7;
    }  
    else
    {
      g_hAGServerThrdProc = AfxBeginThread(AGServerProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
      if (g_hAGServerThrdProc)
      {
        g_hAGServerThrdProc->m_bAutoDelete = true;
        g_hAGServerThrdProc->ResumeThread();
      }
      else
      {
        return 7;
      }
    }
  }
  //CTI主備機通信動態庫
  if (g_nMultiRunMode == 1)
  {
    pTCPHALink = new CTCPHALink;
    if (pTCPHALink == NULL) 
    {
      return 14;
    }
    else
    {
      strcpy(pTCPHALink->m_szAppPath, g_szAppPath);
      if (g_nINIFileType == 0)
        pTCPHALink->ReadIni(g_szServiceINIFileName);
      else
        pTCPHALink->ReadWebSetupIni(g_szServiceINIFileName);
      
      //初始化服務端
      if(pTCPHALink->InitTCPLink() != 0)
      {
        return 15;
      }  
      else
      {
        g_hHAServerThrdProc = AfxBeginThread(HAServerProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
        if (g_hHAServerThrdProc)
        {
          g_hHAServerThrdProc->m_bAutoDelete = true;
          g_hHAServerThrdProc->ResumeThread();
        }
        else
        {
          return 16;
        }
      }
    }
  }

  if (g_WebServerPort > 0)
  {
    if (g_WebServer.Start(g_WebServerPort, g_WebServerTimeOut) == TRUE)
      MyTrace(0, "Run WebServer at port=%d timeout=%d success", g_WebServerPort, g_WebServerTimeOut);
  }
  if (g_bQueryLocaMobileNo == true)
  {
    g_hHTTPThrdProc = AfxBeginThread(HTTPProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
    if (g_hHTTPThrdProc)
    {
      g_hHTTPThrdProc->m_bAutoDelete = true;
      g_hHTTPThrdProc->ResumeThread();
    }
  }
  
  memset(g_szFirstDate, 0, 20);
  strncpy(g_szFirstDate, ReadRegFirstRunDate(), 10);
  g_dwRegRunDays = ReadRegRunDays();
  memset(g_szLastDate, 0, 20);
  if (g_dwRegRunDays > 1)
    strcpy(g_szLastDate, MyIncDays(g_szFirstDate, g_dwRegRunDays-1));
  else
    strcpy(g_szLastDate, g_szFirstDate);
  MyTrace(0, "InitMsg: ReadRegData FirstDate=%s LastDate=%s RunDays=%d", g_szFirstDate, g_szLastDate, g_dwRegRunDays);
  return 0;
}
void ReleaseAllResouce()
{
  if (pTcpAgLink != NULL) //通信消息類
  {
    delete pTcpAgLink;
    pTcpAgLink = NULL;
  }
  MyTrace(0, "ReleaseResouce pTcpAgLink");
  if (pDBClient != NULL) //DB通信消息類
  {
    delete pDBClient;
    pDBClient = NULL;
  }
  MyTrace(0, "ReleaseResouce pDBClient");
  if (pTCPHALink != NULL) //主備機通信消息類
  {
    delete pTCPHALink;
    pTCPHALink = NULL;
  }
  MyTrace(0, "ReleaseResouce pTCPHALink");
  if (pTcpLink != NULL) //通信消息類
  {
    delete pTcpLink;
    pTcpLink = NULL;
  }
  MyTrace(0, "ReleaseResouce pTcpLink");
  if (pFlwRule != NULL) //流程規則類
  {
    delete pFlwRule;
    pFlwRule = NULL;
  }
  MyTrace(0, "ReleaseResouce pFlwRule");
  if (RMsgFifo != NULL) //接收消息隊列
  {
    delete RMsgFifo;
    RMsgFifo = NULL;
  }
  MyTrace(0, "ReleaseResouce RMsgFifo");
  if (pParser != NULL) //解析器
  {
    delete pParser;
    pParser = NULL;
  }
  MyTrace(0, "ReleaseResouce pParser");
  if (pBoxchn != NULL) //通道資源
  {
    delete pBoxchn;
    pBoxchn = NULL;
  }
  MyTrace(0, "ReleaseResouce pBoxchn");
  if (pBoxconf != NULL) //會議資源
  {
    delete pBoxconf;
    pBoxconf = NULL;
  }
  MyTrace(0, "ReleaseResouce pBoxconf");
  if (pCalloutQue != NULL) //呼出隊列
  {
    delete pCalloutQue;
    pCalloutQue = NULL;
  }
  MyTrace(0, "ReleaseResouce pCalloutQue");
  if (pCallbuf != NULL) //呼出緩沖
  {
    delete pCallbuf;
    pCallbuf = NULL;
  }
  MyTrace(0, "ReleaseResouce pCallbuf");
  if (pAgentMng != NULL) //坐席管理類
  {
    delete pAgentMng;
    pAgentMng = NULL;
  }
  MyTrace(0, "ReleaseResouce pAgentMng");
  if (pACDQueue != NULL) //ACD隊列分配
  {
    delete pACDQueue;
    pACDQueue = NULL;
  }
  MyTrace(0, "ReleaseResouce pACDQueue");
  if (pVopGroup != NULL)
  {
    delete pVopGroup;
    pVopGroup = NULL;
  }
  MyTrace(0, "ReleaseResouce pVopGroup");
  if (pSwitchPortList != NULL)
  {
    delete pSwitchPortList;
    pSwitchPortList = NULL;
  }
  MyTrace(0, "ReleaseResouce pSwitchPortList");
  if (pLogInOutFifo != NULL)
  {
    delete pLogInOutFifo;
    pLogInOutFifo = NULL;
  }
  MyTrace(0, "ReleaseResouce pLogInOutFifo");
  if (pSQLSfifo != NULL)
  {
    delete pSQLSfifo;
    pSQLSfifo = NULL;
  }
  MyTrace(0, "ReleaseResouce pSQLSfifo");
  if (g_WebServerPort > 0)
    g_WebServer.Shutdown();
  MyTrace(0, "ReleaseResouce g_WebServer");
  
  if (g_pDbConnect != NULL)
  {
    delete g_pDbConnect;
    g_pDbConnect = NULL;
  }
  MyTrace(0, "ReleaseResouce g_pDbConnect");

  if (pMemvoc != NULL) //內存合成放音文件索引
  {
    delete pMemvoc;
    pMemvoc = NULL;
  }
  MyTrace(0, "ReleaseResouce pMemvoc");
  if (pIVRCfg != NULL) //IVR配置
  {
    delete pIVRCfg;
    pIVRCfg = NULL;
  }
  MyTrace(0, "ReleaseResouce pIVRCfg");
  ::CoUninitialize();
}
//主循環
void APPLoop() 
{
  if (g_bVoiceCardInit == true)
  {
    IvrDrvLoop();
  }
  
  if (g_bChnInit == true)
  {
    TimerAdd();
    ProcessChn();
    ProcessConf();
    ProcAcdQueue();
    ProcCalloutBuf();
    ProcCalloutQue();
    TrkCallMeteCount();
    AgentCallMeteCount();
    SendRouteIdelTrkNum();
  }

  ProcCTILanDown();
  CheckMsgFifo();
}

void OnFireCALLIN(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled)
{
  int nChn, nCsc, nAG, len, VxmlId;
  char szTemp[MAX_TELECODE_LEN];
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv CallIn nChn=%d ChType=%d ChIndex=%d caller=%s called=%s OrgCalled=%s", 
    nChn, ChType, ChIndex, Caller, Called, OrgCalled);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  g_nCallInCount ++;
  DBUpdateCallCDR_UpdateFlag(nChn);
  pBoxchn->ResetChn(nChn);

  if (pChn->lgChnType == 1)
  {
    ChCount.TrkInCount ++;
    ChCount.TrkCallCount ++;
  }
  //是否為純電腦坐席通道處理
  nAG = pChn->nAG;
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (pAG->isWantSeatType(SEATTYPE_AG_PC) && pAG->isTcpLinked()) //pAG->isLogin()) //是電腦坐席且已經登錄 edit 2010-11-08
    {
      SendACK(nChn, 0); //為電腦坐席電話時，摘機直接應答
      pChn->hwState = CHN_HW_VALID;
      pChn->lnState = CHN_LN_SEIZE; //占用
      pChn->CallInOut = CALL_IN;
      pChn->ssState = CHN_SNG_IN_TALK; //呼入通話
      pChn->CanPlayOrRecId = 1;
      pChn->CallTime = time(0);
      pChn->AnsTime = time(0);
      strcpy(pChn->CallerNo, pAG->GetSeatNo().C_Str());
      DispChnStatus(nChn);
      return;
    }
    SetAgentsvState(nAG, AG_SV_INSEIZE);
  }
  pChn->timer = 0;
  pChn->ssState = CHN_SNG_IDLE;
  pChn->hwState = CHN_HW_VALID;
  pChn->lnState = CHN_LN_SEIZE; //占用
  
  if (pChn->lgChnType == CHANNEL_TRUNK) //EDIT 2011-06-20
  {
    pChn->CallInOut = CALL_IN;
    pChn->RecvDialDtmfId = 1;
  } 
  else if (pChn->lgChnType == CHANNEL_REC)
  {
    if (CallParam == 256)
    {
      //監錄通道檢測到呼出
      pChn->CallInOut = CALL_OUT;
      pChn->nCallType = 2;
    } 
    else
    {
      pChn->CallInOut = CALL_IN;
      pChn->RecvDialDtmfId = 1;
    }
  }
  else
  {
    pChn->CallInOut = CALL_OUT;
  }
  
  pChn->CallInId = 1; //有呼叫進入
  
  //設置主叫號碼
  len = strlen(Caller);
  if (len > 0)
  {
    if (Caller[0] == '9' && len >= 7)
    {
      if (pChn->ChStyle == CH_A_EXTN)
      {
        if (pIVRCfg->isAutoDelPSTNPreCode == true)
        {
          strcpy(pChn->CallerNo, &Caller[1]);
        }
        else
        {
          strcpy(pChn->CallerNo, Caller);
        }
        pIVRCfg->WritePSTNPreCode(pChn->ChStyleIndex, "9,");
      }
      else
      {
        strcpy(pChn->CallerNo, Caller);
      }
    }
    else
    {
      strcpy(pChn->CallerNo, Caller);
    }
  }
  else
  {
    if (pChn->ChStyle == CH_A_SEAT)
    {
      nCsc = pChn->lgChnNo;
      if (pBoxchn->isnCscAvail(nCsc))
      {
        strcpy(pChn->CallerNo, pCsc->SeatNo.C_Str());
      }
      else
      {
        strcpy(pChn->CallerNo, "000");
      }
    }
    else if (pChn->ChStyle == CH_A_EXTN)
    {
      strcpy(pChn->CallerNo, "000");
    }
  }

  //設置被叫號碼
  if (pChn->ChStyle == CH_A_EXTN)
  {
    pChn->RingCount ++;
    if (pIVRCfg->PSTNChangeId == 1 && strlen(pIVRCfg->PSTNCode[pChn->ChStyleIndex]) > 0)
    {
      strcpy(pChn->CalledNo, pIVRCfg->PSTNCode[pChn->ChStyleIndex]);
    }
    else
    {
      strcpy(pChn->CalledNo, pIVRCfg->CenterCode);
    }
  }
  else if (pChn->ChStyle == CH_A_SEAT)
  {
    if (pParser->CheckSeatDirectIn(VxmlId))
    {
      strcpy(pChn->CalledNo, "A");
    }
  }
  else if (pChn->ChStyle == CH_D_VOIP || pChn->ChStyle == CH_S_VOIP)
  {
    if (pIVRCfg->VOIPChangeId == 1)
    {
      if (strlen(pIVRCfg->NewVOIPCalled[pChn->ChStyleIndex]) > 0)
        strcpy(pChn->CalledNo, pIVRCfg->NewVOIPCalled[pChn->ChStyleIndex]);
      else if (strlen(pIVRCfg->VOIPDefaultCalled) > 0)
        strcpy(pChn->CalledNo, pIVRCfg->VOIPDefaultCalled);
      else
        strcpy(pChn->CalledNo, Called);
    }
    else
    {
      strcpy(pChn->CalledNo, Called);
    }
  }
  else
  {
    strcpy(pChn->CalledNo, Called);
  }
  if (pIVRCfg->ProcLocaAreaCodeId1 == 1 && strlen(pChn->CalledNo) > pIVRCfg->LocaTelCodeLen)
  {
    len = strlen(pIVRCfg->LocaAreaCode);
    if ((strlen(pChn->CalledNo) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
    {
      if (strncmp(pChn->CalledNo, pIVRCfg->LocaAreaCode, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CalledNo[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CalledNo, szTemp, MAX_TELECODE_LEN);
      }
    }
    len = strlen(pIVRCfg->LocaAreaCode1);
    if ((strlen(pChn->CalledNo) == pIVRCfg->LocaTelCodeLen1+len) && len > 0)
    {
      if (strncmp(pChn->CalledNo, pIVRCfg->LocaAreaCode1, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CalledNo[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CalledNo, szTemp, MAX_TELECODE_LEN);
      }
    }
  }

  //模擬監錄通道號碼處理
  if (pChn->ChStyle == CH_A_RECD)
  {
    if (pChn->CallInOut == CALL_IN)
    {
      if (ChIndex < MAX_CHANNEL_REC_NUM)
        strcpy(pChn->CalledNo, pIVRCfg->RecLineCode[pChn->ChStyleIndex]);
      else
        strcpy(pChn->CalledNo, "R");
    }
    else
    {
      if (ChIndex < MAX_CHANNEL_REC_NUM)
        strcpy(pChn->CallerNo, pIVRCfg->RecLineCode[pChn->ChStyleIndex]);
      else
        strcpy(pChn->CallerNo, "R");
    }
  } 

  pChn->OrgCalledNo = OrgCalled;

  if (pChn->lgChnType == CHANNEL_TRUNK)
  {
    pBoxchn->pIdelTrkChnQueue->TrkNoDelFromQueue(pChn->lgChnNo);
  }
  if (pIVRCfg->isAutoDelMobilePreCode0 == true)
  {
    if (strncmp(pChn->CallerNo, "013", 3) == 0 || strncmp(pChn->CallerNo, "015", 3) == 0 || strncmp(pChn->CallerNo, "018", 3) == 0)
    {
      memset(szTemp, 0, MAX_TELECODE_LEN);
      strncpy(szTemp, &pChn->CallerNo[1], MAX_TELECODE_LEN-2);
      memcpy(pChn->CallerNo, szTemp, MAX_TELECODE_LEN);
    }
  }
  if (pChn->lgChnType == 1)
  {
    strcpy(pChn->CustPhone, pChn->CallerNo);
  }
  else
  {
    len = strlen(pIVRCfg->DialOutPreCode);
    if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
    {
      strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
    }
    else
    {
      strcpy(pChn->CustPhone, pChn->CalledNo);
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
    }
  }
  if (pIVRCfg->ProcLocaAreaCodeId == 1 && strlen(pChn->CustPhone) > pIVRCfg->LocaTelCodeLen)
  {
    len = strlen(pIVRCfg->LocaAreaCode);
    if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
    {
      if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    len = strlen(pIVRCfg->LocaAreaCode1);
    if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1+len) && len > 0)
    {
      if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode1, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    len = strlen(pIVRCfg->LocaAreaCode2);
    if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
    {
      if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode2, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    len = strlen(pIVRCfg->LocaAreaCode3);
    if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
    {
      if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode3, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    len = strlen(pIVRCfg->LocaAreaCode4);
    if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
    {
      if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode4, len) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
        memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
      }
    }
    strcpy(pChn->CallerNo, pChn->CustPhone);
  }
  MyTrace(3, "Recv CallIn nChn=%d CallerNo=%s CalledNo=%s OrgCalledNo=%s CustPhone=%s", 
    nChn, pChn->CallerNo, pChn->CalledNo, pChn->OrgCalledNo.C_Str(), pChn->CustPhone);
  DispChnStatus(nChn);
}
void OnFireRECVSAM(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called)
{
  int nChn, len;
  char szTemp[MAX_TELECODE_LEN];
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv SAM nChn=%d ChType=%d ChIndex=%d Caller=%s called=%s", 
    nChn, ChType, ChIndex, Caller, Called);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  len = strlen(Caller);
  if (len > 0)
  {
    if (pChn->ChStyle == CH_A_EXTN)
    {
      if (Caller[0] == '9' && len >= 7)
      {
        if (pIVRCfg->isAutoDelPSTNPreCode == true)
        {
          strcpy(pChn->CallerNo, &Caller[1]);
        }
        else
        {
          strcpy(pChn->CallerNo, Caller);
        }
        pIVRCfg->WritePSTNPreCode(pChn->ChStyleIndex, "9,");
      }
      else
      {
        strcpy(pChn->CallerNo, Caller);
      }
    }
    else
    {
      if ((strlen(pChn->CallerNo) + len) < MAX_TELECODE_LEN)
        strcat(pChn->CallerNo, Caller);
    }
    if (pIVRCfg->isAutoDelMobilePreCode0 == true)
    {
      if (strncmp(pChn->CallerNo, "013", 3) == 0 || strncmp(pChn->CallerNo, "015", 3) == 0 || strncmp(pChn->CallerNo, "018", 3) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CallerNo[1], MAX_TELECODE_LEN-2);
        memcpy(pChn->CallerNo, szTemp, MAX_TELECODE_LEN);
      }
    }
  }
  
  len = strlen(Called);
  if (len > 0)
  {
    if ((strlen(pChn->CalledNo) + len) < MAX_TELECODE_LEN)
      strcat(pChn->CalledNo, Called);
    //add 2010-06-03
    if (pChn->RecvDialDtmfId == 0 && pChn->ChStyle == CH_A_SEAT)
    {
      if (pChn->WaitDialTimer <= 8)
      {
        if ((strlen(pChn->RecvDialDtmf) + len) < MAX_DTMFBUF_LEN)
        {
          strcat(pChn->RecvDialDtmf, Called);
          SendSeatDialDTMFEvent(nChn, pChn->RecvDialDtmf);
        }
        pChn->WaitDialTimer = 0;
      }
      else
      {
        pChn->RecvDialDtmfId = 1;
      }
    }
  }
  if (pChn->lgChnType == 1)
  {
    strcpy(pChn->CustPhone, pChn->CallerNo);
  }
  else
  {
    len = strlen(pIVRCfg->DialOutPreCode);
    if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
    {
      strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
    }
    else
    {
      strcpy(pChn->CustPhone, pChn->CalledNo);
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
    }
  }
  if (pChn->ChStyle == CH_A_EXTN)
  {
    pChn->RingCount ++;
  }
  pChn->DtmfPauseTimer = 0;
  DispChnStatus(nChn);
}
void OnFireRECVACM(short ChType, short ChIndex, short AcmParam)
{
  US nChn, nQue, nOut, nAcd, nAG;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv ACM nChn=%d ChType=%d ChIndex=%d AcmParam=%x", 
    nChn, ChType, ChIndex, AcmParam);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  nQue = pChn->nQue;
  if (pCalloutQue->isnQueAvail(nQue))
  {
    pQue->CalloutResult = CALLOUT_RING;
    if (pQue->OrgCallType == 1)
    {
      nOut = pQue->nOut;
      pOut->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
    }
    else
    {
      nAcd = pQue->nAcd;
      pAcd->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
    }
    nAG = pQue->nAG;
    if (pAgentMng->isnAGAvail(nAG))
    {
      SetAgentsvState(nAG, AG_SV_INRING);
    }
    
    pChn->ssState = CHN_SNG_OT_RING; //呼出振鈴
    DispChnStatus(nChn);
  }
}
void OnFireRECVACK(short ChType, short ChIndex, short AckParam)
{
  int nChn, nQue;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv ACK nChn=%d ChType=%d ChIndex=%d AckParam=%d", 
    nChn, ChType, ChIndex, AckParam);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  if (AckParam == 9) //應答來話成功（針對發應答指令后，不是馬上返回結果的情況）
  {
    SendAnswerResult(nChn, OnSuccess, "");
    pChn->CanPlayOrRecId = 1;
    pChn->AnsTime = time(0);
    pChn->ssState = CHN_SNG_IN_TALK;
    DispChnStatus(nChn);
    return;
  }
  else if (AckParam == 10) //應答來話失敗（針對發應答指令后，不是馬上返回結果的情況）
  {
    SendAnswerResult(nChn, OnFail, "");
    return;
  }
  else if (AckParam == 2) //錄音通道呼入后摘機應答
  {
    pChn->CanPlayOrRecId = 1;
    pChn->AnsTime = time(0);
    pChn->ssState = CHN_SNG_IN_TALK;
    SendAnswerResult(nChn, OnInAnswer, "");
    DBUpdateCallCDR_AnsTime(nChn);
    DispChnStatus(nChn);
    return;
  }

  if (pChn->lgChnType == 1)
  {
    ChCount.TrkOutAnsCount ++;
  }
  nQue = pChn->nQue;
  if (pCalloutQue->isnQueAvail(nQue))
  {
    pQue->CalloutResult = CALLOUT_CONN;
    pChn->ssState = CHN_SNG_OT_TALK; //呼出振鈴
    pChn->CalloutId = 0;
    pChn->nQue = 0xFFFF;
    pChn->CanPlayOrRecId = 1; //允許開始放錄音
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
}
void OnFireRELEASE(short ChType, short ChIndex, short ReleaseType, short ReleaseReason)
{
  int nChn, nQue;
  CAgent *pAgent=NULL;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Release nChn=%d ChType=%d ChIndex=%d ReleaseType=%d ReleaseReason=%d", 
    nChn, ChType, ChIndex, ReleaseType, ReleaseReason);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  switch (ReleaseType)
  {
  case F_R_NORMAL: //0-正常釋放
  case F_R_CINFAIL: //2-呼入失敗釋放
    if (pChn->lnState == CHN_LN_WAIT_REL && (pChn->ssState == CHN_SNG_IDLE || pChn->ssState == CHN_SNG_DELAY))
    {
      pChn->lnState = CHN_LN_IDLE;
      DispChnStatus(nChn);
      if (pChn->ChStyle == CH_A_SEAT && GetSeatTypeBynChn(nChn) != SEATTYPE_AG_PC)
      {
        pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          SetAgentsvState(pAgent, AG_SV_IDEL);
          //2015-09-06
          if (pAgent->m_Worker.TelLogInOutFlag == 1)
          {
            MyTrace(3, "-Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
              nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
            pAgent->DelayState = 0;
            pAgent->m_Worker.TelLogInOutFlag = 0;
            
            pAgent->ChangeStatus(AG_STATUS_LOGIN);
            pAgent->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
            WriteWorkerLoginStatus(pAgent);
            
            strcpy(pAgent->StateChangeTime, MyGetNow());
            AgentStatusCount();
            SendOneGroupStatusMeteCount(pAgent->nAG);
            SendSystemStatusMeteCount();
            
            DispAgentStatus(pAgent->nAG);
          }
          else if (pAgent->m_Worker.TelLogInOutFlag == 2)
          {
            MyTrace(3, "--Release_Chn nChn=%d nAG=%d DelayState=%d DelayTime=%d TelLogInOutFlag=%d",
              nChn, pAgent->nAG, pAgent->DelayState, pAgent->m_Worker.DelayTime, pAgent->m_Worker.TelLogInOutFlag);
            pAgent->DelayState = 0;
            pAgent->m_Worker.TelLogInOutFlag = 0;
            
            WriteSeatStatus(pAgent, AG_STATUS_LOGOUT, 0);
            WriteWorkerLogoutStatus(pAgent);
            
            int nGroupNo = pAgent->GetGroupNo();
            if (pAgent->m_Worker.oldWorkerNo == 0)
            {
              pAgent->m_Worker.Logout();
              pAgent->m_Worker.ClearState();
            }
            else
            {
              pAgent->m_Worker.OldWorkerLogin();
            }
            pAgent->m_Worker.OldDisturbId = 0;
            pAgent->m_Worker.OldLeaveId = 0;
            
            DispAgentStatus(pAgent->nAG);
            
            AgentStatusCount();
            SendOneGroupStatusMeteCount(pAgent->nAG);
            SendSystemStatusMeteCount();
          }
        }
      }
      break;
    }
    pChn->lnState = CHN_LN_RELEASE; //設置線路釋放標志
    
    //斷開已連接的通道
    //UnLink_All_By_ChnNo(nChn, 1);
    DispChnStatus(nChn);
    break;
    
  case F_R_COUTFAIL: //1-呼出失敗釋放
    if (ReleaseReason == P_R_NORMAL)
    {
      UnLink_All_By_ChnNo(nChn, 1);
      DBUpdateCallCDR_UpdateFlag(nChn);
      pBoxchn->ResetChn(nChn);
      DispChnStatus(nChn);
      break;
    }
    UnLink_All_By_ChnNo(nChn, 1);
    nQue = pChn->nQue;
    if (!pCalloutQue->isnQueAvail(nQue))
    {
      break;
    }
    switch (ReleaseReason)
    {
    case P_R_NORMAL: //正常釋放
      pChn->lnState = CHN_LN_RELEASE; //設置線路釋放標志
      break;
      
    case P_R_COUTNODIALTONE: //1-呼出沒有檢測到撥號音 （只用于外線TB）
      pQue->CalloutResult = CALLOUT_FAIL;
      break;
      
    case P_R_COUTLINEBUSY: //2-呼出線路忙
    case P_R_COUTBUSYLOCAL: //19-市話忙 slb
    case P_R_BUSYUSER: //21-用戶忙 ssb
      pQue->CalloutResult = CALLOUT_SLB;
      break;
    case P_R_COUTBUSYDDD: //20-長話忙 stb
      pQue->CalloutResult = CALLOUT_SLB;
      break;
      
    case P_R_COUTNOSOUND: //3-呼出后沒有聲音（只用于外線TB）
      pQue->CalloutResult = CALLOUT_FAIL;
      break;
      
    case P_R_COUTRINGTIMEOUT: //4-呼出振鈴超時 （對方無人接）
      pQue->CalloutResult = CALLOUT_NOBODY;
      break;
      
    case P_R_COUTNULLDN: //22-空號
      pQue->CalloutResult = CALLOUT_UNN;
      break;
      
    default:
      pQue->CalloutResult = CALLOUT_FAIL;
      break;
    }
    break;
  }
  DispChnStatus(nChn);
}
void OnFirePLAYRESULT(short ChType, short ChIndex, short Result)
{
  int nChn, nAG;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Play Result nChn=%d ChType=%d ChIndex=%d Result=%d", nChn, ChType, ChIndex, Result);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  pChn->curPlayState = 2; //自然結束停止
  nAG = pChn->nAG;
  if (pAgentMng->isnAGAvail(nAG))
  {
    if (pAG->svState == AG_SV_PLAY)
    {
      SendCommonResult(nAG, AGMSG_onplay, OnPlayEnd, "放音結束");
      if (pAG->isWantSeatType(SEATTYPE_AG_PC) || pAG->isWantSeatType(SEATTYPE_RT_TEL))
      {
        pAG->SetAgentsvState(AG_SV_IDEL);
        DispAgentStatus(nAG);
      }
    }
  }
}
void OnFireRECORDRESULT(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen)
{
  int nChn;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Record Result nChn=%d ChType=%d ChIndex=%d Result=%d", 
    nChn, ChType, ChIndex, Result);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  pChn->curRecState = 5;
}
void OnFireRECVDTMF(short ChType, short ChIndex, LPCTSTR Dtmfs)
{
  int nChn, len, len1, len2;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv dtmf nChn=%d ChType=%d ChIndex=%d Dtmfs=%s", 
    nChn, ChType, ChIndex, Dtmfs);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  len1 = strlen(pChn->curRecvDtmf);
  len2 = strlen(Dtmfs);
  if ((len1+len2) >= MAX_DTMFBUF_LEN)
  {
    len = len1+len2-MAX_DTMFBUF_LEN+1;
    memcpy(pChn->curRecvDtmf, &pChn->curRecvDtmf[len], MAX_DTMFBUF_LEN-len);
    pChn->curRecvDtmf[MAX_DTMFBUF_LEN-len-1] = 0;
    strncat(pChn->curRecvDtmf, Dtmfs, len2);
    pChn->curRecvDtmf[MAX_DTMFBUF_LEN] = 0;
  }
  else
  {
    strncat(pChn->curRecvDtmf, Dtmfs, len2);
  }
  //add 2010-06-03
  if (pChn->RecvDialDtmfId == 1)
  {
    return;
  }
  if (pIVRCfg->isAppendSeatDailDTMF == false && pChn->ChStyle == CH_A_SEAT)
  {
    return;
  }
  if (len2 > 0 && (pChn->ChStyle == CH_A_SEAT || pChn->ChStyle == CH_A_RECD))
  {
    if (pChn->WaitDialTimer > 8)
    {
      pChn->RecvDialDtmfId = 1;
      return;
    }
    if ((strlen(pChn->RecvDialDtmf) + len2) < MAX_DTMFBUF_LEN)
    {
      strcat(pChn->RecvDialDtmf, Dtmfs);
      SendSeatDialDTMFEvent(nChn, pChn->RecvDialDtmf);
      strncpy(pChn->CalledNo, pChn->RecvDialDtmf, MAX_TELECODE_LEN-1);
      DispChnStatus(nChn);
    }
    pChn->WaitDialTimer = 0;
    
    len = strlen(pIVRCfg->DialOutPreCode);
    if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
    {
      strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
    }
    else
    {
      strcpy(pChn->CustPhone, pChn->CalledNo);
    }
    len = strlen(pChn->CustPhone);
    if (len > 1)
    {
      if (pChn->CustPhone[len-1] == '#')
      {
        pChn->CustPhone[len-1] = '\0';
      }
    }
    //DBUpdateCallCDR_Phone(nChn);
  }
}
void OnFireCONFPLAYRESULT(short ConfNo, short Result)
{

}
void OnFireCONFRECORDRESULT(short ConfNo, short Result, long RecdSize, long RecdLen)
{

}
void OnFireSENDFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, short SendPages, long SendBytes, long SendSpeed, LPCTSTR ErrorMsg)
{
  // TODO: Add your control notification handler code here
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Send Fax Result nChn=%d ChType=%d ChIndex=%d FaxChIndex=%d Result=%d SendPages=%d SendBytes=%ld SendSpeed=%d ErrorMsg=%s", 
    nChn, ChType, ChIndex, FaxChIndex, Result, SendPages, SendBytes, SendSpeed, ErrorMsg);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  pChn->LinkType[0] = 0;
  if (Result == 0)
    SendSendfaxResult(nChn, OnsFaxend, ErrorMsg, SendPages, SendBytes, SendSpeed);
  else if (Result == 9)
    SendSendfaxResult(nChn, OnsFaxing, ErrorMsg);
  else
    SendSendfaxResult(nChn, OnsFaxFail, ErrorMsg);
}
void OnFireRECVFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, LPCTSTR FaxCSID, LPCTSTR BarCode, short RecvPages, long RecvBytes, long RecvSpeed, LPCTSTR ErrorMsg)
{
  // TODO: Add your control notification handler code here
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Recv Fax Result nChn=%d ChType=%d ChIndex=%d FaxChIndex=%d Result=%d FaxCSID=%s BarCode=%s RecvPages=%d RecvBytes=%ld RecvSpeed=%ld ErrorMsg=%s", 
    nChn, ChType, ChIndex, FaxChIndex, Result, FaxCSID, BarCode, RecvPages, RecvBytes, RecvSpeed, ErrorMsg);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  pChn->LinkType[0] = 0;
  if (Result == 0)
    SendRecvfaxResult(nChn, OnrFaxend, ErrorMsg, FaxCSID, BarCode, RecvPages, RecvBytes, RecvSpeed);
  else if (Result == 9)
    SendRecvfaxResult(nChn, OnrFaxing, ErrorMsg);
  else
    SendRecvfaxResult(nChn, OnrFaxFail, ErrorMsg);
}
void OnFireFLASH(short ChType, short ChIndex)
{
  int nChn;
  
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Flash nChn=%d ssState=%d ChType=%d ChIndex=%d caller=%s called=%s", 
    nChn, pChn->ssState, ChType, ChIndex, pChn->CallerNo, pChn->CalledNo);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  SendLinkChnEvent(nChn, OnFlash, "");
}
void OnFireLINEALARM(short ChType, short ChIndex, short AlarmType, short AlarmParam)
{
  int i, nChn;
  nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv LINEALARM nChn=%d ChType=%d ChIndex=%d AlarmType=%d AlarmParam=%d", 
    nChn, ChType, ChIndex, AlarmType, AlarmParam);
  if (AlarmType == 99 && g_nUSBInitId != 3)
  {
    g_nUSBInitId = 3;
    for (nChn=0; nChn<pBoxchn->MaxChnNum; nChn++)
    {
      pChn->hwState = 0x01;
      Release_Chn(nChn, 1, 1);
      DispChnStatus(nChn);
    }
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  switch (AlarmType)
  {
  case F_AM_CH:
  case F_AM_CHCAS:
    switch (AlarmParam)
    {
    case P_AM_OK: //與P_AM_CASCHREOK相同處理
      //pChn->hwState &= 0xF0;
      if (pChn->hwState != 0)
      {
        pChn->hwState = 0x00;
        if (pBoxchn->IsAllTrkChnAct() == true)
        {
          g_dwRegServerState = g_dwRegServerState & 0xEFF;
          WriteRegServiceRunState();
        }
        WriteAlarmList(1022, ALARM_LEVEL_MINOR, 0, "IVR CH=%d action", pChn->lgChnNo);
      }
      break;
    default: //其他參數為阻塞
      if (pChn->hwState != 0x01)
      {
        g_dwRegServerState = g_dwRegServerState | 0x100;
        WriteRegServiceRunState();
        pChn->hwState = 0x01;
        WriteAlarmList(1021, ALARM_LEVEL_MAJOR, 1, "IVR CH=%d block", pChn->lgChnNo);
      }
      if (pChn->ssState != 0)
      {
        Release_Chn(nChn, 1, 1);
      }
      break;
    }
    DispChnStatus(nChn);
    break;
    case F_AM_PCMSYN:
      switch (AlarmParam)
      {
      case P_AM_SYNOK:
        if (ChIndex % 32 != 0 && ChIndex % 16 == 0)
        {
          nChn = nChn - 16;
        }
        for (i = 0; i < 32; i ++)
        {
          //if (i%16 == 0) continue;
          if (!pBoxchn->isnChnAvail(nChn))
            break;
          //pChn->hwState &= 0x0F;
          pChn->hwState &= 0x00;
          DispChnStatus(nChn);
          WriteAlarmList(1022, ALARM_LEVEL_MINOR, 0, "IVR CH=%d action", pChn->lgChnNo);
          nChn ++;
        }
        if (pBoxchn->IsAllTrkChnAct() == true)
        {
          g_dwRegServerState = g_dwRegServerState & 0xBF;
          WriteRegServiceRunState();
        }
        break;
      default:
        if (ChIndex % 32 != 0 && ChIndex % 16 == 0)
        {
          nChn = nChn - 16;
        }
        for (i = 0; i < 32; i ++)
        {
          //if (i%16 == 0) continue;
          if (!pBoxchn->isnChnAvail(nChn))
            break;
          pChn->hwState |= 0x10;
          WriteAlarmList(1021, ALARM_LEVEL_MAJOR, 1, "IVR CH=%d block", pChn->lgChnNo);
          if (pChn->ssState != 0)
          {
            Release_Chn(nChn, 1, 1);
          }
          DispChnStatus(nChn);
          nChn ++;
        }
        g_dwRegServerState = g_dwRegServerState | 0x40;
        WriteRegServiceRunState();
        break;
      }
      break;
      case F_AM_MTP:
        break;
      case F_AM_REF:
        break;
  }
}
void OnFireRECORDMEMRESULT(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize)
{

}
void OnFireCONFCREATERESULT(short ChType, short ChIndex, short ConfNo, short CreateId, short Result)
{
  int nCfc;
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  nCfc = ConfNo;
  MyTrace(3, "Recv Create Conf Result ConfNo=%d CreateId=%d Result=%d", 
    ConfNo, CreateId, Result);
  if (!pBoxconf->isnCfcAvail(nCfc))
    return;
  if (CreateId == 1)
  {
    //系統會議創建成功
    nCfc = ConfNo;
    if (Result == ConfNo)
      pCfc->CreateId = 3;
    else
      pCfc->CreateId = 2;
    //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
    nCfc = ConfNo+1;
    if (!pBoxconf->isnCfcAvail(nCfc))
      return;
    pBoxconf->CreateAllSysConf(nCfc);
  }
  else
  {
    if (ConfNo == Result)
    {
      pCfc->CreateId = 3;
      Proc_MSG_createcfc_Result(nChn, OnSuccess, ConfNo, "");
    }
    else
    {
      pBoxconf->InitConf(ConfNo);
      Proc_MSG_createcfc_Result(nChn, OnFail, ConfNo, "Create Conf Fail");
    }
    //theApp.m_MyConfStatusDialog->DispCfcstatus(nCfc);
  }
}
void OnFireCONFDESTROYRESULT(short ChType, short ChIndex, short ConfNo, short Result)
{

}
void OnFireCONFJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result)
{

}
void OnFireCONFLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result)
{

}
void OnFireCONFUNJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result)
{

}
void OnFireCONFUNLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result)
{

}
void OnFireCHECKTONERESULT(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result)
{
  // TODO: Add your control notification handler code here
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Check Tone Result nChn=%d ChType=%d ChIndex=%d ToneType=%d ToneParam=%s Result=%d", 
    nChn, ChType, ChIndex, ToneType, ToneParam, Result);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  SendCheckToneResult(nChn, OnToneDetected, ToneType, "");
}
void OnFireSENDFSKRESULT(short ChType, short ChIndex, short FskChIndex, short Result, short Param)
{
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv Send FSK Result nChn=%d ChType=%d ChIndex=%d FskChIndex=%d Result=%d", 
    nChn, ChType, ChIndex, FskChIndex, Result);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  if (Result == 0)
    SendSendFSKResult(nChn, OnSuccess, "");
  else
    SendSendFSKResult(nChn, OnFail, "send fsk fail");
}
void OnFireRECVFSK(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param)
{
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Recv FSK nChn=%d ChType=%d ChIndex=%d FskChIndex=%d FSKCMD=0x%02x FSKLen=%d Result=%d", 
    nChn, ChType, ChIndex, FskChIndex, FSKCMD, FSKLen, Result);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  if (Result == 0)
    SendRecvdFSKResult(nChn, FSKCMD, (char *)FSKStrAddr, FSKLen, OnSuccess, ""); //(char *)FSKStrAddr
  else
    SendRecvdFSKResult(nChn, 0, "", 0, OnFail, "recv fsk fail");
}
void OnFireFAXSTATE(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FaxState)
{
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  pChn->FlwState = FaxState;
  DispChnStatus(nChn);
}
//轉接結果Result：0-對方應答轉接成功 1-轉接失敗 2-轉接超時 3-轉接遇忙 4-轉接空號 9-轉接振鈴
void OnFireTRANSFERRESULT(short ChType, short ChIndex, short Result, LPCTSTR Reason)
{
  int nChn = pBoxchn->Get_ChnNo_By_CardChn(ChType, ChIndex);
  MyTrace(3, "Transfer Result nChn=%d ChType=%d ChIndex=%d Result=%d Reason=%s", 
    nChn, ChType, ChIndex, Result, Reason);
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  if (Result == 0)
  {
    if (pChn->TranIVRId == 9)
    {
      SendTransferResult1(nChn, 0xFFFF, OnOutAnswer, "");
    }
  }
  else if (Result == 1)
  {
    if (pChn->TranIVRId == 9)
      SendTransferResult1(nChn, 0xFFFF, OnOutFail, "");
  }
  else if (Result == 2)
  {
    if (pChn->TranIVRId == 9)
      SendTransferResult1(nChn, 0xFFFF, OnOutTimeout, "");
  }
  else if (Result == 3)
  {
    if (pChn->TranIVRId == 9)
      SendTransferResult1(nChn, 0xFFFF, OnOutBusy, "");
  }
  else if (Result == 4)
  {
    if (pChn->TranIVRId == 9)
      SendTransferResult1(nChn, 0xFFFF, OnOutUNN, "");
  }
  else if (Result == 9)
  {
    if (pChn->TranIVRId == 9)
      SendTransferResult1(nChn, 0xFFFF, OnOutRing, "");
  }
}
void OnFireSTOPTRANSFERRESULT(short ChType, short ChIndex, short Result)
{

}

//設置所有事件回調函數
void SetAllCallBackFunc()
{
  SetFuncOnFireCALLIN(OnFireCALLIN);
  SetFuncOnFireRECVSAM(OnFireRECVSAM);
  SetFuncOnFireRECVACM(OnFireRECVACM);
  SetFuncOnFireRECVACK(OnFireRECVACK);
  SetFuncOnFireRELEASE(OnFireRELEASE);
  SetFuncOnFirePLAYRESULT(OnFirePLAYRESULT);
  SetFuncOnFireRECORDRESULT(OnFireRECORDRESULT);
  SetFuncOnFireRECVDTMF(OnFireRECVDTMF);
  SetFuncOnFireCONFPLAYRESULT(OnFireCONFPLAYRESULT);
  SetFuncOnFireCONFRECORDRESULT(OnFireCONFRECORDRESULT);
  SetFuncOnFireSENDFAXRESULT(OnFireSENDFAXRESULT);
  SetFuncOnFireRECVFAXRESULT(OnFireRECVFAXRESULT);
  SetFuncOnFireFLASH(OnFireFLASH);
  SetFuncOnFireLINEALARM(OnFireLINEALARM);
  SetFuncOnFireRECORDMEMRESULT(OnFireRECORDMEMRESULT);
  SetFuncOnFireCONFCREATERESULT(OnFireCONFCREATERESULT);
  SetFuncOnFireCONFDESTROYRESULT(OnFireCONFDESTROYRESULT);
  SetFuncOnFireCONFJOINRESULT(OnFireCONFJOINRESULT);
  SetFuncOnFireCONFLISTENRESULT(OnFireCONFLISTENRESULT);
  SetFuncOnFireCONFUNJOINRESULT(OnFireCONFUNJOINRESULT);
  SetFuncOnFireCONFUNLISTENRESULT(OnFireCONFUNLISTENRESULT);
  SetFuncOnFireCHECKTONERESULT(OnFireCHECKTONERESULT);
  SetFuncOnFireSENDFSKRESULT(OnFireSENDFSKRESULT);
  SetFuncOnFireRECVFSK(OnFireRECVFSK);
  SetFuncOnFireFAXSTATE(OnFireFAXSTATE);
  SetFuncOnFireTRANSFERRESULT(OnFireTRANSFERRESULT);
  SetFuncOnFireSTOPTRANSFERRESULT(OnFireSTOPTRANSFERRESULT);
}
//------語音機板卡驅動回調函數聲明---------------------------------------------
void OnFireCALLINForVOP(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled)
{
  CVopChn *pVopChn=NULL;
  CAgent *pAgent;

  MyTrace(3, "Recv CALLIN from LOCAVOP ChType=%d ChIndex=%d CallParam=%d Caller=%s Called=%s OrgCalled=%s",
    ChType, ChIndex, CallParam, Caller, Called, OrgCalled);

  if (isMyselfInActive() == false)
  {
    return;
  }
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }

  int nChn;
  char szTemp[MAX_TELECODE_LEN];
  
  nChn = pVopChn->m_nSeizeChn;

  if (pVopChn->m_nVopChnType == 0  || pVopChn->m_nVopChnType == 1 || pVopChn->m_nVopChnType == 2)
  {
    //是IVR語音資源，則直接應答
    if (pParser->isLogin(1) == true && CTILinkClientId != 0)
    {
      POSTACM(ChType, ChIndex, 0);
      POSTACK(ChType, ChIndex, 0);
    }
    return;
  }
  else if (pVopChn->m_nVopChnType == 3 || pVopChn->m_nVopChnType == 4 || pVopChn->m_nVopChnType == 5)
  {
    if (g_nSwitchType == PORT_NOCTI_PBX_TYPE && pBoxchn->isnChnAvail(nChn))
    {
      pAgent = GetAgentBynChn(nChn);
      if (CallParam == 256)
      {
        pChn->timer = 0;
        pChn->DtmfPauseTimer = 0;
        pChn->hwState = CHN_HW_VALID;
        pChn->CallInOut = CALL_OUT;
        pChn->CallInId = 1; 
        pChn->lnState = CHN_LN_SEIZE;
        pChn->ssState = CHN_SNG_OT_RING;
        pChn->CallTime = time(0);
        pChn->RecvDialDtmfId = 0;
        pChn->WaitDialTimer = 0;

        strcpy(pChn->CustPhone, Called);
        strcpy(pChn->CalledNo, Called);

        strcpy(pChn->CallerNo, pChn->DeviceID); //2015-10-27直接為設備號碼
        //if (ChIndex < MAX_CHANNEL_REC_NUM)
        //  strcpy(pChn->CallerNo, pIVRCfg->RecLineCode[pChn->ChStyleIndex]);
        
        if (pAgent != NULL)
        {
          pChn->GenerateCdrSerialNo();
          DBInsertCallCDR(nChn, 1, 1);
          strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
          DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);

          pAgent->SetAgentsvState(AG_SV_OUTSEIZE);
          WriteSeatStatus(pAgent, AG_STATUS_OUTSEIZE, 0, 2, 0);
          DispAgentStatus(pAgent->nAG);
        }
      }
      else
      {
        pChn->timer = 0;
        pChn->DtmfPauseTimer = 0;
        pChn->hwState = CHN_HW_VALID;
        pChn->CallInOut = CALL_IN;
        pChn->CallInId = 1; //有呼叫進入
        pChn->lnState = CHN_LN_SEIZE; //占用
        pChn->ssState = CHN_SNG_IN_RING;
        pChn->CallTime = time(0);

        strcpy(pChn->CallerNo, Caller);
        strcpy(pChn->CustPhone, Caller);
        strcpy(pChn->CalledNo, pChn->DeviceID); //2015-10-27直接為設備號碼
        //if (pChn->ChStyleIndex < MAX_CHANNEL_REC_NUM)
        //  strcpy(pChn->CalledNo, pIVRCfg->RecLineCode[pChn->ChStyleIndex]);

        
        if (pAgent != NULL)
        {
          if (pAgent->TranAGId == 0)
          {
            pChn->GenerateCdrSerialNo();
            DBInsertCallCDR(nChn, 1, 1);
            strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
            DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);

            if (pAgent->GetClientId() != 0x06FF)
            {
              sprintf(SendMsgBuf, "<onacdcallin acdid='%lu' sessionno='%ld' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='0' param='%s'/>", 
                pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CustPhone, pChn->CalledNo, 1, "");
              SendMsg2AG(pAgent->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
            }
            pAgent->SetAgentsvState(AG_SV_INRING);
            WriteSeatStatus(pAgent, AG_STATUS_INRING, 0, 1, 0);
            DispAgentStatus(pAgent->nAG);
          }
        }
      }
      DispChnStatus(nChn);
    }
    return;
  }
  else if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
  {
    strcpy(pVopChn->m_szCallerNo, Caller);
    if (pIVRCfg->isAutoDelMobilePreCode0 == true)
    {
      if (strncmp(pVopChn->m_szCallerNo, "013", 3) == 0 
        || strncmp(pVopChn->m_szCallerNo, "015", 3) == 0 
        || strncmp(pVopChn->m_szCallerNo, "018", 3) == 0)
      {
        char szTemp[MAX_TELECODE_LEN];
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pVopChn->m_szCallerNo[1], MAX_TELECODE_LEN-2);
        memcpy(pVopChn->m_szCallerNo, szTemp, MAX_TELECODE_LEN);
      }
    }
    strcpy(pVopChn->m_szCalledNo, Called);
    if (pBoxchn->isnChnAvail(nChn))
    {
      strcpy(pChn->CallerNo, pVopChn->m_szCallerNo);
    }
    return;
  }

  if (pVopChn->m_nVopChnType != 8) //不是語音中繼
    return;

  if (!pBoxchn->isnChnAvail(nChn))
    return;

  //保存號碼
  if (pChn->VOC_TrunkRecvCallEvent == 0) //add by zgj 2011-11-30
  {
    strcpy(pChn->CallerNo, Caller);
    strcpy(pChn->CustPhone, Caller);
    if (pChn->ChStyle == CH_A_SEAT || pChn->ChStyle == CH_A_EXTN)
    {
      //MyTrace(3, "PSTNChangeId=%d ChStyleIndex=%d PSTNCode=%s", 
      //  pIVRCfg->PSTNChangeId, pChn->ChStyleIndex, pIVRCfg->PSTNCode[pChn->ChStyleIndex]);
      if (pIVRCfg->PSTNChangeId == 1 && strlen(pIVRCfg->PSTNCode[pChn->ChStyleIndex]) > 0)
      {
        strcpy(pChn->CalledNo, pIVRCfg->PSTNCode[pChn->ChStyleIndex]);
      }
      else
      {
        strcpy(pChn->CalledNo, pIVRCfg->CenterCode);
      }
    }
    else
    {
      strcpy(pChn->CalledNo, Called);
    }
    pChn->OrgCalledNo = OrgCalled;
    
    if (pIVRCfg->isAutoDelMobilePreCode0 == true)
    {
      if (strncmp(pChn->CallerNo, "013", 3) == 0 || strncmp(pChn->CallerNo, "015", 3) == 0 || strncmp(pChn->CallerNo, "018", 3) == 0)
      {
        memset(szTemp, 0, MAX_TELECODE_LEN);
        strncpy(szTemp, &pChn->CallerNo[1], MAX_TELECODE_LEN-2);
        memcpy(pChn->CallerNo, szTemp, MAX_TELECODE_LEN);
      }
    }
  }
  else
  {
    if (strlen(pChn->CallerNo) == 0)
    {
      strcpy(pChn->CallerNo, Caller);
      strcpy(pChn->CustPhone, Caller);
    }
    if (strlen(pChn->CalledNo) == 0)
    {
      if (pChn->ChStyle == CH_A_SEAT || pChn->ChStyle == CH_A_EXTN)
      {
        if (pIVRCfg->PSTNChangeId == 1 && strlen(pIVRCfg->PSTNCode[pChn->lgChnNo]) > 0) //2015-11-25 針對語音中繼的修改
        {
          strcpy(pChn->CalledNo, pIVRCfg->PSTNCode[pChn->lgChnNo]); //2015-11-25 針對語音中繼的修改
        }
        else
        {
          strcpy(pChn->CalledNo, pIVRCfg->CenterCode);
        }
      }
      else
      {
        strcpy(pChn->CalledNo, Called);
      }
    }
    else
    {
      if (pIVRCfg->PSTNChangeId == 1 && strlen(pIVRCfg->PSTNCode[pChn->lgChnNo]) > 0) //2015-11-25 針對語音中繼的修改
      {
        strcpy(pChn->CalledNo, pIVRCfg->PSTNCode[pChn->lgChnNo]); //2015-11-25 針對語音中繼的修改
      }
    }
    if (pChn->OrgCalledNo.GetLength() == 0)
    {
      pChn->OrgCalledNo = OrgCalled;
    }
  }

  pBoxchn->pIdelTrkChnQueue->TrkNoDelFromQueue(pChn->lgChnNo); //2015-11-25 針對語音中繼的修改
  
  pChn->timer = 0;
  pChn->DtmfPauseTimer = 0;
  pChn->hwState = CHN_HW_VALID;
  pChn->CallInOut = CALL_IN;
  pChn->CallInId = 1; //有呼叫進入
  pChn->lnState = CHN_LN_SEIZE; //占用
  pChn->ssState = CHN_SNG_IN_RING;
  pChn->CallTime = time(0);
  
  POSTACM(ChType, ChIndex, 0);

  pChn->GenerateCdrSerialNo();
  SendCallinEvent(nChn, 1, 0, pChn->CallData.C_Str());
  DBInsertCallCDR(nChn, 2, 1);
  pChn->CalledPoint = strlen(pChn->CalledNo);

  DispChnStatus(nChn);
}

void OnFireRECVSAMForVOP(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called)
{
  
}

void OnFireRECVACMForVOP(short ChType, short ChIndex, short AcmParam)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv RECVACM from LOCAVOP ChType=%d ChIndex=%d AcmParam=%d",
    ChType, ChIndex, AcmParam);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  if (pVopChn->m_nVopChnType != 8) //不是語音中繼
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  
  int nQue, nOut, nAcd;
  
  nQue = pChn->nQue;
  if (pCalloutQue->isnQueAvail(nQue))
  {
    pQue->CalloutResult = CALLOUT_RING;
    if (pQue->OrgCallType == 1)
    {
      nOut = pQue->nOut;
      pOut->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
    }
    else
    {
      nAcd = pQue->nAcd;
      pAcd->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
    }
    
    pChn->ssState = CHN_SNG_OT_RING; //呼出振鈴
    DispChnStatus(nChn);
  }
}

void OnFireRECVACKForVOP(short ChType, short ChIndex, short AckParam)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv RECVACK from LOCAVOP ChType=%d ChIndex=%d AckParam=%d",
    ChType, ChIndex, AckParam);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (!pBoxchn->isnChnAvail(nChn))
    return;

  if (g_nSwitchType == PORT_NOCTI_PBX_TYPE && (pVopChn->m_nVopChnType == 3 || pVopChn->m_nVopChnType == 4 || pVopChn->m_nVopChnType == 5))
  {
    if (AckParam == 0) //這里以前是 if (AckParam == 2)
    {
      //監錄通道的來話已應答
      pChn->CanPlayOrRecId = 1;
      pChn->AnsTime = time(0);
      pChn->ssState = CHN_SNG_IN_TALK;
      DispChnStatus(nChn);

      CAgent *pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        if (pAgent->TranAGId == 0)
        {
          SendAnswerCallResult(pAgent->nAG);
          pAgent->SetAgentsvState(AG_SV_CONN);
          WriteSeatStatus(pAgent, AG_STATUS_TALK, 1, 1, 1);
          DBUpdateCallCDR_SeatAnsTime(pAgent->nAG, pAgent->m_Worker.AcdedGroupNo);
          DispAgentStatus(pAgent->nAG);
          
          if (pAgent->m_Worker.AutoRecordId == 1 && pAgent->m_Seat.AutoRecordId == 1 && pChn->curRecData.RecState == 0)
          {
            char szTemp[256], pszError[128];
            
            if (strlen(pChn->RecordFileName) == 0)
            {
              pChn->GenerateRecordFileName();  //edit by zgj 2011-07-22
            }
            strcpy(pAgent->RecordFileName, pChn->RecordFileName);
            
            sprintf(szTemp, "%s/%s", pIVRCfg->SeatRecPath.C_Str(), pAgent->RecordFileName);
            CStringX FilePath = AddRootPath_str(pIVRCfg->BoxRecPath, szTemp);
            
            if (CreateFullPathIfNotExist(FilePath.C_Str()) == 0)
            {
              if (SetChnRecordRule(pAgent->GetSeatnChn(), 0, FilePath.C_Str(), "", 0, 0, 1, pszError, 1) == 0)
              {
                DBUpdateCallCDR_RecdFile(pAgent->nAG);
              }
            }
          }
        }
        else
        {
          int nAcd = pChn->nAcd;
          if (pACDQueue->isnAcdAvail(nAcd))
          {
            MyTrace(3, "AgentChn=%d InChn=%d nAcd=%d Set CalloutResult=CALLOUT_CONN",
              nChn, pAcd->Session.nChn, nAcd);
            pAcd->Calleds[0].CalloutResult = CALLOUT_CONN;
            pChn->nAcd = 0xFFFF;
            StopTransfer(pAcd->Session.nChn, 1);
          }
          
          pChn->CanPlayOrRecId = 1;
          pChn->AnsTime = time(0);
          pChn->ssState = CHN_SNG_OT_TALK;
          DispChnStatus(nChn);
        }
      }
    }
    else
    {
      //已收完監錄通道呼出撥的號碼
      pChn->CanPlayOrRecId = 1;
      pChn->AnsTime = time(0);
      pChn->ssState = CHN_SNG_OT_TALK;
      DispChnStatus(nChn);
    }
    return;
  }

  if (pVopChn->m_nVopChnType != 8) //不是語音中繼
  {
    return;
  }
  
  if (AckParam == 9) //應答來話成功（針對發應答指令后，不是馬上返回結果的情況）
  {
    SendAnswerResult(nChn, OnSuccess, "");
    pChn->CanPlayOrRecId = 1;
    pChn->AnsTime = time(0);
    pChn->ssState = CHN_SNG_IN_TALK;
    DispChnStatus(nChn);
    return;
  }
  else if (AckParam == 10) //應答來話失敗（針對發應答指令后，不是馬上返回結果的情況）
  {
    SendAnswerResult(nChn, OnFail, "");
    return;
  }

  int nQue = pChn->nQue;
  if (pCalloutQue->isnQueAvail(nQue))
  {
    pQue->CalloutResult = CALLOUT_CONN;
    pChn->ssState = CHN_SNG_OT_TALK; //呼出振鈴
    pChn->CalloutId = 0;
    pChn->nQue = 0xFFFF;
    pChn->CanPlayOrRecId = 1; //允許開始放錄音
    pChn->AnsTime = time(0);
    DispChnStatus(nChn);
  }
}

void OnFireRELEASEForVOP(short ChType, short ChIndex, short ReleaseType, short ReleaseReason)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv RELEASE from LOCAVOP ChType=%d ChIndex=%d ReleaseType=%d ReleaseReason=%d",
    ChType, ChIndex, ReleaseType, ReleaseReason);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;

  if (pVopChn->m_nVopChnType != 8) //不是語音中繼
  {
    pVopChn->SetssState(0);
    if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
    {
      POSTSTOPRECORD(ChType, ChIndex, 0);
      DBUpdateCallCDR_RelTimeByRecCh(nChn);
    }
    pVopChn->SetsvState(0);
    memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);
    memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
    SendVopStateToAll(pVopChn->m_nVopNo, pVopChn->m_nVopChnNo);
    SendVOPStateToOppIVR(pVopChn->m_nVopNo, pVopChn->m_nVopChnNo);

    if (pBoxchn->isnChnAvail(nChn))
    {
      StopClearPlayDtmfRecBuf(nChn);
      
      if (g_nSwitchType == PORT_NOCTI_PBX_TYPE && pVopChn->m_nVopChnType == 3)
      {
        POSTSTOPRECORD(ChType, ChIndex, 0);
        
        CAgent *pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          pAgent->delaytimer = 0;
          if (pAgent->svState == AG_SV_CONN || pAgent->svState == AG_SV_HOLD)
          {
            DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
            pAgent->DelayState = 1;
            WriteSeatStatus(pAgent, AG_STATUS_ACW, 0);
          }
          else
          {
            DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
            DBUpdateCallCDR_ACWTime(pAgent->nAG);
            WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
          }
          pAgent->SetAgentsvState(AG_SV_IDEL);
          pAgent->ClearSrvLine();
          SendSeatHangon(pAgent->nAG);
          DispAgentStatus(pAgent->nAG);
        }
        DBUpdateCallCDR_RelTime(nChn);
        pBoxchn->ResetChn(nChn);
        DispChnStatus(nChn);
      }
      if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
      {
        pBoxchn->ResetChn(nChn);
        DispChnStatus(nChn);
      }
    }
    return;
  }
  if (!pBoxchn->isnChnAvail(nChn))
    return;

  int nQue;

  switch (ReleaseType)
  {
  case F_R_NORMAL: //0-正常釋放
  case F_R_CINFAIL: //2-呼入失敗釋放
    Release_Chn(nChn, 0, 1);
    break;
    
  case F_R_COUTFAIL: //1-呼出失敗釋放
    if (ReleaseReason == P_R_NORMAL)
    {
      UnLink_All_By_ChnNo(nChn, 1);
      DBUpdateCallCDR_UpdateFlag(nChn);
      pBoxchn->ResetChn(nChn);
      DispChnStatus(nChn);
      break;
    }
    UnLink_All_By_ChnNo(nChn, 1);
    nQue = pChn->nQue;
    if (!pCalloutQue->isnQueAvail(nQue))
    {
      break;
    }
    switch (ReleaseReason)
    {
    case P_R_NORMAL: //正常釋放
      pChn->lnState = CHN_LN_RELEASE; //設置線路釋放標志
      break;
      
    case P_R_COUTNODIALTONE: //1-呼出沒有檢測到撥號音 （只用于外線TB）
      pQue->CalloutResult = CALLOUT_FAIL;
      break;
      
    case P_R_COUTLINEBUSY: //2-呼出線路忙
    case P_R_COUTBUSYLOCAL: //19-市話忙 slb
    case P_R_BUSYUSER: //21-用戶忙 ssb
      pQue->CalloutResult = CALLOUT_SLB;
      break;
    case P_R_COUTBUSYDDD: //20-長話忙 stb
      pQue->CalloutResult = CALLOUT_SLB;
      break;
      
    case P_R_COUTNOSOUND: //3-呼出后沒有聲音（只用于外線TB）
      pQue->CalloutResult = CALLOUT_FAIL;
      break;
      
    case P_R_COUTRINGTIMEOUT: //4-呼出振鈴超時 （對方無人接）
      pQue->CalloutResult = CALLOUT_NOBODY;
      break;
      
    case P_R_COUTNULLDN: //22-空號
      pQue->CalloutResult = CALLOUT_UNN;
      break;
      
    default:
      pQue->CalloutResult = CALLOUT_FAIL;
      break;
    }
    break;
  }
  DispChnStatus(nChn);
}

void OnFirePLAYRESULTForVOP(short ChType, short ChIndex, short Result)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    MyTrace(3, "Recv PLAYRESULT from LOCAVOP ChType=%d ChIndex=%d Result=%d but pVopChn=NULL",
      ChType, ChIndex, Result);
    return;
  }
  pVopChn->SetsvState(0);
  //memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
  //memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    MyTrace(3, "Recv PLAYRESULT from LOCAVOP ChType=%d ChIndex=%d Result=%d nChn=%d state=%d curPlayRuleId=%d PlayState=%d curPlayState=%d PlayTypeId=%d",
      ChType, ChIndex, Result, nChn,
      pChn->curPlayData.state, pChn->curPlayRuleId, pChn->curPlayData.PlayState, pChn->curPlayState, pChn->curPlayData.PlayTypeId == 4);
    pChn->curPlayState = 2; //自然結束停止
  }
  SendVopStateToAll(VOP_LOCA_VOPNO, ChIndex);
  SendVOPStateToOppIVR(VOP_LOCA_VOPNO, ChIndex);
}

void OnFireRECORDRESULTForVOP(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen)
{
  int nChn, nChn1;
  CVopChn *pVopChn=NULL;
  
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    MyTrace(3, "Recv RECORDRESULT from LOCAVOP ChType=%d ChIndex=%d Result=%d RecdSize=%d RecdLen=%d but pVopChn=NULL",
      ChType, ChIndex, Result, RecdSize, RecdLen);
    return;
  }
  pVopChn->SetsvState(0);
  //memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
  //memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
    {
      nChn1 = pChn->nSwtPortID;
      if (pBoxchn->isnChnAvail(nChn1))
      {
        pChn1->curRecState = 5; //錄音結束
      }
      StopClearRecDtmfBuf(nChn);
    } 
    else
    {
      MyTrace(3, "Recv RECORDRESULT from LOCAVOP ChType=%d ChIndex=%d Result=%d RecdSize=%d RecdLen=%d nChn=%d curRecData.state=%d curRecData.RecState=%d",
        ChType, ChIndex, Result, RecdSize, RecdLen, nChn, pChn->curRecData.state, pChn->curRecData.RecState);
      pChn->curRecState = 5; //錄音結束
    }
  }
  SendVopStateToAll(VOP_LOCA_VOPNO, ChIndex);
  SendVOPStateToOppIVR(VOP_LOCA_VOPNO, ChIndex);
}

void OnFireRECVDTMFForVOP(short ChType, short ChIndex, LPCTSTR Dtmfs)
{
  int nChn, len, len1, len2;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv DTMF from LOCAVOP ChType=%d ChIndex=%d Dtmfs=%s",
    ChType, ChIndex, Dtmfs);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    MyTrace(3, "Recv DTMF from LOCAVOP ChType=%d ChIndex=%d Dtmfs=%s but pVopChn=NULL",
      ChType, ChIndex, Dtmfs);
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    len1 = strlen(pChn->curRecvDtmf);
    len2 = strlen(Dtmfs);
    if ((len1+len2) >= MAX_DTMFBUF_LEN)
    {
      len = len1+len2-MAX_DTMFBUF_LEN+1;
      memcpy(pChn->curRecvDtmf, &pChn->curRecvDtmf[len], MAX_DTMFBUF_LEN-len);
      pChn->curRecvDtmf[MAX_DTMFBUF_LEN-len-1] = 0;
      strncat(pChn->curRecvDtmf, Dtmfs, len2);
      pChn->curRecvDtmf[MAX_DTMFBUF_LEN] = 0;
    }
    else
    {
      strncat(pChn->curRecvDtmf, Dtmfs, len2);
    }

    if (g_nSwitchType != PORT_NOCTI_PBX_TYPE)
      return;
    if (pChn->RecvDialDtmfId == 1)
    {
      return;
    }
    if (len2 > 0 && pVopChn->m_nVopChnType == 3)
    {
      if (pChn->WaitDialTimer > 8)
      {
        CAgent *pAgent=NULL;
        pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          if (pAgent->GetClientId() != 0x06FF)
          {
            sprintf(SendMsgBuf, "<onmakecall acdid='%lu' sessionno='%ld' chantype='%d' channo='%d' callerno='%s' calledno='%s' param='%s' result='%d' errorbuf='%s'/>", 
              pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CallerNo, pChn->CustPhone, "", OnOutAnswer, "");
            SendMsg2AG(pAgent->GetClientId(), AGMSG_onmakecall, SendMsgBuf);
          }
          if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
          {
            sprintf(SendMsgBuf, "wsclientid=%s;cmd=dialoutresult;seatno=%s;result=%d;callerno=%s;calledno=%s;", 
              pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), OnOutAnswer, pChn->CallerNo, pChn->CustPhone);
            SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
          }
        }
        pChn->RecvDialDtmfId = 1;
        return;
      }
      if ((strlen(pChn->RecvDialDtmf) + len2) < MAX_DTMFBUF_LEN)
      {
        strcat(pChn->RecvDialDtmf, Dtmfs);
        //SendSeatDialDTMFEvent(nChn, pChn->RecvDialDtmf);
        strncpy(pChn->CalledNo, pChn->RecvDialDtmf, MAX_TELECODE_LEN-1);
        DispChnStatus(nChn);
      }
      else
      {
        pChn->RecvDialDtmfId = 1; //2015-10-27
      }
      pChn->WaitDialTimer = 0;
      
      len = strlen(pIVRCfg->SWTDialOutPreCode);
      if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->SWTDialOutPreCode, len) == 0)
      {
        strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
      }
      else
      {
        strcpy(pChn->CustPhone, pChn->CalledNo);
      }
      len = strlen(pChn->CustPhone);
      if (len > 1)
      {
        if (pChn->CustPhone[len-1] == '#')
        {
          pChn->CustPhone[len-1] = '\0';
        }
      }
      DBUpdateCallCDR_Phone(nChn);
    }
  }
  else
  {
    MyTrace(3, "Recv DTMF from LOCAVOP ChType=%d ChIndex=%d Dtmfs=%s nChn is not Avail",
      ChType, ChIndex, Dtmfs);
  }
}

void OnFireCONFPLAYRESULTForVOP(short ConfNo, short Result)
{
  
}

void OnFireCONFRECORDRESULTForVOP(short ConfNo, short Result, long RecdSize, long RecdLen)
{
  
}

void OnFireSENDFAXRESULTForVOP(short ChType, short ChIndex, short FaxChIndex, short Result, short SendPages, long SendBytes, long SendSpeed, LPCTSTR ErrorMsg)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv Send Fax Result ChType=%d ChIndex=%d FaxChIndex=%d Result=%d SendPages=%d SendBytes=%ld SendSpeed=%d ErrorMsg=%s", 
    ChType, ChIndex, FaxChIndex, Result, SendPages, SendBytes, SendSpeed, ErrorMsg);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    pChn->LinkType[0] = 0;
    if (Result == 0)
      SendSendfaxResult(nChn, OnsFaxend, ErrorMsg, SendPages, SendBytes, SendSpeed);
    else if (Result == 9)
      SendSendfaxResult(nChn, OnsFaxing, ErrorMsg);
    else
      SendSendfaxResult(nChn, OnsFaxFail, ErrorMsg);
  }
}

void OnFireRECVFAXRESULTForVOP(short ChType, short ChIndex, short FaxChIndex, short Result, LPCTSTR FaxCSID, LPCTSTR BarCode, short RecvPages, long RecvBytes, long RecvSpeed, LPCTSTR ErrorMsg)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv Recv Fax Result ChType=%d ChIndex=%d FaxChIndex=%d Result=%d FaxCSID=%s BarCode=%s RecvPages=%d RecvBytes=%ld RecvSpeed=%ld ErrorMsg=%s", 
    ChType, ChIndex, FaxChIndex, Result, FaxCSID, BarCode, RecvPages, RecvBytes, RecvSpeed, ErrorMsg);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    pChn->LinkType[0] = 0;
    if (Result == 0)
      SendRecvfaxResult(nChn, OnrFaxend, ErrorMsg, FaxCSID, BarCode, RecvPages, RecvBytes, RecvSpeed);
    else if (Result == 9)
      SendRecvfaxResult(nChn, OnrFaxing, ErrorMsg);
    else
      SendRecvfaxResult(nChn, OnrFaxFail, ErrorMsg);
  }
}

void OnFireFLASHForVOP(short ChType, short ChIndex)
{
  
}

void OnFireLINEALARMForVOP(short ChType, short ChIndex, short AlarmType, short AlarmParam)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nBoxChn;
  if (AlarmParam == 0)
  {
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->hwState = 0;
      DispChnStatus(nChn);
    }
    if ((pVopChn->m_nLnState&1) == 1)
    {
      MyTrace(3, "Recv LINEALARM from LOCAVOP ChType=%d ChIndex=%d AlarmType=%d AlarmParam=%d",
                  ChType, ChIndex, AlarmType, AlarmParam);
      if (pBoxchn->isnChnAvail(nChn))
        WriteAlarmList(1022, ALARM_LEVEL_MINOR, 0, "DeviceId=%s VocResource action", pChn->DeviceID);
    }
    pVopChn->m_nLnState &= 2;
    if (pVopChn->m_nVopChnType == 0 || pVopChn->m_nVopChnType == 1)
    {
      ClearIVRChBlockFlag();
      g_dwRegServerState = g_dwRegServerState & 0x7F;
      if (pVopGroup->IsAllIVRChnAct() == true)
      {
        g_dwRegServerState = g_dwRegServerState & 0xEFF;
      }
      WriteRegServiceRunState();
    }
    else
    {
      if (pVopGroup->IsAllRECChnAct() == true)
      {
        g_dwRegServerState = g_dwRegServerState & 0xDFF;
        WriteRegServiceRunState();
      }
    }
    SendVopStateToAll(VOP_LOCA_VOPNO, ChIndex);
    SendVOPStateToOppIVR(VOP_LOCA_VOPNO, ChIndex);
  } 
  else
  {
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->hwState = 1;
      DispChnStatus(nChn);
    }
    if (pVopChn->m_nLnState == 0)
    {
      MyTrace(3, "Recv LINEALARM from LOCAVOP ChType=%d ChIndex=%d AlarmType=%d AlarmParam=%d",
                  ChType, ChIndex, AlarmType, AlarmParam);
      if (pBoxchn->isnChnAvail(nChn))
        WriteAlarmList(1021, ALARM_LEVEL_MAJOR, 1, "DeviceId=%s VocResource block", pChn->DeviceID);
    }
    pVopChn->m_nLnState |= 1;
    if (pVopChn->m_nVopChnType == 0 || pVopChn->m_nVopChnType == 1)
    {
      g_dwRegServerState = g_dwRegServerState | 0x100;
      if (pVopGroup->IsAllIVRChnBlock() == true)
      {
        SetIVRChBlockFlag();
        g_dwRegServerState = g_dwRegServerState | 0x80;
      }
      WriteRegServiceRunState();
    }
    else
    {
      g_dwRegServerState = g_dwRegServerState | 0x200;
      WriteRegServiceRunState();
    }
    SendVopStateToAll(VOP_LOCA_VOPNO, ChIndex);
    SendVOPStateToOppIVR(VOP_LOCA_VOPNO, ChIndex);
  }
}

void OnFireRECORDMEMRESULTForVOP(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize)
{
  
}

void OnFireCONFCREATERESULTForVOP(short ChType, short ChIndex, short ConfNo, short CreateId, short Result)
{
  
}

void OnFireCONFDESTROYRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result)
{
  
}

void OnFireCONFJOINRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result)
{
  
}

void OnFireCONFLISTENRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result)
{
  
}

void OnFireCONFUNJOINRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result)
{
  
}

void OnFireCONFUNLISTENRESULTForVOP(short ChType, short ChIndex, short ConfNo, short Result)
{
  
}

void OnFireCHECKTONERESULTForVOP(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result)
{
  
}

void OnFireSENDFSKRESULTForVOP(short ChType, short ChIndex, short FskChIndex, short Result, short Param)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv SENDFSKRESULT from LOCAVOP ChType=%d ChIndex=%d FskChIndex=%d Result=%d", 
    ChType, ChIndex, FskChIndex, Result);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (Result == 0)
      SendSendFSKResult(nChn, OnSuccess, "");
    else
      SendSendFSKResult(nChn, OnFail, "send fsk fail");
  }
}

void OnFireRECVFSKForVOP(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv RECVFSK from LOCAVOP ChType=%d ChIndex=%d FskChIndex=%d FSKCMD=0x%02x FSKLen=%d Result=%d", 
    ChType, ChIndex, FskChIndex, FSKCMD, FSKLen, Result);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (Result == 0)
      SendRecvdFSKResult(nChn, FSKCMD, (char *)FSKStrAddr, FSKLen, OnSuccess, ""); //(char *)FSKStrAddr
    else
      SendRecvdFSKResult(nChn, 0, "", 0, OnFail, "recv fsk fail");
  }
}

void OnFireFAXSTATEForVOP(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FaxState)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (!pBoxchn->isnChnAvail(nChn))
  {
    return;
  }
  pChn->FlwState = FaxState;
  DispChnStatus(nChn);
}
void OnFireTRANSFERRESULTForVOP(short ChType, short ChIndex, short Result, LPCTSTR Reason)
{
  int nChn;
  CVopChn *pVopChn=NULL;
  
  MyTrace(3, "Recv TRANSFERRESULT from LOCAVOP ChType=%d ChIndex=%d Result=%d Reason=%s",
    ChType, ChIndex, Result, Reason);
  if (pVopGroup == NULL)
  {
    return;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(VOP_LOCA_VOPNO, ChIndex);
  if (pVopChn == NULL)
  {
    return;
  }
  if (pVopChn->m_nVopChnType != 8) //不是語音中繼
  {
    return;
  }
  nChn = pVopChn->m_nSeizeChn;
  if (!pBoxchn->isnChnAvail(nChn))
    return;

  int nAcd = pChn->nAcd;
  if (pACDQueue->isnAcdAvail(nAcd))
  {
    if (Result == 0)
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_CONN;
      pChn->nAcd = 0xFFFF;
    }
    else if (Result == 1)
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_FAIL;
      pChn->nAcd = 0xFFFF;
    }
    else if (Result == 2)
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_NOBODY;
      pChn->nAcd = 0xFFFF;
    }
    else if (Result == 3)
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_SLB;
      pChn->nAcd = 0xFFFF;
    }
    else if (Result == 4)
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_UNN;
      pChn->nAcd = 0xFFFF;
    }
    else if (Result == 9)
    {
      pAcd->Calleds[0].CalloutResult = CALLOUT_RING;
    }
  }
  else
  {
    if (Result == 0)
    {
      if (pChn->TranIVRId == 3)
      {
        SendTransferResult1(nChn, 0xFFFF, OnOutAnswer, "");
        pChn->TranIVRId = 0;
        DBUpdateCallCDR_RelReason(nChn, 2);
      }
    }
    else if (Result == 1)
    {
      if (pChn->TranIVRId == 3)
      {
        SendTransferResult1(nChn, 0xFFFF, OnOutFail, "");
        pChn->TranIVRId = 0;
      }
    }
    else if (Result == 2)
    {
      if (pChn->TranIVRId == 9)
        SendTransferResult1(nChn, 0xFFFF, OnOutTimeout, "");
    }
    else if (Result == 3)
    {
      if (pChn->TranIVRId == 9)
        SendTransferResult1(nChn, 0xFFFF, OnOutBusy, "");
    }
    else if (Result == 4)
    {
      if (pChn->TranIVRId == 9)
        SendTransferResult1(nChn, 0xFFFF, OnOutUNN, "");
    }
    else if (Result == 9)
    {
      if (pChn->TranIVRId == 3)
        SendTransferResult1(nChn, 0xFFFF, OnOutRing, "");
    }
  }
}
void OnFireSTOPTRANSFERRESULTForVOP(short ChType, short ChIndex, short Result)
{

}

//設置所有事件回調函數
void SetAllCallBackFuncForVOP()
{
  SetFuncOnFireCALLIN(OnFireCALLINForVOP);
  SetFuncOnFireRECVSAM(OnFireRECVSAMForVOP);
  SetFuncOnFireRECVACM(OnFireRECVACMForVOP);
  SetFuncOnFireRECVACK(OnFireRECVACKForVOP);
  SetFuncOnFireRELEASE(OnFireRELEASEForVOP);
  SetFuncOnFirePLAYRESULT(OnFirePLAYRESULTForVOP);
  SetFuncOnFireRECORDRESULT(OnFireRECORDRESULTForVOP);
  SetFuncOnFireRECVDTMF(OnFireRECVDTMFForVOP);
  SetFuncOnFireCONFPLAYRESULT(OnFireCONFPLAYRESULTForVOP);
  SetFuncOnFireCONFRECORDRESULT(OnFireCONFRECORDRESULTForVOP);
  SetFuncOnFireSENDFAXRESULT(OnFireSENDFAXRESULTForVOP);
  SetFuncOnFireRECVFAXRESULT(OnFireRECVFAXRESULTForVOP);
  SetFuncOnFireFLASH(OnFireFLASHForVOP);
  SetFuncOnFireLINEALARM(OnFireLINEALARMForVOP);
  SetFuncOnFireRECORDMEMRESULT(OnFireRECORDMEMRESULTForVOP);
  SetFuncOnFireCONFCREATERESULT(OnFireCONFCREATERESULTForVOP);
  SetFuncOnFireCONFDESTROYRESULT(OnFireCONFDESTROYRESULTForVOP);
  SetFuncOnFireCONFJOINRESULT(OnFireCONFJOINRESULTForVOP);
  SetFuncOnFireCONFLISTENRESULT(OnFireCONFLISTENRESULTForVOP);
  SetFuncOnFireCONFUNJOINRESULT(OnFireCONFUNJOINRESULTForVOP);
  SetFuncOnFireCONFUNLISTENRESULT(OnFireCONFUNLISTENRESULTForVOP);
  SetFuncOnFireCHECKTONERESULT(OnFireCHECKTONERESULTForVOP);
  SetFuncOnFireSENDFSKRESULT(OnFireSENDFSKRESULTForVOP);
  SetFuncOnFireRECVFSK(OnFireRECVFSKForVOP);
  SetFuncOnFireFAXSTATE(OnFireFAXSTATEForVOP);
  SetFuncOnFireTRANSFERRESULT(OnFireTRANSFERRESULTForVOP);
  SetFuncOnFireSTOPTRANSFERRESULT(OnFireSTOPTRANSFERRESULTForVOP);
}
