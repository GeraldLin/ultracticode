//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procvop.h"
#include "extern.h"

//---------------------------------------------------------------------------
//處理坐席消息
//---------------------------------------------------------------------------
//發送消息到坐席
void SendMsg2VOP(US ClientId, US MsgId, const CH *msg)
{
  pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRVOP<<8) | MsgId, msg);
  if (MsgId != 1)
    MyTrace(2, "0x%04x %s", ClientId, msg);
}
void SendMsg2VOP(US ClientId, US MsgId, const CStringX &msg)
{
	SendMsg2VOP(ClientId, MsgId, msg.C_Str());
}
void SendMsg2IPREC(US ClientId, US MsgId, const CH *msg)
{
  pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRIPREC<<8) | MsgId, msg);
  MyTrace(2, "0x%04x %s", ClientId, msg);
}
//分析坐席來的消息
int Proc_Msg_From_VOP(CXMLRcvMsg &VOPMsg)
{
	unsigned short MsgId=VOPMsg.GetMsgId();

  if(MsgId >= MAX_VOPIVRMSG_NUM)	//ag-->ivr
  {
    //指令編號超出范圍
    MyTrace(0, "VOPmsgid = %d is out of range\n", MsgId);
    return 1;
  }

  if(0!=VOPMsg.ParseRevMsgWithCheck(pFlwRule->VOPIVRMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(0, "RECV MSG ERROR: MsgId = %d MsgBuf = %s\n", MsgId, VOPMsg.GetBuf().C_Str());
    return 2;
  }
  return 0;
}
//取AG消息規定頭
void Get_VOPMSG_HeaderValue(CXMLRcvMsg &VOPMsg)
{
}
//-----------------------------------------------------------------------------
int Proc_VOPMSG_onlogin(CXMLRcvMsg &VOPMsg)	//語音節點資源注冊
{
  short vopno = atoi(VOPMsg.GetAttrValue(1).C_Str());
  if (g_nCardType == 9)
  {
    sprintf(SendMsgBuf, "<loginresult sessionid='0' vopno='%d' result='0' hosttype='%d' activeid='%d'/>",
      vopno, g_nHostType, (g_nHostType==0) ? g_nMasterStatus : g_nStandbyStatus);
    SendMsg2VOP(TestPhoneClientId, VOPMSG_loginresult, SendMsgBuf);

    for (int i = 0; i < MAX_MEM_PLAY_LIST_NUM; i ++)
    {
      if (pMemvoc->Index[i].state == 1)
      {
        sprintf(SendMsgBuf, "<addvocindex sessionid='0' vopno='1' vocindex='%d' filename='%s'/>",
          i, pMemvoc->Index[i].FileName.C_Str());
        SendMsg2VOP(TestPhoneClientId, VOPMSG_addvocindex, SendMsgBuf);
      }
    }
    return 0;
  }
  sprintf(SendMsgBuf, "<loginresult sessionid='0' vopno='%d' result='0' hosttype='%d' activeid='%d'/>",
    vopno, g_nHostType, (g_nHostType==0) ? g_nMasterStatus : g_nStandbyStatus);
  SendMsg2VOP(VOPMsg.GetClientId(), VOPMSG_loginresult, SendMsgBuf);

  SendRunStatusToClient(VOPMsg.GetClientId(), 0x0117);
  Send_SWTMSG_sendivrstatus(vopno, 0);
  return 0;
}
int Proc_VOPMSG_onlinealarm(CXMLRcvMsg &VOPMsg)	//收到線路告警消息
{
  short vopno = atoi(VOPMsg.GetAttrValue(1).C_Str());
  short vopchnno = atoi(VOPMsg.GetAttrValue(2).C_Str());
  short alarmtype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short alarmparam = atoi(VOPMsg.GetAttrValue(4).C_Str());

  CVopChn *pVopChn=NULL;
  if (pVopGroup == NULL)
  {
    return 1;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
  if (pVopChn)
  {
    if (alarmparam == 0) //bit0: 0-正常 1-阻塞 bit1: 0-打開 1-關閉
    {
      //正常
      pVopChn->m_nLnState &= 2;
      if (pVopChn->m_nVopChnType == 0 || pVopChn->m_nVopChnType == 1)
      {
        ClearIVRChBlockFlag();
        g_dwRegServerState = g_dwRegServerState & 0x7F;
        if (pVopGroup->IsAllIVRChnAct() == true)
        {
          g_dwRegServerState = g_dwRegServerState & 0xEFF;
        }
        WriteRegServiceRunState();
      }
      else
      {
        if (pVopGroup->IsAllRECChnAct() == true)
        {
          g_dwRegServerState = g_dwRegServerState & 0xDFF;
          WriteRegServiceRunState();
        }
      }
      WriteAlarmList(1022, ALARM_LEVEL_MINOR, 0, "DeviceId=%s VocResource action", pVopChn->m_szDeviceID);
    }
    else
    {
      //斷線
      pVopChn->m_nLnState |= 1;
      if (pVopChn->m_nVopChnType == 0 || pVopChn->m_nVopChnType == 1)
      {
        g_dwRegServerState = g_dwRegServerState | 0x100;
        if (pVopGroup->IsAllIVRChnBlock() == true)
        {
          SetIVRChBlockFlag();
          g_dwRegServerState = g_dwRegServerState | 0x80;
        }
        WriteRegServiceRunState();
      }
      else
      {
        g_dwRegServerState = g_dwRegServerState | 0x200;
        WriteRegServiceRunState();
      }
      WriteAlarmList(1021, ALARM_LEVEL_MAJOR, 1, "DeviceId=%s VocResource block", pVopChn->m_szDeviceID);
    }
    SendVopStateToAll(vopno, vopchnno);
    SendVOPStateToOppIVR(vopno, vopchnno);
  }
  return 0;
}
int Proc_VOPMSG_onplayresult(CXMLRcvMsg &VOPMsg)	//放音結果消息
{
  short vopno = atoi(VOPMsg.GetAttrValue(1).C_Str());
  short vopchnno = atoi(VOPMsg.GetAttrValue(2).C_Str());
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  short playresult = atoi(VOPMsg.GetAttrValue(5).C_Str());
  
  if (g_nCardType == 9)
  {
    int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->curPlayState = 2;
    }
    return 0;
  }

  CVopChn *pVopChn=NULL;
  if (pVopGroup == NULL)
  {
    return 1;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
  
  int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (playresult == 10)
      pChn->curPlayState = 10; //放音錯誤
    else
      pChn->curPlayState = 2; //結束停止
  }
  if (pVopChn)
  {
    pVopChn->SetsvState(0);
    //memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
    //memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
    SendVopStateToAll(vopno, vopchnno);
    SendVOPStateToOppIVR(vopno, vopchnno);
  }

  return 0;
}
int Proc_VOPMSG_onrecordresult(CXMLRcvMsg &VOPMsg)	//錄音結果消息
{
  short vopno = atoi(VOPMsg.GetAttrValue(1).C_Str());
  short vopchnno = atoi(VOPMsg.GetAttrValue(2).C_Str());
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  short recresult = atoi(VOPMsg.GetAttrValue(5).C_Str());
  long recbytes = atoi(VOPMsg.GetAttrValue(7).C_Str());
  long rectimelen = atoi(VOPMsg.GetAttrValue(8).C_Str());
  
  if (g_nCardType == 9)
  {
    int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      if (recresult == 10)
        pChn->curRecState = 10;
      else
        pChn->curRecState = 5;
    }
    return 0;
  }

  CVopChn *pVopChn=NULL;
  if (pVopGroup == NULL)
  {
    return 1;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
  
  int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
    {
      int nChn1 = pChn->nSwtPortID;
      if (pBoxchn->isnChnAvail(nChn1))
      {
        if (recresult == 10)
          pChn1->curRecState = 10;
        else
          pChn1->curRecState = 5;
      }
      StopClearRecDtmfBuf(nChn);
    } 
    else
    {
      if (recresult == 10)
        pChn->curRecState = 10;
      else
        pChn->curRecState = 5;
    }
  }
  if (pVopChn)
  {
    //memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);//2012-08-27 屏蔽掉 只有在外線釋放時清除
    //memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
    pVopChn->SetsvState(0);
    SendVopStateToAll(vopno, vopchnno);
    SendVOPStateToOppIVR(vopno, vopchnno);
  }
  
  return 0;
}
int Proc_VOPMSG_onrecvdtmf(CXMLRcvMsg &VOPMsg)	//收到DTMF消息
{
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  
  if (g_nCardType == 9)
  {
    int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      int len1 = strlen(pChn->curRecvDtmf);
      int len2 = strlen(VOPMsg.GetAttrValue(5).C_Str());
      if ((len1+len2) >= MAX_DTMFBUF_LEN)
      {
        int len = len1+len2-MAX_DTMFBUF_LEN+1;
        memcpy(pChn->curRecvDtmf, &pChn->curRecvDtmf[len], MAX_DTMFBUF_LEN-len);
        pChn->curRecvDtmf[MAX_DTMFBUF_LEN-len-1] = 0;
        strncat(pChn->curRecvDtmf, VOPMsg.GetAttrValue(5).C_Str(), len2);
        pChn->curRecvDtmf[MAX_DTMFBUF_LEN] = 0;
      }
      else
      {
        strncat(pChn->curRecvDtmf, VOPMsg.GetAttrValue(5).C_Str(), len2);
      }
    }
    return 0;
  }

  if (pVopGroup == NULL)
  {
    return 1;
  }

  int len, len1, len2;
  int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
  if (pBoxchn->isnChnAvail(nChn))
  {
    len1 = strlen(pChn->curRecvDtmf);
    len2 = strlen(VOPMsg.GetAttrValue(5).C_Str());
    if ((len1+len2) >= MAX_DTMFBUF_LEN)
    {
      len = len1+len2-MAX_DTMFBUF_LEN+1;
      memcpy(pChn->curRecvDtmf, &pChn->curRecvDtmf[len], MAX_DTMFBUF_LEN-len);
      pChn->curRecvDtmf[MAX_DTMFBUF_LEN-len-1] = 0;
      strncat(pChn->curRecvDtmf, VOPMsg.GetAttrValue(5).C_Str(), len2);
      pChn->curRecvDtmf[MAX_DTMFBUF_LEN] = 0;
    }
    else
    {
      strncat(pChn->curRecvDtmf, VOPMsg.GetAttrValue(5).C_Str(), len2);
    }
  }
  
  return 0;
}
int Proc_VOPMSG_onsendfaxresult(CXMLRcvMsg &VOPMsg)	//發送傳真結果消息
{
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  short Result = atoi(VOPMsg.GetAttrValue(5).C_Str());
  
  int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (Result == 0)
      SendSendfaxResult(nChn, OnsFaxend, VOPMsg.GetAttrValue(9).C_Str(), 
        atoi(VOPMsg.GetAttrValue(6).C_Str()), 
        atoi(VOPMsg.GetAttrValue(7).C_Str()), 
        atoi(VOPMsg.GetAttrValue(8).C_Str()));
    else if (Result == 9)
      SendSendfaxResult(nChn, OnsFaxing, VOPMsg.GetAttrValue(9).C_Str());
    else
      SendSendfaxResult(nChn, OnsFaxFail, VOPMsg.GetAttrValue(9).C_Str());
  }

  return 0;
}
int Proc_VOPMSG_onrecvfaxresult(CXMLRcvMsg &VOPMsg)	//接收傳真結果消息
{
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  short Result = atoi(VOPMsg.GetAttrValue(5).C_Str());
  
  int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (Result == 0)
      SendRecvfaxResult(nChn, OnrFaxend, VOPMsg.GetAttrValue(10).C_Str(), 
        VOPMsg.GetAttrValue(9).C_Str(), "", 
        atoi(VOPMsg.GetAttrValue(6).C_Str()), 
        atoi(VOPMsg.GetAttrValue(7).C_Str()), 
        atoi(VOPMsg.GetAttrValue(8).C_Str()));
    else if (Result == 9)
      SendRecvfaxResult(nChn, OnrFaxing, VOPMsg.GetAttrValue(10).C_Str());
    else
      SendRecvfaxResult(nChn, OnrFaxFail, VOPMsg.GetAttrValue(10).C_Str());
  }

  return 0;
}
int Proc_VOPMSG_onrelease(CXMLRcvMsg &VOPMsg)	//通道釋放消息
{
  short vopno = atoi(VOPMsg.GetAttrValue(1).C_Str());
  short vopchnno = atoi(VOPMsg.GetAttrValue(2).C_Str());
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());

  if (g_nCardType == 9)
  {
    int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      pChn->lnState = CHN_LN_RELEASE;
    }
    return 0;
  }

  CVopChn *pVopChn=NULL;
  if (pVopGroup == NULL)
  {
    return 1;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
  
  int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);

  if (pVopChn && pBoxchn->isnChnAvail(nChn))
  {
    pVopChn->SetssState(0);
    if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
      SendStopRecord2VOP(nChn, vopno, vopchnno);
    pVopChn->SetsvState(0);
    memset(pVopChn->m_szCallerNo, 0, MAX_TELECODE_LEN);
    memset(pVopChn->m_szCalledNo, 0, MAX_TELECODE_LEN);
    SendVopStateToAll(vopno, vopchnno);
    SendVOPStateToOppIVR(vopno, vopchnno);
    DBUpdateCallCDR_RelTimeByRecCh(nChn);
  }
  return 0;
}
int Proc_VOPMSG_oncallin(CXMLRcvMsg &VOPMsg) //電話呼入消息
{
  short vopno = atoi(VOPMsg.GetAttrValue(1).C_Str());
  short vopchnno = atoi(VOPMsg.GetAttrValue(2).C_Str());
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  
  if (g_nCardType == 9)
  {
    int nChn, nCsc, len;
    char szTemp[MAX_TELECODE_LEN], Caller[MAX_TELECODE_LEN], Called[MAX_TELECODE_LEN];
    
    memset(Caller, 0, MAX_TELECODE_LEN);
    strncpy(Caller, VOPMsg.GetAttrValue(6).C_Str(), MAX_TELECODE_LEN-1);
    memset(Called, 0, MAX_TELECODE_LEN);
    strncpy(Called, VOPMsg.GetAttrValue(7).C_Str(), MAX_TELECODE_LEN-1);

    nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      g_nCallInCount ++;
      DBUpdateCallCDR_UpdateFlag(nChn);
      pBoxchn->ResetChn(nChn);

      if (pChn->lgChnType == 1)
      {
        ChCount.TrkInCount ++;
        ChCount.TrkCallCount ++;
      }
      pChn->timer = 0;
      pChn->ssState = CHN_SNG_IDLE;
      pChn->hwState = CHN_HW_VALID;
      pChn->lnState = CHN_LN_SEIZE; //占用
  
      if (pChn->lgChnType == CHANNEL_TRUNK) //EDIT 2011-06-20
      {
        pChn->CallInOut = CALL_IN;
        pChn->RecvDialDtmfId = 1;
      } 
      else
      {
        pChn->CallInOut = CALL_OUT;
      }
  
      pChn->CallInId = 1; //有呼叫進入
  
      //設置主叫號碼
      len = strlen(Caller);
      if (len > 0)
      {
        if (Caller[0] == '9' && len >= 7)
        {
          if (pChn->ChStyle == CH_A_EXTN)
          {
            if (pIVRCfg->isAutoDelPSTNPreCode == true)
            {
              strcpy(pChn->CallerNo, &Caller[1]);
            }
            else
            {
              strcpy(pChn->CallerNo, Caller);
            }
            pIVRCfg->WritePSTNPreCode(pChn->ChStyleIndex, "9,");
          }
          else
          {
            strcpy(pChn->CallerNo, Caller);
          }
        }
        else
        {
          strcpy(pChn->CallerNo, Caller);
        }
      }
      else
      {
        if (pChn->ChStyle == CH_A_SEAT)
        {
          nCsc = pChn->lgChnNo;
          if (pBoxchn->isnCscAvail(nCsc))
          {
            strcpy(pChn->CallerNo, pCsc->SeatNo.C_Str());
          }
          else
          {
            strcpy(pChn->CallerNo, "000");
          }
        }
        else if (pChn->ChStyle == CH_A_EXTN)
        {
          strcpy(pChn->CallerNo, "000");
        }
      }

      //設置被叫號碼
      strcpy(pChn->CalledNo, Called);

      if (pChn->lgChnType == CHANNEL_TRUNK)
      {
        pBoxchn->pIdelTrkChnQueue->TrkNoDelFromQueue(pChn->lgChnNo);
      }
      if (pIVRCfg->isAutoDelMobilePreCode0 == true)
      {
        if (strncmp(pChn->CallerNo, "013", 3) == 0 || strncmp(pChn->CallerNo, "015", 3) == 0 || strncmp(pChn->CallerNo, "018", 3) == 0)
        {
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pChn->CallerNo[1], MAX_TELECODE_LEN-2);
          memcpy(pChn->CallerNo, szTemp, MAX_TELECODE_LEN);
        }
      }
      if (pChn->lgChnType == 1)
      {
        strcpy(pChn->CustPhone, pChn->CallerNo);
      }
      else
      {
        len = strlen(pIVRCfg->DialOutPreCode);
        if (pIVRCfg->isAutoDelDailOutPreCode == true && len > 0 && len < (int)strlen(pChn->CalledNo) && strncmp(pChn->CalledNo, pIVRCfg->DialOutPreCode, len) == 0)
        {
          strcpy(pChn->CustPhone, &pChn->CalledNo[len]);
        }
        else
        {
          strcpy(pChn->CustPhone, pChn->CalledNo);
        }
        len = strlen(pChn->CustPhone);
        if (len > 1)
        {
          if (pChn->CustPhone[len-1] == '#')
          {
            pChn->CustPhone[len-1] = '\0';
          }
        }
      }
      if (pIVRCfg->ProcLocaAreaCodeId == 1 && strlen(pChn->CustPhone) > pIVRCfg->LocaTelCodeLen)
      {
        len = strlen(pIVRCfg->LocaAreaCode);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode1);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen1+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode1, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode2);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode2, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode3);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode3, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        len = strlen(pIVRCfg->LocaAreaCode4);
        if ((strlen(pChn->CustPhone) == pIVRCfg->LocaTelCodeLen+len) && len > 0)
        {
          if (strncmp(pChn->CustPhone, pIVRCfg->LocaAreaCode4, len) == 0)
          {
            memset(szTemp, 0, MAX_TELECODE_LEN);
            strncpy(szTemp, &pChn->CustPhone[len], MAX_TELECODE_LEN-len);
            memcpy(pChn->CustPhone, szTemp, MAX_TELECODE_LEN);
          }
        }
        strcpy(pChn->CallerNo, pChn->CustPhone);
      }
      DispChnStatus(nChn);
    }
    return 0;
  }

  CVopChn *pVopChn=NULL;
  if (pVopGroup == NULL)
  {
    return 1;
  }
  pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
  
  if (pVopChn)
  {
    pVopChn->SetssState(1);
    if (pVopChn->m_nVopChnType == 6 || pVopChn->m_nVopChnType == 7)
    {
      strcpy(pVopChn->m_szCallerNo, VOPMsg.GetAttrValue(6).C_Str());
      if (pIVRCfg->isAutoDelMobilePreCode0 == true)
      {
        if (strncmp(pVopChn->m_szCallerNo, "013", 3) == 0 
          || strncmp(pVopChn->m_szCallerNo, "015", 3) == 0 
          || strncmp(pVopChn->m_szCallerNo, "018", 3) == 0)
        {
          char szTemp[MAX_TELECODE_LEN];
          memset(szTemp, 0, MAX_TELECODE_LEN);
          strncpy(szTemp, &pVopChn->m_szCallerNo[1], MAX_TELECODE_LEN-2);
          memcpy(pVopChn->m_szCallerNo, szTemp, MAX_TELECODE_LEN);
        }
      }
      
      strcpy(pVopChn->m_szCalledNo, VOPMsg.GetAttrValue(7).C_Str());
      MyTrace(3, "Proc_VOPMSG_oncallin nVopNo=%d nVopChnNo=%d caller=%s called=%s", 
        pVopChn->m_nVopNo, pVopChn->m_nVopChnNo, pVopChn->m_szCallerNo, pVopChn->m_szCalledNo);
    }
    SendVopStateToAll(pVopChn->m_nVopNo, pVopChn->m_nVopChnNo);
    SendVOPStateToOppIVR(pVopChn->m_nVopNo, pVopChn->m_nVopChnNo);
  }
  return 0;
}
int Proc_VOPMSG_onrecvsam(CXMLRcvMsg &VOPMsg) //收到電話呼入后續號碼
{
  return 0;
}
int Proc_VOPMSG_onsendacm(CXMLRcvMsg &VOPMsg) //發送ACM結果
{
  return 0;
}
int Proc_VOPMSG_onsendack(CXMLRcvMsg &VOPMsg) //發送ACK結果
{
  return 0;
}
int Proc_VOPMSG_onrecvacm(CXMLRcvMsg &VOPMsg) //呼出收到ACM
{
  US nChn, nQue, nOut, nAcd, nAG;
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());
  
  if (g_nCardType == 9)
  {
    nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      if (pChn->lgChnType == 1)
      {
        ChCount.TrkOutAnsCount ++;
      }
      nQue = pChn->nQue;
      if (pCalloutQue->isnQueAvail(nQue))
      {
        pQue->CalloutResult = CALLOUT_RING;
        if (pQue->OrgCallType == 1)
        {
          nOut = pQue->nOut;
          pOut->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
        }
        else
        {
          nAcd = pQue->nAcd;
          pAcd->Calleds[pQue->CallPoint].CalloutResult = CALLOUT_RING;
        }
        nAG = pQue->nAG;
        if (pAgentMng->isnAGAvail(nAG))
        {
          SetAgentsvState(nAG, AG_SV_INRING);
        }
        
        pChn->ssState = CHN_SNG_OT_RING; //呼出振鈴
        DispChnStatus(nChn);
      }
    }
  }
  return 0;
}
int Proc_VOPMSG_onrecvack(CXMLRcvMsg &VOPMsg) //呼出收到ACK
{
  short lgchntype = atoi(VOPMsg.GetAttrValue(3).C_Str());
  short lgchnno = atoi(VOPMsg.GetAttrValue(4).C_Str());

  if (g_nCardType == 9)
  {
    int nChn = pBoxchn->Get_nChn_By_LgChn(lgchntype, lgchnno);
    if (pBoxchn->isnChnAvail(nChn))
    {
      if (pChn->lgChnType == 1)
      {
        ChCount.TrkOutAnsCount ++;
      }
      int nQue = pChn->nQue;
      if (pCalloutQue->isnQueAvail(nQue))
      {
        pQue->CalloutResult = CALLOUT_CONN;
        pChn->ssState = CHN_SNG_OT_TALK; //呼出振鈴
        pChn->CalloutId = 0;
        pChn->nQue = 0xFFFF;
        pChn->CanPlayOrRecId = 1; //允許開始放錄音
        pChn->AnsTime = time(0);
        DispChnStatus(nChn);
      }
    }
  }
  return 0;
}
int Proc_VOPMSG_ontransfer(CXMLRcvMsg &VOPMsg) //拍叉簧轉接電話結果
{
  return 0;
}
int Proc_VOPMSG_onivrstatus(CXMLRcvMsg &VOPMsg) //語音機將一方的IVR服務狀態發給另一方
{
  int mactiveid = atoi(VOPMsg.GetAttrValue(2).C_Str());
  int sactiveid = atoi(VOPMsg.GetAttrValue(3).C_Str());
  if (g_nHostType == 0)
  {
    //本機是主用機
    if (g_nMasterStatus == 0 && g_nStandbyStatus == 0 && sactiveid != 1 && StandbyIVRClientId == 0)
    {
      //主機切換為運行狀態
      g_nMasterStatus = 1;
      strcpy(g_szSwitchTime, MyGetNow());
      MyTrace(0, "MasterIVR Switchover to Active because StandbyIVR disactive!!!");
      SendSwitchoverToAllClient(0, "Switchover to Active because StandbyIVR disactive");
      SendHARunStatusToAllLog(g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
    }
  }
  return 0;
}

//-----------------------------------------------------------------------------
//自定義一個函數指針類型
typedef int (*Proc_VoiceMSG)(CXMLRcvMsg &VOPMsg);
Proc_VoiceMSG proc_voicemsg[]=
{
  Proc_VOPMSG_onlogin,	//語音節點資源注冊
  Proc_VOPMSG_onlinealarm,	//收到線路告警消息
  Proc_VOPMSG_onplayresult,	//放音結果消息
  Proc_VOPMSG_onrecordresult,	//錄音結果消息
  Proc_VOPMSG_onrecvdtmf,	//收到DTMF消息
  Proc_VOPMSG_onsendfaxresult,	//發送傳真結果消息
  Proc_VOPMSG_onrecvfaxresult,	//接收傳真結果消息
  Proc_VOPMSG_onrelease,	//通道釋放消息
  Proc_VOPMSG_oncallin, //電話呼入消息
  Proc_VOPMSG_onrecvsam, //收到電話呼入后續號碼
  Proc_VOPMSG_onsendacm, //發送ACM結果
  Proc_VOPMSG_onsendack, //發送ACK結果
  Proc_VOPMSG_onrecvacm, //呼出收到ACM
  Proc_VOPMSG_onrecvack, //呼出收到ACK
  Proc_VOPMSG_ontransfer, //拍叉簧轉接電話結果
  Proc_VOPMSG_onivrstatus, //語音機將一方的IVR服務狀態發給另一方
};
//處理AGENT消息
void ProcVOPMsg(CXMLRcvMsg &VOPMsg)
{
  Get_VOPMSG_HeaderValue(VOPMsg);
  proc_voicemsg[VOPMsg.GetMsgId()](VOPMsg);
}

//-----------------------------------發送消息到語音機--------------------------
//組合發送消息
void Add2VOPMsgHeader(US MsgId, UL sessionid, int vopno, int vopchnno, int chntype, int chnno)
{
  VOPSendMsg.GetBuf().Format("<%s sessionid='%ld' vopno='%d' vopchnno='%d' chantype='%d' channo='%d'",
    pFlwRule->IVRVOPMsgRule[MsgId].MsgNameEng, sessionid, vopno, vopchnno, chntype, chnno);
}
void Add2VOPStrItem(US MsgId, US AttrId, const char *value)
{
  VOPSendMsg.AddItemToBuf(pFlwRule->IVRVOPMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Add2VOPIntItem(US MsgId, US AttrId, int value)
{
  VOPSendMsg.AddIntItemToBuf(pFlwRule->IVRVOPMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Add2VOPLongItem(US MsgId, US AttrId, long value)
{
  VOPSendMsg.AddLongItemToBuf(pFlwRule->IVRVOPMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Add2VOPMsgTail()
{
  VOPSendMsg.AddTailToBuf();
}

//發送文件放音到語音機
int SendPlayFile2VOP(int nChn, int vopno, int vopchnno, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_playfile, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPStrItem(VOPMSG_playfile, 5, FileName);
  Add2VOPIntItem(VOPMSG_playfile, 6, PlayOffset);
  Add2VOPIntItem(VOPMSG_playfile, 7, PlayLength);
  Add2VOPIntItem(VOPMSG_playfile, 8, PlayParam);
  Add2VOPStrItem(VOPMSG_playfile, 9, PlayRule);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_playfile, VOPSendMsg.GetBuf());

  return 0;
}

//發送內存索引放音到語音機
int SendPlayIndex2VOP(int nChn, int vopno, int vopchnno, short VocIndex, short PlayParam, LPCTSTR PlayRule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_playindex, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_playindex, 5, VocIndex);
  Add2VOPIntItem(VOPMSG_playindex, 6, PlayParam);
  Add2VOPStrItem(VOPMSG_playindex, 7, PlayRule);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_playindex, VOPSendMsg.GetBuf());
  
  return 0;
}

//發送停止放音
int SendStopPlay2VOP(int nChn, int vopno, int vopchnno)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_stopplay, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_stopplay, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_stopplay, VOPSendMsg.GetBuf());
  
  return 0;
}

//發送文件錄音到語音機
int SendRecordFile2VOP(int nChn, int vopno, int vopchnno, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  int nAG;

  if (pVopGroup->m_pVops[vopno].m_nVopType == 0)
  {
    Add2VOPMsgHeader(VOPMSG_recordfile, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
    Add2VOPStrItem(VOPMSG_recordfile, 5, FileName);
    Add2VOPIntItem(VOPMSG_recordfile, 6, RecdOffset);
    Add2VOPIntItem(VOPMSG_recordfile, 7, RecdLength);
    Add2VOPIntItem(VOPMSG_recordfile, 8, RecdParam);
    Add2VOPStrItem(VOPMSG_recordfile, 9, RecdRule);
    nAG = pChn->nAG;
    if (pAgentMng->isnAGAvail(nAG))
      Add2VOPStrItem(VOPMSG_recordfile, 10, pAG->m_Seat.IPPhoneBandIP.C_Str());
    else
      Add2VOPStrItem(VOPMSG_recordfile, 10, "");
    Add2VOPMsgTail();
    SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_recordfile, VOPSendMsg.GetBuf());
  }
  else
  {
    char szRecMsg[512];
    nAG = pChn->nAG;

    if (pAgentMng->isnAGAvail(nAG))
    {
      sprintf(szRecMsg, "MsgType=1`MsgName=StartRec`VopNo=%d`VopChnNo=%d`ChnType=%d`ChnNo=%d`ExtnID=%s`PhoneIP=%s`AgentID=%d`AgentName=%s`GroupID=%d`CallerID=%s`CDRSerialNo=%s`FileName=%s`",
        vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo, 
        pAG->GetSeatNo().C_Str(), pAG->m_Seat.IPPhoneBandIP.C_Str(), 
        pAG->GetWorkerNo(), pAG->GetWorkerName().C_Str(), pAG->GetGroupNo(),
        pChn->CustPhone, pAG->CdrSerialNo, FileName);
    }
    else
    {
      sprintf(szRecMsg, "MsgType=1`MsgName=StartRec`VopNo=%d`VopChnNo=%d`ChnType=%d`ChnNo=%d`CallerID=%s`CDRSerialNo=%s`FileName=%s`",
        vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo, 
        pChn->CustPhone, pChn->CdrSerialNo, FileName);
    }
    SendMsg2IPREC(pVopGroup->m_pVops[vopno].m_usClientId, 1, szRecMsg);
  }
  
  return 0;
}

//發送停止錄音
int SendStopRecord2VOP(int nChn, int vopno, int vopchnno)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  int nAG;
  if (pVopGroup->m_pVops[vopno].m_nVopType == 0)
  {
    Add2VOPMsgHeader(VOPMSG_stoprecord, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
    Add2VOPIntItem(VOPMSG_stoprecord, 5, 0);
    nAG = pChn->nAG;
    if (pAgentMng->isnAGAvail(nAG))
      Add2VOPStrItem(VOPMSG_recordfile, 6, pAG->m_Seat.IPPhoneBandIP.C_Str());
    else
      Add2VOPStrItem(VOPMSG_recordfile, 6, "");
    Add2VOPMsgTail();
    SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_stoprecord, VOPSendMsg.GetBuf());
  }
  else
  {
    char szRecMsg[512];
    nAG = pChn->nAG;
    
    if (pAgentMng->isnAGAvail(nAG))
    {
      sprintf(szRecMsg, "MsgType=2`MsgName=StopRec`VopNo=%d`VopChnNo=%d`ChnType=%d`ChnNo=%d`ExtnID=%s`PhoneIP=%s`",
        vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo, 
        pAG->GetSeatNo().C_Str(), pAG->m_Seat.IPPhoneBandIP.C_Str());
    }
    else
    {
      sprintf(szRecMsg, "MsgType=2`MsgName=StopRec`VopNo=%d`VopChnNo=%d`ChnType=%d`ChnNo=%d`",
        vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
    }
    SendMsg2IPREC(pVopGroup->m_pVops[vopno].m_usClientId, 2, szRecMsg);
  }
  
  return 0;
}
//發送DTMF到語音機
int SendSendDTMF2VOP(int nChn, int vopno, int vopchnno, LPCTSTR DTMFStr, short PlayParam, LPCTSTR PlayRule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_senddtmf, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPStrItem(VOPMSG_senddtmf, 5, DTMFStr);
  Add2VOPIntItem(VOPMSG_senddtmf, 6, PlayParam);
  Add2VOPStrItem(VOPMSG_senddtmf, 7, PlayRule);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_senddtmf, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送釋放通道
int SendRelease2VOP(int nChn, int vopno, int vopchnno)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_release, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_release, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_release, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送傳真到語音機
int SendSendFax2VOP(int nChn, int vopno, int vopchnno, LPCTSTR faxfilename, LPCTSTR faxcsid, int param, LPCTSTR rule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_sendfax, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_sendfax, 5, 0);
  Add2VOPStrItem(VOPMSG_sendfax, 6, faxfilename);
  Add2VOPStrItem(VOPMSG_sendfax, 7, faxcsid);
  Add2VOPIntItem(VOPMSG_sendfax, 8, param);
  Add2VOPStrItem(VOPMSG_sendfax, 9, rule);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_sendfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//追加發送傳真到語音機
int SendAppendFax2VOP(int nChn, int vopno, int vopchnno, LPCTSTR faxfilename, LPCTSTR faxcsid, int param, LPCTSTR rule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_appendfax, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_appendfax, 5, 0);
  Add2VOPStrItem(VOPMSG_appendfax, 6, faxfilename);
  Add2VOPStrItem(VOPMSG_appendfax, 7, faxcsid);
  Add2VOPIntItem(VOPMSG_appendfax, 8, param);
  Add2VOPStrItem(VOPMSG_appendfax, 9, rule);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_appendfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//接收傳真到語音機
int SendRecvFax2VOP(int nChn, int vopno, int vopchnno, LPCTSTR faxfilename, LPCTSTR faxcsid, int param, LPCTSTR rule)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_recvfax, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_recvfax, 5, 0);
  Add2VOPStrItem(VOPMSG_recvfax, 6, faxfilename);
  Add2VOPStrItem(VOPMSG_recvfax, 7, faxcsid);
  Add2VOPIntItem(VOPMSG_recvfax, 8, param);
  Add2VOPStrItem(VOPMSG_recvfax, 9, rule);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_recvfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//停止收發傳真到語音機
int SendStopFax2VOP(int nChn, int vopno, int vopchnno)
{
  if (pVopGroup->m_pVops[vopno].m_nLoginState != 1)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_stopfax, pChn->SessionNo, vopno, vopchnno, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_stopfax, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(pVopGroup->m_pVops[vopno].m_usClientId, VOPMSG_stopfax, VOPSendMsg.GetBuf());
  
  return 0;
}

//-----------------------------------------------------------------------------
//發送文件放音到測試話機
int SendPlayFile2TestPhone(int nChn, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_playfile, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPStrItem(VOPMSG_playfile, 5, FileName);
  Add2VOPIntItem(VOPMSG_playfile, 6, PlayOffset);
  Add2VOPIntItem(VOPMSG_playfile, 7, PlayLength);
  Add2VOPIntItem(VOPMSG_playfile, 8, PlayParam);
  Add2VOPStrItem(VOPMSG_playfile, 9, PlayRule);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_playfile, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送內存索引放音到測試話機
int SendPlayIndex2TestPhone(int nChn, short VocIndex, short PlayParam, LPCTSTR PlayRule)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_playindex, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_playindex, 5, VocIndex);
  Add2VOPIntItem(VOPMSG_playindex, 6, PlayParam);
  Add2VOPStrItem(VOPMSG_playindex, 7, PlayRule);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_playindex, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送停止放音測試話機
int SendStopPlay2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_stopplay, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_stopplay, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_stopplay, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送文件錄音到測試話機
int SendRecordFile2TestPhone(int nChn, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_recordfile, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPStrItem(VOPMSG_recordfile, 5, FileName);
  Add2VOPIntItem(VOPMSG_recordfile, 6, RecdOffset);
  Add2VOPIntItem(VOPMSG_recordfile, 7, RecdLength);
  Add2VOPIntItem(VOPMSG_recordfile, 8, RecdParam);
  Add2VOPStrItem(VOPMSG_recordfile, 9, RecdRule);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_recordfile, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送停止錄音測試話機
int SendStopRecord2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_stoprecord, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_stoprecord, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_stoprecord, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送釋放通道測試話機
int SendRelease2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_release, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_release, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_release, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送ACM到測試話機
int SendACM2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_sendacm, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_sendacm, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_sendacm, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送ACK到測試話機
int SendACK2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_sendack, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_sendack, 5, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_sendack, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送呼出到測試話機
int SendCallOut2TestPhone(int nChn, const char *caller, const char *called)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_callout, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_callout, 5, 0);
  Add2VOPIntItem(VOPMSG_callout, 6, 0);
  Add2VOPStrItem(VOPMSG_callout, 7, caller);
  Add2VOPStrItem(VOPMSG_callout, 8, called);
  Add2VOPStrItem(VOPMSG_callout, 9, "");
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_callout, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送取消呼出到測試話機
int SendStopCallout2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_stopcallout, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_stopcallout, 5, 0);
  Add2VOPIntItem(VOPMSG_stopcallout, 6, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_stopcallout, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送傳真到測試話機
int SendSendFax2TestPhone(int nChn, const char *filename)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_sendfax, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_sendfax, 5, 0);
  Add2VOPStrItem(VOPMSG_sendfax, 6, filename);
  Add2VOPIntItem(VOPMSG_sendfax, 7, 0);
  Add2VOPStrItem(VOPMSG_sendfax, 8, "");
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_sendfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//追加發送傳真到測試話機
int SendAppendFax2TestPhone(int nChn, const char *filename)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_appendfax, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_appendfax, 5, 0);
  Add2VOPStrItem(VOPMSG_appendfax, 6, filename);
  Add2VOPIntItem(VOPMSG_appendfax, 7, 0);
  Add2VOPStrItem(VOPMSG_appendfax, 8, "");
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_appendfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//接收傳真到測試話機
int SendRecvFax2TestPhone(int nChn, const char *filename)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_recvfax, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_recvfax, 5, 0);
  Add2VOPStrItem(VOPMSG_recvfax, 6, filename);
  Add2VOPIntItem(VOPMSG_recvfax, 7, 0);
  Add2VOPStrItem(VOPMSG_recvfax, 8, "");
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_recvfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//停止收發傳真到測試話機
int SendStopFax2TestPhone(int nChn)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_stopfax, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPIntItem(VOPMSG_stopfax, 5, 0);
  Add2VOPIntItem(VOPMSG_stopfax, 6, 0);
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_stopfax, VOPSendMsg.GetBuf());
  
  return 0;
}
//發送DTMF到測試話機
int SendSendDTMF2TestPhone(int nChn, const char *dtmfs)
{
  if (TestPhoneLogId == false)
  {
    return 1;
  }
  Add2VOPMsgHeader(VOPMSG_senddtmf, pChn->SessionNo, 1, pChn->lgChnNo, pChn->lgChnType, pChn->lgChnNo);
  Add2VOPStrItem(VOPMSG_senddtmf, 5, dtmfs);
  Add2VOPIntItem(VOPMSG_senddtmf, 6, 0);
  Add2VOPStrItem(VOPMSG_senddtmf, 7, "");
  Add2VOPMsgTail();
  SendMsg2VOP(TestPhoneClientId, VOPMSG_senddtmf, VOPSendMsg.GetBuf());
  
  return 0;
}

//-----------------------------第三方錄音系統----------------------------------
void Set_IVR2REC_Header(UC MsgType, char *MsgName, char *ExtnID)
{
  sprintf(SendMsgBuf, "MsgType=%d`MsgName=%s`ExtnID=%s`", MsgType, MsgName, ExtnID);
}
void Add_IVR2REC_Item(char *ParamName, char *ParamValue)
{
  char szItem[256];
  sprintf(szItem, "%s=%s`", ParamName, ParamValue);
  strcat(SendMsgBuf, szItem);
}
void Add_IVR2REC_Item(char *ParamName, int ParamValue)
{
  char szItem[256];
  sprintf(szItem, "%s=%d`", ParamName, ParamValue);
  strcat(SendMsgBuf, szItem);
}
void SendMsg2REC(US ClientId, US MsgId, const CH *msg)
{
  pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRREC<<8) | MsgId, msg);
  MyTrace(2, "0x%04x %s", ClientId, msg);
}
void Send_Logon_Result_To_REC(const char *ExtnID, int nResult, const char *Errmsg)
{
  if (RECSystemLogId == false)
    return;
  sprintf(SendMsgBuf, "MsgType=0`MsgName=LogonResult`ExtnID=%s`Result=%d`Errmsg=%s`", 
    ExtnID, nResult, Errmsg);
  SendMsg2REC(RECSystemClientId, 0, SendMsgBuf);
}
void Send_Transfer_To_REC(int nAcd, int nAG)
{
  int nChn;
  CStringX ArrString[2];
  if (RECSystemLogId == false)
    return;
  nChn = pAcd->Session.nChn;
  if (!pBoxchn->isnChnAvail(nChn) || !pAgentMng->isnAGAvail(nAG))
    return;
  if (SplitString(pAcd->Session.Param.C_Str(), '`', 2, ArrString) > 0)
  {
    sprintf(SendMsgBuf, "MsgType=1`MsgName=Transfer`ExtnID=%s`SessionId=%lu`CallerID=%s`CalledID=%s`MenuKey=%s`CDRSerialNo=%s`", 
      pAG->GetSeatNo().C_Str(), pChn->SessionNo, pChn->CallerNo, pChn->CalledNo, ArrString[0].C_Str(), pChn->CdrSerialNo);
  } 
  else
  {
    sprintf(SendMsgBuf, "MsgType=1`MsgName=Transfer`ExtnID=%s`SessionId=%lu`CallerID=%s`CalledID=%s`MenuKey=%s`CDRSerialNo=%s`", 
      pAG->GetSeatNo().C_Str(), pChn->SessionNo, pChn->CallerNo, pChn->CalledNo, "0", pChn->CdrSerialNo);
  }
  SendMsg2REC(RECSystemClientId, 1, SendMsgBuf);
}
void Send_StopTransfer_To_REC(int nChn, int nAG)
{
  if (RECSystemLogId == false)
    return;
  if (!pBoxchn->isnChnAvail(nChn) || !pAgentMng->isnAGAvail(nAG))
    return;
  sprintf(SendMsgBuf, "MsgType=2`MsgName=StopTransfer`ExtnID=%s`SessionId=%lu`Reason=2`", 
    pAG->GetSeatNo().C_Str(), pChn->SessionNo);
  SendMsg2REC(RECSystemClientId, 2, SendMsgBuf);
}
void Send_ACDState_To_REC(int ACDNum, int WaitACDNum, int WaitANSNum, const char *CallerList)
{
  if (RECSystemLogId == false)
    return;

  sprintf(SendMsgBuf, "MsgType=4`MsgName=ACDState`ACDNum=%d`WaitACDNum=%d`WaitANSNum=%d`CallerList=%s`", 
    ACDNum, WaitACDNum, WaitANSNum, CallerList);
  SendMsg2REC(RECSystemClientId, 4, SendMsgBuf);
}
void Send_IVRState_To_REC(int IdelNum, int BusyNum, int BlockNum)
{
  if (RECSystemLogId == false)
    return;

  sprintf(SendMsgBuf, "MsgType=5`MsgName=IVRState`IdleNum=%d`BusyNum=%d`BlockNum=%d`", 
    IdelNum, BusyNum, BlockNum);
  SendMsg2REC(RECSystemClientId, 5, SendMsgBuf);
}
void Send_AGState_To_REC(int LogonNum, int LogoffNum, int IdelNum, int BusyNum, int TalkNum, int NoReadyNum)
{
  if (RECSystemLogId == false)
    return;

  sprintf(SendMsgBuf, "MsgType=6`MsgName=AGState`LogonNum=%d`LogoffNum=%d`IdleNum=%d`BusyNum=%d`TalkNum=%d`NoReadyNum=%d`", 
    LogonNum, LogoffNum, IdelNum, BusyNum, TalkNum, NoReadyNum);
  SendMsg2REC(RECSystemClientId, 6, SendMsgBuf);
}
void Send_AGState_To_REC(int nAG)
{
  if (RECSystemLogId == false)
    return;

  char szLogTime[32];
  int nSvState;
  
  if (pAG->GetWorkerNo() == 0)
  {
    if (pAG->m_Worker.LogoutTime == 0)
    {
      strcpy(szLogTime, "");
    }
    else
    {
      strcpy(szLogTime, time_t2str(pAG->m_Worker.LogoutTime));
    }
  } 
  else
  {
    strcpy(szLogTime, time_t2str(pAG->m_Worker.LoginTime));
  }
  if (pAG->DelayState == 0)
  {
    nSvState = pAG->svState;
  } 
  else if (pAG->DelayState == 1)
  {
    nSvState = AG_SV_ACW;
  }
  else
  {
    nSvState = AG_SV_DELAY;
  }

  sprintf(SendMsgBuf, "MsgType=8`MsgName=ExtState`ExtnID=%s`AgentID=%s`GroupID=%d`AgentName=%s`LoginTime=%s`SvState=%d`DisturbID=%d`LeaveID=%d`LeaveReason=%s`", 
    pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), pAG->GetGroupNo(), pAG->GetWorkerName().C_Str(),
    szLogTime, nSvState, pAG->m_Worker.DisturbId, pAG->m_Worker.LeaveId,
    pAG->m_Worker.LeaveReason.C_Str());
  SendMsg2REC(RECSystemClientId, 8, SendMsgBuf);
}

void Send_AGExtAck_To_REC(int nAG)
{
  if (RECSystemLogId == false)
    return;
  int nChn = pAG->GetSeatnChn();
  char szCallerNo[64], szCalledNo[64];

  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pAG->svState != 0)
    {
      if (pChn->CallInOut == 1)
      {
        if (g_nSwitchMode == 0)
        {
          strcpy(szCallerNo, pChn->CalledNo);
          strcpy(szCalledNo, pChn->CallerNo);
        }
        else
        {
          strcpy(szCallerNo, pChn->CallerNo);
          strcpy(szCalledNo, pChn->CalledNo);
        }
      }
      else
      {
        if (g_nSwitchMode == 0)
        {
          strcpy(szCallerNo, pChn->CallerNo);
          strcpy(szCalledNo, pChn->CalledNo);
        }
        else
        {
          strcpy(szCallerNo, pChn->CalledNo);
          strcpy(szCalledNo, pChn->CallerNo);
        }
      }
    }
  }
  
  sprintf(SendMsgBuf, "MsgType=7`MsgName=ExtAck`ExtnID=%s`AgentID=%s`ACDGroup=%d`Trunk=%d`CallerID=%s`CalleeID=%s`CustomerID=%s`", 
    pAG->GetSeatNo().C_Str(), pAG->GetWorkerNoStr(), pAG->GetGroupNo(), 0, szCallerNo, szCalledNo, "");
  SendMsg2REC(RECSystemClientId, 7, SendMsgBuf);
}
//2015-05-30
void Send_ReqCallID_To_REC(int nAG)
{
  if (RECSystemLogId == false)
    return;
  int nChn = pAG->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  if (strlen(pChn->CdrSerialNo) > 0) //2016-01-30 如果serialno為空就不發送
  {
    sprintf(SendMsgBuf, "MsgType=9`MsgName=CallIDReq`ExtnID=%s`SerialNo=%s`Trunk=%s`", pChn->DeviceID, pChn->CdrSerialNo, pChn->TrunkGroupNumber);
    SendMsg2REC(RECSystemClientId, 9, SendMsgBuf);
  }
  else if (strlen(pAG->CdrSerialNo) > 0) //2016-01-30 如果serialno為空就不發送
  {
    sprintf(SendMsgBuf, "MsgType=9`MsgName=CallIDReq`ExtnID=%s`SerialNo=%sTrunk=%s``", pChn->DeviceID, pAG->CdrSerialNo, pChn->TrunkGroupNumber);
    SendMsg2REC(RECSystemClientId, 9, SendMsgBuf);
  }
  pChn->ExtRecordReqFlag = 1;
  pChn->DtmfPauseTimer = 0; //請計時器，這個計時器兼做請求計時器
}
//2015-05-30
void Send_ReqCallID_To_REC1(int nChn)
{
  if (RECSystemLogId == false)
    return;
  if (pChn->ExtRecordReqFlag == 1 && pChn->DtmfPauseTimer > 2)
  {
    if (strlen(pChn->CdrSerialNo) > 0) //2016-01-30 如果serialno為空就不發送
    {
      sprintf(SendMsgBuf, "MsgType=9`MsgName=CallIDReq`ExtnID=%s`SerialNo=%s`Trunk=%s`", pChn->DeviceID, pChn->CdrSerialNo, pChn->TrunkGroupNumber);
      SendMsg2REC(RECSystemClientId, 9, SendMsgBuf);
    }
    pChn->DtmfPauseTimer = 0; //請計時器，這個計時器兼做請求計時器
  }
}

void Proc_Msg_From_REC(UC MsgType, char *msgbuf)
{
  char szExtnID[32];
  char szAgentName[32];
  char szCallerID[32];
  char szCalledID[32];
  int nAgentID; //工號
  int nGroupID; //組號
  int nLineState; //0-空閑掛機狀態Idel、1-斷線阻塞Block、2-呼入振鈴Ring、3-摘機通話Talk
  int nAgentState; //0-未就緒NoReady，1-已就緒Ready，2-免打擾
  int nChn, nAG, nAG1;
  CAgent *pAgent;

  memset(szExtnID, 0, 32);
  strncpy(szExtnID, GetParamStrValue("ExtnID=", msgbuf), 31);
  nAG = pAgentMng->GetnAGBySeatNo(szExtnID);
  if (!pAgentMng->isnAGAvail(nAG))
  {
    return;
  }
  nChn = pAG->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn))
    return;
  
  MyTrace(3, "Proc_Msg_From_REC MsgType=%d ExtnID=%s nAG=%d AgentChn=%d nAcd=%d CdrSerialNo=%s", MsgType, szExtnID, nAG, nChn, pChn->nAcd, pAG->CdrSerialNo);

  switch (MsgType)
  {
  case 0: //Logon登錄消息: MsgType=0`MsgName=Logon`ExtnID=分機號碼`AgentID=Agent工號`AgentName=Agent姓名`GroupID=Agent所屬組號`LineState=分機線路狀態`AgentState=Agent狀態`
    if (g_nSwitchMode != PORT_NOCTI_PBX_TYPE)
      break;

    //WriteSeatStatus(pAG, AG_STATUS_IDEL, 0); //2015-12-28 針對評議中心修改
    pAG->DelayState = 0; //2015-12-28 針對評議中心修改

    memset(szAgentName, 0, 32);
    strncpy(szAgentName, GetParamStrValue("AgentName=", msgbuf), 31);
    nAgentID = GetParamIntValue("AgentID=", msgbuf);
    if (nAgentID == 0)
    {
      Send_Logon_Result_To_REC(szExtnID, 1, "AgentID error");
      break;
    }
    nGroupID = GetParamIntValue("GroupID=", msgbuf);
    if (nGroupID == 0)
    {
      nGroupID = 1;
    }
    nAG1 = pAgentMng->GetnAGByWorkerNo(nAgentID);
    if (pAgentMng->isnAGAvail(nAG1))
    {
      if (nAG1 != nAG)
      {
        Send_Logon_Result_To_REC(szExtnID, 1, "the AgentID had login");
        break;
      }
    }
    pAG->m_Worker.ClearWork();
    if (nAgentID == pAG->m_Worker.PreWorkerNo)
      WriteWorkerLogoutRecord(pAG);
    
    pAG->m_Worker.ClearWorkerParam();
    pAG->m_Worker.WorkerLogin(nAgentID, szAgentName, 1);

    if (nGroupID > 0 && nGroupID < MAX_AG_GROUP)
    {
      pAG->m_Worker.SetWorkerGroupNo(nGroupID, 1);
      pAgentMng->WorkerGroup[nGroupID].state = 1;
    }
    pAG->m_Worker.WorkerCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
    pAG->m_Worker.WorkerCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
    pAG->m_Worker.WorkerCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
    pAG->m_Worker.WorkerCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長
    
    pAG->m_Seat.SeatCallCountParam.CallInWaitOverTimeLen = pIVRCfg->MaxInRingOverTime; //呼入超時應答時長
    pAG->m_Seat.SeatCallCountParam.CallOutWaitOverTimeLen = pIVRCfg->MaxOutRingOverTime; //呼出超時應答時長
    pAG->m_Seat.SeatCallCountParam.CallTalkOverTimeLen = pIVRCfg->MaxTalkOverTime; //通話超時時長
    pAG->m_Seat.SeatCallCountParam.CallACWOverTimeLen = pIVRCfg->MaxACWOverTime; //話后處理超時時長

    pAG->ChangeStatus(AG_STATUS_LOGIN);
    MyTrace(3, "WriteWorkerLoginStatus nAG=%d Proc_Msg_From_REC()", pAG->nAG);
    WriteWorkerLoginStatus(pAG);

    pAG->m_Worker.PreWorkerNo = nAgentID;
    Send_Logon_Result_To_REC(szExtnID, 0, "");
    
    pAG->m_Worker.WorkerCallCountParam.ChangeStatus(AG_STATUS_IDEL);
    
    DispAgentStatus(nAG);
  	break;
  case 1: //LineState分機線路狀態消息: MsgType=1`MsgName=LineState`ExtnID=分機號碼`LineState=分機線路狀態`CallerID=主叫號碼(LineState=2時有效)`
    nLineState = GetParamIntValue("LineState=", msgbuf);
    MyTrace(3, "Proc_Msg_From_REC MsgType=%d LineState=%d ExtnID=%s nAG=%d AgentChn=%d nAcd=%d CdrSerialNo=%s", MsgType, nLineState, szExtnID, nAG, nChn, pChn->nAcd, pAG->CdrSerialNo);
    if (g_nSwitchMode != PORT_NOCTI_PBX_TYPE)
    {
      //錄音文件格式：http://192.168.11.13/Media/20130712/001/20130712105029.wav 
      char szRecServer[64], szRecDate[20], szRecChn[8], szRecFileName[128], szCallId[64], szSerialNo[64], szRecRootPath[128], szRecPathFileName[256];
      memset(szRecServer, 0, 64);
      strncpy(szRecServer, GetParamStrValue("Server=", msgbuf), 63);
      memset(szRecChn, 0, 8);
      strncpy(szRecChn, GetParamStrValue("Channel=", msgbuf), 7);
      memset(szRecFileName, 0, 128);
      strncpy(szRecFileName, GetParamStrValue("FileName=", msgbuf), 127);
      memset(szCallId, 0, 64);
      strncpy(szCallId, GetParamStrValue("CallID=", msgbuf), 63);
      memset(szSerialNo, 0, 64);
      strncpy(szSerialNo, GetParamStrValue("SerialNo=", msgbuf), 63);
      memset(szRecDate, 0, 20);
      strncpy(szRecDate, szRecFileName, 8);
    
      sprintf(szRecRootPath, "http://%s/Media/", szRecServer);
      sprintf(szRecPathFileName, "%s/%s/%s", szRecDate, szRecChn, szRecFileName);
      if (nLineState == 3)
      {
        //2015-05-30
        MyTrace(3, "Proc_Msg_From_REC nAG=%d szSerialNo=%s", nAG, szSerialNo);
        if (strlen(szSerialNo) > 0 && strcmp(szSerialNo, pAG->CdrSerialNo) == 0)
        {
          DBUpdateCallCDR_RecdFile(szSerialNo, szRecRootPath, szRecPathFileName, szCallId); //2015-02-26 只有在摘機時寫callid
          pChn->ExtRecordReqFlag = 2;
          pChn->DtmfPauseTimer = 0;
        }

        strcpy(pAG->ExtRecordServer, szRecRootPath);
        strcpy(pAG->ExtRecordFileName, szRecPathFileName);
        strcpy(pAG->ExtRecordCallID, szCallId);
        if (RECSystemLogId == true && pAG->GetsvState() != 0)
        {
          char DialParam[256];
          sprintf(DialParam, "%s`%s`%s`", pAG->ExtRecordCallID, pAG->ExtRecordFileName, pAG->ExtRecordServer);
          SendCommonResult(nAG, AGMSG_onanswercall, 9, DialParam);
        }
      }
      else
      {
        memset(pAG->ExtRecordServer, 0, MAX_CHAR128_LEN);
        memset(pAG->ExtRecordFileName, 0, MAX_CHAR128_LEN);
        memset(pAG->ExtRecordCallID, 0, MAX_CHAR128_LEN);
      }
      break;
    }
    switch (nLineState)
    {
    case 0: //空閑或掛機狀態Idel
      pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        pAgent->delaytimer = 0;
        if (pAgent->svState == AG_SV_CONN || pAgent->svState == AG_SV_HOLD)
        {
          DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
          pAgent->DelayState = 1;
          WriteSeatStatus(pAgent, AG_STATUS_ACW, 0);
        }
        else
        {
          DBUpdateCallCDR_SeatRelTime(pAgent->nAG);
          DBUpdateCallCDR_ACWTime(pAgent->nAG);
          WriteSeatStatus(pAgent, AG_STATUS_IDEL, 0);
        }
        pAgent->SetAgentsvState(AG_SV_IDEL);
        pAgent->ClearSrvLine();
        SendSeatHangon(pAgent->nAG);
        DispAgentStatus(pAgent->nAG);
      }
    	if (pChn->ssState == CHN_SNG_IN_TALK || pChn->ssState == CHN_SNG_OT_TALK)
      {
        DBUpdateCallCDR_RelTime(nChn);
      }
      pBoxchn->ResetChn(nChn);
      DispChnStatus(nChn);
      break;
    case 1: //線路斷開阻塞Block
      pChn->hwState = 1;
      DispChnStatus(nChn);
      break;
    case 2: //呼入振鈴Ring
      pChn->timer = 0;
      pChn->DtmfPauseTimer = 0;
      pChn->hwState = CHN_HW_VALID;
      pChn->CallInOut = CALL_IN;
      pChn->CallInId = 1; //有呼叫進入
      pChn->lnState = CHN_LN_SEIZE; //占用
      pChn->ssState = CHN_SNG_IN_RING;
      pChn->CallTime = time(0);
      
      memset(szCallerID, 0, 32);
      strncpy(szCallerID, GetParamStrValue("CallerID=", msgbuf), 31);
      strcpy(pChn->CallerNo, szCallerID);
      strcpy(pChn->CustPhone, szCallerID);
      if (pChn->ChStyleIndex < MAX_CHANNEL_REC_NUM)
        strcpy(pChn->CalledNo, pIVRCfg->RecLineCode[pChn->ChStyleIndex]);
      
      pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        if (pAgent->TranAGId == 0)
        {
          pChn->GenerateCdrSerialNo();
          DBInsertCallCDR(nChn, 1, 1);
          strcpy(pAgent->CdrSerialNo, pChn->CdrSerialNo);
          DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);
        }
        else if (pAgent->TranAGId == 1) //2013-09-24 增加處理錄音系統發來的轉座席后的振鈴消息
        {
          int nAcd = pChn->nAcd;
          if (pACDQueue->isnAcdAvail(nAcd))
          {
            MyTrace(3, "nAG=%d AgentChn=%d InChn=%d nAcd=%d Set CalloutResult=CALLOUT_RING",
              nAG, nChn, pAcd->Session.nChn, nAcd);
            pAcd->Calleds[0].CalloutResult = CALLOUT_RING;
            //Reason=2因檢測到振鈴而切換到等待應答狀態
            StopTransfer(pAcd->Session.nChn, 2);
          }
        }
        
        pAgent->SetAgentsvState(AG_SV_INRING);
        WriteSeatStatus(pAgent, AG_STATUS_INRING, 0, 1, 0);
        DispAgentStatus(pAgent->nAG);
        if (pAgent->GetClientId() != 0x06FF)
        {
          sprintf(SendMsgBuf, "<onacdcallin acdid='%d' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='0' param='%s'/>", 
            pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CustPhone, pChn->CalledNo, 1, "");
          SendMsg2AG(pAgent->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
        }
      }
      DispChnStatus(nChn);
      break;
    case 3: //摘機通話Talk
      pAgent = GetAgentBynChn(nChn);
      if (pAgent != NULL)
      {
        if (pAgent->svState == AG_SV_IDEL || pAgent->svState == AG_SV_DELAY)
        {
          pAgent->SetAgentsvState(AG_SV_OUTSEIZE);
          DispAgentStatus(pAgent->nAG);
        }
        if (pAgent->TranAGId == 1)
        {
          int nAcd = pChn->nAcd;
          if (pACDQueue->isnAcdAvail(nAcd))
          {
            MyTrace(3, "nAG=%d AgentChn=%d InChn=%d nAcd=%d Set CalloutResult=CALLOUT_CONN",
              nAG, nChn, pAcd->Session.nChn, nAcd);
            pAcd->Calleds[0].CalloutResult = CALLOUT_CONN;
            pChn->nAcd = 0xFFFF;
            //Reason=1因為錄音系統檢測到摘機而停止轉接
            StopTransfer(pAcd->Session.nChn, 1);
          }

          pChn->CanPlayOrRecId = 1;
          pChn->AnsTime = time(0);
          pChn->ssState = CHN_SNG_OT_TALK;
          DispChnStatus(nChn);
        }
        else
        {
          pChn->timer = 0;
          pChn->DtmfPauseTimer = 0;
          pChn->hwState = CHN_HW_VALID;
          pChn->CallInOut = CALL_OUT;
          pChn->CallInId = 1; //有呼叫進入
          pChn->lnState = CHN_LN_SEIZE; //占用
          pChn->CallTime = time(0);
          pChn->AnsTime = time(0);
          pChn->ssState = CHN_SNG_OT_TALK; //占用
          
          //增加呼出寫話單 2016-02-06
          memset(szCalledID, 0, 32);
          strncpy(szCalledID, GetParamStrValue("CalledID=", msgbuf), 31);
          strcpy(pChn->CallerNo, szExtnID);
          strcpy(pChn->CustPhone, szCalledID);
          strcpy(pChn->CalledNo, szCalledID);
          pChn->GenerateCdrSerialNo();
          DBInsertCallCDR(nChn, 1, 2);
          DBUpdateCallCDR_WorkerNo(nChn, pAgent->nAG);
          
          DispChnStatus(nChn);
          
          pAgent->SetAgentsvState(AG_SV_CONN);
          DispAgentStatus(pAgent->nAG);
        }
      }
      break;
    }
    break;
  case 2: //AgentState坐席狀態消息: MsgType=2`MsgName=AgentState`ExtnID=分機號碼`AgentState=坐席狀態`
    if (g_nSwitchMode != PORT_NOCTI_PBX_TYPE)
      break;
    nAgentState = GetParamIntValue("AgentState=", msgbuf);
    if (nAgentState == 1)
    {
      pAG->m_Worker.SetDisturb(0);
      WriteSeatStatus(pAG, AG_STATUS_IDEL, 0);
    }
    else
    {
      pAG->m_Worker.SetDisturb(1);
      WriteSeatStatus(pAG, AG_STATUS_BUSY, 0);
    }
    break;
  case 3: //Logoff退出消息: MsgType=3`MsgName=Logoff`ExtnID=分機號碼`
    if (g_nSwitchMode != PORT_NOCTI_PBX_TYPE)
      break;
    WriteSeatStatus(pAG, AG_STATUS_LOGOUT, 0);
    WriteWorkerLogoutStatus(pAG);
    if (pAG->m_Worker.oldWorkerNo == 0)
    {
      pAG->m_Worker.Logout();
      pAG->m_Worker.ClearState();
    }
    else
    {
      pAG->m_Worker.OldWorkerLogin();
    }
    pAG->m_Worker.OldDisturbId = 0;
    pAG->m_Worker.OldLeaveId = 0;
    
    DispAgentStatus(nAG);
    break;
  case 4: //TransferResult轉接響應結果消息: MsgType=4`MsgName=TransferResult`ExtnID=分機號碼`SessionId=會話id`Result=結果(0-成功，1-失敗)`RecordFile=錄音文件名'
    break;
  case 5: //RecvDTMF收到的按鍵消息: MsgType=5`MsgName=RecvDTMF`ExtnID=分機號碼`DTMF=按鍵`
    break;
  case 6: //ExtReq詢問分機資訊的消息定義: MsgType=6`MsgName=ExtReq`ExtnID=分機號碼`
    Send_AGExtAck_To_REC(nAG);
    break;
  }
}
void Proc_Msg_From_IPREC(UC MsgType, char *msgbuf)
{
  CVopChn *pVopChn=NULL;
  CAgent *pAgent=NULL;
  char  DialParam[256], szTemp[128];
  short vopno;
  short vopchnno;
  //short lgchntype;
  //short lgchnno;
  short nChn;
  short recresult;

  if (AuthPassId == false && g_nCallInCount > 8)
  {
    MyTrace(0, "License Data is error!!!");
    return;
  }

  if (pVopGroup == NULL)
  {
    return;
  }

  switch (MsgType)
  {
  case 0: //錄音通道狀態
    {
      vopno = GetParamIntValue("VopNo=", msgbuf);
      vopchnno = GetParamIntValue("VopChnNo=", msgbuf);
      recresult = GetParamIntValue("State=", msgbuf);
      pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
      if (pVopChn)
      {
        if (recresult == 0) //bit0: 0-正常 1-阻塞 bit1: 0-打開 1-關閉
        {
          //正常
          pVopChn->m_nLnState &= 2;
        }
        else
        {
          //斷線
          pVopChn->m_nLnState |= 1;
        }
        SendVopStateToAll(vopno, vopchnno);
        SendVOPStateToOppIVR(vopno, vopchnno);
      }
    }
    break;
  case 1: //電話呼叫振鈴事件
    {
      g_nCallInCount++;

      vopno = GetParamIntValue("VopNo=", msgbuf);
      vopchnno = GetParamIntValue("VopChnNo=", msgbuf);
    
      pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
      if (pVopChn == NULL)
        break;
      nChn = pVopChn->m_nSeizeChn;
      if (pBoxchn->isnChnAvail(nChn))
      {
        pBoxchn->ResetChn(nChn);
        pChn->timer = 0;
        pChn->CallTime = time(0);
        pChn->ssState = CHN_SNG_IN_ARRIVE;
        pChn->hwState = CHN_HW_VALID;
        pChn->lnState = CHN_LN_SEIZE; //占用
        pChn->CallInOut = GetParamIntValue("Direct=", msgbuf);
        memset(pChn->CallerNo, 0, MAX_TELECODE_LEN);
        strncpy(pChn->CallerNo, GetParamStrValue("CallerID=", msgbuf), MAX_TELECODE_LEN-1);
        memset(pChn->CalledNo, 0, MAX_TELECODE_LEN);
        strncpy(pChn->CalledNo, GetParamStrValue("CalledID=", msgbuf), MAX_TELECODE_LEN-1);
        if (pChn->CallInOut == 1)
        {
          strcpy(pChn->CustPhone, pChn->CallerNo);
        } 
        else
        {
          strcpy(pChn->CustPhone, pChn->CalledNo);
        }
        pChn->GenerateCdrSerialNo();
        DBInsertCallCDR(nChn, 1, pChn->CallInOut);
        DBUpdateCallCDR_WorkerNo(nChn, pChn->DeviceID, pChn->DeviceID, 2);
        DispChnStatus(nChn);

        pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          sprintf(DialParam, "1`%s`%s`%s", pChn->CdrSerialNo, pChn->RecordFileName, pIVRCfg->SeatRecPath.C_Str());
          WriteSeatStatus(pAgent, AG_STATUS_INRING, pAgent->m_Worker.LeaveId, 1, 0);
          pAgent->SetAgentsvState(AG_SV_INRING);
          DispAgentStatus(pAgent->nAG);
          if (pIVRCfg->isSendACDCallToAgent == true && pAgent->GetClientId() != 0x06FF)
          {
            sprintf(SendMsgBuf, "<onacdcallin acdid='%d' sessionno='%lu' chantype='%d' channo='%d' callerno='%s' calledno='%s' orgcallerno='' orgcalledno='' calltype='%d' funcno='0' param='%s'/>", 
              pAgent->GetClientId()<<16, pChn->SerialNo, pChn->lgChnType, pChn->lgChnNo, pChn->CustPhone, pChn->CalledNo, pChn->CallInOut, DialParam);
            SendMsg2AG(pAgent->GetClientId(), AGMSG_onacdcallin, SendMsgBuf);
          }
          if (WebSocketLogId == true && strlen(pAgent->m_Seat.WSClientId) > 0)
          {
            sprintf(SendMsgBuf, "wsclientid=%s;cmd=acdcallin;seatno=%s;callerno=%s;calledno=%s;param=%s",
              pAgent->m_Seat.WSClientId, pAgent->GetSeatNo().C_Str(), pChn->CustPhone, pChn->CalledNo, DialParam);
            SendMsg2WS(WebSocketClientId, 0, SendMsgBuf);
          }
        }
      }
    }
    break;
  case 2: //電話呼叫信息事件
    {
      vopno = GetParamIntValue("VopNo=", msgbuf);
      vopchnno = GetParamIntValue("VopChnNo=", msgbuf);
      
      pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
      if (pVopChn == NULL)
        break;
      
      nChn = pVopChn->m_nSeizeChn;
      if (pBoxchn->isnChnAvail(nChn))
      {
        memset(pChn->CallerNo, 0, MAX_TELECODE_LEN);
        strncpy(pChn->CallerNo, GetParamStrValue("CallerID=", msgbuf), MAX_TELECODE_LEN-1);
        memset(pChn->CalledNo, 0, MAX_TELECODE_LEN);
        strncpy(pChn->CalledNo, GetParamStrValue("CalledID=", msgbuf), MAX_TELECODE_LEN-1);
        if (pChn->CallInOut == 1)
        {
          strcpy(pChn->CustPhone, pChn->CallerNo);
        } 
        else
        {
          strcpy(pChn->CustPhone, pChn->CalledNo);
        }
        DBUpdateCallCDR_Phone(nChn);
        DispChnStatus(nChn);
      }
    }
    break;
  case 3: //電話呼叫通話事件
    {
      vopno = GetParamIntValue("VopNo=", msgbuf);
      vopchnno = GetParamIntValue("VopChnNo=", msgbuf);

      pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
      if (pVopChn == NULL)
        break;
      
      nChn = pVopChn->m_nSeizeChn;
      if (pBoxchn->isnChnAvail(nChn))
      {
        pChn->AnsTime = time(0);
        pChn->ssState = CHN_SNG_IN_TALK;
        memset(pChn->CallerNo, 0, MAX_TELECODE_LEN);
        strncpy(pChn->CallerNo, GetParamStrValue("CallerID=", msgbuf), MAX_TELECODE_LEN-1);
        memset(pChn->CalledNo, 0, MAX_TELECODE_LEN);
        strncpy(pChn->CalledNo, GetParamStrValue("CalledID=", msgbuf), MAX_TELECODE_LEN-1);
        if (pChn->CallInOut == 1)
        {
          strcpy(pChn->CustPhone, pChn->CallerNo);
        } 
        else
        {
          strcpy(pChn->CustPhone, pChn->CalledNo);
        }
        DBUpdateCallCDR_Phone(nChn);
        DBUpdateCallCDR_AnsTime(nChn);
        DBUpdateCallCDR_SeatAnsTime(nChn, pChn->DeviceID, pChn->DeviceID);

        DispChnStatus(nChn);
        pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          pAgent->SetAgentsvState(AG_SV_CONN);
          WriteSeatStatus(pAgent, AG_STATUS_TALK, 1, 1, 1);
          DispAgentStatus(pAgent->nAG);
        }
      }
    }
    break;
  case 4: //電話呼叫釋放事件
    {
      vopno = GetParamIntValue("VopNo=", msgbuf);
      vopchnno = GetParamIntValue("VopChnNo=", msgbuf);
      
      pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
      if (pVopChn == NULL)
        break;
      
      nChn = pVopChn->m_nSeizeChn;
      if (pBoxchn->isnChnAvail(nChn))
      {
        pAgent = GetAgentBynChn(nChn);
        if (pAgent != NULL)
        {
          pAgent->SetAgentsvState(AG_SV_IDEL);
          SendSeatHangon(pAgent->nAG);
          pAgent->ClearSrvLine();
          DispAgentStatus(pAgent->nAG);
        }
        StopRecord(nChn);
        DBUpdateCallCDR_RelTime(nChn);
        pBoxchn->ResetChn(nChn);
        DispChnStatus(nChn);

        pVopChn->SetssState(0);
        pVopChn->SetsvState(0);
        SendVopStateToAll(vopno, vopchnno);
      }
    }
    break;
  case 5: //錄音事件
    {
      vopno = GetParamIntValue("VopNo=", msgbuf);
      vopchnno = GetParamIntValue("VopChnNo=", msgbuf);
      
      pVopChn = pVopGroup->GetVopChnByVopChnNo(vopno, vopchnno);
      if (pVopChn == NULL)
        break;
      
      nChn = pVopChn->m_nSeizeChn;
      if (pBoxchn->isnChnAvail(nChn))
      {
        memset(szTemp, 0, 128);
        strncpy(szTemp, GetParamStrValue("RecordPath=", msgbuf), 127);
        memset(szTemp, 0, MAX_CHAR128_LEN);
        strncpy(pChn->RecordFileName, GetParamStrValue("RecordFileName=", msgbuf), MAX_CHAR128_LEN-1);
        
        DBUpdateCallCDR_ChnRecdFile(nChn);
        
        pVopChn->SetssState(1);
        pVopChn->SetsvState(2); //正在錄音
        SendVopStateToAll(vopno, vopchnno);
      }
    }
    break;
  }
}
//-----------------------------------------------------------------------------
void SendMsg2COM(US ClientId, US MsgId, const CH *msg)
{
  if (COMServiceLogId == true)
  {
    pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_IVRCOM<<8) | MsgId, msg);
    MyTrace(2, "0x%04x %s", ClientId, msg);
  }
}

//sprintf(SendMsgBuf, "COM=%s`DATA=%s`", "COM13", pszComMsg);
//SendMsg2COM(COMServiceClientId, 1, SendMsgBuf);
void Proc_Msg_From_COM(const char *pszComMsg)
{
  //VMPORT-信箱端口分機號，VMBOX-留言信箱號碼，VMCODE-整合碼，VMCALLERID-來電主叫號碼
  int nChn;
  char szVmPort[32], szVmMsg[64];
  
  memset(szVmPort, 0, 32);
  memset(szVmMsg, 0, 64);

  strcpy(szVmPort, GetParamByName(pszComMsg, "VMPORT", "", 31));
  strcpy(szVmMsg, GetParamByName(pszComMsg, "VMMSG", "", 63));
  nChn = pBoxchn->Get_ChnNo_By_DeviceID(szVmPort);
  if (pBoxchn->isnChnAvail(nChn))
  {
    strcpy(pChn->CallInDtmfBuf, szVmMsg);
    MyTrace(3, "nChn=%d Recv VMMSG=%s", nChn, pChn->CallInDtmfBuf);
  }
}
