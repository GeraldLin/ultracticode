//---------------------------------------------------------------------------
#ifndef FuncfileH
#define FuncfileH
extern short g_VOCInLinux;
//---------------------------------------------------------------------------
//取文件名緩沖區結構
typedef struct
{
	CH FileName[MAX_VAR_DATA_LEN];
	CTime ChangeTime;
	DWORD FileSize;

}VXML_GETFILEATTR_STRUCT;

//在文件名前插入根路徑
CStringX AddRootPath_str(const CStringX rootpath, const char *filename);
CStringX AddRootPath(const CStringX rootpath, const CStringX filename);
//檢查目錄
int MyCheckPath(const char *DirName);
//創建目錄
int MyCreatePath(const char *DirName);
//刪除目錄
int MyDeletePath(const char *DirName);
//取指定目錄下的文件數
int MyGetFileNum(const char *DirName, const char *FileName);
//取指定目錄、特性的文件名
int MyGetFileName(const char *DirName, const char *FileName, int MaxFileNum, VXML_GETFILEATTR_STRUCT *FileNames);
//檢查文件屬性
int MyCheckFileAttr(const char *FilePath, VXML_GETFILEATTR_STRUCT *FileNames);
//取文件大小
long MyCheckFileSize(const char *FilePath);
//檢查文件是否存在
int MyCheckFileExist(const char *FilePath);
//拷貝文件
int MyCopyFile(const char *sFileName, const char *dFileName);
int DeleteFiles(const char *dir);
int CreateAllDirectories(const char *dir);
int GetHDSpace(const char *drive, int &FreeSpace, int &TotalSpace, int &UserSpace);
#ifdef WIN32 //win32環境
BOOL TakeOwnership(LPCTSTR lpszFile);
BOOL SetPrivilege(HANDLE hToken,LPCTSTR lpszPrivilege,BOOL bChange);
BOOL SetPermission(LPCTSTR lpszFile, LPCTSTR lpszAccess, DWORD dwAccessMask);
#endif
int DeleteDir(LPCTSTR lpszName);
int CreateFullPathIfNotExist(const char *PathFileName);
//---------------------------------------------------------------------------
#endif
