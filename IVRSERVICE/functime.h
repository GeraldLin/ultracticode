//---------------------------------------------------------------------------
#ifndef FunctimeH
#define FunctimeH
//---------------------------------------------------------------------------
//---------------日期時間函數------------------------------------------------
int dtStr2time_t(const char *datetimestr, time_t tDateTime);
int dtStr2tm(const char *datetimestr, struct tm *tDateTime);
CH *time_t2str(time_t lt);
CH *time_t2HourMinstr(time_t lt);
CH *time_t2HourMinstr1(time_t lt);
CH *CTime2str(CTime tm);
//取當前日期時間(yyyy-mm-dd hh-mi-ss)
CH *MyGetNow(void);
CH *MyGetToday(void);
CH *MyGetNowEx(void);
UC MyGetNow( CH *Result );
//取當前日期(yyyy-mm-dd)
UC MyGetDate( CH *Result );
//取日期(yyyy-mm-dd)
UC MyGetDate( const CH *vDateTime, CH *Result );
//取當前時間(yyyy-mm-dd)
UC MyGetTime( CH *Result );
//取時間(yyyy-mm-dd)
UC MyGetTime( const CH *vDateTime, CH *Result );
//取當前時間串(hhmmss)
CH *MyGetTimeStr( void );
//取當前日期時間串(yyyymmddhhmmss)
CH *MyGetNowStr( void );
//取當前年(yyyy)
UC MyGetYear( US &Result );
//取年(yyyy)
UC MyGetYear( const CH *vDateTime, US &Result );
//取當前月(mm)
UC MyGetMonth( US &Result );
//取月(mm)
UC MyGetMonth( const CH *vDateTime, US &Result );
//取當前日(dd)
UC MyGetDay( US &Result );
//取日(dd)
UC MyGetDay( const CH *vDateTime, US &Result );
//取當前時(hh)
UC MyGetHour( US &Result );
//取時(hh)
UC MyGetHour( const CH *vDateTime, US &Result );
//取當前分(mi)
UC MyGetMinute( US &Result );
//取分(mi)
UC MyGetMinute( const CH *vDateTime, US &Result );
//取當前秒(ss)
UC MyGetSecond( US &Result );
//取秒(ss)
UC MyGetSecond( const CH *vDateTime, US &Result );
//取當前毫秒(ms)
UC MyGetMsecond( US &Result );
//取毫秒(ms)
UC MyGetMsecond( const CH *vDateTime, US &Result );
//取當前星期(ww)
UC MyGetWeekDay( US &Result );
//取星期(ww)
UC MyGetWeekDay( const CH *vDateTime, US &Result );
//日期時間合法性判斷
UC MyIsDateTime( const CH *vDateTime );
//取日期時間間隔
UC MyDTBetween( const CH *vDateTime1, const CH *vDateTime2, const UC vType, int &Result );
//取日期時間增量
UC MyIncDateTime( const CH *vDateTime, const UC vType, const int vNumber, CH *Result );
CH *MyIncDays(const CH *vDate, int nDays);
//日期時間范圍判斷
UC MyInDateTime( const CH *vDateTime, const CH *vDateTime1, const CH *vDateTime2 );
//啟動定時器
UC MyTimer( const UC vTimerId, const US vTimeLen );
bool IsLeapYear(US year);
US DaysInAMonth(US year, US month);
US DaysInAYear(US year);
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
