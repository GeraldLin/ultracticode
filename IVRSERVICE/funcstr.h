//---------------------------------------------------------------------------
#ifndef FuncstrH
#define FuncstrH
//---------------------------------------------------------------------------
//---------------才﹃ㄧ计--------------------------------------------------
//才﹃
US MyStrLen( const CH *vString );
//才﹃
CH *MySubString( const CH *vString, const US vStart, const US vLenght );
//奔才﹃ㄢ繷
CH *MyTrim( const CH *vString );
//奔才﹃オ
CH *MyLeftTrim( const CH *vString );
//奔才﹃娩
CH *MyRightTrim( const CH *vString );
//眖オ才﹃
CH *MyLeft( const CH *vString, const US vLenght );
//眖才﹃
CH *MyRight( const CH *vString, const US vLenght );
//才﹃
CH *MyStrAdd( const CH *vString1, const CH *vString2 );
//才﹃糶
CH *MyLower( const CH *vString );
//才﹃糶
CH *MyUpper( const CH *vString );
//才﹃竚
US MyStrPos( const CH *vString1, const CH *vString2 );
//ゑ耕才﹃
UC MyStrCmp( const CH *vString1, const CH *vString2 );
//┛菠糶ゑ耕才﹃
UC MyStrCmpNoCase( const CH *vString1, const CH *vString2 );
int SplitTxtLine(const char *txtline, int maxnum, CStringX *arrstring);
//沮だ瞒才だ澄才﹃
int SplitString(const char *txtline, const char splitchar, int maxnum, CStringX *arrstring);
bool IsStrMatch(const CStringX &Rule,const CStringX &code);
//---------------------------------------------------------------------------
int CheckIPv4Address(const char *pszIPAddr);
char *MyGetGUID();
void MyGetGUID(char *pszGUID);
char *GetParamByName(const char *paramstr, const char *paramname, const char *defaultval, int maxlen=255);
char *GetParamStrValue(LPCTSTR ParamName, LPCTSTR MsgBuffer);
char *GetParamStrValue1(LPCTSTR ParamName, LPCTSTR MsgBuffer);
int GetParamIntValue(LPCTSTR ParamName, LPCTSTR MsgBuffer);
//才﹃い把计ParamName-把计MsgBuffer-砆才﹃pszParamValue-才﹃(Τ秸ノだ皌ず璶256)return-true-赣把计false-ぃ赣把计
bool GetParamStrValue(LPCTSTR ParamName, LPCTSTR MsgBuffer, char *pszParamValue);
//才﹃い把计ParamName-把计MsgBuffer-砆才﹃lParamValue-俱计return-true-赣把计false-ぃ赣把计
bool GetParamIntValue(LPCTSTR ParamName, LPCTSTR MsgBuffer, long &lParamValue);
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult);
char *GetSIPPhoneCode(const char *SipUri);
char *GetEmailAddr(const char *EmailUri);

int AnsiToUnicode(const char *ansiStr, wchar_t *outUnicode);
int ConvUnicode2UniStr(WCHAR *pWchar, char *szUnicodeString, int iBuffSize);
int ConvAnsiStrToUniStr(const char *ansiStr, char *ansiUniStr);

//UNICODE才﹃锣传UNICODE计沮:锣传UNICODE计沮  
int ConvUniStr2Unicode(LPCSTR szUnicodeString, WCHAR *pWchar);
//unicode才﹃锣ansi才﹃:0Θ0ア毖
int ustr_astr( WCHAR *unicodestr, char *ansistr );
int ConvUniStrToAnsiStr(const char *ansiUniStr, char *ansiStr);

//URL絪絏
int URLEncode(LPCTSTR pszUrl, LPTSTR pszEncode, int nEncodeLen);
//URL秆絏
BOOL UrlDecode(const char* szSrc, char* pBuf, int cbBufLen);
//UTF8<->ANSI锣传ㄧ计 
void UTF8_ANSI_Convert(const char* strIn,char* strOut, int sourceCodepage, int targetCodepage);
//---------------------------------------------------------------------------
#endif
