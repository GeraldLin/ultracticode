//---------------------------------------------------------------------------
#ifndef ExternH
#define ExternH
//---------------------------------------------------------------------------
extern CMemvoc    *pMemvoc; //內存合成放音文件索引
extern CIVRCfg    *pIVRCfg; //IVR配置
extern CTCPLink   *pTcpLink; //通信消息類
extern CTCPAgServer  *pTcpAgLink; //ag通信消息類
extern CTCPClient *pDBClient;
extern CTCPHALink *pTCPHALink; //主備方式通信消息類
extern CFlwRule   *pFlwRule; //流程規則類
extern CMsgfifo   *RMsgFifo; //接收消息隊列
extern CParser    *pParser; //解析器
extern CBoxchn    *pBoxchn; //通道資源
extern CBoxconf   *pBoxconf; //會議資源
extern CCalloutQue *pCalloutQue; //呼出隊列
extern CCallout   *pCallbuf; //呼出緩沖
extern CAgentMng  *pAgentMng; //坐席管理類
extern CACDQueue  *pACDQueue; //ACD隊列分配
extern CVopGroup  *pVopGroup; //語音機資源
extern CSwitchPortList *pSwitchPortList; //交換機資源
extern CLogInOutFifo *pLogInOutFifo;
extern CSQLSfifo *pSQLSfifo;

extern CACDSplitMng gACDSplitMng;
extern CIVRDeptMng  gIVRDeptMng;
extern CIVRCallCountParamMng gIVRCallCountParamMng;

extern CXMLRcvMsg LogRcvMsg; //接收LOG服務器消息
extern CXMLRcvMsg IVRRcvMsg; //接收IVR服務器消息
extern CXMLMsg    SWTRcvMsg;
extern CXMLMsg    IVRSndMsg; //發送IVR消息
extern CXMLMsg    VOPSendMsg;
extern CXMLMsg    RecvDBMsg;

extern short g_DestIVRServerId;
extern short g_MasterIVRServerId;
extern short g_IVRServerId;
extern short g_nMultiRunMode; //雙機熱備方式
extern short g_nHostType; //本機類型：0-主機 1-備機
extern short g_nMasterStatus; //主機運行狀態：0-待機，1-運行
extern short g_nStandbyStatus; //備機運行狀態：0-待機，1-運行
extern char g_szSwitchTime[32];
extern unsigned short StandbyIVRClientId;
extern char g_szLocaIVRIP[32];
extern char g_szAdapterName[64];

extern short g_nLanDownHAMode;
extern short g_nLanDownCheckTimeLen;
extern short g_nCTILINKDownSwitchOverId; //雙機熱備方式時，CTI鏈路故障是否切換 2016-10-05
extern short g_nDBDownSwitchOverId; //雙機熱備方式時，數據庫故障是否切換 2016-10-05

extern CCriticalSection g_cDownCriticalSection;
extern CCriticalSection g_cHALinkCriticalSection;
extern CCriticalSection g_cMonLinkCriticalSection;

extern volatile bool g_bCTIHAServiceDown; //CTI備用機（或主用機）服務斷開標志
extern time_t g_nCTIHAServiceDownTimer; //CTI備用機（或主用機）服務斷開時間點

extern volatile bool g_bFLWServiceDown; //FLW服務斷開標志
extern time_t g_nFLWServiceDownTimer; //FLW服務斷開時間點

extern volatile bool g_bSWTServiceDown; //SWT服務斷開標志
extern time_t g_nSWTServiceDownTimer; //SWT服務斷開時間點

extern volatile bool g_bCTILinkDown; //CTI鏈路斷開標志
extern time_t g_nCTILinkDownTimer; //CTI鏈路斷開時間點

extern volatile bool g_bWSServiceDown; //WebSocket服務斷開標志
extern time_t g_nWSServiceDownTimer; //WebSocket服務斷開時間點

extern volatile bool g_bDBServiceDown; //DB服務斷開標志
extern time_t g_nDBServiceDownTimer; //DB服務斷開時間點

extern volatile bool g_bDBConnDown; //連接DB失敗標志
extern time_t g_nDBConnDownTimer; //連接DB失敗時間點

extern volatile bool g_bIVRChBlock; //IVR通道全部阻塞標志
extern time_t g_nIVRChBlockTimer; //IVR通道全部阻塞時間點
//---------------------------------------------------------------------------
extern bool g_bRunId;
extern bool g_bAlarmId;
extern bool g_bAlartId;

extern bool g_bDispId;
extern bool g_bSaveId;
extern bool g_bCommId;
extern bool g_bDebugId;
extern bool g_bDrvId;
extern bool g_bSQLId;
extern bool g_bLogSpecId;

extern bool g_bServiceDebugId;

extern short g_nSwitchMode;
extern short g_nSwitchType;
extern short g_nCardType;
extern short g_nCTILinkStatus;
extern short g_VOCInLinux;
extern bool g_bVoiceCardInit;
extern bool g_bChnInit;
extern short g_nUSBInitId;

//---------------------------------------------------------------------------
#define pChn ((pBoxchn->pChn_str) + nChn)
#define pChnn ((pBoxchn->pChn_str) + nChnn)
#define pChn1 ((pBoxchn->pChn_str) + nChn1)
#define pChn2 ((pBoxchn->pChn_str) + nChn2)

#define pAG  (pAgentMng->m_Agent[nAG])
#define pAGn (pAgentMng->m_Agent[nAGn])
#define pAG1 (pAgentMng->m_Agent[nAG1])
#define pAG2 (pAgentMng->m_Agent[nAG2])

#define pTrk ((pBoxchn->pTrk_str) + nTrk)
#define pCsc ((pBoxchn->pCsc_str) + nCsc)

#define pCfc ((pBoxconf->pCfc_str) + nCfc)

#define pOut ((pCallbuf->pOut_str) + nOut)

#define pQue ((pCalloutQue->pQue_str) + nQue)

#define pAcd ((pACDQueue->pACD_str) + nAcd)
#define pAcd1 ((pACDQueue->pACD_str) + nAcd1)
#define pPrior ((pACDQueue->pACD_str) + Prior)
#define pNext ((pACDQueue->pACD_str) + Next)

//extern int nChnn; //當前通道號
extern char SendMsgBuf[MAX_MSG_BUF_LEN]; //通訊消息發送緩沖區
extern VXML_XML_HEADER_STRUCT XMLHeader; //XML發送來的消息頭信息
extern char ErrMsg[MAX_MSG_BUF_LEN]; //錯誤消息緩沖區

extern int nLOGn;
extern unsigned short AGClientId; //當前坐席TCPLINK編號
extern unsigned long ACDIdn; //當前ACD序列號
extern CStringX AGErrMsg;

extern bool TTSLogId;
extern unsigned short TTSClientId;
extern bool CTILinkLogId;
extern unsigned short CTILinkClientId;
extern bool SWTConnectedId;
extern bool RECSystemLogId;
extern unsigned short RECSystemClientId;
extern bool WebSocketLogId;
extern unsigned short WebSocketClientId;
extern bool COMServiceLogId;
extern unsigned short COMServiceClientId;

extern bool TestPhoneLogId;
extern unsigned short TestPhoneClientId;

extern int g_nINIFileType;

extern char g_szUnimeINIFileName[MAX_PATH_FILE_LEN];
extern char g_szServiceINIFileName[MAX_PATH_FILE_LEN];
extern char g_szDBINIFileName[MAX_PATH_FILE_LEN]; //db配置文件
extern char g_szCTILINKINIFileName[MAX_PATH_FILE_LEN]; //CTILINK配置文件
extern char g_szMemVocINIFileName[MAX_PATH_FILE_LEN];

extern char g_szRootPath[MAX_PATH_LEN];
extern char g_szAppPath[MAX_PATH_LEN]; //執行文件路徑
extern char g_szLogPath[MAX_PATH_LEN]; 
extern char g_szCountPath[MAX_PATH_LEN]; //統計文件路徑

extern char g_szDBType[32];
extern int g_nDBType;
extern char g_szSQLNowFunc[16];

extern bool AuthPassId;
extern int AuthMaxTrunk;
extern int AuthMaxSeat;
extern int AuthMaxFax;
extern int AuthMaxSms;
extern int AuthMaxIVR;
extern int AuthMaxTTS;
extern int AuthMaxRECD;
extern int AuthCardType;
extern int AuthSwitchType;
extern char AuthEndDate[20];
extern char AuthKey[256];
extern char g_szFirstDate[20];
extern int g_dwRegRunDays;
extern char g_szLastDate[20];

extern int g_nCallInCount;
extern CChCount ChCount;
extern CChCount TodayChCount[48]; //當天話務統計數據

extern bool g_nSendTraceMsgToLOG;
extern CLogerStatusSet LogerStatusSet[MAX_LOGNODE_NUM];

extern int g_nWriteRegRunDog;
extern DWORD g_dwRegServerState;
extern DWORD g_DestCTIRunState;

extern CWebServiceParam g_WebServiceParam;

extern int g_nDefaultACWTimeLen;

extern LANGID g_LangID;

extern UL g_SetForwardingSessionNo; //會話序列號
extern UL g_SetForwardingCmdAddr; //當前指令行地址

extern CCriticalSection g_cTraceCriticalSection;
extern volatile int g_nExistThread;
extern CWinThread*	g_hIVRClientThrdProc;
extern CWinThread*	g_hAGServerThrdProc;
extern CWinThread*	g_hDBClientThrdProc;

extern bool g_bQueryLocaMobileNo;
extern int  g_nURIEncodeDecode;

extern int g_WebServerPort;
extern int g_WebServerTimeOut;
extern CHTTPServer g_WebServer;
extern CHTTPfifo   g_HTTPfifo;
extern CWinThread*	g_hHTTPThrdProc;
extern CWinThread*	g_hDbThrdProc;

extern char g_szAlarmSqliteDB[MAX_PATH_FILE_LEN]; //告警數據庫文件名

extern int g_nWebServiceSerialNo;

extern char g_szDBServer[32], g_szDBName[32], g_szDBUser[32], g_szDBPwd[64];
extern char g_szConnectionString[256];
extern int g_nConnectDbState; //0-未連接數據庫，1-連接成功，2-連接失敗
extern int g_nParserAccessCodeType;

//---------------------------------------------------------------------------
extern void WriteAlarmList(int nAlarmCode, int nAlarmLevel, int nAlarmState, LPCTSTR lpszFormat, ...);
extern void CheckMsgFifo();
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
extern int WriteRegRunTimerDog();
extern int WriteRegRunDays();
extern int WriteRegServiceRunState();
extern int CheckLocaNetCable(const char *adaptername);

extern bool isMyselfInActive();
extern bool GetCTIHALinkFlag();
extern void ClearIVRChBlockFlag();
extern void SetIVRChBlockFlag();
extern void CloseVoiceCard();

//---------------------------------------------------------------------------
#endif
