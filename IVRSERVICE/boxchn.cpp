//---------------------------------------------------------------------------
/*
通道類定義
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "boxchn.h"

extern short g_IVRServerId;
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
//---------------------------------------------------------------------------
int ChnNum30to32(int ChnNo)
{
  return ChnNo+(ChnNo/15)+1;
}

void CChn::GenerateCdrSerialNo()
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  sprintf(CdrSerialNo, "%04d%02d%02d%02d%02d%02d%d%03d%02d", 
    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
    local->tm_hour, local->tm_min, local->tm_sec,
    lgChnType, lgChnNo, g_IVRServerId);
  DBUpdateCallCDR_RingTimeId = 0;

  char szCallerNo[MAX_TELECODE_LEN], szCalledNo[MAX_TELECODE_LEN];
  int i, j, len;
  
  memset(szCallerNo, 0, MAX_TELECODE_LEN);
  memset(szCalledNo, 0, MAX_TELECODE_LEN);
  
  len = strlen(CallerNo);
  j = 0;
  for (i = 0; i < len; i ++)
  {
    if (CallerNo[i] >= '0' && CallerNo[i] <= '9')
    {
      szCallerNo[j] = CallerNo[i];
      j ++;
    }
  }
  
  len = strlen(CalledNo);
  j = 0;
  for (i = 0; i < len; i ++)
  {
    if (CalledNo[i] >= '0' && CalledNo[i] <= '9')
    {
      szCalledNo[j] = CalledNo[i];
      j ++;
    }
  }
  
  sprintf(RecordFileName, "%04d%02d%02d/%02d%02d%02d_%s_%s_%d%03d.wav", 
    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
    local->tm_hour, local->tm_min, local->tm_sec,
    szCallerNo, szCalledNo,
    lgChnType, lgChnNo);
}
void CChn::GenerateRecordFileName()
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  char szCallerNo[MAX_TELECODE_LEN], szCalledNo[MAX_TELECODE_LEN];
  int i, j, len;
  
  memset(szCallerNo, 0, MAX_TELECODE_LEN);
  memset(szCalledNo, 0, MAX_TELECODE_LEN);

  len = strlen(CallerNo);
  j = 0;
  for (i = 0; i < len; i ++)
  {
    if (CallerNo[i] >= '0' && CallerNo[i] <= '9')
    {
      szCallerNo[j] = CallerNo[i];
      j ++;
    }
  }
  
  len = strlen(CalledNo);
  j = 0;
  for (i = 0; i < len; i ++)
  {
    if (CalledNo[i] >= '0' && CalledNo[i] <= '9')
    {
      szCalledNo[j] = CalledNo[i];
      j ++;
    }
  }

  sprintf(RecordFileName, "%04d%02d%02d/%02d%02d%02d_%s_%s_%d%03d.wav", 
    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
    local->tm_hour, local->tm_min, local->tm_sec,
    szCallerNo, szCalledNo,
    lgChnType, lgChnNo);
}

void CChn::GenerateSerialNo()
{
  static UL sessionid=1;
  UL NodeType, NodeId;
  NodeType = NODE_SWT<<24;
  NodeId = 1 << 16;
  SerialNo = (UL)sessionid | (NodeType | NodeId);
  sessionid++;
  if (sessionid > 65535)
  {
    sessionid = 1;
  }
}

CBoxchn::CBoxchn()
{
  isInit = false;

  MaxChnNum = 0;
  
  MaxTrkNum = 0;
  MaxCscNum = 0;
  MaxRecNum = 0;
  MaxIvrNum = 0;
  MaxFxoNum = 0;
  
	lastnChn = 0;
	lastnTrk = 0;
	lastnCsc = 0;
	lastnRec = 0;

  TrkChnBusyCount = 0;
  TrkChnIdelCount = 0;
  TrkChnBlockCount = 0;
  TrkChnInCount = 0;
  TrkChnOutCount = 0;
  
  IvrChnBusyCount = 0;
  IvrChnIdelCount = 0;
  IvrChnBlockCount = 0;

  pChn_str = NULL;
  for (int i = 0; i < MAXCHGROUPNUM; i++)
		pIdx[i] = NULL;
  pTrk_str = NULL;
  pCsc_str = NULL;
  pRec_str = NULL;
  pVoip_str = NULL;
  pFax_str = NULL;
  pIvr_str = NULL;
  pIdelTrkChnQueue = NULL;

  isDTUseTs32 = 1;
  MaxRouteNum = 0;
  E1TrkSelMode = 0;
  TotalTrkNum = 0;
  TotalSeatNum = 0;
  TotalIvrNum = 0;
}

CBoxchn::~CBoxchn()
{
	if (pChn_str != NULL)
	{
		delete []pChn_str;
  	pChn_str = NULL;
  }
  for (int i = 0; i < MAXCHGROUPNUM; i++)
  {
		if(pIdx[i])
    {
			delete pIdx[i];
      pIdx[i] = NULL;
    }
  }

  if (pTrk_str != NULL)
  {
		delete []pTrk_str;
  	pTrk_str = NULL;
  }

  if (pCsc_str != NULL)
  {
		delete []pCsc_str;
  	pCsc_str = NULL;
  }

  if (pRec_str != NULL)
  {
		delete []pRec_str;
  	pRec_str = NULL;
  }

  if (pVoip_str != NULL)
  {
    delete []pVoip_str;
    pVoip_str = NULL;
  }
  
  if (pFax_str != NULL)
  {
    delete []pFax_str;
    pFax_str = NULL;
  }
  
  if (pIvr_str != NULL)
  {
    delete []pIvr_str;
    pIvr_str = NULL;
  }

  if (pIdelTrkChnQueue != NULL)
  {
    delete pIdelTrkChnQueue;
    pIdelTrkChnQueue = NULL;
  }
}

void CBoxchn::ReleaseAll()
{
  if (pChn_str != NULL)
  {
    delete []pChn_str;
    pChn_str = NULL;
  }
  for (int i = 0; i < MAXCHGROUPNUM; i++)
  {
    if(pIdx[i])
    {
      delete pIdx[i];
      pIdx[i] = NULL;
    }
  }
  
  if (pTrk_str != NULL)
  {
    delete []pTrk_str;
    pTrk_str = NULL;
  }
  
  if (pCsc_str != NULL)
  {
    delete []pCsc_str;
    pCsc_str = NULL;
  }
  
  if (pRec_str != NULL)
  {
    delete []pRec_str;
    pRec_str = NULL;
  }
  
  if (pVoip_str != NULL)
  {
    delete []pVoip_str;
    pVoip_str = NULL;
  }
  
  if (pFax_str != NULL)
  {
    delete []pFax_str;
    pFax_str = NULL;
  }
  
  if (pIvr_str != NULL)
  {
    delete []pIvr_str;
    pIvr_str = NULL;
  }
  
  if (pIdelTrkChnQueue != NULL)
  {
    delete pIdelTrkChnQueue;
    pIdelTrkChnQueue = NULL;
  }
  
  isInit = false;
  
  MaxChnNum = 0;
  
  MaxTrkNum = 0;
  MaxCscNum = 0;
  MaxRecNum = 0;
  MaxIvrNum = 0;
  MaxFaxNum = 0;
  
  isDTUseTs32 = 1;
  MaxRouteNum = 0;
  E1TrkSelMode = 0;
  TotalTrkNum = 0;
  TotalSeatNum = 0;
  TotalIvrNum = 0;
}

int CBoxchn::Init(int TotalChn )
{
	if (TotalChn <= 0) return 1;
  if (isInit == true) return 1;
	MaxChnNum = TotalChn;
  MaxTrkNum = TotalChn;
  MaxCscNum = TotalChn;
  MaxRecNum = TotalChn;
  MaxIvrNum = TotalChn;
  MaxFaxNum = TotalChn;

	pChn_str = new CChn[TotalChn];
  if ( pChn_str == NULL ) return 0x01;

	pTrk_str = new VXML_TRUNK_STRUCT[TotalChn];
  if ( pTrk_str == NULL ) return 0x02;

	pCsc_str = new VXML_SEAT_STRUCT[TotalChn];
	if ( pCsc_str == NULL ) return 0x03;

	pRec_str = new VXML_REC_STRUCT[TotalChn];
	if ( pRec_str == NULL ) return 0x04;

  pIvr_str = new VXML_IVR_STRUCT[TotalChn];
  if ( pIvr_str == NULL ) return 0x05;
  
  pFax_str = new VXML_FAX_STRUCT[TotalChn];
  if ( pFax_str == NULL ) return 0x06;
  
	isInit = true;
	for (int i = 0; i < MaxChnNum; i ++)
	{
		InitChn(i);
		
    InitTrunk(i);
    InitSeat(i);
    InitRec(i);
    InitIvr(i);
    InitFax(i);
	}
	return 0;
}
//設置通道數據
void CBoxchn::SetChData(int nChn, int chstyle, int chstyleindex, int chtype, int chindex, int lgchtype, int lgchindex)
{
  if (nChn >= MaxChnNum) return;
  pChn_str[nChn].state = 1;
  /*if (chstyle == CH_E_SS1 
    || chstyle == CH_E_TUP 
    || chstyle == CH_E_ISDN_U 
    || chstyle == CH_E_ISDN_N 
    || chstyle == CH_E_ISUP)
	  pChn_str[nChn].hwState = 0xFF;
  else
    pChn_str[nChn].hwState = 0;*/

	pChn_str[nChn].ChStyle = (UC)chstyle; //硬件邏輯通道類型
	pChn_str[nChn].ChStyleIndex = chstyleindex; //硬件邏輯通道類型
	pChn_str[nChn].ChType = (UC)chtype; //硬件邏輯通道類型
	pChn_str[nChn].ChIndex = chindex; //硬件邏輯通道號
	pChn_str[nChn].lgChnType = (UC)lgchtype; //業務通道類型
	pChn_str[nChn].lgChnNo = lgchindex; //業務通道號

}
void CBoxchn::SetChDeviceID(int nChn, const char *deviceid)
{
  if (isInit == false || nChn >= MaxChnNum || nChn < 0) return;
  strncpy(pChn_str[nChn].DeviceID, deviceid, MAX_TELECODE_LEN-1);
  MyTrace(3, "SetChDeviceID nChn=%d DeviceID=%s", nChn, pChn_str[nChn].DeviceID);
}
//初始化通道邏輯類型與通道序號對應關系指針 chtype-邏輯通道類型 chnnum-該類型的通道總數
int CBoxchn::InitChnIdx(int chtype, int chnnum)
{
  if (chtype >= MAXCHGROUPNUM || chnnum <= 0) return 1;
  pIdx[chtype] = new CCIdx(chnnum);
  if (pIdx[chtype] == NULL)
    return 2;
  return 0;
}
//設置硬件邏輯通道類型數據
void CBoxchn::SetChtypeData(int chtype, int chindex, int nchn)
{
  if (chtype >= MAXCHGROUPNUM) return;
  if (pIdx[chtype] == NULL) return;
  pIdx[chtype]->SetnChn(chindex, nchn);
}
//設置中繼通道
void CBoxchn::SetTrunkCh(int lgchindex, int nchn)
{
	pTrk_str[lgchindex].state = 1;
	pTrk_str[lgchindex].ChnNo = nchn;
}
//設置坐席通道
void CBoxchn::SetSeatCh(int lgchindex, int nchn)
{
	pCsc_str[lgchindex].state = 1;
	pCsc_str[lgchindex].ChnNo = nchn;
  pCsc_str[lgchindex].SeatNo.Format("%d", 800 + lgchindex);
  pCsc_str[lgchindex].SeatType = 1;
}
void CBoxchn::SetSeatCh(int lgchindex, int nchn, const char *seatno, int seattype)
{
  pCsc_str[lgchindex].state = 1;
  pCsc_str[lgchindex].ChnNo = nchn;
  pCsc_str[lgchindex].SeatNo = seatno;
  pCsc_str[lgchindex].SeatType = seattype;
}
//設置錄音通道
void CBoxchn::SetRecCh(int lgchindex, int nchn)
{
	pRec_str[lgchindex].state = 1;
	pRec_str[lgchindex].ChnNo = nchn;
}

void CBoxchn::SetIvrCh(int lgchindex, int nchn, CVopChn *pVopChn)
{
  pIvr_str[lgchindex].state = 1;
  pIvr_str[lgchindex].ChnNo = nchn;
  pIvr_str[lgchindex].cVopChn = pVopChn;
}

void CBoxchn::SetFaxCh(int lgchindex, int nchn)
{
  pFax_str[lgchindex].state = 1;
  pFax_str[lgchindex].ChnNo = nchn;
}

void CBoxchn::InitTrunk(int nTrk)
{
	if (isInit == false || nTrk >= MaxTrkNum) return;

	pTrk_str[nTrk].state = 0;
	pTrk_str[nTrk].ChnNo = 0xFFFF;
}

void CBoxchn::InitSeat(int nCsc)
{
	if (isInit == false || nCsc >= MaxCscNum) return;

	pCsc_str[nCsc].state = 0;
	pCsc_str[nCsc].ChnNo = 0xFFFF;
  pCsc_str[nCsc].SeatNo.Format("%d", 800 + nCsc);
}

void CBoxchn::InitRec(int nRec)
{
	if (isInit == false || nRec >= MaxRecNum) return;

	pRec_str[nRec].state = 0;
	pRec_str[nRec].ChnNo = 0xFFFF;
}

void CBoxchn::InitIvr(int nIvr)
{
  if (isInit == false || nIvr >= MaxIvrNum || nIvr < 0) return;
  
  pIvr_str[nIvr].state = 0;
  pIvr_str[nIvr].ChnNo = 0xFFFF;
  pIvr_str[nIvr].cVopChn = NULL;
}

void CBoxchn::InitFax(int nFax)
{
  if (isInit == false || nFax >= MaxFaxNum || nFax < 0) return;
  
  pFax_str[nFax].state = 0;
  pFax_str[nFax].ChnNo = 0xFFFF;
}

//-----------開始初始化邏輯通道------------------------------------------------
//清除收碼規則
void CBoxchn::ClearDtmfRule(int nChn, int DtmfRuleId)
{
	pChn_str[nChn].DtmfRule[DtmfRuleId].state = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].ruleid = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].digits.Empty();
	pChn_str[nChn].DtmfRule[DtmfRuleId].breakid = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].transtype = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].minlength = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].maxlength = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].timeout = 0;
	pChn_str[nChn].DtmfRule[DtmfRuleId].termdigits.Empty();
  pChn_str[nChn].DtmfRule[DtmfRuleId].onebitdigits.Empty();
  pChn_str[nChn].DtmfRule[DtmfRuleId].pbxpredigits.Empty();
  pChn_str[nChn].DtmfRule[DtmfRuleId].ValidDTMF.Empty(); //2015-06-29
	pChn_str[nChn].DtmfRule[DtmfRuleId].termid = 0;
  pChn_str[nChn].DtmfRule[DtmfRuleId].firstdigitdelay = 0;
  for (int i=0; i<10; i++)
  {
    pChn_str[nChn].DtmfRule[DtmfRuleId].specpredigitsmaxlength[i] = 0;
    pChn_str[nChn].DtmfRule[DtmfRuleId].specpredigits[i].Empty();
  }
}
//清除放音規則
void CBoxchn::ClearPlayRule(int nChn, int PlayRuleId)
{
	pChn_str[nChn].PlayRule[PlayRuleId].state = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].ruleid = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].format = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].repeat = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].duration = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].broadcast = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].volume = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].speech = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].voice = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].pause = 0;
	pChn_str[nChn].PlayRule[PlayRuleId].error = 0;
}
//初始化放音緩沖
void CBoxchn::ClearPlayData(int nChn)
{
	pChn_str[nChn].curPlayData.state = 0;

  pChn_str[nChn].curPlayData.CanPlayId = 0;
	pChn_str[nChn].curPlayData.PlayState = 0;
	pChn_str[nChn].curPlayData.PlayTypeId = 0;
	pChn_str[nChn].curPlayData.StartPos = 0;
	pChn_str[nChn].curPlayData.Length = 0;
	pChn_str[nChn].curPlayData.DataBufNum = 0;
	for (int i = 0; i < MAX_PLAY_INDEX_NUM; i ++)
	{
		pChn_str[nChn].curPlayData.PlayIndex[i].PlayType = 0;
		pChn_str[nChn].curPlayData.PlayIndex[i].PlayFormat = 0;
		pChn_str[nChn].curPlayData.PlayIndex[i].PlayFile.Empty();
		pChn_str[nChn].curPlayData.PlayIndex[i].PlayIndex = 0xFFFF;
    pChn_str[nChn].curPlayData.PlayIndex[i].repeat = 0; //2015-06-29
	}
  pChn_str[nChn].curPlayData.WaitTTSResult = 0;
  pChn_str[nChn].curPlayData.CurBufId = 0;
  pChn_str[nChn].curPlayData.StreamIndex = 0;
  pChn_str[nChn].curPlayData.TTSResult[0] = 0;
  pChn_str[nChn].curPlayData.TTSResult[1] = 0;
  pChn_str[nChn].curPlayData.StreamLen[0] = 0;
  pChn_str[nChn].curPlayData.StreamLen[1] = 0;

  pChn_str[nChn].curPlayData.PlayPoint = 0;
	pChn_str[nChn].curPlayData.PlayedTime = 0;
	pChn_str[nChn].curPlayData.PlayedLen = 0;
	pChn_str[nChn].curPlayData.PlayEndReleaseId = 0;
}
void CBoxchn::ClearAllDtmfPlayRule(int nChn)
{
	int i;
  //0號為默認收碼規則,不收碼
  pChn_str[nChn].DtmfRule[0].state = 1;
	pChn_str[nChn].DtmfRule[0].ruleid = 0;
  pChn_str[nChn].DtmfRule[0].digits.Empty();
  pChn_str[nChn].DtmfRule[0].breakid = 0;
	pChn_str[nChn].DtmfRule[0].transtype = 0;
  pChn_str[nChn].DtmfRule[0].minlength = 0;
  pChn_str[nChn].DtmfRule[0].maxlength = 0;
	pChn_str[nChn].DtmfRule[0].timeout = 0;
	pChn_str[nChn].DtmfRule[0].termdigits.Empty();
  pChn_str[nChn].DtmfRule[0].onebitdigits.Empty();
  pChn_str[nChn].DtmfRule[0].pbxpredigits.Empty();
  pChn_str[nChn].DtmfRule[0].ValidDTMF.Empty(); //2015-06-29
	pChn_str[nChn].DtmfRule[0].termid = 0;
  pChn_str[nChn].DtmfRule[0].firstdigitdelay = 0;
  for (i=0; i<10; i++)
  {
    pChn_str[nChn].DtmfRule[0].specpredigitsmaxlength[i] = 0;
    pChn_str[nChn].DtmfRule[0].specpredigits[i].Empty();
  }
	for (i = 1; i < MAX_DTMFRULE_BUF; i ++)
	{
		ClearDtmfRule(nChn, i);
	}
	for (i = 0; i <= MAX_PLAYRULE_BUF; i ++)
	{
		ClearPlayRule(nChn, i);
	}
  //0號為默認放音規則,放1遍音
  pChn_str[nChn].PlayRule[0].state = 1;
  pChn_str[nChn].PlayRule[0].repeat = 1; //放1遍
  
  //MAX_PLAYRULE_BUF號為默認放音規則,循環放音
  pChn_str[nChn].PlayRule[MAX_PLAYRULE_BUF].state = 1;
  pChn_str[nChn].PlayRule[MAX_PLAYRULE_BUF].repeat = 0;
}
//初始化錄音緩沖
void CBoxchn::ClearRecData(int nChn)
{
	pChn_str[nChn].curRecData.state = 0;
  pChn_str[nChn].curRecData.WaitRecdId = 0;
	pChn_str[nChn].curRecData.SessionNo = 0;
	pChn_str[nChn].curRecData.CmdAddr = 0;
  pChn_str[nChn].curRecData.lgChnType = 0;
  pChn_str[nChn].curRecData.lgChnNo = 0;
	pChn_str[nChn].curRecData.CanRecId = 0;
	pChn_str[nChn].curRecData.RecState = 0;
	pChn_str[nChn].curRecData.RecTypeId = 0;
	pChn_str[nChn].curRecData.Format = 0;
	pChn_str[nChn].curRecData.RecordFile.Empty();
	pChn_str[nChn].curRecData.TermChar.Empty();
	pChn_str[nChn].curRecData.StartPos = 0;
	pChn_str[nChn].curRecData.MaxTime = 0;
	pChn_str[nChn].curRecData.RecedLen = 0;
	pChn_str[nChn].curRecData.RecedByte = 0;
}
void CBoxchn::InitChn(int nChn)
{
	if (isInit == false || nChn >= MaxChnNum) return;

  pChn_str[nChn].curPlayData.WaitTTSResult = 0;
  pChn_str[nChn].curPlayData.CurBufId = 0;
  pChn_str[nChn].curPlayData.StreamIndex = 0;
  pChn_str[nChn].curPlayData.TTSResult[0] = 0;
  pChn_str[nChn].curPlayData.TTSResult[1] = 0;
  pChn_str[nChn].curPlayData.StreamLen[0] = 0;
  pChn_str[nChn].curPlayData.StreamLen[1] = 0;

  ResetChn(nChn);
	
  pChn_str[nChn].state = 0;
  pChn_str[nChn].CTILinkClientId = 0;
  memset(pChn_str[nChn].DeviceID, 0, MAX_TELECODE_LEN);
	pChn_str[nChn].ChStyle = 0;
	pChn_str[nChn].ChStyleIndex = 0;
	pChn_str[nChn].ChType = 0;
	pChn_str[nChn].ChIndex = 0;
	pChn_str[nChn].lgChnType = 0;
	pChn_str[nChn].lgChnNo = 0;
	pChn_str[nChn].nAG = 0xFFFF;
  pChn_str[nChn].RouteNo = 0;
  pChn_str[nChn].OverflowRouteNo = 0;
	//pChn_str[nChn].hwState = 0xFF;
  pChn_str[nChn].hwState = 0;
  pChn_str[nChn].ssState = 0;
  pChn_str[nChn].RelTime = 0;
  
  pChn_str[nChn].cVopChn = NULL;
  pChn_str[nChn].SeizeVopChnState = 0;
  pChn_str[nChn].VOC_TrunkId = 0;

  memset(pChn_str[nChn].CallInDtmfBuf, 0, MAX_DTMFBUF_LEN);
}

void CBoxchn::ResetChn(int nChn)
{
	int i;
	if (isInit == false || nChn >= MaxChnNum) return;

	pChn_str[nChn].timer = 0;
	pChn_str[nChn].PlayPauseTimer = 0;
	pChn_str[nChn].PlayedTimer = 0;
	pChn_str[nChn].DtmfPauseTimer = 0;
	pChn_str[nChn].RecTimer = 0;
	pChn_str[nChn].CalloutTimer = 0;
  pChn_str[nChn].WaitTTSTimer = 0;
  pChn_str[nChn].WaitDialTimer = 0;

  pChn_str[nChn].ConnID = 0;

  pChn_str[nChn].SessionNo = 0;
	pChn_str[nChn].CmdAddr = 0;

	pChn_str[nChn].lnState = 0;
	//pChn_str[nChn].ssState = CHN_SNG_DELAY;
  pChn_str[nChn].ssState = 0;
	pChn_str[nChn].svState = 0;
  pChn_str[nChn].swState = 0;

	pChn_str[nChn].CfcNo = 0xFFFF;
	pChn_str[nChn].JoinType = 0;
  pChn_str[nChn].JoinTime = 0;

	pChn_str[nChn].CallInOut = 0;
	pChn_str[nChn].CallInId = 0;
  pChn_str[nChn].CallOutRouteNo = 0;

	pChn_str[nChn].CalloutId = 0;
	pChn_str[nChn].nQue = 0xFFFF;
  pChn_str[nChn].nOut = 0xFFFF;
  pChn_str[nChn].DialSessionNo = 0;

  memset(pChn_str[nChn].CustPhone, 0, MAX_TELECODE_LEN);
  memset(pChn_str[nChn].CallerNo, 0, MAX_TELECODE_LEN);
  memset(pChn_str[nChn].CalledNo, 0, MAX_TELECODE_LEN);
  pChn_str[nChn].OrgCallerNo.Empty();
  pChn_str[nChn].OrgCalledNo.Empty();
  pChn_str[nChn].CalledPoint = 0;

  pChn_str[nChn].CallTime = 0;
  pChn_str[nChn].AnsTime = 0;

	for (i = 0; i < MAX_LINK_CHN_NUM; i ++)
	{
		pChn_str[nChn].LinkChn[i] = 0xFFFF;
		pChn_str[nChn].LinkType[i] = 0;
    pChn_str[nChn].SwtLinkChn[i] = 0xFFFF;
    pChn_str[nChn].SwtLinkType[i] = 0;
	}
	pChn_str[nChn].curPlayState = 0;
	pChn_str[nChn].curRecState = 0;
  memset(pChn_str[nChn].curRecvDtmf, 0, MAX_DTMFBUF_LEN);
  memset(pChn_str[nChn].RecvDialDtmf, 0, MAX_DTMFBUF_LEN); //add 2010-06-03
  pChn_str[nChn].RecvDialDtmfId = 0;

	pChn_str[nChn].Volume = 8;
	pChn_str[nChn].PlayBusFlag = 0;
	pChn_str[nChn].RecAGCFlag = 0;
	pChn_str[nChn].RecMixerFlag = 0;
	pChn_str[nChn].CanPlayOrRecId = 0;

	pChn_str[nChn].RecvDtmfId = 0;
	memset(pChn_str[nChn].RecvDtmfBuf, 0, MAX_DTMFBUF_LEN);
	//pChn_str[nChn].RecvAsrBuf.Empty();

	pChn_str[nChn].curSpeech = LANGUAGE_CHN;
  pChn_str[nChn].curPlayRuleId = 0xFFFF;
	pChn_str[nChn].curDtmfRuleId = 0xFFFF;
  pChn_str[nChn].PbxFirstDigitId = 0;
	//pChn_str[nChn].curAsrRuleId = 0xFFFF;

  pChn_str[nChn].RingCount = 0;
  pChn_str[nChn].FlwState.Empty();

  pChn_str[nChn].nAcd = 0xFFFF;
  pChn_str[nChn].CallData.Empty();
  memset(pChn_str[nChn].CustomNo, 0, MAX_CHAR64_LEN);
  memset(pChn_str[nChn].CdrSerialNo, 0, MAX_CHAR64_LEN);
  memset(pChn_str[nChn].RecordFileName, 0, MAX_CHAR128_LEN);
  
  pChn_str[nChn].DBUpdateCallCDR_RingTimeId = 0;
  pChn_str[nChn].IVRStartTime = 0;
  pChn_str[nChn].TranIVRId = 0;
  pChn_str[nChn].TranSessionNo = 0;
  pChn_str[nChn].TranCmdAddr = 0;
  pChn_str[nChn].TranAG = 0xFFFF;
  pChn_str[nChn].TranWaitTime = 0;
  
  pChn_str[nChn].TranStatus = 0;
  pChn_str[nChn].HoldingnChn = 0xFFFF;
  pChn_str[nChn].TranControlnChn = 0xFFFF;
  pChn_str[nChn].TranDestionnChn = 0xFFFF;
  pChn_str[nChn].TranOutForAgent = 0;

  pChn_str[nChn].nSwtPortID = -1;
  pChn_str[nChn].nCallType = 0;
  pChn_str[nChn].nAnswerId = 0;

  pChn_str[nChn].SerialNo = 0;
  
  pChn_str[nChn].WritedCallCDR_RelTimeId = 0;
  pChn_str[nChn].WritedCallCDR_WaitTimeId = 0;

  pChn_str[nChn].IncIVRInTranAGCountId = 0;
  pChn_str[nChn].IncIVRInTranAGAnsCountId = 0;
  pChn_str[nChn].IncIVRInTranAGAbandonCountId = 0;
  pChn_str[nChn].IVRInTranAGTime = 0;

  pChn_str[nChn].VOC_TrunkId_ex = 0;
  pChn_str[nChn].VOC_TrunkRecvCallEvent = 0;

  memset(pChn_str[nChn].ManDialOutId, 0, 32);
  memset(pChn_str[nChn].ManDialOutListId, 0, 64);
  memset(pChn_str[nChn].TranCdrSerialNo, 0, MAX_CHAR64_LEN);
  memset(pChn_str[nChn].TranRecordFileName, 0, MAX_CHAR128_LEN);

  memset(pChn_str[nChn].CRMCallId, 0, 64);
  pChn_str[nChn].ExtRecordReqFlag = 0; //2015-05-30

  memset(pChn_str[nChn].TranUUI, 0, MAX_CHAR128_LEN);
  memset(pChn_str[nChn].CallUCID, 0, MAX_CHAR128_LEN);
  memset(pChn_str[nChn].TrunkGroupNumber, 0, 32); //2016-11-22

  pChn_str[nChn].VocTrknAG = 0xFFFF; //2015-11-25 針對語音中繼的修改增加的參數：語音中繼呼出綁定的座席

  //2015-12-07記錄已分配過的坐席邏輯編號
  for (i = 0; i < MAX_AG_NUM; i ++)
  {
    pChn_str[nChn].AcdednAGList[i] = -1;
    pChn_str[nChn].GetAgentList[i] = -1;
  }
  pChn_str[nChn].GetAgentAcdPoint = -1;
  
  pChn_str[nChn].SetForwardingSessionNo = 0; //設置呼叫轉移會話序列號 2016-07-19
  pChn_str[nChn].SetForwardingCmdAddr = 0; //設置呼叫轉移當前指令行地址 2016-07-19

  pChn_str[nChn].HangonReason = 486; //2016-08-11針對SIP中繼增加掛機原因486 busy here 這里忙

  pChn_str[nChn].SWTACDTimeLen = 0; //2017-01-08

  //初始化放音緩沖
  ClearPlayData(nChn);
	//初始化錄音緩沖
  ClearRecData(nChn);

  ClearAllDtmfPlayRule(nChn);
}
void CBoxchn::ResetAllChn()
{
  for (int nChn = 0; nChn < MaxChnNum; nChn ++)
    ResetChn(nChn);
}
void CBoxchn::TimerAdd(int nTCount)
{
	for (int nChn = 0; nChn < MaxChnNum; nChn ++)
	{
		pChn_str[nChn].timer += nTCount;
		pChn_str[nChn].PlayPauseTimer += nTCount;
		pChn_str[nChn].PlayedTimer += nTCount;
		pChn_str[nChn].DtmfPauseTimer += nTCount;
		pChn_str[nChn].RecTimer += nTCount;
    pChn_str[nChn].WaitDialTimer += nTCount;
    pChn_str[nChn].CalloutTimer += nTCount;
	}
}
//根據業務通道類型、通道號取實際通道號
int CBoxchn::Get_nChn_By_LgChn(int chntype, int chnno)
{
  if (isInit == false || chnno >= MaxChnNum)
  {
	  return 0xFFFF;
  }
	if (chntype == CHANNEL_TRUNK)
	{
		if (pTrk_str[chnno].state == 1)
			return pTrk_str[chnno].ChnNo;
	}
	else if (chntype == CHANNEL_SEAT)
	{
		if (pCsc_str[chnno].state == 1)
			return pCsc_str[chnno].ChnNo;
	}
  else if (chntype == CHANNEL_IVR)
  {
    if (pIvr_str[chnno].state == 1)
      return pIvr_str[chnno].ChnNo;
  }
  else if (chntype == CHANNEL_REC)
  {
    if (pRec_str[chnno].state == 1)
      return pRec_str[chnno].ChnNo;
  }
  return 0xFFFF;
}
//根據業務通道類型、通道號取實際通道號(需要判斷通道狀態)
int CBoxchn::Get_ChnNo_By_LgChn(int chntype, int chnno, int &nChn)
{
	int ChnNo;
	ChnNo = Get_nChn_By_LgChn(chntype, chnno);
	nChn = ChnNo;
	if (isInit == false || ChnNo >= MaxChnNum) 
	{
		//超出范圍
		return 1;
	}

	if (pChn_str[ChnNo].state == 0)
	{
		//通道未開通
		return 2;
	}
	if (pChn_str[ChnNo].hwState != 0)
	{
		//通道硬件狀態錯誤
		return 3;
	}
	if (pChn_str[ChnNo].ssState == CHN_SNG_IDLE)
	{
		//通道未占用
		return 4;
	}
	return 0;
}
//根據語音卡邏輯通道類型、通道號取實際通道號
int CBoxchn::Get_ChnNo_By_CardChn(int chtype, int chindex)
{
  if (isInit == false)
  {
	  return 0xFFFF;
  }
  if (chtype >= MAXCHGROUPNUM) return 0xFFFF;
  if (pIdx[chtype] == NULL) return 0xFFFF;
  return pIdx[chtype]->GetnChn(chindex);
}
int CBoxchn::Get_ChnNo_By_ChStyle(int chstyle, int chstyleindex)
{
  for (int nChn = 0; nChn < MaxChnNum; nChn ++)
  {
    if (pChn_str[nChn].ChStyle == chstyle && pChn_str[nChn].ChStyleIndex == chstyleindex)
      return nChn;
  }
  return 0xFFFF;
}
//取通道類型、通道號
bool CBoxchn::Get_lgChn(int nChn, int &lgchntype, int &lgchnno)
{
  if (isInit == false || nChn >= MaxChnNum) 
  {
    lgchntype = lgchnno = 0;
    return false;
  }
  lgchntype = pChn_str[nChn].lgChnType;
  lgchnno = pChn_str[nChn].lgChnNo;
  return true;
}
//讀內線端口配置
void CBoxchn::Read_SeatNoIniFile(char *filename)
{
  int SeatNum, nCsc, nChnNo;	
  CH pzTemp[16], txtline[128];
  CStringX ArrString[2];

  SeatNum = GetPrivateProfileInt("SEAT","SeatNum", 0, filename);
  for (nCsc = 0; nCsc < SeatNum && nCsc < MaxCscNum; nCsc ++) //2017-01-14可能配置的坐席數比實際的物理通道數多
	{
		sprintf(pzTemp, "Seat[%d]", nCsc);
		GetPrivateProfileString("SEAT", pzTemp, "", txtline, 100, filename);
    if (SplitTxtLine(txtline, 2, ArrString) >= 2)
    {
      pCsc_str[nCsc].SeatNo = ArrString[0];
      pCsc_str[nCsc].SeatType = atoi(ArrString[1].C_Str());
    }
    nChnNo = pCsc_str[nCsc].ChnNo;
    strncpy(pChn_str[nChnNo].DeviceID, pCsc_str[nCsc].SeatNo.C_Str(), MAX_TELECODE_LEN-1);
    MyTrace(3, "SetChDeviceID nChn=%d DeviceID=%s", nChnNo, pChn_str[nChnNo].DeviceID);
	}
}
//讀路由數據
void CBoxchn::Read_Router(char *filename)
{
  CH pzTemp[32], pzRoute[512];
	int i, j, k, l, nFirst, nSecond, id, nChn;

	for (i = 0; i < MAX_ROUTE_NUM; i ++)
	{
    Routes[i].LastIndex = -1;
    Routes[i].TrkTotalNum = 0;
		Routes[i].E1TrkSelMode = 0;
    Routes[i].OldTrkBusyNum = -1;
    Routes[i].E1TrkSelMode = 0;
    memset(Routes[i].IPPreCode, 0, 20);
    for (j = 0; j < MAX_ROUTE_TRUNK_NUM; j ++)
		{
			Routes[i].TrkNoList[j] = 0xFFFF;
		}
    Routes[i].VOCTrunkId = 0;
    Routes[i].SIPRegMode = 0;
    memset(Routes[i].CallerSIPProxyIP, 0, 32);
    memset(Routes[i].CalledSIPProxyIP, 0, 32);
	}
 
  Routes[0].SIPRegMode = GetPrivateProfileInt("ROUTE", "SIPRegMode", 0, filename);
  GetPrivateProfileString("ROUTE", "CallerSIPProxyIP", "", Routes[0].CallerSIPProxyIP, 31, filename);
  GetPrivateProfileString("ROUTE", "CalledSIPProxyIP", "", Routes[0].CalledSIPProxyIP, 31, filename);

  isDTUseTs32 = GetPrivateProfileInt("ROUTE","DTUseTs32", 1, filename);
  E1TrkSelMode = GetPrivateProfileInt("ROUTE","SelMode", 0, filename);
  MaxRouteNum = GetPrivateProfileInt("ROUTE","RouteNum", 0, filename);
	if (MaxRouteNum > 0)
	{
  	//WriteLog("路由參數設置如下:");
		for (i = 1; i <= MaxRouteNum && i < MAX_ROUTE_NUM; i ++)
		{
			sprintf(pzTemp, "Route[%d].SelMode", i);
      Routes[i].E1TrkSelMode = GetPrivateProfileInt("ROUTE",pzTemp, 0, filename);
			
      sprintf(pzTemp, "Route[%d].IPPreCode", i);
			GetPrivateProfileString("ROUTE", pzTemp, "", Routes[i].IPPreCode, 20, filename);
      
      sprintf(pzTemp, "Route[%d].VOCTrunkId", i);
      Routes[i].VOCTrunkId = GetPrivateProfileInt("ROUTE",pzTemp, 0, filename);

      //2015-11-25
      sprintf(pzTemp, "Route[%d].OverflowRouteNo", i);
      Routes[i].OverflowRouteNo = GetPrivateProfileInt("ROUTE",pzTemp, 0, filename);

      sprintf(pzTemp, "Route[%d].SIPRegMode", i);
      Routes[i].SIPRegMode = GetPrivateProfileInt("ROUTE",pzTemp, 0, filename);

      sprintf(pzTemp, "Route[%d].CallerSIPProxyIP", i);
      GetPrivateProfileString("ROUTE", pzTemp, "", Routes[i].CallerSIPProxyIP, 31, filename);

      sprintf(pzTemp, "Route[%d].CalledSIPProxyIP", i);
      GetPrivateProfileString("ROUTE", pzTemp, "", Routes[i].CalledSIPProxyIP, 31, filename);
      
      sprintf(pzTemp, "Route[%d]", i);
      GetPrivateProfileString("ROUTE", pzTemp, "", pzRoute, 512, filename);
			
      memset(pzTemp, 0, 32);
			k = 0;
			l = 0;
			id = 0;
			nFirst = 0xFFFF;
			nSecond = 0xFFFF;
			if (strlen(pzRoute) > 0)
			{
				for (j = 0; j <= (int)strlen(pzRoute); j ++)
				{
					if (pzRoute[j] == ',' || pzRoute[j] == '\0')
					{
						if (k > 0)
						{
							nSecond = atoi(pzTemp);
							if (id == 0)
							{
								if (l < MAX_ROUTE_TRUNK_NUM)
								{
									Routes[i].TrkNoList[l] = nSecond;
                  
                  if (nSecond < MaxTrkNum)
                  {
					          nChn = pTrk_str[nSecond].ChnNo;
                    if (isnChnAvail(nChn))
                    {
                      pChn_str[nChn].RouteNo = i;
                      pChn_str[nChn].OverflowRouteNo = Routes[i].OverflowRouteNo; //2015-11-25
                    }
                  }
									
                  l ++;
									nFirst = nSecond;
								}
								else
								{
									break;
								}
							}
							else if (id == 1)
							{
								if (nFirst < nSecond)
								{
									for (nFirst = nFirst + 1; nFirst <= nSecond && l < MAX_ROUTE_TRUNK_NUM; l ++, nFirst++)
									{
										Routes[i].TrkNoList[l] = nFirst;
                    
                    if (nFirst < MaxTrkNum)
                    {
					            nChn = pTrk_str[nFirst].ChnNo;
                      if (isnChnAvail(nChn))
                      {
                        pChn_str[nChn].RouteNo = i;
                      }
                    }
									}
									nFirst = nSecond;
								}
								else
								{
									if (l < MAX_ROUTE_TRUNK_NUM)
									{
										Routes[i].TrkNoList[l] = nSecond;
                    if (nSecond < MaxTrkNum)
                    {
					            nChn = pTrk_str[nSecond].ChnNo;
                      if (isnChnAvail(nChn))
                      {
                        pChn_str[nChn].RouteNo = i;
                      }
                    }
										l ++;
										nFirst = nSecond;
									}
									else
									{
										break;
									}
								}
							}
						}
						k = 0;
						id = 0;
					}
					else if (pzRoute[j] == '-')
					{
						if (k > 0)
						{
							nSecond = atoi(pzTemp);
							if (id == 0)
							{
								if (l < MAX_ROUTE_TRUNK_NUM)
								{
									Routes[i].TrkNoList[l] = nSecond;
                  
                  if (nSecond < MaxTrkNum)
                  {
					          nChn = pTrk_str[nSecond].ChnNo;
                    if (isnChnAvail(nChn))
                    {
                      pChn_str[nChn].RouteNo = i;
                    }
                  }
									
                  l ++;
									nFirst = nSecond;
								}
								else
								{
									break;
								}
							}
						}
						k = 0;
						id = 1;
					}
					else if (pzRoute[j] >= '0' && pzRoute[j] <= '9')
					{
						if (k < 5)
						{
							pzTemp[k] = pzRoute[j];
							k ++;
							pzTemp[k] = '\0';
						}
						else
						{
							break;
						}
					}
					else if (pzRoute[j] != ' ')
					{
						//非法字符
						break;
					}
				}
			}
      Routes[i].TrkTotalNum = l; //該路由的中繼線總數
      MyTrace(3, "Read Routes[%d]: E1TrkSelMode=%d VOCTrunkId=%d TrkTotalNum=%d TrkList=%s", 
        i, Routes[i].E1TrkSelMode, Routes[i].VOCTrunkId, Routes[i].TrkTotalNum, pzRoute);
		}
	}
}
int CBoxchn::SetOneRoute(int routeno, int selmode, const char *pszChnList, const char *pszIPPreCode)
{
  CH pzTemp[32];
  int j, k, l, nFirst, nSecond, id, nChn;

  if (routeno >= MAX_ROUTE_NUM)
  {
    return 1;
  }
  Routes[routeno].LastIndex = -1;
  Routes[routeno].TrkTotalNum = 0;
  for (j = 0; j < MAX_ROUTE_TRUNK_NUM; j ++)
  {
    Routes[routeno].TrkNoList[j] = 0xFFFF;
  }
  
  Routes[routeno].E1TrkSelMode = (UC)selmode;
  strncpy(Routes[routeno].IPPreCode, pszIPPreCode, 18);
  memset(pzTemp, 0, 32);
	k = 0;
	l = 0;
	id = 0;
	nFirst = 0xFFFF;
	nSecond = 0xFFFF;
	if (strlen(pszChnList) > 0)
	{
		for (j = 0; j <= (int)strlen(pszChnList); j ++)
		{
			if (pszChnList[j] == ',' || pszChnList[j] == '\0')
			{
				if (k > 0)
				{
					nSecond = atoi(pzTemp);
					if (id == 0)
					{
						if (l < MAX_ROUTE_TRUNK_NUM)
						{
							Routes[routeno].TrkNoList[l] = nSecond;
              
              if (nSecond < MaxTrkNum)
              {
					      nChn = pTrk_str[nSecond].ChnNo;
                if (isnChnAvail(nChn))
                {
                  pChn_str[nChn].RouteNo = routeno;
                }
              }
							
              l ++;
							nFirst = nSecond;
						}
						else
						{
							break;
						}
					}
					else if (id == 1)
					{
						if (nFirst < nSecond)
						{
							for (nFirst = nFirst + 1; nFirst <= nSecond && l < MAX_ROUTE_TRUNK_NUM; l ++, nFirst++)
							{
								Routes[routeno].TrkNoList[l] = nFirst;
                
                if (nFirst < MaxTrkNum)
                {
					        nChn = pTrk_str[nFirst].ChnNo;
                  if (isnChnAvail(nChn))
                  {
                    pChn_str[nChn].RouteNo = routeno;
                  }
                }
							}
							nFirst = nSecond;
						}
						else
						{
							if (l < MAX_ROUTE_TRUNK_NUM)
							{
								Routes[routeno].TrkNoList[l] = nSecond;
                if (nSecond < MaxTrkNum)
                {
					        nChn = pTrk_str[nSecond].ChnNo;
                  if (isnChnAvail(nChn))
                  {
                    pChn_str[nChn].RouteNo = routeno;
                  }
                }
								l ++;
								nFirst = nSecond;
							}
							else
							{
								break;
							}
						}
					}
				}
				k = 0;
				id = 0;
			}
			else if (pszChnList[j] == '-')
			{
				if (k > 0)
				{
					nSecond = atoi(pzTemp);
					if (id == 0)
					{
						if (l < MAX_ROUTE_TRUNK_NUM)
						{
							Routes[routeno].TrkNoList[l] = nSecond;
              
              if (nSecond < MaxTrkNum)
              {
					      nChn = pTrk_str[nSecond].ChnNo;
                if (isnChnAvail(nChn))
                {
                  pChn_str[nChn].RouteNo = routeno;
                }
              }
							
              l ++;
							nFirst = nSecond;
						}
						else
						{
							break;
						}
					}
				}
				k = 0;
				id = 1;
			}
			else if (pszChnList[j] >= '0' && pszChnList[j] <= '9')
			{
				if (k < 5)
				{
					pzTemp[k] = pszChnList[j];
					k ++;
					pzTemp[k] = '\0';
				}
				else
				{
					break;
				}
			}
			else if (pszChnList[j] != ' ')
			{
				//非法字符
				break;
			}
		}
	}
  Routes[routeno].TrkTotalNum = l; //該路由的中繼線總數
  MyTrace(3, "Modify Routes[%d]: E1TrkSelMode=%d TrkTotalNum=%d TrkList=%s", 
    routeno, Routes[routeno].E1TrkSelMode, Routes[routeno].TrkTotalNum, pszChnList);
  return 0;
}
//根據坐席號取通道號
int CBoxchn::Get_IdelChnNo_By_SeatNo(const char *seatno)
{
  int nChn;
  for (int nCsc = 0; nCsc < MaxCscNum; nCsc ++)
  {
    if (pCsc_str[nCsc].SeatNo.Compare(seatno) == 0)
    {
		  nChn = pCsc_str[nCsc].ChnNo;
      if (isnChnAvail(nChn) && pChn_str[nChn].state == 1
        && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
        && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
        return nChn;
    }
  }
  return 0xFFFF;
}
int CBoxchn::Get_ChnNo_By_DeviceID(const char *deviceid)
{
  for (int nChn = 0; nChn < MaxChnNum; nChn ++)
  {
    if (strcmp(pChn_str[nChn].DeviceID, deviceid) == 0)
    {
      return nChn;
    }
  }
  return 0xFFFF;
}
int CBoxchn::Get_IdelIVRChnNo()
{
  static int last_nIvr = -1;
  int i, nChn = 0xFFFF, nIvr;
  
  for (i = 0; i < MaxChnNum; i ++)
  {
    nIvr = (last_nIvr + 1) % (MaxChnNum);
    last_nIvr = nIvr;
    if (pIvr_str[nIvr].state == 1)
    {
      nChn = pIvr_str[nIvr].ChnNo;
      if (isnChnAvail(nChn))
      {
        if (pChn_str[nChn].state == 1 && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == 0 
          && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
        {
          return nChn;
        }
      }
    }
  }
  return -1;
}
//根據路由號取空閑的中繼通道
int CBoxchn::Get_IdelChnNo_By_Route(int Route, UC &ChType, int &ChIndex)
{
  int i, nChn = 0xFFFF, nTrk;
  unsigned short Next;
	static int last_nTrk = -1, last_RouteIndex = -1;

	if (Route == 0)
	{
		if (E1TrkSelMode != 0)
    {
      for (i = 0; i < MaxChnNum; i ++)
		  {
			  last_nTrk = (last_nTrk + 1) % (MaxChnNum);
			  nTrk = last_nTrk;
			  if (pTrk_str[nTrk].state == 1)
			  {
				  nChn = pTrk_str[nTrk].ChnNo;
          if (isnChnAvail(nChn))
          {
				    ChType = pChn_str[nChn].ChType;
            ChIndex = pChn_str[nChn].ChIndex;
            if ((pChn_str[nChn].ChStyle == CH_E_SS1 || pChn_str[nChn].ChStyle == CH_E_TUP 
              || pChn_str[nChn].ChStyle == CH_E_ISDN_U || pChn_str[nChn].ChStyle == CH_E_ISDN_N 
              || pChn_str[nChn].ChStyle == CH_E_ISUP))
            {
              if (isDTUseTs32 == 0)
                ChIndex = ChnNum30to32(ChIndex);
              if ((ChIndex % 16) == 0)
                continue;
              if (E1TrkSelMode == 1)
              {
                if ((ChIndex % 2) == 0)
                  continue;
              }
              else if (E1TrkSelMode == 2)
              {
                if ((ChIndex % 2) == 1)
                  continue;
              }
            }
            if (pChn_str[nChn].state == 1 && pChn_str[nChn].lgChnType == 1 
              && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
              && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
				    {
						  MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d router = %d for callout", 
                nChn, ChType, ChIndex, Route);
					    pChn_str[nChn].CallOutRouteNo = Route;
              return nChn;
				    }
          }
			  }
		  }
    }
    //沒有找到規定的奇偶電路則找一個空閑的
		for (i = 0; i < MaxChnNum; i ++)
		{
			last_nTrk = (last_nTrk + 1) % (MaxChnNum);
			nTrk = last_nTrk;
			if (pTrk_str[nTrk].state == 1)
			{
				nChn = pTrk_str[nTrk].ChnNo;
        if (isnChnAvail(nChn))
        {
				  ChType = pChn_str[nChn].ChType;
          ChIndex = pChn_str[nChn].ChIndex;
          if ((pChn_str[nChn].ChStyle == CH_E_SS1 || pChn_str[nChn].ChStyle == CH_E_TUP 
            || pChn_str[nChn].ChStyle == CH_E_ISDN_U || pChn_str[nChn].ChStyle == CH_E_ISDN_N 
            || pChn_str[nChn].ChStyle == CH_E_ISUP))
          {
            if (isDTUseTs32 == 0)
              ChIndex = ChnNum30to32(ChIndex);
            if ((ChIndex % 16) == 0)
              continue;
          }
          if (pChn_str[nChn].state == 1 && pChn_str[nChn].lgChnType == 1 
            && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
            && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
				  {
						MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d router = %d for callout", 
              nChn, ChType, ChIndex, Route);
            pChn_str[nChn].CallOutRouteNo = Route;
					  return nChn;
				  }
        }
			}
		}
	}
	else if (Route > 0)
	{
		if (Routes[Route].E1TrkSelMode > 0 && Routes[Route].E1TrkSelMode <= 3)
    {
      if (Routes[Route].E1TrkSelMode == 3)
      {
        last_RouteIndex = -1;
      }
      else
      {
        last_RouteIndex = Routes[Route].LastIndex;
      }
      for (i = 0; i < Routes[Route].TrkTotalNum && i < MAX_ROUTE_TRUNK_NUM; i ++)
		  {
			  last_RouteIndex = (last_RouteIndex + 1) % Routes[Route].TrkTotalNum; //MAX_ROUTE_TRUNK_NUM;
        Routes[Route].LastIndex = last_RouteIndex;
			  nTrk = Routes[Route].TrkNoList[last_RouteIndex];
			  if (isnTrkAvail(nTrk))
			  {
				  if (pTrk_str[nTrk].state == 1)
				  {
					  nChn = pTrk_str[nTrk].ChnNo;
            if (isnChnAvail(nChn))
            {
              ChType = pChn_str[nChn].ChType;
              ChIndex = pChn_str[nChn].ChIndex;
              if ((pChn_str[nChn].ChStyle == CH_E_SS1 || pChn_str[nChn].ChStyle == CH_E_TUP 
                || pChn_str[nChn].ChStyle == CH_E_ISDN_U || pChn_str[nChn].ChStyle == CH_E_ISDN_N 
                || pChn_str[nChn].ChStyle == CH_E_ISUP))
              {
                if (isDTUseTs32 == 0)
                  ChIndex = ChnNum30to32(ChIndex);
                if ((ChIndex % 16) == 0)
                  continue; //不要占數字中繼的0和16時隙
                if (Routes[Route].E1TrkSelMode == 1)
                {
                  if ((ChIndex % 2) == 0)
                    continue;
                }
                else if (Routes[Route].E1TrkSelMode == 2)
                {
                  if ((ChIndex % 2) == 1)
                    continue;
                }
              }
					    if (pChn_str[nChn].state == 1 && pChn_str[nChn].lgChnType == 1 
                && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
                && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
					    {
						    MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d router = %d for callout", 
                  nChn, ChType, ChIndex, Route);
                pChn_str[nChn].CallOutRouteNo = Route;
                return nChn;
					    }
            }
				  }
			  }
		  }
    }
    else if (Routes[Route].E1TrkSelMode == 4) //按固定逆序選空閑的電路
    {
      for (i = (Routes[Route].TrkTotalNum-1); i >= 0; i --)
      {
        nTrk = Routes[Route].TrkNoList[i];
        if (isnTrkAvail(nTrk))
        {
          if (pTrk_str[nTrk].state == 1)
          {
            nChn = pTrk_str[nTrk].ChnNo;
            if (isnChnAvail(nChn))
            {
              ChType = pChn_str[nChn].ChType;
              ChIndex = pChn_str[nChn].ChIndex;
              if ((pChn_str[nChn].ChStyle == CH_E_SS1 || pChn_str[nChn].ChStyle == CH_E_TUP 
                || pChn_str[nChn].ChStyle == CH_E_ISDN_U || pChn_str[nChn].ChStyle == CH_E_ISDN_N 
                || pChn_str[nChn].ChStyle == CH_E_ISUP))
              {
                if (isDTUseTs32 == 0)
                  ChIndex = ChnNum30to32(ChIndex);
                if ((ChIndex % 16) == 0)
                  continue; //不要占數字中繼的0和16時隙
              }
              if (pChn_str[nChn].state == 1 && pChn_str[nChn].lgChnType == 1 
                && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
                && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
              {
                MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d router = %d for callout", 
                  nChn, ChType, ChIndex, Route);
                pChn_str[nChn].CallOutRouteNo = Route;
                return nChn;
              }
            }
          }
        }
      }
    }
    else if (Routes[Route].E1TrkSelMode == 5) //選擇空閑時間最長電路
    {
      Next = pIdelTrkChnQueue->m_pIdelTrkNo[0].Next;
      while (pIdelTrkChnQueue->isnQueAvail(Next))
      {
        nTrk = pIdelTrkChnQueue->m_pIdelTrkNo[Next].TrkNo;
        for (i = 0; i < Routes[Route].TrkTotalNum && i < MAX_ROUTE_TRUNK_NUM; i ++)
        {
          if (nTrk == Routes[Route].TrkNoList[i])
          {
            if (isnTrkAvail(nTrk))
            {
              if (pTrk_str[nTrk].state == 1)
              {
                nChn = pTrk_str[nTrk].ChnNo;
                if (isnChnAvail(nChn))
                {
                  ChType = pChn_str[nChn].ChType;
                  ChIndex = pChn_str[nChn].ChIndex;
                  if ((pChn_str[nChn].ChStyle == CH_E_SS1 || pChn_str[nChn].ChStyle == CH_E_TUP 
                    || pChn_str[nChn].ChStyle == CH_E_ISDN_U || pChn_str[nChn].ChStyle == CH_E_ISDN_N 
                    || pChn_str[nChn].ChStyle == CH_E_ISUP))
                  {
                    if (isDTUseTs32 == 0)
                      ChIndex = ChnNum30to32(ChIndex);
                    if ((ChIndex % 16) == 0)
                      continue; //不要占數字中繼的0和16時隙
                  }
                  if (pChn_str[nChn].state == 1 && pChn_str[nChn].lgChnType == 1 
                    && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
                    && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
                  {
                    MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d router = %d for callout", 
                      nChn, ChType, ChIndex, Route);
                    pChn_str[nChn].CallOutRouteNo = Route;
                    return nChn;
                  }
                }
              }
            }
          }
        }
        Next = pIdelTrkChnQueue->m_pIdelTrkNo[Next].Next;
      } //end while
    } //end if
    //沒有找到規定的奇偶電路則找一個空閑的
    if (Routes[Route].E1TrkSelMode == 3)
    {
      last_RouteIndex = -1;
    }
    else
    {
      last_RouteIndex = Routes[Route].LastIndex;
    }
		for (i = 0; (i < Routes[Route].TrkTotalNum && i < MAX_ROUTE_TRUNK_NUM); i ++)
		{
			last_RouteIndex = (last_RouteIndex + 1) % Routes[Route].TrkTotalNum; //MAX_ROUTE_TRUNK_NUM;
      Routes[Route].LastIndex = last_RouteIndex;
			nTrk = Routes[Route].TrkNoList[last_RouteIndex];
			if (isnTrkAvail(nTrk))
			{
				if (pTrk_str[nTrk].state == 1)
				{
					nChn = pTrk_str[nTrk].ChnNo;
          if (isnChnAvail(nChn))
          {
            ChType = pChn_str[nChn].ChType;
            ChIndex = pChn_str[nChn].ChIndex;
            if ((pChn_str[nChn].ChStyle == CH_E_SS1 || pChn_str[nChn].ChStyle == CH_E_TUP 
              || pChn_str[nChn].ChStyle == CH_E_ISDN_U || pChn_str[nChn].ChStyle == CH_E_ISDN_N 
              || pChn_str[nChn].ChStyle == CH_E_ISUP))
            {
              if (isDTUseTs32 == 0)
                ChIndex = ChnNum30to32(ChIndex);
              if ((ChIndex % 16) == 0)
                continue;
            }
					  if (pChn_str[nChn].state == 1 && pChn_str[nChn].lgChnType == 1 
              && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
              && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
					  {
						  MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d router = %d for callout", 
                nChn, ChType, ChIndex, Route);
              pChn_str[nChn].CallOutRouteNo = Route;
              return nChn;
					  }
          }
				}
			}
		}
	}
  MyTrace(3, "All trk is busy at router = %d", Route);
	return 0xFFFF;
}
//根據通道號取業務通道類型及通道號
int CBoxchn::GetLgChBynChn(int nChn, int &lgchtype, int &lgchnno)
{
  if (isnChnAvail(nChn))
  {
    lgchtype = pChn_str[nChn].lgChnType;
    lgchnno = pChn_str[nChn].lgChnNo;
    return 0;
  }
  else
  {
    lgchtype = 0;
    lgchnno = 0;
    return 1;
  }
}
int CBoxchn::Get_IdelChnNo_By_LgChn(int lgchntype, int lgchnno, UC &ChType, int &ChIndex)
{
  int nChn;
  
  nChn = Get_nChn_By_LgChn(lgchntype, lgchnno);
  if (isnChnAvail(nChn))
  {
    ChType = pChn_str[nChn].ChType;
    ChIndex = pChn_str[nChn].ChIndex;
		if (pChn_str[nChn].state == 1
      && pChn_str[nChn].hwState == 0 && pChn_str[nChn].lnState == CHN_LN_IDLE
      && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
		{
			MyTrace(3, "Get idel nChn = %d chtype=%d chindex = %d for callout", 
        nChn, ChType, ChIndex);
      return nChn;
		}
  }
  MyTrace(3, "The chn is busy ChnType = %d ChnNo = %d", lgchntype, lgchnno);
  return 0xFFFF;
}
int CBoxchn::isTheChIdel(int nChn, bool bACDToAgentWhenChBlock)
{
  if (isnChnAvail(nChn))
  {
    if (pChn_str[nChn].state == 1
      && (bACDToAgentWhenChBlock == true || pChn_str[nChn].hwState == 0)
      && pChn_str[nChn].lnState == CHN_LN_IDLE
      && pChn_str[nChn].ssState == 0 && pChn_str[nChn].CalloutId == 0)
    {
      MyTrace(3, "isTheChIdel nChn=%d state=%d hwState=%d lnState=%d ssState=%d CalloutId=%d idel",
        nChn, pChn_str[nChn].state, pChn_str[nChn].hwState, pChn_str[nChn].lnState, pChn_str[nChn].ssState, pChn_str[nChn].CalloutId);
      return 0; //空閑
    }
    else
    {
      MyTrace(3, "isTheChIdel nChn=%d state=%d hwState=%d lnState=%d ssState=%d CalloutId=%d busy",
        nChn, pChn_str[nChn].state, pChn_str[nChn].hwState, pChn_str[nChn].lnState, pChn_str[nChn].ssState, pChn_str[nChn].CalloutId);
      return 1; //忙
    }
  }
  return 2; //不存在該通道
}
void CBoxchn::GenerateCdrSerialNo(int nChn)
{
  if (isnChnAvail(nChn))
  {
    struct tm *local;
    time_t lt;
    lt=time(NULL);
    local=localtime(&lt);

    sprintf(pChn_str[nChn].CdrSerialNo, "%04d%02d%02d%02d%02d%02d%d%03d%02d", 
      local->tm_year+1900, local->tm_mon+1, local->tm_mday,
      local->tm_hour, local->tm_min, local->tm_sec,
      pChn_str[nChn].lgChnType, pChn_str[nChn].lgChnNo, g_IVRServerId);
  }
}
void CBoxchn::GenerateRecordFileName(int nChn)
{
  if (isnChnAvail(nChn))
  {
    struct tm *local;
    time_t lt;
    lt=time(NULL);
    local=localtime(&lt);
    
    char szCallerNo[MAX_TELECODE_LEN], szCalledNo[MAX_TELECODE_LEN];
    int i, j, len;
    
    memset(szCallerNo, 0, MAX_TELECODE_LEN);
    memset(szCalledNo, 0, MAX_TELECODE_LEN);
    
    len = strlen(pChn_str[nChn].CallerNo);
    j = 0;
    for (i = 0; i < len; i ++)
    {
      if (pChn_str[nChn].CallerNo[i] >= '0' && pChn_str[nChn].CallerNo[i] <= '9')
      {
        szCallerNo[j] = pChn_str[nChn].CallerNo[i];
        j ++;
      }
    }
    
    len = strlen(pChn_str[nChn].CalledNo);
    j = 0;
    for (i = 0; i < len; i ++)
    {
      if (pChn_str[nChn].CalledNo[i] >= '0' && pChn_str[nChn].CalledNo[i] <= '9')
      {
        szCalledNo[j] = pChn_str[nChn].CalledNo[i];
        j ++;
      }
    }

    sprintf(pChn_str[nChn].RecordFileName, "%04d%02d%02d/%02d%02d%02d_%s_%s_%d%03d.wav", 
      local->tm_year+1900, local->tm_mon+1, local->tm_mday,
      local->tm_hour, local->tm_min, local->tm_sec,
      szCallerNo, szCalledNo,
      pChn_str[nChn].lgChnType, pChn_str[nChn].lgChnNo);
  }
}
void CBoxchn::ClearnSwtPortID(int nPortID)
{
  for (int nChn = 0; nChn < MaxChnNum; nChn ++)
  {
    if (pChn_str[nChn].nSwtPortID == nPortID)
      pChn_str[nChn].nSwtPortID = -1;
  }
}
bool CBoxchn::IsAllTrkChnAct()
{
  bool bAct=true;
  for (int nChn = 0; nChn < MaxChnNum; nChn ++)
  {
    if (pChn_str[nChn].lgChnType == CHANNEL_TRUNK && pChn_str[nChn].hwState != 0)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
