//---------------------------------------------------------------------------
#include "stdafx.h"
#include "proclog.h"
#include "extern.h"
//---------------------------------------------------------------------------
//發送消息到坐席
void SendMsg2LOG(int nLOG, US MsgId, const CH *msg)
{
  pTcpLink->SendMessage2CLIENT((NODE_LOG<<8)|LogerStatusSet[nLOG].LogerClientId, (MSGTYPE_IVRLOG<<8) | MsgId, msg);
  if (MsgId == LOGMSG_onivronoff || MsgId == LOGMSG_onharunstatus) // || MsgId == LOGMSG_ongetagentstatus)
  {
    MyTrace(2, "%s", msg);
  }
}
void SendMsg2LOG(int nLOG, US MsgId, const CStringX &msg)
{
  SendMsg2LOG(nLOG, MsgId, msg.C_Str());
}
int Proc_Msg_From_LOG(CXMLRcvMsg &LOGMsg)
{
  unsigned short MsgId=LOGMsg.GetMsgId();
  
  if(MsgId >= MAX_LOGIVRMSG_NUM)	//ag-->ivr
  {
    //指令編號超出范圍
    MyTrace(0, "LOGmsgid = %d is out of range\n", MsgId);
    return 1;
  }
  
  if(0!=LOGMsg.ParseSndMsgWithCheck(pFlwRule->LOGIVRMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(0, "parse LOGER msg error: MsgId = %d MsgBuf = %s\n", MsgId, LOGMsg.GetBuf().C_Str());
    return 2;
  }
  return 0;
}
//取AG消息規定頭
void Get_LOGMSG_HeaderValue(CXMLRcvMsg &LOGMsg)
{
  nLOGn = 0;
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].LogerClientId == atoi(LOGMsg.GetAttrValue(0).C_Str()))
      nLOGn = nLOG;
  }
}
//設置發送到AG的消息頭
void Set_IVR2LOG_Header(int MsgId)
{
  IVRSndMsg.GetBuf().Format("<%s logid='%d'", pFlwRule->IVRLOGMsgRule[MsgId].MsgNameEng, LogerStatusSet[nLOGn].LogerClientId);
}
void Set_IVR2LOG_Header(int MsgId, int nLOG)
{
  IVRSndMsg.GetBuf().Format("<%s logid='%ld'", pFlwRule->IVRLOGMsgRule[MsgId].MsgNameEng, LogerStatusSet[nLOG].LogerClientId);
}
//增加發送到AG的消息屬性
void Set_IVR2LOG_Item(int MsgId, int AttrId, const char *value)
{
  IVRSndMsg.AddItemToBuf(pFlwRule->IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2LOG_Item(int MsgId, int AttrId, const CStringX &value)
{
  IVRSndMsg.AddItemToBuf(pFlwRule->IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2LOG_IntItem(int MsgId, int AttrId, int value)
{
  IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2LOG_LongItem(int MsgId, int AttrId, long value)
{
  IVRSndMsg.AddLongItemToBuf(pFlwRule->IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_IVR2LOG_BoolItem(int MsgId, int AttrId, bool value)
{
  if (value == true)
  {
    IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, 1);
  }
  else
  {
    IVRSndMsg.AddIntItemToBuf(pFlwRule->IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, 0);
  }
}
void Set_IVR2LOG_Tail()
{
  IVRSndMsg.AddTailToBuf();
}

void SendAGStateToLOG(int nAG, int nLOG)
{
  char szTemp[128];
  int nChn;
  if (pAgentMng->isnAGAvail(nAG) && nLOG < MAX_LOGNODE_NUM)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAgStatus == true)
    {
      Set_IVR2LOG_Header(LOGMSG_ongetagentstatus, nLOG);
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 1, nAG);
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 2, pAG->GetSeatNo());
      if (pAG->GetClientId() == 0x06FF)
      {
        Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 3, "");
      }
      else
      {
        if (pAG->GetClientId() & 0x8000)
        {
          Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 3, pAG->GetClientId()&0x7FFF);
        } 
        else
        {
          Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 3, pAG->GetClientId()&0x00FF);
        }
      }
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 4, pAG->GetSeatType());
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 5, pAG->GetSeatGroupNo());
      
      if (pAG->m_Seat.BandIP.GetLength() == 0)
      {
        if (pAG->m_Seat.SeatIP.GetLength() == 0)
        {
          Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 6, "");
        }
        else
        {
          sprintf(szTemp, "SeatIP=%s", pAG->m_Seat.SeatIP.C_Str());
          Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 6, szTemp);
        }
      } 
      else
      {
        if (pAG->m_Seat.SeatIP.GetLength() == 0)
        {
          sprintf(szTemp, "BandIP=%s;PhoneIP=%s", pAG->m_Seat.BandIP.C_Str(), pAG->m_Seat.IPPhoneBandIP.C_Str());
          Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 6, szTemp);
        }
        else
        {
          sprintf(szTemp, "BandIP=%s;SeatIP=%s;PhoneIP=%s", pAG->m_Seat.BandIP.C_Str(), pAG->m_Seat.SeatIP.C_Str(), pAG->m_Seat.IPPhoneBandIP.C_Str());
          Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 6, szTemp);
        }
      }

      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 7, pAG->GetWorkerNo());
      if (pAG->GetWorkerNo() == 0)
      {
        if (pAG->m_Worker.LogoutTime == 0)
        {
          Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 8, "");
        }
        else
        {
          Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 8, time_t2str(pAG->m_Worker.LogoutTime));
        }
      } 
      else
      {
        Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 8, time_t2str(pAG->m_Worker.LoginTime));
      }
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 9, pAG->GetWorkerName());
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 10, pAgentMng->GetGroupNoList(nAG));
      if (pAG->DelayState == 0 || pAG->svState > 0)
      {
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 11, pAG->svState);
      } 
      else if (pAG->DelayState == 1)
      {
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 11, AG_SV_ACW);
      }
      else
      {
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 11, AG_SV_DELAY);
      }
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 12, pAG->m_Worker.DisturbId);
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 13, pAG->m_Worker.LeaveId);
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 14, pAG->m_Worker.WorkerCallCountParam.CallInAnsCount);
      Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 15, pAG->m_Worker.WorkerCallCountParam.CallInNoAnsCount);
      
      if (pAG->m_Worker.DisturbId == 3)
      {
        if (pAG->m_Seat.SeatLangID == 1)
          sprintf(szTemp, "外出值班電話:%s", pAG->m_Worker.DutyPhone.C_Str());
        else if (pAG->m_Seat.SeatLangID == 2)
          sprintf(szTemp, "���|�G痁?杠:%s", pAG->m_Worker.DutyPhone.C_Str());
        else
          sprintf(szTemp, "GoOutDutyTel:%s", pAG->m_Worker.DutyPhone.C_Str());
      }
      else
      {
        sprintf(szTemp, "%s", pAG->m_Worker.LeaveReason.C_Str());
      }
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 16, szTemp);
      nChn = pAG->m_Seat.nChn;
      if (pBoxchn->isnChnAvail(nChn))
      {
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 17, pChn->lgChnType);
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 18, pChn->lgChnNo);
      }
      else
      {
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 17, 0);
        Set_IVR2LOG_IntItem(LOGMSG_ongetagentstatus, 18, 0);
      }
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 19, pAG->m_Worker.AccountNo);
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 20, pAG->m_Worker.DepartmentNo);
      
      nChn = pAG->GetSeatnChn();
      memset(szTemp, 0, 128);
      if (pBoxchn->isnChnAvail(nChn))
      {
        if (pAG->svState != 0)
        {
          if (pChn->CallInOut == 1)
          {
            sprintf(szTemp, "%d,%s,%s,%d,", pChn->CallInOut, pChn->CallerNo, time_t2str(pChn->CallTime), pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
          }
          else if (pChn->CallInOut == 2)
          {
            sprintf(szTemp, "%d,%s,%s,%d,", pChn->CallInOut, pChn->CalledNo, time_t2str(pChn->CallTime), pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
          }
          else
          {
            sprintf(szTemp, "0,,,%d,", pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
          }
        }
        else
        {
          sprintf(szTemp, "0,,,%d,", pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
        }
      }
      else
      {
        sprintf(szTemp, "0,,,%d,", pAG->m_Worker.WorkerCallCountParam.CurStatusStartTime);
      }
      Set_IVR2LOG_Item(LOGMSG_ongetagentstatus, 21, szTemp);

      Set_IVR2LOG_Tail();
      
      SendMsg2LOG(nLOG, LOGMSG_ongetagentstatus, IVRSndMsg.GetBuf());
    }
  }
}
void SendAllAGStateToLOG(int nLOG)
{
  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    SendAGStateToLOG(nAG, nLOG);
  }
}
void SendChStateToLOG(int nChn, int nLOG)
{
  Set_IVR2LOG_Header(LOGMSG_ongetchnstatus, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 1, nChn);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 2, pChn->ChStyle);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 3, pChn->ChStyleIndex);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 4, pChn->lgChnType);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 5, pChn->lgChnNo);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 6, pChn->hwState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 7, pChn->lnState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 8, pChn->ssState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetchnstatus, 9, pChn->CallInOut);
  Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 10, pChn->CallerNo);
  Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 11, pChn->CalledNo);
  //呼叫時間
  if (pChn->CallTime == 0)
  {
    Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 12, "");
  }
  else
  {
    Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 12, time_t2str(pChn->CallTime));
  }
  //應答時間
  if (pChn->AnsTime == 0)
  {
    Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 13, "");
  }
  else
  {
    Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 13, time_t2str(pChn->AnsTime));
  }
  Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 14, pChn->FlwState);
  Set_IVR2LOG_Item(LOGMSG_ongetchnstatus, 15, pChn->DeviceID);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_ongetchnstatus, IVRSndMsg.GetBuf());
}
void SendAllCHStateToLOG(int nLOG)
{
  int nChn;
  
  if (nLOG < MAX_LOGNODE_NUM)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetChnStatus == true)
    {
      for (nChn = 0; nChn < pBoxchn->GetMaxChnNum(); nChn ++)
      {
        SendChStateToLOG(nChn, nLOG);
      }
    }
  }
}
void SendVopStateToAll(int nVopNo)
{
  for (int nVopChnNo=0; nVopChnNo<pVopGroup->m_pVops[nVopNo].m_nVopChnNum; nVopChnNo++)
  {
    SendVopStateToAll(nVopNo, nVopChnNo);
    SendVOPStateToOppIVR(nVopNo, nVopChnNo);
  }
}

void SendVopStateToAll(int nVopNo, int nVopChnNo)
{
  int nChn;
  if (g_nSwitchMode == 0 || pVopGroup == NULL)
  {
    return;
  }
  
  Set_IVR2LOG_Header(LOGMSG_ongetvopstatus, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 1, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nIndex);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 2, nVopNo);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 3, nVopChnNo);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 4, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopChnType);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 5, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nChStyle);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 6, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nChIndex);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 7, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 8, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 9, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSsState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 10, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSvState);
  Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 11, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_szDeviceID);
  nChn = pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn) && pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSvState > 0)
  {
    Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 12, pChn->CustPhone);
    if (pChn->AnsTime == 0)
    {
      Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, "");
    }
    else
    {
      Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, time_t2str(pChn->AnsTime));
    }
  }
  else
  {
    Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 12, "");
    Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, "");
  }
  Set_IVR2LOG_Tail();
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetChnStatus == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ongetvopstatus, IVRSndMsg.GetBuf());
    }
  }
}
void SendVopStateToLOG(int nVopNo, int nVopChnNo, int nLOG)
{
  int nChn;
  if (g_nSwitchMode == 0 || pVopGroup == NULL)
  {
    return;
  }
  Set_IVR2LOG_Header(LOGMSG_ongetvopstatus, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 1, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nIndex);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 2, nVopNo);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 3, nVopChnNo);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 4, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nVopChnType);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 5, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nChStyle);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 6, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nChIndex);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 7, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 8, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nLnState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 9, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSsState);
  Set_IVR2LOG_IntItem(LOGMSG_ongetvopstatus, 10, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSvState);
  Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 11, pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_szDeviceID);
  nChn = pVopGroup->m_pVops[nVopNo].m_pVopChns[nVopChnNo].m_nSeizeChn;
  if (pBoxchn->isnChnAvail(nChn))
  {
    if (pChn->CallInOut == 1)
    {
      Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 12, pChn->CallerNo);
      if (pChn->AnsTime == 0)
      {
        Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, "");
      }
      else
      {
        Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, time_t2str(pChn->AnsTime));
      }
    } 
    else if (pChn->CallInOut == 2)
    {
      Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 12, pChn->CalledNo);
      if (pChn->AnsTime == 0)
      {
        Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, "");
      }
      else
      {
        Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, time_t2str(pChn->AnsTime));
      }
    }
    else
    {
      Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 12, "");
      Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, "");
    }
  }
  else
  {
    Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 12, "");
    Set_IVR2LOG_Item(LOGMSG_ongetvopstatus, 13, "");
  }
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_ongetvopstatus, IVRSndMsg.GetBuf());
}
void SendAllVOPStateToLOG(int nLOG)
{
  int nVopNo, nVopChnNo;

  if (g_nSwitchMode == 0 || pVopGroup == NULL)
  {
    return;
  }
  
  if (nLOG < MAX_LOGNODE_NUM)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetChnStatus == true)
    {
      for (nVopNo = 0; nVopNo < pVopGroup->m_nVopNum; nVopNo ++)
      {
        for (nVopChnNo = 0; nVopChnNo < pVopGroup->m_pVops[nVopNo].m_nVopChnNum; nVopChnNo ++)
        {
          SendVopStateToLOG(nVopNo, nVopChnNo, nLOG);
        }
      }
    }
  }
}
void SendVOPStateToOppIVR(int nVopNo, int nVopChnNo)
{
}
void SendaLLVOPStateToOppIVR()
{
}
void SendACDQueueInfoToLOG(int nLOG)
{
  int queunum, waitacdcount, waitanscount;
  CStringX queueinfo;
  
  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAcdStatus == true)
  {
    queunum = GetQueueInfo(queueinfo, waitacdcount, waitanscount);
    Set_IVR2LOG_Header(LOGMSG_ongetqueueinfo, nLOG);
    Set_IVR2LOG_IntItem(LOGMSG_ongetqueueinfo, 1, queunum);
    Set_IVR2LOG_Item(LOGMSG_ongetqueueinfo, 2, queueinfo);
    Set_IVR2LOG_IntItem(AGMSG_ongetqueueinfo, 3, waitacdcount);
    Set_IVR2LOG_IntItem(AGMSG_ongetqueueinfo, 4, waitanscount);
    Set_IVR2LOG_Tail();
    SendMsg2LOG(nLOG, LOGMSG_ongetqueueinfo, IVRSndMsg.GetBuf());
  }
}
void SendINIParam(int nLOG, const char *pszSectName, const char *pszKeyName, const char *pszValue, int nEndId)
{
  Set_IVR2LOG_Header(LOGMSG_onrecviniparam, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onrecviniparam, 1, 1);
  Set_IVR2LOG_Item(LOGMSG_onrecviniparam, 2, "IVRSERVICE.ini");
  Set_IVR2LOG_Item(LOGMSG_onrecviniparam, 3, pszSectName);
  Set_IVR2LOG_Item(LOGMSG_onrecviniparam, 4, pszKeyName);
  Set_IVR2LOG_Item(LOGMSG_onrecviniparam, 5, pszValue);
  Set_IVR2LOG_IntItem(LOGMSG_onrecviniparam, 6, nEndId);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onrecviniparam, IVRSndMsg.GetBuf());
}
void SendLineMsg(int nINIType, int nLOG, const char *pszLineMsg, int nLineId)
{
  Set_IVR2LOG_Header(LOGMSG_onrecvtxtline, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onrecvtxtline, 1, nINIType);
  Set_IVR2LOG_Item(LOGMSG_onrecvtxtline, 2, pszLineMsg);
  Set_IVR2LOG_IntItem(LOGMSG_onrecvtxtline, 3, nLineId);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onrecvtxtline, IVRSndMsg.GetBuf());
}
void SendINIFile(int nLOG)
{
  FILE *fp1;
  char szLineMsg[256];

  fp1 = fopen(g_szServiceINIFileName, "r");
  if (fp1 != NULL)
  {
    SendLineMsg(1, nLOG, "", 0);
    while (!feof(fp1))
    {
      if (fgets(szLineMsg, 256, fp1) > 0)
      {
        SendLineMsg(1, nLOG, szLineMsg, 1);
      }
    }
    SendLineMsg(1, nLOG, "", 2);
    fclose(fp1);
  }
}
void SendCTILINKINIFile(int nLOG)
{
  FILE *fp1;
  char szLineMsg[256];
  
  fp1 = fopen(g_szCTILINKINIFileName, "r");
  if (fp1 != NULL)
  {
    SendLineMsg(4, nLOG, "", 0);
    while (!feof(fp1))
    {
      if (fgets(szLineMsg, 256, fp1) > 0)
      {
        SendLineMsg(4, nLOG, szLineMsg, 1);
      }
    }
    SendLineMsg(4, nLOG, "", 2);
    fclose(fp1);
  }
}
//-----------------------------------------------------------------------------
//發送登錄結果結果消息
void SendLoginResultToLOG(int nLOG, int result, const char *error)
{
  Set_IVR2LOG_Header(LOGMSG_onlogin, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onlogin, 1, 1);
  Set_IVR2LOG_IntItem(LOGMSG_onlogin, 2, result);
  Set_IVR2LOG_Item(LOGMSG_onlogin, 3, error);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onlogin, IVRSndMsg.GetBuf());
}
//發送通用結果消息
void SendCommonResultToLOG(int nLOG, int onMsgId, int result, const char *error)
{
  Set_IVR2LOG_Header(onMsgId, nLOG);
  Set_IVR2LOG_IntItem(onMsgId, 1, result);
  Set_IVR2LOG_Item(onMsgId, 2, error);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, onMsgId, IVRSndMsg.GetBuf());
}
void SendMonitorONOFFStatus(int nLOG)
{
  Set_IVR2LOG_Header(LOGMSG_onivronoff, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onivronoff, 1, pBoxchn->GetMaxChnNum());
  Set_IVR2LOG_IntItem(LOGMSG_onivronoff, 2, pAgentMng->MaxUseNum);
  Set_IVR2LOG_BoolItem(LOGMSG_onivronoff, 3, LogerStatusSet[nLOG].bGetAcdStatus);
  Set_IVR2LOG_BoolItem(LOGMSG_onivronoff, 4, LogerStatusSet[nLOG].bGetAgStatus);
  Set_IVR2LOG_BoolItem(LOGMSG_onivronoff, 5, LogerStatusSet[nLOG].bGetChnStatus);
  Set_IVR2LOG_BoolItem(LOGMSG_onivronoff, 6, LogerStatusSet[nLOG].bGetIVRMsg);
  Set_IVR2LOG_BoolItem(LOGMSG_onivronoff, 7, LogerStatusSet[nLOG].bGetTrkCallCount);
  Set_IVR2LOG_BoolItem(LOGMSG_onivronoff, 8, LogerStatusSet[nLOG].bGetAgCallCount);
  if (pVopGroup)
  {
    Set_IVR2LOG_IntItem(LOGMSG_onivronoff, 9, pVopGroup->m_nVopChnNum);
  } 
  else
  {
    Set_IVR2LOG_IntItem(LOGMSG_onivronoff, 9, 0);
  }
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onivronoff, IVRSndMsg.GetBuf());
}
void SendSaveMsgONOFFStatus(int nLOG)
{
  Set_IVR2LOG_Header(LOGMSG_onsetivrsavemsgid, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onsetivrsavemsgid, 1, g_bAlartId?1:0);
  Set_IVR2LOG_IntItem(LOGMSG_onsetivrsavemsgid, 2, g_bCommId?1:0);
  Set_IVR2LOG_IntItem(LOGMSG_onsetivrsavemsgid, 3, g_bDebugId?1:0);
  Set_IVR2LOG_IntItem(LOGMSG_onsetivrsavemsgid, 4, g_bDrvId?1:0);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onsetivrsavemsgid, IVRSndMsg.GetBuf());
}
void SendAlarmMsg(int nLOG, int nAlarmCode, int nAlarlLevel, int nAlarmOnOff, const char *pszAlarmMsg)
{
  Set_IVR2LOG_Header(LOGMSG_onivralarmmsg, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onivralarmmsg, 1, nAlarmCode);
  Set_IVR2LOG_IntItem(LOGMSG_onivralarmmsg, 2, nAlarlLevel);
  Set_IVR2LOG_IntItem(LOGMSG_onivralarmmsg, 3, nAlarmOnOff);
  Set_IVR2LOG_Item(LOGMSG_onivralarmmsg, 4, pszAlarmMsg);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onivralarmmsg, IVRSndMsg.GetBuf());
}
void SendHARunStatusToLOG(int nLOG, int nHostType, int nActiveId, const char *pszSwitchTime, const char *pszReason)
{
  if (g_nMultiRunMode == 0)
  {
    return;
  }
  Set_IVR2LOG_Header(LOGMSG_onharunstatus, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onharunstatus, 1, nHostType);
  Set_IVR2LOG_IntItem(LOGMSG_onharunstatus, 2, nActiveId);
  Set_IVR2LOG_Item(LOGMSG_onharunstatus, 3, pszSwitchTime);
  Set_IVR2LOG_Item(LOGMSG_onharunstatus, 4, pszReason);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onharunstatus, IVRSndMsg.GetBuf());
}
void SendHARunStatusToAllLog(int nHostType, int nActiveId, const char *pszSwitchTime, const char *pszReason)
{
  if (g_nMultiRunMode == 0)
  {
    return;
  }
  Set_IVR2LOG_Header(LOGMSG_onharunstatus, 0);
  Set_IVR2LOG_IntItem(LOGMSG_onharunstatus, 1, nHostType);
  Set_IVR2LOG_IntItem(LOGMSG_onharunstatus, 2, nActiveId);
  Set_IVR2LOG_Item(LOGMSG_onharunstatus, 3, pszSwitchTime);
  Set_IVR2LOG_Item(LOGMSG_onharunstatus, 4, pszReason);
  Set_IVR2LOG_Tail();

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2)
    {
      SendMsg2LOG(nLOG, LOGMSG_onharunstatus, IVRSndMsg.GetBuf());
    }
  }
}
void SendTrkCountData(int nCountTimeId, const char *pszCountData)
{
  Set_IVR2LOG_Header(LOGMSG_ontrkcountdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_ontrkcountdata, 1, nCountTimeId);
  Set_IVR2LOG_Item(LOGMSG_ontrkcountdata, 2, pszCountData);
  Set_IVR2LOG_Tail();

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetTrkCallCount == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_ontrkcountdata, IVRSndMsg.GetBuf());
    }
  }
}
void SendAgCountData(int nCountTimeId, const char *pszCountData)
{
  Set_IVR2LOG_Header(LOGMSG_onagcountdata, 0);
  Set_IVR2LOG_IntItem(LOGMSG_onagcountdata, 1, nCountTimeId);
  Set_IVR2LOG_Item(LOGMSG_onagcountdata, 2, pszCountData);
  Set_IVR2LOG_Tail();
  
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAgCallCount == true)
    {
      SendMsg2LOG(nLOG, LOGMSG_onagcountdata, IVRSndMsg.GetBuf());
    }
  }
}
void SendTrkCountDataToLOG(int nLOG)
{
  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetTrkCallCount == true)
  {
    for (int i = 0; i < 48; i ++)
    {
      Set_IVR2LOG_Header(LOGMSG_ontrkcountdata, nLOG);
      Set_IVR2LOG_IntItem(LOGMSG_ontrkcountdata, 1, i);
      Set_IVR2LOG_Item(LOGMSG_ontrkcountdata, 2, TodayChCount[i].strTrkData);
      Set_IVR2LOG_Tail();
      SendMsg2LOG(nLOG, LOGMSG_ontrkcountdata, IVRSndMsg.GetBuf());
    }
  }
}
void SendAgCountDataToLOG(int nLOG)
{
  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetAgCallCount == true)
  {
    for (int i = 0; i < 48; i ++)
    {
      Set_IVR2LOG_Header(LOGMSG_onagcountdata, nLOG);
      Set_IVR2LOG_IntItem(LOGMSG_onagcountdata, 1, i);
      Set_IVR2LOG_Item(LOGMSG_onagcountdata, 2, TodayChCount[i].strAgData);
      Set_IVR2LOG_Tail();
      SendMsg2LOG(nLOG, LOGMSG_onagcountdata, IVRSndMsg.GetBuf());
    }
  }
}
void SendModifyseatdataResult(int nLOG, int nAG, int nMaxAgNum, int nResult, const char *pszError)
{
  Set_IVR2LOG_Header(LOGMSG_onmodifyseatdata, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onmodifyseatdata, 1, nAG);
  Set_IVR2LOG_IntItem(LOGMSG_onmodifyseatdata, 2, nMaxAgNum);
  Set_IVR2LOG_IntItem(LOGMSG_onmodifyseatdata, 3, nResult);
  Set_IVR2LOG_Item(LOGMSG_onmodifyseatdata, 4, pszError);
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onmodifyseatdata, IVRSndMsg.GetBuf());
}
void SendQueryTrunkGroupToLOG(int nLOG, const char *trunkgroup, int idletrunks, int usedtrunks, int blocktrunks)
{
  Set_IVR2LOG_Header(LOGMSG_onquerytrunkgroup, nLOG);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 1, 1);
  Set_IVR2LOG_Item(LOGMSG_onquerytrunkgroup, 2, trunkgroup);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 3, idletrunks);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 4, usedtrunks);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 5, blocktrunks);
  Set_IVR2LOG_Item(LOGMSG_onquerytrunkgroup, 6, "");
  Set_IVR2LOG_Tail();
  SendMsg2LOG(nLOG, LOGMSG_onquerytrunkgroup, IVRSndMsg.GetBuf());
}
void SendQueryTrunkGroupToAllLOG(const char *trunkgroup, int idletrunks, int usedtrunks, int blocktrunks)
{
  Set_IVR2LOG_Header(LOGMSG_onquerytrunkgroup, 0);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 1, 1);
  Set_IVR2LOG_Item(LOGMSG_onquerytrunkgroup, 2, trunkgroup);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 3, idletrunks);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 4, usedtrunks);
  Set_IVR2LOG_IntItem(LOGMSG_onquerytrunkgroup, 5, blocktrunks);
  Set_IVR2LOG_Item(LOGMSG_onquerytrunkgroup, 6, "");
  Set_IVR2LOG_Tail();

  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2)
    {
      SendMsg2LOG(nLOG, LOGMSG_onquerytrunkgroup, IVRSndMsg.GetBuf());
    }
  }
}
//-----------------------------------------------------------------------------
//登錄
int Proc_LOGMSG_login(CXMLRcvMsg &LOGMsg)
{
  char szTemp[256], szPassword1[256], szPassword2[256];
  
  if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "administrator") == 0)
  {
    GetPrivateProfileString("LOGUSER", "administrator", "", szTemp, 256, g_szServiceINIFileName);
    strcpy(LogerStatusSet[nLOGn].LoginUserName, "administrator");
    LogerStatusSet[nLOGn].LoginLevel = 1;
  }
  else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "manager") == 0)
  {
    GetPrivateProfileString("LOGUSER", "manager", "", szTemp, 256, g_szServiceINIFileName);
    strcpy(LogerStatusSet[nLOGn].LoginUserName, "manager");
    LogerStatusSet[nLOGn].LoginLevel = 2;
  }
  else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "operator") == 0)
  {
    GetPrivateProfileString("LOGUSER", "operator", "", szTemp, 256, g_szServiceINIFileName);
    strcpy(LogerStatusSet[nLOGn].LoginUserName, "operator");
    LogerStatusSet[nLOGn].LoginLevel = 3;
  }
  else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "monitor") == 0)
  {
    GetPrivateProfileString("LOGUSER", "monitor", "", szTemp, 256, g_szServiceINIFileName);
    strcpy(LogerStatusSet[nLOGn].LoginUserName, "monitor");
    LogerStatusSet[nLOGn].LoginLevel = 4;
  }
  else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "board") == 0)
  {
    GetPrivateProfileString("LOGUSER", "board", "", szTemp, 256, g_szServiceINIFileName);
    strcpy(LogerStatusSet[nLOGn].LoginUserName, "board");
    LogerStatusSet[nLOGn].LoginLevel = 5;
  }
  else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "rtxexstate") == 0)
  {
    strcpy(LogerStatusSet[nLOGn].LoginUserName, "rtxexstate");
    LogerStatusSet[nLOGn].LoginLevel = 6;
    LogerStatusSet[nLOGn].bGetChnStatus = false;
    LogerStatusSet[nLOGn].bGetAgStatus = true;
    LogerStatusSet[nLOGn].bGetAcdStatus = false;
  }
  else
  {
    SendLoginResultToLOG(nLOGn, OnFail, "登錄用戶名錯誤");
    MyTrace(1, "LogId=%d Login UserName error", nLOGn);
    return 1;
  }
  strcpy(szPassword1, DecodePass(szTemp));
  strcpy(szPassword2, DecodePass(LOGMsg.GetAttrValue(2).C_Str()));
//   MyTrace(0, "Proc_LOGMSG_login szPassword1=%s szPassword2=%s", szPassword1, szPassword2);
//   if (LogerStatusSet[nLOGn].LoginLevel != 6 && strcmp(szPassword1, szPassword2) != 0)
//   {
//     memset(LogerStatusSet[nLOGn].LoginUserName, 0, 32);
//     SendLoginResultToLOG(nLOGn, OnFail, "登錄密碼錯誤");
//     MyTrace(1, "LogId=%d Login Password error", nLOGn);
//     return 1;
//   }
  
  LogerStatusSet[nLOGn].nLoginId = 2;
  
  SendLoginResultToLOG(nLOGn, OnSuccess, "");
  SendHARunStatusToLOG(nLOGn, g_nHostType, g_nHostType==0?g_nMasterStatus:g_nStandbyStatus, g_szSwitchTime, "");
  
  CheckLOGTraceMsgId();
  if (LogerStatusSet[nLOGn].LoginLevel < 5)
  {
    if (atoi(LOGMsg.GetAttrValue(3).C_Str()) == 1)
    {
      SendINIFile(nLOGn);
      SendCTILINKINIFile(nLOGn);
    }
    
    SendMonitorONOFFStatus(nLOGn);
    SendSaveMsgONOFFStatus(nLOGn);
  }

  return 0;
}
//登出
int Proc_LOGMSG_logout(CXMLRcvMsg &LOGMsg)
{
  LogerStatusSet[nLOGn].nLoginId = 1;
  LogerStatusSet[nLOGn].Init();
  CheckLOGTraceMsgId();
  return 0;
}
//取ACD分配隊列信息
int Proc_LOGMSG_getqueueinfo(CXMLRcvMsg &LOGMsg)
{
  int nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (nOpenFlag == 1)
  {
    LogerStatusSet[nLOGn].bGetAcdStatus = true;
    SendACDQueueInfoToLOG(nLOGn);
  } 
  else
  {
    LogerStatusSet[nLOGn].bGetAcdStatus = true;
  }
  return 0;
}
//取所有坐席的狀態信息
int Proc_LOGMSG_getagentstatus(CXMLRcvMsg &LOGMsg)
{
  int nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (nOpenFlag == 1)
  {
    LogerStatusSet[nLOGn].bGetAgStatus = true;
    SendAllAGStateToLOG(nLOGn);
    SendSystemStatusMeteCount(nLOGn);
    SendAllGroupStatusMeteCount(nLOGn);
    SendAllSeatStatusMeteCount(nLOGn);
  } 
  else
  {
    LogerStatusSet[nLOGn].bGetAgStatus = false;
  }
  return 0;
}
//取所有通道的狀態信息
int Proc_LOGMSG_getchnstatus(CXMLRcvMsg &LOGMsg)
{
  int nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (nOpenFlag == 1)
  {
    LogerStatusSet[nLOGn].bGetChnStatus = true;
    SendAllCHStateToLOG(nLOGn);
    SendAllVOPStateToLOG(nLOGn);
  } 
  else
  {
    LogerStatusSet[nLOGn].bGetChnStatus = false;
  }
  return 0;
}
//取ivr日志跟蹤信息
int Proc_LOGMSG_getivrtracemsg(CXMLRcvMsg &LOGMsg)
{
  int nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (nOpenFlag == 1)
  {
    LogerStatusSet[nLOGn].bGetIVRMsg = true;
  } 
  else
  {
    LogerStatusSet[nLOGn].bGetIVRMsg = false;
  }
  CheckLOGTraceMsgId();
  return 0;
}
//取已加載的流程信息
int Proc_LOGMSG_getloadedflws(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//取flw日志跟蹤信息
int Proc_LOGMSG_getflwtracemsg(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//強行釋放通道
int Proc_LOGMSG_releasechn(CXMLRcvMsg &LOGMsg)
{
  int nChn = atoi(LOGMsg.GetAttrValue(1).C_Str());
  Release_Chn(nChn, 1, 1);
  HangonIVR(nChn);
  return 0;
}
//強拆其他坐席的電話
int Proc_LOGMSG_forceclear(CXMLRcvMsg &LOGMsg)
{
  int nAG1, nChn1;
  nAG1 = GetnAGbyWorkerNoSeatNo(LOGMsg.GetAttrValue(1).C_Str(), atoi(LOGMsg.GetAttrValue(2).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforceclear, OnFail, "該強拆的坐席或工號不存在");
    return 1;
  }
  nChn1 = pAG1->GetSeatnChn();
  if (!pBoxchn->isnChnAvail(nChn1))
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforceclear, OnFail, "該強拆的坐席未綁定電話通道");
    return 1;
  }
  //2013-11-14增加強行釋放IVR
  CVopChn *pVopChn = pChn1->cVopChn;
  if (pVopChn)
  {
    if (pChn1->lgChnType == CHANNEL_IVR)
    {
      Release(nChn1, 0);
    }
  }
  Release_Chn(nChn1, 1, 1);
  SendCommonResultToLOG(nLOGn, LOGMSG_onforceclear, OnSuccess, "");
  return 0;
}
//強制將其他話務員退出
int Proc_LOGMSG_forcelogout(CXMLRcvMsg &LOGMsg)
{
  int nAG1, nQue;
  nAG1 = GetnAGbyWorkerNoSeatNo(LOGMsg.GetAttrValue(1).C_Str(), atoi(LOGMsg.GetAttrValue(2).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforcelogout, OnFail, "該坐席或工號不存在");
    return 1;
  }
  nQue = pAG1->nQue;
  if (pCalloutQue->isnQueAvail(nQue) && pAG1->isWantSeatType(SEATTYPE_AG_PC))
  {
    pQue->CalloutResult = CALLOUT_REFUSE; //如果此時有來話,將拒絕應答
  }
  if (pAG1->m_Worker.oldWorkerNo == 0)
  {
    pAG1->m_Worker.Logout();
    pAG1->m_Worker.ClearState();
  }
  else
  {
    pAG1->m_Worker.OldWorkerLogin();
  }
  DispAgentStatus(nAG1);
  SendCommonResultToLOG(nLOGn, LOGMSG_onforcelogout, OnSuccess, "");
  SendCommonResult(nAG1, AGMSG_onforcelogout, OnSuccess, "");
  return 0;
}
//強制其他坐席免打擾
int Proc_LOGMSG_forcedisturb(CXMLRcvMsg &LOGMsg)
{
  int nAG1;
  nAG1 = GetnAGbyWorkerNoSeatNo(LOGMsg.GetAttrValue(1).C_Str(), atoi(LOGMsg.GetAttrValue(2).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforcedisturb, OnFail, "該坐席或工號不存在");
    return 1;
  }
  if (pAG1->isLogin() == false)
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforcedisturb, OnFail, "該坐席或工號未登錄");
    return 1;
  }
  pAG1->m_Worker.SetDisturb(1);
  DispAgentStatus(nAG1);
  SendCommonResultToLOG(nLOGn, LOGMSG_onforcedisturb, OnSuccess, "");
  SendCommonResult(nAG1, AGMSG_onforcedisturb, OnSuccess, "");
  return 0;
}
//強制其他坐席空閑
int Proc_LOGMSG_forceready(CXMLRcvMsg &LOGMsg)
{
  int nAG1;
  nAG1 = GetnAGbyWorkerNoSeatNo(LOGMsg.GetAttrValue(1).C_Str(), atoi(LOGMsg.GetAttrValue(2).C_Str()));
  if (!pAgentMng->isnAGAvail(nAG1))
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforceready, OnFail, "該坐席或工號不存在");
    return 1;
  }
  if (pAG1->isLogin() == false)
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onforceready, OnFail, "該坐席或工號未登錄");
    return 1;
  }
  pAG1->m_Worker.SetDisturb(0);
  pAG1->m_Worker.SetLeave(0, "強制空閑");
  DispAgentStatus(nAG1);
  SendCommonResultToLOG(nLOGn, LOGMSG_onforceready, OnSuccess, "");
  SendCommonResult(nAG1, AGMSG_onforceready, OnSuccess, "");
  return 0;
}
//修改路由參數
int Proc_LOGMSG_modifyroutedata(CXMLRcvMsg &LOGMsg)
{
  int routeno = atoi(LOGMsg.GetAttrValue(1).C_Str());
  int selmode = atoi(LOGMsg.GetAttrValue(3).C_Str());
  pBoxchn->SetOneRoute(routeno, selmode, LOGMsg.GetAttrValue(2).C_Str(), LOGMsg.GetAttrValue(4).C_Str());
  SendCommonResultToLOG(nLOGn, LOGMSG_onmodifyroutedata, OnSuccess, "");
  return 0;
}
//修改坐席分機號
int Proc_LOGMSG_modifyseatno(CXMLRcvMsg &LOGMsg)
{
  int nAG = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (ModifySeatNo(nAG, LOGMsg.GetAttrValue(2).C_Str()) == 0)
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onmodifyseatno, OnSuccess, "");
    DispAgentStatus(nAG);
  }
  else
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onmodifyseatno, OnFail, "");
  }
  return 0;
}
//修改模擬外線接入號碼參數
int Proc_LOGMSG_modifyextline(CXMLRcvMsg &LOGMsg)
{
  CH param[128];
  int j=0;
  CStringX ArrString[24], ArrString1[24];

  int lineno = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (lineno < MAX_CHANGE_EXT_NUM)
  {
    strncpy(pIVRCfg->PSTNCode[lineno], LOGMsg.GetAttrValue(2).C_Str(), 20);
    strncpy(pIVRCfg->PreCode[lineno], LOGMsg.GetAttrValue(3).C_Str(), 20);
    strncpy(pIVRCfg->IPPreCode[lineno], LOGMsg.GetAttrValue(4).C_Str(), 20);

    strncpy(param, LOGMsg.GetAttrValue(5).C_Str(), 120);
    pIVRCfg->PBXExtrnList[lineno].ExtrnPreNum = 0;
    if (strlen(param) > 0)
    {
      int nExtrnPreNum=SplitString(param, ';', 20, ArrString);
      for (int n=0; n<nExtrnPreNum; n++)
      {
        if (ArrString[n].GetLength() > 0)
        {
          int nItemNum=SplitString(ArrString[n].C_Str(), ',', 2, ArrString1);
          if (nItemNum>=2)
          {
            if (ArrString1[0].GetLength() > 0)
            {
              pIVRCfg->PBXExtrnList[lineno].PBXExtrn[j].ExtrnLen = atoi(ArrString1[1].C_Str());
              if (pIVRCfg->PBXExtrnList[lineno].PBXExtrn[j].ExtrnLen > 0)
              {
                strncpy(pIVRCfg->PBXExtrnList[lineno].PBXExtrn[j].ExtrnPreCode, ArrString1[0].C_Str(), 9);
                j++;
                pIVRCfg->PBXExtrnList[lineno].ExtrnPreNum = j;
              }
            }
          }
        }
      }
    }
  }
  SendCommonResultToLOG(nLOGn, LOGMSG_onmodifyextline, OnSuccess, "");
  return 0;
}
//加載業務流程
int Proc_LOGMSG_loadflw(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//添加接入號碼
int Proc_LOGMSG_addaccesscode(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//刪除接入號碼
int Proc_LOGMSG_delaccesscode(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//設置IVR服務自動保存日志標志
int Proc_LOGMSG_setivrsavemsgid(CXMLRcvMsg &LOGMsg)
{
  if (atoi(LOGMsg.GetAttrValue(1).C_Str()) == 0)
  {
    g_bAlartId=false;
    WritePrivateProfileString("LOG", "AlartId", "0", g_szServiceINIFileName);
  }
  else
  {
    g_bAlartId=true;
    WritePrivateProfileString("LOG", "AlartId", "1", g_szServiceINIFileName);
  }
  if (atoi(LOGMsg.GetAttrValue(2).C_Str()) == 0)
  {
    g_bCommId=false;
    WritePrivateProfileString("LOG", "CommId", "0", g_szServiceINIFileName);
  }
  else
  {
    g_bCommId=true;
    WritePrivateProfileString("LOG", "CommId", "1", g_szServiceINIFileName);
  }
  if (atoi(LOGMsg.GetAttrValue(3).C_Str()) == 0)
  {
    g_bDebugId=false;
    g_bSQLId=false;
    WritePrivateProfileString("LOG", "DebugId", "0", g_szServiceINIFileName);
    WritePrivateProfileString("LOG", "SQLId", "0", g_szServiceINIFileName);
  }
  else
  {
    g_bDebugId=true;
    g_bSQLId=true;
    WritePrivateProfileString("LOG", "DebugId", "1", g_szServiceINIFileName);
    WritePrivateProfileString("LOG", "SQLId", "1", g_szServiceINIFileName);
  }
  if (atoi(LOGMsg.GetAttrValue(4).C_Str()) == 0)
  {
    g_bDrvId=false;
    if (g_nCardType != 0)
      SetWriteDebugId(0);
    WritePrivateProfileString("LOG", "DrvId", "0", g_szServiceINIFileName);
  }
  else
  {
    g_bDrvId=true;
    if (g_nCardType != 0)
      SetWriteDebugId(1);
    WritePrivateProfileString("LOG", "DrvId", "1", g_szServiceINIFileName);
  }
  
  (g_bCommId==true || g_bDebugId==true) ? g_bSaveId = true : g_bSaveId = false;
  return 0;
}
//設置FLW服務自動保存日志標志
int Proc_LOGMSG_setflwsavemsgid(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//修改INI配置文件參數
int Proc_LOGMSG_modifyiniparam(CXMLRcvMsg &LOGMsg)
{
  WritePrivateProfileString(LOGMsg.GetAttrValue(2).C_Str(), 
    LOGMsg.GetAttrValue(3).C_Str(), 
    LOGMsg.GetAttrValue(4).C_Str(), g_szServiceINIFileName);
  return 0;
}
//修改坐席參數
int Proc_LOGMSG_modifyseatdata(CXMLRcvMsg &LOGMsg)
{
  char szError[128];
  int nAG, nSeatType, nPortNo, nResult;

  memset(szError, 0, 128);
  nSeatType = atoi(LOGMsg.GetAttrValue(1).C_Str());
  nPortNo = atoi(LOGMsg.GetAttrValue(2).C_Str());
  if (nSeatType == SEATTYPE_AG_TEL || nSeatType == SEATTYPE_AG_PC)
  {
    nResult = SetOneLocaSeatParam(nSeatType, nPortNo, LOGMsg.GetAttrValue(3).C_Str(), nAG, szError);
    MyTrace(5, "SetOneLocaSeatParam SeatType=%d PortNo=%d SeatData=%s Result=%d nAG=%d error=%s",
      nSeatType, nPortNo, LOGMsg.GetAttrValue(3).C_Str(), nResult, nAG, szError);
    if (nResult == 0)
    {
      SendModifyseatdataResult(nLOGn, nAG, pAgentMng->MaxUseNum, OnSuccess, "");
      DispAgentStatus(nAG);
    }
    else
    {
      SendModifyseatdataResult(nLOGn, nAG, pAgentMng->MaxUseNum, OnFail, szError);
    }
  }
  if (nSeatType == SEATTYPE_RT_TEL)
  {
    nResult = SetOneRemoteSeatParam(nSeatType, nPortNo, LOGMsg.GetAttrValue(3).C_Str(), nAG, szError);
    MyTrace(5, "SetOneRemoteSeatParam SeatType=%d PortNo=%d SeatData=%s Result=%d nAG=%d error=%s",
      nSeatType, nPortNo, LOGMsg.GetAttrValue(3).C_Str(), nResult, nAG, szError);
    if (nResult == 0)
    {
      SendModifyseatdataResult(nLOGn, nAG, pAgentMng->MaxUseNum, OnSuccess, "");
      DispAgentStatus(nAG);
    }
    else
    {
      SendModifyseatdataResult(nLOGn, nAG, pAgentMng->MaxUseNum, OnFail, szError);
    }
  }
  return 0;
}
//取中繼實時話務統計數據
int Proc_LOGMSG_gettrkcallcount(CXMLRcvMsg &LOGMsg)
{
  int nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (nOpenFlag == 2)
  {
    SendNowTrkCallMeteCount();
  }
  else if (nOpenFlag == 1)
  {
    LogerStatusSet[nLOGn].bGetTrkCallCount = true;
    SendTrkCountDataToLOG(nLOGn);
  } 
  else
  {
    LogerStatusSet[nLOGn].bGetTrkCallCount = false;
  }
  return 0;
}
//取坐席實時話務統計數據
int Proc_LOGMSG_getagcallcount(CXMLRcvMsg &LOGMsg)
{
  int nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
  if (nOpenFlag == 2)
  {
    SendNowAgentCallMeteCount();
  }
  else if (nOpenFlag == 1)
  {
    LogerStatusSet[nLOGn].bGetAgCallCount = true;
    SendAgCountDataToLOG(nLOGn);
  } 
  else
  {
    LogerStatusSet[nLOGn].bGetAgCallCount = false;
  }
  return 0;
}
int Proc_LOGMSG_autoflwonoff(CXMLRcvMsg &LOGMsg)
{
  return 0;
}
//修改登錄密碼
int Proc_LOGMSG_modifypassword(CXMLRcvMsg &LOGMsg)
{
  char szTemp[256], szPassword1[256], szPassword2[256];

  if (LogerStatusSet[nLOGn].nLoginId != 2)
  {
    SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnFail, "未登錄");
    return 1;
  }
  if (LogerStatusSet[nLOGn].LoginLevel == 1)
  {
    GetPrivateProfileString("LOGUSER", "administrator", "", szTemp, 256, g_szServiceINIFileName);
    
    strcpy(szPassword1, DecodePass(szTemp));
    strcpy(szPassword2, DecodePass(LOGMsg.GetAttrValue(2).C_Str()));
    
    if (strcmp(szPassword1, szPassword2) == 0)
    {
      //administrator可以修改所有密碼
      if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "administrator") == 0)
      {
        WritePrivateProfileString("LOGUSER", "administrator", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "manager") == 0)
      {
        WritePrivateProfileString("LOGUSER", "manager", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "operator") == 0)
      {
        WritePrivateProfileString("LOGUSER", "operator", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "monitor") == 0)
      {
        WritePrivateProfileString("LOGUSER", "monitor", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "board") == 0)
      {
        WritePrivateProfileString("LOGUSER", "board", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
    }
  }
  else if (LogerStatusSet[nLOGn].LoginLevel == 2)
  {
    GetPrivateProfileString("LOGUSER", "manager", "", szTemp, 256, g_szServiceINIFileName);
    
    strcpy(szPassword1, DecodePass(szTemp));
    strcpy(szPassword2, DecodePass(LOGMsg.GetAttrValue(2).C_Str()));
    
    if (strcmp(szPassword1, szPassword2) == 0)
    {
      //manager可以修改manager，operator的密碼
      if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "manager") == 0)
      {
        WritePrivateProfileString("LOGUSER", "manager", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "operator") == 0)
      {
        WritePrivateProfileString("LOGUSER", "operator", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "monitor") == 0)
      {
        WritePrivateProfileString("LOGUSER", "monitor", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
      else if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "board") == 0)
      {
        WritePrivateProfileString("LOGUSER", "board", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
    }
  }
  else if (LogerStatusSet[nLOGn].LoginLevel == 3)
  {
    GetPrivateProfileString("LOGUSER", "operator", "", szTemp, 256, g_szServiceINIFileName);
    
    strcpy(szPassword1, DecodePass(szTemp));
    strcpy(szPassword2, DecodePass(LOGMsg.GetAttrValue(2).C_Str()));
    
    if (strcmp(szPassword1, szPassword2) == 0)
    {
      //operator只能修改operator的密碼
      if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "operator") == 0)
      {
        WritePrivateProfileString("LOGUSER", "operator", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
    }
  }
  else if (LogerStatusSet[nLOGn].LoginLevel == 4)
  {
    GetPrivateProfileString("LOGUSER", "monitor", "", szTemp, 256, g_szServiceINIFileName);
    
    strcpy(szPassword1, DecodePass(szTemp));
    strcpy(szPassword2, DecodePass(LOGMsg.GetAttrValue(2).C_Str()));
    
    if (strcmp(szPassword1, szPassword2) == 0)
    {
      //monitor只能修改monitor的密碼
      if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "monitor") == 0)
      {
        WritePrivateProfileString("LOGUSER", "monitor", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
    }
  }
  else if (LogerStatusSet[nLOGn].LoginLevel == 5)
  {
    GetPrivateProfileString("LOGUSER", "board", "", szTemp, 256, g_szServiceINIFileName);
    
    strcpy(szPassword1, DecodePass(szTemp));
    strcpy(szPassword2, DecodePass(LOGMsg.GetAttrValue(2).C_Str()));
    
    if (strcmp(szPassword1, szPassword2) == 0)
    {
      //board只能修改board的密碼
      if (MyStrCmpNoCase(LOGMsg.GetAttrValue(1).C_Str(), "board") == 0)
      {
        WritePrivateProfileString("LOGUSER", "board", LOGMsg.GetAttrValue(3).C_Str(), g_szServiceINIFileName);
        SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnSuccess, "");
        return 0;
      }
    }
  }
  SendCommonResultToLOG(nLOGn, LOGMSG_onmodifypassword, OnFail, "無權修改該用戶密碼");
  return 1;
}
//修改CTILINK參數
int Proc_LOGMSG_modifyctilinkdata(CXMLRcvMsg &LOGMsg)
{
  WritePrivateProfileString(LOGMsg.GetAttrValue(2).C_Str(), 
    LOGMsg.GetAttrValue(3).C_Str(), 
    LOGMsg.GetAttrValue(4).C_Str(), g_szCTILINKINIFileName);
  return 0;
}
//自定義一個函數指針類型
typedef int (*Proc_LogMSG)(CXMLRcvMsg &LOGMsg);
Proc_LogMSG proc_logmsg[]=
{
  Proc_LOGMSG_login,	  //登錄
  Proc_LOGMSG_logout,    //登出
  
  Proc_LOGMSG_getqueueinfo,	  //取ACD分配隊列信息
  Proc_LOGMSG_getagentstatus,	  //取所有坐席的狀態信息
  Proc_LOGMSG_getchnstatus,	  //取所有通道的狀態信息
  Proc_LOGMSG_getivrtracemsg,	  //取ivr日志跟蹤信息
  Proc_LOGMSG_getloadedflws,	  //取已加載的流程信息
  Proc_LOGMSG_getflwtracemsg,	  //取flw日志跟蹤信息
  
  Proc_LOGMSG_releasechn,	  //強行釋放通道
  Proc_LOGMSG_forceclear,	  //強拆其他坐席的電話
  Proc_LOGMSG_forcelogout,	  //強制將其他話務員退出
  Proc_LOGMSG_forcedisturb,	  //強制其他坐席免打擾
  Proc_LOGMSG_forceready,	//強制其他坐席空閑
  
  Proc_LOGMSG_modifyroutedata,	//修改路由參數
  Proc_LOGMSG_modifyseatno, //修改坐席分機號
  Proc_LOGMSG_modifyextline,	//修改模擬外線接入號碼參數
  
  Proc_LOGMSG_loadflw,	//加載業務流程
  Proc_LOGMSG_addaccesscode,	//添加接入號碼
  Proc_LOGMSG_delaccesscode,	//刪除接入號碼
  
  Proc_LOGMSG_setivrsavemsgid,	//設置IVR服務自動保存日志標志
  Proc_LOGMSG_setflwsavemsgid,	//設置FLW服務自動保存日志標志
  
  Proc_LOGMSG_modifyiniparam,	//修改INI配置文件參數
  Proc_LOGMSG_modifyseatdata,	//修改坐席參數
  
  Proc_LOGMSG_gettrkcallcount,	//取中繼實時話務統計數據
  Proc_LOGMSG_getagcallcount,	//取坐席實時話務統計數據
  
  Proc_LOGMSG_autoflwonoff,
  
  Proc_LOGMSG_modifypassword, //修改登錄密碼
  Proc_LOGMSG_modifyctilinkdata, //修改CTILINK參數
};
//處理監控發來的消息
void ProcLOGMsg(CXMLRcvMsg &LOGMsg)
{
  Get_LOGMSG_HeaderValue(LOGMsg);
  if (nLOGn >= MAX_LOGNODE_NUM)
  {
    return;
  }
  proc_logmsg[LOGMsg.GetMsgId()](LOGMsg);
}

void CheckLOGTraceMsgId()
{
  g_nSendTraceMsgToLOG = false;
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].bGetIVRMsg == true)
    {
      g_nSendTraceMsgToLOG = true;
    }
  }
}

//=============================================================================
//備機發送狀態到主機
void SendMsgToMasterIVR(int nMasterStatus, int nStandbyStatus, const char *pszReason)
{
  if (g_nMultiRunMode == 0)
    return;
  char msgbuf[256];
  
  sprintf(msgbuf, "<standbystatus sessionid='%d' nodetype='%d' nodeno='%d' masterstatus='%d' standbystatus='%d' reason='%s'/>",
    0, 0, 2, nMasterStatus, nStandbyStatus, pszReason);
  pTCPHALink->SendMessage2Master(HAMSG_standbystatus, msgbuf);
  MyTrace(2, "0x%04x %s", pTCPHALink->MasterIVRServer.Serverid, msgbuf);
}
//主機發送狀態到備機
void SendMsgToStandbyIVR(int nMasterStatus, int nStandbyStatus, const char *pszReason)
{
  if (g_nMultiRunMode == 0)
    return;
  char msgbuf[256];
  
  sprintf(msgbuf, "<onmasterstatus sessionid='%d' nodetype='%d' nodeno='%d' masterstatus='%d' standbystatus='%d' reason='%s'/>",
    0, 0, 1, nMasterStatus, nStandbyStatus, pszReason);
  pTCPHALink->SendMessage2CLIENT(StandbyIVRClientId, (MSGTYPE_HAIVR<<8) | HAMSG_onmasterstatus, msgbuf);
  MyTrace(2, "0x%04x %s", StandbyIVRClientId, msgbuf);
}
//發送運行切換指令到各客戶端節點
void SendSwitchoverToAllClient(int nReason, const char *pszReason)
{
  if (g_nMultiRunMode == 0)
    return;
  char msgbuf[256], msgbuf1[256];
  
  if (g_nHostType == 0)
  {
    sprintf(msgbuf, "<sendswitchover sessionid='0' switchid='1' hosttype='%d' activeid='%d' reason='%d' reasonmsg='%s'/>",
      g_nHostType, g_nMasterStatus, nReason, pszReason);
    sprintf(msgbuf1, "wsclientid=allclient;cmd=switchover;hosttype=%d;activeid=%d;reason=%d;reasonmsg=%s;",
      g_nHostType, g_nMasterStatus, nReason, pszReason);
  }
  else
  {
    sprintf(msgbuf, "<sendswitchover sessionid='0' switchid='1' hosttype='%d' activeid='%d' reason='%d' reasonmsg='%s'/>",
      g_nHostType, g_nStandbyStatus, nReason, pszReason);
    sprintf(msgbuf1, "wsclientid=allclient;cmd=switchover;hosttype=%d;activeid=%d;reason=%d;reasonmsg=%s;",
      g_nHostType, g_nStandbyStatus, nReason, pszReason);
  }
  
  if (CTILinkClientId > 0)
    SendMsg2SWT(CTILinkClientId, SWTMSG_sendswitchover, msgbuf);
  
  if (WebSocketClientId > 0)
    SendMsg2WS(WebSocketClientId, 2, msgbuf1, true);

  for (int nAG = 0; nAG < pAgentMng->MaxUseNum; nAG ++)
  {
    if (pAG->isTcpLinked() == true)
    {
      SendMsg2AG(pAG->GetClientId(), AGMSG_onswitchover, msgbuf);
    }
  }
}
//發送運行狀態到某客戶端
void SendRunStatusToClient(unsigned long usClientId, int nMsgId)
{
  if (g_nMultiRunMode == 0)
    return;
  char msgbuf[256];
  
  if (g_nHostType == 0)
  {
    sprintf(msgbuf, "<sendswitchover sessionid='0' switchid='1' hosttype='%d' activeid='%d' reason='%d' reasonmsg='%s'/>",
      g_nHostType, g_nMasterStatus, 0, "");
  }
  else
  {
    sprintf(msgbuf, "<sendswitchover sessionid='0' switchid='1' hosttype='%d' activeid='%d' reason='%d' reasonmsg='%s'/>",
      g_nHostType, g_nStandbyStatus, 0, "");
  }
  
  UC NodeType = (UC)((usClientId>>8)&0xff);
  switch (NodeType)
  {
  case NODE_SWT:
    pTcpLink->SendMessage2CLIENT(usClientId, nMsgId, msgbuf);
    MyTrace(2, "0x%04x %s", usClientId, msgbuf);
    break;
  case NODE_CSC:
    pTcpAgLink->SendMessage2CLIENT(usClientId, nMsgId, msgbuf);
    MyTrace(2, "0x%04x %s", usClientId, msgbuf);
    break;
  }
}
//發送運行狀態到websocket客戶端
void SendRunStatusToWSClient(const char *pszSeatNo)
{
  if (g_nMultiRunMode == 0)
    return;
  char msgbuf[256];
  
  if (g_nHostType == 0)
  {
    sprintf(msgbuf, "wsclientid=%s;cmd=switchover;hosttype=%d;activeid=%d;reason=%d;reasonmsg=%s;",
      pszSeatNo, g_nHostType, g_nMasterStatus, 0, "");
  }
  else
  {
    sprintf(msgbuf, "wsclientid=%s;cmd=switchover;hosttype=%d;activeid=%d;reason=%d;reasonmsg=%s;",
      pszSeatNo, g_nHostType, g_nStandbyStatus, 0, "");
  }
  
  if (WebSocketClientId > 0)
    SendMsg2WS(WebSocketClientId, 0, msgbuf, true);
}

//=============================================================================
