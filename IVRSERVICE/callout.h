//---------------------------------------------------------------------------
#ifndef CalloutH
#define CalloutH

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//呼出記錄
class CCalloutRecd
{
public:
  UC state;
  UL timer; //計時器
  US RingTime; //等待振鈴時間

	UC CallerType; //主叫類別(bit0-bit3: 0-內線分機 1-普通市話 2-國內備用 3-國內有效 4-國際)
	UC CallType; //呼叫類別 0-普通呼叫 1-話務員呼叫
  CStringX CallerNo; //主叫號碼
  CH CalledNo[MAX_TELECODE_LEN]; //被叫號碼
  CStringX OrgCalledNo; //原被叫號碼
	UC CalledNoPoint; //已發送被叫號碼位數（用于發送后續號碼）

  US OutnChn; //呼出通道號
  US OutRouteNo; //呼出路由號
  UC CancelId; //取消呼出標志 1-取消
  UC CalloutResult; //參考呼出結果宏定義

  UC OrgCallType; //發起呼叫類型 1-流程呼出 2-分配坐席呼出
  US nOut; //所屬的呼出緩沖序號
  US nAcd; //所屬的ACD緩沖序號
  US nAG;
  UC CallPoint; //

public:
	CCalloutRecd()
  {
    Init();
  }
	virtual ~CCalloutRecd()
  {
  }
  void Init()
  {
    state = 0;
  }
};

//呼出隊列
class CCalloutQue
{
private:
  bool isInit;

public:
	CCalloutQue();
	virtual ~CCalloutQue();
	
  int MaxQueNum;
	CCalloutRecd	  *pQue_str;

  int Init(int maxquenum);

  void TimerAdd(int nTCount);
  bool isnQueAvail(int nQue)
  {
    return nQue < MaxQueNum ? true : false;
  }
  void InitQue(int nQue);
  int Get_Idle_nQue();
};

//-----------------------------------------------------------------------------
//--------------------------------callout指令----------------------------------
//被叫信息
class CCalled
{
public:
  CH CalledNo[MAX_TELECODE_LEN]; //被叫號碼
  UC CalledType; //被叫號碼類型 1-外線 2-內線
  CCalloutRecd *pQue_str; //占用的呼叫隊指針
  UC CalloutResult; //參考呼出結果宏定義
  US OutnChn; //呼出通道號
  US RouteNo; //呼出路由 edit 2007-10-14 add
  US CurRouteNo; //當前呼出的路由 edit 2007-10-19 add
  US CurPoint; //當前呼出路由的序號 edit 2007-10-20 add
  US nAG;
public:
  CCalled()
  {
    pQue_str = NULL;
    nAG = 0xFFFF;
    CalloutResult = 0;
  }
};

//會話參數
class CSession
{
public:
	UL SessionNo; //會話序列號
	UL CmdAddr; //該會話分配坐席的指令行地址
  US nChn; //該會話的通道號
  UL DialSerialNo; //呼叫序列號
	US SerialNo; //呼出子標志號

  UC CmdId; //發起呼叫的流程指令 1-callout指令 2-transfer指令 3-坐席轉接呼出 4-轉接振鈴后控制轉接的通道退出轉接
  UC VxmlId; //解析器編號
  UC FuncNo; //流程功能號
  CStringX Param; //附加參數
  
  CStringX CallerNo; //主叫號碼 用于彈屏
  CStringX CalledNo; //被叫號碼 用于彈屏
  
  CH CdrSerialNo[MAX_CHAR64_LEN]; //轉接的目的通道將要寫的話單編號
  CH RecordFileName[MAX_CHAR128_LEN]; //轉接的目的通道將要錄的錄音文件名

  void GenerateCdrSerialNo(int lgChnType, int lgChnNo);

  void Init();
};

//呼出記錄緩沖區數據
class CCalloutBuf
{
public:
	UC state; //數據狀態 0: 無 1：有
  UL timer; //呼出計時器
  UC CallStep;

  US RingTime; //呼出等候應答時間
  US IntervalTime; //呼出失敗后重新呼叫間隔時間(當CallNum>1時有效)

  CSession Session; //發起該呼叫的會話參數

  CStringX CallerNo; //主叫號碼
  CStringX OrgCalledNo; //主叫號碼
	UC CallerType; //主叫類別
	UC CallType; //呼叫類別 0-普通呼叫 1-話務員呼叫
  UC CallSeatType; //呼叫坐席電話類型: 0-呼入來電 1-轉接來電 2-呼出電話 3-代接電話

  UC CancelId; //呼出控制標志 0-無呼出 1-正在呼出 2-取消呼出
	US RouteNo[MAX_CALLED_NUM]; //呼叫路由
  UC RouteNum; //呼出路由數
  
  UC CallMode; //呼叫模式 0-順呼 1-同呼
  UC CallTimes; //呼叫次數
  UC CalledTimes; //已經呼叫次數(當有多個被叫時，只有所有被叫呼叫一遍才算1次)
  UC CalledNum; //被叫號碼個數
  UC StartPoint; //開始呼叫的被叫指針
  UC CallPoint; //當前呼叫的被叫號碼指針（當有多個號碼且為順呼方式時）
  US OutnChn; //指定呼出的通道號(只有在被叫號碼為1個時有效)
  CCalled Calleds[MAX_CALLED_NUM];
  time_t CallTime; //開始呼叫時間

  UC TranMode; //轉接類型:1-盲轉 2-協商轉接 3-會議轉接
  CStringX WaitVocFile; //轉接時聽的等待音
  UC TranCallFailReturn;
  int nAG;
  
public:
	CCalloutBuf()
  {
    Init();
  }
	virtual ~CCalloutBuf()
  {
  }
  void Init()
  {
    state = 0;
  }
};

class CCallout
{
private:
  bool isInit;

public:
	CCallout();
	virtual ~CCallout();
	
  int MaxOutNum;
	CCalloutBuf	  *pOut_str;

  int Init(int maxoutnum);

  void TimerAdd(int nTCount);
  bool isnOutAvail(int nOut)
  {
    return nOut < MaxOutNum ? true : false;
  }
  void InitOut(int nOut);
  int Get_Idle_nOut();
  int Get_nOut_By_DialSerialNo(UL DialSerialNo);
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//被分配的坐席信息
class CACDCalled
{
public:
  US nAG; //座席邏輯編號
  CCalloutRecd *pQue_str; //占用的呼叫隊指針
  UC CalloutResult; //參考呼出結果宏定義
  
public:
  CACDCalled();
};

//會話參數
class CACDSession
{
public:
	UL SessionNo; //會話序列號
	UL CmdAddr; //該會話分配坐席的指令行地址
  US nChn; //該會話的通道號
  UL DialSerialNo; //呼叫序列號
	US SerialNo; //呼出子標志號

  UC CallerType; //主叫類別(bit0-bit3: 0-內線分機 1-普通市話 2-國內備用 3-國內有效 4-國際)  //add 2010-06-03
  UC CallType; //呼叫類別 0-普通呼叫 1-話務員呼叫 //add 2010-06-03

  CStringX CallerNo; //主叫號碼
  CStringX CalledNo; //被叫號碼

  UC CmdId; //發起呼叫的流程指令 1-callseat指令 2-綁定遠程坐席呼叫坐席電話 3-坐席轉接呼叫坐席電話
  UC VxmlId; //解析器編號
  UC FuncNo; //流程功能號
  CStringX ServiceType; //流程傳來的選擇服務類型
  CStringX CRDSerialNo; //流程傳來的詳細話單號
  CStringX Param; //流程傳來的附加參數
  int SrvType;
  int SrvSubType;
  UC MediaType; //媒體類型：0-電話，1-IM，2-email，3-fax
};

//分配規則參數
class CACDRule
{
public:
  UC SeatType; //需要分配的坐席類型
  
  CStringX SeatNo; //需要分配的坐席號 “0”表示不區分座席號
  US WorkerNo; //需要分配的工號 “0”表示不區分工號

  UC GroupNo[MAX_GROUP_NUM_FORWORKER]; //話務員組號(0-不區分組號,初始值置為0xFF)
  UC Level[MAX_GROUP_NUM_FORWORKER]; //話務分配級別(0-不區分級別,初始值置為0)
  UC NowLevel[MAX_GROUP_NUM_FORWORKER]; //當前分配的話務級別,初始值為Level
  UC SeatGroupNo; //需要分配的坐席組號 (0-不區分組號)
  UC AcdedGroupNo; //分配了的組號
  UC AcdedGroupIndex; //分配了的組號的索引順相
  UC AcdedLevel; //分配了的技能級別
  
  UC AcdRule; //坐席分配規則(0-循環振鈴順序分配 1-固定振鈴順序分配 2-循環應答順序分配 3-優先分配應答次數最少的座席 4-優先分配服務時長最少的座席 5-優先分配應答次數最少的話務員 6-優先分配服務時長最少的話務員 7-IVR返回服務的工號 8-閑置時間最長的優先(2016-02-12))
  UC LevelRule; //話務員級別分配原則（0-不區分 1-分配等于Level定義的話務員 2-分配大于等于Level定義的話務員 3-分配小于等于Level定義的話務員 4-從Level級別開始向高級別分配 5-從Level級別開始向低級別分配）

  UC CallMode; //呼叫模式標志(0-單個戶呼叫 1-同時呼叫指定話務員組號 2-直接取空閑的坐席號 3-查詢登錄的坐席數)

  US WaitTimeLen; //總等待時長
  US RingTimeLen; //等待每個坐席振鈴時長
  UC BusyWaitId; //坐席全忙時等待標志(0-不等待 1-等待空閑坐席直到超時，2-等待空閑坐席（跳過已分配過的坐席）直到超時，3-先取出所有的而符合條件的坐席，然后按順序分配) //2015-12-07
  UC Priority; //優先分配標志(0-255)
  
  time_t QueueTime; //進入ACD隊列，排隊等待時間
  time_t StartRingTime; //已成功分配到坐席，坐席開始振鈴時間
  short AcdednAGList[MAX_AG_NUM]; //已經分配過的座席序號（初始值置為-1，防止分配后該座席未應答后反復分配）

  int AcdedCount; //已嘗試ACD分配次數

  UC TranCallFailCallBack; //轉接失敗后返回的呼叫
};

//ACD分配數據類
class CACDdata
{
public:
  UC state; //數據狀態 0: 無 1：有
  US timer; //狀態定時器
  US waittimer; //等待定時器
  US ringtimer; //振鈴定時器
  
  UC acdState; //分配狀態：0-等待分配 1-等待呼叫結果 2-已經振鈴,等待應答結果 3-已分配過坐席但未成功應答，等待時間未到，繼續等待重新分配
  UC StopId; //取消分配坐席標志：0-否 1-是
  
  CACDSession Session; //發起該呼叫的會話參數
  CACDRule ACDRule; //分配規則
  UC CallSeatType; //呼叫坐席電話類型: 0-呼入來電 1-轉接來電 2-呼出電話 3-代接電話

  UC CalledNum; //同時呼出的坐席號碼個數
  CACDCalled Calleds[MAX_CALLED_NUM]; //同時呼叫指定話務員組號時，呼出的坐席號碼信息
	
  US Prior; //對應的前一序號, 0表示第1個
	US Next; //對應的下一序號, 0xFFFF表示結束
};


//ACD分配隊列
class CACDQueue
{
private:
  bool isInit;

public:
  int MaxAcdNum;
	CACDdata *pACD_str;

  int AcdCount; //ACD隊列總等待數
  int WaitAcdCount; //等待分配空閑坐席數
  int WaitAnsCount; //已分配到空閑坐席等待應答數
  CStringX CallerList;

public:
  CACDQueue();
  virtual ~CACDQueue();

  void InitAcd(int nAcd);
  int Init(int maxacdnum);
  void TimerAdd(int nTCount);

  bool isnAcdAvail(int nAcd)
  {
    return nAcd < MaxAcdNum ? true : false;
  }
  int Get_Idle_nAcd();
  void JoinACDQueue(int nAcd);
  void DeleACDQueue(int nAcd);
  int Get_nAcd_By_DialSerialNo(UL DialSerialNo);
  
};

//---------------------------------------------------------------------------
#endif
