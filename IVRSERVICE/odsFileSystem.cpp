//////////////////////////////////////////////////////////////
//
// odsFileSystem.cpp
//
//		和文件系統相關的函數
//
// by Wang Yong Gang, 1999-10-16
// 在 張軍 1998/05/20 版本基礎上完成
//
//////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "odsFileSystem.h"

//------------------------------------------------------------
// 刪除路徑名最后的反斜線(格式1)
//------------------------------------------------------------
void odsRemoveLastSlash( LPTSTR lpszDirectory )
{
	int	i = lstrlen(lpszDirectory);
	if (i == 0)
		return;
 
	// 如果最后一個字符是(\)，則刪除它	
	if ( lpszDirectory[i - 1] == _T('\\') )
		lpszDirectory[i - 1] = _T('\0');	
}

//------------------------------------------------------------
// 刪除路徑名最后的反斜線(格式2)
//------------------------------------------------------------
void odsRemoveLastSlash( CString& strDirectory )
{
	TCHAR* p = strDirectory.GetBuffer(0);
	odsRemoveLastSlash( p );
	strDirectory.ReleaseBuffer();
}

//------------------------------------------------------------
// 判斷由路徑名和文件名合成的全稱文件名長度是否超過了系統允許的最大長度
//------------------------------------------------------------
BOOL odsIsFullNameOverflow( LPCTSTR lpszDirectoryName, LPCTSTR lpszFileName )
{	
	return ( lstrlen(lpszDirectoryName) + lstrlen(lpszFileName) + 1
				> _MAX_PATH ) ? TRUE : FALSE;
}

//------------------------------------------------------------
// 從文件全路徑名中分離出不包含路徑的文件名
//------------------------------------------------------------ 
void odsGetFileNameFromPath( LPCTSTR lpszFileFullPath, LPTSTR lpszFileName )
{
	TCHAR szFileName[_MAX_PATH];
	TCHAR szExtName[_MAX_PATH];

	//從文件全路徑中分離出文件名和擴展名
	_tsplitpath( lpszFileFullPath, NULL, NULL, szFileName, szExtName );
	
	//以文件名和擴展名合成文件名
	wsprintf( lpszFileName, _T("%s%s"), szFileName, szExtName );	
}

//------------------------------------------------------------
// 判斷兩個目錄是否在同一個驅動器上
//------------------------------------------------------------
BOOL odsIsDirInSameDriver( LPCTSTR lpszDirectory1, LPCTSTR lpszDirectory2 )
{
	TCHAR	szDriver1[_MAX_DRIVE + 1];
	TCHAR	szDriver2[_MAX_DRIVE + 1];

	// 從路徑中取出驅動器號
	_tsplitpath( lpszDirectory1, szDriver1, NULL, NULL, NULL );
	_tsplitpath( lpszDirectory2, szDriver2, NULL, NULL, NULL );

	//比較驅動器號是否相等
	if ( _tcsicmp(szDriver1, szDriver2) == 0 )
		return TRUE;
	else
		return FALSE;
}

//------------------------------------------------------------
// 取得指定路徑的根目錄
//------------------------------------------------------------
void odsGetRootDir( LPCTSTR lpszDirectoryName, LPTSTR lpszRootDirectory )
{
	TCHAR	szDrive[_MAX_DRIVE];
	szDrive[0] = _T('\0');

	//分解目錄
	_tsplitpath( lpszDirectoryName, szDrive, NULL, NULL, NULL );

	//生成根目錄
	wsprintf(lpszRootDirectory, _T("%s\\"), szDrive);
}

//------------------------------------------------------------
// 判斷指定的文件是否存在
//------------------------------------------------------------
BOOL odsIsFileExist( LPCTSTR lpszFileName )
{
	WIN32_FIND_DATA	FindData;
	HANDLE			hFindData;

	//執行查找操作
	hFindData = FindFirstFile
			 (
			  lpszFileName,		//要查找的文件名稱
			  &FindData			//返回的查找結果
			 );

	//如果沒有找到
	if (hFindData == INVALID_HANDLE_VALUE)
		return FALSE;

	//關閉查找句柄
	FindClose(hFindData);

	//判斷找到的文件類型是否是文件
	if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		return FALSE;

	return TRUE;
}

//------------------------------------------------------------
// 判斷指定的目錄是否存在
//------------------------------------------------------------
BOOL odsIsDirExist( LPCTSTR lpszDirectoryName )
{
	WIN32_FIND_DATA	FindData;
	HANDLE				hFindData;
	TCHAR				szRootDirectory[_MAX_DRIVE + 2];

	//判斷是否是根目錄
	odsGetRootDir( lpszDirectoryName, szRootDirectory );
	if (_tcsicmp(lpszDirectoryName, szRootDirectory) == 0)
		return TRUE; 

	//執行查找操作
	hFindData = FindFirstFile
			 (
			  lpszDirectoryName,	//要查找的目錄名稱
			  &FindData				//返回的查找結果
			 );

	//如果沒有找到
	if (hFindData == INVALID_HANDLE_VALUE)
		return FALSE;

	//關閉查找句柄
	FindClose(hFindData);

	//判斷找到的文件是否是目錄
	if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;
	else
		return FALSE;
}


//------------------------------------------------------------
// 取指定文件的大小( 返回 0xFFFFFFFF 表示失敗 )，
//		lpFileSizeHigh 可以為 NULL
//------------------------------------------------------------
DWORD odsGetFileSize( LPCTSTR lpszFileName, LPDWORD lpFileSizeHigh )
{
	WIN32_FIND_DATA	FindData;
	HANDLE			hFindData;

	//執行查找操作
	hFindData = FindFirstFile
			 (
			  lpszFileName,		//要查找的文件名稱
			  &FindData			//返回的查找結果
			 );

	//如果沒有找到
	if ( hFindData == INVALID_HANDLE_VALUE )
	{
		if (lpFileSizeHigh != NULL)
			(*lpFileSizeHigh) = 0xFFFFFFFF;
		return 0xFFFFFFFF;
	}

	//關閉查找句柄
	FindClose(hFindData);

	// 如果找到的是目錄，也返回失敗
	if ( FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
	{
		if (lpFileSizeHigh != NULL)
			(*lpFileSizeHigh) = 0xFFFFFFFF;
		return 0xFFFFFFFF;
	}

	if (lpFileSizeHigh != NULL)
		(*lpFileSizeHigh) = FindData.nFileSizeHigh;
	return FindData.nFileSizeLow;
}

//------------------------------------------------------------
// 達到目錄所在的硬盤剩余空間的大小，失敗則返回負數
//------------------------------------------------------------
__int64 odsGetDiskFreeSpace( LPCTSTR strDir )
{
	ULARGE_INTEGER ul1, ul2;
	if (!GetDiskFreeSpaceEx( strDir, &ul1, &ul2, NULL ))
		return (__int64)( -1 );
	else
		return (__int64)( ul1.QuadPart );
}

//------------------------------------------------------------
// 將目錄中的內容從原始目錄拷貝到目標目錄，且可包含子目錄
//------------------------------------------------------------
BOOL odsCopyDirectory
	( 
		LPCTSTR		lpszSourceDirectory,			// 原始目錄
		LPCTSTR		lpszDestinationDirectory,		// 目的目錄
		BOOL		bIncludeSubDir,					// 是否包含子目錄
		BOOL		bOverwriteExist,				// 是否覆蓋已有的目錄及文件
		STODSERROR* pstError						// 返回錯誤信息
	)
{
	LPTSTR				lpszExistFileName;
	LPTSTR				lpszNewFileName;
	LPTSTR				lpszPath;
	TCHAR				lpszBuffer[ 3 * _MAX_PATH ];

	lpszPath = lpszBuffer;
	lpszExistFileName = lpszBuffer + _MAX_PATH;
	lpszNewFileName = lpszBuffer + _MAX_PATH * 2;

	BOOL				bReturnCode = TRUE;
	WIN32_FIND_DATA		FindData;
	HANDLE				hFindData;

	//判斷原始路徑是否存在
	if (!odsIsDirExist(lpszSourceDirectory))
	{
		odsMakeError( pstError, ODS_ERR_NODIR, ODS_MSG_NODIR );
		return FALSE;
	}
 
	//生成查找的文件名
	wsprintf( lpszPath, _T("%s\\*.*"), lpszSourceDirectory );

	//判斷目標目錄是否存在，如果不存在則創建它
	if (!odsIsDirExist(lpszDestinationDirectory))
	{
		if (!CreateDirectory(lpszDestinationDirectory, NULL))
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_CREATEDIR );
			odsMakeError( pstError, ODS_ERR_CREATEDIR, NULL );
			return FALSE;
		}
	}
	else if ( !bOverwriteExist )
	{
		odsMakeError( pstError, ODS_ERR_OVERWRITE, ODS_MSG_OVERWRITE );
		return FALSE;
	}

	//執行查找
	hFindData = FindFirstFile(lpszPath, &FindData);
	if (hFindData == INVALID_HANDLE_VALUE)
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_BROWSEDIR );
		odsMakeError( pstError, ODS_ERR_BROWSEDIR, NULL );		
		return FALSE;
	}

	bReturnCode = TRUE;

	do
	{
		//判斷是否是本級目錄
		if (lstrcmp(FindData.cFileName, _T(".")) == 0)
			continue;

		//判斷是否是上級目錄
		if (lstrcmp(FindData.cFileName, _T("..")) == 0)
			continue;

		//判斷要生成的文件名的長度
		if( odsIsFullNameOverflow(lpszDestinationDirectory, FindData.cFileName) )
		{
			odsMakeError( pstError, ODS_ERR_NAMEOVERFLOW, ODS_MSG_NAMEOVERFLOW );
			bReturnCode = FALSE;
			break;
		}

		//生成查到的文件名
		wsprintf(lpszExistFileName, _T("%s\\%s"),lpszSourceDirectory, FindData.cFileName);
		//生成要創建的文件名
		wsprintf(lpszNewFileName, _T("%s\\%s"), lpszDestinationDirectory, FindData.cFileName);

		//根據查到文件的類型，做不同的操作
		if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if ( bIncludeSubDir )
				bReturnCode = odsCopyDirectory
						  (
						   lpszExistFileName, 
						   lpszNewFileName, 
						   bIncludeSubDir, 
						   bOverwriteExist,
						   pstError
						  );
		}
		else
		{
			if (! CopyFile( lpszExistFileName, lpszNewFileName, !bOverwriteExist ) )
			{
				odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_COPYDIR );
				odsMakeError( pstError, ODS_ERR_COPYDIR, NULL );
				bReturnCode = FALSE;
			}

			// 為防止從只讀介質上復制到硬盤后，文件帶有只讀屬性而無法修改
			// 需要在此處判斷并去除文件的只讀屬性
			DWORD dwAttr = GetFileAttributes( lpszNewFileName );
			if ( dwAttr & FILE_ATTRIBUTE_READONLY )
				SetFileAttributes( lpszNewFileName, dwAttr & (~FILE_ATTRIBUTE_READONLY) );
		}

		if (!bReturnCode)
			break;
	}while ( FindNextFile(hFindData, &FindData) );

	// 關閉查找句柄
	FindClose(hFindData);
 	
	return(bReturnCode);
}


//------------------------------------------------------------
// 刪除指定的目錄
//	bIncludeSubDir = TRUE,  刪除指定的目錄及其所有子目錄（包括所有文件）
//	bIncludeSubDir = FALSE, 僅刪除指定目錄中的文件，不刪除子目錄和子目錄
//							內的文件，也不刪除指定目錄本身。
//------------------------------------------------------------
BOOL odsRemoveDirectory( LPCTSTR lpszDirectoryName, BOOL bIncludeSubDir, STODSERROR* pstError )
{
	BOOL				bReturnCode = TRUE;	
	WIN32_FIND_DATA		FindData;
	HANDLE				hFindData;	
	TCHAR				szPath[_MAX_PATH];

	//判斷目錄是否存在
	if( !odsIsDirExist(lpszDirectoryName) )
	{
		odsMakeError( pstError, ODS_ERR_NODIR, ODS_MSG_NODIR );
		return(FALSE);
	}

	//生成查找的文件名
	wsprintf(szPath, _T("%s\\*.*"), lpszDirectoryName);
	
	//進行查找操作
	hFindData = FindFirstFile
				 (
				  szPath,
				  &FindData
				 );

	//如果找到文件
	if(hFindData != INVALID_HANDLE_VALUE)
	{
		do
		{
			//判斷是否是本級目錄
			if (lstrcmp(FindData.cFileName, _T(".")) == 0)
				continue;

			//判斷是否是上級目錄
			if (lstrcmp(FindData.cFileName, _T("..")) == 0)
				continue;

			//生成要刪除的文件名
			wsprintf(szPath, _T("%s\\%s"), lpszDirectoryName, FindData.cFileName);

			//判斷文件的類型
			if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{				
				if ( bIncludeSubDir )
					bReturnCode = odsRemoveDirectory(szPath, TRUE, pstError);
			}
			else
			{
				if (! DeleteFile( szPath ) )
				{
					odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_REMOVEDIR );
					odsMakeError( pstError, ODS_ERR_REMOVEDIR, NULL );
					bReturnCode = FALSE;
				}				
			}

			if ( !bReturnCode )			
				break;			
		}while (FindNextFile(hFindData, &FindData));

		//關閉查找句柄
		FindClose(hFindData);		
	}	// end of if (hFindData != INVALID_HANDLE_VALUE)

	if (bReturnCode )
	{
		//刪除指定目錄本身
		if ( bIncludeSubDir )
			bReturnCode = RemoveDirectory(lpszDirectoryName);
		if (!bReturnCode)
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_REMOVEDIR );
			odsMakeError( pstError, ODS_ERR_REMOVEDIR, NULL );
		}
	}
	
	return(bReturnCode);
}

//------------------------------------------------------------ 
// 設置目錄內所有文件的屬性
//	bIncludeSubDir			是否包含子目錄內的文件
//------------------------------------------------------------ 
BOOL odsSetFileAttributesInDir( LPCTSTR lpszDirectoryName, DWORD dwFileAttributes, BOOL bIncludeSubDir, STODSERROR* pstError )
{
	BOOL				bReturnCode = TRUE;	
	WIN32_FIND_DATA		FindData;
	HANDLE				hFindData;	
	TCHAR				szPath[_MAX_PATH];

	//判斷目錄是否存在
	if( !odsIsDirExist(lpszDirectoryName) )
	{
		odsMakeError( pstError, ODS_ERR_NODIR, ODS_MSG_NODIR );
		return(FALSE);
	}

	//生成查找的文件名
	wsprintf(szPath, _T("%s\\*.*"), lpszDirectoryName);
	
	//進行查找操作
	hFindData = FindFirstFile
				 (
				  szPath,
				  &FindData
				 );

	//如果找到文件
	if(hFindData != INVALID_HANDLE_VALUE)
	{
		do
		{
			//判斷是否是本級目錄
			if (lstrcmp(FindData.cFileName, _T(".")) == 0)
				continue;

			//判斷是否是上級目錄
			if (lstrcmp(FindData.cFileName, _T("..")) == 0)
				continue;

			//生成要設置屬性的文件名
			wsprintf(szPath, _T("%s\\%s"), lpszDirectoryName, FindData.cFileName);

			//判斷文件的類型
			if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{				
				if ( bIncludeSubDir )
					bReturnCode = odsSetFileAttributesInDir(szPath, dwFileAttributes, TRUE, pstError);
			}
			else
			{
				if (!SetFileAttributes( szPath, dwFileAttributes ))
				{
					odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_SETATTR );
					odsMakeError( pstError, ODS_ERR_SETATTR, NULL );
					bReturnCode = FALSE;
				}
			}

			if ( !bReturnCode )
				break;			
		}while (FindNextFile(hFindData, &FindData));

		//關閉查找句柄
		FindClose(hFindData);		
	}	// end of if (hFindData != INVALID_HANDLE_VALUE)

	return(bReturnCode);
}

//------------------------------------------------------------
// 移動目錄 - bDirect 指明是直接移動還是先復制再刪除
//------------------------------------------------------------
BOOL odsMoveDirectory
		( 
			LPCTSTR lpszSourceDirectory, 
			LPCTSTR lpszDestinationDirectory, 
			BOOL bDirect,
			STODSERROR* pstError 
		)
{
	BOOL	bReturnCode;

	//判斷目錄是否存在
	if( !odsIsDirExist(lpszSourceDirectory) )
	{
		odsMakeError( pstError, ODS_ERR_NODIR, ODS_MSG_NODIR );
		return(FALSE);
	}

	if ( bDirect && odsIsDirInSameDriver( lpszSourceDirectory, lpszDestinationDirectory) )
	{
		//執行移動操作
		bReturnCode = MoveFile
			   (
			    lpszSourceDirectory,
				lpszDestinationDirectory
			   );
		if ( !bReturnCode )
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_MOVEDIR );
			odsMakeError( pstError, ODS_ERR_MOVEDIR, NULL );
		}
	}
	else
	{
		//執行拷貝操作
		bReturnCode = odsCopyDirectory
			   (
				(LPSTR)lpszSourceDirectory,
				(LPSTR)lpszDestinationDirectory,
				TRUE,									// include sub dir
				TRUE,									// overwrite
				pstError
			   );

		if ( bReturnCode )
		{
			//執行刪除操作
			bReturnCode = odsRemoveDirectory
			   (
			    (LPSTR)lpszSourceDirectory, 
				TRUE,
				pstError
			   );
		}
	}

	return(bReturnCode);
}


//------------------------------------------------------------
// 瀏覽目錄及其子目錄中指定類型的文件
// 參數說明:
// 	lpszDirectoryName	要瀏覽的目錄名稱
//	dwFileType			要瀏覽文件的類型，可為以下值或它們的組合
//							FILE_ATTRIBUTE_ARCHIVE 
//							FILE_ATTRIBUTE_COMPRESSED 
//							FILE_ATTRIBUTE_DIRECTORY 
//							FILE_ATTRIBUTE_HIDDEN 
//							FILE_ATTRIBUTE_NORMAL 
//							FILE_ATTRIBUTE_OFFLINE 
//							FILE_ATTRIBUTE_READONLY 
//							FILE_ATTRIBUTE_SYSTEM 
//							FILE_ATTRIBUTE_TEMPORARY 
//	bIncludeSubDir		是否包含子目錄中文件的標志
//							TRUE	包含子目錄中的文件
//							FALSE	不包含子目錄中的文件
//	FileBrowseFunc		回調函數的指針，在瀏覽過程中，瀏覽函數會將找到符合條件的
//						文件名傳給此函數，此回調函數可做相應的操作，不可以為 NULL
//	pstError			錯誤信息結構指針，可以為 NULL
//------------------------------------------------------------ 
BOOL odsBrowseDirectory
		(
			LPCTSTR lpszDirectoryName, 
			DWORD dwFileType, 
			BOOL bIncludeSubDir, 
			FILEBROWSEFUNC FileBrowseFunc, 
			STODSERROR* pstError
		)
{
	BOOL				bReturnCode = TRUE;
	WIN32_FIND_DATA		FindData;
	HANDLE				hFindData;	
	TCHAR				szPath[_MAX_PATH];

	pstError->nCode = 0; pstError->strMsg[0] = _T('\0');

	//判斷目錄是否存在
	if ( !odsIsDirExist(lpszDirectoryName) )
	{
		odsMakeError( pstError, ODS_ERR_NODIR, ODS_MSG_NODIR );
		return(FALSE);
	}

	//生成查找的文件名
	wsprintf(szPath, _T("%s\\*.*"), lpszDirectoryName);

	//進行查找操作
	hFindData = FindFirstFile
			 (
			  szPath,
			  &FindData
			 );

	//如果沒有找到
	if (hFindData == INVALID_HANDLE_VALUE)
		return(TRUE);

	bReturnCode = TRUE;
	do
	{
		//判斷是否是本級目錄
		if (lstrcmp(FindData.cFileName, _T(".")) == 0)
			continue;

		//判斷是否是上級目錄
		if(lstrcmp(FindData.cFileName, _T("..")) == 0)
			continue;

		//生成找到的文件名
		wsprintf(szPath, _T("%s\\%s"), lpszDirectoryName, FindData.cFileName);

		//如果是子目錄
		if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if (dwFileType & FILE_ATTRIBUTE_DIRECTORY)
				bReturnCode = FileBrowseFunc(szPath, pstError);

			if (bReturnCode  && bIncludeSubDir )
				bReturnCode = odsBrowseDirectory(szPath, dwFileType, bIncludeSubDir, 
												 FileBrowseFunc, pstError );
		}
		else
		{
			if (FindData.dwFileAttributes & dwFileType)
				bReturnCode = FileBrowseFunc(szPath, pstError);
		}

		if(!bReturnCode)
			break;
	}while (FindNextFile(hFindData, &FindData) );

	//關閉查找句柄
	FindClose(hFindData);

	return(bReturnCode);
}


//------------------------------------------------------------ 
// 取得指定目錄的屬性
//		包括此目錄包含的子目錄的個數，文件的個數，以及目錄以字節為單位的大小
//------------------------------------------------------------ 
BOOL odsGetDirProperty( LPCTSTR lpszDirectoryName, BOOL bIncludeSubDir, 
					 STDIRPROPERTY* lpDirectoryProperty, STODSERROR* pstError )
{
	BOOL				bReturnCode = TRUE;
	WIN32_FIND_DATA		FindData;
	HANDLE				hFindData;
	TCHAR				szPath[_MAX_PATH];
	STDIRPROPERTY		DirectoryProperty;

	lpDirectoryProperty->dwDirNum = 0;
	lpDirectoryProperty->dwFileNum = 0;
	lpDirectoryProperty->i64Size = 0;

	//判斷目錄是否存在
	if( !odsIsDirExist(lpszDirectoryName) )
	{
		odsMakeError( pstError, ODS_ERR_NODIR, ODS_MSG_NODIR );
		return(FALSE);
	}

	//生成查找的文件名
	wsprintf(szPath, _T("%s\\*.*"), lpszDirectoryName);

	//進行查找操作
	hFindData = FindFirstFile
			 (
			  szPath,
			  &FindData
			 );

	//如果沒有找到
	if (hFindData == INVALID_HANDLE_VALUE)
		return(TRUE);

	bReturnCode = TRUE;

	do
	{
		//判斷是否是本級目錄
		if (lstrcmp(FindData.cFileName, _T(".")) == 0)
			continue;

		//判斷是否是上級目錄
		if (lstrcmp(FindData.cFileName, _T("..")) == 0)
			continue;

		//生成找到的文件名
		wsprintf(szPath, _T("%s\\%s"), lpszDirectoryName, FindData.cFileName);

		//判斷文件的類型
		if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			lpDirectoryProperty->dwDirNum ++;
			if (bIncludeSubDir )
			{
				bReturnCode = odsGetDirProperty( szPath, TRUE, &DirectoryProperty, pstError );
				if( bReturnCode )
				{
					lpDirectoryProperty->dwDirNum += DirectoryProperty.dwDirNum;
					lpDirectoryProperty->dwFileNum += DirectoryProperty.dwFileNum;
					lpDirectoryProperty->i64Size += DirectoryProperty.i64Size;
				}
			}
		}
		else
		{
			lpDirectoryProperty->dwFileNum ++;
			lpDirectoryProperty->i64Size += 
				((__int64)FindData.nFileSizeHigh * (__int64)MAXDWORD) + (__int64)FindData.nFileSizeLow;
		}

		if (!bReturnCode)
			break;

	}while(FindNextFile(hFindData, &FindData) );

	//關閉查找句柄
	FindClose(hFindData);

	return(bReturnCode);
} 

