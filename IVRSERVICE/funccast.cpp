//---------------------------------------------------------------------------
#include "stdafx.h"
#include "funccast.h"
//---------------------------------------------------------------------------
//---------------摸锣传ㄧ计--------------------------------------------------
char *EncodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  rtn[j] = 'p';
  for(unsigned short i = 0; i < strlen(Pass); i++)
  {
    char Inc = (char)((GetTickCount()+i*11) % 19);
    j++;
    rtn[j] = char(Inc + 'a');
    j++;
    rtn[j] = char(Pass[i] + Inc - 15);
  }
  j++;
  rtn[j] = 's';
  Base64Encode(key, (UC *)rtn);
  return key;
}

char *DecodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  Base64Decode((UC *)key, Pass);
  for (unsigned short i=1; i<strlen(key)-2; i++)
  {
    char Inc = key[i] - 'a';
    i++;
    rtn[j++] = char(key[i] - Inc + 15);
  }
  return rtn;
}

//俱计才﹃Τ耞(0-Τ俱计才﹃ㄣ砰计1-禬璖瞅2-礚俱计才﹃)
int Check_Int_Str(const CH *ValueStr, int &IntValue)
{
	int len;
	char c;

	len = strlen(ValueStr);
	if (len == 0) return 1;
	for (int i = 0; i < len; i ++)
	{
		c = ValueStr[i];
		if (isdigit(c) == 0) 
			return 1;
	}
	IntValue = atoi(ValueStr);
	return 0;
}
int Check_long_Str(const CH *ValueStr, long &IntValue)
{
	int len;
	char c;

	len = strlen(ValueStr);
	if (len == 0) return 1;
	for (int i = 0; i < len; i ++)
	{
		c = ValueStr[i];
		if (isdigit(c) == 0) 
			return 1;
	}
	IntValue = atol(ValueStr);
	return 0;
}
//俱计锣才﹃
int Int_To_Str( int IntValue, CH *ValueStr )
{
    sprintf(ValueStr,"%d",IntValue);// itoa(IntValue, ValueStr, 10);
    return 0;
}
//疊翴计才﹃Τ耞
int Check_Float_Str(const CH *ValueStr, double &FloatValue )
{
	int len;
	char c;

	len = strlen(ValueStr);
	if (len == 0) return 1;
	for (int i = 0; i < len; i ++)
	{
		c = ValueStr[i];
		if (c < '0' && c > '9' && c != '.') 
			return 1;
	}
	FloatValue = atof(ValueStr);
	return 0;
}
//疊翴计锣才﹃
int Float_To_Str(double FloatValue, CH *ValueStr )
{
    sprintf(ValueStr, "%f", FloatValue);
    return 0;
}
//---------------------------------------------------------------------------
