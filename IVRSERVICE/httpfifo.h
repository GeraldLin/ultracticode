//---------------------------------------------------------------------------
#ifndef __RHTTPFIFO_H_
#define __RHTTPFIFO_H_
//---------------------------------------------------------------------------
#include <afxmt.h>
//---------------------------------------------------------------------------

typedef struct
{
  unsigned short MsgLen;
  char MsgBuf[4096];

}VXML_RECV_HTTP_STRUCT;

class CHTTPfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_RECV_HTTP_STRUCT     *pMsgs;
  
  CCriticalSection m_cCriticalSection;
  
public:
  CHTTPfifo()
  {
    Head=0;
    Tail=0;
    resv=0;
    Len=512;
    pMsgs=new VXML_RECV_HTTP_STRUCT[Len];
  }
  
  ~CHTTPfifo()
  {
    if (pMsgs != NULL)
    {
      delete []pMsgs;
      pMsgs = NULL;
    }
  }
  
  bool	IsFull()
  {
    unsigned short temp=Tail+1;
    return temp==Len?Head==0:temp==Head;
  }
  bool 	IsEmpty()
  {
    bool bResult;
    
    m_cCriticalSection.Lock();
    Head==Tail ? bResult=true : bResult=false;
    m_cCriticalSection.Unlock();
    
    return bResult;
  }
  VXML_RECV_HTTP_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
    if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(const char *MsgBuf, unsigned short MsgLen);
  unsigned short  Read(char *MsgBuf, unsigned short &MsgLen);
};

//---------------------------------------------------------------------------
#endif
