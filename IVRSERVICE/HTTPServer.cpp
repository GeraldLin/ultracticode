/****************************************************************************************
* ///////////////////////////////////////////////////////////////////////////////////////
*	Original Filename: 	HTTPServer.cpp
*
*	History:
*	Created/Modified by				Date			Main Purpose/Changes
*	Souren M. Abeghyan				2001/05/25		Implementation of the CHTTPServer class
*	
*	Comments:	
* \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
****************************************************************************************/
#include "stdafx.h"
#include "HTTPServer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHTTPServer::CHTTPServer()
{
	//
	// Init MIME Types
	//
	MimeTypes["doc"]	= "application/msword";
	MimeTypes["bin"]	= "application/octet-stream";
	MimeTypes["dll"]	= "application/octet-stream";
	MimeTypes["exe"]	= "application/octet-stream";
	MimeTypes["pdf"]	= "application/pdf";
	MimeTypes["p7c"]	= "application/pkcs7-mime";
	MimeTypes["ai"]		= "application/postscript";
	MimeTypes["eps"]	= "application/postscript";
	MimeTypes["ps"]		= "application/postscript";
	MimeTypes["rtf"]	= "application/rtf";
	MimeTypes["fdf"]	= "application/vnd.fdf";
	MimeTypes["arj"]	= "application/x-arj";
	MimeTypes["gz"]		= "application/x-gzip";
	MimeTypes["class"]	= "application/x-java-class";
	MimeTypes["js"]		= "application/x-javascript";
	MimeTypes["lzh"]	= "application/x-lzh";
	MimeTypes["lnk"]	= "application/x-ms-shortcut";
	MimeTypes["tar"]	= "application/x-tar";
	MimeTypes["hlp"]	= "application/x-winhelp";
	MimeTypes["cert"]	= "application/x-x509-ca-cert";
	MimeTypes["zip"]	= "application/zip";
	MimeTypes["cab"]	= "application/x-compressed";
	MimeTypes["arj"]	= "application/x-compressed";
	MimeTypes["aif"]	= "audio/aiff";
	MimeTypes["aifc"]	= "audio/aiff";
	MimeTypes["aiff"]	= "audio/aiff";
	MimeTypes["au"]		= "audio/basic";
	MimeTypes["snd"]	= "audio/basic";
	MimeTypes["mid"]	= "audio/midi";
	MimeTypes["rmi"]	= "audio/midi";
	MimeTypes["mp3"]	= "audio/mpeg";
	MimeTypes["vox"]	= "audio/voxware";
	MimeTypes["wav"]	= "audio/wav";
	MimeTypes["ra"]		= "audio/x-pn-realaudio";
	MimeTypes["ram"]	= "audio/x-pn-realaudio";
	MimeTypes["bmp"]	= "image/bmp";
	MimeTypes["gif"]	= "image/gif";
	MimeTypes["jpeg"]	= "image/jpeg";
	MimeTypes["jpg"]	= "image/jpeg";
	MimeTypes["tif"]	= "image/tiff";
	MimeTypes["tiff"]	= "image/tiff";
	MimeTypes["xbm"]	= "image/xbm";
	MimeTypes["wrl"]	= "model/vrml";
	MimeTypes["htm"]	= "text/html";
	MimeTypes["html"]	= "text/html";
	MimeTypes["c"]		= "text/plain";
	MimeTypes["cpp"]	= "text/plain";
	MimeTypes["def"]	= "text/plain";
	MimeTypes["h"]		= "text/plain";
	MimeTypes["txt"]	= "text/plain";
	MimeTypes["rtx"]	= "text/richtext";
	MimeTypes["rtf"]	= "text/richtext";
	MimeTypes["java"]	= "text/x-java-source";
	MimeTypes["css"]	= "text/css";
	MimeTypes["mpeg"]	= "video/mpeg";
	MimeTypes["mpg"]	= "video/mpeg";
	MimeTypes["mpe"]	= "video/mpeg";
	MimeTypes["avi"]	= "video/msvideo";
	MimeTypes["mov"]	= "video/quicktime";
	MimeTypes["qt"]		= "video/quicktime";
	MimeTypes["shtml"]	= "wwwserver/html-ssi";
	MimeTypes["asa"]	= "wwwserver/isapi";
	MimeTypes["asp"]	= "wwwserver/isapi";
	MimeTypes["cfm"]	= "wwwserver/isapi";
	MimeTypes["dbm"]	= "wwwserver/isapi";
	MimeTypes["isa"]	= "wwwserver/isapi";
	MimeTypes["plx"]	= "wwwserver/isapi";
	MimeTypes["url"]	= "wwwserver/isapi";
	MimeTypes["cgi"]	= "wwwserver/isapi";
	MimeTypes["php"]	= "wwwserver/isapi";
	MimeTypes["wcgi"]	= "wwwserver/isapi";
}

CHTTPServer::~CHTTPServer()
{

}


int CHTTPServer::GotConnection(char*, int)
{
	return 0;
}


int CHTTPServer::DataSent(DWORD)
{
	return 0;
}



BOOL CHTTPServer::Start(int Port, int PersTO)
{
	m_HomeDir		= "C:\\ServerRoot";
	m_DefIndex		= "index.htm";

	if(m_HomeDir.substr(m_HomeDir.size() - 1, 1) != "\\")
		m_HomeDir += "\\";

	return Run(Port, PersTO);
}


BOOL CHTTPServer::IsComplete(string szRequest)
{
  string szMethod, szLength;
  int n,n1,n2,len1,len2;

  n = szRequest.find(" ", 0);
  if(n == string::npos)
    return FALSE;

  szMethod = szRequest.substr(0, n);
  n2 = szRequest.find("\r\n\r\n", 0);
  if(n2 == string::npos)
  {
    return FALSE;
  }
  else
  {
    if(szMethod == "GET")
    {
      return TRUE;
    }
  }
  
  n = szRequest.find("Content-Length: ", 0);
  if(n == string::npos)
    return FALSE;
  
  n1 = szRequest.find("\r\n", n);
  if(n1 == string::npos)
    return FALSE;
  
  szLength = szRequest.substr(n + 16, n1-n-16);
  len1 = atoi(szLength.c_str());

  len2 = szRequest.length();

  if ((len2-n2-4) >= len1)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}




BOOL CHTTPServer::ParseRequest(string szRequest, string &szResponse, BOOL &bKeepAlive)
{
	//
	// Simple Parsing of Request
	//
	string szMethod;
	string szResult;
  string szPostParam("");
	string szFileExt;
	string szStatusCode("200 OK");
	string szContentType("text/html");
	string szConnectionType("close");
	string szNotFoundMessage;
	string szDateTime;
	char pResponseHeader[2048];
	fpos_t lengthActual = 0, length = 0;
	int n;
				
	//
	// Check Method
	//
	n = szRequest.find(" ", 0);
	if(n != string::npos)
	{
		szMethod = szRequest.substr(0, n);
		if(szMethod == "GET")
		{
			//
			// Get file name
			// 
			int n1 = szRequest.find(" ", n + 1);
			if(n1 != string::npos)
			{
				szPostParam = szRequest.substr(n + 1, 2);
				if(szPostParam == "/?")
				{
          szPostParam = szRequest.substr(n + 3, n1 - n - 3);
				}
        else
        {
          szPostParam = szRequest.substr(n + 1, n1 - n - 1);
        }
        m_HTTPfifo.Write(szPostParam.c_str(), szPostParam.length());
        LogErrorMessage(LOGFILENAME, "POSTPARAM", szPostParam.c_str());
        szResult = "OK";
			}
			else
			{
				LogErrorMessage(LOGFILENAME, "No 'space' found in Request String #1", "ParseRequest");
				return FALSE;
			}
		}
    else if(szMethod == "POST")
    {
      int n1 = szRequest.find("\r\n\r\n", 0);
      if(n1 != string::npos)
      {
        szPostParam = szRequest.substr(n1 + 4, szRequest.length()-n1-4);
        m_HTTPfifo.Write(szPostParam.c_str(), szPostParam.length());
        LogErrorMessage(LOGFILENAME, "POSTPARAM", szPostParam.c_str());
      }
      szResult = "OK";
    }
		else
		{
			szStatusCode = "501 Not Implemented";
			szResult = "FAIL";
		}
	}
	else
	{
		LogErrorMessage(LOGFILENAME, "No 'space' found in Request String #2", "ParseRequest");
		return FALSE;
	}

	//
	// Determine Connection type
	//
	n = szRequest.find("\nConnection: Keep-Alive", 0);
	if(n != string::npos)
		bKeepAlive = TRUE;

	//
	// Obtain current GMT date/time
	//
	char szDT[128];
	struct tm *newtime;
	long ltime;
	
	time(&ltime);
	newtime = gmtime(&ltime);
	strftime(szDT, 128,
		"%a, %d %b %Y %H:%M:%S GMT", newtime);

	if(szResult == "OK")				
	{
		sprintf(pResponseHeader, "HTTP/1.0 %s\r\nDate: %s\r\nServer: %s\r\nAccept-Ranges: bytes\r\nContent-Length: %d\r\nConnection: %s\r\nContent-Type: %s\r\n\r\n%s",
			szStatusCode.c_str(), szDT, SERVERNAME, 2, bKeepAlive ? "Keep-Alive" : "close", szContentType.c_str(), szResult.c_str());
	}
	else
	{
		sprintf(pResponseHeader, "HTTP/1.0 %s\r\nContent-Length: %d\r\nContent-Type: text/html\r\nDate: %s\r\nServer: %s\r\n\r\n%s",
			szStatusCode.c_str(), 4, szDT, SERVERNAME, szResult.c_str());
		bKeepAlive = FALSE;  
	}

	szResponse = string(pResponseHeader);
	

	return TRUE;
}
unsigned short CHTTPServer::Write(const char *MsgBuf, unsigned short MsgLen)
{
  return m_HTTPfifo.Write(MsgBuf, MsgLen);
}
unsigned short CHTTPServer::Read(char *MsgBuf, unsigned short &MsgLen)
{
  return m_HTTPfifo.Read(MsgBuf, MsgLen);
}