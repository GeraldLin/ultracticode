//---------------------------------------------------------------------------
#ifndef __BOXCONF_H_
#define __BOXCONF_H_
//---------------------------------------------------------------------------
//#include ""
//---------------------------------------------------------------------------
//會議數據
typedef struct
{
	UC state; //數據狀態 0: 無 1：有
  UL timer;
	UL PlayPauseTimer; //放音間隔定時器
	UL PlayedTimer; //已放音時長定時器
	UL RecTimer; //已錄音時長定時器

	US GroupNo; //對應的邏輯會議組號
  US RoomNo; //對應的邏輯會議室號

  UC CreateId; //創建結果標志: 0-未創建 1-正在創建 2-創建失敗 3-創建成功
  UC UseId; //使用標志：0-未使用 1-打開 2-關閉

  UC ChType; //建立會議的主邏輯通道類型
  US ChIndex; //建立會議的主邏輯通道號
  UC CreateChnExitId; //創建會議的主邏輯通道創建后退出標志(0-未 1-創建后已自動退出)
	
	CStringX Name; //[MAX_USERNAME_LEN]; //會議名稱
	CStringX Owner; //[MAX_USERNAME_LEN]; //所有者名稱
	CStringX Password; //[MAX_PASSWORD_LEN]; //加入密碼

  US MaxPresiders; //允許主持人數: 
  US MaxTalkers; //允許加入人數: 
	US MaxListeners; //創建會議時要預留1個旁聽會議通道, 允許旁聽人數 0-標志不限制,但不能大于系統定義的最大數
	UC FreeId; //最后一位成員退出后釋放會議資源標志(0-不釋放 1-釋放 2-當會議室只有2人時退出并自動雙向連接通話)
	UC AutoRecordId; //自動錄音標志(0-不 1-錄)(只有在會議室有人加入時才錄,為空后自動停止)

  US Presideds; //已加入主持人數
	US Talkeds; //已加入人數
	US Listeneds; //已旁聽人數

  US Presiders[MAX_PRESIDER_NUM]; //已加入主持人通道序列 0xFFFF表示無
	US Talkers[MAX_TALKERS_NUM]; //已加入人通道序列 0xFFFF表示無
	US Listeners[MAX_LISTENERS_NUM]; //已旁聽人通道序列 0xFFFF表示無
	UC EmptyEvent[MAX_LISTENERS_NUM]; //旁聽聊天室有空位事件標志(0-無 1-有)

  UC KnockTone; //加入會議敲門聲標志: 0-不播放 1-播放加入敲門聲 2-播放旁聽敲門聲

	VXML_PLAY_DATA_BUF_STRUCT curPlayData; //會議放音數據
	VXML_RECORD_DATA_BUF_STRUCT curRecData; //會議錄音數據
	VXML_PLAYRULE_STRUCT curPlayRule; //放音規則

	UC curPlayState; //當前放音狀態
					/*
					0: 未放音
					1: 放音正在進行。
					2: 放音正常結束。
					3: 放音操作因檢測到DTMF 按鍵字符而結束。
					4: 放音操作因檢測到bargein而結束。
					5: 放音操作因檢測到對端掛機而停止。
					6: 放音操作被應用程序停止
					7: 放音操作被暫停
					8: 放音操作因下總線而停止（只針對數字卡）
					9: 放音操作因網絡中斷而停止
          10: 放音分配資源失敗
					*/
	UC curRecState; //當前錄音狀態
					/*
					0: 未錄音
					1: 錄音正在進行 
					2: 錄音操作被應用程序停止
					3: 錄音操作因檢測到DTMF 按鍵字符而結束
					4: 錄音操作因檢測到對端掛機而結束
					5: 錄音操作自然結束（到達指定的字節長度或時間）
					6: 文件錄音操作被暫停
					7: 錄音操作因寫文件失敗而停止
					8: 放音操作因網絡中斷而停止
          10: 放音分配資源失敗
					*/
}VXML_CONF_STRUCT;

//---------------------------------------------------------------------------
class CBoxconf
{
private:
  bool isInit;

public:
	CBoxconf();
	virtual ~CBoxconf();

  int MaxCfcNum;
  VXML_CONF_STRUCT		*pCfc_str;
  
public:
  int Init(int TotalConf);
  
  void ClearPlayData(US nCfc);
  void ClearRecData(US nCfc);
  void ClearPlayRule(US nCfc);
  void InitConf(US ConfNo);
	void ResetConf(US ConfNo);
	
  void TimerAdd(int nTCount);
  int GetMaxCfcNum()
  {return MaxCfcNum;}
  bool isnCfcAvail(int nCfc)
  {
    return nCfc < MaxCfcNum ? true : false;
  }
  int Get_Idle_nCfc(); //取空閑的會議室號
  
  int MyCreateOneConf(int nCfc); //創建會議
  int CreateAllSysConf(int ncfc); //創建系統會議室
  void Read_ConfIniFile(char *filename); //讀會議配置文件
  void Read_OneConfIniFile(char *filename, int nCfc);
};
//---------------------------------------------------------------------------
#endif
