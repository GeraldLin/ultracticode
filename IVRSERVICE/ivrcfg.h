//---------------------------------------------------------------------------
#ifndef __IVRCFG_H_
#define __IVRCFG_H_
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#define MAX_ROUTE_NUM			      128 //最大路由數 2013-09-22路由數從32增加到128
#define MAX_ROUTE_TRUNK_NUM     512 //每條路由最大中繼數
#define MAX_CHANGE_NUM			    32 //最大號碼轉換數
#define MAX_CHANGE_EXT_NUM      64 
#define MAX_CHANGE_VOIP_NUM     128 
#define MAX_CHANNEL_REC_NUM     128

//企業內部分機結構
class CPBXExtrn
{
public:
  char ExtrnPreCode[10];
  int  ExtrnLen;
public:
  CPBXExtrn()
  {
    memset(ExtrnPreCode, 0, 10);
    ExtrnLen = 0;
  }
};
class CPBXExtrnList
{
public:
  int ExtrnPreNum;
  CPBXExtrn PBXExtrn[20];
public:
  CPBXExtrnList()
  {
    ExtrnPreNum = 0;
  }
};
//與第三方WEBSERVICE接口配置參數
class CWebServiceParam
{
public:
  char WS_wsdladdress[256];
  char WS_soapaction[256];
  char WS_startenvelope[256];
  char WS_startbody[256];
  char WS_interface[256];
  short WS_paramcount;
  char WS_paramstring[32][256];
public:
  CWebServiceParam()
  {
    memset(WS_wsdladdress, 0, 256);
    memset(WS_soapaction, 0, 256);
    memset(WS_startenvelope, 0, 256);
    memset(WS_startbody, 0, 256);
    memset(WS_interface, 0, 256);
    WS_paramcount = 0;
    for (int i=0; i<32; i++)
    {
      memset(WS_paramstring[i], 0, 256);
    }
  }
};

class CIVRCfg
{
public:
	CIVRCfg();
	virtual ~CIVRCfg();
	
  CH AppPath[MAX_PATH_LEN];
  CH INIFileName[MAX_PATH_FILE_LEN];

  bool AutoSendACMId;	//號碼收全后自動送ACM信號，默認為true
  bool isAutoSendACM()
  {return AutoSendACMId;}
  bool AutoSendANCId; //自動發送ANC信號
  bool isAutoSendANC()
  {return AutoSendANCId;}

  int  VoiceCardInitState; //雙機模式時語音卡初始狀態：0-正常初始化，1-主機啟動時初始化語音卡，備機不初始化語音卡，主備倒換時再初始化

  bool isWriteTimerDogLogId;
  int  WriteTimerDogLogCount;

  bool isDigitOneSayYao;
  bool isMCITestFlag;

  UC CountCycle; //統計周期標志
  bool isCountCall; //是否統計話務量
  bool isWriteCountCallTxt; //是否將統計話務量寫到文本文件
  bool isWriteCountCallDB; //是否將統計話務量寫到數據庫
  bool isAutoDelPSTNPreCode; //是否自動刪除模擬外線匯線通字冠
  bool isAutoDelMobilePreCode0; //是否自動刪除手機號碼前面的0
  bool isAutoAddTWMobilePreCode0; //是否自動添加臺灣手機號碼前面的0
  bool isAutoWritePSTNPreCode; //是否自動添加模擬外線匯線通字冠
  bool isSendRouteIdelTrkNum; //是否實時發送路由空閑中繼數給流程解析器
  bool isSendACDCallToAgent; //是否由IVR服務發送acd電話分配到電腦坐席
  bool isHangonAutoCancelCall; //通道掛機時是否自動取消該通道發起的呼叫
  bool isRecordTranOutCall; //是否對轉接的外線錄音
  bool isWriteSeatStatus; //是否寫坐席狀態流水記錄
  bool isWriteSeatStatusOnLogout; //不登錄是否也寫坐席狀態流水記錄 2016-03-17
  bool isOnlySendMySeatStatus; //是否只送本坐席狀態
  bool isOnlySendACDQueueToLeader; //是否只送排隊信息給管理者
  bool isKeepLastSeatStatus; //坐席重新登錄時是否維持以前最后的狀態
  bool isUpdateSeatStateToDB; //是否寫坐席實時狀態到數據表
  bool isUpdateACDQueueToDB; //是否寫實時排隊信息到數據表
  bool isUpdateAgentSumDataToDB; //是否寫坐席實時統計數據到數據表
  bool isUpdateGroupSumDataToDB; //是否寫話務組實時統計數據到數據表
  int  isWriteAlarmToDB; //是否寫告警信息到數據庫：1-寫到DB,2-寫到sqlite數據庫
  bool isCheckPasswordWebLogin; //2015-10-22坐席登錄時由CTI驗證話務員資料時，是否需要密碼
  bool isOnlyPickupCallExistSeatNo; //2016-06-13只代接存在的坐席分機
  
  bool isSendSeatStateToGW; //是否發送坐席狀態到webservice接口
  bool isAppendSeatDailDTMF; //是否將坐席撥的后續DTMF作為被叫號碼
  bool isSeatRecordAfterTalk; //交換機坐席通道錄音方式：false-摘機應答后就開始錄 true-實際通話后開始錄
  int  RecordOneFileForLocaCall; //內部呼叫是否只錄1個文件
  int  TranCallFailReturnType; //坐席轉接電話在未轉通前掛機，但失敗后是否再返回該坐席。0-不返回 1-返回該坐席號 2-返回該坐席所在組號
  bool isAgentLoginSwitch; //坐席是否登錄到交換機的ACD隊列
  bool isSetREADYonAgentLogin; //坐席登陸時是否設置READY到交換機 //2014-10-15
  int  isSetReadyStateonAgentSetDisturb; //2016-10-13 坐席設置示忙示閑時是否設置NOTREADY、READY到交換機: 0-不設置，1-SETBUSY,SETLEVEL,SETACW都設置，2-SETBUSY,SETLEVEL設置,SETACW 掛機時不設置
  bool isSetNOTREADYonAgentLogout; //坐席出陸時是否設置NOTREADY到交換機 //2014-10-15
  bool isAgentLoginACDSplit; //坐席是否登錄到交換機的ACD組
  bool isWorkerLogOutAfterunTcpLink; //坐席與服務器網絡斷開后，是否自動退出話務員

  int  AgentLogOnOffMode; //坐席登錄模式：0-原來的標準模式，1-Logon為登錄群組，Logoff登出群組及DND,示忙DND,小休DND

  int  AutoSetAgentLeaveNoAnsCount; //累計多少次未應答ACD來話時將該坐席置于離席狀態,0表示不設置
  bool isACDToAgentWhenChBlock; //當坐席通道阻塞時是否允許分配坐席（交換機版本當有監錄時）
  bool isCTILinkCanRecord; //CTILINK接口是否可以錄音
  bool isExternalRecord; //是否是外置錄音機
  bool isOnlyRecSystem; //只是錄音系統
  bool isInsertRecCallidList; //是否插入詳細通話SerialNo與錄音系統CALLID對照資料表 2016-02-12
  int  MaxWaitIVRReturnTimelen; //等待IVR返回坐席最大等待時長 2016-02-12
  
  bool isAutoAddDailOutPreCode; //是否客戶端呼出時自動加撥出局碼
  bool isAutoAddTranOutPreCode; //是否客戶端轉外線時自動加撥出局碼
  bool isAutoDelDailOutPreCode; //寫詳細話單時是否自動刪除呼出號碼前的出局碼

  bool isAutoAddDailOutIPCode; //是否客戶端轉外線時自動加撥出局碼
  bool isAutoDelDailOutIPCode; //寫詳細話單時是否自動刪除呼出號碼前的IP字冠

  bool isCDRSwapCallerCalledNo; //寫詳細話單時是否需要調換主被叫號碼字段位置
  bool isUpdateDialOutId; //是否更新人工外呼記錄的DialOutId到tbcallcdr，和把SerialNo寫到人工外呼記錄
  bool isUpdateCRMCallId; //是否更新CRM外呼記錄的CallId到tbcallcdr，以便crm查詢
  bool isInsertDialOutListId; //是否插入手動外呼的詳細記錄
  CH   UpdateDialOutIdFieldName[50];
  CH   UpdateDialOutRelFieldName[50];
  int  MinCDRCalledNoLen; //話單中允許的最小外呼號碼長度，小于該長度的將刪除
  bool isInsertIVRDTMFTraceId; //是否插入未通過IVR的呼入呼出的跟蹤記錄
  bool isCDRInsertDeptNo; //是否在話單里插入部門編號
  bool isCDRInsertOrgCallNo; //是否在話單里插入原主、被叫號碼
  bool isCDRInsertCallUCID; //是否在話單里插入呼叫唯一標識
  bool isSumCallExtnToExtn; //是否統計分機之間的呼叫 //2015-12-26

  int  GetIdleSeatCountType; //2016-05-10統計空閑坐席數方式：1-只返回空閑數，2-返回空閑數和通話數，中間用逗號隔開

  int  MaxTranOutTalkTimeLen; //轉接呼出最長通話時長秒
  
  bool isClearLastACDnAG0Hour; //是否在每天0點清除ACD排隊記錄，重新從第1個坐席開始
  int  ClearCallCountType; //實時話務統計定期清零方式 0-不清零 1-每天0時清零 2-整點清零 3-不同工號登錄時清零 4-不同班次號登錄時清零 5-按定制時間清零
  CH   ClearTimeList[24][5]; //自動清楚時間列表,格式為: HH:MM;HH:MM ... 必須按從小到大排列
  int  ClearTimeListNum;
  
  int  ControlCallModal; //呼叫控制模式: 0-全部由流程解析器控制 1-由本服務控制寫詳細話單,控制呼叫
  int  SP_InsertCDR; //是否通過存儲過程插入話單
  int  SP_UpDateAreaName; //是否通過存儲過程更新區域名稱
  CH   WaitVocFile[256];
  bool isDispSQLMsg; //是否顯示sql通信信息
  int  MaxWorkerNoLen; //工號固定長度，位數不夠左補0
  bool isInsertCallTrace;

  int  MinChangeDutyLen; //最短倒班間隔長(小時)
  
  int DebugId; //調試信息顯示標志
  bool isDebug()
  {return DebugId>=1?true:false;}

	//與呼叫有關的配置
  CH CenterCode[20];			//本系統局向號(呼出時如該項為空則呼出主叫號碼為流程指令CallerNo設置的號碼、非空時為該號碼)
  bool CallerId;              //呼出時主叫號碼標志，為1時則將CenterCode作為主叫號碼

  int ProcLocaAreaCodeId; //來電主叫號碼中本地區號處理方式：0-不處理，1-刪除市話號碼前的本地區號，2-市話號碼前增加本地區號
  int ProcLocaAreaCodeId1; //來電被叫號碼中本地區號處理方式：0-不處理，1-刪除市話號碼前的本地區號，2-市話號碼前增加本地區號
  int LocaTelCodeLen; //本地市話號碼長度
  int LocaTelCodeLen1; //本地市話號碼長度1
  CH LocaAreaCode[20]; //本地區號
  CH LocaAreaCode1[20]; //本地區號1
  CH LocaAreaCode2[20]; //本地區號2
  CH LocaAreaCode3[20]; //本地區號3
  CH LocaAreaCode4[20]; //本地區號4
  
  CH CentrexCode[20]; //交換機呼入主叫中的匯線通字冠，如：9,0

  UC CalledChangeId;        //該參數為被叫號碼轉換標志(1：轉換 0：不轉換)
  CH OldCalled[MAX_CHANGE_NUM][20];      //轉換前的號碼
  CH NewCalled[MAX_CHANGE_NUM][20];      //轉換后的號碼

  bool isClearCallerNoChar; //是否清除模擬外線主叫號碼中的字符
  UC CallerChangeId;        //該參數為主叫號碼轉換標志(1：轉換 0：不轉換)
  CH OldCaller[MAX_CHANGE_NUM][20];      //轉換前的號碼
  CH NewCaller[MAX_CHANGE_NUM][20];      //轉換后的號碼

  CH DialOutPreCode[20]; //呼出加撥字冠,如先撥9+外線號碼
  int AnsAfterRingCount; //模擬外線呼入后響幾次鈴才應答
	UC PSTNChangeId;			//模擬分機號碼轉換標志(1：轉換 0：不轉換)
	CH PSTNCode[MAX_CHANGE_EXT_NUM][20];			//轉換后的被叫號碼
	
  CH PreCode[MAX_CHANGE_EXT_NUM][20];			//呼出時插撥的號碼,主要針對外線為匯線通或為企業總機
  CH IPPreCode[MAX_CHANGE_EXT_NUM][20];			//呼出時插撥的IP字冠,主要針對長途呼出

  CH RecLineCode[MAX_CHANNEL_REC_NUM][20];			//模擬監錄通道的號碼

  CPBXExtrnList PBXExtrnList[MAX_CHANGE_EXT_NUM];

  bool isHangoffOnIVRCallout; //2015-10-20 交換機版本自動外呼時先自動摘機

  CH SWTDialOutPreCode[20]; //交換機呼出加撥字冠,如先撥9+外線號碼
  CH SWTDialOutPreCode1[20]; //交換機呼出加撥字冠,如先撥9+外線號碼
  CH SWTDialOutPreCode2[20]; //交換機呼出加撥字冠,如先撥9+外線號碼
  CH SWTDialOutPreCode3[20]; //交換機呼出加撥字冠,如先撥9+外線號碼
  CH SWTDialOutPreCode4[20]; //交換機呼出加撥字冠,如先撥9+外線號碼
  CH SWTPreCode[20];			//交換機呼出時插撥的號碼,主要針對外線為匯線通或為企業總機
  CH SWTIPPreCode[20];			//交換機呼出時插撥的IP字冠,主要針對長途呼出
  CH SWTPreCodeEx[20]; //針對查詢異地手機轉換后，強制加的出局碼 2016-11-28
  CPBXExtrnList SWTPBXExtrnList; //交換機呼出不需要加撥占線字冠的例外設置

  //數據表轉存參數
  bool isDumpCRMRecord;
  UC SourDBId;
  CH QuerySqls[MAX_SQLS_LEN];
  UC DestDBId;
  CH InsertSqls[MAX_SQLS_LEN];

  //通過webservice查詢客戶資料
  bool isQueryCustomerByWebservice;
  CH QueryCustomerWebserviceAddr[64];
  CH QueryCustomerWebserviceParam[128];
  
  UC VOIPChangeId; //VOIP呼入被叫號碼轉換標志(1：轉換 0：不轉換)
  CH VOIPDefaultCalled[20];
  CH NewVOIPCalled[MAX_CHANGE_VOIP_NUM][20]; //轉換后的號碼

  CH OXEAlternateCallDTMF[20]; //

  int MaxInRingOverTime; //最大振鈴超時警告時長
  int MaxOutRingOverTime; //最大振鈴超時警告時長
  int MaxTalkOverTime; //最大通話超時警告時長
  int MaxACWOverTime;

  CStringX MemIndexPath; //內存語音索引文件路徑
  CStringX PlayFilePath; //放音文件路徑
  CStringX BoxRecPath; //普通留言文件路徑
  CStringX SeatRecPath; //坐席監錄語音文件路徑
  CStringX ConfRecPath; //會議自動錄音路徑
  CStringX SendFaxPath; //發送傳真文件路徑
  CStringX RecvFaxPath; //接收傳真文件路徑
  
//   UC UpFskHeader; //默認上行（終端->中心）FSK包頭標志 0x87（十進制為135） 
//   UC DownFskHeader; //默認下行（中心->終端）FSK包頭標志 0x84（十進制為132） 
  
  void ReadIniFile(); //讀配置文件
  void ChangeCode(char *callerno, char *calledno); //號碼轉換

  void WritePSTNPreCode(int lineno, const char *precode);
};

//某時段通道占用統計
class CChCount
{
public:
  int TrkInCount; //中繼呼入總數
  int TrkInAnsCount; //中繼呼入應答總數
  int TrkInCur; //呼入中繼當前同時占用數
  int TrkInMin; //呼入中繼最小同時占用數
  int TrkInMax; //呼入中繼最大同時占用數
  
  int TrkOutCount; //中繼呼出總數
  int TrkOutAnsCount; //中繼呼出應答總數
  int TrkOutCur; //呼出中繼當前同時占用數
  int TrkOutMin; //呼出中繼最小同時占用數
  int TrkOutMax; //呼出中繼最大同時占用數

  int TrkCallCount; //中繼呼入呼出總次數
  int TrkBusyCount; //中繼當前占用數
  int TrkBusyMin; //中繼最小同時占用數
  int TrkBusyMax; //中繼最大同時占用數
  
  int TrkBusy0; //中繼線無占用累計次數
  int TrkBusy0Per; //中繼線無占用百分比概率
  
  int TrkBusy0_25; //中繼線占用<=%25累計次數
  int TrkBusy0_25Per; //中繼線占用<=%25百分比概率
  
  int TrkBusy25_50; //中繼線占用>%25 and <=50%累計次數
  int TrkBusy25_50Per; //中繼線占用>%25 and <=50%百分比概率
  
  int TrkBusy50_75; //中繼線占用>%50 and <=75%累計次數
  int TrkBusy50_75Per; //中繼線占用>%50 and <=75%百分比概率
  
  int TrkBusy75_90; //中繼線占用>75% and <=90%累計次數
  int TrkBusy75_90Per; //中繼線占用>75% and <=90%百分比概率
  
  int TrkBusy90_100; //中繼線占用>90%累計次數
  int TrkBusy90_100Per; //中繼線占用>90%百分比概率

  int TrkBusyTotal;

  int AcdCallCount; //ACD分配坐席總呼叫次數
  int AcdCallFailCount; //ACD分配失敗總次數
  int AcdCallSuccCount; //ACD分配成功總次數（表示已分配到空閑坐席）
  int AcdCallGiveUpCount; //ACD分配放棄總次數
  int AcdCallAnsCount; //ACD分配坐席應答總次數
  int AcdCallAvgWaitTime; //ACD分配平均排隊時長(從呼入到坐席應答之間的時長平均值)
  long AcdCallTotalWaitTime; //ACD分配總的排隊時長
  int AcdCallAvgAnsTime; //ACD分配坐席平均等待應答時長
  long AcdCallTotalAnsTime; //ACD分配坐席總的等待時長
  int AcdCallAvgGiveUpTime; //ACD分配坐席平均放棄等待時長
  long AcdCallTotalGiveUpTime; //ACD分配坐席總的放棄等待時長

  int AgLoginCur;
  int AgLoginMax; //最大坐席登錄數
  int AgLoginMin; //最小坐席登錄數
  int AgBusyCur; //座席當前同時占用數
  int AgBusyMin; //座席最小同時占用數
  int AgBusyMax; //座席最大同時占用數

  int AgBusy0; //座席無占用累計次數
  int AgBusy0Per; //座席無占用百分比概率
  
  int AgBusy0_25; //座席占用<=%25累計次數
  int AgBusy0_25Per; //座席占用<=%25百分比概率
  
  int AgBusy25_50; //座席占用>%25 and <=50%累計次數
  int AgBusy25_50Per; //座席占用>%25 and <=50%百分比概率
  
  int AgBusy50_75; //座席占用>%50 and <=75%累計次數
  int AgBusy50_75Per; //座席占用>%50 and <=75%百分比概率
  
  int AgBusy75_90; //座席占用>75% and <=90%累計次數
  int AgBusy75_90Per; //座席占用>75% and <=90%百分比概率
  
  int AgBusy90_100; //座席占用>90%累計次數
  int AgBusy90_100Per; //座席占用>90%百分比概率

  int AgBusyTotal;

  char strTrkData[256];
  char strAgData[256];

public:
	CChCount();
	virtual ~CChCount();

  void Init();
  void InitTrkData();
  void InitAgData();
  const CChCount& operator=(const CChCount& chcount);
  void SetTrkData(const CChCount& chcount);
  void SetAgData(const CChCount& chcount);
};
//---------------------------------------------------------------------------
#endif
