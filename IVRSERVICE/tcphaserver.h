// TCPServer.h: interface for the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#ifndef __TCPHASERVER__H__
#define __TCPHASERVER__H__
//---------------------------------------------------------------------------

class CTCPHALink
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnLOGLogin,
                          ONAPPCLOSE OnLOGClose,
                          ONAPPRECEIVEDATA OnLOGReceiveData
                          );
  typedef void (*APPINITS)(unsigned short server_id,
                          unsigned short server_port,
                          ONAPPLOGIN OnIVRLogin,
                          ONAPPCLOSE OnIVRClose,
                          ONAPPRECEIVEDATA OnIVRReceiveData
                          );
public:
  bool isLinked; //是否曾連接過
  TServer  MasterIVRServer;		//主用IVR服務（本方是備機時）
  bool  IsStandbyIVRServerConnected; //與備機連接狀態（本方是主機時）

  short m_nMultiRunMode; //雙機熱備方式
  short m_nHostType; //本機類型：0-主機 1-備機

	void ReleaseDll();
public:
	CTCPHALink();
	virtual ~CTCPHALink();
  unsigned short IVRServerId; //本端ID標號
  int IVRServerPort;
  
  unsigned short GetMasterIVRServerId()
  {return MasterIVRServer.Serverid;}
  void SetMasterIVRLinked() {MasterIVRServer.IsConnected=true;}
  void SetMasterIVRUnlink() {MasterIVRServer.IsConnected=false;}
  bool IsMasterIVRConnected()
  {return MasterIVRServer.IsConnected;}

  void SetStandbyIVRLinked() {IsStandbyIVRServerConnected=true;}
  void SetStandbyIVRUnlink() {IsStandbyIVRServerConnected=false;}
  bool IsStandbyIVRConnected()
  {return IsStandbyIVRServerConnected;}

  char m_szAppPath[MAX_PATH_LEN];
  char m_szDllFile[MAX_PATH_LEN];
  bool LoadDll();
  bool IsOpen; //dll is opend
  bool isIVRInit; //服務器初始化標志
	
  //申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;
  APPINITS        AppInitS;

public:
  int InitIVRServer();
  int SendMessage(US ServerId, US MsgId, int MsgLen, const CH *MsgBuf);
  int SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf);
  int SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len);

  int SendMessage2Master(US MsgId, const CH *MsgBuf);

  bool ReadIni(char *filename);
  bool ReadWebSetupIni(char *filename);

  int ConnectMasterIVRServer();

  int InitTCPLink();

public:
	//組合消息類型及消息編號
  US CombineID(UC IdType, UC IdNo)
  {
    US m_i1, m_i2;
    m_i1 = ( IdType << 8 ) & 0xFF00;
    m_i2 = IdNo & 0x00FF;
    return (m_i1 | m_i2);
  }
	//分解對象標識及編號
	void DisCombineID(UC ID, UC &IdType, US &IdNo)
	{
	  IdType = (UC)(( ID >> 8 ) & 0xFF);
	  IdNo = ID & 0x00FF;
	}
  void DisCombineMsgID(US MsgNo, UC &MsgType, UC &MsgId)
  {
    MsgType = ( MsgNo >> 8 ) & 0xFF;
    MsgId = MsgNo & 0xFF;
  }
};

extern void OnIVRReceiveData_A(unsigned short remoteid,unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len); //need define in main.cpp
extern void OnIVRLogin_A(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose_A(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnIVRReceiveData_B(unsigned short remoteid,unsigned short msgtype,
                             const unsigned char *buf,
                             unsigned long len); //need define in main.cpp
extern void OnIVRLogin_B(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose_B(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

//---------------------------------------------------------------------------
#endif // !defined(AFX_TCPSERVER_H__A15B7B15_5824_444E_9B1E_A087BAA5B4CC__INCLUDED_)
