// VopChn.cpp: implementation of the CVocChn class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VopChn.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVopChn::CVopChn()
{
  InitData();
}

CVopChn::~CVopChn()
{

}

void CVopChn::InitData()
{
  m_nValid = 0;

  m_nIndex = -1;

  m_dwTimer = 0;
  m_nSvState = 0;
  m_nLnState = 0;
  m_nSsState = 0;
  
  m_nVopNo = 0;
  m_nChStyle = 0;
  m_nChIndex = -1;
  m_nVopChnType = -1;

  m_nBoxChn = -1;
  m_nSwtPortID = -1;
  memset(m_szDeviceID, 0, MAX_TELECODE_LEN);
  
  m_nSeizeChn = -1;
  m_tSeizeTime = 0;

  memset(m_szCallerNo, 0, MAX_TELECODE_LEN);
  memset(m_szCalledNo, 0, MAX_TELECODE_LEN);
}

void CVopChn::SetCardData(short nChStyle, short nChIndex, short nVopChnType)
{
  m_nChStyle = nChStyle;
  m_nChIndex = nChIndex;
  m_nVopChnType = nVopChnType;
}

void CVopChn::SetSwtData(short nVopNo, short nVopChnNo, short nSwtPortID)
{
  m_nValid = 1;
  m_nVopNo = nVopNo;
  m_nVopChnNo = nVopChnNo;
  if (nSwtPortID >=0 && nSwtPortID < 2048)
    m_nSwtPortID = nSwtPortID;
}

void CVopChn::SetSwtPortID(short nSwtPortID)
{
  m_nSwtPortID = nSwtPortID;
}
void CVopChn::SetSwtDeviceID(const char *pszDeviceID)
{
  strncpy(m_szDeviceID, pszDeviceID, MAX_TELECODE_LEN-1);
}

void CVopChn::SetVopChnType(short nVopChnType)
{
  m_nVopChnType = nVopChnType;
}

void CVopChn::SetBoxChnNo(short nChn)
{
  m_nBoxChn = nChn;
}

void CVopChn::SetsvState(short nState)
{
  m_nSvState = nState;
}

void CVopChn::SetlnState(short nState)
{
  m_nLnState = nState;
}

void CVopChn::SetssState(short nState)
{
  m_nSsState = nState;
}

short CVopChn::GetsvState()
{
  return m_nSvState;
}

short CVopChn::GetlnState()
{
  return m_nLnState;
}

short CVopChn::GetssState()
{
  return m_nSsState;
}

void CVopChn::SeizeVopChn(short nChn)
{
  m_nSeizeChn = nChn;
  m_tSeizeTime = time(0);
}

void CVopChn::ReleaseVopChn()
{
  m_nSeizeChn = -1;
  m_tSeizeTime = 0;
}

//------語音機函數-----------------------------------------------------------------------
CVop::CVop()
{
  m_nValid = 0;
  m_nVopType = 0;
  m_usClientId = 0;
  m_nLoginState = 0;

  m_nVopChnNum = 0;
  m_pVopChns = NULL;
  memset(m_szGroupExtension, 0, MAX_TELECODE_LEN);
  memset(m_szIVRHostId, 0, 64);

  m_nLastVopChnNo = -1;
}

CVop::CVop(short nVopChnNum)
{
  InitData(nVopChnNum);
}

CVop::~CVop()
{
  if (m_pVopChns)
  {
    delete []m_pVopChns;
    m_pVopChns = NULL;
  }
}

bool CVop::InitData(short nVopChnNum)
{
  if (nVopChnNum <= 0)
  {
    return false;
  }
  m_pVopChns = new CVopChn[nVopChnNum];
  m_nVopChnNum = nVopChnNum;
  m_nLastVopChnNo = -1;
  if (m_pVopChns)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void CVop::SetGroupExtension(const char *extension)
{
  strncpy(m_szGroupExtension, extension, MAX_TELECODE_LEN-1);
}

void CVop::OpenVop()
{
  m_nValid = 1;
}

void CVop::CloseVop()
{
  m_nValid = 0;
}

void CVop::SetClientID(unsigned short usClientId)
{
  m_usClientId = usClientId;
}

void CVop::SetLoginState(short nLoginState)
{
  m_nLoginState = nLoginState;
}

bool CVop::isVopIndexValid(short nVopChnNo)
{
  if (nVopChnNo >= 0  && nVopChnNo < m_nVopChnNum)
  {
    return true;
  } 
  else
  {
    return false;
  }
}

void CVop::SetCardData(short nVopChnNo, short nChStyle, short nChIndex, short nVopChnType)
{
  if (isVopIndexValid(nVopChnNo))
  {
    m_pVopChns[nVopChnNo].SetCardData(nChStyle, nChIndex, nVopChnType);
  }
}

void CVop::SetSwtData(short nVopChnNo, short nVopNo, short nSwtPortID)
{
  if (isVopIndexValid(nVopChnNo))
  {
    m_pVopChns[nVopChnNo].SetSwtData(nVopNo, nVopChnNo, nSwtPortID);
  }
}

void CVop::SetSwtDeviceID(short nVopChnNo, const char *pszDeviceID)
{
  if (isVopIndexValid(nVopChnNo))
  {
    m_pVopChns[nVopChnNo].SetSwtDeviceID(pszDeviceID);
  }
}

void CVop::SetVopChnType(short nVopChnNo, short nVopChnType)
{
  if (isVopIndexValid(nVopChnNo))
  {
    m_pVopChns[nVopChnNo].SetVopChnType(nVopChnType);
  }
}

void CVop::SetBoxChnNo(short nVopChnNo, short nChn)
{
  if (isVopIndexValid(nVopChnNo))
  {
    m_pVopChns[nVopChnNo].SetBoxChnNo(nChn);
  }
}

void CVop::SetsvState(short nVopChnNo, short nState)
{
  if (isVopIndexValid(nVopChnNo))
  {
    m_pVopChns[nVopChnNo].SetsvState(nState);
  }
}

CVopChn *CVop::SeizeVopChn(short nChn)
{
  short nVopChnNo;

  for (int i = 0; i < m_nVopChnNum; i ++)
  {
    nVopChnNo = (m_nLastVopChnNo+1)%m_nVopChnNum;
    m_nLastVopChnNo = nVopChnNo;
    if (m_pVopChns[nVopChnNo].m_nValid == 1
      && m_pVopChns[nVopChnNo].m_nSvState == 0
      && m_pVopChns[nVopChnNo].m_nLnState == 0
      && m_pVopChns[nVopChnNo].m_nSsState == 0
      && m_pVopChns[nVopChnNo].m_nSeizeChn < 0)
    {
      m_pVopChns[nVopChnNo].SeizeVopChn(nChn);
      return &m_pVopChns[nVopChnNo];
    }
  }
  return NULL;
}

CVopChn *CVop::GetVopChnByVopChnNo(short nVopChnNo)
{
  if (isVopIndexValid(nVopChnNo))
  {
    return &m_pVopChns[nVopChnNo];
  }
  return NULL;
}

CVopChn *CVop::GetVopChnBySwtPortID(short nPortID)
{
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nSwtPortID == nPortID)
    {
      return &m_pVopChns[nVopChnNo];
    }
  }
  return NULL;
}
CVopChn *CVop::GetVopChnByChnNo(short nChn)
{
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nBoxChn == nChn)
    {
      return &m_pVopChns[nVopChnNo];
    }
  }
  return NULL;
}
CVopChn *CVop::GetVopChnByDeviceID(const char *pszDeviceID)
{
  if (strlen(pszDeviceID) == 0)
    return NULL;

  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (strcmp(m_pVopChns[nVopChnNo].m_szDeviceID, pszDeviceID) == 0)
    {
      return &m_pVopChns[nVopChnNo];
    }
  }
  return NULL;
}
short CVop::GetChnNoByCustPhone(const char *pszPhone)
{
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nVopChnType == 7 && strcmp(m_pVopChns[nVopChnNo].m_szCallerNo, pszPhone) == 0)
    {
      return m_pVopChns[nVopChnNo].m_nBoxChn;
    }
  }
  return -1;
}

void CVop::ReleaseAllVopChn()
{
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
    m_pVopChns[nVopChnNo].ReleaseVopChn();
}

void CVop::BlockVopChn()
{
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
    m_pVopChns[nVopChnNo].m_nLnState |= 0x01;
}

void CVop::FreeVopChn()
{
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
    m_pVopChns[nVopChnNo].m_nLnState = 0;
}

bool CVop::IsAllVopChnAct()
{
  bool bAct=true;
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nLnState != 0)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
bool CVop::IsAllIVRChnAct()
{
  bool bAct=true;
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nVopChnType <= 1 && m_pVopChns[nVopChnNo].m_nLnState != 0)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
bool CVop::IsAllRECChnAct()
{
  bool bAct=true;
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nVopChnType > 1 && m_pVopChns[nVopChnNo].m_nLnState != 0)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}

bool CVop::IsAllIVRChnBlock()
{
  bool bAct=true;
  for (short nVopChnNo = 0; nVopChnNo < m_nVopChnNum; nVopChnNo ++)
  {
    if (m_pVopChns[nVopChnNo].m_nVopChnType <= 1 && m_pVopChns[nVopChnNo].m_nLnState == 0)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
//---語音機組群類函數--------------------------------------------------------------------------

CVopGroup::CVopGroup()
{
  m_nVopNum = 0;
  m_nVopChnNum = 0;
  m_pVops = NULL;
  m_nLastVopNo = -1;
}

CVopGroup::CVopGroup(short nVopNum)
{
  InitData(nVopNum);
}

CVopGroup::~CVopGroup()
{
  if (m_pVops)
  {
    delete []m_pVops;
    m_pVops = NULL;
  }
}

bool CVopGroup::InitData(short nVopNum)
{
  if (nVopNum <= 0)
  {
    return false;
  }
  m_pVops = new CVop[nVopNum];
  m_nVopNum = nVopNum;
  m_nLastVopNo = -1;
  if (m_pVops)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void CVopGroup::SetDeviceID(short nPortID, short nChn, const char *pszDeviceID)
{
  CVopChn *pVopChn=NULL;
  short nVopNo;

  if (m_nVopNum == 0 || m_pVops == NULL)
  {
    return;
  }
  for (nVopNo = 0; nVopNo < m_nVopNum; nVopNo++)
  {
    pVopChn = m_pVops[nVopNo].GetVopChnBySwtPortID(nPortID);
    if (pVopChn != NULL)
    {
      pVopChn->SetSwtDeviceID(pszDeviceID);
      pVopChn->SetBoxChnNo(nChn);
      return;
    }
  }
  for (nVopNo = 0; nVopNo < m_nVopNum; nVopNo++)
  {
    pVopChn = m_pVops[nVopNo].GetVopChnByDeviceID(pszDeviceID);
    if (pVopChn != NULL)
    {
      pVopChn->SetSwtPortID(nPortID);
      pVopChn->SetBoxChnNo(nChn);
      return;
    }
  }
}

CVopChn *CVopGroup::SeizeVopChn(short nChn)
{
  short nVopNo;

  for (int i = 0; i < m_nVopNum; i ++)
  {
    nVopNo = (m_nLastVopNo+1)%m_nVopNum;
    m_nLastVopNo = nVopNo;
    return m_pVops[nVopNo].SeizeVopChn(nChn);
  }
  return NULL;
}

CVopChn *CVopGroup::GetVopChnByVopChnNo(short nVopNo, short nVopChnNo)
{
  if (nVopNo >= m_nVopNum)
  {
    return NULL;
  }
  return m_pVops[nVopNo].GetVopChnByVopChnNo(nVopChnNo);
}

CVopChn *CVopGroup::GetVopChnBySwtPortID(short nPortID)
{
  CVopChn *pVopChn=NULL;
  
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    pVopChn = m_pVops[nVopNo].GetVopChnBySwtPortID(nPortID);
    if (pVopChn)
      return pVopChn;
  }
  return NULL;
}

CVopChn *CVopGroup::GetVopChnByChnNo(short nChn)
{
  CVopChn *pVopChn=NULL;

  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    pVopChn = m_pVops[nVopNo].GetVopChnByChnNo(nChn);
    if (pVopChn)
      return pVopChn;
  }
  return NULL;
}
CVopChn *CVopGroup::GetVopChnByDeviceID(const char *pszDeviceID)
{
  CVopChn *pVopChn=NULL;
  
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    pVopChn = m_pVops[nVopNo].GetVopChnByDeviceID(pszDeviceID);
    if (pVopChn)
      return pVopChn;
  }
  return NULL;
}
char *CVopGroup::GetGroupExtension(short nVopNo)
{
  if (nVopNo >= m_nVopNum)
  {
    return NULL;
  }
  return m_pVops[nVopNo].m_szGroupExtension;
}
short CVopGroup::GetChnNoByCustPhone(const char *pszPhone)
{
  short nBoxChn;
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    nBoxChn = m_pVops[nVopNo].GetChnNoByCustPhone(pszPhone);
    if (nBoxChn >= 0)
      return nBoxChn;
  }
  return -1;
}
void CVopGroup::ReleaseAllVop()
{
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
    m_pVops[nVopNo].ReleaseAllVopChn();
}

void CVopGroup::BlockVopChn(short nVopNo)
{
  if (nVopNo >= m_nVopNum)
  {
    return;
  }
  m_pVops[nVopNo].BlockVopChn();
}

void CVopGroup::FreeVopChn(short nVopNo)
{
  if (nVopNo >= m_nVopNum)
  {
    return;
  }
  m_pVops[nVopNo].FreeVopChn();
}

bool CVopGroup::IsAllVopChnAct()
{
  bool bAct=true;
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    if (m_pVops[nVopNo].IsAllVopChnAct() == false)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
bool CVopGroup::IsAllIVRChnAct()
{
  bool bAct=true;
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    if (m_pVops[nVopNo].IsAllIVRChnAct() == false)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
bool CVopGroup::IsAllRECChnAct()
{
  bool bAct=true;
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    if (m_pVops[nVopNo].IsAllRECChnAct() == false)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}

bool CVopGroup::IsAllIVRChnBlock()
{
  bool bAct=true;
  for (short nVopNo = 0; nVopNo < m_nVopNum; nVopNo ++)
  {
    if (m_pVops[nVopNo].IsAllIVRChnBlock() == false)
    {
      bAct = false;
      break;
    }
  }
  return bAct;
}
//-----------------------------------------------------------------------------
