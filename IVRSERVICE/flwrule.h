//---------------------------------------------------------------------------
#ifndef FlwRuleH
#define FlwRuleH
//---------------------------------------------------------------------------
//消息方向說明：

//發送消息結構：(客戶端 >> 服務器) VXML_SEND_MSG_ATTR_RULE_STRUCT  [upload]

//接收消息結構：(客戶端 << 服務器) VXML_RECV_MSG_ATTR_RULE_STRUCT  [download]

//注：但LOG服務器收發均采用發送消息結構
//---------------------------------------------------------------------------
//流程指令規則類
class CFlwRule
{

public:
	CFlwRule();
	virtual ~CFlwRule();

  //整數范圍規則
  const VXML_INTEGER_RANGE_RULE_STRUCT      *IntRange;
  //單字符范圍規則
  const VXML_CHAR_RANGE_RULE_STRUCT         *CharRange;
  //字符串中允許出現的字符規則
  const VXML_STRING_RANGE_RULE_STRUCT       *StrRange;
  //整數枚舉字符串規則
  const VXML_MACRO_INT_RANGE_RULE_STRUCT    *MacroIntRange;
  //枚舉字符串規則
  const VXML_MACRO_STR_RANGE_RULE_STRUCT    *MacroStrRange;

  const VXML_RECV_MSG_CMD_RULE_STRUCT       *DBXMLMsgRule;
  const VXML_RECV_MSG_CMD_RULE_STRUCT       *CFGSRVMsgRule;
  
  //parser發送的消息規則
  const VXML_SEND_MSG_CMD_RULE_STRUCT				*XMLIVRMsgRule;
  
  //agent發送來的消息規則
  const VXML_SEND_MSG_CMD_RULE_STRUCT				*AGIVRMsgRule;

  //ivr返回坐席消息處理結果
  const VXML_RECV_MSG_CMD_RULE_STRUCT       *IVRAGMsgRule;
  
  //log發送來的消息規則
  const VXML_SEND_MSG_CMD_RULE_STRUCT				*LOGIVRMsgRule;
  
  //ivr返回log消息處理結果
  const VXML_RECV_MSG_CMD_RULE_STRUCT       *IVRLOGMsgRule;

  //ctilink發送來的消息規則
  const VXML_RECV_MSG_CMD_RULE_STRUCT				*SWTIVRMsgRule;
  
  //ivr返回ctilink消息處理結果
  const VXML_SEND_MSG_CMD_RULE_STRUCT       *IVRSWTMsgRule;
  
  //IVR發送消息規則
  const VXML_SEND_MSG_CMD_RULE_STRUCT				*IVRVOPMsgRule;
  
  //VOP返回消息處理結果
  const VXML_RECV_MSG_CMD_RULE_STRUCT       *VOPIVRMsgRule;

  //備機到主機消息規則
  const VXML_SEND_MSG_CMD_RULE_STRUCT				*S_TO_MMsgRule;
  
  //主機到備機消息規則
  const VXML_RECV_MSG_CMD_RULE_STRUCT       *M_TO_SMsgRule;

  //消息屬性參數值檢查(服務器 >> 客戶端)
  int Parse_Msg_Attr_Value(const VXML_RECV_MSG_ATTR_RULE_STRUCT &AttrRule,CStringX &ValueStr);
  //消息屬性參數值檢查(客戶端 >> 服務器)
  int Parse_Msg_Attr_Value(const VXML_SEND_MSG_ATTR_RULE_STRUCT &AttrRule,CStringX &ValueStr);
};
//---------------------------------------------------------------------------
#endif
