// VocChn.h: interface for the CVocChn class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VOPCHN_H__48504E84_65C6_459C_9ED5_A1625385CD8D__INCLUDED_)
#define AFX_VOPCHN_H__48504E84_65C6_459C_9ED5_A1625385CD8D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define   MAX_VOICE_NUM     8 //最大語音節點數
#define   VOP_LOCA_VOPNO    0 //本地語音節點編號

//語音資源通道類
class CVopChn
{
public:
  short m_nValid; //0-未配置 1-啟動 2-關閉

  short m_nIndex; //所有的資源通道統一索引號
  DWORD m_dwTimer; //計時器
  short m_nSvState; //資源通道服務狀態 0-空閑 1-正在放音 2-正在錄音 3-正在發送傳真 4-正在接收傳真
  short m_nLnState; //資源通道線路狀態 bit0: 0-正常 1-阻塞 bit1: 0-打開 1-關閉
  short m_nSsState; //呼叫信令狀態 0-空閑 1-占用

  short m_nVopNo; //對應的語音機編號 0-表示為IVR本地語音節點
  short m_nVopChnNo; //對應的語音資源號（每個語音機的資源通道編號）
  short m_nVopChnType; //對應的語音資源類型：-1表示未知 0-IVR/FAX語音資源(不處理呼叫信息) 1-IVR語音資源 2-FAX語音資源 3-模擬話機坐席監錄 4-數字話機坐席監錄 5-VOIP坐席監錄 6-模擬中繼監錄 7-數字中繼監錄 8-IVR語音中繼(可處理呼叫信息) 9-VP資源 10-電話錄音
  short m_nChStyle; //語音卡的按信令劃分通道類型
  short m_nChIndex; //語音卡的邏輯通道號
  
  short m_nBoxChn; //對應的BoxChn通道號
  
  short m_nSwtPortID; //對應連接語音資源通道的交換機的邏輯端口號
  char  m_szDeviceID[MAX_TELECODE_LEN]; //設備編號

  short m_nSeizeChn; //占用該資源的的通道號
  time_t m_tSeizeTime;

  char m_szCallerNo[MAX_TELECODE_LEN]; //數字中繼監錄通道的主叫號碼
  char m_szCalledNo[MAX_TELECODE_LEN]; //數字中繼監錄通道的被叫號碼

public:
	CVopChn();
	virtual ~CVopChn();

  void InitData();

  void SetCardData(short nChStyle, short nChIndex, short nVopChnType=0);
  void SetSwtData(short nVopNo, short nVopChnNo, short nSwtPortID);
  void SetSwtPortID(short nSwtPortID);
  void SetSwtDeviceID(const char *pszDeviceID);
  void SetVopChnType(short nVopChnType);
  void SetBoxChnNo(short nChn);
  
  void SetsvState(short nState);
  void SetlnState(short nState);
  void SetssState(short nState);
  
  short GetsvState();
  short GetlnState();
  short GetssState();
  
  void SeizeVopChn(short nChn);
  void ReleaseVopChn();
};

//語音機資源通道列表
class CVop
{
public:
  short m_nValid; //該語音機是否啟用 1-是 0否
  short m_nVopType; //語音機類型：0-平臺帶的，1-第3方錄音
  unsigned short m_usClientId; //該語音機對應的客戶端ID 0x03nn
  short m_nLoginState; //該語音機登錄標志 0-未登錄 1-登錄成功 2-登錄失敗

  char  m_szGroupExtension[MAX_TELECODE_LEN]; //語音機對應的交換機分機連選號碼
  char  m_szIVRHostId[64]; //語音機對應的交換機內部設置的IVRhost標志
  
  short m_nVopChnNum; //該語音總的語音通道數量
  CVopChn *m_pVopChns;
  
  short m_nLastVopChnNo;

public:
  CVop();
  CVop(short nVopChnNum);
  virtual ~CVop();

  bool InitData(short nVopChnNum);
  void SetGroupExtension(const char *extension);
  
  void OpenVop();
  void CloseVop();
  
  void SetClientID(unsigned short usClientId);
  void SetLoginState(short nLoginState);
  bool isVopIndexValid(short nVopChnNo);
  
  void SetCardData(short nVopChnNo, short nChStyle, short nChIndex, short nVopChnType=0);
  void SetSwtData(short nVopChnNo, short nVopNo, short nSwtPortID);
  void SetSwtDeviceID(short nVopChnNo, const char *pszDeviceID);
  void SetVopChnType(short nVopChnNo, short nVopChnType);
  void SetBoxChnNo(short nVopChnNo, short nChn);

  void SetsvState(short nVopChnNo, short nState);

  CVopChn *SeizeVopChn(short nChn);

  CVopChn *GetVopChnByVopChnNo(short nVopChnNo);
  CVopChn *GetVopChnBySwtPortID(short nPortID);
  CVopChn *GetVopChnByChnNo(short nChn);
  CVopChn *GetVopChnByDeviceID(const char *pszDeviceID);

  short GetChnNoByCustPhone(const char *pszPhone);

  void ReleaseAllVopChn();
  void BlockVopChn();
  void FreeVopChn();

  bool IsAllVopChnAct();
  bool IsAllIVRChnAct();
  bool IsAllRECChnAct();

  bool IsAllIVRChnBlock();
};

//語音機列表
class CVopGroup
{
public:
  short m_nVopNum;
  short m_nVopChnNum;
  CVop *m_pVops;

  short m_nLastVopNo;

public:
  CVopGroup();
  CVopGroup(short nVopNum);
  virtual ~CVopGroup();

  bool InitData(short nVopNum);

  void SetDeviceID(short nPortID, short nChn, const char *pszDeviceID);

  CVopChn *SeizeVopChn(short nChn);
  
  CVopChn *GetVopChnByVopChnNo(short nVopNo, short nVopChnNo);
  CVopChn *GetVopChnBySwtPortID(short nPortID);
  CVopChn *GetVopChnByChnNo(short nChn);
  CVopChn *GetVopChnByDeviceID(const char *pszDeviceID);

  short GetChnNoByCustPhone(const char *pszPhone);

  char *GetGroupExtension(short nVopNo);
  void BlockVopChn(short nVopNo);
  void FreeVopChn(short nVopNo);

  bool IsAllVopChnAct();
  bool IsAllIVRChnAct();
  bool IsAllRECChnAct();

  bool IsAllIVRChnBlock();

  void ReleaseAllVop();
};

//交換機端口
class CSwitchPort
{
public:
  short m_nValid; //該端口是否有效
  short m_nPortID; //交換機邏輯端口號
  char m_szDeviceID[32]; //設備號碼:m_nPortType=2時表示為分機號 m_nPortType=1時表示為中繼接入號
  char m_szFlwAccessCode[32]; //流程接入號
  
  short m_nChStyle;     //通道信令類型
  short m_nChStyleIndex;//按信令類型排列的通道序號
  short m_nChType;      //硬件邏輯通道類型
  short m_nChIndex;     //硬件邏輯通道號
  short m_nLgChType;    //業務邏輯通道類型: 1-中繼 2-坐席 3-IVR 4-錄音 5-傳真 6-會議 7-VOIP
  short m_nLgChIndex;   //業務邏輯通道號
  short m_dwPhysPort;   //實際的物理端口號
  
public:
  CSwitchPort()
  {
    m_nValid = 0;
    m_nPortID = -1;
    memset(m_szDeviceID, 0, 32);
    memset(m_szFlwAccessCode, 0, 32);
    
    m_nChStyle = 0;
    m_nChStyleIndex = 0;
    m_nChType = 0;
    m_nChIndex = 0;
    m_nLgChType = 0;
    m_nLgChIndex = 0;
    m_dwPhysPort = 0;
  }
  virtual ~CSwitchPort(){}
};

class CSwitchPortList
{
public:
  short m_nSwitchID;

  short m_nMaxPortNum;
  CSwitchPort *m_pSwitchPort;

public:
  CSwitchPortList()
  {
    m_nSwitchID = 10;
    m_nMaxPortNum = 0;
    m_pSwitchPort = NULL;
  }
  virtual ~CSwitchPortList()
  {
    if (m_pSwitchPort)
    {
      delete []m_pSwitchPort;
      m_pSwitchPort = NULL;
    }
  }
  bool InitData(short nPortNum)
  {
    m_pSwitchPort = new CSwitchPort[nPortNum];
    m_nMaxPortNum = nPortNum;
    if (m_pSwitchPort)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  bool isPortIDValid(short nPortID)
  {
    if (nPortID >= 0  && nPortID < m_nMaxPortNum)
    {
      return true;
    } 
    else
    {
      return false;
    }
  }
  short GetPortIDByDeviceID(const char *pszDeviceID)
  {
    if (strlen(pszDeviceID) == 0)
    {
      return -1;
    }
    for (int i=0; i<m_nMaxPortNum; i++)
    {
      if (strcmp(pszDeviceID, m_pSwitchPort[i].m_szDeviceID) == 0)
      {
        return m_pSwitchPort[i].m_nPortID;
      }
    }
    return -1;
  }
  CSwitchPort *GetSwitchPortByDeviceID(const char *pszDeviceID)
  {
    if (strlen(pszDeviceID) == 0)
    {
      return NULL;
    }
    for (int i=0; i<m_nMaxPortNum; i++)
    {
      if (strcmp(pszDeviceID, m_pSwitchPort[i].m_szDeviceID) == 0)
      {
        return &m_pSwitchPort[i];
      }
    }
    return NULL;
  }
  char *GetDeviceIDByPortID(short nPortID)
  {
    if (isPortIDValid(nPortID) == true)
    {
      return m_pSwitchPort[nPortID].m_szDeviceID;
    }
    else
    {
      return "";
    }
  }
  short GetPortIDByPhysPort(short dwPhysPort)
  {
    if (dwPhysPort == 0xFFFFFFFF)
    {
      return -1;
    }
    for (int i=0; i<m_nMaxPortNum; i++)
    {
      if (m_pSwitchPort[i].m_dwPhysPort == dwPhysPort)
      {
        return i;
      }
    }
    return -1;
  }
  void SetPortData(short nPortID, const char *pszDeviceID, short nChStyle, short nChStyleIndex, short nChType, short nChIndex, short nLgChType, short nLgChIndex, short dwPhysPort, const char *pszFlwAccessCode)
  {
    if (isPortIDValid(nPortID) == true)
    {
      m_pSwitchPort[nPortID].m_nValid = 1;
      m_pSwitchPort[nPortID].m_nPortID = nPortID;
      
      m_pSwitchPort[nPortID].m_nChStyle = nChStyle;
      m_pSwitchPort[nPortID].m_nChStyleIndex = nChStyleIndex;
      m_pSwitchPort[nPortID].m_nChType = nChType;
      m_pSwitchPort[nPortID].m_nChIndex = nChIndex;
      m_pSwitchPort[nPortID].m_nLgChType = nLgChType;
      m_pSwitchPort[nPortID].m_nLgChIndex = nLgChIndex;
      m_pSwitchPort[nPortID].m_dwPhysPort = dwPhysPort;

      strncpy(m_pSwitchPort[nPortID].m_szDeviceID, pszDeviceID, 31);
      strncpy(m_pSwitchPort[nPortID].m_szFlwAccessCode, pszFlwAccessCode, 31);
    }
  }
  bool isThePortExist(short nPortID, const char *pszDeviceID)
  {
    if (isPortIDValid(nPortID) == true)
    {
      if (m_pSwitchPort[nPortID].m_nLgChType == 1)
      {
        return true;
      }
      if (strcmp(pszDeviceID, m_pSwitchPort[nPortID].m_szDeviceID) == 0)
      {
        return true;
      }
    }
    return false;
  }
  void ReadIniSetParam(const char *pszIniFileName)
  {
    //配置文件格式：[PORT]
    //              Port[n]=端口類型,硬件類型,設備號碼
    int nPortID, nPortNum, nLgChType, nLgChIndex, nChStyle, nChStyleIndex;
    char szTemp[16], szItemList[128];
    CStringX strItemList[5];
    int TRUChnNum=0, USRChnNum=0, IVRChnNum=0, RECChnNum=0, FAXChnNum=0, H323ChnNum=0, SIPChnNum=0, H323TRKChnNum=0, SIPTRKChnNum=0;
    int SEATChnNum=0, EXTChnNum=0, ISDNChnNum=0, NO1ChnNum=0, TUPChnNum=0, ISUPChnNum=0, PRIChnNum=0, EMChnNum=0;
    short dwPhysPort;

    nPortNum = GetPrivateProfileInt("PORT","PortNum", 0, pszIniFileName);
    
    for (nPortID=0; nPortID<nPortNum; nPortID++)
    {
      sprintf(szTemp, "PORT[%d]", nPortID);
      GetPrivateProfileString("PORT", szTemp, "", szItemList, 128, pszIniFileName);
      if (strlen(szItemList) > 0)
      {
        if (SplitString(szItemList, ';', 5, strItemList) >= 4)
        {
          nLgChType = atoi(strItemList[1].C_Str());
          nChStyle = atoi(strItemList[2].C_Str());
          dwPhysPort = atoi(strItemList[3].C_Str());
          nLgChIndex = -1;
          nChStyleIndex = -1;
          switch (nLgChType) //1-中繼 2-坐席 3-IVR 4-錄音 5-傳真 6-會議 7-VOIP
          {
          case 1: //中繼
            nLgChIndex = TRUChnNum;
            TRUChnNum++;
            break;
          case 2: //坐席
            nLgChIndex = USRChnNum;
            USRChnNum++;
            break;
          case 3: //IVR
            nLgChIndex = IVRChnNum;
            IVRChnNum++;
            break;
          case 4: //錄音
            nLgChIndex = RECChnNum;
            RECChnNum++;
            break;
          case 5: //傳真
            nLgChIndex = FAXChnNum;
            FAXChnNum++;
            break;
          }
          
          switch (nChStyle) //1-模擬坐席通道 2-模擬外線通道 3-2B+D數字話機 8-數字通道（中國No.1信令） 9-TUP中繼 10-ISUP中繼 11-PRI用戶側 12-PRI網絡側 13-硬傳真資源 14-軟傳真資源 15-VOIP-H323 16-VOIP(sip) 17-4線E&M 18-SIP-TRUNKING 19-H323-TRUNKING
          {
          case 1:
            nChStyleIndex = SEATChnNum;
            SEATChnNum++;
            break;
          case 2:
            nChStyleIndex = EXTChnNum;
            EXTChnNum++;
            break;
          case 3:
            nChStyleIndex = ISDNChnNum;
            ISDNChnNum++;
            break;
          case 8:
            nChStyleIndex = NO1ChnNum;
            NO1ChnNum++;
            break;
          case 9:
            nChStyleIndex = TUPChnNum;
            TUPChnNum++;
            break;
          case 10:
            nChStyleIndex = ISUPChnNum;
            ISUPChnNum++;
            break;
          case 11:
            nChStyleIndex = PRIChnNum;
            PRIChnNum++;
            break;
          case 12:
            nChStyleIndex = PRIChnNum;
            PRIChnNum++;
            break;
          case 15:
            nChStyleIndex = H323ChnNum;
            H323ChnNum++;
            break;
          case 16:
            nChStyleIndex = SIPChnNum;
            SIPChnNum++;
            break;
          case 17:
            nChStyleIndex = EMChnNum;
            EMChnNum++;
            break;
          case 18:
            nChStyleIndex = SIPTRKChnNum;
            SIPTRKChnNum++;
            break;
          case 19:
            nChStyleIndex = H323TRKChnNum;
            H323TRKChnNum++;
            break;
          }
          if (nLgChIndex < 0 || nChStyleIndex < 0)
          {
            continue;
          }
          SetPortData(nPortID, strItemList[0].C_Str(), nChStyle, nChStyleIndex, PORT_AVAYA_IPO_TYPE, nPortID, nLgChType, nLgChIndex, dwPhysPort, strItemList[4].C_Str());
          //WriteLog("SetPortData nPortID=%d DeviceID=%s nChStyle=%d nChStyleIndex=%d nLgChType=%d nLgChIndex=%d dwPhysPort=%d",
          // nPortID, strItemList[0].c_str(), nChStyle, nChStyleIndex, nLgChType, nLgChIndex, dwPhysPort);
        }
      }
    }
  }
};

#endif // !defined(AFX_VOPCHN_H__48504E84_65C6_459C_9ED5_A1625385CD8D__INCLUDED_)
