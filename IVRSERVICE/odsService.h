//////////////////////////////////////////////////////////////////////////
//
// odsService.h 
//
//		有關 NT 服務控制的通用函數定義
//
//
//////////////////////////////////////////////////////////////////////////

#if !defined(_ODS_20_SERVICE_H_)
#define _ODS_20_SERVICE_H_

#include "odsError.h"

//-----------------------------------------------------------------------
// odsServiceMessageBox				- 服務中彈出的對話框
//-----------------------------------------------------------------------
void odsServiceMessageBox( LPCTSTR lpszText );

//----------------------------------------------------------------
// odsGetServiceStatus			- 取得指定機器上的服務程序的狀態
//
// 參數:
//		lpszComputerName		- 服務程序所在的計算機名稱，如果為 NULL
//								- 則指本地機器
//		lpszServiceName			- 服務程序名稱
//		lpdwServiceStatus		- 返回服務程序的狀態
//		pstError				- 錯誤信息結構指針，可以為 NULL//
// 返回值:
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsGetServiceStatus( LPCTSTR strComputerName, LPCTSTR strServiceName, 
						  LPDWORD lpdwServiceStatus, STODSERROR* pstError );

//-----------------------------------------------------------------------
// odsReportStatusToSCMgr				- 將自己的狀態通知服務控制臺
//
// 參數：
//		hServiceStatus					- 服務狀態句柄
//		dwCurrentState					- 要設置的狀態
//		DWORD dwErrorCode				- 錯誤碼，0 表示無錯誤
//		dwWaitHint						- 控制器等待間隔
//		pstError						- 接收錯誤信息指針（可以為 NULL ）
// 返回：
//		bSuccess						- 成功與否
//-----------------------------------------------------------------------
BOOL odsReportStatusToSCMgr(SERVICE_STATUS_HANDLE hServiceStatus,
							DWORD dwCurrentState,
							DWORD dwErrorCode,
							DWORD dwWaitHint,
							STODSERROR* pstError );

//-----------------------------------------------------------------------
// odsInstallService				- 安裝服務
// 
// 參數：
//		strComputerName				- 計算機名（NULL表示本地計算機）
//		strServiceName				- 服務名稱
//		strServicePath				- 服務路徑
//		strDisplayName				- 服務的顯示名稱
//		dwStartType					- 服務的啟動方式
//									- SERVICE_AUTO_START 或 SERVICE_DEMAND_START
//		strDependencies				- 服務的依賴關系（可以為 NULL ）
//		pstError					- 接收錯誤信息指針（可以為 NULL ）
// 返回：
//		bSuccess					- 成功與否
//-----------------------------------------------------------------------
BOOL odsInstallService( LPCTSTR strComputerName, 
					   LPCTSTR strServiceName, 
					   LPCTSTR strServicePath, 
					   LPCTSTR strDisplayName, 
					   DWORD dwStartType,
					   LPCTSTR strDependencies,
					   STODSERROR* pstError );

//----------------------------------------------------------------
// odsRemoveService					- 刪除某計算機上的服務
//
// 參數：
//		strComputerName				- 計算機名，為 NULL 時表示本地計算機
//		strServiceName				- 服務名
//		dwWaitTime					- 等待時間
//		pstError					- 錯誤信息結構指針，可以為 NULL
// 返回：
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsRemoveService( LPCTSTR strComputerName, LPCTSTR strServiceName, DWORD dwWaitTime, STODSERROR* pstError );

//----------------------------------------------------------------
// odsStartService					- 啟動服務
//
// 參數:
//		lpszComputerName			- 服務程序所在的計算機名稱，如果為 NULL
//									- 則指本地計算機
//		lpszServiceName				- 要啟動服務的名稱
//		dwWaitTime					- 等待服務啟動的時間，以(毫秒)為單位
//		pstError					- 錯誤信息結構指針，可以為 NULL
//
// 返回:
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsStartService(LPCTSTR strComputerName, LPCTSTR strServiceName, 
				   DWORD dwWaitTime, STODSERROR* pstError);

//----------------------------------------------------------------
// odsStopService					- 停止服務
//
// 參數:
//		lpszComputerName			- 服務程序所在的計算機名稱，如果為 NULL
//									- 則指本地計算機
//		lpszServiceName				- 要啟動服務的名稱
//		dwWaitTime					- 等待服務啟動的時間，以(毫秒)為單位
//		pstError					- 錯誤信息結構指針，可以為 NULL
//
// 返回:
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsStopService( LPCTSTR strComputerName, LPCTSTR strServiceName, 
					DWORD dwWaitTime, STODSERROR* pstError );

#endif // _ODS_20_SERVICE_H_