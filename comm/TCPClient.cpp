// TCPClient.cpp: implementation of the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#pragma hdrstop
#include "TcpClient.h"
//---------------------------------------------------------------------------
#include <Messages.hpp>
#include <system.hpp>
#include <sysutils.hpp>
#include <inifiles.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTCPClient::CTCPClient()
{
	hdll = NULL;
  strcpy(DllFile, "tmTcpClient.dll");
  ClientId = 0x0a01;
  LoadDll();
}

CTCPClient::~CTCPClient()
{
  ReleaseDll();
}

bool CTCPClient::LoadDll()
{
	hdll = LoadLibrary(DllFile);
	if ( hdll != NULL )
	{
    AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(!AppInitS || !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MessageBoxA(NULL, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MessageBoxA(NULL, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		return false;
	}
}

void CTCPClient::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}
//組合對象編碼
unsigned short CTCPClient::CombineObjectID( unsigned char ObjectType, unsigned char ObjectNo )
{
  unsigned short o_i1, o_i2;
  o_i1 = ( ObjectType << 8 ) & 0xFF00;
  o_i2 = ObjectNo & 0x00FF;
  return (o_i1 | o_i2);
}
//組合消息類型及消息編號
unsigned short CTCPClient::CombineMessageID(unsigned char MsgType, unsigned char MsgNo)
{
  unsigned short m_i1, m_i2;
  m_i1 = ( MsgType << 8 ) & 0xFF00;
  m_i2 = MsgNo & 0x00FF;
  return (m_i1 | m_i2);
}
//分解對象標識及編號
void CTCPClient::GetClientID(unsigned short Msg_wParamHi, unsigned char &ClientType, unsigned char &ClientNo)
{
  unsigned char C_T, C_N;
  C_T = ( Msg_wParamHi >> 8 ) & 0x00FF;
  C_N = Msg_wParamHi & 0x00FF;
  ClientType = C_T;
  ClientNo = C_N;
}
//分解消息類型及消息編號
void CTCPClient::GetMsgID(unsigned short Msg_wParamLo, unsigned char &MsgType, unsigned char &MsgNo)
{
  unsigned char M_T, M_N;
  M_T = ( Msg_wParamLo >> 8 ) & 0x00FF;
  M_N = Msg_wParamLo & 0x00FF;
  MsgType = M_T;
  MsgNo = M_N;
}
//發送消息
int CTCPClient::SendMessage(unsigned short ServerId, unsigned short MsgId, int MsgLen, const unsigned char *MsgBuf)
{
	int SendedLen;

  SendedLen = AppSendData(ServerId, MsgId, (unsigned char *)MsgBuf, MsgLen);
	return SendedLen;
}
int CTCPClient::IVRSendMessage(unsigned short MsgId, const unsigned char *MsgBuf)
{
  return SendMessage(IVRServer.Serverid, (MSGTYPE_IVRLOG<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPClient::FLWSendMessage(unsigned short MsgId, const unsigned char *MsgBuf)
{
  return SendMessage(FLWServer.Serverid, (MSGTYPE_IVRLOG<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPClient::DBSendMessage(unsigned short MsgId, const unsigned char *MsgBuf)
{
  return SendMessage(DBServer.Serverid, (MSGTYPE_IVRLOG<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPClient::CTRLSendMessage(unsigned short MsgId, const unsigned char *MsgBuf)
{
  return SendMessage(CTRLServer.Serverid, (MSGTYPE_IVRLOG<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}

bool CTCPClient::ReadIni(const char *filename)
{
  char pzSection[128], serverip[32];
  ClientId = CombineObjectID(NODE_LOG, GetPrivateProfileInt("CONFIG", "ClientId", 1, filename));

  if (GetPrivateProfileInt("CONFIG", "RunOnServicePCId", 1, filename) == 1)
    strcpy(serverip, "127.0.0.1");
  else
    GetPrivateProfileString("CONFIG", "ServerIP", "127.0.0.1", serverip, 30, filename);

  IVRServer.IsConnected=false;
  strcpy(IVRServer.ServerIP, serverip);
  IVRServer.ServerPort = GetPrivateProfileInt("CONFIG", "IVRServerPort", 5227, filename);
  IVRServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("CONFIG", "IVRServerId", 1, filename));
  
  FLWServer.IsConnected=false;    
  strcpy(FLWServer.ServerIP, serverip);
  FLWServer.ServerPort = GetPrivateProfileInt("CONFIG", "FLWServerPort", 5229, filename);
  FLWServer.Serverid = CombineObjectID(NODE_XML,GetPrivateProfileInt("CONFIG", "FLWServerId", 0, filename));
  
  DBServer.IsConnected=false;
  strcpy(DBServer.ServerIP, serverip);
  DBServer.ServerPort = GetPrivateProfileInt("CONFIG", "DBServerPort", 5228, filename);
  DBServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("CONFIG", "DBServerId", 0, filename));
  
  CTRLServer.IsConnected=false;
  strcpy(CTRLServer.ServerIP, serverip);
  CTRLServer.ServerPort = GetPrivateProfileInt("CONFIG", "CTRLServerPort", 5226, filename);
  CTRLServer.Serverid = CombineObjectID(NODE_CTRL,GetPrivateProfileInt("CONFIG", "CTRLServerId", 0, filename));

  return true;
}    
int CTCPClient::ConnectIVRServer()
{
  if (IsOpen == true)
  {
    AppInit(IVRServer.Serverid,
            ClientId,
            IVRServer.ServerIP,
            IVRServer.ServerPort,
            OnIVRLogin,
            OnIVRClose,
            OnIVRReceiveData);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPClient::ConnectFLWServer()
{
  if (IsOpen == true)
  {
    if ((FLWServer.Serverid&0xFF) == 0) return 0;
    AppInit(FLWServer.Serverid,
            ClientId,
            FLWServer.ServerIP,
            FLWServer.ServerPort,
            OnFLWLogin,
            OnFLWClose,
            OnFLWReceiveData);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPClient::ConnectDBServer()
{
  if (IsOpen == true)
  {
    if ((DBServer.Serverid&0xFF) == 0) return 0;
    AppInit(DBServer.Serverid,
            ClientId,
            DBServer.ServerIP,
            DBServer.ServerPort,
            OnDBLogin,
            OnDBClose,
            OnDBReceiveData);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPClient::ConnectCTRLServer()
{
  if (IsOpen == true)
  {
    if ((CTRLServer.Serverid&0xFF) == 0) return 0;
    AppInit(CTRLServer.Serverid,
            ClientId,
            CTRLServer.ServerIP,
            CTRLServer.ServerPort,
            OnCTRLLogin,
            OnCTRLClose,
            OnCTRLReceiveData);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPClient::ConnectServer()
{
  //int result;

  ConnectIVRServer();
  ConnectFLWServer();
  ConnectDBServer();
  return 0;
}
