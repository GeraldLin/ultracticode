// TCPServer.cpp: implementation of the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

#pragma hdrstop
#include "TcpServer.h"
//---------------------------------------------------------------------------
#include <Messages.hpp>
#include <system.hpp>
#include <sysutils.hpp>
#include <inifiles.hpp>
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTCPServer::CTCPServer()
{
  AnsiString temp;

	IsOpen=false;
  IsRun = false;

  strcpy(DllFile, "tmtcpserver.dll");
  ServerId = 0x1003;
  ServerPort = 5003;
}

CTCPServer::~CTCPServer()
{
    if (IsOpen == true)
	    ReleaseDll();
}

bool CTCPServer::LoadDll()
{
    if (IsOpen == true)
        return true;
	hdll = LoadLibrary(DllFile);
	if ( hdll != NULL )
	{
		AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
        AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
        AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
        AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

        if(  !AppInitS || !AppCloseAll || !AppMainLoop || !AppSendData)
        {
 //           MessageBoxA(NULL, "動態連接庫TcpLink.DLL加載失敗！請檢查軟件設置！","警告",MB_OK|MB_ICONWARNING);
		    return false;
        }
		IsOpen=true;
		return true;
	}
	else
	{
//		MessageBoxA(NULL, "動態連接庫加載失敗！請檢查軟件設置！","警告",MB_OK|MB_ICONWARNING);
		return false;
	}
}

void CTCPServer::ReleaseDll()
{
    if ( hdll != NULL )
    {
		AppCloseAll();
		FreeLibrary( hdll );
    }
}

//組合消息類型及消息編號
US CTCPServer::CombineID(UC IdType, UC IdNo)
{
    US m_i1, m_i2;
    m_i1 = ( IdType << 8 ) & 0xFF00;
    m_i2 = IdNo & 0x00FF;
    return (m_i1 | m_i2);
}
//分解對象標識及編號
void CTCPServer::DisCombineID(US ID, UC &IdType, UC &IdNo)
{
    IdType = ( ID >> 8 ) & 0x00FF;
    IdNo = ID & 0x00FF;
}
/*
//分解消息類型及消息編號
void CTCPServer::GetMsgID(US Msg_wParamLo, UC &MsgType, UC &MsgNo)
{
    UC M_T, M_N;
    M_T = ( Msg_wParamLo >> 8 ) & 0x00FF;
    M_N = Msg_wParamLo & 0x00FF;
    MsgType = M_T;
    MsgNo = M_N;
}*/
//發送消息
int CTCPServer::SendMessage(US ClientId, US MsgId, long MsgLen, const UC *MsgBuf)
{
  int SendedLen = 0;
  //char szTemp[2048];

  if (IsOpen == true)
  {
    SendedLen = AppSendData( ClientId, MsgId, MsgBuf ,MsgLen);
    //sprintf(szTemp, "Send ClientId=%04x MsgId=%04x MsgBuf=%s", ClientId, MsgId, MsgBuf);
  }
  return SendedLen;
}
int CTCPServer::Run(short nServerId, short nServerPort)
{
  if (IsRun == true)
    return 3;
  if (LoadDll() == true)
  {
    if (AppInitS(nServerId,
                 nServerPort,
                 OnAppLogin,
                 OnAppClose,
                 OnAppReceiveData))
    {
        IsRun = true;
        return 0;
    }
    else
    {
        return 2;
    }
  }
  else
  {
      return 1;
  }
}
int CTCPServer::Run(void)
{
  if (IsRun == true)
    return 3;
  if (LoadDll() == true)
  {
    if (AppInitS(ServerId,
                 ServerPort,
                 OnAppLogin,
                 OnAppClose,
                 OnAppReceiveData))
    {
        IsRun = true;
        return 0;
    }
    else
    {
        return 2;
    }
  }
  else
  {
      return 1;
  }
}

