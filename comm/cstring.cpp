#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#define MINCSTRINGLEN		8	    //最小字符串分配長度(包括結尾0)	

#include "cstring.h"
/*
static unsigned  short cstrlen(LPCSTR lpsz)  
{
	unsigned  short len=0;
	while(lpsz[len])
		len++;
	return len;	
}

static void cstrcpy(LPSTR lpdz,LPCSTR lpsz)  
{
	unsigned  short len=0;
	while(lpsz[len])
	{
		lpdz[len]=lpsz[len];
		len++;
	}		
	lpdz[len]=0;	
}  
	   
static void cstrncpyaddterm(LPSTR lpdz,LPCSTR lpsz,unsigned  short len)  
{
	unsigned  short i=0;
	for(i=0;i<len;i++)
	{
		if(lpsz[i]==0)
			break;
		lpdz[i]=lpsz[i];
	}		
	lpdz[i]=0;	
} */ 

void CString::Init()
{
	 pC=0;   // pointer to ref counted string data
	 DataLength=        // length of data (unincluding terminator)
	 AllocLength=0;       // length of allocation	
}	

//實際至少分配nLen+1(除非超過最大限制),保留原有數據,
//如果實際當前緩沖區中可操作區域有效長度(不包括結尾0)>nLen,則返回nLen
//否則返回當前緩沖區中可操作區域有效長度(不包括結尾0)(溢出情況)
unsigned int CString::AllocBuffer(unsigned int nLen)
{	
	if(nLen < AllocLength) //目前緩沖區足夠長
		return nLen;
				
	AllocLength=(nLen>MAXCSTRINGLEN-1)?	MAXCSTRINGLEN-1 : nLen<MINCSTRINGLEN-1? MINCSTRINGLEN-1 : nLen;
	AllocLength++; //包括結尾0
	TCHAR *pCNew=new TCHAR[AllocLength];
	if(pC)
	{
		memcpy(pCNew,pC,DataLength);
	  pCNew[DataLength]=0;
		delete[]pC;
	}	
	else
	  pCNew[0]=0;
	pC=pCNew;
	
	return (nLen < AllocLength-1) ? nLen : (AllocLength-1);
}	
	 
void CString::Release()
{
	if(pC)
		delete []pC;
	Init();
}

void CString::Empty() //常用
{
	if(pC)
	{
		pC[0]=0;
		DataLength=0;
	}		
}
	
void CString::AllocAndCopy(LPCSTR lpch, unsigned int nLength)
{
	if(nLength>0)
	{
		nLength=AllocBuffer(nLength);
		AssignCopy(lpch,nLength);	
	}		
}
	
CString::~CString()		
{
	Release();
}	   
   
CString::CString()
{
	Init();
}
  
CString::CString(const CString& stringSrc)
{
	Init();
	LPCSTR p=stringSrc.C_Str();
	int l=stringSrc.GetLength();
	AllocAndCopy(p,l);			
}
	 
// from a single character
CString::CString(TCHAR ch, unsigned int nRepeat)
{
	Init();
  if(nRepeat>0)
	{
		nRepeat=AllocBuffer(nRepeat);
		
		for(unsigned int i=0;i<nRepeat;i++)
			pC[i]=ch;
		pC[nRepeat]=0;
		if(ch) //如果填充的是0，則數據長度=0
			DataLength=nRepeat;
	}		
}

// from an ANSI string (converts to TCHAR)
CString::CString(LPCSTR lpsz)
{
	int nLength=strlen(lpsz);

	Init();
	AllocAndCopy(lpsz,nLength);	
}	
		
	// subset of characters from an ANSI string (converts to TCHAR)
CString::CString(LPCSTR lpch, unsigned int nLength)
{
	Init();
	AllocAndCopy(lpch,nLength);			
	
}
	
	// from unsigned  characters
CString::CString(const unsigned  char* psz)
{
	int nLength=strlen((char *)psz);

	Init();
	AllocAndCopy((char *)psz,nLength);	
}	
		
		 
//////////////////////賦值(注意不能防止自己賦值給自己)
void CString::AssignCopy(LPCTSTR lpszSrcData,unsigned int nSrcLen)
{	
	DataLength=nSrcLen;
	memcpy(pC,lpszSrcData,DataLength);
	pC[DataLength]=0;
}

//添加數據，檢查是否超出分配長度(不考慮AllocLength==0情況)
void CString::AppendDataWithCheck( LPCTSTR lpszSrcData,unsigned int nSrcLen)
{
	if(nSrcLen>(AllocLength-DataLength-1))
		nSrcLen=AllocLength-DataLength-1;
		
	memcpy(pC+DataLength,lpszSrcData,nSrcLen);
	DataLength+=nSrcLen;
	pC[DataLength]=0;
}

	// ref-counted copy from another CString
const CString& CString::operator=(const CString& stringSrc)
{	
	if(pC==stringSrc.pC)
		return *this; //不能賦值給自己

	unsigned int len=stringSrc.GetLength();
		
	Empty();
	if(len>0)
	{
		len=AllocBuffer(len);
		AssignCopy(stringSrc.pC,len);//C_Str());	
	}
	return *this;
}	
	
	// set string content to single character
const CString& CString::operator=(TCHAR ch)
{
	if(1<AllocLength)
		Empty();
	else	
	{
		Release();
		AllocBuffer(1); //分配一段較長的字節
	}
	DataLength=1;
	pC[0]=ch;
	pC[1]=0;
	
	return *this;
}
	
	// copy string content from ANSI string (converts to TCHAR)
const CString& CString::operator=(LPCSTR lpsz)
{
	unsigned  int len=strlen(lpsz);
	if(len==0)
		Empty();
	else
	{
		if(len<AllocLength)
			Empty();
		else	
		{
			Release();
			len=AllocBuffer(len);
		}
		AssignCopy(lpsz,len);	
	}	

	return *this;
}


// printf-like formatting using passed string
void CString::Format(LPCTSTR lpszFormat, ...)
{
	int len;
	char buf[2048];
	va_list		ArgList;
	va_start(ArgList, lpszFormat);
	len=vsprintf(buf, lpszFormat, ArgList);

	va_end (ArgList);
	if(len<=0)
		Empty();
	else
	{
		len=AllocBuffer(len);
		AssignCopy(buf,len);	
	}	
}
	
	
	// concatenate from another CString
const CString& CString::operator+=(const CString& string)
{
	unsigned  int len=string.GetLength(); //串長度
	if(len>0)
	{	
		AllocBuffer(DataLength+len);
		AppendDataWithCheck(string.pC,len);
	}
	return *this;
}		

	// concatenate a single character
const CString& CString::operator+=(TCHAR ch)
{
	AllocBuffer(DataLength+1);
	AppendDataWithCheck(&ch,1);
	return *this;	
}

		// concatenate a UNICODE character after converting it to TCHAR
const CString& CString::operator+=(LPCTSTR lpsz)
{
	unsigned  int len=strlen(lpsz); //串長度
	
	AllocBuffer(DataLength+len);
	AppendDataWithCheck(lpsz,len);	
	return *this;
}
		
	// append pC by string's nCount characters starting at zero-based nFirst
void CString::AppendMidFrom(const CString &string, unsigned int nFirst, unsigned int nCount)  //常用
{
	if((nFirst>=string.DataLength) || (nCount<=0))
		return;
		
	if(nCount>	string.DataLength-nFirst)
		nCount =	string.DataLength-nFirst;
	
	AllocBuffer(DataLength+nCount);//分配新的大空間
	AppendDataWithCheck(string.pC+nFirst,nCount);		
}


// 將內部數據從0開始復制到buf,并添加結尾0,注意必須保證buf有足夠長度保存所有數據
void CString::CopyOutWithTerm(LPTSTR buf) const 
{
	if(0==DataLength)	 //源沒有足夠數據
	{
		buf[0]=0; //返回空串
		return ;
	}	
		
	memcpy(buf,pC,DataLength+1); //復制數據，包括0			
}

	// 將內部數據從pos開始復制到buf,并添加結尾0,注意nBufSize為包括0在內的緩沖區buf大小
	//返回包括0在內的復制數據長度
int CString::CopyOutWithTerm(LPTSTR buf, unsigned int nBufSize, unsigned int pos ) const 
{
	unsigned int avail;
	if(nBufSize==0) //目標無法容納數據
		return 0;   //不作任何操作返回
	if(pos>=DataLength)	 //源沒有足夠數據
	{
		buf[0]=0; //返回空串
		return 1;
	}	
	
	avail=DataLength-pos+1;  //包括0
	if(	avail>	nBufSize)
		avail=nBufSize;
	
	memcpy(buf,pC+pos,avail); //復制數據，包括0		
	return avail;
}	
		
///尋找/測試


// find character starting at zero-based index and going right
int CString::Find(TCHAR ch, unsigned int nStart) const //常用
{
	unsigned  int i;
	for(i=nStart;i<DataLength;i++)
		if(pC[i]==ch)
			return i;
	return -1;		
}
	
// find first instance of any character in passed string
int CString::FindOneOf(LPCTSTR lpszCharSet) const //常用
{
	unsigned  int i,j;
	unsigned  int len=strlen(lpszCharSet);
	for(i=0;i<DataLength;i++)
		for(j=0;j<len;j++)
			if(pC[i]==lpszCharSet[j])
				return i;
	return -1;			
}	

	// find first instance of substring starting at zero-based index
int CString::Find(LPCTSTR lpszSub, unsigned int nStart) const //常用
{
	unsigned  int i;
	unsigned  int len=strlen(lpszSub);
	if(DataLength<len+nStart)
		return -1;
	for(i=nStart;i<=DataLength-len;i++)
		if(memcmp(pC+i,lpszSub,len)==0)
			return i;
	return -2;			
	
}
	
// find space char from nStart,return pos
int CString::FindSpace(unsigned int nStart) const //常用
{
	while(nStart<DataLength)
	{
		if(isspace(pC[nStart])) //空白
			return nStart;
		nStart++;
	}
	return -1; //沒有找到
}
	
// find not space char from nStart,return pos
int CString::FindNotSpace(unsigned int nStart) const //常用
{
	while(nStart<DataLength)
	{
		if(!isspace(pC[nStart])) //空白
			return nStart;
		nStart++;
	}
	return -1; //沒有找到
}	
		
		
	// find character starting at right
int CString::ReverseFind(TCHAR ch) const //常用
{
	int pos=(int)DataLength-1;
	while(pos>=0)
	{
		if(pC[pos]==ch)
			return pos;
		pos--;	
	}
	return pos;
}
	
	 	// 只比較開始的n個字節
int CString::CompareN(LPCTSTR lpsz,unsigned int nCount) const//常用
{
	if(nCount==0)
		return 0; //不需要比較
		
	if(pC)
		return strncmp(pC,lpsz,nCount);
	else
		return lpsz[0]?-1:0; //測試空串	
}
		
	 	// straight character comparison
int CString::Compare(LPCTSTR lpsz) const //常用
{
	if(pC)
		return strcmp(pC,lpsz);
	else
		return lpsz[0]?-1:0; //測試空串	
}

	// compare ignoring case
int CString::CompareNoCase(LPCTSTR lpsz) const //常用
{
	if(pC)
		return strcmpi(pC,lpsz);
	else
		return lpsz[0]?-1:0; //測試空串	
}	


	

/////修改
	// NLS aware conversion to uppercase
void CString::MakeUpper() //常用
{
	for(unsigned  int i=0;i<DataLength;i++)
		pC[i]=toupper(pC[i]);
}	

	// NLS aware conversion to lowercase
void CString::MakeLower() //常用
{
	for(unsigned  int i=0;i<DataLength;i++)
		pC[i]=tolower(pC[i]);
}	

	// remove whitespace starting from right edge
void CString::TrimRight() //常用
{
	if(DataLength==0)
		return;
	while(DataLength>0)
	{
		if(!isspace(pC[--DataLength])) //空白
		{
			DataLength++; //回復為實際長度
			break;
		}	
	}
	pC[DataLength]=0;	
}

	// remove whitespace starting from left side
void CString::TrimLeft()//常用
{
	unsigned  int pos=0; 
	while(pos<DataLength)
	{
		if(!isspace(pC[pos])) //空白
			break;
		pos++;
	}
	if(pos==0)
		return;
		
	DataLength-=pos;
	memmove(pC,pC+pos,DataLength+1); //移動包括最后的0(memmove可以處理重疊指針)
}
