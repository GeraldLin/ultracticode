//-----------------------------------------------------------------------------
#ifndef __LINUX_IVRDRV_H__
#define __LINUX_IVRDRV_H__
//-----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

const char *GetIvrDrvVersion();
const char *GetDevice();
short GetDeviceId();
short SetWriteDebugId(short WriteDebugId);

short MAININIT(short InitParam);
short MAINEXIT();
short GetChnNum();
short GetConfNum();
short GetFaxChnNum();
short GetRecChnNum();
short GetTrunkNum();
short GetSeatNum();
short GetChnNumByType(short ChType);
short GetStartChIndexByType(short ChType);
short GetChStyle(short ChnNo);
short GetChStyleIndex(short ChnNo);
short GetChType(short ChnNo);
short GetChIndex(short ChnNo);
short GetLgChType(short ChnNo);
short GetLgChIndex(short ChnNo);

short ADDPLAYINDEX(short VocIndex, LPCTSTR FileName, LPCTSTR AliasName);
short POSTPLAYFILE(short ChType, short ChIndex, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
short POSTPLAYMEM(short ChType, short ChIndex, LPCTSTR MemBuf, long BufSize, long PlayOffset, long PlayLength, short PlayParam, short PlayRule);
short POSTPLAYINDEX(short ChType, short ChIndex, short VocIndex, short PlayParam, LPCTSTR PlayRule);
short POSTPLAYTTSSTR(short ChType, short ChIndex, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
short POSTPLAYTTSFILE(short ChType, short ChIndex, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
short POSTSENDDTMF(short ChType, short ChIndex, LPCTSTR DtmfStr, short SendParam, LPCTSTR SendRule);
short POSTSTOPPLAY(short ChType, short ChIndex, short StopParam);

short POSTSTARTRECVFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short CRCType, short RecvParam, LPCTSTR RecvRule);
short POSTSTOPRECVFSK(short ChType, short ChIndex, short FskChIndex);
short POSTSENDFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short FskCMD, long FskDataAddr, short FskDataLen, short CRCType, short SendParam, LPCTSTR SendRule);
short POSTSTOPSENDFSK(short ChType, short ChIndex, short FskChIndex);

short POSTRECORDFILE(short ChType, short ChIndex, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
short POSTRECORDMEM(short ChType, short ChIndex, long RecdLength, short RecdParam, LPCTSTR RecdRule);
short POSTSTOPRECORD(short ChType, short ChIndex, short StopParam);
short POSTSETRPPARAM(short ChType, short ChIndex, short SetParam, LPCTSTR SetRule);

short CONFPLAYFILE(short ConfNo, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
short CONFPLAYINDEX(short ConfNo, short VocIndex, short PlayParam, LPCTSTR PlayRule);
short CONFPLAYTTSSTR(short ConfNo, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
short CONFPLAYTTSFILE(short ConfNo, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
short CONFSTOPPLAY(short ConfNo, short StopParam);
short CONFRECORDFILE(short ConfNo, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
short CONFSTOPRECORD(short ConfNo, short StopParam);
short CONFSETPARAM(short ConfNo, short SetParam, LPCTSTR SetRule);
//發送傳真
short POSTSENDFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
//接收傳真
short POSTRECVFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR BarCode);

short POSTSTOPFAX(short ChType, short ChIndex, short FaxChIndex, short StopParam);
short POSTCHECKTONE(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam);

short POSTCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
short POSTCANCELCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag);
short POSTSAM(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called);
short POSTACM(short ChType, short ChIndex, short AcmParam);
short POSTACK(short ChType, short ChIndex, short AckParam);
short POSTFLASH(short ChType, short ChIndex, short FlashParam);
short POSTRELEASE(short ChType, short ChIndex, short Reason);

short POSTTALK(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short TalkParam);
short POSTMONITOR(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short MonitorParam);
short POSTDISCON(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short DisconParam);
short POSTSETVCPARAM(short ChType, short ChIndex, short VCMode, short VCParam);

short CONFCREATE(short ChType, short ChIndex, short ConfNo, short TalkNum, short ListenNum, short ConfParam);
short CONFDESTROY(short ChType, short ChIndex, short ConfNo, short DestroyParam);
short CONFJOIN(short ChType, short ChIndex, short ConfNo, short JoinParam);
short CONFUNJOIN(short ChType, short ChIndex, short ConfNo, short UnjoinParam);
short CONFLISTEN(short ChType, short ChIndex, short ConfNo, short ListenParam);
short CONFUNLISTEN(short ChType, short ChIndex, short ConfNo, short UnlistenParam);

void IvrDrvLoop();

void OnFireCALLIN(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
void OnFireRECVSAM(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called);
void OnFireRECVACM(short ChType, short ChIndex, short AcmParam);
void OnFireRECVACK(short ChType, short ChIndex, short AckParam);
void OnFireRELEASE(short ChType, short ChIndex, short ReleaseType, short ReleaseReason);

void OnFirePLAYRESULT(short ChType, short ChIndex, short Result);
void OnFireRECORDRESULT(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen);
void OnFireRECVDTMF(short ChType, short ChIndex, LPCTSTR Dtmfs);

void OnFireCONFPLAYRESULT(short ConfNo, short Result);
void OnFireCONFRECORDRESULT(short ConfNo, short Result, long RecdSize, long RecdLen);
//發送傳真結果
void OnFireSENDFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, short SendPages, long SendBytes, long SendSpeed, LPCTSTR ErrorMsg);
//接收傳真結果
void OnFireRECVFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, LPCTSTR FaxCSID, LPCTSTR BarCode, short RecvPages, long RecvBytes, long RecvSpeed, LPCTSTR ErrorMsg);

void OnFireFLASH(short ChType, short ChIndex);
void OnFireLINEALARM(short ChType, short ChIndex, short AlarmType, short AlarmParam);
void OnFireRECORDMEMRESULT(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize);

void OnFireCONFCREATERESULT(short ChType, short ChIndex, short ConfNo, short CreateId, short Result);
void OnFireCONFDESTROYRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCHECKTONERESULT(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result);

void OnFireSENDFSKRESULT(short ChType, short ChIndex, short FskChIndex, short Result, short Param);
void OnFireRECVFSK(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param);

#ifdef  __cplusplus
}
#endif

//-----------------------------------------------------------------------------
#endif
