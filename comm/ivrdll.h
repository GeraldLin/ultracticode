//-----------------------------------------------------------------------------
//各種語音卡API功能封裝dll函數接口
//-----------------------------------------------------------------------------
#ifndef _IVRDRV_API_INCLUDED_
#define _IVRDRV_API_INCLUDED_
//-----------------------------------------------------------------------------

#ifdef WIN32 //win32環境下板卡接口函數定義

//start WIN32
#define MYLIBAPI __declspec(dllexport)

MYLIBAPI const char *GetIvrDrvVersion();
MYLIBAPI const char *GetDevice();
MYLIBAPI short GetDeviceId();
MYLIBAPI short SetWriteDebugId(short WriteDebugId);

MYLIBAPI short MAININIT(short InitParam);
MYLIBAPI short MAINEXIT();
MYLIBAPI short GetChnNum();
MYLIBAPI short GetConfNum();
MYLIBAPI short GetFaxChnNum();
MYLIBAPI short GetRecChnNum();
MYLIBAPI short GetTrunkNum();
MYLIBAPI short GetSeatNum();
MYLIBAPI short GetChnNumByType(short ChType);
MYLIBAPI short GetStartChIndexByType(short ChType);
MYLIBAPI short GetChStyle(short ChnNo);
MYLIBAPI short GetChStyleIndex(short ChnNo);
MYLIBAPI short GetChType(short ChnNo);
MYLIBAPI short GetChIndex(short ChnNo);
MYLIBAPI short GetLgChType(short ChnNo);
MYLIBAPI short GetLgChIndex(short ChnNo);

MYLIBAPI short ADDPLAYINDEX(short VocIndex, LPCTSTR FileName, LPCTSTR AliasName);
MYLIBAPI short POSTPLAYFILE(short ChType, short ChIndex, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTPLAYMEM(short ChType, short ChIndex, LPCTSTR MemBuf, long BufSize, long PlayOffset, long PlayLength, short PlayParam, short PlayRule);
MYLIBAPI short POSTPLAYINDEX(short ChType, short ChIndex, short VocIndex, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTPLAYTTSSTR(short ChType, short ChIndex, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTPLAYTTSFILE(short ChType, short ChIndex, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short POSTSENDDTMF(short ChType, short ChIndex, LPCTSTR DtmfStr, short SendParam, LPCTSTR SendRule);
MYLIBAPI short POSTSTOPPLAY(short ChType, short ChIndex, short StopParam);

MYLIBAPI short POSTSTARTRECVFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short CRCType, short RecvParam, LPCTSTR RecvRule);
MYLIBAPI short POSTSTOPRECVFSK(short ChType, short ChIndex, short FskChIndex);
MYLIBAPI short POSTSENDFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short FskCMD, long FskDataAddr, short FskDataLen, short CRCType, short SendParam, LPCTSTR SendRule);
MYLIBAPI short POSTSTOPSENDFSK(short ChType, short ChIndex, short FskChIndex);

MYLIBAPI short POSTRECORDFILE(short ChType, short ChIndex, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
MYLIBAPI short POSTRECORDMEM(short ChType, short ChIndex, long RecdLength, short RecdParam, LPCTSTR RecdRule);
MYLIBAPI short POSTSTOPRECORD(short ChType, short ChIndex, short StopParam);
MYLIBAPI short POSTSETRPPARAM(short ChType, short ChIndex, short SetParam, LPCTSTR SetRule);

MYLIBAPI short CONFPLAYFILE(short ConfNo, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFPLAYINDEX(short ConfNo, short VocIndex, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFPLAYTTSSTR(short ConfNo, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFPLAYTTSFILE(short ConfNo, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
MYLIBAPI short CONFSTOPPLAY(short ConfNo, short StopParam);
MYLIBAPI short CONFRECORDFILE(short ConfNo, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
MYLIBAPI short CONFSTOPRECORD(short ConfNo, short StopParam);
MYLIBAPI short CONFSETPARAM(short ConfNo, short SetParam, LPCTSTR SetRule);
//發送傳真
MYLIBAPI short POSTSENDFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
MYLIBAPI short POSTAPPENDFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
//接收傳真
MYLIBAPI short POSTRECVFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR BarCode);

MYLIBAPI short POSTSTOPFAX(short ChType, short ChIndex, short FaxChIndex, short StopParam);
MYLIBAPI short POSTCHECKTONE(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam);

MYLIBAPI short POSTCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
MYLIBAPI short POSTCANCELCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag);
MYLIBAPI short POSTSAM(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called);
MYLIBAPI short POSTACM(short ChType, short ChIndex, short AcmParam);
MYLIBAPI short POSTACK(short ChType, short ChIndex, short AckParam);
MYLIBAPI short POSTFLASH(short ChType, short ChIndex, short FlashParam);
MYLIBAPI short POSTRELEASE(short ChType, short ChIndex, short Reason);

MYLIBAPI short POSTTALK(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short TalkParam);
MYLIBAPI short POSTMONITOR(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short MonitorParam);
MYLIBAPI short POSTDISCON(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short DisconParam);
MYLIBAPI short POSTSETVCPARAM(short ChType, short ChIndex, short VCMode, short VCParam);

MYLIBAPI short CONFCREATE(short ChType, short ChIndex, short ConfNo, short TalkNum, short ListenNum, short ConfParam);
MYLIBAPI short CONFDESTROY(short ChType, short ChIndex, short ConfNo, short DestroyParam);
MYLIBAPI short CONFJOIN(short ChType, short ChIndex, short ConfNo, short JoinParam);
MYLIBAPI short CONFUNJOIN(short ChType, short ChIndex, short ConfNo, short UnjoinParam);
MYLIBAPI short CONFLISTEN(short ChType, short ChIndex, short ConfNo, short ListenParam);
MYLIBAPI short CONFUNLISTEN(short ChType, short ChIndex, short ConfNo, short UnlistenParam);

MYLIBAPI short POSTTRANSFER(short ChType, short ChIndex, short TranType, LPCTSTR Called, const char *TranData);
MYLIBAPI short POSTSTOPTRANSFER(short ChType, short ChIndex, short Reason);

MYLIBAPI void IvrDrvLoop();

//設置回調函數
MYLIBAPI void SetFuncOnFireCALLIN(void (*onFireCALLIN)(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled));
MYLIBAPI void SetFuncOnFireRECVSAM(void (*onFireRECVSAM)(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called));
MYLIBAPI void SetFuncOnFireRECVACM(void (*onFireRECVACM)(short ChType, short ChIndex, short AcmParam));
MYLIBAPI void SetFuncOnFireRECVACK(void (*onFireRECVACK)(short ChType, short ChIndex, short AckParam));
MYLIBAPI void SetFuncOnFireRELEASE(void (*onFireRELEASE)(short ChType, short ChIndex, short ReleaseType, short ReleaseReason));
MYLIBAPI void SetFuncOnFirePLAYRESULT(void (*onFirePLAYRESULT)(short ChType, short ChIndex, short Result));
MYLIBAPI void SetFuncOnFireRECORDRESULT(void (*onFireRECORDRESULT)(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen));
MYLIBAPI void SetFuncOnFireRECVDTMF(void (*onFireRECVDTMF)(short ChType, short ChIndex, LPCTSTR Dtmfs));
MYLIBAPI void SetFuncOnFireCONFPLAYRESULT(void (*onFireCONFPLAYRESULT)(short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFRECORDRESULT(void (*onFireCONFRECORDRESULT)(short ConfNo, short Result, long RecdSize, long RecdLen));
MYLIBAPI void SetFuncOnFireSENDFAXRESULT(void (*onFireSENDFAXRESULT)(short ChType, short ChIndex, short FaxChIndex, short Result, short SendPages, long SendBytes, long SendSpeed, LPCTSTR ErrorMsg));
MYLIBAPI void SetFuncOnFireRECVFAXRESULT(void (*onFireRECVFAXRESULT)(short ChType, short ChIndex, short FaxChIndex, short Result, LPCTSTR FaxCSID, LPCTSTR BarCode, short RecvPages, long RecvBytes, long RecvSpeed, LPCTSTR ErrorMsg));
MYLIBAPI void SetFuncOnFireFLASH(void (*onFireFLASH)(short ChType, short ChIndex));
MYLIBAPI void SetFuncOnFireLINEALARM(void (*onFireLINEALARM)(short ChType, short ChIndex, short AlarmType, short AlarmParam));
MYLIBAPI void SetFuncOnFireRECORDMEMRESULT(void (*onFireRECORDMEMRESULT)(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize));
MYLIBAPI void SetFuncOnFireCONFCREATERESULT(void (*onFireCONFCREATERESULT)(short ChType, short ChIndex, short ConfNo, short CreateId, short Result));
MYLIBAPI void SetFuncOnFireCONFDESTROYRESULT(void (*onFireCONFDESTROYRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFJOINRESULT(void (*onFireCONFJOINRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFLISTENRESULT(void (*onFireCONFLISTENRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFUNJOINRESULT(void (*onFireCONFUNJOINRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCONFUNLISTENRESULT(void (*onFireCONFUNLISTENRESULT)(short ChType, short ChIndex, short ConfNo, short Result));
MYLIBAPI void SetFuncOnFireCHECKTONERESULT(void (*onFireCHECKTONERESULT)(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result));
MYLIBAPI void SetFuncOnFireSENDFSKRESULT(void (*onFireSENDFSKRESULT)(short ChType, short ChIndex, short FskChIndex, short Result, short Param));
MYLIBAPI void SetFuncOnFireRECVFSK(void (*onFireRECVFSK)(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param));
MYLIBAPI void SetFuncOnFireFAXSTATE(void (*onFireFAXSTATE)(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FaxState));
//轉接結果Result：0-轉接成功 1-轉接失敗 2-正在振鈴 3-轉接撥號失敗 9-盲轉成功
MYLIBAPI void SetFuncOnFireTRANSFERRESULT(void (*onFireTRANSFERRESULT)(short ChType, short ChIndex, short Result, LPCTSTR Reason));
MYLIBAPI void SetFuncOnFireSTOPTRANSFERRESULT(void (*onFireSTOPTRANSFERRESULT)(short ChType, short ChIndex, short Result));

//初始化完成標志
MYLIBAPI void SetFuncOnFireINITCARDRESULT(void (*onFireINITCARDRESULT)(short Result, short TotalChnNum));
MYLIBAPI void SetFuncOnFireGETAUTHID(void (*onFireGETAUTHID)(short Result, LPCTSTR Reason));

//end WIN32

#else //_LINUX 環境下板卡接口函數定義

//start _LINUX
#ifdef __cplusplus
extern "C" {
#endif

const char *GetIvrDrvVersion();
const char *GetDevice();
short GetDeviceId();
short SetWriteDebugId(short WriteDebugId);

short MAININIT(short InitParam);
short MAINEXIT();
short GetChnNum();
short GetConfNum();
short GetFaxChnNum();
short GetRecChnNum();
short GetTrunkNum();
short GetSeatNum();
short GetChnNumByType(short ChType);
short GetStartChIndexByType(short ChType);
short GetChStyle(short ChnNo);
short GetChStyleIndex(short ChnNo);
short GetChType(short ChnNo);
short GetChIndex(short ChnNo);
short GetLgChType(short ChnNo);
short GetLgChIndex(short ChnNo);

short ADDPLAYINDEX(short VocIndex, LPCTSTR FileName, LPCTSTR AliasName);
short POSTPLAYFILE(short ChType, short ChIndex, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
short POSTPLAYMEM(short ChType, short ChIndex, LPCTSTR MemBuf, long BufSize, long PlayOffset, long PlayLength, short PlayParam, short PlayRule);
short POSTPLAYINDEX(short ChType, short ChIndex, short VocIndex, short PlayParam, LPCTSTR PlayRule);
short POSTPLAYTTSSTR(short ChType, short ChIndex, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
short POSTPLAYTTSFILE(short ChType, short ChIndex, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
short POSTSENDDTMF(short ChType, short ChIndex, LPCTSTR DtmfStr, short SendParam, LPCTSTR SendRule);
short POSTSTOPPLAY(short ChType, short ChIndex, short StopParam);

short POSTSTARTRECVFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short CRCType, short RecvParam, LPCTSTR RecvRule);
short POSTSTOPRECVFSK(short ChType, short ChIndex, short FskChIndex);
short POSTSENDFSK(short ChType, short ChIndex, short FskChIndex, short FskHeader, short FskCMD, long FskDataAddr, short FskDataLen, short CRCType, short SendParam, LPCTSTR SendRule);
short POSTSTOPSENDFSK(short ChType, short ChIndex, short FskChIndex);

short POSTRECORDFILE(short ChType, short ChIndex, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
short POSTRECORDMEM(short ChType, short ChIndex, long RecdLength, short RecdParam, LPCTSTR RecdRule);
short POSTSTOPRECORD(short ChType, short ChIndex, short StopParam);
short POSTSETRPPARAM(short ChType, short ChIndex, short SetParam, LPCTSTR SetRule);

short CONFPLAYFILE(short ConfNo, LPCTSTR FileName, long PlayOffset, long PlayLength, short PlayParam, LPCTSTR PlayRule);
short CONFPLAYINDEX(short ConfNo, short VocIndex, short PlayParam, LPCTSTR PlayRule);
short CONFPLAYTTSSTR(short ConfNo, LPCTSTR TtsStr, short PlayParam, LPCTSTR PlayRule);
short CONFPLAYTTSFILE(short ConfNo, LPCTSTR FileName, short PlayParam, LPCTSTR PlayRule);
short CONFSTOPPLAY(short ConfNo, short StopParam);
short CONFRECORDFILE(short ConfNo, LPCTSTR FileName, long RecdOffset, long RecdLength, short RecdParam, LPCTSTR RecdRule);
short CONFSTOPRECORD(short ConfNo, short StopParam);
short CONFSETPARAM(short ConfNo, short SetParam, LPCTSTR SetRule);
//發送傳真
short POSTSENDFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
short POSTAPPENDFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR Resolution, short TotalPages, LPCTSTR PageList, LPCTSTR FaxHeader, LPCTSTR FaxFooter, LPCTSTR BarCode);
//接收傳真
short POSTRECVFAX(short ChType, short ChIndex, short FaxChIndex, LPCTSTR FileName, LPCTSTR FaxCSID, LPCTSTR BarCode);

short POSTSTOPFAX(short ChType, short ChIndex, short FaxChIndex, short StopParam);
short POSTCHECKTONE(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam);

short POSTCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
short POSTCANCELCALLOUT(short ChType, short ChIndex, short CallParam, short CallFlag);
short POSTSAM(short ChType, short ChIndex, short CallParam, short CallFlag, LPCTSTR Caller, LPCTSTR Called);
short POSTACM(short ChType, short ChIndex, short AcmParam);
short POSTACK(short ChType, short ChIndex, short AckParam);
short POSTFLASH(short ChType, short ChIndex, short FlashParam);
short POSTRELEASE(short ChType, short ChIndex, short Reason);

short POSTTALK(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short TalkParam);
short POSTMONITOR(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short MonitorParam);
short POSTDISCON(short ChType1, short ChIndex1, short ChType2, short ChIndex2, short DisconParam);
short POSTSETVCPARAM(short ChType, short ChIndex, short VCMode, short VCParam);

short CONFCREATE(short ChType, short ChIndex, short ConfNo, short TalkNum, short ListenNum, short ConfParam);
short CONFDESTROY(short ChType, short ChIndex, short ConfNo, short DestroyParam);
short CONFJOIN(short ChType, short ChIndex, short ConfNo, short JoinParam);
short CONFUNJOIN(short ChType, short ChIndex, short ConfNo, short UnjoinParam);
short CONFLISTEN(short ChType, short ChIndex, short ConfNo, short ListenParam);
short CONFUNLISTEN(short ChType, short ChIndex, short ConfNo, short UnlistenParam);

short POSTTRANSFER(short ChType, short ChIndex, short TranType, LPCTSTR Called, const char *TranData);
short POSTSTOPTRANSFER(short ChType, short ChIndex, short Reason);

void IvrDrvLoop();

void OnFireCALLIN(short ChType, short ChIndex, short CallParam, LPCTSTR Caller, LPCTSTR Called, LPCTSTR OrgCalled);
void OnFireRECVSAM(short ChType, short ChIndex, LPCTSTR Caller, LPCTSTR Called);
void OnFireRECVACM(short ChType, short ChIndex, short AcmParam);
void OnFireRECVACK(short ChType, short ChIndex, short AckParam);
void OnFireRELEASE(short ChType, short ChIndex, short ReleaseType, short ReleaseReason);

void OnFirePLAYRESULT(short ChType, short ChIndex, short Result);
void OnFireRECORDRESULT(short ChType, short ChIndex, short Result, long RecdSize, long RecdLen);
void OnFireRECVDTMF(short ChType, short ChIndex, LPCTSTR Dtmfs);

void OnFireCONFPLAYRESULT(short ConfNo, short Result);
void OnFireCONFRECORDRESULT(short ConfNo, short Result, long RecdSize, long RecdLen);
//發送傳真結果
void OnFireSENDFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, short SendPages, long SendBytes, long SendSpeed, LPCTSTR ErrorMsg);
//接收傳真結果
void OnFireRECVFAXRESULT(short ChType, short ChIndex, short FaxChIndex, short Result, LPCTSTR FaxCSID, LPCTSTR BarCode, short RecvPages, long RecvBytes, long RecvSpeed, LPCTSTR ErrorMsg);

void OnFireFLASH(short ChType, short ChIndex);
void OnFireLINEALARM(short ChType, short ChIndex, short AlarmType, short AlarmParam);
void OnFireRECORDMEMRESULT(short ChType, short ChIndex, short Result, LPCTSTR MemBuf, long RecdSize);

void OnFireCONFCREATERESULT(short ChType, short ChIndex, short ConfNo, short CreateId, short Result);
void OnFireCONFDESTROYRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNJOINRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCONFUNLISTENRESULT(short ChType, short ChIndex, short ConfNo, short Result);
void OnFireCHECKTONERESULT(short ChType, short ChIndex, short ToneType, LPCTSTR ToneParam, short Result);

void OnFireSENDFSKRESULT(short ChType, short ChIndex, short FskChIndex, short Result, short Param);
void OnFireRECVFSK(short ChType, short ChIndex, short FskChIndex, short FSKCMD, long FSKStrAddr, short FSKLen, short Result, short Param);
//轉接結果Result：0-轉接成功 1-轉接失敗 2-正在振鈴 3-轉接撥號失敗 9-盲轉成功
void OnFireTRANSFERRESULT(short ChType, short ChIndex, short Result, LPCTSTR Reason);
void OnFireSTOPTRANSFERRESULT(short ChType, short ChIndex, short Result);
//初始化完成標志
void OnFireINITCARDRESULT(short Result, short TotalChnNum);
void OnFireGETAUTHID(short Result, LPCTSTR Reason);

#ifdef  __cplusplus
}
#endif
//end _LINUX

#endif

//-----------------------------------------------------------------------------
#endif
