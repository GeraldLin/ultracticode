// TCPServer.h: interface for the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#ifndef TcpServerH
#define TcpServerH
//---------------------------------------------------------------------------
#include <system.hpp>
#include <stdio.h>
#include <string.h>
#include "boxinc.h"
//---------------------------------------------------------------------------

class CTCPServer
{
	//HINSTANCE hdll; //動態庫句柄；
  void *hdll; //動態庫句柄；
	typedef int  (*APPCLOSEALL)(void);
    typedef int  (*APPMAINLOOP)(void);
    typedef int  (*APPSENDDATA)(unsigned short remoteid,
                                unsigned short msgtype,
                                const unsigned char *buf,
                                unsigned long len);

    typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                    unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len);
    typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
    typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

    typedef int  (*APPINITS)(unsigned short server_id,                            
                            unsigned short server_port,
                            ONAPPLOGIN OnAppLogin,
                            ONAPPCLOSE OnAppClose,
                            ONAPPRECEIVEDATA OnAppReceiveData
                            );
public:
    bool IsOpen; //dll is opend
    bool IsRun;
private:
    void ReleaseDll();

public:
    bool LoadDll();

	char DllFile[250];

    UC ServerType;
    UC ServerNo;
    US ServerId;
	char ServerIP[25];
	short ServerPort;

	APPCLOSEALL     AppCloseAll;
    APPMAINLOOP     AppMainLoop;
    APPSENDDATA     AppSendData;
    APPINITS        AppInitS;

	CTCPServer();
	virtual ~CTCPServer();
	//static CTCPServer& Object();
	//組合消息類型及消息編號
    US CombineID(UC IdType, UC IdNo);
    //分解對象標識及編號
    void DisCombineID(US ID, UC &IdType, UC &IdNo);
    //發送消息
    int SendMessage(US ClientId, US MsgId, long MsgLen, const UC *MsgBuf );
    int Run(short nServerId, short nServerPort);
    int Run(void);
};
//#define mTCPServer CTCPServer::Object()

//tcllink.dll callback function,

extern void OnAppReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnAppLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnAppClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp


#endif // !defined(AFX_TCPSERVER_H__A15B7B15_5824_444E_9B1E_A087BAA5B4CC__INCLUDED_)
