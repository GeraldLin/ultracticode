// TCPClient.h: interface for the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#ifndef TcpClientH
#define TcpClientH
//---------------------------------------------------------------------------
#include <system.hpp>
#include <stdio.h>
#include <string.h>
#include "../include/msgh/tlinkid.h"
#include "../include/msgh/ivrlog.h"
//---------------------------------------------------------------------------

struct TServer
{
	char  ServerIP[32];
	short ServerPort;
	unsigned short Serverid;
  bool  IsConnected;
};

class CTCPClient  
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnAppLogin,
                          ONAPPCLOSE OnAppClose,
                          ONAPPRECEIVEDATA OnAppReceiveData
                          );
  typedef void (*APPINITS)(unsigned short server_id,
    unsigned short server_port,
    ONAPPLOGIN OnAppLogin,
    ONAPPCLOSE OnAppClose,
    ONAPPRECEIVEDATA OnAppReceiveData);
private:
	TServer  IVRServer;		//IVR流程服務器
  TServer  FLWServer;    //流程解析器
  TServer  DBServer;    //數據庫網關
  TServer  CTRLServer;    //linux版本平臺服務控制

  void ReleaseDll();
public:
	CTCPClient();
	virtual ~CTCPClient();
  unsigned short ClientId;

  unsigned short GetIVRServerId()
  {return IVRServer.Serverid;}
  
  void SetIVRLinked() {IVRServer.IsConnected=true;}
  void SetIVRUnlink() {IVRServer.IsConnected=false;}
  bool IsIVRConnected()
  {return IVRServer.IsConnected;}

  void SetFLWLinked() {FLWServer.IsConnected=true;}
  void SetFLWUnlink() {FLWServer.IsConnected=false;}
  bool IsFLWConnected()
  {return FLWServer.IsConnected;}

  void SetDBLinked() {DBServer.IsConnected=true;}
  void SetDBUnlink() {DBServer.IsConnected=false;}
  bool IsDBConnected()
  {return DBServer.IsConnected;}

  void SetCTRLLinked() {CTRLServer.IsConnected=true;}
  void SetCTRLUnlink() {CTRLServer.IsConnected=false;}
  bool IsCTRLConnected()
  {return CTRLServer.IsConnected;}

	char DllFile[64];
  bool LoadDll();
  bool IsOpen; //dll is opend

  bool ReadIni(const char *filename);

  int ConnectIVRServer();
  int ConnectFLWServer();
  int ConnectDBServer();
  int ConnectCTRLServer();
  int ConnectServer();

	//申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;
  APPINITS        AppInitS;

  int SendMessage(unsigned short ServerId, unsigned short MsgId, int MsgLen, const unsigned char *MsgBuf);
  int IVRSendMessage(unsigned short MsgId, const unsigned char *MsgBuf);
  int FLWSendMessage(unsigned short MsgId, const unsigned char *MsgBuf);
  int DBSendMessage(unsigned short MsgId, const unsigned char *MsgBuf);
  int CTRLSendMessage(unsigned short MsgId, const unsigned char *MsgBuf);

  //組合對象編碼
  unsigned short CombineObjectID( unsigned char ObjectType, unsigned char ObjectNo );
	//組合消息類型及消息編號
  unsigned short CombineMessageID(unsigned char MsgType, unsigned char MsgNo);
  //分解對象標識及編號
  void GetClientID(unsigned short Msg_wParamHi, unsigned char &ClientType, unsigned char &ClientNo);
  //分解消息類型及消息編號
  void GetMsgID(unsigned short Msg_wParamLo, unsigned char &MsgType, unsigned char &MsgNo);
};
//tcllinkc.dll callback function,
extern void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnIVRLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnFLWReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnFLWLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnFLWClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnDBLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnDBClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp


extern void OnCTRLReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnCTRLLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnCTRLClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

#endif // !defined(AFX_TCPSERVER_H__A15B7B15_5824_444E_9B1E_A087BAA5B4CC__INCLUDED_)
