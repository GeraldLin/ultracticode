#ifndef __LINKLIB_H__
#define __LINKLIB_H__

/*  server id / client id / port 使用原則

  進程之間不允許使用相同的本地id號,server不允許使用相同的port來listen
  每個進程允許建立一個server tcplink 和 多個client tcplink(使用同一個本地 id 號)
  
  建立新的server tcplink,本地serverid 和 先前已經建立的任何 client tcplink 的 clientid 必須相同
	
  每建立一個新的client tcplink,本地clientid 和 本次的server id 不能重復(不能自己連接自己)
  每建立一個新的client tcplink,本地clientid 和 先前已經建立的 server tcplink 的 本地serverid 必須相同
  每建立一個新的client tcplink,本地clientid 和 先前已經建立的 client tcplink 的 本地clientid 必須相同(一個程序只能作為一個client)
  每建立一個新的client tcplink,本次serverid 和 先前已經建立的 client tcplink 的 serverid 不能重復(對同一個server不能建立兩個連接)
*/

//本頭文件只被DLL的調用者使用，注意版本升級時的修改
#define MAX_PACK_LENGTH		16384          //用戶數據內容最大長度+1

#ifdef __cplusplus
extern "C" {
#endif

//App初始化參數,最先調用的函數,0=fail,1=ok
int AppInit(  //client端調用
			 unsigned short server_id,
			 unsigned short client_id,
			 const char * server_ip_address,
			 unsigned short server_port,
			 void (*onAppLogin)(unsigned short serverid,unsigned short clientid), //事件回調
			 void (*onAppClose)(unsigned short serverid,unsigned short clientid),  //連接斷開
			 void (*onAppReceiveData)(	unsigned short remoteid,
										unsigned short msgtype,
										const unsigned char *buf,unsigned long len)  //數據收到
			);
//App初始化參數,最先調用的函數,0=fail,1=ok
int AppInitS( //server 端調用
			 unsigned short server_id,
			 unsigned short server_port,
			 void (*onAppLogin)(unsigned short serverid,unsigned short clientid), //事件回調
			 void (*onAppClose)(unsigned short serverid,unsigned short clientid),  //連接斷開
			 void (*onAppReceiveData)(	unsigned short remoteid,
										unsigned short msgtype,
										const unsigned char *buf,unsigned long len)  //數據收到
			);

//退出時調用,調用后不要再調用AppMainLoop/AppSendData
void AppCloseAll();

//app發送數據
int AppSendData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf,unsigned long len);

//App定時調用，內部產生onxxx回調事件
int AppMainLoop();

#ifdef  __cplusplus
}
#endif

#endif
