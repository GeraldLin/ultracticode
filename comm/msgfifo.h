//---------------------------------------------------------------------------

#ifndef msgfifoH
#define msgfifoH

#include <stdio.h>
#include <system.hpp>
#include <string.h>
#include "boxinc.h"

//---------------------------------------------------------------------------
//接收消息緩沖結構
typedef struct
{
  US ClientId;
	US MsgId; //指令編號
  CH MsgBuf[2048];

}VXML_RECV_MSG_STRUCT;

class CMsgfifo
{
public:
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_RECV_MSG_STRUCT      *pMsgs;

public:
	CMsgfifo(unsigned short len)
	{
		Head=0;
		Tail=0;
		resv=0;
		Len=len;
		pMsgs=new VXML_RECV_MSG_STRUCT[Len];
	}
	
	~CMsgfifo()
	{
		delete []pMsgs;
	}
	
	bool	IsFull()
	{
		unsigned short temp=Tail+1;
		return temp==Len?Head==0:temp==Head;
	}
	bool 	IsEmpty()
	{
		return Head==Tail;
	}
  VXML_RECV_MSG_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
  	if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(US ClientId, US MsgId, const char *MsgBuf);
  unsigned short  Read(US &ClientId, US &MsgId, char *MsgBuf);
  unsigned short  Read(VXML_RECV_MSG_STRUCT *RecvMsg);
};

//---------------------------------------------------------------------------
#endif
