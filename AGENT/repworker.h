//---------------------------------------------------------------------------

#ifndef repworkerH
#define repworkerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
//---------------------------------------------------------------------------
class TFormRepWorker : public TForm
{
__published:	// IDE-managed Components
  TQuickRep *QuickRep1;
  TQRBand *PageHeaderBand1;
  TQRBand *DetailBand1;
  TQRBand *PageFooterBand1;
  TQRLabel *QRLabel1;
  TQRSysData *QRSysData2;
  TQRLabel *QRLabelTotal;
  TQRSysData *QRSysData1;
  TQRLabel *QRLabel2;
  TQRLabel *QRLabel3;
  TQRLabel *QRLabel4;
  TQRLabel *QRLabel5;
  TQRLabel *QRLabel6;
  TQRLabel *QRLabel7;
  TQRLabel *QRLabel8;
  TQRLabel *QRLabel9;
  TQRDBText *QRDBText1;
  TQRDBText *QRDBText2;
  TQRDBText *QRDBText3;
  TQRDBText *QRDBText4;
  TQRDBText *QRDBText5;
  TQRDBText *QRDBText6;
  TQRDBText *QRDBText7;
  TQRDBText *QRDBText8;
  TQRLabel *QRLabel10;
  TQRDBText *QRDBText9;
private:	// User declarations
public:		// User declarations
  __fastcall TFormRepWorker(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRepWorker *FormRepWorker;
//---------------------------------------------------------------------------
#endif
