//---------------------------------------------------------------------------

#ifndef editrecvfaxruleH
#define editrecvfaxruleH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormRecvFaxRule : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editRuleItem;
  TLabel *Label4;
  TComboBox *cbRuleType;
  TComboBox *cbDepartment;
  TComboBox *cbWorkerNo;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editRuleItemChange(TObject *Sender);
  void __fastcall cbRuleTypeChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormRecvFaxRule(TComponent* Owner);

  int nRuleType;
  int nSelectRuleId;
  CRecvFaxRuleSet RecvFaxRuleSet;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetModifyType(int nruleid, bool isinsert);
  void SetRecvFaxRuleSet(CRecvFaxRuleSet &ruleset);
  int GetRecvFaxRuleSet();
  void ClearRecvFaxRuleSet();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRecvFaxRule *FormRecvFaxRule;
//---------------------------------------------------------------------------
#endif
