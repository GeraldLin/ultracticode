object FormZSKSub: TFormZSKSub
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #26989#21209#30693#35672#24235#23376#20998#39006#35338#24687
  ClientHeight = 306
  ClientWidth = 443
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 13
    Top = 13
    Width = 86
    Height = 14
    AutoSize = False
    Caption = #20027#20998#39006#27161#38988#65306
  end
  object Label3: TLabel
    Left = 27
    Top = 81
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #35443#32048#35498#26126#65306
  end
  object Label11: TLabel
    Left = 51
    Top = 194
    Width = 44
    Height = 14
    AutoSize = False
    Caption = #20633#35387#65306
  end
  object Label8: TLabel
    Left = 14
    Top = 48
    Width = 87
    Height = 14
    AutoSize = False
    Caption = #23376#20998#39006#27161#38988#65306
  end
  object Label1: TLabel
    Left = 24
    Top = 229
    Width = 72
    Height = 13
    Caption = #37325#35201#24615#24207#34399#65306
  end
  object editMainTitle: TEdit
    Left = 104
    Top = 9
    Width = 325
    Height = 21
    TabStop = False
    Color = clGrayText
    Enabled = False
    TabOrder = 0
  end
  object editRemark: TEdit
    Left = 104
    Top = 191
    Width = 325
    Height = 21
    TabOrder = 3
    OnChange = editSubTitleChange
  end
  object memoExplain: TMemo
    Left = 104
    Top = 78
    Width = 325
    Height = 96
    MaxLength = 1000
    ScrollBars = ssVertical
    TabOrder = 2
    OnChange = editSubTitleChange
  end
  object Button1: TButton
    Left = 104
    Top = 265
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 286
    Top = 265
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 5
    OnClick = Button2Click
  end
  object editSubTitle: TEdit
    Left = 104
    Top = 43
    Width = 325
    Height = 21
    TabOrder = 1
    OnChange = editSubTitleChange
  end
  object cseOrderNo: TCSpinEdit
    Left = 104
    Top = 224
    Width = 60
    Height = 22
    TabOrder = 6
    OnChange = editSubTitleChange
  end
end
