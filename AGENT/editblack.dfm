object FormEditBlack: TFormEditBlack
  Left = 426
  Top = 312
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #22686#21152#40657#21517#21934
  ClientHeight = 187
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 52
    Top = 48
    Width = 60
    Height = 13
    Caption = #38651#35441#34399#30908#65306
  end
  object Label2: TLabel
    Left = 52
    Top = 107
    Width = 60
    Height = 13
    Caption = #20633#35387#21517#31281#65306
  end
  object Label3: TLabel
    Left = 52
    Top = 74
    Width = 60
    Height = 13
    Caption = #38480#21046#22825#25976#65306
  end
  object Label4: TLabel
    Left = 52
    Top = 17
    Width = 60
    Height = 13
    Caption = #21517#21934#39006#22411#65306
  end
  object editPhone: TEdit
    Left = 139
    Top = 43
    Width = 131
    Height = 21
    TabOrder = 0
    OnChange = editPhoneChange
  end
  object editRemark: TEdit
    Left = 139
    Top = 104
    Width = 131
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 69
    Top = 139
    Width = 82
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 182
    Top = 139
    Width = 81
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object cseRestDays: TCSpinEdit
    Left = 139
    Top = 69
    Width = 131
    Height = 22
    MaxValue = 65535
    TabOrder = 4
    Value = 7
  end
  object cbBlackType: TComboBox
    Left = 139
    Top = 9
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    Text = #30333#21517#21934
    OnKeyPress = cbBlackTypeKeyPress
    Items.Strings = (
      #40657#21517#21934
      #30333#21517#21934)
  end
end
