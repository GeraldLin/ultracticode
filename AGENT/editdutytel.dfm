object FormEditDutyTel: TFormEditDutyTel
  Left = 706
  Top = 193
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#20540#29677#38651#35441
  ClientHeight = 216
  ClientWidth = 528
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 288
    Top = 20
    Width = 84
    Height = 12
    Caption = #20540#29677#26085#26399#39006#22411#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 33
    Top = 44
    Width = 84
    Height = 12
    Caption = #38283#22987#20540#29677#26085#26399#65306
  end
  object Label3: TLabel
    Left = 288
    Top = 44
    Width = 84
    Height = 12
    Caption = #32080#26463#20540#29677#26085#26399#65306
  end
  object Label4: TLabel
    Left = 8
    Top = 68
    Width = 108
    Height = 12
    Caption = #19978#21320#20540#29677#38283#22987#26178#38291#65306
  end
  object Label5: TLabel
    Left = 8
    Top = 93
    Width = 108
    Height = 12
    Caption = #19978#21320#20540#29677#32080#26463#26178#38291#65306
  end
  object Label6: TLabel
    Left = 264
    Top = 69
    Width = 108
    Height = 12
    Caption = #19979#21320#20540#29677#38283#22987#26178#38291#65306
  end
  object Label7: TLabel
    Left = 264
    Top = 93
    Width = 108
    Height = 12
    Caption = #19979#21320#20540#29677#32080#26463#26178#38291#65306
  end
  object Label8: TLabel
    Left = 56
    Top = 116
    Width = 60
    Height = 12
    Caption = #20540#29677#38651#35441#65306
  end
  object Label9: TLabel
    Left = 312
    Top = 117
    Width = 60
    Height = 12
    Caption = #20633#29992#38651#35441#65306
  end
  object Label10: TLabel
    Left = 56
    Top = 140
    Width = 60
    Height = 12
    Caption = #21855#29992#29376#24907#65306
  end
  object Label11: TLabel
    Left = 69
    Top = 20
    Width = 48
    Height = 12
    Caption = #20540#29677#20154#65306
  end
  object cbWorkerName: TComboBox
    Left = 120
    Top = 16
    Width = 140
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    OnChange = cbWorkerNameChange
  end
  object cbDayType: TComboBox
    Left = 376
    Top = 16
    Width = 140
    Height = 20
    ItemHeight = 12
    TabOrder = 1
    Text = #27599#21608#26576#22825
    OnChange = cbDayTypeChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #27599#21608#26576#22825
      #27599#26376#26576#22825
      #20855#39636#26576#22825)
  end
  object dtpAMStartTime: TDateTimePicker
    Left = 120
    Top = 64
    Width = 51
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 2
    OnChange = cbWorkerNameChange
  end
  object dtpAMEndTime: TDateTimePicker
    Left = 120
    Top = 88
    Width = 51
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 3
    OnChange = cbWorkerNameChange
  end
  object dtpPMStartTime: TDateTimePicker
    Left = 376
    Top = 64
    Width = 51
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 4
    OnChange = cbWorkerNameChange
  end
  object dtpPMEndTime: TDateTimePicker
    Left = 376
    Top = 88
    Width = 51
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 5
    OnChange = cbWorkerNameChange
  end
  object editDutyTel1: TEdit
    Left = 120
    Top = 112
    Width = 140
    Height = 20
    TabOrder = 6
    OnChange = cbWorkerNameChange
  end
  object editDutyTel2: TEdit
    Left = 376
    Top = 112
    Width = 140
    Height = 20
    TabOrder = 7
    OnChange = cbWorkerNameChange
  end
  object cbOpenFlag: TComboBox
    Left = 120
    Top = 136
    Width = 140
    Height = 20
    ItemHeight = 12
    TabOrder = 8
    Text = #21855#29992
    OnChange = cbWorkerNameChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #21855#29992
      #20572#29992)
  end
  object cbStartWeek: TComboBox
    Left = 120
    Top = 40
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 9
    Text = #26143#26399#20845
    OnChange = cbWorkerNameChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #26143#26399#26085
      #26143#26399#19968
      #26143#26399#20108
      #26143#26399#19977
      #26143#26399#22235
      #26143#26399#20116
      #26143#26399#20845)
  end
  object cbEndWeek: TComboBox
    Left = 376
    Top = 40
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 10
    Text = #26143#26399#20845
    OnChange = cbWorkerNameChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #26143#26399#26085
      #26143#26399#19968
      #26143#26399#20108
      #26143#26399#19977
      #26143#26399#22235
      #26143#26399#20116
      #26143#26399#20845)
  end
  object cbStartDay: TComboBox
    Left = 120
    Top = 40
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 11
    Text = #27599#26376'1'#34399
    OnChange = cbWorkerNameChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #27599#26376'1'#34399
      #27599#26376'2'#34399
      #27599#26376'3'#34399
      #27599#26376'4'#34399
      #27599#26376'5'#34399
      #27599#26376'6'#34399
      #27599#26376'7'#34399
      #27599#26376'8'#34399
      #27599#26376'9'#34399
      #27599#26376'10'#34399
      #27599#26376'11'#34399
      #27599#26376'12'#34399
      #27599#26376'13'#34399
      #27599#26376'14'#34399
      #27599#26376'15'#34399
      #27599#26376'16'#34399
      #27599#26376'17'#34399
      #27599#26376'18'#34399
      #27599#26376'19'#34399
      #27599#26376'20'#34399
      #27599#26376'21'#34399
      #27599#26376'22'#34399
      #27599#26376'23'#34399
      #27599#26376'24'#34399
      #27599#26376'25'#34399
      #27599#26376'26'#34399
      #27599#26376'27'#34399
      #27599#26376'28'#34399
      #27599#26376'29'#34399
      #27599#26376'30'#34399
      #27599#26376'31'#34399)
  end
  object cbEndDay: TComboBox
    Left = 376
    Top = 40
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 12
    Text = #27599#26376'1'#34399
    OnChange = cbWorkerNameChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #27599#26376'1'#34399
      #27599#26376'2'#34399
      #27599#26376'3'#34399
      #27599#26376'4'#34399
      #27599#26376'5'#34399
      #27599#26376'6'#34399
      #27599#26376'7'#34399
      #27599#26376'8'#34399
      #27599#26376'9'#34399
      #27599#26376'10'#34399
      #27599#26376'11'#34399
      #27599#26376'12'#34399
      #27599#26376'13'#34399
      #27599#26376'14'#34399
      #27599#26376'15'#34399
      #27599#26376'16'#34399
      #27599#26376'17'#34399
      #27599#26376'18'#34399
      #27599#26376'19'#34399
      #27599#26376'20'#34399
      #27599#26376'21'#34399
      #27599#26376'22'#34399
      #27599#26376'23'#34399
      #27599#26376'24'#34399
      #27599#26376'25'#34399
      #27599#26376'26'#34399
      #27599#26376'27'#34399
      #27599#26376'28'#34399
      #27599#26376'29'#34399
      #27599#26376'30'#34399
      #27599#26376'31'#34399)
  end
  object dtpStartDate: TDateTimePicker
    Left = 120
    Top = 40
    Width = 81
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.524660544
    Time = 40944.524660544
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 13
    OnChange = cbWorkerNameChange
  end
  object dtpEndDate: TDateTimePicker
    Left = 376
    Top = 40
    Width = 81
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5247254745
    Time = 40944.5247254745
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 14
    OnChange = cbWorkerNameChange
  end
  object dtpStartTime: TDateTimePicker
    Left = 204
    Top = 40
    Width = 53
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5249775694
    Time = 40944.5249775694
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 15
    Visible = False
    OnChange = cbWorkerNameChange
  end
  object dtpEndTime: TDateTimePicker
    Left = 460
    Top = 40
    Width = 55
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5249775694
    Time = 40944.5249775694
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 16
    Visible = False
    OnChange = cbWorkerNameChange
  end
  object Button1: TButton
    Left = 120
    Top = 176
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 17
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 304
    Top = 176
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 18
    OnClick = Button2Click
  end
end
