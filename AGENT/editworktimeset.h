//---------------------------------------------------------------------------

#ifndef editworktimesetH
#define editworktimesetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditWorkTimeSet : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label11;
  TComboBox *cbDayType;
  TDateTimePicker *dtpAMStartTime;
  TDateTimePicker *dtpAMEndTime;
  TDateTimePicker *dtpPMStartTime;
  TDateTimePicker *dtpPMEndTime;
  TComboBox *cbStartWeek;
  TComboBox *cbEndWeek;
  TComboBox *cbStartDay;
  TComboBox *cbEndDay;
  TDateTimePicker *dtpStartDate;
  TDateTimePicker *dtpEndDate;
  TLabel *Label10;
  TComboBox *cbOpenFlag;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label8;
  TComboBox *cbWorkType;
  TEdit *editHotLine;
  TLabel *Label9;
  TEdit *editCTID;
  void __fastcall cbDayTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall editHotLineChange(TObject *Sender);
  void __fastcall cbDayTypeChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditWorkTimeSet(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CWorkTimeSet WorkTimeSet;
  void SetModifyType(bool isinsert);
  void SetWorkTimeSetData(CWorkTimeSet& worktimeset);
  int GetWorkTimeSetData();
  void ClearWorkTimeSetData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditWorkTimeSet *FormEditWorkTimeSet;
//---------------------------------------------------------------------------
#endif
