object FormQASel: TFormQASel
  Left = 192
  Top = 110
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #21839#21367#35519#26597
  ClientHeight = 356
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 8
    Width = 313
    Height = 57
    Caption = #35531#36984#25799#21839#21367#21029':'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      #32173#20462#35426#21839#31687#38988#30446
      #26032#35037#27231#35426#21839#31687#38988#30446)
    TabOrder = 0
  end
  object RadioGroup2: TRadioGroup
    Left = 8
    Top = 72
    Width = 313
    Height = 209
    Caption = #35531#36984#25799#21312#22495#21029':'
    ItemIndex = 0
    Items.Strings = (
      #20339#32879#26377#32218#38651#35222
      #21271#28207#26377#32218#38651#35222
      #22823#23663#26377#32218#38651#35222
      #21488#28771#20339#20809#38651#35338
      #20013#25237#26377#32218#38651#35222
      #21488#28771#22522#30990#38283#30332#31185#25216'('#32929')'#20844#21496)
    TabOrder = 1
  end
  object Button1: TButton
    Left = 16
    Top = 304
    Width = 82
    Height = 25
    Caption = #36681#21839#21367#35519#26597
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 128
    Top = 304
    Width = 75
    Height = 25
    Caption = #25499#26039
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 240
    Top = 304
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button3Click
  end
end
