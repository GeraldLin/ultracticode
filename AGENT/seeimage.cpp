//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "seeimage.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormImageSee *FormImageSee;
//---------------------------------------------------------------------------
__fastcall TFormImageSee::TFormImageSee(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormImageSee::SetImagFileName(AnsiString filename)
{
  strImgFileName = filename;
}
void __fastcall TFormImageSee::FormShow(TObject *Sender)
{
  Image1->Picture->LoadFromFile(strImgFileName);
}
//---------------------------------------------------------------------------
