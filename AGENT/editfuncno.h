//---------------------------------------------------------------------------

#ifndef editfuncnoH
#define editfuncnoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditFuncNo : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editFlwFile;
  TEdit *editFlwName;
  TCSpinEdit *cseFuncNo;
  TButton *Button1;
  TButton *Button2;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall cseFuncNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditFuncNo(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetDispValue(bool bInsertId, int nFuncNo, AnsiString strFlwFile, AnsiString strFlwName);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditFuncNo *FormEditFuncNo;
//---------------------------------------------------------------------------
#endif
