//---------------------------------------------------------------------------

#ifndef editacptsubtypeH
#define editacptsubtypeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormAcptSubType : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editAcptSubTypeId;
  TEdit *editAcptSubTypeName;
  TMemo *memoRemark;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editAcptSubTypeIdChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAcptSubType(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAcptSubType *FormAcptSubType;
//---------------------------------------------------------------------------
#endif
