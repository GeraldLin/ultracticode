//---------------------------------------------------------------------------

#ifndef editzskmainH
#define editzskmainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormZSKMain : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label11;
  TEdit *editMainTitle;
  TEdit *editRemark;
  TMemo *memoExplain;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label1;
  TCSpinEdit *cseOrderNo;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editMainTitleChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormZSKMain(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CZSKMain ZSKMain;
  void SetModifyType(bool isinsert);
  void SetZSKMainData(CZSKMain& zskmain);
  int GetZSKMainData();
  void ClearZSKMainData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormZSKMain *FormZSKMain;
//---------------------------------------------------------------------------
#endif
