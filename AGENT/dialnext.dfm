object FormDialNext: TFormDialNext
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #35531#30906#23450#25765#19979#19968#26781#34399#30908
  ClientHeight = 149
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 25
    Top = 20
    Width = 27
    Height = 12
    Caption = #22995#21517':'
  end
  object Label2: TLabel
    Left = 209
    Top = 20
    Width = 27
    Height = 12
    Caption = #24615#21029':'
  end
  object Label3: TLabel
    Left = 24
    Top = 51
    Width = 27
    Height = 12
    Caption = #24180#40801':'
  end
  object Label4: TLabel
    Left = 185
    Top = 51
    Width = 51
    Height = 12
    Caption = #20449#24687#20358#28304':'
  end
  object Button1: TButton
    Left = 144
    Top = 88
    Width = 105
    Height = 41
    Caption = #22806#25765
    TabOrder = 0
    OnClick = Button1Click
  end
  object editCustName: TEdit
    Left = 56
    Top = 16
    Width = 121
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 1
  end
  object editCustSex: TEdit
    Left = 240
    Top = 16
    Width = 121
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 2
  end
  object editCustAge: TEdit
    Left = 56
    Top = 48
    Width = 121
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 3
  end
  object editCustSource: TEdit
    Left = 240
    Top = 48
    Width = 121
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 4
  end
end
