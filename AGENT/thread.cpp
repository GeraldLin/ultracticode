//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "thread.h"
#include "DataModule.h"
#include "main.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CMyThread::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall CMyThread::CMyThread(bool CreateSuspended)
  : TThread(CreateSuspended)
{
  Priority = tpLower;
  FreeOnTerminate = true;
}
//---------------------------------------------------------------------------
void __fastcall CMyThread::Execute()
{
  //---- Place thread code here ----
}
//---------------------------------------------------------------------------
