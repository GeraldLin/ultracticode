object FormEditDispItemSel: TFormEditDispItemSel
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #38917#30446#36984#25799#35373#32622
  ClientHeight = 326
  ClientWidth = 397
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 135
    Top = 21
    Width = 60
    Height = 13
    Caption = #38917#30446#21517#31281#65306
  end
  object Label2: TLabel
    Left = 135
    Top = 46
    Width = 60
    Height = 13
    Caption = #21295#20986#38918#24207#65306
  end
  object Label3: TLabel
    Left = 3
    Top = 69
    Width = 192
    Height = 13
    Caption = #33258#21205#20786#23384#21040#21463#29702#35352#32160#25844#23637#27396#20301#32232#34399#65306
  end
  object Label9: TLabel
    Left = 111
    Top = 205
    Width = 84
    Height = 13
    Caption = #36039#26009#35712#21462#20358#28304#65306
  end
  object Label10: TLabel
    Left = 111
    Top = 227
    Width = 84
    Height = 13
    Caption = 'CRM'#27396#20301#27161#31805#65306
  end
  object editItemName: TEdit
    Left = 200
    Top = 17
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object CheckBox1: TCheckBox
    Left = 136
    Top = 92
    Width = 166
    Height = 18
    TabOrder = 1
    OnClick = CheckBox1Click
  end
  object CheckBox2: TCheckBox
    Left = 136
    Top = 118
    Width = 166
    Height = 18
    TabOrder = 2
    OnClick = CheckBox1Click
  end
  object CheckBox3: TCheckBox
    Left = 136
    Top = 144
    Width = 166
    Height = 18
    TabOrder = 3
    OnClick = CheckBox1Click
  end
  object CheckBox4: TCheckBox
    Left = 136
    Top = 170
    Width = 166
    Height = 18
    TabOrder = 4
    OnClick = CheckBox1Click
  end
  object Button1: TButton
    Left = 51
    Top = 284
    Width = 82
    Height = 27
    Caption = #20462#25913
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 254
    Top = 284
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 6
    OnClick = Button2Click
  end
  object cseExportOrderId: TCSpinEdit
    Left = 200
    Top = 40
    Width = 131
    Height = 22
    MaxValue = 128
    TabOrder = 7
    OnChange = CheckBox1Click
  end
  object cseAcceptExId: TCSpinEdit
    Left = 200
    Top = 64
    Width = 131
    Height = 22
    MaxValue = 32
    TabOrder = 8
    OnChange = CheckBox1Click
  end
  object cbReadDataType: TComboBox
    Left = 199
    Top = 200
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 9
    Text = 'DB'#36039#26009#24235
    OnChange = CheckBox1Click
    OnKeyPress = cbReadDataTypeKeyPress
    Items.Strings = (
      'DB'#36039#26009#24235
      'HTTP'#20171#38754
      'WEBSERVICE'#20171#38754
      #21516#27493'CRM'#36039#26009)
  end
  object editReadFieldTag: TEdit
    Left = 198
    Top = 223
    Width = 131
    Height = 21
    MaxLength = 20
    TabOrder = 10
    OnChange = CheckBox1Click
  end
  object ckSaveToDBFlag: TCheckBox
    Left = 135
    Top = 250
    Width = 231
    Height = 18
    Caption = #26159#21542#23559'CRM'#27396#20301#36039#26009#20786#23384#21040#23458#25142#36039#26009#34920
    Checked = True
    State = cbChecked
    TabOrder = 11
    OnClick = CheckBox1Click
  end
end
