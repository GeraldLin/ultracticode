//---------------------------------------------------------------------------

#ifndef viewemailH
#define viewemailH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormViewEmail : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel2;
  TLabel *Label1;
  TButton *btClose;
  TComboBox *cbWorkerNo;
  TButton *Button1;
  TLabel *Label2;
  TEdit *editEmailFrom;
  TLabel *Label3;
  TEdit *editEmailTo;
  TLabel *Label4;
  TEdit *editEmailSubject;
  TMemo *memoEmailContent;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall btCloseClick(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormViewEmail(TComponent* Owner);

  CAccept m_Accept;
  int m_EmailId;
  AnsiString m_strEmailFrom;
  AnsiString m_strEmailTo;
  AnsiString m_strEmailSubject;
  AnsiString m_strEmailFileName;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormViewEmail *FormViewEmail;
//---------------------------------------------------------------------------
#endif
