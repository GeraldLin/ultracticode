//---------------------------------------------------------------------------

#ifndef editpasswordH
#define editpasswordH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormEditPassword : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editWorkerNo;
  TLabel *Label2;
  TEdit *editOldPsw;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editNewPsw1;
  TEdit *editNewPsw2;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editOldPswChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditPassword(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditPassword *FormEditPassword;
//---------------------------------------------------------------------------
#endif
