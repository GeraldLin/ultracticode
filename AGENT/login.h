//----------------------------------------------------------------------------
#ifndef loginH
#define loginH
//----------------------------------------------------------------------------
#include <vcl\Buttons.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\Classes.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Windows.hpp>
#include <vcl\System.hpp>
#include "CSPIN.h"
//----------------------------------------------------------------------------
class TFormLogin : public TForm
{
__published:
	TLabel *Label1;
  TEdit *edtPassword;
  TLabel *Label2;
  TBitBtn *BitBtn1;
  TBitBtn *BitBtn2;
  TLabel *Label3;
  TLabel *Label4;
  TComboBox *cbWrokerGroup;
  TLabel *Label5;
  TComboBox *cbStateAfterLogin;
  TCheckBox *ckClearId;
  TLabel *Label6;
  TComboBox *cbDutyNo;
  TCheckBox *ckAutoLogin;
  TEdit *editWorkerNo;
  void __fastcall BitBtn1Click(TObject *Sender);
  void __fastcall BitBtn2Click(TObject *Sender);
  void __fastcall FormActivate(TObject *Sender);
  void __fastcall cbWrokerGroupKeyPress(TObject *Sender, char &Key);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall editWorkerNoExit(TObject *Sender);
private:
public:
	virtual __fastcall TFormLogin(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TFormLogin *FormLogin;
//----------------------------------------------------------------------------
#endif    
