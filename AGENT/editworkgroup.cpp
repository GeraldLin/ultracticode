//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editworkgroup.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormWorkerGroup *FormWorkerGroup;

extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormWorkerGroup::TFormWorkerGroup(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormWorkerGroup->cbDepartmentID, dmAgent->DepartmentItemList, "");
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormWorkerGroup::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormWorkerGroup::editGroupNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormWorkerGroup::Button1Click(TObject *Sender)
{
  int nGroupNo, nDepartmentID;

  if (MyIsNumber(editGroupNo->Text) != 0)
  {
    MessageBox(NULL,"請輸入值機群組號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editGroupName->Text == "")
  {
    MessageBox(NULL,"請輸入值機群組名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nGroupNo = StrToInt(editGroupNo->Text);
  nDepartmentID = dmAgent->DepartmentItemList.GetItemValue(cbDepartmentID->Text);
  if (nDepartmentID < 0)
    nDepartmentID = 0;
  if (nGroupNo < 0 || nGroupNo > 255)
  {
    MessageBox(NULL,"對不起，值機群組號範圍為1-255！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsWorkerGroupExist(nGroupNo) == 1)
    {
      MessageBox(NULL,"對不起，該值機群組號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertWorkerGroupRecord(nGroupNo, editGroupName->Text, cseACWTimer->Value, editDialoutPreCode->Text, nDepartmentID) == 0)
    {
      dmAgent->adoQryWorkerGroup->Close();
      dmAgent->adoQryWorkerGroup->Open();
      dmAgent->UpdateWorkerGroupListItems();
      this->Close();
      //MessageBox(NULL,"新增值機組名稱成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增值機群組名稱失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateWorkerGroupRecord(nGroupNo, editGroupName->Text, cseACWTimer->Value, editDialoutPreCode->Text, nDepartmentID) == 0)
    {
      dmAgent->adoQryWorkerGroup->Close();
      dmAgent->adoQryWorkerGroup->Open();
      dmAgent->UpdateWorkerGroupListItems();
      this->Close();
      //MessageBox(NULL,"修改值機組名稱成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改值機群組名稱失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormWorkerGroup::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormWorkerGroup->editGroupNo->SetFocus();
  else
    FormWorkerGroup->editGroupName->SetFocus();
}
//---------------------------------------------------------------------------

