//---------------------------------------------------------------------------

#ifndef editproductH
#define editproductH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditProduct : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label13;
  TEdit *editProductNo;
  TEdit *editName;
  TEdit *editProductType;
  TEdit *editBrand;
  TEdit *editShortDesc;
  TEdit *editUnit;
  TEdit *editWeight;
  TEdit *editMarketPrice;
  TEdit *editMemberPrice;
  TEdit *editProductImg;
  TEdit *editStorage;
  TEdit *editRemark;
  TMemo *memoDescription;
  TButton *Button1;
  TButton *Button2;
  TButton *Button3;
  TOpenDialog *OpenDialog1;
  TLabel *Label14;
  TEdit *editSupplierName;
  TLabel *Label15;
  TEdit *editSupplierTel;
  TLabel *Label16;
  TEdit *editSupplierMem;
  TLabel *Label17;
  TEdit *editSupplierAddr;
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall editProductNoChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditProduct(TComponent* Owner);

  CProducts Products;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetModifyType(bool isinsert);
  void SetProducts(CProducts &products);
  int GetProducts();
  void ClearProducts();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditProduct *FormEditProduct;
//---------------------------------------------------------------------------
#endif
