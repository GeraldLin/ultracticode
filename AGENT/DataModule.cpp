//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include <DateUtils.hpp>
#include "DataModule.h"
#include "main.h"
#include "worker.h"
#include "acceptgroup.h"
#include "editivrmenuset.h"
#include "editrecvfaxrule.h"
#include "login.h"
#include "editcustom.h"
#include "dispcustomer.h"
#include "acceptproc.h"
#include "procrecvsms.h"
#include "editzsksubject.h"
#include "acptitemset.h"
#include "editcustexset.h"
#include "sendsms.h"
#include "dialseatno.h"
#include "dialer.h"
#include "editdialouttask.h"
#include "editdialoutitemset.h"
#include "allocdialout.h"
#include "callin1.h"
#include "dialoutcallback.h"
#include "editworkgroup.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TdmAgent *dmAgent;
extern int MaxFaxNum;
extern AnsiString strCurVersion, strSrvVersion, strUpdateVersion, strSendFaxTaskId;
extern int g_nBandSeatNoType;
//資料庫連接參數
extern AnsiString strDBType, strDBCfgType, strDBServer, strDataBase, strUserId, strPsw, LocaAreaCode, strProvider, RecvFaxPath;
extern AnsiString strCRMDBType, strCRMDBServer, strCRMDataBase, strCRMUserId, strCRMPsw;
extern bool g_bCRMDBOpen;

extern CSysVar gSysvar;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
extern CCallMsg CallMsg;
extern TStringList *strList1;

extern AnsiString MyNowFuncName;
extern int CustExOpenId, SmsOpenId, MaxFaxNum, CurMaxFaxNum;
extern int curSelectTable;

extern int g_nAheadNoticeTime;
extern int g_nOrderEndCode1;
extern int g_nRejectEndCode1;
extern int DialOutCallBackPromptId;
extern AnsiString DialOutCallBackPromptMsg;
extern AnsiString strQueryCustDataURL;
extern int g_nQueryCustDataURLEncodeType;

//2017-10-08
extern AnsiString g_strIDFieldName; //身份證客戶資料擴展欄位字段名
extern int g_nIDFieldIndex; //身份證客戶資料擴展欄位字段索引
extern AnsiString g_strACFieldName; //會員編號客戶資料擴展欄位字段名
extern int g_nACFieldIndex; //會員編號客戶資料擴展欄位字段索引
extern AnsiString g_strTELFieldName; //聯絡號碼客戶資料擴展欄位字段名
extern int g_nTELFieldIndex; //聯絡號碼客戶資料擴展欄位字段索引

extern CAccept MyAccept;
extern CAcceptEx MyAcceptEx;
extern CCustomer MyCustomer; //當前客戶
extern CCustEx MyCustEx;

extern int g_nTranListFlag; //2018-08-26

extern AnsiString strWaitProcAcceptWhere;
extern AnsiString strWaitPCallBackWhere;

int g_nFieldCount=0;

//select a.*,(select b.Explain from tbWorkerType as b where a.workertype=b.workertype) from tbWorker as a
//update tbcallcdr set CustomNo=(select top 1 CustomNo from tbcustomer b where b.MobileNo=CallerNo or b.TeleNo=CallerNo or b.FaxNo=CallerNo) where len(CallerNo)>0
//---------------------------------------------------------------------------
__fastcall TdmAgent::TdmAgent(TComponent* Owner)
  : TDataModule(Owner)
{
  for (int i=0; i<31; i++)
  {
    for (int j=0; j<31; j++)
      DialoutDispColor[i][j] = clBlack;
  }
}
//---------------------------------------------------------------------------
int TdmAgent::GetMyAgentVersion(AnsiString &strVersion, int &nFileSize)
{
  char sqlbuf[128];
  int nResult=1;

  if (ADOConnection1->Connected == false)
    return 1;
  if (strDBType == "SQLSERVER")
    sprintf( sqlbuf, "Select top 1 MyAgentVersion,MyAgentFileSize from tbmyprogram where id=1 order by id");
  else if (strDBType == "MYSQL")
    sprintf( sqlbuf, "Select MyAgentVersion,MyAgentFileSize from tbmyprogram where id=1 order by id limit 1");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strVersion = adoqueryPub->FieldByName("MyAgentVersion")->AsString;
      nFileSize = adoqueryPub->FieldByName("MyAgentFileSize")->AsInteger;
      nResult = 0;
    }
    dmAgent->adoqueryPub->Close();
    if (nResult == 1)
    {
      sprintf( sqlbuf, "intsert into tbmyprogram (id) values (1)");
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
    }
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::DownloadMyAgent()
{
  TADOBlobStream* pBlobRead;
  TMemoryStream *pMemStream;
  char sqlbuf[256];
  AnsiString strOldFileName, strFileName;
  int nFileSize, nResult=1;

  if (ADOConnection1->Connected == false)
    return 1;
  if (strDBType == "SQLSERVER")
    sprintf( sqlbuf, "Select top 1 MyAgentFileSize,MyAgentExe from tbmyprogram where id=1 order by id");
  else if (strDBType == "MYSQL")
    sprintf( sqlbuf, "Select MyAgentFileSize,MyAgentExe from tbmyprogram where id=1 order by id limit 1");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      nFileSize = adoqueryPub->FieldByName("MyAgentFileSize")->AsInteger;
      pBlobRead = new TADOBlobStream((TBlobField*)dmAgent->adoqueryPub->FieldByName("MyAgentExe"),bmRead);
      pMemStream = new TMemoryStream();

      try
      {
        pMemStream->LoadFromStream(pBlobRead);
        if (pMemStream->Size == nFileSize)
        {
          strOldFileName = ExtractFilePath(Application->ExeName)+"MyAgentOld.exe";
          strFileName = ExtractFilePath(Application->ExeName)+"QuarkCallAgent.exe";

          DeleteFile(strOldFileName);
          RenameFile(strFileName, strOldFileName);
          pMemStream->SaveToFile(strFileName.c_str());
          nResult = 0;
        }
      }
      __finally
      {
        delete pMemStream;
        delete pBlobRead;
      }
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::UploadMyAgent(AnsiString strFileName, AnsiString strVersion)
{
  DWORD dwFileSize;
  int nResult=1;
  char sqlbuf[256];
  TMemoryStream *pMemStream;

  if (ADOConnection1->Connected == false)
    return 1;
  if (MyGetFileSize(strFileName.c_str(), dwFileSize) == false)
  {
    return 1;
  }

  sprintf(sqlbuf, "update tbmyprogram set MyAgentVersion='%s',MyAgentFileSize=%d,MyAgentUploadTime=%s,MyAgentExe=:MyblobData where id=1",
    strVersion.c_str(), dwFileSize, MyNowFuncName.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);

    pMemStream = new TMemoryStream();

    try
    {
      pMemStream->LoadFromFile(strFileName);
      pMemStream->Position = 0;
      adoqueryPub->Parameters->ParamByName("MyblobData")->DataType = ftBlob;
      adoqueryPub->Parameters->ParamByName("MyblobData")->LoadFromStream(pMemStream, ftBlob);
      adoqueryPub->ExecSQL();
      nResult = 0;
    }
    __finally
    {
      delete pMemStream;
    }
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::QueryNewVersion()
{
  AnsiString strVersion;
  int nFileSize;

  if (GetMyAgentVersion(strVersion, nFileSize) == 0)
  {
    if (VersionCmp(strVersion, strCurVersion) > 0 && VersionCmp(strVersion, strUpdateVersion) > 0)
    {
      strSrvVersion = strVersion;
      return 1;
    }
  }
  return 0;
}
void TdmAgent::ReadSelTableList()
{
    TListItem  *ListItem1;
    char sqlbuf[256];
    int i=0;

    SelectTableCount = 0;
    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbSelTableList order by id");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
      SelectTable[i].nSelectTableId = i;
      SelectTable[i].strTableName = adoqueryPub->FieldByName("strTableName")->AsString;
      SelectTable[i].strItemId = adoqueryPub->FieldByName("strItemId")->AsString; //選擇項數值
      SelectTable[i].strItemName = adoqueryPub->FieldByName("strItemName")->AsString; //選擇項名稱

      SelectTable[i].strCaption = adoqueryPub->FieldByName("strCaption")->AsString; //窗口標題
      if (i == 1)
      {
        //處理結果
        //FormMain->Label7->Caption = SelectTable[i].strCaption+"：";
        //FormMain->Label7->Left = FormMain->cmbDealState1->Left-FormMain->Label7->Width-2;

        //adoQryCurAcceptAcceptScoreName->DisplayLabel = SelectTable[i].strCaption;
        //adoQryOldAcceptAcceptScoreName->DisplayLabel = SelectTable[i].strCaption;
        //FormMain->dbgCurAccept->Columns->Items[14]->Title->Caption = SelectTable[i].strCaption;
        //FormMain->dbgOldAccept->Columns->Items[14]->Title->Caption = SelectTable[i].strCaption;

        if (adoqueryPub->FieldByName("DispFlag")->AsInteger == 1)
        {
          FormMain->Label7->Visible = true;
          FormMain->cmbDealState1->Visible = true;
          FormMain->Label5->Visible = true;
          FormMain->cmbDealState->Visible = true;
        }
        else
        {
          FormMain->Label7->Visible = false;
          FormMain->cmbDealState1->Visible = false;
          FormMain->Label5->Visible = false;
          FormMain->cmbDealState->Visible = false;
        }
      }
      else if (i == 2)
      {
        //派勤方式
        //FormMain->Label124->Caption = SelectTable[i].strCaption+"：";
        //FormMain->Label124->Left = FormMain->cbAcptSubType->Left-FormMain->Label124->Width-2;

        //adoQryCurAcceptAcptSubTypeName->DisplayLabel = SelectTable[i].strCaption;
        //adoQryOldAcceptAcptSubTypeName->DisplayLabel = SelectTable[i].strCaption;

        //FormMain->Label6->Caption = SelectTable[i].strCaption+"：";
        //FormMain->Label8->Caption = SelectTable[i].strCaption+"：";

        if (adoqueryPub->FieldByName("DispFlag")->AsInteger == 1)
        {
          FormMain->Label124->Visible = true;
          FormMain->cbAcptSubType->Visible = true;
          FormMain->Label6->Visible = true;
          FormMain->cbSubType->Visible = true;
          FormMain->dbgCurAccept->Columns->Items[9]->Visible = true;
          FormMain->dbgOldAccept->Columns->Items[9]->Visible = true;
        }
        else
        {
          FormMain->Label124->Visible = false;
          FormMain->cbAcptSubType->Visible = false;
          FormMain->Label6->Visible = false;
          FormMain->cbSubType->Visible = false;
          FormMain->dbgCurAccept->Columns->Items[9]->Visible = false;
          FormMain->dbgOldAccept->Columns->Items[9]->Visible = false;
        }
      }
      else if (i == 6)
      {
        FormMain->lbCustLevel->Caption = SelectTable[i].strCaption+"：";
      }
      else if (i == 18)
      {
        //FormMain->Label72->Caption = SelectTable[i].strCaption+"：";
        //FormMain->Label72->Left = FormMain->cbSrvSubType->Left-FormMain->Label72->Width-2;

        //FormMain->Label70->Caption = SelectTable[i].strCaption+"：";
        //FormMain->Label70->Left = FormMain->cbSelSrvSubType->Left-FormMain->Label70->Width-2;

        //FormMain->dbgCurAccept->Columns->Items[8]->Title->Caption = SelectTable[i].strCaption;
        //FormMain->dbgOldAccept->Columns->Items[8]->Title->Caption = SelectTable[i].strCaption;
      }

      SelectTable[i].strItemIdDisp = adoqueryPub->FieldByName("strItemIdDisp")->AsString; //選擇項數值輸入框顯示的名稱
      SelectTable[i].strItemNameDisp = adoqueryPub->FieldByName("strItemNameDisp")->AsString; //選擇項名稱輸入框顯示的名稱

      SelectTable[i].strInpuItemIdErrMsg = adoqueryPub->FieldByName("strInpuItemIdErrMsg")->AsString; //輸入選擇項數值錯誤時提示的信息
      SelectTable[i].strInputItemNameErrMsg = adoqueryPub->FieldByName("strInputItemNameErrMsg")->AsString; //輸入選擇項名稱錯誤時提示的信息

      SelectTable[i].strExistMsg = adoqueryPub->FieldByName("strExistMsg")->AsString; //選擇項數值存在時提示的信息
      SelectTable[i].strInsertSuccMsg = adoqueryPub->FieldByName("strInsertSuccMsg")->AsString; //插入選擇項數值成功時提示的信息
      SelectTable[i].strInsertFailMsg = adoqueryPub->FieldByName("strInsertFailMsg")->AsString; //插入選擇項數值失敗時提示的信息
      SelectTable[i].strUpdateSuccMsg = adoqueryPub->FieldByName("strUpdateSuccMsg")->AsString; //修改選擇項數值成功時提示的信息
      SelectTable[i].strUpdateFailMsg = adoqueryPub->FieldByName("strUpdateFailMsg")->AsString; //修改選擇項數值失敗時提示的信息
      SelectTable[i].strDeletePromptMsg = adoqueryPub->FieldByName("strDeletePromptMsg")->AsString; //刪除選擇項數值前提示的信息
      SelectTable[i].strDeleteSuccMsg = adoqueryPub->FieldByName("strDeleteSuccMsg")->AsString; //刪除選擇項數值成功時提示的信息

      SelectTable[i].nMinValue = adoqueryPub->FieldByName("nMinValue")->AsInteger; //刪除選擇項數值成功時提示的信息
      SelectTable[i].nMaxValue = adoqueryPub->FieldByName("nMaxValue")->AsInteger; //刪除選擇項數值成功時提示的信息

      ListItem1 = FormMain->lvSelItem->Items->Add();
      ListItem1->Caption = IntToStr(i+1);
      ListItem1->SubItems->Insert(0, SelectTable[i].strCaption);
      i++;

      adoqueryPub->Next();
    }
    adoqueryPub->Close();
    SelectTableCount = i;
    if (SelectTableCount > 0)
    {
      QuerySelTableList(0);
      curSelectTable = 0;

      FormMain->GroupBox48->Visible = true;
      FormMain->Panel24->Visible = true;
      adoQryProdType->Open();
      if (!dmAgent->adoQryProdType->Eof)
      {
        QueryProdModelTypeList(adoQryProdType->FieldByName("ProdType")->AsInteger);

        QuerySrvTypeList(adoQryProdType->FieldByName("ProdType")->AsInteger);
        if (!adoQrySrvType->Eof)
          QuerySrvSubTypeList(adoQrySrvType->FieldByName("ProdType")->AsInteger, adoQrySrvType->FieldByName("SrvType")->AsInteger);
        else
          adoQrySrvSubType->Close();
      }
      else
      {
        adoQryProdModel->Close();
        adoQrySrvType->Close();
        adoQrySrvSubType->Close();
      }

      QueryEndCode1List();
      if (!adoQryEndCode1->Eof)
        QueryEndCode2List(adoQryEndCode1->FieldByName("EndCode1")->AsInteger);
      else
        adoQryEndCode2->Close();
    }
}
void TdmAgent::QuerySelTableList(int nTableId)
{
  char sqlbuf[256];

  FormMain->GroupBox50->Caption = SelectTable[nTableId].strCaption + "多選項參數設置：";
  sprintf(sqlbuf, "Select %s as ItemId,%s as ItemName from %s order by %s",
    SelectTable[nTableId].strItemId.c_str(),
    SelectTable[nTableId].strItemName.c_str(),
    SelectTable[nTableId].strTableName.c_str(),
    SelectTable[nTableId].strItemId.c_str());
  try
  {
    adoQrySelItem->SQL->Clear();
    adoQrySelItem->SQL->Add((char *)sqlbuf);
    adoQrySelItem->Open();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QueryProdModelTypeList(int nProdType)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "Select * from tbProdModelType where ProdType=%d order by ProdType,ProdModelType", nProdType);
  try
  {
    adoQryProdModel->SQL->Clear();
    adoQryProdModel->SQL->Add((char *)sqlbuf);
    adoQryProdModel->Open();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QuerySrvTypeList(int nProdType)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "Select * from tbSrvType where ProdType=%d order by ProdType,SrvType", nProdType);
  try
  {
    adoQrySrvType->SQL->Clear();
    adoQrySrvType->SQL->Add((char *)sqlbuf);
    adoQrySrvType->Open();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QuerySrvSubTypeList(int nProdType, int nSrvType)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "Select * from tbSrvSubType where ProdType=%d and SrvType=%d order by ProdType,SrvType,SrvSubType", nProdType, nSrvType);
  try
  {
    adoQrySrvSubType->SQL->Clear();
    adoQrySrvSubType->SQL->Add((char *)sqlbuf);
    adoQrySrvSubType->Open();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QueryEndCode1List()
{
  char sqlbuf[256];

  sprintf(sqlbuf, "Select * from tbEndCode1 order by EndCode1");
  try
  {
    adoQryEndCode1->SQL->Clear();
    adoQryEndCode1->SQL->Add((char *)sqlbuf);
    adoQryEndCode1->Open();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QueryEndCode2List(int nEndCode1)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "Select * from tbEndCode2 where EndCode1=%d order by EndCode1,EndCode2", nEndCode1);
  try
  {
    adoQryEndCode2->SQL->Clear();
    adoQryEndCode2->SQL->Add((char *)sqlbuf);
    adoQryEndCode2->Open();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::ReadProvidAreaList(CLookupItemList &itemlist, int ParentId, int RegionLevel)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select id,RegionName from tbRegion where ParentId=%d and RegionLevel=%d order by RegionIndex",
      ParentId, RegionLevel);
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("id")->AsInteger;
        itemname = adoqueryPub->FieldByName("RegionName")->AsString;
        itemlist.AddItem(itemvalue, itemname);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::AddProvidAreaCmbItem(TComboBox *combobox, int ParentId, AnsiString strPY)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select RegionName from tbregion where ParentId=%d and pyszm like '%s%%' order by id",
      ParentId, strPY.c_str());
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
        itemname = adoqueryPub->FieldByName("RegionName")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadFlwFuncNoList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select FuncNo,Remark from tbFlwFuncNo order by FuncNo");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("FuncNo")->AsInteger;
        itemname = adoqueryPub->FieldByName("Remark")->AsString;
        itemlist.AddItem(itemvalue, itemname);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadLookupItemList(CLookupItemList &itemlist, AnsiString strTableName, AnsiString strIDFiledName, AnsiString strNameFiledName)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select %s,%s from %s order by %s", strIDFiledName.c_str(), strNameFiledName.c_str(), strTableName.c_str(), strIDFiledName.c_str());
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName(strIDFiledName)->AsInteger;
        itemname = adoqueryPub->FieldByName(strNameFiledName)->AsString;
        itemlist.AddItem(itemvalue, itemname);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}

void TdmAgent::ReadLookupItemList(CLookupItemList &itemlist, int itemid)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select itemvalue,itemname from tbLookupItems where itemid=%d order by itemvalue", itemid);
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("itemvalue")->AsInteger;
        itemname = adoqueryPub->FieldByName("itemname")->AsString;
        itemlist.AddItem(itemvalue, itemname);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadProdTypeItemList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname, url, btn, demo;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbProdType order by DispOrder,ProdType");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("ProdType")->AsInteger;
        itemname = adoqueryPub->FieldByName("ProdName")->AsString;
        url = adoqueryPub->FieldByName("ProdURL")->AsString;
        btn = adoqueryPub->FieldByName("URLBtnName")->AsString;
        demo = adoqueryPub->FieldByName("ProdDemo")->AsString;
        itemlist.AddItem(itemvalue, itemname, demo, url, btn);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadProdModelTypeItemList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue, itemsubvalue;
    AnsiString itemname, url, btn, demo;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbProdModelType order by ProdType,DispOrder,ProdModelName,ProdModelType");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("ProdType")->AsInteger;
        itemsubvalue = adoqueryPub->FieldByName("ProdModelType")->AsInteger;
        itemname = adoqueryPub->FieldByName("ProdModelName")->AsString;
        url = adoqueryPub->FieldByName("ProdModelURL")->AsString;
        btn = adoqueryPub->FieldByName("URLBtnName")->AsString;
        demo = adoqueryPub->FieldByName("ProdModelDemo")->AsString;
        itemlist.AddItem(itemvalue, itemsubvalue, itemname, demo, url, btn);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadSrvTypeItemList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue, itemsubvalue;
    AnsiString itemname, url, btn, demo;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbSrvType order by ProdType,DispOrder,SrvType");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("ProdType")->AsInteger;
        itemsubvalue = adoqueryPub->FieldByName("SrvType")->AsInteger;
        itemname = adoqueryPub->FieldByName("SrvName")->AsString;
        url = adoqueryPub->FieldByName("SrvURL")->AsString;
        btn = adoqueryPub->FieldByName("URLBtnName")->AsString;
        demo = adoqueryPub->FieldByName("SrvDemo")->AsString;
        itemlist.AddItem(itemvalue, itemsubvalue, itemname, demo, url, btn);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadSrvSubTypeItemList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue, itemsubvalue, itemsubexvalue;
    AnsiString itemname, url, demo, btn;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbSrvSubType order by ProdType,SrvType,DispOrder,SrvSubType");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("ProdType")->AsInteger;
        itemsubvalue = adoqueryPub->FieldByName("SrvType")->AsInteger;
        itemsubexvalue = adoqueryPub->FieldByName("SrvSubType")->AsInteger;
        itemname = adoqueryPub->FieldByName("SrvSubName")->AsString;
        url = adoqueryPub->FieldByName("SrvSubURL")->AsString;
        btn = adoqueryPub->FieldByName("URLBtnName")->AsString;
        demo = adoqueryPub->FieldByName("SrvSubDemo")->AsString;
        itemlist.AddItem(itemvalue, itemsubvalue, itemsubexvalue, itemname, demo, url, btn);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadEndCode1ItemList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname, url;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbEndCode1 order by EndCode1");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("EndCode1")->AsInteger;
        itemname = adoqueryPub->FieldByName("EndCodeName")->AsString;
        itemlist.AddItem(itemvalue, itemname);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}

void TdmAgent::ReadEndCode2ItemList(CLookupItemList &itemlist)
{
    char sqlbuf[256];
    int itemnum, itemvalue, itemsubvalue;
    AnsiString itemname, url, strColor;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbEndCode2 order by EndCode1,EndCode2");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    itemnum = adoqueryPub->RecordCount;
    if (itemlist.InitData(itemnum) == 0)
    {
      while ( !adoqueryPub->Eof )
      {
        itemvalue = adoqueryPub->FieldByName("EndCode1")->AsInteger;
        itemsubvalue = adoqueryPub->FieldByName("EndCode2")->AsInteger;
        itemname = adoqueryPub->FieldByName("EndCodeName")->AsString;
        url = adoqueryPub->FieldByName("EndCodeURL")->AsString;
        strColor = adoqueryPub->FieldByName("DispColor")->AsString;

        if (itemvalue < 32 && itemsubvalue < 32)
        {
        if (strColor.AnsiCompareIC("clBlack") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clBlack;
        else if (strColor.AnsiCompareIC("clMaroon") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clMaroon;
        else if (strColor.AnsiCompareIC("clGreen") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clGreen;
        else if (strColor.AnsiCompareIC("clOlive") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clOlive;
        else if (strColor.AnsiCompareIC("clNavy") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clNavy;
        else if (strColor.AnsiCompareIC("clPurple") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clPurple;
        else if (strColor.AnsiCompareIC("clTeal") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clTeal;
        else if (strColor.AnsiCompareIC("clGray") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clGray;
        else if (strColor.AnsiCompareIC("clSilver") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clSilver;
        else if (strColor.AnsiCompareIC("clRed") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clRed;
        else if (strColor.AnsiCompareIC("clLime") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clLime;
        else if (strColor.AnsiCompareIC("clYellow") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clYellow;
        else if (strColor.AnsiCompareIC("clBlue") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clBlue;
        else if (strColor.AnsiCompareIC("clFuchsia") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clFuchsia;
        else if (strColor.AnsiCompareIC("clAqua") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clAqua;
        else if (strColor.AnsiCompareIC("clLtGray") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clLtGray;
        else if (strColor.AnsiCompareIC("clDkGray") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clDkGray;
        else if (strColor.AnsiCompareIC("clWhite") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clWhite;
        else if (strColor.AnsiCompareIC("clMoneyGreen") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clMoneyGreen;
        else if (strColor.AnsiCompareIC("clSkyBlue") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clSkyBlue;
        else if (strColor.AnsiCompareIC("clCream") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clCream;
        else if (strColor.AnsiCompareIC("clMedGray") == 0)
          DialoutDispColor[itemvalue][itemsubvalue] = clMedGray;
        else
          DialoutDispColor[itemvalue][itemsubvalue] = clBlack;
        }

        itemlist.AddItem(itemvalue, itemsubvalue, itemname, "", url);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadLookupItemList()
{
  if (ADOConnection1->Connected == false)
    return;
  ReadFlwFuncNoList(FlwFuncNoList);
  ReadProvidAreaList(ProvinceList, 0, 1);

  ReadLookupItemList(ControlTypeItemList, "tbcontroltype", "TControlType", "TControlTypeName");

  ReadLookupItemList(LeaveTypeItemList, "tbLeaveType", "LeaveId", "LeaveName");
  ReadLookupItemList(YesNoItemList, "tbYesNo", "ValueId", "ValueName");
  ReadLookupItemList(CustClassList, "tbCustClass", "CustClassID", "CustClassName");
  ReadLookupItemList(WorkerRoleList, "tbRole", "RoleID", "RoleName");
  ReadLookupItemList(WorkerGroupList, "tbWorkerGroup", "GroupNo", "GroupName");
  ReadLookupItemList(DutyNoItemList, "tbdutynotype", "DutyNo", "DutyName");

  ReadProdTypeItemList(ProdTypeItemList);
  ReadProdModelTypeItemList(ProdModelTypeItemList);
  ReadSrvTypeItemList(SrvTypeItemList);
  ReadSrvSubTypeItemList(SrvSubTypeItemList);
  ReadEndCode1ItemList(EndCode1ItemList);
  ReadEndCode2ItemList(EndCode2ItemList);

  ReadLookupItemList(AcptSubTypeItemList, "tbacptsubtype", "AcptSubTypeId", "AcptSubTypeName");
  ReadLookupItemList(CustTypeItemList, "tbCustType", "CustType", "CustTypeName");
  ReadLookupItemList(CustLevelItemList, "tbCustLevel", "CustLevelId", "CustLevelName");
  ReadLookupItemList(ScoreNameList, "tbScore", "ScoreId", "ScoreName");
  ReadLookupItemList(TalkScoreNameList, "tbTalkScore", "TalkScoreId", "TalkScoreName");
  ReadLookupItemList(CallRightItemList, "tbCallRight", "CallRightId", "CallRightName");
  ReadLookupItemList(AcceptProcItemList, "tbAcceptProcType", "AcceptProcId", "AcceptProcName");
  ReadLookupItemList(NextProcTypeItemList, "tbNextProcType", "NextProcTypeId", "NextProcTypeName");
  ReadLookupItemList(NextProcResultItemList, "tbNextProcResult", "NextProcResultId", "NextProcResultName");
  ReadLookupItemList(DepartmentItemList, "tbDepartment", "DepartmentId", "DepartmentName");
  ReadLookupItemList(SendSmsTypeItemList, "tbSendSMSType", "SendSMSTypeId", "SendSMSTypeName");
  ReadLookupItemList(RecvSmsTypeItemList, "tbRecvSMSType", "RecvSMSTypeId", "RecvSMSTypeName");
  ReadLookupItemList(SmsProcTypeItemList, "tbSmsProcType", "SmsProcType", "SmsProcTypeName");
  ReadLookupItemList(SendSmsStatusItemList, "tbSendSmsStatus", "SendSmsStatusId", "SendSmsStatusName");
  ReadLookupItemList(RecdListenIdItemList, "tbRecdListenType", "RecdListenTypeId", "RecdListenTypeName");
  ReadLookupItemList(FaxReadIdItemList, "tbfaxreadType", "ReadId", "ReadIdName");
  ReadLookupItemList(RecdProcTypeItemList, "tbRecdProcType", "RecdProcTypeId", "RecdProcTypeName");
  ReadLookupItemList(DialoutResultItemList, "tbDialoutResult", "DialoutResult", "DialoutResultName");
  ReadLookupItemList(DialOutTaskItemList, "tbdialouttask", "TaskId", "TaskName");
  ReadLookupItemList(PBXBandModeItemList, "tbPBXBandMode", "PBXBandMode", "PBXBandModeName");
  ReadLookupItemList(PBXVoiceBoxItemList, "tbPBXVoiceBox", "PBXVoiceBox", "PBXVoiceBoxName");
  ReadLookupItemList(ZSKIssueStatusItemList, "tbZSKIssueStatus", "IssueStatusId", "IssueStatusName");
  ReadLookupItemList(SendFaxStateItemList, "tbSendFaxState", "SendFaxStateId", "SendFaxStateName");
  ReadLookupItemList(DocTranStateItemList, "tbDocTranState", "DocTranStateId", "DocTranStateName");

  if (g_nBandSeatNoType > 0)
  {
    dmAgent->AddCmbItem(FormAcptItemSet->cbControlType, dmAgent->ControlTypeItemList, "");
    dmAgent->AddCmbItem(FormEditCustExSet->cbControlType, dmAgent->ControlTypeItemList, "");
    dmAgent->AddCmbItem(FormEditDialOutItemSet->cbControlType, dmAgent->ControlTypeItemList, "");
  }
  if (g_nBandSeatNoType > 0)
  {
    dmAgent->ReadTranSeatFuncKey();
    dmAgent->ReadCallOutFuncKey();
  }

  //設置離席狀態菜單
  if (LeaveTypeItemList.GetItemName(1) != "")
  {
    FormMain->mLeaveId1->Visible = true;
    FormMain->mLeaveId1->Caption = LeaveTypeItemList.GetItemName(1);
  }
  if (LeaveTypeItemList.GetItemName(2) != "")
  {
    FormMain->mLeaveId2->Visible = true;
    FormMain->mLeaveId2->Caption = LeaveTypeItemList.GetItemName(2);
  }
  if (LeaveTypeItemList.GetItemName(3) != "")
  {
    FormMain->mLeaveId3->Visible = true;
    FormMain->mLeaveId3->Caption = LeaveTypeItemList.GetItemName(3);
  }
  if (LeaveTypeItemList.GetItemName(4) != "")
  {
    FormMain->mLeaveId4->Visible = true;
    FormMain->mLeaveId4->Caption = LeaveTypeItemList.GetItemName(4);
  }
  if (LeaveTypeItemList.GetItemName(5) != "")
  {
    FormMain->mLeaveId5->Visible = true;
    FormMain->mLeaveId5->Caption = LeaveTypeItemList.GetItemName(5);
  }
  if (LeaveTypeItemList.GetItemName(6) != "")
  {
    FormMain->mLeaveId6->Visible = true;
    FormMain->mLeaveId6->Caption = LeaveTypeItemList.GetItemName(6);
  }
  if (LeaveTypeItemList.GetItemName(7) != "")
  {
    FormMain->mLeaveId7->Visible = true;
    FormMain->mLeaveId7->Caption = LeaveTypeItemList.GetItemName(7);
  }
  if (LeaveTypeItemList.GetItemName(8) != "")
  {
    FormMain->mLeaveId8->Visible = true;
    FormMain->mLeaveId8->Caption = LeaveTypeItemList.GetItemName(8);
  }
  if (LeaveTypeItemList.GetItemName(9) != "")
  {
    FormMain->mLeaveId9->Visible = true;
    FormMain->mLeaveId9->Caption = LeaveTypeItemList.GetItemName(9);
  }

  //話務員資料
  if (g_nBandSeatNoType > 0)
  {
    AddCmbItem(FormWorker->cbDepartmentID, DepartmentItemList, "");
    AddCmbItem(FormWorker->cmbWWorkType, WorkerRoleList, "");
    AddCmbItem(FormWorker->cmbWCallRight, CallRightItemList, "");
    AddCmbItem(FormWorker->cbGroupNo, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo1, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo2, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo3, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo4, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo5, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo6, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo7, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo8, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo9, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo10, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo11, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo12, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo13, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo14, WorkerGroupList, "不選擇");
    AddCmbItem(FormWorker->cbGroupNo15, WorkerGroupList, "不選擇");

    AddCmbItem(FormWorkerGroup->cbDepartmentID, dmAgent->DepartmentItemList, "");

    AddWorkerNameCmbItem(FormIVRMenuSet->editWorkerNo);
  }

  //ACD分配組設置
  if (g_nBandSeatNoType > 0)
  {
    FormAcceptGroup->cbGroupNo->Items->Add("不選擇");
    FormAcceptGroup->cbGroupNo->Items->Add("----------");
    FormAcceptGroup->cbGroupNo->Items->Add("留言建議");
    FormAcceptGroup->cbGroupNo->Items->Add("IVR提示語音");
    if (MaxFaxNum > 0)
      FormAcceptGroup->cbGroupNo->Items->Add("接收傳真");
    FormAcceptGroup->cbGroupNo->Items->Add("----------");
    AddCmbItem(FormAcceptGroup->cbGroupNo, WorkerGroupList, "", false);
    FormAcceptGroup->cbGroupNo->Items->Add("----------");
    AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo, false);

    AddCmbItem(FormAcceptGroup->cbGroupNo1, WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo1->Items->Add("----------");
    AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo1, false);

    AddCmbItem(FormAcceptGroup->cbGroupNo2, WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo2->Items->Add("----------");
    AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo2, false);

    AddCmbItem(FormAcceptGroup->cbGroupNo3, WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo3->Items->Add("----------");
    AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo3, false);

    AddCmbItem(FormAcceptGroup->cbGroupNo4, WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo4->Items->Add("----------");
    AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo4, false);

    AddCmbItem(FormAcceptGroup->cbSrvType, ProdTypeItemList, "");

    AddCmbItem(FormLogin->cbWrokerGroup, WorkerGroupList, "不選擇");
    AddCmbItem(FormLogin->cbDutyNo, DutyNoItemList, "不選擇");

    AddWorkerNameCmbItem(FormRecvFaxRule->cbWorkerNo);
    AddDepartmentCmbItem(FormRecvFaxRule->cbDepartment);

    dmAgent->AddCmbItem(FormEditDialOutTask->cbGroupNo, WorkerGroupList, "不轉值機坐席");
    dmAgent->AddCmbItem(FormEditDialOutTask->cbFuncNo, FlwFuncNoList, "系統預設流程");
  }

  //修改客戶資料
  if (g_nBandSeatNoType > 0)
  {
    AddCmbItem(FormEditCustom->edtCustType, CustTypeItemList, "");
    AddCmbItem(FormEditCustom->edtCustLevel, CustLevelItemList, "");
    AddCmbItem(FormEditCustom->cbProvince, ProvinceList, "");
    AddCmbItem(FormEditCustom->cbCustClass, CustClassList, "");
    //FormEditCustom->CreateCustExControl(); //2017-01-02 del
    //FormDispCustom->CreateCustExControl(); //2017-01-02 del
  }

  AddCmbItem(FormMain->cbDepartmentID, DepartmentItemList, "所有部門"); //增加?分不同部?的?限 2019-08-04

  AddCmbItem(FormMain->cbCProvince, ProvinceList, "不選擇");
  AddCmbItem(FormMain->cbUserType, CustTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbUserLevel, CustLevelItemList, "不選擇");

  //業務受理
  AddCmbItem(FormMain->edtCustType, CustTypeItemList, "");
  AddCmbItem(FormMain->edtCustLevel, CustLevelItemList, "");
  AddCmbItem(FormMain->cmbProvince, ProvinceList, "");
  AddCmbItem(FormMain->cbSelProdType, ProdTypeItemList, "");
  AddCmbItem(FormMain->cbProdType, ProdTypeItemList, "");
  AddCmbItem(FormMain->cbProdType1, ProdTypeItemList, "");
  //AddCmbItem(FormMain->cbAcceptType, SrvTypeItemList, "");
  AddCmbItem(FormMain->cbAcptSubType, AcptSubTypeItemList, "");

  //業務處理
  AddCmbItem(FormMain->cbOldAcceptSrvType, ProdTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbOldAcceptDealResult, AcceptProcItemList, "不選擇");
  //AddCmbItem(FormMain->cbSrvType, SrvTypeItemList, "");
  //AddCmbItem(FormMain->cbCurSrvType, SrvTypeItemList, "");
  AddCmbItem(FormMain->cbSubType, AcptSubTypeItemList, "");
  AddCmbItem(FormMain->cbCurSubType, AcptSubTypeItemList, "");
  AddCmbItem(FormMain->cbSubType1, AcptSubTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbAcceptScore, TalkScoreNameList, "");
  AddCmbItem(FormMain->cbCurAcceptScore, TalkScoreNameList, "");
  AddCmbItem(FormMain->cmbDealState, AcceptProcItemList, "");
  AddCmbItem(FormMain->cmbCurDealState, AcceptProcItemList, "");
  AddCmbItem(FormMain->cmbDealState1, AcceptProcItemList, "");
  //AddCmbItem(FormMain->cbSelSrvType, SrvTypeItemList, "");

  if (g_nBandSeatNoType > 0)
  {
    AddCmbItem(FormAcceptProc->cmbDealState, AcceptProcItemList, "");
    AddCmbItem(FormAcceptProc->cbNextProcType, NextProcTypeItemList, "");
    AddCmbItem(FormAcceptProc->cbNextProcResult, NextProcResultItemList, "");
  }

  //留言建議
  AddCmbItem(FormMain->cbRecdListenId, RecdListenIdItemList, "不選擇");
  AddCmbItem(FormMain->cbRecdProcId, RecdProcTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbRecdProcState, RecdProcTypeItemList, "");

  //接收傳真
  AddCmbItem(FormMain->cbRfaxReadId, FaxReadIdItemList, "不選擇");
  AddCmbItem(FormMain->cbRfaxProcId, RecdProcTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbRfaxProcState, RecdProcTypeItemList, "");

  //發送傳真
  AddCmbItem(FormMain->cbSendFaxState, SendFaxStateItemList, "所有狀態");

  //自動外呼
  AddCmbItem(FormMain->cbDialOutResult, DialoutResultItemList, "不選擇");
  AddCmbItem(FormMain->cbDialOutResultH, DialoutResultItemList, "不選擇");
  AddCmbItem(FormMain->cbDialOutResult1, DialoutResultItemList, "不選擇");
  AddCmbItem(FormMain->cbDialResult3, DialoutResultItemList, "不選擇");
  AddCmbItem(FormMain->cbDialOutResult2, DialoutResultItemList, "不選擇");
  AddCmbItem(FormMain->cbDialOutResult3, DialoutResultItemList, "");
  AddCmbItem(FormMain->cbTaskId2, DialOutTaskItemList, "不選擇");

  AddCmbItem(FormMain->cbEndCode1, EndCode1ItemList, "不選擇");
  AddCmbItem(FormMain->cbEndCode12, EndCode1ItemList, "不選擇");
  AddCmbItem(FormMain->cbEndCode13, EndCode1ItemList, "不選擇");
  AddCmbItem(FormMain->cbEndCode14, EndCode1ItemList, "");

  //接收短信
  AddCmbItem(FormMain->cbRSReadFlag, SmsProcTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbProcSrvType, RecvSmsTypeItemList, "不選擇");
  if (g_nBandSeatNoType > 0)
  {
    AddCmbItem(FormRecvSmsProc->cbReadFlag, SmsProcTypeItemList, "");
    AddCmbItem(FormRecvSmsProc->cbProcSrvType, RecvSmsTypeItemList, "");
  }

  //發送短信
  AddCmbItem(FormMain->cbSSSendStatus, SendSmsStatusItemList, "不選擇");
  AddCmbItem(FormMain->cbSSSmsSrvType, SendSmsTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbSendSmsSrvType, SendSmsTypeItemList, "");
  if (g_nBandSeatNoType > 0)
  {
    dmAgent->AddCmbItem(FormSendSms->cbSendSmsSrvType, dmAgent->SendSmsTypeItemList, "");
  }

  //業務知識庫
  if (g_nBandSeatNoType > 0)
  {
    AddCmbItem(FormZSKSubject->cbIssueStatus, ZSKIssueStatusItemList, "");
    AddCmbItem(FormZSKSubject->cbSecrecyId, CustClassList, "");
    AddCmbItem(FormZSKSubject->cbDepartmentId, DepartmentItemList, "");
    FormZSKSubject->cbDepartmentId->Items->Add("所有部門");
  }

  //通話記錄
  AddCmbItem(FormMain->cbGroupNo, WorkerGroupList, "所有");
  AddCmbItem(FormMain->cbScore, ScoreNameList, "所有");
  AddCmbItem(FormMain->cbTalkScore, TalkScoreNameList, "");
  AddCmbItem(FormMain->cbCDRAcceptType, ProdTypeItemList, "不選擇");

  //業務受理統計
  //AddCmbItem(FormMain->cbOldAcceptSrvType1, SrvTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbOldAcceptDealResult1, AcceptProcItemList, "不選擇");

}
void TdmAgent::AddProdTypeCmbItem(TComboBox *combobox)
{
  char sqlbuf[512];
  AnsiString itemname;
  if (ADOConnection1->Connected == false)
    return;
  combobox->Items->Clear();

  sprintf(sqlbuf, "Select ProdName from tbProdType where GroupNoList='0' or (%d>0 and CHARINDEX('%d',GroupNoList)>0) or (%d>0 and CHARINDEX('%d',GroupNoList)>0) or (%d>0 and CHARINDEX('%d',GroupNoList)>0) or (%d>0 and CHARINDEX('%d',GroupNoList)>0) or (%d>0 and CHARINDEX('%d',GroupNoList)>0) order by DispOrder,ProdType",
    MyWorker.GroupNo, MyWorker.GroupNo, MyWorker.GroupNo1, MyWorker.GroupNo1, MyWorker.GroupNo2, MyWorker.GroupNo2, MyWorker.GroupNo3, MyWorker.GroupNo3, MyWorker.GroupNo4, MyWorker.GroupNo4);
  //WriteErrprMsg("%s", sqlbuf);
  try
  {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
  }
  catch ( ... )
  {
      return;
  }
  while ( !adoqueryPub->Eof )
  {
      itemname = adoqueryPub->FieldByName("ProdName")->AsString;
      combobox->Items->Add(itemname);
      adoqueryPub->Next();
  }
  adoqueryPub->Close();
}
void TdmAgent::AddCmbItem(TComboBox *combobox, int itemid, AnsiString AffixItem, bool bClearBefore)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    if (bClearBefore == true)
      combobox->Items->Clear();
    if (AffixItem.Length() > 0)
      combobox->Items->Add(AffixItem);
    sprintf(sqlbuf, "Select itemvalue,itemname from tbLookupItems where itemid=%d order by itemvalue", itemid);
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
        itemname = adoqueryPub->FieldByName("itemname")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
void TdmAgent::AddCmbItem(TComboBox *combobox, CLookupItemList &itemlist, AnsiString AffixItem, bool bClearBefore)
{
  if (bClearBefore == true)
    combobox->Items->Clear();
  if (AffixItem.Length() > 0)
    combobox->Items->Add(AffixItem);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    combobox->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
//2015-03-24
int TdmAgent::GetSecondItemNum(CLookupItemList &itemlist, int nFirstValue, int &nSecondType)
{
  int nCount=0;
  nSecondType = 0;
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nFirstValue)
    {
      if (nSecondType == 0)
        nSecondType = itemlist.pLookupItem[i].ItemSubValue;
      nCount++;
    }
  }
  return nCount;
}
void TdmAgent::AddSecondCmbItem(TComboBox *combobox, CLookupItemList &itemlist, int nFirstValue, AnsiString AffixItem, bool bClearBefore)
{
  if (bClearBefore == true)
    combobox->Items->Clear();
  if (AffixItem.Length() > 0)
    combobox->Items->Add(AffixItem);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nFirstValue)
      combobox->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
void TdmAgent::AddThirdCmbItem(TComboBox *combobox, CLookupItemList &itemlist, int nFirstValue, int nSecondValue, AnsiString AffixItem, bool bClearBefore)
{
  if (bClearBefore == true)
    combobox->Items->Clear();
  if (AffixItem.Length() > 0)
    combobox->Items->Add(AffixItem);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nFirstValue && itemlist.pLookupItem[i].ItemSubValue == nSecondValue)
      combobox->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
//2015-03-24
int TdmAgent::GetFirstItemNum(CLookupItemList &itemlist, int &nFirstType)
{
  if (itemlist.ItemNum == 1)
    nFirstType = itemlist.pLookupItem[0].ItemValue;
  else
    nFirstType = 0;
  return itemlist.ItemNum;
}
int TdmAgent::GetThirdItemNum(CLookupItemList &itemlist, int nFirstValue, int nSecondValue, int &nThirdType)
{
  int nCount=0;
  nThirdType = 0;
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nFirstValue && itemlist.pLookupItem[i].ItemSubValue == nSecondValue)
    {
      if (nThirdType == 0)
        nThirdType = itemlist.pLookupItem[i].ItemSubExValue;
      nCount++;
    }
  }
  return nCount;
}
bool TdmAgent::IsProdTypeExist1(int nProdType)
{
  return ProdTypeItemList.IsItemExist(nProdType);
}
bool TdmAgent::IsProdModelTypeExist1(int nProdType, int nProdModelType)
{
  return ProdModelTypeItemList.IsItemExist(nProdType, nProdModelType);
}
bool TdmAgent::IsSrvTypeExist1(int nProdType, int nSrvType)
{
  return SrvTypeItemList.IsItemExist(nProdType, nSrvType);
}
bool TdmAgent::IsSrvSubTypeExist1(int nProdType, int nSrvType, int nSrvSubType)
{
  return SrvSubTypeItemList.IsItemExist(nProdType, nSrvType, nSrvSubType);
}

void TdmAgent::AddRgItem(TRadioGroup *radiogroup, CLookupItemList &itemlist)
{
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    radiogroup->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
void TdmAgent::AddDialGrItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData, int nCols)
{
  int nIndex=MyStrToInt(DefaultData);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TDialCheckBoxGroupList[AcptControlNo][i] = new TRadioButton(this);
    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%nCols))
    {
    case 0:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    case 4:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1000;
      break;
    case 5:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1250;
      break;
    case 6:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1500;
      break;
    case 7:
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1750;
      break;
    }
    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Top = (i/nCols)*22+14;
    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Width = 180;

    if (i == nIndex)
    {
      ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Checked = true;
    }

    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->OnClick = FormMain->memoDialOutLogChange;
    ((TRadioButton *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}
void TdmAgent::AddDialGbItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData, int nCols)
{
  int num = MySplit(DefaultData.c_str(), ';', strList1);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TDialCheckBoxGroupList[AcptControlNo][i] = new TCheckBox(this);
    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%nCols))
    {
    case 0:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    case 4:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1000;
      break;
    case 5:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1250;
      break;
    case 6:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1500;
      break;
    case 7:
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Left = 1750;
      break;
    }
    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Top = (i/nCols)*22+14;
    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Width = 180;

    if (i < num)
    {
      if (strList1->Strings[i] == "1")
        ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Checked = true;
      else
        ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }
    else
    {
      ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }

    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->OnClick = FormMain->memoDialOutLogChange;
    ((TCheckBox *)FormMain->TDialCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}
void TdmAgent::AddGrItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData, int nCols)
{
  int nIndex=MyStrToInt(DefaultData);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TCheckBoxGroupList[AcptControlNo][i] = new TRadioButton(this);
    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%nCols))
    {
    case 0:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    case 4:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1000;
      break;
    case 5:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1250;
      break;
    case 6:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1500;
      break;
    case 7:
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1750;
      break;
    }
    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Top = (i/nCols)*22+14;
    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Width = 180;

    if (i == nIndex)
    {
      ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Checked = true;
    }

    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->OnClick = FormMain->memoAcceptContChange;
    ((TRadioButton *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}
void TdmAgent::AddCurGrItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData)
{
  int nIndex=MyStrToInt(DefaultData);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TCurCheckBoxGroupList[AcptControlNo][i] = new TRadioButton(this);
    ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%4))
    {
    case 0:
      ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    }
    ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Top = (i/4)*22+14;
    ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Width = 180;

    if (i == nIndex)
    {
      ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Checked = true;
    }

    ((TRadioButton *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}
void TdmAgent::AddOldGrItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData)
{
  int nIndex=MyStrToInt(DefaultData);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TOldCheckBoxGroupList[AcptControlNo][i] = new TRadioButton(this);
    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%4))
    {
    case 0:
      ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    }
    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Top = (i/4)*22+14;
    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Width = 180;

    if (i == nIndex)
    {
      ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Checked = true;
    }

    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->OnClick = FormMain->MyOldInputOnChangeEvent;
    ((TRadioButton *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}

void TdmAgent::AddGbItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData, int nCols)
{
  int num = MySplit(DefaultData.c_str(), ';', strList1);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TCheckBoxGroupList[AcptControlNo][i] = new TCheckBox(this);
    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%nCols))
    {
    case 0:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    case 4:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1000;
      break;
    case 5:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1250;
      break;
    case 6:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1500;
      break;
    case 7:
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Left = 1750;
      break;
    }
    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Top = (i/nCols)*22+14;
    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Width = 180;

    if (i < num)
    {
      if (strList1->Strings[i] == "1")
        ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Checked = true;
      else
        ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }
    else
    {
      ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }

    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->OnClick = FormMain->memoAcceptContChange;
    ((TCheckBox *)FormMain->TCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}
void TdmAgent::AddCurGbItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData)
{
  int num = MySplit(DefaultData.c_str(), ';', strList1);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TCurCheckBoxGroupList[AcptControlNo][i] = new TCheckBox(this);
    ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%4))
    {
    case 0:
      ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    }
    ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Top = (i/4)*22+14;
    ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Width = 180;
    if (i < num)
    {
      if (strList1->Strings[i] == "1")
        ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Checked = true;
      else
        ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }
    else
    {
      ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }
    ((TCheckBox *)FormMain->TCurCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}
void TdmAgent::AddOldGbItem(TGroupBox *groupbox, CLookupItemList &itemlist, int AcptControlNo, AnsiString DefaultData)
{
  int num = MySplit(DefaultData.c_str(), ';', strList1);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    FormMain->TOldCheckBoxGroupList[AcptControlNo][i] = new TCheckBox(this);
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Parent = groupbox;
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Caption = itemlist.pLookupItem[i].ItemName;
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    switch ((i%4))
    {
    case 0:
      ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 6;
      break;
    case 1:
      ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 250;
      break;
    case 2:
      ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 500;
      break;
    case 3:
      ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Left = 750;
      break;
    }
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Top = (i/4)*22+14;
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Width = 180;
    if (i < num)
    {
      if (strList1->Strings[i] == "1")
        ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Checked = true;
      else
        ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }
    else
    {
      ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Checked = false;
    }
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->OnClick = FormMain->MyOldInputOnChangeEvent;
    ((TCheckBox *)FormMain->TOldCheckBoxGroupList[AcptControlNo][i])->Show();
  }
}

void TdmAgent::AddSrvSubTypeCmbItem(TComboBox *combobox, CLookupItemList &itemlist, int nSrvType)
{
  combobox->Items->Clear();
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nSrvType)
      combobox->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
void TdmAgent::AddSrvSubTypeCmbItem(TComboBox *combobox, int nSrvType)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select SrvSubName from tbSrvSubType where SrvType=%d order by SrvSubType", nSrvType);
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
        itemname = adoqueryPub->FieldByName("SrvSubName")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
int TdmAgent::GetSrvSubType(int nSrvType, AnsiString ItemName)
{
  char sqlbuf[256];
  int nSrvSubType=-1;

  sprintf(sqlbuf, "Select SrvSubType from tbSrvSubType where SrvType=%d and SrvSubName='%s'", nSrvType, ItemName.c_str());
  try
  {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
  }
  catch ( ... )
  {
      return -1;
  }
  if ( !adoqueryPub->Eof )
  {
      nSrvSubType = adoqueryPub->FieldByName("SrvSubType")->AsInteger;
  }
  adoqueryPub->Close();
  return nSrvSubType;
}

void TdmAgent::AddEndCode2CmbItem(TComboBox *combobox, CLookupItemList &itemlist, int nEndCode1)
{
  combobox->Items->Clear();
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nEndCode1)
      combobox->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
void TdmAgent::AddEndCode2CmbItem(TComboBox *combobox, int nEndCode1)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select EndCodeName from tbEndCode2 where EndCode1=%d order by EndCode2", nEndCode1);
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
        itemname = adoqueryPub->FieldByName("EndCodeName")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
int TdmAgent::GetEndCode2(int nEndCode1, AnsiString ItemName)
{
  char sqlbuf[256];
  int nEndCode2=-1;

  sprintf(sqlbuf, "Select EndCode2 from tbEndCode2 where EndCode1=%d and EndCodeName='%s'", nEndCode1, ItemName.c_str());
  try
  {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
  }
  catch ( ... )
  {
      return -1;
  }
  if ( !adoqueryPub->Eof )
  {
      nEndCode2 = adoqueryPub->FieldByName("EndCode2")->AsInteger;
  }
  adoqueryPub->Close();
  return nEndCode2;
}

void TdmAgent::AddProdModelTypeCmbItem(TComboBox *combobox, CLookupItemList &itemlist, int nProdType, AnsiString AffixItem)
{
  combobox->Items->Clear();
  if (AffixItem != "")
    combobox->Items->Add(AffixItem);
  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (itemlist.pLookupItem[i].ItemValue == nProdType)
      combobox->Items->Add(itemlist.pLookupItem[i].ItemName);
  }
}
void TdmAgent::AddProdModelTypeCmbItem(TComboBox *combobox, int nProdType)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select ProdModelName from tbProdModelType where ProdType=%d order by DispOrder,ProdModelType", nProdType);
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
        itemname = adoqueryPub->FieldByName("ProdModelName")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
int TdmAgent::GetProdModelType(int nProdType, AnsiString ItemName)
{
  char sqlbuf[256];
  int nProdModelType=-1;

  sprintf(sqlbuf, "Select ProdModelType from tbProdModelType where ProdType=%d and ProdModelName='%s'", nProdType, ItemName.c_str());
  try
  {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
  }
  catch ( ... )
  {
      return -1;
  }
  if ( !adoqueryPub->Eof )
  {
      nProdModelType = adoqueryPub->FieldByName("ProdModelType")->AsInteger;
  }
  adoqueryPub->Close();
  return nProdModelType;
}
AnsiString TdmAgent::GetDispStrByBoolean(bool bValue)
{
  if (bValue == true)
    return "是";
  else
    return "否";
}
void TdmAgent::AddDepartmentCmbItem(TComboBox *combobox)
{
    char sqlbuf[256];
    //int itemvalue;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbDepartment order by DepartmentID");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    combobox->Items->Clear();
    while ( !adoqueryPub->Eof )
    {
      //itemvalue = adoqueryPub->FieldByName("DepartmentID")->AsInteger;
      itemname = adoqueryPub->FieldByName("DepartmentName")->AsString;
      combobox->Items->Add(itemname);
      adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
void TdmAgent::ReadDeptSeatNo(int DeptNo)
{
    char sqlbuf[256];
    AnsiString SeatNo;

    FormMain->SeatList.ClearDeptSeatData();
    if (DeptNo == 0)
      return;
    if (ADOConnection1->Connected == false)
      return;

    sprintf(sqlbuf, "Select SeatNo from tbDeptSeatNo where DepartmentID=%d", DeptNo);

    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    if (adoqueryPub->RecordCount > 0)
    {
      FormMain->SeatList.SetDeptSeatData(MyWorker.DepartmentID, MySeat.SeatNo);
      while ( !adoqueryPub->Eof )
      {
        SeatNo = adoqueryPub->FieldByName("SeatNo")->AsString;
        FormMain->SeatList.SetDeptSeatData(DeptNo, SeatNo);
        adoqueryPub->Next();
      }
    }
    adoqueryPub->Close();
}
AnsiString TdmAgent::ReadDeptIVRNo(int DeptNo)
{
  char sqlbuf[128];
  AnsiString IVRNO="";

  sprintf(sqlbuf, "Select IVRNO from tbDepartment where DepartmentID=%d", DeptNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      IVRNO = adoqueryPub->FieldByName("IVRNO")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return IVRNO;
}
void TdmAgent::AddWorkerCmbItem(TComboBox *combobox, bool bClearBefore)
{
    char sqlbuf[256];
    int itemvalue;
    AnsiString strWorkerNo, strWorkerName, strDepartmentName;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select WorkerNo,WorkerName,DepartmentName from vm_Worker order by DepartmentID,WorkerNo");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    if (bClearBefore == true)
      combobox->Items->Clear();
    while ( !adoqueryPub->Eof )
    {
      //strWorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString;
      strWorkerName = adoqueryPub->FieldByName("WorkerName")->AsString;
      strDepartmentName = adoqueryPub->FieldByName("DepartmentName")->AsString;
      combobox->Items->Add(strWorkerName+"-"+strDepartmentName);
      adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
void TdmAgent::AddWorkerListView(TListView *pListView, AnsiString sqlwhere)
{
    char sqlbuf[2048];
    int i=0;
    AnsiString strWorkerNo, strWorkerName, strGroupName, strGroupName0, strGroupName1, strGroupName2, strGroupName3, strGroupName4;
    TListItem  *ListItem1;

    if (ADOConnection1->Connected == false)
      return;
    if (sqlwhere == "")
      sprintf(sqlbuf, "Select WorkerNo,WorkerName,GroupName0,GroupName1,GroupName2,GroupName3,GroupName4 from vm_Worker order by WorkerNo");
    else
      sprintf(sqlbuf, "Select WorkerNo,WorkerName,GroupName0,GroupName1,GroupName2,GroupName3,GroupName4 from vm_Worker where %s order by WorkerNo", sqlwhere.c_str());
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    pListView->Items->Clear();
    while ( !adoqueryPub->Eof )
    {
      strWorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString;
      strWorkerName = adoqueryPub->FieldByName("WorkerName")->AsString;
      strGroupName0 = adoqueryPub->FieldByName("GroupName0")->AsString;
      strGroupName1 = adoqueryPub->FieldByName("GroupName1")->AsString;
      strGroupName2 = adoqueryPub->FieldByName("GroupName2")->AsString;
      strGroupName3 = adoqueryPub->FieldByName("GroupName3")->AsString;
      strGroupName4 = adoqueryPub->FieldByName("GroupName4")->AsString;
      if (strGroupName0 != "")
        strGroupName = strGroupName0;
      if (strGroupName1 != "")
      {
        if (strGroupName == "")
          strGroupName = strGroupName1;
        else
          strGroupName = strGroupName+","+strGroupName1;
      }
      if (strGroupName2 != "")
      {
        if (strGroupName == "")
          strGroupName = strGroupName2;
        else
          strGroupName = strGroupName+","+strGroupName2;
      }
      if (strGroupName3 != "")
      {
        if (strGroupName == "")
          strGroupName = strGroupName3;
        else
          strGroupName = strGroupName+","+strGroupName3;
      }
      if (strGroupName4 != "")
      {
        if (strGroupName == "")
          strGroupName = strGroupName4;
        else
          strGroupName = strGroupName+","+strGroupName4;
      }

      ListItem1 = pListView->Items->Add();
      ListItem1->Checked = true;
      ListItem1->Caption = IntToStr(i);
      ListItem1->SubItems->Insert(0, strWorkerNo);
      ListItem1->SubItems->Insert(1, strWorkerName);
      ListItem1->SubItems->Insert(2, strGroupName);
      ListItem1->SubItems->Insert(3, "0");
      ListItem1->SubItems->Insert(4, "0");
      i++;

      adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
void TdmAgent::AddGroupListView(CLookupItemList &itemlist, TListView *pListView)
{
  TListItem  *ListItem1;

  pListView->Items->Clear();

  for (int i = 0; i < itemlist.ItemNum; i ++)
  {
    if (MyWorker.WorkType == 9)
    {
      ListItem1 = pListView->Items->Add();
      ListItem1->Checked = false;
      ListItem1->Caption = IntToStr(itemlist.pLookupItem[i].ItemValue);
      ListItem1->SubItems->Insert(0, itemlist.pLookupItem[i].ItemName);
    }
    else
    {
      if ((MyWorker.GroupNo > 0 && itemlist.pLookupItem[i].ItemValue == MyWorker.GroupNo)
           || (MyWorker.GroupNo1 > 0 && itemlist.pLookupItem[i].ItemValue == MyWorker.GroupNo1)
           || (MyWorker.GroupNo2 > 0 && itemlist.pLookupItem[i].ItemValue == MyWorker.GroupNo2)
           || (MyWorker.GroupNo3 > 0 && itemlist.pLookupItem[i].ItemValue == MyWorker.GroupNo3)
           || (MyWorker.GroupNo4 > 0 && itemlist.pLookupItem[i].ItemValue == MyWorker.GroupNo4))
      {
        ListItem1 = pListView->Items->Add();
        ListItem1->Checked = false;
        ListItem1->Caption = IntToStr(itemlist.pLookupItem[i].ItemValue);
        ListItem1->SubItems->Insert(0, itemlist.pLookupItem[i].ItemName);
      }
    }
  }
}
void TdmAgent::AddWorkerNameCmbItem(TComboBox *combobox, bool bClearBefore)
{
    char sqlbuf[256];
    int itemvalue;
    AnsiString strWorkerName;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select WorkerName from tbWorker order by WorkerNo");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    if (bClearBefore == true)
      combobox->Items->Clear();
    while ( !adoqueryPub->Eof )
    {
      strWorkerName = adoqueryPub->FieldByName("WorkerName")->AsString;
      combobox->Items->Add(strWorkerName);
      adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
void TdmAgent::AddWorkerNoNameCmbItem(TComboBox *combobox, int nWorkType)
{
    char sqlbuf[256];
    AnsiString strWorkerNo, strWorkerName;

    if (ADOConnection1->Connected == false)
      return;
    if (nWorkType == 0)
      sprintf(sqlbuf, "Select WorkerNo,WorkerName from tbWorker order by WorkerNo");
    else if (nWorkType == 1)
      sprintf(sqlbuf, "Select WorkerNo,WorkerName from tbWorker where (WorkerType=1 or WorkerType=2) order by WorkerNo");
    else if (nWorkType == 3)
      sprintf(sqlbuf, "Select WorkerNo,WorkerName from tbWorker where (WorkerType=3 or WorkerType=4) order by WorkerNo");
    else
      sprintf(sqlbuf, "Select WorkerNo,WorkerName from tbWorker where WorkerType=9 order by WorkerNo");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    //if (bClearBefore == true)
      combobox->Items->Clear();
    while ( !adoqueryPub->Eof )
    {
      strWorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString;
      strWorkerName = adoqueryPub->FieldByName("WorkerName")->AsString;
      combobox->Items->Add(strWorkerNo+"-"+strWorkerName);
      adoqueryPub->Next();
    }
    adoqueryPub->Close();
}
//---------------------------------------------------------------------------
int TdmAgent::ConnectDB()
{
  char ConnectionString[256];

  try
  {
    ADOConnection1->Connected = false;
    if (strDBCfgType == "SQLSERVER")
    {
      if (strProvider.Length() == 0)
      {
        sprintf(ConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s",
        strPsw.c_str(), strUserId.c_str(), strDataBase.c_str(), strDBServer.c_str());
      }
      else
      {
        AnsiString strTemp, strTemp1;
        strTemp1 = strProvider;
        MyReplace(strTemp1, "$S_DBSERVER", strDBServer, strTemp);
        MyReplace(strTemp, "$S_DATABASE", strDataBase, strTemp1);
        MyReplace(strTemp1, "$S_USERID", strUserId, strTemp);
        MyReplace(strTemp, "$S_USERPWD", strPsw, strTemp1);
        strcpy(ConnectionString, strTemp1.c_str());
      }
    }
    else if (strDBCfgType == "ORACLE")
    {
        sprintf(ConnectionString, "Provider=OraOLEDB.Oracle.1;Password=%s;Persist Security Info=True;User ID=%s;Data Source=%s",
        strPsw.c_str(), strUserId.c_str(), strDBServer.c_str());
    }
    else if (strDBCfgType == "MYSQL")
    {
      sprintf(ConnectionString, "Driver={MySQL ODBC 5.1 Driver};server=%s;uid=%s;pwd=%s;database=%s;",
        strDBServer.c_str(), strUserId.c_str(), strPsw.c_str(), strDataBase.c_str());
    }
    else if (strDBCfgType == "ODBC")
    {
      sprintf(ConnectionString, "Data Source=%s;UID=%s;PWD=%s;",
        strDBServer.c_str(), strUserId.c_str(), strPsw.c_str());
    }
    else
    {
      return 2;//數據庫參數設置錯誤
    }
    ADOConnection1->ConnectionString = (char *)ConnectionString;
    ADOConnection1->Connected = true;
    return 0;
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("ConnectDB %s error=%s", ConnectionString, exception.Message.c_str());
    ADOConnection1->Connected = false;
    FormMain->StatusBar1->Panels->Items[7]->Text = "失敗("+strDBCfgType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    return 1; //連接數據庫失敗
  }
}
int TdmAgent::ConnectCRMDB()
{
  char ConnectionString[256];

  if (g_bCRMDBOpen == false)
    return 1;
  try
  {
    ADOConnection4->Connected = false;
    if (strCRMDBType == "SQLSERVER")
    {
      sprintf(ConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s",
        strCRMPsw.c_str(), strCRMUserId.c_str(), strCRMDataBase.c_str(), strCRMDBServer.c_str());
    }
    else if (strCRMDBType == "ORACLE")
    {
        sprintf(ConnectionString, "Provider=OraOLEDB.Oracle.1;Password=%s;Persist Security Info=True;User ID=%s;Data Source=%s",
        strCRMPsw.c_str(), strCRMUserId.c_str(), strCRMDBServer.c_str());
    }
    else if (strCRMDBType == "MYSQL")
    {
      sprintf(ConnectionString, "Driver={MySQL ODBC 5.1 Driver};server=%s;uid=%s;pwd=%s;database=%s;",
        strCRMDBServer.c_str(), strCRMUserId.c_str(), strCRMPsw.c_str(), strCRMDataBase.c_str());
    }
    else if (strCRMDBType == "ODBC")
    {
      sprintf(ConnectionString, "Data Source=%s;UID=%s;PWD=%s;",
        strCRMDBServer.c_str(), strCRMUserId.c_str(), strCRMPsw.c_str());
    }
    else
    {
      return 2;//數據庫參數設置錯誤
    }
    ADOConnection4->ConnectionString = (char *)ConnectionString;
    ADOConnection4->Connected = true;
    FormMain->StatusBar1->Panels->Items[13]->Text = "連接第三方資料庫成功";
    return 0;
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("ConnectCRMDB error=%s", exception.Message.c_str());
    ADOConnection4->Connected = false;
    FormMain->StatusBar1->Panels->Items[13]->Text = "連接第三方資料庫失敗";
    return 1; //連接數據庫失敗
  }
}
int TdmAgent::ReConnectDB(void)
{
  try
  {
    ADOConnection1->Connected = true;
    FormMain->StatusBar1->Panels->Items[7]->Text = "成功("+strDBCfgType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    FormMain->ToolBar1->Color = clBtnFace;
    return 0;
  }
  catch (Exception &exception)
  {
    ADOConnection1->Connected = false;
    FormMain->ToolBar1->Color = clRed;
    FormMain->StatusBar1->Panels->Items[7]->Text = "失敗("+strDBCfgType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    return 1;
  }
}
void TdmAgent::CloseAllDB()
{
  if (ADOConnection1->Connected == false)
    return;
  adotbWorker->Close();
  adoQryBlack->Close();
  adoQyrCallCdr->Close();
  adoQryOPLog->Close();
  adotbZSKMain->Close();
  adotbZSKSub->Close();
  adotbZSKSubject->Close();
  adoQryRecvSms->Close();
  adoQrySendSms->Close();
  adoQryCalloutTask->Close();
  adoQryCalloutTele->Close();
  adoQryCurAccept->Close();
  adoQryCurAcceptProc->Close();
  adoQryOldAccept->Close();
  adoQryOldAcceptProc->Close();
  adoQryUserBase->Close();
  adoQryCustExSet->Close();
  adoQryCustExFieldList->Close();
  adoQryDirectDial->Close();
  adoQrySubPhone->Close();
  adoQryWork->Close();
  adoQryAccept->Close();
  adoQryWork1->Close();
  adoQryAccept1->Close();
  adoQryDirectDial->Close();
  adoQrySubPhone->Close();
  adoQryDirectPhone->Close();
  adoQryRecvMsg->Close();
  adoQrySendMsg->Close();
  adoQryRecord->Close();

  adoQrySrvType->Close();
  adoQryWorkerGroup->Close();
  adoQrySearchCust->Close();
  adoQryAcceptGroup->Close();
  adoQryRecvFax->Close();

  adoQryAcptItemSet->Close();
  adoQryAcptItemList->Close();
  adoQryCustomSet->Close();
  adoQryAcceptSet->Close();
  adoQryCallCDRSet->Close();
  adoQryIVRSet->Close();

  adoQryRecvFaxRule->Close();
  adoQrySendFax->Close();
  adoQryRecvFax->Close();

  adoQryTabAuthSet->Close();
  adoQryDialOutTask->Close();
  adoQryDialOutTele->Close();
  adoQryDialOutItemSet->Close();
  adoQryDialOutItemList->Close();
  adoQryDialOutProc->Close();

  FormMain->tvZSKQry->Items->Clear();
}
//---------------------------------------------------------------------------
AnsiString TdmAgent::GetTelOfCity(AnsiString &tel, short ChanType)
{
  AnsiString temp, code="";
  char sqlbuf[256];
  AnsiString itemname="未知歸屬地";

  //if (ChanType == 2)
  //{
  //  itemname = "內部坐席";
  //  return itemname;
  //}
  if (tel == "000" || tel == "")
  {
    itemname = "未知主叫";
    return itemname;
  }
  else if (tel.SubString(1,1) != "0" && tel.SubString(1,1) != "1" && tel.SubString(1,1) != "9" && tel.Length() <= 4)
  {
    itemname = "內部分機";
    return itemname;
  }
  else if (tel.SubString(1,2) == "13" || tel.SubString(1,2) == "15" || tel.SubString(1,2) == "18")
  {
    if (strDBType == "SQLSERVER")
      sprintf(sqlbuf, "Select top 1 city,code from tbMobileList where num=left('%s',len(num))", tel.c_str());
    else if (strDBType == "MYSQL")
      sprintf(sqlbuf, "Select city,code from tbMobileList where num=left('%s',LENGTH(num)) limit 1", tel.c_str());
    try
    {
      adoqueryPub1->SQL->Clear();
      adoqueryPub1->SQL->Add((char *)sqlbuf);
      adoqueryPub1->Open();
    }
    catch ( ... )
    {
      return itemname;
    }
    if ( !adoqueryPub1->Eof )
    {
      itemname = adoqueryPub1->FieldByName("city")->AsString;
      code = adoqueryPub1->FieldByName("code")->AsString;
    }
    adoqueryPub1->Close();
    if (LocaAreaCode != code)
      tel = "0"+tel;
    return itemname;
  }
  else if (tel.SubString(1,3) == "013" || tel.SubString(1,3) == "015" || tel.SubString(1,3) == "018")
  {
    temp = tel.SubString(2,11);
    if (strDBType == "SQLSERVER")
      sprintf(sqlbuf, "Select top 1 city,code from tbMobileList where num=left('%s',len(num))", temp.c_str());
    else if (strDBType == "MYSQL")
      sprintf(sqlbuf, "Select city,code from tbMobileList where num=left('%s',LENGTH(num)) limit 1", temp.c_str());
    try
    {
      adoqueryPub1->SQL->Clear();
      adoqueryPub1->SQL->Add((char *)sqlbuf);
      adoqueryPub1->Open();
    }
    catch ( ... )
    {
      return itemname;
    }
    if ( !adoqueryPub1->Eof )
    {
      itemname = adoqueryPub1->FieldByName("city")->AsString;
      code = adoqueryPub1->FieldByName("code")->AsString;
    }
    if (LocaAreaCode == code)
      tel = tel.SubString(2,11);
    adoqueryPub1->Close();
    return itemname;
  }
  else if (tel.SubString(1,1) == "0")
  {
    if (strDBType == "SQLSERVER")
      sprintf(sqlbuf, "Select top 1 city,code from tbAreaCode where code=left('%s',len(code))", tel.c_str());
    else if (strDBType == "MYSQL")
      sprintf(sqlbuf, "Select city,code from tbAreaCode where code=left('%s',LENGTH(code)) limit 1", tel.c_str());
    try
    {
      adoqueryPub1->SQL->Clear();
      adoqueryPub1->SQL->Add((char *)sqlbuf);
      adoqueryPub1->Open();
    }
    catch ( ... )
    {
      return itemname;
    }
    if ( !adoqueryPub1->Eof )
    {
      itemname = adoqueryPub1->FieldByName("city")->AsString;
      code = adoqueryPub1->FieldByName("code")->AsString;
    }
    if (LocaAreaCode != "")
    {
      if (LocaAreaCode == tel.SubString(1,LocaAreaCode.Length()))
        tel = tel.SubString(LocaAreaCode.Length()+1,8);
    }
    adoqueryPub1->Close();
    return itemname;
  }
  else
  {
    return "本地市話";
  }
}
bool TdmAgent::IsLocaMobile(AnsiString strMobile)
{
  char sqlbuf[256];
  int result=0;

    if (strDBType == "SQLSERVER")
      sprintf(sqlbuf, "Select top 1 city from tbMobileList where code='%s' and num=left('%s',len(num))", LocaAreaCode.c_str(), strMobile.c_str());
    else if (strDBType == "MYSQL")
      sprintf(sqlbuf, "Select city from tbMobileList where code='%s' and num=left('%s',LENGTH(num)) limit 1", LocaAreaCode.c_str(), strMobile.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
bool TdmAgent::IsRemoteMobile(AnsiString strMobile)
{
  char sqlbuf[256];
  int result=0;

    if (strDBType == "SQLSERVER")
      sprintf(sqlbuf, "Select top 1 city from tbMobileList where code<>'%s' and num=left('%s',len(num))", LocaAreaCode.c_str(), strMobile.c_str());
    else if (strDBType == "MYSQL")
      sprintf(sqlbuf, "Select city from tbMobileList where code<>'%s' and num=left('%s',LENGTH(num)) limit 1", LocaAreaCode.c_str(), strMobile.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsBlackExist(AnsiString telecode)
{
  char sqlbuf[256];
  int result=0;

  sprintf(sqlbuf, "Select id from tbBlack where telecode='%s' and OpenFlag=1", telecode.c_str());
  try
  {
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();

    if ( !adoqueryPub1->Eof )
    {
      result = 1;
    }
    adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertBlack(AnsiString telecode, AnsiString remark, int flag)
{
  char sqlbuf[1024];

  if (telecode == "")
    return 1;
  if (strDBType == "SQLSERVER")
    sprintf( sqlbuf, "insert into tbBlack (TeleCode,OpenFlag,AddWorkerNo,EndTime,Remark) values ('%s',%d,'%s',%s,'%s')",
      telecode.c_str(), flag, MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str(), remark.c_str());
  else if (strDBType == "MYSQL")
    sprintf( sqlbuf, "insert into tbBlack (TeleCode,OpenFlag,AddWorkerNo,EndTime,Remark) values ('%s',%d,'%s',%s,'%s')",
      telecode.c_str(), flag, MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str(), remark.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQyrCallCdrCalcFields(TDataSet *DataSet)
{
  switch (adoQyrCallCdrCallInOut->AsInteger)
  {
  case 1:
    adoQyrCallCdrDispCallInOut->AsString = "撥入";
    break;
  case 2:
    adoQyrCallCdrDispCallInOut->AsString = "撥出";
    break;
  case 3:
    adoQyrCallCdrDispCallInOut->AsString = "內部";
    break;
  }
  switch (adoQyrCallCdrSeatAnsId->AsInteger)
  {
  case 0:
    adoQyrCallCdrDispSeatAnsId->AsString = "否";
    adoQyrCallCdrTalkTimeLen->AsInteger = 0;
    adoQyrCallCdrDispTalkTimeLen->AsString = "0秒";
    break;
  case 1:
    adoQyrCallCdrDispSeatAnsId->AsString = "是";
    try
    {
      adoQyrCallCdrTalkTimeLen->AsInteger = SecondsBetween(adoQyrCallCdrSeatAnsTime->AsDateTime, adoQyrCallCdrSeatRelTime->AsDateTime);
      adoQyrCallCdrDispTalkTimeLen->AsString = IntToStr(adoQyrCallCdrTalkTimeLen->AsInteger)+"秒"; //GetTimeString(adoQyrCallCdrTalkTimeLen->AsInteger));
    }
    catch (...)
    {
      adoQyrCallCdrTalkTimeLen->AsInteger = 0;
      adoQyrCallCdrDispTalkTimeLen->AsString = "0秒";
    }
    break;
  case 2:
    adoQyrCallCdrDispSeatAnsId->AsString = "否";
    adoQyrCallCdrTalkTimeLen->AsInteger = 0;
    adoQyrCallCdrDispTalkTimeLen->AsString = "0秒";
    break;
  case 3:
    adoQyrCallCdrDispSeatAnsId->AsString = "否";
    adoQyrCallCdrTalkTimeLen->AsInteger = 0;
    adoQyrCallCdrDispTalkTimeLen->AsString = "0秒";
    break;
  }
  try
  {
    adoQyrCallCdrDispTotalTimeLen->AsString = IntToStr(SecondsBetween(adoQyrCallCdrRelTime->AsDateTime, adoQyrCallCdrCallTime->AsDateTime))+"秒"; //GetTimeString(SecondsBetween(adoQyrCallCdrRelTime->AsDateTime, adoQyrCallCdrCallTime->AsDateTime));
  }
  catch (...)
  {
    adoQyrCallCdrDispTotalTimeLen->AsString = "0秒";
  }
  if (adoQyrCallCdrRecdFile->AsString != "")
    adoQyrCallCdrDispRecd->AsString = "是";
  else
    adoQyrCallCdrDispRecd->AsString = "否";
  adoQyrCallCdrTalkScoreName->AsString = TalkScoreNameList.GetItemNameEx(adoQyrCallCdrTalkScore->AsInteger);

  adoQyrCallCdrScoreName->AsString = ScoreNameList.GetItemNameEx(adoQyrCallCdrScore->AsInteger%10);
  adoQyrCallCdrDispScoreName2->AsString = ScoreNameList.GetItemNameEx(adoQyrCallCdrScore->AsInteger/10); //2021-12-24 中國信托新增滿意度

  adoQyrCallCdrGroupName->AsString = WorkerGroupList.GetItemNameEx(adoQyrCallCdrGroupNo->AsInteger);
  adoQyrCallCdrWorkerName->AsString = GetWorkerName(adoQyrCallCdrWorkerNo->AsString);
  adoQyrCallCdrDispProdType->AsString = ProdTypeItemList.GetItemNameEx(adoQyrCallCdrProdType->AsInteger);
  adoQyrCallCdrDispSrvType->AsString = SrvTypeItemList.GetItemNameEx(adoQyrCallCdrProdType->AsInteger, adoQyrCallCdrSrvType->AsInteger);
  adoQyrCallCdrDispSrvSubType->AsString = SrvSubTypeItemList.GetItemNameEx(adoQyrCallCdrProdType->AsInteger, adoQyrCallCdrSrvType->AsInteger, adoQyrCallCdrSrvSubType->AsInteger);

  switch (adoQyrCallCdrAnsId->AsInteger)
  {
  case 1:
    adoQyrCallCdrDispAnsId->AsString = "人工";
    break;
  case 2:
    adoQyrCallCdrDispAnsId->AsString = "IVR";
    break;
  default:
    adoQyrCallCdrDispAnsId->AsString = "其他";
    break;
  }
  adoQyrCallCdrDispCallerNo->AsString = GetHidePhoneNum(2, adoQyrCallCdrCallerNo->AsString);
}
//---------------------------------------------------------------------------

//寫操作日志
int TdmAgent::InsertOPLog(CMySeat myseat, CMyWorker myworker, int opcode, LPCTSTR lpszFormat, ...)
{
  char sqlbuf[2048];
  char buf[512];

  if (ADOConnection1->Connected == false)
    return 1;
  va_list		ArgList;
	va_start(ArgList, lpszFormat);
	_vsnprintf(buf, 512, lpszFormat, ArgList);
	va_end (ArgList);

  sprintf( sqlbuf, "insert into tbOPLog (SeatNo,WorkerNo,WorkerName,OPCode,Remark,OPTime) values ('%s','%s','%s',%d,'%s',%s)",
    MySeat.SeatNo.c_str(),MyWorker.WorkerNo.c_str(),MyWorker.WorkerName.c_str(),opcode,buf, MyNowFuncName.c_str());
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub1->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetWorkerRecord(TADOTable *adotable, CMyWorker &myworker)
{
  if (adotable->State == dsInactive || adotable->Eof)
    return 1;
  try
  {
    myworker.Id = adotable->FieldByName("Id")->AsInteger; //序列號（自動）
    myworker.WorkerNo = adotable->FieldByName("WorkerNo")->AsString; //話務員工號
    myworker.WorkType = adotable->FieldByName("WorkerType")->AsInteger; //職能級別
    myworker.DepartmentID = adotable->FieldByName("DepartmentID")->AsInteger; //部門編號
    myworker.GroupNo = adotable->FieldByName("GroupNo")->AsInteger; //技能組號
    myworker.GroupNo1 = adotable->FieldByName("GroupNo1")->AsInteger; //技能組號
    myworker.GroupNo2 = adotable->FieldByName("GroupNo2")->AsInteger; //技能組號
    myworker.GroupNo3 = adotable->FieldByName("GroupNo3")->AsInteger; //技能組號
    myworker.GroupNo4 = adotable->FieldByName("GroupNo4")->AsInteger; //技能組號
    myworker.GroupNo5 = adotable->FieldByName("GroupNo5")->AsInteger; //技能組號
    myworker.GroupNo6 = adotable->FieldByName("GroupNo6")->AsInteger; //技能組號
    myworker.GroupNo7 = adotable->FieldByName("GroupNo7")->AsInteger; //技能組號
    myworker.GroupNo8 = adotable->FieldByName("GroupNo8")->AsInteger; //技能組號
    myworker.GroupNo9 = adotable->FieldByName("GroupNo9")->AsInteger; //技能組號
    myworker.GroupNo10 = adotable->FieldByName("GroupNo10")->AsInteger; //技能組號
    myworker.GroupNo11 = adotable->FieldByName("GroupNo11")->AsInteger; //技能組號
    myworker.GroupNo12 = adotable->FieldByName("GroupNo12")->AsInteger; //技能組號
    myworker.GroupNo13 = adotable->FieldByName("GroupNo13")->AsInteger; //技能組號
    myworker.GroupNo14 = adotable->FieldByName("GroupNo14")->AsInteger; //技能組號
    myworker.GroupNo15 = adotable->FieldByName("GroupNo15")->AsInteger; //技能組號
    myworker.wLevel = adotable->FieldByName("SkillLevel")->AsInteger; //技能級別
    myworker.wLevel1 = adotable->FieldByName("SkillLevel1")->AsInteger; //技能級別
    myworker.wLevel2 = adotable->FieldByName("SkillLevel2")->AsInteger; //技能級別
    myworker.wLevel3 = adotable->FieldByName("SkillLevel3")->AsInteger; //技能級別
    myworker.wLevel4 = adotable->FieldByName("SkillLevel4")->AsInteger; //技能級別
    myworker.wLevel5 = adotable->FieldByName("SkillLevel5")->AsInteger; //技能級別
    myworker.wLevel6 = adotable->FieldByName("SkillLevel6")->AsInteger; //技能級別
    myworker.wLevel7 = adotable->FieldByName("SkillLevel7")->AsInteger; //技能級別
    myworker.wLevel8 = adotable->FieldByName("SkillLevel8")->AsInteger; //技能級別
    myworker.wLevel9 = adotable->FieldByName("SkillLevel9")->AsInteger; //技能級別
    myworker.wLevel10 = adotable->FieldByName("SkillLevel10")->AsInteger; //技能級別
    myworker.wLevel11 = adotable->FieldByName("SkillLevel11")->AsInteger; //技能級別
    myworker.wLevel12 = adotable->FieldByName("SkillLevel12")->AsInteger; //技能級別
    myworker.wLevel13 = adotable->FieldByName("SkillLevel13")->AsInteger; //技能級別
    myworker.wLevel14 = adotable->FieldByName("SkillLevel14")->AsInteger; //技能級別
    myworker.wLevel15 = adotable->FieldByName("SkillLevel15")->AsInteger; //技能級別
    myworker.CallRight = adotable->FieldByName("CallRight")->AsInteger; //呼出權限
    myworker.Password = adotable->FieldByName("Password")->AsString; //登錄密碼
    myworker.WorkerName = adotable->FieldByName("WorkerName")->AsString; //話務員姓名
    myworker.Tele = adotable->FieldByName("Tele")->AsString; //聯系電話
    myworker.Mobile = adotable->FieldByName("Mobile")->AsString; //手機號碼
    myworker.Address = adotable->FieldByName("Address")->AsString; //住址
    myworker.Email = adotable->FieldByName("Email")->AsString; //郵箱
    myworker.SubPhone = adotable->FieldByName("SubPhone")->AsString; //分機
    myworker.EmployeeID = adotable->FieldByName("EmployeeID")->AsString;
    myworker.HideCallerNo = adotable->FieldByName("HideCallerNo")->AsInteger;
    myworker.AutoAnswerId = adotable->FieldByName("AutoAnswerId")->AsInteger;
    myworker.ProcDialoutNextType = adotable->FieldByName("ProcDialoutNextType")->AsInteger;
    myworker.SWTGroupID = adotable->FieldByName("SWTGroupID")->AsString;
    myworker.SWTGroupPWD = adotable->FieldByName("SWTGroupPWD")->AsString;
    //myworker.PWDModifyFlag = adotable->FieldByName("PWDModifyFlag")->AsInteger;
    //myworker.PWDModifyDate = adotable->FieldByName("PWDModifyDate")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertWorker(CMyWorker &myworker)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbWorker (\
WorkerNo,\
WorkerType,\
DepartmentID,\
GroupNo,\
GroupNo1,\
GroupNo2,\
GroupNo3,\
GroupNo4,\
GroupNo5,\
GroupNo6,\
GroupNo7,\
GroupNo8,\
GroupNo9,\
GroupNo10,\
GroupNo11,\
GroupNo12,\
GroupNo13,\
GroupNo14,\
GroupNo15,\
SkillLevel,\
SkillLevel1,\
SkillLevel2,\
SkillLevel3,\
SkillLevel4,\
SkillLevel5,\
SkillLevel6,\
SkillLevel7,\
SkillLevel8,\
SkillLevel9,\
SkillLevel10,\
SkillLevel11,\
SkillLevel12,\
SkillLevel13,\
SkillLevel14,\
SkillLevel15,\
CallRight,\
Password,\
WorkerName,\
Tele,\
Mobile,\
Address,\
SubPhone,\
EmployeeID,\
Email,\
HideCallerNo,\
AutoAnswerId,\
ProcDialoutNextType,\
SWTGroupID,\
SWTGroupPWD,\
PWDModifyFlag,\
PWDModifyDate\
) values ('%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s',%d,%d,%d,'%s','%s',0,getdate())",
      myworker.WorkerNo.c_str(),
      myworker.WorkType,
      myworker.DepartmentID,
      myworker.GroupNo,
      myworker.GroupNo1,
      myworker.GroupNo2,
      myworker.GroupNo3,
      myworker.GroupNo4,
      myworker.GroupNo5,
      myworker.GroupNo6,
      myworker.GroupNo7,
      myworker.GroupNo8,
      myworker.GroupNo9,
      myworker.GroupNo10,
      myworker.GroupNo11,
      myworker.GroupNo12,
      myworker.GroupNo13,
      myworker.GroupNo14,
      myworker.GroupNo15,
      myworker.wLevel,
      myworker.wLevel1,
      myworker.wLevel2,
      myworker.wLevel3,
      myworker.wLevel4,
      myworker.wLevel5,
      myworker.wLevel6,
      myworker.wLevel7,
      myworker.wLevel8,
      myworker.wLevel9,
      myworker.wLevel10,
      myworker.wLevel11,
      myworker.wLevel12,
      myworker.wLevel13,
      myworker.wLevel14,
      myworker.wLevel15,
      myworker.CallRight,
      myworker.Password.c_str(),
      myworker.WorkerName.c_str(),
      myworker.Tele.c_str(),
      myworker.Mobile.c_str(),
      myworker.Address.c_str(),
      myworker.SubPhone.c_str(),
      myworker.EmployeeID.c_str(),
      myworker.Email.c_str(),
      myworker.HideCallerNo,
      myworker.AutoAnswerId,
      myworker.ProcDialoutNextType,
      myworker.SWTGroupID.c_str(),
      myworker.SWTGroupPWD.c_str()
      );

  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateWorker(CMyWorker &myworker, bool bPwdModify)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbWorker set \
WorkerNo='%s',\
WorkerType=%d,\
DepartmentID=%d,\
GroupNo=%d,\
GroupNo1=%d,\
GroupNo2=%d,\
GroupNo3=%d,\
GroupNo4=%d,\
GroupNo5=%d,\
GroupNo6=%d,\
GroupNo7=%d,\
GroupNo8=%d,\
GroupNo9=%d,\
GroupNo10=%d,\
GroupNo11=%d,\
GroupNo12=%d,\
GroupNo13=%d,\
GroupNo14=%d,\
GroupNo15=%d,\
SkillLevel=%d,\
SkillLevel1=%d,\
SkillLevel2=%d,\
SkillLevel3=%d,\
SkillLevel4=%d,\
SkillLevel5=%d,\
SkillLevel6=%d,\
SkillLevel7=%d,\
SkillLevel8=%d,\
SkillLevel9=%d,\
SkillLevel10=%d,\
SkillLevel11=%d,\
SkillLevel12=%d,\
SkillLevel13=%d,\
SkillLevel14=%d,\
SkillLevel15=%d,\
CallRight=%d,\
Password='%s',\
WorkerName='%s',\
Tele='%s',\
Mobile='%s',\
Address='%s',\
SubPhone='%s',\
EmployeeID='%s',\
Email='%s',\
HideCallerNo=%d,\
AutoAnswerId=%d,\
ProcDialoutNextType=%d,\
SWTGroupID='%s',\
SWTGroupPWD='%s',\
PWDModifyFlag=%d,\
PWDModifyDate=getdate() \
where id=%d",
      myworker.WorkerNo.c_str(),
      myworker.WorkType,
      myworker.DepartmentID,
      myworker.GroupNo,
      myworker.GroupNo1,
      myworker.GroupNo2,
      myworker.GroupNo3,
      myworker.GroupNo4,
      myworker.GroupNo5,
      myworker.GroupNo6,
      myworker.GroupNo7,
      myworker.GroupNo8,
      myworker.GroupNo9,
      myworker.GroupNo10,
      myworker.GroupNo11,
      myworker.GroupNo12,
      myworker.GroupNo13,
      myworker.GroupNo14,
      myworker.GroupNo15,
      myworker.wLevel,
      myworker.wLevel1,
      myworker.wLevel2,
      myworker.wLevel3,
      myworker.wLevel4,
      myworker.wLevel5,
      myworker.wLevel6,
      myworker.wLevel7,
      myworker.wLevel8,
      myworker.wLevel9,
      myworker.wLevel10,
      myworker.wLevel11,
      myworker.wLevel12,
      myworker.wLevel13,
      myworker.wLevel14,
      myworker.wLevel15,
      myworker.CallRight,
      myworker.Password.c_str(),
      myworker.WorkerName.c_str(),
      myworker.Tele.c_str(),
      myworker.Mobile.c_str(),
      myworker.Address.c_str(),
      myworker.SubPhone.c_str(),
      myworker.EmployeeID.c_str(),
      myworker.Email.c_str(),
      myworker.HideCallerNo,
      myworker.AutoAnswerId,
      myworker.ProcDialoutNextType,
      myworker.SWTGroupID.c_str(),
      myworker.SWTGroupPWD.c_str(),
      bPwdModify==true?1:0,
      myworker.Id);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}

int TdmAgent::IsWorkerNoExist(AnsiString workerno)
{
  char sqlbuf[256];
  int result=0;

  sprintf(sqlbuf, "Select id from tbWorker where WorkerNo='%s'", workerno.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->Open();

    if ( !dmAgent->adoqueryPub->Eof )
    {
      result = 1;
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
void TdmAgent::DeleteWorkerNo(AnsiString workerno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbWorker where WorkerNo='%s'", workerno.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::GetWorkerNo(AnsiString strWorkerName, AnsiString &strWorkerNo)
{
  char sqlbuf[128];
  bool nResult=0;

  sprintf(sqlbuf, "Select WorkerNo from tbWorker where WorkerName='%s'", strWorkerName.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strWorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::GetWorkerName(AnsiString strWorkerNo, AnsiString &strWorkerName)
{
  char sqlbuf[128];
  bool nResult=0;

  strWorkerName = strWorkerNo;
  sprintf(sqlbuf, "Select WorkerName from tbWorker where WorkerNo='%s'", strWorkerNo.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strWorkerName = adoqueryPub->FieldByName("WorkerName")->AsString;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
AnsiString TdmAgent::GetWorkerName(AnsiString strWorkerNo)
{
  char sqlbuf[128];
  AnsiString strWorkerName="";

  if (strWorkerNo == "")
    return strWorkerName;
  sprintf(sqlbuf, "Select WorkerName from tbWorker where WorkerNo='%s'", strWorkerNo.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strWorkerName = adoqueryPub->FieldByName("WorkerName")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return strWorkerName;
}
AnsiString TdmAgent::GetGroupNameOrWorkerName(int nGroupNo)
{
  AnsiString strTemp;
  if (nGroupNo < 256)
  {
    strTemp = WorkerGroupList.GetItemName(nGroupNo);
  }
  else if (nGroupNo == 256)
  {
    strTemp = "留言建議";
  }
  else if (nGroupNo == 257)
  {
    strTemp = "IVR提示語音";
  }
  else if (nGroupNo == 258)
  {
    strTemp = "接收傳真";
  }
  else
  {
    GetWorkerName(nGroupNo, strTemp);
  }
  return strTemp;
}
int TdmAgent::GetGroupNoByName(AnsiString strName, AnsiString &strGroupNoTel, int &GroupNoType)
{
  int nTemp;
  AnsiString strWorkerNo;

  GroupNoType = 0;
  if (strName == "")
  {
    strGroupNoTel = "0";
    return 0;
  }

  //轉接的是否為特殊功能
  if (strName == "留言建議")
  {
    strGroupNoTel = 256;
    return 32;
  }
  if (strName == "IVR提示語音")
  {
    strGroupNoTel = 257;
    return 33;
  }
  if (strName == "接收傳真")
  {
    strGroupNoTel = 258;
    return 34;
  }
  if (strName == "電話會議")
  {
    strGroupNoTel = 259;
    return 35;
  }
  if (strName == "索取傳真")
  {
    strGroupNoTel = 260;
    return 36;
  }
  if (strName == "定制IVR子選單")
  {
    strGroupNoTel = 261;
    return 37;
  }
  if (strName == "呼入轉呼出")
  {
    strGroupNoTel = 262;
    return 38;
  }

  //轉接的是否為話務組
  nTemp = WorkerGroupList.GetItemValue(strName);
  if (nTemp > 0)
  {
    strGroupNoTel = nTemp;
    return nTemp;
  }
  //轉接的是否為話務員
  if (GetWorkerNo(strName, strWorkerNo) == 1)
  {
    strGroupNoTel = strWorkerNo;
    nTemp = StrToInt(strWorkerNo);
    return nTemp;
  }
  //是否為有效的外線號碼
  if (MyIsDigits(strName) == 0)
  {
    if (strName.Length() > 6)
    {
      strGroupNoTel = strName;
      return -1;
    }
    else
    {
      GroupNoType = 1;
      strGroupNoTel = strName;
      nTemp = StrToInt(strName);
      return nTemp;
    }
  }
  else
  {
    strGroupNoTel = "0";
    return 0;
  }
}
int TdmAgent::GetWorkerPsw(AnsiString workerno, AnsiString &strPassword, int &nWorkerType)
{
  char sqlbuf[128];
  bool nResult=0;

  sprintf(sqlbuf, "Select Password,WorkerType from tbWorker where WorkerNo='%s'", workerno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strPassword = adoqueryPub->FieldByName("Password")->AsString;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::UpdateWorkerPsw(AnsiString workerno, AnsiString strPassword)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbWorker set Password='%s',PWDModifyFlag=1,PWDModifyDate=getdate() where WorkerNo='%s'", strPassword.c_str(), workerno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
//---------------------------------------------------------------------------

void TdmAgent::RefreshZSKMain()
{
  adotbZSKMain->Close();
  adotbZSKMain->Open();
  FormMain->lblZSKMainRecordCount->Caption = adotbZSKMain->RecordCount;
  adotbZSKSub->Close();
  adotbZSKSub->Open();
  FormMain->lblZSKSubRecordCount->Caption = adotbZSKSub->RecordCount;
  adotbZSKSubject->Close();
  adotbZSKSubject->Open();
  FormMain->lblZSKSubjectRecordCount->Caption = adotbZSKSubject->RecordCount;
}
void TdmAgent::RefreshZSKSub()
{
  adotbZSKSub->Close();
  adotbZSKSub->Open();
  FormMain->lblZSKSubRecordCount->Caption = adotbZSKSub->RecordCount;
  adotbZSKSubject->Close();
  adotbZSKSubject->Open();
  FormMain->lblZSKSubjectRecordCount->Caption = adotbZSKSubject->RecordCount;
}
void TdmAgent::RefreshZSKSubject()
{
  adotbZSKSubject->Close();
  adotbZSKSubject->Open();
  FormMain->lblZSKSubjectRecordCount->Caption = adotbZSKSubject->RecordCount;
}

//---------------------------------------------------------------------------
int TdmAgent::GetZSKMainRecord(TADOTable *adotable, CZSKMain& zskmain)
{
  if (adotable->State == dsInactive || adotable->Eof)
    return 1;
  try
  {
      zskmain.MainNo = adotable->FieldByName("MainNo")->AsInteger;
      zskmain.MainTitle = adotable->FieldByName("MainTitle")->AsString;
      zskmain.Explain = adotable->FieldByName("MainExplain")->AsString;
      zskmain.IssueStatus = adotable->FieldByName("IssueStatus")->AsInteger;
      zskmain.IssueDate = adotable->FieldByName("IssueDate")->AsString;
      zskmain.IssueMan = adotable->FieldByName("IssueMan")->AsString;
      zskmain.AccessRight = adotable->FieldByName("AccessRight")->AsInteger;
      zskmain.CheckTime = adotable->FieldByName("CheckTime")->AsString;
      zskmain.CheckMan = adotable->FieldByName("CheckMan")->AsString;
      zskmain.orderno1 = adotable->FieldByName("orderno1")->AsInteger;
      zskmain.Remark = adotable->FieldByName("Remark")->AsString;
      return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetZSKSubRecord(TADOTable *adotable, CZSKSub& zsksub)
{
  if (adotable->State == dsInactive || adotable->Eof)
    return 1;
  try
  {
      zsksub.SubNo = adotable->FieldByName("SubNo")->AsInteger;
      zsksub.MainNo = adotable->FieldByName("MainNo")->AsInteger;
      zsksub.MainTitle = adotbZSKMain->FieldByName("MainTitle")->AsString;
      zsksub.SubTitle = adotable->FieldByName("SubTitle")->AsString;
      zsksub.Explain = adotable->FieldByName("SubExplain")->AsString;
      zsksub.IssueStatus = adotable->FieldByName("IssueStatus")->AsInteger;
      zsksub.IssueDate = adotable->FieldByName("IssueDate")->AsString;
      zsksub.IssueMan = adotable->FieldByName("IssueMan")->AsString;
      zsksub.AccessRight = adotable->FieldByName("AccessRight")->AsInteger;
      zsksub.CheckTime = adotable->FieldByName("CheckTime")->AsString;
      zsksub.CheckMan = adotable->FieldByName("CheckMan")->AsString;
      zsksub.orderno2 = adotable->FieldByName("orderno2")->AsInteger;
      zsksub.Remark = adotable->FieldByName("Remark")->AsString;
      return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetZSKSubjectRecord(TADOTable *adotable, CZSKSubject& zsksubject)
{
  if (adotable->State == dsInactive || adotable->Eof)
    return 1;
  try
  {
      zsksubject.SubjectNo = adotable->FieldByName("SubjectNo")->AsInteger;
      zsksubject.Gid = adotable->FieldByName("Gid")->AsString;
      zsksubject.MainNo = adotable->FieldByName("MainNo")->AsInteger;
      zsksubject.SubNo = adotable->FieldByName("SubNo")->AsInteger;
      zsksubject.MainTitle = adotbZSKMain->FieldByName("MainTitle")->AsString;
      zsksubject.SubTitle = adotbZSKSub->FieldByName("SubTitle")->AsString;
      zsksubject.SubjectTitle = adotable->FieldByName("SubjectTitle")->AsString;
      zsksubject.ExplainCont = adotable->FieldByName("ExplainCont")->AsString;
      zsksubject.ExplainImage = adotable->FieldByName("ImageFileName")->AsString;
      zsksubject.ExplainFile = adotable->FieldByName("ExplainFileName")->AsString;
      zsksubject.IssueStatus = adotable->FieldByName("IssueStatus")->AsInteger;
      zsksubject.IssueDate = adotable->FieldByName("IssueDate")->AsString;
      zsksubject.IssueMan = adotable->FieldByName("IssueMan")->AsString;
      zsksubject.AccessRight = adotable->FieldByName("AccessRight")->AsInteger;
      zsksubject.CheckTime = adotable->FieldByName("CheckTime")->AsString;
      zsksubject.CheckMan = adotable->FieldByName("CheckMan")->AsString;
      zsksubject.VersionNo = adotable->FieldByName("VersionNo")->AsString;
      zsksubject.SecrecyId = adotable->FieldByName("SecrecyId")->AsInteger;
      zsksubject.DepartmentId = adotable->FieldByName("DepartmentId")->AsInteger;
      zsksubject.orderno3 = adotable->FieldByName("orderno3")->AsInteger;
      zsksubject.Remark = adotable->FieldByName("Remark")->AsString;
      return 0;
  }
  catch (...)
  {
    return 1;
  }
}
//---------------------------------------------------------------------------
int TdmAgent::GetZSKMainRecord(TADOQuery *adoquery, CZSKMain *zskmain)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
      zskmain->MainNo = adoquery->FieldByName("MainNo")->AsInteger;
      zskmain->MainTitle = adoquery->FieldByName("MainTitle")->AsString;
      zskmain->Explain = adoquery->FieldByName("MainExplain")->AsString;
      zskmain->IssueStatus = adoquery->FieldByName("IssueStatus")->AsInteger;
      zskmain->IssueDate = adoquery->FieldByName("IssueDate")->AsString;
      zskmain->IssueMan = adoquery->FieldByName("IssueMan")->AsString;
      zskmain->AccessRight = adoquery->FieldByName("AccessRight")->AsInteger;
      zskmain->CheckTime = adoquery->FieldByName("CheckTime")->AsString;
      zskmain->CheckMan = adoquery->FieldByName("CheckMan")->AsString;
      zskmain->orderno1 = adoquery->FieldByName("orderno1")->AsInteger;
      zskmain->Remark = adoquery->FieldByName("Remark")->AsString;
      return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetZSKSubRecord(TADOQuery *adoquery, CZSKSub *zsksub)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
      zsksub->SubNo = adoquery->FieldByName("SubNo")->AsInteger;
      zsksub->MainNo = adoquery->FieldByName("MainNo")->AsInteger;
      zsksub->MainTitle = adoqueryPub->FieldByName("MainTitle")->AsString;
      zsksub->SubTitle = adoquery->FieldByName("SubTitle")->AsString;
      zsksub->Explain = adoquery->FieldByName("SubExplain")->AsString;
      zsksub->IssueStatus = adoquery->FieldByName("IssueStatus")->AsInteger;
      zsksub->IssueDate = adoquery->FieldByName("IssueDate")->AsString;
      zsksub->IssueMan = adoquery->FieldByName("IssueMan")->AsString;
      zsksub->AccessRight = adoquery->FieldByName("AccessRight")->AsInteger;
      zsksub->CheckTime = adoquery->FieldByName("CheckTime")->AsString;
      zsksub->CheckMan = adoquery->FieldByName("CheckMan")->AsString;
      zsksub->orderno2 = adoquery->FieldByName("orderno2")->AsInteger;
      zsksub->Remark = adoquery->FieldByName("Remark")->AsString;
      return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetZSKSubjectRecord(TADOQuery *adoquery, CZSKSubject *zsksubject)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
      zsksubject->SubjectNo = adoquery->FieldByName("SubjectNo")->AsInteger;
      zsksubject->Gid = adoquery->FieldByName("Gid")->AsString;
      zsksubject->MainNo = adoquery->FieldByName("MainNo")->AsInteger;
      zsksubject->SubNo = adoquery->FieldByName("SubNo")->AsInteger;
      zsksubject->MainTitle = adoqueryPub->FieldByName("MainTitle")->AsString;
      zsksubject->SubTitle = adoqueryPub1->FieldByName("SubTitle")->AsString;
      zsksubject->SubjectTitle = adoquery->FieldByName("SubjectTitle")->AsString;
      zsksubject->ExplainCont = adoquery->FieldByName("ExplainCont")->AsString;
      zsksubject->ExplainImage = adoquery->FieldByName("ImageFileName")->AsString;
      zsksubject->ExplainFile = adoquery->FieldByName("ExplainFileName")->AsString;
      zsksubject->IssueStatus = adoquery->FieldByName("IssueStatus")->AsInteger;
      zsksubject->IssueDate = adoquery->FieldByName("IssueDate")->AsString;
      zsksubject->IssueMan = adoquery->FieldByName("IssueMan")->AsString;
      zsksubject->AccessRight = adoquery->FieldByName("AccessRight")->AsInteger;
      zsksubject->CheckTime = adoquery->FieldByName("CheckTime")->AsString;
      zsksubject->CheckMan = adoquery->FieldByName("CheckMan")->AsString;
      zsksubject->VersionNo = adoquery->FieldByName("VersionNo")->AsString;
      zsksubject->SecrecyId = adoquery->FieldByName("SecrecyId")->AsInteger;
      zsksubject->DepartmentId = adoquery->FieldByName("DepartmentId")->AsInteger;
      zsksubject->orderno3 = adoquery->FieldByName("orderno3")->AsInteger;
      zsksubject->Remark = adoquery->FieldByName("Remark")->AsString;
      return 0;
  }
  catch (...)
  {
    return 1;
  }
}
void TdmAgent::GetZSKAppendixFileList(CZSKSubject *zsksubject, TComboBox *combobox)
{
    char sqlbuf[256];
    AnsiString strId, strFileName, itemname;

    if (ADOConnection1->Connected == false)
      return;

    combobox->Items->Clear();

    if (zsksubject->ExplainImage != "")
    {
      strFileName = "[1-"+IntToStr(zsksubject->SubjectNo)+"]->"+zsksubject->ExplainImage;
      combobox->Items->Add(strFileName);
    }
    if (zsksubject->ExplainFile != "")
    {
      strFileName = "[2-"+IntToStr(zsksubject->SubjectNo)+"]->"+zsksubject->ExplainFile;
      combobox->Items->Add(strFileName);
    }

    sprintf(sqlbuf, "Select Id,AppendixFileName from tbZSKAppendix where ZSKGid='%s' order by id",
      zsksubject->Gid.c_str());
    try
    {
        adoqueryPub1->SQL->Clear();
        adoqueryPub1->SQL->Add((char *)sqlbuf);
        adoqueryPub1->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub1->Eof )
    {
        strId = adoqueryPub1->FieldByName("Id")->AsString;
        strFileName = adoqueryPub1->FieldByName("AppendixFileName")->AsString;
        itemname = "[3-"+strId+"]->"+strFileName;
        combobox->Items->Add(itemname);
        adoqueryPub1->Next();
    }
    adoqueryPub1->Close();
}
void TdmAgent::GetZSKAppendixFileList(CZSKSubject *zsksubject, TListView *listview)
{
    char sqlbuf[256];
    AnsiString strId, strFileName, itemname;
    TListItem  *ListItem1;

    if (ADOConnection1->Connected == false)
      return;

    listview->Items->Clear();

    if (zsksubject->ExplainImage != "")
    {
      ListItem1 = listview->Items->Add();
      ListItem1->Caption = "1-"+IntToStr(zsksubject->SubjectNo);
      ListItem1->SubItems->Insert(0, zsksubject->ExplainImage);
      ListItem1->SubItems->Insert(1, "");
    }
    if (zsksubject->ExplainFile != "")
    {
      ListItem1 = listview->Items->Add();
      ListItem1->Caption = "2-"+IntToStr(zsksubject->SubjectNo);
      ListItem1->SubItems->Insert(0, zsksubject->ExplainFile);
      ListItem1->SubItems->Insert(1, "");
    }

    sprintf(sqlbuf, "Select Id,AppendixFileName from tbZSKAppendix where ZSKGid='%s' order by id",
      zsksubject->Gid.c_str());
    try
    {
        adoqueryPub1->SQL->Clear();
        adoqueryPub1->SQL->Add((char *)sqlbuf);
        adoqueryPub1->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub1->Eof )
    {
        strId = adoqueryPub1->FieldByName("Id")->AsString;
        strFileName = adoqueryPub1->FieldByName("AppendixFileName")->AsString;
        ListItem1 = listview->Items->Add();
        ListItem1->Caption = "3-"+strId;
        ListItem1->SubItems->Insert(0, strFileName);
        ListItem1->SubItems->Insert(1, "");
        adoqueryPub1->Next();
    }
    adoqueryPub1->Close();
}
//---------------------------------------------------------------------------
int TdmAgent::InsertZSKMainRecord(CZSKMain &zskmain)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbZSKMain (\
MainTitle,\
MainExplain,\
IssueStatus,\
IssueMan,\
CheckMan,\
IssueDate,\
updatetime1,\
updateman1,\
orderno1,\
Remark\
) values ('%s','%s',%d,'%s','%s',%s,%s,'%s',%d,'%s')",
      zskmain.MainTitle.c_str(),
      zskmain.Explain.c_str(),
      zskmain.IssueStatus,
      MyWorker.WorkerNo.c_str(),
      MyWorker.WorkerNo.c_str(),
      MyNowFuncName.c_str(),
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      zskmain.orderno1,
      zskmain.Remark.c_str());

  try
  {
    //FormMain->memoAcceptCont->Lines->Add((char *)sqlbuf);
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertZSKSubRecord(CZSKSub& zsksub)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbZSKSub (\
MainNo,\
SubTitle,\
SubExplain,\
IssueStatus,\
IssueMan,\
CheckMan,\
IssueDate,\
updatetime2,\
updateman2,\
orderno2,\
Remark\
) values (%d,'%s','%s',%d,'%s','%s',%s,%s,'%s',%d,'%s')",
      zsksub.MainNo,
      zsksub.SubTitle.c_str(),
      zsksub.Explain.c_str(),
      zsksub.IssueStatus,
      MyWorker.WorkerNo.c_str(),
      MyWorker.WorkerNo.c_str(),
      MyNowFuncName.c_str(),
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      zsksub.orderno2,
      zsksub.Remark.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertZSKSubjectRecord(CZSKSubject& zsksubject)
{
  char sqlbuf[4096];
  sprintf( sqlbuf, "insert into tbZSKSubject (\
Gid,\
MainNo,\
SubNo,\
SubjectTitle,\
ExplainCont,\
ImageFileName,\
ExplainFileName,\
IssueStatus,\
IssueMan,\
CheckMan,\
VersionNo,\
SecrecyId,\
DepartmentId,\
IssueDate,\
updatetime3,\
updateman3,\
orderno3,\
Remark\
) values ('%s',%d,%d,'%s','%s','%s','%s',%d,'%s','%s','%s',%d,%d,%s,%s,'%s',%d,'%s')",
      zsksubject.Gid.c_str(),
      zsksubject.MainNo,
      zsksubject.SubNo,
      zsksubject.SubjectTitle.c_str(),
      zsksubject.ExplainCont.c_str(),
      zsksubject.ExplainImage.c_str(),
      zsksubject.ExplainFile.c_str(),
      zsksubject.IssueStatus,
      MyWorker.WorkerNo.c_str(),
      MyWorker.WorkerNo.c_str(),
      zsksubject.VersionNo.c_str(),
      zsksubject.SecrecyId,
      zsksubject.DepartmentId,
      MyNowFuncName.c_str(),
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      zsksubject.orderno3,
      zsksubject.Remark.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
//---------------------------------------------------------------------------
int TdmAgent::UpdateZSKMainRecord(CZSKMain& zskmain)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbZSKMain set \
MainTitle='%s',\
MainExplain='%s',\
IssueStatus=%d,\
CheckMan='%s',\
updatetime1=%s,\
updateman1=%s,\
orderno1=%d,\
Remark='%s' \
where MainNo=%d",
      zskmain.MainTitle.c_str(),
      zskmain.Explain.c_str(),
      zskmain.IssueStatus,
      MyWorker.WorkerNo.c_str(),
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      zskmain.orderno1,
      zskmain.Remark.c_str(),
      zskmain.MainNo);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateZSKSubRecord(CZSKSub& zsksub)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbZSKSub set \
MainNo=%d,\
SubTitle='%s',\
SubExplain='%s',\
IssueStatus=%d,\
CheckMan='%s',\
updatetime2=%s,\
updateman2=%s,\
orderno2=%d,\
Remark='%s' \
where SubNo=%d",
      zsksub.MainNo,
      zsksub.SubTitle.c_str(),
      zsksub.Explain.c_str(),
      zsksub.IssueStatus,
      MyWorker.WorkerNo.c_str(),
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      zsksub.orderno2,
      zsksub.Remark.c_str(),
      zsksub.SubNo);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateZSKSubjectRecord(CZSKSubject& zsksubject)
{
  char sqlbuf[4096];
  sprintf( sqlbuf, "update tbZSKSubject set \
MainNo=%d,\
SubNo=%d,\
SubjectTitle='%s',\
ExplainCont='%s',\
ImageFileName='%s',\
ExplainFileName='%s',\
IssueStatus=%d,\
IssueMan='%s',\
AccessRight=%d,\
CheckTime=%s,\
CheckMan='%s',\
VersionNo='%s',\
SecrecyId=%d,\
DepartmentId=%d,\
updatetime3=%s,\
updateman3=%s,\
orderno3=%d,\
Remark='%s' \
where subjectNo=%d",
      zsksubject.MainNo,
      zsksubject.SubNo,
      zsksubject.SubjectTitle.c_str(),
      zsksubject.ExplainCont.c_str(),
      zsksubject.ExplainImage.c_str(),
      zsksubject.ExplainFile.c_str(),
      zsksubject.IssueStatus,
      zsksubject.IssueMan.c_str(),
      zsksubject.AccessRight,
      MyNowFuncName.c_str(),
      zsksubject.CheckMan.c_str(),
      zsksubject.VersionNo.c_str(),
      zsksubject.SecrecyId,
      zsksubject.DepartmentId,
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      zsksubject.orderno3,
      zsksubject.Remark.c_str(),
      zsksubject.SubjectNo);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("UpdateZSKSubjectRecord sql=%s", sqlbuf);
    WriteErrprMsg("UpdateZSKSubjectRecord error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::UpdateZSKSubjectExplain(int subjectno, const char *pszFieldName, const char *pszFileName)
{
  char sqlbuf[256];
  TMemoryStream *pMemStream;

  sprintf(sqlbuf, "update tbZSKSubject set %s=:MyblobData where SubjectNo=%d", pszFieldName, subjectno);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    pMemStream = new TMemoryStream();

    try
    {
      pMemStream->LoadFromFile((char *)pszFileName);
      pMemStream->Position = 0;
      adoqueryPub->Parameters->ParamByName("MyblobData")->DataType = ftBlob;
      adoqueryPub->Parameters->ParamByName("MyblobData")->LoadFromStream(pMemStream, ftBlob);
      adoqueryPub->ExecSQL();
    }
    __finally
    {
      delete pMemStream;
    }

    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("UpdateZSKSubjectExplain error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::UpdateZSKSubjectExplain(AnsiString gid, const char *pszFieldName, const char *pszFileName)
{
  char sqlbuf[256];
  TMemoryStream *pMemStream;

  sprintf(sqlbuf, "update tbZSKSubject set %s=:MyblobData where gid='%s'", pszFieldName, gid.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    pMemStream = new TMemoryStream();

    try
    {
      pMemStream->LoadFromFile((char *)pszFileName);
      pMemStream->Position = 0;
      adoqueryPub->Parameters->ParamByName("MyblobData")->DataType = ftBlob;
      adoqueryPub->Parameters->ParamByName("MyblobData")->LoadFromStream(pMemStream, ftBlob);
      adoqueryPub->ExecSQL();
    }
    __finally
    {
      delete pMemStream;
    }

    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("UpdateZSKSubjectExplain2 error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::InsertZSKAppendix(AnsiString gid, const char *pszPathName, const char *pszFileName)
{
  char sqlbuf[512], szPathFileName[256];
  TMemoryStream *pMemStream;

  sprintf(sqlbuf, "insert into tbZSKAppendix (Gid,ZSKGid,AppendixFileName,Appendix) values ('%s','%s','%s',:MyblobData)",
    MyGetGIUD().c_str(), gid.c_str(), pszFileName);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    pMemStream = new TMemoryStream();

    try
    {
      sprintf(szPathFileName, "%s\\%s", pszPathName, pszFileName);
      pMemStream->LoadFromFile((char *)szPathFileName);
      pMemStream->Position = 0;
      adoqueryPub->Parameters->ParamByName("MyblobData")->DataType = ftBlob;
      adoqueryPub->Parameters->ParamByName("MyblobData")->LoadFromStream(pMemStream, ftBlob);
      adoqueryPub->ExecSQL();
    }
    __finally
    {
      delete pMemStream;
    }

    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("UpdateZSKSubjectExplain2 error=%s", exception.Message.c_str());
    return 1;
  }
}
void TdmAgent::IncClickRate(int subjectno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbZSKSubject set QueryCount=QueryCount+1 where SubjectNo=%d", subjectno);
  try
  {
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf);
    adoqueryPub1->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//---------------------------------------------------------------------------
void TdmAgent::DeleteZSKMainRecord(int mainno, bool isdelall)
{
  char sqlbuf1[256];
  char sqlbuf2[256];
  char sqlbuf3[256];

  sprintf(sqlbuf1, "delete from tbZSKMain where MainNo=%d", mainno);
  if (isdelall)
  {
    sprintf(sqlbuf2, "delete from tbZSKSub where MainNo=%d", mainno);
    sprintf(sqlbuf3, "delete from tbZSKSubject where MainNo=%d", mainno);
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf1);
    adoqueryPub->ExecSQL();
    if (isdelall)
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf2);
      adoqueryPub->ExecSQL();

      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf3);
      adoqueryPub->ExecSQL();
    }
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteZSKSubRecord(int subno, bool isdelall)
{
  char sqlbuf1[256];
  char sqlbuf2[256];

  sprintf(sqlbuf1, "delete from tbZSKSub where SubNo=%d", subno);
  if (isdelall)
  {
    sprintf(sqlbuf2, "delete from tbZSKSubject where SubNo=%d", subno);
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf1);
    adoqueryPub->ExecSQL();
    if (isdelall)
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf2);
      adoqueryPub->ExecSQL();
    }
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteZSKSubjectRecord(int subjectno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbZSKSubject where SubjectNo=%d", subjectno);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteZSKAppendix(AnsiString gid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbZSKAppendix where ZSKGid='%s'", gid.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//---------------------------------------------------------------------------

int TdmAgent::WriteSendSms(AnsiString mobileno, AnsiString sms, int smstype, int priority)
{
  char sqlbuf[512];

  if (sms == "")
    return 3;
  if ((mobileno.Length() == 11 && (mobileno.SubString(1,2) == "13" || mobileno.SubString(1,2) == "15" || mobileno.SubString(1,2) == "18"))
    || (mobileno.Length() == 12 && (mobileno.SubString(1,3) == "013" || mobileno.SubString(1,3) == "015" || mobileno.SubString(1,3) == "018")))
  {
    sprintf( sqlbuf, "insert into tbSendSms (SmsSrvType,Mobile,SmsMsg,AcceptBy,Priority,PreSendTime) values (%d,'%s','%s','%s',%d,%s)",
      smstype, mobileno.c_str(), sms.c_str(), MyWorker.WorkerNo.c_str(), priority, MyNowFuncName.c_str());
    try
    {
      dmAgent->adoqueryPub->SQL->Clear();
      dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
      dmAgent->adoqueryPub->ExecSQL();
      return 0;
    }
    catch ( ... )
    {
      return 1;
    }
  }
  return 2;
}
/*int TdmAgent::WriteSendSms(AnsiString mobileno, AnsiString sms, int smstype, AnsiString sendtime, int priority)
{
  char sqlbuf[512];

  if (sms == "")
    return 3;
  if ((mobileno.Length() == 11 && (mobileno.SubString(1,2) == "13" || mobileno.SubString(1,2) == "15" || mobileno.SubString(1,2) == "18"))
    || (mobileno.Length() == 12 && (mobileno.SubString(1,3) == "013" || mobileno.SubString(1,3) == "015" || mobileno.SubString(1,3) == "018")))
  {
    sprintf( sqlbuf, "insert into tbSendSms (SmsSrvType,Mobile,SmsMsg,PreSendTime,AcceptBy,Priority,PreSendTime) values (%d,'%s','%s','%s','%d',%d,%s)",
      smstype, mobileno.c_str(), sms.c_str(), sendtime.c_str(), MyWorker.WorkerNo,priority, MyNowFuncName.c_str());
    try
    {
      dmAgent->adoqueryPub->SQL->Clear();
      dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
      dmAgent->adoqueryPub->ExecSQL();
      return 0;
    }
    catch ( ... )
    {
      return 1;
    }
  }
  return 2;
}*/
int TdmAgent::GetRecvSmsRecord(TADOQuery *adoquery, CRecvSms *recvsms)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  recvsms->id = adoquery->FieldByName("id")->AsInteger;
	  recvsms->Mobile = adoquery->FieldByName("Mobile")->AsString;
	  recvsms->SmsMsg = adoquery->FieldByName("SmsMsg")->AsString;
	  recvsms->RecvTime = adoquery->FieldByName("RecvTime")->AsString;
	  recvsms->ReadFlag = adoquery->FieldByName("ReadFlag")->AsInteger;
	  recvsms->ReadTime = adoquery->FieldByName("ReadTime")->AsString;
	  recvsms->ProcSrvType = adoquery->FieldByName("ProcSrvType")->AsInteger;
	  recvsms->ProcSrvData = adoquery->FieldByName("ProcSrvData")->AsString;
	  recvsms->ProcBy = adoquery->FieldByName("ProcBy")->AsString;
	  recvsms->Remark = adoquery->FieldByName("Remark")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvSmsRecord(CRecvSms *recvsms)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbRecvSms set \
ReadFlag=%d,\
ReadTime='%s',\
ProcSrvType=%d,\
ProcSrvData='%s',\
ProcBy='%s',\
Remark='%s' \
where id=%d",
	  recvsms->ReadFlag,
	  recvsms->ReadTime.c_str(),
	  recvsms->ProcSrvType,
	  recvsms->ProcSrvData.c_str(),
	  recvsms->ProcBy.c_str(),
	  recvsms->Remark.c_str(),
    recvsms->id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteRecvSmsRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbRecvSms where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteSendSmsRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbSendSms where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::QueryNewRecvSmsCount()
{
  char sqlbuf[256];
  int recordcount=0;

  if (SmsOpenId == 0)
    return 0;
  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbRecvSms where ReadFlag=0");
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    FormMain->lbRecvNewSms->Caption = recordcount;
    adoQrySms->Close();
    if (recordcount == 0)
    {
      FormMain->lbRecvNewSms->Color = clBtnFace;
      FormMain->Label73->Color = clBtnFace;
    }
    else
    {
      FormMain->lbRecvNewSms->Color = clRed;
      FormMain->Label73->Color = clRed;
    }
    return recordcount;
  }
  catch ( ... )
  {
    ADOConnection1->Connected = false;
    FormMain->StatusBar1->Panels->Items[7]->Text = "失敗("+strDBType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    FormMain->ToolBar1->Color = clRed;
    FormMain->lbRecvNewSms->Caption = 0;
    FormMain->lbRecvNewSms->Color = clBtnFace;
  }
  return recordcount;
}
int TdmAgent::QueryNewRecvMsgCount()
{
  char sqlbuf[256];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbrecvmsg where WorkerNo='%s' and ReadFlag=0", MyWorker.WorkerNo.c_str());
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
  }
  catch ( ... )
  {
  }
  return recordcount;
}
void TdmAgent::UpdateRecvMsgReadFlag()
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbRecvMsg set ReadFlag=1 where ReadFlag=0");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::InsertRecvMsgData(AnsiString nRecvWorkerNo, AnsiString nSendWorkerNo, AnsiString strSendWorkerName, AnsiString msg, int nReadFlag)
{
  char sqlbuf[1024];

  sprintf( sqlbuf, "insert into tbRecvMsg (WorkerNo,SendWorkerNo,SendName,Msg,RecvTime,ReadFlag) values ('%s','%s','%s','%s',%s,%d)",
    nRecvWorkerNo.c_str(), nSendWorkerNo.c_str(), strSendWorkerName.c_str(), msg.c_str(), MyNowFuncName.c_str(), nReadFlag);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return;
  }
  catch ( ... )
  {
    return;
  }
}
void TdmAgent::InsertSendMsgData(AnsiString nRecvWorkerNo, AnsiString strRecvWorkerName, AnsiString msg)
{
  char sqlbuf[1024];

  sprintf( sqlbuf, "insert into tbSendMsg (SendWorkerNo,SendWorkerName,RecvWorkerNo,RecvWorkerName,Msg,SendTime) values ('%s','%s','%s','%s','%s',%s)",
    MyWorker.WorkerNo.c_str(), MyWorker.WorkerName.c_str(), nRecvWorkerNo.c_str(), strRecvWorkerName.c_str(), msg.c_str(), MyNowFuncName.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return;
  }
  catch ( ... )
  {
    return;
  }
}
void TdmAgent::DeleteOneRecvMsgRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbRecvMsg where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteOneSendMsgRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbSendMsg where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteAllRecvMsgRecord()
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbRecvMsg where WorkerNo='%s'", MyWorker.WorkerNo.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//---------------------------------------------------------------------------
void TdmAgent::GetAllAcptItemSet()
{
  int i, j, k, nSrvTypeCount=0;
  int itemvalue, itemsubvalue, itemsubexvalue;

  if (FormMain->m_pAcptItemSetList != NULL)
  {
    delete []FormMain->m_pAcptItemSetList;
    FormMain->m_pAcptItemSetList=NULL;
  }

  FormMain->m_pAcptItemSetList = new CAcptItemSetList[512];
  if (FormMain->m_pAcptItemSetList == NULL)
    return;


  if (GetAllAcptItemSet(0, 0, 0, FormMain->m_pAcptItemSetList[nSrvTypeCount]) == 0)
    nSrvTypeCount++;

  for (i=0; i<ProdTypeItemList.ItemNum; i++)
  {
    itemvalue = ProdTypeItemList.pLookupItem[i].ItemValue;
    if (SrvTypeItemList.IsItemExist(itemvalue) == true)
    {
      for (j=0; j<SrvTypeItemList.ItemNum; j++)
      {
        if (itemvalue == SrvTypeItemList.pLookupItem[j].ItemValue)
        {
          itemsubvalue = SrvTypeItemList.pLookupItem[j].ItemSubValue;
          if (SrvSubTypeItemList.IsItemExist(itemvalue, itemsubvalue) == true)
          {
          }
          else
          {
            if (nSrvTypeCount < 512)
            if (GetAllAcptItemSet(itemvalue, itemsubvalue, 0, FormMain->m_pAcptItemSetList[nSrvTypeCount]) == 0)
              nSrvTypeCount++;
            if (nSrvTypeCount >= 512)
              break;
          }
        }
      }
    }
    else
    {
      if (nSrvTypeCount < 512)
      if (GetAllAcptItemSet(itemvalue, 0, 0, FormMain->m_pAcptItemSetList[nSrvTypeCount]) == 0)
        nSrvTypeCount++;
      if (nSrvTypeCount >= 512)
        break;
    }
  }
  for (k=0; k<SrvSubTypeItemList.ItemNum; k++)
  {
    itemvalue = SrvSubTypeItemList.pLookupItem[k].ItemValue;
    itemsubvalue = SrvSubTypeItemList.pLookupItem[k].ItemSubValue;
    itemsubexvalue = SrvSubTypeItemList.pLookupItem[k].ItemSubExValue;
    if (nSrvTypeCount < 512)
    if (GetAllAcptItemSet(itemvalue, itemsubvalue, itemsubexvalue, FormMain->m_pAcptItemSetList[nSrvTypeCount]) == 0)
      nSrvTypeCount++;
    if (nSrvTypeCount >= 512)
      break;
  }

  FormMain->AcptItemSetListCount = nSrvTypeCount;
}
int TdmAgent::GetAllAcptItemSet(int nProdType, int nSrvType, int nSrvSubType, CAcptItemSetList &acptitemsetlist)
{
  char sqlbuf[256];
  int i=0, nControlCount;

  if (GetCombineDefaultItemId(nProdType, nSrvType, nSrvSubType) == 0)
    sprintf(sqlbuf, "select * from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d order by ProdType,SrvType,SrvSubType,Tid", nProdType, nSrvType, nSrvSubType);
  else
    sprintf(sqlbuf, "select * from tbacptitemset where (ProdType=0 and SrvType=0 and SrvSubType=0) or (ProdType=%d and SrvType=%d and SrvSubType=%d) order by ProdType,SrvType,SrvSubType,Tid", nProdType, nSrvType, nSrvSubType);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub1->Open();
    nControlCount = dmAgent->adoqueryPub1->RecordCount;
    if (nControlCount == 0)
      return 1;
    if (acptitemsetlist.InitData(nControlCount) != 0)
      return 1;
    dmAgent->adoqueryPub1->First();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      if (dmAgent->GetAcptItemSetRecord(dmAgent->adoqueryPub1, &acptitemsetlist.pAcptItemSet[i]) == 0)
      {
        GetAllAcptItemSetList(acptitemsetlist.pAcptItemSet[i]);
        i++;
      }
      dmAgent->adoqueryPub1->Next();
    }
    acptitemsetlist.nProdType = nProdType;
    acptitemsetlist.nSrvType = nSrvType;
    acptitemsetlist.nSrvSubType = nSrvSubType;
    dmAgent->adoqueryPub1->Close();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::GetAllAcptItemSetList(CAcptItemSet &acptitemset)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname, itemdemo, itemurl;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select TItemId,TItemText,TItemDemo,TItemURL from tbacptitemlist where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid=%d order by TItemId",
      acptitemset.ProdType, acptitemset.SrvType, acptitemset.SrvSubType, acptitemset.Tid);
    try
    {
      adoqueryPub2->SQL->Clear();
      adoqueryPub2->SQL->Add((char *)sqlbuf);
      adoqueryPub2->Open();
      itemnum = adoqueryPub2->RecordCount;
      if (acptitemset.TLookupItemList.InitData(itemnum) == 0)
      {
        while ( !adoqueryPub2->Eof )
        {
          itemvalue = adoqueryPub2->FieldByName("TItemId")->AsInteger;
          itemname = adoqueryPub2->FieldByName("TItemText")->AsString;
          itemdemo = adoqueryPub2->FieldByName("TItemDemo")->AsString;
          itemurl = adoqueryPub2->FieldByName("TItemURL")->AsString;
          acptitemset.TLookupItemList.AddItem(itemvalue, itemname, itemdemo, itemurl);
          //WriteErrprMsg("ProdType=%d SrvType=%d SrvSubType=%d Tid=%d TItemId=%d TItemText=%s",
          //  acptitemset.ProdType, acptitemset.SrvType, acptitemset.SrvSubType, acptitemset.Tid, itemvalue, itemname.c_str());
          adoqueryPub2->Next();
        }
      }
      adoqueryPub2->Close();
    }
    catch ( ... )
    {
    }
}
//---------------------------------------------------------------------------
void TdmAgent::QueryAcptItemSet(int nProdType, int nSrvType, int nSrvSubType)
{
  char sqlbuf[256];

  if (nSrvType < 0)
  {
    adoQryAcptItemSet->Close();
    return;
  }
  sprintf(sqlbuf, "Select * from vm_acptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d order by ProdType,SrvType,SrvSubType,Tid", nProdType, nSrvType, nSrvSubType);
  try
  {
    adoQryAcptItemSet->SQL->Clear();
    adoQryAcptItemSet->SQL->Add((char *)sqlbuf );
    adoQryAcptItemSet->Open();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::IsAcptItemSetTidExist(int nProdType, int nSrvType, int nSrvSubType, int nTid)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select tid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d", nProdType, nSrvType, nSrvSubType, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsLinkItemExist(int nProdType, int nSrvType, int nSrvSubType)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select tid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and TControlType=%d", nProdType, nSrvType, nSrvSubType, 10);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsLinkItemExist(int nProdType, int nSrvType, int nSrvSubType, int nTid)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select tid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and TControlType=%d and Tid<>%d", nProdType, nSrvType, nSrvSubType, 10, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::GetAcptItemSetRecord(TADOQuery *adoquery, CAcptItemSet *acptitemset)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  acptitemset->id = adoquery->FieldByName("id")->AsInteger;
	  acptitemset->SrvType = adoquery->FieldByName("SrvType")->AsInteger;
	  acptitemset->Tid = adoquery->FieldByName("Tid")->AsInteger;
	  acptitemset->TFieldName = adoquery->FieldByName("TFieldName")->AsString;
	  acptitemset->TControlType = adoquery->FieldByName("TControlType")->AsInteger;
	  acptitemset->TLabelCaption = adoquery->FieldByName("TLabelCaption")->AsString;
	  acptitemset->TXpos = adoquery->FieldByName("TXpos")->AsInteger;
	  acptitemset->TYpos = adoquery->FieldByName("TYpos")->AsInteger;
	  acptitemset->THeigth = adoquery->FieldByName("THeigth")->AsInteger;
	  acptitemset->TWith = adoquery->FieldByName("TWith")->AsInteger;
	  acptitemset->TDataType = adoquery->FieldByName("TDataType")->AsInteger;
	  acptitemset->TDefaultData = adoquery->FieldByName("TDefaultData")->AsString;
	  acptitemset->TMinValue = adoquery->FieldByName("TMinValue")->AsInteger;
	  acptitemset->TMaxValue = adoquery->FieldByName("TMaxValue")->AsInteger;
    acptitemset->TIsAllowNull = adoquery->FieldByName("TIsAllowNull")->AsInteger;
    acptitemset->TExportId = adoquery->FieldByName("TExportId")->AsInteger;
    acptitemset->SrvSubType = adoquery->FieldByName("SrvSubType")->AsInteger;
    acptitemset->TLinkId = adoquery->FieldByName("TLinkId")->AsString;
    acptitemset->TMaskEditStr = adoquery->FieldByName("TMaskEditStr")->AsString;
    acptitemset->THintStr = adoquery->FieldByName("THintStr")->AsString;
    acptitemset->CombineMode = adoquery->FieldByName("CombineMode")->AsInteger;
    acptitemset->CombineTid = adoquery->FieldByName("CombineTid")->AsInteger;
    acptitemset->CombineValue = adoquery->FieldByName("CombineValue")->AsString;
    acptitemset->ExportOrderId = adoquery->FieldByName("ExportOrderId")->AsInteger;
    acptitemset->ProdType = adoquery->FieldByName("ProdType")->AsInteger;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetAcptItemSetRecord(int nProdType, int nSrvType, int nSrvSubType, int nTid, CAcptItemSet *acptitemset)
{
  char sqlbuf[128];
  int result=1;

  sprintf(sqlbuf, "Select * from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d",
    nProdType, nSrvType, nSrvSubType, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    acptitemset->id = adoqueryPub->FieldByName("id")->AsInteger;
	    acptitemset->SrvType = adoqueryPub->FieldByName("SrvType")->AsInteger;
      acptitemset->Tid = adoqueryPub->FieldByName("Tid")->AsInteger;
      acptitemset->TFieldName = adoqueryPub->FieldByName("TFieldName")->AsString;
      acptitemset->TControlType = adoqueryPub->FieldByName("TControlType")->AsInteger;
      acptitemset->TLabelCaption = adoqueryPub->FieldByName("TLabelCaption")->AsString;
      acptitemset->TXpos = adoqueryPub->FieldByName("TXpos")->AsInteger;
      acptitemset->TYpos = adoqueryPub->FieldByName("TYpos")->AsInteger;
      acptitemset->THeigth = adoqueryPub->FieldByName("THeigth")->AsInteger;
      acptitemset->TWith = adoqueryPub->FieldByName("TWith")->AsInteger;
      acptitemset->TDataType = adoqueryPub->FieldByName("TDataType")->AsInteger;
      acptitemset->TDefaultData = adoqueryPub->FieldByName("TDefaultData")->AsString;
      acptitemset->TMinValue = adoqueryPub->FieldByName("TMinValue")->AsInteger;
      acptitemset->TMaxValue = adoqueryPub->FieldByName("TMaxValue")->AsInteger;
      acptitemset->TIsAllowNull = adoqueryPub->FieldByName("TIsAllowNull")->AsInteger;
      acptitemset->TExportId = adoqueryPub->FieldByName("TExportId")->AsInteger;
      acptitemset->SrvSubType = adoqueryPub->FieldByName("SrvSubType")->AsInteger;
      acptitemset->TLinkId = adoqueryPub->FieldByName("TLinkId")->AsString;
      acptitemset->TMaskEditStr = adoqueryPub->FieldByName("TMaskEditStr")->AsString;
      acptitemset->THintStr = adoqueryPub->FieldByName("THintStr")->AsString;
      acptitemset->CombineMode = adoqueryPub->FieldByName("CombineMode")->AsInteger;
      acptitemset->CombineTid = adoqueryPub->FieldByName("CombineTid")->AsInteger;
      acptitemset->CombineValue = adoqueryPub->FieldByName("CombineValue")->AsString;
      acptitemset->ExportOrderId = adoqueryPub->FieldByName("ExportOrderId")->AsInteger;
      acptitemset->ProdType = adoqueryPub->FieldByName("ProdType")->AsInteger;
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertAcptItemSet(CAcptItemSet &acptitemset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbacptitemset (\
SrvType,\
Tid,\
TFieldName,\
TControlType,\
TLabelCaption,\
TXpos,\
TYpos,\
THeigth,\
TWith,\
TDataType,\
TDefaultData,\
TMinValue,\
TMaxValue,\
TIsAllowNull,\
TExportId,\
SrvSubType,\
TLinkId,\
TMaskEditStr,\
THintStr,\
ExportOrderId,\
CombineMode,\
CombineTid,\
CombineValue,\
ProdType\
) values (%d,%d,'%s',%d,'%s',%d,%d,%d,%d,%d,'%s',%d,%d,%d,%d,%d,'%s','%s','%s',%d,%d,%d,'%s',%d)",
  acptitemset.SrvType,
  acptitemset.Tid,
  acptitemset.TFieldName.c_str(),
  acptitemset.TControlType,
  acptitemset.TLabelCaption.c_str(),
  acptitemset.TXpos,
  acptitemset.TYpos,
  acptitemset.THeigth,
  acptitemset.TWith,
  acptitemset.TDataType,
  acptitemset.TDefaultData.c_str(),
  acptitemset.TMinValue,
  acptitemset.TMaxValue,
  acptitemset.TIsAllowNull,
  acptitemset.TExportId,
  acptitemset.SrvSubType,
  acptitemset.TLinkId.c_str(),
  acptitemset.TMaskEditStr.c_str(),
  acptitemset.THintStr.c_str(),
  acptitemset.ExportOrderId,
  acptitemset.CombineMode,
  acptitemset.CombineTid,
  acptitemset.CombineValue.c_str(),
  acptitemset.ProdType
  );

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateAcptItemSet(CAcptItemSet &acptitemset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbacptitemset set \
TFieldName='%s',\
TControlType=%d,\
TLabelCaption='%s',\
TXpos=%d,\
TYpos=%d,\
THeigth=%d,\
TWith=%d,\
TDataType=%d,\
TDefaultData='%s',\
TMinValue=%d,\
TMaxValue=%d,\
TIsAllowNull=%d,\
TExportId=%d,\
TLinkId='%s',\
TMaskEditStr='%s',\
THintStr='%s',\
ExportOrderId=%d,\
CombineMode=%d,\
CombineTid=%d,\
CombineValue='%s' \
where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid=%d",
  acptitemset.TFieldName.c_str(),
  acptitemset.TControlType,
  acptitemset.TLabelCaption.c_str(),
  acptitemset.TXpos,
  acptitemset.TYpos,
  acptitemset.THeigth,
  acptitemset.TWith,
  acptitemset.TDataType,
  acptitemset.TDefaultData.c_str(),
  acptitemset.TMinValue,
  acptitemset.TMaxValue,
  acptitemset.TIsAllowNull,
  acptitemset.TExportId,
  acptitemset.TLinkId.c_str(),
  acptitemset.TMaskEditStr.c_str(),
  acptitemset.THintStr.c_str(),
  acptitemset.ExportOrderId,
  acptitemset.CombineMode,
  acptitemset.CombineTid,
  acptitemset.CombineValue.c_str(),
  acptitemset.ProdType,
  acptitemset.SrvType,
  acptitemset.SrvSubType,
  acptitemset.Tid);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DelAcptItemSet(int nProdType, int nSrvType, int nSrvSubType, int nTid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d",
    nProdType, nSrvType, nSrvSubType, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  sprintf(sqlbuf, "delete from tbacptitemlist where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d",
    nProdType, nSrvType, nSrvSubType, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  return 0;
}

int TdmAgent::IsAcptItemListExist(int nProdType, int nSrvType, int nSrvSubType, int nTid, int nItemNo)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select TitemId from tbacptitemlist where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d and TitemId=%d",
    nProdType, nSrvType, nSrvSubType, nTid, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertAcptItemListRecord(int nProdType, int nSrvType, int nSrvSubType, int nTid, int nItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo)
{
  char sqlbuf[4500];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbacptitemlist (SrvType,SrvSubType,tid,TitemId,TitemText,TItemDemo,TItemURL,ProdType) values (%d,%d,%d,%d,'%s','%s','%s',%d)",
    nSrvType, nSrvSubType, nTid, nItemNo, strItemName.c_str(), strItemDemo.c_str(), strItemURL.c_str(), nProdType);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateAcptItemListRecord(int nProdType, int nSrvType, int nSrvSubType, int nTid, int nItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo)
{
  char sqlbuf[4500];

  sprintf( sqlbuf, "update tbacptitemlist set TitemText='%s',TItemURL='%s',TItemDemo='%s' where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d and TitemId=%d",
    strItemName.c_str(), strItemURL.c_str(), strItemDemo.c_str(), nProdType, nSrvType, nSrvSubType, nTid, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteAcptItemListRecord(int nProdType, int nSrvType, int nSrvSubType, int nTid, int nItemNo)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbacptitemlist where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d and TitemId=%d",
    nProdType, nSrvType, nSrvSubType, nTid, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::AddAcptItemListCmbItem(TComboBox *combobox, int nProdType, int nSrvType, int nSrvSubType, int nTid)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select TItemText from tbacptitemlist where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d order by TItemId",
      nProdType, nSrvType, nSrvSubType, nTid);
    try
    {
        adoqueryPub1->SQL->Clear();
        adoqueryPub1->SQL->Add((char *)sqlbuf);
        adoqueryPub1->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub1->Eof )
    {
        itemname = adoqueryPub1->FieldByName("TItemText")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub1->Next();
    }
    adoqueryPub1->Close();
}
int TdmAgent::GetAcptSelItemValue(CAcptItemSet &acptitemset, AnsiString strItemName)
{
    char sqlbuf[256];
    int nResult=-1;

    if (ADOConnection1->Connected == false)
      return nResult;
    sprintf(sqlbuf, "Select TItemId from tbacptitemlist where ProdType=%d and SrvType=%d and SrvSubType=%d and tid=%d and TItemText='%s'",
      acptitemset.ProdType, acptitemset.SrvType, acptitemset.SrvSubType, acptitemset.Tid, strItemName.c_str());
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
        if ( !adoqueryPub->Eof )
        {
            nResult = adoqueryPub->FieldByName("TItemId")->AsInteger;
        }
        adoqueryPub->Close();
    }
    catch ( ... )
    {
    }
    return nResult;
}

int TdmAgent::IsAcceptExExist(AnsiString GId)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select gid from tbacceptex where gid='%s'", GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertAcceptEx(CAcceptEx &acceptex)
{
  char sqlbuf1[4096], sqlbuf2[3584], szTemp[256];

  if (acceptex.nItemNum == 0)
    return 1;
  memset(sqlbuf1, 0, 4096);
  memset(sqlbuf2, 0, 3584);
  sprintf(sqlbuf1, "insert into tbacceptex (gid,SerialNo");
  sprintf(sqlbuf2, ") values ('%s','%s'", acceptex.GId.c_str(), acceptex.SerialNo.c_str());
  for (int i=1; i<=acceptex.nItemNum; i++)
  {
    sprintf(szTemp,",ItemData%d", i);
    strcat(sqlbuf1, szTemp);

    sprintf(szTemp,",'%s'", acceptex.Itemdata[i-1].c_str());
    strcat(sqlbuf2, szTemp);
  }
  strcat(sqlbuf2, ")");
  strcat(sqlbuf1, sqlbuf2);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf1 );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("InsertAcceptEx error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::UpdateAcceptEx(CAcceptEx &acceptex)
{
  char sqlbuf1[4096], szTemp[256];

  if (acceptex.nItemNum == 0)
    return 1;
  memset(sqlbuf1, 0, 4096);
  sprintf(sqlbuf1, "update tbacceptex set SerialNo='%s'", acceptex.SerialNo.c_str());
  for (int i=1; i<=acceptex.nItemNum; i++)
  {
    sprintf(szTemp,",ItemData%d=", i);
    strcat(sqlbuf1, szTemp);

    sprintf(szTemp,"'%s'", acceptex.Itemdata[i-1].c_str());
    strcat(sqlbuf1, szTemp);
  }
  sprintf(szTemp, " where gid='%s'", acceptex.GId.c_str());
  strcat(sqlbuf1, szTemp);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf1 );
    //WriteErrprMsg("UpdateAcceptEx sql=%s", sqlbuf1);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("UpdateAcceptEx error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::GetAcceptExRecord(AnsiString GId, CAcceptEx *acceptex)
{
  char sqlbuf[128];
  int result=1;
  AnsiString strFieldName;

  sprintf(sqlbuf, "Select * from tbacceptex where GId='%s'",
    GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    acceptex->GId = adoqueryPub->FieldByName("GId")->AsString;
	    acceptex->SerialNo = adoqueryPub->FieldByName("SerialNo")->AsString;
      for (int i=1; i<=MAX_ACPT_CONTROL_NUM; i++)
      {
        strFieldName = "Itemdata"+IntToStr(i);
        acceptex->Itemdata[i-1] = adoqueryPub->FieldByName(strFieldName)->AsString;
      }
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
//---------------------------------------------------------------------------
//插入業務受理資料, 受理渠道MediaType類型: 0-電話,1-IM,2-EMAIL,3-FAX
int TdmAgent::InsertAccept(CAccept &accept, bool bClearAcceptCont)
{
  char sqlbuf[512];

  if (accept.SerialNo == "")
   return 2;
  sprintf( sqlbuf, "insert into tbAccept (\
GID,\
SerialNo,\
CustomTel,\
SeatNo,\
WorkerNo,\
CustomNo,\
ACDTime,\
RelTime,\
ACWTime,\
AcptCont,\
AcptType,\
SrvSubType,\
MediaType,\
ProdType,\
ProdModelType,\
DepartmentNo,\
AcptTime,\
PutTime,\
AnsId,\
DealState\
) values ('%s','%s','%s','%s','%s','%s',%s,%s,%s,'未記錄',%d,%d,%d,%d,%d,%d,%s,%s,1,%d)",
      accept.GId.c_str(),
      accept.SerialNo.c_str(),
      accept.CustomTel.c_str(),
      accept.SeatNo.c_str(),
      accept.WorkerNo.c_str(),
      accept.CustomNo.c_str(),
      MyNowFuncName.c_str(),
      MyNowFuncName.c_str(),
      MyNowFuncName.c_str(),
      accept.AcptType,
      accept.SrvSubType,
      accept.MediaType,
      accept.ProdType,
      accept.ProdModelType,
      MyWorker.DepartmentID, //2018-05-15新增部們編號
      MyNowFuncName.c_str(),
      MyNowFuncName.c_str(),
      accept.DealState);
  try
  {
    WriteErrprMsg("InsertAccept sqls=%s", sqlbuf);
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->ExecSQL();
    //accept.SerialNo = "";
    if (bClearAcceptCont == true)
      FormMain->memoAcceptCont->Lines->Clear();
    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("InsertAccept error=%s", exception.Message.c_str());
    return 1;
  }
}
AnsiString TdmAgent::MyGetGIUD()
{
  AnsiString szGUID;
  char temp[40];

  GUID guid;
  CreateGUID(guid);
  sprintf(temp, "%08lx-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
    guid.Data1, guid.Data2, guid.Data3,
    guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
		guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);
  szGUID = (char *)temp;
  return szGUID;
}
//修改受理時間
void TdmAgent::UpdateAcceptTime(CAccept accept)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbAccept set AcptTime=%s,PutTime=%s,RelTime=%s,ACWTime=%s,AnsId=1,RecdRootPath='%s',RecdFile='%s' where gid='%s'",
    MyNowFuncName.c_str(), MyNowFuncName.c_str(), MyNowFuncName.c_str(), MyNowFuncName.c_str(),
    accept.RecdRootPath.c_str(), accept.RecdFile.c_str(), accept.GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateAcceptCustomData(CAccept accept)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbAccept set CustomNo='%s' where gid='%s'", accept.CustomNo.c_str(), accept.GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::UpdateAcceptCallBackTime(AnsiString gid, int flag, AnsiString cbtime)
{
  char sqlbuf[256];
  int nCount;

  if (flag == 1)
    sprintf(sqlbuf, "update tbAccept set CallBackId=1,CallBackTime='%s',CallBackWorkerNo='%s' where GID='%s'", cbtime.c_str(), MyWorker.WorkerNo.c_str(), gid.c_str());
  else
    sprintf(sqlbuf, "update tbAccept set CallBackId=0,CallBackTime=NULL,CallBackWorkerNo='' where GID='%s'", gid.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::QueryAcceptCallBackRecords()
{
  char sqlbuf[512];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbaccept where CallBackWorkerNo='%s' and CallBackId=1 and CallBackTime<GETDATE()",
    MyWorker.WorkerNo.c_str());
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
    FormMain->lbAcceptCallBackCount->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbAcceptCallBackCount->Color = clBtnFace;
      FormMain->lbAcceptCallBack->Color = clBtnFace;
    }
    else
    {
      FormMain->lbAcceptCallBackCount->Color = clRed;
      FormMain->lbAcceptCallBack->Color = clRed;
    }
    return recordcount;
  }
  catch ( Exception &exception )
  {
    if (exception.Message.AnsiPos("連線失敗") > 0 || exception.Message.AnsiPos("不存在或拒絕存取。") > 0 || exception.Message.AnsiPos("逾時過期") > 0)
      ADOConnection1->Connected = false;
    FormMain->lbAcceptCallBackCount->Color = clBtnFace;
    FormMain->lbAcceptCallBackCount->Caption = 0;
    FormMain->lbAcceptCallBack->Color = clBtnFace;
  }
  return recordcount;
}
int TdmAgent::QueryAcceptCallBackRecordsEx()
{
  char sqlbuf[512];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbaccept where CallBackWorkerNo='%s' and CallBackId=1 and CallBackTime<GETDATE()",
    MyWorker.WorkerNo.c_str());
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
  }
  catch ( Exception &exception )
  {
  }
  return recordcount;
}
void TdmAgent::UpdateAcceptRecordData(CAccept accept)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbAccept set RecdRootPath='%s',RecdFile='%s',RecdCallID='%s' where gid='%s'",
    accept.RecdRootPath.c_str(), accept.RecdFile.c_str(), accept.RecdCallID.c_str(), accept.GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateRelTime(AnsiString GId)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbAccept set RelTime=%s,PutTime=%s,ACWTime=%s where gid='%s'",
    MyNowFuncName.c_str(), MyNowFuncName.c_str(), MyNowFuncName.c_str(), GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateACWTime(AnsiString GId)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbAccept set ACWTime=%s where gid='%s'",
    MyNowFuncName.c_str(), GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::DelCustomer(AnsiString strWhereSql)
{
  int nRow=0;
  char sqlbuf[256];

  if (strWhereSql == "")
  {
    sprintf( sqlbuf, "delete from tbcustomer");
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  else
  {
    sprintf(sqlbuf, "delete from tbcustomer where %s", strWhereSql.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  return nRow;
}
int TdmAgent::DelAccept(AnsiString strWhereSql)
{
  int nRow=0;
  char sqlbuf[256];

  if (strWhereSql == "")
  {
    sprintf( sqlbuf, "delete from tbAccept");
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
    sprintf( sqlbuf, "delete from tbAcceptex");
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
    sprintf( sqlbuf, "delete from tbAcceptproc");
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
    }
    catch ( ... )
    {
    }
  }
  else
  {
    sprintf(sqlbuf, "delete from tbAcceptproc where gid in (select gid from tbAccept where %s)", strWhereSql.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
    }
    catch ( ... )
    {
    }
    sprintf(sqlbuf, "delete from tbAcceptex where gid in (select gid from tbAccept where %s)", strWhereSql.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
    }
    catch ( ... )
    {
    }
    sprintf(sqlbuf, "delete from tbAccept where %s", strWhereSql.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  return nRow;
}
void TdmAgent::UpdateSrvType(AnsiString SerialNo, int nProdType, int SrvType, int SrvSubType)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbCallCDR set ProdType=%d,SrvType=%d,SrvSubType=%d where SerialNo='%s'", nProdType, SrvType, SrvSubType, SerialNo.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::DelCallCDR(AnsiString strWhereSql)
{
  int nRow=0;
  char sqlbuf[256];

  if (strWhereSql == "")
  {
    sprintf( sqlbuf, "delete from tbcallcdr");
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  else
  {
    sprintf(sqlbuf, "delete from tbcallcdr where %s", strWhereSql.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  return nRow;
}
int TdmAgent::DelOPLog(AnsiString strWhereSql)
{
  int nRow=0;
  char sqlbuf[256];

  if (strWhereSql == "")
  {
    sprintf( sqlbuf, "delete from tboplog");
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  else
  {
    sprintf(sqlbuf, "delete from tboplog where %s", strWhereSql.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      nRow = adoqueryPub->RowsAffected;
    }
    catch ( ... )
    {
    }
  }
  return nRow;
}
int TdmAgent::GetRecdFileName(AnsiString serialno, AnsiString &recdpath, AnsiString &recdfile)
{
  char sqlbuf[128];
  bool nResult=0;

  recdpath = "";
  recdfile = "";
  sprintf(sqlbuf, "Select RecdRootPath,RecdFile from tbcallcdr where SerialNo='%s'", serialno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      recdpath = adoqueryPub->FieldByName("RecdRootPath")->AsString;
      recdfile = adoqueryPub->FieldByName("RecdFile")->AsString;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::GetTalkScore(AnsiString serialno)
{
  char sqlbuf[128];
  int  nScore=0;

  sprintf(sqlbuf, "Select Score from tbcallcdr where SerialNo='%s'", serialno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      nScore = adoqueryPub->FieldByName("Score")->AsInteger;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nScore;
}
AnsiString TdmAgent::GetTalkScoreName(AnsiString serialno)
{
  return ScoreNameList.GetItemName(GetTalkScore(serialno));
}
int TdmAgent::IsRecdFileExist(AnsiString serialno)
{
  char sqlbuf[128];
  bool nResult=0;
  AnsiString strRecdFile="";

  sprintf(sqlbuf, "Select RecdFile from tbcallcdr where SerialNo='%s'", serialno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strRecdFile = adoqueryPub->FieldByName("RecdFile")->AsString;
      if (strRecdFile.Length() > 0)
        nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
AnsiString TdmAgent::GetRecdFileName(AnsiString serialno)
{
  char sqlbuf[128];
  AnsiString strRecdFile="";

  sprintf(sqlbuf, "Select RecdFile from tbcallcdr where SerialNo='%s'", serialno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strRecdFile = adoqueryPub->FieldByName("RecdFile")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return strRecdFile;
}
//修改受理內容
int TdmAgent::EditAcceptCont(AnsiString GId, AnsiString cont, int nAcptType, int nSrvSubType, int nAcptSubType, int nProdType, int nProdModelType)
{
  char sqlbuf[8000];
  AnsiString strTemp;
  AnsiString strCont = cont + "\r\n" + DateTimeToStr(Now()) + " [" + MyWorker.WorkerName + "-" + MyWorker.WorkerNo + "]"; //2017-09-17

  strTemp = StringReplace(strCont, "'", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);
  strTemp = StringReplace(strTemp, "\"", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);

  if (strDBType == "SQLSERVER")
    sprintf( sqlbuf, "update tbAccept set AcptCont='%s',AcptType=%d,SrvSubType=%d,AcptSubType=%d,ProdType=%d,ProdModelType=%d where gid='%s'",
      strTemp.c_str(), nAcptType, nSrvSubType, nAcptSubType, nProdType, nProdModelType, GId.c_str());
  else if (strDBType == "MYSQL")
    sprintf( sqlbuf, "update tbAccept set AcptCont='%s',AcptType=%d,SrvSubType=%d,AcptSubType=%d,ProdType=%d,ProdModelType=%d where gid='%s'",
      strTemp.c_str(), nAcptType, nSrvSubType, nAcptSubType, nProdType, nProdModelType, GId.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    WriteErrprMsg("EditAcceptCont sql=%s", sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("EditAcceptCont error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::UpdateAcceptCont(AnsiString GId, AnsiString cont, int nAcptType, int nSrvSubType, int nAcptSubType, int nDealState, int nProdType, int nProdModelType, AnsiString strTel)
{
  char sqlbuf[8000];
  AnsiString strTemp;
  AnsiString strCont;
  if (cont == "")
    strCont = "未登記";
  else
    strCont = cont + "\r\n" + DateTimeToStr(Now()) + " [" + MyWorker.WorkerName + "-" + MyWorker.WorkerNo + "]"; //2017-09-17

  strTemp = StringReplace(strCont, "'", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);
  strTemp = StringReplace(strTemp, "\"", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);

  sprintf( sqlbuf, "update tbAccept set DealState=%d,AcptType=%d,SrvSubType=%d,AcptSubType=%d,ProdType=%d,ProdModelType=%d,AcptCont='%s',PutTime=%s,AcptLen=DATEDIFF(ss,AcptTime,%s),CustomTel='%s' where gid='%s'",
      nDealState, nAcptType, nSrvSubType, nAcptSubType, nProdType, nProdModelType, strTemp.c_str(), MyNowFuncName.c_str(), MyNowFuncName.c_str(), strTel.c_str(), GId.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    WriteErrprMsg("UpdateAcceptCont sql=%s", sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( Exception &exception )
  {
    WriteErrprMsg("UpdateAcceptCont error=%s", exception.Message.c_str());
    return 1;
  }
}
int TdmAgent::DeleteAccept(AnsiString GId)
{
  char sqlbuf[128];
  sprintf( sqlbuf, "delete from tbAccept where gid='%s'", GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  sprintf( sqlbuf, "delete from tbAcceptex where gid='%s'", GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  return 0;
}
int TdmAgent::UpdateAcceptType(AnsiString GId, int srvtype)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbAccept set AcptType=%d where gid='%s'", srvtype, GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateAcceptScore(AnsiString GId, int nScore)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbaccept set AcceptScore=%d where GId='%s'", nScore, GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateCallTalkScore(int id, int nTalkScore)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbcallcdr set TalkScore=%d where id=%d", nTalkScore, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
//修改處理內容
void TdmAgent::UpdateDealCont(AnsiString GId, AnsiString cont, int nDealState)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbAccept set DealLog='%s',DealTime=%s,DealWorker='%s',DealState=%d where gid='%s'",
    cont.c_str(), MyNowFuncName.c_str(), MyWorker.WorkerNo.c_str(), nDealState, GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateAcceptAnsid(AnsiString GId)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbAccept set AnsId=1,AcptTime=%s,PutTime=%s,RelTime=%s where gid='%s'",
    MyNowFuncName.c_str(), MyNowFuncName.c_str(), MyNowFuncName.c_str(), GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateDealState(AnsiString GId, int state)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbAccept set DealState=%d,DealTime=%s,DealWorker='%s' where gid='%s'",
    state, MyNowFuncName.c_str(), MyWorker.WorkerNo.c_str(), GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QueryCurAccept(AnsiString custno, AnsiString teleno, AnsiString ItemData1)
{
  char sqlbuf[512];
  int nProdType, nSrvType, nSrvSubType, nTemp;
  AnsiString dealcont, acceptcont, strGID;
  AnsiString sqlWhere="";
  CAcceptEx acceptex;
  
  if (ItemData1 != "")
  {
    sqlWhere = "(AccountNo='"+ItemData1+"'";
    //sqlWhere = "ItemData1='"+ItemData1+"'";
  }
  /*else
  {
    adoQryCurAccept->Close();
    return;
  }*/

  if (teleno != "")
  {
    if (sqlWhere != "")
    {
      if (custno != "0" && custno != "")
        sqlWhere = sqlWhere + " or CustomNo='"+custno+"'";
    }
    else
    {
      sqlWhere = sqlWhere + "(CustomNo='"+custno+"'";
    }
    if (sqlWhere != "")
      sqlWhere = sqlWhere + " or CustomTel like '%"+teleno+"'";
    else
      sqlWhere = "(CustomTel like '%"+teleno+"'";
    sqlWhere = sqlWhere + ") and AnsId=1";
  }
  else
  {
    if (sqlWhere != "")
    {
      if (custno != "0" && custno != "")
        sqlWhere = sqlWhere + " or CustomNo='"+custno+"'";
    }
    else
    {
      sqlWhere = sqlWhere + "(CustomNo='"+custno+"'";
    }
    sqlWhere = sqlWhere + ") and AnsId=1";
  }
  //sqlWhere = sqlWhere;

  if (strDBType == "SQLSERVER")
  {
    sprintf(sqlbuf, "Select top 50 * from vm_Acceptcust1 where %s order by ACDTime desc", sqlWhere.c_str());
  }
  else if (strDBType == "MYSQL")
  {
    sprintf(sqlbuf, "Select * from vm_Acceptcust1 where %s order by ACDTime desc limit 50", sqlWhere.c_str());
  }

  //WriteErrprMsg("QueryCurAccept sqls=%s", sqlbuf);
  try
  {
    adoQryCurAccept->SQL->Clear();
    adoQryCurAccept->SQL->Add((char *)sqlbuf );
    adoQryCurAccept->Open();
    CallMsg.QYBH = ItemData1;
    if (!adoQryCurAccept->Eof)
    {
      FormCallin1->edtLastCallTime->Text = adoQryCurAccept->FieldByName("ACDTime")->AsString;
      FormCallin1->edtLastWorkerName->Text = adoQryCurAccept->FieldByName("WorkerName")->AsString;
    }
    else
    {
      FormCallin1->edtLastCallTime->Text = "";
      FormCallin1->edtLastWorkerName->Text = "";
    }
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCurAccept error=%s", exception.Message.c_str());
    return;
  }
  if (dmAgent->adoQryCurAccept->State == dsInactive || dmAgent->adoQryCurAccept->Eof)
  {
    FormMain->memoCurAcptCont->Lines->Clear();
    FormMain->memoCurDealCont->Lines->Clear();
    FormMain->plCurAcptItem->Visible = false;
    return;
  }

  try
  {
    dealcont = dmAgent->adoQryCurAccept->FieldByName("DealLog")->AsString;
    acceptcont = dmAgent->adoQryCurAccept->FieldByName("AcptCont")->AsString;

    nProdType = dmAgent->adoQryCurAccept->FieldByName("ProdType")->AsInteger;
    FormMain->cbProdType2->Text = dmAgent->ProdTypeItemList.GetItemName(nProdType);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("ProdModelType")->AsInteger;
    FormMain->cbProdModelType2->Text = dmAgent->ProdModelTypeItemList.GetItemName(nProdType, nTemp);

    nSrvType = dmAgent->adoQryCurAccept->FieldByName("AcptType")->AsInteger;
    FormMain->cbCurSrvType->Text = dmAgent->SrvTypeItemList.GetItemName(nSrvType);

    dmAgent->AddSrvSubTypeCmbItem(FormMain->cbCurSrvSubType, dmAgent->SrvSubTypeItemList, nSrvType);
    nSrvSubType = dmAgent->adoQryCurAccept->FieldByName("SrvSubType")->AsInteger;
    FormMain->cbCurSrvSubType->Text = dmAgent->SrvSubTypeItemList.GetItemName(nProdType, nSrvType, nSrvSubType);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("AcptSubType")->AsInteger;
    FormMain->cbCurSubType->Text = dmAgent->AcptSubTypeItemList.GetItemName(nTemp);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("DealState")->AsInteger;
    FormMain->cmbCurDealState->Text = dmAgent->AcceptProcItemList.GetItemName(nTemp);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("AcceptScore")->AsInteger;
    FormMain->cbCurAcceptScore->Text = dmAgent->TalkScoreNameList.GetItemName(nTemp);

    FormMain->memoCurDealCont->Lines->Clear();
    if (dealcont.Length() > 0)
      FormMain->memoCurDealCont->Lines->Add(dealcont);
    FormMain->btnCurDealCont->Enabled = false;
    FormMain->memoCurAcptCont->Lines->Clear();
    if (acceptcont.Length() > 0)
      FormMain->memoCurAcptCont->Lines->Add(acceptcont);

    FormMain->btCurEditAcptCont->Enabled = false;
    FormMain->btCurAcptScore->Enabled = false;
    
    strGID = dmAgent->adoQryCurAccept->FieldByName("GID")->AsString;
    if (dmAgent->GetAcceptExRecord(strGID, &acceptex) == 0)
    {
      FormMain->plCurAcptItem->Visible = true;
      FormMain->CreateCurUserAcptItem(nProdType, nSrvType, nSrvSubType, acceptex);
    }
    else
    {
      FormMain->plCurAcptItem->Visible = false;
    }
  }
  catch (...)
  {
  }
  FormMain->PageControl1->ActivePage = FormMain->TabSheet5;
}
void TdmAgent::QueryCurAcceptEx(AnsiString ItemData1, AnsiString teleno)
{
  char sqlbuf[512];
  int nProdType, nSrvType, nSrvSubType, nTemp;
  AnsiString dealcont, acceptcont, strGID;
  AnsiString sqlWhere="";
  CAcceptEx acceptex;

  WriteErrprMsgEx("Start QueryCurAcceptEx !!!");
  
  if (ItemData1 != "")
  {
    sqlWhere = "CustItemData1='"+ItemData1+"'";
  }
  else if (teleno != "")
  {
    sqlWhere = "CustomTel='"+teleno+"'";
  }
  else
  {
    adoQryCurAccept->Close();
    return;
  }
  sqlWhere = sqlWhere + " and AnsId=1 and acdtime>dateadd(d,-90,getdate())";

  if (strDBType == "SQLSERVER")
  {
    sprintf(sqlbuf, "Select * from vm_Acceptcust2 where %s order by ACDTime desc", sqlWhere.c_str());
  }
  else if (strDBType == "MYSQL")
  {
    sprintf(sqlbuf, "Select * from vm_Acceptcust2 where %s order by ACDTime desc limit 50", sqlWhere.c_str());
  }

  WriteErrprMsg("QueryCurAccept sqls=%s", sqlbuf);
  try
  {
    adoQryCurAccept->SQL->Clear();
    adoQryCurAccept->SQL->Add((char *)sqlbuf );
    adoQryCurAccept->Open();
    CallMsg.QYBH = ItemData1;
    if (!adoQryCurAccept->Eof)
    {
      FormCallin1->edtLastCallTime->Text = adoQryCurAccept->FieldByName("ACDTime")->AsString;
      FormCallin1->edtLastWorkerName->Text = adoQryCurAccept->FieldByName("WorkerName")->AsString;
    }
    else
    {
      FormCallin1->edtLastCallTime->Text = "";
      FormCallin1->edtLastWorkerName->Text = "";
    }
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCurAccept error=%s", exception.Message.c_str());

    WriteErrprMsgEx("End QueryCurAcceptEx !!!");
    return;
  }
  if (dmAgent->adoQryCurAccept->State == dsInactive || dmAgent->adoQryCurAccept->Eof)
  {
    FormMain->memoCurAcptCont->Lines->Clear();
    FormMain->memoCurDealCont->Lines->Clear();
    FormMain->plCurAcptItem->Visible = false;
    WriteErrprMsgEx("End QueryCurAcceptEx !!!");
    return;
  }

  try
  {
    dealcont = dmAgent->adoQryCurAccept->FieldByName("DealLog")->AsString;
    acceptcont = dmAgent->adoQryCurAccept->FieldByName("AcptCont")->AsString;

    nProdType = dmAgent->adoQryCurAccept->FieldByName("ProdType")->AsInteger;
    FormMain->cbProdType2->Text = dmAgent->ProdTypeItemList.GetItemName(nProdType);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("ProdModelType")->AsInteger;
    FormMain->cbProdModelType2->Text = dmAgent->ProdModelTypeItemList.GetItemName(nProdType, nTemp);

    nSrvType = dmAgent->adoQryCurAccept->FieldByName("AcptType")->AsInteger;
    FormMain->cbCurSrvType->Text = dmAgent->SrvTypeItemList.GetItemName(nSrvType);

    dmAgent->AddSrvSubTypeCmbItem(FormMain->cbCurSrvSubType, dmAgent->SrvSubTypeItemList, nSrvType);
    nSrvSubType = dmAgent->adoQryCurAccept->FieldByName("SrvSubType")->AsInteger;
    FormMain->cbCurSrvSubType->Text = dmAgent->SrvSubTypeItemList.GetItemName(nProdType, nSrvType, nSrvSubType);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("AcptSubType")->AsInteger;
    FormMain->cbCurSubType->Text = dmAgent->AcptSubTypeItemList.GetItemName(nTemp);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("DealState")->AsInteger;
    FormMain->cmbCurDealState->Text = dmAgent->AcceptProcItemList.GetItemName(nTemp);

    nTemp = dmAgent->adoQryCurAccept->FieldByName("AcceptScore")->AsInteger;
    FormMain->cbCurAcceptScore->Text = dmAgent->TalkScoreNameList.GetItemName(nTemp);

    FormMain->memoCurDealCont->Lines->Clear();
    if (dealcont.Length() > 0)
      FormMain->memoCurDealCont->Lines->Add(dealcont);
    FormMain->btnCurDealCont->Enabled = false;
    FormMain->memoCurAcptCont->Lines->Clear();
    if (acceptcont.Length() > 0)
      FormMain->memoCurAcptCont->Lines->Add(acceptcont);

    FormMain->btCurEditAcptCont->Enabled = false;
    FormMain->btCurAcptScore->Enabled = false;
    
    strGID = dmAgent->adoQryCurAccept->FieldByName("GID")->AsString;
    if (dmAgent->GetAcceptExRecord(strGID, &acceptex) == 0)
    {
      FormMain->plCurAcptItem->Visible = true;
      FormMain->CreateCurUserAcptItem(nProdType, nSrvType, nSrvSubType, acceptex);
    }
    else
    {
      FormMain->plCurAcptItem->Visible = false;
    }
  }
  catch (...)
  {
  }
  FormMain->PageControl1->ActivePage = FormMain->TabSheet5;
  WriteErrprMsgEx("End QueryCurAcceptEx !!!");
}
void TdmAgent::QueryCurAcceptByTel(AnsiString teleno)
{
  char sqlbuf[256];

  if (strDBType == "SQLSERVER")
    sprintf(sqlbuf, "Select top 20 * from vm_Acceptcust1 where AnsId=1 and CustomTel like '%%%s' order by ACDTime desc", teleno.c_str());
  else if (strDBType == "MYSQL")
    sprintf(sqlbuf, "Select * from vm_tbAcceptcust1 where AnsId=1 and CustomTel like '%%%s' order by ACDTime desc limit 20", teleno.c_str());
  try
  {
    adoQryCurAccept->SQL->Clear();
    adoQryCurAccept->SQL->Add((char *)sqlbuf );
    adoQryCurAccept->Open();
  }
  catch (...)
  {
  }
}
//后續處理
void TdmAgent::InsertAcceptProcRecord(AnsiString GID, AnsiString WorkerNo, AnsiString ProcLog, int nNextProcType, AnsiString NextProcWorkerNo, int nNextprocDepartmentId, int nNextprocResult)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "insert into tbAcceptProc (GID,WorkerNo,ProcLog,NextProcType,NextProcWorkerNo,NextprocDepartmentId,ProcResult) values ('%s','%s','%s',%d,'%s',%d,%d)",
    GID.c_str(), WorkerNo.c_str(), ProcLog.c_str(), nNextProcType, NextProcWorkerNo.c_str(), nNextprocDepartmentId, nNextprocResult);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::UpdateAcceptProcCont(int Id, AnsiString WorkerNo, AnsiString ProcLog, int nNextProcType,AnsiString NextProcWorkerNo, int nNextprocDepartmentId, int nNextprocResult)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbAcceptProc set WorkerNo='%s',ProcTime=%s,ProcLog='%s',NextProcType=%d,NextProcWorkerNo='%s',NextprocDepartmentId=%d,ProcResult=%d where id=%d",
    WorkerNo.c_str(), MyNowFuncName.c_str(), ProcLog.c_str(), nNextProcType, NextProcWorkerNo.c_str(), nNextprocDepartmentId, nNextprocResult, Id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetAcceptRecord(TADOQuery *adoquery, CAccept *accept)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  accept->SerialNo = adoquery->FieldByName("SerialNo")->AsString;
	  accept->CustomNo = adoquery->FieldByName("CustomNo")->AsString;
	  accept->CustName = adoquery->FieldByName("CustName")->AsString;
	  accept->CustomTel = adoquery->FieldByName("CustomTel")->AsString;
	  accept->SrvName = adoquery->FieldByName("SrvName")->AsString;
	  accept->AcptTime = adoquery->FieldByName("AcptTime")->AsString;
	  accept->WorkerNo = adoquery->FieldByName("WorkerNo")->AsString;
	  accept->WorkerName = adoquery->FieldByName("WorkerName")->AsString;
	  accept->AcptCont = adoquery->FieldByName("AcptCont")->AsString;
	  accept->DealLog = adoquery->FieldByName("DealLog")->AsString;
	  accept->DealResult = adoquery->FieldByName("AcceptProcName")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
void TdmAgent::UpdateAcceptProcRespondId(int Id, int nRespondId)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbAcceptProc set RespondId=%d,RespondTime=%s where id=%d",
    nRespondId, MyNowFuncName.c_str(), Id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::QueryDepartmentAcceptProcCount(int nDepartmentId)
{
  char sqlbuf[256];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbAccept where gid in (select gid from tbacceptproc where RespondId=0 and NextProcType=1 and NextprocDepartmentId=%d)", nDepartmentId);
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
    FormMain->lbNewAcceptForDepartment->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbNewAcceptForDepartment->Color = clBtnFace;
      //FormMain->Label70->Color = clBtnFace;
    }
    else
    {
      FormMain->lbNewAcceptForDepartment->Color = clRed;
      //FormMain->Label70->Color = clRed;
    }
    return recordcount;
  }
  catch ( ... )
  {
    ADOConnection1->Connected = false;
    FormMain->StatusBar1->Panels->Items[7]->Text = "失敗("+strDBType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    FormMain->ToolBar1->Color = clRed;
    FormMain->lbNewAcceptForDepartment->Caption = 0;
    FormMain->lbNewAcceptForDepartment->Color = clBtnFace;
  }
  return recordcount;
}
int TdmAgent::QueryWorkerAcceptProcCount(AnsiString nWorkerNo)
{
  char sqlbuf[512];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbAccept where (WorkerNo='%d' and DealState=0) or (gid in (select gid from tbacceptproc where RespondId=0 and NextProcType=2 and NextProcWorkerNo=%d))",
    nWorkerNo, nWorkerNo);
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
    FormMain->lbNewAcceptForWorker->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbNewAcceptForWorker->Color = clBtnFace;
      FormMain->Label69->Color = clBtnFace;
    }
    else
    {
      FormMain->lbNewAcceptForWorker->Color = clRed;
      FormMain->Label69->Color = clRed;
    }
    return recordcount;
  }
  catch ( ... )
  {
    //ADOConnection1->Connected = false;
    //FormMain->StatusBar1->Panels->Items[7]->Text = "失敗("+strDBType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    //FormMain->ToolBar1->Color = clRed;
    FormMain->lbNewAcceptForWorker->Color = clBtnFace;
    FormMain->lbNewAcceptForWorker->Caption = 0;
  }
  return recordcount;
}
int TdmAgent::QueryWaitDealAccept(AnsiString strDealWhere)
{
  char sqlbuf[512];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  AnsiString strTemp;

  MyReplace(strDealWhere, "$S_WorkerNo", MyWorker.WorkerNo, strTemp);
  sprintf(sqlbuf, "select count(*) as newcount from tbAccept where %s",
    strTemp.c_str());
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
    FormMain->lbWaitProcCount->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbWaitProcCount->Color = clBtnFace;
      FormMain->lbWaitProc->Color = clBtnFace;
    }
    else
    {
      FormMain->lbWaitProcCount->Color = clRed;
      FormMain->lbWaitProc->Color = clRed;
    }
    return recordcount;
  }
  catch ( ... )
  {
    //ADOConnection1->Connected = false;
    //FormMain->StatusBar1->Panels->Items[7]->Text = "失敗("+strDBType+";"+strDBServer+";"+strDataBase+";"+strUserId+")";
    //FormMain->ToolBar1->Color = clRed;
    FormMain->lbWaitProcCount->Color = clBtnFace;
    FormMain->lbWaitProcCount->Caption = 0;
  }
  return recordcount;
}
int TdmAgent::QueryWaitDealAcceptEx()
{
  char sqlbuf[512];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  AnsiString strTemp;

  MyReplace(strWaitProcAcceptWhere, "$S_WorkerNo", MyWorker.WorkerNo, strTemp);
  sprintf(sqlbuf, "select count(*) as newcount from tbAccept where %s",
    strTemp.c_str());
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
  }
  catch ( ... )
  {
  }
  return recordcount;
}
int TdmAgent::QueryDialOutCallBackRecords()
{
  char sqlbuf[512];
  int nId,recordcount=0;
  bool bPrompt=false;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbdialouttele where EndCode1<>%d and TaskWorkerNo='%s' and CallBackId=1 and DATEADD(n,0-%d,CallBackTime)<GETDATE()",
    g_nRejectEndCode1, MyWorker.WorkerNo.c_str(), g_nAheadNoticeTime);
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
    FormMain->lbDialOutCallBackCount->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbDialOutCallBackCount->Color = clBtnFace;
      FormMain->lbDialOutCallBack->Color = clBtnFace;
    }
    else
    {
      FormMain->lbDialOutCallBackCount->Color = clRed;
      FormMain->lbDialOutCallBack->Color = clRed;
    }
  }
  catch ( Exception &exception )
  {
    if (exception.Message.AnsiPos("連線失敗") > 0 || exception.Message.AnsiPos("不存在或拒絕存取。") > 0 || exception.Message.AnsiPos("逾時過期") > 0)
      ADOConnection1->Connected = false;
    FormMain->lbDialOutCallBackCount->Color = clBtnFace;
    FormMain->lbDialOutCallBack->Caption = 0;
  }
  if (recordcount > 0)
  {
    sprintf(sqlbuf, "select top 1 Id,CalledNo,CustName from tbdialouttele where EndCode1<>%d and TaskWorkerNo='%s' and CallBackId=1 and CallBackTime<GETDATE() order by CallBackTime,id",
      g_nRejectEndCode1, MyWorker.WorkerNo.c_str());
    try
    {
      adoQrySms->SQL->Clear();
      adoQrySms->SQL->Add((char *)sqlbuf);
      adoQrySms->Open();
      if (!adoQrySms->Eof)
      {
        nId = adoQrySms->FieldByName("Id")->AsInteger;
        if (DialOutCallBackPromptId != nId)
        {
          DialOutCallBackPromptId = nId;
          if (recordcount == 1)
          {
            FormCallBackPrompt->Button1->Visible = true;
            FormCallBackPrompt->Label2->Visible = true;
            FormCallBackPrompt->ComboBox1->Visible = true;
            FormCallBackPrompt->Button2->Visible = true;
            DialOutCallBackPromptMsg = "您好:有1個號碼預約回撥,號碼是:"+adoQrySms->FieldByName("CalledNo")->AsString+",姓名是:"+adoQrySms->FieldByName("CustName")->AsString;
          }
          else
          {
            FormCallBackPrompt->Button1->Visible = false;
            FormCallBackPrompt->Label2->Visible = false;
            FormCallBackPrompt->ComboBox1->Visible = false;
            FormCallBackPrompt->Button2->Visible = false;
            DialOutCallBackPromptMsg = "您好:有"+IntToStr(recordcount)+"個號碼預約回撥";
          }
          bPrompt = true;
        }
      }
      adoQrySms->Close();
      if (bPrompt == true)
        PostMessage(Application->MainForm->Handle, WM_USER_DIALLCALLBACK_MSG, 0L, 0L);
      return recordcount;
    }
    catch (...)
    {
    }
  }
  DialOutCallBackPromptId = -1;
  DialOutCallBackPromptMsg = "";
  return recordcount;
}
int TdmAgent::QueryDialOutCallBackRecordsEx()
{
  char sqlbuf[512];
  int nId,recordcount=0;
  //bool bPrompt=false;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbdialouttele where EndCode1<>%d and TaskWorkerNo='%s' and CallBackId=1 and DATEADD(n,0-%d,CallBackTime)<GETDATE()",
    g_nRejectEndCode1, MyWorker.WorkerNo.c_str(), g_nAheadNoticeTime);
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
  }
  catch ( Exception &exception )
  {
  }
  return recordcount;
}
//---------------------------------------------------------------------------


void __fastcall TdmAgent::adoQryCalloutTeleCalcFields(TDataSet *DataSet)
{
  adoQryCalloutTeleDispLastResult->AsString = DialoutResultItemList.GetItemName(adoQryCalloutTeleLastResult->AsInteger);
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryCalloutTaskCalcFields(TDataSet *DataSet)
{
  adoQryCalloutTaskDispStartId->AsString = YesNoItemList.GetItemName(adoQryCalloutTaskStartId->AsInteger);
  adoQryCalloutTaskDispFuncNo->AsString = FlwFuncNoList.GetItemName(adoQryCalloutTaskFuncNo->AsInteger);
  if (adoQryCalloutTaskGroupNo->AsInteger == 0)
    adoQryCalloutTaskDispGroupNo->AsString = "不轉值機坐席";
  else
    adoQryCalloutTaskDispGroupNo->AsString = WorkerGroupList.GetItemName(adoQryCalloutTaskGroupNo->AsInteger);
  if (adoQryCalloutTaskMixerType->AsInteger == 0)
    adoQryCalloutTaskDispMixerType->AsString = "不合成";
  else if (adoQryCalloutTaskMixerType->AsInteger == 1)
    adoQryCalloutTaskDispMixerType->AsString = "播報數字";
  else if (adoQryCalloutTaskMixerType->AsInteger == 2)
    adoQryCalloutTaskDispMixerType->AsString = "播報金額";
  else if (adoQryCalloutTaskMixerType->AsInteger == 3)
    adoQryCalloutTaskDispMixerType->AsString = "播報數值";

  switch (adoQryCalloutTaskExportId->AsInteger)
  {
  case 0:
    adoQryCalloutTaskDispExportId->AsString = "不處理";
    break;
  case 1:
    adoQryCalloutTaskDispExportId->AsString = "自動刪除成功記錄";
    break;
  case 2:
    adoQryCalloutTaskDispExportId->AsString = "自動刪除失敗記錄";
    break;
  case 3:
    adoQryCalloutTaskDispExportId->AsString = "自動刪除成功及失敗記錄";
    break;
  case 4:
    adoQryCalloutTaskDispExportId->AsString = "自動將成功記錄匯入歷史表";
    break;
  case 5:
    adoQryCalloutTaskDispExportId->AsString = "自動將失敗記錄匯入歷史表";
    break;
  case 6:
    adoQryCalloutTaskDispExportId->AsString = "自動將成功及失敗記錄匯入歷史表";
    break;
  default:
    adoQryCalloutTaskDispExportId->AsString = "不處理";
    break;
  }
  switch (adoQryCalloutTaskCallType->AsInteger)
  {
  case 0:
    adoQryCalloutTaskDispCallType->AsString = "外撥後播放語音";
    break;
  case 1:
    adoQryCalloutTaskDispCallType->AsString = "外撥後發送傳真";
    break;
  case 2:
    adoQryCalloutTaskDispCallType->AsString = "外撥成功後轉坐席";
    break;
  case 3:
    adoQryCalloutTaskDispCallType->AsString = "先撥坐席然後外撥";
    break;
  default:
    adoQryCalloutTaskDispCallType->AsString = "外撥後播放語音";
    break;
  }
}
//---------------------------------------------------------------------------
int TdmAgent::GetDialOutTaskRecord(TADOQuery *adoquery, CDialOutTask *dialouttask)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  dialouttask->TaskId = adoquery->FieldByName("TaskId")->AsInteger;
	  dialouttask->TaskName = adoquery->FieldByName("TaskName")->AsString;
	  dialouttask->StartDate = adoquery->FieldByName("StartDate")->AsString;
	  dialouttask->EndDate = adoquery->FieldByName("EndDate")->AsString;
	  dialouttask->StartTime = adoquery->FieldByName("StartTime")->AsString;
	  dialouttask->EndTime = adoquery->FieldByName("EndTime")->AsString;
	  dialouttask->StartTime2 = adoquery->FieldByName("StartTime2")->AsString;
	  dialouttask->EndTime2 = adoquery->FieldByName("EndTime2")->AsString;
	  dialouttask->StartId = adoquery->FieldByName("StartId")->AsInteger;
	  dialouttask->StartMem = adoquery->FieldByName("StartMem")->AsString;
	  dialouttask->StopMem = adoquery->FieldByName("StopMem")->AsString;
	  dialouttask->TotalTeles = adoquery->FieldByName("TotalTeles")->AsInteger;
	  dialouttask->CalledCount = adoquery->FieldByName("CalledCount")->AsInteger;
	  dialouttask->AnsCount = adoquery->FieldByName("AnsCount")->AsInteger;
	  dialouttask->DeviceNo = adoquery->FieldByName("DeviceNo")->AsInteger;
	  dialouttask->RouteNo = adoquery->FieldByName("RouteNo")->AsInteger;
	  dialouttask->MaxChnNum = adoquery->FieldByName("MaxChnNum")->AsInteger;
	  dialouttask->CallerNo = adoquery->FieldByName("CallerNo")->AsString;
	  dialouttask->MaxCalledTimes = adoquery->FieldByName("MaxCalledTimes")->AsInteger;
	  dialouttask->MaxAnsTimes = adoquery->FieldByName("MaxAnsTimes")->AsInteger;
	  dialouttask->NoAnsDelay = adoquery->FieldByName("NoAnsDelay")->AsInteger;
	  dialouttask->BusyDelay = adoquery->FieldByName("BusyDelay")->AsInteger;
	  dialouttask->FileName = adoquery->FieldByName("FileName")->AsString;
	  dialouttask->MixerType = adoquery->FieldByName("MixerType")->AsInteger;
	  dialouttask->FileName2 = adoquery->FieldByName("FileName2")->AsString;
	  dialouttask->GroupNo = adoquery->FieldByName("GroupNo")->AsInteger;
	  dialouttask->FuncNo = adoquery->FieldByName("FuncNo")->AsInteger;
	  dialouttask->Param = adoquery->FieldByName("Param")->AsString;
	  dialouttask->Remark = adoquery->FieldByName("Remark")->AsString;
	  dialouttask->ExportId = adoquery->FieldByName("ExportId")->AsInteger;
	  dialouttask->CallType = adoquery->FieldByName("CallType")->AsInteger;
	  dialouttask->PlayDelay = adoquery->FieldByName("PlayDelay")->AsInteger;
	  dialouttask->TTSType = adoquery->FieldByName("TTSType")->AsInteger;
	  dialouttask->TTSUpdateId = adoquery->FieldByName("TTSUpdateId")->AsInteger;
	  dialouttask->TTSData = adoquery->FieldByName("TTSData")->AsString;
	  dialouttask->TTSFileName = adoquery->FieldByName("TTSFileName")->AsString;
	  dialouttask->ConfirmType = adoquery->FieldByName("ConfirmType")->AsInteger;
	  dialouttask->ConfirmVoc = adoquery->FieldByName("ConfirmVoc")->AsString;
	  dialouttask->ConfirmDTMF = adoquery->FieldByName("ConfirmDTMF")->AsString;
    dialouttask->TranVDN = adoquery->FieldByName("TranVDN")->AsString;
    dialouttask->DialPreCode = adoquery->FieldByName("DialPreCode")->AsString;
    dialouttask->ListenTimeLen = adoquery->FieldByName("ListenTimeLen")->AsInteger;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertDialOutTaskRecord(CDialOutTask *dialouttask)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbCallOutTask (\
TaskName,\
StartId,\
CallerNo,\
StartDate,\
EndDate,\
StartTime,\
EndTime,\
StartTime2,\
EndTime2,\
MaxCalledTimes,\
MaxAnsTimes,\
NoAnsDelay,\
BusyDelay,\
RouteNo,\
MaxChnNum,\
GroupNo,\
FileName,\
MixerType,\
FileName2,\
FuncNo,\
ExportId,\
CallType,\
PlayDelay,\
TTSType,\
TTSUpdateId,\
TTSFileName,\
TTSData,\
ConfirmType,\
ConfirmVoc,\
ConfirmDTMF,\
TranVDN,\
DialPreCode,\
ListenTimeLen\
) values ('%s',%d,'%s','%s','%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,'%s',%d,'%s',%d,%d,%d,%d,%d,%d,'%s','%s',%d,'%s','%s','%s','%s',%d)",
	  dialouttask->TaskName.c_str(),
    dialouttask->StartId,
    dialouttask->CallerNo.c_str(),
    dialouttask->StartDate.c_str(),
    dialouttask->EndDate.c_str(),
    dialouttask->StartTime.c_str(),
    dialouttask->EndTime.c_str(),
    dialouttask->StartTime2.c_str(),
    dialouttask->EndTime2.c_str(),
    dialouttask->MaxCalledTimes,
    dialouttask->MaxAnsTimes,
    dialouttask->NoAnsDelay,
    dialouttask->BusyDelay,
    dialouttask->RouteNo,
    dialouttask->MaxChnNum,
    dialouttask->GroupNo,
    dialouttask->FileName.c_str(),
    dialouttask->MixerType,
    dialouttask->FileName2.c_str(),
    dialouttask->FuncNo,
    dialouttask->ExportId,
    dialouttask->CallType,
    dialouttask->PlayDelay,
    dialouttask->TTSType,
    dialouttask->TTSUpdateId,
    dialouttask->TTSFileName.c_str(),
    dialouttask->TTSData.c_str(),
    dialouttask->ConfirmType,
    dialouttask->ConfirmVoc.c_str(),
    dialouttask->ConfirmDTMF.c_str(),
    dialouttask->TranVDN.c_str(),
    dialouttask->DialPreCode.c_str(),
    dialouttask->ListenTimeLen
    );

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateDialOutTaskRecord(CDialOutTask *dialouttask)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbCallOutTask set \
TaskName='%s',\
StartId=%d,\
CallerNo='%s',\
StartDate='%s',\
EndDate='%s',\
StartTime='%s',\
EndTime='%s',\
StartTime2='%s',\
EndTime2='%s',\
MaxCalledTimes=%d,\
MaxAnsTimes=%d,\
NoAnsDelay=%d,\
BusyDelay=%d,\
RouteNo=%d,\
MaxChnNum=%d,\
GroupNo=%d,\
FileName='%s',\
MixerType=%d,\
FileName2='%s',\
FuncNo=%d,\
ExportId=%d,\
CallType=%d,\
PlayDelay=%d,\
TTSType=%d,\
TTSUpdateId=%d,\
TTSFileName='%s',\
TTSData='%s',\
ConfirmType=%d,\
ConfirmVoc='%s',\
ConfirmDTMF='%s',\
TranVDN='%s',\
DialPreCode='%s',\
ListenTimeLen=%d \
where taskid=%d",
	  dialouttask->TaskName.c_str(),
    dialouttask->StartId,
    dialouttask->CallerNo.c_str(),
    dialouttask->StartDate.c_str(),
    dialouttask->EndDate.c_str(),
    dialouttask->StartTime.c_str(),
    dialouttask->EndTime.c_str(),
    dialouttask->StartTime2.c_str(),
    dialouttask->EndTime2.c_str(),
    dialouttask->MaxCalledTimes,
    dialouttask->MaxAnsTimes,
    dialouttask->NoAnsDelay,
    dialouttask->BusyDelay,
    dialouttask->RouteNo,
    dialouttask->MaxChnNum,
    dialouttask->GroupNo,
    dialouttask->FileName.c_str(),
    dialouttask->MixerType,
    dialouttask->FileName2.c_str(),
    dialouttask->FuncNo,
    dialouttask->ExportId,
    dialouttask->CallType,
    dialouttask->PlayDelay,
    dialouttask->TTSType,
    dialouttask->TTSUpdateId,
    dialouttask->TTSFileName.c_str(),
    dialouttask->TTSData.c_str(),
    dialouttask->ConfirmType,
    dialouttask->ConfirmVoc.c_str(),
    dialouttask->ConfirmDTMF.c_str(),
    dialouttask->TranVDN.c_str(),
    dialouttask->DialPreCode.c_str(),
    dialouttask->ListenTimeLen,
    dialouttask->TaskId);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteDialOutTaskRecord(int taskid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCallOutTask where taskid=%d", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteDialOutTeleRecord(int taskid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCallOutTele where taskid=%d", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::UpdateDialOutTaskStartId(int taskid, int startid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbCallOutTask set startid=%d where taskid=%d", startid, taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertDialOutTeleRecord(int taskid, AnsiString calledno)
{
  char sqlbuf[256];
  if (calledno.Length() == 0)
    return 1;
  sprintf(sqlbuf, "insert into tbCallOutTele (taskid,calledno,InsertTime) values (%d,'%s',%s)",
    taskid, calledno.c_str(), MyNowFuncName.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertDialOutTeleRecord(int taskid, AnsiString calledno, AnsiString custname, AnsiString MixerValue, int Priority, int BatchNo)
{
  char sqlbuf[256];
  if (calledno.Length() == 0)
    return 1;
  sprintf(sqlbuf, "insert into tbCallOutTele (taskid,calledno,CustName,MixerValue,Priority,InsertTime,BatchId) values (%d,'%s','%s','%s',%d,%s,%d)",
    taskid, calledno.c_str(), custname.c_str(), MixerValue.c_str(), Priority, MyNowFuncName.c_str(), BatchNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteDialOutTeleRecord(int taskid, AnsiString calledno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCallOutTele where taskid=%d and CalledNo='%s'", taskid, calledno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteOneDialOutTeleRecord(int serialno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCallOutTele where serialno=%d", serialno);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteAllDialOutTeleRecord(int taskid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCallOutTele where taskid=%d", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateDialOutPriority(int serialno, int Priority)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbCallOutTele set Priority=%d where SerialNo=%d", Priority, serialno);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
AnsiString TdmAgent::GetNameByTele(AnsiString calledno)
{
  char sqlbuf[256];
  AnsiString itemname="";

  if (strDBType == "SQLSERVER")
    sprintf(sqlbuf, "Select top 1 CustName from tbCallOutTele where CalledNo='%s'", calledno.c_str());
  else if (strDBType == "MYSQL")
    sprintf(sqlbuf, "Select CustName from tbCallOutTele where CalledNo='%s' limit 1", calledno.c_str());
  try
  {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
  }
  catch ( ... )
  {
      return itemname;
  }
  if ( !adoqueryPub->Eof )
  {
    itemname = adoqueryPub->FieldByName("AlarmName")->AsString;
  }
  adoqueryPub->Close();
  return itemname;
}
//---------------------------------------------------------------------------
//2016-12-31通過HTTP從第3放查詢客戶資料
int TdmAgent::QueryCustExDataFromCRM(CCustomer *customer, CCustEx *custex)
{
  int i;
  AnsiString strCustExData;
  char szTemp[1024], szAnsiTemp[1024];
  if (strQueryCustDataURL == "")
    return 0;
  AnsiString strURLTemp = strQueryCustDataURL;
  try
  {
    WriteErrprMsg("QueryCustExDataFromCRM Itemdata[0]=%s Itemdata[1]=%s", custex->Itemdata[0].c_str(), custex->Itemdata[1].c_str());
    if (custex->Itemdata[0] == "" && custex->Itemdata[1] == "")
      return 0;

    FormMain->ProcPopURL(g_nQueryCustDataURLEncodeType, strURLTemp, &gSysvar);
    FormMain->ProcPopURL(g_nQueryCustDataURLEncodeType, strURLTemp, customer, custex);
    strCustExData = FormMain->IdHTTP1->Get(strURLTemp);
    WriteErrprMsg("httpget URL=%s %s", strURLTemp.c_str(), strCustExData.c_str());
    if (strCustExData == "{}" || strCustExData == "FAIL" || strCustExData.Pos("Socket Error #") > 0)
    {
      FormMain->IdHTTP1->DisconnectSocket();
      return 0;
    }
    memset(szTemp, 0, 1024);
    memset(szAnsiTemp, 0, 1024);
    strncpy(szTemp, strCustExData.c_str(), 1023);
    UTF8_ANSI_Convert(szTemp, szAnsiTemp, CP_UTF8, CP_ACP);

    FormMain->IdHTTP1->DisconnectSocket();

    WriteErrprMsg("httpget URL=%s %s", strURLTemp.c_str(), szAnsiTemp);

    for (i=0; i<MAX_CUSTOMER_CONTROL_NUM && FormMain->m_pCustomerItemSetList->ItemNum; i++)
    {
      if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadDataType == 2 || FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadDataType == 4) //2017-12-12從HTTP讀資料
      {
        if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustName);
          customer->CustName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustType") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustTypeName);
          customer->CustTypeName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustLevel") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustLevelName);
          customer->CustLevelName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustSex") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustSex);
          customer->CustSex = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("MobileNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->MobileNo);
          customer->MobileNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("TeleNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->TeleNo);
          customer->TeleNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("FaxNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->FaxNo);
          customer->FaxNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("ContAddr") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->ContAddr);
          customer->ContAddr = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 100);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("PostNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->PostNo);
          customer->PostNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("EMail") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->EMail);
          customer->EMail = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("Province") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->Province);
          customer->Province = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CityName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CityName);
          customer->CityName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CountyName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CountyName);
          customer->CountyName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("ZoneName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->ZoneName);
          customer->ZoneName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CorpName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CorpName);
          customer->CorpName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 100);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("SubPhone") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->SubPhone);
          customer->SubPhone = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("Remark") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->Remark);
          customer->Remark = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 250);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("AccountNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->AccountNo);
          customer->AccountNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 64);
        }
      }
    }

    for (i=0; i<MAX_CUST_CONTROL_NUM && FormMain->m_pCustExItemSetList->ItemNum; i++)
    {
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 2 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31從HTTP讀資料
      {
        //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadFieldTag.c_str(), custex->Itemdata[i]);
        AnsiString strValue = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadFieldTag.c_str(), "", 250);
        int len = strValue.Length();
        if (len >= FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue
          && (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue == 0 || len <= FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue))
          custex->Itemdata[i] = strValue;
        //if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType == 7)
        //  custex->Itemdata[i] = custex->Itemdata[i].SubString(1, 10);
      }
    }
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustExDataFromCRM URL=%s %s", strURLTemp.c_str(), exception.Message.c_str());
    return 0;
  }
  return 1;
}
int TdmAgent::QueryCustExDataFromCRM(AnsiString strID, AnsiString strAC, CCustomer *customer, CCustEx *custex)
{
  int i;
  AnsiString strCustExData;
  char szTemp[1024], szAnsiTemp[1024];
  if (strQueryCustDataURL == "")
    return 0;
  AnsiString strURLTemp = strQueryCustDataURL;
  try
  {
    WriteErrprMsg("QueryCustExDataFromCRM strID=%s strAC=%s", strID.c_str(), strAC.c_str());

    custex->Itemdata[0] == strAC;
    custex->Itemdata[1] == strID;
    WriteErrprMsg("QueryCustExDataFromCRM Itemdata[0]=%s Itemdata[1]=%s", custex->Itemdata[0].c_str(), custex->Itemdata[1].c_str());
    if (custex->Itemdata[0] == "" && custex->Itemdata[1] == "")
      return 0;

    FormMain->ProcPopURL(g_nQueryCustDataURLEncodeType, strURLTemp, &gSysvar);
    FormMain->ProcPopURL(g_nQueryCustDataURLEncodeType, strURLTemp, customer, custex);
    strCustExData = FormMain->IdHTTP1->Get(strURLTemp);
    WriteErrprMsg("httpget URL=%s respond=%s", strURLTemp.c_str(), strCustExData.c_str());
    if (strCustExData == "{}" || strCustExData == "FAIL" || strCustExData.Pos("Socket Error #") > 0)
    {
      FormMain->IdHTTP1->DisconnectSocket();
      return 0;
    }
    memset(szTemp, 0, 1024);
    memset(szAnsiTemp, 0, 1024);
    strncpy(szTemp, strCustExData.c_str(), 1023);
    UTF8_ANSI_Convert(szTemp, szAnsiTemp, CP_UTF8, CP_ACP);

    FormMain->IdHTTP1->DisconnectSocket();

    WriteErrprMsg("httpget URL=%s %s", strURLTemp.c_str(), szAnsiTemp);

    for (i=0; i<MAX_CUSTOMER_CONTROL_NUM && FormMain->m_pCustomerItemSetList->ItemNum; i++)
    {
      if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadDataType == 2 || FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadDataType == 4) //2017-12-12從HTTP讀資料
      {
        if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustName);
          customer->CustName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustType") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustTypeName);
          customer->CustTypeName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustLevel") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustLevelName);
          customer->CustLevelName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CustSex") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CustSex);
          customer->CustSex = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("MobileNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->MobileNo);
          customer->MobileNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("TeleNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->TeleNo);
          customer->TeleNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("FaxNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->FaxNo);
          customer->FaxNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("ContAddr") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->ContAddr);
          customer->ContAddr = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 100);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("PostNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->PostNo);
          customer->PostNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("EMail") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->EMail);
          customer->EMail = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("Province") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->Province);
          customer->Province = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CityName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CityName);
          customer->CityName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CountyName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CountyName);
          customer->CountyName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("ZoneName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->ZoneName);
          customer->ZoneName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("CorpName") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->CorpName);
          customer->CorpName = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 100);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("SubPhone") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->SubPhone);
          customer->SubPhone = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 50);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("Remark") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->Remark);
          customer->Remark = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 250);
        }
        else if (FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].CFieldName.AnsiCompareIC("AccountNo") == 0)
        {
          //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), customer->AccountNo);
          customer->AccountNo = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustomerItemSetList->m_CustomerItemSet[i].ReadFieldTag.c_str(), "", 64);
        }
      }
    }

    for (i=0; i<MAX_CUST_CONTROL_NUM && FormMain->m_pCustExItemSetList->ItemNum; i++)
    {
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 2 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31從HTTP讀資料
      {
        //GetJSONFieldValue(szAnsiTemp, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadFieldTag.c_str(), custex->Itemdata[i]);
        custex->Itemdata[i] = (AnsiString)GetParamByName(szAnsiTemp, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadFieldTag.c_str(), "", 250);
        //if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType == 7)
        //  custex->Itemdata[i] = custex->Itemdata[i].SubString(1, 10);
      }
    }
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustExDataFromCRM URL=%s %s", strURLTemp.c_str(), exception.Message.c_str());
    return 0;
  }
  return 1;
}
int TdmAgent::QueryCustom(AnsiString teleno, CCustomer *customer, CCustEx *custex)
{
  AnsiString strFieldName, strAreaPhone="none";
  char sqlbuf[512];

  sprintf(sqlbuf,"Select * from tbCustomer where MobileNo='%s' or TeleNo='%s' or FaxNo='%s' or SubPhone='%s'",
    teleno.c_str(), teleno.c_str(), teleno.c_str(), teleno.c_str());

  try
  {
    //WriteErrprMsg("QueryCustom sqls=%s", sqlbuf);
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();

    if ( !adoqueryPub1->Eof )
    {
	    customer->id = adoqueryPub1->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub1->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub1->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub1->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub1->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub1->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub1->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub1->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub1->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub1->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub1->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub1->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub1->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub1->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub1->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub1->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub1->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub1->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub1->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub1->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub1->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub1->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub1->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub1->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub1->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub1->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub1->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub1->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub1->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub1->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub1->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub1->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub1->FieldByName(strFieldName)->AsString;
          }
        }
      }
      adoqueryPub1->Close();
      //QueryCustExDataFromCRM(customer, custex); //2016-12-31
      return 0;
    }
    adoqueryPub1->Close();
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustom error=%s", exception.Message.c_str());
  }
  customer->Init();
  custex->Init();
  return 1;
}
int TdmAgent::QueryCustomByCustExField_TEL(AnsiString teleno, CCustomer *customer, CCustEx *custex)
{
  AnsiString strFieldName, strAreaPhone="none";
  char sqlbuf[512];

  try
  {
    //WriteErrprMsg("QueryCustom sqls=%s", sqlbuf);
    //先查其它三個欄位  2017-12-13
    sprintf(sqlbuf,"Select * from tbCustomer where %s='%s'", g_strTELFieldName.c_str(), teleno.c_str());

    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();

    if ( !adoqueryPub1->Eof )
    {
	    customer->id = adoqueryPub1->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub1->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub1->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub1->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub1->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub1->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub1->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub1->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub1->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub1->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub1->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub1->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub1->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub1->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub1->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub1->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub1->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub1->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub1->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub1->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub1->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub1->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub1->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub1->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub1->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub1->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub1->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub1->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub1->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub1->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub1->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub1->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub1->FieldByName(strFieldName)->AsString;
          }
        }
      }
      adoqueryPub1->Close();
      //QueryCustExDataFromCRM(customer, custex); //按號碼就不查http 2017-12-13
      return 0;
    }
    adoqueryPub1->Close();
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustom error=%s", exception.Message.c_str());
  }
  customer->Init();
  custex->Init();
  return 1;
}
int TdmAgent::QueryCustom(AnsiString teleno, CCustomer *customer, CCustEx *custex, int &recordnum)
{
  AnsiString strFieldName, strAreaPhone="none";
  char sqlbuf[512];

    sprintf(sqlbuf,"Select * from tbCustomer where MobileNo='%s' or TeleNo='%s' or FaxNo='%s' or SubPhone='%s'",
      teleno.c_str(), teleno.c_str(), teleno.c_str(), teleno.c_str());
  try
  {
    //WriteErrprMsg("QueryCustom sqls=%s", sqlbuf);
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();
    recordnum = adoqueryPub1->RecordCount;

    if ( !adoqueryPub1->Eof )
    {
	    customer->id = adoqueryPub1->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub1->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub1->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub1->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub1->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub1->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub1->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub1->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub1->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub1->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub1->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub1->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub1->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub1->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub1->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub1->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub1->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub1->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub1->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub1->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub1->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub1->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub1->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub1->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub1->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub1->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub1->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub1->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub1->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub1->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub1->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub1->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub1->FieldByName(strFieldName)->AsString;
          }
        }
      }
      adoqueryPub1->Close();
      //QueryCustExDataFromCRM(customer, custex); //按號碼就不查http 2017-12-13
      return 0;
    }
    adoqueryPub1->Close();
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustom error=%s", exception.Message.c_str());
  }
  customer->Init();
  custex->Init();
  return 1;
}
//2017-10-08
int TdmAgent::QueryCustomByCustExField_TEL(AnsiString teleno, CCustomer *customer, CCustEx *custex, int &recordnum)
{
  AnsiString strFieldName;
  char sqlbuf[512];

  try
  {
    //先查其它三個欄位  2017-12-13
    sprintf(sqlbuf,"Select * from tbCustomer where %s='%s'", g_strTELFieldName.c_str(), teleno.c_str());

    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();
    recordnum = adoqueryPub1->RecordCount;

    if ( !adoqueryPub1->Eof )
    {
	    customer->id = adoqueryPub1->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub1->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub1->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub1->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub1->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub1->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub1->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub1->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub1->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub1->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub1->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub1->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub1->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub1->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub1->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub1->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub1->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub1->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub1->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub1->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub1->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub1->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub1->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub1->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub1->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub1->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub1->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub1->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub1->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub1->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub1->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub1->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub1->FieldByName(strFieldName)->AsString;
          }
        }
      }
      adoqueryPub1->Close();
      //QueryCustExDataFromCRM(customer, custex); //按號碼就不查http 2017-12-13
      return 0;
    }
    adoqueryPub1->Close();
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustom error=%s", exception.Message.c_str());
  }
  customer->Init();
  custex->Init();
  return 1;
}
int TdmAgent::QueryCustomByEmail(AnsiString email, CCustomer *customer, CCustEx *custex, int &recordnum)
{
  AnsiString strFieldName, strAreaPhone="none";
  char sqlbuf[512];

  sprintf(sqlbuf,"Select * from tbCustomer where EMail='%s'", email.c_str());
  try
  {
    //WriteErrprMsg("QueryCustom sqls=%s", sqlbuf);
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();
    recordnum = adoqueryPub1->RecordCount;

    if ( !adoqueryPub1->Eof )
    {
	    customer->id = adoqueryPub1->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub1->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub1->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub1->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub1->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub1->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub1->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub1->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub1->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub1->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub1->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub1->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub1->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub1->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub1->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub1->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub1->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub1->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub1->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub1->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub1->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub1->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub1->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub1->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub1->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub1->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub1->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub1->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub1->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub1->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub1->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub1->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub1->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub1->FieldByName(strFieldName)->AsString;
          }
        }
      }
      adoqueryPub1->Close();
      //QueryCustExDataFromCRM(customer, custex); //按號碼就不查http 2017-12-13
      return 0;
    }
    adoqueryPub1->Close();
  }
  catch (Exception &exception)
  {
    WriteErrprMsg("QueryCustom error=%s", exception.Message.c_str());
  }
  customer->Init();
  custex->Init();
  return 1;
}
AnsiString TdmAgent::GetCustomNoByFaxNo(AnsiString faxno)
{
  char sqlbuf[512];
  AnsiString strCustomNo="";

  if (strDBType == "SQLSERVER")
    sprintf(sqlbuf,"Select CustomNo from tbCustomer where MobileNo='%s' or TeleNo='%s' or FaxNo='%s'",
      faxno.c_str(), faxno.c_str(), faxno.c_str());
  else if (strDBType == "MYSQL")
    sprintf(sqlbuf,"Select CustomNo from tbCustomer where MobileNo='%s' or TeleNo='%s' or FaxNo='%s'",
      faxno.c_str(), faxno.c_str(), faxno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    strCustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return strCustomNo;
}
int TdmAgent::QueryCustomByCustNo(AnsiString custno, CCustomer *customer, CCustEx *custex)
{
  AnsiString sqlbuf, strFieldName;
  int result=1;

  if (custno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where CustomNo='"+custno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    customer->id = adoqueryPub->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub->FieldByName(strFieldName)->AsString;
          }
        }
      }
      result = 0;
      //QueryCustExDataFromCRM(customer, custex); //按號碼就不查http 2017-12-13
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::QueryCustomByAccountNo(AnsiString accountno, CCustomer *customer, CCustEx *custex)
{
  AnsiString sqlbuf, strFieldName;
  int result=1;

  if (accountno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where AccountNo='"+accountno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    customer->id = adoqueryPub->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub->FieldByName(strFieldName)->AsString;
          }
        }
      }
      result = 0;
      //QueryCustExDataFromCRM(customer, custex); //按號碼就不查http 2017-12-13
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::QueryCustomByItemData1(AnsiString accountno, CCustomer *customer, CCustEx *custex, int &recordnum)
{
  AnsiString sqlbuf, strFieldName;
  int result=1;

  WriteErrprMsg("QueryCustomByItemData1 accountno=%s", accountno.c_str());

  recordnum = 0;
  if (accountno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where ItemData1='"+accountno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();
    recordnum = adoqueryPub->RecordCount;

    if ( !adoqueryPub->Eof )
    {
	    customer->id = adoqueryPub->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub->FieldByName(strFieldName)->AsString;
          }
        }
      }
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  if (result != 0)
  {
    if (QueryCustExDataFromCRM("", accountno, customer, custex) == 1) //2017-12-13
      result = 0;
  }
  return result;
}
int TdmAgent::QueryCustomByItemData2(AnsiString accountno, CCustomer *customer, CCustEx *custex, int &recordnum)
{
  AnsiString sqlbuf, strFieldName;
  int result=1;

  WriteErrprMsg("QueryCustomByItemData2 accountno=%s", accountno.c_str());

  recordnum = 0;
  if (accountno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where ItemData2='"+accountno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();
    recordnum = adoqueryPub->RecordCount;

    if ( !adoqueryPub->Eof )
    {
	    customer->id = adoqueryPub->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub->FieldByName(strFieldName)->AsString;
          }
        }
      }
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  if (result != 0)
  {
    if (QueryCustExDataFromCRM(accountno, "", customer, custex) == 1) //2017-12-13
      result = 0;
  }
  return result;
}
int TdmAgent::QueryCustomByItemData1B(AnsiString accountno, CCustomer *customer, CCustEx *custex)
{
  AnsiString sqlbuf, strFieldName;
  int result=1;

  if (accountno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where ItemData1="+accountno;
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    customer->id = adoqueryPub->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub->FieldByName(strFieldName)->AsString;
          }
        }
      }
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  if (result != 0)
  {
    if (QueryCustExDataFromCRM("", accountno, customer, custex) == 1) //2016-12-31
      result = 0;
  }
  return result;
}
int TdmAgent::QueryTeleByCustNo(AnsiString custno, AnsiString &strMobileNo, AnsiString &strTeleNo, AnsiString &strFaxNo, AnsiString &strSubPhone)
{
  AnsiString sqlbuf;
  int result=1;

  strMobileNo = "";
  strTeleNo = "";
  strFaxNo = "";
  strSubPhone = "";
  if (custno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where CustomNo='"+custno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    strMobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    strTeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    strFaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    strSubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::QueryCustomByAccountNo(AnsiString custno, CCustomer *customer, CCustEx *custex, int &recordnum)
{
  AnsiString sqlbuf, strFieldName;
  int result=1;

  recordnum = 0;
  if (custno == "")
    return 1;
  sqlbuf = "Select * from tbCustomer where AccountNo='"+custno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();
    recordnum = adoqueryPub->RecordCount;

    if ( !adoqueryPub->Eof )
    {
	    customer->id = adoqueryPub->FieldByName("id")->AsInteger;
	    customer->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
	    customer->CustName = adoqueryPub->FieldByName("CustName")->AsString;
	    customer->CorpName = adoqueryPub->FieldByName("CorpName")->AsString;
	    customer->CustType = adoqueryPub->FieldByName("CustType")->AsInteger;
	    customer->CustLevel = adoqueryPub->FieldByName("CustLevel")->AsInteger;
	    customer->CustClass = adoqueryPub->FieldByName("CustClass")->AsInteger;
  	  //customer->PriceModule = adoqueryPub->FieldByName("PriceModule")->AsInteger;
	    //customer->Discount = adoqueryPub->FieldByName("Discount")->AsInteger;
	    customer->CustSex = adoqueryPub->FieldByName("CustSex")->AsString;
	    customer->MobileNo = adoqueryPub->FieldByName("MobileNo")->AsString;
	    customer->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
	    customer->FaxNo = adoqueryPub->FieldByName("FaxNo")->AsString;
	    customer->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
	    customer->EMail = adoqueryPub->FieldByName("EMail")->AsString;
	    customer->ContAddr = adoqueryPub->FieldByName("ContAddr")->AsString;
	    customer->PostNo = adoqueryPub->FieldByName("PostNo")->AsString;
	    customer->Province = adoqueryPub->FieldByName("Province")->AsString;
	    customer->CityName = adoqueryPub->FieldByName("CityName")->AsString;
	    customer->CountyName = adoqueryPub->FieldByName("CountyName")->AsString;
	    customer->ZoneName = adoqueryPub->FieldByName("ZoneName")->AsString;
      //customer->EnsureMoney = adoqueryPub->FieldByName("EnsureMoney")->AsFloat;
	    customer->RegTime = adoqueryPub->FieldByName("RegTime")->AsString;
	    customer->RegWorker = adoqueryPub->FieldByName("RegWorker")->AsString;
      customer->DepartmentId = adoqueryPub->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
      customer->WorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員
	    customer->MdfDate = adoqueryPub->FieldByName("MdfDate")->AsString;
	    customer->MdfWorker = adoqueryPub->FieldByName("MdfWorker")->AsString;
	    //customer->TotalPoints = adoqueryPub->FieldByName("TotalPoints")->AsInteger;
	    //customer->Points = adoqueryPub->FieldByName("Points")->AsInteger;
	    customer->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      customer->NewId = adoqueryPub->FieldByName("NewId")->AsInteger;
      customer->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;

      if (CustExOpenId == 1)
      {
        custex->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
        for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
        {
          if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
          {
            strFieldName = "ItemData"+IntToStr(i+1);
            custex->Itemdata[i] = adoqueryPub->FieldByName(strFieldName)->AsString;
          }
        }
      }
      result = 0;
      //QueryCustExDataFromCRM(customer, custex); //2016-12-31
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
AnsiString TdmAgent::GetCustNameByCustNo(AnsiString custno)
{
  AnsiString sqlbuf;
  AnsiString strCustName="";

  if (custno == "")
    return strCustName;
  sqlbuf = "Select CustName from tbCustomer where CustomNo='"+custno + "'";
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    strCustName = adoqueryPub->FieldByName("CustName")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return strCustName;
}
int TdmAgent::GetCustomRecord(TADOQuery *adoquery, CCustomer *customer)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
    customer->id = adoquery->FieldByName("id")->AsInteger;
    customer->CustomNo = adoquery->FieldByName("CustomNo")->AsString;
    customer->CustName = adoquery->FieldByName("CustName")->AsString;
    customer->CorpName = adoquery->FieldByName("CorpName")->AsString;
    customer->CustType = adoquery->FieldByName("CustType")->AsInteger;
    //customer->CustTypeName = adoquery->FieldByName("CustTypeName")->AsString;
    customer->CustLevel = adoquery->FieldByName("CustLevel")->AsInteger;
    //customer->CustLevelName = adoquery->FieldByName("CustLevelName")->AsString;
    customer->CustClass = adoquery->FieldByName("CustClass")->AsInteger;
    //customer->CustClassName = adoquery->FieldByName("CustClassName")->AsString;
    //customer->PriceModule = adoquery->FieldByName("PriceModule")->AsInteger;
    //customer->Discount = adoquery->FieldByName("Discount")->AsInteger;
    customer->CustSex = adoquery->FieldByName("CustSex")->AsString;
    customer->MobileNo = adoquery->FieldByName("MobileNo")->AsString;
    customer->TeleNo = adoquery->FieldByName("TeleNo")->AsString;
    customer->FaxNo = adoquery->FieldByName("FaxNo")->AsString;
    customer->SubPhone = adoquery->FieldByName("SubPhone")->AsString;
    customer->EMail = adoquery->FieldByName("EMail")->AsString;
    customer->ContAddr = adoquery->FieldByName("ContAddr")->AsString;
    customer->PostNo = adoquery->FieldByName("PostNo")->AsString;
    customer->Province = adoquery->FieldByName("Province")->AsString;
    customer->CityName = adoquery->FieldByName("CityName")->AsString;
    customer->CountyName = adoquery->FieldByName("CountyName")->AsString;
    customer->ZoneName = adoquery->FieldByName("ZoneName")->AsString;
    //customer->EnsureMoney = adoquery->FieldByName("EnsureMoney")->AsFloat;
    customer->RegTime = adoquery->FieldByName("RegTime")->AsString;
    customer->RegWorker = adoquery->FieldByName("RegWorker")->AsString;
    //customer->RegWorkerName = adoquery->FieldByName("WorkerName")->AsString;

    customer->DepartmentId = adoquery->FieldByName("DepartmentID")->AsInteger; //2018-06-03 所屬部門
    customer->WorkerNo = adoquery->FieldByName("WorkerNo")->AsString; //2018-06-03 所屬值機員

    customer->MdfDate = adoquery->FieldByName("MdfDate")->AsString;
    customer->MdfWorker = adoquery->FieldByName("MdfWorker")->AsString;

    //customer->TotalPoints = adoquery->FieldByName("TotalPoints")->AsInteger;
    //customer->Points = adoquery->FieldByName("Points")->AsInteger;
    customer->Remark = adoquery->FieldByName("Remark")->AsString;
    customer->AccountNo = adoquery->FieldByName("AccountNo")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetCustExRecord(TADOQuery *adoquery, CCustEx *custex)
{
  AnsiString strFieldName;
  if (CustExOpenId == 0)
    return 1;
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
    custex->CustomNo = adoquery->FieldByName("CustomNo")->AsString;
    for (int i=0; i<MAX_CUST_CONTROL_NUM && i<FormMain->m_pCustExItemSetList->ItemNum; i++) //2017-01-02 edit
    {
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType == 4) //2016-12-31只讀允許的欄位
      {
        strFieldName = "ItemData"+IntToStr(i+1);
        custex->Itemdata[i] = adoquery->FieldByName(strFieldName)->AsString;
      }
    }
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertCustom(CCustomer *customer)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbCustomer (\
CustomNo,\
CustName,\
CorpName,\
CustType,\
CustLevel,\
CustClass,\
CustSex,\
MobileNo,\
TeleNo,\
FaxNo,\
SubPhone,\
EMail,\
ContAddr,\
PostNo,\
Province,\
CityName,\
CountyName,\
ZoneName,\
RegWorker,\
RegTime,\
DepartmentID,\
WorkerNo,\
MdfDate,\
NewId,\
AccountNo,\
Remark \
) values ('%s','%s','%s',%d,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%s,%d,'%s',%s,0,'%s','%s')",
      customer->CustomNo.c_str(),
      customer->CustName.c_str(),
      customer->CorpName.c_str(),
      customer->CustType,
      customer->CustLevel,
      customer->CustClass,
      customer->CustSex.c_str(),
      customer->MobileNo.c_str(),
      customer->TeleNo.c_str(),
      customer->FaxNo.c_str(),
      customer->SubPhone.c_str(),
      customer->EMail.c_str(),
      customer->ContAddr.c_str(),
      customer->PostNo.c_str(),
      customer->Province.c_str(),
      customer->CityName.c_str(),
      customer->CountyName.c_str(),
      customer->ZoneName.c_str(),
      MyWorker.WorkerNo.c_str(),
      MyNowFuncName.c_str(),
      customer->DepartmentId, //2018-06-03
      customer->WorkerNo.c_str(), //2018-06-03
      MyNowFuncName.c_str(),
      customer->AccountNo.c_str(),
      customer->Remark.c_str());
  try
  {
    WriteErrprMsg("InsertCustom sqls=%s", sqlbuf);
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertCustom(AnsiString customno, AnsiString mobileno, AnsiString teleno, AnsiString custname, AnsiString custaddr, AnsiString accountno, AnsiString itemdata1, AnsiString itemdata2, AnsiString itemdata3, AnsiString itemdata4)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into tbCustomer (CustomNo,MobileNo,TeleNo,CustName,ContAddr,AccountNo,NewId,ItemData1,ItemData2,ItemData3,ItemData4) values ('%s','%s','%s','%s','%s','%s',0,'%s','%s','%s','%s')",
      customno.c_str(), mobileno.c_str(), teleno.c_str(), custname.c_str(), custaddr.c_str(), accountno.c_str(), itemdata1.c_str(), itemdata2.c_str(), itemdata3.c_str(), itemdata4.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertCustom(AnsiString customno, AnsiString mobileno, AnsiString teleno, AnsiString custname, AnsiString custaddr, AnsiString accountno)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into tbCustomer (CustomNo,MobileNo,TeleNo,CustName,ContAddr,AccountNo,NewId) values ('%s','%s','%s','%s','%s','%s',0)",
      customno.c_str(), mobileno.c_str(), teleno.c_str(), custname.c_str(), custaddr.c_str(), accountno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateCustom(CCustomer *customer)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbCustomer set \
CustName='%s',\
CorpName='%s',\
CustType=%d,\
CustLevel=%d,\
CustClass=%d,\
CustSex='%s',\
MobileNo='%s',\
TeleNo='%s',\
FaxNo='%s',\
SubPhone='%s',\
EMail='%s',\
ContAddr='%s',\
PostNo='%s',\
Province='%s',\
CityName='%s',\
CountyName='%s',\
ZoneName='%s',\
RegWorker='%s',\
DepartmentID=%d,\
WorkerNo='%s',\
MdfDate=%s,\
MdfWorker='%s',\
NewId=0,\
AccountNo='%s',\
Remark='%s' \
where CustomNo='%s'",
      customer->CustName.c_str(),
      customer->CorpName.c_str(),
      customer->CustType,
      customer->CustLevel,
      customer->CustClass,
      customer->CustSex.c_str(),
      customer->MobileNo.c_str(),
      customer->TeleNo.c_str(),
      customer->FaxNo.c_str(),
      customer->SubPhone.c_str(),
      customer->EMail.c_str(),
      customer->ContAddr.c_str(),
      customer->PostNo.c_str(),
      customer->Province.c_str(),
      customer->CityName.c_str(),
      customer->CountyName.c_str(),
      customer->ZoneName.c_str(),
      MyWorker.WorkerNo.c_str(),
      customer->DepartmentId, //2018-06-03
      customer->WorkerNo.c_str(), //2018-06-03
      MyNowFuncName.c_str(),
      MyWorker.WorkerNo.c_str(),
      customer->AccountNo.c_str(),
      customer->Remark.c_str(),
      customer->CustomNo.c_str());
  try
  {
    WriteErrprMsg("UpdateCustom sqls=%s", sqlbuf);
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateCustEx(CCustEx &custex)
{
  int n=0;
  char sqlbuf1[4096], szTemp[256];

  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList->ItemNum == 0)
    return 0;
  memset(sqlbuf1, 0, 4096);
  sprintf(sqlbuf1, "update tbcustomer set ");
  for (int i=1; i<=MAX_CUST_CONTROL_NUM && i<=FormMain->m_pCustExItemSetList->ItemNum; i++) //2017-01-02 edit
  {
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i-1].SaveToDBFlag == 1) //2016-12-31只保存允許的欄位
    {
      if (n == 0)
        sprintf(szTemp,"ItemData%d=", i);
      else
        sprintf(szTemp,",ItemData%d=", i);
      strcat(sqlbuf1, szTemp);

      sprintf(szTemp,"'%s'", custex.Itemdata[i-1].c_str());
      strcat(sqlbuf1, szTemp);
      n++;
    }
  }
  sprintf(szTemp, " where CustomNo='%s'", custex.CustomNo.c_str());
  strcat(sqlbuf1, szTemp);
  try
  {
    WriteErrprMsg("UpdateCustEx sqls=%s", sqlbuf1);
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf1 );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteCustom(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCustomer where id='%d'", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteCustom(AnsiString custno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbCustomer where CustomNo='%s'", custno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::IsCustomNoExist(AnsiString custno)
{
  char sqlbuf[256];
  int result=0;

  sprintf(sqlbuf, "Select id from tbCustomer where CustomNo='%s'", custno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsCustomNoExist_PersonID(AnsiString custno, AnsiString &strID)
{
  char sqlbuf[256];
  int result=0;

  strID = "";
  sprintf(sqlbuf, "Select %s from tbCustomer where CustomNo='%s'", g_strIDFieldName.c_str(), custno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strID = adoqueryPub->FieldByName(g_strIDFieldName)->AsString;
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsCustomNoExist_AC(AnsiString custno, AnsiString &strAC)
{
  char sqlbuf[256];
  int result=0;

  strAC = "";
  sprintf(sqlbuf, "Select %s from tbCustomer where CustomNo='%s'", g_strACFieldName.c_str(), custno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strAC = adoqueryPub->FieldByName(g_strACFieldName)->AsString;
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsCustomTeleExist(AnsiString tele)
{
  char sqlbuf[512];
  int result=0;

  if (strDBType == "SQLSERVER")
    sprintf(sqlbuf, "Select id from tbCustomer where (right(MobileNo,10)=right('%s',10) and len(MobileNo)>=10) or (TeleNo=right('%s',len(TeleNo)) and len(TeleNo)>7) or (FaxNo=right('%s',len(FaxNo)) and len(FaxNo)>7)",
    tele.c_str(), tele.c_str(), tele.c_str());
  else if (strDBType == "MYSQL")
    sprintf(sqlbuf, "Select id from tbCustomer where (right(MobileNo,10)=right('%s',10) and LENGTH(MobileNo)>=10) or (TeleNo=right('%s',LENGTH(TeleNo)) and LENGTH(TeleNo)>7) or (FaxNo=right('%s',LENGTH(FaxNo)) and LENGTH(FaxNo)>7)",
    tele.c_str(), tele.c_str(), tele.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
//2017-10-08
int TdmAgent::SaveCustomerDataByID(AnsiString strID, CCustomer &customer, CCustEx &custex, AnsiString &strCustomNo)
{
  char sqlbuf[256];
  int result=1;
  bool bExist=false;
  AnsiString strID1;

  if (strID == "")
    return 1;
  sprintf(sqlbuf, "Select CustomNo from tbCustomer where %s='%s'", g_strIDFieldName.c_str(), strID.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strCustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
      bExist = true;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  WriteErrprMsg("SaveCustomerDataByID bExist=%d strID=%s strCustomNo=%s customer.CustomNo=%s", bExist, strID.c_str(), strCustomNo.c_str(), customer.CustomNo.c_str());
  if (bExist == true)
  {
    //該身份證已存在
    customer.CustomNo = strCustomNo;
    custex.CustomNo = customer.CustomNo;
    FormMain->edtCustomNo->Text = customer.CustomNo;
    MyAccept.CustomNo = customer.CustomNo;
    if (UpdateCustom(&customer) == 0)
    {
      UpdateCustEx(custex);
      UpdateAcceptCustomData(MyAccept);
      result = 0;
    }
  }
  else
  {
    //該身份證不存在
    MyAccept.CustomNo = customer.CustomNo;
    if (dmAgent->IsCustomNoExist_PersonID(customer.CustomNo, strID1) == 0)
    {
      //客戶編號也不存在
      if (InsertCustom(&customer) == 0)
      {
        UpdateCustEx(custex);
        UpdateAcceptCustomData(MyAccept);
        result = 0;
      }
    }
    else
    {
      if (strID1 == "" || strID == strID1)
      {
        //本客戶編號的記綠沒有身份證字號或相同
        if (UpdateCustom(&customer) == 0)
        {
          UpdateCustEx(custex);
          UpdateAcceptCustomData(MyAccept);
          result = 0;
        }
      }
      else
      {
        if (MyAccept.SerialNo != "")
          customer.CustomNo = MyAccept.SerialNo;
        else
          customer.CustomNo = MyGetCustomNo();
        custex.CustomNo = customer.CustomNo;
        FormMain->edtCustomNo->Text = customer.CustomNo;
        MyAccept.CustomNo = customer.CustomNo;
        if (InsertCustom(&customer) == 0)
        {
          UpdateCustEx(custex);
          UpdateAcceptCustomData(MyAccept);
          result = 0;
        }
      }
    }
  }

  return result;
}
//2017-10-08
int TdmAgent::SaveCustomerDataByAC(AnsiString strAC, CCustomer &customer, CCustEx &custex, AnsiString &strCustomNo)
{
  char sqlbuf[256];
  int result=1;
  bool bExist=false;
  AnsiString strAC1;

  if (strAC == "")
    return 1;
  sprintf(sqlbuf, "Select CustomNo from tbCustomer where %s='%s'", g_strACFieldName.c_str(), strAC.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strCustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
      bExist = true;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  WriteErrprMsg("SaveCustomerDataByAC bExist=%d strAC=%s strCustomNo=%s customer.CustomNo=%s", bExist, strAC.c_str(), strCustomNo.c_str(), customer.CustomNo.c_str());
  if (bExist == true)
  {
    //該會員已存在
    customer.CustomNo = strCustomNo;
    custex.CustomNo = customer.CustomNo;
    FormMain->edtCustomNo->Text = customer.CustomNo;
    MyAccept.CustomNo = customer.CustomNo;
    if (UpdateCustom(&customer) == 0)
    {
      UpdateCustEx(custex);
      UpdateAcceptCustomData(MyAccept);
      result = 0;
    }
  }
  else
  {
    //該會員證不存在
    MyAccept.CustomNo = customer.CustomNo;
    if (dmAgent->IsCustomNoExist_AC(customer.CustomNo, strAC1) == 0)
    {
      //客戶編號也不存在
      if (InsertCustom(&customer) == 0)
      {
        UpdateCustEx(custex);
        UpdateAcceptCustomData(MyAccept);
        result = 0;
      }
    }
    else
    {
      if (strAC1 == "" || strAC == strAC1)
      {
        //本會員編號的記綠沒有身份證字號或相同
        if (UpdateCustom(&customer) == 0)
        {
          UpdateCustEx(custex);
          UpdateAcceptCustomData(MyAccept);
          result = 0;
        }
      }
      else
      {
        if (MyAccept.SerialNo != "")
          customer.CustomNo = MyAccept.SerialNo;
        else
          customer.CustomNo = MyGetCustomNo();
        custex.CustomNo = customer.CustomNo;
        FormMain->edtCustomNo->Text = customer.CustomNo;
        MyAccept.CustomNo = customer.CustomNo;
        if (InsertCustom(&customer) == 0)
        {
          UpdateCustEx(custex);
          UpdateAcceptCustomData(MyAccept);
          result = 0;
        }
      }
    }
  }

  return result;
}
//2017-10-08
int TdmAgent::SaveCustomerDataByTEL(AnsiString strTEL, CCustomer &customer, CCustEx &custex, AnsiString &strCustomNo)
{
  char sqlbuf[256];
  int result=1;
  bool bExist=false;

  if (strTEL == "")
    return 1;
  sprintf(sqlbuf, "Select CustomNo from tbCustomer where %s='%s'", g_strTELFieldName.c_str(), strTEL.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strCustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
      bExist = true;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  WriteErrprMsg("SaveCustomerDataByTEL bExist=%d strTEL=%s strCustomNo=%s customer.CustomNo=%s", bExist, strTEL.c_str(), strCustomNo.c_str(), customer.CustomNo.c_str());
  if (bExist == true)
  {
    //該號碼已存在
    customer.CustomNo = strCustomNo;
    custex.CustomNo = customer.CustomNo;
    FormMain->edtCustomNo->Text = customer.CustomNo;
    MyAccept.CustomNo = customer.CustomNo;
    if (UpdateCustom(&customer) == 0)
    {
      UpdateCustEx(custex);
      UpdateAcceptCustomData(MyAccept);
      result = 0;
    }
  }
  else
  {
    //該號碼不存在
    if (dmAgent->IsCustomNoExist(customer.CustomNo) == 0)
    {
      //客戶編號也不存在
      if (InsertCustom(&customer) == 0)
      {
        UpdateCustEx(custex);
        UpdateAcceptCustomData(MyAccept);
        result = 0;
      }
    }
    else
    {
      if (MyAccept.SerialNo != "")
        customer.CustomNo = MyAccept.SerialNo;
      else
        customer.CustomNo = MyGetCustomNo();
      custex.CustomNo = customer.CustomNo;
      FormMain->edtCustomNo->Text = customer.CustomNo;
      MyAccept.CustomNo = customer.CustomNo;

      if (InsertCustom(&customer) == 0)
      {
        UpdateCustEx(custex);
        UpdateAcceptCustomData(MyAccept);
        result = 0;
      }
    }
  }

  return result;
}
int TdmAgent::GetCustomerNoByTEL(AnsiString strTEL, AnsiString &strCustomNo, AnsiString &strID, AnsiString &strAC)
{
  char sqlbuf[256];
  int result=1;

  strCustomNo="";
  sprintf(sqlbuf, "Select CustomNo,%s,%s from tbCustomer where %s='%s'", g_strIDFieldName.c_str(), g_strACFieldName.c_str(), g_strTELFieldName.c_str(), strTEL.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strCustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
      strID = adoqueryPub->FieldByName(g_strIDFieldName)->AsString;
      strAC = adoqueryPub->FieldByName(g_strACFieldName)->AsString;
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  WriteErrprMsg("GetCustomerNoByTEL result=%d strTEL=%s strCustomNo=%s strID=%s strAC=%s", result, strTEL.c_str(), strCustomNo.c_str(), strID.c_str(), strAC.c_str());
  return result;
}
//返回結果：1-存在相同的客戶名稱 2-存在相同的電話號碼
int TdmAgent::IsCustomerExist(AnsiString CustName, AnsiString tele1, AnsiString tele2, AnsiString tele3, bool bSameCustName)
{
  char sqlbuf[512];
  int result=0;
  AnsiString strTemp, strLocalPhone="";
  if (CustName == "" && tele1 == "" && tele2 == "" && tele3 == "")
    return 0;
  if (bSameCustName == false)
  {
    //不允許存在相同的客戶名稱
    if (strDBType == "SQLSERVER")
    {
      sprintf(sqlbuf, "Select id from tbCustomer where CustName='%s'",
        CustName.c_str());
    }
    else if (strDBType == "MYSQL")
    {
      sprintf(sqlbuf, "Select id from tbCustomer where CustName='%s'",
        CustName.c_str());
    }
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->Open();

      if ( !adoqueryPub->Eof )
      {
        result = 1;
      }
      adoqueryPub->Close();
    }
    catch ( ... )
    {
    }
    if (result > 0)
      return result;
  }
  if (LocaAreaCode.Length() > 2 && tele2.Length() > 7)
  {
    strTemp = tele2.SubString(1, LocaAreaCode.Length());
    if (strTemp == LocaAreaCode)
    {
      strLocalPhone = tele2.SubString(LocaAreaCode.Length(), 20);
    }
  }
  if (strDBType == "SQLSERVER")
  {
    if (strLocalPhone == "")
      sprintf(sqlbuf, "Select id from tbCustomer where (len(MobileNo)>0 and MobileNo='%s') or (len(TeleNo)>0 and TeleNo='%s')",
        tele1.c_str(), tele2.c_str());
    else
      sprintf(sqlbuf, "Select id from tbCustomer where (len(MobileNo)>0 and MobileNo='%s') or (len(TeleNo)>0 and TeleNo='%s') or (len(TeleNo)>0 and TeleNo='%s')",
        tele1.c_str(), tele2.c_str(), LocaAreaCode.c_str());
  }
  else if (strDBType == "MYSQL")
  {
    if (strLocalPhone == "")
      sprintf(sqlbuf, "Select id from tbCustomer where (length(MobileNo)>0 and MobileNo='%s') or (length(TeleNo)>0 and TeleNo='%s')",
        tele1.c_str(), tele2.c_str());
    else
      sprintf(sqlbuf, "Select id from tbCustomer where (length(MobileNo)>0 and MobileNo='%s') or (length(TeleNo)>0 and TeleNo='%s') or (length(TeleNo)>0 and TeleNo='%s')",
        tele1.c_str(), tele2.c_str(), LocaAreaCode.c_str());
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 2;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsCustomerExist1(AnsiString CustName, AnsiString tele1, bool bSameCustName, bool bSameMobile)
{
  char sqlbuf[512];
  int result=0;

  if (bSameCustName == false)
  {
    //不允許存在相同的客戶名稱
    if (strDBType == "SQLSERVER")
    {
      sprintf(sqlbuf, "Select id from tbCustomer where CustName='%s'",
        CustName.c_str());
    }
    else if (strDBType == "MYSQL")
    {
      sprintf(sqlbuf, "Select id from tbCustomer where CustName='%s'",
        CustName.c_str());
    }
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->Open();

      if ( !adoqueryPub->Eof )
      {
        result = 1;
      }
      adoqueryPub->Close();
    }
    catch ( ... )
    {
    }
    if (result > 0)
      return result;
  }
  if (bSameMobile == false)
  {
    //不允許存在相同的手機號碼
    if (strDBType == "SQLSERVER")
    {
      sprintf(sqlbuf, "Select id from tbCustomer where len(MobileNo)>0 and MobileNo='%s'",
          tele1.c_str());
    }
    else if (strDBType == "MYSQL")
    {
      sprintf(sqlbuf, "Select id from tbCustomer where length(MobileNo)>0 and MobileNo='%s'",
          tele1.c_str());
    }
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->Open();

      if ( !adoqueryPub->Eof )
      {
        result = 2;
      }
      adoqueryPub->Close();
    }
    catch ( ... )
    {
    }
  }
  return result;
}
int TdmAgent::UpdateCustomPoints(AnsiString custno, int points, int oldpoints)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbCustomer set TotalPoints=TotalPoints+%d-%d,Points=%d where CustomNo='%s'",
    points, oldpoints, points, custno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetCustTypeByCustNo(AnsiString custno)
{
  char sqlbuf[512];
  int result=-1;

  sprintf(sqlbuf, "Select CustType from tbCustomer where CustomNo='%s'", custno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = adoqueryPub->FieldByName("CustType")->AsInteger;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::QueryCustomByParam(AnsiString strParam, CCustomer *customer)
{
  return 0;
}
void TdmAgent::UpdateAcceptCustomNo(CAccept *accept)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbaccept set CustomNo='%s' where GId='%s'",
    accept->CustomNo.c_str(), accept->GId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateCDRCustomNo(CAccept *accept)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbCallcdr set CustomNo='%s' where SerialNo='%s'",
    accept->CustomNo.c_str(), accept->SerialNo.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//-----------------------------------------------------------------------------
void __fastcall TdmAgent::adoQryCurAcceptCalcFields(TDataSet *DataSet)
{
  adoQryCurAcceptAcceptScoreName->AsString = TalkScoreNameList.GetItemName(adoQryCurAcceptAcceptScore->AsInteger);
  adoQryCurAcceptWorkerName->AsString = GetWorkerName(adoQryCurAcceptWorkerNo->AsString);
  //adoQryCurAcceptCustName->AsString = GetCustNameByCustNo(adoQryCurAcceptCustomNo->AsString);

  adoQryCurAcceptDispProdType->AsString = ProdTypeItemList.GetItemName(adoQryCurAcceptProdType->AsInteger);
  adoQryCurAcceptDispProdModelType->AsString = ProdModelTypeItemList.GetItemName(adoQryCurAcceptProdType->AsInteger, adoQryCurAcceptProdModelType->AsInteger);
  adoQryCurAcceptSrvName->AsString = SrvTypeItemList.GetItemName(adoQryCurAcceptProdType->AsInteger, adoQryCurAcceptAcptType->AsInteger);
  adoQryCurAcceptDispSrvSubType->AsString = SrvSubTypeItemList.GetItemName(adoQryCurAcceptProdType->AsInteger, adoQryCurAcceptAcptType->AsInteger, adoQryCurAcceptSrvSubType->AsInteger);

  adoQryCurAcceptAcptSubTypeName->AsString = AcptSubTypeItemList.GetItemName(adoQryCurAcceptAcptSubType->AsInteger);
  adoQryCurAcceptAcceptProcName->AsString = AcceptProcItemList.GetItemName(adoQryCurAcceptDealState->AsInteger);
  //adoQryCurAcceptTalkScoreName->AsString = GetTalkScoreName(adoQryCurAcceptSerialNo->AsString);

  if (adoQryCurAcceptAnsId->AsInteger == 1)
  {
    if (IsRecdFileExist(adoQryCurAcceptSerialNo->AsString) == 1)
      adoQryCurAcceptDispRecdFile->AsString = "有";
    else
      adoQryCurAcceptDispRecdFile->AsString = "無";

    adoQryCurAcceptDispAnsId->AsString = "是";
  }
  else if (adoQryCurAcceptAnsId->AsInteger == 2)
  {
    adoQryCurAcceptDispRecdFile->AsString = "無";
    adoQryCurAcceptDispAnsId->AsString = "IVR";
  }
  else
  {
    adoQryCurAcceptDispRecdFile->AsString = "無";
    adoQryCurAcceptDispAnsId->AsString = "否";
  }
  if (adoQryCurAcceptAcptCont->AsString.Length() >= 500)
    adoQryCurAcceptDispAcptCont->AsString = adoQryCurAcceptAcptCont->AsString.SubString(1, 498);
  else
    adoQryCurAcceptDispAcptCont->AsString = adoQryCurAcceptAcptCont->AsString;
  adoQryCurAcceptDispCustomTel->AsString = GetHidePhoneNum(2, adoQryCurAcceptCustomTel->AsString);
}
//---------------------------------------------------------------------------

int TdmAgent::GetSubPhoneRecord(TADOQuery *adoquery, CSubPhone *subphone)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  subphone->id = adoquery->FieldByName("id")->AsInteger;
    subphone->SubPhone = adoquery->FieldByName("SubPhone")->AsString;
    subphone->Password = adoquery->FieldByName("Password")->AsString;
	  subphone->UserName = adoquery->FieldByName("UserName")->AsString;
	  subphone->CallRight = adoquery->FieldByName("CallRight")->AsInteger;
	  subphone->Mobile = adoquery->FieldByName("Mobile")->AsString;
	  subphone->BusyTran = adoquery->FieldByName("BusyTran")->AsString;
	  subphone->NoAnsTran = adoquery->FieldByName("NoAnsTran")->AsString;
	  subphone->BangMode = adoquery->FieldByName("BangMode")->AsInteger;
	  subphone->CRBT = adoquery->FieldByName("CRBT")->AsString;
	  subphone->VoiceBox = adoquery->FieldByName("VoiceBox")->AsInteger;
	  subphone->Remark = adoquery->FieldByName("Remark")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetSubPhoneRecord(AnsiString subcode, CSubPhone *subphone)
{
  char sqlbuf[256];
  int nResult = 1;

  sprintf(sqlbuf, "Select * from tbSubPhone where SubPhone='%s'", subcode.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    subphone->id = adoqueryPub->FieldByName("id")->AsInteger;
      subphone->SubPhone = adoqueryPub->FieldByName("SubPhone")->AsString;
      subphone->Password = adoqueryPub->FieldByName("Password")->AsString;
	    subphone->UserName = adoqueryPub->FieldByName("UserName")->AsString;
	    subphone->CallRight = adoqueryPub->FieldByName("CallRight")->AsInteger;
	    subphone->Mobile = adoqueryPub->FieldByName("Mobile")->AsString;
	    subphone->BusyTran = adoqueryPub->FieldByName("BusyTran")->AsString;
	    subphone->NoAnsTran = adoqueryPub->FieldByName("NoAnsTran")->AsString;
	    subphone->BangMode = adoqueryPub->FieldByName("BangMode")->AsInteger;
	    subphone->CRBT = adoqueryPub->FieldByName("CRBT")->AsString;
	    subphone->VoiceBox = adoqueryPub->FieldByName("VoiceBox")->AsInteger;
	    subphone->Remark = adoqueryPub->FieldByName("Remark")->AsString;
      nResult = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::InsertSubPhoneRecord(CSubPhone *subphone)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbSubPhone (\
SubPhone,\
Password,\
UserName,\
CallRight,\
Mobile,\
BusyTran,\
NoAnsTran,\
BangMode,\
CRBT,\
VoiceBox,\
Remark\
) values ('%s','%s','%s',%d,'%s','%s','%s',%d,'%s',%d,'%s')",
    subphone->SubPhone.c_str(),
    "123456",
    subphone->UserName.c_str(),
    subphone->CallRight,
    subphone->Mobile.c_str(),
    subphone->BusyTran.c_str(),
    subphone->NoAnsTran.c_str(),
    subphone->BangMode,
    subphone->CRBT.c_str(),
    subphone->VoiceBox,
	  subphone->Remark.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateSubPhoneRecord(CSubPhone *subphone)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbSubPhone set \
Password='%s',\
UserName='%s',\
CallRight=%d,\
Mobile='%s',\
BusyTran='%s',\
NoAnsTran='%s',\
BangMode=%d,\
CRBT='%s',\
VoiceBox=%d,\
Remark='%s' \
where id=%d",
    subphone->Password.c_str(),
    subphone->UserName.c_str(),
    subphone->CallRight,
    subphone->Mobile.c_str(),
    subphone->BusyTran.c_str(),
    subphone->NoAnsTran.c_str(),
    subphone->BangMode,
    subphone->CRBT.c_str(),
    subphone->VoiceBox,
	  subphone->Remark.c_str(),
    subphone->id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteSubPhoneRecord(AnsiString subcode)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbSubPhone where SubPhone='%s'", subcode.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//-----------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryDirectPhoneCalcFields(TDataSet *DataSet)
{
  if (adoQryDirectPhonePhoneType->AsInteger == 1)
    adoQryDirectPhoneDispPhoneType->AsString = "總機號碼";
  else
    adoQryDirectPhoneDispPhoneType->AsString = "直撥號碼";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::DataModuleCreate(TObject *Sender)
{
  ADOConnection1->Connected = false;  
}
//---------------------------------------------------------------------------

int TdmAgent::GetAcceptGroupRecord(TADOQuery *adoquery, CAcceptGroup *acceptgroup)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  acceptgroup->id = adoquery->FieldByName("id")->AsInteger;
	  acceptgroup->CalledNo = adoquery->FieldByName("CalledNo")->AsString;
	  acceptgroup->DTMFKey = adoquery->FieldByName("DTMFKey")->AsString;

	  acceptgroup->GroupNoTel1 = adoquery->FieldByName("GroupNo1")->AsString;
	  acceptgroup->GroupNoTel2 = adoquery->FieldByName("GroupNo2")->AsString;
	  acceptgroup->GroupNoTel3 = adoquery->FieldByName("GroupNo3")->AsString;
	  acceptgroup->GroupNoTel4 = adoquery->FieldByName("GroupNo4")->AsString;
	  acceptgroup->GroupNoTel5 = adoquery->FieldByName("GroupNo5")->AsString;

    if (acceptgroup->GroupNoTel1.Length() < 6)
	    acceptgroup->GroupNo1 = adoquery->FieldByName("GroupNo1")->AsInteger;
    else
	    acceptgroup->GroupNo1 = -1;
    acceptgroup->GroupNo1Type = adoquery->FieldByName("GroupNo1Type")->AsInteger;

    if (acceptgroup->GroupNoTel2.Length() < 6)
	    acceptgroup->GroupNo2 = adoquery->FieldByName("GroupNo2")->AsInteger;
    else
	    acceptgroup->GroupNo2 = -1;
    acceptgroup->GroupNo2Type = adoquery->FieldByName("GroupNo2Type")->AsInteger;

    if (acceptgroup->GroupNoTel3.Length() < 6)
	    acceptgroup->GroupNo3 = adoquery->FieldByName("GroupNo3")->AsInteger;
    else
	    acceptgroup->GroupNo3 = -1;
    acceptgroup->GroupNo3Type = adoquery->FieldByName("GroupNo3Type")->AsInteger;

    if (acceptgroup->GroupNoTel4.Length() < 6)
	    acceptgroup->GroupNo4 = adoquery->FieldByName("GroupNo4")->AsInteger;
    else
	    acceptgroup->GroupNo4 = -1;
    acceptgroup->GroupNo4Type = adoquery->FieldByName("GroupNo4Type")->AsInteger;

    if (acceptgroup->GroupNoTel5.Length() < 6)
	    acceptgroup->GroupNo5 = adoquery->FieldByName("GroupNo5")->AsInteger;
    else
	    acceptgroup->GroupNo5 = -1;
    acceptgroup->GroupNo5Type = adoquery->FieldByName("GroupNo5Type")->AsInteger;

	  acceptgroup->SrvType = adoquery->FieldByName("SrvType")->AsInteger;
    acceptgroup->SrvName = adoquery->FieldByName("SrvName")->AsString;
	  acceptgroup->CalledType = adoquery->FieldByName("CalledType")->AsInteger;
	  acceptgroup->DTMFType = adoquery->FieldByName("DTMFType")->AsInteger;
	  acceptgroup->VocId = adoquery->FieldByName("VocId")->AsInteger;
    acceptgroup->CorpVoc = adoquery->FieldByName("CorpVoc")->AsString;
    acceptgroup->MenuVoc = adoquery->FieldByName("MenuVoc")->AsString;
	  acceptgroup->WorkerNoId = adoquery->FieldByName("WorkerNoId")->AsInteger;
	  acceptgroup->MenuKeys = adoquery->FieldByName("MenuKeys")->AsString;

	  acceptgroup->MenuDtmfACK = adoquery->FieldByName("MenuDtmfACK")->AsInteger;
	  acceptgroup->DtmfACKVoc = adoquery->FieldByName("DtmfACKVoc")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertAcceptGroupRecord(CAcceptGroup *acceptgroup)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into tbAcceptGroup (\
CalledNo,\
DTMFKey,\
GroupNo1,\
GroupNo2,\
GroupNo3,\
GroupNo4,\
GroupNo5,\
GroupNo1Type,\
GroupNo2Type,\
GroupNo3Type,\
GroupNo4Type,\
GroupNo5Type,\
SrvType,\
SrvName,\
CalledType,\
DTMFType,\
VocId,\
CorpVoc,\
MenuVoc,\
WorkerNoId,\
MenuKeys,\
MenuDtmfACK,\
DtmfACKVoc\
) values ('%s','%s','%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,%d,'%s',%d,%d,%d,'%s','%s',%d,'%s',%d,'%s')",
  acceptgroup->CalledNo.c_str(),
  acceptgroup->DTMFKey.c_str(),
  acceptgroup->GroupNoTel1.c_str(),
  acceptgroup->GroupNoTel2.c_str(),
  acceptgroup->GroupNoTel3.c_str(),
  acceptgroup->GroupNoTel4.c_str(),
  acceptgroup->GroupNoTel5.c_str(),
  acceptgroup->GroupNo1Type,
  acceptgroup->GroupNo2Type,
  acceptgroup->GroupNo3Type,
  acceptgroup->GroupNo4Type,
  acceptgroup->GroupNo5Type,
  acceptgroup->SrvType,
  acceptgroup->SrvName.c_str(),
  acceptgroup->CalledType,
  acceptgroup->DTMFType,
  acceptgroup->VocId,
  acceptgroup->CorpVoc.c_str(),
  acceptgroup->MenuVoc.c_str(),
  acceptgroup->WorkerNoId,
  acceptgroup->MenuKeys.c_str(),
  acceptgroup->MenuDtmfACK,
  acceptgroup->DtmfACKVoc.c_str()
  );

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateAcceptGroupRecord(CAcceptGroup *acceptgroup)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbAcceptGroup set \
CalledNo='%s',\
DTMFKey='%s',\
GroupNo1='%s',\
GroupNo2='%s',\
GroupNo3='%s',\
GroupNo4='%s',\
GroupNo5='%s',\
GroupNo1Type=%d,\
GroupNo2Type=%d,\
GroupNo3Type=%d,\
GroupNo4Type=%d,\
GroupNo5Type=%d,\
SrvType=%d,\
SrvName='%s',\
CalledType=%d,\
DTMFType=%d,\
VocId=%d,\
CorpVoc='%s',\
MenuVoc='%s',\
WorkerNoId=%d,\
MenuKeys='%s',\
MenuDtmfACK=%d,\
DtmfACKVoc='%s' \
where id=%d",
  acceptgroup->CalledNo.c_str(),
  acceptgroup->DTMFKey.c_str(),
  acceptgroup->GroupNoTel1.c_str(),
  acceptgroup->GroupNoTel2.c_str(),
  acceptgroup->GroupNoTel3.c_str(),
  acceptgroup->GroupNoTel4.c_str(),
  acceptgroup->GroupNoTel5.c_str(),
  acceptgroup->GroupNo1Type,
  acceptgroup->GroupNo2Type,
  acceptgroup->GroupNo3Type,
  acceptgroup->GroupNo4Type,
  acceptgroup->GroupNo5Type,
  acceptgroup->SrvType,
  acceptgroup->SrvName.c_str(),
  acceptgroup->CalledType,
  acceptgroup->DTMFType,
  acceptgroup->VocId,
  acceptgroup->CorpVoc.c_str(),
  acceptgroup->MenuVoc.c_str(),
  acceptgroup->WorkerNoId,
  acceptgroup->MenuKeys.c_str(),
  acceptgroup->MenuDtmfACK,
  acceptgroup->DtmfACKVoc.c_str(),
  acceptgroup->id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteAcceptGroupRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbAcceptGroup where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::GetRecdRecord(TADOQuery *adoquery, CRecdRecord *recd)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  recd->id = adoquery->FieldByName("id")->AsInteger;
	  recd->SerialNo = adoquery->FieldByName("SerialNo")->AsString;
	  recd->CallerNo = adoquery->FieldByName("CallerNo")->AsString;
	  recd->CustNo = adoquery->FieldByName("CustNo")->AsString;
	  recd->CalledNo = adoquery->FieldByName("CalledNo")->AsString;
	  recd->CallTime = adoquery->FieldByName("CallTime")->AsString;
	  recd->RecdFile = adoquery->FieldByName("RecdFile")->AsString;
	  recd->ListenId = adoquery->FieldByName("ListenId")->AsInteger;
	  recd->RecdContent = adoquery->FieldByName("RecdContent")->AsString;
	  recd->ListenWorkerNo = adoquery->FieldByName("ListenWorkerNo")->AsString;
	  recd->ListenTime = adoquery->FieldByName("ListenTime")->AsString;
	  recd->ProcId = adoquery->FieldByName("ProcId")->AsInteger;
	  recd->ProcNote = adoquery->FieldByName("ProcNote")->AsString;
	  recd->ProcWorkerNo = adoquery->FieldByName("ProcWorkerNo")->AsString;
	  recd->ProcTime = adoquery->FieldByName("ProcTime")->AsString;

    recd->CustName = adoquery->FieldByName("CustName")->AsString;
    recd->ContAddr = adoquery->FieldByName("ContAddr")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::UpdateRecdListenContent(int id, int ListenId, AnsiString listenContent, AnsiString workerno)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbRecord set ListenId=%d,RecdContent='%s',ListenWorkerNo='%s',ListenTime=%s where id=%d",
    ListenId, listenContent.c_str(), workerno.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecdProcNote(int id, int ProcId, AnsiString ProcNote, AnsiString workerno)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbRecord set ProcId=%d,ProcNote='%s',ProcWorkerNo='%s',ProcTime=%s where id=%d",
    ProcId, ProcNote.c_str(), workerno.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetNewRecdCount()
{
  char sqlbuf[256];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbRecord where CallType>=0 and ListenId=0 and ProcId=0 and GETDATE()<dateadd(d,7,CallTime)");
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
    FormMain->lbNewRecd->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbNewRecd->Color = clBtnFace;
      FormMain->Label71->Color = clBtnFace;
    }
    else
    {
      FormMain->lbNewRecd->Color = clRed;
      FormMain->Label71->Color = clRed;
    }
    return recordcount;
  }
  catch ( ... )
  {
    FormMain->lbNewRecd->Color = clBtnFace;
    FormMain->lbNewRecd->Caption = 0;
  }
  return recordcount;
}
int TdmAgent::GetNewRecdCountEx()
{
  char sqlbuf[256];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as newcount from tbRecord where CallType>=0 and ListenId=0 and ProcId=0 and GETDATE()<dateadd(d,7,CallTime)");
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    adoQrySms->Close();
  }
  catch ( ... )
  {
  }
  return recordcount;
}
void __fastcall TdmAgent::adoQryRecordCalcFields(TDataSet *DataSet)
{
  adoQryRecordDispListenId->AsString = RecdListenIdItemList.GetItemName(adoQryRecordListenId->AsInteger);
  adoQryRecordDispProcId->AsString = RecdProcTypeItemList.GetItemName(adoQryRecordProcId->AsInteger);
  switch (adoQryRecordCallType->AsInteger)
  {
  case 0:
    adoQryRecordDispRecdId->AsString = "未接來電";
    break;
  case 1:
    adoQryRecordDispRecdId->AsString = "下班來電留言";
    break;
  case 2:
    adoQryRecordDispRecdId->AsString = "上班來電留言";
    break;
  default:
    adoQryRecordDispRecdId->AsString = "未接來電";
    break;
  }
}
int TdmAgent::UpdateRfaxReadContent(int id, int ReadId, AnsiString faxContent, AnsiString workerno)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvfax set ReadId=%d,FaxContent='%s',ReadWorkerNo='%d',ReadTime=%s where id=%d",
    ReadId, faxContent.c_str(), workerno.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRfaxProcNote(int id, int ProcId, AnsiString ProcNote, AnsiString workerno)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvfax set ProcId=%d,ProcNote='%s',ProcWorkerNo='%d',ProcTime=%s where id=%d",
    ProcId, ProcNote.c_str(), workerno.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetNewRfaxCount()
{
  char sqlbuf[256];
  int recordcount=0;

  if (MaxFaxNum == 0)
    return 0;
  if (ADOConnection1->Connected == false)
    return 0;
  if (MyWorker.WorkType < 3)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvfax where ReadId=0 and ProcId=0 and RecvWorkerNo='%s'",
      MyWorker.WorkerNo.c_str());
  else if (MyWorker.WorkType < 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvfax where ReadId=0 and ProcId=0 and (RecvDepartmentId=%d or RecvWorkerNo='%s')",
      MyWorker.DepartmentID, MyWorker.WorkerNo.c_str());
  else if (MyWorker.WorkType == 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvfax where ReadId=0 and ProcId=0");
  else
    return 0;
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    FormMain->lbRecvNewFax->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbRecvNewFax->Color = clBtnFace;
      FormMain->Label156->Color = clBtnFace;
    }
    else
    {
      FormMain->lbRecvNewFax->Color = clRed;
      FormMain->Label156->Color = clRed;
    }
    return recordcount;
  }
  catch ( Exception &exception )
  {
    if (exception.Message.AnsiPos("連線失敗") > 0 || exception.Message.AnsiPos("不存在或拒絕存取。") > 0 || exception.Message.AnsiPos("逾時過期") > 0)
      ADOConnection1->Connected = false;
    FormMain->Label156->Color = clBtnFace;
    FormMain->lbRecvNewFax->Color = clBtnFace;
    FormMain->lbRecvNewFax->Caption = 0;
  }
  return recordcount;
}
int TdmAgent::GetNewRfaxCountEx()
{
  char sqlbuf[256];
  int recordcount=0;

  if (MaxFaxNum == 0)
    return 0;
  if (ADOConnection1->Connected == false)
    return 0;
  if (MyWorker.WorkType < 3)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvfax where ReadId=0 and ProcId=0 and RecvWorkerNo='%s'",
      MyWorker.WorkerNo.c_str());
  else if (MyWorker.WorkType < 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvfax where ReadId=0 and ProcId=0 and (RecvDepartmentId=%d or RecvWorkerNo='%s')",
      MyWorker.DepartmentID, MyWorker.WorkerNo.c_str());
  else if (MyWorker.WorkType == 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvfax where ReadId=0 and ProcId=0");
  else
    return 0;
  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
  }
  catch ( Exception &exception )
  {
  }
  return recordcount;
}
int TdmAgent::GetNewEmailCount()
{
  char sqlbuf[256];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  if (MyWorker.WorkType < 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvemail where EmailType=3 and emlTo='%s' and ProcFlag=0 and RecvFinishedId=1", MyWorker.Email.c_str());
  else if (MyWorker.WorkType == 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvemail where EmailType=2 and ProcFlag=0 and RecvFinishedId=1");

  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
    FormMain->lbRecvNewEmail->Caption = recordcount;
    if (recordcount == 0)
    {
      FormMain->lbRecvNewEmail->Color = clBtnFace;
      FormMain->Label332->Color = clBtnFace;
    }
    else
    {
      FormMain->lbRecvNewEmail->Color = clRed;
      FormMain->Label332->Color = clRed;
    }
    return recordcount;
  }
  catch ( Exception &exception )
  {
    if (exception.Message.AnsiPos("連線失敗") > 0 || exception.Message.AnsiPos("不存在或拒絕存取。") > 0 || exception.Message.AnsiPos("逾時過期") > 0)
      ADOConnection1->Connected = false;
    FormMain->Label332->Color = clBtnFace;
    FormMain->lbRecvNewEmail->Color = clBtnFace;
    FormMain->lbRecvNewEmail->Caption = 0;
  }
  return recordcount;
}
int TdmAgent::GetNewEmailCountEx()
{
  char sqlbuf[256];
  int recordcount=0;

  if (ADOConnection1->Connected == false)
    return 0;
  if (MyWorker.WorkType < 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvemail where EmailType=3 and emlTo='%s' and ProcFlag=0 and RecvFinishedId=1", MyWorker.Email.c_str());
  else if (MyWorker.WorkType == 9)
    sprintf(sqlbuf, "select count(*) as newcount from tbrecvemail where EmailType=2 and ProcFlag=0 and RecvFinishedId=1");

  try
  {
    adoQrySms->SQL->Clear();
    adoQrySms->SQL->Add((char *)sqlbuf);
    adoQrySms->Open();
    recordcount = adoQrySms->FieldByName("newcount")->AsInteger;
  }
  catch ( Exception &exception )
  {
  }
  return recordcount;
}
//---------------------------------------------------------------------------
int TdmAgent::IsProdTypeExist(int nProdType)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select ProdType from tbProdType where ProdType=%d", nProdType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertProdTypeRecord(int nProdType, AnsiString ProdName, AnsiString strGroupNo, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbProdType (ProdType,ProdName,GroupNoList,URLBtnName,ProdURL,ProdDemo,DispOrder) values (%d,'%s','%s','%s','%s','%s',%d)",
    nProdType, ProdName.c_str(), strGroupNo.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateProdTypeRecord(int nProdType, AnsiString ProdName, AnsiString strGroupNo, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbProdType set ProdName='%s',GroupNoList='%s',URLBtnName='%s',ProdURL='%s',ProdDemo='%s',DispOrder=%d where ProdType=%d",
    ProdName.c_str(), strGroupNo.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder, nProdType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteProdTypeRecord(int nProdType)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbProdType where ProdType=%d", nProdType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}

int TdmAgent::IsSrvTypeExist(int nProdType, int nSrvType)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select SrvType from tbSrvType where ProdType=%d and SrvType=%d", nProdType, nSrvType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}

int TdmAgent::InsertSrvTypeRecord(int nProdType, int nSrvType, AnsiString SrvName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbSrvType (ProdType,SrvType,SrvName,URLBtnName,SrvURL,SrvDemo,DispOrder) values (%d,%d,'%s','%s','%s','%s',%d)",
    nProdType, nSrvType, SrvName.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateSrvTypeRecord(int nProdType, int nSrvType, AnsiString SrvName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbSrvType set SrvName='%s',URLBtnName='%s',SrvURL='%s',SrvDemo='%s',DispOrder=%d where ProdType=%d and SrvType=%d",
    SrvName.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder, nProdType, nSrvType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteSrvTypeRecord(int nProdType, int nSrvType)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbSrvType where ProdType=%d and SrvType=%d", nProdType, nSrvType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateSrvTypeListItems()
{
  /*ReadLookupItemList(SrvTypeItemList, "tbSrvType", "SrvType", "SrvName");

  AddCmbItem(FormMain->cbAcceptType, SrvTypeItemList, "");
  AddCmbItem(FormAcceptGroup->cbSrvType, SrvTypeItemList, "");
  AddCmbItem(FormMain->cbSrvType, SrvTypeItemList, "");
  AddCmbItem(FormMain->cbCurSrvType, SrvTypeItemList, "");
  AddCmbItem(FormMain->cbOldAcceptSrvType, ProdTypeItemList, "不選擇");
  AddCmbItem(FormMain->cbOldAcceptSrvType1, SrvTypeItemList, "不選擇");*/
}
//---------------------------------------------------------------------------
int TdmAgent::IsEndCode1Exist(int nEndCode1)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select EndCode1 from tbEndCode1 where EndCode1=%d", nEndCode1);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}

int TdmAgent::InsertEndCode1Record(int nEndCode1, AnsiString EndCodeName, int nDispOrder)
{
  char sqlbuf[128];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbEndCode1 (EndCode1,EndCodeName,DispOrder) values ('%d','%s',%d)", nEndCode1, EndCodeName.c_str(), nDispOrder);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateEndCode1Record(int nEndCode1, AnsiString EndCodeName, int nDispOrder)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbEndCode1 set EndCodeName='%s',DispOrder=%d where EndCode1=%d", EndCodeName.c_str(), nDispOrder, nEndCode1);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteEndCode1Record(int nEndCode1)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbEndCode1 where EndCode1=%d", nEndCode1);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateEndCode1ListItems()
{
  ReadLookupItemList(EndCode1ItemList, "tbEndCode1", "EndCode1", "EndCodeName");

  //AddCmbItem(FormMain->, EndCode1ItemList, "");
  AddCmbItem(FormMain->cbEndCode1, EndCode1ItemList, "不選擇");
}
//------------------------------------------------------------------------------
void TdmAgent::UpdateProdTypeListItems()
{
  ReadProdTypeItemList(ProdTypeItemList);

  AddCmbItem(FormMain->cbSelProdType, ProdTypeItemList, "");
  AddCmbItem(FormMain->cbProdType, ProdTypeItemList, "");
  AddCmbItem(FormMain->cbProdType1, ProdTypeItemList, "");
}
int TdmAgent::IsProdModelTypeExist(int nProdType, int nProdModelType)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select ProdModelType from tbProdModelType where ProdType=%d and ProdModelType=%d", nProdType, nProdModelType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertProdModelTypeRecord(int nProdType, int nProdModelType, AnsiString ProdModelName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbProdModelType (ProdType,ProdModelType,ProdModelName,URLBtnName,ProdModelURL,ProdModelDemo,DispOrder) values ('%d','%d','%s','%s','%s','%s',%d)",
    nProdType, nProdModelType, ProdModelName.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateProdModelTypeRecord(int nProdType, int nProdModelType, AnsiString ProdModelName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbProdModelType set ProdModelName='%s',URLBtnName='%s',ProdModelURL='%s',ProdModelDemo='%s',DispOrder=%d where ProdType=%d and ProdModelType=%d",
    ProdModelName.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder, nProdType, nProdModelType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteProdModelTypeRecord(int nProdType, int nProdModelType)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbProdModelType where ProdType=%d and ProdModelType=%d", nProdType, nProdModelType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//-----------------------------------------------------------------------------
int TdmAgent::IsWorkerGroupExist(int nGroupNo)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select GroupNo from tbWorkerGroup where GroupNo=%d", nGroupNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertWorkerGroupRecord(int nGroupNo, AnsiString GroupName, int acwtimer, AnsiString strDialoutPreCode, int nDepartmentID)
{
  char sqlbuf[128];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbWorkerGroup (GroupNo,GroupName,ACWTimer,DialoutPreCode,DepartmentID) values (%d,'%s',%d,'%s',%d)",
    nGroupNo, GroupName.c_str(), acwtimer, strDialoutPreCode.c_str(), nDepartmentID);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateWorkerGroupRecord(int nGroupNo, AnsiString GroupName, int acwtimer, AnsiString strDialoutPreCode, int nDepartmentID)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbWorkerGroup set GroupName='%s',ACWTimer=%d,DialoutPreCode='%s',DepartmentID=%d where GroupNo=%d",
    GroupName.c_str(), acwtimer, strDialoutPreCode.c_str(), nDepartmentID, nGroupNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteWorkerGroupRecord(int nGroupNo)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbWorkerGroup where GroupNo=%d", nGroupNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::GetGroupACWTimer(int nGroupNo)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select ACWTimer from tbWorkerGroup where GroupNo=%d", nGroupNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = adoqueryPub->FieldByName("ACWTimer")->AsInteger;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
AnsiString TdmAgent::GetGroupDialoutPreCode(int nGroupNo)
{
  char sqlbuf[128];
  AnsiString strDialoutPreCode="";

  sprintf(sqlbuf, "Select DialoutPreCode from tbWorkerGroup where GroupNo=%d", nGroupNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strDialoutPreCode = adoqueryPub->FieldByName("DialoutPreCode")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return strDialoutPreCode;
}

void TdmAgent::UpdateWorkerGroupListItems()
{
  ReadLookupItemList(WorkerGroupList, "tbWorkerGroup", "GroupNo", "GroupName");

  FormAcceptGroup->cbGroupNo->Items->Add("不選擇");
  FormAcceptGroup->cbGroupNo->Items->Add("----------");
  FormAcceptGroup->cbGroupNo->Items->Add("留言建議");
  FormAcceptGroup->cbGroupNo->Items->Add("IVR提示語音");
  if (MaxFaxNum > 0)
    FormAcceptGroup->cbGroupNo->Items->Add("接收傳真");
  FormAcceptGroup->cbGroupNo->Items->Add("----------");
  AddCmbItem(FormAcceptGroup->cbGroupNo, WorkerGroupList, "", false);
  FormAcceptGroup->cbGroupNo->Items->Add("----------");
  AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo, false);

  AddCmbItem(FormAcceptGroup->cbGroupNo1, WorkerGroupList, "不選擇");
  AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo1, false);
  AddCmbItem(FormAcceptGroup->cbGroupNo2, WorkerGroupList, "不選擇");
  AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo2, false);
  AddCmbItem(FormAcceptGroup->cbGroupNo3, WorkerGroupList, "不選擇");
  AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo3, false);
  AddCmbItem(FormAcceptGroup->cbGroupNo4, WorkerGroupList, "不選擇");
  AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo4, false);

  AddCmbItem(FormWorker->cbGroupNo, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo1, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo2, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo3, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo4, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo5, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo6, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo7, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo8, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo9, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo10, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo11, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo12, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo13, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo14, WorkerGroupList, "不選擇");
  AddCmbItem(FormWorker->cbGroupNo15, WorkerGroupList, "不選擇");

  AddCmbItem(FormLogin->cbWrokerGroup, WorkerGroupList, "不選擇");
  AddCmbItem(FormLogin->cbDutyNo, DutyNoItemList, "不選擇");

  AddCmbItem(FormMain->cbGroupNo, WorkerGroupList, "所有");

  AddCmbItem(FormEditDialOutTask->cbGroupNo, dmAgent->WorkerGroupList, "播放提示語音");
}
//----------------------------------------------------------------------------
//---------------------------------------------------------------------------
int TdmAgent::GetDepartmentId(AnsiString DepartmentName, int &nDepartmentID)
{
  char sqlbuf[128];
  bool nResult=0;

  sprintf(sqlbuf, "Select DepartmentID from tbDepartment where DepartmentName='%s'", DepartmentName.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      nDepartmentID = adoqueryPub->FieldByName("DepartmentID")->AsInteger;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
//------------------------------------------------------------------------------
//----------------------------------------------------------------------------
void __fastcall TdmAgent::adoQryCurAcceptProcCalcFields(TDataSet *DataSet)
{
  if (adoQryCurAcceptProcNextProcType->AsInteger == 1)
    adoQryCurAcceptProcDispNextName->AsString = adoQryCurAcceptProcDepartmentName->AsString;
  else if (adoQryCurAcceptProcNextProcType->AsInteger == 2)
    adoQryCurAcceptProcDispNextName->AsString = adoQryCurAcceptProcWorkerName1->AsString;
  else
    adoQryCurAcceptProcDispNextName->AsString = "無";
  if (adoQryCurAcceptProcNextProcType->AsInteger == 0)
  {
    adoQryCurAcceptProcDispRespond->AsString = "無";
  }
  else
  {
    adoQryCurAcceptProcDispRespond->AsString = adoQryCurAcceptProcRespondName->AsString;
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryOldAcceptProcCalcFields(TDataSet *DataSet)
{
  if (adoQryOldAcceptProcNextProcType->AsInteger == 1)
    adoQryOldAcceptProcDispNextName->AsString = adoQryOldAcceptProcDepartmentName->AsString;
  else if (adoQryOldAcceptProcNextProcType->AsInteger == 2)
    adoQryOldAcceptProcDispNextName->AsString = adoQryOldAcceptProcWorkerName1->AsString;
  else
    adoQryOldAcceptProcDispNextName->AsString = "無";
  if (adoQryOldAcceptProcNextProcType->AsInteger == 0)
  {
    adoQryOldAcceptProcDispRespond->AsString = "無";
  }
  else
  {
    adoQryOldAcceptProcDispRespond->AsString = adoQryOldAcceptProcRespondName->AsString;
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryAcceptGroupCalcFields(TDataSet *DataSet)
{
  AnsiString strWorkerName;
  adoQryAcceptGroupDispSrvName->AsString = ProdTypeItemList.GetItemName(adoQryAcceptGroupSrvType->AsInteger);

  if (adoQryAcceptGroupGroupNo1->AsString.Length() > 6)
  {
    adoQryAcceptGroupDispGrouNo1->AsString = "外線:"+adoQryAcceptGroupGroupNo1->AsString;
  }
  else
  {
    if (adoQryAcceptGroupGroupNo1->AsInteger < 1)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "未選擇";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger < 256)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "群組:"+WorkerGroupList.GetItemNameEx(adoQryAcceptGroupGroupNo1->AsInteger);
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 256)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:留言";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 257)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:IVR提示語音";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 258)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:接收傳真";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 259)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:電話會議";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 260)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:索取傳真";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 261)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:定制IVR子選單";
    }
    else if (adoQryAcceptGroupGroupNo1->AsInteger == 262)
    {
      adoQryAcceptGroupDispGrouNo1->AsString = "IVR功能:外轉外";
    }
    else
    {
      if (adoQryAcceptGroupGroupNo1Type->AsInteger == 0)
      {
        GetWorkerName(adoQryAcceptGroupGroupNo1->AsInteger, strWorkerName);
        adoQryAcceptGroupDispGrouNo1->AsString = "值機員:"+strWorkerName;
      }
      else
      {
        adoQryAcceptGroupDispGrouNo1->AsString = "分機號碼:"+adoQryAcceptGroupGroupNo1->AsString;
      }
    }
  }

  if (adoQryAcceptGroupGroupNo2->AsString.Length() > 6)
  {
    adoQryAcceptGroupDispGrouNo2->AsString = "外線:"+adoQryAcceptGroupGroupNo2->AsString;
  }
  else
  {
    if (adoQryAcceptGroupGroupNo2->AsInteger < 1)
    {
      adoQryAcceptGroupDispGrouNo2->AsString = "未選擇";
    }
    else if (adoQryAcceptGroupGroupNo2->AsInteger < 256)
    {
      adoQryAcceptGroupDispGrouNo2->AsString = "群組:"+WorkerGroupList.GetItemNameEx(adoQryAcceptGroupGroupNo2->AsInteger);
    }
    else if (adoQryAcceptGroupGroupNo2->AsInteger == 256)
    {
      adoQryAcceptGroupDispGrouNo2->AsString = "IVR功能:留言";
    }
    else
    {
      if (adoQryAcceptGroupGroupNo2Type->AsInteger == 0)
      {
        GetWorkerName(adoQryAcceptGroupGroupNo2->AsInteger, strWorkerName);
        adoQryAcceptGroupDispGrouNo2->AsString = "值機員:"+strWorkerName;
      }
      else
      {
        adoQryAcceptGroupDispGrouNo2->AsString = "分機號碼:"+adoQryAcceptGroupGroupNo2->AsString;
      }
    }
  }

  if (adoQryAcceptGroupGroupNo3->AsString.Length() > 6)
  {
    adoQryAcceptGroupDispGrouNo3->AsString = "外線:"+adoQryAcceptGroupGroupNo3->AsString;
  }
  else
  {
    if (adoQryAcceptGroupGroupNo3->AsInteger < 1)
    {
      adoQryAcceptGroupDispGrouNo3->AsString = "未選擇";
    }
    else if (adoQryAcceptGroupGroupNo3->AsInteger < 256)
    {
      adoQryAcceptGroupDispGrouNo3->AsString = "群組:"+WorkerGroupList.GetItemNameEx(adoQryAcceptGroupGroupNo3->AsInteger);
    }
    else if (adoQryAcceptGroupGroupNo3->AsInteger == 256)
    {
      adoQryAcceptGroupDispGrouNo3->AsString = "IVR功能:留言";
    }
    else
    {
      if (adoQryAcceptGroupGroupNo3Type->AsInteger == 0)
      {
        GetWorkerName(adoQryAcceptGroupGroupNo3->AsInteger, strWorkerName);
        adoQryAcceptGroupDispGrouNo3->AsString = "值機員:"+strWorkerName;
      }
      else
      {
        adoQryAcceptGroupDispGrouNo3->AsString = "分機號碼:"+adoQryAcceptGroupGroupNo3->AsString;
      }
    }
  }

  if (adoQryAcceptGroupGroupNo4->AsString.Length() > 6)
  {
    adoQryAcceptGroupDispGrouNo4->AsString = "外線:"+adoQryAcceptGroupGroupNo4->AsString;
  }
  else
  {
    if (adoQryAcceptGroupGroupNo4->AsInteger < 1)
    {
      adoQryAcceptGroupDispGrouNo4->AsString = "未選擇";
    }
    else if (adoQryAcceptGroupGroupNo4->AsInteger < 256)
    {
      adoQryAcceptGroupDispGrouNo4->AsString = "群組:"+WorkerGroupList.GetItemNameEx(adoQryAcceptGroupGroupNo4->AsInteger);
    }
    else if (adoQryAcceptGroupGroupNo4->AsInteger == 256)
    {
      adoQryAcceptGroupDispGrouNo4->AsString = "IVR功能:留言";
    }
    else
    {
      if (adoQryAcceptGroupGroupNo4Type->AsInteger == 0)
      {
        GetWorkerName(adoQryAcceptGroupGroupNo4->AsInteger, strWorkerName);
        adoQryAcceptGroupDispGrouNo4->AsString = "值機員:"+strWorkerName;
      }
      else
      {
        adoQryAcceptGroupDispGrouNo4->AsString = "分機號碼:"+adoQryAcceptGroupGroupNo4->AsString;
      }
    }
  }

  if (adoQryAcceptGroupGroupNo5->AsString.Length() > 6)
  {
    adoQryAcceptGroupDispGrouNo5->AsString = "外線:"+adoQryAcceptGroupGroupNo5->AsString;
  }
  else
  {
    if (adoQryAcceptGroupGroupNo5->AsInteger < 1)
    {
      adoQryAcceptGroupDispGrouNo5->AsString = "未選擇";
    }
    else if (adoQryAcceptGroupGroupNo5->AsInteger < 256)
    {
      adoQryAcceptGroupDispGrouNo5->AsString = "群組:"+WorkerGroupList.GetItemNameEx(adoQryAcceptGroupGroupNo5->AsInteger);
    }
    else if (adoQryAcceptGroupGroupNo5->AsInteger == 256)
    {
      adoQryAcceptGroupDispGrouNo5->AsString = "IVR功能:留言";
    }
    else
    {
      if (adoQryAcceptGroupGroupNo5Type->AsInteger == 0)
      {
        GetWorkerName(adoQryAcceptGroupGroupNo5->AsInteger, strWorkerName);
        adoQryAcceptGroupDispGrouNo5->AsString = "值機員:"+strWorkerName;
      }
      else
      {
        adoQryAcceptGroupDispGrouNo5->AsString = "分機號碼:"+adoQryAcceptGroupGroupNo5->AsString;
      }
    }
  }

  if (adoQryAcceptGroupCalledType->AsInteger == 0)
  {
    if (adoQryAcceptGroupCalledNo->AsString == "*")
    {
      adoQryAcceptGroupDispCalledNo->AsString = "所有撥入號碼";
    }
    else
    {
      adoQryAcceptGroupDispCalledNo->AsString = "撥入號碼:"+adoQryAcceptGroupCalledNo->AsString;
    }
  }
  else
  {
    if (adoQryAcceptGroupCalledNo->AsString == "*")
    {
      adoQryAcceptGroupDispCalledNo->AsString = "所有來電號碼";
    }
    else
    {
      adoQryAcceptGroupDispCalledNo->AsString = "來電號碼:"+adoQryAcceptGroupCalledNo->AsString;
    }
  }
  if (adoQryAcceptGroupDTMFType->AsInteger == 0)
  {
    adoQryAcceptGroupDispDTMFKey->AsString = "IVR按鍵:"+adoQryAcceptGroupDTMFKey->AsString;
  }
  else if (adoQryAcceptGroupDTMFType->AsInteger == 1)
  {
    adoQryAcceptGroupDispDTMFKey->AsString = "客戶類型:"+CustTypeItemList.GetItemName(adoQryAcceptGroupDTMFKey->AsInteger);
  }
  else if (adoQryAcceptGroupDTMFType->AsInteger == 2)
  {
    adoQryAcceptGroupDispDTMFKey->AsString = "客戶級別:"+CustLevelItemList.GetItemName(adoQryAcceptGroupDTMFKey->AsInteger);
  }
  else if (adoQryAcceptGroupDTMFType->AsInteger == 3)
  {
    adoQryAcceptGroupDispDTMFKey->AsString = "外線迴路:"+adoQryAcceptGroupDTMFKey->AsString;
  }
  switch (adoQryAcceptGroupVocId->AsInteger)
  {
  case 0:
    adoQryAcceptGroupDispVocId->AsString = "不播報語音直接轉值機員";
    break;
  case 1:
    adoQryAcceptGroupDispVocId->AsString = "按系統設定參數播報語音";
    break;
  case 2:
    adoQryAcceptGroupDispVocId->AsString = "播報個性化公司語音";
    break;
  case 3:
    adoQryAcceptGroupDispVocId->AsString = "播報個性化公司及語音選單";
    break;
  case 4:
    adoQryAcceptGroupDispVocId->AsString = "播報總機語音選單";
    break;
  default:
    adoQryAcceptGroupDispVocId->AsString = "不播報語音直接轉值機員";
    break;
  }
  if (adoQryAcceptGroupWorkerNoId->AsInteger == 0)
  {
    adoQryAcceptGroupDispWorkerNoId->AsString = "不播報值機員編號";
  }
  else
  {
    adoQryAcceptGroupDispWorkerNoId->AsString = "按系統參數設定";
  }
  switch (adoQryAcceptGroupMenuDtmfACK->AsInteger)
  {
  case 0:
    adoQryAcceptGroupDispMenuDtmfACK->AsString = "不按鍵確認";
    break;
  case 1:
    adoQryAcceptGroupDispMenuDtmfACK->AsString = "按1鍵確認";
    break;
  case 2:
    adoQryAcceptGroupDispMenuDtmfACK->AsString = "按1鍵確認并儲存";
    break;
  default:
    adoQryAcceptGroupDispMenuDtmfACK->AsString = "不按鍵確認";
    break;
  }
}
//---------------------------------------------------------------------------
int TdmAgent::IsSelItemIdExist(int nSelectTableId, CSelectRecord *pSelectRecord)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select %s from %s where %s=%d",
    SelectTable[nSelectTableId].strItemId.c_str(),
    SelectTable[nSelectTableId].strTableName.c_str(),
    SelectTable[nSelectTableId].strItemId.c_str(),
    pSelectRecord->nItemIdValue);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertSelItemIdRecord(int nSelectTableId, CSelectRecord *pSelectRecord)
{
  char sqlbuf[128];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into %s (%s,%s) values (%d,'%s')",
    SelectTable[nSelectTableId].strTableName.c_str(),
    SelectTable[nSelectTableId].strItemId.c_str(),
    SelectTable[nSelectTableId].strItemName.c_str(),
    pSelectRecord->nItemIdValue,
    pSelectRecord->strItemNameValue.c_str()
    );
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateSelItemIdRecord(int nSelectTableId, CSelectRecord *pSelectRecord)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update %s set %s='%s' where %s=%d",
    SelectTable[nSelectTableId].strTableName.c_str(),
    SelectTable[nSelectTableId].strItemName.c_str(),
    pSelectRecord->strItemNameValue.c_str(),
    SelectTable[nSelectTableId].strItemId.c_str(),
    pSelectRecord->nItemIdValue);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteSelItemIdRecord(int nSelectTableId, int nItemId)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from %s where %s=%d",
    SelectTable[nSelectTableId].strTableName.c_str(),
    SelectTable[nSelectTableId].strItemId.c_str(),
    nItemId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteSelItemIdRecord(int nSelectTableId, CSelectRecord *pSelectRecord)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from %s where %s=%d",
    SelectTable[nSelectTableId].strTableName.c_str(),
    SelectTable[nSelectTableId].strItemId.c_str(),
    pSelectRecord->nItemIdValue);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateSelItemIdListItems(int nSelectTableId)
{
  //修改(受理業務類型)下拉框選擇列表
  if (SelectTable[0].bModifyId == true)
  {
    ReadLookupItemList(SrvTypeItemList, "tbSrvType", "SrvType", "SrvName");
    AddCmbItem(FormAcceptGroup->cbSrvType, ProdTypeItemList, "");
    AddCmbItem(FormMain->cbAcceptType, SrvTypeItemList, "");
    AddCmbItem(FormMain->cbOldAcceptSrvType, ProdTypeItemList, "不選擇");
    AddCmbItem(FormMain->cbSrvType, SrvTypeItemList, "");
    AddCmbItem(FormMain->cbCurSrvType, SrvTypeItemList, "");
    AddCmbItem(FormMain->cbOldAcceptSrvType1, SrvTypeItemList, "不選擇");

    ReadLookupItemList(ProdTypeItemList, "tbProdType", "ProdType", "ProdName");
    AddCmbItem(FormMain->cbSelProdType, ProdTypeItemList, "");
    AddCmbItem(FormMain->cbProdType, ProdTypeItemList, "");
    AddCmbItem(FormMain->cbProdType1, ProdTypeItemList, "");
  }
  //修改(工單處理結果)下拉框選擇列表
  if (SelectTable[1].bModifyId == true)
  {
		ReadLookupItemList(AcceptProcItemList, "tbAcceptProcType", "AcceptProcId", "AcceptProcName");
  	AddCmbItem(FormMain->cbOldAcceptDealResult, AcceptProcItemList, "不選擇");
  	AddCmbItem(FormMain->cmbDealState, AcceptProcItemList, "");
  	AddCmbItem(FormMain->cmbCurDealState, AcceptProcItemList, "");
  	AddCmbItem(FormMain->cmbDealState1, AcceptProcItemList, "");
    AddCmbItem(FormAcceptProc->cmbDealState, AcceptProcItemList, "");
  	AddCmbItem(FormMain->cbOldAcceptDealResult1, AcceptProcItemList, "不選擇");
  }
  //修改(受理工單類型)下拉框選擇列表
  if (SelectTable[2].bModifyId == true)
  {
		ReadLookupItemList(AcptSubTypeItemList, "tbacptsubtype", "AcptSubTypeId", "AcptSubTypeName");
  	AddCmbItem(FormMain->cbAcptSubType, AcptSubTypeItemList, "");
  	AddCmbItem(FormMain->cbSubType, AcptSubTypeItemList, "");
    AddCmbItem(FormMain->cbCurSubType, AcptSubTypeItemList, "");
  	AddCmbItem(FormMain->cbSubType1, AcptSubTypeItemList, "不選擇");
  }
  //修改(客戶對服務評價)下拉框選擇列表
  if (SelectTable[3].bModifyId == true)
  {
    ReadLookupItemList(ScoreNameList, "tbScore", "ScoreId", "ScoreName");
    AddCmbItem(FormMain->cbScore, ScoreNameList, "所有");
  }
  //修改(服務質檢結果)下拉框選擇列表
  if (SelectTable[4].bModifyId == true)
  {
    ReadLookupItemList(TalkScoreNameList, "tbTalkScore", "TalkScoreId", "TalkScoreName");
    AddCmbItem(FormMain->cbAcceptScore, TalkScoreNameList, "");
    AddCmbItem(FormMain->cbCurAcceptScore, TalkScoreNameList, "");
    AddCmbItem(FormMain->cbTalkScore, TalkScoreNameList, "");
  }
  //修改(客戶類型)下拉框選擇列表
  if (SelectTable[5].bModifyId == true)
  {
    ReadLookupItemList(CustTypeItemList, "tbCustType", "CustType", "CustTypeName");
    AddCmbItem(FormEditCustom->edtCustType, CustTypeItemList, "");
    AddCmbItem(FormMain->cbUserType, CustTypeItemList, "不選擇");
    AddCmbItem(FormMain->edtCustType, CustTypeItemList, "");
  }
  //修改(客戶劃分)下拉框選擇列表
  if (SelectTable[6].bModifyId == true)
  {
    ReadLookupItemList(CustLevelItemList, "tbCustLevel", "CustLevelId", "CustLevelName");
    AddCmbItem(FormEditCustom->edtCustLevel, CustLevelItemList, "");
    AddCmbItem(FormMain->cbUserLevel, CustLevelItemList, "不選擇");
    AddCmbItem(FormMain->edtCustLevel, CustLevelItemList, "");
  }
  //設置離席狀態菜單
  if (SelectTable[7].bModifyId == true)
  {
    SelectTable[7].bModifyId = false;
    ReadLookupItemList(LeaveTypeItemList, "tbLeaveType", "LeaveId", "LeaveName");

    if (LeaveTypeItemList.GetItemName(1) != "")
    {
      FormMain->mLeaveId1->Visible = true;
      FormMain->mLeaveId1->Caption = LeaveTypeItemList.GetItemName(1);
    }
    if (LeaveTypeItemList.GetItemName(2) != "")
    {
      FormMain->mLeaveId2->Visible = true;
      FormMain->mLeaveId2->Caption = LeaveTypeItemList.GetItemName(2);
    }
    if (LeaveTypeItemList.GetItemName(3) != "")
    {
      FormMain->mLeaveId3->Visible = true;
      FormMain->mLeaveId3->Caption = LeaveTypeItemList.GetItemName(3);
    }
    if (LeaveTypeItemList.GetItemName(4) != "")
    {
      FormMain->mLeaveId4->Visible = true;
      FormMain->mLeaveId4->Caption = LeaveTypeItemList.GetItemName(4);
    }
    if (LeaveTypeItemList.GetItemName(5) != "")
    {
      FormMain->mLeaveId5->Visible = true;
      FormMain->mLeaveId5->Caption = LeaveTypeItemList.GetItemName(5);
    }
    if (LeaveTypeItemList.GetItemName(6) != "")
    {
      FormMain->mLeaveId6->Visible = true;
      FormMain->mLeaveId6->Caption = LeaveTypeItemList.GetItemName(6);
    }
    if (LeaveTypeItemList.GetItemName(7) != "")
    {
      FormMain->mLeaveId7->Visible = true;
      FormMain->mLeaveId7->Caption = LeaveTypeItemList.GetItemName(7);
    }
    if (LeaveTypeItemList.GetItemName(8) != "")
    {
      FormMain->mLeaveId8->Visible = true;
      FormMain->mLeaveId8->Caption = LeaveTypeItemList.GetItemName(8);
    }
    if (LeaveTypeItemList.GetItemName(9) != "")
    {
      FormMain->mLeaveId9->Visible = true;
      FormMain->mLeaveId9->Caption = LeaveTypeItemList.GetItemName(9);
    }
  }
  //修改(工單后續處理狀態)下拉框選擇列表
  if (SelectTable[8].bModifyId == true)
  {
    ReadLookupItemList(NextProcResultItemList, "tbNextProcResult", "NextProcResultId", "NextProcResultName");
    AddCmbItem(FormAcceptProc->cbNextProcResult, NextProcResultItemList, "");
  }
  //修改(工單后續處理方式)下拉框選擇列表
  if (SelectTable[9].bModifyId == true)
  {
    ReadLookupItemList(NextProcTypeItemList, "tbNextProcType", "NextProcTypeId", "NextProcTypeName");
    AddCmbItem(FormAcceptProc->cbNextProcType, NextProcTypeItemList, "");
  }
  //修改(工單后續處理響應狀態)下拉框選擇列表
  if (SelectTable[10].bModifyId == true)
  {
  }
  //修改(付款方式)下拉框選擇列表
  if (SelectTable[11].bModifyId == true)
  {
  }
  //修改(留言處理結果)下拉框選擇列表
  if (SelectTable[12].bModifyId == true)
  {
    ReadLookupItemList(RecdProcTypeItemList, "tbRecdProcType", "RecdProcTypeId", "RecdProcTypeName");
    AddCmbItem(FormMain->cbRecdProcId, RecdProcTypeItemList, "不選擇");
    AddCmbItem(FormMain->cbRecdProcState, RecdProcTypeItemList, "");
    AddCmbItem(FormMain->cbRfaxProcId, RecdProcTypeItemList, "不選擇");
    AddCmbItem(FormMain->cbRfaxProcState, RecdProcTypeItemList, "");
  }
  //修改(接收短信類型)下拉框選擇列表
  if (SelectTable[13].bModifyId == true)
  {
  	ReadLookupItemList(RecvSmsTypeItemList, "tbRecvSMSType", "RecvSMSTypeId", "RecvSMSTypeName");
		AddCmbItem(FormMain->cbProcSrvType, RecvSmsTypeItemList, "不選擇");
    AddCmbItem(FormRecvSmsProc->cbProcSrvType, RecvSmsTypeItemList, "");
  }
  //修改(發送短信類型)下拉框選擇列表
  if (SelectTable[14].bModifyId == true)
  {
  	ReadLookupItemList(SendSmsTypeItemList, "tbSendSMSType", "SendSMSTypeId", "SendSMSTypeName");
  	AddCmbItem(FormMain->cbSSSmsSrvType, SendSmsTypeItemList, "不選擇");
  	AddCmbItem(FormMain->cbSendSmsSrvType, SendSmsTypeItemList, "不選擇");
    AddCmbItem(FormSendSms->cbSendSmsSrvType, SendSmsTypeItemList, "");
  }
  //修改(短信處理狀態)下拉框選擇列表
  if (SelectTable[15].bModifyId == true)
  {
    ReadLookupItemList(SmsProcTypeItemList, "tbSmsProcType", "SmsProcType", "SmsProcTypeName");
    AddCmbItem(FormMain->cbRSReadFlag, SmsProcTypeItemList, "不選擇");
    AddCmbItem(FormRecvSmsProc->cbReadFlag, SmsProcTypeItemList, "");
  }
  //修改(部門管理)下拉框選擇列表
  if (SelectTable[16].bModifyId == true)
  {
    ReadLookupItemList(DepartmentItemList, "tbDepartment", "DepartmentId", "DepartmentName");
    AddCmbItem(FormWorker->cbDepartmentID, DepartmentItemList, "");
    AddCmbItem(FormWorkerGroup->cbDepartmentID, dmAgent->DepartmentItemList, "");
  }
}

int TdmAgent::IsSrvSubTypeExist(int nProdType, int nSrvType, int nSrvSubType)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select * from tbSrvSubType where ProdType=%d and SrvType=%d and SrvSubType=%d", nProdType, nSrvType, nSrvSubType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertSrvSubType(int nProdType, int nSrvType, int nSrvSubType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  sprintf(sqlbuf, "insert into tbSrvSubType (ProdType,SrvType,SrvSubType,SrvSubName,URLBtnName,SrvSubURL,SrvSubDemo,DispOrder) values (%d,%d,%d,'%s','%s','%s','%s',%d)",
    nProdType, nSrvType, nSrvSubType, strItemName.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
  }
  return 1;
}
int TdmAgent::UpdateSrvSubType(int nProdType, int nSrvType, int nSrvSubType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  char sqlbuf[4096];

  sprintf(sqlbuf, "update tbSrvSubType set SrvSubName='%s',URLBtnName='%s',SrvSubURL='%s',SrvSubDemo='%s',DispOrder=%d where ProdType=%d and SrvType=%d and SrvSubType=%d",
    strItemName.c_str(), strURLBtnName.c_str(), strItemURL.c_str(), strDemo.c_str(), nDispOrder, nProdType, nSrvType, nSrvSubType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
  }
  return 1;
}
int TdmAgent::DeleteSrvSubType(int nProdType, int nSrvType, int nSrvSubType)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbSrvSubType where ProdType=%d and SrvType=%d and SrvSubType=%d", nProdType, nSrvType, nSrvSubType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
  }
  return 1;
}
int TdmAgent::GetCombineDefaultItemId(int nProdType, int nSrvType, int nSrvSubType)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select CombineDefaultItemId from tbSrvSubType where ProdType=%d and SrvType=%d and SrvSubType=%d", nProdType, nSrvType, nSrvSubType);
  try
  {
    adoqueryPub2->SQL->Clear();
    adoqueryPub2->SQL->Add((char *)sqlbuf );
    adoqueryPub2->Open();

    if ( !adoqueryPub2->Eof )
    {
      result = adoqueryPub2->FieldByName("CombineDefaultItemId")->AsInteger;
    }
    adoqueryPub2->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
void TdmAgent::UpdateCombineDefaultItemId(int nProdType, int nSrvType, int nSrvSubType, int nId)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "update tbSrvSubType set CombineDefaultItemId=%d where ProdType=%d and SrvType=%d and SrvSubType=%d", nId, nProdType, nSrvType, nSrvSubType);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//------------------------------------------------------------------------------

int TdmAgent::IsEndCode2Exist(int nEndCode1, int nEndCode2)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select * from tbEndCode2 where EndCode1=%d and EndCode2=%d", nEndCode1, nEndCode2);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertEndCode2(int nEndCode1, int nEndCode2, AnsiString strItemName, AnsiString strItemURL, AnsiString stColor, int nDispOrder)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "insert into tbEndCode2 (EndCode1,EndCode2,EndCodeName,EndCodeURL,DispColor,DispOrder) values (%d,%d,'%s','%s','%s',%d)",
    nEndCode1, nEndCode2, strItemName.c_str(), strItemURL.c_str(), stColor.c_str(), nDispOrder);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
  }
  return 1;
}
int TdmAgent::UpdateEndCode2(int nEndCode1, int nEndCode2, AnsiString strItemName, AnsiString strItemURL, AnsiString stColor, int nDispOrder)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbEndCode2 set EndCodeName='%s',EndCodeURL='%s',DispColor='%s',DispOrder=%d where EndCode1=%d and EndCode2=%d",
    strItemName.c_str(), strItemURL.c_str(), stColor.c_str(), nDispOrder, nEndCode1, nEndCode2);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
  }
  return 1;
}
int TdmAgent::DeleteEndCode2(int nEndCode1, int nEndCode2)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbEndCode2 where EndCode1=%d and EndCode2=%d", nEndCode1, nEndCode2);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
  }
  return 1;
}
//------------------------------------------------------------------------------
void __fastcall TdmAgent::adoQryAcptItemSetCalcFields(TDataSet *DataSet)
{
  adoQryAcptItemSetDispIsAllowNull->AsString = (adoQryAcptItemSetTIsAllowNull->AsInteger == 1) ? "是" : "否";
  adoQryAcptItemSetDispExportId->AsString = (adoQryAcptItemSetTExportId->AsInteger == 1) ? "是" : "否";
  if (adoQryAcptItemSetCombineMode->AsInteger == 0)
    adoQryAcptItemSetDispCombineMode->AsString = "不合并";
  else if (adoQryAcptItemSetCombineMode->AsInteger == 1)
    adoQryAcptItemSetDispCombineMode->AsString = "替換";
  else if (adoQryAcptItemSetCombineMode->AsInteger == 2)
    adoQryAcptItemSetDispCombineMode->AsString = "合并";
}
//---------------------------------------------------------------------------
int TdmAgent::UpdateRecvFaxReadFlag(int id, int readid, AnsiString content)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvfax set ReadId=%d,FaxContent='%s',ReadWorkerNo='%s',ReadTime=%s where id=%d",
    readid, content.c_str(), MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvFaxReadFlag(int id, int readid)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvfax set ReadId=%d,ReadWorkerNo='%s',ReadTime=%s where id=%d",
    readid, MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvFaxProcFlag(int id, int procid, AnsiString content)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvfax set ProcId=%d,ProcNote='%s',ProcWorkerNo='%s',ProcTime=%s where id=%d",
    procid, content.c_str(), MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvFaxProcFlag(int id, int procid, AnsiString strWorkerNo, AnsiString content)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvfax set ProcId=%d,ProcNote='%s',ProcWorkerNo='%s',ProcTime=%s where id=%d",
    procid, content.c_str(), strWorkerNo.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}

int TdmAgent::UpdateRecvEmailProcFlag(int id, int procid, AnsiString content)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvemail set ProcFlag=%d,ProcContent='%s',WorkerNo='%s',ProcTime=%s where id=%d",
    procid, content.c_str(), MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvEmailProcFlag(int id, int procid, AnsiString strWorkerNo, AnsiString content)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvemail set ProcFlag=%d,ProcContent='%s',WorkerNo='%s',ProcTime=%s where id=%d",
    procid, content.c_str(), strWorkerNo.c_str(), MyNowFuncName.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}

int TdmAgent::DelRecvEmail(int id)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "update tbrecvemail set DeleteFlag=1 where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}

void __fastcall TdmAgent::adoQryUserBaseCalcFields(TDataSet *DataSet)
{
  adoQryUserBaseCustTypeName->AsString = CustTypeItemList.GetItemName(adoQryUserBaseCustType->AsInteger);
  adoQryUserBaseCustLevelName->AsString = CustLevelItemList.GetItemName(adoQryUserBaseCustLevel->AsInteger);
  for (int i=0; i<FormMain->m_CustExDispFieldList.FieldNum; i++)
  {
    int nFieldId = FormMain->m_CustExDispFieldList.m_CustExDispField[i].FieldId;
    if (nFieldId >=1 && nFieldId <= 32)
    {
      if (FormMain->m_CustExDispFieldList.m_CustExDispField[i].ControlType == 2)
        m_pCustExDispField[nFieldId-1]->AsString = FormMain->m_pCustExItemSetList->m_CustExItemSet[nFieldId-1].TLookupItemList.GetItemName(MyStrToInt(m_pCustExField[nFieldId-1]->AsString));
      else if (FormMain->m_CustExDispFieldList.m_CustExDispField[i].ControlType == 8)
        m_pCustExDispField[nFieldId-1]->AsString = YesNoItemList.GetItemName(MyStrToInt(m_pCustExField[nFieldId-1]->AsString));
    }
  }
  adoQryUserBaseDispMobileNo->AsString = GetHidePhoneNum(2, adoQryUserBaseMobileNo->AsString);
  adoQryUserBaseDispTeleNo->AsString = GetHidePhoneNum(2, adoQryUserBaseTeleNo->AsString);
  adoQryUserBaseDispFaxNo->AsString = GetHidePhoneNum(2, adoQryUserBaseFaxNo->AsString);
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQrySendSmsCalcFields(TDataSet *DataSet)
{
  adoQrySendSmsSendSMSTypeName->AsString = SendSmsTypeItemList.GetItemName(adoQrySendSmsSmsSrvType->AsInteger);
  adoQrySendSmsSendSmsStatusName->AsString = SendSmsStatusItemList.GetItemName(adoQrySendSmsSendStatus->AsInteger);
  adoQrySendSmsWorkerName->AsString = GetWorkerName(adoQrySendSmsAcceptBy->AsInteger);
}
//---------------------------------------------------------------------------
int TdmAgent::UpdateCustomSetDispItem(int id, bool bDispId, bool bAcceptId, bool bExportId, int nExportOrderId, int nAcceptExId, int nReadDataType, AnsiString strReadFieldTag, int nSaveToDBFlag)
{
  char sqlbuf[256];

  sprintf( sqlbuf, "update tbcustomset set DispId=%d,InputId=%d,ExportId=%d,ExportOrderId=%d,AcceptExId=%d,ReadDataType=%d,ReadFieldTag='%s',SaveToDBFlag=%d where id=%d",
    bDispId, bAcceptId, bExportId, nExportOrderId, nAcceptExId, nReadDataType, strReadFieldTag.c_str(), nSaveToDBFlag, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateAcceptSetDispItem(int id, bool bDispId, bool bExportId, int nExportOrderId)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbAcceptset set DispId=%d,ExportId=%d,ExportOrderId=%d where id=%d",
    bDispId, bExportId, nExportOrderId, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateCallCDRSetDispItem(int id, bool bDispId)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbcallcdrset set DispId=%d where id=%d",
    bDispId, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void __fastcall TdmAgent::adoQryCustomSetCalcFields(TDataSet *DataSet)
{
  adoQryCustomSetDispDispId->AsString = (adoQryCustomSetDispId->AsInteger==1)?"是":"否";
  adoQryCustomSetDispInputId->AsString = (adoQryCustomSetInputId->AsInteger==1)?"是":"否";
  adoQryCustomSetDispExportId->AsString = (adoQryCustomSetExportId->AsInteger==1)?"是":"否";
  if (adoQryCustomSetReadDataType->AsInteger == 1)
    adoQryCustomSetDispReadDataType->AsString = "DB資料庫";
  else if (adoQryCustomSetReadDataType->AsInteger == 2)
    adoQryCustomSetDispReadDataType->AsString = "HTTP介面";
  else if (adoQryCustomSetReadDataType->AsInteger == 3)
    adoQryCustomSetDispReadDataType->AsString = "WEBSERVICE介面";
  else if (adoQryCustomSetReadDataType->AsInteger == 4)
    adoQryCustomSetDispReadDataType->AsString = "同步CRM資料";
  else
    adoQryCustomSetDispReadDataType->AsString = "DB資料庫";
  adoQryCustomSetDispSaveToDBFlag->AsString = (adoQryCustomSetSaveToDBFlag->AsInteger==1)?"是":"否";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryOldAcceptCalcFields(TDataSet *DataSet)
{
  adoQryOldAcceptAcceptScoreName->AsString = TalkScoreNameList.GetItemName(adoQryOldAcceptAcceptScore->AsInteger);
  //adoQryOldAcceptWorkerName->AsString = GetWorkerName(adoQryOldAcceptWorkerNo->AsString);

  adoQryOldAcceptDispProdType->AsString = ProdTypeItemList.GetItemNameEx(adoQryOldAcceptProdType->AsInteger);
  adoQryOldAcceptDispProdModelType->AsString = ProdModelTypeItemList.GetItemNameEx(adoQryOldAcceptProdType->AsInteger, adoQryOldAcceptProdModelType->AsInteger);
  adoQryOldAcceptSrvName->AsString = SrvTypeItemList.GetItemNameEx(adoQryOldAcceptProdType->AsInteger, adoQryOldAcceptAcptType->AsInteger);
  adoQryOldAcceptDispSrvSubType->AsString = SrvSubTypeItemList.GetItemNameEx(adoQryOldAcceptProdType->AsInteger, adoQryOldAcceptAcptType->AsInteger, adoQryOldAcceptSrvSubType->AsInteger);

  adoQryOldAcceptAcptSubTypeName->AsString = AcptSubTypeItemList.GetItemNameEx(adoQryOldAcceptAcptSubType->AsInteger);
  adoQryOldAcceptAcceptProcName->AsString = AcceptProcItemList.GetItemNameEx(adoQryOldAcceptDealState->AsInteger);
  //adoQryOldAcceptTalkScoreName->AsString = GetTalkScoreName(adoQryOldAcceptSerialNo->AsString);

  if (adoQryOldAcceptAnsId->AsInteger == 1)
  {
    if (IsRecdFileExist(adoQryOldAcceptSerialNo->AsString) == 1)
      adoQryOldAcceptDispRecdFile->AsString = "有";
    else
      adoQryOldAcceptDispRecdFile->AsString = "無";

    adoQryOldAcceptDispAnsId->AsString = "是";
  }
  else if (adoQryOldAcceptAnsId->AsInteger == 2)
  {
    adoQryOldAcceptDispRecdFile->AsString = "無";
    adoQryOldAcceptDispAnsId->AsString = "IVR";
  }
  else
  {
    adoQryOldAcceptDispRecdFile->AsString = "無";
    adoQryOldAcceptDispAnsId->AsString = "否";
  }
  if (adoQryOldAcceptMobileNo->AsString.Length() > 0)
    adoQryOldAcceptDispPhone->AsString = adoQryOldAcceptMobileNo->AsString;
  else if (adoQryOldAcceptTeleNo->AsString.Length() > 0)
    adoQryOldAcceptDispPhone->AsString = adoQryOldAcceptTeleNo->AsString;
  else if (adoQryOldAcceptFaxNo->AsString.Length() > 0)
    adoQryOldAcceptDispPhone->AsString = adoQryOldAcceptFaxNo->AsString;
  else
    adoQryOldAcceptDispPhone->AsString = adoQryOldAcceptCustomTel->AsString;

  if (adoQryOldAcceptCallBackId->AsInteger == 0)
    adoQryOldAcceptDispCallBackId->AsString = "不預約回撥";
  else if (adoQryOldAcceptCallBackId->AsInteger == 1)
    adoQryOldAcceptDispCallBackId->AsString = "預約回撥";
  else if (adoQryOldAcceptCallBackId->AsInteger == 2)
    adoQryOldAcceptDispCallBackId->AsString = "已回撥";
  else
    adoQryOldAcceptDispCallBackId->AsString = "不預約回撥";
  adoQryOldAcceptDispCustomTel->AsString = GetHidePhoneNum(2, adoQryOldAcceptCustomTel->AsString);
  if (adoQryOldAcceptMediaType->AsInteger == 1)
    adoQryOldAcceptDispMediaType->AsString = "IM會話";
  else if (adoQryOldAcceptMediaType->AsInteger == 2)
    adoQryOldAcceptDispMediaType->AsString = "Email";
  else if (adoQryOldAcceptMediaType->AsInteger == 3)
    adoQryOldAcceptDispMediaType->AsString = "Fax傳真";
  else
    adoQryOldAcceptDispMediaType->AsString = "電話";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryAcceptSetCalcFields(TDataSet *DataSet)
{
  adoQryAcceptSetDispDispId->AsString = (adoQryAcceptSetDispId->AsInteger==1)?"是":"否";
  adoQryAcceptSetDispExportId->AsString = (adoQryAcceptSetExportId->AsInteger==1)?"是":"否";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryCallCDRSetCalcFields(TDataSet *DataSet)
{
  adoQryCallCDRSetDispDispId->AsString = (adoQryCallCDRSetDispId->AsInteger==1)?"是":"否";
}
//---------------------------------------------------------------------------

void TdmAgent::ReadTranSeatFuncKey()
{
  char sqlbuf[128];
  int nFuncKeyId;
  AnsiString strSeatNo;

  if (ADOConnection1->Connected == false)
    return;
  sprintf(sqlbuf, "Select FuncKeyId,SeatNo from tbtranseatfunckey order by FuncKeyId");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    while ( !adoqueryPub->Eof )
    {
      nFuncKeyId = adoqueryPub->FieldByName("FuncKeyId")->AsInteger;
      strSeatNo = adoqueryPub->FieldByName("SeatNo")->AsString;
      switch (nFuncKeyId)
      {
      case 1:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF1->Caption = "F1("+strSeatNo+")";
          FormDialSeatNo->btFuncF1->Enabled = true;
          FormMain->cbSeatFuncKey->Text = "F1";
          FormMain->editSeatFuncKeySeatNo->Text = strSeatNo;
        }
        else
        {
          FormDialSeatNo->btFuncF1->Caption = "F1";
          FormDialSeatNo->btFuncF1->Enabled = false;
          FormMain->cbSeatFuncKey->Text = "F1";
          FormMain->editSeatFuncKeySeatNo->Text = "";
        }
        break;
      case 2:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF2->Caption = "F2("+strSeatNo+")";
          FormDialSeatNo->btFuncF2->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF2->Caption = "F2";
          FormDialSeatNo->btFuncF2->Enabled = false;
        }
        break;
      case 3:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF3->Caption = "F3("+strSeatNo+")";
          FormDialSeatNo->btFuncF3->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF3->Caption = "F3";
          FormDialSeatNo->btFuncF3->Enabled = false;
        }
        break;
      case 4:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF4->Caption = "F4("+strSeatNo+")";
          FormDialSeatNo->btFuncF4->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF4->Caption = "F4";
          FormDialSeatNo->btFuncF4->Enabled = false;
        }
        break;
      case 5:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF5->Caption = "F5("+strSeatNo+")";
          FormDialSeatNo->btFuncF5->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF5->Caption = "F5";
          FormDialSeatNo->btFuncF5->Enabled = false;
        }
        break;
      case 6:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF6->Caption = "F6("+strSeatNo+")";
          FormDialSeatNo->btFuncF6->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF6->Caption = "F6";
          FormDialSeatNo->btFuncF6->Enabled = false;
        }
        break;
      case 7:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF7->Caption = "F7("+strSeatNo+")";
          FormDialSeatNo->btFuncF7->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF7->Caption = "F7";
          FormDialSeatNo->btFuncF7->Enabled = false;
        }
        break;
      case 8:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF8->Caption = "F8("+strSeatNo+")";
          FormDialSeatNo->btFuncF8->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF8->Caption = "F8";
          FormDialSeatNo->btFuncF8->Enabled = false;
        }
        break;
      case 9:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF9->Caption = "F9("+strSeatNo+")";
          FormDialSeatNo->btFuncF9->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF9->Caption = "F9";
          FormDialSeatNo->btFuncF9->Enabled = false;
        }
        break;
      case 10:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF10->Caption = "F10("+strSeatNo+")";
          FormDialSeatNo->btFuncF10->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF10->Caption = "F10";
          FormDialSeatNo->btFuncF10->Enabled = false;
        }
        break;
      case 11:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF11->Caption = "F11("+strSeatNo+")";
          FormDialSeatNo->btFuncF11->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF11->Caption = "F11";
          FormDialSeatNo->btFuncF11->Enabled = false;
        }
        break;
      case 12:
        if (strSeatNo.Length() > 0)
        {
          FormDialSeatNo->btFuncF12->Caption = "F12("+strSeatNo+")";
          FormDialSeatNo->btFuncF12->Enabled = true;
        }
        else
        {
          FormDialSeatNo->btFuncF12->Caption = "F12";
          FormDialSeatNo->btFuncF12->Enabled = false;
        }
        break;
      }
      adoqueryPub->Next();
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}

int TdmAgent::GetTranSeatFuncKey(int nFuncKeyId, AnsiString &strSeatNo, AnsiString &strWorkerNo)
{
  char sqlbuf[128];
  bool nResult=0;

  strSeatNo="";
  strWorkerNo="";
  sprintf(sqlbuf, "Select SeatNo,WorkerNo from tbtranseatfunckey where FuncKeyId=%d", nFuncKeyId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strSeatNo = adoqueryPub->FieldByName("SeatNo")->AsString;
      strWorkerNo = adoqueryPub->FieldByName("WorkerNo")->AsString;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::UpdateTranSeatFuncKey(int nFuncKeyId, AnsiString strSeatNo, AnsiString strWorkerNo)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbtranseatfunckey set SeatNo='%s',WorkerNo='%s' where FuncKeyId=%d",
    strSeatNo.c_str(), strWorkerNo.c_str(), nFuncKeyId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::ReadCallOutFuncKey()
{
  char sqlbuf[128];
  int nFuncKeyId;
  AnsiString strCalledNo,strCalledName;

  if (ADOConnection1->Connected == false)
    return;
  sprintf(sqlbuf, "Select FuncKeyId,CalledNo,CalledName from tbtranoutfunckey order by FuncKeyId");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    while ( !adoqueryPub->Eof )
    {
      nFuncKeyId = adoqueryPub->FieldByName("FuncKeyId")->AsInteger;
      strCalledNo = adoqueryPub->FieldByName("CalledNo")->AsString;
      strCalledName = adoqueryPub->FieldByName("CalledName")->AsString;
      switch (nFuncKeyId)
      {
      case 1:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF1->Caption = "F1("+strCalledName+")";
          FormDial->lbFuncF1->Caption = strCalledNo;
          FormDial->btFuncF1->Enabled = true;
          FormMain->cbOutFuncKey->Text = "F1";
          FormMain->editFuncKeyCalledNo->Text = strCalledNo;
          FormMain->editFuncKeyName->Text = strCalledName;
        }
        else
        {
          FormDial->btFuncF1->Caption = "F1";
          FormDial->lbFuncF1->Caption = "";
          FormDial->btFuncF1->Enabled = false;
          FormMain->cbOutFuncKey->Text = "F1";
          FormMain->editFuncKeyCalledNo->Text = "";
          FormMain->editFuncKeyName->Text = "";
        }
        break;
      case 2:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF2->Caption = "F2("+strCalledName+")";
          FormDial->lbFuncF2->Caption = strCalledNo;
          FormDial->btFuncF2->Enabled = true;
        }
        else
        {
          FormDial->btFuncF2->Caption = "F2";
          FormDial->lbFuncF2->Caption = "";
          FormDial->btFuncF2->Enabled = false;
        }
        break;
      case 3:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF3->Caption = "F3("+strCalledName+")";
          FormDial->lbFuncF3->Caption = strCalledNo;
          FormDial->btFuncF3->Enabled = true;
        }
        else
        {
          FormDial->btFuncF3->Caption = "F3";
          FormDial->lbFuncF3->Caption = "";
          FormDial->btFuncF3->Enabled = false;
        }
        break;
      case 4:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF4->Caption = "F4("+strCalledName+")";
          FormDial->lbFuncF4->Caption = strCalledNo;
          FormDial->btFuncF4->Enabled = true;
        }
        else
        {
          FormDial->btFuncF4->Caption = "F4";
          FormDial->lbFuncF4->Caption = "";
          FormDial->btFuncF4->Enabled = false;
        }
        break;
      case 5:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF5->Caption = "F5("+strCalledName+")";
          FormDial->lbFuncF5->Caption = strCalledNo;
          FormDial->btFuncF5->Enabled = true;
        }
        else
        {
          FormDial->btFuncF5->Caption = "F5";
          FormDial->lbFuncF5->Caption = "";
          FormDial->btFuncF5->Enabled = false;
        }
        break;
      case 6:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF6->Caption = "F6("+strCalledName+")";
          FormDial->lbFuncF6->Caption = strCalledNo;
          FormDial->btFuncF6->Enabled = true;
        }
        else
        {
          FormDial->btFuncF6->Caption = "F6";
          FormDial->lbFuncF6->Caption = "";
          FormDial->btFuncF6->Enabled = false;
        }
        break;
      case 7:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF7->Caption = "F7("+strCalledName+")";
          FormDial->lbFuncF7->Caption = strCalledNo;
          FormDial->btFuncF7->Enabled = true;
        }
        else
        {
          FormDial->btFuncF7->Caption = "F7";
          FormDial->lbFuncF7->Caption = "";
          FormDial->btFuncF7->Enabled = false;
        }
        break;
      case 8:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF8->Caption = "F8("+strCalledName+")";
          FormDial->lbFuncF8->Caption = strCalledNo;
          FormDial->btFuncF8->Enabled = true;
        }
        else
        {
          FormDial->btFuncF8->Caption = "F8";
          FormDial->lbFuncF8->Caption = "";
          FormDial->btFuncF8->Enabled = false;
        }
        break;
      case 9:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF9->Caption = "F9("+strCalledName+")";
          FormDial->lbFuncF9->Caption = strCalledNo;
          FormDial->btFuncF9->Enabled = true;
        }
        else
        {
          FormDial->btFuncF9->Caption = "F9";
          FormDial->lbFuncF9->Caption = "";
          FormDial->btFuncF9->Enabled = false;
        }
        break;
      case 10:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF10->Caption = "F10("+strCalledName+")";
          FormDial->lbFuncF10->Caption = strCalledNo;
          FormDial->btFuncF10->Enabled = true;
        }
        else
        {
          FormDial->btFuncF10->Caption = "F10";
          FormDial->lbFuncF10->Caption = "";
          FormDial->btFuncF10->Enabled = false;
        }
        break;
      case 11:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF11->Caption = "F11("+strCalledName+")";
          FormDial->lbFuncF11->Caption = strCalledNo;
          FormDial->btFuncF11->Enabled = true;
        }
        else
        {
          FormDial->btFuncF11->Caption = "F11";
          FormDial->lbFuncF11->Caption = "";
          FormDial->btFuncF11->Enabled = false;
        }
        break;
      case 12:
        if (strCalledNo.Length() > 0)
        {
          FormDial->btFuncF12->Caption = "F12("+strCalledName+")";
          FormDial->lbFuncF12->Caption = strCalledNo;
          FormDial->btFuncF12->Enabled = true;
        }
        else
        {
          FormDial->btFuncF12->Caption = "F12";
          FormDial->lbFuncF12->Caption = "";
          FormDial->btFuncF12->Enabled = false;
        }
        break;
      }
      adoqueryPub->Next();
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::GetCallOutFuncKey(int nFuncKeyId, AnsiString &strCalledNo, AnsiString &strCalledName)
{
  char sqlbuf[128];
  bool nResult=0;

  strCalledNo="";
  strCalledName="";
  sprintf(sqlbuf, "Select CalledNo,CalledName from tbtranoutfunckey where FuncKeyId=%d", nFuncKeyId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strCalledNo = adoqueryPub->FieldByName("CalledNo")->AsString;
      strCalledName = adoqueryPub->FieldByName("CalledName")->AsString;
      nResult = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::UpdateCallOutFuncKey(int nFuncKeyId, AnsiString strCalledNo, AnsiString strCalledName)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbtranoutfunckey set CalledNo='%s',CalledName='%s' where FuncKeyId=%d",
    strCalledNo.c_str(), strCalledName.c_str(), nFuncKeyId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetIVRMenuSetRecord(TADOQuery *adoquery, CIVRMenuSet *ivrmenuset)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  ivrmenuset->id = adoquery->FieldByName("id")->AsInteger;
	  ivrmenuset->CalledNo = adoquery->FieldByName("CalledNo")->AsString;
	  ivrmenuset->WellcomeId = adoquery->FieldByName("WellcomeId")->AsInteger;
	  ivrmenuset->VocMenuId = adoquery->FieldByName("VocMenuId")->AsInteger;
	  ivrmenuset->DIDSeatNo = adoquery->FieldByName("DIDSeatNo")->AsString;
	  ivrmenuset->DIDWorkerNo = adoquery->FieldByName("DIDWorkerNo")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertIVRMenuSetRecord(CIVRMenuSet *ivrmenuset)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into tbivrmenuset (\
CalledNo,\
WellcomeId,\
VocMenuId,\
DIDSeatNo,\
DIDWorkerNo\
) values ('%s',%d,%d,'%s','%s')",
  ivrmenuset->CalledNo.c_str(),
  ivrmenuset->WellcomeId,
  ivrmenuset->VocMenuId,
  ivrmenuset->DIDSeatNo.c_str(),
  ivrmenuset->DIDWorkerNo.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateIVRMenuSetRecord(CIVRMenuSet *ivrmenuset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbivrmenuset set \
CalledNo='%s',\
WellcomeId=%d,\
VocMenuId=%d,\
DIDSeatNo='%s',\
DIDWorkerNo='%s' \
where id=%d",
  ivrmenuset->CalledNo.c_str(),
  ivrmenuset->WellcomeId,
  ivrmenuset->VocMenuId,
  ivrmenuset->DIDSeatNo.c_str(),
  ivrmenuset->DIDWorkerNo.c_str(),
  ivrmenuset->id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteIVRMenuSetRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbivrmenuset where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}


void __fastcall TdmAgent::adoQryIVRSetCalcFields(TDataSet *DataSet)
{
  if (adoQryIVRSetWellcomeId->AsInteger == 0)
    adoQryIVRSetDispWellcomeId->AsString = "不播報";
  else
    adoQryIVRSetDispWellcomeId->AsString = "播報個性化歡迎詞";
  if (adoQryIVRSetVocMenuId->AsInteger == 0)
    adoQryIVRSetDispVocMenuId->AsString = "不播報";
  else
    adoQryIVRSetDispVocMenuId->AsString = "播報個性化語音選單";
  adoQryIVRSetDispWorkerName->AsString = GetWorkerName(adoQryIVRSetDIDWorkerNo->AsString);
}
//---------------------------------------------------------------------------
void TdmAgent::UpdatemaxFaxChnNum(int maxfaxnum, int maxsmsnum)
{
  char sqlbuf[128];

  if (ADOConnection1->Connected == false)
    return;
  if (CurMaxFaxNum == maxfaxnum)
    return;
  sprintf(sqlbuf, "update tbsysset set MaxFaxChn=%d,SmsId=%d where id=1", maxfaxnum, maxsmsnum);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QryRecvFaxRuleSet(int nSelectRuleId)
{
  AnsiString sqlBuf;

  switch (nSelectRuleId)
  {
  case 1: //按通道號分發
    sqlBuf = "select id,ChnNo as ruleitem,RecvDepartmentId,RecvWorkerNo from tbRecvfaxRuleChn order by id";
    break;
  case 2: //按被叫號碼分發
    sqlBuf = "select id,CalledNo as ruleitem,RecvDepartmentId,RecvWorkerNo from tbRecvfaxRuleCalled order by id";
    break;
  case 3: //按主叫號碼分發
    sqlBuf = "select id,CallerNo as ruleitem,RecvDepartmentId,RecvWorkerNo from tbRecvfaxRuleCaller order by id";
    break;
  case 4: //按傳真標識分發
    sqlBuf = "select id,CSID as ruleitem,RecvDepartmentId,RecvWorkerNo from tbRecvfaxRuleCSID order by id";
    break;
  case 5: //按二維碼分發
    sqlBuf = "select id,Barcode as ruleitem,RecvDepartmentId,RecvWorkerNo from tbRecvfaxRuleBarcode order by id";
    break;
  default:
    return;
  }
  try
  {
    adoQryRecvFaxRule->SQL->Clear();
    adoQryRecvFaxRule->SQL->Add(sqlBuf);
    adoQryRecvFaxRule->Open();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::GetRecvFaxRuleSetRecord(TADOQuery *adoquery, int nSelectRuleId, CRecvFaxRuleSet *pRecvFaxRuleSet)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  pRecvFaxRuleSet->nRuleId = nSelectRuleId;

	  pRecvFaxRuleSet->id = adoquery->FieldByName("id")->AsInteger;
	  pRecvFaxRuleSet->RuleItem = adoquery->FieldByName("RuleItem")->AsString;
	  pRecvFaxRuleSet->DepartmentId = adoquery->FieldByName("RecvDepartmentId")->AsInteger;
	  pRecvFaxRuleSet->WorkerNo = adoquery->FieldByName("RecvWorkerNo")->AsString;

	  pRecvFaxRuleSet->DepartmentName = GetWorkerName(pRecvFaxRuleSet->WorkerNo);
	  pRecvFaxRuleSet->WorkerName = DepartmentItemList.GetItemName(pRecvFaxRuleSet->DepartmentId);
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::IsRecvFaxRuleItemIdExist(int nSelectRuleId, AnsiString strRuleItem)
{
  AnsiString sqlBuf;
  int result=0;

  switch (nSelectRuleId)
  {
  case 1: //按通道號分發
    sqlBuf = "select id from tbRecvfaxRuleChn where ChnNo='"+strRuleItem+"'";
    break;
  case 2: //按被叫號碼分發
    sqlBuf = "select id from tbRecvfaxRuleCalled where CalledNo='"+strRuleItem+"'";
    break;
  case 3: //按主叫號碼分發
    sqlBuf = "select id from tbRecvfaxRuleCaller where CallerNo='"+strRuleItem+"'";
    break;
  case 4: //按傳真標識分發
    sqlBuf = "select id from tbRecvfaxRuleCSID where CSID='"+strRuleItem+"'";
    break;
  case 5: //按二維碼分發
    sqlBuf = "select id from tbRecvfaxRuleBarcode where Barcode='"+strRuleItem+"'";
    break;
  default:
    return result;
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlBuf);
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertRecvFaxRuleRecord(int nSelectRuleId, CRecvFaxRuleSet *pRecvFaxRuleSet)
{
  char sqlbuf[256];

  switch (nSelectRuleId)
  {
  case 1: //按通道號分發
    sprintf(sqlbuf, "insert into tbRecvfaxRuleChn (ChnNo,RecvDepartmentId,RecvWorkerNo) values ('%s',%d,'%s')",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str());
    break;
  case 2: //按被叫號碼分發
    sprintf(sqlbuf, "insert into tbRecvfaxRuleCalled (CalledNo,RecvDepartmentId,RecvWorkerNo) values ('%s',%d,'%s')",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str());
    break;
  case 3: //按主叫號碼分發
    sprintf(sqlbuf, "insert into tbRecvfaxRuleCaller (CallerNo,RecvDepartmentId,RecvWorkerNo) values ('%s',%d,'%s')",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str());
    break;
  case 4: //按傳真標識分發
    sprintf(sqlbuf, "insert into tbRecvfaxRuleCSID (CSID,RecvDepartmentId,RecvWorkerNo) values ('%s',%d,'%s')",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str());
    break;
  case 5: //按二維碼分發
    sprintf(sqlbuf, "insert into tbRecvfaxRuleBarcode (Barcode,RecvDepartmentId,RecvWorkerNo) valuse ('%s',%d,'%s')",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str());
    break;
  default:
    return 1;
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvFaxRuleRecord(int nSelectRuleId, CRecvFaxRuleSet *pRecvFaxRuleSet)
{
  char sqlbuf[256];

  switch (nSelectRuleId)
  {
  case 1: //按通道號分發
    sprintf(sqlbuf, "update tbRecvfaxRuleChn set ChnNo='%s',RecvDepartmentId=%d,RecvWorkerNo='%s' where id=%d",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str(), pRecvFaxRuleSet->id);
    break;
  case 2: //按被叫號碼分發
    sprintf(sqlbuf, "update tbRecvfaxRuleCalled set CalledNo='%s',RecvDepartmentId=%d,RecvWorkerNo='%s' where id=%d",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str(), pRecvFaxRuleSet->id);
    break;
  case 3: //按主叫號碼分發
    sprintf(sqlbuf, "update tbRecvfaxRuleCaller set CallerNo='%s',RecvDepartmentId=%d,RecvWorkerNo='%s' where id=%d",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str(), pRecvFaxRuleSet->id);
    break;
  case 4: //按傳真標識分發
    sprintf(sqlbuf, "update tbRecvfaxRuleCSID set CSID='%s',RecvDepartmentId=%d,RecvWorkerNo='%s' where id=%d",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str(), pRecvFaxRuleSet->id);
    break;
  case 5: //按二維碼分發
    sprintf(sqlbuf, "update tbRecvfaxRuleBarcode set Barcode='%s',RecvDepartmentId=%d,RecvWorkerNo='%s' where id=%d",
      pRecvFaxRuleSet->RuleItem.c_str(), pRecvFaxRuleSet->DepartmentId, pRecvFaxRuleSet->WorkerNo.c_str(), pRecvFaxRuleSet->id);
    break;
  default:
    return 1;
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteRecvFaxRuleRecord(int nSelectRuleId, int nItemId)
{
  char sqlbuf[256];

  switch (nSelectRuleId)
  {
  case 1: //按通道號分發
    sprintf(sqlbuf, "delete from tbRecvfaxRuleChn where id=%d", nItemId);
    break;
  case 2: //按被叫號碼分發
    sprintf(sqlbuf, "delete from tbRecvfaxRuleCalled where id=%d", nItemId);
    break;
  case 3: //按主叫號碼分發
    sprintf(sqlbuf, "delete from tbRecvfaxRuleCaller where id=%d", nItemId);
    break;
  case 4: //按傳真標識分發
    sprintf(sqlbuf, "delete from tbRecvfaxRuleCSID where id=%d", nItemId);
    break;
  case 5: //按二維碼分發
    sprintf(sqlbuf, "delete from tbRecvfaxRuleBarcode where id=%d", nItemId);
    break;
  default:
    return 1;
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
AnsiString TdmAgent::GetRecvFaxRuleName(int nRuleId)
{
  AnsiString dtrRecvFaxRuleName="";
  switch (nRuleId)
  {
  case 0:
    dtrRecvFaxRuleName = "按傳真信箱號分發";
    break;
  case 1:
    dtrRecvFaxRuleName = "按通道號分發";
    break;
  case 2:
    dtrRecvFaxRuleName = "按被叫號碼分發";
    break;
  case 3:
    dtrRecvFaxRuleName = "按主叫號碼分發";
    break;
  case 4:
    dtrRecvFaxRuleName = "按傳真標識分發";
    break;
  }
  return dtrRecvFaxRuleName;
}
int TdmAgent::GetRecvFaxRuleId(AnsiString strRuleName)
{
  if (strRuleName == "按傳真信箱號分發")
    return 0;
  else if (strRuleName == "按通道號分發")
    return 1;
  else if (strRuleName == "按被叫號碼分發")
    return 2;
  else if (strRuleName == "按主叫號碼分發")
    return 3;
  else if (strRuleName == "按傳真標識分發")
    return 4;
  else
    return 0;
}
void TdmAgent::GetRecvFaxRuleOrder()
{
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add("select OrderNo1,OrderNo2,OrderNo3,OrderNo4,OrderNo5 from tbRecvfaxRuleOrder where id=1");
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      FormMain->editRecvFaxRuleOrder1->Text = GetRecvFaxRuleName(adoqueryPub->FieldByName("OrderNo1")->AsInteger);
      FormMain->cbRecvFaxRuleOrder2->Text = GetRecvFaxRuleName(adoqueryPub->FieldByName("OrderNo2")->AsInteger);
      FormMain->cbRecvFaxRuleOrder3->Text = GetRecvFaxRuleName(adoqueryPub->FieldByName("OrderNo3")->AsInteger);
      FormMain->cbRecvFaxRuleOrder4->Text = GetRecvFaxRuleName(adoqueryPub->FieldByName("OrderNo4")->AsInteger);
      FormMain->cbRecvFaxRuleOrder5->Text = GetRecvFaxRuleName(adoqueryPub->FieldByName("OrderNo5")->AsInteger);
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::UpdateRecvFaxRuleOrder(int nOrder2, int nOrder3, int nOrder4, int nOrder5)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "update tbRecvfaxRuleOrder set OrderNo2=%d,OrderNo3=%d,OrderNo4=%d,OrderNo5=%d where id=1",
    nOrder2, nOrder3, nOrder4, nOrder5);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}

void __fastcall TdmAgent::adoQryRecvFaxRuleCalcFields(TDataSet *DataSet)
{
  if (adoQryRecvFaxRuleRecvDepartmentId->AsInteger == 0
    && (adoQryRecvFaxRuleRecvWorkerNo->AsString == "0" || adoQryRecvFaxRuleRecvWorkerNo->AsString == ""))
  {
    adoQryRecvFaxRuleDispRecvFaxType->AsString = "公司公共傳真";
    adoQryRecvFaxRuleDispDepartmentName->AsString = "無";
    adoQryRecvFaxRuleDispWorkerName->AsString = "無";
  }
  else if (adoQryRecvFaxRuleRecvDepartmentId->AsInteger != 0
    && (adoQryRecvFaxRuleRecvWorkerNo->AsString == "0" || adoQryRecvFaxRuleRecvWorkerNo->AsString == ""))
  {
    adoQryRecvFaxRuleDispRecvFaxType->AsString = "部門公共傳真";
    adoQryRecvFaxRuleDispDepartmentName->AsString = dmAgent->DepartmentItemList.GetItemName(adoQryRecvFaxRuleRecvDepartmentId->AsInteger);
    adoQryRecvFaxRuleDispWorkerName->AsString = "無";
  }
  else
  {
    adoQryRecvFaxRuleDispRecvFaxType->AsString = "個人傳真";
    adoQryRecvFaxRuleDispDepartmentName->AsString = "無";
    adoQryRecvFaxRuleDispWorkerName->AsString = dmAgent->GetWorkerName(adoQryRecvFaxRuleRecvWorkerNo->AsString);
  }
}
//---------------------------------------------------------------------------
int TdmAgent::InsertSendFaxTask(CSendFaxFileList *pSendFaxFileList, const char *faxnos)
{
  char sqlbuf[256];
  AnsiString strTaskGid;
  TStringList *strList = new TStringList;
  int i, nNum, nResult=3, count=0;

  if (pSendFaxFileList->nFileNum == 0)
    return 1;
  nNum = MySplit(faxnos, ';', strList);
  if (nNum == 0)
    return 2;
  try
  {
    if (strSendFaxTaskId == "")
    {
      strTaskGid = MyGetGIUD();
      sprintf(sqlbuf, "insert into tbSendfaxTask (TaskGid,SendRootPath,TotalFileNum,TaskTime) values ('%s','%s',%d,%s)",
        strTaskGid.c_str(), RecvFaxPath.c_str(), pSendFaxFileList->nFileNum, MyNowFuncName.c_str());

      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      for (i=0; i<5; i++)
      {
        if (pSendFaxFileList->SendFaxFileDoc[i].Length() > 0)
        {
          sprintf(sqlbuf, "insert into tbSendfaxFile (TaskGid,SendRootPath,SendFile) values ('%s','%s','%s')",
            strTaskGid.c_str(), RecvFaxPath.c_str(), pSendFaxFileList->SendFaxFileDoc[i].c_str());
          adoqueryPub->SQL->Clear();
          adoqueryPub->SQL->Add((char *)sqlbuf );
          adoqueryPub->ExecSQL();
        }
      }
    }
    else
    {
      strTaskGid = strSendFaxTaskId;
      sprintf(sqlbuf, "update tbSendfaxTask set TotalTeleNum=TotalTeleNum+%d where TaskGid='%s'", nNum, strTaskGid.c_str());

      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
    }
    strSendFaxTaskId = strTaskGid;
    for (int i=0; i<nNum; i++)
    {
      if (strList->Strings[i].Length() == 0)
        continue;
      sprintf(sqlbuf, "insert into tbSendfaxTele (TaskGid,FaxNo,CustomNo,SendWorkerNo,SendTime,SendState) values ('%s','%s','%s','%s',%s,1)",
        strTaskGid.c_str(), strList->Strings[i].c_str(), GetCustomNoByFaxNo(strList->Strings[i]).c_str(),
        MyWorker.WorkerNo.c_str(), MyNowFuncName.c_str());

      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
      count++;
    }
    if (count > 0)
    {
      sprintf(sqlbuf, "update tbSendfaxTask set TotalTeleNum=TotalTeleNum+%d where TaskGid='%s'", count, strTaskGid.c_str());
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf );
      adoqueryPub->ExecSQL();
    }
    nResult = 0;
  }
  __finally
  {
    delete strList;
  }
  return nResult;
}
int TdmAgent::UpdateSendFaxState(int id, int nState)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "update tbSendfaxTele set SendState=%d,SendedTimes=0 where id=%d",
    nState, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateTranState(AnsiString strTaskGid, int nState)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "update tbSendfaxTask set TranAllId=%d where TaskGid='%s'",
    nState, strTaskGid.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateSendFaxDelFlag(int id, int nState)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "update tbSendfaxTele set DelFlag=%d where id=%d",
    nState, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateRecvFaxDelFlag(int id, int nState)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "update tbrecvfax set DelFlag=%d where id=%d",
    nState, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertSendFaxTele(AnsiString strTaskGid, AnsiString strFaxNo)
{
  char sqlbuf[512];

  sprintf(sqlbuf, "insert into tbSendfaxTele (TaskGid,FaxNo,CustomNo,SendWorkerNo,SendTime,SendState) values ('%s','%s','%s','%s',%s,1)",
    strTaskGid.c_str(), strFaxNo.c_str(), GetCustomNoByFaxNo(strFaxNo).c_str(),
    MyWorker.WorkerNo, MyNowFuncName.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetRecvFaxRecord(TADOQuery *adoquery, CRecvFaxRecord *pRecvFaxRecord)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  pRecvFaxRecord->id = adoquery->FieldByName("id")->AsInteger;
	  pRecvFaxRecord->SerialNo = adoquery->FieldByName("SerialNo")->AsString;
	  pRecvFaxRecord->CallerNo = adoquery->FieldByName("CallerNo")->AsString;
	  pRecvFaxRecord->CustomNo = adoquery->FieldByName("CustomNo")->AsString;
	  pRecvFaxRecord->CalledNo = adoquery->FieldByName("CalledNo")->AsString;
	  pRecvFaxRecord->RecvTime = adoquery->FieldByName("RecvTime")->AsString;
	  pRecvFaxRecord->RecvRootPath = adoquery->FieldByName("RecvRootPath")->AsString;
	  pRecvFaxRecord->RecvFile = adoquery->FieldByName("RecvFile")->AsString;
	  pRecvFaxRecord->FaxCSID = adoquery->FieldByName("FaxCSID")->AsString;
	  pRecvFaxRecord->RecvDepartmentId = adoquery->FieldByName("RecvDepartmentId")->AsInteger;
	  pRecvFaxRecord->RecvWorkerNo = adoquery->FieldByName("RecvWorkerNo")->AsString;
	  pRecvFaxRecord->RecvPages = adoquery->FieldByName("RecvPages")->AsInteger;
	  pRecvFaxRecord->RecvSizes = adoquery->FieldByName("RecvSizes")->AsInteger;
	  pRecvFaxRecord->RecvSpeed = adoquery->FieldByName("RecvSpeed")->AsInteger;
	  pRecvFaxRecord->SourceId = adoquery->FieldByName("SourceId")->AsInteger;
	  pRecvFaxRecord->SortType = adoquery->FieldByName("SortType")->AsInteger;
	  pRecvFaxRecord->ChnType = adoquery->FieldByName("ChnType")->AsInteger;
	  pRecvFaxRecord->ChnNo = adoquery->FieldByName("ChnNo")->AsInteger;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::TranSendRecvFaxRecord(CRecvFaxRecord *pRecvFaxRecord, AnsiString strFaxNo)
{
  char sqlbuf[512];
  AnsiString strTaskGid;

  try
  {
    strTaskGid = MyGetGIUD();
    sprintf(sqlbuf, "insert into tbSendfaxTask (TaskGid,SendRootPath,FaxFile,TaskTime,TotalFileNum,TotalTeleNum,TranAllId) values ('%s','%s','%s',%s,1,1,2)",
      strTaskGid.c_str(), pRecvFaxRecord->RecvRootPath.c_str(), pRecvFaxRecord->RecvFile.c_str(), MyNowFuncName.c_str());

    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();

    sprintf(sqlbuf, "insert into tbSendfaxFile (TaskGid,SendRootPath,FaxFile,TranId) values ('%s','%s','%s',2)",
      strTaskGid.c_str(), pRecvFaxRecord->RecvRootPath.c_str(), pRecvFaxRecord->RecvFile.c_str());
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();

    sprintf(sqlbuf, "insert into tbSendfaxTele (TaskGid,FaxNo,CustomNo,SendWorkerNo,SendTime,SendState) values ('%s','%s','%s','%s',%s,1)",
      strTaskGid.c_str(), strFaxNo.c_str(), GetCustomNoByFaxNo(strFaxNo).c_str(),
      MyWorker.WorkerNo, MyNowFuncName.c_str());

    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::TranInterRecvFaxRecord(CRecvFaxRecord *pRecvFaxRecord, AnsiString strWorkerNo)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into tbrecvfax (\
SerialNo,\
CallerNo,\
CustomNo,\
CalledNo,\
RecvTime,\
RecvRootPath,\
RecvFile,\
FaxCSID,\
RecvWorkerNo,\
RecvPages,\
RecvSizes,\
RecvSpeed,\
ChnType,\
ChnNo,\
SourceId\
) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,1)",
  pRecvFaxRecord->SerialNo.c_str(),
  pRecvFaxRecord->CallerNo.c_str(),
  pRecvFaxRecord->CustomNo.c_str(),
  pRecvFaxRecord->CalledNo.c_str(),
  pRecvFaxRecord->RecvTime.c_str(),
  pRecvFaxRecord->RecvRootPath.c_str(),
  pRecvFaxRecord->RecvFile.c_str(),
  pRecvFaxRecord->FaxCSID.c_str(),
  strWorkerNo.c_str(),
  pRecvFaxRecord->RecvPages,
  pRecvFaxRecord->RecvSizes,
  pRecvFaxRecord->RecvSpeed,
  pRecvFaxRecord->ChnType,
  pRecvFaxRecord->ChnNo);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQrySendFaxCalcFields(TDataSet *DataSet)
{
  adoQrySendFaxDispSendState->AsString = SendFaxStateItemList.GetItemName(adoQrySendFaxSendState->AsInteger);
  adoQrySendFaxDispTranAllId->AsString = DocTranStateItemList.GetItemName(adoQrySendFaxTranAllId->AsInteger);
  adoQrySendFaxCustName->AsString = GetCustNameByCustNo(adoQrySendFaxCustomNo->AsString);
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryRecvFaxCalcFields(TDataSet *DataSet)
{
  adoQryRecvFaxDispReadId->AsString = FaxReadIdItemList.GetItemName(adoQryRecvFaxReadId->AsInteger);
  adoQryRecvFaxDispProcId->AsString = RecdProcTypeItemList.GetItemName(adoQryRecvFaxProcId->AsInteger);
  switch (adoQryRecvFaxTranEmailId->AsInteger)
  {
  case 0: adoQryRecvFaxDispTranEmailId->AsString = "不轉發"; break;
  case 1: adoQryRecvFaxDispTranEmailId->AsString = "等待轉發"; break;
  case 2: adoQryRecvFaxDispTranEmailId->AsString = "正在轉發"; break;
  case 3: adoQryRecvFaxDispTranEmailId->AsString = "轉發成功"; break;
  case 4: adoQryRecvFaxDispTranEmailId->AsString = "轉發失敗"; break;
  }
}
//---------------------------------------------------------------------------
void TdmAgent::QueryCustExItemSet()
{
  try
  {
    adoQryCustExSet->Close();
    adoQryCustExSet->Open();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::IsCustExItemSetFieldIdExist(int nFieldId)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select CFieldId from tbcustexset where CFieldId=%d", nFieldId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::GetCustExItemSetRecord(TADOQuery *adoquery, CCustExItemSet *custexitemset)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
    custexitemset->state = 1;
	  custexitemset->CFieldId = adoquery->FieldByName("CFieldId")->AsInteger;
	  custexitemset->CFieldName = adoquery->FieldByName("CFieldName")->AsString;
	  custexitemset->CControlType = adoquery->FieldByName("CControlType")->AsInteger;
	  custexitemset->CLabelCaption = adoquery->FieldByName("CLabelCaption")->AsString;
	  //custexitemset->CXpos = adoquery->FieldByName("CXpos")->AsInteger;
	  //custexitemset->CYpos = adoquery->FieldByName("CYpos")->AsInteger;
	  //custexitemset->CHeigth = adoquery->FieldByName("CHeigth")->AsInteger;
	  custexitemset->CWidth = adoquery->FieldByName("CWidth")->AsInteger;
	  //custexitemset->CDataType = adoquery->FieldByName("CDataType")->AsInteger;
	  custexitemset->CDefaultData = adoquery->FieldByName("CDefaultData")->AsString;
	  custexitemset->CMinValue = adoquery->FieldByName("CMinValue")->AsInteger;
	  custexitemset->CMaxValue = adoquery->FieldByName("CMaxValue")->AsInteger;
    custexitemset->CIsAllowNull = adoquery->FieldByName("CIsAllowNull")->AsInteger;
    custexitemset->CDispId = adoquery->FieldByName("CDispId")->AsInteger;
    custexitemset->CDispWidth = adoquery->FieldByName("CDispWidth")->AsInteger;
    custexitemset->CInputId = adoquery->FieldByName("CInputId")->AsInteger;
    custexitemset->CSearchId = adoquery->FieldByName("CSearchId")->AsInteger;
    custexitemset->CIsUsed = adoquery->FieldByName("CIsUsed")->AsInteger;
    custexitemset->CExportId = adoquery->FieldByName("CExportId")->AsInteger;
    custexitemset->ExportOrderId = adoquery->FieldByName("ExportOrderId")->AsInteger;
    custexitemset->AcceptExId = adoquery->FieldByName("AcceptExId")->AsInteger;

    custexitemset->ReadDataType = adoquery->FieldByName("ReadDataType")->AsInteger;
	  custexitemset->ReadFieldTag = adoquery->FieldByName("ReadFieldTag")->AsString;
    custexitemset->SaveToDBFlag = adoquery->FieldByName("SaveToDBFlag")->AsInteger;
    //custexitemset->ReadDataMinLen = adoquery->FieldByName("ReadDataMinLen")->AsInteger; //2022-01-21
    //custexitemset->ReadDataMaxLen = adoquery->FieldByName("ReadDataMaxLen")->AsInteger; //2022-01-21

    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetCustExItemSetRecord(int nFieldId, CCustExItemSet *custexitemset)
{
  char sqlbuf[128];
  int result=1;

  sprintf(sqlbuf, "Select * from tbcustexset where CFieldId=%d", nFieldId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      custexitemset->state = 1;
      custexitemset->CFieldId = adoqueryPub->FieldByName("CFieldId")->AsInteger;
      custexitemset->CFieldName = adoqueryPub->FieldByName("CFieldName")->AsString;
      custexitemset->CControlType = adoqueryPub->FieldByName("CControlType")->AsInteger;
      custexitemset->CLabelCaption = adoqueryPub->FieldByName("CLabelCaption")->AsString;
      //custexitemset->CXpos = adoqueryPub->FieldByName("CXpos")->AsInteger;
      //custexitemset->CYpos = adoqueryPub->FieldByName("CYpos")->AsInteger;
      //custexitemset->CHeigth = adoqueryPub->FieldByName("CHeigth")->AsInteger;
      custexitemset->CWidth = adoqueryPub->FieldByName("CWidth")->AsInteger;
      //custexitemset->CDataType = adoqueryPub->FieldByName("CDataType")->AsInteger;
      custexitemset->CDefaultData = adoqueryPub->FieldByName("CDefaultData")->AsString;
      custexitemset->CMinValue = adoqueryPub->FieldByName("CMinValue")->AsInteger;
      custexitemset->CMaxValue = adoqueryPub->FieldByName("CMaxValue")->AsInteger;
      custexitemset->CIsAllowNull = adoqueryPub->FieldByName("CIsAllowNull")->AsInteger;
      custexitemset->CDispId = adoqueryPub->FieldByName("CDispId")->AsInteger;
      custexitemset->CDispWidth = adoqueryPub->FieldByName("CDispWidth")->AsInteger;
      custexitemset->CInputId = adoqueryPub->FieldByName("CInputId")->AsInteger;
      custexitemset->CSearchId = adoqueryPub->FieldByName("CSearchId")->AsInteger;
      custexitemset->CIsUsed = adoqueryPub->FieldByName("CIsUsed")->AsInteger;
      custexitemset->CExportId = adoqueryPub->FieldByName("CExportId")->AsInteger;
      custexitemset->ExportOrderId = adoqueryPub->FieldByName("ExportOrderId")->AsInteger;
      custexitemset->AcceptExId = adoqueryPub->FieldByName("AcceptExId")->AsInteger;

      custexitemset->ReadDataType = adoqueryPub->FieldByName("ReadDataType")->AsInteger;
	    custexitemset->ReadFieldTag = adoqueryPub->FieldByName("ReadFieldTag")->AsString;
      custexitemset->SaveToDBFlag = adoqueryPub->FieldByName("SaveToDBFlag")->AsInteger;
      //custexitemset->ReadDataMinLen = adoqueryPub->FieldByName("ReadDataMinLen")->AsInteger; //2022-01-21
      //custexitemset->ReadDataMaxLen = adoqueryPub->FieldByName("ReadDataMaxLen")->AsInteger; //2022-01-21
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertCustExItemSet(CCustExItemSet *custexitemset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbcustexset (\
CFieldId,\
CFieldName,\
CControlType,\
CLabelCaption,\
CWidth,\
CDefaultData,\
CMinValue,\
CMaxValue,\
CIsAllowNull,\
CDispId,\
CInputId,\
CSearchId,\
CIsUsed,\
CExportId,\
ExportOrderId,\
AcceptExId,\
ReadDataType,\
ReadFieldTag,\
SaveToDBFlag\
) values (%d,'%s',%d,'%s',%d,'%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s',%d)",
  custexitemset->CFieldId,
  custexitemset->CFieldName.c_str(),
  custexitemset->CControlType,
  custexitemset->CLabelCaption.c_str(),
  custexitemset->CWidth,
  custexitemset->CDefaultData.c_str(),
  custexitemset->CMinValue,
  custexitemset->CMaxValue,
  custexitemset->CIsAllowNull,
  custexitemset->CDispId,
  custexitemset->CInputId,
  custexitemset->CSearchId,
  custexitemset->CIsUsed,
  custexitemset->CExportId,
  custexitemset->ExportOrderId,
  custexitemset->AcceptExId,
  custexitemset->ReadDataType,
  custexitemset->ReadFieldTag.c_str(),
  custexitemset->SaveToDBFlag
  );

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateCustExItemSet(CCustExItemSet *custexitemset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbcustexset set \
CFieldName='%s',\
CControlType=%d,\
CLabelCaption='%s',\
CWidth=%d,\
CDefaultData='%s',\
CMinValue=%d,\
CMaxValue=%d,\
CIsAllowNull=%d,\
CDispId=%d,\
CInputId=%d,\
CSearchId=%d,\
CIsUsed=%d,\
CExportId=%d,\
ExportOrderId=%d,\
AcceptExId=%d,\
ReadDataType=%d,\
ReadFieldTag='%s',\
SaveToDBFlag=%d \
where CFieldId=%d",
  custexitemset->CFieldName.c_str(),
  custexitemset->CControlType,
  custexitemset->CLabelCaption.c_str(),
  custexitemset->CWidth,
  custexitemset->CDefaultData.c_str(),
  custexitemset->CMinValue,
  custexitemset->CMaxValue,
  custexitemset->CIsAllowNull,
  custexitemset->CDispId,
  custexitemset->CInputId,
  custexitemset->CSearchId,
  custexitemset->CIsUsed,
  custexitemset->CExportId,
  custexitemset->ExportOrderId,
  custexitemset->AcceptExId,
  custexitemset->ReadDataType,
  custexitemset->ReadFieldTag.c_str(),
  custexitemset->SaveToDBFlag,
  custexitemset->CFieldId);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DelCustExItemSet(int nFieldId)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbcustexset where CFieldId=%d",
    nFieldId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  sprintf(sqlbuf, "delete from tbcustexfieldlist where CFieldId=%d",
    nFieldId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  return 0;
}
int TdmAgent::IsCustExItemListExist(int nFieldId, int nItemNo)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select CItemId from tbcustexfieldlist where CFieldId=%d and CItemId=%d",
    nFieldId, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertCustExItemListRecord(int nFieldId, int nItemNo, AnsiString strItemName)
{
  char sqlbuf[128];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbcustexfieldlist (CFieldId,CItemId,CItemText) values (%d,%d,'%s')",
    nFieldId, nItemNo, strItemName.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateCustExItemListRecord(int nFieldId, int nItemNo, AnsiString strItemName)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbcustexfieldlist set CItemText='%s' where CFieldId=%d and CItemId=%d",
    strItemName.c_str(), nFieldId, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteCustExItemListRecord(int nFieldId, int nItemNo)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbcustexfieldlist where CFieldId=%d and CItemId=%d",
    nFieldId, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::AddCustExItemListCmbItem(TComboBox *combobox, int nFieldId)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select CItemText from tbcustexfieldlist where CFieldId=%d order by CItemId",
      nFieldId);
    try
    {
        adoqueryPub1->SQL->Clear();
        adoqueryPub1->SQL->Add((char *)sqlbuf);
        adoqueryPub1->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub1->Eof )
    {
        itemname = adoqueryPub1->FieldByName("CItemText")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub1->Next();
    }
    adoqueryPub1->Close();
}
int TdmAgent::GetCustExSelItemValue(int nFieldId, AnsiString strItemName)
{
    char sqlbuf[256];
    int nResult=-1;

    if (ADOConnection1->Connected == false)
      return nResult;
    sprintf(sqlbuf, "Select CItemId from tbcustexfieldlist where CFieldId=%d and CItemText='%s'",
      nFieldId, strItemName.c_str());
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
        if ( !adoqueryPub->Eof )
        {
            nResult = adoqueryPub->FieldByName("CItemId")->AsInteger;
        }
        adoqueryPub->Close();
    }
    catch ( ... )
    {
    }
    return nResult;
}
//---------------------------------------------------------------------------
int TdmAgent::GetCustomerItemSet(CCustomerItemSetList *customeritemsetlist)
{
  char sqlbuf[128];
  int nFieldId, nControlCount;

  sprintf(sqlbuf, "select * from tbcustomset order by Id");
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub1->Open();
    nControlCount = dmAgent->adoqueryPub1->RecordCount;
    if (nControlCount == 0)
      return 1;
    if (customeritemsetlist->InitData(nControlCount) != 0)
      return 1;
    dmAgent->adoqueryPub1->First();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Id")->AsInteger;
      if (nFieldId >= MAX_CUSTOMER_CONTROL_NUM)
        break;

      customeritemsetlist->m_CustomerItemSet[nFieldId].state = 1;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CFieldId = nFieldId;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CFieldName = dmAgent->adoqueryPub1->FieldByName("FieldName")->AsString;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CControlType = dmAgent->adoqueryPub1->FieldByName("ControlType")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CLabelCaption = dmAgent->adoqueryPub1->FieldByName("FieldExplain")->AsString;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CDispId = dmAgent->adoqueryPub1->FieldByName("DispId")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CDispWidth = dmAgent->adoqueryPub1->FieldByName("DispWidth")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CInputId = dmAgent->adoqueryPub1->FieldByName("InputId")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].CExportId = dmAgent->adoqueryPub1->FieldByName("ExportId")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].ExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].AcceptExId = dmAgent->adoqueryPub1->FieldByName("AcceptExId")->AsInteger;

      customeritemsetlist->m_CustomerItemSet[nFieldId].ReadDataType = dmAgent->adoqueryPub1->FieldByName("ReadDataType")->AsInteger;
      customeritemsetlist->m_CustomerItemSet[nFieldId].ReadFieldTag = dmAgent->adoqueryPub1->FieldByName("ReadFieldTag")->AsString;
      customeritemsetlist->m_CustomerItemSet[nFieldId].SaveToDBFlag = dmAgent->adoqueryPub1->FieldByName("SaveToDBFlag")->AsInteger;
      //customeritemsetlist->m_CustomerItemSet[nFieldId].ReadDataMinLen = dmAgent->adoqueryPub1->FieldByName("ReadDataMinLen")->AsInteger; //2022-01-21
      //customeritemsetlist->m_CustomerItemSet[nFieldId].ReadDataMaxLen = dmAgent->adoqueryPub1->FieldByName("ReadDataMaxLen")->AsInteger; //2022-01-21

      WriteErrprMsg("GetCustomerItemSet nFieldId=%d CFieldName=%s ReadDataType=%d ReadFieldTag=%s SaveToDBFlag=%d",
        nFieldId,
        customeritemsetlist->m_CustomerItemSet[nFieldId].CFieldName.c_str(),
        customeritemsetlist->m_CustomerItemSet[nFieldId].ReadDataType,
        customeritemsetlist->m_CustomerItemSet[nFieldId].ReadFieldTag.c_str(),
        customeritemsetlist->m_CustomerItemSet[nFieldId].SaveToDBFlag);

      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetAllCustExItemSet(CCustExItemSetList *custexitemsetlist)
{
  char sqlbuf[128];
  int nFieldId, nControlCount;

  if (CustExOpenId == 0)
    return 2;
  sprintf(sqlbuf, "select * from tbcustexset where CFieldId>0 and CIsUsed=1 order by CFieldId");
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub1->Open();
    nControlCount = dmAgent->adoqueryPub1->RecordCount;
    if (nControlCount == 0)
      return 1;
    if (custexitemsetlist->InitData(nControlCount) != 0)
      return 1;
    dmAgent->adoqueryPub1->First();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("CFieldId")->AsInteger;
      if (nFieldId > MAX_CUST_CONTROL_NUM)
        break;
      if (dmAgent->GetCustExItemSetRecord(dmAgent->adoqueryPub1, &custexitemsetlist->m_CustExItemSet[nFieldId-1]) == 0)
      {
        GetAllCustExItemSetList(custexitemsetlist->m_CustExItemSet[nFieldId-1]);
      }
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::GetAllCustExItemSetList(CCustExItemSet &custexitemset)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select CItemId,CItemText from tbcustexfieldlist where CFieldId=%d order by CItemId",
      custexitemset.CFieldId);
    try
    {
      adoqueryPub2->SQL->Clear();
      adoqueryPub2->SQL->Add((char *)sqlbuf);
      adoqueryPub2->Open();
      itemnum = adoqueryPub2->RecordCount;
      if (custexitemset.TLookupItemList.InitData(itemnum) == 0)
      {
        while ( !adoqueryPub2->Eof )
        {
          itemvalue = adoqueryPub2->FieldByName("CItemId")->AsInteger;
          itemname = adoqueryPub2->FieldByName("CItemText")->AsString;
          //custexitemset.TLookupItemList.AddItem(itemvalue, IntToStr(itemvalue)+"-"+itemname);
          custexitemset.TLookupItemList.AddItem(itemvalue, itemname);
          adoqueryPub2->Next();
        }
      }
      adoqueryPub2->Close();
    }
    catch ( ... )
    {
    }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryCustExSetCalcFields(TDataSet *DataSet)
{
  adoQryCustExSetDispIsAllowNull->AsString = (adoQryCustExSetCIsAllowNull->AsInteger==1)?"是":"否";
  adoQryCustExSetDispDispId->AsString = (adoQryCustExSetCDispId->AsInteger==1)?"是":"否";
  if (adoQryCustExSetCInputId->AsInteger == 0)
    adoQryCustExSetDispInputId->AsString = "否";
  else if (adoQryCustExSetCInputId->AsInteger == 1)
    adoQryCustExSetDispInputId->AsString = "是";
  else if (adoQryCustExSetCInputId->AsInteger == 2)
    adoQryCustExSetDispInputId->AsString = "不能修改";
  else
    adoQryCustExSetDispInputId->AsString = "是";

  adoQryCustExSetDispSearchId->AsString = (adoQryCustExSetCSearchId->AsInteger==1)?"是":"否";
  adoQryCustExSetDispIsUsed->AsString = (adoQryCustExSetCIsUsed->AsInteger==1)?"是":"否";
  adoQryCustExSetDispExportId->AsString = (adoQryCustExSetCExportId->AsInteger==1)?"是":"否";
  if (adoQryCustExSetReadDataType->AsInteger == 1)
    adoQryCustExSetDispReadDataType->AsString = "DB資料庫";
  else if (adoQryCustExSetReadDataType->AsInteger == 2)
    adoQryCustExSetDispReadDataType->AsString = "HTTP介面";
  else if (adoQryCustExSetReadDataType->AsInteger == 3)
    adoQryCustExSetDispReadDataType->AsString = "WEBSERVICE介面";
  else if (adoQryCustExSetReadDataType->AsInteger == 4)
    adoQryCustExSetDispReadDataType->AsString = "同步CRM資料";
  else
    adoQryCustExSetDispReadDataType->AsString = "DB資料庫";
  adoQryCustExSetDispSaveToDBFlag->AsString = (adoQryCustExSetSaveToDBFlag->AsInteger==1)?"是":"否";
}
//---------------------------------------------------------------------------
void TdmAgent::QryCustomAllField()
{
  char sqlbuf[128];
  AnsiString strFieldName, strFieldExplain;
  int nFieldNum=0, nFieldId, nControlType, nAcceptExId;

  sprintf(sqlbuf, "Select FieldName,FieldExplain,AcceptExId from tbcustomset order by id");
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub->Open();
    while ( !dmAgent->adoqueryPub->Eof )
    {
      strFieldName = dmAgent->adoqueryPub->FieldByName("FieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub->FieldByName("FieldExplain")->AsString;
      nAcceptExId = dmAgent->adoqueryPub->FieldByName("AcceptExId")->AsInteger;

      FormMain->m_CustomAllFieldList.m_CustomExportField[nFieldNum].SetData(0, 0, strFieldName, strFieldExplain);

      if (strFieldName.AnsiCompareIC("CustomNo") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_CustomNo = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CustName") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_CustName = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CorpName") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_CorpName = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CustType") == 0)
      {
        FormMain->m_CustDataToAcceptExId.AcceptExId_CustType = nAcceptExId;
        //FormMain->lbCustType->Caption = strFieldExplain+"：";
        //FormEditCustom->Label5->Caption = strFieldExplain+"：";
        //FormDispCustom->Label5->Caption = strFieldExplain+"：";
      }
      else if (strFieldName.AnsiCompareIC("CustClass") == 0)
      {
        FormMain->m_CustDataToAcceptExId.AcceptExId_CustClass = nAcceptExId;
      }
      else if (strFieldName.AnsiCompareIC("PriceModule") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_PriceModule = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("Discount") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_Discount = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CustSex") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_CustSex = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("MobileNo") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_MobileNo = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("TeleNo") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_TeleNo = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("FaxNo") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_FaxNo = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("EMail") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_EMail = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("ContAddr") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_ContAddr = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("PostNo") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_PostNo = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("Province") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_Province = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CityName") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_CityName = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CountyName") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_CountyName = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("ZoneName") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_ZoneName = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("EnsureMoney") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_EnsureMoney = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("RegTime") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_RegTime = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("RegWorker") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_RegWorker = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("MdfDate") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_MdfDate = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("MdfWorker") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_MdfWorker = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("TotalPoints") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_TotalPoints = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("Points") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_Points = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("Remark") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_Remark = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("SubPhone") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_SubPhone = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("NewId") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_NewId = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("CustLevel") == 0)
      {
        FormMain->m_CustDataToAcceptExId.AcceptExId_CustLevel = nAcceptExId;
        FormMain->lbCustLevel->Caption = strFieldExplain+"：";
        if (FormEditCustom)
        {
          FormEditCustom->Label18->Caption = strFieldExplain+"：";
          FormDispCustom->Label18->Caption = strFieldExplain+"：";
        }
      }
      else if (strFieldName.AnsiCompareIC("Password") == 0)
        FormMain->m_CustDataToAcceptExId.AcceptExId_Password = nAcceptExId;
      else if (strFieldName.AnsiCompareIC("AccountNo") == 0)
      {
        FormMain->m_CustDataToAcceptExId.AcceptExId_AccountNo = nAcceptExId;
        FormMain->lbAccountNo->Caption = strFieldExplain+"：";
        if (FormEditCustom)
          FormEditCustom->lbAccountNo->Caption = strFieldExplain+"：";
        if (FormEditCustom)
          FormDispCustom->lbAccountNo->Caption = strFieldExplain+"：";
        adoQryUserBaseAccountNo->DisplayLabel = strFieldExplain;
        adoQrySearchCustAccountNo->DisplayLabel = strFieldExplain;
      }

      nFieldNum++;
      FormMain->m_CustomAllFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }

  sprintf(sqlbuf, "Select CFieldId,CControlType,CFieldName,CLabelCaption,AcceptExId from tbcustexset where CFieldId>0 order by CFieldId");
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub->Open();
    while ( !dmAgent->adoqueryPub->Eof )
    {
      nFieldId = dmAgent->adoqueryPub->FieldByName("CFieldId")->AsInteger;
      nControlType = dmAgent->adoqueryPub->FieldByName("CControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub->FieldByName("CFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub->FieldByName("CLabelCaption")->AsString;
      MyReplace(strFieldExplain, "", "_", strFieldExplain);
      nAcceptExId = dmAgent->adoqueryPub->FieldByName("AcceptExId")->AsInteger;

      FormMain->m_CustomAllFieldList.m_CustomExportField[nFieldNum].SetData(nControlType, nFieldId, strFieldName, strFieldExplain);
      FormMain->m_CustDataToAcceptExId.AcceptExId_ItemData[nFieldId-1] = nAcceptExId;
      nFieldNum++;
      FormMain->m_CustomAllFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}

void TdmAgent::QryCustomExportField()
{
  char sqlbuf[256];
  AnsiString strFieldName, strFieldExplain;
  int nFieldNum=0, nFieldId, nControlType, nExportOrderId;

  sprintf(sqlbuf, "Select FieldName,FieldExplain,ExportOrderId from tbcustomset where ExportId=1 order by id");
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub->Open();
    while ( !dmAgent->adoqueryPub->Eof )
    {
      strFieldName = dmAgent->adoqueryPub->FieldByName("FieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub->FieldByName("FieldExplain")->AsString;
      MyReplace(strFieldExplain, "", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub->FieldByName("ExportOrderId")->AsInteger;
      FormMain->m_CustomExportFieldList.m_CustomExportField[nFieldNum].SetData(3, 0, 0, strFieldName, strFieldExplain, nExportOrderId); //2014-11-09 update
      nFieldNum++;
      FormMain->m_CustomExportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }

  sprintf(sqlbuf, "Select CFieldId,CControlType,CFieldName,CLabelCaption,ExportOrderId from tbcustexset where CFieldId>0 and CExportId=1 order by CFieldId");
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub->Open();
    while ( !dmAgent->adoqueryPub->Eof )
    {
      nFieldId = dmAgent->adoqueryPub->FieldByName("CFieldId")->AsInteger;
      nControlType = dmAgent->adoqueryPub->FieldByName("CControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub->FieldByName("CFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub->FieldByName("CLabelCaption")->AsString;
      MyReplace(strFieldExplain, "", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub->FieldByName("ExportOrderId")->AsInteger;

      FormMain->m_CustomExportFieldList.m_CustomExportField[nFieldNum].SetData(4, nControlType, nFieldId, strFieldName, strFieldExplain, nExportOrderId); //2014-11-09 update
      nFieldNum++;
      FormMain->m_CustomExportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QryCustExDispField()
{
  char sqlbuf[256];
  AnsiString strFieldName, strFieldExplain;
  int nFieldNum=0, nFieldId, nControlType;

  sprintf(sqlbuf, "Select CFieldId,CControlType,CFieldName,CLabelCaption from tbcustexset where CFieldId>0 and CDispId=1 and (CControlType=2 or CControlType=8) order by CFieldId");
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub->Open();
    while ( !dmAgent->adoqueryPub->Eof )
    {
      nFieldId = dmAgent->adoqueryPub->FieldByName("CFieldId")->AsInteger;
      nControlType = dmAgent->adoqueryPub->FieldByName("CControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub->FieldByName("CFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub->FieldByName("CLabelCaption")->AsString;
      MyReplace(strFieldExplain, "", "_", strFieldExplain);
      FormMain->m_CustExDispFieldList.m_CustExDispField[nFieldNum].SetData(nControlType, nFieldId, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      FormMain->m_CustExDispFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  m_pCustExField[0] = adoQryUserBaseItemData1;
  m_pCustExField[1] = adoQryUserBaseItemData2;
  m_pCustExField[2] = adoQryUserBaseItemData3;
  m_pCustExField[3] = adoQryUserBaseItemData4;
  m_pCustExField[4] = adoQryUserBaseItemData5;
  m_pCustExField[5] = adoQryUserBaseItemData6;
  m_pCustExField[6] = adoQryUserBaseItemData7;
  m_pCustExField[7] = adoQryUserBaseItemData8;
  m_pCustExField[8] = adoQryUserBaseItemData9;
  m_pCustExField[9] = adoQryUserBaseItemData10;
  m_pCustExField[10] = adoQryUserBaseItemData11;
  m_pCustExField[11] = adoQryUserBaseItemData12;
  m_pCustExField[12] = adoQryUserBaseItemData13;
  m_pCustExField[13] = adoQryUserBaseItemData14;
  m_pCustExField[14] = adoQryUserBaseItemData15;
  m_pCustExField[15] = adoQryUserBaseItemData16;
  m_pCustExField[16] = adoQryUserBaseItemData17;
  m_pCustExField[17] = adoQryUserBaseItemData18;
  m_pCustExField[18] = adoQryUserBaseItemData19;
  m_pCustExField[19] = adoQryUserBaseItemData20;
  m_pCustExField[20] = adoQryUserBaseItemData21;
  m_pCustExField[21] = adoQryUserBaseItemData22;
  m_pCustExField[22] = adoQryUserBaseItemData23;
  m_pCustExField[23] = adoQryUserBaseItemData24;
  m_pCustExField[24] = adoQryUserBaseItemData25;
  m_pCustExField[25] = adoQryUserBaseItemData26;
  m_pCustExField[26] = adoQryUserBaseItemData27;
  m_pCustExField[27] = adoQryUserBaseItemData28;
  m_pCustExField[28] = adoQryUserBaseItemData29;
  m_pCustExField[29] = adoQryUserBaseItemData30;
  m_pCustExField[30] = adoQryUserBaseItemData31;
  m_pCustExField[31] = adoQryUserBaseItemData32;

  m_pCustExDispField[0] = adoQryUserBaseDispItemData1;
  m_pCustExDispField[1] = adoQryUserBaseDispItemData2;
  m_pCustExDispField[2] = adoQryUserBaseDispItemData3;
  m_pCustExDispField[3] = adoQryUserBaseDispItemData4;
  m_pCustExDispField[4] = adoQryUserBaseDispItemData5;
  m_pCustExDispField[5] = adoQryUserBaseDispItemData6;
  m_pCustExDispField[6] = adoQryUserBaseDispItemData7;
  m_pCustExDispField[7] = adoQryUserBaseDispItemData8;
  m_pCustExDispField[8] = adoQryUserBaseDispItemData9;
  m_pCustExDispField[9] = adoQryUserBaseDispItemData10;
  m_pCustExDispField[10] = adoQryUserBaseDispItemData11;
  m_pCustExDispField[11] = adoQryUserBaseDispItemData12;
  m_pCustExDispField[12] = adoQryUserBaseDispItemData13;
  m_pCustExDispField[13] = adoQryUserBaseDispItemData14;
  m_pCustExDispField[14] = adoQryUserBaseDispItemData15;
  m_pCustExDispField[15] = adoQryUserBaseDispItemData16;
  m_pCustExDispField[16] = adoQryUserBaseDispItemData17;
  m_pCustExDispField[17] = adoQryUserBaseDispItemData18;
  m_pCustExDispField[18] = adoQryUserBaseDispItemData19;
  m_pCustExDispField[19] = adoQryUserBaseDispItemData20;
  m_pCustExDispField[20] = adoQryUserBaseDispItemData21;
  m_pCustExDispField[21] = adoQryUserBaseDispItemData22;
  m_pCustExDispField[22] = adoQryUserBaseDispItemData23;
  m_pCustExDispField[23] = adoQryUserBaseDispItemData24;
  m_pCustExDispField[24] = adoQryUserBaseDispItemData25;
  m_pCustExDispField[25] = adoQryUserBaseDispItemData26;
  m_pCustExDispField[26] = adoQryUserBaseDispItemData27;
  m_pCustExDispField[27] = adoQryUserBaseDispItemData28;
  m_pCustExDispField[28] = adoQryUserBaseDispItemData29;
  m_pCustExDispField[29] = adoQryUserBaseDispItemData30;
  m_pCustExDispField[30] = adoQryUserBaseDispItemData31;
  m_pCustExDispField[31] = adoQryUserBaseDispItemData32;

  m_pDialOutField[0] = adoQryDialOutTeleItemData1;
  m_pDialOutField[1] = adoQryDialOutTeleItemData2;
  m_pDialOutField[2] = adoQryDialOutTeleItemData3;
  m_pDialOutField[3] = adoQryDialOutTeleItemData4;
  m_pDialOutField[4] = adoQryDialOutTeleItemData5;
  m_pDialOutField[5] = adoQryDialOutTeleItemData6;
  m_pDialOutField[6] = adoQryDialOutTeleItemData7;
  m_pDialOutField[7] = adoQryDialOutTeleItemData8;
  m_pDialOutField[8] = adoQryDialOutTeleItemData9;
  m_pDialOutField[9] = adoQryDialOutTeleItemData10;
  m_pDialOutField[10] = adoQryDialOutTeleItemData11;
  m_pDialOutField[11] = adoQryDialOutTeleItemData12;
  m_pDialOutField[12] = adoQryDialOutTeleItemData13;
  m_pDialOutField[13] = adoQryDialOutTeleItemData14;
  m_pDialOutField[14] = adoQryDialOutTeleItemData15;
  m_pDialOutField[15] = adoQryDialOutTeleItemData16;
  m_pDialOutField[16] = adoQryDialOutTeleItemData17;
  m_pDialOutField[17] = adoQryDialOutTeleItemData18;
  m_pDialOutField[18] = adoQryDialOutTeleItemData19;
  m_pDialOutField[19] = adoQryDialOutTeleItemData20;
  m_pDialOutField[20] = adoQryDialOutTeleItemData21;
  m_pDialOutField[21] = adoQryDialOutTeleItemData22;
  m_pDialOutField[22] = adoQryDialOutTeleItemData23;
  m_pDialOutField[23] = adoQryDialOutTeleItemData24;
  m_pDialOutField[24] = adoQryDialOutTeleItemData25;
  m_pDialOutField[25] = adoQryDialOutTeleItemData26;
  m_pDialOutField[26] = adoQryDialOutTeleItemData27;
  m_pDialOutField[27] = adoQryDialOutTeleItemData28;
  m_pDialOutField[28] = adoQryDialOutTeleItemData29;
  m_pDialOutField[29] = adoQryDialOutTeleItemData30;
  m_pDialOutField[30] = adoQryDialOutTeleItemData31;
  m_pDialOutField[31] = adoQryDialOutTeleItemData32;

  m_pDialOutFieldD[0] = adoQryDialOutTeleDItemData1;
  m_pDialOutFieldD[1] = adoQryDialOutTeleDItemData2;
  m_pDialOutFieldD[2] = adoQryDialOutTeleDItemData3;
  m_pDialOutFieldD[3] = adoQryDialOutTeleDItemData4;
  m_pDialOutFieldD[4] = adoQryDialOutTeleDItemData5;
  m_pDialOutFieldD[5] = adoQryDialOutTeleDItemData6;
  m_pDialOutFieldD[6] = adoQryDialOutTeleDItemData7;
  m_pDialOutFieldD[7] = adoQryDialOutTeleDItemData8;
  m_pDialOutFieldD[8] = adoQryDialOutTeleDItemData9;
  m_pDialOutFieldD[9] = adoQryDialOutTeleDItemData10;
  m_pDialOutFieldD[10] = adoQryDialOutTeleDItemData11;
  m_pDialOutFieldD[11] = adoQryDialOutTeleDItemData12;
  m_pDialOutFieldD[12] = adoQryDialOutTeleDItemData13;
  m_pDialOutFieldD[13] = adoQryDialOutTeleDItemData14;
  m_pDialOutFieldD[14] = adoQryDialOutTeleDItemData15;
  m_pDialOutFieldD[15] = adoQryDialOutTeleDItemData16;
  m_pDialOutFieldD[16] = adoQryDialOutTeleDItemData17;
  m_pDialOutFieldD[17] = adoQryDialOutTeleDItemData18;
  m_pDialOutFieldD[18] = adoQryDialOutTeleDItemData19;
  m_pDialOutFieldD[19] = adoQryDialOutTeleDItemData20;
  m_pDialOutFieldD[20] = adoQryDialOutTeleDItemData21;
  m_pDialOutFieldD[21] = adoQryDialOutTeleDItemData22;
  m_pDialOutFieldD[22] = adoQryDialOutTeleDItemData23;
  m_pDialOutFieldD[23] = adoQryDialOutTeleDItemData24;
  m_pDialOutFieldD[24] = adoQryDialOutTeleDItemData25;
  m_pDialOutFieldD[25] = adoQryDialOutTeleDItemData26;
  m_pDialOutFieldD[26] = adoQryDialOutTeleDItemData27;
  m_pDialOutFieldD[27] = adoQryDialOutTeleDItemData28;
  m_pDialOutFieldD[28] = adoQryDialOutTeleDItemData29;
  m_pDialOutFieldD[29] = adoQryDialOutTeleDItemData30;
  m_pDialOutFieldD[30] = adoQryDialOutTeleDItemData31;
  m_pDialOutFieldD[31] = adoQryDialOutTeleDItemData32;

  m_pProcDialOutField[0] = adoQryDialOutProcItemData1;
  m_pProcDialOutField[1] = adoQryDialOutProcItemData2;
  m_pProcDialOutField[2] = adoQryDialOutProcItemData3;
  m_pProcDialOutField[3] = adoQryDialOutProcItemData4;
  m_pProcDialOutField[4] = adoQryDialOutProcItemData5;
  m_pProcDialOutField[5] = adoQryDialOutProcItemData6;
  m_pProcDialOutField[6] = adoQryDialOutProcItemData7;
  m_pProcDialOutField[7] = adoQryDialOutProcItemData8;
  m_pProcDialOutField[8] = adoQryDialOutProcItemData9;
  m_pProcDialOutField[9] = adoQryDialOutProcItemData10;
  m_pProcDialOutField[10] = adoQryDialOutProcItemData11;
  m_pProcDialOutField[11] = adoQryDialOutProcItemData12;
  m_pProcDialOutField[12] = adoQryDialOutProcItemData13;
  m_pProcDialOutField[13] = adoQryDialOutProcItemData14;
  m_pProcDialOutField[14] = adoQryDialOutProcItemData15;
  m_pProcDialOutField[15] = adoQryDialOutProcItemData16;
  m_pProcDialOutField[16] = adoQryDialOutProcItemData17;
  m_pProcDialOutField[17] = adoQryDialOutProcItemData18;
  m_pProcDialOutField[18] = adoQryDialOutProcItemData19;
  m_pProcDialOutField[19] = adoQryDialOutProcItemData20;
  m_pProcDialOutField[20] = adoQryDialOutProcItemData21;
  m_pProcDialOutField[21] = adoQryDialOutProcItemData22;
  m_pProcDialOutField[22] = adoQryDialOutProcItemData23;
  m_pProcDialOutField[23] = adoQryDialOutProcItemData24;
  m_pProcDialOutField[24] = adoQryDialOutProcItemData25;
  m_pProcDialOutField[25] = adoQryDialOutProcItemData26;
  m_pProcDialOutField[26] = adoQryDialOutProcItemData27;
  m_pProcDialOutField[27] = adoQryDialOutProcItemData28;
  m_pProcDialOutField[28] = adoQryDialOutProcItemData29;
  m_pProcDialOutField[29] = adoQryDialOutProcItemData30;
  m_pProcDialOutField[30] = adoQryDialOutProcItemData31;
  m_pProcDialOutField[31] = adoQryDialOutProcItemData32;
}
void TdmAgent::SetCustExDispFieldForSech()
{
  m_pCustExFieldSech[0] = adoQrySearchCustItemData1;
  m_pCustExFieldSech[1] = adoQrySearchCustItemData2;
  m_pCustExFieldSech[2] = adoQrySearchCustItemData3;
  m_pCustExFieldSech[3] = adoQrySearchCustItemData4;
  m_pCustExFieldSech[4] = adoQrySearchCustItemData5;
  m_pCustExFieldSech[5] = adoQrySearchCustItemData6;
  m_pCustExFieldSech[6] = adoQrySearchCustItemData7;
  m_pCustExFieldSech[7] = adoQrySearchCustItemData8;
  m_pCustExFieldSech[8] = adoQrySearchCustItemData9;
  m_pCustExFieldSech[9] = adoQrySearchCustItemData10;
  m_pCustExFieldSech[10] = adoQrySearchCustItemData11;
  m_pCustExFieldSech[11] = adoQrySearchCustItemData12;
  m_pCustExFieldSech[12] = adoQrySearchCustItemData13;
  m_pCustExFieldSech[13] = adoQrySearchCustItemData14;
  m_pCustExFieldSech[14] = adoQrySearchCustItemData15;
  m_pCustExFieldSech[15] = adoQrySearchCustItemData16;
  m_pCustExFieldSech[16] = adoQrySearchCustItemData17;
  m_pCustExFieldSech[17] = adoQrySearchCustItemData18;
  m_pCustExFieldSech[18] = adoQrySearchCustItemData19;
  m_pCustExFieldSech[19] = adoQrySearchCustItemData20;
  m_pCustExFieldSech[20] = adoQrySearchCustItemData21;
  m_pCustExFieldSech[21] = adoQrySearchCustItemData22;
  m_pCustExFieldSech[22] = adoQrySearchCustItemData23;
  m_pCustExFieldSech[23] = adoQrySearchCustItemData24;
  m_pCustExFieldSech[24] = adoQrySearchCustItemData25;
  m_pCustExFieldSech[25] = adoQrySearchCustItemData26;
  m_pCustExFieldSech[26] = adoQrySearchCustItemData27;
  m_pCustExFieldSech[27] = adoQrySearchCustItemData28;
  m_pCustExFieldSech[28] = adoQrySearchCustItemData29;
  m_pCustExFieldSech[29] = adoQrySearchCustItemData30;
  m_pCustExFieldSech[30] = adoQrySearchCustItemData31;
  m_pCustExFieldSech[31] = adoQrySearchCustItemData32;

  m_pCustExDispFieldSech[0] = adoQrySearchCustDispItemData1;
  m_pCustExDispFieldSech[1] = adoQrySearchCustDispItemData2;
  m_pCustExDispFieldSech[2] = adoQrySearchCustDispItemData3;
  m_pCustExDispFieldSech[3] = adoQrySearchCustDispItemData4;
  m_pCustExDispFieldSech[4] = adoQrySearchCustDispItemData5;
  m_pCustExDispFieldSech[5] = adoQrySearchCustDispItemData6;
  m_pCustExDispFieldSech[6] = adoQrySearchCustDispItemData7;
  m_pCustExDispFieldSech[7] = adoQrySearchCustDispItemData8;
  m_pCustExDispFieldSech[8] = adoQrySearchCustDispItemData9;
  m_pCustExDispFieldSech[9] = adoQrySearchCustDispItemData10;
  m_pCustExDispFieldSech[10] = adoQrySearchCustDispItemData11;
  m_pCustExDispFieldSech[11] = adoQrySearchCustDispItemData12;
  m_pCustExDispFieldSech[12] = adoQrySearchCustDispItemData13;
  m_pCustExDispFieldSech[13] = adoQrySearchCustDispItemData14;
  m_pCustExDispFieldSech[14] = adoQrySearchCustDispItemData15;
  m_pCustExDispFieldSech[15] = adoQrySearchCustDispItemData16;
  m_pCustExDispFieldSech[16] = adoQrySearchCustDispItemData17;
  m_pCustExDispFieldSech[17] = adoQrySearchCustDispItemData18;
  m_pCustExDispFieldSech[18] = adoQrySearchCustDispItemData19;
  m_pCustExDispFieldSech[19] = adoQrySearchCustDispItemData20;
  m_pCustExDispFieldSech[20] = adoQrySearchCustDispItemData21;
  m_pCustExDispFieldSech[21] = adoQrySearchCustDispItemData22;
  m_pCustExDispFieldSech[22] = adoQrySearchCustDispItemData23;
  m_pCustExDispFieldSech[23] = adoQrySearchCustDispItemData24;
  m_pCustExDispFieldSech[24] = adoQrySearchCustDispItemData25;
  m_pCustExDispFieldSech[25] = adoQrySearchCustDispItemData26;
  m_pCustExDispFieldSech[26] = adoQrySearchCustDispItemData27;
  m_pCustExDispFieldSech[27] = adoQrySearchCustDispItemData28;
  m_pCustExDispFieldSech[28] = adoQrySearchCustDispItemData29;
  m_pCustExDispFieldSech[29] = adoQrySearchCustDispItemData30;
  m_pCustExDispFieldSech[30] = adoQrySearchCustDispItemData31;
  m_pCustExDispFieldSech[31] = adoQrySearchCustDispItemData32;
}
void TdmAgent::QryAcceptBaseExportField()
{
  char sqlbuf[128];
  AnsiString strFieldName, strFieldExplain;
  int nFieldNum=0, nFieldId, nControlType, nExportOrderId;

  sprintf(sqlbuf, "Select FieldName,AcceptItemName,ExportOrderId from tbacceptset where ExportId=1 and CanExport=1 order by id");
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    FormMain->m_AcceptBaseExportFieldList.SrvType = 0;
    FormMain->m_AcceptBaseExportFieldList.SrvName = "受理基本記錄";
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      strFieldName = dmAgent->adoqueryPub1->FieldByName("FieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("AcceptItemName")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      MyReplace(strFieldExplain, "/", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      FormMain->m_AcceptBaseExportFieldList.m_AcceptExportField[nFieldNum].SetData(1, 0, 0, strFieldName, strFieldExplain, nExportOrderId); //2014-11-09 update
      nFieldNum++;
      FormMain->m_AcceptBaseExportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QryAcceptExportField()
{
  char sqlbuf[256];
  int i=0, nProdType, nSrvType, nSrvSubType, nSrvTypeCount;
  AnsiString strProdName, strSrvName, strSrvSubName;

  sprintf(sqlbuf, "select * from vm_srvsubtype order by ProdType,SrvType,SrvsubType");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();
    nSrvTypeCount = adoqueryPub->RecordCount;
    nSrvTypeCount++;

    if (FormMain->m_pAcceptExportFieldList != NULL)
    {
      delete []FormMain->m_pAcceptExportFieldList;
      FormMain->m_pAcceptExportFieldList=NULL;
    }
    if (FormMain->m_AcceptExportFieldOrderList != NULL)
    {
      delete []FormMain->m_AcceptExportFieldOrderList;
      FormMain->m_AcceptExportFieldOrderList=NULL;
    }
    FormMain->AcceptExportFieldListCount = nSrvTypeCount;
    FormMain->m_pAcceptExportFieldList = new CAcceptExportFieldList[nSrvTypeCount];
    if (FormMain->m_pAcceptExportFieldList == NULL)
      return;
    FormMain->m_AcceptExportFieldOrderList = new CAcceptExportFieldOrderList[nSrvTypeCount];

    g_nFieldCount = QryAcceptExportField(0, 0, 0, "預設業務受理", FormMain->m_pAcceptExportFieldList[i]);
    i++;

    adoqueryPub->First();
    while ( !adoqueryPub->Eof )
    {
      nProdType = adoqueryPub->FieldByName("ProdType")->AsInteger;
      nSrvType = adoqueryPub->FieldByName("SrvType")->AsInteger;
      nSrvSubType = adoqueryPub->FieldByName("SrvSubType")->AsInteger;
      strProdName = adoqueryPub->FieldByName("ProdName")->AsString;
      if (strProdName == "")
        strProdName = nProdType;
      strSrvName = adoqueryPub->FieldByName("SrvName")->AsString;
      if (strSrvName == "")
        strSrvName = nSrvType;
      strSrvSubName = adoqueryPub->FieldByName("SrvSubName")->AsString;
      MyReplace(strProdName, " ", "_", strProdName);
      MyReplace(strSrvName, " ", "_", strSrvName);
      MyReplace(strSrvSubName, " ", "_", strSrvSubName);
      MyReplace(strProdName, "(", "_", strProdName);
      MyReplace(strSrvName, "(", "_", strSrvName);
      MyReplace(strSrvSubName, "(", "_", strSrvSubName);
      MyReplace(strProdName, ")", "_", strProdName);
      MyReplace(strSrvName, ")", "_", strSrvName);
      MyReplace(strSrvSubName, ")", "_", strSrvSubName);
      MyReplace(strProdName, "/", "_", strProdName);
      MyReplace(strSrvName, "/", "_", strSrvName);
      MyReplace(strSrvSubName, "/", "_", strSrvSubName);
      MyReplace(strProdName, "+", "_", strProdName);
      MyReplace(strSrvName, "+", "_", strSrvName);
      MyReplace(strSrvSubName, "+", "_", strSrvSubName);
      MyReplace(strProdName, "-", "_", strProdName);
      MyReplace(strSrvName, "-", "_", strSrvName);
      MyReplace(strSrvSubName, "-", "_", strSrvSubName);

      MyReplace(strProdName, ".", "_", strProdName);
      MyReplace(strSrvName, ".", "_", strSrvName);
      MyReplace(strSrvSubName, ".", "_", strSrvSubName);

      MyReplace(strProdName, ".", "_", strProdName);
      MyReplace(strSrvName, ".", "_", strSrvName);
      MyReplace(strSrvSubName, ".", "_", strSrvSubName);
      QryAcceptExportField(nProdType, nSrvType, nSrvSubType, strProdName+"_"+strSrvName+"_"+strSrvSubName, FormMain->m_pAcceptExportFieldList[i]);
      i++;
      adoqueryPub->Next();
    }

    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::QryAcceptExportField(int nProdType, int nSrvType, int nSrvSubType, AnsiString srvname, CAcceptExportFieldList &acceptexportfieldlist)
{
  char sqlbuf[256];
  AnsiString strFieldName, strFieldExplain, strCombineValue;
  int nFieldNum=0, nFieldId, nControlType, nExportOrderId, nCombineMode, nCombineTid, nCount, nFieldCount;

  sprintf(sqlbuf, "Select FieldName,AcceptItemName,ExportOrderId from tbacceptset where ExportId=1 and CanExport=1 order by id");
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      strFieldName = dmAgent->adoqueryPub1->FieldByName("FieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("AcceptItemName")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      MyReplace(strFieldExplain, "/", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetData(1, 0, 0, strFieldName, strFieldExplain, nExportOrderId); //2014-11-09 update
      nFieldNum++;
      acceptexportfieldlist.FieldNum = nFieldNum;
      if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
        break;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }

  nCount = 0;

  if (GetCombineDefaultItemId(nProdType, nSrvType, nSrvSubType) == 1)
  {
    sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption,ExportOrderId,CombineMode,CombineValue,CombineTid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid>0 and TExportId=1 order by Tid", 0, 0, 0);
    try
    {
      dmAgent->adoqueryPub1->SQL->Clear();
      dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
      dmAgent->adoqueryPub1->Open();
      acceptexportfieldlist.ProdType = nProdType;
      acceptexportfieldlist.SrvType = nSrvType;
      acceptexportfieldlist.SrvSubType = nSrvSubType;
      acceptexportfieldlist.SrvName = srvname;
      while ( !dmAgent->adoqueryPub1->Eof )
      {
        nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
        nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
        strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
        strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
        MyReplace(strFieldExplain, " ", "_", strFieldExplain);
        MyReplace(strFieldExplain, "/", "_", strFieldExplain);
        nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
        nCombineMode = dmAgent->adoqueryPub1->FieldByName("CombineMode")->AsInteger;
        strCombineValue = dmAgent->adoqueryPub1->FieldByName("CombineValue")->AsString;
        nCombineTid = dmAgent->adoqueryPub1->FieldByName("CombineTid")->AsInteger;

        acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain, nExportOrderId); //2014-11-09 update
        acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetCombineData(nCombineMode, nCombineTid, strCombineValue);
        nFieldNum++;
        nCount++;
        acceptexportfieldlist.FieldNum = nFieldNum;
        if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
          break;
        dmAgent->adoqueryPub1->Next();
      }
      dmAgent->adoqueryPub1->Close();
    }
    catch ( ... )
    {
    }
  }
  if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
    return nCount;

  nFieldCount = 0;
  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption,ExportOrderId,CombineMode,CombineValue,CombineTid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid>0 and TExportId=1 order by Tid", nProdType, nSrvType, nSrvSubType);
  //WriteErrprMsg("QryAcceptExportField %s", sqlbuf);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    acceptexportfieldlist.ProdType = nProdType;
    acceptexportfieldlist.SrvType = nSrvType;
    acceptexportfieldlist.SrvSubType = nSrvSubType;
    acceptexportfieldlist.SrvName = srvname;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      MyReplace(strFieldExplain, "/", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      nCombineMode = dmAgent->adoqueryPub1->FieldByName("CombineMode")->AsInteger;
      strCombineValue = dmAgent->adoqueryPub1->FieldByName("CombineValue")->AsString;
      nCombineTid = dmAgent->adoqueryPub1->FieldByName("CombineTid")->AsInteger;

      if (g_nFieldCount > 0)
      {
        strFieldName = "Itemdata"+IntToStr(nFieldCount+g_nFieldCount+1);
      }
      //WriteErrprMsg("QryAcceptExportField Tid=%d TControlType=%d TFieldName=%s TLabelCaption=%s ExportOrderId=%d CombineMode=%d CombineValue=%s CombineTid=%d",
      // nFieldId, nControlType, strFieldName.c_str(), strFieldExplain.c_str(), nExportOrderId, nCombineMode, strCombineValue.c_str(), nCombineTid);

      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain, nExportOrderId, g_nFieldCount); //2014-11-09 update
      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetCombineData(nCombineMode, nCombineTid, strCombineValue);
      nFieldNum++;
      nCount++;
      acceptexportfieldlist.FieldNum = nFieldNum;
      if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
        break;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  if (nCount > 0)
    return nCount;

  if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
    return nCount;

  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption,ExportOrderId,CombineMode,CombineValue,CombineTid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid>0 and TExportId=1 order by Tid", nProdType, nSrvType, 0);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    acceptexportfieldlist.ProdType = nProdType;
    acceptexportfieldlist.SrvType = nSrvType;
    acceptexportfieldlist.SrvSubType = nSrvSubType;
    acceptexportfieldlist.SrvName = srvname;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      MyReplace(strFieldExplain, "/", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      nCombineMode = dmAgent->adoqueryPub1->FieldByName("CombineMode")->AsInteger;
      strCombineValue = dmAgent->adoqueryPub1->FieldByName("CombineValue")->AsString;
      nCombineTid = dmAgent->adoqueryPub1->FieldByName("CombineTid")->AsInteger;

      if (g_nFieldCount > 0)
      {
        strFieldName = "Itemdata"+IntToStr(nCount+g_nFieldCount+1);
      }

      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain, nExportOrderId, g_nFieldCount); //2014-11-09 update
      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetCombineData(nCombineMode, nCombineTid, strCombineValue);
      nFieldNum++;
      nCount++;
      acceptexportfieldlist.FieldNum = nFieldNum;
      if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
        break;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  if (nCount > 0)
    return nCount;

  if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
    return nCount;

  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption,ExportOrderId,CombineMode,CombineValue,CombineTid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid>0 and TExportId=1 order by Tid", nProdType, 0, 0);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    acceptexportfieldlist.ProdType = nProdType;
    acceptexportfieldlist.SrvType = nSrvType;
    acceptexportfieldlist.SrvSubType = nSrvSubType;
    acceptexportfieldlist.SrvName = srvname;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      MyReplace(strFieldExplain, "/", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      nCombineMode = dmAgent->adoqueryPub1->FieldByName("CombineMode")->AsInteger;
      strCombineValue = dmAgent->adoqueryPub1->FieldByName("CombineValue")->AsString;
      nCombineTid = dmAgent->adoqueryPub1->FieldByName("CombineTid")->AsInteger;

      if (g_nFieldCount > 0)
      {
        strFieldName = "Itemdata"+IntToStr(nCount+g_nFieldCount+1);
      }

      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain, nExportOrderId, g_nFieldCount); //2014-11-09 update
      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetCombineData(nCombineMode, nCombineTid, strCombineValue);
      nFieldNum++;
      nCount++;
      acceptexportfieldlist.FieldNum = nFieldNum;
      if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
        break;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  if (nCount > 0)
    return nCount;

  if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
    return nCount;

  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption,ExportOrderId,CombineMode,CombineValue,CombineTid from tbacptitemset where ProdType=%d and SrvType=%d and SrvSubType=%d and Tid>0 and TExportId=1 order by Tid", 0, 0, 0);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    acceptexportfieldlist.ProdType = nProdType;
    acceptexportfieldlist.SrvType = nSrvType;
    acceptexportfieldlist.SrvSubType = nSrvSubType;
    acceptexportfieldlist.SrvName = srvname;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      MyReplace(strFieldExplain, "/", "_", strFieldExplain);
      nExportOrderId = dmAgent->adoqueryPub1->FieldByName("ExportOrderId")->AsInteger;
      nCombineMode = dmAgent->adoqueryPub1->FieldByName("CombineMode")->AsInteger;
      strCombineValue = dmAgent->adoqueryPub1->FieldByName("CombineValue")->AsString;
      nCombineTid = dmAgent->adoqueryPub1->FieldByName("CombineTid")->AsInteger;

      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain, nExportOrderId, g_nFieldCount); //2014-11-09 update
      acceptexportfieldlist.m_AcceptExportField[nFieldNum].SetCombineData(nCombineMode, nCombineTid, strCombineValue);
      nFieldNum++;
      acceptexportfieldlist.FieldNum = nFieldNum;
      if (nFieldNum >= MAX_ACCEPT_EXPORT_FIELD_NUM)
        break;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  return nCount;
}

void __fastcall TdmAgent::adoQryRecvMsgCalcFields(TDataSet *DataSet)
{
  adoQryRecvMsgDispReadFlag->AsString = (adoQryRecvMsgReadFlag->AsInteger==1)?"是":"否";
}
//---------------------------------------------------------------------------
void TdmAgent::QryTabPageAuth()
{
  char sqlbuf[128];
  int i=0;

  //2019-08-04
  if (MyWorker.DepartmentID <= 0)
  {
    sprintf(sqlbuf, "select * from tbtabauth order by MainTabID,SubTabID");
  }
  else
  {
    sprintf(sqlbuf, "select * from tbtabauth_Dept%d order by MainTabID,SubTabID", MyWorker.DepartmentID);
  }
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();
    FormMain->MyTabAuthList.TabNum = adoqueryPub->RecordCount;

    adoqueryPub->First();
    while ( !adoqueryPub->Eof )
    {
      FormMain->MyTabAuthList.m_CTabAuth[i].MainTabID = adoqueryPub->FieldByName("MainTabID")->AsInteger;
      FormMain->MyTabAuthList.m_CTabAuth[i].SubTabID = adoqueryPub->FieldByName("SubTabID")->AsInteger;
      FormMain->MyTabAuthList.m_CTabAuth[i].TabName = adoqueryPub->FieldByName("TabName")->AsString;
      FormMain->MyTabAuthList.m_CTabAuth[i].AdminAuth = adoqueryPub->FieldByName("AdminAuth")->AsInteger;
      FormMain->MyTabAuthList.m_CTabAuth[i].MaintAuth = adoqueryPub->FieldByName("MaintAuth")->AsInteger;
      FormMain->MyTabAuthList.m_CTabAuth[i].OpertAuth = adoqueryPub->FieldByName("OpertAuth")->AsInteger;
      FormMain->MyTabAuthList.m_CTabAuth[i].MonitAuth = adoqueryPub->FieldByName("MonitAuth")->AsInteger;
      FormMain->MyTabAuthList.m_CTabAuth[i].AgentAuth = adoqueryPub->FieldByName("AgentAuth")->AsInteger;
      i++;
      adoqueryPub->Next();
    }

    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}

void __fastcall TdmAgent::adoQryTabAuthSetCalcFields(TDataSet *DataSet)
{
  adoQryTabAuthSetDispAdminAuth->AsString = (adoQryTabAuthSetAdminAuth->AsInteger==1)?"允許":"不允許";
  adoQryTabAuthSetDispMaintAuth->AsString = (adoQryTabAuthSetMaintAuth->AsInteger==1)?"允許":"不允許";
  adoQryTabAuthSetDispOpertAuth->AsString = (adoQryTabAuthSetOpertAuth->AsInteger==1)?"允許":"不允許";
  adoQryTabAuthSetDispMonitAuth->AsString = (adoQryTabAuthSetMonitAuth->AsInteger==1)?"允許":"不允許";
  adoQryTabAuthSetDispAgentAuth->AsString = (adoQryTabAuthSetAgentAuth->AsInteger==1)?"允許":"不允許";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryAcceptProcCalcFields(TDataSet *DataSet)
{
  if (adoQryOldAcceptProcNextProcType->AsInteger == 1)
    adoQryAcceptProcDispNextName->AsString = adoQryAcceptProcDepartmentName->AsString;
  else if (adoQryOldAcceptProcNextProcType->AsInteger == 2)
    adoQryAcceptProcDispNextName->AsString = adoQryAcceptProcWorkerName1->AsString;
  else
    adoQryAcceptProcDispNextName->AsString = "無";
}
//---------------------------------------------------------------------------
int TdmAgent::IsFlwFuncNoExist(int nFlwFuncNo)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select FuncNo from tbflwfuncno where FuncNo=%d", nFlwFuncNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertFlwFuncNoRecord(int nFlwFuncNo, AnsiString FlwFile, AnsiString FlwName)
{
  char sqlbuf[128];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbflwfuncno (FuncNo,FlwName,Remark) values ('%d','%s','%s')",
    nFlwFuncNo, FlwFile.c_str(), FlwName.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateFlwFuncNoRecord(int nFlwFuncNo, AnsiString FlwFile, AnsiString FlwName)
{
  char sqlbuf[128];

  sprintf( sqlbuf, "update tbflwfuncno set FlwName='%s',Remark='%s' where FuncNo=%d",
    FlwFile.c_str(), FlwName.c_str(), nFlwFuncNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteFlwFuncNoRecord(int nFlwFuncNo)
{
  char sqlbuf[128];

  sprintf(sqlbuf, "delete from tbflwfuncno where FuncNo=%d", nFlwFuncNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::UpdateFlwFuncNoListItems()
{
  ReadFlwFuncNoList(FlwFuncNoList);
  AddCmbItem(FormEditDialOutTask->cbFuncNo, FlwFuncNoList, "系統預設流程");
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryCalloutCalcFields(TDataSet *DataSet)
{
  if (adoQryCalloutitem->AsInteger == 9999)
    adoQryCalloutDispItemName->AsString = "總計";
  else
    adoQryCalloutDispItemName->AsString = DialoutResultItemList.GetItemName(adoQryCalloutitem->AsInteger);
}
//---------------------------------------------------------------------------
int TdmAgent::IsDialOutCalledNoExist(int taskid, AnsiString calledno)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select id from tbdialouttele where TaskId=%d and CalledNo='%s'", taskid, calledno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsDialOutCalledNoCanImport(int taskid, AnsiString calledno, bool bSameMobile)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select id from tbdialouttele where TaskId=%d and CalledNo='%s' and EndCode1<>'%d'", taskid, calledno.c_str(), g_nRejectEndCode1);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::GetManDialOutTaskRecord(TADOQuery *adoquery, CManDialOutTask *mandialouttask)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  mandialouttask->TaskId = adoquery->FieldByName("TaskId")->AsInteger;
	  mandialouttask->TaskCode = adoquery->FieldByName("TaskCode")->AsString;
	  mandialouttask->TaskName = adoquery->FieldByName("TaskName")->AsString;
	  mandialouttask->StartId = adoquery->FieldByName("StartId")->AsInteger;
	  mandialouttask->StartDate = adoquery->FieldByName("StartDate")->AsString;
	  mandialouttask->EndDate = adoquery->FieldByName("EndDate")->AsString;
	  mandialouttask->CallerNo = adoquery->FieldByName("CallerNo")->AsString;
	  mandialouttask->CreateTime = adoquery->FieldByName("CreateTime")->AsString;
	  mandialouttask->CreateWorker = adoquery->FieldByName("CreateWorker")->AsString;
	  mandialouttask->Param = adoquery->FieldByName("Param")->AsString;
	  mandialouttask->URLAddr = adoquery->FieldByName("URLAddr")->AsString;
    mandialouttask->URLOrder = adoquery->FieldByName("URLOrder")->AsString;
    mandialouttask->URLQuery = adoquery->FieldByName("URLQuery")->AsString;
    mandialouttask->URLManOrder = adoquery->FieldByName("URLManOrder")->AsString;
    mandialouttask->URLPrice = adoquery->FieldByName("URLPrice")->AsString;
	  mandialouttask->Remark = adoquery->FieldByName("Remark")->AsString;
    mandialouttask->DialPreCode = adoquery->FieldByName("DialPreCode")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertManDialOutTaskRecord(CManDialOutTask *mandialouttask)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbDialOutTask (\
TaskCode,\
TaskName,\
StartId,\
StartDate,\
EndDate,\
CallerNo,\
CreateTime,\
CreateWorker,\
Param,\
URLAddr,\
URLOrder,\
URLQuery,\
URLManOrder,\
URLPrice,\
DialPreCode,\
Remark\
) values ('%s','%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
	  mandialouttask->TaskCode.c_str(),
	  mandialouttask->TaskName.c_str(),
    mandialouttask->StartId,
    mandialouttask->StartDate.c_str(),
    mandialouttask->EndDate.c_str(),
    mandialouttask->CallerNo.c_str(),
    mandialouttask->CreateTime.c_str(),
    mandialouttask->CreateWorker.c_str(),
    mandialouttask->Param.c_str(),
    mandialouttask->URLAddr.c_str(),
    mandialouttask->URLOrder.c_str(),
    mandialouttask->URLQuery.c_str(),
    mandialouttask->URLManOrder.c_str(),
    mandialouttask->URLPrice.c_str(),
    mandialouttask->DialPreCode.c_str(),
    mandialouttask->Remark.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateManDialOutTaskRecord(CManDialOutTask *mandialouttask)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbDialOutTask set \
TaskCode='%s',\
TaskName='%s',\
StartId=%d,\
StartDate='%s',\
EndDate='%s',\
CallerNo='%s',\
CreateTime='%s',\
CreateWorker='%s',\
Param='%s',\
URLAddr='%s',\
URLOrder='%s',\
URLQuery='%s',\
URLManOrder='%s',\
URLPrice='%s',\
DialPreCode='%s',\
Remark='%s' \
where taskid=%d",
	  mandialouttask->TaskCode.c_str(),
	  mandialouttask->TaskName.c_str(),
    mandialouttask->StartId,
    mandialouttask->StartDate.c_str(),
    mandialouttask->EndDate.c_str(),
    mandialouttask->CallerNo.c_str(),
    mandialouttask->CreateTime.c_str(),
    mandialouttask->CreateWorker.c_str(),
    mandialouttask->Param.c_str(),
    mandialouttask->URLAddr.c_str(),
    mandialouttask->URLOrder.c_str(),
    mandialouttask->URLQuery.c_str(),
    mandialouttask->URLManOrder.c_str(),
    mandialouttask->URLPrice.c_str(),
    mandialouttask->DialPreCode.c_str(),
    mandialouttask->Remark.c_str(),
    mandialouttask->TaskId);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteManDialOutTaskRecord(int taskid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbDialOutTask where taskid=%d", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::DeleteManDialOutTeleRecord(int taskid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbDialOutTele where taskid=%d", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::UpdateManDialOutTaskStartId(int taskid, int startid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbDialOutTask set startid=%d where taskid=%d", startid, taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::GetManDialOutTeleAllocId(int taskid)
{
  int nAllocId=1;
  char sqlbuf[128];

  sprintf(sqlbuf, "Select top 1 AllocId from tbdialouttele where TaskId=%d order by AllocId desc", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      nAllocId = adoqueryPub->FieldByName("AllocId")->AsInteger;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nAllocId;
}
int TdmAgent::InsertManDialOutTeleRecord(int taskid, AnsiString batchid, AnsiString mobileno, AnsiString teleno, AnsiString custname, AnsiString custaddr, AnsiString accountno, int nAllocId, AnsiString workerno)
{
  char sqlbuf[1024];
  AnsiString customno;

  customno = GetCustomNoByFaxNo(mobileno);
  if (customno == "")
  {
    customno = MyGetCustomNo(nAllocId);
    if (InsertCustom(customno, mobileno, teleno, custname, custaddr, accountno, "", "", "", "") == 1)
      customno = "";
  }
  sprintf(sqlbuf, "insert into tbDialOutTele (DialId,TaskId,BatchId,CalledNo,TeleNo,CustName,CustAddr,CustomNo,AccountNo,AllocId,TaskWorkerNo) values ('%s',%d,'%s','%s','%s','%s','%s','%s','%s',%d,'%s')",
    MyGetGIUD().c_str(), taskid, batchid, mobileno.c_str(), teleno.c_str(), custname.c_str(), custaddr.c_str(), customno.c_str(), accountno.c_str(), nAllocId, workerno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertManDialOutTeleRecord(int taskid, AnsiString batchid, AnsiString mobileno, AnsiString teleno, AnsiString custname, AnsiString custaddr, AnsiString accountno, AnsiString itemdata1, AnsiString itemdata2, AnsiString itemdata3, AnsiString itemdata4, int nAllocId, AnsiString workerno)
{
  char sqlbuf[1024];
  AnsiString customno;

  customno = GetCustomNoByFaxNo(mobileno);
  if (customno == "")
  {
    customno = MyGetCustomNo(nAllocId);
    if (InsertCustom(customno, mobileno, teleno, custname, custaddr, accountno, itemdata1, itemdata2, itemdata3, itemdata4) == 1)
      customno = "";
  }
  sprintf(sqlbuf, "insert into tbDialOutTele (DialId,TaskId,BatchId,CalledNo,TeleNo,CustName,CustAddr,CustomNo,AccountNo,ItemData1,ItemData2,ItemData3,ItemData4,AllocId,TaskWorkerNo) values ('%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,'%s')",
    MyGetGIUD().c_str(), taskid, batchid, mobileno.c_str(), teleno.c_str(), custname.c_str(), custaddr.c_str(), customno.c_str(), accountno.c_str(), itemdata1.c_str(), itemdata2.c_str(), itemdata3.c_str(), itemdata4.c_str(), nAllocId, workerno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::InsertManDialOutDialListRecord(AnsiString DialListId, AnsiString DialId, AnsiString CalledNo)
{
  char sqlbuf[1024];

  sprintf(sqlbuf, "insert into tbDialOutList (DialListId,DialId,CalledNo,TalkWorkerNo) values ('%s','%s','%s','%s')",
    DialListId.c_str(), DialId.c_str(), CalledNo.c_str(), MyWorker.WorkerNo.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteManDialOutTeleRecord(int taskid, AnsiString calledno)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbDialOutTele where taskid=%d and CalledNo='%s'", taskid, calledno.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateManDialOutDialListRecord(AnsiString DialListId, AnsiString DialId, int Endcode1, int Endcode2, AnsiString DialLog)
{
  char sqlbuf[8000];
  //2015-04-09
  sprintf(sqlbuf, "update tbDialOutList set EndCode1=%d,EndCode2=%d,DialLog='%s',SaveTime=getdate() where DialListId='%s' and DialId='%s'",
    Endcode1, Endcode2, DialLog.c_str(), DialListId.c_str(), DialId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
AnsiString TdmAgent::GetLastDialListIdByDialId(AnsiString DialId)
{
  AnsiString strDialListId="";
  char sqlbuf[128];

  sprintf(sqlbuf, "Select top 1 DialListId from tbDialOutList where DialId='%s' order by Id desc", DialId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      strDialListId = adoqueryPub->FieldByName("DialListId")->AsString;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return strDialListId;
}
//2015-04-09
int TdmAgent::UpdateManDialOutDialListRecord(AnsiString DialListId, AnsiString DialId, AnsiString DialLog)
{
  char sqlbuf[8000];
  //2015-04-09
  sprintf(sqlbuf, "update tbDialOutList set DialLog='%s',SaveTime=getdate() where DialListId='%s' and DialId='%s'",
    DialLog.c_str(), DialListId.c_str(), DialId.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteOneManDialOutTeleRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbDialOutTele where Id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DeleteAllManDialOutTeleRecord(int taskid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbDialOutTele where taskid=%d", taskid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateManDialOutPriority(int dialid, int Priority)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbDialOutTele set Priority=%d where DialId=%d", Priority, dialid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateDialOutResult(int dialid, int dialresult)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "update tbDialOutTele set LastResult=%d where Id=%d", dialresult, dialid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::AllocDialOutWorkerNo(int mindialid, int maxdialid, AnsiString strWorkerNo)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set CalledTimes=0,AnsTimes=0,LastResult=0,TaskWorkerNo='%s',AllocWorkerNo='%s',AllocTime=GETDATE() where Id>=%d and Id<%d",
    strWorkerNo.c_str(), MyWorker.WorkerNo.c_str(), mindialid, maxdialid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::AllocDialOutWorkerNoByAllocId(int taskid, int minallocid, int maxallocid, AnsiString strWorkerNo)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set CalledTimes=0,AnsTimes=0,LastResult=0,TaskWorkerNo='%s',AllocWorkerNo='%s',AllocTime=GETDATE() where TaskId=%d and AllocId>=%d and AllocId<%d",
    strWorkerNo.c_str(), MyWorker.WorkerNo.c_str(), taskid, minallocid, maxallocid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::AllocDialOutWorkerNo(int id, AnsiString strWorkerNo)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set CalledTimes=0,AnsTimes=0,LastResult=0,TaskWorkerNo='%s',AllocWorkerNo='%s',AllocTime=GETDATE() where Id=%d",
    strWorkerNo.c_str(), MyWorker.WorkerNo.c_str(), id);
  try
  {
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf);
    adoqueryPub1->ExecSQL();
    nCount = adoqueryPub1->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::SearchAllocedSameCalled(int taskid, AnsiString strCalledNo, AnsiString strWorkerNo)
{
  int nResult=0;
  char sqlbuf[128];

  sprintf(sqlbuf, "Select top 1 Id from tbdialouttele where TaskId=%d and CalledNo='%s' and TaskWorkerNo='%s'",
    taskid, strCalledNo.c_str(), strWorkerNo.c_str());
  try
  {
    adoqueryPub1->SQL->Clear();
    adoqueryPub1->SQL->Add((char *)sqlbuf );
    adoqueryPub1->Open();

    if ( !adoqueryPub1->Eof )
    {
      nResult = 1;
    }
    adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::DisAllocDialOutWorkerNo(int id)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set CalledTimes=0,AnsTimes=0,LastResult=0,TaskWorkerNo='',AllocWorkerNo='',AllocTime=NULL where Id=%d",
    id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::ClearDialOutDialOutResult(int id)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set CalledTimes=0,AnsTimes=0,LastResult=0 where Id=%d",
    id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::ClearDialOutDialOutEndcode(int id)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set EndCode1=0,EndCode2=0 where Id=%d",
    id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::SetDialOutDialOutCallBack(int id, int flag, AnsiString cbtime)
{
  char sqlbuf[256];
  int nCount;

  if (flag == 1)
    sprintf(sqlbuf, "update tbDialOutTele set CallBackId=1,CallBackTime='%s' where Id=%d", cbtime.c_str(), id);
  else
    sprintf(sqlbuf, "update tbDialOutTele set CallBackId=0,CallBackTime=NULL where Id=%d", id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::UpdateDialOutCalledNo(int id, AnsiString calledno)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set CalledNo='%s' where Id=%d", calledno.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
int TdmAgent::UpdateDialOutTeleNo(int id, AnsiString teleno)
{
  char sqlbuf[256];
  int nCount;

  sprintf(sqlbuf, "update tbDialOutTele set TeleNo='%s' where Id=%d", teleno.c_str(), id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
    nCount = adoqueryPub->RowsAffected;
    return nCount;
  }
  catch ( ... )
  {
    return 0;
  }
}
AnsiString TdmAgent::GetManDialOutCustNameByTele(AnsiString calledno)
{
  char sqlbuf[256];
  AnsiString itemname="";

  if (strDBType == "SQLSERVER")
    sprintf(sqlbuf, "Select top 1 CustName from tbDialOutTele where CalledNo='%s'", calledno.c_str());
  else if (strDBType == "MYSQL")
    sprintf(sqlbuf, "Select CustName from tbDialOutTele where CalledNo='%s' limit 1", calledno.c_str());
  try
  {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
  }
  catch ( ... )
  {
      return itemname;
  }
  if ( !adoqueryPub->Eof )
  {
    itemname = adoqueryPub->FieldByName("CustName")->AsString;
  }
  adoqueryPub->Close();
  return itemname;
}

void __fastcall TdmAgent::adoQryDialOutTaskCalcFields(TDataSet *DataSet)
{
  adoQryDialOutTaskDispStartId->AsString = YesNoItemList.GetItemName(adoQryDialOutTaskStartId->AsInteger);
  adoQryDialOutTaskDispTotalCount->AsInteger = GetDialOutTotalRecordCount(adoQryDialOutTaskTaskId->AsInteger);
  adoQryDialOutTaskDispAllocCount->AsInteger = GetDialOutTotalAllocCount(adoQryDialOutTaskTaskId->AsInteger);
  adoQryDialOutTaskDispOrderCount->AsInteger = GetDialOutTotalOrderCount(adoQryDialOutTaskTaskId->AsInteger);
}
//---------------------------------------------------------------------------
  /*adoQryDialOutTeleDispLastResult->AsString = DialoutResultItemList.GetItemName(adoQryDialOutTeleLastResult->AsInteger);
  adoQryDialOutTeleTaskWorkerName->AsString = GetWorkerName(adoQryDialOutTeleTaskWorkerNo->AsString);
  adoQryDialOutTeleTalkWorkerName->AsString = GetWorkerName(adoQryDialOutTeleTalkWorkerNo->AsString);
  if (adoQryDialOutTeleRecdFile->AsString != "")
    adoQryDialOutTeleDispRecdFile->AsString = "是";
  else
    adoQryDialOutTeleDispRecdFile->AsString = "否";*/

void TdmAgent::QueryDialOutItemSet(int TaskId)
{
  char sqlbuf[128];

  if (TaskId < 0)
  {
    adoQryDialOutItemSet->Close();
    return;
  }
  sprintf(sqlbuf, "Select * from vm_dialoutitemset where TaskId=%d", TaskId);
  try
  {
    adoQryDialOutItemSet->SQL->Clear();
    adoQryDialOutItemSet->SQL->Add((char *)sqlbuf );
    adoQryDialOutItemSet->Open();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::IsDialOutLinkItemExist(int TaskId)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select tid from tbdialoutitemset where TaskId=%d and TControlType=%d", TaskId, 10);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::IsDialOutItemSetTidExist(int TaskId, int nTid)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select tid from tbdialoutitemset where TaskId=%d and tid=%d", TaskId, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::GetDialOutItemSetRecord(TADOQuery *adoquery, CDialOutItemSet *dialoutitemset)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  dialoutitemset->id = adoquery->FieldByName("id")->AsInteger;
	  dialoutitemset->TaskId = adoquery->FieldByName("TaskId")->AsInteger;
	  dialoutitemset->Tid = adoquery->FieldByName("Tid")->AsInteger;
	  dialoutitemset->TFieldName = adoquery->FieldByName("TFieldName")->AsString;
	  dialoutitemset->TControlType = adoquery->FieldByName("TControlType")->AsInteger;
	  dialoutitemset->TLabelCaption = adoquery->FieldByName("TLabelCaption")->AsString;
	  dialoutitemset->TXpos = adoquery->FieldByName("TXpos")->AsInteger;
	  dialoutitemset->TYpos = adoquery->FieldByName("TYpos")->AsInteger;
	  dialoutitemset->THeigth = adoquery->FieldByName("THeigth")->AsInteger;
	  dialoutitemset->TWith = adoquery->FieldByName("TWith")->AsInteger;
	  dialoutitemset->TDataType = adoquery->FieldByName("TDataType")->AsInteger;
	  dialoutitemset->TDefaultData = adoquery->FieldByName("TDefaultData")->AsString;
	  dialoutitemset->TMinValue = adoquery->FieldByName("TMinValue")->AsInteger;
	  dialoutitemset->TMaxValue = adoquery->FieldByName("TMaxValue")->AsInteger;
    dialoutitemset->TIsAllowNull = adoquery->FieldByName("TIsAllowNull")->AsInteger;
    dialoutitemset->TExportId = adoquery->FieldByName("TExportId")->AsInteger;
    dialoutitemset->TDispId = adoquery->FieldByName("TDispId")->AsInteger;
    dialoutitemset->TModifyId = adoquery->FieldByName("TModifyId")->AsInteger;
	  dialoutitemset->TMaskEditStr = adoquery->FieldByName("TMaskEditStr")->AsString;
	  dialoutitemset->THintStr = adoquery->FieldByName("THintStr")->AsString;
    dialoutitemset->SearchId = adoquery->FieldByName("SearchId")->AsInteger; //2019-05-27
    dialoutitemset->SearchOrder = adoquery->FieldByName("SearchOrder")->AsInteger; //2019-05-27
    dialoutitemset->DispOrder = adoquery->FieldByName("DispOrder")->AsInteger; //2019-05-27
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetDialOutItemSetRecord(int TaskId, int nTid, CDialOutItemSet *dialoutitemset)
{
  char sqlbuf[128];
  int result=1;

  sprintf(sqlbuf, "Select * from tbdialoutitemset where TaskId=%d and tid=%d",
    TaskId, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
	    dialoutitemset->id = adoqueryPub->FieldByName("id")->AsInteger;
	    dialoutitemset->TaskId = adoqueryPub->FieldByName("TaskId")->AsInteger;
      dialoutitemset->Tid = adoqueryPub->FieldByName("Tid")->AsInteger;
      dialoutitemset->TFieldName = adoqueryPub->FieldByName("TFieldName")->AsString;
      dialoutitemset->TControlType = adoqueryPub->FieldByName("TControlType")->AsInteger;
      dialoutitemset->TLabelCaption = adoqueryPub->FieldByName("TLabelCaption")->AsString;
      dialoutitemset->TXpos = adoqueryPub->FieldByName("TXpos")->AsInteger;
      dialoutitemset->TYpos = adoqueryPub->FieldByName("TYpos")->AsInteger;
      dialoutitemset->THeigth = adoqueryPub->FieldByName("THeigth")->AsInteger;
      dialoutitemset->TWith = adoqueryPub->FieldByName("TWith")->AsInteger;
      dialoutitemset->TDataType = adoqueryPub->FieldByName("TDataType")->AsInteger;
      dialoutitemset->TDefaultData = adoqueryPub->FieldByName("TDefaultData")->AsString;
      dialoutitemset->TMinValue = adoqueryPub->FieldByName("TMinValue")->AsInteger;
      dialoutitemset->TMaxValue = adoqueryPub->FieldByName("TMaxValue")->AsInteger;
      dialoutitemset->TIsAllowNull = adoqueryPub->FieldByName("TIsAllowNull")->AsInteger;
      dialoutitemset->TExportId = adoqueryPub->FieldByName("TExportId")->AsInteger;
      dialoutitemset->TDispId = adoqueryPub->FieldByName("TDispId")->AsInteger;
      dialoutitemset->TModifyId = adoqueryPub->FieldByName("TModifyId")->AsInteger;
	    dialoutitemset->TMaskEditStr = adoqueryPub->FieldByName("TMaskEditStr")->AsString;
	    dialoutitemset->THintStr = adoqueryPub->FieldByName("THintStr")->AsString;
      dialoutitemset->SearchId = adoqueryPub->FieldByName("SearchId")->AsInteger; //2019-05-27
      dialoutitemset->SearchOrder = adoqueryPub->FieldByName("SearchOrder")->AsInteger; //2019-05-27
      dialoutitemset->DispOrder = adoqueryPub->FieldByName("DispOrder")->AsInteger; //2019-05-27
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertDialOutItemSet(CDialOutItemSet &dialoutitemset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "insert into tbdialoutitemset (\
TaskId,\
Tid,\
TFieldName,\
TControlType,\
TLabelCaption,\
TXpos,\
TYpos,\
THeigth,\
TWith,\
TDataType,\
TDefaultData,\
TMinValue,\
TMaxValue,\
TIsAllowNull,\
TExportId,\
TDispId,\
TModifyId,\
TMaskEditStr,\
SearchId,\
SearchOrder,\
DispOrder,\
THintStr\
) values (%d,%d,'%s',%d,'%s',%d,%d,%d,%d,%d,'%s',%d,%d,%d,%d,%d,%d,'%s',%d,%d,%d,'%s')",
  dialoutitemset.TaskId,
  dialoutitemset.Tid,
  dialoutitemset.TFieldName.c_str(),
  dialoutitemset.TControlType,
  dialoutitemset.TLabelCaption.c_str(),
  dialoutitemset.TXpos,
  dialoutitemset.TYpos,
  dialoutitemset.THeigth,
  dialoutitemset.TWith,
  dialoutitemset.TDataType,
  dialoutitemset.TDefaultData.c_str(),
  dialoutitemset.TMinValue,
  dialoutitemset.TMaxValue,
  dialoutitemset.TIsAllowNull,
  dialoutitemset.TExportId,
  dialoutitemset.TDispId,
  dialoutitemset.TModifyId,
  dialoutitemset.TMaskEditStr.c_str(),
  dialoutitemset.SearchId,
  dialoutitemset.SearchOrder,
  dialoutitemset.DispOrder,
  dialoutitemset.THintStr.c_str()
  );

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateDialOutItemSet(CDialOutItemSet &dialoutitemset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbdialoutitemset set \
TFieldName='%s',\
TControlType=%d,\
TLabelCaption='%s',\
TXpos=%d,\
TYpos=%d,\
THeigth=%d,\
TWith=%d,\
TDataType=%d,\
TDefaultData='%s',\
TMinValue=%d,\
TMaxValue=%d,\
TIsAllowNull=%d,\
TExportId=%d,\
TDispId=%d,\
TModifyId=%d,\
TMaskEditStr='%s',\
SearchId=%d,\
SearchOrder=%d,\
DispOrder=%d,\
THintStr='%s' \
where TaskId=%d and Tid=%d",
  dialoutitemset.TFieldName.c_str(),
  dialoutitemset.TControlType,
  dialoutitemset.TLabelCaption.c_str(),
  dialoutitemset.TXpos,
  dialoutitemset.TYpos,
  dialoutitemset.THeigth,
  dialoutitemset.TWith,
  dialoutitemset.TDataType,
  dialoutitemset.TDefaultData.c_str(),
  dialoutitemset.TMinValue,
  dialoutitemset.TMaxValue,
  dialoutitemset.TIsAllowNull,
  dialoutitemset.TExportId,
  dialoutitemset.TDispId,
  dialoutitemset.TModifyId,
  dialoutitemset.TMaskEditStr.c_str(),
  dialoutitemset.SearchId,
  dialoutitemset.SearchOrder,
  dialoutitemset.DispOrder,
  dialoutitemset.THintStr.c_str(),
  dialoutitemset.TaskId,
  dialoutitemset.Tid);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::DelDialOutItemSet(int TaskId, int nTid)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbdialoutitemset where TaskId=%d and tid=%d",
    TaskId, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  sprintf(sqlbuf, "delete from tbdialloutitemlist where TaskId=%d and tid=%d",
    TaskId, nTid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  return 0;
}

int TdmAgent::IsDialOutItemListExist(int TaskId, int nTid, int nItemNo)
{
  char sqlbuf[128];
  int result=0;

  sprintf(sqlbuf, "Select TitemId from tbdialloutitemlist where TaskId=%d and tid=%d and TitemId=%d",
    TaskId, nTid, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      result = 1;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::InsertDialOutItemListRecord(int TaskId, int nTid, int nItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo)
{
  char sqlbuf[4096];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbdialloutitemlist (TaskId,tid,TitemId,TitemText,TItemURL,TItemDemo) values (%d,%d,%d,'%s','%s','%s')",
    TaskId, nTid, nItemNo, strItemName.c_str(), strItemURL.c_str(), strItemDemo.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateDialOutItemListRecord(int TaskId, int nTid, int nItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo)
{
  char sqlbuf[4096];

  sprintf( sqlbuf, "update tbdialloutitemlist set TitemText='%s',TItemURL='%s',TItemDemo='%s' where TaskId=%d and tid=%d and TitemId=%d",
    strItemName.c_str(), strItemURL.c_str(), strItemDemo.c_str(), TaskId, nTid, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteDialOutItemListRecord(int TaskId, int nTid, int nItemNo)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbdialloutitemlist where TaskId=%d and tid=%d and TitemId=%d",
    TaskId, nTid, nItemNo);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::AddDialOutItemListCmbItem(TComboBox *combobox, int TaskId, int nTid)
{
    char sqlbuf[256];
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;
    combobox->Items->Clear();
    sprintf(sqlbuf, "Select TItemText from tbdialloutitemlist where TaskId=%d and tid=%d order by TItemId",
      TaskId, nTid);
    try
    {
        adoqueryPub1->SQL->Clear();
        adoqueryPub1->SQL->Add((char *)sqlbuf);
        adoqueryPub1->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub1->Eof )
    {
        itemname = adoqueryPub1->FieldByName("TItemText")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub1->Next();
    }
    adoqueryPub1->Close();
}
int TdmAgent::GetDialOutSelItemValue(CDialOutItemSet &dialoutitemset, AnsiString strItemName)
{
    char sqlbuf[256];
    int nResult=-1;

    if (ADOConnection1->Connected == false)
      return nResult;
    sprintf(sqlbuf, "Select TItemId from tbdialloutitemlist where TaskId=%d and tid=%d and TItemText='%s'",
      dialoutitemset.TaskId, dialoutitemset.Tid, strItemName.c_str());
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
        if ( !adoqueryPub->Eof )
        {
            nResult = adoqueryPub->FieldByName("TItemId")->AsInteger;
        }
        adoqueryPub->Close();
    }
    catch ( ... )
    {
    }
    return nResult;
}
//------------------------------------------------------------------------------
void __fastcall TdmAgent::adoQryDialOutItemSetCalcFields(TDataSet *DataSet)
{
  adoQryDialOutItemSetDispIsAllowNull->AsString = (adoQryDialOutItemSetTIsAllowNull->AsInteger == 1) ? "是" : "否";
  adoQryDialOutItemSetDispExportId->AsString = (adoQryDialOutItemSetTExportId->AsInteger == 1) ? "是" : "否";
  adoQryDialOutItemSetDispDispId->AsString = (adoQryDialOutItemSetTDispId->AsInteger == 1) ? "是" : "否";

  if (adoQryDialOutItemSetTModifyId->AsInteger == 0)
    adoQryDialOutItemSetDispModifyId->AsString = "受理介面不顯示";
  else if (adoQryDialOutItemSetTModifyId->AsInteger == 1)
    adoQryDialOutItemSetDispModifyId->AsString = "受理介面顯示";
  else if (adoQryDialOutItemSetTModifyId->AsInteger == 2)
    adoQryDialOutItemSetDispModifyId->AsString = "受理介面輸入";

  if (adoQryDialOutItemSetSearchId->AsInteger == 0)
    adoQryDialOutItemSetDispSearchId->AsString = "無";
  else if (adoQryDialOutItemSetSearchId->AsInteger == 1)
    adoQryDialOutItemSetDispSearchId->AsString = "模糊查詢";
  else if (adoQryDialOutItemSetSearchId->AsInteger == 2)
    adoQryDialOutItemSetDispSearchId->AsString = "精準查詢";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryDialOutTeleCalcFields(TDataSet *DataSet)
{
  adoQryDialOutTeleDispLastResult->AsString = DialoutResultItemList.GetItemName(adoQryDialOutTeleLastResult->AsInteger);
  if (adoQryDialOutTeleRecdFile->AsString != "")
    adoQryDialOutTeleDispRecd->AsString = "是";
  else
    adoQryDialOutTeleDispRecd->AsString = "否";
  if (adoQryDialOutTeleCallBackId->AsInteger == 0)
    adoQryDialOutTeleDispCallBackId->AsString = "不預約回撥";
  else if (adoQryDialOutTeleCallBackId->AsInteger == 1)
    adoQryDialOutTeleDispCallBackId->AsString = "預約回撥";
  else if (adoQryDialOutTeleCallBackId->AsInteger == 2)
    adoQryDialOutTeleDispCallBackId->AsString = "已回撥";
  else
    adoQryDialOutTeleDispCallBackId->AsString = "不預約回撥";
  adoQryDialOutTeleDispEndCode1->AsString = EndCode1ItemList.GetItemName(adoQryDialOutTeleEndCode1->AsInteger);
  adoQryDialOutTeleDispEndCode2->AsString = EndCode2ItemList.GetItemName(adoQryDialOutTeleEndCode1->AsInteger, adoQryDialOutTeleEndCode2->AsInteger);
  if (adoQryDialOutTeleTaskWorkerNo->AsString == "")
    adoQryDialOutTeleDispAllocState->AsString = "未分派";
  else
    adoQryDialOutTeleDispAllocState->AsString = "已分派";
}

//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryDialOutProcCalcFields(TDataSet *DataSet)
{
  adoQryDialOutProcDispLastResult->AsString = DialoutResultItemList.GetItemName(adoQryDialOutProcLastResult->AsInteger);
  if (adoQryDialOutProcRecdFile->AsString != "")
    adoQryDialOutProcDispRecd->AsString = "是";
  else
    adoQryDialOutProcDispRecd->AsString = "否";
  if (adoQryDialOutProcCallBackId->AsInteger == 0)
    adoQryDialOutProcDispCallBackId->AsString = "不預約回撥";
  else if (adoQryDialOutProcCallBackId->AsInteger == 1)
    adoQryDialOutProcDispCallBackId->AsString = "預約回撥";
  else if (adoQryDialOutProcCallBackId->AsInteger == 2)
    adoQryDialOutProcDispCallBackId->AsString = "已回撥";
  else
    adoQryDialOutProcDispCallBackId->AsString = "不預約回撥";
  adoQryDialOutProcDispEndCode1->AsString = EndCode1ItemList.GetItemName(adoQryDialOutProcEndCode1->AsInteger);
  adoQryDialOutProcDispEndCode2->AsString = EndCode2ItemList.GetItemName(adoQryDialOutProcEndCode1->AsInteger, adoQryDialOutProcEndCode2->AsInteger);
  adoQryDialOutProcDispCalledNo->AsString = GetHidePhoneNum(3, adoQryDialOutProcCalledNo->AsString);
  adoQryDialOutProcDispTeleNo->AsString = GetHidePhoneNum(3, adoQryDialOutProcTeleNo->AsString);
}
//---------------------------------------------------------------------------
void TdmAgent::GetAllDialOutItemSet()
{
  char sqlbuf[128];
  int i=0, nTaskId, nTaskIdCount;

  sprintf(sqlbuf, "select TaskId from tbdialouttask order by TaskId");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();
    nTaskIdCount = adoqueryPub->RecordCount+1;
    if (nTaskIdCount == 0)
      return;

    if (FormMain->m_pDialOutItemSetList != NULL)
    {
      delete []FormMain->m_pDialOutItemSetList;
      FormMain->m_pDialOutItemSetList=NULL;
    }
    FormMain->DialOutItemSetListCount = nTaskIdCount;
    FormMain->m_pDialOutItemSetList = new CDialOutItemSetList[nTaskIdCount];
    if (FormMain->m_pDialOutItemSetList == NULL)
      return;

    if (GetAllDialOutItemSet(0, FormMain->m_pDialOutItemSetList[i]) == 0)
      i++;
    adoqueryPub->First();
    while ( !adoqueryPub->Eof )
    {
      nTaskId = adoqueryPub->FieldByName("TaskId")->AsInteger;
      if (GetAllDialOutItemSet(nTaskId, FormMain->m_pDialOutItemSetList[i]) == 0)
        i++;
      adoqueryPub->Next();
    }

    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::GetAllDialOutItemSet(int nTaskId, CDialOutItemSetList &dialoutitemsetlist)
{
  char sqlbuf[128];
  int i=0, nControlCount;

  sprintf(sqlbuf, "select * from tbdialoutitemset where TaskId=%d order by TaskId,Tid", nTaskId);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub1->Open();
    nControlCount = dmAgent->adoqueryPub1->RecordCount;
    if (nControlCount == 0)
      return 1;
    if (dialoutitemsetlist.InitData(nControlCount) != 0)
      return 1;
    dmAgent->adoqueryPub1->First();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      if (dmAgent->GetDialOutItemSetRecord(dmAgent->adoqueryPub1, &dialoutitemsetlist.pDialOutItemSet[i]) == 0)
      {
        GetAllDialOutItemSetList(dialoutitemsetlist.pDialOutItemSet[i]);
        i++;
      }
      dmAgent->adoqueryPub1->Next();
    }
    dialoutitemsetlist.TaskId = nTaskId;
    dmAgent->adoqueryPub1->Close();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::GetAllDialOutItemSetList(CDialOutItemSet &dialoutitemset)
{
    char sqlbuf[256];
    int itemnum, itemvalue;
    AnsiString itemname, itemurl, itemdemo;

    if (ADOConnection1->Connected == false)
      return;
    sprintf(sqlbuf, "Select * from tbdialloutitemlist where TaskId=%d and Tid=%d order by TItemId",
      dialoutitemset.TaskId, dialoutitemset.Tid);
    try
    {
      adoqueryPub2->SQL->Clear();
      adoqueryPub2->SQL->Add((char *)sqlbuf);
      adoqueryPub2->Open();
      itemnum = adoqueryPub2->RecordCount;
      if (dialoutitemset.TLookupItemList.InitData(itemnum) == 0)
      {
        while ( !adoqueryPub2->Eof )
        {
          itemvalue = adoqueryPub2->FieldByName("TItemId")->AsInteger;
          itemname = adoqueryPub2->FieldByName("TItemText")->AsString;
          itemurl = adoqueryPub2->FieldByName("TItemURL")->AsString;
          itemdemo = adoqueryPub2->FieldByName("TItemDemo")->AsString;
          dialoutitemset.TLookupItemList.AddItem(itemvalue, itemname, itemdemo, itemurl);
          adoqueryPub2->Next();
        }
      }
      adoqueryPub2->Close();
    }
    catch ( ... )
    {
    }
}
//------------------------------------------------------------------------------
void TdmAgent::QryDialOutImportField()
{
  char sqlbuf[128];
  int i=0, nTaskId, nTaskCount;
  AnsiString strTaskName;

  sprintf(sqlbuf, "select TaskId,TaskName from tbdialouttask order by TaskId");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();
    nTaskCount = adoqueryPub->RecordCount+1;

    if (FormMain->m_pDialOutImportFieldList != NULL)
    {
      delete []FormMain->m_pDialOutImportFieldList;
      FormMain->m_pDialOutImportFieldList=NULL;
    }
    FormMain->DialOutImportTaskNum = nTaskCount;
    FormMain->m_pDialOutImportFieldList = new CDialOutImportFieldList[nTaskCount];
    if (FormMain->m_pDialOutImportFieldList == NULL)
      return;

    if (nTaskCount > 1)
    {
      adoqueryPub->First();
      while ( !adoqueryPub->Eof )
      {
        nTaskId = adoqueryPub->FieldByName("TaskId")->AsInteger;
        strTaskName = adoqueryPub->FieldByName("TaskName")->AsString;
        QryDialOutImportField(nTaskId, strTaskName, FormMain->m_pDialOutImportFieldList[i]);
          i++;
        adoqueryPub->Next();
      }
    }
    QryDialOutImportField(0, "確省任務", FormMain->m_pDialOutImportFieldList[i]);
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QryDialOutImportField(int TaskId, AnsiString TaskName, CDialOutImportFieldList &DialOutImportFieldList)
{
  char sqlbuf[256];
  AnsiString strFieldName, strFieldExplain;
  int nFieldNum=0, nFieldId, nControlType, nCount;

  sprintf(sqlbuf, "Select FieldName,FieldExplain,ControlType from tbdialoutfieldset where CanImport=1 and ImportId=1 order by id");
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      strFieldName = dmAgent->adoqueryPub1->FieldByName("FieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("FieldExplain")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      nControlType = dmAgent->adoqueryPub1->FieldByName("ControlType")->AsInteger;
      DialOutImportFieldList.m_DialOutImportField[nFieldNum].SetData(1, nControlType, 0, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      DialOutImportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }

  nCount = 0;
  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption from tbdialoutitemset where TaskId=%d and Tid>0 order by Tid", TaskId);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    DialOutImportFieldList.TaskId = TaskId;
    DialOutImportFieldList.TaskName = TaskName;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      DialOutImportFieldList.m_DialOutImportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      nCount++;
      DialOutImportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  if (nCount > 0)
    return;
  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption from tbdialoutitemset where TaskId=%d and Tid>0 order by Tid", 0);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    DialOutImportFieldList.TaskId = TaskId;
    DialOutImportFieldList.TaskName = TaskName;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      DialOutImportFieldList.m_DialOutImportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      nCount++;
      DialOutImportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QryDialOutExportField()
{
  char sqlbuf[128];
  int i=0, nTaskId, nTaskCount;
  AnsiString strTaskName;

  sprintf(sqlbuf, "select TaskId,TaskName from tbdialouttask order by TaskId");
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->Open();
    nTaskCount = adoqueryPub->RecordCount+1;

    if (FormMain->m_pDialOutExportFieldList != NULL)
    {
      delete []FormMain->m_pDialOutExportFieldList;
      FormMain->m_pDialOutExportFieldList=NULL;
    }
    FormMain->DialOutExportTaskNum = nTaskCount;
    FormMain->m_pDialOutExportFieldList = new CDialOutExportFieldList[nTaskCount];
    if (FormMain->m_pDialOutExportFieldList == NULL)
      return;

    if (nTaskCount > 1)
    {
      adoqueryPub->First();
      while ( !adoqueryPub->Eof )
      {
        nTaskId = adoqueryPub->FieldByName("TaskId")->AsInteger;
        strTaskName = adoqueryPub->FieldByName("TaskName")->AsString;
        QryDialOutExportField(nTaskId, strTaskName, FormMain->m_pDialOutExportFieldList[i]);
          i++;
        adoqueryPub->Next();
      }
    }
    QryDialOutExportField(0, "確省任務", FormMain->m_pDialOutExportFieldList[i]);
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
void TdmAgent::QryDialOutExportField(int TaskId, AnsiString TaskName, CDialOutExportFieldList &DialOutExportFieldList)
{
  char sqlbuf[256];
  AnsiString strFieldName, strFieldExplain;
  int nFieldNum=0, nFieldId, nControlType, nCount;

  sprintf(sqlbuf, "Select FieldName,FieldExplain,ControlType from tbdialoutfieldset where CanExport=1 and ExportId=1 order by id");
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      strFieldName = dmAgent->adoqueryPub1->FieldByName("FieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("FieldExplain")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      nControlType = dmAgent->adoqueryPub1->FieldByName("ControlType")->AsInteger;
      DialOutExportFieldList.m_DialOutExportField[nFieldNum].SetData(1, nControlType, 0, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      DialOutExportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }

  nCount = 0;
  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption from tbdialoutitemset where TaskId=%d and Tid>0 and TExportId=1 order by Tid", TaskId);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    DialOutExportFieldList.TaskId = TaskId;
    DialOutExportFieldList.TaskName = TaskName;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      DialOutExportFieldList.m_DialOutExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      nCount++;
      DialOutExportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
  if (nCount > 0)
    return;
  sprintf(sqlbuf, "Select Tid,TControlType,TFieldName,TLabelCaption from tbdialoutitemset where TaskId=%d and Tid>0 and TExportId=1 order by Tid", 0);
  try
  {
    dmAgent->adoqueryPub1->SQL->Clear();
    dmAgent->adoqueryPub1->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub1->Open();
    DialOutExportFieldList.TaskId = TaskId;
    DialOutExportFieldList.TaskName = TaskName;
    while ( !dmAgent->adoqueryPub1->Eof )
    {
      nFieldId = dmAgent->adoqueryPub1->FieldByName("Tid")->AsInteger;
      nControlType = dmAgent->adoqueryPub1->FieldByName("TControlType")->AsInteger;
      strFieldName = dmAgent->adoqueryPub1->FieldByName("TFieldName")->AsString;
      strFieldExplain = dmAgent->adoqueryPub1->FieldByName("TLabelCaption")->AsString;
      MyReplace(strFieldExplain, " ", "_", strFieldExplain);
      DialOutExportFieldList.m_DialOutExportField[nFieldNum].SetData(2, nControlType, nFieldId, strFieldName, strFieldExplain); //2014-11-09 update
      nFieldNum++;
      nCount++;
      DialOutExportFieldList.FieldNum = nFieldNum;
      dmAgent->adoqueryPub1->Next();
    }
    dmAgent->adoqueryPub1->Close();
  }
  catch ( ... )
  {
  }
}
//------------------------------------------------------------------------------
int TdmAgent::GetManDialOutTeleRecord(TADOQuery *adoquery, CDialOutTele *pDialOutTele)
{
  AnsiString strFieldName;
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
    pDialOutTele->Id = adoquery->FieldByName("Id")->AsInteger;
    pDialOutTele->DialId = adoquery->FieldByName("DialId")->AsString;
	  pDialOutTele->TaskId = adoquery->FieldByName("TaskId")->AsInteger;
    pDialOutTele->TaskCode = adoquery->FieldByName("TaskCode")->AsString;
    pDialOutTele->TaskName = adoquery->FieldByName("TaskName")->AsString;
    pDialOutTele->BatchId = adoquery->FieldByName("BatchId")->AsString;
    pDialOutTele->CallerNo = adoquery->FieldByName("CallerNo")->AsString;
    pDialOutTele->CalledNo = adoquery->FieldByName("CalledNo")->AsString;
    pDialOutTele->TeleNo = adoquery->FieldByName("TeleNo")->AsString;
    pDialOutTele->CustName = adoquery->FieldByName("CustName")->AsString;
    pDialOutTele->CustAddr = adoquery->FieldByName("CustAddr")->AsString;
    pDialOutTele->CustomNo = adoquery->FieldByName("CustomNo")->AsString;
    pDialOutTele->AccountNo = adoquery->FieldByName("AccountNo")->AsString;
    pDialOutTele->InsertTime = adoquery->FieldByName("InsertTime")->AsString;
    pDialOutTele->CalledTimes = adoquery->FieldByName("CalledTimes")->AsInteger;
    pDialOutTele->AnsTimes = adoquery->FieldByName("AnsTimes")->AsInteger;
    pDialOutTele->LastCallTime = adoquery->FieldByName("LastCallTime")->AsString;
    pDialOutTele->AnsTime = adoquery->FieldByName("AnsTime")->AsString;
    pDialOutTele->RelTime = adoquery->FieldByName("RelTime")->AsString;
    pDialOutTele->LastResult = adoquery->FieldByName("LastResult")->AsInteger;
    pDialOutTele->EndCode1 = adoquery->FieldByName("EndCode1")->AsInteger;
    pDialOutTele->EndCode2 = adoquery->FieldByName("EndCode2")->AsInteger;
    pDialOutTele->OrderCode = adoquery->FieldByName("OrderCode")->AsString;
    pDialOutTele->CallBackId = adoquery->FieldByName("CallBackId")->AsInteger;
    pDialOutTele->CallBackTime = adoquery->FieldByName("CallBackTime")->AsString;
    pDialOutTele->TaskWorkerNo = adoquery->FieldByName("TaskWorkerNo")->AsString;
    pDialOutTele->TalkWorkerNo = adoquery->FieldByName("TalkWorkerNo")->AsString;
    pDialOutTele->Priority = adoquery->FieldByName("Priority")->AsInteger;
    pDialOutTele->SerialNo = adoquery->FieldByName("SerialNo")->AsString;
    pDialOutTele->RecdRootPath = adoquery->FieldByName("RecdRootPath")->AsString;
    pDialOutTele->RecdFile = adoquery->FieldByName("RecdFile")->AsString;
    pDialOutTele->DialLog = adoquery->FieldByName("DialLog")->AsString;
    pDialOutTele->URLAddr = adoquery->FieldByName("URLAddr")->AsString;
    pDialOutTele->URLOrder = adoquery->FieldByName("URLOrder")->AsString;
    pDialOutTele->URLQuery = adoquery->FieldByName("URLQuery")->AsString;
    pDialOutTele->URLManOrder = adoquery->FieldByName("URLManOrder")->AsString;
    pDialOutTele->URLPrice = adoquery->FieldByName("URLPrice")->AsString;

    pDialOutTele->ItemNum = 0;
    for (int i=1; i<=MAX_DIALOUT_CONTROL_NUM; i++)
    {
      strFieldName = "Itemdata"+IntToStr(i);
      pDialOutTele->ItemData[i-1] = adoquery->FieldByName(strFieldName)->AsString;
      pDialOutTele->ItemNum ++;
    }

    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::GetManDialOutTeleRecord(int recordid, CDialOutTele *pDialOutTele)
{
  AnsiString strFieldName;
  char sqlbuf[128];
  int result=1;

  sprintf(sqlbuf, "Select * from vm_dialout where Id=%d",
    recordid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      pDialOutTele->Id = adoqueryPub->FieldByName("Id")->AsInteger;
      pDialOutTele->DialId = adoqueryPub->FieldByName("DialId")->AsString;
      pDialOutTele->TaskId = adoqueryPub->FieldByName("TaskId")->AsInteger;
      pDialOutTele->TaskCode = adoqueryPub->FieldByName("TaskCode")->AsString;
      pDialOutTele->TaskName = adoqueryPub->FieldByName("TaskName")->AsString;
      pDialOutTele->BatchId = adoqueryPub->FieldByName("BatchId")->AsString;
      pDialOutTele->CallerNo = adoqueryPub->FieldByName("CallerNo")->AsString;
      pDialOutTele->CalledNo = adoqueryPub->FieldByName("CalledNo")->AsString;
      pDialOutTele->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
      pDialOutTele->CustName = adoqueryPub->FieldByName("CustName")->AsString;
      pDialOutTele->CustAddr = adoqueryPub->FieldByName("CustAddr")->AsString;
      pDialOutTele->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
      pDialOutTele->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;
      pDialOutTele->InsertTime = adoqueryPub->FieldByName("InsertTime")->AsString;
      pDialOutTele->CalledTimes = adoqueryPub->FieldByName("CalledTimes")->AsInteger;
      pDialOutTele->AnsTimes = adoqueryPub->FieldByName("AnsTimes")->AsInteger;
      pDialOutTele->LastCallTime = adoqueryPub->FieldByName("LastCallTime")->AsString;
      pDialOutTele->AnsTime = adoqueryPub->FieldByName("AnsTime")->AsString;
      pDialOutTele->RelTime = adoqueryPub->FieldByName("RelTime")->AsString;
      pDialOutTele->LastResult = adoqueryPub->FieldByName("LastResult")->AsInteger;
      pDialOutTele->EndCode1 = adoqueryPub->FieldByName("EndCode1")->AsInteger;
      pDialOutTele->EndCode2 = adoqueryPub->FieldByName("EndCode2")->AsInteger;
      pDialOutTele->OrderCode = adoqueryPub->FieldByName("OrderCode")->AsString;
      pDialOutTele->CallBackId = adoqueryPub->FieldByName("CallBackId")->AsInteger;
      pDialOutTele->CallBackTime = adoqueryPub->FieldByName("CallBackTime")->AsString;
      pDialOutTele->TaskWorkerNo = adoqueryPub->FieldByName("TaskWorkerNo")->AsString;
      pDialOutTele->TalkWorkerNo = adoqueryPub->FieldByName("TalkWorkerNo")->AsString;
      pDialOutTele->Priority = adoqueryPub->FieldByName("Priority")->AsInteger;
      pDialOutTele->SerialNo = adoqueryPub->FieldByName("SerialNo")->AsString;
      pDialOutTele->RecdRootPath = adoqueryPub->FieldByName("RecdRootPath")->AsString;
      pDialOutTele->RecdFile = adoqueryPub->FieldByName("RecdFile")->AsString;
      pDialOutTele->DialLog = adoqueryPub->FieldByName("DialLog")->AsString;
      pDialOutTele->URLAddr = adoqueryPub->FieldByName("URLAddr")->AsString;
      pDialOutTele->URLOrder = adoqueryPub->FieldByName("URLOrder")->AsString;
      pDialOutTele->URLQuery = adoqueryPub->FieldByName("URLQuery")->AsString;
      pDialOutTele->URLManOrder = adoqueryPub->FieldByName("URLManOrder")->AsString;
      pDialOutTele->URLPrice = adoqueryPub->FieldByName("URLPrice")->AsString;
      pDialOutTele->ItemNum = 0;
      for (int i=1; i<=MAX_DIALOUT_CONTROL_NUM; i++)
      {
        strFieldName = "Itemdata"+IntToStr(i);
        pDialOutTele->ItemData[i-1] = adoqueryPub->FieldByName(strFieldName)->AsString;
        pDialOutTele->ItemNum ++;
      }
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::GetManDialOutTeleRecord(AnsiString dialid, CDialOutTele *pDialOutTele)
{
  AnsiString strFieldName;
  char sqlbuf[128];
  int result=1;

  sprintf(sqlbuf, "Select * from vm_dialout where DialId='%s'",
    dialid.c_str());
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add(sqlbuf );
    adoqueryPub->Open();

    if ( !adoqueryPub->Eof )
    {
      pDialOutTele->Id = adoqueryPub->FieldByName("Id")->AsInteger;
      pDialOutTele->DialId = adoqueryPub->FieldByName("DialId")->AsString;
      pDialOutTele->TaskId = adoqueryPub->FieldByName("TaskId")->AsInteger;
      pDialOutTele->TaskCode = adoqueryPub->FieldByName("TaskCode")->AsString;
      pDialOutTele->TaskName = adoqueryPub->FieldByName("TaskName")->AsString;
      pDialOutTele->BatchId = adoqueryPub->FieldByName("BatchId")->AsString;
      pDialOutTele->CallerNo = adoqueryPub->FieldByName("CallerNo")->AsString;
      pDialOutTele->CalledNo = adoqueryPub->FieldByName("CalledNo")->AsString;
      pDialOutTele->TeleNo = adoqueryPub->FieldByName("TeleNo")->AsString;
      pDialOutTele->CustName = adoqueryPub->FieldByName("CustName")->AsString;
      pDialOutTele->CustAddr = adoqueryPub->FieldByName("CustAddr")->AsString;
      pDialOutTele->CustomNo = adoqueryPub->FieldByName("CustomNo")->AsString;
      pDialOutTele->AccountNo = adoqueryPub->FieldByName("AccountNo")->AsString;
      pDialOutTele->InsertTime = adoqueryPub->FieldByName("InsertTime")->AsString;
      pDialOutTele->CalledTimes = adoqueryPub->FieldByName("CalledTimes")->AsInteger;
      pDialOutTele->AnsTimes = adoqueryPub->FieldByName("AnsTimes")->AsInteger;
      pDialOutTele->LastCallTime = adoqueryPub->FieldByName("LastCallTime")->AsString;
      pDialOutTele->AnsTime = adoqueryPub->FieldByName("AnsTime")->AsString;
      pDialOutTele->RelTime = adoqueryPub->FieldByName("RelTime")->AsString;
      pDialOutTele->LastResult = adoqueryPub->FieldByName("LastResult")->AsInteger;
      pDialOutTele->EndCode1 = adoqueryPub->FieldByName("EndCode1")->AsInteger;
      pDialOutTele->EndCode2 = adoqueryPub->FieldByName("EndCode2")->AsInteger;
      pDialOutTele->OrderCode = adoqueryPub->FieldByName("OrderCode")->AsString;
      pDialOutTele->CallBackId = adoqueryPub->FieldByName("CallBackId")->AsInteger;
      pDialOutTele->CallBackTime = adoqueryPub->FieldByName("CallBackTime")->AsString;
      pDialOutTele->TaskWorkerNo = adoqueryPub->FieldByName("TaskWorkerNo")->AsString;
      pDialOutTele->TalkWorkerNo = adoqueryPub->FieldByName("TalkWorkerNo")->AsString;
      pDialOutTele->Priority = adoqueryPub->FieldByName("Priority")->AsInteger;
      pDialOutTele->SerialNo = adoqueryPub->FieldByName("SerialNo")->AsString;
      pDialOutTele->RecdRootPath = adoqueryPub->FieldByName("RecdRootPath")->AsString;
      pDialOutTele->RecdFile = adoqueryPub->FieldByName("RecdFile")->AsString;
      pDialOutTele->DialLog = adoqueryPub->FieldByName("DialLog")->AsString;
      pDialOutTele->URLAddr = adoqueryPub->FieldByName("URLAddr")->AsString;
      pDialOutTele->URLOrder = adoqueryPub->FieldByName("URLOrder")->AsString;
      pDialOutTele->URLQuery = adoqueryPub->FieldByName("URLQuery")->AsString;
      pDialOutTele->URLManOrder = adoqueryPub->FieldByName("URLManOrder")->AsString;
      pDialOutTele->URLPrice = adoqueryPub->FieldByName("URLPrice")->AsString;
      pDialOutTele->ItemNum = 0;
      for (int i=1; i<=MAX_DIALOUT_CONTROL_NUM; i++)
      {
        strFieldName = "Itemdata"+IntToStr(i);
        pDialOutTele->ItemData[i-1] = adoqueryPub->FieldByName(strFieldName)->AsString;
        pDialOutTele->ItemNum ++;
      }
      result = 0;
    }
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return result;
}
int TdmAgent::UpdateManDialOutTeleRecord(CDialOutTele *pDialOutTele)
{
  char sqlbuf1[8000], szTemp[256];
  //2015-04-09
  memset(sqlbuf1, 0, 8000);
  sprintf(sqlbuf1, "update tbdialouttele set SaveTime=getdate(),TeleNo='%s',CustName='%s',CustAddr='%s',AccountNo='%s',EndCode1=%d,EndCode2=%d,OrderCode='%s',DialLog='%s'",
    pDialOutTele->TeleNo.c_str(), pDialOutTele->CustName.c_str(), pDialOutTele->CustAddr.c_str(), pDialOutTele->AccountNo.c_str(), pDialOutTele->EndCode1, pDialOutTele->EndCode2, pDialOutTele->OrderCode.c_str(), pDialOutTele->DialLog.c_str());
  for (int i=1; i<=pDialOutTele->ItemNum; i++)
  {
    sprintf(szTemp,",ItemData%d=", i);
    strcat(sqlbuf1, szTemp);

    sprintf(szTemp,"'%s'", pDialOutTele->ItemData[i-1].c_str());
    strcat(sqlbuf1, szTemp);
  }
  sprintf(szTemp, " where Id=%d", pDialOutTele->Id);
  strcat(sqlbuf1, szTemp);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf1 );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}


void __fastcall TdmAgent::adoQryDialOutCalcFields(TDataSet *DataSet)
{
  if (adoQryDialOutitem->AsInteger == 9999)
    adoQryDialOutDispItemName->AsString = "總計";
  else
    adoQryDialOutDispItemName->AsString = DialoutResultItemList.GetItemName(adoQryDialOutitem->AsInteger);
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryBlackCalcFields(TDataSet *DataSet)
{
  adoQryBlackDispOpenFlag->AsString = (adoQryBlackOpenFlag->AsInteger==1)?"黑名單":"白名單";  
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryCalloutTeleHCalcFields(TDataSet *DataSet)
{
  adoQryCalloutTeleHDispLastResult->AsString = DialoutResultItemList.GetItemName(adoQryCalloutTeleHLastResult->AsInteger);  
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryDutyTelCalcFields(TDataSet *DataSet)
{
  switch (adoQryDutyTelDayType->AsInteger)
  {
    case 1:
      adoQryDutyTeldispDayType->AsString = "每周某天";
      if (adoQryDutyTelStartDayTime->AsString == "0")
        adoQryDutyTeldispStartDayTime->AsString = "星期日";
      else if (adoQryDutyTelStartDayTime->AsString == "1")
        adoQryDutyTeldispStartDayTime->AsString = "星期一";
      else if (adoQryDutyTelStartDayTime->AsString == "2")
        adoQryDutyTeldispStartDayTime->AsString = "星期二";
      else if (adoQryDutyTelStartDayTime->AsString == "3")
        adoQryDutyTeldispStartDayTime->AsString = "星期三";
      else if (adoQryDutyTelStartDayTime->AsString == "4")
        adoQryDutyTeldispStartDayTime->AsString = "星期四";
      else if (adoQryDutyTelStartDayTime->AsString == "5")
        adoQryDutyTeldispStartDayTime->AsString = "星期五";
      else if (adoQryDutyTelStartDayTime->AsString == "6")
        adoQryDutyTeldispStartDayTime->AsString = "星期六";
      else
        adoQryDutyTeldispStartDayTime->AsString = "";

      if (adoQryDutyTelEndDayTime->AsString == "0")
        adoQryDutyTeldispEndDayTime->AsString = "星期日";
      else if (adoQryDutyTelEndDayTime->AsString == "1")
        adoQryDutyTeldispEndDayTime->AsString = "星期一";
      else if (adoQryDutyTelEndDayTime->AsString == "2")
        adoQryDutyTeldispEndDayTime->AsString = "星期二";
      else if (adoQryDutyTelEndDayTime->AsString == "3")
        adoQryDutyTeldispEndDayTime->AsString = "星期三";
      else if (adoQryDutyTelEndDayTime->AsString == "4")
        adoQryDutyTeldispEndDayTime->AsString = "星期四";
      else if (adoQryDutyTelEndDayTime->AsString == "5")
        adoQryDutyTeldispEndDayTime->AsString = "星期五";
      else if (adoQryDutyTelEndDayTime->AsString == "6")
        adoQryDutyTeldispEndDayTime->AsString = "星期六";
      else
        adoQryDutyTeldispEndDayTime->AsString = "";
      break;
    case 2:
      adoQryDutyTeldispDayType->AsString = "每月某天";
      adoQryDutyTeldispStartDayTime->AsString = "每月" + adoQryDutyTelStartDayTime->AsString + "號";
      adoQryDutyTeldispEndDayTime->AsString = "每月" + adoQryDutyTelEndDayTime->AsString + "號";
      break;
    case 3:
      adoQryDutyTeldispDayType->AsString = "具體某天";
      adoQryDutyTeldispStartDayTime->AsString = adoQryDutyTelStartDayTime->AsString;
      adoQryDutyTeldispEndDayTime->AsString = adoQryDutyTelEndDayTime->AsString;
      break;
    case 4:
      adoQryDutyTeldispDayType->AsString = "連續時間段";
      adoQryDutyTeldispStartDayTime->AsString = adoQryDutyTelStartDayTime->AsString;
      adoQryDutyTeldispEndDayTime->AsString = adoQryDutyTelEndDayTime->AsString;
      break;
    default:
      adoQryDutyTeldispDayType->AsString = "每周某天";
      adoQryDutyTeldispStartDayTime->AsString = adoQryDutyTelStartDayTime->AsString;
      adoQryDutyTeldispEndDayTime->AsString = adoQryDutyTelEndDayTime->AsString;
      break;
  }
  if (adoQryDutyTelOpenFlag->AsInteger == 0)
    adoQryDutyTeldispOpenFlag->AsString = "停用";
  else
    adoQryDutyTeldispOpenFlag->AsString = "啟用";
}
//---------------------------------------------------------------------------
int TdmAgent::GetDutyTelRecord(TADOQuery *adoquery, CDutyTel *dutytel)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  dutytel->id = adoquery->FieldByName("id")->AsInteger;
	  dutytel->DayType = adoquery->FieldByName("DayType")->AsInteger;
	  dutytel->StartDayTime = adoquery->FieldByName("StartDayTime")->AsString;
	  dutytel->EndDayTime = adoquery->FieldByName("EndDayTime")->AsString;
	  dutytel->StartTime1 = adoquery->FieldByName("StartTime1")->AsString;
	  dutytel->EndTime1 = adoquery->FieldByName("EndTime1")->AsString;
	  dutytel->StartTime2 = adoquery->FieldByName("StartTime2")->AsString;
	  dutytel->EndTime2 = adoquery->FieldByName("EndTime2")->AsString;
	  dutytel->DutyTele1 = adoquery->FieldByName("DutyTele1")->AsString;
	  dutytel->DutyTele2 = adoquery->FieldByName("DutyTele2")->AsString;
	  dutytel->OpenFlag = adoquery->FieldByName("OpenFlag")->AsInteger;
	  dutytel->WorkerName = adoquery->FieldByName("WorkerName")->AsString;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertDutyTelRecord(CDutyTel *dutytel)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into tbdutytele (\
DayType,\
StartDayTime,\
EndDayTime,\
StartTime1,\
EndTime1,\
StartTime2,\
EndTime2,\
DutyTele1,\
DutyTele2,\
OpenFlag,\
WorkerName,\
InsertTime,\
UpdateDateTime,\
Status\
) values (%d,'%s','%s','%s','%s','%s','%s','%s','%s',%d,'%s',%s,%s,0)",
  dutytel->DayType,
  dutytel->StartDayTime.c_str(),
  dutytel->EndDayTime.c_str(),
  dutytel->StartTime1.c_str(),
  dutytel->EndTime1.c_str(),
  dutytel->StartTime2.c_str(),
  dutytel->EndTime2.c_str(),
  dutytel->DutyTele1.c_str(),
  dutytel->DutyTele2.c_str(),
  dutytel->OpenFlag,
  dutytel->WorkerName.c_str(),
  MyNowFuncName.c_str(),
  MyNowFuncName.c_str());

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateDutyTelRecord(CDutyTel *dutytel)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update tbdutytele set \
DayType=%d,\
StartDayTime='%s',\
EndDayTime='%s',\
StartTime1='%s',\
EndTime1='%s',\
StartTime2='%s',\
EndTime2='%s',\
DutyTele1='%s',\
DutyTele2='%s',\
OpenFlag=%d,\
WorkerName='%s',\
UpdateDateTime=%s \
where id=%d",
  dutytel->DayType,
  dutytel->StartDayTime.c_str(),
  dutytel->EndDayTime.c_str(),
  dutytel->StartTime1.c_str(),
  dutytel->EndTime1.c_str(),
  dutytel->StartTime2.c_str(),
  dutytel->EndTime2.c_str(),
  dutytel->DutyTele1.c_str(),
  dutytel->DutyTele2.c_str(),
  dutytel->OpenFlag,
  dutytel->WorkerName.c_str(),
  MyNowFuncName.c_str(),
  dutytel->id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteDutyTelRecord(int id)
{
  char sqlbuf[256];

  sprintf(sqlbuf, "delete from tbdutytele where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
int TdmAgent::InsertHolidayRecord(AnsiString strCTID, AnsiString strHolidayName, AnsiString strStartDay, AnsiString strEdnDay, int nOpenFlag)
{
  char sqlbuf[256];

  if (ADOConnection1->Connected == false)
    return 1;
  if (FormMain->ckHDCloudDB->Checked == true)
    sprintf( sqlbuf, "insert into callcenterdb.dbo.tbCcHolidaySet (CTID,Hotline,HolidayName,StartDay,EndDay,OpenFlag) values ('%s','*','%s','%s','%s',%d)",
    strCTID.c_str(), strHolidayName.c_str(), strStartDay.c_str(), strEdnDay.c_str(), nOpenFlag);
  else
    sprintf( sqlbuf, "insert into tbHoliday (CTID,Hotline,HolidayName,StartDay,EndDay,OpenFlag) values ('%s','*','%s','%s','%s',%d)",
    strCTID.c_str(), strHolidayName.c_str(), strStartDay.c_str(), strEdnDay.c_str(), nOpenFlag);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateHolidayRecord(int id, AnsiString strCTID, AnsiString strHolidayName, AnsiString strStartDay, AnsiString strEdnDay, int nOpenFlag)
{
  char sqlbuf[256];

  if (FormMain->ckHDCloudDB->Checked == true)
    sprintf( sqlbuf, "update callcenterdb.dbo.tbCcHolidaySet set CTID='%s',HolidayName='%s',StartDay='%s',EndDay='%s',OpenFlag=%d where id=%d",
    strCTID.c_str(), strHolidayName.c_str(), strStartDay.c_str(), strEdnDay.c_str(), nOpenFlag, id);
  else
    sprintf( sqlbuf, "update tbHoliday set CTID='%s',HolidayName='%s',StartDay='%s',EndDay='%s',OpenFlag=%d where id=%d",
    strCTID.c_str(), strHolidayName.c_str(), strStartDay.c_str(), strEdnDay.c_str(), nOpenFlag, id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteHolidayRecord(int id)
{
  char sqlbuf[256];

  if (FormMain->ckHDCloudDB->Checked == true)
    sprintf(sqlbuf, "delete from callcenterdb.dbo.tbCcHolidaySet where id=%d", id);
  else
    sprintf(sqlbuf, "delete from tbHoliday where id=%d", id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryHolidayCalcFields(TDataSet *DataSet)
{
  if (adoQryHolidayOpenFlag->AsInteger == 1)
    adoQryHolidayDispOpenFlag->AsString = "啟用";
  else
    adoQryHolidayDispOpenFlag->AsString = "停用";
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryWorkTimeSetCalcFields(TDataSet *DataSet)
{
  switch (adoQryWorkTimeSetDayType->AsInteger)
  {
    case 2:
      adoQryWorkTimeSetDispDayType->AsString = "每月某天";
      adoQryWorkTimeSetDispStartDay->AsString = "每月" + adoQryWorkTimeSetStartDay->AsString + "日";
      adoQryWorkTimeSetDispEndDay->AsString = "每月" + adoQryWorkTimeSetEndDay->AsString + "日";
      break;
    case 3:
      adoQryWorkTimeSetDispDayType->AsString = "具體某天";
      adoQryWorkTimeSetDispStartDay->AsString = adoQryWorkTimeSetStartDay->AsString;
      adoQryWorkTimeSetDispEndDay->AsString = adoQryWorkTimeSetEndDay->AsString;
      break;
    case 1:
    default:
      adoQryWorkTimeSetDispDayType->AsString = "每周某天";
      if (adoQryWorkTimeSetStartDay->AsString == "0")
        adoQryWorkTimeSetDispStartDay->AsString = "星期日";
      else if (adoQryWorkTimeSetStartDay->AsString == "1")
        adoQryWorkTimeSetDispStartDay->AsString = "星期一";
      else if (adoQryWorkTimeSetStartDay->AsString == "2")
        adoQryWorkTimeSetDispStartDay->AsString = "星期二";
      else if (adoQryWorkTimeSetStartDay->AsString == "3")
        adoQryWorkTimeSetDispStartDay->AsString = "星期三";
      else if (adoQryWorkTimeSetStartDay->AsString == "4")
        adoQryWorkTimeSetDispStartDay->AsString = "星期四";
      else if (adoQryWorkTimeSetStartDay->AsString == "5")
        adoQryWorkTimeSetDispStartDay->AsString = "星期五";
      else if (adoQryWorkTimeSetStartDay->AsString == "6")
        adoQryWorkTimeSetDispStartDay->AsString = "星期六";
      else
        adoQryWorkTimeSetDispStartDay->AsString = "";

      if (adoQryWorkTimeSetEndDay->AsString == "0")
        adoQryWorkTimeSetDispEndDay->AsString = "星期日";
      else if (adoQryWorkTimeSetEndDay->AsString == "1")
        adoQryWorkTimeSetDispEndDay->AsString = "星期一";
      else if (adoQryWorkTimeSetEndDay->AsString == "2")
        adoQryWorkTimeSetDispEndDay->AsString = "星期二";
      else if (adoQryWorkTimeSetEndDay->AsString == "3")
        adoQryWorkTimeSetDispEndDay->AsString = "星期三";
      else if (adoQryWorkTimeSetEndDay->AsString == "4")
        adoQryWorkTimeSetDispEndDay->AsString = "星期四";
      else if (adoQryWorkTimeSetEndDay->AsString == "5")
        adoQryWorkTimeSetDispEndDay->AsString = "星期五";
      else if (adoQryWorkTimeSetEndDay->AsString == "6")
        adoQryWorkTimeSetDispEndDay->AsString = "星期六";
      else
        adoQryWorkTimeSetDispEndDay->AsString = "";
      break;
  }
  if (adoQryWorkTimeSetOpenFlag->AsInteger == 0)
    adoQryWorkTimeSetDispOpenFlag->AsString = "停用";
  else
    adoQryWorkTimeSetDispOpenFlag->AsString = "啟用";

  if (adoQryWorkTimeSetHotLine->AsString == "*")
    adoQryWorkTimeSetDispHotLine->AsString = "所有號碼";
  else
    adoQryWorkTimeSetDispHotLine->AsString = adoQryWorkTimeSetHotLine->AsString;
  switch (adoQryWorkTimeSetWorkType->AsInteger)
  {
  case 0:
    adoQryWorkTimeSetDispWorkType->AsString = "下班時間";
    break;
  case 1:
    adoQryWorkTimeSetDispWorkType->AsString = "上班時間";
    break;
  case 2:
    adoQryWorkTimeSetDispWorkType->AsString = "放假時間";
    break;
  case 3:
    adoQryWorkTimeSetDispWorkType->AsString = "午休時間";
    break;
  case 4:
    adoQryWorkTimeSetDispWorkType->AsString = "值班時間";
    break;
  case 5:
    adoQryWorkTimeSetDispWorkType->AsString = "晚餐時間";
    break;
  }
}
int TdmAgent::GetWorkTimeSetRecord(TADOQuery *adoquery, CWorkTimeSet *worktimeset)
{
  if (adoquery->State == dsInactive || adoquery->Eof)
    return 1;
  try
  {
	  worktimeset->id = adoquery->FieldByName("id")->AsInteger;
    worktimeset->CTID = adoquery->FieldByName("CTID")->AsString;
	  worktimeset->HotLine = adoquery->FieldByName("HotLine")->AsString;
	  worktimeset->DayType = adoquery->FieldByName("DayType")->AsInteger;
	  worktimeset->StartDay = adoquery->FieldByName("StartDay")->AsString;
	  worktimeset->EndDay = adoquery->FieldByName("EndDay")->AsString;
	  worktimeset->StartTime1 = adoquery->FieldByName("StartTime1")->AsString;
	  worktimeset->EndTime1 = adoquery->FieldByName("EndTime1")->AsString;
	  worktimeset->StartTime2 = adoquery->FieldByName("StartTime2")->AsString;
	  worktimeset->EndTime2 = adoquery->FieldByName("EndTime2")->AsString;
	  worktimeset->WorkType = adoquery->FieldByName("WorkType")->AsInteger;
	  worktimeset->OpenFlag = adoquery->FieldByName("OpenFlag")->AsInteger;
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int TdmAgent::InsertWorkTimeSetRecord(CWorkTimeSet *worktimeset)
{
  char sqlbuf[2048];

  sprintf( sqlbuf, "insert into %s (\
CTID,\
HotLine,\
DayType,\
StartDay,\
EndDay,\
StartTime1,\
EndTime1,\
StartTime2,\
EndTime2,\
WorkType,\
OpenFlag\
) values ('%s','%s',%d,'%s','%s','%s','%s','%s','%s',%d,%d)",
  FormMain->ckWTCloudDB->Checked == true ? "callcenterdb.dbo.tbCcWorkerTimeSet" : "tbWorkTimeSet",
  worktimeset->CTID.c_str(),
  worktimeset->HotLine.c_str(),
  worktimeset->DayType,
  worktimeset->StartDay.c_str(),
  worktimeset->EndDay.c_str(),
  worktimeset->StartTime1.c_str(),
  worktimeset->EndTime1.c_str(),
  worktimeset->StartTime2.c_str(),
  worktimeset->EndTime2.c_str(),
  worktimeset->WorkType,
  worktimeset->OpenFlag);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateWorkTimeSetRecord(CWorkTimeSet *worktimeset)
{
  char sqlbuf[2048];
  sprintf( sqlbuf, "update %s set \
CTID='%s',\
HotLine='%s',\
DayType=%d,\
StartDay='%s',\
EndDay='%s',\
StartTime1='%s',\
EndTime1='%s',\
StartTime2='%s',\
EndTime2='%s',\
WorkType=%d,\
OpenFlag=%d \
where id=%d",
  FormMain->ckWTCloudDB->Checked == true ? "callcenterdb.dbo.tbCcWorkerTimeSet" : "tbWorkTimeSet",
  worktimeset->CTID.c_str(),
  worktimeset->HotLine.c_str(),
  worktimeset->DayType,
  worktimeset->StartDay.c_str(),
  worktimeset->EndDay.c_str(),
  worktimeset->StartTime1.c_str(),
  worktimeset->EndTime1.c_str(),
  worktimeset->StartTime2.c_str(),
  worktimeset->EndTime2.c_str(),
  worktimeset->WorkType,
  worktimeset->OpenFlag,
  worktimeset->id);

  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
void TdmAgent::DeleteWorkTimeSetRecord(int id)
{
  char sqlbuf[256];

  if (FormMain->ckWTCloudDB->Checked == true)
    sprintf(sqlbuf, "delete from callcenterdb.dbo.tbCcWorkerTimeSet where id=%d", id);
  else
    sprintf(sqlbuf, "delete from tbWorkTimeSet where id=%d", id);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adoQryWorkerGroupCalcFields(TDataSet *DataSet)
{
  if (adoQryWorkerGroupACWTimer->AsInteger == 0)
    adoQryWorkerGroupDispACWTimer->AsString = "按系統設定參數";
  else
    adoQryWorkerGroupDispACWTimer->AsString = IntToStr(adoQryWorkerGroupACWTimer->AsInteger)+"秒";
  adoQryWorkerGroupDepartmentName->AsString = DepartmentItemList.GetItemName(adoQryWorkerGroupDepartmentID->AsInteger);
}
//---------------------------------------------------------------------------


void __fastcall TdmAgent::adoQryDialOutTeleDCalcFields(TDataSet *DataSet)
{
  adoQryDialOutTeleDDispLastResult->AsString = DialoutResultItemList.GetItemName(adoQryDialOutTeleDLastResult->AsInteger);
  if (adoQryDialOutTeleDRecdFile->AsString != "")
    adoQryDialOutTeleDDispRecd->AsString = "是";
  else
    adoQryDialOutTeleDDispRecd->AsString = "否";
  if (adoQryDialOutTeleDCallBackId->AsInteger == 0)
    adoQryDialOutTeleDDispCallBackId->AsString = "不預約回撥";
  else if (adoQryDialOutTeleDCallBackId->AsInteger == 1)
    adoQryDialOutTeleDDispCallBackId->AsString = "預約回撥";
  else if (adoQryDialOutTeleDCallBackId->AsInteger == 2)
    adoQryDialOutTeleDDispCallBackId->AsString = "已回撥";
  else
    adoQryDialOutTeleDDispCallBackId->AsString = "不預約回撥";
  adoQryDialOutTeleDDispEndCode1->AsString = EndCode1ItemList.GetItemName(adoQryDialOutTeleDEndCode1->AsInteger);
  adoQryDialOutTeleDDispEndCode2->AsString = EndCode2ItemList.GetItemName(adoQryDialOutTeleDEndCode1->AsInteger, adoQryDialOutTeleDEndCode2->AsInteger);
  if (adoQryDialOutTeleDTaskWorkerNo->AsString == "")
    adoQryDialOutTeleDDispAllocState->AsString = "未分派";
  else
    adoQryDialOutTeleDDispAllocState->AsString = "已分派";
}
//---------------------------------------------------------------------------
int TdmAgent::GetDialOutTotalRecordCount(int TaskId)
{
  int nCount=0;
  char sqlbuf[256];

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as counts from tbdialouttele where taskid=%d", TaskId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->Open();
    nCount = adoqueryPub->FieldByName("counts")->AsInteger;
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nCount;
}
int TdmAgent::GetDialOutTotalAllocCount(int TaskId)
{
  int nCount=0;
  char sqlbuf[256];

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as counts from tbdialouttele where taskid=%d and len(TaskWorkerNo)>0", TaskId);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->Open();
    nCount = adoqueryPub->FieldByName("counts")->AsInteger;
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nCount;
}
int TdmAgent::GetDialOutTotalOrderCount(int TaskId)
{
  int nCount=0;
  char sqlbuf[256];

  if (ADOConnection1->Connected == false)
    return 0;
  sprintf(sqlbuf, "select count(*) as counts from tbdialouttele where taskid=%d and EndCode1=%d", TaskId, g_nOrderEndCode1);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->Open();
    nCount = adoqueryPub->FieldByName("counts")->AsInteger;
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nCount;
}

void __fastcall TdmAgent::adoQryDialOutListCalcFields(TDataSet *DataSet)
{
  adoQryDialOutListDispCallResult->AsString = DialoutResultItemList.GetItemName(adoQryDialOutListCallResult->AsInteger);
  adoQryDialOutListDispEndCode1->AsString = EndCode1ItemList.GetItemName(adoQryDialOutListEndCode1->AsInteger);
  adoQryDialOutListDispEndCode2->AsString = EndCode2ItemList.GetItemName(adoQryDialOutListEndCode1->AsInteger, adoQryDialOutListEndCode2->AsInteger);
}
//---------------------------------------------------------------------------
AnsiString TdmAgent::GetPopString(int popid)
{
  AnsiString strPop="";
  char sqlbuf[256];

  if (ADOConnection1->Connected == false)
    return 0;
  if (g_nTranListFlag == 0)
    sprintf(sqlbuf, "select MenuName from tbTranList where Id=%d", popid);
  else
    sprintf(sqlbuf, "select MenuName from callcenterdb.dbo.tbCcTranList where Id=%d", popid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->Open();
    if (!adoqueryPub->Eof)
      strPop = adoqueryPub->FieldByName("MenuName")->AsString;
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  WriteErrprMsg("GetPopString sqlbuf=%s strPop=%s", sqlbuf, strPop);
  return strPop;
}
AnsiString TdmAgent::GetPopURL(int popid)
{
  AnsiString strPop="";
  char sqlbuf[256];

  if (ADOConnection1->Connected == false)
    return strPop;
  if (g_nTranListFlag == 0)
    sprintf(sqlbuf, "select CallInPopURL from tbTranList where Id=%d", popid);
  else
    sprintf(sqlbuf, "select CallInPopURL from callcenterdb.dbo.tbCcTranList where Id=%d", popid);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf);
    adoqueryPub->Open();
    if (!adoqueryPub->Eof)
      strPop = adoqueryPub->FieldByName("CallInPopURL")->AsString;
    adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  WriteErrprMsg("GetPopString sqlbuf=%s strPop=%s", sqlbuf, strPop);
  return strPop;
}

void __fastcall TdmAgent::adoQryRecvEmailCalcFields(TDataSet *DataSet)
{
  switch (adoQryRecvEmailProcFlag->AsInteger)
  {
  case 0:
    adoQryRecvEmailDispProcFlag->AsString = "未處理";
    break;
  case 1:
    adoQryRecvEmailDispProcFlag->AsString = "等待處理";
    break;
  case 2:
    adoQryRecvEmailDispProcFlag->AsString = "正在處理";
    break;
  case 3:
    adoQryRecvEmailDispProcFlag->AsString = "處理完成";
    break;
  }
  switch (adoQryRecvEmailTranEmailId->AsInteger)
  {
  case 0: adoQryRecvEmailDispTranEmailId->AsString = "不轉發"; break;
  case 1: adoQryRecvEmailDispTranEmailId->AsString = "等待轉發"; break;
  case 2: adoQryRecvEmailDispTranEmailId->AsString = "正在轉發"; break;
  case 3: adoQryRecvEmailDispTranEmailId->AsString = "轉發成功"; break;
  case 4: adoQryRecvEmailDispTranEmailId->AsString = "轉發失敗"; break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TdmAgent::adotbWorkerCalcFields(TDataSet *DataSet)
{
  if (adotbWorkerAutoAnswerId->AsInteger == 1)
    adotbWorkerDispAutoAnswerId->AsString = "是";
  else
    adotbWorkerDispAutoAnswerId->AsString = "否";

  if (adotbWorkerProcDialoutNextType->AsInteger == 1)
    adotbWorkerDispProcDialoutNextType->AsString = "是";
  else
    adotbWorkerDispProcDialoutNextType->AsString = "否";
}
//---------------------------------------------------------------------------
void TdmAgent::AddGroupNameCmbItemByWorkerNo(TComboBox *combobox, AnsiString strWorkerNo)
{
    char sqlbuf[256];
    int nDepartmentID=0;
    AnsiString itemname;

    if (ADOConnection1->Connected == false)
      return;

    sprintf(sqlbuf, "Select DepartmentID from tbworker where WorkerNo='%s' order by GroupNo", strWorkerNo.c_str());
    try
    {
      adoqueryPub->SQL->Clear();
      adoqueryPub->SQL->Add((char *)sqlbuf);
      adoqueryPub->Open();
      if ( !adoqueryPub->Eof )
      {
        nDepartmentID = adoqueryPub->FieldByName("DepartmentID")->AsInteger;
      }
    }
    catch ( ... )
    {
    }
    adoqueryPub->Close();

    combobox->Items->Clear();
    combobox->Items->Add("不選擇");

    if (nDepartmentID > 0)
      sprintf(sqlbuf, "Select GroupName from tbworkergroup where DepartmentID=0 or DepartmentID=%d order by GroupNo", nDepartmentID);
    else
      sprintf(sqlbuf, "Select GroupName from tbworkergroup order by GroupNo");
    try
    {
        adoqueryPub->SQL->Clear();
        adoqueryPub->SQL->Add((char *)sqlbuf);
        adoqueryPub->Open();
    }
    catch ( ... )
    {
        return;
    }
    while ( !adoqueryPub->Eof )
    {
        itemname = adoqueryPub->FieldByName("GroupName")->AsString;
        combobox->Items->Add(itemname);
        adoqueryPub->Next();
    }
    adoqueryPub->Close();
}

void __fastcall TdmAgent::adoQrySearchCustCalcFields(TDataSet *DataSet)
{
  adoQrySearchCustCustTypeName->AsString = CustTypeItemList.GetItemName(adoQrySearchCustCustType->AsInteger);
  adoQrySearchCustCustLevelName->AsString = CustLevelItemList.GetItemName(adoQrySearchCustCustLevel->AsInteger);
  for (int i=0; i<FormMain->m_CustExDispFieldList.FieldNum; i++)
  {
    int nFieldId = FormMain->m_CustExDispFieldList.m_CustExDispField[i].FieldId;
    if (nFieldId >=1 && nFieldId <= 32)
    {
      if (FormMain->m_CustExDispFieldList.m_CustExDispField[i].ControlType == 2)
        m_pCustExDispFieldSech[nFieldId-1]->AsString = FormMain->m_pCustExItemSetList->m_CustExItemSet[nFieldId-1].TLookupItemList.GetItemName(MyStrToInt(m_pCustExFieldSech[nFieldId-1]->AsString));
      else if (FormMain->m_CustExDispFieldList.m_CustExDispField[i].ControlType == 8)
        m_pCustExDispFieldSech[nFieldId-1]->AsString = YesNoItemList.GetItemName(MyStrToInt(m_pCustExFieldSech[nFieldId-1]->AsString));
    }
  }
  adoQrySearchCustDispMobileNo->AsString = GetHidePhoneNum(2, adoQrySearchCustMobileNo->AsString);
  adoQrySearchCustDispTeleNo->AsString = GetHidePhoneNum(2, adoQrySearchCustTeleNo->AsString);
  adoQrySearchCustDispFaxNo->AsString = GetHidePhoneNum(2, adoQrySearchCustFaxNo->AsString);
}
//---------------------------------------------------------------------------
int TdmAgent::QueryWebURLAddr(int nSelectTable, AnsiString &strURLName, AnsiString &strURLAddr, int &nSortType)
{
  char sqlbuf[256];
  int nResult=0;

  strURLName = "";
  strURLAddr = "";
  sprintf( sqlbuf, "Select URLName,URLAddr,SortType from tbURLAddrSet where URLId=%d", nSelectTable);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->Open();

    if ( !dmAgent->adoqueryPub->Eof )
    {
      strURLName = dmAgent->adoqueryPub->FieldByName("URLName")->AsString;
      strURLAddr = dmAgent->adoqueryPub->FieldByName("URLAddr")->AsString;
      nSortType = dmAgent->adoqueryPub->FieldByName("SortType")->AsInteger;
      nResult = 1;
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
  return nResult;
}
int TdmAgent::InsertWebURLAddr(int nSelectTable, AnsiString strURLName, AnsiString strURLAddr, int nSortType)
{
  char sqlbuf[512];

  if (ADOConnection1->Connected == false)
    return 1;

  sprintf( sqlbuf, "insert into tbURLAddrSet (URLId,URLName,URLAddr,SortType) values (%d,'%s','%s',%d)",
    nSelectTable, strURLName.c_str(), strURLAddr.c_str(), nSortType);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}
int TdmAgent::UpdateWebURLAddr(int nSelectTable, AnsiString strURLName, AnsiString strURLAddr, int nSortType)
{
  char sqlbuf[512];

  sprintf( sqlbuf, "update tbURLAddrSet set URLName='%s',URLAddr='%s',SortType=%d where URLId=%d",
    strURLName.c_str(), strURLAddr.c_str(), nSortType, nSelectTable);
  try
  {
    adoqueryPub->SQL->Clear();
    adoqueryPub->SQL->Add((char *)sqlbuf );
    adoqueryPub->ExecSQL();
    return 0;
  }
  catch ( ... )
  {
    return 1;
  }
}

