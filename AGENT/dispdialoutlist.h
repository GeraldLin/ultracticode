//---------------------------------------------------------------------------

#ifndef dispdialoutlistH
#define dispdialoutlistH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "DataModule.h"
#include <ExtCtrls.hpp>
#include "CSPIN.h"
#include <Menus.hpp>
#include <OleCtrls.hpp>
#include "WMPLib_OCX.h"
//---------------------------------------------------------------------------
class TFormDispDialOutList : public TForm
{
__published:	// IDE-managed Components
  TDBGrid *DBGrid1;
  TPanel *Panel1;
  TGroupBox *GroupBox1;
  TPanel *Panel2;
  TButton *Button1;
  TMemo *Memo1;
  TLabel *Label239;
  TLabel *lblDLSTRecdcount;
  TLabel *lblDLSTPages;
  TLabel *lblDLSTLocaPage;
  TLabel *Label246;
  TButton *btnDLSTPrior;
  TButton *btnDLSTNext;
  TButton *btnDLSTGo;
  TCSpinEdit *cseDLSTLocaPage;
  TButton *Button2;
  TPopupMenu *PopupMenu1;
  TMenuItem *N1;
  TMenuItem *N2;
  TWindowsMediaPlayer *WindowsMediaPlayer1;
  TButton *Button3;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall DBGrid1CellClick(TColumn *Column);
  void __fastcall btnDLSTPriorClick(TObject *Sender);
  void __fastcall btnDLSTNextClick(TObject *Sender);
  void __fastcall btnDLSTGoClick(TObject *Sender);
  void __fastcall Memo1Change(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall N1Click(TObject *Sender);
  void __fastcall N2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDispDialOutList(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDispDialOutList *FormDispDialOutList;
//---------------------------------------------------------------------------
#endif
