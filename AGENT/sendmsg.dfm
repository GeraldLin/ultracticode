object FormSendMsg: TFormSendMsg
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #22352#24109#20043#38291#30332#36865#35338#24687
  ClientHeight = 160
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 286
    Top = 25
    Width = 60
    Height = 12
    Caption = #22352#24109#32232#34399#65306
    Visible = False
  end
  object Label2: TLabel
    Left = 9
    Top = 91
    Width = 60
    Height = 12
    Caption = #25991#23383#35338#24687#65306
  end
  object Label3: TLabel
    Left = 286
    Top = 47
    Width = 60
    Height = 12
    Caption = #22519#27231#32232#34399#65306
  end
  object cbSeatNo: TComboBox
    Left = 355
    Top = 22
    Width = 105
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    Visible = False
  end
  object editSendMsg: TEdit
    Left = 78
    Top = 87
    Width = 434
    Height = 20
    MaxLength = 250
    TabOrder = 1
  end
  object Button1: TButton
    Left = 121
    Top = 121
    Width = 82
    Height = 27
    Caption = #30332#36865
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 321
    Top = 121
    Width = 81
    Height = 27
    Caption = #38364#38281
    TabOrder = 3
    OnClick = Button2Click
  end
  object RadioGroup1: TRadioGroup
    Left = 69
    Top = 17
    Width = 209
    Height = 50
    Caption = #30332#36865#26041#24335#36984#25799#65306
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      #25353#20998#27231#34399#30908
      #25353#22519#27231#32232#34399)
    TabOrder = 4
    OnClick = RadioGroup1Click
  end
  object cbWorkerNo: TComboBox
    Left = 355
    Top = 43
    Width = 105
    Height = 20
    ItemHeight = 12
    TabOrder = 5
  end
end
