object FormAllocDailOut: TFormAllocDailOut
  Left = 405
  Top = 162
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24179#22343#20998#37197#20154#24037#22806#25765#20219#21209#35373#23450
  ClientHeight = 539
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 206
    Top = 13
    Width = 72
    Height = 12
    Caption = #36984#25799#20540#27231#21729#65306
  end
  object Label2: TLabel
    Left = 6
    Top = 13
    Width = 60
    Height = 12
    Caption = #36984#25799#32676#32068#65306
  end
  object Label3: TLabel
    Left = 153
    Top = 503
    Width = 99
    Height = 12
    Caption = #27599#20491#36984#23450#30340#35373#23450#25976':'
  end
  object Label4: TLabel
    Left = 27
    Top = 460
    Width = 51
    Height = 12
    Caption = #32317#22806#25765#25976':'
  end
  object Button1: TButton
    Left = 453
    Top = 495
    Width = 82
    Height = 27
    Caption = #38283#22987#20998#37197
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 588
    Top = 495
    Width = 81
    Height = 27
    Caption = #36864#20986
    TabOrder = 1
    OnClick = Button2Click
  end
  object ListView1: TListView
    Left = 201
    Top = 35
    Width = 496
    Height = 361
    Checkboxes = True
    Columns = <
      item
        Caption = #24207#34399
        Width = 45
      end
      item
        Caption = #22519#27231#32232#34399
        Width = 65
      end
      item
        Caption = #22995#21517
        Width = 87
      end
      item
        Caption = #35441#21209#32068
        Width = 140
      end
      item
        Caption = #24453#20998#37197#32317#25976
        Width = 75
      end
      item
        Caption = #24050#20998#37197#32317#25976
        Width = 75
      end>
    GridLines = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
    OnDblClick = ListView1DblClick
    OnSelectItem = ListView1SelectItem
  end
  object Panel1: TPanel
    Left = 16
    Top = 408
    Width = 665
    Height = 33
    BevelOuter = bvNone
    TabOrder = 3
    object CheckBox1: TCheckBox
      Left = 9
      Top = 4
      Width = 183
      Height = 18
      Caption = #24050#20998#37197#34399#30908#26159#21542#37325#26032#20998#37197#65311
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 240
      Top = 4
      Width = 193
      Height = 18
      Caption = #24050#22806#21628#25104#21151#35352#37636#26159#21542#37325#26032#20998#37197#65311
      TabOrder = 1
      OnClick = CheckBox1Click
    end
    object CheckBox3: TCheckBox
      Left = 481
      Top = 4
      Width = 174
      Height = 18
      Caption = #24050#25104#20132#34399#30908#26159#21542#37325#26032#20998#37197#65311
      TabOrder = 2
      OnClick = CheckBox1Click
    end
  end
  object Panel2: TPanel
    Left = 280
    Top = 208
    Width = 337
    Height = 41
    Caption = #27491#22312#20998#37197#22806#25765#34399#30908'(0/0'#26781')'#65292#35531#31245#31561'......'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = False
  end
  object CheckBox5: TCheckBox
    Left = 644
    Top = 8
    Width = 53
    Height = 17
    Caption = #20840#36984'?'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = CheckBox5Click
  end
  object Button3: TButton
    Left = 580
    Top = 6
    Width = 54
    Height = 20
    Caption = #21453#36984
    TabOrder = 6
    OnClick = Button3Click
  end
  object ListView2: TListView
    Left = 8
    Top = 35
    Width = 180
    Height = 294
    Checkboxes = True
    Columns = <
      item
        Caption = #32676#32068#32232#34399
        Width = 60
      end
      item
        Caption = #32676#32068#21517#31281
        Width = 115
      end>
    GridLines = True
    ReadOnly = True
    TabOrder = 7
    ViewStyle = vsReport
    OnChange = ListView2Change
  end
  object Button4: TButton
    Left = 8
    Top = 336
    Width = 180
    Height = 25
    Caption = #36984#25799#21246#36984#32676#32068#30340#20540#27231#21729
    ParentShowHint = False
    ShowHint = False
    TabOrder = 8
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 8
    Top = 368
    Width = 180
    Height = 25
    Caption = #36984#25799#25152#26377#32676#32068#30340#20540#27231#21729
    TabOrder = 9
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 74
    Top = 6
    Width = 54
    Height = 20
    Caption = #21453#36984
    TabOrder = 10
    OnClick = Button6Click
  end
  object CheckBox6: TCheckBox
    Left = 136
    Top = 8
    Width = 53
    Height = 17
    Caption = #20840#36984'?'
    Checked = True
    State = cbChecked
    TabOrder = 11
    OnClick = CheckBox6Click
  end
  object Button7: TButton
    Left = 48
    Top = 496
    Width = 75
    Height = 25
    Caption = #24179#22343#20998#37197
    TabOrder = 12
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 336
    Top = 496
    Width = 90
    Height = 25
    Caption = #25353#35373#23450#25976#20998#37197
    TabOrder = 13
    OnClick = Button8Click
  end
  object Edit1: TEdit
    Left = 256
    Top = 499
    Width = 75
    Height = 20
    TabOrder = 14
    Text = '0'
  end
  object Edit2: TEdit
    Left = 88
    Top = 456
    Width = 121
    Height = 20
    Color = clSilver
    ReadOnly = True
    TabOrder = 15
    Text = '0'
  end
end
