//---------------------------------------------------------------------------

#ifndef editdispitemselH
#define editdispitemselH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditDispItemSel : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editItemName;
  TCheckBox *CheckBox1;
  TCheckBox *CheckBox2;
  TCheckBox *CheckBox3;
  TCheckBox *CheckBox4;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label2;
  TCSpinEdit *cseExportOrderId;
  TLabel *Label3;
  TCSpinEdit *cseAcceptExId;
  TLabel *Label9;
  TComboBox *cbReadDataType;
  TLabel *Label10;
  TEdit *editReadFieldTag;
  TCheckBox *ckSaveToDBFlag;
  void __fastcall CheckBox1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbReadDataTypeKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditDispItemSel(TComponent* Owner);

  int nItemType;
  int nItemId;
  int nMustInput;

  void SetCheckBoxVisible(int itemtype, int mustinput=0);
  void SetCheckBoxValue(AnsiString strItemName, int itemid, int nCkValue1, int nCkValue2, int nCkValue3, int nCkValue4, int nExportOrderId, int nAcceptExId);
  void SetCRMSetValue(int nReadDataType, AnsiString strReadFieldTag, int nSaveToDBFlag);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditDispItemSel *FormEditDispItemSel;
//---------------------------------------------------------------------------
#endif
