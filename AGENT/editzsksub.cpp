//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editzsksub.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormZSKSub *FormZSKSub;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormZSKSub::TFormZSKSub(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormZSKSub::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormZSKSub->Caption = "新增業務知識庫子分類訊息";
  }
  else
  {
    Button1->Caption = "修改";
    FormZSKSub->Caption = "修改業務知識庫子分類訊息";
  }
}
void TFormZSKSub::SetZSKSubData(CZSKSub& zsksub)
{
  ZSKSub = zsksub;
  editMainTitle->Text = zsksub.MainTitle;
  editSubTitle->Text = zsksub.SubTitle;
  memoExplain->Lines->Clear();
  memoExplain->Lines->Add(zsksub.Explain);
  editRemark->Text = zsksub.Remark;
  cseOrderNo->Value = zsksub.orderno2;
  Button1->Enabled = false;
}
int TFormZSKSub::GetZSKSubData()
{
  //ZSKSub.SubNo;
  //ZSKSub.MainNo;
  ZSKSub.MainTitle = editMainTitle->Text;
  if (editSubTitle->Text == "")
  {
    MessageBox(NULL,"請輸入子分類標題！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  ZSKSub.SubTitle = editSubTitle->Text;
  ZSKSub.Explain = memoExplain->Lines->Text;
  ZSKSub.Remark = editRemark->Text;
  ZSKSub.orderno2 = cseOrderNo->Value;
  return 0;
}
void TFormZSKSub::ClearZSKSubData(CZSKMain& zskmain)
{
  ZSKSub.SubNo = 0;
  ZSKSub.MainNo = zskmain.MainNo;
  editMainTitle->Text = zskmain.MainTitle;
  editSubTitle->Text = "";
  memoExplain->Lines->Clear();
  editRemark->Text = "";
  Button1->Enabled = false;
}

void __fastcall TFormZSKSub::cbIssueStatusKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSub::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSub::editSubTitleChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSub::Button1Click(TObject *Sender)
{
  if (GetZSKSubData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertZSKSubRecord(ZSKSub) == 0)
    {
      dmAgent->RefreshZSKSub();
      dmAgent->InsertOPLog(MySeat, MyWorker, 1030, "值機員新增業務知識庫為：%s的子分類訊息資料", ZSKSub.SubTitle.c_str());
      //MessageBox(NULL,"新增業務知識庫子分類訊息成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增業務知識庫子分類訊息失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateZSKSubRecord(ZSKSub) == 0)
    {
      dmAgent->RefreshZSKSub();
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員修改業務知識庫為：%s的子分類訊息資料", ZSKSub.SubTitle.c_str());
      //MessageBox(NULL,"修改業務知識庫子分類訊息成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改業務知識庫子分類訊息失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSub::FormShow(TObject *Sender)
{
  FormZSKSub->editSubTitle->SetFocus();  
}
//---------------------------------------------------------------------------

