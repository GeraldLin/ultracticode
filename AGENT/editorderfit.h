//---------------------------------------------------------------------------

#ifndef editorderfitH
#define editorderfitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormEditOrderFit : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TEdit *editPreFitDate;
  TEdit *editFactFitDate;
  TEdit *editFitSrvCorp;
  TEdit *editFitMen;
  TEdit *editFitMenTel;
  TComboBox *cbFitStatus;
  TMemo *memoFitNote;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label10;
  TEdit *editOrderId;
  TMonthCalendar *MonthCalendar1;
  TButton *Button3;
  TButton *Button4;
  void __fastcall cbFitStatusKeyPress(TObject *Sender, char &Key);
  void __fastcall editPreFitDateChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button4Click(TObject *Sender);
  void __fastcall MonthCalendar1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditOrderFit(TComponent* Owner);

  int GetDateId;
  COrders Orders;
  void SetOrderFitData(COrders& orders);
  int GetOrderFitData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditOrderFit *FormEditOrderFit;
//---------------------------------------------------------------------------
#endif
