//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editblack.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditBlack *FormEditBlack;
extern CMySeat MySeat;
extern CMyWorker MyWorker;
extern AnsiString MyNowFuncName;
extern AnsiString strDBType;
//---------------------------------------------------------------------------
__fastcall TFormEditBlack::TFormEditBlack(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormEditBlack::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditBlack::editPhoneChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditBlack::Button1Click(TObject *Sender)
{
  char sqlbuf[256];
  char dispbuf[256];
  int nBlackType;

  if (editPhone->Text == "")
  {
    MessageBox(NULL,"請輸入電話號碼！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (MyIsDigits(editPhone->Text) != 0)
  {
    MessageBox(NULL,"輸入的電話號碼有誤！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (dmAgent->IsBlackExist(editPhone->Text) == 1)
  {
    sprintf(dispbuf, "電話號碼 %s 已經存在!", editPhone->Text.c_str());
    MessageBox(NULL,dispbuf,"訊息提示",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (cbBlackType->Text == "白名單")
    nBlackType = 0;
  else
    nBlackType = 1;

  if (strDBType == "SQLSERVER")
    sprintf( sqlbuf, "insert into tbBlack (TeleCode,OpenFlag,AddWorkerNo,EndTime,Remark) values ('%s',%d,'%d',(dateadd(d,%d,%s)),'%s')",
      editPhone->Text.c_str(), nBlackType, MyWorker.WorkerNo, cseRestDays->Value, MyNowFuncName.c_str(), editRemark->Text.c_str());
  else if (strDBType == "MYSQL")
    sprintf( sqlbuf, "insert into tbBlack (TeleCode,OpenFlag,AddWorkerNo,AddTime,EndTime,Remark) values ('%s',%d,'%d',%s,(ADDDATE(%s,interval %d day)),'%s')",
      editPhone->Text.c_str(), nBlackType, MyWorker.WorkerNo, MyNowFuncName.c_str(), MyNowFuncName.c_str(), cseRestDays->Value, editRemark->Text.c_str());
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    dmAgent->adoQryBlack->Close();
    dmAgent->adoQryBlack->Open();
    dmAgent->InsertOPLog(MySeat, MyWorker, 1030, "值機員新增黑白名單號碼%s的資料", editPhone->Text.c_str());
    this->Close();
    //MessageBox(NULL,"黑白名單新增成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
  catch ( ... )
  {
    MessageBox(NULL,"黑白名單新增失敗！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditBlack::FormShow(TObject *Sender)
{
  FormEditBlack->editPhone->SetFocus();  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditBlack::cbBlackTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

