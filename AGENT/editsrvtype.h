//---------------------------------------------------------------------------

#ifndef editsrvtypeH
#define editsrvtypeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditSrvType : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editSrvName;
  TButton *Button1;
  TButton *Button2;
  TEdit *editSrvType;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editItemURL;
  TMemo *memoDemo;
  TLabel *Label7;
  TCSpinEdit *cseDispOrder;
  TLabel *Label8;
  TEdit *editURLBtnName;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSrvTypeChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSrvType(TComponent* Owner);

  int ProdType;
  bool InsertId; //true-表示是增加记录 false-表示囊改记录
  void SetDispMsg(bool bInsertId, int nProdType, int nSrvType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSrvType *FormEditSrvType;
//---------------------------------------------------------------------------
#endif
