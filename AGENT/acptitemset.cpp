//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "acptitemset.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormAcptItemSet *FormAcptItemSet;

extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormAcptItemSet::TFormAcptItemSet(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormAcptItemSet->cbControlType, dmAgent->ControlTypeItemList, "");
  }
  m_nSrvType = -1;
}
//---------------------------------------------------------------------------
void TFormAcptItemSet::SetSrvType(int prodtype, int srvtype, int srvsubtype)
{
  m_nProdType = prodtype;
  m_nSrvType = srvtype;
  m_nSrvSubType = srvsubtype;
}
void TFormAcptItemSet::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    editTid->ReadOnly = false;
    editTid->Color = clWindow;
    FormAcptItemSet->Caption = "新增業務受理定制項輸入框參數";
  }
  else
  {
    Button1->Caption = "修改";
    editTid->ReadOnly = true;
    editTid->Color = clSilver;
    FormAcptItemSet->Caption = "修改業務受理定制項輸入框參數";
  }
}
void TFormAcptItemSet::SetAcptItemSetData(CAcptItemSet& acptitemset)
{
  AcptItemSet = acptitemset;
  m_nProdType = acptitemset.ProdType;
  m_nSrvType = acptitemset.SrvType;
  m_nSrvSubType = acptitemset.SrvSubType;

  editTid->Text = AcptItemSet.Tid;
  cbControlType->Text = dmAgent->ControlTypeItemList.GetItemName(AcptItemSet.TControlType);
  editLabelName->Text = AcptItemSet.TLabelCaption;
  editDefaultData->Text = AcptItemSet.TDefaultData;
  cseTMinValue->Value = AcptItemSet.TMinValue;
  cseTMaxValue->Value = AcptItemSet.TMaxValue;
  ckIsAllowNull->Checked = (AcptItemSet.TIsAllowNull == 0) ? false : true;
  ckExportId->Checked = (AcptItemSet.TExportId == 0) ? false : true;
  if (cbControlType->Text == "短文本框" || cbControlType->Text == "長文本框")
  {
    Label7->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormAcptItemSet->Height = 360;
  }
  else if (cbControlType->Text == "規定格式輸入框")
  {
    Label7->Visible = true;
    editMaskEdit->Visible = true;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormAcptItemSet->Height = 570;
  }
  else
  {
    Label7->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小值：";
    Label5->Caption = "最大值：";
    FormAcptItemSet->Height = 360;
  }
  //editTLinkId->Text = AcptItemSet.TLinkId;
  editMaskEdit->Text = AcptItemSet.TMaskEditStr;
  editHintStr->Text = AcptItemSet.THintStr;

  if (AcptItemSet.CombineMode == 0)
    cbCombineMode->Text = "不合并";
  else if (AcptItemSet.CombineMode == 1)
    cbCombineMode->Text = "替換";
  else if (AcptItemSet.CombineMode == 2)
    cbCombineMode->Text = "合并";
  cseCombineTid->Value = AcptItemSet.CombineTid;
  editCombineValue->Text = AcptItemSet.CombineValue;
  cseExportOrderId->Value = AcptItemSet.ExportOrderId;
  switch (AcptItemSet.TWith)
  {
    case 0: cbWith->Text = "標准寬度"; break;
    case 1: cbWith->Text = "標准寬度四分之一"; break;
    case 2: cbWith->Text = "標准寬度二分之一"; break;
    case 3: cbWith->Text = "標准寬度四分之三"; break;
    default: cbWith->Text = "標准寬度"; break;
  }
}
int TFormAcptItemSet::GetAcptItemSetData()
{
  if (MyIsNumber(editTid->Text) != 0)
  {
    MessageBox(NULL,"請輸入受理框序號！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  int tid = StrToInt(editTid->Text);
  if (tid < 1 || tid > MAX_ACPT_CONTROL_NUM)
  {
    MessageBox(NULL,"對不起，受理框序號取值範圍為1-32","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (editLabelName->Text == "")
  {
    MessageBox(NULL,"請輸入受理項名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  AcptItemSet.ProdType = m_nProdType;
  AcptItemSet.SrvType = m_nSrvType;
  AcptItemSet.SrvSubType = m_nSrvSubType;
  AcptItemSet.Tid = tid;
  AcptItemSet.TFieldName = "ItemData"+IntToStr(tid);
  AcptItemSet.TControlType = dmAgent->ControlTypeItemList.GetItemValue(cbControlType->Text);
  AcptItemSet.TLabelCaption = editLabelName->Text;

  AcptItemSet.TDefaultData = editDefaultData->Text;
  if (AcptItemSet.TControlType == 2)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsNumber(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，選項框預設值必須為整數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      if (cseTMinValue->Value != 0 && cseTMaxValue->Value != 0 &&
        (StrToInt(editDefaultData->Text) < cseTMinValue->Value || StrToInt(editDefaultData->Text) > cseTMaxValue->Value))
      {
        MessageBox(NULL,"對不起，選項框預設值不在設置的最小最大值範圍內","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      AcptItemSet.TDefaultData = IntToStr(cseTMinValue->Value);
    }
  }
  else if (AcptItemSet.TControlType == 5)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsNumber(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，數值框預設值必須為整數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      if (cseTMinValue->Value != 0 && cseTMaxValue->Value != 0 &&
        (StrToInt(editDefaultData->Text) < cseTMinValue->Value || StrToInt(editDefaultData->Text) > cseTMaxValue->Value))
      {
        MessageBox(NULL,"對不起，數值框預設值不在設置的最小最大值範圍內","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      AcptItemSet.TDefaultData = IntToStr(cseTMinValue->Value);
    }
  }
  else if (AcptItemSet.TControlType == 6)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsFloat(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，金額框預設值必須為整數或小數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      AcptItemSet.TDefaultData = "0";
    }
  }
  else if (AcptItemSet.TControlType == 7)
  {
    if (editDefaultData->Text != "" && MyIsDate(editDefaultData->Text) != 0)
    {
      MessageBox(NULL,"對不起，日期類型預設值必須為正確的日期","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
  }
  else if (AcptItemSet.TControlType == 8)
  {
    if (editDefaultData->Text != "")
    {
      if (editDefaultData->Text != "1" && editDefaultData->Text != "0")
      {
        MessageBox(NULL,"對不起，是非類型預設值必須為0或1","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      AcptItemSet.TDefaultData = "1";
    }
  }

  AcptItemSet.TMinValue = cseTMinValue->Value;
  AcptItemSet.TMaxValue = cseTMaxValue->Value;

  (ckIsAllowNull->Checked == true) ? AcptItemSet.TIsAllowNull = 1 : AcptItemSet.TIsAllowNull = 0;
  (ckExportId->Checked == true) ? AcptItemSet.TExportId = 1 : AcptItemSet.TExportId = 0;

  //AcptItemSet.TLinkId = editTLinkId->Text;
  AcptItemSet.TMaskEditStr = editMaskEdit->Text;
  AcptItemSet.THintStr = editHintStr->Text;

  if (cbCombineMode->Text == "替換")
    AcptItemSet.CombineMode = 1;
  else if (cbCombineMode->Text == "合并")
    AcptItemSet.CombineMode = 2;
  else
    AcptItemSet.CombineMode = 0;
  AcptItemSet.CombineTid = cseCombineTid->Value;
  AcptItemSet.CombineValue = editCombineValue->Text;
  AcptItemSet.ExportOrderId = cseExportOrderId->Value;

  if (cbWith->Text == "標准寬度")
    AcptItemSet.TWith = 0;
  else if (cbWith->Text == "標准寬度四分之一")
    AcptItemSet.TWith = 1;
  else if (cbWith->Text == "標准寬度二分之一")
    AcptItemSet.TWith = 2;
  else if (cbWith->Text == "標准寬度四分之三")
    AcptItemSet.TWith = 3;
  return 0;
}
void TFormAcptItemSet::ClearAcptItemSetData()
{
  editTid->Text = "";
  cbControlType->Text = dmAgent->ControlTypeItemList.GetItemName(1);
  editLabelName->Text = "";
  editDefaultData->Text = "";
  cseTMinValue->Value = 0;
  cseTMaxValue->Value = 0;
  //editTLinkId->Text = "";
  editMaskEdit->Text = "";
  editHintStr->Text = "";
  cbCombineMode->Text = "不合并";
  cseCombineTid->Value = 0;
  editCombineValue->Text = "";
  cseExportOrderId->Value = 0;
  cbWith->Text = "標准寬度";

  ckIsAllowNull->Checked = true;
  FormAcptItemSet->Height = 360;
}
//---------------------------------------------------------------------------
void __fastcall TFormAcptItemSet::cbControlTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormAcptItemSet::Button2Click(TObject *Sender)
{
  if (InsertId == false)
    ClearAcptItemSetData();
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormAcptItemSet::editTidChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormAcptItemSet::Button1Click(TObject *Sender)
{
  CAcptItemSet acptitemset;
  if (GetAcptItemSetData() != 0)
  {
    return;
  }
  if (InsertId == true)
  {
    if (AcptItemSet.TControlType == 10 && dmAgent->IsLinkItemExist(AcptItemSet.ProdType, AcptItemSet.SrvType, AcptItemSet.SrvSubType) == 1)
    {
      MessageBox(NULL,"對不起，最多只能創建一個關聯文本下拉框！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->IsAcptItemSetTidExist(AcptItemSet.ProdType, AcptItemSet.SrvType, AcptItemSet.SrvSubType, AcptItemSet.Tid) == 1)
    {
      MessageBox(NULL,"對不起，該業務受理框序號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertAcptItemSet(AcptItemSet) == 0)
    {
      dmAgent->adoQryAcptItemSet->Close();
      dmAgent->adoQryAcptItemSet->Open();
      if (dmAgent->GetAcptItemSetRecord(dmAgent->adoQryAcptItemSet, &acptitemset) == 0)
      {
        FormMain->QueryCurAcptItemList(acptitemset);
      }
      else
      {
        FormMain->gbAcptItemList->Visible = false;
        dmAgent->adoQryAcptItemList->Close();
      }
      ClearAcptItemSetData();
      this->Close();
      //MessageBox(NULL,"新增業務受理框成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增業務受理框失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (AcptItemSet.TControlType == 10 && dmAgent->IsLinkItemExist(AcptItemSet.ProdType, AcptItemSet.SrvType, AcptItemSet.SrvSubType, AcptItemSet.Tid) == 1)
    {
      MessageBox(NULL,"對不起，最多只能創建一個關聯文本下拉框！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->UpdateAcptItemSet(AcptItemSet) == 0)
    {
      dmAgent->adoQryAcptItemSet->Close();
      dmAgent->adoQryAcptItemSet->Open();
      if (dmAgent->GetAcptItemSetRecord(dmAgent->adoQryAcptItemSet, &acptitemset) == 0)
      {
        FormMain->QueryCurAcptItemList(acptitemset);
      }
      else
      {
        FormMain->gbAcptItemList->Visible = false;
        dmAgent->adoQryAcptItemList->Close();
      }
      ClearAcptItemSetData();
      this->Close();
      //MessageBox(NULL,"業務受理框修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"業務受理框修改失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAcptItemSet::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormAcptItemSet->editTid->SetFocus();
  else
    FormAcptItemSet->editLabelName->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormAcptItemSet::cbControlTypeChange(TObject *Sender)
{
  Button1->Enabled = true;
  if (cbControlType->Text == "短文本框" || cbControlType->Text == "長文本框")
  {
    Label7->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormAcptItemSet->Height = 360;
  }
  else if (cbControlType->Text == "規定格式輸入框")
  {
    Label7->Visible = true;
    editMaskEdit->Visible = true;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormAcptItemSet->Height = 570;
  }
  else
  {
    Label7->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小值：";
    Label5->Caption = "最大值：";
    FormAcptItemSet->Height = 360;
  }
}
//---------------------------------------------------------------------------

