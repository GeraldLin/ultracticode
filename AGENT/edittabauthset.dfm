object FormTabAuthSet: TFormTabAuthSet
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #30028#38754#25805#20316#27402#38480#35373#23450
  ClientHeight = 232
  ClientWidth = 277
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 22
    Width = 84
    Height = 13
    Caption = #25805#20316#38913#38754#21517#31281#65306
  end
  object editTabName: TEdit
    Left = 113
    Top = 17
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object ckAdmin: TCheckBox
    Left = 43
    Top = 52
    Width = 166
    Height = 18
    Caption = #20801#35377#31995#32113#31649#29702#21729#25805#20316#65311
    TabOrder = 1
    OnClick = ckAdminClick
  end
  object ckMaint: TCheckBox
    Left = 43
    Top = 78
    Width = 166
    Height = 18
    Caption = #20801#35377#37096#38272#31649#29702#21729#25805#20316#65311
    TabOrder = 2
    OnClick = ckAdminClick
  end
  object ckMonit: TCheckBox
    Left = 43
    Top = 104
    Width = 166
    Height = 18
    Caption = #20801#35377#29677#38263#22519#27231#21729#25805#20316#65311
    TabOrder = 3
    OnClick = ckAdminClick
  end
  object ckOpert: TCheckBox
    Left = 43
    Top = 130
    Width = 166
    Height = 18
    Caption = #20801#35377#26222#36890#32887#21729#25805#20316#65311
    TabOrder = 4
    OnClick = ckAdminClick
  end
  object ckAgent: TCheckBox
    Left = 43
    Top = 156
    Width = 166
    Height = 18
    Caption = #20801#35377#26222#36890#22519#27231#21729#25805#20316#65311
    TabOrder = 5
    OnClick = ckAdminClick
  end
  object Button1: TButton
    Left = 43
    Top = 191
    Width = 82
    Height = 27
    Caption = #20462#25913
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 165
    Top = 191
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 7
    OnClick = Button2Click
  end
end
