object FormPrdDetail: TFormPrdDetail
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20135#21697#35814#32454#36164#26009#20171#32461
  ClientHeight = 389
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 57
    Top = 167
    Width = 37
    Height = 13
    AutoSize = False
    Caption = #25551#36848#65306
  end
  object Label5: TLabel
    Left = 56
    Top = 323
    Width = 37
    Height = 13
    AutoSize = False
    Caption = #22270#29255#65306
  end
  object Label1: TLabel
    Left = 31
    Top = 12
    Width = 60
    Height = 13
    Caption = #21830#21697#32534#21495#65306
  end
  object Label2: TLabel
    Left = 239
    Top = 12
    Width = 60
    Height = 13
    Caption = #21830#21697#21517#31216#65306
  end
  object Label3: TLabel
    Left = 27
    Top = 36
    Width = 60
    Height = 13
    Caption = #21830#21697#22411#21495#65306
  end
  object Label6: TLabel
    Left = 267
    Top = 36
    Width = 36
    Height = 13
    Caption = #21697#29260#65306
  end
  object Label7: TLabel
    Left = 51
    Top = 140
    Width = 36
    Height = 13
    Caption = #31616#20171#65306
  end
  object Label14: TLabel
    Left = 19
    Top = 60
    Width = 72
    Height = 13
    Caption = #20379#24212#21830#21517#31216#65306
  end
  object Label15: TLabel
    Left = 227
    Top = 60
    Width = 72
    Height = 13
    Caption = #20379#24212#21830#21495#30721#65306
  end
  object Label16: TLabel
    Left = 3
    Top = 84
    Width = 84
    Height = 13
    Caption = #20379#24212#21830#32852#31995#20154#65306
  end
  object Label17: TLabel
    Left = 19
    Top = 108
    Width = 72
    Height = 13
    Caption = #20379#24212#21830#22320#22336#65306
  end
  object memoDescription: TMemo
    Left = 96
    Top = 168
    Width = 337
    Height = 145
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object editInFocusImageUrl: TEdit
    Left = 96
    Top = 320
    Width = 313
    Height = 21
    ReadOnly = True
    TabOrder = 1
  end
  object Button1: TButton
    Left = 200
    Top = 352
    Width = 75
    Height = 25
    Caption = #20851#38381
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 409
    Top = 320
    Width = 25
    Height = 21
    Caption = '...'
    TabOrder = 3
    OnClick = Button2Click
  end
  object editProductNo: TEdit
    Left = 96
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object editName: TEdit
    Left = 312
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object editProductType: TEdit
    Left = 96
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object editBrand: TEdit
    Left = 312
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object editShortDesc: TEdit
    Left = 96
    Top = 136
    Width = 337
    Height = 21
    TabOrder = 8
  end
  object editSupplierName: TEdit
    Left = 96
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 9
  end
  object editSupplierTel: TEdit
    Left = 312
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 10
  end
  object editSupplierMem: TEdit
    Left = 96
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 11
  end
  object editSupplierAddr: TEdit
    Left = 96
    Top = 104
    Width = 337
    Height = 21
    TabOrder = 12
  end
end
