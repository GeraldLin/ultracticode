//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editcallbacktime.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditCallBackTime *FormEditCallBackTime;
//---------------------------------------------------------------------------
__fastcall TFormEditCallBackTime::TFormEditCallBackTime(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCallBackTime::Button2Click(TObject *Sender)
{
  dmAgent->SetDialOutDialOutCallBack(Id, 0, "");
  try
  {
    dmAgent->adoQryDialOutProc->Close();
    dmAgent->adoQryDialOutProc->Open();
    dmAgent->QueryDialOutCallBackRecords();
  }
  catch (...)
  {
  }
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCallBackTime::Button1Click(TObject *Sender)
{
  AnsiString strTime = MonthCalendar1->Date.FormatString("yyyy-mm-dd ")+IntToStr(CSpinEdit1->Value)+":"+IntToStr(CSpinEdit2->Value)+":00";
  dmAgent->SetDialOutDialOutCallBack(Id, 1, strTime);
  try
  {
    dmAgent->adoQryDialOutProc->Close();
    dmAgent->adoQryDialOutProc->Open();
    dmAgent->QueryDialOutCallBackRecords();
  }
  catch (...)
  {
  }
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCallBackTime::Button3Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

