//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "searchcust.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormSeachCustomer *FormSeachCustomer;
extern AnsiString strDBType;
extern bool NewCustId;
extern CAccept MyAccept;
extern CCustomer MyCustomer; //當前客戶
extern CCustEx MyCustEx;
extern BSTR bEmpty;
extern int CustExOpenId;
extern int GetControlWith(int nDefaultWith, int nType);
//---------------------------------------------------------------------------
__fastcall TFormSeachCustomer::TFormSeachCustomer(TComponent* Owner)
  : TForm(Owner)
{
  for (int i=0; i<MAX_CUST_QUERYCONTROL_NUM; i++)
  {
    CustExQryItemId[i] = -1;
    CustExQrySql[i] = "";
    TCustExQryLabelList[i] = NULL;
    TCustExQryInputList[i] = NULL;
  }
  CustExQryItemNum = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormSeachCustomer::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSeachCustomer::Button1Click(TObject *Sender)
{
  char sqlbuf[512];
  AnsiString strUSRWhereSql="";

  if (CustExQryItemNum > 0 && ckCustExSearch->Checked == true)
  {
    for (int i=0; i<MAX_CUST_QUERYCONTROL_NUM; i++)
    {
      if (TCustExQryInputList[i] == NULL)
        continue;
      switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[CustExQryItemId[i]].CControlType)
      {
      case 1: //文本下拉框
        if (((TComboBox *)TCustExQryInputList[i])->Text != "不選擇")
        {
          if (strUSRWhereSql == "")
            strUSRWhereSql = CustExQrySql[i]+"'"+((TComboBox *)TCustExQryInputList[i])->Text+"'";
          else
            strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'"+((TComboBox *)TCustExQryInputList[i])->Text+"'";
        }
        break;
      case 2: //數值下拉框
        if (((TComboBox *)TCustExQryInputList[i])->Text != "不選擇")
        {
          if (strUSRWhereSql == "")
            strUSRWhereSql = CustExQrySql[i]+"'"+IntToStr(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemValue(((TComboBox *)TCustExQryInputList[i])->Text))+"'";
          else
            strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'"+IntToStr(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemValue(((TComboBox *)TCustExQryInputList[i])->Text))+"'";
        }
        break;
      case 3: //長文本框
        if (((TEdit *)TCustExQryInputList[i])->Text != "")
        {
          if (strUSRWhereSql == "")
            strUSRWhereSql = CustExQrySql[i]+"'%"+((TEdit *)TCustExQryInputList[i])->Text+"%'";
          else
            strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'%"+((TEdit *)TCustExQryInputList[i])->Text+"%'";
        }
        break;
      case 4: //短文本框
        if (((TEdit *)TCustExQryInputList[i])->Text != "")
        {
          if (strUSRWhereSql == "")
            strUSRWhereSql = CustExQrySql[i]+"'%"+((TEdit *)TCustExQryInputList[i])->Text+"%'";
          else
            strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'%"+((TEdit *)TCustExQryInputList[i])->Text+"%'";
        }
        break;
      case 5: //數值框
        if (strUSRWhereSql == "")
          strUSRWhereSql = CustExQrySql[i]+"'"+IntToStr(((TCSpinEdit *)TCustExQryInputList[i])->Value)+"'";
        else
          strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'"+IntToStr(((TCSpinEdit *)TCustExQryInputList[i])->Value)+"'";
        break;
      case 6: //金額框
        if (strUSRWhereSql == "")
          strUSRWhereSql = CustExQrySql[i]+"'"+((TEdit *)TCustExQryInputList[i])->Text+"'";
        else
          strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'"+((TEdit *)TCustExQryInputList[i])->Text+"'";
        break;
      case 7: //日期框
        if (strUSRWhereSql == "")
          strUSRWhereSql = CustExQrySql[i]+"'"+((TDateTimePicker *)TCustExQryInputList[i])->Date.FormatString("yyyy-mm-dd")+"'";
        else
          strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'"+((TDateTimePicker *)TCustExQryInputList[i])->Date.FormatString("yyyy-mm-dd")+"'";
        break;
      case 8: //是非框
        if (((TComboBox *)TCustExQryInputList[i])->Text == "是")
        {
          if (strUSRWhereSql == "")
            strUSRWhereSql = CustExQrySql[i]+"'1'";
          else
            strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'1'";
        }
        else if (((TComboBox *)TCustExQryInputList[i])->Text == "否")
        {
          if (strUSRWhereSql == "")
            strUSRWhereSql = CustExQrySql[i]+"'0'";
          else
            strUSRWhereSql = strUSRWhereSql+" and "+CustExQrySql[i]+"'0'";
        }
        break;
      }
    }
  }

  if (strUSRWhereSql == "")
    sprintf(sqlbuf, "Select top 50 * from tbCustomer where CustName like '%%%s%%' or CorpName like '%%%s%%' or MobileNo like '%%%s%%' or TeleNo like '%%%s%%' or FaxNo like '%%%s%%' or AccountNo='%s'",
      Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str());
  else
    sprintf(sqlbuf, "Select top 50 * from tbCustomer where (CustName like '%%%s%%' or CorpName like '%%%s%%' or MobileNo like '%%%s%%' or TeleNo like '%%%s%%' or FaxNo like '%%%s%%' or AccountNo='%s') AND %s",
      Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), Edit1->Text.c_str(), strUSRWhereSql.c_str());

  try
  {
    dmAgent->adoQrySearchCust->SQL->Clear();
    dmAgent->adoQrySearchCust->SQL->Add((char *)sqlbuf );
    dmAgent->adoQrySearchCust->Open();
  }
  catch ( ... )
  {
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormSeachCustomer::Button3Click(TObject *Sender)
{
  CAcceptEx acceptex;
  if (dmAgent->adoQrySearchCust->State == dsInactive || dmAgent->adoQrySearchCust->Eof)
    return;
  if ( MessageBox( NULL, "您確認將選擇的來電者資料為當前來電者嗎", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
    return;
  if (dmAgent->GetCustomRecord(dmAgent->adoQrySearchCust, &MyCustomer) == 0)
  {
    dmAgent->GetCustExRecord(dmAgent->adoQrySearchCust, &MyCustEx);
    NewCustId = false;
    FormMain->SetCustom(&MyCustomer);
    FormMain->SetCustEx(MyCustEx);
    MyAccept.CustomNo = MyCustomer.CustomNo;
    dmAgent->UpdateAcceptCustomNo(&MyAccept);
    dmAgent->UpdateCDRCustomNo(&MyAccept);
    dmAgent->adoQrySearchCust->Close();

    dmAgent->QueryCurAccept(MyCustomer.CustomNo, "", "");

    if (FormMain->ckClearAcptData->Checked == true)
    {
      FormMain->SetCustDataToAcceptExData(&MyCustomer, &MyCustEx, &acceptex);
      FormMain->SetAcceptExData(&acceptex);
    }

    this->Close();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormSeachCustomer::Button4Click(TObject *Sender)
{
  FormMain->MyAgent1->AnswerCall(1, bEmpty, bEmpty);
}
//---------------------------------------------------------------------------
void TFormSeachCustomer::CreateCustExQryControl()
{
  static bool bCreated=false;
  int nShortEditWith=120;
  int nEditLeft=100, nEditTop=50;
  int i, j=0;
  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList->ItemNum == 0)
  {
    plCustExQry->Height = 45;
    return;
  }
  if (bCreated == true)
    return;
  bCreated = true;
  for (i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].state == 0)
      continue;
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 0)
      continue;
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CSearchId == 0)
      continue;
    //輸入框控件
    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TComboBox *)TCustExQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TCustExQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TComboBox *)TCustExQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      dmAgent->AddCmbItem((TComboBox *)TCustExQryInputList[j], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 2: //數值下拉框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TComboBox *)TCustExQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TCustExQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TComboBox *)TCustExQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TCustExQryInputList[j])->OnKeyDown = /FormKeyDown;
      ((TComboBox *)TCustExQryInputList[j])->OnKeyPress = FormMain->MyComboBoxKeyPressEvent;
      dmAgent->AddCmbItem((TComboBox *)TCustExQryInputList[j], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 3: //長文本框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TEdit(this);
      ((TEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TEdit *)TCustExQryInputList[j])->Text = "";
      ((TEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TEdit *)TCustExQryInputList[j])->MaxLength = 50;
      //((TEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+" like ";
      j++;
      break;
    case 4: //短文本框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TEdit(this);
      ((TEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TEdit *)TCustExQryInputList[j])->Text = "";
      ((TEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TEdit *)TCustExQryInputList[j])->MaxLength = 50;
      //((TEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+" like ";
      j++;
      break;
    case 5: //數值框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+">=";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TCSpinEdit(this);
      ((TCSpinEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TCSpinEdit *)TCustExQryInputList[j])->MinValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue;
      ((TCSpinEdit *)TCustExQryInputList[j])->MaxValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue;
      ((TCSpinEdit *)TCustExQryInputList[j])->Value = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue;
      ((TCSpinEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TCSpinEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TCSpinEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      //((TCSpinEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TCSpinEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+">=";
      j++;
      if (j >= MAX_CUST_QUERYCONTROL_NUM)
        break;
      if (nEditLeft == 850)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
      else
      {
        nEditLeft = nEditLeft+250;
      }
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"<=";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TCSpinEdit(this);
      ((TCSpinEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TCSpinEdit *)TCustExQryInputList[j])->MinValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue;
      ((TCSpinEdit *)TCustExQryInputList[j])->MaxValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue;
      ((TCSpinEdit *)TCustExQryInputList[j])->Value = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue;
      ((TCSpinEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TCSpinEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TCSpinEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      //((TCSpinEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TCSpinEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"<=";
      j++;
      break;
    case 6: //金額框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+">=";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TEdit(this);
      ((TEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TEdit *)TCustExQryInputList[j])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue;
      ((TEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TEdit *)TCustExQryInputList[j])->MaxLength = 20;
      //((TEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+">=";
      j++;
      if (j >= MAX_CUST_QUERYCONTROL_NUM)
        break;
      if (nEditLeft == 850)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
      else
      {
        nEditLeft = nEditLeft+250;
      }
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"<=";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TEdit(this);
      ((TEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TEdit *)TCustExQryInputList[j])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue;
      ((TEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TEdit *)TCustExQryInputList[j])->MaxLength = 20;
      //((TEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"<=";
      j++;
      break;
    case 7: //日期框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+">=";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TDateTimePicker(this);
      ((TDateTimePicker *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TDateTimePicker *)TCustExQryInputList[j])->Kind = dtkDate;
      ((TDateTimePicker *)TCustExQryInputList[j])->Date = IncMonth(Date(),-1);
      ((TDateTimePicker *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TDateTimePicker *)TCustExQryInputList[j])->Top = nEditTop;
      ((TDateTimePicker *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      //((TDateTimePicker *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TDateTimePicker *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+">=";
      j++;
      if (j >= MAX_CUST_QUERYCONTROL_NUM)
        break;
      if (nEditLeft == 850)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
      else
      {
        nEditLeft = nEditLeft+250;
      }
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"<=";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TDateTimePicker(this);
      ((TDateTimePicker *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TDateTimePicker *)TCustExQryInputList[j])->Kind = dtkDate;
      ((TDateTimePicker *)TCustExQryInputList[j])->Date = Date();
      ((TDateTimePicker *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TDateTimePicker *)TCustExQryInputList[j])->Top = nEditTop;
      ((TDateTimePicker *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      //((TDateTimePicker *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TDateTimePicker *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"<=";
      j++;
      break;
    case 8: //是非框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TComboBox *)TCustExQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TCustExQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TComboBox *)TCustExQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TComboBox *)TCustExQryInputList[j])->OnKeyPress = FormMain->MyComboBoxKeyPressEvent;
      dmAgent->AddCmbItem((TComboBox *)TCustExQryInputList[j], dmAgent->YesNoItemList, "不選擇");
      ((TComboBox *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 9: //長文本下拉框
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TComboBox *)TCustExQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TCustExQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TComboBox *)TCustExQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      dmAgent->AddCmbItem((TComboBox *)TCustExQryInputList[j], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    default:
      //輸入標簽控件
      TCustExQryLabelList[j] = new TLabel(this);
      TCustExQryLabelList[j]->Parent = plCustExQry;
      TCustExQryLabelList[j]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExQryLabelList[j]->Left = nEditLeft-TCustExQryLabelList[j]->Width-2;
      TCustExQryLabelList[j]->Top = nEditTop+4;
      TCustExQryLabelList[j]->Show();
      //輸入框控件
      TCustExQryInputList[j] = new TEdit(this);
      ((TEdit *)TCustExQryInputList[j])->Parent = plCustExQry;
      ((TEdit *)TCustExQryInputList[j])->Text = "";
      ((TEdit *)TCustExQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TCustExQryInputList[j])->Top = nEditTop;
      ((TEdit *)TCustExQryInputList[j])->Width = GetControlWith(nShortEditWith, FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CWidth);
      ((TEdit *)TCustExQryInputList[j])->MaxLength = 50;
      //((TEdit *)TCustExQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TCustExQryInputList[j])->Show();
      CustExQryItemId[j] = i;
      CustExQrySql[j] = "ItemData"+IntToStr(i+1)+" like ";
      j++;
      break;
    }
    if (j >= MAX_CUST_QUERYCONTROL_NUM)
      break;
    if (nEditLeft == 850)
    {
      nEditLeft = 100;
      nEditTop = nEditTop+22;
    }
    else
    {
      nEditLeft = nEditLeft+250;
    }
  }
  CustExQryItemNum = j;
  if (CustExQryItemNum > 0)
  {
    plCustExQry->Height = 110;
  }
}

void __fastcall TFormSeachCustomer::FormShow(TObject *Sender)
{
  CreateCustExQryControl();
  SetDispCustomItem();
  SetDispCustExItem();
}
//---------------------------------------------------------------------------

void __fastcall TFormSeachCustomer::FormDestroy(TObject *Sender)
{
    for (int i=0; i<MAX_CUST_QUERYCONTROL_NUM; i++)
    {
      if (TCustExQryLabelList[i] != NULL)
      {
        delete TCustExQryLabelList[i];
        TCustExQryLabelList[i] = NULL;
      }
      if (TCustExQryInputList[i] != NULL)
      {
        delete TCustExQryInputList[i];
        TCustExQryInputList[i] = NULL;
      }
    }
}
//---------------------------------------------------------------------------
void TFormSeachCustomer::SetDispCustomItem()
{
  char sqlbuf[128];
  AnsiString FieldExplain;
  int nItemId, nDispId, nDispWidth;
  static bool bId=false;

  if (bId == true)
    return;
  bId = true;

  sprintf(sqlbuf, "Select id,dispid,dispwidth,FieldExplain from tbcustomset order by id");
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf);
    dmAgent->adoqueryPub->Open();
    while ( !dmAgent->adoqueryPub->Eof )
    {
      nItemId = dmAgent->adoqueryPub->FieldByName("id")->AsInteger;
      nDispId = dmAgent->adoqueryPub->FieldByName("dispid")->AsInteger;
      nDispWidth = dmAgent->adoqueryPub->FieldByName("dispwidth")->AsInteger;
      FieldExplain = dmAgent->adoqueryPub->FieldByName("FieldExplain")->AsString;

      dgCustom->Columns->Items[nItemId]->Visible = (nDispId==1) ? true : false;
      if (nDispId==1)
      {
        //dgCustom->Columns->Items[nItemId]->Title->Caption = FieldExplain;
        dgCustom->Columns->Items[nItemId]->Width = nDispWidth;
      }
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
  }
}
void TFormSeachCustomer::SetDispCustExItem()
{
  int nItemId, nDispId, nDispWidth;
  static bool bId=false;

  if (bId == true)
    return;
  bId = true;

  if (FormMain->m_pCustExItemSetList->ItemNum == 0)
    return;
  for (nItemId=0; nItemId<MAX_CUST_CONTROL_NUM; nItemId++)
  {
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].state == 0)
      continue;
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CDispId == 1 &&
      (FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].ReadDataType == 1 || FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].ReadDataType == 4 || 
      FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].SaveToDBFlag == 1)) //2017-01-02 edit
    {
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CControlType == 2 || FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CControlType == 8)
      {
        dgCustom->Columns->Items[nItemId*2+21]->Visible = true;
        dgCustom->Columns->Items[nItemId*2+21]->Title->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CLabelCaption;
        dgCustom->Columns->Items[nItemId*2+21]->Width = FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CDispWidth;
      }
      else
      {
        dgCustom->Columns->Items[nItemId*2+20]->Visible = true;
        dgCustom->Columns->Items[nItemId*2+20]->Title->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CLabelCaption;
        dgCustom->Columns->Items[nItemId*2+20]->Width = FormMain->m_pCustExItemSetList->m_CustExItemSet[nItemId].CDispWidth;
      }
    }
  }
}

