//---------------------------------------------------------------------------

#ifndef sumdialoutH
#define sumdialoutH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
//---------------------------------------------------------------------------
class TFormSunDialOut : public TForm
{
__published:	// IDE-managed Components
  TQuickRep *QuickRep1;
  TQRBand *PageHeaderBand1;
  TQRSysData *QRSysData2;
  TQRLabel *QRLabel1;
  TQRLabel *QRLabelTotal;
  TQRLabel *QRLabel2;
  TQRLabel *QRLabel3;
  TQRLabel *QRLabel4;
  TQRLabel *QRLabel5;
  TQRBand *DetailBand1;
  TQRDBText *QRDBText1;
  TQRDBText *QRDBText2;
  TQRDBText *QRDBText3;
  TQRDBText *QRDBText4;
  TQRBand *PageFooterBand1;
  TQRSysData *QRSysData1;
private:	// User declarations
public:		// User declarations
  __fastcall TFormSunDialOut(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSunDialOut *FormSunDialOut;
//---------------------------------------------------------------------------
#endif
