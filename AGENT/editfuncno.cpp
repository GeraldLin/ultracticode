//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editfuncno.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditFuncNo *FormEditFuncNo;
//---------------------------------------------------------------------------
__fastcall TFormEditFuncNo::TFormEditFuncNo(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditFuncNo::SetDispValue(bool bInsertId, int nFuncNo, AnsiString strFlwFile, AnsiString strFlwName)
{
  InsertId = bInsertId;
  if (InsertId == true)
  {
    cseFuncNo->Value = 0;
    cseFuncNo->Color = clWindow;
    cseFuncNo->ReadOnly = false;
    editFlwFile->Text = "";
    editFlwName->Text = "";
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    cseFuncNo->Value = nFuncNo;
    cseFuncNo->Color = clGrayText;
    cseFuncNo->ReadOnly = true;
    editFlwFile->Text = strFlwFile;
    editFlwName->Text = strFlwName;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}

void __fastcall TFormEditFuncNo::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditFuncNo->cseFuncNo->SetFocus();
  else
    FormEditFuncNo->editFlwFile->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditFuncNo::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditFuncNo::cseFuncNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditFuncNo::Button1Click(TObject *Sender)
{
  if (cseFuncNo->Value == 0)
  {
    MessageBox(NULL,"對不起，IVR流程功能號不能為0！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editFlwFile->Text == "")
  {
    MessageBox(NULL,"對不起，請輸入IVR流程文件名！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editFlwName->Text == "")
  {
    MessageBox(NULL,"對不起，請輸入IVR流程名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (InsertId == true)
  {
    if (dmAgent->IsFlwFuncNoExist(cseFuncNo->Value) == 1)
    {
      MessageBox(NULL,"對不起，該IVR流程功能號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertFlwFuncNoRecord(cseFuncNo->Value, editFlwFile->Text, editFlwName->Text) == 0)
    {
      dmAgent->adoQryFlwFuncNo->Close();
      dmAgent->adoQryFlwFuncNo->Open();
      dmAgent->UpdateFlwFuncNoListItems();
      this->Close();
      //MessageBox(NULL,"新增IVR流程功能號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增IVR流程功能號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateFlwFuncNoRecord(cseFuncNo->Value, editFlwFile->Text, editFlwName->Text) == 0)
    {
      dmAgent->adoQryFlwFuncNo->Close();
      dmAgent->adoQryFlwFuncNo->Open();
      dmAgent->UpdateFlwFuncNoListItems();
      this->Close();
      //MessageBox(NULL,"修改IVR流程功能號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改IVR流程功能號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
