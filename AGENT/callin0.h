//---------------------------------------------------------------------------

#ifndef callin0H
#define callin0H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormCallin0 : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *lblCallerNo;
  TSpeedButton *SpeedButton2;
  TLabel *Label12;
  TLabel *lbTranPhone;
  TButton *Button1;
  TLabel *lbSrvType;
  TLabel *lblCalledNo;
  TLabel *Label3;
  void __fastcall SpeedButton2Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall FormKeyPress(TObject *Sender, char &Key);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormCallin0(TComponent* Owner);
  void SetCallerNo(AnsiString callerno);
  void SetCalledNo(AnsiString calledno, int inout=1);
  void SetCallerName(AnsiString callername);
  void SetTelOfCity(AnsiString city);
  void SetTranPhone(int nacdcalltype, const char *transeatno);
  void SetSrvType(CAccept &accept);
  void SetSrvType(int nAcptType, int nAcptSubType);
  void SetScreenPop(int nPopId);
  void SetCallType(int nCallType);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormCallin0 *FormCallin0;
//---------------------------------------------------------------------------
#endif
