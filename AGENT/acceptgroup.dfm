object FormAcceptGroup: TFormAcceptGroup
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'ACD'#35441#21209#32068#20998#37197#21443#25976
  ClientHeight = 413
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label7: TLabel
    Left = 77
    Top = 385
    Width = 443
    Height = 13
    AutoSize = False
    Caption = #27880#65306#27794#26377#25353#37749#36984#25799#30452#25509#36681#24231#24109#35531#35373#23450#28858'0'#12290#19981#35373#23450#27966#32218#26041#24335#35531':'#19981#36984#25799#65292
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = #26032#32048#26126#39636
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 172
    Top = 342
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 457
    Top = 342
    Width = 81
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 1
    OnClick = Button2Click
  end
  object GroupBox1: TGroupBox
    Left = 9
    Top = 9
    Width = 376
    Height = 70
    Caption = #34399#30908#21028#26039#36984#25799#65306
    TabOrder = 2
    object Label1: TLabel
      Left = 52
      Top = 48
      Width = 60
      Height = 12
      Caption = #38651#35441#34399#30908#65306
    end
    object Label12: TLabel
      Left = 26
      Top = 22
      Width = 84
      Height = 12
      Caption = #34399#30908#39006#22411#36984#25799#65306
    end
    object editCalledNo: TEdit
      Left = 121
      Top = 43
      Width = 131
      Height = 20
      TabOrder = 0
      OnChange = editCalledNoChange
    end
    object cbCalledType: TComboBox
      Left = 121
      Top = 17
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 1
      Text = #25353#25765#20837#34399#30908
      OnChange = editCalledNoChange
      OnKeyPress = cbCalledTypeKeyPress
      Items.Strings = (
        #25353#25765#20837#34399#30908
        #25353#20358#38651#34399#30908)
    end
  end
  object GroupBox2: TGroupBox
    Left = 398
    Top = 9
    Width = 270
    Height = 70
    Caption = #20998#37197#26041#24335#36984#25799#65306
    TabOrder = 3
    object Label13: TLabel
      Left = 52
      Top = 22
      Width = 60
      Height = 12
      Caption = #36984#25799#26041#24335#65306
    end
    object Label2: TLabel
      Left = 34
      Top = 48
      Width = 80
      Height = 12
      Caption = 'IVR'#36984#21934#25353#37749#65306
    end
    object Label14: TLabel
      Left = 52
      Top = 48
      Width = 60
      Height = 12
      Caption = #23458#25142#39006#22411#65306
    end
    object Label15: TLabel
      Left = 52
      Top = 47
      Width = 60
      Height = 12
      Caption = #23458#25142#32026#21029#65306
    end
    object cbDTMFType: TComboBox
      Left = 121
      Top = 17
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 0
      Text = 'IVR'#25353#37749
      OnChange = cbDTMFTypeChange
      OnKeyPress = cbCalledTypeKeyPress
      Items.Strings = (
        'IVR'#25353#37749
        #23458#25142#39006#22411
        #23458#25142#32026#21029
        #22806#32218#36852#36335)
    end
    object editDTMFKey: TEdit
      Left = 121
      Top = 43
      Width = 131
      Height = 20
      Hint = #27794#26377#25353#37749#36984#25799#35531#35373#32622#28858#31354
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = editCalledNoChange
    end
    object cbCustType: TComboBox
      Left = 121
      Top = 43
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 2
      OnChange = editCalledNoChange
      OnKeyPress = cbCalledTypeKeyPress
    end
    object cbCustLevel: TComboBox
      Left = 121
      Top = 43
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 3
      OnChange = editCalledNoChange
      OnKeyPress = cbCalledTypeKeyPress
    end
  end
  object GroupBox3: TGroupBox
    Left = 9
    Top = 87
    Width = 376
    Height = 234
    Caption = #35441#21209#32068#36984#25799#65306
    TabOrder = 4
    object Label3: TLabel
      Left = 26
      Top = 22
      Width = 84
      Height = 12
      Caption = #39318#36984#27966#32218#26041#24335#65306
    end
    object Label4: TLabel
      Left = 2
      Top = 48
      Width = 108
      Height = 12
      Caption = #31532#20108#38918#20301#27966#32218#26041#24335#65306
    end
    object Label5: TLabel
      Left = 2
      Top = 74
      Width = 108
      Height = 12
      Caption = #31532#19977#38918#20301#27966#32218#26041#24335#65306
    end
    object Label9: TLabel
      Left = 2
      Top = 100
      Width = 108
      Height = 12
      Caption = #31532#22235#38918#20301#27966#32218#26041#24335#65306
    end
    object Label10: TLabel
      Left = 2
      Top = 126
      Width = 108
      Height = 12
      Caption = #31532#20116#38918#20301#27966#32218#26041#24335#65306
    end
    object Label11: TLabel
      Left = 14
      Top = 152
      Width = 96
      Height = 12
      Caption = #21463#29702#30340#26989#21209#39006#22411#65306
    end
    object cbGroupNo: TComboBox
      Left = 121
      Top = 17
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 0
      Text = #19981#36984#25799
      OnChange = editCalledNoChange
    end
    object cbGroupNo1: TComboBox
      Left = 121
      Top = 43
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 1
      Text = #19981#36984#25799
      OnChange = editCalledNoChange
    end
    object cbGroupNo2: TComboBox
      Left = 121
      Top = 69
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 2
      Text = #19981#36984#25799
      OnChange = editCalledNoChange
    end
    object cbGroupNo3: TComboBox
      Left = 121
      Top = 95
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 3
      Text = #19981#36984#25799
      OnChange = editCalledNoChange
    end
    object cbGroupNo4: TComboBox
      Left = 121
      Top = 121
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 4
      Text = #19981#36984#25799
      OnChange = editCalledNoChange
    end
    object cbSrvType: TComboBox
      Left = 121
      Top = 147
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 5
      OnChange = editCalledNoChange
    end
    object ckGroupNo1Type: TCheckBox
      Left = 255
      Top = 21
      Width = 113
      Height = 17
      Caption = #26159#21542#28858#20998#27231#34399#30908'?'
      TabOrder = 6
      OnClick = editCalledNoChange
    end
    object ckGroupNo2Type: TCheckBox
      Left = 255
      Top = 47
      Width = 113
      Height = 17
      Caption = #26159#21542#28858#20998#27231#34399#30908'?'
      TabOrder = 7
      OnClick = editCalledNoChange
    end
    object ckGroupNo3Type: TCheckBox
      Left = 255
      Top = 73
      Width = 113
      Height = 17
      Caption = #26159#21542#28858#20998#27231#34399#30908'?'
      TabOrder = 8
      OnClick = editCalledNoChange
    end
    object ckGroupNo4Type: TCheckBox
      Left = 255
      Top = 99
      Width = 113
      Height = 17
      Caption = #26159#21542#28858#20998#27231#34399#30908'?'
      TabOrder = 9
      OnClick = editCalledNoChange
    end
    object ckGroupNo5Type: TCheckBox
      Left = 255
      Top = 125
      Width = 113
      Height = 17
      Caption = #26159#21542#28858#20998#27231#34399#30908'?'
      TabOrder = 10
      OnClick = editCalledNoChange
    end
  end
  object GroupBox4: TGroupBox
    Left = 398
    Top = 87
    Width = 270
    Height = 234
    Caption = #25773#25918#35486#38899#36984#25799#65306
    TabOrder = 5
    object Label6: TLabel
      Left = 73
      Top = 202
      Width = 36
      Height = 12
      Caption = #20633#35387#65306
    end
    object Label16: TLabel
      Left = 52
      Top = 22
      Width = 60
      Height = 12
      Caption = #35486#38899#36984#25799#65306
    end
    object Label17: TLabel
      Left = 13
      Top = 48
      Width = 96
      Height = 12
      Caption = #20491#24615#21270#21839#20505#35486#38899#65306
    end
    object Label18: TLabel
      Left = 13
      Top = 73
      Width = 96
      Height = 12
      Caption = #20491#24615#21270#36984#21934#35486#38899#65306
    end
    object Label19: TLabel
      Left = 2
      Top = 126
      Width = 120
      Height = 12
      Caption = #25773#22577#20540#27231#21729#32232#34399#36984#25799#65306
    end
    object Label20: TLabel
      Left = 13
      Top = 99
      Width = 96
      Height = 12
      Caption = #20491#24615#21270#36984#21934#25353#37749#65306
    end
    object Label21: TLabel
      Left = 8
      Top = 152
      Width = 102
      Height = 12
      Caption = #36681#25509#21069#25353'1'#37749#30906#35469#65306
    end
    object Label22: TLabel
      Left = 38
      Top = 178
      Width = 72
      Height = 12
      Caption = #30906#35469#35486#38899#27284#65306
    end
    object editSrvName: TEdit
      Left = 121
      Top = 197
      Width = 131
      Height = 20
      TabOrder = 0
      OnChange = editCalledNoChange
    end
    object cbVocId: TComboBox
      Left = 121
      Top = 17
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 1
      Text = #25353#31995#32113#35373#23450#21443#25976#25773#22577#35486#38899
      OnChange = editCalledNoChange
      OnKeyPress = cbCalledTypeKeyPress
      Items.Strings = (
        #19981#25773#22577#35486#38899#30452#25509#36681#20540#27231#21729
        #25353#31995#32113#35373#23450#21443#25976#25773#22577#35486#38899
        #25773#22577#20491#24615#21270#20844#21496#35486#38899
        #25773#22577#20491#24615#21270#20844#21496#21450#35486#38899#36984#21934
        #25773#22577#32317#27231#35486#38899#36984#21934)
    end
    object editCorpVoc: TEdit
      Left = 121
      Top = 43
      Width = 131
      Height = 20
      TabOrder = 2
      OnChange = editCalledNoChange
    end
    object editMenuVoc: TEdit
      Left = 121
      Top = 69
      Width = 131
      Height = 20
      TabOrder = 3
      OnChange = editCalledNoChange
    end
    object cbWorkerNoId: TComboBox
      Left = 121
      Top = 121
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 4
      Text = #19981#25773#22577#20540#27231#21729#32232#34399
      OnChange = editCalledNoChange
      OnKeyPress = cbCalledTypeKeyPress
      Items.Strings = (
        #19981#25773#22577#20540#27231#21729#32232#34399
        #25353#31995#32113#21443#25976#35373#23450)
    end
    object editMenuKeys: TEdit
      Left = 121
      Top = 95
      Width = 131
      Height = 20
      TabOrder = 5
      OnChange = editCalledNoChange
    end
    object cbMenuDtmfACK: TComboBox
      Left = 121
      Top = 147
      Width = 131
      Height = 20
      ItemHeight = 12
      TabOrder = 6
      Text = #19981#25353#37749#30906#35469
      OnChange = editCalledNoChange
      OnKeyPress = cbCalledTypeKeyPress
      Items.Strings = (
        #19981#25353#37749#30906#35469
        #25353'1'#37749#30906#35469
        #25353'1'#37749#30906#35469#24182#20786#23384)
    end
    object editDtmfACKVoc: TEdit
      Left = 121
      Top = 173
      Width = 131
      Height = 20
      TabOrder = 7
      OnChange = editCalledNoChange
    end
  end
end
