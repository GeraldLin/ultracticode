object FormAcceptProc: TFormAcceptProc
  Left = 317
  Top = 124
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24460#32396#34389#29702#24773#27841#35352#37636
  ClientHeight = 452
  ClientWidth = 853
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 853
    Height = 44
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 15
      Width = 72
      Height = 12
      Caption = #20358#38651#32773#22995#21517#65306
    end
    object Label2: TLabel
      Left = 217
      Top = 15
      Width = 72
      Height = 12
      Caption = #20358#38651#32773#34399#30908#65306
    end
    object Label3: TLabel
      Left = 420
      Top = 14
      Width = 96
      Height = 12
      Caption = #26412#24037#21934#34389#29702#32080#26524#65306
    end
    object editCustName: TEdit
      Left = 78
      Top = 11
      Width = 131
      Height = 20
      Color = clGrayText
      ReadOnly = True
      TabOrder = 0
    end
    object editCustomTel: TEdit
      Left = 286
      Top = 11
      Width = 131
      Height = 20
      Color = clGrayText
      ReadOnly = True
      TabOrder = 1
    end
    object Button1: TButton
      Left = 763
      Top = 9
      Width = 81
      Height = 27
      Caption = #38364#38281
      TabOrder = 2
      OnClick = Button1Click
    end
    object cmbDealState: TComboBox
      Left = 529
      Top = 11
      Width = 98
      Height = 20
      ItemHeight = 12
      TabOrder = 3
      OnChange = cmbDealStateChange
      OnKeyPress = cmbDealStateKeyPress
    end
    object Button4: TButton
      Left = 633
      Top = 9
      Width = 98
      Height = 27
      Caption = #20786#23384#34389#29702#32080#26524
      TabOrder = 4
      OnClick = Button4Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 303
    Width = 853
    Height = 149
    Align = alBottom
    Caption = #24460#32396#36319#36914#34389#29702#20839#23481#65306
    TabOrder = 1
    object Panel2: TPanel
      Left = 575
      Top = 14
      Width = 276
      Height = 133
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 0
      object Label4: TLabel
        Left = 9
        Top = 12
        Width = 108
        Height = 12
        Caption = #24460#32396#36319#36914#26041#24335#36984#25799#65306
      end
      object Label5: TLabel
        Left = 48
        Top = 38
        Width = 72
        Height = 12
        Caption = #36319#36914#32773#36984#25799#65306
      end
      object Label6: TLabel
        Left = 35
        Top = 64
        Width = 84
        Height = 12
        Caption = #36319#36914#34389#29702#32080#26524#65306
      end
      object Button2: TButton
        Left = 95
        Top = 95
        Width = 109
        Height = 27
        Caption = #22686#21152#34389#29702#35352#37636
        TabOrder = 0
        OnClick = Button2Click
      end
      object cbNextProcType: TComboBox
        Left = 130
        Top = 9
        Width = 131
        Height = 20
        ItemHeight = 12
        TabOrder = 1
        Text = #28961#38656#24460#32396#34389#29702
        OnChange = cbNextProcTypeChange
        OnKeyPress = cmbDealStateKeyPress
      end
      object cbNextProcSel: TComboBox
        Left = 130
        Top = 35
        Width = 131
        Height = 20
        ItemHeight = 12
        TabOrder = 2
        OnChange = cbNextProcSelChange
        OnKeyPress = cmbDealStateKeyPress
      end
      object cbNextProcResult: TComboBox
        Left = 130
        Top = 61
        Width = 131
        Height = 20
        ItemHeight = 12
        TabOrder = 3
        OnChange = memoAcceptProcChange
        OnKeyPress = cmbDealStateKeyPress
      end
    end
    object memoAcceptProc: TMemo
      Left = 2
      Top = 14
      Width = 573
      Height = 133
      Align = alClient
      MaxLength = 4000
      ScrollBars = ssBoth
      TabOrder = 1
      OnChange = memoAcceptProcChange
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 44
    Width = 853
    Height = 259
    Hint = #24392#20986#33756#21934#35531#25353#40736#27161#21491#37749
    Align = alClient
    DataSource = dmAgent.dsOldAcceptProc
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ReadOnly = True
    ShowHint = True
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #26032#32048#26126#39636
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ProcTime'
        Width = 125
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NextProcResultName'
        Width = 106
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WorkerNo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WorkerName'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DepartmentName'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NextProcTypeName'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispNextName'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispRespond'
        Width = 56
        Visible = True
      end>
  end
  object PopupMenu1: TPopupMenu
    AutoHotkeys = maManual
    Left = 16
    Top = 80
    object N1: TMenuItem
      Caption = #25509#21463#35442#24037#21934
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #25298#32085#35442#24037#21934
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #28155#21152#34389#29702#35352#37636
      OnClick = N4Click
    end
  end
end
