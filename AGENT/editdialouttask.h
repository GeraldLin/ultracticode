//---------------------------------------------------------------------------

#ifndef editdialouttaskH
#define editdialouttaskH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include <ComCtrls.hpp>
#include "public.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFormEditDialOutTask : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label12;
  TLabel *Label13;
  TEdit *editTaskName;
  TDateTimePicker *dtpStartDate;
  TDateTimePicker *dtpEndDate;
  TDateTimePicker *dtpStartTime;
  TDateTimePicker *dtpEndTime;
  TComboBox *cbStartId;
  TCSpinEdit *cseRouteNo;
  TCSpinEdit *cseMaxCalledTimes;
  TCSpinEdit *cseNoAnsDelay;
  TEdit *editCallerNo;
  TEdit *editFileName;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label14;
  TLabel *Label17;
  TDateTimePicker *dtpStartTime2;
  TDateTimePicker *dtpEndTime2;
  TLabel *Label11;
  TEdit *editFileName2;
  TLabel *Label15;
  TComboBox *cbGroupNo;
  TComboBox *cbMixerType;
  TLabel *Label16;
  TCSpinEdit *cseMaxAnsTimes;
  TLabel *Label18;
  TCSpinEdit *cseMaxChnNum;
  TLabel *Label19;
  TComboBox *cbFuncNo;
  TComboBox *cbExportId;
  TLabel *Label20;
  TLabel *Label21;
  TCSpinEdit *csePlayDelay;
  TLabel *Label22;
  TComboBox *cbCallType;
  TLabel *Label23;
  TEdit *editTTSData;
  TLabel *Label24;
  TComboBox *cbTTSType;
  TLabel *Label25;
  TLabel *Label26;
  TComboBox *cbConfirmType;
  TLabel *Label27;
  TEdit *editConfirmVoc;
  TLabel *Label28;
  TEdit *editConfirmDTMF;
  TLabel *Label29;
  TEdit *editTranVDN;
  TLabel *Label30;
  TCSpinEdit *cseListenTimeLen;
  TLabel *Label31;
  TEdit *editDialPreCode;
  void __fastcall cbStartIdKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editTaskNameChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall editTTSDataChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditDialOutTask(TComponent* Owner);

  bool InsertId; //true-表示是增加記錄 false-表示修改記錄
  CDialOutTask DialOutTask;
  void SetModifyType(bool isinsert);
  void SetDialOutTaskData(CDialOutTask& dialouttask);
  int GetDialOutTaskData();
  void ClearDialOutTaskData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditDialOutTask *FormEditDialOutTask;
//---------------------------------------------------------------------------
#endif
