//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>
#include "dialoutcallback.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormCallBackPrompt *FormCallBackPrompt;
//---------------------------------------------------------------------------
__fastcall TFormCallBackPrompt::TFormCallBackPrompt(TComponent* Owner)
  : TForm(Owner)
{
  Id = -1;
}
//---------------------------------------------------------------------------
void __fastcall TFormCallBackPrompt::Button3Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------


void __fastcall TFormCallBackPrompt::Button2Click(TObject *Sender)
{
  TDateTime t;
  if (ComboBox1->Text == "30����")
    t = IncMinute(Now(), 30);
  else if (ComboBox1->Text == "60����")
    t = IncMinute(Now(), 60);
  else
    t = IncMinute(Now(), 10);
  AnsiString strTime = t.FormatString("yyyy-mm-dd hh:nn:00");
  dmAgent->SetDialOutDialOutCallBack(Id, 1, strTime);
  try
  {
    dmAgent->adoQryDialOutProc->Close();
    dmAgent->adoQryDialOutProc->Open();
    dmAgent->QueryDialOutCallBackRecords();
  }
  catch (...)
  {
  }
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormCallBackPrompt::Button1Click(TObject *Sender)
{
  FormMain->ProcDialOutCallBack(Id);
  this->Close();
}
//---------------------------------------------------------------------------

