//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "allocdialtask.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAllocDialTask *FormAllocDialTask;

extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormAllocDialTask::TFormAllocDialTask(TComponent* Owner)
  : TForm(Owner)
{
  TaskId = 0;
}

//---------------------------------------------------------------------------
void __fastcall TFormAllocDialTask::Button2Click(TObject *Sender)
{
  this->Close();
}

//---------------------------------------------------------------------------
void __fastcall TFormAllocDialTask::Button1Click(TObject *Sender)
{
    AnsiString sqlbuf, strWorkerNo, strCalledNo;
    int taskid, nDialId, nRecdCount, AllocCount=0;
    char dispbuf[128];

    if (cbWorkerNo->Text == "")
    {
      MessageBox(NULL,"請選擇值機編號！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    strWorkerNo = SplitStringFirstItem(cbWorkerNo->Text, "-");

    if (strWhere.Length() > 0)
      sqlbuf = "Select TaskId,Id,CalledNo from tbDialOutTele where "+strWhere+" order by Id";
    else
      sqlbuf = "Select TaskId,Id,CalledNo from tbDialOutTele order by Id";

    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add(sqlbuf);
    dmAgent->adoqueryPub->Open();
    nRecdCount = dmAgent->adoqueryPub->RecordCount;

    Panel1->Caption = "正在分配外撥號碼(0/"+IntToStr(nRecdCount)+"條)，請稍等......";
    Panel1->Visible = true;
    Application->ProcessMessages();

    while ( !dmAgent->adoqueryPub->Eof )
    {
      taskid = dmAgent->adoqueryPub->Fields->Fields[0]->AsInteger;
      nDialId = dmAgent->adoqueryPub->Fields->Fields[1]->AsInteger;
      strCalledNo = dmAgent->adoqueryPub->Fields->Fields[2]->AsString;

      if (dmAgent->SearchAllocedSameCalled(taskid, strCalledNo, strWorkerNo) == 0)
      {
        if (dmAgent->AllocDialOutWorkerNo(nDialId, strWorkerNo) > 0)
        {
          AllocCount = AllocCount+1;
        }
      }
      if ((AllocCount%50) == 0)
      {
        Panel1->Caption = "正在分配外撥號碼("+IntToStr(AllocCount)+"/"+IntToStr(nRecdCount)+"條)，請稍等......";
        Application->ProcessMessages();
      }
      dmAgent->adoqueryPub->Next();
    }
    dmAgent->adoqueryPub->Close();
    sprintf(dispbuf, "分配 %d/%d 條記錄", AllocCount, nRecdCount);
    MessageBox(NULL,dispbuf,"訊息提示",MB_OK|MB_ICONINFORMATION);
    Panel1->Visible = false;
    this->Close();
}

//---------------------------------------------------------------------------
void __fastcall TFormAllocDialTask::cbWorkerNoKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

