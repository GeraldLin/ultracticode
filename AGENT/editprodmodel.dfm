object FormEditProdModel: TFormEditProdModel
  Left = 662
  Top = 385
  BorderStyle = bsSingle
  Caption = #20462#25913#29986#21697#22411#34399
  ClientHeight = 385
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lbItemId: TLabel
    Left = 58
    Top = 21
    Width = 51
    Height = 12
    Caption = #29986#21697#22411#34399':'
  end
  object lbItemName: TLabel
    Left = 281
    Top = 21
    Width = 75
    Height = 12
    Caption = #29986#21697#22411#34399#21517#31281':'
  end
  object Label1: TLabel
    Left = 35
    Top = 80
    Width = 74
    Height = 12
    Caption = #29986#21697#22411#34399'URL:'
  end
  object Label2: TLabel
    Left = 32
    Top = 112
    Width = 51
    Height = 12
    Caption = #35441#34899#33139#26412':'
  end
  object Label7: TLabel
    Left = 261
    Top = 107
    Width = 96
    Height = 12
    Caption = #19979#25289#26694#39023#31034#38918#24207#65306
  end
  object Label8: TLabel
    Left = 27
    Top = 53
    Width = 83
    Height = 12
    Caption = 'URL'#25353#37397#21517#31281#65306
  end
  object editItemId: TEdit
    Left = 114
    Top = 17
    Width = 150
    Height = 20
    TabOrder = 0
    OnChange = editItemIdChange
  end
  object editItemName: TEdit
    Left = 362
    Top = 17
    Width = 150
    Height = 20
    TabOrder = 1
    OnChange = editItemIdChange
  end
  object Button1: TButton
    Left = 115
    Top = 335
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 341
    Top = 335
    Width = 82
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editItemURL: TEdit
    Left = 114
    Top = 76
    Width = 399
    Height = 20
    TabOrder = 4
    OnChange = editItemIdChange
  end
  object memoDemo: TMemo
    Left = 32
    Top = 128
    Width = 481
    Height = 185
    MaxLength = 3500
    ScrollBars = ssBoth
    TabOrder = 5
    OnChange = editItemIdChange
  end
  object cseDispOrder: TCSpinEdit
    Left = 363
    Top = 102
    Width = 75
    Height = 21
    TabOrder = 6
    OnChange = editItemIdChange
  end
  object editURLBtnName: TEdit
    Left = 113
    Top = 48
    Width = 151
    Height = 20
    TabOrder = 7
    OnChange = editItemIdChange
  end
end
