object FormZSKSubject: TFormZSKSubject
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #26989#21209#30693#35672#24235#38988#24235#35338#24687
  ClientHeight = 590
  ClientWidth = 676
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 26
    Top = 13
    Width = 72
    Height = 13
    Caption = #20027#20998#39006#27161#38988#65306
  end
  object Label3: TLabel
    Left = 38
    Top = 99
    Width = 60
    Height = 13
    Caption = #35443#32048#35498#26126#65306
  end
  object Label4: TLabel
    Left = 482
    Top = 497
    Width = 60
    Height = 13
    Caption = #30332#24067#29376#24907#65306
  end
  object Label11: TLabel
    Left = 62
    Top = 471
    Width = 36
    Height = 13
    Caption = #20633#35387#65306
  end
  object Label8: TLabel
    Left = 26
    Top = 39
    Width = 72
    Height = 13
    Caption = #23376#20998#39006#27161#38988#65306
  end
  object Label12: TLabel
    Left = 38
    Top = 74
    Width = 60
    Height = 13
    Caption = #38988#24235#27161#38988#65306
  end
  object Label14: TLabel
    Left = 38
    Top = 301
    Width = 60
    Height = 13
    Caption = #35299#31572#38468#20214#65306
  end
  object Label15: TLabel
    Left = 497
    Top = 74
    Width = 48
    Height = 13
    Caption = #29256#26412#34399#65306
  end
  object Label1: TLabel
    Left = 38
    Top = 497
    Width = 60
    Height = 13
    Caption = #26597#30475#31684#22285#65306
  end
  object Label5: TLabel
    Left = 241
    Top = 497
    Width = 84
    Height = 13
    Caption = #20801#35377#26597#30475#37096#38272#65306
  end
  object Label6: TLabel
    Left = 26
    Top = 525
    Width = 72
    Height = 13
    Caption = #37325#35201#24615#24207#34399#65306
  end
  object editMainTitle: TEdit
    Left = 104
    Top = 9
    Width = 556
    Height = 21
    TabStop = False
    Color = clGrayText
    Enabled = False
    TabOrder = 0
  end
  object cbIssueStatus: TComboBox
    Left = 546
    Top = 493
    Width = 108
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = #24050#23529#26680
    OnChange = cbIssueStatusChange
    OnKeyPress = cbIssueStatusKeyPress
  end
  object editRemark: TEdit
    Left = 104
    Top = 467
    Width = 556
    Height = 21
    TabOrder = 5
    OnChange = editSubjectTitleChange
  end
  object memoExplainCont: TMemo
    Left = 104
    Top = 95
    Width = 556
    Height = 192
    MaxLength = 3200
    ScrollBars = ssBoth
    TabOrder = 3
    OnChange = editSubjectTitleChange
  end
  object Button1: TButton
    Left = 199
    Top = 552
    Width = 82
    Height = 27
    Caption = #20462#25913
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 433
    Top = 552
    Width = 82
    Height = 27
    Caption = #21462#28040
    TabOrder = 7
    OnClick = Button2Click
  end
  object editSubTitle: TEdit
    Left = 104
    Top = 35
    Width = 556
    Height = 21
    TabStop = False
    Color = clGrayText
    Enabled = False
    TabOrder = 1
  end
  object editSubjectTitle: TEdit
    Left = 104
    Top = 69
    Width = 382
    Height = 21
    TabOrder = 2
    OnChange = editSubjectTitleChange
  end
  object Button3: TButton
    Left = 633
    Top = 295
    Width = 27
    Height = 21
    Hint = #26032#22686#38468#20214
    Caption = '...'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
    OnClick = Button3Click
  end
  object editVersionNo: TEdit
    Left = 551
    Top = 69
    Width = 109
    Height = 21
    TabStop = False
    TabOrder = 9
    OnChange = editSubjectTitleChange
  end
  object cbSecrecyId: TComboBox
    Left = 104
    Top = 493
    Width = 108
    Height = 21
    ItemHeight = 13
    TabOrder = 10
    OnChange = editSubjectTitleChange
    OnKeyPress = cbIssueStatusKeyPress
    Items.Strings = (
      #24453#23529#26680
      #24050#30332#24067
      #24050#23529#26680
      #20462#35330#20013)
  end
  object cbDepartmentId: TComboBox
    Left = 329
    Top = 493
    Width = 109
    Height = 21
    ItemHeight = 13
    TabOrder = 11
    OnChange = editSubjectTitleChange
    OnKeyPress = cbIssueStatusKeyPress
  end
  object cseOrderNo: TCSpinEdit
    Left = 104
    Top = 520
    Width = 60
    Height = 22
    TabOrder = 12
    OnChange = editSubjectTitleChange
  end
  object lvAppendix: TListView
    Left = 104
    Top = 296
    Width = 521
    Height = 150
    Columns = <
      item
        Caption = 'Id'
      end
      item
        Caption = #27284#26696#21517#31281
        Width = 200
      end
      item
        Caption = #36039#26009#22846
        Width = 260
      end>
    ReadOnly = True
    RowSelect = True
    PopupMenu = PopupMenu1
    TabOrder = 13
    ViewStyle = vsReport
    OnSelectItem = lvAppendixSelectItem
  end
  object OpenDialog2: TOpenDialog
    DefaultExt = '*.doc'
    Filter = 
      'Word'#25991#27284'(*.doc)|*.doc;*.docx|Excel'#25991#27284'(*.txt)|*.xsl;*.xslx|pdf'#25991#27284'(*.p' +
      'df)|*.pdf|'#25991#26412#25991#27284'(*.txt)|*.txt|'#25152#26377#25991#27284'(*.*)|*.*'
    Left = 8
    Top = 160
  end
  object PopupMenu1: TPopupMenu
    Left = 32
    Top = 200
    object N1: TMenuItem
      Caption = #21034#38500#36984#25799#30340#38468#20214
      OnClick = N1Click
    end
  end
end
