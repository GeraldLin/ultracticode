//---------------------------------------------------------------------------
#ifndef ProcWebServiceH
#define ProcWebServiceH

#include <stdio.h>
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "AgentImportDefine.h"
//---------------------------------------------------------------------------
//与第三方WEBSERVICE接口配置参数
class CWebServiceParam
{
public:
  char WS_wsdladdress[256];
  char WS_soapaction[256];
  char WS_startenvelope[256];
  char WS_startbody[256];
  char WS_interface[256];
  short WS_paramcount;
  char WS_paramstring[32][256];
public:
  CWebServiceParam()
  {
    memset(WS_wsdladdress, 0, 256);
    memset(WS_soapaction, 0, 256);
    memset(WS_startenvelope, 0, 256);
    memset(WS_startbody, 0, 256);
    memset(WS_interface, 0, 256);
    WS_paramcount = 0;
    for (int i=0; i<32; i++)
    {
      memset(WS_paramstring[i], 0, 256);
    }
  }
};

//客户资料字段参数定义
typedef struct
{
  char CF_FIELD_STARTTAG[128];
  char CF_FIELD_ENDTAG[128];
  char CF_FIELD_NAME[128];
  
}VXML_CUSTOM_FIELD_STRUCT;

class CCustomerField
{
public:
  bool bResultTag;
  char CF_RESULT_STARTTAG[128];
  char CF_RESULT_ENDTAG[128];
  char CF_RESULT_OK[128];
  char CF_RESULT_FAIL[128];

  bool bRecordNumTag;
  char CF_RECORDNUM_STARTTAG[128];
  char CF_RECORDNUM_ENDTAG[128];

  char INSERTSQL[2048];

  int  FIELDNUM;
  VXML_CUSTOM_FIELD_STRUCT *pFIELD;
public:
  CCustomerField()
  {
    bResultTag = false;
    memset(CF_RESULT_STARTTAG, 0, 128);
    memset(CF_RESULT_ENDTAG, 0, 128);
    memset(CF_RESULT_OK, 0, 128);
    memset(CF_RESULT_FAIL, 0, 128);
    
    bRecordNumTag = false;
    memset(CF_RECORDNUM_STARTTAG, 0, 128);
    memset(CF_RECORDNUM_ENDTAG, 0, 128);
    
    memset(INSERTSQL, 0, 2048);

    FIELDNUM = 0;
    pFIELD = NULL;
  }
  virtual ~CCustomerField()
  {
    if (pFIELD)
    {
      delete [] pFIELD;
      pFIELD = NULL;
      FIELDNUM = 0;
    }
  }
  bool Init(int fieldnum)
  {
    if (fieldnum <= 0)
    {
      return false;
    }
    pFIELD = new VXML_CUSTOM_FIELD_STRUCT[fieldnum];
    if (pFIELD)
    {
      FIELDNUM = fieldnum;
      return true;
    }
    else
    {
      return false;
    }
  }
};

bool Fun_LoadAgentDLL();
bool Fun_FreeAgentDLL();

int SplitString(const char *pzSource, char cSplitChar, TStringList *strList);
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult);
void MyGetMarkerString(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, int nIndex, int nMaxLen, char *pszResult, bool &bFinded);
void ReadCustomerFieldSetup(const char *pszINIFileName);
int ProcQueryCostomerResult(const char *pXMLResult, char *pszClientIDList);

//---------------------------------------------------------------------------
#endif
