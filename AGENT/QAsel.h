//---------------------------------------------------------------------------

#ifndef QAselH
#define QAselH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormQASel : public TForm
{
__published:	// IDE-managed Components
  TRadioGroup *RadioGroup1;
  TRadioGroup *RadioGroup2;
  TButton *Button1;
  TButton *Button2;
  TButton *Button3;
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormQASel(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormQASel *FormQASel;
//---------------------------------------------------------------------------
#endif
