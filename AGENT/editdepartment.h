//---------------------------------------------------------------------------

#ifndef editdepartmentH
#define editdepartmentH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormDepartment : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editDepartmentID;
  TEdit *editDepartmentName;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editDepartmentIDChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDepartment(TComponent* Owner);
  bool InsertId; //true-表示是增加记录 false-表示修改记录
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDepartment *FormDepartment;
//---------------------------------------------------------------------------
#endif
