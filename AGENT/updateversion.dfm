object FormUpdateVersion: TFormUpdateVersion
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #29256#26412#21319#32026#25552#31034
  ClientHeight = 146
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbNewVersion: TLabel
    Left = 17
    Top = 26
    Width = 432
    Height = 17
    Caption = #24744#22909#65292#20282#26381#22120#19978#29694#26377#26368#26032#29256#26412'(X.X.X.X)'#21487#20197#21319#32026#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = #32048#26126#39636
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 95
    Top = 69
    Width = 82
    Height = 27
    Caption = #29694#22312#21319#32026
    Default = True
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 295
    Top = 69
    Width = 81
    Height = 27
    Cancel = True
    Caption = #31245#24460#21319#32026
    TabOrder = 1
    OnClick = Button2Click
  end
  object ckUpdatePrompt: TCheckBox
    Left = 17
    Top = 113
    Width = 149
    Height = 18
    Caption = #19981#20877#25552#31034#21319#32026#20449#24687#65311
    TabOrder = 2
    OnClick = ckUpdatePromptClick
  end
end
