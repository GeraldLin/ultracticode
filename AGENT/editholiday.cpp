//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editholiday.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditHoliday *FormEditHoliday;
//---------------------------------------------------------------------------
__fastcall TFormEditHoliday::TFormEditHoliday(TComponent* Owner)
  : TForm(Owner)
{
  this->InsertId = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditHoliday::cbOpenFlagKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditHoliday::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditHoliday::editHolidayNameChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditHoliday::FormShow(TObject *Sender)
{
  editHolidayName->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditHoliday::Button1Click(TObject *Sender)
{
  int nOpenFlag=0;
  AnsiString strCTID, strHolidayName, strStartDay, strEndDay;

  if (editHolidayName->Text == "")
  {
    MessageBox(NULL,"請輸入國定假期名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  strCTID = editCTID->Text;
  strHolidayName = editHolidayName->Text;
  strStartDay = dtpStartDay->Date.FormatString("yyyy-mm-dd").c_str();
  strEndDay = dtpEndDay->Date.FormatString("yyyy-mm-dd").c_str();
  if (cbOpenFlag->Text == "啟用")
    nOpenFlag = 1;

  if (InsertId == true)
  {
    if (dmAgent->InsertHolidayRecord(strCTID, strHolidayName, strStartDay, strEndDay, nOpenFlag) == 0)
    {
      dmAgent->adoQryHoliday->Close();
      dmAgent->adoQryHoliday->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增國定假期失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateHolidayRecord(nId, strCTID, strHolidayName, strStartDay, strEndDay, nOpenFlag) == 0)
    {
      dmAgent->adoQryHoliday->Close();
      dmAgent->adoQryHoliday->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改國定假期失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
