//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editacptitemlist.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditAcptItemList *FormEditAcptItemList;
//---------------------------------------------------------------------------
__fastcall TFormEditAcptItemList::TFormEditAcptItemList(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}

//---------------------------------------------------------------------------
void TFormEditAcptItemList::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    editItemNo->ReadOnly = false;
    editItemNo->Color = clWindow;
    FormEditAcptItemList->Caption = "新增輸入框選擇項資料";
  }
  else
  {
    Button1->Caption = "修改";
    editItemNo->ReadOnly = true;
    editItemNo->Color = clSilver;
    FormEditAcptItemList->Caption = "修改輸入框選擇項資料";
  }
}

void TFormEditAcptItemList::SetItemData(CAcptItemSet& acptitemset, AnsiString strItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo)
{
  if (acptitemset.TControlType == 10)
  {
    FormEditAcptItemList->Height = 470;
    Label5->Visible = true;
    editItemURL->Visible = true;
    Label6->Visible = true;
    memoItemDemo->Visible = true;
    Button1->Top = 375;
    Button2->Top = 375;
  }
  else
  {
    FormEditAcptItemList->Height = 195;
    Label5->Visible = false;
    editItemURL->Visible = false;
    Label6->Visible = false;
    memoItemDemo->Visible = false;
    Button1->Top = 105;
    Button2->Top = 105;
  }

  AcptItemSet = acptitemset;
  editSrvType->Text = dmAgent->SrvTypeItemList.GetItemName(acptitemset.ProdType, acptitemset.SrvType);
  editLabelCaption->Text = acptitemset.TLabelCaption;
  editItemNo->Text = strItemNo;
  editItemName->Text = strItemName;
  editItemURL->Text = strItemURL;
  memoItemDemo->Lines->Clear();
  if (strItemDemo.Length() > 0)
    memoItemDemo->Lines->Add(strItemDemo);
  Button1->Enabled = false;
}

void __fastcall TFormEditAcptItemList::Button2Click(TObject *Sender)
{
  this->Close();
}

//---------------------------------------------------------------------------
void __fastcall TFormEditAcptItemList::editItemNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}

//---------------------------------------------------------------------------
void __fastcall TFormEditAcptItemList::Button1Click(TObject *Sender)
{
  int nItemNo;

  if (MyIsNumber(editItemNo->Text) != 0)
  {
    MessageBox(NULL,"請輸入下拉框選擇項編號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,"請輸入下拉框選擇項名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  nItemNo = StrToInt(editItemNo->Text);
  if (AcptItemSet.TMinValue != 0 && AcptItemSet.TMaxValue != 0 && (nItemNo < AcptItemSet.TMinValue || nItemNo > AcptItemSet.TMaxValue))
  {
    MessageBox(NULL,"對不起，您輸入的下拉框選擇項編號超出允許範圍！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsAcptItemListExist(AcptItemSet.ProdType, AcptItemSet.SrvType, AcptItemSet.SrvSubType, AcptItemSet.Tid, nItemNo) == 1)
    {
      MessageBox(NULL,"對不起，該下拉框選擇項編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertAcptItemListRecord(AcptItemSet.ProdType, AcptItemSet.SrvType, AcptItemSet.SrvSubType, AcptItemSet.Tid, nItemNo, editItemName->Text, editItemURL->Text, memoItemDemo->Lines->Text) == 0)
    {
      dmAgent->adoQryAcptItemList->Close();
      dmAgent->adoQryAcptItemList->Open();
      this->Close();
      //MessageBox(NULL,"新增下拉框選擇項編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增下拉框選擇項編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateAcptItemListRecord(AcptItemSet.ProdType, AcptItemSet.SrvType, AcptItemSet.SrvSubType, AcptItemSet.Tid, nItemNo, editItemName->Text, editItemURL->Text, memoItemDemo->Lines->Text) == 0)
    {
      dmAgent->adoQryAcptItemList->Close();
      dmAgent->adoQryAcptItemList->Open();
      this->Close();
      //MessageBox(NULL,"修改下拉框選擇項編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改下拉框選擇項編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}

//---------------------------------------------------------------------------
void __fastcall TFormEditAcptItemList::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditAcptItemList->editItemNo->SetFocus();
  else
    FormEditAcptItemList->editItemName->SetFocus();
}
//---------------------------------------------------------------------------


