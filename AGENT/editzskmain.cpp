//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editzskmain.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormZSKMain *FormZSKMain;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormZSKMain::TFormZSKMain(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void TFormZSKMain::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormZSKMain->Caption = "新增業務知識庫主分類訊息";
  }
  else
  {
    Button1->Caption = "修改";
    FormZSKMain->Caption = "修改業務知識庫主分類訊息";
  }
}
void TFormZSKMain::SetZSKMainData(CZSKMain& zskmain)
{
  ZSKMain = zskmain;
  editMainTitle->Text = zskmain.MainTitle;
  memoExplain->Lines->Clear();
  memoExplain->Lines->Add(zskmain.Explain);
  editRemark->Text = zskmain.Remark;
  cseOrderNo->Value = zskmain.orderno1;

  Button1->Enabled = false;
}
int TFormZSKMain::GetZSKMainData()
{
  //ZSKMain.MainNo;
  if (editMainTitle->Text == "")
  {
    MessageBox(NULL,"請輸入主分類標題！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  ZSKMain.MainTitle = editMainTitle->Text;
  ZSKMain.Explain = memoExplain->Lines->Text;
  ZSKMain.Remark = editRemark->Text;
  ZSKMain.orderno1 = cseOrderNo->Value;
  return 0;
}
void TFormZSKMain::ClearZSKMainData()
{
  ZSKMain.MainNo = 0;
  editMainTitle->Text = "";
  memoExplain->Lines->Clear();
  editRemark->Text = "";
  Button1->Enabled = false;
}

void __fastcall TFormZSKMain::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKMain::editMainTitleChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKMain::Button1Click(TObject *Sender)
{
  if (GetZSKMainData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertZSKMainRecord(ZSKMain) == 0)
    {
      dmAgent->RefreshZSKMain();
      dmAgent->InsertOPLog(MySeat, MyWorker, 1030, "值機員新增業務知識庫為：%s的主分類訊息資料", ZSKMain.MainTitle.c_str());
      //MessageBox(NULL,"新增業務知識庫主分類訊息成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增業務知識庫主分類訊息失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateZSKMainRecord(ZSKMain) == 0)
    {
      dmAgent->RefreshZSKMain();
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員修改業務知識庫為：%s的主分類訊息資料", ZSKMain.MainTitle.c_str());
      //MessageBox(NULL,"修改業務知識庫主分類訊息成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改業務知識庫主分類訊息失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKMain::FormShow(TObject *Sender)
{
  FormZSKMain->editMainTitle->SetFocus();
}
//---------------------------------------------------------------------------


