object FormDispCustom: TFormDispCustom
  Left = 241
  Top = 118
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #20358#38651#32773#36039#26009
  ClientHeight = 397
  ClientWidth = 1152
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 550
    Top = 355
    Width = 81
    Height = 27
    Caption = #38364#38281
    TabOrder = 0
    OnClick = Button1Click
  end
  object gbCustBase: TGroupBox
    Left = 9
    Top = 9
    Width = 1136
    Height = 165
    Caption = #20358#38651#32773#22522#26412#36039#26009#65306
    TabOrder = 1
    object Label2: TLabel
      Left = 18
      Top = 22
      Width = 72
      Height = 13
      Caption = #20358#38651#32773#32232#34399#65306
    end
    object Label3: TLabel
      Left = 18
      Top = 69
      Width = 72
      Height = 13
      Caption = #20358#38651#32773#22995#21517#65306
    end
    object Label4: TLabel
      Left = 30
      Top = 92
      Width = 60
      Height = 13
      Caption = #20844#21496#21517#31281#65306
    end
    object Label5: TLabel
      Left = 18
      Top = 44
      Width = 72
      Height = 13
      Caption = #20358#38651#32773#36523#20221#65306
    end
    object Label6: TLabel
      Left = 270
      Top = 68
      Width = 36
      Height = 13
      Caption = #24615#21029#65306
    end
    object Label7: TLabel
      Left = 472
      Top = 46
      Width = 36
      Height = 13
      Caption = #25163#27231#65306
    end
    object Label8: TLabel
      Left = 650
      Top = 46
      Width = 60
      Height = 13
      Caption = #38651#35441#34399#30908#65306
    end
    object Label9: TLabel
      Left = 472
      Top = 69
      Width = 36
      Height = 13
      Caption = #20659#30495#65306
    end
    object Label10: TLabel
      Left = 650
      Top = 68
      Width = 60
      Height = 13
      Caption = #38651#23376#37109#20214#65306
    end
    object Label11: TLabel
      Left = 472
      Top = 92
      Width = 36
      Height = 13
      Caption = #22320#22336#65306
    end
    object Label12: TLabel
      Left = 30
      Top = 117
      Width = 60
      Height = 13
      Caption = #37109#36958#21312#30908#65306
    end
    object Label1: TLabel
      Left = 54
      Top = 141
      Width = 36
      Height = 13
      Caption = #20633#35387#65306
    end
    object Label14: TLabel
      Left = 270
      Top = 116
      Width = 36
      Height = 13
      Caption = #32291#24066#65306
    end
    object Label15: TLabel
      Left = 448
      Top = 117
      Width = 60
      Height = 13
      Caption = #37129#37806#24066#21312#65306
    end
    object Label16: TLabel
      Left = 674
      Top = 141
      Width = 36
      Height = 13
      Caption = #26449#37324#65306
      Enabled = False
      Visible = False
    end
    object Label13: TLabel
      Left = 650
      Top = 116
      Width = 60
      Height = 13
      Caption = #26597#35426#31684#22285#65306
    end
    object Label17: TLabel
      Left = 448
      Top = 140
      Width = 60
      Height = 13
      Caption = #23567#21312#27155#30436#65306
      Enabled = False
      Visible = False
    end
    object Label18: TLabel
      Left = 243
      Top = 46
      Width = 60
      Height = 13
      Caption = #30003#35380#23565#35937#65306
    end
    object Label19: TLabel
      Left = 634
      Top = 20
      Width = 72
      Height = 13
      Caption = #36774#20844#23460#38651#35441#65306
    end
    object lbAccountNo: TLabel
      Left = 441
      Top = 20
      Width = 60
      Height = 13
      Caption = #32113#19968#32232#34399#65306
    end
    object edtCustomNo: TEdit
      Left = 100
      Top = 17
      Width = 206
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 0
    end
    object edtCustName: TEdit
      Left = 100
      Top = 65
      Width = 108
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtCorpName: TEdit
      Left = 100
      Top = 89
      Width = 314
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object edtMobileNo: TEdit
      Left = 511
      Top = 41
      Width = 109
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
    object edtTeleNo: TEdit
      Left = 717
      Top = 41
      Width = 109
      Height = 21
      ReadOnly = True
      TabOrder = 4
    end
    object edtFaxNo: TEdit
      Left = 511
      Top = 65
      Width = 109
      Height = 21
      ReadOnly = True
      TabOrder = 5
    end
    object edtEMail: TEdit
      Left = 717
      Top = 65
      Width = 109
      Height = 21
      ReadOnly = True
      TabOrder = 6
    end
    object edtContAddr: TEdit
      Left = 511
      Top = 89
      Width = 315
      Height = 21
      ReadOnly = True
      TabOrder = 7
    end
    object edtPostNo: TEdit
      Left = 100
      Top = 113
      Width = 108
      Height = 21
      ReadOnly = True
      TabOrder = 8
    end
    object edtRemark: TEdit
      Left = 100
      Top = 137
      Width = 314
      Height = 21
      ReadOnly = True
      TabOrder = 9
    end
    object edtCustSex: TComboBox
      Left = 306
      Top = 65
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 10
      Items.Strings = (
        #30007
        #22899)
    end
    object edtCustType: TComboBox
      Left = 100
      Top = 41
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 11
      Items.Strings = (
        #26222#36890#23458#25142
        #39640#32026#23458#25142
        'VIP'#23458#25142)
    end
    object cbProvince: TComboBox
      Left = 306
      Top = 113
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 12
    end
    object cbCityName: TComboBox
      Left = 511
      Top = 113
      Width = 109
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 13
    end
    object cbCountyName: TComboBox
      Left = 717
      Top = 137
      Width = 109
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 14
      Visible = False
    end
    object cbCustClass: TComboBox
      Left = 717
      Top = 113
      Width = 109
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 15
      Text = #20840#37096#21487#26597#30475
    end
    object edtZoneName: TEdit
      Left = 511
      Top = 137
      Width = 109
      Height = 21
      ReadOnly = True
      TabOrder = 16
      Visible = False
    end
    object edtCustLevel: TComboBox
      Left = 306
      Top = 41
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 17
      Items.Strings = (
        #26222#36890#23458#25142
        #39640#32026#23458#25142
        'VIP'#23458#25142)
    end
    object edtSubPhone: TEdit
      Left = 717
      Top = 15
      Width = 109
      Height = 21
      ReadOnly = True
      TabOrder = 18
    end
    object edtAccountNo: TEdit
      Left = 511
      Top = 16
      Width = 110
      Height = 21
      ReadOnly = True
      TabOrder = 19
    end
  end
  object gbCustEx: TGroupBox
    Left = 9
    Top = 179
    Width = 1136
    Height = 171
    Caption = #20358#38651#32773#25844#20805#36039#26009#65306
    TabOrder = 2
    object sbCustEx: TScrollBox
      Left = 2
      Top = 15
      Width = 1132
      Height = 154
      Align = alClient
      TabOrder = 0
    end
  end
end
