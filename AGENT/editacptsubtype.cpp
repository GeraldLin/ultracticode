//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editacptsubtype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAcptSubType *FormAcptSubType;
//---------------------------------------------------------------------------
__fastcall TFormAcptSubType::TFormAcptSubType(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormAcptSubType::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormAcptSubType::editAcptSubTypeIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcptSubType::Button1Click(TObject *Sender)
{
  int nSubType;

  if (MyIsNumber(editAcptSubTypeId->Text) != 0)
  {
    MessageBox(NULL,"請輸入工單類型編號！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editAcptSubTypeName->Text == "")
  {
    MessageBox(NULL,"請輸入工單類型名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nSubType = StrToInt(editAcptSubTypeId->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsAcptSubTypeExist(nSubType) == 1)
    {
      MessageBox(NULL,"對不起，該工單類型編號已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertAcptSubTypeRecord(nSubType, editAcptSubTypeName->Text, memoRemark->Text) == 0)
    {
      dmAgent->adoQryAcptSubType->Close();
      dmAgent->adoQryAcptSubType->Open();
      dmAgent->UpdateAcptSubTypeListItems();
      MessageBox(NULL,"增加工單類型編號成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加工單類型編號失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateAcptSubTypeRecord(nSubType, editAcptSubTypeName->Text, memoRemark->Text) == 0)
    {
      dmAgent->adoQryAcptSubType->Close();
      dmAgent->adoQryAcptSubType->Open();
      dmAgent->UpdateAcptSubTypeListItems();
      MessageBox(NULL,"修改工單類型編號成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改工單類型編號失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

