object FormEditManDialOutTask: TFormEditManDialOutTask
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20154#24037#22806#21628#20219#21209#35373#23450
  ClientHeight = 424
  ClientWidth = 845
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 57
    Top = 21
    Width = 60
    Height = 12
    Caption = #20219#21209#21517#31281#65306
  end
  object Label8: TLabel
    Left = 9
    Top = 47
    Width = 108
    Height = 12
    Caption = #22806#25765#36865#30340#20027#21483#34399#30908#65306
  end
  object Label6: TLabel
    Left = 341
    Top = 47
    Width = 84
    Height = 12
    Caption = #21855#21205#22806#25765#27161#35468#65306
  end
  object Label2: TLabel
    Left = 33
    Top = 75
    Width = 84
    Height = 12
    Caption = #38283#22987#22806#25765#26085#26399#65306
  end
  object Label3: TLabel
    Left = 292
    Top = 75
    Width = 84
    Height = 12
    Caption = #32080#26463#22806#25765#26085#26399#65306
  end
  object Label4: TLabel
    Left = 57
    Top = 102
    Width = 60
    Height = 12
    Caption = #38468#21152#21443#25976#65306
  end
  object Label5: TLabel
    Left = 57
    Top = 247
    Width = 60
    Height = 12
    Caption = #20219#21209#25551#36848#65306
  end
  object Label7: TLabel
    Left = 32
    Top = 126
    Width = 83
    Height = 12
    Caption = #25104#20132#20462#25913'URL'#65306
  end
  object Label9: TLabel
    Left = 277
    Top = 21
    Width = 60
    Height = 12
    Caption = #20219#21209#32232#34399#65306
  end
  object Label10: TLabel
    Left = 57
    Top = 150
    Width = 59
    Height = 12
    Caption = #25104#20132'URL'#65306
  end
  object Label11: TLabel
    Left = 57
    Top = 174
    Width = 59
    Height = 12
    Caption = #26597#35426'URL'#65306
  end
  object Label12: TLabel
    Left = 32
    Top = 198
    Width = 83
    Height = 12
    Caption = #25163#21205#25104#20132'URL'#65306
  end
  object Label13: TLabel
    Left = 57
    Top = 222
    Width = 59
    Height = 12
    Caption = #20729#30446'URL'#65306
  end
  object Label14: TLabel
    Left = 544
    Top = 75
    Width = 63
    Height = 12
    Caption = #22806#25765#25235#21462#30908':'
  end
  object editTaskName: TEdit
    Left = 123
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editTaskNameChange
  end
  object editCallerNo: TEdit
    Left = 123
    Top = 44
    Width = 131
    Height = 20
    Hint = #21482#36969#29992#19968#39636#27231
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = editTaskNameChange
  end
  object cbStartId: TComboBox
    Left = 430
    Top = 44
    Width = 42
    Height = 20
    ItemHeight = 12
    TabOrder = 2
    Text = #26159
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #26159
      #21542)
  end
  object dtpStartDate: TDateTimePicker
    Left = 123
    Top = 71
    Width = 92
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 3
    OnChange = editTaskNameChange
  end
  object dtpEndDate: TDateTimePicker
    Left = 381
    Top = 71
    Width = 92
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 4
    OnChange = editTaskNameChange
  end
  object editParam: TEdit
    Left = 123
    Top = 97
    Width = 710
    Height = 20
    TabOrder = 5
    OnChange = editTaskNameChange
  end
  object memoRemark: TMemo
    Left = 123
    Top = 247
    Width = 710
    Height = 106
    MaxLength = 3000
    ScrollBars = ssBoth
    TabOrder = 6
    OnChange = editTaskNameChange
  end
  object Button1: TButton
    Left = 270
    Top = 376
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 521
    Top = 376
    Width = 82
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 8
    OnClick = Button2Click
  end
  object editURLAddr: TEdit
    Left = 123
    Top = 121
    Width = 710
    Height = 20
    TabOrder = 9
    OnChange = editTaskNameChange
  end
  object editTaskCode: TEdit
    Left = 341
    Top = 17
    Width = 131
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 10
    OnChange = editTaskNameChange
  end
  object editURLOrder: TEdit
    Left = 123
    Top = 145
    Width = 710
    Height = 20
    TabOrder = 11
    OnChange = editTaskNameChange
  end
  object editURLQuery: TEdit
    Left = 123
    Top = 169
    Width = 710
    Height = 20
    TabOrder = 12
    OnChange = editTaskNameChange
  end
  object editURLManOrder: TEdit
    Left = 123
    Top = 193
    Width = 710
    Height = 20
    TabOrder = 13
    OnChange = editTaskNameChange
  end
  object editURLPrice: TEdit
    Left = 123
    Top = 217
    Width = 710
    Height = 20
    TabOrder = 14
    OnChange = editTaskNameChange
  end
  object editDialPreCode: TEdit
    Left = 619
    Top = 71
    Width = 131
    Height = 20
    TabOrder = 15
    OnChange = editTaskNameChange
  end
end
