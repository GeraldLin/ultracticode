object FormEditOrderList: TFormEditOrderList
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #21457#36135#24773#20917#30331#35760
  ClientHeight = 179
  ClientWidth = 511
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 33
    Top = 12
    Width = 70
    Height = 13
    Caption = #23450#21333#32534#21495#65306
  end
  object Label2: TLabel
    Left = 289
    Top = 36
    Width = 70
    Height = 13
    Caption = #20135#21697#31867#22411#65306
  end
  object Label3: TLabel
    Left = 289
    Top = 12
    Width = 70
    Height = 13
    Caption = #20135#21697#22411#21495#65306
  end
  object Label4: TLabel
    Left = 21
    Top = 68
    Width = 84
    Height = 13
    Caption = #24050#21457#36135#25968#37327#65306
  end
  object Label5: TLabel
    Left = 28
    Top = 92
    Width = 70
    Height = 13
    Caption = #21830#21697#26465#30721#65306
  end
  object Label6: TLabel
    Left = 33
    Top = 36
    Width = 70
    Height = 13
    Caption = #35746#36141#25968#37327#65306
  end
  object Label7: TLabel
    Left = 96
    Top = 120
    Width = 177
    Height = 13
    AutoSize = False
    Caption = #22791#27880#65306#26377#22810#20010#26102#29992#36887#21495#38548#24320
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object editOrderId: TEdit
    Left = 96
    Top = 8
    Width = 145
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editProductName: TEdit
    Left = 352
    Top = 32
    Width = 145
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 1
  end
  object editProductNo: TEdit
    Left = 352
    Top = 8
    Width = 145
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 2
  end
  object cseExpressTotals: TCSpinEdit
    Left = 96
    Top = 64
    Width = 65
    Height = 22
    TabOrder = 3
    OnChange = cseExpressTotalsChange
  end
  object editBarCode: TEdit
    Left = 96
    Top = 88
    Width = 401
    Height = 21
    TabOrder = 4
    OnChange = cseExpressTotalsChange
  end
  object editTotals: TEdit
    Left = 96
    Top = 32
    Width = 145
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 5
  end
  object Button1: TButton
    Left = 104
    Top = 144
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 328
    Top = 144
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 7
    OnClick = Button2Click
  end
end
