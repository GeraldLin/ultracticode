object FormEditSelItem: TFormEditSelItem
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #36984#25799#21443#25976#38917#35373#23450
  ClientHeight = 169
  ClientWidth = 405
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lbItemId: TLabel
    Left = 17
    Top = 21
    Width = 84
    Height = 12
    Caption = #36984#25799#38917#30446#25976#20540#65306
  end
  object lbItemName: TLabel
    Left = 17
    Top = 48
    Width = 84
    Height = 12
    Caption = #36984#25799#38917#30446#21517#31281#65306
  end
  object lbURLAddr: TLabel
    Left = 71
    Top = 72
    Width = 30
    Height = 12
    Caption = 'URL'#65306
    Visible = False
  end
  object Label1: TLabel
    Left = 47
    Top = 97
    Width = 54
    Height = 12
    Caption = 'URL'#39006#21029#65306
  end
  object editItemId: TEdit
    Left = 106
    Top = 17
    Width = 287
    Height = 20
    TabOrder = 0
    OnChange = editItemIdChange
  end
  object editItemName: TEdit
    Left = 106
    Top = 44
    Width = 287
    Height = 20
    TabOrder = 1
    OnChange = editItemIdChange
  end
  object Button1: TButton
    Left = 99
    Top = 127
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 237
    Top = 127
    Width = 82
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editURLAddr: TEdit
    Left = 106
    Top = 68
    Width = 287
    Height = 20
    TabOrder = 4
    Visible = False
    OnChange = editItemIdChange
  end
  object cseSortType: TCSpinEdit
    Left = 106
    Top = 93
    Width = 121
    Height = 21
    MaxValue = 3
    MinValue = 1
    TabOrder = 5
    Value = 1
    OnChange = editItemIdChange
  end
end
