//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <inifiles.hpp>
#include "dialseatno.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDialSeatNo *FormDialSeatNo;
extern BSTR bEmpty;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
extern CCallMsg CallMsg;

//話務員密碼驗證成功
extern bool WorkerLoginId;
extern int g_nBandSeatNoType;
extern int TranAnsId;
//---------------------------------------------------------------------------
__fastcall TFormDialSeatNo::TFormDialSeatNo(TComponent* Owner)
  : TForm(Owner)
{
  DialFlag = 0;
  if (g_nBandSeatNoType == 0)
    dmAgent->ReadTranSeatFuncKey();
}
//---------------------------------------------------------------------------
void __fastcall TFormDialSeatNo::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormDialSeatNo::Button1Click(TObject *Sender)
{
  AnsiString SeatNo="", TranParam;
  WideString wSeatNo, wTranParam;
  BSTR bSeatNo, bTranParam;

  int workerno;

  if (RadioGroup1->ItemIndex == 0)
  {
    SeatNo = cbSeatNo->Text;
  }
  else
  {
    try
    {
      workerno = StrToInt(cbWorkerNo->Text);
      SeatNo = FormMain->SeatList.GetSeatNo(workerno);
    }
    catch (...)
    {
      MessageBox(NULL,"工號輸入錯誤!!!","警告",MB_OK|MB_ICONINFORMATION);
      return;
    }
  }

  if (SeatNo.Length() > 0)
  {
    wSeatNo = WideString(SeatNo);
    bSeatNo = ( wchar_t * )wSeatNo;
    switch (DialFlag)
    {
    case 1: //1-轉接坐席
      if (CallMsg.QYBH == "")
      {
        if (ckTranType->Checked == false)
          FormMain->MyAgent1->ConsultTranCall(2,bSeatNo,bEmpty);
        else
          FormMain->MyAgent1->BlindTranCall(2,bSeatNo,bEmpty);
      }
      else
      {
        TranParam = "ItemData1="+CallMsg.QYBH;
        wTranParam = WideString(TranParam);
        bTranParam = ( wchar_t * )wTranParam;
        if (ckTranType->Checked == false)
          FormMain->MyAgent1->ConsultTranCall(2,bSeatNo,bTranParam);
        else
          FormMain->MyAgent1->BlindTranCall(2,bSeatNo,bTranParam);
      }

      FormMain->ToolButton8->Enabled = true;
      TranAnsId = 1;
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "轉接電話到坐席%s", SeatNo.c_str());
      break;
    case 2: //2-代接
      FormMain->MyAgent1->Pickup(bSeatNo,0,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "代接坐席%s的電話", SeatNo.c_str());
      break;
    case 3: //3-強插
      FormMain->MyAgent1->Insert(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "插話坐席%s", SeatNo.c_str());
      break;
    case 4: //4-監聽
      FormMain->MyAgent1->Listen(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "監聽坐席%s", SeatNo.c_str());
      break;
    case 5: //5-強拆
      FormMain->MyAgent1->ForceClear(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "斷話坐席%s", SeatNo.c_str());
      break;
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormDialSeatNo::RadioGroup1Click(TObject *Sender)
{
  if (RadioGroup1->ItemIndex == 0)
  {
    Label1->Visible = true;
    cbSeatNo->Visible = true;
    Label2->Visible = false;
    cbWorkerNo->Visible = false;
    cbSeatNo->SetFocus();
  }
  else
  {
    Label1->Visible = false;
    cbSeatNo->Visible = false;
    Label2->Visible = true;
    cbWorkerNo->Visible = true;
    cbWorkerNo->SetFocus();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::Button3Click(TObject *Sender)
{
  AnsiString SeatNo="";
  WideString wSeatNo;
  BSTR bSeatNo;

  int workerno;

  if (RadioGroup1->ItemIndex == 0)
  {
    SeatNo = cbSeatNo->Text;
  }
  else
  {
    try
    {
      workerno = StrToInt(cbWorkerNo->Text);
      SeatNo = FormMain->SeatList.GetSeatNo(workerno);
    }
    catch (...)
    {
      MessageBox(NULL,"工號輸入錯誤!!!","警告",MB_OK|MB_ICONINFORMATION);
      return;
    }
  }

  if (SeatNo.Length() > 0)
  {
    wSeatNo = WideString(SeatNo);
    bSeatNo = ( wchar_t * )wSeatNo;
    FormMain->MyAgent1->ConfTranCall(2,bSeatNo,0,bEmpty);
    this->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  WideString wSeatNo;
  BSTR bSeatNo;
  AnsiString strSeatNo="", strWorkerNo="";
  int nFuncKeyId=0;
  int workerno;

  switch (Key)
  {
  case VK_F1:
    nFuncKeyId = 1;
    break;
  case VK_F2:
    nFuncKeyId = 2;
    break;
  case VK_F3:
    nFuncKeyId = 3;
    break;
  case VK_F4:
    nFuncKeyId = 4;
    break;
  case VK_F5:
    nFuncKeyId = 5;
    break;
  case VK_F6:
    nFuncKeyId = 6;
    break;
  case VK_F7:
    nFuncKeyId = 7;
    break;
  case VK_F8:
    nFuncKeyId = 8;
    break;
  case VK_F9:
    nFuncKeyId = 9;
    break;
  case VK_F10:
    nFuncKeyId = 10;
    break;
  case VK_F11:
    nFuncKeyId = 11;
    break;
  case VK_F12:
    nFuncKeyId = 12;
    break;
  }

  if (nFuncKeyId == 0)
    return;
  if (dmAgent->GetTranSeatFuncKey(nFuncKeyId, strSeatNo, strWorkerNo) == 0)
    return;
  if (strSeatNo == "" && strWorkerNo == "")
    return;
  if (strSeatNo == "")
  {
    try
    {
      workerno = StrToInt(strWorkerNo);
      strSeatNo = FormMain->SeatList.GetSeatNo(workerno);
    }
    catch (...)
    {
      return;
    }
  }
  if (strSeatNo.Length() > 0)
  {
    wSeatNo = WideString(strSeatNo);
    bSeatNo = ( wchar_t * )wSeatNo;
    switch (DialFlag)
    {
    case 1: //1-轉接坐席
      if (ckTranType->Checked == false)
        FormMain->MyAgent1->ConsultTranCall(2,bSeatNo,bEmpty);
      else
        FormMain->MyAgent1->BlindTranCall(2,bSeatNo,bEmpty);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "轉接電話到坐席%s", SeatNo.c_str());
      break;
    case 2: //2-代接
      FormMain->MyAgent1->Pickup(bSeatNo,0,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "代接坐席%s的電話", SeatNo.c_str());
      break;
    case 3: //3-強插
      FormMain->MyAgent1->Insert(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "插話坐席%s", SeatNo.c_str());
      break;
    case 4: //4-監聽
      FormMain->MyAgent1->Listen(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "監聽坐席%s", SeatNo.c_str());
      break;
    case 5: //5-強拆
      FormMain->MyAgent1->ForceClear(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "斷話坐席%s", SeatNo.c_str());
      break;
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------
void TFormDialSeatNo::FuncKeyTran(int nFuncKeyId)
{
  WideString wSeatNo;
  BSTR bSeatNo;
  AnsiString strSeatNo="", strWorkerNo="";
  int workerno;

  if (dmAgent->GetTranSeatFuncKey(nFuncKeyId, strSeatNo, strWorkerNo) == 0)
    return;
  if (strSeatNo == "" && strWorkerNo == "")
    return;
  if (strSeatNo == "")
  {
    try
    {
      workerno = StrToInt(strWorkerNo);
      strSeatNo = FormMain->SeatList.GetSeatNo(workerno);
    }
    catch (...)
    {
      return;
    }
  }
  if (strSeatNo.Length() > 0)
  {
    wSeatNo = WideString(strSeatNo);
    bSeatNo = ( wchar_t * )wSeatNo;
    switch (DialFlag)
    {
    case 1: //1-轉接坐席
      if (ckTranType->Checked == false)
        FormMain->MyAgent1->ConsultTranCall(2,bSeatNo,bEmpty);
      else
        FormMain->MyAgent1->BlindTranCall(2,bSeatNo,bEmpty);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "轉接電話到坐席%s", SeatNo.c_str());
      break;
    case 2: //2-代接
      FormMain->MyAgent1->Pickup(bSeatNo,0,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "代接坐席%s的電話", SeatNo.c_str());
      break;
    case 3: //3-強插
      FormMain->MyAgent1->Insert(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "插話坐席%s", SeatNo.c_str());
      break;
    case 4: //4-監聽
      FormMain->MyAgent1->Listen(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "監聽坐席%s", SeatNo.c_str());
      break;
    case 5: //5-強拆
      FormMain->MyAgent1->ForceClear(bSeatNo,0);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "斷話坐席%s", SeatNo.c_str());
      break;
    }
    this->Close();
  }
}

void __fastcall TFormDialSeatNo::btFuncF1Click(TObject *Sender)
{
  FuncKeyTran(1);
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF2Click(TObject *Sender)
{
  FuncKeyTran(2);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF3Click(TObject *Sender)
{
  FuncKeyTran(3);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF4Click(TObject *Sender)
{
  FuncKeyTran(4);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF5Click(TObject *Sender)
{
  FuncKeyTran(5);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF6Click(TObject *Sender)
{
  FuncKeyTran(6);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF7Click(TObject *Sender)
{
  FuncKeyTran(7);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF8Click(TObject *Sender)
{
  FuncKeyTran(8);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF9Click(TObject *Sender)
{
  FuncKeyTran(9);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF10Click(TObject *Sender)
{
  FuncKeyTran(10);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF11Click(TObject *Sender)
{
  FuncKeyTran(11);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btFuncF12Click(TObject *Sender)
{
  FuncKeyTran(12);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDialSeatNo::btUpDownClick(TObject *Sender)
{
  static bool nVsible=true;
  if (nVsible == true)
  {
    nVsible = false;
    gbFastDial->Visible = false;
    btUpDown->Hint = "顯示快速撥號功能鍵";
    ckTranType->Top = 159;
    FormDialSeatNo->Height = 239;
  }
  else
  {
    nVsible = true;
    gbFastDial->Visible = true;
    btUpDown->Hint = "隱藏快速撥號功能鍵";
    ckTranType->Top = 329;
    FormDialSeatNo->Height = 409;
  }
}
//---------------------------------------------------------------------------

