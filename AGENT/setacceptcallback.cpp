//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "setacceptcallback.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormSetAcceptCallBack *FormSetAcceptCallBack;
//---------------------------------------------------------------------------
__fastcall TFormSetAcceptCallBack::TFormSetAcceptCallBack(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSetAcceptCallBack::Button1Click(TObject *Sender)
{
  AnsiString strTime = MonthCalendar1->Date.FormatString("yyyy-mm-dd ")+IntToStr(CSpinEdit1->Value)+":"+IntToStr(CSpinEdit2->Value)+":00";
  dmAgent->UpdateAcceptCallBackTime(GId, 1, strTime);
  try
  {
    dmAgent->adoQryOldAccept->Close();
    dmAgent->adoQryOldAccept->Open();
    dmAgent->QueryAcceptCallBackRecords();
  }
  catch (...)
  {
  }
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSetAcceptCallBack::Button2Click(TObject *Sender)
{
  dmAgent->UpdateAcceptCallBackTime(GId, 0, "");
  try
  {
    dmAgent->adoQryOldAccept->Close();
    dmAgent->adoQryOldAccept->Open();
    dmAgent->QueryAcceptCallBackRecords();
  }
  catch (...)
  {
  }
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSetAcceptCallBack::Button3Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
