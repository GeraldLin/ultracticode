object FormAcceptProcId: TFormAcceptProcId
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24037#21333#22788#29702#32467#26524#35774#32622
  ClientHeight = 117
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 19
    Width = 108
    Height = 12
    Caption = #24037#21333#22788#29702#32467#26524#32534#21495#65306
  end
  object Label2: TLabel
    Left = 16
    Top = 51
    Width = 108
    Height = 12
    Caption = #24037#21333#22788#29702#32467#26524#21517#31216#65306
  end
  object editAcceptProcId: TEdit
    Left = 128
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editAcceptProcIdChange
  end
  object editAcceptProcName: TEdit
    Left = 128
    Top = 48
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editAcceptProcIdChange
  end
  object Button1: TButton
    Left = 40
    Top = 80
    Width = 75
    Height = 25
    Caption = #30830#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 152
    Top = 80
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
end
