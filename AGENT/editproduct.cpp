//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editproduct.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditProduct *FormEditProduct;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
extern AnsiString ShareDisk;
AnsiString ProductImageFile;
bool bCopyProductImage=false;

//---------------------------------------------------------------------------
__fastcall TFormEditProduct::TFormEditProduct(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditProduct::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "增加";
    FormEditProduct->Caption = "增加產品資料";
  }
  else
  {
    Button1->Caption = "修改";
    FormEditProduct->Caption = "修改產品資料";
  }
}
void TFormEditProduct::SetProducts(CProducts &products)
{
  Products = products;
  editProductNo->Text = products.ProductNo;
  editName->Text = products.ProductName;
  editProductType->Text = products.ProductType;
  editBrand->Text = products.Brand;
  editSupplierName->Text = products.SupplierName;
  editSupplierMem->Text = products.SupplierMem;
  editSupplierTel->Text = products.SupplierTel;
  editSupplierAddr->Text = products.SupplierAddr;
  editShortDesc->Text = products.ShortDesc;
  editUnit->Text = products.Unit;
  editWeight->Text = products.Weight;
  editMarketPrice->Text = products.MarketPrice;
  editMemberPrice->Text = products.MemberPrice;
  editProductImg->Text = products.ProductImg;
  memoDescription->Lines->Clear();
  memoDescription->Lines->Add(products.Description);
  editStorage->Text = products.Storage;
  editRemark->Text = products.Remark;
  Button1->Enabled = false;
}
int TFormEditProduct::GetProducts()
{
  if (editProductNo->Text == "")
  {
    MessageBox(NULL,"請輸入產品編號！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  Products.ProductNo = editProductNo->Text;
  if (editName->Text == "")
  {
    MessageBox(NULL,"請輸入產品名稱！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  Products.ProductName = editName->Text;
  Products.ProductType = editProductType->Text;
  Products.Brand = editBrand->Text;
  Products.SupplierName = editSupplierName->Text;
  Products.SupplierMem = editSupplierMem->Text;
  Products.SupplierTel = editSupplierTel->Text;
  Products.SupplierAddr = editSupplierAddr->Text;
  Products.ShortDesc = editShortDesc->Text;
  Products.Unit = editUnit->Text;

  if (MyIsNumber(editWeight->Text) != 0)
  {
    MessageBox(NULL,"輸入的重量有誤！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  Products.Weight = StrToInt(editWeight->Text);

  if (MyIsFloat(editMarketPrice->Text) != 0)
  {
    MessageBox(NULL,"輸入的市場價有誤！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  Products.MarketPrice = StrToFloat(editMarketPrice->Text);
  if (MyIsFloat(editMemberPrice->Text) != 0)
  {
    MessageBox(NULL,"輸入的會員價有誤！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  Products.MemberPrice = StrToFloat(editMemberPrice->Text);

  Products.ProductImg = editProductImg->Text;
  Products.Description = memoDescription->Lines->Text;

  if (MyIsNumber(editStorage->Text) != 0)
  {
    MessageBox(NULL,"輸入的庫存數量有誤！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  Products.Storage = StrToInt(editStorage->Text);

  Products.Remark = editRemark->Text;
  return 0;
}
void TFormEditProduct::ClearProducts()
{
  editProductNo->Text = "";
  editName->Text = "";
  editProductType->Text = "";
  editBrand->Text = "";
  Products.SupplierName = "";
  Products.SupplierTel = "";
  editShortDesc->Text = "";
  editUnit->Text = "";
  editWeight->Text = 0;
  editMarketPrice->Text = 0;
  editMemberPrice->Text = 0;
  editProductImg->Text = "";
  memoDescription->Lines->Clear();
  editStorage->Text = 0;
  editRemark->Text = "";
}

void __fastcall TFormEditProduct::Button3Click(TObject *Sender)
{
  if (OpenDialog1->Execute())
  {
    editProductImg->Text = ExtractFileName(OpenDialog1->FileName);
    ProductImageFile = OpenDialog1->FileName;
    bCopyProductImage=true;
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditProduct::Button1Click(TObject *Sender)
{
  AnsiString temp;
  
  if (GetProducts() != 0)
  {
    return;
  }
  if (InsertId == true)
  {
    if (dmAgent->IsProductNoExist(Products.ProductNo) == 1)
    {
      MessageBox(NULL,"對不起，該產品編號已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertProductRecord(&Products) == 0)
    {
      if (bCopyProductImage==true)
      {
        temp = ShareDisk+"/"+Products.ProductImg;
        if (CopyFile(ProductImageFile.c_str(), temp.c_str(), FALSE) == false)
        {
          MessageBox(NULL,"拷貝圖片文件失敗！","信息提示",MB_OK|MB_ICONINFORMATION);
        }
      }
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員增加產品編號為：%s的資料", Products.ProductNo.c_str());
      dmAgent->adoQryProducts->Close();
      dmAgent->adoQryProducts->Open();
      MessageBox(NULL,"增加產品資料成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      ClearProducts();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加產品資料失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateProductRecord(&Products) == 0)
    {
      if (bCopyProductImage==true)
      {
        temp = ShareDisk+"/"+Products.ProductImg;
        if (CopyFile(ProductImageFile.c_str(), temp.c_str(), FALSE) == false)
        {
          MessageBox(NULL,"拷貝圖片文件失敗！","信息提示",MB_OK|MB_ICONINFORMATION);
        }
      }
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改客戶編號為：%s的資料", Products.ProductNo.c_str());
      dmAgent->adoQryProducts->Close();
      dmAgent->adoQryProducts->Open();
      MessageBox(NULL,"產品資料修改成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      ClearProducts();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"產品資料修改失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProduct::editProductNoChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProduct::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProduct::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  if (InsertId == false)
    ClearProducts();
}
//---------------------------------------------------------------------------

