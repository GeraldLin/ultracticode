//---------------------------------------------------------------------------

#ifndef callin1H
#define callin1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "public.h"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormCallin1 : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel1;
  TLabel *Label1;
  TLabel *lblCallerNo;
  TPanel *Panel2;
  TPanel *Panel3;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label11;
  TEdit *edtCustomNo;
  TEdit *edtCustType;
  TEdit *edtCustName;
  TEdit *edtCustSex;
  TEdit *edtCorpName;
  TEdit *edtContAddr;
  TSpeedButton *SpeedButton2;
  TLabel *Label14;
  TLabel *Label15;
  TLabel *Label12;
  TLabel *lbTranPhone;
  TEdit *edtProvince;
  TEdit *edtCityName;
  TButton *Button1;
  TLabel *lbSrvType;
  TLabel *Label7;
  TLabel *lblCalledNo;
  TLabel *lbAccountNo;
  TEdit *edtAccountNo;
  TLabel *lbMobileNo;
  TEdit *edtMobileNo;
  TDBGrid *DBGrid1;
  TLabel *Label8;
  TEdit *edtLastCallTime;
  TLabel *Label9;
  TEdit *edtLastWorkerName;
  void __fastcall SpeedButton2Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall FormKeyPress(TObject *Sender, char &Key);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormCallin1(TComponent* Owner);
  void SetCustom(CCustomer *customer);
  void SetCallerNo(AnsiString callerno);
  void SetCalledNo(AnsiString calledno, int inout=1);
  void SetTelOfCity(AnsiString city);
  void SetTranPhone(int nacdcalltype, const char *transeatno);
  void SetSrvType(CAccept &accept);
  void SetScreenPop(int nPopId);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormCallin1 *FormCallin1;
//---------------------------------------------------------------------------
#endif
