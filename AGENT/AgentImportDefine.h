/********************************************************************
*	文件名:		AgentImportDefine.h.h
*	目的  :		使用SOAP ToolKit 3.0 API 封装调用WebService的接口
*	创建时间:	2011年3月21日   11时58分
*	作者: 		luodongxi
/*********************************************************************/

//1.0 SDK interface definitions

//************************************************************************
//	函数名:		_FagentSetAgentConfig
//	参数  :		nTimeOutSecs:			调用 WebService 的超时时间, 单位为秒, 默认设置为 60 秒
//				bWriteLog:				是否记录日志信息, true 表示记录, false 表示不记录, 默认设置为 false
//  返回值:		true or false
//  功能说明:	是否记录日志信息
//	创建时间:	2011年3月21日   10时02分
//	作者: 		luodongxi
//************************************************************************
typedef int _FagentSetAgentConfig(int nTimeOutSecs, bool bWriteLog);


//************************************************************************
//	函数名:		_FagentCallWebService
//	参数  :		strWSDLAddress:			SOAP协议中的 EndPointURL 值, 不需要带后面的 ?wsdl, 
//										例如: http://10.160.130.127:9080/eemis/services/CrjWsYyxtInterface
//				strSoapAction:			SOAP协议中的 SoapAction 值, 接口中一般直接填写: ""
//				strStartEnvelopeString:	开始构建SOAP消息, 这是一个格式化的串, 由3个字符串通过竖线("|")连接起来, 接口中一般直接填写: "|NONE|" 3个字符串的原型为: string par_Prefix, string par_enc_style_uri, string par_encoding 具体的说明可以参考 Soap ToolKit 3.0 开发手册
//				strStartBody:			开始构建SOAP消息体, 接口中一般直接填写: "NONE" 
//				strInterfaceString:		开始设置接口的函数信息, 这是一个格式化的串, 由4个字符串通过竖线("|")连接起来, 4个字符串依次表示: 接口函数名称, 接口函数的命名空间, 接口函数的编码风格(缺省为NONE), 命名空间的前缀, 
//										例如: "GetWaitingTask|http://ws.webapp.com/|NONE|m"
//				nParamCount:			参数的个数
//				strParamString:			所有参数的集合, 多个参数之间通过竖线("|")连接起来, 每个参数由5个字符串组成, 每个字符串通过分号(";")连接起来, 5个字符串分别表示: 参数名称, 参数的命名空间(一般为空), 参数的编码风格(一般为NONE), 参数的前缀(一般为空), 参数的值
//										例如: 当 nParamCount 为2的时候, strParamString为: "topNumber;;NONE;;10|serverIP;;NONE;;192.168.0.179"
//				strReturnXmlString:		接口返回的完整的 xml 数据包, 由调用方自己解析具体的字段
//				strErrorMsg:			如果接口调用失败, 返回的错误信息
//  返回值:		true or false, true表示成功, false表示失败
//  功能说明:	通过 SOAP TOOLKIT 3.0 的API来调用 WebService
//	创建时间:	2011年3月21日   10时22分
//	作者: 		luodongxi
//************************************************************************
typedef int _FagentCallWebService(char *strWSDLAddress, char *strSoapAction, char *strStartEnvelopeString, char *strStartBody, char *strInterfaceString, int nParamCount, char *strParamString, char **strReturnXmlString, char **strErrorMsg);

//注意: 上述 _agentCallWebService 参数说明是参考如下 WebService 接口写的.
/////////////////////////////////////////////////////////////////////////////////
//Request
/*
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<SOAP-ENV:Body>
		<m:GetWaitingTask xmlns:m="http://ws.webapp.com/">
			<topNumber>100</topNumber>
			<serverIP>192.168.0.152</serverIP>
		</m:GetWaitingTask>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
*/

//Failed Response
/*
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<ns1:GetWaitingTaskResponse xmlns:ns1="http://ws.webapp.com/">
			<return>
				<strResult>-1</strResult>
			</return>
		</ns1:GetWaitingTaskResponse>
	</soap:Body>
</soap:Envelope>
*/

//Succeed Response
/*
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<ns1:GetWaitingTaskResponse xmlns:ns1="http://ws.webapp.com/">
			<return>
				<strResult>1</strResult>
				<getFaxSendTaskList>
					<Subject>退电通知单</Subject>
					<Receiver>美国</Receiver>
					<PhoneNumber>206;888;999;555</PhoneNumber>
					<TaskSubmitTime>2010-04-14 15:08:45</TaskSubmitTime>
					<SendState>1</SendState>
					<BoardServerName>center</BoardServerName>
					<Channel>0</Channel>
					<BeginTime>1900-01-01 00:00:00</BeginTime>
					<EndTime>1900-01-01 00:00:00</EndTime>
					<LastErrorMsg/>
					<ScheduledDateTime>1900-01-01 00:00:00</ScheduledDateTime>
					<FailedTimes>0</FailedTimes>
					<username>admin</username>
				</getFaxSendTaskList>
			</return>
		</ns1:GetWaitingTaskResponse>
	</soap:Body>
</soap:Envelope>
*/
/////////////////////////////////////////////////////////////////////////////////

//************************************************************************
//	函数名:		_FagentFreeMemory
//	参数  :		pReturnXmlMemory:		在 agentCallWebService 接口中返回XML结果的时候, 申请的内存地址
//				pErrorMsgMemory:		在 agentCallWebService 接口中返回错误信息的时候, 申请的内存地址
//  返回值:		true or false
//  功能说明:	释放 agentCallWebService 接口中申请的内存地址
//	创建时间:	2011年3月21日   10时02分
//	作者: 		luodongxi
//************************************************************************
typedef int _FagentFreeMemory(char *pReturnXmlMemory, char *pErrorMsgMemory);

