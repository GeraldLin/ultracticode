object FormEditOrderPay: TFormEditOrderPay
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#35746#21333#20184#27454#29366#24577
  ClientHeight = 273
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label167: TLabel
    Left = 25
    Top = 108
    Width = 84
    Height = 13
    Caption = #26159#21542#24320#21457#31080#65306
  end
  object Label168: TLabel
    Left = 39
    Top = 132
    Width = 70
    Height = 13
    Caption = #21457#31080#25260#22836#65306
  end
  object Label1: TLabel
    Left = 271
    Top = 28
    Width = 70
    Height = 13
    Caption = #24635#35746#36141#25968#65306
  end
  object Label2: TLabel
    Left = 25
    Top = 51
    Width = 84
    Height = 13
    Caption = #23450#21333#24635#37329#39069#65306
  end
  object Label3: TLabel
    Left = 67
    Top = 76
    Width = 42
    Height = 13
    Caption = #36816#36153#65306
  end
  object Label4: TLabel
    Left = 257
    Top = 76
    Width = 84
    Height = 13
    Caption = #24212#20184#24635#37329#39069#65306
  end
  object Label5: TLabel
    Left = 39
    Top = 212
    Width = 70
    Height = 13
    Caption = #25903#20184#29366#24577#65306
  end
  object Label6: TLabel
    Left = 53
    Top = 188
    Width = 56
    Height = 13
    Caption = #20184#27454#20154#65306
  end
  object Label7: TLabel
    Left = 271
    Top = 163
    Width = 70
    Height = 13
    Caption = #24050#20184#37329#39069#65306
  end
  object Label8: TLabel
    Left = 53
    Top = 28
    Width = 56
    Height = 13
    Caption = #35746#21333#21495#65306
  end
  object Label9: TLabel
    Left = 39
    Top = 163
    Width = 70
    Height = 13
    Caption = #20184#27454#26102#38388#65306
  end
  object Label11: TLabel
    Left = 272
    Top = 212
    Width = 70
    Height = 13
    Caption = #35746#21333#29366#24577#65306
  end
  object Label12: TLabel
    Left = 243
    Top = 51
    Width = 98
    Height = 13
    Caption = #25240#25187#21518#24635#37329#39069#65306
  end
  object Label13: TLabel
    Left = 271
    Top = 187
    Width = 70
    Height = 13
    Caption = #27424#36153#37329#39069#65306
  end
  object cbOIsNeedBill: TComboBox
    Left = 112
    Top = 104
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = #26159
    OnChange = cbOIsNeedBillChange
    OnKeyPress = cbOIsNeedBillKeyPress
  end
  object edtOBillName: TEdit
    Left = 112
    Top = 128
    Width = 353
    Height = 21
    TabOrder = 1
    OnChange = cbOIsNeedBillChange
  end
  object edtOProductTotal: TEdit
    Left = 344
    Top = 24
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 2
  end
  object edtOOrderAmount: TEdit
    Left = 112
    Top = 48
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 3
  end
  object edtOExpressFee: TEdit
    Left = 112
    Top = 72
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 4
  end
  object edtOPayAmount: TEdit
    Left = 344
    Top = 72
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 5
  end
  object cbOPayState: TComboBox
    Left = 112
    Top = 208
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = #23578#26410#25903#20184
    OnChange = cbOPayStateChange
    OnKeyPress = cbOIsNeedBillKeyPress
  end
  object edtOPayMen: TEdit
    Left = 112
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 7
    OnChange = cbOIsNeedBillChange
  end
  object edtOPayedAmount: TEdit
    Left = 344
    Top = 160
    Width = 121
    Height = 21
    TabOrder = 8
    OnChange = cbOIsNeedBillChange
    OnExit = edtOPayedAmountExit
  end
  object Button1: TButton
    Left = 104
    Top = 240
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 9
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 296
    Top = 240
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 10
    OnClick = Button2Click
  end
  object edtOOrderId: TEdit
    Left = 112
    Top = 24
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 11
  end
  object edtOPayedTime: TEdit
    Left = 112
    Top = 160
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 12
    OnChange = cbOIsNeedBillChange
  end
  object Button3: TButton
    Left = 234
    Top = 160
    Width = 19
    Height = 20
    Caption = '...'
    TabOrder = 13
    OnClick = Button3Click
  end
  object cbOOrderState: TComboBox
    Left = 344
    Top = 208
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 14
    Text = #26410#21457#36135
    OnChange = cbOIsNeedBillChange
    OnKeyPress = cbOIsNeedBillKeyPress
  end
  object edtODiscountAmount: TEdit
    Left = 344
    Top = 48
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 15
  end
  object MonthCalendar1: TMonthCalendar
    Left = 112
    Top = 0
    Width = 309
    Height = 154
    Date = 40022.8614498264
    TabOrder = 16
    Visible = False
    OnClick = MonthCalendar1Click
  end
  object editONoPayAmount: TEdit
    Left = 343
    Top = 184
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 17
    OnChange = cbOIsNeedBillChange
  end
end
