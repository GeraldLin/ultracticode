//---------------------------------------------------------------------------

#ifndef repcustomH
#define repcustomH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <ExtCtrls.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
//---------------------------------------------------------------------------
class TFormRepCustomList : public TForm
{
__published:	// IDE-managed Components
  TADOQuery *ADOQuery1;
  TQuickRep *QuickRep1;
  TQRBand *PageHeaderBand1;
  TQRLabel *QRLabel1;
  TQRSysData *QRSysData2;
  TQRLabel *QRLabelTotal;
  TQRBand *DetailBand1;
  TQRDBText *QRDBText1;
  TQRDBText *QRDBText3;
  TQRDBText *QRDBText4;
  TQRDBText *QRDBText17;
  TQRDBText *QRDBText18;
  TQRBand *PageFooterBand1;
  TQRSysData *QRSysData1;
  TStringField *ADOQuery1CustomNo;
  TStringField *ADOQuery1CustName;
  TStringField *ADOQuery1CorpName;
  TWordField *ADOQuery1CustType;
  TStringField *ADOQuery1CustSex;
  TStringField *ADOQuery1MobileNo;
  TStringField *ADOQuery1TeleNo;
  TStringField *ADOQuery1FaxNo;
  TStringField *ADOQuery1EMail;
  TStringField *ADOQuery1ContAddr;
  TStringField *ADOQuery1PostNo;
  TStringField *ADOQuery1CustTypeName;
  TQRDBText *QRDBText2;
  TQRDBText *QRDBText5;
  TQRDBText *QRDBText6;
  TQRDBText *QRDBText7;
  TQRDBText *QRDBText8;
  TQRDBText *QRDBText9;
  TQRLabel *QRLabel2;
  TQRLabel *QRLabel3;
  TQRLabel *QRLabel4;
  TQRLabel *QRLabel6;
  TQRLabel *QRLabel7;
  TQRLabel *QRLabel8;
  TQRLabel *QRLabel9;
  TStringField *ADOQuery1DispMobileNo;
  TStringField *ADOQuery1DispTeleNo;
  TStringField *ADOQuery1DispFaxNo;
  void __fastcall ADOQuery1CalcFields(TDataSet *DataSet);
private:	// User declarations
public:		// User declarations
  __fastcall TFormRepCustomList(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRepCustomList *FormRepCustomList;
//---------------------------------------------------------------------------
#endif
