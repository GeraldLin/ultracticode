//---------------------------------------------------------------------------

#ifndef acceptgroupH
#define acceptgroupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormAcceptGroup : public TForm
{
__published:	// IDE-managed Components
  TButton *Button1;
  TButton *Button2;
  TLabel *Label7;
  TGroupBox *GroupBox1;
  TLabel *Label1;
  TEdit *editCalledNo;
  TLabel *Label12;
  TComboBox *cbCalledType;
  TGroupBox *GroupBox2;
  TComboBox *cbDTMFType;
  TLabel *Label13;
  TLabel *Label2;
  TEdit *editDTMFKey;
  TLabel *Label14;
  TLabel *Label15;
  TComboBox *cbCustType;
  TComboBox *cbCustLevel;
  TGroupBox *GroupBox3;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TComboBox *cbGroupNo;
  TComboBox *cbGroupNo1;
  TComboBox *cbGroupNo2;
  TComboBox *cbGroupNo3;
  TComboBox *cbGroupNo4;
  TComboBox *cbSrvType;
  TGroupBox *GroupBox4;
  TLabel *Label6;
  TEdit *editSrvName;
  TLabel *Label16;
  TComboBox *cbVocId;
  TLabel *Label17;
  TLabel *Label18;
  TEdit *editCorpVoc;
  TEdit *editMenuVoc;
  TLabel *Label19;
  TComboBox *cbWorkerNoId;
  TLabel *Label20;
  TEdit *editMenuKeys;
  TCheckBox *ckGroupNo1Type;
  TCheckBox *ckGroupNo2Type;
  TCheckBox *ckGroupNo3Type;
  TCheckBox *ckGroupNo4Type;
  TCheckBox *ckGroupNo5Type;
  TLabel *Label21;
  TComboBox *cbMenuDtmfACK;
  TLabel *Label22;
  TEdit *editDtmfACKVoc;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editCalledNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbGroupNoKeyPress(TObject *Sender, char &Key);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbCalledTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall cbDTMFTypeChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAcceptGroup(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CAcceptGroup AcceptGroup;
  void SetModifyType(bool isinsert);
  void SetAcceptGroupData(CAcceptGroup& acceptgroup);
  int GetAcceptGroupData();
  void ClearAcceptGroupData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAcceptGroup *FormAcceptGroup;
//---------------------------------------------------------------------------
#endif
