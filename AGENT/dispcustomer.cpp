//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dispcustomer.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDispCustom *FormDispCustom;

extern int CustExOpenId, g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormDispCustom::TFormDispCustom(TComponent* Owner)
  : TForm(Owner)
{
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    TCustExLabelList[i] = NULL;
    TCustExInputList[i] = NULL;
  }
  if (g_nBandSeatNoType == 0)
    CreateCustExControl();
}
//---------------------------------------------------------------------------
void TFormDispCustom::CreateCustExControl()
{
  static bool bCreated=false;
  int nShortEditWith=120, nLongEditWith=420;
  int i, nControlCount, nEditLeft=100, nEditTop=6;

  if (bCreated == true)
    return;
  bCreated = true;
  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList->ItemNum == 0)
  {
    gbCustEx->Visible = false;
    Button1->Top = 170;
    FormDispCustom->Height = 250;
    return;
  }

  for (i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].state == 0 || (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType != 1 && FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType != 4)) //2017-01-02 edit
      continue;
    //輸入標簽控件
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType == 3)
    {
      if (nEditLeft == 400)
      {
        nEditLeft = 700;
      }
      else if (nEditLeft == 1000)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
    }
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType != 8)
    {
      TCustExLabelList[i] = new TLabel(this);
      TCustExLabelList[i]->Parent = sbCustEx;
      TCustExLabelList[i]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExLabelList[i]->Left = nEditLeft-TCustExLabelList[i]->Width-2;
      TCustExLabelList[i]->Top = nEditTop+4;
      TCustExLabelList[i]->Show();
    }
    //輸入框控件
    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      TCustExInputList[i] = new TComboBox(this);
      ((TComboBox *)TCustExInputList[i])->Parent = sbCustEx;
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TComboBox *)TCustExInputList[i])->Left = nEditLeft;
      ((TComboBox *)TCustExInputList[i])->Top = nEditTop;
      ((TComboBox *)TCustExInputList[i])->Width = nShortEditWith;
      ((TComboBox *)TCustExInputList[i])->MaxLength = 50;
      ((TComboBox *)TCustExInputList[i])->Enabled = false;
      dmAgent->AddCmbItem((TComboBox *)TCustExInputList[i], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "");
      ((TComboBox *)TCustExInputList[i])->Show();
      break;
    case 2: //數值下拉框
      TCustExInputList[i] = new TComboBox(this);
      ((TComboBox *)TCustExInputList[i])->Parent = sbCustEx;
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemName(MyStrToInt(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData));
      ((TComboBox *)TCustExInputList[i])->Left = nEditLeft;
      ((TComboBox *)TCustExInputList[i])->Top = nEditTop;
      ((TComboBox *)TCustExInputList[i])->Width = nShortEditWith;
      ((TComboBox *)TCustExInputList[i])->MaxLength = 50;
      ((TComboBox *)TCustExInputList[i])->Enabled = false;
      dmAgent->AddCmbItem((TComboBox *)TCustExInputList[i], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "");
      ((TComboBox *)TCustExInputList[i])->Show();
      break;
    case 3: //長文本框
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nLongEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 250;
      ((TEdit *)TCustExInputList[i])->ReadOnly = true;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    case 4: //短文本框
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 50;
      ((TEdit *)TCustExInputList[i])->ReadOnly = true;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    case 5: //數值框
      TCustExInputList[i] = new TCSpinEdit(this);
      ((TCSpinEdit *)TCustExInputList[i])->Parent = sbCustEx;
      ((TCSpinEdit *)TCustExInputList[i])->MinValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue;
      ((TCSpinEdit *)TCustExInputList[i])->MaxValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue;
      ((TCSpinEdit *)TCustExInputList[i])->Value = MyStrToInt(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData);
      ((TCSpinEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TCSpinEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TCSpinEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TCSpinEdit *)TCustExInputList[i])->Enabled = false;
      ((TCSpinEdit *)TCustExInputList[i])->Show();
      break;
    case 6: //金額框
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 20;
      ((TEdit *)TCustExInputList[i])->ReadOnly = true;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    case 7: //日期框
      TCustExInputList[i] = new TDateTimePicker(this);
      ((TDateTimePicker *)TCustExInputList[i])->Parent = sbCustEx;
      ((TDateTimePicker *)TCustExInputList[i])->Kind = dtkDate;
      ((TDateTimePicker *)TCustExInputList[i])->Date = MyStrToDate(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData);
      ((TDateTimePicker *)TCustExInputList[i])->Left = nEditLeft;
      ((TDateTimePicker *)TCustExInputList[i])->Top = nEditTop;
      ((TDateTimePicker *)TCustExInputList[i])->Width = nShortEditWith;
      ((TDateTimePicker *)TCustExInputList[i])->Enabled = false;
      ((TDateTimePicker *)TCustExInputList[i])->Show();
      break;
    case 8: //是非框
      TCustExInputList[i] = new TCheckBox(this);
      ((TCheckBox *)TCustExInputList[i])->Parent = sbCustEx;
      ((TCheckBox *)TCustExInputList[i])->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption;
      ((TCheckBox *)TCustExInputList[i])->Checked = (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData=="1") ? true : false;
      ((TCheckBox *)TCustExInputList[i])->Left = nEditLeft-80;
      ((TCheckBox *)TCustExInputList[i])->Top = nEditTop+4;
      ((TCheckBox *)TCustExInputList[i])->Width = nShortEditWith;
      ((TCheckBox *)TCustExInputList[i])->Enabled = false;
      ((TCheckBox *)TCustExInputList[i])->Show();
      break;
    default:
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 50;
      ((TEdit *)TCustExInputList[i])->Enabled = false;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    }
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType != 3)
    {
      if (nEditLeft == 1000)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
      else
      {
        nEditLeft = nEditLeft+300;
      }
    }
    else
    {
      if (nEditLeft == 100)
      {
        nEditLeft = 700;
      }
      else if (nEditLeft == 700)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
    }
  }
  if (nEditLeft == 100 && nEditTop == 6)
  {
    gbCustEx->Visible = false;
    Button1->Top = 180;
    FormDispCustom->Height = 270;
    return;
  }
  int nRows = (nEditTop-6)/22+1;
  if (nEditLeft == 100 && nRows > 0)
    nRows--;
  if (nRows < 6)
  {
    gbCustEx->Height = 168-(6-nRows)*22;
    Button1->Top = 348-(6-nRows)*22;
    FormDispCustom->Height = 430-(6-nRows)*22;
  }
}
void TFormDispCustom::SetCustom(CCustomer &customer)
{
  edtCustomNo->Text = customer.CustomNo; //客戶編號
  edtCustName->Text = customer.CustName; //客戶名稱
  edtCorpName->Text = customer.CorpName; //公司名稱

  edtCustType->Text = dmAgent->CustTypeItemList.GetItemName(customer.CustType); //客戶類型
  edtCustLevel->Text = dmAgent->CustLevelItemList.GetItemName(customer.CustLevel); //客戶劃分

  cbCustClass->Text = dmAgent->CustClassList.GetItemName(customer.CustClass);

  edtCustSex->Text = customer.CustSex; //性別

  edtMobileNo->Text = GetHidePhoneNum(2, customer.MobileNo); //忒儂
  edtTeleNo->Text = GetHidePhoneNum(2, customer.TeleNo); //嘐趕
  edtFaxNo->Text = GetHidePhoneNum(2, customer.FaxNo); //換淩

  edtSubPhone->Text = customer.SubPhone;
  edtEMail->Text = customer.EMail; //郵箱
  edtContAddr->Text = customer.ContAddr; //地址
  edtZoneName->Text = customer.ZoneName; //地址
  edtPostNo->Text = customer.PostNo; //郵編
  edtRemark->Text = customer.Remark; //備注
  cbProvince->Text = customer.Province;
  cbCityName->Text = customer.CityName;
  cbCountyName->Text = customer.CountyName;
  edtAccountNo->Text = customer.AccountNo;
}
//---------------------------------------------------------------------------
void TFormDispCustom::SetCustEx(CCustEx &custex)
{
  AnsiString strDT;
  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList == NULL)
    return;
  if (FormMain->m_pCustExItemSetList->ItemNum == 0)
    return;
  CreateCustExControl(); //2017-01-02 add
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (TCustExInputList[i] == NULL)
      continue;
    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      ((TComboBox *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 2: //數值下拉框
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemName(MyStrToInt(custex.Itemdata[i]));
      break;
    case 3: //長文本框
      ((TEdit *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 4: //短文本框
      ((TEdit *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 5: //數值框
      ((TCSpinEdit *)TCustExInputList[i])->Value = MyStrToInt(custex.Itemdata[i]);
      break;
    case 6: //金額框
      ((TEdit *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 7: //日期框
      strDT = custex.Itemdata[i].SubString(1, 10);
      ((TDateTimePicker *)TCustExInputList[i])->Date = MyStrToDate(strDT);
      break;
    case 8: //是非框
      ((TCheckBox *)TCustExInputList[i])->Checked = (custex.Itemdata[i]=="1") ? true : false;
      break;
    }
  }
}

void __fastcall TFormDispCustom::Button1Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------


void __fastcall TFormDispCustom::FormShow(TObject *Sender)
{
  CreateCustExControl();
}
//---------------------------------------------------------------------------

void __fastcall TFormDispCustom::FormDestroy(TObject *Sender)
{
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (TCustExLabelList[i] != NULL)
    {
      delete TCustExLabelList[i];
      TCustExLabelList[i] = NULL;
    }
    if (TCustExInputList[i] != NULL)
    {
      delete TCustExInputList[i];
      TCustExInputList[i] = NULL;
    }
  }
}
//---------------------------------------------------------------------------

