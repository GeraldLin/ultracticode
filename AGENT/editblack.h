//---------------------------------------------------------------------------

#ifndef editblackH
#define editblackH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditBlack : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editPhone;
  TEdit *editRemark;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label3;
  TCSpinEdit *cseRestDays;
  TLabel *Label4;
  TComboBox *cbBlackType;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editPhoneChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbBlackTypeKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditBlack(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditBlack *FormEditBlack;
//---------------------------------------------------------------------------
#endif
