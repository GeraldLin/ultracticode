//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "seefaxfile.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "FAXEDITLib_OCX"
#pragma resource "*.dfm"
TFormFaxSee *FormFaxSee;
//---------------------------------------------------------------------------
__fastcall TFormFaxSee::TFormFaxSee(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormFaxSee::SetFaxFileName(AnsiString filename)
{
  strFaxFileName = filename;
}

void __fastcall TFormFaxSee::FormShow(TObject *Sender)
{
  WideString wFaxFile;
  BSTR bFaxFile;

  wFaxFile = WideString(strFaxFileName);
  bFaxFile = ( wchar_t * )wFaxFile;
  FaxEdit1->OpenFaxFile(bFaxFile);
}
//---------------------------------------------------------------------------

