object FormCallBackPrompt: TFormCallBackPrompt
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #38651#35441#22238#25765#25552#37266
  ClientHeight = 153
  ClientWidth = 552
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 32
    Width = 417
    Height = 19
    Caption = #24744#22909':'#26377'1'#20491#34399#30908#38928#32004#22238#25765','#34399#30908#26159':8888888888888'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = #26032#32048#26126#39636
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 40
    Top = 100
    Width = 51
    Height = 12
    Caption = #24310#21518#26178#38291':'
  end
  object Button1: TButton
    Left = 296
    Top = 93
    Width = 75
    Height = 25
    Caption = #22238#25765
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 194
    Top = 93
    Width = 75
    Height = 25
    Caption = #24310#21518#25552#37266
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 400
    Top = 93
    Width = 75
    Height = 25
    Caption = #38364#38281
    TabOrder = 2
    OnClick = Button3Click
  end
  object ComboBox1: TComboBox
    Left = 96
    Top = 96
    Width = 90
    Height = 20
    ItemHeight = 12
    TabOrder = 3
    Text = '10'#20998#37912
    Items.Strings = (
      '10'#20998#37912
      '30'#20998#37912
      '60'#20998#37912)
  end
end
