object FormDispSupplier: TFormDispSupplier
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #26174#31034#20379#24212#21830#36164#26009
  ClientHeight = 394
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 7
    Top = 36
    Width = 81
    Height = 13
    AutoSize = False
    Caption = #20379#24212#21830#21517#31216#65306
  end
  object Label2: TLabel
    Left = 48
    Top = 60
    Width = 38
    Height = 13
    AutoSize = False
    Caption = #30005#35805#65306
  end
  object Label3: TLabel
    Left = 48
    Top = 83
    Width = 38
    Height = 13
    AutoSize = False
    Caption = #25163#26426#65306
  end
  object Label4: TLabel
    Left = 48
    Top = 107
    Width = 38
    Height = 13
    AutoSize = False
    Caption = #22320#22336#65306
  end
  object Label5: TLabel
    Left = 20
    Top = 132
    Width = 65
    Height = 13
    AutoSize = False
    Caption = #32852#31995#26041#24335#65306
  end
  object Label6: TLabel
    Left = 48
    Top = 153
    Width = 39
    Height = 13
    AutoSize = False
    Caption = #22791#27880#65306
  end
  object Label7: TLabel
    Left = 24
    Top = 11
    Width = 62
    Height = 13
    AutoSize = False
    Caption = #20379#24212#21830'ID'#65306
  end
  object editSupplierId: TEdit
    Left = 88
    Top = 8
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 0
  end
  object editName: TEdit
    Left = 88
    Top = 32
    Width = 401
    Height = 21
    ReadOnly = True
    TabOrder = 1
  end
  object editTelPhone: TEdit
    Left = 88
    Top = 56
    Width = 401
    Height = 21
    ReadOnly = True
    TabOrder = 2
  end
  object editCellPhone: TEdit
    Left = 88
    Top = 80
    Width = 401
    Height = 21
    ReadOnly = True
    TabOrder = 3
  end
  object editAddress: TEdit
    Left = 88
    Top = 104
    Width = 401
    Height = 21
    ReadOnly = True
    TabOrder = 4
  end
  object editContact: TEdit
    Left = 88
    Top = 128
    Width = 401
    Height = 21
    ReadOnly = True
    TabOrder = 5
  end
  object memoRemark: TMemo
    Left = 88
    Top = 152
    Width = 401
    Height = 200
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 6
  end
  object Button1: TButton
    Left = 216
    Top = 360
    Width = 75
    Height = 25
    Caption = #20851#38381
    TabOrder = 7
    OnClick = Button1Click
  end
end
