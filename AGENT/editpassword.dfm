object FormEditPassword: TFormEditPassword
  Left = 192
  Top = 110
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#22519#27231#21729#30331#37636#23494#30908
  ClientHeight = 200
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 73
    Top = 21
    Width = 60
    Height = 12
    Caption = #20540#27231#32232#34399#65306
  end
  object Label2: TLabel
    Left = 85
    Top = 56
    Width = 48
    Height = 12
    Caption = #33290#23494#30908#65306
  end
  object Label3: TLabel
    Left = 85
    Top = 90
    Width = 48
    Height = 12
    Caption = #26032#23494#30908#65306
  end
  object Label4: TLabel
    Left = 61
    Top = 126
    Width = 72
    Height = 12
    Caption = #30906#35469#26032#23494#30908#65306
  end
  object editWorkerNo: TEdit
    Left = 139
    Top = 17
    Width = 131
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editOldPsw: TEdit
    Left = 139
    Top = 52
    Width = 131
    Height = 20
    MaxLength = 20
    PasswordChar = '*'
    TabOrder = 1
    OnChange = editOldPswChange
  end
  object editNewPsw1: TEdit
    Left = 139
    Top = 87
    Width = 131
    Height = 20
    MaxLength = 20
    PasswordChar = '*'
    TabOrder = 2
    OnChange = editOldPswChange
  end
  object editNewPsw2: TEdit
    Left = 139
    Top = 121
    Width = 131
    Height = 20
    MaxLength = 20
    PasswordChar = '*'
    TabOrder = 3
    OnChange = editOldPswChange
  end
  object Button1: TButton
    Left = 43
    Top = 156
    Width = 82
    Height = 27
    Caption = #20462#25913
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 182
    Top = 156
    Width = 81
    Height = 27
    Caption = #25918#26820
    TabOrder = 5
    OnClick = Button2Click
  end
end
