//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("main.cpp", FormMain);
USEFORM("acceptgroup.cpp", FormAcceptGroup);
USEFORM("callin0.cpp", FormCallin0);
USEFORM("callin1.cpp", FormCallin1);
USEFORM("DataModule.cpp", dmAgent); /* TDataModule: File Type */
USEFORM("dialer.cpp", FormDial);
USEFORM("dialseatno.cpp", FormDialSeatNo);
USEFORM("dispcustomer.cpp", FormDispCustom);
USEFORM("disprecvmsg.cpp", FormRecvMsg);
USEFORM("dispzskmain.cpp", FormDispZSKMain);
USEFORM("dispzsksub.cpp", FormDispZSKSub);
USEFORM("dispzsksubject.cpp", FormDispZSKSubject);
USEFORM("editcustom.cpp", FormEditCustom);
USEFORM("editdialouttask.cpp", FormEditDialOutTask);
USEFORM("editsubphone.cpp", FormEditSubPhone);
USEFORM("editzskmain.cpp", FormZSKMain);
USEFORM("editzsksub.cpp", FormZSKSub);
USEFORM("editzsksubject.cpp", FormZSKSubject);
USEFORM("login.cpp", FormLogin);
USEFORM("procrecvsms.cpp", FormRecvSmsProc);
USEFORM("seatparam.cpp", FormSeatParam);
USEFORM("sendmsg.cpp", FormSendMsg);
USEFORM("sendsms.cpp", FormSendSms);
USEFORM("showmsg.cpp", FormMsg);
USEFORM("worker.cpp", FormWorker);
USEFORM("editworkgroup.cpp", FormWorkerGroup);
USEFORM("acceptproc.cpp", FormAcceptProc);
USEFORM("repcustom.cpp", FormRepCustomList);
USEFORM("repaccept.cpp", FormRepAccept);
USEFORM("editblack.cpp", FormEditBlack);
USEFORM("searchcust.cpp", FormSeachCustomer);
USEFORM("editselitem.cpp", FormEditSelItem);
USEFORM("u_About.cpp", AboutBox);
USEFORM("editpassword.cpp", FormEditPassword);
USEFORM("updateversion.cpp", FormUpdateVersion);
USEFORM("acptitemset.cpp", FormAcptItemSet);
USEFORM("editacptitemlist.cpp", FormEditAcptItemList);
USEFORM("editivrmenuset.cpp", FormIVRMenuSet);
USEFORM("editrecvfaxrule.cpp", FormRecvFaxRule);
USEFORM("seeimage.cpp", FormImageSee);
USEFORM("editcustexset.cpp", FormEditCustExSet);
USEFORM("editcustexitemlist.cpp", FormEditCustExItemList);
USEFORM("editdispitemsel.cpp", FormEditDispItemSel);
USEFORM("repcdrlist.cpp", FormRepCDRList);
USEFORM("repworker.cpp", FormRepWorker);
USEFORM("repacpt.cpp", FormRepAcpt);
USEFORM("edittabauthset.cpp", FormTabAuthSet);
USEFORM("repsumcallout.cpp", FormSumCallout);
USEFORM("editmandialouttask.cpp", FormEditManDialOutTask);
USEFORM("editdialoutitemset.cpp", FormEditDialOutItemSet);
USEFORM("editdialoutitemlist.cpp", FormEditDialOutItemList);
USEFORM("allocdialout.cpp", FormAllocDailOut);
USEFORM("sumdialout.cpp", FormSunDialOut);
USEFORM("editdutytel.cpp", FormEditDutyTel);
USEFORM("editsrvsubtype.cpp", FormEditSrvSubType);
USEFORM("crmdbparam.cpp", FormCRMDBParam);
USEFORM("editholiday.cpp", FormEditHoliday);
USEFORM("QAsel.cpp", FormQASel);
USEFORM("editworktimeset.cpp", FormEditWorkTimeSet);
USEFORM("editsrvtype.cpp", FormEditSrvType);
USEFORM("editprodmodel.cpp", FormEditProdModel);
USEFORM("editendcode1.cpp", FormEditEndCode1);
USEFORM("editendcode2.cpp", FormEditEndCode2);
USEFORM("adddialouttele.cpp", FormAddDialOutTele);
USEFORM("editcallbacktime.cpp", FormEditCallBackTime);
USEFORM("dispdialoutlist.cpp", FormDispDialOutList);
USEFORM("allocdialtask.cpp", FormAllocDialTask);
USEFORM("dispanswerdemo.cpp", FormAnswerDemo);
USEFORM("setacceptcallback.cpp", FormSetAcceptCallBack);
USEFORM("editprodtype.cpp", FormEditProdType);
USEFORM("dialoutcallback.cpp", FormCallBackPrompt);
USEFORM("dialnext.cpp", FormDialNext);
USEFORM("viewfax.cpp", FormViewFax);
USEFORM("viewemail.cpp", FormViewEmail);
USEFORM("selsendfaxdoc.cpp", FormSendFaxDocList);
USEFORM("searchdialoutcust.cpp", FormSearchDialoutCust);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  try
  {
    Application->Initialize();
    Application->Title = "�q���y�u";
     Application->CreateForm(__classid(TdmAgent), &dmAgent);
     Application->CreateForm(__classid(TFormMain), &FormMain);
     Application->CreateForm(__classid(TFormLogin), &FormLogin);
     Application->CreateForm(__classid(TFormAcceptGroup), &FormAcceptGroup);
     Application->CreateForm(__classid(TFormCallin0), &FormCallin0);
     Application->CreateForm(__classid(TFormCallin1), &FormCallin1);
     Application->CreateForm(__classid(TFormDial), &FormDial);
     Application->CreateForm(__classid(TFormDialSeatNo), &FormDialSeatNo);
     Application->CreateForm(__classid(TFormDispCustom), &FormDispCustom);
     Application->CreateForm(__classid(TFormRecvMsg), &FormRecvMsg);
     Application->CreateForm(__classid(TFormDispZSKMain), &FormDispZSKMain);
     Application->CreateForm(__classid(TFormDispZSKSub), &FormDispZSKSub);
     Application->CreateForm(__classid(TFormDispZSKSubject), &FormDispZSKSubject);
     Application->CreateForm(__classid(TFormEditCustom), &FormEditCustom);
     Application->CreateForm(__classid(TFormEditDialOutTask), &FormEditDialOutTask);
     Application->CreateForm(__classid(TFormEditSubPhone), &FormEditSubPhone);
     Application->CreateForm(__classid(TFormZSKMain), &FormZSKMain);
     Application->CreateForm(__classid(TFormZSKSub), &FormZSKSub);
     Application->CreateForm(__classid(TFormZSKSubject), &FormZSKSubject);
     Application->CreateForm(__classid(TFormRecvSmsProc), &FormRecvSmsProc);
     Application->CreateForm(__classid(TFormSeatParam), &FormSeatParam);
     Application->CreateForm(__classid(TFormSendMsg), &FormSendMsg);
     Application->CreateForm(__classid(TFormSendSms), &FormSendSms);
     Application->CreateForm(__classid(TFormMsg), &FormMsg);
     Application->CreateForm(__classid(TFormWorker), &FormWorker);
     Application->CreateForm(__classid(TFormWorkerGroup), &FormWorkerGroup);
     Application->CreateForm(__classid(TFormAcceptProc), &FormAcceptProc);
     Application->CreateForm(__classid(TFormRepCustomList), &FormRepCustomList);
     Application->CreateForm(__classid(TFormRepAccept), &FormRepAccept);
     Application->CreateForm(__classid(TFormEditBlack), &FormEditBlack);
     Application->CreateForm(__classid(TFormSeachCustomer), &FormSeachCustomer);
     Application->CreateForm(__classid(TFormEditSelItem), &FormEditSelItem);
     Application->CreateForm(__classid(TFormEditPassword), &FormEditPassword);
     Application->CreateForm(__classid(TFormUpdateVersion), &FormUpdateVersion);
     Application->CreateForm(__classid(TAboutBox), &AboutBox);
     Application->CreateForm(__classid(TFormAcptItemSet), &FormAcptItemSet);
     Application->CreateForm(__classid(TFormEditAcptItemList), &FormEditAcptItemList);
     Application->CreateForm(__classid(TFormIVRMenuSet), &FormIVRMenuSet);
     Application->CreateForm(__classid(TFormRecvFaxRule), &FormRecvFaxRule);
     Application->CreateForm(__classid(TFormImageSee), &FormImageSee);
     Application->CreateForm(__classid(TFormEditCustExSet), &FormEditCustExSet);
     Application->CreateForm(__classid(TFormEditCustExItemList), &FormEditCustExItemList);
     Application->CreateForm(__classid(TFormEditDispItemSel), &FormEditDispItemSel);
     Application->CreateForm(__classid(TFormRepCDRList), &FormRepCDRList);
     Application->CreateForm(__classid(TFormRepWorker), &FormRepWorker);
     Application->CreateForm(__classid(TFormRepAcpt), &FormRepAcpt);
     Application->CreateForm(__classid(TFormTabAuthSet), &FormTabAuthSet);
     Application->CreateForm(__classid(TFormSumCallout), &FormSumCallout);
     Application->CreateForm(__classid(TFormEditManDialOutTask), &FormEditManDialOutTask);
     Application->CreateForm(__classid(TFormEditDialOutItemSet), &FormEditDialOutItemSet);
     Application->CreateForm(__classid(TFormEditDialOutItemList), &FormEditDialOutItemList);
     Application->CreateForm(__classid(TFormAllocDailOut), &FormAllocDailOut);
     Application->CreateForm(__classid(TFormSunDialOut), &FormSunDialOut);
     Application->CreateForm(__classid(TFormEditDutyTel), &FormEditDutyTel);
     Application->CreateForm(__classid(TFormEditSrvSubType), &FormEditSrvSubType);
     Application->CreateForm(__classid(TFormCRMDBParam), &FormCRMDBParam);
     Application->CreateForm(__classid(TFormEditHoliday), &FormEditHoliday);
     Application->CreateForm(__classid(TFormQASel), &FormQASel);
     Application->CreateForm(__classid(TFormEditWorkTimeSet), &FormEditWorkTimeSet);
     Application->CreateForm(__classid(TFormEditSrvType), &FormEditSrvType);
     Application->CreateForm(__classid(TFormEditProdModel), &FormEditProdModel);
     Application->CreateForm(__classid(TFormEditEndCode1), &FormEditEndCode1);
     Application->CreateForm(__classid(TFormEditEndCode2), &FormEditEndCode2);
     Application->CreateForm(__classid(TFormAddDialOutTele), &FormAddDialOutTele);
     Application->CreateForm(__classid(TFormEditCallBackTime), &FormEditCallBackTime);
     Application->CreateForm(__classid(TFormDispDialOutList), &FormDispDialOutList);
     Application->CreateForm(__classid(TFormAllocDialTask), &FormAllocDialTask);
     Application->CreateForm(__classid(TFormAnswerDemo), &FormAnswerDemo);
     Application->CreateForm(__classid(TFormSetAcceptCallBack), &FormSetAcceptCallBack);
     Application->CreateForm(__classid(TFormEditProdType), &FormEditProdType);
     Application->CreateForm(__classid(TFormCallBackPrompt), &FormCallBackPrompt);
     Application->CreateForm(__classid(TFormDialNext), &FormDialNext);
     Application->CreateForm(__classid(TFormViewFax), &FormViewFax);
     Application->CreateForm(__classid(TFormViewEmail), &FormViewEmail);
     Application->CreateForm(__classid(TFormSendFaxDocList), &FormSendFaxDocList);
     Application->CreateForm(__classid(TFormSearchDialoutCust), &FormSearchDialoutCust);
     Application->Run();
  }
  catch (Exception &exception)
  {
    Application->ShowException(&exception);
  }
  catch (...)
  {
    try
    {
      throw Exception("");
    }
    catch (Exception &exception)
    {
      Application->ShowException(&exception);
    }
  }
  return 0;
}
//---------------------------------------------------------------------------
