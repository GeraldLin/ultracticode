//---------------------------------------------------------------------------

#ifndef editdialoutitemsetH
#define editdialoutitemsetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditDialOutItemSet : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TEdit *editTid;
  TComboBox *cbControlType;
  TEdit *editLabelName;
  TEdit *editDefaultData;
  TCSpinEdit *cseTMinValue;
  TCSpinEdit *cseTMaxValue;
  TButton *Button1;
  TButton *Button2;
  TCheckBox *ckIsAllowNull;
  TCheckBox *ckExportId;
  TCheckBox *ckDispId;
  TLabel *Label7;
  TComboBox *cbModifyId;
  TLabel *Label8;
  TLabel *Label9;
  TEdit *editMaskEdit;
  TEdit *editHintStr;
  TMemo *Memo1;
  TLabel *Label13;
  TComboBox *cbWith;
  TLabel *Label10;
  TComboBox *cbSearchId;
  TLabel *Label11;
  TCSpinEdit *cseSearchOrder;
  TLabel *Label12;
  TCSpinEdit *cseDispOrder;
  void __fastcall cbControlTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editTidChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbControlTypeChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditDialOutItemSet(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  int m_nTaskId;
  CDialOutItemSet DialOutItemSet;
  void SetModifyType(bool isinsert);
  void SetDialOutItemSetData(CDialOutItemSet& dialoutitemset);
  int GetDialOutItemSetData();
  void ClearDialOutItemSetData();
  void SetTaskId(int taskid);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditDialOutItemSet *FormEditDialOutItemSet;
//---------------------------------------------------------------------------
#endif
