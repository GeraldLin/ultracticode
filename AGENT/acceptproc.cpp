//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "acceptproc.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAcceptProc *FormAcceptProc;
extern int g_nBandSeatNoType;
extern CMyWorker MyWorker;
//---------------------------------------------------------------------------
__fastcall TFormAcceptProc::TFormAcceptProc(TComponent* Owner)
  : TForm(Owner)
{
  ProcId = 1;
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormAcceptProc->cmbDealState, dmAgent->AcceptProcItemList, "");
    dmAgent->AddCmbItem(FormAcceptProc->cbNextProcType, dmAgent->NextProcTypeItemList, "");
    dmAgent->AddCmbItem(FormAcceptProc->cbNextProcResult, dmAgent->NextProcResultItemList, "");
  }
}
//---------------------------------------------------------------------------
void TFormAcceptProc::SetAcceptdata(AnsiString gid, AnsiString custname, AnsiString customtel)
{
  GID = gid;
  editCustName->Text = custname;
  editCustomTel->Text = customtel;
}

void __fastcall TFormAcceptProc::Button1Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::DBGrid1CellClick(TColumn *Column)
{
  AnsiString dealcont;
  int nNextProcType;

  InsertId = false;
  Button2->Caption = "修改處理記錄";
  if (ProcId == 0)
  {
    if (dmAgent->adoQryCurAcceptProc->State == dsInactive || dmAgent->adoQryCurAcceptProc->Eof)
      return;
    try
    {
      dealcont = dmAgent->adoQryCurAcceptProc->FieldByName("ProcLog")->AsString;
      memoAcceptProc->Lines->Clear();
      memoAcceptProc->Lines->Add(dealcont);
    }
    catch (...)
    {
      return;
    }
    return;
  }

  if (dmAgent->adoQryOldAcceptProc->State == dsInactive || dmAgent->adoQryOldAcceptProc->Eof)
    return;
  try
  {
    dealcont = dmAgent->adoQryOldAcceptProc->FieldByName("ProcLog")->AsString;
    nNextProcType = dmAgent->adoQryOldAcceptProc->FieldByName("NextProcType")->AsInteger;
    cbNextProcType->Text = dmAgent->NextProcTypeItemList.GetItemName(nNextProcType);
    if (nNextProcType == 1)
    {
      dmAgent->AddCmbItem(cbNextProcSel, dmAgent->DepartmentItemList, "");
      cbNextProcSel->Text = dmAgent->adoQryOldAcceptProc->FieldByName("DepartmentName1")->AsString;
    }
    else if (nNextProcType == 2)
    {
      dmAgent->AddWorkerCmbItem(cbNextProcSel);
      cbNextProcSel->Text = dmAgent->adoQryOldAcceptProc->FieldByName("WorkerName1")->AsString;
    }
    cbNextProcResult->Text = dmAgent->NextProcResultItemList.GetItemName(dmAgent->adoQryOldAcceptProc->FieldByName("ProcResult")->AsInteger);

    memoAcceptProc->Lines->Clear();
    memoAcceptProc->Lines->Add(dealcont);
    Button2->Enabled = false;
    if (MyWorker.WorkerNo == dmAgent->adoQryOldAcceptProc->FieldByName("WorkerNo")->AsInteger
        || MyWorker.DepartmentID == dmAgent->adoQryOldAcceptProc->FieldByName("DepartmentID")->AsInteger)
    {
      FormAcceptProc->memoAcceptProc->ReadOnly = false;
      FormAcceptProc->Panel2->Visible = true;
    }
    else
    {
      FormAcceptProc->memoAcceptProc->ReadOnly = true;
      FormAcceptProc->Panel2->Visible = false;
    }
    if (MyWorker.WorkerNo == dmAgent->adoQryOldAcceptProc->FieldByName("NextProcWorkerNo")->AsInteger
        || MyWorker.DepartmentID == dmAgent->adoQryOldAcceptProc->FieldByName("NextprocDepartmentId")->AsInteger)
    {
      N1->Enabled = true;
      N2->Enabled = true;
    }
    else
    {
      N1->Enabled = false;
      N2->Enabled = false;
    }
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::Button2Click(TObject *Sender)
{
  int id, nNextProcType, nDepartmentId=0, nNextProcResult;
  AnsiString strWorkerNo="0";

  if (InsertId == false)
  {
    if (dmAgent->adoQryOldAcceptProc->State == dsInactive || dmAgent->adoQryOldAcceptProc->Eof)
      return;
    if ( MessageBox( NULL, "您確認要修改處理內容嗎？", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      id = dmAgent->adoQryOldAcceptProc->FieldByName("Id")->AsInteger;

      nNextProcResult = dmAgent->NextProcResultItemList.GetItemValue(cbNextProcResult->Text);
      nNextProcType = dmAgent->NextProcTypeItemList.GetItemValue(cbNextProcType->Text);
      if (nNextProcType == 1)
      {
        dmAgent->GetDepartmentId(cbNextProcSel->Text, nDepartmentId);
      }
      else if (nNextProcType == 2)
      {
        dmAgent->GetWorkerNo(SplitStringFirstItem(cbNextProcSel->Text, "-"), strWorkerNo);
      }
      else
      {
        nNextProcType = 0;
      }

      dmAgent->UpdateAcceptProcCont(id, MyWorker.WorkerNo, memoAcceptProc->Lines->Text, nNextProcType, strWorkerNo, nDepartmentId,nNextProcResult);

      dmAgent->adoQryOldAcceptProc->Close();
      dmAgent->adoQryOldAcceptProc->Open();
      Button2->Enabled = false;
    }
    return;
  }
  if ( MessageBox( NULL, "您確認要增加當前的處理內容記錄嗎？", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    nNextProcResult = dmAgent->NextProcResultItemList.GetItemValue(cbNextProcResult->Text);
    nNextProcType = dmAgent->NextProcTypeItemList.GetItemValue(cbNextProcType->Text);
    if (nNextProcType == 1)
    {
      dmAgent->GetDepartmentId(cbNextProcSel->Text, nDepartmentId);
    }
    else if (nNextProcType == 2)
    {
      dmAgent->GetWorkerNo(SplitStringFirstItem(cbNextProcSel->Text, "-"), strWorkerNo);
    }
    else
    {
      nNextProcType = 0;
    }

    dmAgent->InsertAcceptProcRecord(GID, MyWorker.WorkerNo, memoAcceptProc->Lines->Text, nNextProcType, strWorkerNo, nDepartmentId, nNextProcResult);

    dmAgent->adoQryOldAcceptProc->Close();
    dmAgent->adoQryOldAcceptProc->Open();
    Button2->Enabled = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::memoAcceptProcChange(TObject *Sender)
{
    Button2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::Button4Click(TObject *Sender)
{
  int state;
  if ( MessageBox( NULL, "您確認要修改處理結果嗎？", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    state = dmAgent->AcceptProcItemList.GetItemValue(cmbDealState->Text);
    dmAgent->UpdateDealState(GID, state);
    dmAgent->adoQryOldAccept->Close();
    dmAgent->adoQryOldAccept->Open();
    Button4->Enabled = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::cmbDealStateChange(TObject *Sender)
{
  Button4->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::cmbDealStateKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::cbNextProcTypeChange(TObject *Sender)
{
  int nNextProcType = dmAgent->NextProcTypeItemList.GetItemValue(cbNextProcType->Text);
  if (nNextProcType == 1)
  {
    dmAgent->AddCmbItem(cbNextProcSel, dmAgent->DepartmentItemList, "");
  }
  else if (nNextProcType == 2)
  {
    dmAgent->AddWorkerCmbItem(cbNextProcSel);
  }
  else
  {
    cbNextProcSel->Items->Clear();
  }
  Button2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::cbNextProcSelChange(TObject *Sender)
{
  Button2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::N4Click(TObject *Sender)
{
  InsertId = true;
  memoAcceptProc->Lines->Clear();
  Button2->Caption = "增加處理記錄";
  cbNextProcType->Text = "無需後續處理";
  memoAcceptProc->ReadOnly = false;
  Panel2->Visible = true;
  Button2->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::N1Click(TObject *Sender)
{
  if (dmAgent->adoQryOldAcceptProc->State == dsInactive || dmAgent->adoQryOldAcceptProc->Eof)
    return;
  int id = dmAgent->adoQryOldAcceptProc->FieldByName("Id")->AsInteger;
  dmAgent->UpdateAcceptProcRespondId(id, 1);
  dmAgent->adoQryOldAcceptProc->Close();
  dmAgent->adoQryOldAcceptProc->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptProc::N2Click(TObject *Sender)
{
  if (dmAgent->adoQryOldAcceptProc->State == dsInactive || dmAgent->adoQryOldAcceptProc->Eof)
    return;
  int id = dmAgent->adoQryOldAcceptProc->FieldByName("Id")->AsInteger;
  dmAgent->UpdateAcceptProcRespondId(id, 2);
  dmAgent->adoQryOldAcceptProc->Close();
  dmAgent->adoQryOldAcceptProc->Open();
}
//---------------------------------------------------------------------------

