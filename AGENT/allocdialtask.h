//---------------------------------------------------------------------------

#ifndef allocdialtaskH
#define allocdialtaskH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormAllocDialTask : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label3;
  TComboBox *cbWorkerNo;
  TButton *Button1;
  TButton *Button2;
  TPanel *Panel1;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbWorkerNoKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAllocDialTask(TComponent* Owner);

  int TaskId;
  AnsiString strWhere;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAllocDialTask *FormAllocDialTask;
//---------------------------------------------------------------------------
#endif
