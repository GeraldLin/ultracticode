//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editselitem.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditSelItem *FormEditSelItem;
//---------------------------------------------------------------------------
__fastcall TFormEditSelItem::TFormEditSelItem(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditSelItem::SetDispMsg(int nSelectTableId)
{
  SelectTableId = nSelectTableId;
  FormEditSelItem->Caption = dmAgent->SelectTable[SelectTableId].strCaption;
  lbItemId->Caption = dmAgent->SelectTable[SelectTableId].strItemIdDisp;
  lbItemName->Caption = dmAgent->SelectTable[SelectTableId].strItemNameDisp;
}
void TFormEditSelItem::SetDispValue(bool bInsertId, int nItemvalue, AnsiString strItemName)
{
  InsertId = bInsertId;
  if (InsertId == true)
  {
    editItemId->Text = "";
    editItemId->Color = clWindow;
    editItemId->ReadOnly = false;
    editItemName->Text = "";
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editItemId->Text = nItemvalue;
    editItemId->Color = clSilver;
    editItemId->ReadOnly = true;
    editItemName->Text = strItemName;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}
void __fastcall TFormEditSelItem::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditSelItem::editItemIdChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditSelItem::Button1Click(TObject *Sender)
{
  char dispbuf[256];
  CSelectRecord SelectRecord;

  if (MyIsNumber(editItemId->Text) != 0)
  {
    MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strInpuItemIdErrMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strInputItemNameErrMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  SelectRecord.nItemIdValue = StrToInt(editItemId->Text);
  if (dmAgent->SelectTable[SelectTableId].nMinValue == 0
    && dmAgent->SelectTable[SelectTableId].nMinValue < dmAgent->SelectTable[SelectTableId].nMaxValue)
  {
    if (SelectRecord.nItemIdValue < dmAgent->SelectTable[SelectTableId].nMinValue
      || SelectRecord.nItemIdValue > dmAgent->SelectTable[SelectTableId].nMaxValue)
    {
      sprintf(dispbuf, "對不起，您輸入的值超出允許範圍(%d - %d)",
        dmAgent->SelectTable[SelectTableId].nMinValue, dmAgent->SelectTable[SelectTableId].nMaxValue);
      MessageBox(NULL,dispbuf,"訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
  }

  SelectRecord.strItemNameValue = editItemName->Text;

  if (InsertId == true)
  {
    if (dmAgent->IsSelItemIdExist(SelectTableId, &SelectRecord) == 1)
    {
      MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strExistMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (SelectTableId == 18)
    {
      if (dmAgent->InsertWebURLAddr(SelectRecord.nItemIdValue, SelectRecord.strItemNameValue, editURLAddr->Text, cseSortType->Value) == 0)
      {
        dmAgent->adoQrySelItem->Close();
        dmAgent->adoQrySelItem->Open();
        dmAgent->SelectTable[SelectTableId].bModifyId = true;
        dmAgent->UpdateSelItemIdListItems(SelectTableId);
        this->Close();
        //MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strInsertSuccMsg.c_str(),"訊息提示",MB_OK|MB_ICONINFORMATION);
      }
      else
      {
        MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strInsertFailMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
      }
    }
    else
    {
      if (dmAgent->InsertSelItemIdRecord(SelectTableId, &SelectRecord) == 0)
      {
        dmAgent->adoQrySelItem->Close();
        dmAgent->adoQrySelItem->Open();
        dmAgent->SelectTable[SelectTableId].bModifyId = true;
        dmAgent->UpdateSelItemIdListItems(SelectTableId);
        this->Close();
        //MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strInsertSuccMsg.c_str(),"訊息提示",MB_OK|MB_ICONINFORMATION);
      }
      else
      {
        MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strInsertFailMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
      }
    }
  }
  else
  {
    if (SelectTableId == 18)
    {
      if (dmAgent->UpdateWebURLAddr(SelectRecord.nItemIdValue, SelectRecord.strItemNameValue, editURLAddr->Text, cseSortType->Value) == 0)
      {
        dmAgent->adoQrySelItem->Close();
        dmAgent->adoQrySelItem->Open();
        dmAgent->SelectTable[SelectTableId].bModifyId = true;
        dmAgent->UpdateSelItemIdListItems(SelectTableId);
        this->Close();
        //MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strUpdateSuccMsg.c_str(),"訊息提示",MB_OK|MB_ICONINFORMATION);
      }
      else
      {
        MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strUpdateFailMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
      }
    }
    else
    {
      if (dmAgent->UpdateSelItemIdRecord(SelectTableId, &SelectRecord) == 0)
      {
        dmAgent->adoQrySelItem->Close();
        dmAgent->adoQrySelItem->Open();
        dmAgent->SelectTable[SelectTableId].bModifyId = true;
        dmAgent->UpdateSelItemIdListItems(SelectTableId);
        this->Close();
        //MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strUpdateSuccMsg.c_str(),"訊息提示",MB_OK|MB_ICONINFORMATION);
      }
      else
      {
        MessageBox(NULL,dmAgent->SelectTable[SelectTableId].strUpdateFailMsg.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
      }
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditSelItem::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditSelItem->editItemId->SetFocus();
  else
    FormEditSelItem->editItemName->SetFocus();
}
//---------------------------------------------------------------------------

