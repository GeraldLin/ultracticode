//---------------------------------------------------------------------------

#ifndef editacptitemlistH
#define editacptitemlistH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditAcptItemList : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editLabelCaption;
  TLabel *Label2;
  TEdit *editItemNo;
  TLabel *Label3;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label4;
  TEdit *editSrvType;
  TLabel *Label5;
  TEdit *editItemURL;
  TLabel *Label6;
  TMemo *memoItemDemo;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditAcptItemList(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CAcptItemSet AcptItemSet;
  void SetModifyType(bool isinsert);
  void SetItemData(CAcptItemSet& acptitemset, AnsiString strItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo);

};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditAcptItemList *FormEditAcptItemList;
//---------------------------------------------------------------------------
#endif
