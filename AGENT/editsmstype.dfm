object FormEditSmsType: TFormEditSmsType
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #35774#32622#30701#20449#19994#21153#31867#22411
  ClientHeight = 132
  ClientWidth = 316
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 19
    Width = 153
    Height = 13
    AutoSize = False
    Caption = #25509#25910#30701#20449#19994#21153#31867#22411#32534#21495#65306
  end
  object Label2: TLabel
    Left = 16
    Top = 52
    Width = 154
    Height = 13
    AutoSize = False
    Caption = #25509#25910#30701#20449#19994#21153#31867#22411#21517#31216#65306
  end
  object editSmsTypeId: TEdit
    Left = 176
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
    OnChange = editSmsTypeIdChange
  end
  object editSmsTypeName: TEdit
    Left = 176
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = editSmsTypeIdChange
  end
  object Button1: TButton
    Left = 40
    Top = 88
    Width = 75
    Height = 25
    Caption = #30830#23450
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 200
    Top = 88
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
end
