//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editdialoutitemset.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditDialOutItemSet *FormEditDialOutItemSet;
extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormEditDialOutItemSet::TFormEditDialOutItemSet(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormEditDialOutItemSet->cbControlType, dmAgent->ControlTypeItemList, "");
  }
  m_nTaskId = -1;
}
//---------------------------------------------------------------------------
void TFormEditDialOutItemSet::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    editTid->ReadOnly = false;
    editTid->Color = clWindow;
  }
  else
  {
    Button1->Caption = "修改";
    editTid->ReadOnly = true;
    editTid->Color = clSilver;
  }
}
void TFormEditDialOutItemSet::SetDialOutItemSetData(CDialOutItemSet& dialoutitemset)
{
  DialOutItemSet = dialoutitemset;
  m_nTaskId = dialoutitemset.TaskId;
  
  editTid->Text = DialOutItemSet.Tid;
  cbControlType->Text = dmAgent->ControlTypeItemList.GetItemName(DialOutItemSet.TControlType);
  editLabelName->Text = DialOutItemSet.TLabelCaption;
  editDefaultData->Text = DialOutItemSet.TDefaultData;
  cseTMinValue->Value = DialOutItemSet.TMinValue;
  cseTMaxValue->Value = DialOutItemSet.TMaxValue;
  ckIsAllowNull->Checked = (DialOutItemSet.TIsAllowNull == 0) ? false : true;
  ckExportId->Checked = (DialOutItemSet.TExportId == 0) ? false : true;
  ckDispId->Checked = (DialOutItemSet.TDispId == 0) ? false : true;
  if (DialOutItemSet.TModifyId == 0)
    cbModifyId->Text = "受理介面不顯示";
  else if (DialOutItemSet.TModifyId == 1)
    cbModifyId->Text = "受理介面顯示";
  else if (DialOutItemSet.TModifyId == 2)
    cbModifyId->Text = "受理介面輸入";
  editMaskEdit->Text = DialOutItemSet.TMaskEditStr;
  editHintStr->Text = DialOutItemSet.THintStr;

  if (cbControlType->Text == "短文本框" || cbControlType->Text == "長文本框")
  {
    Label8->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormEditDialOutItemSet->Height = 390;
  }
  else if (cbControlType->Text == "規定格式輸入框")
  {
    Label8->Visible = true;
    editMaskEdit->Visible = true;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormEditDialOutItemSet->Height = 720;
  }
  else
  {
    Label8->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小值：";
    Label5->Caption = "最大值：";
    FormEditDialOutItemSet->Height = 390;
  }
  switch (DialOutItemSet.TWith)
  {
    case 0: cbWith->Text = "標准寬度"; break;
    case 1: cbWith->Text = "標准寬度四分之一"; break;
    case 2: cbWith->Text = "標准寬度二分之一"; break;
    case 3: cbWith->Text = "標准寬度四分之三"; break;
    default: cbWith->Text = "標准寬度"; break;
  }
  if (DialOutItemSet.SearchId == 0)
    cbSearchId->Text = "無";
  else if (DialOutItemSet.SearchId == 1)
    cbSearchId->Text = "模糊查詢";
  else if (DialOutItemSet.SearchId == 2)
    cbSearchId->Text = "精準查詢";
  cseSearchOrder->Value = DialOutItemSet.SearchOrder;
  cseDispOrder->Value = DialOutItemSet.DispOrder;

  Button1->Enabled = false;
}
int TFormEditDialOutItemSet::GetDialOutItemSetData()
{
  if (MyIsNumber(editTid->Text) != 0)
  {
    MessageBox(NULL,"請輸入受理框序號！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  int tid = StrToInt(editTid->Text);
  if (tid < 1 || tid > MAX_ACPT_CONTROL_NUM)
  {
    MessageBox(NULL,"對不起，受理框序號取值範圍為1-20","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (editLabelName->Text == "")
  {
    MessageBox(NULL,"請輸入受理項名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  DialOutItemSet.TaskId = m_nTaskId;
  DialOutItemSet.Tid = tid;
  DialOutItemSet.TFieldName = "ItemData"+IntToStr(tid);
  DialOutItemSet.TControlType = dmAgent->ControlTypeItemList.GetItemValue(cbControlType->Text);
  DialOutItemSet.TLabelCaption = editLabelName->Text;

  DialOutItemSet.TDefaultData = editDefaultData->Text;
  if (DialOutItemSet.TControlType == 2)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsNumber(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，選項框預設值必須為整數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      if (cseTMinValue->Value != 0 && cseTMaxValue->Value != 0 &&
        (StrToInt(editDefaultData->Text) < cseTMinValue->Value || StrToInt(editDefaultData->Text) > cseTMaxValue->Value))
      {
        MessageBox(NULL,"對不起，選項框預設值不在設置的最小最大值範圍內","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      DialOutItemSet.TDefaultData = IntToStr(cseTMinValue->Value);
    }
  }
  else if (DialOutItemSet.TControlType == 5)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsNumber(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，數值框預設值必須為整數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      if (cseTMinValue->Value != 0 && cseTMaxValue->Value != 0 &&
        (StrToInt(editDefaultData->Text) < cseTMinValue->Value || StrToInt(editDefaultData->Text) > cseTMaxValue->Value))
      {
        MessageBox(NULL,"對不起，數值框預設值不在設置的最小最大值範圍內","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      DialOutItemSet.TDefaultData = IntToStr(cseTMinValue->Value);
    }
  }
  else if (DialOutItemSet.TControlType == 6)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsFloat(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，金額框預設值必須為整數或小數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      DialOutItemSet.TDefaultData = "0";
    }
  }
  else if (DialOutItemSet.TControlType == 7)
  {
    if (editDefaultData->Text != "" && MyIsDate(editDefaultData->Text) != 0)
    {
      MessageBox(NULL,"對不起，日期類型預設值必須為正確的日期","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
  }
  else if (DialOutItemSet.TControlType == 8)
  {
    if (editDefaultData->Text != "")
    {
      if (editDefaultData->Text != "1" && editDefaultData->Text != "0")
      {
        MessageBox(NULL,"對不起，是非類型預設值必須為0或1","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      DialOutItemSet.TDefaultData = "1";
    }
  }

  DialOutItemSet.TMinValue = cseTMinValue->Value;
  DialOutItemSet.TMaxValue = cseTMaxValue->Value;

  (ckIsAllowNull->Checked == true) ? DialOutItemSet.TIsAllowNull = 1 : DialOutItemSet.TIsAllowNull = 0;
  (ckExportId->Checked == true) ? DialOutItemSet.TExportId = 1 : DialOutItemSet.TExportId = 0;
  (ckDispId->Checked == true) ? DialOutItemSet.TDispId = 1 : DialOutItemSet.TDispId = 0;
  if (cbModifyId->Text == "受理介面不顯示")
    DialOutItemSet.TModifyId = 0;
  else if (cbModifyId->Text == "受理介面顯示")
    DialOutItemSet.TModifyId = 1;
  else if (cbModifyId->Text == "受理介面輸入")
    DialOutItemSet.TModifyId = 2;
  DialOutItemSet.TMaskEditStr = editMaskEdit->Text;
  DialOutItemSet.THintStr = editHintStr->Text;

  if (cbWith->Text == "標准寬度")
    DialOutItemSet.TWith = 0;
  else if (cbWith->Text == "標准寬度四分之一")
    DialOutItemSet.TWith = 1;
  else if (cbWith->Text == "標准寬度二分之一")
    DialOutItemSet.TWith = 2;
  else if (cbWith->Text == "標准寬度四分之三")
    DialOutItemSet.TWith = 3;

  if (cbSearchId->Text == "無")
    DialOutItemSet.SearchId = 0;
  else if (cbSearchId->Text == "模糊查詢")
    DialOutItemSet.SearchId = 1;
  else if (cbSearchId->Text == "精準查詢")
    DialOutItemSet.SearchId = 2;
  DialOutItemSet.SearchOrder = cseSearchOrder->Value;
  DialOutItemSet.DispOrder = cseDispOrder->Value;

  return 0;
}
void TFormEditDialOutItemSet::ClearDialOutItemSetData()
{
  editTid->Text = "";
  cbControlType->Text = dmAgent->ControlTypeItemList.GetItemName(1);
  editLabelName->Text = "";
  editDefaultData->Text = "";
  cseTMinValue->Value = 0;
  cseTMaxValue->Value = 0;
  ckIsAllowNull->Checked = true;
  ckExportId->Checked = true;
  ckDispId->Checked = true;
  cbModifyId->Text = "受理介面輸入";
  editMaskEdit->Text = "";
  editHintStr->Text = "";
  cbSearchId->Text = "無";
  cseSearchOrder->Value = 0;
  cseDispOrder->Value = 0;
  FormEditDialOutItemSet->Height = 390;

  Button1->Enabled = false;
}
void TFormEditDialOutItemSet::SetTaskId(int taskid)
{
  m_nTaskId = taskid;
}

void __fastcall TFormEditDialOutItemSet::cbControlTypeKeyPress(
      TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutItemSet::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutItemSet::editTidChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutItemSet::FormShow(TObject *Sender)
{
  if (InsertId == true)
    editTid->SetFocus();
  else
    editLabelName->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutItemSet::Button1Click(TObject *Sender)
{
  CDialOutItemSet dialoutitemset;
  if (GetDialOutItemSetData() != 0)
  {
    return;
  }
  if (InsertId == true)
  {
    if (DialOutItemSet.TControlType == 10 && dmAgent->IsDialOutLinkItemExist(DialOutItemSet.TaskId) == 1)
    {
      MessageBox(NULL,"對不起，最多只能創建一個關聯文本下拉框！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->IsDialOutItemSetTidExist(DialOutItemSet.TaskId, DialOutItemSet.Tid) == 1)
    {
      MessageBox(NULL,"對不起，該業務受理框序號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertDialOutItemSet(DialOutItemSet) == 0)
    {
      dmAgent->adoQryDialOutItemSet->Close();
      dmAgent->adoQryDialOutItemSet->Open();
      if (dmAgent->GetDialOutItemSetRecord(dmAgent->adoQryDialOutItemSet, &dialoutitemset) == 0)
      {
        FormMain->QueryCurDialOutItemList(dialoutitemset);
      }
      else
      {
        FormMain->gbDialOutItemSetList->Visible = false;
        dmAgent->adoQryDialOutItemList->Close();
      }
      GetDialOutItemSetData();
      this->Close();
      //MessageBox(NULL,"新增業務受理框成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增業務受理框失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateDialOutItemSet(DialOutItemSet) == 0)
    {
      dmAgent->adoQryDialOutItemSet->Close();
      dmAgent->adoQryDialOutItemSet->Open();
      if (dmAgent->GetDialOutItemSetRecord(dmAgent->adoQryDialOutItemSet, &dialoutitemset) == 0)
      {
        FormMain->QueryCurDialOutItemList(dialoutitemset);
      }
      else
      {
        FormMain->gbDialOutItemSetList->Visible = false;
        dmAgent->adoQryDialOutItemList->Close();
      }
      GetDialOutItemSetData();
      this->Close();
      //MessageBox(NULL,"業務受理框修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"業務受理框修改失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutItemSet::cbControlTypeChange(
      TObject *Sender)
{
  Button1->Enabled = true;
  if (cbControlType->Text == "短文本框" || cbControlType->Text == "長文本框")
  {
    Label8->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormEditDialOutItemSet->Height = 390;
  }
  else if (cbControlType->Text == "規定格式輸入框")
  {
    Label8->Visible = true;
    editMaskEdit->Visible = true;
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
    FormEditDialOutItemSet->Height = 720;
  }
  else
  {
    Label8->Visible = false;
    editMaskEdit->Visible = false;
    Label6->Caption = "最小值：";
    Label5->Caption = "最大值：";
    FormEditDialOutItemSet->Height = 390;
  }
}
//---------------------------------------------------------------------------

