//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editdutytel.h"
#include "main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditDutyTel *FormEditDutyTel;
//---------------------------------------------------------------------------
__fastcall TFormEditDutyTel::TFormEditDutyTel(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditDutyTel::LoadWorkerNameList()
{
  dmAgent->AddWorkerNameCmbItem(cbWorkerName, true);
}
void TFormEditDutyTel::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormEditDutyTel->Caption = "新增值班電話";
  }
  else
  {
    Button1->Caption = "修改";
    FormEditDutyTel->Caption = "修改值班電話";
  }
}
void TFormEditDutyTel::SetDutyTelData(CDutyTel& dutytel)
{
  DutyTel = dutytel;
  LoadWorkerNameList();
  cbWorkerName->Text = DutyTel.WorkerName;
  switch (DutyTel.DayType)
  {
  case 1:
    cbDayType->Text = "每周某天";

    cbStartWeek->Visible = true;
    cbEndWeek->Visible = true;
    if (DutyTel.StartDayTime == "0")
      cbStartWeek->Text = "星期日";
    else if (DutyTel.StartDayTime == "1")
      cbStartWeek->Text = "星期一";
    else if (DutyTel.StartDayTime == "2")
      cbStartWeek->Text = "星期二";
    else if (DutyTel.StartDayTime == "3")
      cbStartWeek->Text = "星期三";
    else if (DutyTel.StartDayTime == "4")
      cbStartWeek->Text = "星期四";
    else if (DutyTel.StartDayTime == "5")
      cbStartWeek->Text = "星期五";
    else if (DutyTel.StartDayTime == "6")
      cbStartWeek->Text = "星期六";

    if (DutyTel.EndDayTime == "0")
      cbEndWeek->Text = "星期日";
    else if (DutyTel.EndDayTime == "1")
      cbEndWeek->Text = "星期一";
    else if (DutyTel.EndDayTime == "2")
      cbEndWeek->Text = "星期二";
    else if (DutyTel.EndDayTime == "3")
      cbEndWeek->Text = "星期三";
    else if (DutyTel.EndDayTime == "4")
      cbEndWeek->Text = "星期四";
    else if (DutyTel.EndDayTime == "5")
      cbEndWeek->Text = "星期五";
    else if (DutyTel.EndDayTime == "6")
      cbEndWeek->Text = "星期六";

    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
    dtpStartTime->Visible = false;
    dtpEndTime->Visible = false;
    break;
  case 2:
    cbDayType->Text = "每月某天";

    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = true;
    cbEndDay->Visible = true;
    cbStartDay->Text = "每月"+DutyTel.StartDayTime+"號";
    cbEndDay->Text = "每月"+DutyTel.EndDayTime+"號";

    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
    dtpStartTime->Visible = false;
    dtpEndTime->Visible = false;
    break;
  case 3:
    cbDayType->Text = "具體某天";

    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = true;
    dtpEndDate->Visible = true;
    dtpStartDate->Date = StrToDateTime(DutyTel.StartDayTime);
    dtpEndDate->Date = StrToDateTime(DutyTel.EndDayTime);

    dtpStartTime->Visible = false;
    dtpEndTime->Visible = false;
    break;
  case 4:
    cbDayType->Text = "連續時間段";

    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = true;
    dtpEndDate->Visible = true;
    dtpStartDate->Date = StrToDateTime(DutyTel.StartDayTime+":00");
    dtpEndDate->Date = StrToDateTime(DutyTel.EndDayTime+":59");
    dtpStartTime->Visible = true;
    dtpEndTime->Visible = true;
    dtpStartTime->Time = StrToDateTime(DutyTel.StartDayTime+":00");
    dtpStartTime->Time = StrToDateTime(DutyTel.EndDayTime+":59");
    break;
  }
  dtpAMStartTime->Time = StrToDateTime(DutyTel.StartTime1+":00");
  dtpAMEndTime->Time = StrToDateTime(DutyTel.EndTime1+":59");
  dtpPMStartTime->Time = StrToDateTime(DutyTel.StartTime2+":00");
  dtpPMEndTime->Time = StrToDateTime(DutyTel.EndTime2+":59");
  editDutyTel1->Text = DutyTel.DutyTele1;
  editDutyTel2->Text = DutyTel.DutyTele2;
  if (DutyTel.OpenFlag == 0)
    cbOpenFlag->Text = "停用";
  else
    cbOpenFlag->Text = "啟用";

  Button1->Enabled = false;
}
int TFormEditDutyTel::GetDutyTelData()
{
  AnsiString strTemp;
  if (cbWorkerName->Text == "")
  {
    MessageBox(NULL,"請輸入值班人的姓名！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (editDutyTel1->Text == "")
  {
    MessageBox(NULL,"請輸入值班電話！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (cbDayType->Text == "每周某天")
  {
    DutyTel.DayType = 1;
    if (cbStartWeek->Text == "星期日")
      DutyTel.StartDayTime = 0;
    else if (cbStartWeek->Text == "星期一")
      DutyTel.StartDayTime = 1;
    else if (cbStartWeek->Text == "星期二")
      DutyTel.StartDayTime = 2;
    else if (cbStartWeek->Text == "星期三")
      DutyTel.StartDayTime = 3;
    else if (cbStartWeek->Text == "星期四")
      DutyTel.StartDayTime = 4;
    else if (cbStartWeek->Text == "星期五")
      DutyTel.StartDayTime = 5;
    else if (cbStartWeek->Text == "星期六")
      DutyTel.StartDayTime = 6;
    if (cbEndWeek->Text == "星期日")
      DutyTel.EndDayTime = 0;
    else if (cbEndWeek->Text == "星期一")
      DutyTel.EndDayTime = 1;
    else if (cbEndWeek->Text == "星期二")
      DutyTel.EndDayTime = 2;
    else if (cbEndWeek->Text == "星期三")
      DutyTel.EndDayTime = 3;
    else if (cbEndWeek->Text == "星期四")
      DutyTel.EndDayTime = 4;
    else if (cbEndWeek->Text == "星期五")
      DutyTel.EndDayTime = 5;
    else if (cbEndWeek->Text == "星期六")
      DutyTel.EndDayTime = 6;
  }
  else if (cbDayType->Text == "每月某天")
  {
    DutyTel.DayType = 2;
    if (cbStartDay->Text.Length() == 7)
      strTemp = cbStartDay->Text.SubString(5,1);
    else
      strTemp = cbStartDay->Text.SubString(5,2);
    DutyTel.StartDayTime = StrToInt(strTemp);
    if (cbEndDay->Text.Length() == 7)
      strTemp = cbEndDay->Text.SubString(5,1);
    else
      strTemp = cbEndDay->Text.SubString(5,2);
    DutyTel.EndDayTime = StrToInt(strTemp);
  }
  else if (cbDayType->Text == "具體某天")
  {
    DutyTel.DayType = 3;
    DutyTel.StartDayTime = dtpStartDate->Date.FormatString("yyyy-mm-dd");
    DutyTel.EndDayTime = dtpEndDate->Date.FormatString("yyyy-mm-dd");
  }
  else if (cbDayType->Text == "連續時間段")
  {
    DutyTel.DayType = 4;
    DutyTel.StartDayTime = dtpStartDate->Date.FormatString("yyyy-mm-dd")+" "+dtpStartTime->Date.FormatString("hh:nn");
    DutyTel.EndDayTime = dtpEndDate->Date.FormatString("yyyy-mm-dd")+" "+dtpEndTime->Date.FormatString("hh:nn");
  }
  DutyTel.StartTime1 = dtpAMStartTime->Date.FormatString("hh:nn");
  DutyTel.EndTime1 = dtpAMEndTime->Date.FormatString("hh:nn");
  DutyTel.StartTime2 = dtpPMStartTime->Date.FormatString("hh:nn");
  DutyTel.EndTime2 = dtpPMEndTime->Date.FormatString("hh:nn");
  DutyTel.DutyTele1 = editDutyTel1->Text;
  DutyTel.DutyTele2 = editDutyTel2->Text;
  if (cbOpenFlag->Text == "啟用")
    DutyTel.OpenFlag = 1;
  else
    DutyTel.OpenFlag = 0;
  DutyTel.WorkerName = cbWorkerName->Text;
  return 0;
}
void TFormEditDutyTel::ClearDutyTelData()
{
}

void __fastcall TFormEditDutyTel::cbDayTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDutyTel::cbWorkerNameChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDutyTel::cbDayTypeChange(TObject *Sender)
{
  Button1->Enabled = true;
  if (cbDayType->Text == "每周某天")
  {
    cbStartWeek->Visible = true;
    cbEndWeek->Visible = true;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
    dtpStartTime->Visible = false;
    dtpEndTime->Visible = false;
  }
  else if (cbDayType->Text == "每月某天")
  {
    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = true;
    cbEndDay->Visible = true;
    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
    dtpStartTime->Visible = false;
    dtpEndTime->Visible = false;
  }
  else if (cbDayType->Text == "具體某天")
  {
    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = true;
    dtpEndDate->Visible = true;
    dtpStartTime->Visible = false;
    dtpEndTime->Visible = false;
  }
  else if (cbDayType->Text == "連續時間段")
  {
    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = true;
    dtpEndDate->Visible = true;
    dtpStartTime->Visible = true;
    dtpEndTime->Visible = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDutyTel::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDutyTel::Button1Click(TObject *Sender)
{
  if (GetDutyTelData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertDutyTelRecord(&DutyTel) == 0)
    {
      //MessageBox(NULL,"新增值班電話記錄成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryDutyTel->Close();
      dmAgent->adoQryDutyTel->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增值班電話記錄失敗!","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateDutyTelRecord(&DutyTel) == 0)
    {
      //MessageBox(NULL,"修改值班電話記錄成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryDutyTel->Close();
      dmAgent->adoQryDutyTel->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改值班電話記錄失敗!","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

