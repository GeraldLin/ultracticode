//---------------------------------------------------------------------------

#ifndef repacceptH
#define repacceptH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
//---------------------------------------------------------------------------
class TFormRepAccept : public TForm
{
__published:	// IDE-managed Components
  TQuickRep *QuickRep1;
  TQRBand *QRBand1;
  TQRLabel *QRLabel1;
  TQRLabel *QRLabel2;
  TQRLabel *QRLabel3;
  TQRLabel *QRLabel4;
  TQRLabel *QRLabel5;
  TQRLabel *SerialNo;
  TQRLabel *CustomNo;
  TQRLabel *CustomTel;
  TQRLabel *CustName;
  TQRLabel *QRLabel6;
  TQRLabel *QRLabel7;
  TQRLabel *QRLabel8;
  TQRLabel *QRLabel9;
  TQRLabel *AcptType;
  TQRLabel *AcptTime;
  TQRLabel *WorkerNo;
  TQRLabel *WorkerName;
  TQRLabel *QRLabel10;
  TQRLabel *QRLabel11;
  TQRLabel *DealState;
  TQRLabel *QRLabel12;
  TQRRichText *QRRichText1;
  TQRRichText *QRRichText2;
  TQRBand *QRBand2;
  TADOQuery *ADOQuery1;
  TStringField *ADOQuery1WorkerNo;
  TDateTimeField *ADOQuery1ProcTime;
  TMemoField *ADOQuery1ProcLog;
  TStringField *ADOQuery1WorkerName;
  TQRLabel *QRLabel14;
  TQRLabel *QRLabel15;
  TQRLabel *QRLabel16;
  TQRDBText *QRDBText1;
  TQRDBText *QRDBText2;
  TQRDBText *QRDBText3;
  TQRDBRichText *QRDBRichText1;
  TQRLabel *QRLabel13;
private:	// User declarations
public:		// User declarations
  __fastcall TFormRepAccept(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRepAccept *FormRepAccept;
//---------------------------------------------------------------------------
#endif
