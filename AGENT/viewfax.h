//---------------------------------------------------------------------------

#ifndef viewfaxH
#define viewfaxH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ImgeditLibCtl_OCX.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <OleCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormViewFax : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel1;
  TPanel *Panel2;
  TImgEdit *ImgEdit1;
  TPanel *Panel3;
  TEdit *editFaxPage;
  TSpeedButton *sbPrevPage;
  TSpeedButton *sbNextPage;
  TSpeedButton *sbZoomOut;
  TSpeedButton *sbZoomIn;
  TSpeedButton *sbRotLeft;
  TSpeedButton *sbRotRight;
  TSpeedButton *spFitWidth;
  TSpeedButton *SpeedButton1;
  TButton *btClose;
  TLabel *Label1;
  TComboBox *cbWorkerNo;
  TButton *Button1;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall sbPrevPageClick(TObject *Sender);
  void __fastcall sbNextPageClick(TObject *Sender);
  void __fastcall sbZoomOutClick(TObject *Sender);
  void __fastcall sbZoomInClick(TObject *Sender);
  void __fastcall spFitWidthClick(TObject *Sender);
  void __fastcall sbRotLeftClick(TObject *Sender);
  void __fastcall sbRotRightClick(TObject *Sender);
  void __fastcall SpeedButton1Click(TObject *Sender);
  void __fastcall btCloseClick(TObject *Sender);
  void __fastcall FormResize(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormViewFax(TComponent* Owner);

  CAccept m_Accept;
  int m_nPageCount;
  int m_nCurPage;
  int m_FaxId;
  AnsiString m_strFaxFileName;
  void SetFaxFileName(AnsiString filename);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormViewFax *FormViewFax;
//---------------------------------------------------------------------------
#endif
