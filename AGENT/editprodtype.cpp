//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editprodtype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditProdType *FormEditProdType;
//---------------------------------------------------------------------------
__fastcall TFormEditProdType::TFormEditProdType(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditProdType::SetDispMsg(bool bInsertId, int nProdType, AnsiString strItemName, AnsiString strGroupNo, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  InsertId = bInsertId;
  if (InsertId == true)
  {
    editProdType->Text = "";
    editProdType->Color = clWindow;
    editProdType->ReadOnly = false;
    editProdName->Text = "";
    editGroupNoList->Text = "0";
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = "";
    memoDemo->Lines->Clear();
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editProdType->Text = nProdType;
    editProdType->Color = clSilver;
    editProdType->ReadOnly = true;
    editProdName->Text = strItemName;
    editGroupNoList->Text = strGroupNo;
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = strItemURL;
    memoDemo->Lines->Clear();
    if (strDemo.Length() > 0)
      memoDemo->Lines->Add(strDemo);
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}

void __fastcall TFormEditProdType::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProdType::editProdTypeChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProdType::FormShow(TObject *Sender)
{
  if (InsertId == true)
    editProdType->SetFocus();
  else
    editProdName->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProdType::cbGroupNoKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProdType::Button1Click(TObject *Sender)
{
  int nProdType;

  if (MyIsNumber(editProdType->Text) != 0)
  {
    MessageBox(NULL,"請輸入產品或服務代碼！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editProdName->Text == "")
  {
    MessageBox(NULL,"請輸入產品或服務名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nProdType = StrToInt(editProdType->Text);
  if (nProdType <= 0)
  {
    MessageBox(NULL,"對不起，產品或服務代碼必須大于0！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (editGroupNoList->Text == "")
    editGroupNoList->Text = "0";

  if (InsertId == true)
  {
    if (dmAgent->IsProdTypeExist(nProdType) == 1)
    {
      MessageBox(NULL,"對不起，該產品或服務編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertProdTypeRecord(nProdType, editProdName->Text, editGroupNoList->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryProdType->Close();
      dmAgent->adoQryProdType->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增產品或服務編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateProdTypeRecord(nProdType, editProdName->Text, editGroupNoList->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryProdType->Close();
      dmAgent->adoQryProdType->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改產品或服務編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

