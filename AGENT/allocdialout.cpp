//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "allocdialout.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAllocDailOut *FormAllocDailOut;
extern int g_nBandSeatNoType;
extern AnsiString strDBType;
extern int g_nOrderEndCode1;
int g_nTotalAllocCount=0;
int g_nAllocCount[256];
int g_nAllocedCount[256];
int g_nListViewIndex[256];
int g_nSelAllocIndex=-1;
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormAllocDailOut::TFormAllocDailOut(TComponent* Owner)
  : TForm(Owner)
{
  TaskId = 0;
}

//---------------------------------------------------------------------------
void __fastcall TFormAllocDailOut::Button2Click(TObject *Sender)
{
  this->Close();  
}

//---------------------------------------------------------------------------
void __fastcall TFormAllocDailOut::Button1Click(TObject *Sender)
{
  char dispbuf[128];
  AnsiString strWhere, sqlbuf, strCalledNo;
  AnsiString sqlTaskId="";
  AnsiString sqlWorkerNo="";
  AnsiString sqlResult="";
  AnsiString sqlOrder="";
  TListItem  *ListItem1;
  TStringList *MyStringList = new TStringList();

  int ItemCount, nRecdCount, nFirstDialId, nEndDialId, AllocCount=0, nDialId;
  int c=0, n=0;
  bool bAlloced=false;

  try
  {
    int j=0;
    ItemCount = ListView1->Items->Count;
    for (int i = 0; i < ItemCount && i < 256; i ++)
    {
      ListItem1 = ListView1->Items->Item[i];
      if (ListItem1->Checked == true)
      {
        MyStringList->Add(ListItem1->SubItems->Strings[0]);
        g_nListViewIndex[j] = i;
        g_nAllocCount[j] = StrToInt(ListItem1->SubItems->Strings[3]);
        j++;
      }
      g_nAllocedCount[i] = 0;
    }
    if (MyStringList->Count > 0)
    {
      strWhere = strSqlWhere;
      if (CheckBox1->Checked == false)
      {
        if (strWhere == "")
          strWhere = "(TaskWorkerNo='' or TaskWorkerNo is null)";
        else
          strWhere = strWhere + " and (TaskWorkerNo='' or TaskWorkerNo is null)";
      }
      if (CheckBox2->Checked == false)
      {
        if (strWhere == "")
          strWhere = "LastResult<>102";
        else
          strWhere = strWhere + " and LastResult<>102";
      }
      if (CheckBox3->Checked == false)
      {
        if (strWhere == "")
          strWhere = "EndCode1<>"+IntToStr(g_nOrderEndCode1);
        else
          strWhere = strWhere + " and EndCode1<>"+IntToStr(g_nOrderEndCode1);
      }
      if (TaskId > 0)
      {
        if (strWhere == "")
          strWhere = "TaskId=" + IntToStr(TaskId);
        else
          strWhere = strWhere + " and TaskId=" + IntToStr(TaskId);
      }

      if (strWhere.Length() > 0)
        sqlbuf = "Select TaskId,Id,CalledNo from tbDialOutTele where "+strWhere+" order by Id";
      else
        sqlbuf = "Select TaskId,Id,CalledNo from tbDialOutTele order by Id";

      dmAgent->adoqueryPub->SQL->Clear();
      dmAgent->adoqueryPub->SQL->Add(sqlbuf);
      dmAgent->adoqueryPub->Open();
      nRecdCount = dmAgent->adoqueryPub->RecordCount;

      Panel2->Caption = "正在分配外撥號碼(0/"+IntToStr(nRecdCount)+"條)，請稍等......";
      Panel2->Visible = true;
      Application->ProcessMessages();

      while ( !dmAgent->adoqueryPub->Eof )
      {
        //taskid = dmAgent->adoqueryPub->Fields->Fields[0]->AsInteger;
        nDialId = dmAgent->adoqueryPub->Fields->Fields[1]->AsInteger;
        strCalledNo = dmAgent->adoqueryPub->Fields->Fields[2]->AsString;

        bAlloced = false;
        for (n=0; n<MyStringList->Count; n++)
        {
          if (g_nAllocedCount[c] < g_nAllocCount[c])
          {
            bAlloced = true;
            break;
          }
          else
          {
            c = (c+1)%MyStringList->Count;
          }
        }
        if (bAlloced == false)
        {
          break;
        }
        if (dmAgent->AllocDialOutWorkerNo(nDialId, MyStringList->Strings[c]) > 0)
        {
          AllocCount = AllocCount+1;
          g_nAllocedCount[c] = g_nAllocedCount[c]+1;
          c = (c+1)%MyStringList->Count;
        }

        if ((AllocCount%50) == 0)
        {
          Panel2->Caption = "正在分配外撥號碼("+IntToStr(AllocCount)+"/"+IntToStr(nRecdCount)+"條)，請稍等......";
          Application->ProcessMessages();
        }
        dmAgent->adoqueryPub->Next();
      }
      dmAgent->adoqueryPub->Close();
    }
    for (int i = 0; i < MyStringList->Count; i ++)
    {
      ListItem1 = ListView1->Items->Item[g_nListViewIndex[i]];
      ListItem1->SubItems->Strings[4] = IntToStr(g_nAllocedCount[i]);
    }
  }
  __finally
  {
    delete MyStringList;
    MyStringList = NULL;
    Panel2->Visible = false;
  }
  Panel2->Visible = false;
  if (AllocCount > 0)
  {
    sprintf(dispbuf, "成功分配 %d 條記錄！", AllocCount);
    MessageBox(NULL,dispbuf,"訊息提示",MB_OK|MB_ICONINFORMATION);
  }
  else
  {
    MessageBox(NULL,"未分配成功記錄！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::FormShow(TObject *Sender)
{
  try
  {
    AnsiString strWhere, sqlbuf;

    strWhere = strSqlWhere;
    if (CheckBox1->Checked == false)
    {
      if (strWhere == "")
        strWhere = "(TaskWorkerNo='' or TaskWorkerNo is null)";
      else
        strWhere = strWhere + " and (TaskWorkerNo='' or TaskWorkerNo is null)";
    }
    if (CheckBox2->Checked == false)
    {
      if (strWhere == "")
        strWhere = "LastResult<>102";
      else
        strWhere = strWhere + " and LastResult<>102";
    }
    if (CheckBox3->Checked == false)
    {
      if (strWhere == "")
        strWhere = "EndCode1<>"+IntToStr(g_nOrderEndCode1);
      else
        strWhere = strWhere + " and EndCode1<>"+IntToStr(g_nOrderEndCode1);
    }
    if (TaskId > 0)
    {
      if (strWhere == "")
        strWhere = "TaskId=" + IntToStr(TaskId);
      else
        strWhere = strWhere + " and TaskId=" + IntToStr(TaskId);
    }

    if (strWhere.Length() > 0)
      sqlbuf = "Select count(*) as newcount from tbDialOutTele where "+strWhere+"";
    else
      sqlbuf = "Select count(*) as newcount from tbDialOutTele";

    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add(sqlbuf);
    dmAgent->adoqueryPub->Open();
    g_nTotalAllocCount = dmAgent->adoqueryPub->FieldByName("newcount")->AsInteger;
    Edit2->Text = g_nTotalAllocCount;
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
    Edit2->Text = "0";
    g_nTotalAllocCount = 0;
  }
  dmAgent->AddGroupListView(dmAgent->WorkerGroupList, FormAllocDailOut->ListView2);
  ListView1->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::CheckBox5Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int ItemCount = ListView1->Items->Count;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView1->Items->Item[i];
    ListItem1->Checked = CheckBox5->Checked;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::Button3Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int ItemCount = ListView1->Items->Count;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView1->Items->Item[i];
    ListItem1->Checked = ! ListItem1->Checked;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::ListView2Change(TObject *Sender,
      TListItem *Item, TItemChange Change)
{
  //MessageBox(NULL,"未分配成功記錄！","訊息提示",MB_OK|MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::Button5Click(TObject *Sender)
{
  AnsiString strGroupNoList="", sql;
  if (MyWorker.WorkType == 9)
  {
    dmAgent->AddWorkerListView(FormAllocDailOut->ListView1, "");
  }
  else
  {
    if (MyWorker.GroupNo > 0)
    {
      if (strGroupNoList == "")
        strGroupNoList = MyWorker.GroupNo;
      else
        strGroupNoList = strGroupNoList + "," + IntToStr(MyWorker.GroupNo);
    }
    if (MyWorker.GroupNo1 > 0)
    {
      if (strGroupNoList == "")
        strGroupNoList = MyWorker.GroupNo1;
      else
        strGroupNoList = strGroupNoList + "," + IntToStr(MyWorker.GroupNo1);
    }
    if (MyWorker.GroupNo2 > 0)
    {
      if (strGroupNoList == "")
        strGroupNoList = MyWorker.GroupNo2;
      else
        strGroupNoList = strGroupNoList + "," + IntToStr(MyWorker.GroupNo2);
    }
    if (MyWorker.GroupNo3 > 0)
    {
      if (strGroupNoList == "")
        strGroupNoList = MyWorker.GroupNo3;
      else
        strGroupNoList = strGroupNoList + "," + IntToStr(MyWorker.GroupNo3);
    }
    if (MyWorker.GroupNo4 > 0)
    {
      if (strGroupNoList == "")
        strGroupNoList = MyWorker.GroupNo4;
      else
        strGroupNoList = strGroupNoList + "," + IntToStr(MyWorker.GroupNo4);
    }
    sql.sprintf("GroupNo in (%s) or GroupNo1 in (%s) or GroupNo2 in (%s) or GroupNo3 in (%s) or GroupNo4 in (%s)", strGroupNoList, strGroupNoList, strGroupNoList, strGroupNoList ,strGroupNoList);
    dmAgent->AddWorkerListView(FormAllocDailOut->ListView1, sql);
  }
  for (int i = 0; i < 256; i ++)
  {
    g_nAllocCount[i] = 0;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::Button4Click(TObject *Sender)
{
  TListItem  *ListItem1;
  AnsiString strGroupNoList, sql;
  int nCount=0;

  int ItemCount = ListView2->Items->Count;
  for (int i = 0; i < ItemCount && i < 32; i ++)
  {
    if (i < 256)
      g_nAllocCount[i] = 0;
    ListItem1 = ListView2->Items->Item[i];
    if (ListItem1->Checked == true)
    {
      if (nCount == 0)
        strGroupNoList = ListItem1->Caption;
      else
        strGroupNoList = strGroupNoList+","+ListItem1->Caption;
      nCount++;
    }
  }
  if (nCount == 0)
  {
    ListView1->Items->Clear();
    return;
  }
  sql.sprintf("GroupNo in (%s) or GroupNo1 in (%s) or GroupNo2 in (%s) or GroupNo3 in (%s) or GroupNo4 in (%s)", strGroupNoList, strGroupNoList, strGroupNoList, strGroupNoList ,strGroupNoList);
  dmAgent->AddWorkerListView(FormAllocDailOut->ListView1, sql);
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::Button6Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int ItemCount = ListView2->Items->Count;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView2->Items->Item[i];
    ListItem1->Checked = ! ListItem1->Checked;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::CheckBox6Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int ItemCount = ListView2->Items->Count;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView2->Items->Item[i];
    ListItem1->Checked = CheckBox6->Checked;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::Button7Click(TObject *Sender)
{
  if (g_nTotalAllocCount == 0)
  {
    MessageBox(NULL,"沒有可分配的號碼！","訊息提示",MB_OK|MB_ICONINFORMATION);
    return;
  }
  TListItem  *ListItem1;
  int SelCount=0, AllocCount, ys, j;
  int ItemCount = ListView1->Items->Count;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView1->Items->Item[i];
    if (ListItem1->Checked == true)
    {
      SelCount++;
    }
  }
  AllocCount = g_nTotalAllocCount/SelCount;
  ys = g_nTotalAllocCount-AllocCount*SelCount;
  j=0;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView1->Items->Item[i];
    if (ListItem1->Checked == true)
    {
      if (j<ys)
        ListItem1->SubItems->Strings[3] = IntToStr(AllocCount+1);
      else
        ListItem1->SubItems->Strings[3] = IntToStr(AllocCount);
      j++;
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::Button8Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int ItemCount = ListView1->Items->Count;
  for (int i = 0; i < ItemCount && i < 256; i ++)
  {
    ListItem1 = ListView1->Items->Item[i];
    if (ListItem1->Checked == true)
    {
      ListItem1->SubItems->Strings[3] = MyStrToInt(Edit1->Text);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::ListView1SelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nSelAllocIndex = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::ListView1DblClick(TObject *Sender)
{
  if (g_nSelAllocIndex < 0)
    return;
  AnsiString strNum="";
  if (InputQuery("輸入提示：", "請輸入給該值機員分配的數量：", strNum))
  {
    if (MyIsNumber(strNum) == 0)
    {
      TListItem  *ListItem1;
      ListItem1 = ListView1->Items->Item[g_nSelAllocIndex];
      ListItem1->SubItems->Strings[3] = strNum;
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormAllocDailOut::CheckBox1Click(TObject *Sender)
{ //2015-05-23
  try
  {
    AnsiString strWhere, sqlbuf;

    strWhere = strSqlWhere;
    if (CheckBox1->Checked == false)
    {
      if (strWhere == "")
        strWhere = "(TaskWorkerNo='' or TaskWorkerNo is null)";
      else
        strWhere = strWhere + " and (TaskWorkerNo='' or TaskWorkerNo is null)";
    }
    if (CheckBox2->Checked == false)
    {
      if (strWhere == "")
        strWhere = "LastResult<>102";
      else
        strWhere = strWhere + " and LastResult<>102";
    }
    if (CheckBox3->Checked == false)
    {
      if (strWhere == "")
        strWhere = "EndCode1<>"+IntToStr(g_nOrderEndCode1);
      else
        strWhere = strWhere + " and EndCode1<>"+IntToStr(g_nOrderEndCode1);
    }
    if (TaskId > 0)
    {
      if (strWhere == "")
        strWhere = "TaskId=" + IntToStr(TaskId);
      else
        strWhere = strWhere + " and TaskId=" + IntToStr(TaskId);
    }

    if (strWhere.Length() > 0)
      sqlbuf = "Select count(*) as newcount from tbDialOutTele where "+strWhere+"";
    else
      sqlbuf = "Select count(*) as newcount from tbDialOutTele";

    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add(sqlbuf);
    dmAgent->adoqueryPub->Open();
    g_nTotalAllocCount = dmAgent->adoqueryPub->FieldByName("newcount")->AsInteger;
    Edit2->Text = g_nTotalAllocCount;
    dmAgent->adoqueryPub->Close();
  }
  catch ( ... )
  {
    Edit2->Text = "0";
    g_nTotalAllocCount = 0;
  }
}
//---------------------------------------------------------------------------

