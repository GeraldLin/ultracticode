//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <inifiles.hpp>
#include "seatparam.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormSeatParam *FormSeatParam;
//---------------------------------------------------------------------------
__fastcall TFormSeatParam::TFormSeatParam(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSeatParam::btCloseClick(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void TFormSeatParam::ReadIniFile()
{
  TIniFile *IniKey    = new TIniFile ( ExtractFilePath(Application->ExeName)+"agent.ini" );

  cspServerID->Value = IniKey->ReadInteger ( "CONFIGURE", "ServerID", 1 );
  cspServerPort->Value = IniKey->ReadInteger ( "CONFIGURE", "ServerPort", 5227 );
  edServerIP->Text = IniKey->ReadString ( "CONFIGURE", "ServerIP", "127.0.0.1" );
  edSeatIP->Text = FormMain->IdIPWatch1->LocalIP();

  int nBandSeatNoType = IniKey->ReadInteger ( "CONFIGURE", "BandSeatNoType", 1 );
  if (nBandSeatNoType == 1)
    cbGetSeatNo->Text = "與IP綁定";
  else if (nBandSeatNoType == 2)
    cbGetSeatNo->Text = "手動綁定";
  else
    cbGetSeatNo->Text = "不綁定";

  ckDBParamType->Checked = IniKey->ReadBool ( "CONFIGURE", "DBParamType", false );
  
  edSeatNo->Text = IniKey->ReadString ( "CONFIGURE", "SeatNo", "" );
  if (IniKey->ReadBool ( "CONFIGURE", "AutoReady", true ) == true)
    cbAutoReady->Text = "是";
  else
    cbAutoReady->Text = "否";

  cbxDBType->Text = IniKey->ReadString ( "CONFIGURE", "DBTYPE", "SQLSERVER" );
  edtServer->Text = IniKey->ReadString ( "CONFIGURE", "DBSERVER", "127.0.0.1" );
  edtDB->Text = IniKey->ReadString ( "CONFIGURE", "DATABASE", "ivrdb" );
  edtUSER->Text = IniKey->ReadString ( "CONFIGURE", "USERID", "myvxml" );
  edtPSW->Text = DecodePass(IniKey->ReadString ( "CONFIGURE", "PASSWORD", "pebpyhns{khc_s" ));

  if (nBandSeatNoType == 2)
  {
    Label52->Visible = true;
    edSeatNo->Visible = true;
  }
  else
  {
    Label52->Visible = false;
    edSeatNo->Visible = false;
  }

  if (nBandSeatNoType == 0 || ckDBParamType->Checked == true)
  {
    GroupBox2->Visible = true;
    FormSeatParam->Height = 520;
    btSaveParam->Top = 434;
    btClose->Top = 434;
  }
  else
  {
    GroupBox2->Visible = false;
    FormSeatParam->Height = 355;
    btSaveParam->Top = 264;
    btClose->Top = 264;
  }
  if (cbxDBType->Text == "ORACLE")
  {
    Label37->Caption = "TNS Service Name:";
    Label16->Visible = false;
    edtDB->Visible = false;
  }
  else if (cbxDBType->Text == "ODBC")
  {
    Label37->Caption = "DNS Name:";
    Label16->Visible = false;
    edtDB->Visible = false;
  }
  else
  {
    Label37->Caption = "資料伺服器名稱:";
    Label16->Visible = true;
    edtDB->Visible = true;
  }

  delete IniKey;
  btSaveParam->Enabled = false;
}
void __fastcall TFormSeatParam::btSaveParamClick(TObject *Sender)
{
  if ( MessageBox( NULL, "您真的要修改坐席參數嗎？", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    TIniFile *IniKey    = new TIniFile ( ExtractFilePath(Application->ExeName)+"agent.ini" ) ;

    IniKey->WriteInteger( "CONFIGURE", "ServerID", cspServerID->Value );
    IniKey->WriteString( "CONFIGURE", "ServerIP", edServerIP->Text );
    IniKey->WriteInteger( "CONFIGURE", "ServerPort", cspServerPort->Value );
    if (cbGetSeatNo->Text == "與IP綁定")
      IniKey->WriteInteger( "CONFIGURE", "BandSeatNoType", 1 );
    else if (cbGetSeatNo->Text == "手動綁定")
      IniKey->WriteInteger( "CONFIGURE", "BandSeatNoType", 2 );
    else
      IniKey->WriteInteger( "CONFIGURE", "BandSeatNoType", 0 );

    IniKey->WriteString( "CONFIGURE", "SeatIP", edSeatIP->Text );
    IniKey->WriteString( "CONFIGURE", "SeatNo", edSeatNo->Text );
    if (cbAutoReady->Text == "是")
      IniKey->WriteInteger( "CONFIGURE", "AutoReady", 1 );
    else
      IniKey->WriteInteger( "CONFIGURE", "AutoReady", 0 );
    IniKey->WriteBool( "CONFIGURE", "DBParamType", ckDBParamType->Checked );

    IniKey->WriteString( "CONFIGURE", "DBTYPE", cbxDBType->Text );
    IniKey->WriteString( "CONFIGURE", "DBSERVER", edtServer->Text );
    IniKey->WriteString( "CONFIGURE", "DATABASE", edtDB->Text );
    IniKey->WriteString( "CONFIGURE", "USERID", edtUSER->Text );
    IniKey->WriteString( "CONFIGURE", "PASSWORD", EncodePass(edtPSW->Text));

    delete IniKey;
    btSaveParam->Enabled = false;
    this->Close();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormSeatParam::cbxDBTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormSeatParam::cspServerPortChange(TObject *Sender)
{
  btSaveParam->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormSeatParam::cbGetSeatNoChange(TObject *Sender)
{
  btSaveParam->Enabled = true;
  if (cbGetSeatNo->Text == "不綁定")
  {
    GroupBox2->Visible = true;
    FormSeatParam->Height = 520;
    btSaveParam->Top = 434;
    btClose->Top = 434;
  }
  else
  {
    GroupBox2->Visible = false;
    FormSeatParam->Height = 355;
    btSaveParam->Top = 264;
    btClose->Top = 264;
  }
  if (cbGetSeatNo->Text == "手動綁定")
  {
    Label52->Visible = true;
    edSeatNo->Visible = true;
  }
  else
  {
    Label52->Visible = false;
    edSeatNo->Visible = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormSeatParam::ckDBParamTypeClick(TObject *Sender)
{
  if (ckDBParamType->Checked == true)
  {
    GroupBox2->Visible = true;
    FormSeatParam->Height = 520;
    btSaveParam->Top = 434;
    btClose->Top = 434;
  }
  else
  {
    GroupBox2->Visible = false;
    FormSeatParam->Height = 355;
    btSaveParam->Top = 264;
    btClose->Top = 264;
  }
  btSaveParam->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormSeatParam::cbxDBTypeChange(TObject *Sender)
{
  btSaveParam->Enabled = true;
  if (cbxDBType->Text == "ORACLE")
  {
    Label37->Caption = "TNS Service Name:";
    Label16->Visible = false;
    edtDB->Visible = false;
  }
  else if (cbxDBType->Text == "ODBC")
  {
    Label37->Caption = "DNS Name:";
    Label16->Visible = false;
    edtDB->Visible = false;
  }
  else
  {
    Label37->Caption = "資料伺服器名稱:";
    Label16->Visible = true;
    edtDB->Visible = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormSeatParam::FormShow(TObject *Sender)
{
  ReadIniFile();
}
//---------------------------------------------------------------------------

