//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dialnext.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDialNext *FormDialNext;
//---------------------------------------------------------------------------
__fastcall TFormDialNext::TFormDialNext(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormDialNext::Button1Click(TObject *Sender)
{
  FormMain->DialNextPhone();
  this->Close();
}
//---------------------------------------------------------------------------
