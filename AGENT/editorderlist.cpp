//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editorderlist.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditOrderList *FormEditOrderList;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormEditOrderList::TFormEditOrderList(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditOrderList::SetOrderListData(COrderlist &orderlist)
{
  Orderlist = orderlist;
  editOrderId->Text = Orderlist.OrderId;
  editProductNo->Text = Orderlist.ProductNo;
  editProductName->Text = Orderlist.ProductName;
  editTotals->Text = Orderlist.Totals;
  cseExpressTotals->Value = Orderlist.ExpressTotals;
  editBarCode->Text = Orderlist.BarCode;
  Button1->Enabled = false;
}
int TFormEditOrderList::GetOrderListData()
{
  Orderlist.ExpressTotals = cseExpressTotals->Value;
  Orderlist.BarCode = editBarCode->Text;
  return 0;
}
void __fastcall TFormEditOrderList::cseExpressTotalsChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditOrderList::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderList::Button1Click(TObject *Sender)
{
  char dispbuf[256];

  if (GetOrderListData() != 0)
    return;

  if (dmAgent->UpdateExpressTotals(Orderlist.id, Orderlist.ExpressTotals, Orderlist.BarCode) == 0)
  {
    dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改訂單：%s 產品%s的發貨情況記錄",
      Orderlist.OrderId.c_str(), Orderlist.ProductNo.c_str());
    MessageBox(NULL,"修改訂單付款情況成功!","信息提示",MB_OK|MB_ICONINFORMATION);
    dmAgent->adoQryOrderItem->Close();
    dmAgent->adoQryOrderItem->Open();
    this->Close();
  }
  else
  {
    MessageBox(NULL,"修改訂單發貨情況失敗!","信息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------

