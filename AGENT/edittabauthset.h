//---------------------------------------------------------------------------

#ifndef edittabauthsetH
#define edittabauthsetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormTabAuthSet : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editTabName;
  TCheckBox *ckAdmin;
  TCheckBox *ckMaint;
  TCheckBox *ckMonit;
  TCheckBox *ckOpert;
  TCheckBox *ckAgent;
  TButton *Button1;
  TButton *Button2;
  void __fastcall ckAdminClick(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormTabAuthSet(TComponent* Owner);

  CTabAuth curTabAuth;
  void SetTabAuth(CTabAuth &tabauth);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormTabAuthSet *FormTabAuthSet;
//---------------------------------------------------------------------------
#endif
