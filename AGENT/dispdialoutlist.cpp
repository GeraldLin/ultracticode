//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "dispdialoutlist.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "WMPLib_OCX"
#pragma resource "*.dfm"
TFormDispDialOutList *FormDispDialOutList;

extern int MaxRecordPerPage;
extern int DLSTRecdcount;
extern AnsiString DLSTWhereSql;
extern int DLSTTotalPage;
extern int CurDLSTPageNo;
extern AnsiString FtpServer1;

//---------------------------------------------------------------------------
__fastcall TFormDispDialOutList::TFormDispDialOutList(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormDispDialOutList::Button1Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormDispDialOutList::DBGrid1CellClick(TColumn *Column)
{
  if (dmAgent->adoQryDialOutList->Eof)
    dmAgent->adoQryDialOutList->Prior();
  try
  {
    AnsiString strDialLog = dmAgent->adoQryDialOutList->FieldByName("DialLog")->AsString;
    Memo1->Lines->Clear();
    if (strDialLog.Length() > 0)
      Memo1->Lines->Add(strDialLog);
    Button2->Enabled = false;
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormDispDialOutList::btnDLSTPriorClick(TObject *Sender)
{
  char sqlbuf[1024];
  int n;

  if (CurDLSTPageNo > 1)
  {
    n = (CurDLSTPageNo-2)*MaxRecordPerPage;
    if (DLSTWhereSql.Length() > 0)
        sprintf(sqlbuf, "select top %d * from tbdialoutlist where %s and id not in (select top %d id from tbdialoutlist where %s order by id desc) order by id desc",
          MaxRecordPerPage, DLSTWhereSql.c_str(), n, DLSTWhereSql.c_str());
      else
        sprintf(sqlbuf, "select top %d * from tbdialoutlist where id not in (select top %d id from tbdialoutlist order by id desc) order by id desc",
          MaxRecordPerPage, n);

    try
    {
      dmAgent->adoQyrCallCdr->SQL->Clear();
      dmAgent->adoQyrCallCdr->SQL->Add((char *)sqlbuf);
      dmAgent->adoQyrCallCdr->Open();

      CurDLSTPageNo = CurDLSTPageNo-1;
      lblDLSTLocaPage->Caption = "第"+IntToStr(CurDLSTPageNo)+"頁";
      cseDLSTLocaPage->Value = CurDLSTPageNo;
      if (CurDLSTPageNo == 1)
      {
        btnDLSTPrior->Enabled = false;
      }
      if (DLSTTotalPage > 1)
        btnDLSTNext->Enabled = true;
      //2015-04-09
      AnsiString strDialLog = dmAgent->adoQryDialOutList->FieldByName("DialLog")->AsString;
      Memo1->Lines->Clear();
      if (strDialLog.Length() > 0)
        Memo1->Lines->Add(strDialLog);
      Button2->Enabled = false;
    }
    catch (...)
    {
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDispDialOutList::btnDLSTNextClick(TObject *Sender)
{
  char sqlbuf[1024];
  int n;

  if (CurDLSTPageNo < DLSTTotalPage)
  {
    n = (CurDLSTPageNo)*MaxRecordPerPage;
    if (DLSTWhereSql.Length() > 0)
        sprintf(sqlbuf, "select top %d * from tbdialoutlist where %s and id not in (select top %d id from tbdialoutlist where %s order by id desc) order by id desc",
          MaxRecordPerPage, DLSTWhereSql.c_str(), n, DLSTWhereSql.c_str());
      else
        sprintf(sqlbuf, "select top %d * from tbdialoutlist where id not in (select top %d id from tbdialoutlist order by id desc) order by id desc",
          MaxRecordPerPage, n);

    try
    {
      dmAgent->adoQyrCallCdr->SQL->Clear();
      dmAgent->adoQyrCallCdr->SQL->Add((char *)sqlbuf);
      dmAgent->adoQyrCallCdr->Open();

      CurDLSTPageNo = CurDLSTPageNo+1;
      lblDLSTLocaPage->Caption = "第"+IntToStr(CurDLSTPageNo)+"頁";
      cseDLSTLocaPage->Value = CurDLSTPageNo;
      if (CurDLSTPageNo == DLSTTotalPage)
        btnDLSTNext->Enabled = false;
      if (DLSTTotalPage > 1)
        btnDLSTPrior->Enabled = true;
      //2015-04-09
      AnsiString strDialLog = dmAgent->adoQryDialOutList->FieldByName("DialLog")->AsString;
      Memo1->Lines->Clear();
      if (strDialLog.Length() > 0)
        Memo1->Lines->Add(strDialLog);
      Button2->Enabled = false;
    }
    catch (...)
    {
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDispDialOutList::btnDLSTGoClick(TObject *Sender)
{
  char sqlbuf[1024];
  int n;

  if (cseDLSTLocaPage->Value >= 1 && cseDLSTLocaPage->Value <= DLSTTotalPage)
  {
    n = (cseDLSTLocaPage->Value-1)*MaxRecordPerPage;
    if (DLSTWhereSql.Length() > 0)
      sprintf(sqlbuf, "select top %d * from tbdialoutlist where %s and id not in (select top %d id from tbdialoutlist where %s order by id desc) order by id desc",
        MaxRecordPerPage, DLSTWhereSql.c_str(), n, DLSTWhereSql.c_str());
    else
      sprintf(sqlbuf, "select top %d * from tbdialoutlist where id not in (select top %d id from tbdialoutlist order by id desc) order by id desc",
        MaxRecordPerPage, n);

    try
    {
      dmAgent->adoQyrCallCdr->SQL->Clear();
      dmAgent->adoQyrCallCdr->SQL->Add((char *)sqlbuf);
      dmAgent->adoQyrCallCdr->Open();

      CurDLSTPageNo = cseDLSTLocaPage->Value;
      lblDLSTLocaPage->Caption = "第"+IntToStr(CurDLSTPageNo)+"頁";
      if (CurDLSTPageNo == 1)
      {
        btnDLSTPrior->Enabled = false;
        if (DLSTTotalPage > 1)
          btnDLSTNext->Enabled = true;
      }
      else if (CurDLSTPageNo == DLSTTotalPage)
      {
        btnDLSTNext->Enabled = false;
        if (DLSTTotalPage > 1)
          btnDLSTPrior->Enabled = true;
      }
      else
      {
        btnDLSTPrior->Enabled = true;
        btnDLSTNext->Enabled = true;
      }
      //2015-04-09
      AnsiString strDialLog = dmAgent->adoQryDialOutList->FieldByName("DialLog")->AsString;
      Memo1->Lines->Clear();
      if (strDialLog.Length() > 0)
        Memo1->Lines->Add(strDialLog);
      Button2->Enabled = false;
    }
    catch (...)
    {
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDispDialOutList::Memo1Change(TObject *Sender)
{
  Button2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormDispDialOutList::Button2Click(TObject *Sender)
{
  //2015-04-09
  try
  {
    AnsiString strDialListId = dmAgent->adoQryDialOutList->FieldByName("DialListId")->AsString;
    AnsiString strDialId = dmAgent->adoQryDialOutList->FieldByName("DialId")->AsString;
    dmAgent->UpdateManDialOutDialListRecord(strDialListId, strDialId, Memo1->Lines->Text);
    Button2->Enabled = false;
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDispDialOutList::N1Click(TObject *Sender)
{
  if (dmAgent->adoQryDialOutList->State == dsInactive)
    return;
  if (dmAgent->adoQryDialOutList->Eof)
    dmAgent->adoQryDialOutList->Prior();

  try
  {
    AnsiString strRecdFile = dmAgent->GetRecdFileName(dmAgent->adoQryDialOutList->FieldByName("SerialNo")->AsString);
    if (strRecdFile.Length() > 0)
    {
      WindowsMediaPlayer1->Visible = true;
      WindowsMediaPlayer1->URL =  FtpServer1+strRecdFile;
      N1->Enabled = false;
    }
    else
    {
      MessageBox(NULL,"該記錄沒有錄音檔!!!","警告",MB_OK|MB_ICONWARNING);
    }
  }
  catch (...)
  {
    MessageBox(NULL,"下載錄音檔失敗!!!","警告",MB_OK|MB_ICONWARNING);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDispDialOutList::N2Click(TObject *Sender)
{
  WindowsMediaPlayer1->controls->stop();
  WindowsMediaPlayer1->Visible = false;
  N1->Enabled = true;
}
//---------------------------------------------------------------------------

