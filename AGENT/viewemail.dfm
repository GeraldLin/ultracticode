object FormViewEmail: TFormViewEmail
  Left = 473
  Top = 108
  Width = 950
  Height = 942
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = #27298#35222'Email'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 942
    Height = 117
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 11
      Top = 15
      Width = 39
      Height = 12
      Caption = #20540#27231#21729':'
    end
    object Label2: TLabel
      Left = 11
      Top = 44
      Width = 39
      Height = 12
      Caption = #30332#20214#20154':'
    end
    object Label3: TLabel
      Left = 11
      Top = 68
      Width = 39
      Height = 12
      Caption = #25910#20214#20154':'
    end
    object Label4: TLabel
      Left = 11
      Top = 92
      Width = 33
      Height = 12
      Caption = #20027'  '#26088':'
    end
    object btClose: TButton
      Left = 856
      Top = 8
      Width = 75
      Height = 25
      Caption = #38364#38281
      TabOrder = 0
      OnClick = btCloseClick
    end
    object cbWorkerNo: TComboBox
      Left = 56
      Top = 11
      Width = 145
      Height = 20
      ItemHeight = 12
      TabOrder = 1
    end
    object Button1: TButton
      Left = 208
      Top = 8
      Width = 90
      Height = 25
      Caption = #27966#30332#20108#32218#34389#29702
      TabOrder = 2
      OnClick = Button1Click
    end
    object editEmailFrom: TEdit
      Left = 56
      Top = 40
      Width = 875
      Height = 20
      Color = clSilver
      ReadOnly = True
      TabOrder = 3
    end
    object editEmailTo: TEdit
      Left = 56
      Top = 64
      Width = 875
      Height = 20
      Color = clSilver
      ReadOnly = True
      TabOrder = 4
    end
    object editEmailSubject: TEdit
      Left = 56
      Top = 88
      Width = 875
      Height = 20
      Color = clSilver
      ReadOnly = True
      TabOrder = 5
    end
  end
  object memoEmailContent: TMemo
    Left = 0
    Top = 117
    Width = 942
    Height = 791
    Align = alClient
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
  end
end
