object FormEditCustom: TFormEditCustom
  Left = 320
  Top = 180
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#20358#38651#32773#36039#26009
  ClientHeight = 413
  ClientWidth = 1152
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 308
    Top = 371
    Width = 81
    Height = 27
    Caption = #20445#23384
    Default = True
    Enabled = False
    TabOrder = 0
    OnClick = BitBtn1Click
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 741
    Top = 371
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 1
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
  object gbCustBase: TGroupBox
    Left = 9
    Top = 9
    Width = 1136
    Height = 165
    Caption = #20358#38651#32773#22522#26412#36039#26009#65306
    TabOrder = 2
    object Label2: TLabel
      Left = 18
      Top = 22
      Width = 72
      Height = 13
      Caption = #20358#38651#32773#32232#34399#65306
    end
    object Label3: TLabel
      Left = 18
      Top = 46
      Width = 72
      Height = 13
      Caption = #20358#38651#32773#22995#21517#65306
    end
    object Label4: TLabel
      Left = 30
      Top = 92
      Width = 60
      Height = 13
      Caption = #20844#21496#21517#31281#65306
    end
    object Label5: TLabel
      Left = 18
      Top = 68
      Width = 72
      Height = 13
      Caption = #20358#38651#32773#36523#20221#65306
    end
    object Label6: TLabel
      Left = 265
      Top = 44
      Width = 36
      Height = 13
      Caption = #24615#21029#65306
    end
    object Label7: TLabel
      Left = 469
      Top = 46
      Width = 36
      Height = 13
      Caption = #25163#27231#65306
    end
    object Label8: TLabel
      Left = 648
      Top = 46
      Width = 60
      Height = 13
      Caption = #38651#35441#34399#30908#65306
    end
    object Label9: TLabel
      Left = 469
      Top = 69
      Width = 36
      Height = 13
      Caption = #20659#30495#65306
    end
    object Label10: TLabel
      Left = 648
      Top = 68
      Width = 60
      Height = 13
      Caption = #38651#23376#37109#20214#65306
    end
    object Label11: TLabel
      Left = 469
      Top = 92
      Width = 36
      Height = 13
      Caption = #22320#22336#65306
    end
    object Label12: TLabel
      Left = 30
      Top = 117
      Width = 60
      Height = 13
      Caption = #37109#36958#21312#34399#65306
    end
    object Label1: TLabel
      Left = 54
      Top = 141
      Width = 36
      Height = 13
      Caption = #20633#35387#65306
    end
    object Label14: TLabel
      Left = 263
      Top = 116
      Width = 36
      Height = 13
      Caption = #32291#24066#65306
    end
    object Label15: TLabel
      Left = 445
      Top = 117
      Width = 60
      Height = 13
      Caption = #37129#37806#24066#21312#65306
    end
    object Label16: TLabel
      Left = 672
      Top = 141
      Width = 36
      Height = 13
      Caption = #26449#37324#65306
      Enabled = False
      Visible = False
    end
    object Label13: TLabel
      Left = 648
      Top = 116
      Width = 60
      Height = 13
      Caption = #26597#35426#31684#22285#65306
    end
    object Label17: TLabel
      Left = 445
      Top = 140
      Width = 60
      Height = 13
      Caption = #23567#21312#27155#30436#65306
      Enabled = False
      Visible = False
    end
    object Label18: TLabel
      Left = 237
      Top = 69
      Width = 60
      Height = 13
      Caption = #30003#35380#23565#35937#65306
    end
    object Label19: TLabel
      Left = 632
      Top = 20
      Width = 72
      Height = 13
      Caption = #36774#20844#23460#38651#35441#65306
    end
    object lbAccountNo: TLabel
      Left = 441
      Top = 20
      Width = 60
      Height = 13
      Caption = #32113#19968#32232#34399#65306
    end
    object edtCustomNo: TEdit
      Left = 100
      Top = 17
      Width = 109
      Height = 21
      TabStop = False
      Color = clGrayText
      ReadOnly = True
      TabOrder = 0
    end
    object edtCustName: TEdit
      Left = 100
      Top = 41
      Width = 108
      Height = 21
      TabOrder = 1
      OnChange = edtCustNameChange
    end
    object edtCorpName: TEdit
      Left = 100
      Top = 89
      Width = 314
      Height = 21
      TabOrder = 2
      OnChange = edtCustNameChange
    end
    object edtMobileNo: TEdit
      Left = 511
      Top = 41
      Width = 109
      Height = 21
      TabOrder = 3
      OnChange = edtMobileNoChange
    end
    object edtTeleNo: TEdit
      Left = 717
      Top = 41
      Width = 109
      Height = 21
      TabOrder = 4
      OnChange = edtTeleNoChange
    end
    object edtFaxNo: TEdit
      Left = 511
      Top = 65
      Width = 109
      Height = 21
      TabOrder = 5
      OnChange = edtFaxNoChange
    end
    object edtEMail: TEdit
      Left = 717
      Top = 65
      Width = 109
      Height = 21
      TabOrder = 6
      OnChange = edtCustNameChange
    end
    object edtContAddr: TEdit
      Left = 511
      Top = 89
      Width = 315
      Height = 21
      TabOrder = 7
      OnChange = edtCustNameChange
    end
    object edtPostNo: TEdit
      Left = 100
      Top = 113
      Width = 108
      Height = 21
      TabOrder = 8
      OnChange = edtCustNameChange
    end
    object edtRemark: TEdit
      Left = 100
      Top = 137
      Width = 314
      Height = 21
      TabOrder = 9
      OnChange = edtCustNameChange
    end
    object edtCustSex: TComboBox
      Left = 306
      Top = 41
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 10
      OnChange = edtCustNameChange
      OnKeyPress = edtCustTypeKeyPress
      Items.Strings = (
        #30007
        #22899
        #20854#20182)
    end
    object edtCustType: TComboBox
      Left = 100
      Top = 65
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 11
      OnChange = edtCustNameChange
      OnKeyPress = edtCustTypeKeyPress
      Items.Strings = (
        #26222#36890#23458#25142
        #39640#32026#23458#25142
        'VIP'#23458#25142)
    end
    object cbProvince: TComboBox
      Left = 306
      Top = 113
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 12
      OnExit = cbProvinceExit
    end
    object cbCityName: TComboBox
      Left = 511
      Top = 113
      Width = 109
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 13
      OnExit = cbCityNameExit
    end
    object cbCountyName: TComboBox
      Left = 717
      Top = 137
      Width = 109
      Height = 21
      AutoDropDown = True
      Enabled = False
      ItemHeight = 13
      TabOrder = 14
      Visible = False
      OnChange = edtCustNameChange
    end
    object cbCustClass: TComboBox
      Left = 717
      Top = 113
      Width = 109
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 15
      Text = #20840#37096#21487#26597#30475
      OnChange = edtCustNameChange
    end
    object edtZoneName: TEdit
      Left = 511
      Top = 137
      Width = 109
      Height = 21
      Enabled = False
      TabOrder = 16
      Visible = False
      OnChange = edtCustNameChange
    end
    object edtCustLevel: TComboBox
      Left = 306
      Top = 65
      Width = 108
      Height = 21
      AutoDropDown = True
      ItemHeight = 13
      TabOrder = 17
      OnChange = edtCustNameChange
      OnKeyPress = edtCustTypeKeyPress
      Items.Strings = (
        #26222#36890#23458#25142
        #39640#32026#23458#25142
        'VIP'#23458#25142)
    end
    object ckSameCustName: TCheckBox
      Left = 215
      Top = 17
      Width = 186
      Height = 19
      Caption = #20801#35377#22686#21152#37325#35206#20358#38651#32773#22995#21517#35352#37636'?'
      Checked = True
      State = cbChecked
      TabOrder = 18
    end
    object edtSubPhone: TEdit
      Left = 717
      Top = 15
      Width = 109
      Height = 21
      TabOrder = 19
      OnChange = edtCustNameChange
    end
    object edtAccountNo: TEdit
      Left = 511
      Top = 16
      Width = 110
      Height = 21
      TabOrder = 20
      OnChange = edtCustNameChange
    end
  end
  object gbCustEx: TGroupBox
    Left = 9
    Top = 179
    Width = 1136
    Height = 182
    Caption = #20358#38651#32773#25844#20805#36039#26009#65306
    TabOrder = 3
    object sbCustEx: TScrollBox
      Left = 2
      Top = 15
      Width = 1132
      Height = 165
      Align = alClient
      TabOrder = 0
    end
  end
  object btQryAS400: TButton
    Left = 16
    Top = 371
    Width = 75
    Height = 27
    Caption = #26597#35426'AS400'
    TabOrder = 4
    Visible = False
    OnClick = btQryAS400Click
  end
end
