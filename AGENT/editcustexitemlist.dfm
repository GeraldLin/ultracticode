object FormEditCustExItemList: TFormEditCustExItemList
  Left = 196
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20358#38651#32773#36039#26009#25844#20805#38917#36984#25799#38917#35373#32622
  ClientHeight = 164
  ClientWidth = 298
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 31
    Top = 20
    Width = 84
    Height = 12
    Caption = #36039#26009#38917#30446#21517#31281#65306
  end
  object Label2: TLabel
    Left = 44
    Top = 52
    Width = 72
    Height = 12
    Caption = #36984#25799#38917#32232#34399#65306
  end
  object Label3: TLabel
    Left = 44
    Top = 84
    Width = 72
    Height = 12
    Caption = #36984#25799#38917#21517#31281#65306
  end
  object editLabelCaption: TEdit
    Left = 120
    Top = 16
    Width = 121
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editItemNo: TEdit
    Left = 120
    Top = 48
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editItemNoChange
  end
  object editItemName: TEdit
    Left = 120
    Top = 80
    Width = 121
    Height = 20
    MaxLength = 50
    TabOrder = 2
    OnChange = editItemNoChange
  end
  object Button1: TButton
    Left = 56
    Top = 120
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 168
    Top = 120
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
end
