//---------------------------------------------------------------------------

#ifndef editdutytelH
#define editdutytelH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditDutyTel : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TComboBox *cbWorkerName;
  TComboBox *cbDayType;
  TDateTimePicker *dtpAMStartTime;
  TDateTimePicker *dtpAMEndTime;
  TDateTimePicker *dtpPMStartTime;
  TDateTimePicker *dtpPMEndTime;
  TEdit *editDutyTel1;
  TEdit *editDutyTel2;
  TComboBox *cbOpenFlag;
  TComboBox *cbStartWeek;
  TComboBox *cbEndWeek;
  TComboBox *cbStartDay;
  TComboBox *cbEndDay;
  TDateTimePicker *dtpStartDate;
  TDateTimePicker *dtpEndDate;
  TDateTimePicker *dtpStartTime;
  TDateTimePicker *dtpEndTime;
  TButton *Button1;
  TButton *Button2;
  void __fastcall cbDayTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall cbWorkerNameChange(TObject *Sender);
  void __fastcall cbDayTypeChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditDutyTel(TComponent* Owner);

  void LoadWorkerNameList();
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CDutyTel DutyTel;
  void SetModifyType(bool isinsert);
  void SetDutyTelData(CDutyTel& dutytel);
  int GetDutyTelData();
  void ClearDutyTelData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditDutyTel *FormEditDutyTel;
//---------------------------------------------------------------------------
#endif
