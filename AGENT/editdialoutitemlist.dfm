object FormEditDialOutItemList: TFormEditDialOutItemList
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20154#24037#22806#25765#22810#38917#36984#25799#21443#25976
  ClientHeight = 375
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 290
    Top = 20
    Width = 72
    Height = 13
    Caption = #36664#20837#26694#21517#31281#65306
  end
  object Label2: TLabel
    Left = 44
    Top = 44
    Width = 72
    Height = 13
    Caption = #36984#25799#38917#32232#34399#65306
  end
  object Label3: TLabel
    Left = 290
    Top = 44
    Width = 72
    Height = 13
    Caption = #36984#25799#38917#21517#31281#65306
  end
  object Label4: TLabel
    Left = 32
    Top = 20
    Width = 84
    Height = 13
    Caption = #22806#25765#20219#21209#21517#31281#65306
  end
  object Label5: TLabel
    Left = 14
    Top = 69
    Width = 94
    Height = 13
    Caption = #38364#32879#30340'URL'#32178#22336#65306
  end
  object Label6: TLabel
    Left = 14
    Top = 93
    Width = 120
    Height = 13
    Caption = #38364#32879#30340#27161#28310#22238#24489#33139#26412#65306
  end
  object editLabelCaption: TEdit
    Left = 366
    Top = 16
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editItemNo: TEdit
    Left = 120
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = editItemNoChange
  end
  object editItemName: TEdit
    Left = 366
    Top = 40
    Width = 121
    Height = 21
    MaxLength = 50
    TabOrder = 2
    OnChange = editItemNoChange
  end
  object Button1: TButton
    Left = 104
    Top = 336
    Width = 75
    Height = 25
    Caption = #30906#23450
    Default = True
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 320
    Top = 336
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
  object editTaskName: TEdit
    Left = 120
    Top = 16
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 5
  end
  object editItemURL: TEdit
    Left = 119
    Top = 65
    Width = 367
    Height = 21
    MaxLength = 500
    TabOrder = 6
    OnChange = editItemNoChange
  end
  object memoItemDemo: TMemo
    Left = 13
    Top = 112
    Width = 475
    Height = 209
    MaxLength = 4000
    ScrollBars = ssVertical
    TabOrder = 7
    OnChange = editItemNoChange
  end
end
