//---------------------------------------------------------------------------

#ifndef updateversionH
#define updateversionH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormUpdateVersion : public TForm
{
__published:	// IDE-managed Components
  TLabel *lbNewVersion;
  TButton *Button1;
  TButton *Button2;
  TCheckBox *ckUpdatePrompt;
  void __fastcall ckUpdatePromptClick(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormUpdateVersion(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormUpdateVersion *FormUpdateVersion;
//---------------------------------------------------------------------------
#endif
