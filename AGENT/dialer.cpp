//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <inifiles.hpp>
#include "dialer.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDial *FormDial;
extern BSTR bEmpty;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
//話務員密碼驗證成功
extern bool WorkerLoginId;

extern g_nSwitchType;

extern CCallMsg CallMsg;
extern int TranAnsId;
extern bool bMobileId;
extern int g_nBandSeatNoType;
extern AnsiString g_strDialoutPreCode;

AnsiString strEmpty1 = "";
WideString wEmpty1 = WideString(strEmpty1);
BSTR bEmpty1 = ( wchar_t * )wEmpty1;

extern AnsiString strDTMF[16], strAppPath;
//---------------------------------------------------------------------------
__fastcall TFormDial::TFormDial(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
    dmAgent->ReadCallOutFuncKey();
  ckMobileId->Checked = bMobileId;

  TIniFile *IniKey = new TIniFile ( strAppPath+"agent.ini" );
  ckAutoAdd9OnDial->Checked = IniKey->ReadBool ( "CONFIGURE", "AutoAdd9OnDial", false );
  ckAutoAdd9OnTran->Checked = IniKey->ReadBool ( "CONFIGURE", "AutoAdd9OnTran", false );
  delete IniKey;
}
//---------------------------------------------------------------------------
void __fastcall TFormDial::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormDial::Button1Click(TObject *Sender)
{
  AnsiString DialNo;
  WideString wdialNo, wCallerNo;
  BSTR bDialNo, bCallerNo;
  int calledtype;

  DialNo = cbDialNum->Text;
  if (bMobileId == true)
  {
    if (DialNo.Length() == 11)
    {
      if (DialNo.SubString(1,2) == "13" || DialNo.SubString(1,2) == "15" || DialNo.SubString(1,2) == "18")
      {
        if (dmAgent->IsLocaMobile(DialNo) == 0)
          DialNo = "0"+DialNo;
      }
    }
  }
  if (DialNo.Length() > 0)
  {
    switch (DialFlag)
    {
    case 1:
      AddPhoneComboBox(cbDialNum, DialNo);
      if (FormMain->SeatList.IsSeatNoExist(DialNo) == false)
      {
        if (g_strDialoutPreCode == "")
        {
          if (ckAutoAdd9OnDial->Checked == true)
            DialNo = "9"+DialNo;
        }
        else
        {
          DialNo = g_strDialoutPreCode+DialNo;
        }

        calledtype = 1;
      }
      else
      {
        if (DialNo.Length() > 4 || DialNo[1] == '1')
        {
          if (g_strDialoutPreCode == "")
          {
            if (ckAutoAdd9OnDial->Checked == true)
              DialNo = "9"+DialNo;
          }
          else
          {
            DialNo = g_strDialoutPreCode+DialNo;
          }
          calledtype = 1;
        }
        else
        {
          calledtype = 2;
        }
      }

      wdialNo = WideString(DialNo);
      bDialNo = ( wchar_t * )wdialNo;

      wCallerNo = WideString(MySeat.SeatNo);
      bCallerNo = ( wchar_t * )wCallerNo;

      MyWorker.CallOutCount ++;
      FormMain->lblCallinCount->Caption = "撥入:"+IntToStr(MyWorker.CallInCount)+"/撥出:"+IntToStr(MyWorker.CallOutCount);

      CallMsg.state = true;
      CallMsg.AnsId = false;
      CallMsg.WaitTimer = 0;
      CallMsg.TalkTimer = 0;
      CallMsg.CallerNo = DialNo;
      CallMsg.CallTime = DateTimeToStr(Now());
      time(&CallMsg.st);
      FormMain->DispCallMsg();
      FormMain->MyAgent1->MakeCall(bCallerNo, 0, bDialNo, calledtype, bEmpty1, bEmpty1, 1, 0, bEmpty1);
      break;
    case 2:
      AddPhoneComboBox(cbDialNum, DialNo);
      if (g_nSwitchType > 0)
      {
        if (g_strDialoutPreCode == "")
        {
          if (ckAutoAdd9OnTran->Checked == true)
            DialNo = "9"+DialNo;
        }
        else
        {
          DialNo = g_strDialoutPreCode+DialNo;
        }
      }
      wdialNo = WideString(DialNo);

      bDialNo = ( wchar_t * )wdialNo;

      if (ckTranType->Checked == false)
        FormMain->MyAgent1->ConsultTranCall(1,bDialNo,bEmpty1);
      else
        FormMain->MyAgent1->BlindTranCall(1,bDialNo,bEmpty1);

      FormMain->ToolButton8->Enabled = true;
      TranAnsId = 1;
      break;
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormDial::Button3Click(TObject *Sender)
{
  AnsiString DialNo;
  WideString wdialNo, wCallerNo;
  BSTR bDialNo, bCallerNo;

  DialNo = cbDialNum->Text;
  if (DialNo.Length() > 0)
  {
    if (DialFlag == 2)
    {
      AddPhoneComboBox(cbDialNum, DialNo);

      if (g_nSwitchType > 0)
      {
        if (g_strDialoutPreCode == "")
        {
          if (ckAutoAdd9OnTran->Checked == true)
            DialNo = "9"+DialNo;
        }
        else
        {
          DialNo = g_strDialoutPreCode+DialNo;
        }
      }

      wdialNo = WideString(DialNo);
      bDialNo = ( wchar_t * )wdialNo;

      FormMain->MyAgent1->ConfTranCall(1,bDialNo,0,bEmpty);
      FormMain->ToolButton8->Enabled = true;
      TranAnsId = 1;
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF1Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "1";
  try
  {
    MediaPlayer1->FileName = strDTMF[1];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF2Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "2";
  try
  {
    MediaPlayer1->FileName = strDTMF[2];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF3Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "3";
  try
  {
    MediaPlayer1->FileName = strDTMF[3];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF4Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "4";
  try
  {
    MediaPlayer1->FileName = strDTMF[4];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF5Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "5";
  try
  {
    MediaPlayer1->FileName = strDTMF[5];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF6Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "6";
  try
  {
    MediaPlayer1->FileName = strDTMF[6];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF7Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "7";
  try
  {
    MediaPlayer1->FileName = strDTMF[7];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF8Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "8";
  try
  {
    MediaPlayer1->FileName = strDTMF[8];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMF9Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "9";
  try
  {
    MediaPlayer1->FileName = strDTMF[9];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMFXClick(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "*";
  try
  {
    MediaPlayer1->FileName = strDTMF[10];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTM0Click(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "0";
  try
  {
    MediaPlayer1->FileName = strDTMF[0];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btDTMFJClick(TObject *Sender)
{
  cbDialNum->Text = cbDialNum->Text + "#";
  try
  {
    MediaPlayer1->FileName = strDTMF[11];
    MediaPlayer1->Open();
    MediaPlayer1->Play();
  }
  catch (...)
  {
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::ckMobileIdClick(TObject *Sender)
{
  bMobileId = ckMobileId->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  AnsiString DialNo;
  WideString wdialNo, wCallerNo;
  BSTR bDialNo, bCallerNo;
  int calledtype;

  AnsiString strCalledNo, strCalledName;
  int nFuncKeyId=0;

  switch (Key)
  {
  case VK_F1:
    nFuncKeyId = 1;
    break;
  case VK_F2:
    nFuncKeyId = 2;
    break;
  case VK_F3:
    nFuncKeyId = 3;
    break;
  case VK_F4:
    nFuncKeyId = 4;
    break;
  case VK_F5:
    nFuncKeyId = 5;
    break;
  case VK_F6:
    nFuncKeyId = 6;
    break;
  case VK_F7:
    nFuncKeyId = 7;
    break;
  case VK_F8:
    nFuncKeyId = 8;
    break;
  case VK_F9:
    nFuncKeyId = 9;
    break;
  case VK_F10:
    nFuncKeyId = 10;
    break;
  case VK_F11:
    nFuncKeyId = 11;
    break;
  case VK_F12:
    nFuncKeyId = 12;
    break;
  case VK_RETURN:
    nFuncKeyId = 13;
    break;
  case VK_NUMPAD0:
  case '0':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[0];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD1:
  case '1':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[1];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD2:
  case '2':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[2];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD3:
  case '3':
    if (Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[3];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD4:
  case '4':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[4];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD5:
  case '5':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[5];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD6:
  case '6':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[6];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD7:
  case '7':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[7];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD8:
  case '8':
    if (Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[8];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case VK_NUMPAD9:
  case '9':
    if (Shift.Contains(ssShift)||Shift.Contains(ssAlt)||Shift.Contains(ssCtrl))
      break;
    try
    {
      MediaPlayer1->FileName = strDTMF[9];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case '*':
    try
    {
      MediaPlayer1->FileName = strDTMF[10];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  case '#':
    try
    {
      MediaPlayer1->FileName = strDTMF[11];
      MediaPlayer1->Open();
      MediaPlayer1->Play();
    }
    catch (...)
    {
    }
    break;
  }
  if (nFuncKeyId == 13)
  {
    DialNo = cbDialNum->Text;
    if (bMobileId == true)
    {
      if (DialNo.Length() == 11)
      {
        if (DialNo.SubString(1,2) == "13" || DialNo.SubString(1,2) == "15" || DialNo.SubString(1,2) == "18")
        {
          if (dmAgent->IsLocaMobile(DialNo) == 0)
            DialNo = "0"+DialNo;
        }
      }
    }
    if (DialNo.Length() > 0)
    {
      switch (DialFlag)
      {
      case 1:
        AddPhoneComboBox(cbDialNum, DialNo);
        if (FormMain->SeatList.IsSeatNoExist(DialNo) == false)
        {
          if (g_strDialoutPreCode == "")
          {
            if (ckAutoAdd9OnDial->Checked == true)
              DialNo = "9"+DialNo;
          }
          else
          {
            DialNo = g_strDialoutPreCode+DialNo;
          }
          calledtype = 1;
        }
        else
        {
          if (DialNo.Length() > 3 || DialNo[1] == '1')
          {
            DialNo = "9"+DialNo;
            calledtype = 1;
          }
          else
          {
            calledtype = 2;
          }
        }

        wdialNo = WideString(DialNo);
        bDialNo = ( wchar_t * )wdialNo;

        wCallerNo = WideString(MySeat.SeatNo);
        bCallerNo = ( wchar_t * )wCallerNo;

        MyWorker.CallOutCount ++;
        FormMain->lblCallinCount->Caption = "撥入:"+IntToStr(MyWorker.CallInCount)+"/撥出:"+IntToStr(MyWorker.CallOutCount);

        CallMsg.state = true;
        CallMsg.AnsId = false;
        CallMsg.WaitTimer = 0;
        CallMsg.TalkTimer = 0;
        FormMain->MyAgent1->MakeCall(bCallerNo, 0, bDialNo, calledtype, bEmpty1, bEmpty1, 1, 0, bEmpty1);
        break;
      case 2:
        AddPhoneComboBox(cbDialNum, DialNo);
        wdialNo = WideString(DialNo);
        bDialNo = ( wchar_t * )wdialNo;

        if (ckTranType->Checked == false)
          FormMain->MyAgent1->ConsultTranCall(1,bDialNo,bEmpty1);
        else
          FormMain->MyAgent1->BlindTranCall(1,bDialNo,bEmpty1);

        FormMain->ToolButton8->Enabled = true;
        TranAnsId = 1;
        break;
      }
      this->Close();
    }
    return;
  }
  if (nFuncKeyId == 0)
    return;
  if (dmAgent->GetCallOutFuncKey(nFuncKeyId, strCalledNo, strCalledName) == 0)
    return;
  if (strCalledNo == "")
    return;

  if (bMobileId == true)
  {
    if (strCalledNo.Length() == 11)
    {
      if (strCalledNo.SubString(1,2) == "13" || strCalledNo.SubString(1,2) == "15" || strCalledNo.SubString(1,2) == "18")
      {
        if (dmAgent->IsLocaMobile(strCalledNo) == 0)
          strCalledNo = "0"+strCalledNo;
      }
    }
  }
  if (strCalledNo.Length() > 0)
  {
    switch (DialFlag)
    {
    case 1:
      AddPhoneComboBox(cbDialNum, strCalledNo);
      if (FormMain->SeatList.IsSeatNoExist(strCalledNo) == false)
      {
        strCalledNo = "9"+strCalledNo;
        calledtype = 1;
      }
      else
      {
        if (strCalledNo.Length() > 3 || strCalledNo[1] == '1')
        {
          strCalledNo = "9"+strCalledNo;
          calledtype = 1;
        }
        else
        {
          calledtype = 2;
        }
      }

      wdialNo = WideString(strCalledNo);
      bDialNo = ( wchar_t * )wdialNo;

      wCallerNo = WideString(MySeat.SeatNo);
      bCallerNo = ( wchar_t * )wCallerNo;

      MyWorker.CallOutCount ++;
      FormMain->lblCallinCount->Caption = "撥入:"+IntToStr(MyWorker.CallInCount)+"/撥出:"+IntToStr(MyWorker.CallOutCount);

      CallMsg.state = true;
      CallMsg.AnsId = false;
      CallMsg.WaitTimer = 0;
      CallMsg.TalkTimer = 0;
      FormMain->MyAgent1->MakeCall(bCallerNo, 0, bDialNo, calledtype, bEmpty1, bEmpty1, 1, 0, bEmpty1);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "撥出電話%s", DialNo.c_str());
      break;
    case 2:
      AddPhoneComboBox(cbDialNum, strCalledNo);
      wdialNo = WideString(strCalledNo);
      bDialNo = ( wchar_t * )wdialNo;

      if (ckTranType->Checked == false)
        FormMain->MyAgent1->ConsultTranCall(1,bDialNo,bEmpty1);
      else
        FormMain->MyAgent1->BlindTranCall(1,bDialNo,bEmpty1);

      FormMain->ToolButton8->Enabled = true;
      TranAnsId = 1;
      break;
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------
void TFormDial::FuncKeyDialOut(int nFuncKeyId)
{
  WideString wdialNo, wCallerNo;
  BSTR bDialNo, bCallerNo;
  int calledtype;

  AnsiString strCalledNo, strCalledName;
  if (dmAgent->GetCallOutFuncKey(nFuncKeyId, strCalledNo, strCalledName) == 0)
    return;
  if (strCalledNo == "")
    return;

  if (bMobileId == true)
  {
    if (strCalledNo.Length() == 11)
    {
      if (strCalledNo.SubString(1,2) == "13" || strCalledNo.SubString(1,2) == "15" || strCalledNo.SubString(1,2) == "18")
      {
        if (dmAgent->IsLocaMobile(strCalledNo) == 0)
          strCalledNo = "0"+strCalledNo;
      }
    }
  }
  if (strCalledNo.Length() > 0)
  {
    switch (DialFlag)
    {
    case 1:
      AddPhoneComboBox(cbDialNum, strCalledNo);
      if (FormMain->SeatList.IsSeatNoExist(strCalledNo) == false)
      {
        strCalledNo = "9"+strCalledNo;
        calledtype = 1;
      }
      else
      {
        if (strCalledNo.Length() > 3 || strCalledNo[1] == '1')
        {
          if (g_strDialoutPreCode == "")
          {
            if (ckAutoAdd9OnDial->Checked == true)
              strCalledNo = "9"+strCalledNo;
          }
          else
          {
            strCalledNo = g_strDialoutPreCode+strCalledNo;
          }
          calledtype = 1;
        }
        else
        {
          calledtype = 2;
        }
      }

      wdialNo = WideString(strCalledNo);
      bDialNo = ( wchar_t * )wdialNo;

      wCallerNo = WideString(MySeat.SeatNo);
      bCallerNo = ( wchar_t * )wCallerNo;

      MyWorker.CallOutCount ++;
      FormMain->lblCallinCount->Caption = "撥入:"+IntToStr(MyWorker.CallInCount)+"/撥出:"+IntToStr(MyWorker.CallOutCount);

      CallMsg.state = true;
      CallMsg.AnsId = false;
      CallMsg.WaitTimer = 0;
      CallMsg.TalkTimer = 0;
      FormMain->MyAgent1->MakeCall(bCallerNo, 0, bDialNo, calledtype, bEmpty1, bEmpty1, 1, 0, bEmpty1);
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1042, "呼出電話%s", DialNo.c_str());
      break;
    case 2:
      AddPhoneComboBox(cbDialNum, strCalledNo);
      wdialNo = WideString(strCalledNo);
      bDialNo = ( wchar_t * )wdialNo;

      if (ckTranType->Checked == false)
        FormMain->MyAgent1->ConsultTranCall(1,bDialNo,bEmpty1);
      else
        FormMain->MyAgent1->BlindTranCall(1,bDialNo,bEmpty1);
        
      FormMain->ToolButton8->Enabled = true;
      TranAnsId = 1;
      break;
    }
    this->Close();
  }
}

void __fastcall TFormDial::btFuncF1Click(TObject *Sender)
{
  FuncKeyDialOut(1);
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF2Click(TObject *Sender)
{
  FuncKeyDialOut(2);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF3Click(TObject *Sender)
{
  FuncKeyDialOut(3);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF4Click(TObject *Sender)
{
  FuncKeyDialOut(4);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF5Click(TObject *Sender)
{
  FuncKeyDialOut(5);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF6Click(TObject *Sender)
{
  FuncKeyDialOut(6);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF7Click(TObject *Sender)
{
  FuncKeyDialOut(7);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF8Click(TObject *Sender)
{
  FuncKeyDialOut(8);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF9Click(TObject *Sender)
{
  FuncKeyDialOut(9);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF10Click(TObject *Sender)
{
  FuncKeyDialOut(10);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF11Click(TObject *Sender)
{
  FuncKeyDialOut(11);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btFuncF12Click(TObject *Sender)
{
  FuncKeyDialOut(12);  
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::FormShow(TObject *Sender)
{
  cbDialNum->Text = "";
  cbDialNum->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::btUpDownClick(TObject *Sender)
{
  static bool nVsible=true;
  if (nVsible == true)
  {
    nVsible = false;
    gbFastDial->Visible = false;
    btUpDown->Hint = "顯示快速撥號功能鍵";
    ckMobileId->Top = 267;
    ckTranType->Top = 267;
    FormDial->Height = 355;
  }
  else
  {
    nVsible = true;
    gbFastDial->Visible = true;
    btUpDown->Hint = "隱藏快速撥號功能鍵";
    ckMobileId->Top = 496;
    ckTranType->Top = 496;
    FormDial->Height = 599;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::ckAutoAdd9OnDialClick(TObject *Sender)
{
  TIniFile *IniKey = new TIniFile ( strAppPath+"agent.ini" );
  IniKey->WriteBool( "CONFIGURE", "AutoAdd9OnDial", ckAutoAdd9OnDial->Checked );
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormDial::ckAutoAdd9OnTranClick(TObject *Sender)
{
  TIniFile *IniKey = new TIniFile ( strAppPath+"agent.ini" );
  IniKey->WriteBool( "CONFIGURE", "AutoAdd9OnTran", ckAutoAdd9OnTran->Checked );
  delete IniKey;
}
//---------------------------------------------------------------------------

