object FormEditAcptItemList: TFormEditAcptItemList
  Left = 357
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#36664#20837#26694#36984#25799#38917#36039#26009
  ClientHeight = 426
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 292
    Top = 22
    Width = 72
    Height = 13
    Caption = #36664#20837#26694#21517#31281#65306
  end
  object Label2: TLabel
    Left = 56
    Top = 50
    Width = 72
    Height = 13
    Caption = #36984#25799#38917#32232#34399#65306
  end
  object Label3: TLabel
    Left = 56
    Top = 77
    Width = 72
    Height = 13
    Caption = #36984#25799#38917#21517#31281#65306
  end
  object Label4: TLabel
    Left = 44
    Top = 22
    Width = 84
    Height = 13
    Caption = #21463#29702#26989#21209#39006#22411#65306
  end
  object Label5: TLabel
    Left = 33
    Top = 109
    Width = 94
    Height = 13
    Caption = #38364#32879#30340'URL'#32178#22336#65306
  end
  object Label6: TLabel
    Left = 7
    Top = 133
    Width = 84
    Height = 13
    Caption = #38364#32879#35441#34899#33139#26412#65306
  end
  object editLabelCaption: TEdit
    Left = 374
    Top = 17
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editItemNo: TEdit
    Left = 138
    Top = 46
    Width = 131
    Height = 21
    TabOrder = 1
    OnChange = editItemNoChange
  end
  object editItemName: TEdit
    Left = 138
    Top = 73
    Width = 367
    Height = 21
    MaxLength = 250
    TabOrder = 2
    OnChange = editItemNoChange
  end
  object Button1: TButton
    Left = 117
    Top = 383
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 326
    Top = 383
    Width = 81
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
  object editSrvType: TEdit
    Left = 138
    Top = 17
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 5
  end
  object editItemURL: TEdit
    Left = 138
    Top = 105
    Width = 367
    Height = 21
    MaxLength = 500
    TabOrder = 6
    OnChange = editItemNoChange
  end
  object memoItemDemo: TMemo
    Left = 8
    Top = 152
    Width = 497
    Height = 209
    MaxLength = 4000
    ScrollBars = ssVertical
    TabOrder = 7
    OnChange = editItemNoChange
  end
end
