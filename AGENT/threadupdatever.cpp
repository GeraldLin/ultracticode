//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "threadupdatever.h"
#include "main.h"
#include "DataModule.h"
#pragma package(smart_init)

extern CMyWorker MyWorker;
extern bool g_bDispWaitProcAccept;

//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CThreadUpdateVer::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall CThreadUpdateVer::CThreadUpdateVer(bool CreateSuspended)
  : TThread(CreateSuspended)
{
    Priority = tpNormal;
    FreeOnTerminate = true;
}
//---------------------------------------------------------------------------
void __fastcall CThreadUpdateVer::Execute()
{
  static int nType=0;
  int nCount;
  while ( !Terminated )
  {
    if (MyWorker.LoginId == true)
    {
      if (nType == 0)
      {
        nCount = dmAgent->GetNewRecdCountEx();
        PostMessage(Application->MainForm->Handle, WM_USER_QUERYCOUNT_MSG, (WPARAM)nType, (LPARAM)nCount);
        nType = 1;
      }
      else if (nType == 1)
      {
        if (g_bDispWaitProcAccept == true && (MyWorker.WorkType == 3 || MyWorker.WorkType == 4))
        {
          nCount = dmAgent->QueryWaitDealAcceptEx();
          PostMessage(Application->MainForm->Handle, WM_USER_QUERYCOUNT_MSG, (WPARAM)nType, (LPARAM)nCount);
        }
        nType = 2;
      }
      else if (nType == 2)
      {
        nCount = dmAgent->QueryDialOutCallBackRecordsEx();
        PostMessage(Application->MainForm->Handle, WM_USER_QUERYCOUNT_MSG, (WPARAM)nType, (LPARAM)nCount);
        nType = 3;
      }
      else if (nType == 3)
      {
        nCount = dmAgent->QueryAcceptCallBackRecordsEx();
        PostMessage(Application->MainForm->Handle, WM_USER_QUERYCOUNT_MSG, (WPARAM)nType, (LPARAM)nCount);
        nType = 4;
      }
      else if (nType == 4)
      {
        nCount = dmAgent->GetNewRfaxCountEx();
        PostMessage(Application->MainForm->Handle, WM_USER_QUERYCOUNT_MSG, (WPARAM)nType, (LPARAM)nCount);
        nType = 5;
      }
      else if (nType == 5)
      {
        nCount = dmAgent->GetNewEmailCountEx();
        PostMessage(Application->MainForm->Handle, WM_USER_QUERYCOUNT_MSG, (WPARAM)nType, (LPARAM)nCount);
        nType = 0;
      }
    }
    ::Sleep(10000);
  }
}
//---------------------------------------------------------------------------
