//---------------------------------------------------------------------------

#ifndef editsubphoneH
#define editsubphoneH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditSubPhone : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TEdit *editSubPhone;
  TEdit *editPassword;
  TEdit *editUserName;
  TComboBox *cbCallRight;
  TEdit *editMobile;
  TEdit *editBusyTran;
  TEdit *editNoAnsTran;
  TComboBox *cbBangMode;
  TComboBox *cbCRBT;
  TComboBox *cbVoiceBox;
  TEdit *editRemark;
  TButton *Button1;
  TButton *Button2;
  void __fastcall cbCallRightKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSubPhoneChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSubPhone(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CSubPhone SubPhone;
  void SetModifyType(bool isinsert);
  void SetSubPhoneData(CSubPhone& subphone);
  int GetSubPhoneData();
  void ClearSubPhoneData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSubPhone *FormEditSubPhone;
//---------------------------------------------------------------------------
#endif
