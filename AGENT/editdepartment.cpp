//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editdepartment.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDepartment *FormDepartment;
//---------------------------------------------------------------------------
__fastcall TFormDepartment::TFormDepartment(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormDepartment::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormDepartment::editDepartmentIDChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormDepartment::Button1Click(TObject *Sender)
{
  int nDepartmentID;

  if (MyIsNumber(editDepartmentID->Text) != 0)
  {
    MessageBox(NULL,"請輸入部門編碼！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editDepartmentID->Text == "0")
  {
    MessageBox(NULL,"部門編碼必須大于0！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editDepartmentName->Text == "")
  {
    MessageBox(NULL,"請輸入部門名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nDepartmentID = StrToInt(editDepartmentID->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsDepartmentExist(nDepartmentID) == 1)
    {
      MessageBox(NULL,"對不起，該部門編碼已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertDepartmentRecord(nDepartmentID, editDepartmentName->Text) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員增加業務編號為：%d的資料", nSrvType);
      dmAgent->adoQryDepartment->Close();
      dmAgent->adoQryDepartment->Open();
      dmAgent->UpdateDepartmentListItems();
      MessageBox(NULL,"增加部門名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加部門名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateDepartmentRecord(nDepartmentID, editDepartmentName->Text) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改業務編號為：%d的資料", nSrvType);
      dmAgent->adoQryDepartment->Close();
      dmAgent->adoQryDepartment->Open();
      dmAgent->UpdateDepartmentListItems();
      MessageBox(NULL,"修改部門名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改部門名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
