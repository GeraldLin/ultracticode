//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editordersms.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormOrderSms *FormOrderSms;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormOrderSms::TFormOrderSms(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormOrderSms::SetOrderData(CMyOrder& myorder)
{
  TListItem  *ListItem1;
  MyOrders = myorder;

  ListItem1 = ListView1->Items->Item[0];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.OrderId;

  ListItem1 = ListView1->Items->Item[1];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.OrderTime;

  ListItem1 = ListView1->Items->Item[2];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.CustomNo;

  ListItem1 = ListView1->Items->Item[3];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.CustTele;

  ListItem1 = ListView1->Items->Item[4];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.CustName;

  ListItem1 = ListView1->Items->Item[5];
	ListItem1->SubItems->Strings[0] = dmAgent->PayTypeItemList.GetItemName(MyOrders.m_Orders.PayType);

  ListItem1 = ListView1->Items->Item[6];
	ListItem1->SubItems->Strings[0] = dmAgent->ExpressTypeItemList.GetItemName(MyOrders.m_Orders.ExpressType);

  ListItem1 = ListView1->Items->Item[7];
	ListItem1->SubItems->Strings[0] = dmAgent->YesNoItemList.GetItemName(MyOrders.m_Orders.IsNeedBill);

  ListItem1 = ListView1->Items->Item[8];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.BillName;

  ListItem1 = ListView1->Items->Item[9];
	ListItem1->SubItems->Strings[0] = IntToStr(MyOrders.m_Orders.ProductTotal);

  ListItem1 = ListView1->Items->Item[10];
	ListItem1->SubItems->Strings[0] = MoneyToStr(MyOrders.m_Orders.OrderAmount);

  ListItem1 = ListView1->Items->Item[11];
  ListItem1->SubItems->Strings[0] = MoneyToStr(MyOrders.m_Orders.DiscountAmount);

  ListItem1 = ListView1->Items->Item[12];
	ListItem1->SubItems->Strings[0] = MoneyToStr(MyOrders.m_Orders.ExpressFee);

  ListItem1 = ListView1->Items->Item[13];
	ListItem1->SubItems->Strings[0] = MoneyToStr(MyOrders.m_Orders.PayAmount);

  ListItem1 = ListView1->Items->Item[14];
	ListItem1->SubItems->Strings[0] = dmAgent->PayStateItemList.GetItemName(MyOrders.m_Orders.PayState);

  ListItem1 = ListView1->Items->Item[15];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.PayMen;

  ListItem1 = ListView1->Items->Item[16];
	ListItem1->SubItems->Strings[0] = MoneyToStr(MyOrders.m_Orders.PayedAmount);

  ListItem1 = ListView1->Items->Item[17];
  ListItem1->SubItems->Strings[0] = MoneyToStr(MyOrders.m_Orders.NoPayAmount);

  ListItem1 = ListView1->Items->Item[18];
  ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.PayedTime;

  ListItem1 = ListView1->Items->Item[19];
	ListItem1->SubItems->Strings[0] = dmAgent->OrderStateItemList.GetItemName(MyOrders.m_Orders.OrderState);

  ListItem1 = ListView1->Items->Item[20];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.ExpressNo;

  ListItem1 = ListView1->Items->Item[21];
  ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.ExpressCorp;

  ListItem1 = ListView1->Items->Item[22];
  ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.ExpressTele;

  ListItem1 = ListView1->Items->Item[23];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.RecvName;

  ListItem1 = ListView1->Items->Item[24];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.RecvAddr;

  ListItem1 = ListView1->Items->Item[25];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.RecvPost;

  ListItem1 = ListView1->Items->Item[26];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.RecvTele;

  ListItem1 = ListView1->Items->Item[27];
	ListItem1->SubItems->Strings[0] = dmAgent->OrderFitStatusItemList.GetItemName(MyOrders.m_Orders.FitStatus);

  ListItem1 = ListView1->Items->Item[28];
  ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.PreFitDate;

  ListItem1 = ListView1->Items->Item[29];
  ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.FactFitDate;

  ListItem1 = ListView1->Items->Item[30];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.FitSrvCorp;

  ListItem1 = ListView1->Items->Item[31];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.FitCorpTele;

  ListItem1 = ListView1->Items->Item[32];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.FitMen;

  ListItem1 = ListView1->Items->Item[33];
	ListItem1->SubItems->Strings[0] = MyOrders.m_Orders.FitMenTel;
}
void __fastcall TFormOrderSms::Button1Click(TObject *Sender)
{
  AnsiString msg, tempmsg;
  TListItem  *ListItem1;

  msg = "訂單短信提示:";
  memoSms->Lines->Clear();
  int ItemCount = ListView1->Items->Count;
  for (int i = 0; i < ItemCount; i ++)
  {
    ListItem1 = ListView1->Items->Item[i];
    if (ListItem1->Checked)
    {
      if (ListItem1->SubItems->Strings[0] != "")
      {
        tempmsg = msg;
        msg = msg + ListItem1->Caption+":"+ListItem1->SubItems->Strings[0];
        if (msg.Length() > 140)
        {
          msg = tempmsg + "`";
          memoSms->Lines->Add(msg);
          msg = "[續上] " + ListItem1->Caption+":"+ListItem1->SubItems->Strings[0];
        }
      }
    }
  }
  msg = msg + "`";
  memoSms->Lines->Add(msg);
  //if (memoSms->Lines->Text.Length() > 140)
  //{
  //  MessageBox(NULL,"短信內容以超出長度限制!","信息提示",MB_OK|MB_ICONINFORMATION);
  //}
}
//---------------------------------------------------------------------------
void __fastcall TFormOrderSms::Button9Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormOrderSms::Button10Click(TObject *Sender)
{
  memoSms->Lines->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormOrderSms::Button8Click(TObject *Sender)
{
  SendSms(editSendMobile->Text, memoSms->Lines->Text,
    dmAgent->SendSmsSrvTypeItemList.GetItemValue(cbSendSmsSrvType->Text), 1);
}
//---------------------------------------------------------------------------

void __fastcall TFormOrderSms::FormCreate(TObject *Sender)
{
  TListItem  *ListItem1;

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "定單編號";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "下單時間";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "客戶編號";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "客戶電話";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "客戶名稱";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "支付方式";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "配送方式";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "開發票否";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "發票抬頭";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "產品總訂購數";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "定單總金額";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "折扣后定單總金額";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "運費";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "應付總金額";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "支付狀態";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "付款人";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "已付金額";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "欠費金額";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "付款時間";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "定單狀態";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "快遞單號";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "快遞物流公司";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "快遞物流公司電話";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "收貨人姓名";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "收貨人地址";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "收貨人郵編";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "收貨人電話";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "安裝狀態";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "預計安裝時間";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "實際安裝時間";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "安裝服務商";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "服務商電話";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "安裝人員";
  ListItem1->SubItems->Insert(0, "");

  ListItem1 = ListView1->Items->Add();
  ListItem1->Caption = "安裝人員聯系電話";
  ListItem1->SubItems->Insert(0, "");

  dmAgent->AddCmbItem(cbSendSmsSrvType, dmAgent->SendSmsSrvTypeItemList, "");
}
//---------------------------------------------------------------------------

void __fastcall TFormOrderSms::Button4Click(TObject *Sender)
{
  SendSms(MyOrders.m_Orders.CustTele, memoSms->Lines->Text,
    dmAgent->SendSmsSrvTypeItemList.GetItemValue(cbSendSmsSrvType->Text), 1);
}
//---------------------------------------------------------------------------

void __fastcall TFormOrderSms::Button6Click(TObject *Sender)
{
  SendSms(MyOrders.m_Orders.RecvTele, memoSms->Lines->Text,
    dmAgent->SendSmsSrvTypeItemList.GetItemValue(cbSendSmsSrvType->Text), 1);
}
//---------------------------------------------------------------------------

void __fastcall TFormOrderSms::Button7Click(TObject *Sender)
{
  SendSms(MyOrders.m_Orders.FitMenTel, memoSms->Lines->Text,
    dmAgent->SendSmsSrvTypeItemList.GetItemValue(cbSendSmsSrvType->Text), 1);
}
//---------------------------------------------------------------------------


void __fastcall TFormOrderSms::Button12Click(TObject *Sender)
{
  SendSms(MyOrders.m_Orders.FitCorpTele, memoSms->Lines->Text,
    dmAgent->SendSmsSrvTypeItemList.GetItemValue(cbSendSmsSrvType->Text), 1);
}
//---------------------------------------------------------------------------


void __fastcall TFormOrderSms::Button13Click(TObject *Sender)
{
  AnsiString msg;
  bool nextid=false;

  memoSms->Lines->Clear();
  memoSms->Lines->Add("訂單明細短信提示：");
  for (int i=0; i<MAX_ORDERLIST_NUM; i++)
  {
    if (MyOrders.m_Orderlist[i].state != 0)
    {
      if (nextid == false)
      {
        msg = "訂單編號:" + MyOrders.m_Orders.OrderId;
        nextid = true;
      }
      else
      {
        msg = "[續上]訂單編號:" + MyOrders.m_Orders.OrderId;
      }
      msg = msg + "產品編號:" + MyOrders.m_Orderlist[i].ProductNo;
      msg = msg + " 產品名稱:" + MyOrders.m_Orderlist[i].ProductName;
      msg = msg + " 訂購數量:" + IntToStr(MyOrders.m_Orderlist[i].Totals);
      if (ckPrice->Checked)
      {
        msg = msg + " 單價:" + MoneyToStr(MyOrders.m_Orderlist[i].Price);
        msg = msg + " 小計:" + MoneyToStr(MyOrders.m_Orderlist[i].SubAmount);
      }
      msg = msg + "`";
      memoSms->Lines->Add(msg);
    }
  }
  //if (memoSms->Lines->Text.Length() > 140)
  //{
  //  MessageBox(NULL,"短信內容以超出長度限制!","信息提示",MB_OK|MB_ICONINFORMATION);
  //}
}
//---------------------------------------------------------------------------
void TFormOrderSms::SendSms(AnsiString mobileno, AnsiString msg, int smstype, int priority)
{
  int i, j=0;
  char sms[200];

  memset(sms, 0, 200);
  for (i=0; i<msg.Length(); i++)
  {
    if (msg[i+1] == '`' || msg[i+1] == '\0')
    {
      if (j > 0)
      {
        if (dmAgent->WriteSendSms(mobileno, (char *)sms, smstype, priority) != 0)
          return;
        j = 0;
      }
    }
    else
    {
      if (msg[i+1] != '\r' && msg[i+1] != '\n' && msg[i+1] != '\t' && j < 140)
      {
        sms[j] = msg[i+1];
        j++;
      }
    }
  }
  MessageBox(NULL,"發送短信提交成功！","信息提示",MB_OK|MB_ICONINFORMATION);
}
