//---------------------------------------------------------------------------

#ifndef threadH
#define threadH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class CMyThread : public TThread
{            
private:
protected:
  void __fastcall Execute();
public:
  __fastcall CMyThread(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
