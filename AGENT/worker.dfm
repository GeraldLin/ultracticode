object FormWorker: TFormWorker
  Left = 323
  Top = 153
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20540#27231#21729#36039#26009
  ClientHeight = 724
  ClientWidth = 503
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 26
    Top = 17
    Width = 60
    Height = 12
    Caption = #20540#27231#32232#34399#65306
  end
  object Label2: TLabel
    Left = 26
    Top = 117
    Width = 60
    Height = 12
    Caption = #27402#38480#35282#33394#65306
  end
  object Label3: TLabel
    Left = 20
    Top = 141
    Width = 66
    Height = 12
    Caption = #31532'1'#20540#27231#32068#65306
  end
  object Label4: TLabel
    Left = 244
    Top = 144
    Width = 78
    Height = 12
    Caption = #31532'1'#25216#33021#32026#21029#65306
  end
  object Label5: TLabel
    Left = 259
    Top = 44
    Width = 60
    Height = 12
    Caption = #30331#37636#23494#30908#65306
  end
  object Label6: TLabel
    Left = 284
    Top = 15
    Width = 36
    Height = 12
    Caption = #22995#21517#65306
  end
  object Label7: TLabel
    Left = 26
    Top = 557
    Width = 60
    Height = 12
    Caption = #38651#35441#34399#30908#65306
  end
  object Label8: TLabel
    Left = 27
    Top = 582
    Width = 60
    Height = 12
    Caption = #25163#27231#34399#30908#65306
  end
  object Label9: TLabel
    Left = 50
    Top = 605
    Width = 36
    Height = 12
    Caption = #20303#22336#65306
  end
  object Label10: TLabel
    Left = 260
    Top = 582
    Width = 60
    Height = 12
    Caption = #38651#23376#37109#20214#65306
  end
  object Label11: TLabel
    Left = 258
    Top = 117
    Width = 60
    Height = 12
    Caption = #21628#20986#27402#38480#65306
  end
  object Label12: TLabel
    Left = 20
    Top = 165
    Width = 66
    Height = 12
    Caption = #31532'2'#20540#27231#32068#65306
  end
  object Label13: TLabel
    Left = 20
    Top = 192
    Width = 66
    Height = 12
    Caption = #31532'3'#20540#27231#32068#65306
  end
  object Label14: TLabel
    Left = 20
    Top = 217
    Width = 66
    Height = 12
    Caption = #31532'4'#20540#27231#32068#65306
  end
  object Label15: TLabel
    Left = 20
    Top = 242
    Width = 66
    Height = 12
    Caption = #31532'5'#20540#27231#32068#65306
  end
  object Label16: TLabel
    Left = 26
    Top = 40
    Width = 60
    Height = 12
    Caption = #25152#23660#37096#38272#65306
  end
  object Label17: TLabel
    Left = 260
    Top = 557
    Width = 60
    Height = 12
    Caption = #20998#27231#34399#30908#65306
  end
  object Label18: TLabel
    Left = 26
    Top = 63
    Width = 60
    Height = 12
    Caption = #21729#24037#32232#34399#65306
  end
  object Label19: TLabel
    Left = 20
    Top = 266
    Width = 66
    Height = 12
    Caption = #31532'6'#20540#27231#32068#65306
  end
  object Label20: TLabel
    Left = 20
    Top = 290
    Width = 66
    Height = 12
    Caption = #31532'7'#20540#27231#32068#65306
  end
  object Label21: TLabel
    Left = 20
    Top = 314
    Width = 66
    Height = 12
    Caption = #31532'8'#20540#27231#32068#65306
  end
  object Label22: TLabel
    Left = 20
    Top = 338
    Width = 66
    Height = 12
    Caption = #31532'9'#20540#27231#32068#65306
  end
  object Label23: TLabel
    Left = 14
    Top = 362
    Width = 72
    Height = 12
    Caption = #31532'10'#20540#27231#32068#65306
  end
  object Label24: TLabel
    Left = 14
    Top = 386
    Width = 72
    Height = 12
    Caption = #31532'11'#20540#27231#32068#65306
  end
  object Label25: TLabel
    Left = 14
    Top = 410
    Width = 72
    Height = 12
    Caption = #31532'12'#20540#27231#32068#65306
  end
  object Label26: TLabel
    Left = 14
    Top = 434
    Width = 72
    Height = 12
    Caption = #31532'13'#20540#27231#32068#65306
  end
  object Label27: TLabel
    Left = 14
    Top = 458
    Width = 72
    Height = 12
    Caption = #31532'14'#20540#27231#32068#65306
  end
  object Label28: TLabel
    Left = 14
    Top = 482
    Width = 72
    Height = 12
    Caption = #31532'15'#20540#27231#32068#65306
  end
  object Label29: TLabel
    Left = 14
    Top = 506
    Width = 72
    Height = 12
    Caption = #31532'16'#20540#27231#32068#65306
  end
  object Label30: TLabel
    Left = 244
    Top = 168
    Width = 78
    Height = 12
    Caption = #31532'2'#25216#33021#32026#21029#65306
  end
  object Label31: TLabel
    Left = 244
    Top = 192
    Width = 78
    Height = 12
    Caption = #31532'3'#25216#33021#32026#21029#65306
  end
  object Label32: TLabel
    Left = 244
    Top = 216
    Width = 78
    Height = 12
    Caption = #31532'4'#25216#33021#32026#21029#65306
  end
  object Label33: TLabel
    Left = 244
    Top = 240
    Width = 78
    Height = 12
    Caption = #31532'5'#25216#33021#32026#21029#65306
  end
  object Label34: TLabel
    Left = 244
    Top = 264
    Width = 78
    Height = 12
    Caption = #31532'6'#25216#33021#32026#21029#65306
  end
  object Label35: TLabel
    Left = 244
    Top = 288
    Width = 78
    Height = 12
    Caption = #31532'7'#25216#33021#32026#21029#65306
  end
  object Label36: TLabel
    Left = 244
    Top = 312
    Width = 78
    Height = 12
    Caption = #31532'8'#25216#33021#32026#21029#65306
  end
  object Label37: TLabel
    Left = 244
    Top = 336
    Width = 78
    Height = 12
    Caption = #31532'9'#25216#33021#32026#21029#65306
  end
  object Label38: TLabel
    Left = 238
    Top = 360
    Width = 84
    Height = 12
    Caption = #31532'10'#25216#33021#32026#21029#65306
  end
  object Label39: TLabel
    Left = 238
    Top = 384
    Width = 84
    Height = 12
    Caption = #31532'11'#25216#33021#32026#21029#65306
  end
  object Label40: TLabel
    Left = 238
    Top = 408
    Width = 84
    Height = 12
    Caption = #31532'12'#25216#33021#32026#21029#65306
  end
  object Label41: TLabel
    Left = 238
    Top = 432
    Width = 84
    Height = 12
    Caption = #31532'13'#25216#33021#32026#21029#65306
  end
  object Label42: TLabel
    Left = 238
    Top = 456
    Width = 84
    Height = 12
    Caption = #31532'14'#25216#33021#32026#21029#65306
  end
  object Label43: TLabel
    Left = 238
    Top = 480
    Width = 84
    Height = 12
    Caption = #31532'15'#25216#33021#32026#21029#65306
  end
  object Label44: TLabel
    Left = 238
    Top = 504
    Width = 84
    Height = 12
    Caption = #31532'16'#25216#33021#32026#21029#65306
  end
  object Label45: TLabel
    Left = 178
    Top = 91
    Width = 144
    Height = 12
    Caption = #20154#24037#22806#25765#26159#21542#37782#23450#19979#19968#26781#65306
  end
  object Label46: TLabel
    Left = 8
    Top = 531
    Width = 78
    Height = 12
    Caption = #30331#37636'PBX'#32676#32068#65306
  end
  object Label47: TLabel
    Left = 242
    Top = 531
    Width = 78
    Height = 12
    Caption = #30331#37636'PBX'#23494#30908#65306
  end
  object edtWWorkerNo: TEdit
    Left = 95
    Top = 13
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = edtWWorkerNoChange
  end
  object cmbWWorkType: TComboBox
    Left = 95
    Top = 112
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 3
    Text = #26222#36890#20540#27231#21729
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
    Items.Strings = (
      '1-'#26222#36890#20540#27231#21729
      '2-'#29677#38263#20540#27231#21729
      '3-'#31649#29702#21729)
  end
  object cspWwLevel: TCSpinEdit
    Left = 329
    Top = 140
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 10
    OnChange = edtWWorkerNoChange
  end
  object edtWPassword: TEdit
    Left = 329
    Top = 35
    Width = 131
    Height = 20
    PasswordChar = '*'
    TabOrder = 1
    OnChange = edtWWorkerNoChange
  end
  object edtWWorkerName: TEdit
    Left = 329
    Top = 11
    Width = 131
    Height = 20
    TabOrder = 11
    OnChange = edtWWorkerNoChange
  end
  object edtWTele: TEdit
    Left = 95
    Top = 553
    Width = 131
    Height = 20
    TabOrder = 12
    OnChange = edtWWorkerNoChange
  end
  object edtWMobile: TEdit
    Left = 95
    Top = 577
    Width = 131
    Height = 20
    TabOrder = 13
    OnChange = edtWWorkerNoChange
  end
  object edtWAddress: TEdit
    Left = 95
    Top = 601
    Width = 365
    Height = 20
    TabOrder = 14
    OnChange = edtWWorkerNoChange
  end
  object Button1: TButton
    Left = 122
    Top = 673
    Width = 81
    Height = 27
    Caption = #20462#25913
    Default = True
    Enabled = False
    TabOrder = 15
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 295
    Top = 673
    Width = 82
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 16
    OnClick = Button2Click
  end
  object edtWEmail: TEdit
    Left = 329
    Top = 577
    Width = 131
    Height = 20
    TabOrder = 17
    OnChange = edtWWorkerNoChange
  end
  object cmbWCallRight: TComboBox
    Left = 329
    Top = 112
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 4
    Text = #25765#25171#22283#20839#38263#36884
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
    Items.Strings = (
      #21482#38480#25509#32893#38651#35441
      #25765#25171#20839#37096#38651#35441
      #25765#25171#24066#20839#38651#35441
      #25765#25171#24066#20839#38651#35441#21450#25163#27231
      #25765#25171#22283#20839#38263#36884
      #25765#25171#28207#28595#38263#36884
      #25765#25171#22283#38555#38263#36884)
  end
  object cbGroupNo: TComboBox
    Left = 95
    Top = 137
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 5
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo1: TComboBox
    Left = 95
    Top = 161
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 6
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo2: TComboBox
    Left = 95
    Top = 187
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 7
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo3: TComboBox
    Left = 95
    Top = 212
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 8
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo4: TComboBox
    Left = 95
    Top = 238
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 9
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbDepartmentID: TComboBox
    Left = 95
    Top = 35
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 2
    OnChange = edtWWorkerNoChange
  end
  object edtSubPhone: TEdit
    Left = 329
    Top = 553
    Width = 131
    Height = 20
    TabOrder = 18
    OnChange = edtWWorkerNoChange
  end
  object edtEmployeeID: TEdit
    Left = 95
    Top = 59
    Width = 131
    Height = 20
    TabOrder = 19
    OnChange = edtWWorkerNoChange
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 640
    Width = 105
    Height = 17
    Caption = #38577#34255#20358#38651#34399#30908'?'
    TabOrder = 20
    OnClick = edtWWorkerNoChange
  end
  object CheckBox2: TCheckBox
    Left = 120
    Top = 640
    Width = 105
    Height = 17
    Caption = #38577#34255#22806#25765#34399#30908'?'
    TabOrder = 21
    OnClick = edtWWorkerNoChange
  end
  object CheckBox3: TCheckBox
    Left = 232
    Top = 640
    Width = 129
    Height = 17
    Caption = #38577#34255#23458#25142#36039#26009#34399#30908'?'
    TabOrder = 22
    OnClick = edtWWorkerNoChange
  end
  object CheckBox4: TCheckBox
    Left = 368
    Top = 640
    Width = 129
    Height = 17
    Caption = #38577#34255#22806#25765#36039#26009#34399#30908'?'
    TabOrder = 23
    OnClick = edtWWorkerNoChange
  end
  object cbGroupNo5: TComboBox
    Left = 95
    Top = 262
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 24
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo6: TComboBox
    Left = 95
    Top = 286
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 25
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo7: TComboBox
    Left = 95
    Top = 310
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 26
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo8: TComboBox
    Left = 95
    Top = 334
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 27
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo9: TComboBox
    Left = 95
    Top = 358
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 28
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo10: TComboBox
    Left = 95
    Top = 382
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 29
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo11: TComboBox
    Left = 95
    Top = 406
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 30
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo12: TComboBox
    Left = 95
    Top = 430
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 31
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo13: TComboBox
    Left = 95
    Top = 454
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 32
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo14: TComboBox
    Left = 95
    Top = 478
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 33
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cbGroupNo15: TComboBox
    Left = 95
    Top = 502
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 34
    Text = #19981#36984#25799
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
  end
  object cspWwLevel1: TCSpinEdit
    Left = 329
    Top = 164
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 35
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel2: TCSpinEdit
    Left = 329
    Top = 188
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 36
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel3: TCSpinEdit
    Left = 329
    Top = 212
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 37
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel4: TCSpinEdit
    Left = 329
    Top = 236
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 38
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel5: TCSpinEdit
    Left = 329
    Top = 260
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 39
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel6: TCSpinEdit
    Left = 329
    Top = 284
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 40
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel7: TCSpinEdit
    Left = 329
    Top = 308
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 41
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel8: TCSpinEdit
    Left = 329
    Top = 332
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 42
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel9: TCSpinEdit
    Left = 329
    Top = 356
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 43
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel10: TCSpinEdit
    Left = 329
    Top = 380
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 44
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel11: TCSpinEdit
    Left = 329
    Top = 404
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 45
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel12: TCSpinEdit
    Left = 329
    Top = 428
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 46
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel13: TCSpinEdit
    Left = 329
    Top = 452
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 47
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel14: TCSpinEdit
    Left = 329
    Top = 476
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 48
    OnChange = edtWWorkerNoChange
  end
  object cspWwLevel15: TCSpinEdit
    Left = 329
    Top = 500
    Width = 131
    Height = 21
    MaxValue = 15
    MinValue = 1
    TabOrder = 49
    OnChange = edtWWorkerNoChange
  end
  object ckAutoAnswerId: TCheckBox
    Left = 328
    Top = 64
    Width = 97
    Height = 17
    Caption = #33258#21205#25033#31572'?'
    TabOrder = 50
    OnClick = edtWWorkerNoChange
  end
  object cbProcDialoutNextType: TComboBox
    Left = 327
    Top = 87
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 51
    Text = #19981#37782#23450
    OnChange = edtWWorkerNoChange
    OnKeyPress = cmbWWorkTypeKeyPress
    Items.Strings = (
      #19981#37782#23450
      #37782#23450)
  end
  object edtSWTGroupID: TEdit
    Left = 95
    Top = 527
    Width = 131
    Height = 20
    TabOrder = 52
    OnChange = edtWWorkerNoChange
  end
  object edtSWTGroupPWD: TEdit
    Left = 329
    Top = 527
    Width = 131
    Height = 20
    TabOrder = 53
    OnChange = edtWWorkerNoChange
  end
end
