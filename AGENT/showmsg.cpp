//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "showmsg.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMsg *FormMsg;
//---------------------------------------------------------------------------
__fastcall TFormMsg::TFormMsg(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormMsg::SetShowMsg(char *msg)
{
  lblMsg->Caption = (char *)msg;
}
void TFormMsg::SetShowMsg(AnsiString msg)
{
  lblMsg->Caption = msg;
}
void TFormMsg::SetShowMsg(LPCTSTR lpszFormat, ...)
{
  char buf[2048];
  va_list		ArgList;
	va_start(ArgList, lpszFormat);
	_vsnprintf(buf, 2048, lpszFormat, ArgList);
	va_end (ArgList);
  SetShowMsg(buf);
}
void __fastcall TFormMsg::Button1Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
