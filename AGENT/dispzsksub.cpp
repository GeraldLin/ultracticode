//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dispzsksub.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDispZSKSub *FormDispZSKSub;
//---------------------------------------------------------------------------
__fastcall TFormDispZSKSub::TFormDispZSKSub(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormDispZSKSub::SetZSKSubData(CZSKSub& zsksub)
{
  editMainTitle->Text = zsksub.MainTitle;
  editSubTitle->Text = zsksub.SubTitle;
  memoExplain->Lines->Clear();
  memoExplain->Lines->Add(zsksub.Explain);
  editRemark->Text = zsksub.Remark;
}
void __fastcall TFormDispZSKSub::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

