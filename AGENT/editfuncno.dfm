object FormEditFuncNo: TFormEditFuncNo
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'IVR'#27969#31243#21151#33021#34399#35373#23450
  ClientHeight = 174
  ClientWidth = 272
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 22
    Width = 90
    Height = 13
    Caption = 'IVR'#27969#31243#21151#33021#34399#65306
  end
  object Label2: TLabel
    Left = 32
    Top = 56
    Width = 78
    Height = 13
    Caption = 'IVR'#27969#31243#27284#21517#65306
  end
  object Label3: TLabel
    Left = 32
    Top = 91
    Width = 78
    Height = 13
    Caption = 'IVR'#27969#31243#21517#31281#65306
  end
  object editFlwFile: TEdit
    Left = 121
    Top = 52
    Width = 131
    Height = 21
    TabOrder = 0
    OnChange = cseFuncNoChange
  end
  object editFlwName: TEdit
    Left = 121
    Top = 87
    Width = 131
    Height = 21
    TabOrder = 1
    OnChange = cseFuncNoChange
  end
  object cseFuncNo: TCSpinEdit
    Left = 121
    Top = 17
    Width = 131
    Height = 22
    MaxValue = 128
    TabOrder = 2
    OnChange = cseFuncNoChange
  end
  object Button1: TButton
    Left = 26
    Top = 130
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 165
    Top = 130
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
end
