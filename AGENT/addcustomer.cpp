//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "addcustomer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAddCustom *FormAddCustom;
//---------------------------------------------------------------------------
__fastcall TFormAddCustom::TFormAddCustom(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormAddCustom::SetCustom(CCustomer *customer)
{
  edtCustomNo->Text = customer->CustomNo; //客戶編號
  edtCustName->Text = customer->CustName; //客戶名稱
  edtCorpName->Text = customer->CorpName; //公司名稱
  edtCustType->Text = customer->CustType; //客戶類型
  edtCustSex->Text = customer->CustSex; //性別
  edtMobileNo->Text = customer->MobileNo; //手機
  edtTeleNo->Text = customer->TeleNo; //固話
  edtFaxNo->Text = customer->FaxNo; //傳真
  edtEMail->Text = customer->EMail; //郵箱
  edtContAddr->Text = customer->ContAddr; //地址
  edtPostNo->Text = customer->PostNo; //郵編
  edtPoints->Text = customer->Points; //積分
  edtRemark->Text = customer->Remark; //備注
}
//---------------------------------------------------------------------------

void __fastcall TFormAddCustom::Button1Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------


