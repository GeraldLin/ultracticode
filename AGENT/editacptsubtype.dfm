object FormAcptSubType: TFormAcptSubType
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24037#21333#31867#22411#35774#32622
  ClientHeight = 212
  ClientWidth = 458
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 20
    Width = 84
    Height = 12
    Caption = #24037#21333#31867#22411#32534#21495#65306
  end
  object Label2: TLabel
    Left = 232
    Top = 19
    Width = 84
    Height = 12
    Caption = #24037#21333#31867#22411#21517#31216#65306
  end
  object Label3: TLabel
    Left = 16
    Top = 40
    Width = 144
    Height = 12
    Caption = #40664#35748#33258#21160#22635#20805#30340#21463#29702#20449#24687#65306
  end
  object editAcptSubTypeId: TEdit
    Left = 104
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editAcptSubTypeIdChange
  end
  object editAcptSubTypeName: TEdit
    Left = 320
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editAcptSubTypeIdChange
  end
  object memoRemark: TMemo
    Left = 16
    Top = 56
    Width = 425
    Height = 105
    ScrollBars = ssBoth
    TabOrder = 2
    OnChange = editAcptSubTypeIdChange
  end
  object Button1: TButton
    Left = 112
    Top = 176
    Width = 75
    Height = 25
    Caption = #30830#23450
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 288
    Top = 176
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
end
