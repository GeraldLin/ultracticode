//---------------------------------------------------------------------------

#ifndef editholidayH
#define editholidayH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormEditHoliday : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editHolidayName;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TDateTimePicker *dtpStartDay;
  TDateTimePicker *dtpEndDay;
  TComboBox *cbOpenFlag;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label5;
  TEdit *editCTID;
  void __fastcall cbOpenFlagKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editHolidayNameChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditHoliday(TComponent* Owner);

  bool InsertId;
  int nId;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditHoliday *FormEditHoliday;
//---------------------------------------------------------------------------
#endif
