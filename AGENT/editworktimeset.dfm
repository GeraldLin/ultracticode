object FormEditWorkTimeSet: TFormEditWorkTimeSet
  Left = 194
  Top = 115
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #19978#29677#26178#20214#35373#23450
  ClientHeight = 222
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 288
    Top = 44
    Width = 84
    Height = 12
    Caption = #19978#29677#26085#26399#39006#22411#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 55
    Top = 68
    Width = 54
    Height = 12
    Caption = #38283#22987#26085#26399':'
  end
  object Label3: TLabel
    Left = 311
    Top = 68
    Width = 54
    Height = 12
    Caption = #25130#27490#26085#26399':'
  end
  object Label4: TLabel
    Left = 32
    Top = 92
    Width = 78
    Height = 12
    Caption = #19978#21320#19978#29677#26178#38291';'
  end
  object Label5: TLabel
    Left = 32
    Top = 117
    Width = 78
    Height = 12
    Caption = #19978#21320#19979#29677#26178#38291';'
  end
  object Label6: TLabel
    Left = 287
    Top = 93
    Width = 78
    Height = 12
    Caption = #19979#21320#19978#29677#26178#38291';'
  end
  object Label7: TLabel
    Left = 287
    Top = 117
    Width = 78
    Height = 12
    Caption = #19979#21320#19979#29677#26178#38291';'
  end
  object Label11: TLabel
    Left = 54
    Top = 44
    Width = 54
    Height = 12
    Caption = #20195#34920#34399#30908':'
  end
  object Label10: TLabel
    Left = 312
    Top = 140
    Width = 54
    Height = 12
    Caption = #21855#29992#29376#24907':'
  end
  object Label8: TLabel
    Left = 56
    Top = 140
    Width = 54
    Height = 12
    Caption = #26178#38291#39006#21029':'
  end
  object Label9: TLabel
    Left = 54
    Top = 20
    Width = 54
    Height = 12
    Caption = #36890#19968#32232#34399':'
  end
  object cbDayType: TComboBox
    Left = 376
    Top = 40
    Width = 140
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    Text = #27599#21608#26576#22825
    OnChange = cbDayTypeChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #27599#21608#26576#22825
      #27599#26376#26576#22825
      #20855#39636#26576#22825)
  end
  object dtpAMStartTime: TDateTimePicker
    Left = 120
    Top = 88
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 1
    OnChange = editHotLineChange
  end
  object dtpAMEndTime: TDateTimePicker
    Left = 120
    Top = 112
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 2
    OnChange = editHotLineChange
  end
  object dtpPMStartTime: TDateTimePicker
    Left = 376
    Top = 88
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 3
    OnChange = editHotLineChange
  end
  object dtpPMEndTime: TDateTimePicker
    Left = 376
    Top = 112
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5202569792
    Time = 40944.5202569792
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 4
    OnChange = editHotLineChange
  end
  object cbStartWeek: TComboBox
    Left = 120
    Top = 64
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 5
    Text = #26143#26399#20845
    OnChange = editHotLineChange
    Items.Strings = (
      #26143#26399#26085
      #26143#26399#19968
      #26143#26399#20108
      #26143#26399#19977
      #26143#26399#22235
      #26143#26399#20116
      #26143#26399#20845)
  end
  object cbEndWeek: TComboBox
    Left = 376
    Top = 64
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 6
    Text = #26143#26399#20845
    OnChange = editHotLineChange
    Items.Strings = (
      #26143#26399#26085
      #26143#26399#19968
      #26143#26399#20108
      #26143#26399#19977
      #26143#26399#22235
      #26143#26399#20116
      #26143#26399#20845)
  end
  object cbStartDay: TComboBox
    Left = 120
    Top = 64
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 7
    Text = #27599#26376'1'#26085
    OnChange = editHotLineChange
    Items.Strings = (
      #27599#26376'1'#26085
      #27599#26376'2'#26085
      #27599#26376'3'#26085
      #27599#26376'4'#26085
      #27599#26376'5'#26085
      #27599#26376'6'#26085
      #27599#26376'7'#26085
      #27599#26376'8'#26085
      #27599#26376'9'#26085
      #27599#26376'10'#26085
      #27599#26376'11'#26085
      #27599#26376'12'#26085
      #27599#26376'13'#26085
      #27599#26376'14'#26085
      #27599#26376'15'#26085
      #27599#26376'16'#26085
      #27599#26376'17'#26085
      #27599#26376'18'#26085
      #27599#26376'19'#26085
      #27599#26376'20'#26085
      #27599#26376'21'#26085
      #27599#26376'22'#26085
      #27599#26376'23'#26085
      #27599#26376'24'#26085
      #27599#26376'25'#26085
      #27599#26376'26'#26085
      #27599#26376'27'#26085
      #27599#26376'28'#26085
      #27599#26376'29'#26085
      #27599#26376'30'#26085
      #27599#26376'31'#26085)
  end
  object cbEndDay: TComboBox
    Left = 376
    Top = 64
    Width = 85
    Height = 20
    ItemHeight = 12
    TabOrder = 8
    Text = #27599#26376'1'#26085
    OnChange = editHotLineChange
    Items.Strings = (
      #27599#26376'1'#26085
      #27599#26376'2'#26085
      #27599#26376'3'#26085
      #27599#26376'4'#26085
      #27599#26376'5'#26085
      #27599#26376'6'#26085
      #27599#26376'7'#26085
      #27599#26376'8'#26085
      #27599#26376'9'#26085
      #27599#26376'10'#26085
      #27599#26376'11'#26085
      #27599#26376'12'#26085
      #27599#26376'13'#26085
      #27599#26376'14'#26085
      #27599#26376'15'#26085
      #27599#26376'16'#26085
      #27599#26376'17'#26085
      #27599#26376'18'#26085
      #27599#26376'19'#26085
      #27599#26376'20'#26085
      #27599#26376'21'#26085
      #27599#26376'22'#26085
      #27599#26376'23'#26085
      #27599#26376'24'#26085
      #27599#26376'25'#26085
      #27599#26376'26'#26085
      #27599#26376'27'#26085
      #27599#26376'28'#26085
      #27599#26376'29'#26085
      #27599#26376'30'#26085
      #27599#26376'31'#26085)
  end
  object dtpStartDate: TDateTimePicker
    Left = 120
    Top = 64
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.524660544
    Time = 40944.524660544
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 9
    OnChange = editHotLineChange
  end
  object dtpEndDate: TDateTimePicker
    Left = 376
    Top = 64
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 40944.5247254745
    Time = 40944.5247254745
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 10
    OnChange = editHotLineChange
  end
  object cbOpenFlag: TComboBox
    Left = 376
    Top = 136
    Width = 140
    Height = 20
    ItemHeight = 12
    TabOrder = 11
    Text = #21855#29992
    OnChange = editHotLineChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #21855#29992
      #20572#29992)
  end
  object Button1: TButton
    Left = 144
    Top = 176
    Width = 75
    Height = 25
    Caption = #30830#23450
    TabOrder = 12
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 328
    Top = 176
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 13
    OnClick = Button2Click
  end
  object cbWorkType: TComboBox
    Left = 120
    Top = 136
    Width = 140
    Height = 20
    ItemHeight = 12
    TabOrder = 14
    Text = #19978#29677#26178#38291
    OnChange = editHotLineChange
    OnKeyPress = cbDayTypeKeyPress
    Items.Strings = (
      #19978#29677#26178#38291
      #19979#29677#26178#38291
      #25918#20551#26178#38291
      #21320#20241#26178#38291
      #20540#29677#26178#38291
      #26202#39184#26178#38291)
  end
  object editHotLine: TEdit
    Left = 120
    Top = 40
    Width = 137
    Height = 20
    TabOrder = 15
    Text = '*'
    OnChange = editHotLineChange
  end
  object editCTID: TEdit
    Left = 120
    Top = 16
    Width = 137
    Height = 20
    TabOrder = 16
    Text = '*'
    OnChange = editHotLineChange
  end
end
