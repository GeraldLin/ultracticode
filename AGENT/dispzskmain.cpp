//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dispzskmain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDispZSKMain *FormDispZSKMain;
//---------------------------------------------------------------------------
__fastcall TFormDispZSKMain::TFormDispZSKMain(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormDispZSKMain::SetZSKMainData(CZSKMain& zskmain)
{
  editMainTitle->Text = zskmain.MainTitle;
  memoExplain->Lines->Clear();
  memoExplain->Lines->Add(zskmain.Explain);
  editRemark->Text = zskmain.Remark;
}
void __fastcall TFormDispZSKMain::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
