//---------------------------------------------------------------------------

#ifndef dispzskmainH
#define dispzskmainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormDispZSKMain : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label11;
  TEdit *editMainTitle;
  TEdit *editRemark;
  TMemo *memoExplain;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDispZSKMain(TComponent* Owner);

  void SetZSKMainData(CZSKMain& zskmain);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDispZSKMain *FormDispZSKMain;
//---------------------------------------------------------------------------
#endif
