//---------------------------------------------------------------------------

#ifndef dispanswerdemoH
#define dispanswerdemoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormAnswerDemo : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel1;
  TButton *Button1;
  TMemo *memoDemo;
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAnswerDemo(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAnswerDemo *FormAnswerDemo;
//---------------------------------------------------------------------------
#endif
