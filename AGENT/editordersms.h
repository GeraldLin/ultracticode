//---------------------------------------------------------------------------

#ifndef editordersmsH
#define editordersmsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "public.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormOrderSms : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TButton *Button1;
  TMemo *memoSms;
  TButton *Button4;
  TButton *Button6;
  TButton *Button7;
  TLabel *Label4;
  TEdit *editSendMobile;
  TButton *Button8;
  TButton *Button9;
  TButton *Button10;
  TButton *Button12;
  TLabel *Label113;
  TComboBox *cbSendSmsSrvType;
  TListView *ListView1;
  TButton *Button13;
  TCheckBox *ckPrice;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button9Click(TObject *Sender);
  void __fastcall Button10Click(TObject *Sender);
  void __fastcall Button8Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall Button4Click(TObject *Sender);
  void __fastcall Button6Click(TObject *Sender);
  void __fastcall Button7Click(TObject *Sender);
  void __fastcall Button12Click(TObject *Sender);
  void __fastcall Button13Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormOrderSms(TComponent* Owner);

  CMyOrder MyOrders;
  void SetOrderData(CMyOrder& myorder);
  void SendSms(AnsiString mobileno, AnsiString msg, int smstype, int priority);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormOrderSms *FormOrderSms;
//---------------------------------------------------------------------------
#endif
