object FormEditCustType: TFormEditCustType
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#23458#25143#31867#22411
  ClientHeight = 139
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 84
    Height = 12
    Caption = #23458#25143#31867#22411#32534#21495#65306
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 84
    Height = 12
    Caption = #23458#25143#31867#22411#21517#31216#65306
  end
  object editCustType: TEdit
    Left = 120
    Top = 8
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editCustTypeChange
  end
  object editCustTypeName: TEdit
    Left = 120
    Top = 40
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editCustTypeChange
  end
  object Button1: TButton
    Left = 40
    Top = 88
    Width = 75
    Height = 25
    Caption = #30830#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 160
    Top = 88
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
end
