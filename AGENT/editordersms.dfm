object FormOrderSms: TFormOrderSms
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #35746#21333#30701#20449
  ClientHeight = 428
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 13
    Width = 112
    Height = 13
    Caption = #35831#36873#25321#35746#21333#39033#30446#65306
  end
  object Label4: TLabel
    Left = 288
    Top = 364
    Width = 70
    Height = 13
    Caption = #25163#26426#21495#30721#65306
  end
  object Label113: TLabel
    Left = 288
    Top = 13
    Width = 93
    Height = 13
    AutoSize = False
    Caption = #30701#20449#19994#21153#31867#22411#65306
  end
  object Button1: TButton
    Left = 8
    Top = 392
    Width = 129
    Height = 25
    Caption = #29983#25104#35746#21333#30701#20449#20869#23481
    TabOrder = 0
    OnClick = Button1Click
  end
  object memoSms: TMemo
    Left = 288
    Top = 40
    Width = 465
    Height = 313
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Button4: TButton
    Left = 288
    Top = 392
    Width = 81
    Height = 25
    Caption = #21457#36865#32473#23458#25143
    TabOrder = 2
    OnClick = Button4Click
  end
  object Button6: TButton
    Left = 376
    Top = 392
    Width = 97
    Height = 25
    Caption = #21457#36865#32473#25910#36135#20154
    TabOrder = 3
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 640
    Top = 392
    Width = 113
    Height = 25
    Caption = #21457#36865#32473#23433#35013#20154
    TabOrder = 4
    OnClick = Button7Click
  end
  object editSendMobile: TEdit
    Left = 360
    Top = 360
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object Button8: TButton
    Left = 480
    Top = 360
    Width = 36
    Height = 22
    Caption = #21457#36865
    TabOrder = 6
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 680
    Top = 8
    Width = 75
    Height = 25
    Caption = #20851#38381
    TabOrder = 7
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 512
    Top = 8
    Width = 75
    Height = 25
    Caption = #28165#38500
    TabOrder = 8
    OnClick = Button10Click
  end
  object Button12: TButton
    Left = 480
    Top = 392
    Width = 153
    Height = 25
    Caption = #21457#36865#32473#23433#35013#26381#21153#21830
    TabOrder = 9
    OnClick = Button12Click
  end
  object cbSendSmsSrvType: TComboBox
    Left = 384
    Top = 10
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 10
    Text = #23450#21333#20869#23481
    Items.Strings = (
      #20419#38144#27963#21160
      #23450#21333#20869#23481
      #36890#30693#26381#21153#21830#23433#35013
      #23433#35013#27979#35797
      #24320#36890#36890#30693
      #25253#35686#36890#30693
      #30331#38470#39564#35777#23494#30721
      #21806#21518#26381#21153
      #32493#36153#25552#31034
      #20652#36153#36890#30693
      #20572#26426#36890#30693
      #20020#26102#21457#36865
      #33410#26085#31069#31119)
  end
  object ListView1: TListView
    Left = 8
    Top = 40
    Width = 273
    Height = 313
    Checkboxes = True
    Columns = <
      item
        Caption = #39033#30446
        Width = 110
      end
      item
        Caption = #20869#23481
        Width = 150
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 11
    ViewStyle = vsReport
  end
  object Button13: TButton
    Left = 144
    Top = 392
    Width = 137
    Height = 25
    Caption = #29983#25104#35746#21333#26126#32454#30701#20449
    TabOrder = 12
    OnClick = Button13Click
  end
  object ckPrice: TCheckBox
    Left = 8
    Top = 360
    Width = 177
    Height = 17
    Caption = #35746#21333#26126#32454#26159#21542#21253#21547#21333#20215#65311
    TabOrder = 13
  end
end
