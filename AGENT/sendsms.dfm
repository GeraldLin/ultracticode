object FormSendSms: TFormSendSms
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #30332#36865#25163#27231#31777#35338
  ClientHeight = 130
  ClientWidth = 556
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 17
    Width = 76
    Height = 14
    AutoSize = False
    Caption = #25163#27231#34399#30908#65306
  end
  object Label2: TLabel
    Left = 17
    Top = 52
    Width = 76
    Height = 14
    AutoSize = False
    Caption = #31777#35338#20839#23481#65306
  end
  object Label113: TLabel
    Left = 295
    Top = 21
    Width = 100
    Height = 14
    AutoSize = False
    Caption = #31777#35338#26989#21209#39006#22411#65306
  end
  object editMobileNo: TEdit
    Left = 95
    Top = 17
    Width = 131
    Height = 21
    MaxLength = 20
    TabOrder = 0
  end
  object editSms: TEdit
    Left = 95
    Top = 52
    Width = 435
    Height = 21
    MaxLength = 250
    TabOrder = 1
  end
  object Button1: TButton
    Left = 156
    Top = 87
    Width = 81
    Height = 27
    Caption = #30332#36865
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 338
    Top = 87
    Width = 81
    Height = 27
    Caption = #38364#38281
    TabOrder = 3
    OnClick = Button2Click
  end
  object cbSendSmsSrvType: TComboBox
    Left = 399
    Top = 17
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = #26410#27512#39006
    Items.Strings = (
      #26989#21209#23459#20659
      #35330#21934#26381#21209
      #21806#21518#26381#21209
      #38283#37782#26381#21209
      #20652#32371#26381#21209#36027
      #23433#35037#28204#35430
      #22577#35686#36890#30693
      #30331#37636#39511#35657#23494#30908
      #33256#26178#30332#36865)
  end
end
