object FormRecvSmsProc: TFormRecvSmsProc
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #34389#29702#25509#25910#31777#35338
  ClientHeight = 456
  ClientWidth = 512
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 14
    Top = 13
    Width = 70
    Height = 14
    AutoSize = False
    Caption = #25163#27231#34399#30908#65306
  end
  object Label2: TLabel
    Left = 14
    Top = 46
    Width = 70
    Height = 14
    AutoSize = False
    Caption = #31777#35338#20839#23481#65306
  end
  object Label3: TLabel
    Left = 291
    Top = 12
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #25509#25910#26178#38291#65306
  end
  object Label4: TLabel
    Left = 14
    Top = 178
    Width = 70
    Height = 14
    AutoSize = False
    Caption = #34389#29702#27161#35468#65306
  end
  object Label5: TLabel
    Left = 291
    Top = 178
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #34389#29702#26178#38291#65306
  end
  object Label6: TLabel
    Left = 14
    Top = 212
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #26989#21209#39006#22411#65306
  end
  object Label7: TLabel
    Left = 14
    Top = 246
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #34389#29702#20839#23481#65306
  end
  object Label8: TLabel
    Left = 38
    Top = 386
    Width = 39
    Height = 14
    AutoSize = False
    Caption = #20633#35387#65306
  end
  object Label9: TLabel
    Left = 303
    Top = 213
    Width = 55
    Height = 15
    AutoSize = False
    Caption = #34389#29702#20154#65306
  end
  object editMobile: TEdit
    Left = 87
    Top = 9
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editRecvTime: TEdit
    Left = 364
    Top = 9
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 1
  end
  object memoSmsMsg: TMemo
    Left = 87
    Top = 43
    Width = 408
    Height = 123
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object editReadTime: TEdit
    Left = 364
    Top = 173
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 3
    OnChange = cbReadFlagChange
  end
  object editRemark: TEdit
    Left = 87
    Top = 381
    Width = 408
    Height = 21
    TabOrder = 4
    OnChange = cbReadFlagChange
  end
  object memoProcSrvData: TMemo
    Left = 87
    Top = 243
    Width = 408
    Height = 131
    ScrollBars = ssBoth
    TabOrder = 5
    OnChange = cbReadFlagChange
  end
  object Button1: TButton
    Left = 121
    Top = 416
    Width = 82
    Height = 27
    Caption = #20445#23384
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 329
    Top = 416
    Width = 82
    Height = 27
    Caption = #21462#28040
    TabOrder = 7
    OnClick = Button2Click
  end
  object cbProcSrvType: TComboBox
    Left = 87
    Top = 208
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 8
    OnChange = cbReadFlagChange
    OnKeyPress = cbReadFlagKeyPress
  end
  object cbReadFlag: TComboBox
    Left = 87
    Top = 173
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 9
    OnChange = cbReadFlagChange
    OnKeyPress = cbReadFlagKeyPress
  end
  object editProcBy: TEdit
    Left = 364
    Top = 208
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 10
  end
end
