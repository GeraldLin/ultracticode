//---------------------------------------------------------------------------

#ifndef disprecvmsgH
#define disprecvmsgH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormRecvMsg : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editSeatNo;
  TEdit *editWorkerNo;
  TEdit *editWorkerName;
  TEdit *editMsg;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label5;
  TEdit *editSendMsg;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormRecvMsg(TComponent* Owner);

  void SetMsgData(AnsiString seatno, AnsiString workerno, AnsiString workername, AnsiString msg);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRecvMsg *FormRecvMsg;
//---------------------------------------------------------------------------
#endif
