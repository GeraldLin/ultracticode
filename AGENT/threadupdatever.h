//---------------------------------------------------------------------------

#ifndef threadupdateverH
#define threadupdateverH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class CThreadUpdateVer : public TThread
{            
private:
protected:
  void __fastcall Execute();
public:
  __fastcall CThreadUpdateVer(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
