object FormCallin1: TFormCallin1
  Left = 668
  Top = 146
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24392#20986#23458#25142#36039#26009
  ClientHeight = 682
  ClientWidth = 523
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 12
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 523
    Height = 192
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 4
      Width = 222
      Height = 35
      Caption = #20358#38651#32773#34399#30908#65306
      Color = clInactiveCaptionText
      Font.Charset = ANSI_CHARSET
      Font.Color = clTeal
      Font.Height = -35
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lblCallerNo: TLabel
      Left = 240
      Top = 4
      Width = 281
      Height = 35
      AutoSize = False
      Caption = '13307551234'
      Color = clAqua
      Font.Charset = ANSI_CHARSET
      Font.Color = clTeal
      Font.Height = -35
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label12: TLabel
      Left = 5
      Top = 81
      Width = 513
      Height = 36
      AutoSize = False
      Caption = #34399#30908#39006#21029#65306
      Color = clInactiveCaptionText
      Font.Charset = ANSI_CHARSET
      Font.Color = clTeal
      Font.Height = -35
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object lbTranPhone: TLabel
      Left = 5
      Top = 120
      Width = 513
      Height = 35
      AutoSize = False
      Caption = #36681#25509#30340#22352#24109#34399#65306
      Color = clInactiveCaptionText
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -35
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object lbSrvType: TLabel
      Left = 5
      Top = 158
      Width = 513
      Height = 27
      AutoSize = False
      Caption = #26989#21209#39006#22411#65306
      Color = clBlue
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -23
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label7: TLabel
      Left = 5
      Top = 43
      Width = 185
      Height = 35
      Caption = #25765#20837#34399#30908#65306
      Color = clInactiveCaptionText
      Font.Charset = ANSI_CHARSET
      Font.Color = clTeal
      Font.Height = -35
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lblCalledNo: TLabel
      Left = 240
      Top = 43
      Width = 281
      Height = 35
      AutoSize = False
      Caption = '13307551234'
      Color = clAqua
      Font.Charset = ANSI_CHARSET
      Font.Color = clTeal
      Font.Height = -35
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 604
    Width = 523
    Height = 78
    Align = alBottom
    TabOrder = 1
    object SpeedButton2: TSpeedButton
      Left = 347
      Top = 7
      Width = 70
      Height = 65
      Caption = #25298#32085
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -21
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      Glyph.Data = {
        36080000424D3608000000000000360400002800000020000000200000000100
        0800000000000004000000000000000000000001000000010000FFFFFF000000
        FF002020FF003030FF004040FF005050FF003838FF002828FF007070FF001818
        FF001010FF005858FF007878FF006060FF009090FF006868FF008080FF004848
        FF00B0B0FF00A0A0FF008888FF00C0C0FF00A8A8FF009898FF00D0D0FF00C8C8
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000191919191919
        1919191919191919191919191919191919191919191919191919191919191919
        1919191919190A0A0A0A090A0A19191919191919191919191919191919191919
        191919010A090902020207020202090919191919191919191919191919191919
        19010A0902070703030306030303070702091919191919191919191919191919
        0109020703060604040411040404060603070219191919191919191919191901
        090207030604110505050B050505110406030702191919191919191919190109
        0203060411050B0D0D0D0F0D0D0D0B0511040603021919191919191919010902
        030604050B0D0F0808080C0808080F0D0B0504060302191919191919010A0207
        0604050B0F080C0C10101410100C0C080F0B0504060702191919191901090703
        04050B0F080C10140E0E0E0E0E14100C080F0B0504030719191919010A020306
        110B0F0810140E171313131313170E1410080F0B110603021919190109070604
        050D080C140E1316121212121216130E140C080D050406071919190A09070611
        0B0F0C100E13161215151515151216130E100C0F0B1106071919010A02030405
        0D080C1417161215191818181915121617140C080D0504030219010A02030000
        0000000000000000000000000000000000000000000504030219010A02030000
        0000000000000000000000000000000000000000000504030219010907060000
        0000000000000000000000000000000000000000000B11060719010A02030000
        0000000000000000000000000000000000000000000504030219010A02030000
        0000000000000000000000000000000000000000000504030219010A02030405
        0D080C1417161215191818181915121617140C080D0504030219190A09070611
        0B0F0C100E13161215151515151216130E100C0F0B1106071919190109070604
        050D080C140E1316121212121216130E140C080D05040607191919010A020306
        110B0F0810140E171313131313170E1410080F0B110603021919191901090703
        04050B0F080C10140E0E0E0E0E14100C080F0B050403071919191919010A0207
        0604050B0F080C0C10101410100C0C080F0B0504060702191919191919010902
        030604050B0D0F0808080C0808080F0D0B050406030219191919191919190109
        0203060411050B0D0D0D0F0D0D0D0B0511040603021919191919191919191901
        090207030604110505050B050505110406030702191919191919191919191919
        0109020703060604040411040404060603070219191919191919191919191919
        19010A0902070703030306030303070702091919191919191919191919191919
        191919010A090902020207020202090919191919191919191919191919191919
        1919191919190A0A0A0A090A0A19191919191919191919191919}
      Layout = blGlyphTop
      ParentFont = False
      Visible = False
      OnClick = SpeedButton2Click
    end
    object Button1: TButton
      Left = 225
      Top = 9
      Width = 71
      Height = 65
      Caption = #30906#23450
      Default = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -20
      Font.Name = #27161#26999#39636
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 192
    Width = 523
    Height = 412
    Align = alClient
    TabOrder = 2
    object Label2: TLabel
      Left = 44
      Top = 13
      Width = 72
      Height = 12
      Caption = #20358#38651#32773#32232#34399#65306
    end
    object Label3: TLabel
      Left = 44
      Top = 39
      Width = 72
      Height = 12
      Caption = #20358#38651#32773#22995#21517#65306
    end
    object Label4: TLabel
      Left = 56
      Top = 64
      Width = 60
      Height = 12
      Caption = #20844#21496#21517#31281#65306
    end
    object Label5: TLabel
      Left = 283
      Top = 13
      Width = 72
      Height = 12
      Caption = #20358#38651#32773#36523#20221#65306
    end
    object Label6: TLabel
      Left = 319
      Top = 38
      Width = 36
      Height = 12
      Caption = #24615#21029#65306
    end
    object Label11: TLabel
      Left = 80
      Top = 90
      Width = 36
      Height = 12
      Caption = #22320#22336#65306
    end
    object Label14: TLabel
      Left = 80
      Top = 116
      Width = 36
      Height = 12
      Caption = #32291#24066#65306
    end
    object Label15: TLabel
      Left = 294
      Top = 117
      Width = 60
      Height = 12
      Caption = #37129#24066#37806#21312#65306
    end
    object lbAccountNo: TLabel
      Left = 294
      Top = 143
      Width = 60
      Height = 12
      Caption = #32113#19968#32232#34399#65306
    end
    object lbMobileNo: TLabel
      Left = 43
      Top = 143
      Width = 72
      Height = 12
      Caption = #20358#38651#32773#34399#30908#65306
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #26032#32048#26126#39636
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 32
      Top = 172
      Width = 84
      Height = 12
      Caption = #19978#27425#20358#38651#26178#38291#65306
    end
    object Label9: TLabel
      Left = 259
      Top = 173
      Width = 96
      Height = 12
      Caption = #19978#27425#26381#21209#20540#27231#21729#65306
    end
    object edtCustomNo: TEdit
      Left = 121
      Top = 9
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 0
    end
    object edtCustType: TEdit
      Left = 360
      Top = 9
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 1
    end
    object edtCustName: TEdit
      Left = 121
      Top = 35
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 2
    end
    object edtCustSex: TEdit
      Left = 360
      Top = 35
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 3
    end
    object edtCorpName: TEdit
      Left = 121
      Top = 61
      Width = 370
      Height = 20
      ReadOnly = True
      TabOrder = 4
    end
    object edtContAddr: TEdit
      Left = 121
      Top = 87
      Width = 370
      Height = 20
      ReadOnly = True
      TabOrder = 5
    end
    object edtProvince: TEdit
      Left = 121
      Top = 113
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 6
    end
    object edtCityName: TEdit
      Left = 360
      Top = 113
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 7
    end
    object edtAccountNo: TEdit
      Left = 360
      Top = 139
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 8
    end
    object edtMobileNo: TEdit
      Left = 121
      Top = 139
      Width = 131
      Height = 20
      TabOrder = 9
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 200
      Width = 505
      Height = 201
      DataSource = dmAgent.dsCurAccept
      TabOrder = 10
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #26032#32048#26126#39636
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ACDTime'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SrvName'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DispAcptCont'
          Width = 214
          Visible = True
        end>
    end
    object edtLastCallTime: TEdit
      Left = 121
      Top = 168
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 11
    end
    object edtLastWorkerName: TEdit
      Left = 360
      Top = 168
      Width = 131
      Height = 20
      ReadOnly = True
      TabOrder = 12
    end
  end
end
