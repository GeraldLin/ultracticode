//---------------------------------------------------------------------------

#ifndef editendcode1H
#define editendcode1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditEndCode1 : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editEndCodeName;
  TButton *Button1;
  TButton *Button2;
  TEdit *editEndCode1;
  TLabel *Label7;
  TCSpinEdit *cseDispOrder;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editEndCode1Change(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditEndCode1(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetDispMsg(bool bInsertId, int nEndCode1, AnsiString strItemName, int nDispOrder);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditEndCode1 *FormEditEndCode1;
//---------------------------------------------------------------------------
#endif
