//---------------------------------------------------------------------------

#ifndef workerH
#define workerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormWorker : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TEdit *edtWWorkerNo;
  TComboBox *cmbWWorkType;
  TCSpinEdit *cspWwLevel;
  TEdit *edtWPassword;
  TEdit *edtWWorkerName;
  TEdit *edtWTele;
  TEdit *edtWMobile;
  TEdit *edtWAddress;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label10;
  TEdit *edtWEmail;
  TComboBox *cmbWCallRight;
  TLabel *Label11;
  TComboBox *cbGroupNo;
  TLabel *Label12;
  TComboBox *cbGroupNo1;
  TLabel *Label13;
  TComboBox *cbGroupNo2;
  TLabel *Label14;
  TComboBox *cbGroupNo3;
  TLabel *Label15;
  TComboBox *cbGroupNo4;
  TLabel *Label16;
  TComboBox *cbDepartmentID;
  TLabel *Label17;
  TEdit *edtSubPhone;
  TLabel *Label18;
  TEdit *edtEmployeeID;
  TCheckBox *CheckBox1;
  TCheckBox *CheckBox2;
  TCheckBox *CheckBox3;
  TCheckBox *CheckBox4;
  TLabel *Label19;
  TComboBox *cbGroupNo5;
  TLabel *Label20;
  TComboBox *cbGroupNo6;
  TLabel *Label21;
  TComboBox *cbGroupNo7;
  TLabel *Label22;
  TComboBox *cbGroupNo8;
  TLabel *Label23;
  TComboBox *cbGroupNo9;
  TLabel *Label24;
  TComboBox *cbGroupNo10;
  TLabel *Label25;
  TComboBox *cbGroupNo11;
  TLabel *Label26;
  TComboBox *cbGroupNo12;
  TLabel *Label27;
  TComboBox *cbGroupNo13;
  TLabel *Label28;
  TComboBox *cbGroupNo14;
  TLabel *Label29;
  TComboBox *cbGroupNo15;
  TLabel *Label30;
  TCSpinEdit *cspWwLevel1;
  TLabel *Label31;
  TCSpinEdit *cspWwLevel2;
  TLabel *Label32;
  TCSpinEdit *cspWwLevel3;
  TLabel *Label33;
  TCSpinEdit *cspWwLevel4;
  TLabel *Label34;
  TCSpinEdit *cspWwLevel5;
  TLabel *Label35;
  TCSpinEdit *cspWwLevel6;
  TLabel *Label36;
  TCSpinEdit *cspWwLevel7;
  TLabel *Label37;
  TCSpinEdit *cspWwLevel8;
  TLabel *Label38;
  TCSpinEdit *cspWwLevel9;
  TLabel *Label39;
  TCSpinEdit *cspWwLevel10;
  TLabel *Label40;
  TCSpinEdit *cspWwLevel11;
  TLabel *Label41;
  TCSpinEdit *cspWwLevel12;
  TLabel *Label42;
  TCSpinEdit *cspWwLevel13;
  TLabel *Label43;
  TCSpinEdit *cspWwLevel14;
  TLabel *Label44;
  TCSpinEdit *cspWwLevel15;
  TCheckBox *ckAutoAnswerId;
  TLabel *Label45;
  TComboBox *cbProcDialoutNextType;
  TLabel *Label46;
  TEdit *edtSWTGroupID;
  TLabel *Label47;
  TEdit *edtSWTGroupPWD;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall edtWWorkerNoChange(TObject *Sender);
  void __fastcall cmbWWorkTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormWorker(TComponent* Owner);

  bool InsertId;
  bool bPwdModify;
  int tbId;
  void SetWorker(CMyWorker *worker);
  int GetWorker(CMyWorker *worker);

  void SetAddEnable();
  void SetAllEnable();
  void SetMyEnable();
  void SetDispEnable();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormWorker *FormWorker;
//---------------------------------------------------------------------------
#endif
