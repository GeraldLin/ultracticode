//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "sendsms.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormSendSms *FormSendSms;
extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormSendSms::TFormSendSms(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormSendSms->cbSendSmsSrvType, dmAgent->SendSmsTypeItemList, "");
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormSendSms::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSendSms::Button1Click(TObject *Sender)
{
  if (FormMain->SendSms(editMobileNo->Text, editSms->Text, dmAgent->SendSmsTypeItemList.GetItemValue(cbSendSmsSrvType->Text), 1) == 0)
    this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSendSms::FormCreate(TObject *Sender)
{
  dmAgent->AddCmbItem(cbSendSmsSrvType, dmAgent->SendSmsTypeItemList, "");
}
//---------------------------------------------------------------------------

