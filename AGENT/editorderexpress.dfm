object FormEditOrderExpress: TFormEditOrderExpress
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24555#36882#21457#36135#24773#20917#30331#35760
  ClientHeight = 245
  ClientWidth = 465
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label166: TLabel
    Left = 23
    Top = 43
    Width = 70
    Height = 13
    Caption = #37197#36865#26041#24335#65306
  end
  object Label209: TLabel
    Left = 255
    Top = 44
    Width = 70
    Height = 13
    Caption = #29289#27969#21333#21495#65306
  end
  object Label210: TLabel
    Left = 23
    Top = 68
    Width = 70
    Height = 13
    Caption = #29289#27969#20844#21496#65306
  end
  object Label211: TLabel
    Left = 227
    Top = 133
    Width = 98
    Height = 13
    Caption = #29289#27969#20844#21496#30005#35805#65306
  end
  object Label212: TLabel
    Left = 9
    Top = 92
    Width = 84
    Height = 13
    Caption = #25910#36135#20154#22995#21517#65306
  end
  object Label213: TLabel
    Left = 9
    Top = 156
    Width = 84
    Height = 13
    Caption = #25910#36135#20154#22320#22336#65306
  end
  object Label214: TLabel
    Left = 9
    Top = 133
    Width = 84
    Height = 13
    Caption = #25910#36135#20154#37038#32534#65306
  end
  object Label215: TLabel
    Left = 241
    Top = 92
    Width = 84
    Height = 13
    Caption = #25910#36135#20154#30005#35805#65306
  end
  object Label218: TLabel
    Left = 51
    Top = 180
    Width = 42
    Height = 13
    Caption = #22791#27880#65306
  end
  object Label8: TLabel
    Left = 37
    Top = 12
    Width = 56
    Height = 13
    Caption = #35746#21333#21495#65306
  end
  object Label11: TLabel
    Left = 256
    Top = 12
    Width = 70
    Height = 13
    Caption = #35746#21333#29366#24577#65306
  end
  object cbOExpressType: TComboBox
    Left = 96
    Top = 40
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = #26410#30830#23450
    OnChange = cbOExpressTypeChange
    OnKeyPress = cbOExpressTypeKeyPress
  end
  object edtOExpressNo: TEdit
    Left = 328
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = cbOExpressTypeChange
  end
  object edtOExpressCorp: TEdit
    Left = 96
    Top = 64
    Width = 353
    Height = 21
    TabOrder = 2
    OnChange = cbOExpressTypeChange
  end
  object edtOExpressTele: TEdit
    Left = 328
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 3
    OnChange = cbOExpressTypeChange
  end
  object edtORecvName: TEdit
    Left = 96
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 4
    OnChange = cbOExpressTypeChange
  end
  object edtORecvAddr: TEdit
    Left = 96
    Top = 152
    Width = 353
    Height = 21
    TabOrder = 5
    OnChange = cbOExpressTypeChange
  end
  object edtORecvPost: TEdit
    Left = 96
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 6
    OnChange = cbOExpressTypeChange
  end
  object edtORecvTele: TEdit
    Left = 328
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 7
    OnChange = cbOExpressTypeChange
  end
  object edtORemark: TEdit
    Left = 96
    Top = 176
    Width = 353
    Height = 21
    TabOrder = 8
    OnChange = cbOExpressTypeChange
  end
  object edtOOrderId: TEdit
    Left = 96
    Top = 8
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 9
  end
  object Button1: TButton
    Left = 96
    Top = 208
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 10
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 304
    Top = 208
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 11
    OnClick = Button2Click
  end
  object cbOOrderState: TComboBox
    Left = 328
    Top = 6
    Width = 121
    Height = 21
    Color = clGrayText
    Enabled = False
    ItemHeight = 13
    TabOrder = 12
    Text = #26410#21457#36135
    OnChange = cbOExpressTypeChange
  end
end
