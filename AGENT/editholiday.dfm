object FormEditHoliday: TFormEditHoliday
  Left = 455
  Top = 195
  BorderStyle = bsSingle
  Caption = #22283#23450#20551#26399#35373#23450
  ClientHeight = 220
  ClientWidth = 311
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 72
    Top = 44
    Width = 51
    Height = 12
    Caption = #22283#23450#20551#26399':'
  end
  object Label2: TLabel
    Left = 72
    Top = 76
    Width = 51
    Height = 12
    Caption = #38283#22987#26085#26399':'
  end
  object Label3: TLabel
    Left = 72
    Top = 108
    Width = 51
    Height = 12
    Caption = #32080#26463#26085#26399':'
  end
  object Label4: TLabel
    Left = 72
    Top = 140
    Width = 51
    Height = 12
    Caption = #21855#29992#29376#24907':'
  end
  object Label5: TLabel
    Left = 72
    Top = 20
    Width = 51
    Height = 12
    Caption = #36890#19968#32232#30908':'
  end
  object editHolidayName: TEdit
    Left = 128
    Top = 40
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editHolidayNameChange
  end
  object dtpStartDay: TDateTimePicker
    Left = 128
    Top = 72
    Width = 121
    Height = 20
    CalAlignment = dtaLeft
    Date = 41871.5983604282
    Time = 41871.5983604282
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 1
    OnChange = editHolidayNameChange
  end
  object dtpEndDay: TDateTimePicker
    Left = 128
    Top = 104
    Width = 121
    Height = 20
    CalAlignment = dtaLeft
    Date = 41871.5984240856
    Time = 41871.5984240856
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 2
    OnChange = editHolidayNameChange
  end
  object cbOpenFlag: TComboBox
    Left = 128
    Top = 136
    Width = 121
    Height = 20
    ItemHeight = 12
    TabOrder = 3
    Text = #21855#29992
    OnChange = editHolidayNameChange
    OnKeyPress = cbOpenFlagKeyPress
    Items.Strings = (
      #21855#29992
      #20572#29992)
  end
  object Button1: TButton
    Left = 40
    Top = 176
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 184
    Top = 176
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 5
    OnClick = Button2Click
  end
  object editCTID: TEdit
    Left = 128
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 6
    OnChange = editHolidayNameChange
  end
end
