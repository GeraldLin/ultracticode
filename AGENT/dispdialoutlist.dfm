object FormDispDialOutList: TFormDispDialOutList
  Left = 376
  Top = 216
  BorderStyle = bsSingle
  Caption = #22806#25765#35352#37636
  ClientHeight = 553
  ClientWidth = 1032
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 1032
    Height = 361
    Align = alClient
    DataSource = dmAgent.dsDialOutList
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #26032#32048#26126#39636
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CalledNo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TalkWorkerNo'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CallTime'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AnsTime'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RelTime'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispCallResult'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispEndCode1'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispEndCode2'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SaveTime'
        Width = 120
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 361
    Width = 1032
    Height = 192
    Align = alBottom
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 1030
      Height = 148
      Align = alClient
      Caption = #35527#35441#35352#37636':'
      TabOrder = 0
      object Memo1: TMemo
        Left = 2
        Top = 14
        Width = 1026
        Height = 132
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
        OnChange = Memo1Change
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 149
      Width = 1030
      Height = 42
      Align = alBottom
      BevelOuter = bvLowered
      TabOrder = 1
      object Label239: TLabel
        Left = 9
        Top = 17
        Width = 60
        Height = 12
        Caption = #32317#35352#37636#25976#65306
      end
      object lblDLSTRecdcount: TLabel
        Left = 78
        Top = 17
        Width = 6
        Height = 12
        Caption = '0'
      end
      object lblDLSTPages: TLabel
        Left = 121
        Top = 17
        Width = 30
        Height = 12
        Caption = #20849'0'#38913
      end
      object lblDLSTLocaPage: TLabel
        Left = 182
        Top = 17
        Width = 30
        Height = 12
        Caption = #31532'0'#38913
      end
      object Label246: TLabel
        Left = 584
        Top = 14
        Width = 12
        Height = 12
        Caption = #38913
      end
      object Button1: TButton
        Left = 939
        Top = 9
        Width = 75
        Height = 25
        Caption = #38364#38281
        TabOrder = 0
        OnClick = Button1Click
      end
      object btnDLSTPrior: TButton
        Left = 251
        Top = 9
        Width = 82
        Height = 25
        Caption = #19978#19968#38913
        TabOrder = 1
        OnClick = btnDLSTPriorClick
      end
      object btnDLSTNext: TButton
        Left = 347
        Top = 9
        Width = 81
        Height = 25
        Caption = #19979#19968#38913
        TabOrder = 2
        OnClick = btnDLSTNextClick
      end
      object btnDLSTGo: TButton
        Left = 451
        Top = 9
        Width = 81
        Height = 25
        Caption = #36339#21040
        TabOrder = 3
        OnClick = btnDLSTGoClick
      end
      object cseDLSTLocaPage: TCSpinEdit
        Left = 535
        Top = 11
        Width = 45
        Height = 21
        MaxValue = 1
        TabOrder = 4
      end
      object Button2: TButton
        Left = 616
        Top = 9
        Width = 85
        Height = 25
        Caption = #20445#23384#35527#35441#35352#37636
        TabOrder = 5
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 720
        Top = 8
        Width = 75
        Height = 25
        Caption = #20572#27490#25918#38899
        TabOrder = 6
        OnClick = N2Click
      end
    end
  end
  object WindowsMediaPlayer1: TWindowsMediaPlayer
    Left = 424
    Top = 72
    Width = 245
    Height = 240
    TabOrder = 2
    Visible = False
    ControlData = {
      000300000800000000000500000000000000F03F030000000000050000000000
      0000000008000200000000000300010000000B00FFFF0300000000000B00FFFF
      08000200000000000300320000000B00000008000A000000660075006C006C00
      00000B0000000B0000000B00FFFF0B00FFFF0B00000008000200000000000800
      020000000000080002000000000008000200000000000B00000052190000CE18
      0000}
  end
  object PopupMenu1: TPopupMenu
    Left = 48
    Top = 48
    object N1: TMenuItem
      Caption = #25773#25918#37636#38899
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #20572#27490#25918#38899
      OnClick = N2Click
    end
  end
end
