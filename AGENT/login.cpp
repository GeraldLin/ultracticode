//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include <inifiles.hpp>
#include "login.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormLogin *FormLogin;
extern int g_nBandSeatNoType, g_nNewMsg;
extern bool DisturbId;
extern AnsiString strAppPath;
//---------------------------------------------------------------------
__fastcall TFormLogin::TFormLogin(TComponent* AOwner)
	: TForm(AOwner)
{
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormLogin->cbWrokerGroup, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormLogin->cbDutyNo, dmAgent->DutyNoItemList, "不選擇");
  }
}
//---------------------------------------------------------------------
void __fastcall TFormLogin::BitBtn1Click(TObject *Sender)
{
  char dispbuf[128];
  short clearid=0;
  short nGroupNo = dmAgent->WorkerGroupList.GetItemValue(cbWrokerGroup->Text);
  short nDutyNo = dmAgent->DutyNoItemList.GetItemValue(cbDutyNo->Text);
  if (nGroupNo < 0)
    nGroupNo = 0;
  if (ckClearId->Checked == true)
    clearid = 1;
  if (cbStateAfterLogin->Text == "值機")
  {
    FormMain->StatusBar1->Panels->Items[8]->Text = "非值機";
    FormMain->N9->Caption = "非值機";
    DisturbId = false;
    FormMain->WorkerLogin(editWorkerNo->Text, edtPassword->Text, nGroupNo, 0, clearid, nDutyNo, ckAutoLogin->Checked);
  }
  else
  {
    FormMain->StatusBar1->Panels->Items[8]->Text = "值機";
    FormMain->N9->Caption = "值機";
    DisturbId = true;
    FormMain->WorkerLogin(editWorkerNo->Text, edtPassword->Text, nGroupNo, 1, clearid, nDutyNo, ckAutoLogin->Checked);
  }
  TIniFile *IniKey    = new TIniFile ( strAppPath+"agent.ini" ) ;
  IniKey->WriteBool( "CONFIGURE", "NoReadyOnLogin", DisturbId == true ? 1 : 0);
  delete IniKey;

  this->Close();
  if (g_nNewMsg > 0)
  {
    sprintf(dispbuf, "您收到 %d 條新的內部訊息,請到 訊息管理 查看！", g_nNewMsg);
    MessageBox(NULL,dispbuf,"訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormLogin::BitBtn2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormLogin::FormActivate(TObject *Sender)
{
  if (editWorkerNo->Text == "0" || editWorkerNo->Text == "")
    editWorkerNo->SetFocus();
  else
    edtPassword->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormLogin::cbWrokerGroupKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormLogin::FormShow(TObject *Sender)
{
  TIniFile *IniKey    = new TIniFile ( strAppPath+"agent.ini" ) ;
  bool bNoReady = IniKey->ReadBool( "CONFIGURE", "NoReadyOnLogin", false);
  delete IniKey;
  if (bNoReady == true)
    cbStateAfterLogin->Text = "非值機";
  else
    cbStateAfterLogin->Text = "值機";
  dmAgent->AddGroupNameCmbItemByWorkerNo(FormLogin->cbWrokerGroup, editWorkerNo->Text);
}
//---------------------------------------------------------------------------

void __fastcall TFormLogin::editWorkerNoExit(TObject *Sender)
{
  dmAgent->AddGroupNameCmbItemByWorkerNo(FormLogin->cbWrokerGroup, editWorkerNo->Text);
}
//---------------------------------------------------------------------------

