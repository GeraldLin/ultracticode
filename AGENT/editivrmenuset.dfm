object FormIVRMenuSet: TFormIVRMenuSet
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20491#24615#21270'IVR'#35486#38899#35373#23450
  ClientHeight = 205
  ClientWidth = 312
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 79
    Top = 13
    Width = 72
    Height = 13
    Caption = #25765#20837#30340#34399#30908#65306
  end
  object Label2: TLabel
    Left = 43
    Top = 40
    Width = 108
    Height = 13
    Caption = #25773#25918#27489#36814#35422#30340#26041#24335#65306
  end
  object Label3: TLabel
    Left = 24
    Top = 66
    Width = 126
    Height = 13
    Caption = #25773#25918'IVR'#35486#38899#33756#21934#26041#24335#65306
  end
  object Label4: TLabel
    Left = 18
    Top = 91
    Width = 132
    Height = 13
    Caption = #30452#25509#36681#25509#30340#22352#24109#20998#27231#34399#65306
  end
  object Label5: TLabel
    Left = 41
    Top = 118
    Width = 108
    Height = 13
    Caption = #30452#25509#36681#25509#30340#20540#27231#21729#65306
  end
  object editCalledNo: TEdit
    Left = 165
    Top = 9
    Width = 131
    Height = 21
    TabOrder = 0
    OnChange = editCalledNoChange
  end
  object cbWellcomeId: TComboBox
    Left = 165
    Top = 35
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = #25773#22577#20491#24615#21270#27489#36814#35422
    OnChange = editCalledNoChange
    OnKeyPress = cbWellcomeIdKeyPress
    Items.Strings = (
      #19981#25773#22577
      #25773#22577#20491#24615#21270#27489#36814#35422)
  end
  object cbVocMenuId: TComboBox
    Left = 165
    Top = 61
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Text = #25773#22577#20491#24615#21270#35486#38899#33756#21934
    OnChange = editCalledNoChange
    OnKeyPress = cbWellcomeIdKeyPress
    Items.Strings = (
      #19981#25773#22577
      #25773#22577#20491#24615#21270#35486#38899#33756#21934)
  end
  object editSeatNo: TEdit
    Left = 165
    Top = 87
    Width = 131
    Height = 21
    TabOrder = 3
    OnChange = editCalledNoChange
  end
  object editWorkerNo: TComboBox
    Left = 165
    Top = 113
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    OnChange = editCalledNoChange
  end
  object Button1: TButton
    Left = 52
    Top = 156
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 191
    Top = 156
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 6
    OnClick = Button2Click
  end
end
