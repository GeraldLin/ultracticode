object FormWorkerGroup: TFormWorkerGroup
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#22519#27231#32068
  ClientHeight = 214
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 66
    Top = 21
    Width = 72
    Height = 12
    Caption = #20540#27231#32676#32068#34399#65306
  end
  object Label2: TLabel
    Left = 54
    Top = 48
    Width = 84
    Height = 12
    Caption = #20540#27231#32676#32068#21517#31281#65306
  end
  object Label3: TLabel
    Left = 7
    Top = 105
    Width = 131
    Height = 12
    Caption = 'ACW'#35441#24460#34389#29702#26178#38263'('#31186')'#65306
  end
  object Label4: TLabel
    Left = 66
    Top = 133
    Width = 72
    Height = 12
    Caption = #22806#25765#25235#21462#30908#65306
  end
  object Label16: TLabel
    Left = 78
    Top = 77
    Width = 60
    Height = 12
    Caption = #25152#22312#37096#38272#65306
  end
  object editGroupNo: TEdit
    Left = 144
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editGroupNoChange
  end
  object editGroupName: TEdit
    Left = 144
    Top = 44
    Width = 131
    Height = 20
    TabOrder = 1
    OnChange = editGroupNoChange
  end
  object Button1: TButton
    Left = 52
    Top = 168
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 182
    Top = 168
    Width = 81
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object cseACWTimer: TCSpinEdit
    Left = 144
    Top = 100
    Width = 131
    Height = 21
    MaxValue = 36000
    TabOrder = 4
    OnChange = editGroupNoChange
  end
  object editDialoutPreCode: TEdit
    Left = 144
    Top = 129
    Width = 131
    Height = 20
    TabOrder = 5
    OnChange = editGroupNoChange
  end
  object cbDepartmentID: TComboBox
    Left = 143
    Top = 72
    Width = 131
    Height = 20
    AutoDropDown = True
    ItemHeight = 12
    TabOrder = 6
    OnChange = editGroupNoChange
  end
end
