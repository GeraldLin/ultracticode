object FormCRMDBParam: TFormCRMDBParam
  Left = 192
  Top = 114
  Width = 282
  Height = 307
  Caption = #31532#19977#26041#36039#26009#24235#21443#25976#35373#23450
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object GroupBox2: TGroupBox
    Left = 17
    Top = 44
    Width = 243
    Height = 166
    Caption = #31532#19977#26041#36039#26009#24235#21443#25976#35373#23450#65306
    TabOrder = 0
    object Label15: TLabel
      Left = 9
      Top = 24
      Width = 72
      Height = 12
      Caption = #36039#26009#24235#39006#22411#65306
    end
    object Label37: TLabel
      Left = 9
      Top = 51
      Width = 96
      Height = 12
      Caption = #36039#26009#20282#26381#22120#21517#31281#65306
    end
    object Label16: TLabel
      Left = 9
      Top = 78
      Width = 72
      Height = 12
      Caption = #36039#26009#24235#21517#31281#65306
    end
    object Label35: TLabel
      Left = 9
      Top = 105
      Width = 60
      Height = 12
      Caption = #30331#37636#24115#34399#65306
    end
    object Label36: TLabel
      Left = 9
      Top = 132
      Width = 60
      Height = 12
      Caption = #30331#37636#23494#30908#65306
    end
    object cbxDBType: TComboBox
      Left = 126
      Top = 18
      Width = 108
      Height = 20
      ItemHeight = 12
      TabOrder = 0
      Text = 'SQLSERVER'
      OnChange = cbxDBTypeChange
      OnKeyPress = cbxDBTypeKeyPress
      Items.Strings = (
        'SQLSERVER'
        'ORACLE'
        'SYBASE'
        'MYSQL'
        'ACCESS'
        'ODBC'
        'NONE')
    end
    object edtServer: TEdit
      Left = 126
      Top = 46
      Width = 108
      Height = 20
      TabOrder = 1
      Text = '127.0.0.1'
      OnChange = edtServerChange
    end
    object edtDB: TEdit
      Left = 126
      Top = 73
      Width = 108
      Height = 20
      TabOrder = 2
      Text = 'callcenterdb'
      OnChange = edtServerChange
    end
    object edtUSER: TEdit
      Left = 126
      Top = 100
      Width = 108
      Height = 20
      TabOrder = 3
      Text = 'sa'
      OnChange = edtServerChange
    end
    object edtPSW: TEdit
      Left = 126
      Top = 127
      Width = 108
      Height = 20
      MaxLength = 50
      PasswordChar = '*'
      TabOrder = 4
      Text = 'sa'
      OnChange = edtServerChange
    end
  end
  object ckCRMDBParam: TCheckBox
    Left = 16
    Top = 16
    Width = 241
    Height = 17
    Caption = #36899#25509#31532#19977#26041#36039#26009#24235#26597#35338#23458#25142#36039#26009'?'
    TabOrder = 1
    OnClick = cbxDBTypeChange
  end
  object btSaveParam: TButton
    Left = 43
    Top = 229
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 2
    OnClick = btSaveParamClick
  end
  object btClose: TButton
    Left = 155
    Top = 229
    Width = 82
    Height = 27
    Caption = #38364#38281
    TabOrder = 3
    OnClick = btCloseClick
  end
end
