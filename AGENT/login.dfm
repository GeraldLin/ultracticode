object FormLogin: TFormLogin
  Left = 518
  Top = 265
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #22519#27231#21729#30331#37636
  ClientHeight = 275
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 110
    Top = 42
    Width = 36
    Height = 12
    Caption = #23494#30908#65306
  end
  object Label2: TLabel
    Left = 85
    Top = 13
    Width = 60
    Height = 12
    Caption = #20540#27231#32232#34399#65306
  end
  object Label3: TLabel
    Left = 75
    Top = 70
    Width = 72
    Height = 12
    Caption = #33256#26178#20540#27231#32068#65306
  end
  object Label4: TLabel
    Left = 9
    Top = 255
    Width = 351
    Height = 12
    Caption = #25552#37266#65306#19981#36984#25799#33256#26178#25216#33021#32068#26178#34920#31034#25353#35442#32232#34399#35373#23450#30340#20540#27231#32068#30331#37636#12290
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #32048#26126#39636
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label5: TLabel
    Left = 76
    Top = 134
    Width = 72
    Height = 12
    Caption = #30331#37636#24460#29376#24907#65306
  end
  object Label6: TLabel
    Left = 89
    Top = 105
    Width = 60
    Height = 12
    Caption = #29677#27425#36984#25799#65306
  end
  object edtPassword: TEdit
    Left = 156
    Top = 38
    Width = 131
    Height = 20
    PasswordChar = '*'
    TabOrder = 0
    Text = '123456'
  end
  object BitBtn1: TBitBtn
    Left = 87
    Top = 217
    Width = 81
    Height = 27
    Caption = #30331#37636
    Default = True
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 234
    Top = 217
    Width = 81
    Height = 27
    Cancel = True
    Caption = #25918#26820
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object cbWrokerGroup: TComboBox
    Left = 156
    Top = 66
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 1
    Text = #19981#36984#25799
    OnKeyPress = cbWrokerGroupKeyPress
  end
  object cbStateAfterLogin: TComboBox
    Left = 156
    Top = 130
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 4
    Text = #20540#27231
    OnKeyPress = cbWrokerGroupKeyPress
    Items.Strings = (
      #20540#27231
      #38750#20540#27231)
  end
  object ckClearId: TCheckBox
    Left = 95
    Top = 165
    Width = 244
    Height = 18
    Caption = #30331#37636#24460#28165#38500#20043#21069#36890#35441#32047#35336#36039#26009'?'
    TabOrder = 5
  end
  object cbDutyNo: TComboBox
    Left = 156
    Top = 101
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 6
    Text = #19981#36984#25799
    OnKeyPress = cbWrokerGroupKeyPress
  end
  object ckAutoLogin: TCheckBox
    Left = 95
    Top = 186
    Width = 129
    Height = 17
    Caption = #19979#27425#33258#21205#30331#37636'?'
    TabOrder = 7
  end
  object editWorkerNo: TEdit
    Left = 155
    Top = 8
    Width = 132
    Height = 20
    TabOrder = 8
    OnExit = editWorkerNoExit
  end
end
