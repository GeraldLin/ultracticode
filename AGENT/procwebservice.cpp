//---------------------------------------------------------------------------
#include <system.hpp>
#include "procwebservice.h"
#include "DataModule.h"
//---------------------------------------------------------------------------

extern void WriteErrprMsg(LPCTSTR lpszFormat, ...);

CWebServiceParam g_WebServiceParam;
CCustomerField g_CustomerField;

HINSTANCE									g_hAgentDll				= NULL;

//数据库模块导出函数
_FagentSetAgentConfig	*_agentSetAgentConfig	= NULL;	
_FagentCallWebService	*_agentCallWebService	= NULL;	
_FagentFreeMemory		  *_agentFreeMemory	= NULL;

//************************************************************************
//				Load dynamic link library <QuarkCallWebServiceAgent.dll>
//************************************************************************
bool Fun_LoadAgentDLL()
{
  char sDllFile[] = "QuarkCallWebServiceAgent.dll";
  
  g_hAgentDll = LoadLibrary(sDllFile);
  if (g_hAgentDll == NULL)
  {	
    return false;
  }
  
  _agentSetAgentConfig = (_FagentSetAgentConfig *)GetProcAddress(g_hAgentDll, "agentSetAgentConfig");
  if (_agentSetAgentConfig == NULL)
  {
    goto ERR_DOCDB_FLAG;
  }
  
  _agentCallWebService = (_FagentCallWebService *)GetProcAddress(g_hAgentDll, "agentCallWebService");
  if (_agentCallWebService == NULL) 
  {
    goto ERR_DOCDB_FLAG;
  } 
  
  _agentFreeMemory = (_FagentFreeMemory *)GetProcAddress(g_hAgentDll, "agentFreeMemory");
  if (_agentFreeMemory == NULL) 
  {
    goto ERR_DOCDB_FLAG;
  }

  //if (g_bSaveId == true)
  {
    _agentSetAgentConfig(30, true);
  }

  return true;
  
ERR_DOCDB_FLAG:
  Fun_FreeAgentDLL();
  
  return false;
}

//************************************************************************
//				Free dynamic link library <QuarkCallWebServiceAgent.dll>
//************************************************************************
bool Fun_FreeAgentDLL()
{
  if (g_hAgentDll != NULL)
  {
    FreeLibrary(g_hAgentDll);
    
    g_hAgentDll					= NULL;
    _agentSetAgentConfig		= NULL;
    _agentCallWebService		= NULL;
  }
  return true;
}

int SplitString(const char *pzSource, char cSplitChar, TStringList *strList)
{
  char fieldvalue[128];
  short n,j=0,k=0, len;
  char ch;

  strList->Clear();
  len = strlen(pzSource);
  memset(fieldvalue, 0, 128);
  for (n = 0; n < len+1 && n < 512; n ++)
  {
    ch = pzSource[n];
    if (ch == cSplitChar || ch == '\0')
    {
      strList->Add((char *)fieldvalue);
      j ++;
      k = 0;
      memset(fieldvalue, 0, 128);
      if (j >= 50) //2015-05-29 bug
        break;
    }
    else
    {
      if (k < 128)
      {
        fieldvalue[k] = ch;
        k ++;
      }
    }
  }
  return j;
}

void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult)
{
  int i=0, nTemp, nLen, nOldLen, nNewLen, nCopyedLen=0;
  char *pszStart, *pszFinded;
  
  memset(pszResult, 0, nOutputMaxLen);

  nOldLen = strlen(pszOldStr);
  nNewLen = strlen(pszNewStr);
  if (nOldLen == 0)
  {
    memcpy(pszResult, pszSource, nOutputMaxLen-1);
  }
  else
  {
    pszStart = (char *)pszSource;
    
    do 
    {
      pszFinded = strstr(pszStart, pszOldStr);
      if (pszFinded)
      {
        //找到需要替换的字符串
        if (i < nStartIndex)
        {
          //没有到需要更换的位置索引，则直接拷贝该段字符
          nLen = (int)(pszFinded-pszStart)+nOldLen;
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strncat(pszResult, pszStart, nLen);
            pszStart = pszStart+nLen;
            nCopyedLen = nTemp;
          }
          else
          {
            //超过了限定长度则结束
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
            break;
          }
        }
        else
        {
          //到需要更换的位置索引，则拷贝该段前面不匹配的字符串，再追加新的替换字符
          nLen = (int)(pszFinded-pszStart);
          if (nLen > 0)
          {
            //拷贝前面不匹配的字符串
            nTemp = nCopyedLen + nLen;
            if (nTemp < nOutputMaxLen)
            {
              strncat(pszResult, pszStart, nLen);
              pszStart = pszStart+nLen+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszStart, nLen);
              break;
            }
            //追加新的替换字符
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
          else
          {
            //前面没有不匹配的字符串，直接追加新的替换字符
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              pszStart = pszStart+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
        }
        i++;
        if (i>=nReplaceMaxNum)
        {
          //已经替换了需要的数目
          nLen = strlen(pszStart);
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strcat(pszResult, pszStart);
            nCopyedLen = nTemp;
          }
          else
          {
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
          }
          break;
        }
      } 
      else
      {
        //未找到需要替换的字符串
        nLen = strlen(pszStart);
        nTemp = nCopyedLen + nLen;
        if (nTemp < nOutputMaxLen)
        {
          strcat(pszResult, pszStart);
          nCopyedLen = nTemp;
        }
        else
        {
          nLen = nOutputMaxLen-nCopyedLen-1;
          strncat(pszResult, pszStart, nLen);
        }
        break;
      }
    } while (true);
  }
}
void MyGetMarkerString(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, int nIndex, int nMaxLen, char *pszResult, bool &bFinded)
{
  int nLen, nStartMarkerLen, nEndMarkerLen;
  char *pszStart, *pszFinded;
  int i=0;
  
  memset(pszResult, 0, nMaxLen);
  bFinded = false;

  nStartMarkerLen = strlen(pszStartMarker);
  nEndMarkerLen = strlen(pszEndMarker);

  if (nStartMarkerLen == 0)
  {
    //规定无开始标记串，则起始为0开始
    if (nEndMarkerLen == 0)
    {
      //规定无开始标记串，又规定无结束标记串，则不需要处理，取原始字符串
      memcpy(pszResult, pszSource, nMaxLen-1);
      bFinded = true;
    } 
    else
    {
      //规定无开始标记串，规定有结束串标记
      pszFinded = strstr((char *)pszSource, pszEndMarker);
      if (pszFinded)
      {
        nLen = (int)(pszFinded - pszSource);
        if (nLen >= nMaxLen)
        {
          nLen = nMaxLen-1;
        }
        memcpy(pszResult, pszSource, nLen);
        bFinded = true;
      }
    }
  } 
  else
  {
    //规定有开始标记串
    pszStart=(char *)pszSource;
    do 
    {
      pszFinded = strstr(pszStart, pszStartMarker);
      if (pszFinded)
      {
        pszStart = pszFinded+nStartMarkerLen;
        if (i == nIndex)
        {
          pszFinded = strstr(pszStart, pszEndMarker);
          if (pszFinded)
          {
            nLen = (int)(pszFinded - pszStart);
            if (nLen >= nMaxLen)
            {
              nLen = nMaxLen-1;
            }
            memcpy(pszResult, pszStart, nLen);
            bFinded = true;
          }
          break;
        }
        i++;
      }
      else
      {
        break;
      }
    } while (true);
  }
}
char *MyGetCustomNo()
{
    static int c=0;
    static char Result[30];
    Word Year, Month, Day, Hour, Min, Sec, MSec;
    TDateTime dtPresent = Now();
    DecodeDate(dtPresent, Year, Month, Day);
    DecodeTime(dtPresent, Hour, Min, Sec, MSec);
    c++;
    sprintf(Result, "%04d%02d%02d%02d%02d%02d%03d9%03d", Year, Month, Day, Hour, Min, Sec, MSec, c);
    if (c >= 999)
      c = 0;
    return Result;
}
char *MyGetGUID()
{
  static char szGUID[64];
  
  memset(szGUID, 0, 64);
  GUID guid = GUID_NULL;
  ::CoCreateGuid(&guid);
  if (guid == GUID_NULL)
    return "";
  sprintf(szGUID, "%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
    guid.Data1, guid.Data2, guid.Data3,
    guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
    guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);
  return szGUID;
}

void ReadCustomerFieldSetup(const char *pszINIFileName)
{
  int num1,num2;
  char szTemp[1024], szSection[64];
  TStringList *ArrString1, *ArrString2;

  ArrString1 = new TStringList;
  ArrString2 = new TStringList;

  GetPrivateProfileString("WEBSERVICEPARAM", "wsdladdress", "", g_WebServiceParam.WS_wsdladdress, 255, pszINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "soapaction", "", g_WebServiceParam.WS_soapaction, 255, pszINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "startenvelope", "", g_WebServiceParam.WS_startenvelope, 255, pszINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "startbody", "", g_WebServiceParam.WS_startbody, 255, pszINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "interface", "", g_WebServiceParam.WS_interface, 255, pszINIFileName);
  g_WebServiceParam.WS_paramcount = GetPrivateProfileInt("WEBSERVICEPARAM", "paramcount", 0, pszINIFileName);
  for (int i=0; i<g_WebServiceParam.WS_paramcount; i++)
  {
    sprintf(szTemp, "paramstring%d", i+1);
    GetPrivateProfileString("WEBSERVICEPARAM", szTemp, "", g_WebServiceParam.WS_paramstring[i], 255, pszINIFileName);
  }

  GetPrivateProfileString("QUERYCUSTOMER", "RESULT", "", szTemp, 255, pszINIFileName);
  if (strlen(szTemp) > 0)
  {
    num1 = SplitString(szTemp, '`', ArrString1);
    if (num1 >= 3)
    {
      strncpy(g_CustomerField.CF_RESULT_STARTTAG, ArrString1->Strings[0].c_str(), 127);
      strncpy(g_CustomerField.CF_RESULT_ENDTAG, ArrString1->Strings[2].c_str(), 127);
      num2 = SplitString(ArrString1->Strings[1].c_str(), '|', ArrString2);
      if (num2 >= 2)
      {
        strncpy(g_CustomerField.CF_RESULT_OK, ArrString2->Strings[0].c_str(), 127);
        strncpy(g_CustomerField.CF_RESULT_FAIL, ArrString2->Strings[1].c_str(), 127);
        g_CustomerField.bResultTag = true;
      }
    }
  }
  GetPrivateProfileString("QUERYCUSTOMER", "RECORDNUM", "", szTemp, 255, pszINIFileName);
  if (strlen(szTemp) > 0)
  {
    num1 = SplitString(szTemp, '`', ArrString1);
    if (num1 >= 2)
    {
      strncpy(g_CustomerField.CF_RECORDNUM_STARTTAG, ArrString1->Strings[0].c_str(), 127);
      strncpy(g_CustomerField.CF_RECORDNUM_ENDTAG, ArrString1->Strings[1].c_str(), 127);
      g_CustomerField.bRecordNumTag = true;
    }
  }
  int j=0;
  g_CustomerField.FIELDNUM = GetPrivateProfileInt("QUERYCUSTOMER","FIELDNUM", 0, pszINIFileName);
  if (g_CustomerField.FIELDNUM > 0)
  {
    g_CustomerField.Init(g_CustomerField.FIELDNUM);
  }
  for (int i=0; i<g_CustomerField.FIELDNUM; i++)
  {
    sprintf(szSection, "FIELD[%d]", i);
    GetPrivateProfileString("QUERYCUSTOMER", szSection, "", szTemp, 255, pszINIFileName);
    if (strlen(szTemp) > 0)
    {
      num1 = SplitString(szTemp, '`', ArrString1);
      if (num1 >= 3)
      {
        strncpy(g_CustomerField.pFIELD[j].CF_FIELD_STARTTAG, ArrString1->Strings[0].c_str(), 127);
        strncpy(g_CustomerField.pFIELD[j].CF_FIELD_NAME, ArrString1->Strings[1].c_str(), 127);
        strncpy(g_CustomerField.pFIELD[j].CF_FIELD_ENDTAG, ArrString1->Strings[2].c_str(), 127);
        j++;
      }
    }
  }
  GetPrivateProfileString("QUERYCUSTOMER", "INSERTSQL", "", g_CustomerField.INSERTSQL, 2047, pszINIFileName);

  delete ArrString1;
  delete ArrString2;
}

int ProcQueryCostomerResult(const char *pXMLResult, char *pszClientIDList)
{
  bool bFinded, bSuccess=false;
  char szResult[256], szOldSql[4096], szNewSql[4096];
  int i, nRecordNum=0, nRet=0;

  if (g_CustomerField.bResultTag == true)
  {
    MyGetMarkerString(pXMLResult, g_CustomerField.CF_RESULT_STARTTAG, g_CustomerField.CF_RESULT_ENDTAG, 0, 256, szResult, bFinded);
    if (bFinded == true)
    {
      if (strcmp(szResult, g_CustomerField.CF_RESULT_OK) == 0)
      {
        bSuccess = true;
      }
    }
  }
  else
  {
    bSuccess = true;
  }
  if (bSuccess == false)
  {
    return nRet;
  }

  if (g_CustomerField.bRecordNumTag == true)
  {
    MyGetMarkerString(pXMLResult, g_CustomerField.CF_RECORDNUM_STARTTAG, g_CustomerField.CF_RECORDNUM_ENDTAG, 0, 256, szResult, bFinded);
    if (bFinded == true)
    {
      nRecordNum = atoi(szResult);
      if (nRecordNum > 128)
      {
        nRecordNum = 128;
      }
    }
  }
  else
  {
    nRecordNum = 128;
  }
  if (nRecordNum == 0)
    return nRet;
  int nIndex=0;
  
  while (nIndex < nRecordNum)
  {
    strcpy(szOldSql, g_CustomerField.INSERTSQL);
    MyStringReplace(szOldSql, "$S_CustomNo", MyGetCustomNo(), 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    MyStringReplace(szOldSql, "$S_GUID", MyGetGUID(), 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    sprintf(szResult, "%d", nIndex);
    MyStringReplace(szOldSql, "$S_RecordNo", szResult, 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);

    for (i=0; i<g_CustomerField.FIELDNUM; i++)
    {
      MyGetMarkerString(pXMLResult, g_CustomerField.pFIELD[i].CF_FIELD_STARTTAG, g_CustomerField.pFIELD[i].CF_FIELD_ENDTAG, nIndex, 256, szResult, bFinded);
      //MyTrace(1, "%s %s %s %s %s", g_CustomerField.pFIELD[i].CF_FIELD_STARTTAG, g_CustomerField.pFIELD[i].CF_FIELD_NAME, g_CustomerField.pFIELD[i].CF_FIELD_ENDTAG, szResult, bFinded==true ? "true" : "false");

      if (bFinded == true)
      {
        MyStringReplace(szOldSql, g_CustomerField.pFIELD[i].CF_FIELD_NAME, szResult, 0, 16, 4096, szNewSql);
        memcpy(szOldSql, szNewSql, 4096);
        if (nIndex == 0)
        {
          if (i == 1)
          {
            strcpy(pszClientIDList, "'");
            strcat(pszClientIDList, szResult);
            strcat(pszClientIDList, "'");
          }
        }
        else
        {
          if (i == 1)
          {
            strcat(pszClientIDList, ",'");
            strcat(pszClientIDList, szResult);
            strcat(pszClientIDList, "'");
          }
        }
      }
      else
      {
        MyStringReplace(szOldSql, g_CustomerField.pFIELD[i].CF_FIELD_NAME, "", 0, 16, 4096, szNewSql);
        memcpy(szOldSql, szNewSql, 4096);
      }
    }
    try
    {
      dmAgent->adoqueryPub->SQL->Clear();
      dmAgent->adoqueryPub->SQL->Add((char *)szNewSql );
      dmAgent->adoqueryPub->ExecSQL();
      WriteErrprMsg("%s success", szNewSql);
      nRet++;
    }
    catch ( ... )
    {
      WriteErrprMsg("%s fail", szNewSql);
    }
    nIndex++;
  }
  WriteErrprMsg("ClientIDList=%s nRet=%d", pszClientIDList, nRet);
  return nRet;
}

