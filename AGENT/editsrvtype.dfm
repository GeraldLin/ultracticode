object FormEditSrvType: TFormEditSrvType
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#26989#21209#39006#22411
  ClientHeight = 382
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 25
    Top = 20
    Width = 60
    Height = 12
    Caption = #26989#21209#20195#30908#65306
  end
  object Label2: TLabel
    Left = 303
    Top = 20
    Width = 60
    Height = 12
    Caption = #26989#21209#21517#31281#65306
  end
  object Label3: TLabel
    Left = 28
    Top = 85
    Width = 54
    Height = 12
    Caption = #26989#21209'URL'#65306
  end
  object Label4: TLabel
    Left = 8
    Top = 112
    Width = 54
    Height = 12
    Caption = #35441#34899#33139#26412':'
  end
  object Label7: TLabel
    Left = 266
    Top = 107
    Width = 96
    Height = 12
    Caption = #19979#25289#26694#39023#31034#38918#24207#65306
  end
  object Label8: TLabel
    Left = 5
    Top = 53
    Width = 78
    Height = 12
    Caption = 'URL'#25353#37397#21517#31281#65306
  end
  object editSrvName: TEdit
    Left = 368
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editSrvTypeChange
  end
  object Button1: TButton
    Left = 120
    Top = 336
    Width = 75
    Height = 25
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 320
    Top = 336
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editSrvType: TEdit
    Left = 90
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editSrvTypeChange
  end
  object editItemURL: TEdit
    Left = 90
    Top = 80
    Width = 399
    Height = 20
    TabOrder = 4
    OnChange = editSrvTypeChange
  end
  object memoDemo: TMemo
    Left = 8
    Top = 128
    Width = 481
    Height = 185
    MaxLength = 3500
    ScrollBars = ssBoth
    TabOrder = 5
    OnChange = editSrvTypeChange
  end
  object cseDispOrder: TCSpinEdit
    Left = 368
    Top = 102
    Width = 75
    Height = 21
    TabOrder = 6
    OnChange = editSrvTypeChange
  end
  object editURLBtnName: TEdit
    Left = 90
    Top = 48
    Width = 122
    Height = 20
    TabOrder = 7
    OnChange = editSrvTypeChange
  end
end
