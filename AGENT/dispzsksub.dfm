object FormDispZSKSub: TFormDispZSKSub
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #39023#31034#26989#21209#30693#35672#24235#23376#20998#39006#20449#24687
  ClientHeight = 258
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 13
    Top = 13
    Width = 86
    Height = 14
    AutoSize = False
    Caption = #20027#20998#39006#27161#38988#65306
  end
  object Label3: TLabel
    Left = 28
    Top = 81
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #35443#32048#35498#26126#65306
  end
  object Label11: TLabel
    Left = 59
    Top = 185
    Width = 44
    Height = 14
    AutoSize = False
    Caption = #20633#35387#65306
  end
  object Label8: TLabel
    Left = 14
    Top = 48
    Width = 87
    Height = 14
    AutoSize = False
    Caption = #23376#20998#39006#27161#38988#65306
  end
  object editMainTitle: TEdit
    Left = 104
    Top = 9
    Width = 325
    Height = 21
    Enabled = False
    TabOrder = 0
  end
  object editRemark: TEdit
    Left = 104
    Top = 182
    Width = 325
    Height = 21
    Enabled = False
    TabOrder = 1
  end
  object memoExplain: TMemo
    Left = 104
    Top = 78
    Width = 325
    Height = 96
    Enabled = False
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object Button2: TButton
    Left = 191
    Top = 217
    Width = 81
    Height = 27
    Caption = #38364#38281
    TabOrder = 3
    OnClick = Button2Click
  end
  object editSubTitle: TEdit
    Left = 104
    Top = 43
    Width = 325
    Height = 21
    Enabled = False
    TabOrder = 4
  end
end
