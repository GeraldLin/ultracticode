//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "acceptgroup.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormAcceptGroup *FormAcceptGroup;

extern int g_nBandSeatNoType;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
extern int MaxFaxNum;
//---------------------------------------------------------------------------
__fastcall TFormAcceptGroup::TFormAcceptGroup(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
  {
    FormAcceptGroup->cbGroupNo->Items->Add("不選擇");
    FormAcceptGroup->cbGroupNo->Items->Add("--功能選單----");
    FormAcceptGroup->cbGroupNo->Items->Add("留言");
    FormAcceptGroup->cbGroupNo->Items->Add("IVR提示語音");
    if (MaxFaxNum > 0)
    {
      FormAcceptGroup->cbGroupNo->Items->Add("接收傳真");
      FormAcceptGroup->cbGroupNo->Items->Add("索取傳真");
    }
    FormAcceptGroup->cbGroupNo->Items->Add("定制IVR子選單");
    FormAcceptGroup->cbGroupNo->Items->Add("外轉外");
    FormAcceptGroup->cbGroupNo->Items->Add("--群組--");

    dmAgent->AddCmbItem(FormAcceptGroup->cbGroupNo, dmAgent->WorkerGroupList, "", false);
    FormAcceptGroup->cbGroupNo->Items->Add("--值機員------");
    dmAgent->AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo, false);

    dmAgent->AddCmbItem(FormAcceptGroup->cbGroupNo1, dmAgent->WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo1->Items->Add("--值機員------");
    dmAgent->AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo1, false);

    dmAgent->AddCmbItem(FormAcceptGroup->cbGroupNo2, dmAgent->WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo2->Items->Add("--值機員------");
    dmAgent->AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo2, false);

    dmAgent->AddCmbItem(FormAcceptGroup->cbGroupNo3, dmAgent->WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo3->Items->Add("--值機員------");
    dmAgent->AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo3, false);

    dmAgent->AddCmbItem(FormAcceptGroup->cbGroupNo4, dmAgent->WorkerGroupList, "不選擇");
    FormAcceptGroup->cbGroupNo4->Items->Add("--值機員------");
    dmAgent->AddWorkerNameCmbItem(FormAcceptGroup->cbGroupNo4, false);

    dmAgent->AddCmbItem(FormAcceptGroup->cbSrvType, dmAgent->ProdTypeItemList, "");

    dmAgent->AddCmbItem(FormAcceptGroup->cbCustType, dmAgent->CustTypeItemList, "");
    dmAgent->AddCmbItem(FormAcceptGroup->cbCustLevel, dmAgent->CustLevelItemList, "");
  }
}

//---------------------------------------------------------------------------
void TFormAcceptGroup::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "增加";
    FormAcceptGroup->Caption = "增加ACD派線方式";
  }
  else
  {
    Button1->Caption = "修改";
    FormAcceptGroup->Caption = "修改ACD派線方式";
  }
}
void TFormAcceptGroup::SetAcceptGroupData(CAcceptGroup& acceptgroup)
{
  AcceptGroup = acceptgroup;
  if (acceptgroup.CalledType == 0)
    cbCalledType->Text = "按撥入號碼";
  else
    cbCalledType->Text = "按來電號碼";
  editCalledNo->Text = acceptgroup.CalledNo;

  if (acceptgroup.DTMFType == 1)
  {
    //客戶類型
    cbDTMFType->Text = "客戶類型";
    editDTMFKey->Text = "0";
    cbCustType->Text = dmAgent->CustTypeItemList.GetItemName(StrToInt(acceptgroup.DTMFKey));
    cbCustLevel->Text = dmAgent->CustTypeItemList.GetItemName(0);
    Label2->Visible = false;
    editDTMFKey->Visible = false;
    Label14->Visible = true;
    cbCustType->Visible = true;
    Label15->Visible = false;
    cbCustLevel->Visible = false;
  }
  else if (acceptgroup.DTMFType == 2)
  {
    //客戶級別
    cbDTMFType->Text = "客戶級別";
    editDTMFKey->Text = "0";
    cbCustType->Text = dmAgent->CustTypeItemList.GetItemName(0);
    cbCustLevel->Text = dmAgent->CustTypeItemList.GetItemName(StrToInt(acceptgroup.DTMFKey));
    Label2->Visible = false;
    editDTMFKey->Visible = false;
    Label14->Visible = false;
    cbCustType->Visible = false;
    Label15->Visible = true;
    cbCustLevel->Visible = true;
  }
  else if (acceptgroup.DTMFType == 3)
  {
    //外線迴路
    cbDTMFType->Text = "外線迴路";
    editDTMFKey->Text = acceptgroup.DTMFKey;
    cbCustType->Text = dmAgent->CustTypeItemList.GetItemName(0);
    cbCustLevel->Text = dmAgent->CustTypeItemList.GetItemName(0);
    Label2->Visible = true;
    Label2->Caption = "外線迴路:";
    editDTMFKey->Visible = true;
    Label14->Visible = false;
    cbCustType->Visible = false;
    Label15->Visible = false;
    cbCustLevel->Visible = false;
  }
  else
  {
    //菜單按鍵
    cbDTMFType->Text = "IVR按鍵";
    editDTMFKey->Text = acceptgroup.DTMFKey;
    cbCustType->Text = dmAgent->CustTypeItemList.GetItemName(0);
    cbCustLevel->Text = dmAgent->CustTypeItemList.GetItemName(0);
    Label2->Visible = true;
    Label2->Caption = "IVR選單按鍵：";
    editDTMFKey->Visible = true;
    Label14->Visible = false;
    cbCustType->Visible = false;
    Label15->Visible = false;
    cbCustLevel->Visible = false;
  }

  if (acceptgroup.VocId == 0)
  {
    cbVocId->Text = "不播報語音直接轉值機員";
  }
  else if (acceptgroup.VocId == 1)
  {
    cbVocId->Text = "按系統設定參數播報語音";
  }
  else if (acceptgroup.VocId == 2)
  {
    cbVocId->Text = "播報個性化公司語音";
  }
  else if (acceptgroup.VocId == 3)
  {
    cbVocId->Text = "播報個性化公司及語音選單";
  }
  else if (acceptgroup.VocId == 4)
  {
    cbVocId->Text = "播報總機語音選單";
  }
  editCorpVoc->Text = acceptgroup.CorpVoc;
  editMenuVoc->Text = acceptgroup.MenuVoc;

  if (acceptgroup.WorkerNoId == 0)
    cbWorkerNoId->Text = "不播報值機員編號";
  else
    cbWorkerNoId->Text = "按系統參數設定";

  if (acceptgroup.GroupNo1 == 0)
    cbGroupNo->Text = "不選擇";
  else if (acceptgroup.GroupNo1 < 0)
    cbGroupNo->Text = acceptgroup.GroupNoTel1;
  else
    cbGroupNo->Text = dmAgent->GetGroupNameOrWorkerName(acceptgroup.GroupNo1);
  ckGroupNo1Type->Checked = acceptgroup.GroupNo1Type==0 ? false : true;

  if (acceptgroup.GroupNo2 == 0)
    cbGroupNo1->Text = "不選擇";
  else if (acceptgroup.GroupNo2 < 0)
    cbGroupNo1->Text = acceptgroup.GroupNoTel2;
  else
    cbGroupNo1->Text = dmAgent->GetGroupNameOrWorkerName(acceptgroup.GroupNo2);
  ckGroupNo2Type->Checked = acceptgroup.GroupNo2Type==0 ? false : true;

  if (acceptgroup.GroupNo3 == 0)
    cbGroupNo2->Text = "不選擇";
  else if (acceptgroup.GroupNo3 < 0)
    cbGroupNo2->Text = acceptgroup.GroupNoTel3;
  else
    cbGroupNo2->Text = dmAgent->GetGroupNameOrWorkerName(acceptgroup.GroupNo3);
  ckGroupNo3Type->Checked = acceptgroup.GroupNo3Type==0 ? false : true;

  if (acceptgroup.GroupNo4 == 0)
    cbGroupNo3->Text = "不選擇";
  else if (acceptgroup.GroupNo4 < 0)
    cbGroupNo3->Text = acceptgroup.GroupNoTel4;
  else
    cbGroupNo3->Text = dmAgent->GetGroupNameOrWorkerName(acceptgroup.GroupNo4);
  ckGroupNo4Type->Checked = acceptgroup.GroupNo4Type==0 ? false : true;

  if (acceptgroup.GroupNo5 == 0)
    cbGroupNo4->Text = "不選擇";
  else if (acceptgroup.GroupNo5 < 0)
    cbGroupNo4->Text = acceptgroup.GroupNoTel5;
  else
    cbGroupNo4->Text = dmAgent->GetGroupNameOrWorkerName(acceptgroup.GroupNo5);
  ckGroupNo5Type->Checked = acceptgroup.GroupNo5Type==0 ? false : true;

  editSrvName->Text = acceptgroup.SrvName;
  cbSrvType->Text = dmAgent->ProdTypeItemList.GetItemName(acceptgroup.SrvType);
  editMenuKeys->Text = AcceptGroup.MenuKeys;

  switch (acceptgroup.MenuDtmfACK)
  {
  case 0:
    cbMenuDtmfACK->Text = "不按鍵確認";
    break;
  case 1:
    cbMenuDtmfACK->Text = "按1鍵確認";
    break;
  case 2:
    cbMenuDtmfACK->Text = "按1鍵確認并儲存";
    break;
  default:
    cbMenuDtmfACK->Text = "不按鍵確認";
    break;
  }
  editDtmfACKVoc->Text = acceptgroup.DtmfACKVoc;

  Button1->Enabled = false;
}
int TFormAcceptGroup::GetAcceptGroupData()
{
  int GroupNoType;
  if (editCalledNo->Text == "")
  {
    MessageBox(NULL,"請輸入電話號碼(所有號碼請用*號)！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (cbCalledType->Text == "按撥入號碼")
    AcceptGroup.CalledType = 0;
  else
    AcceptGroup.CalledType = 1;
  AcceptGroup.CalledNo = editCalledNo->Text;

  if (cbDTMFType->Text == "IVR按鍵")
  {
    AcceptGroup.DTMFType = 0;
    AcceptGroup.DTMFKey = editDTMFKey->Text;
  }
  else if (cbDTMFType->Text == "客戶類型")
  {
    AcceptGroup.DTMFType = 1;
    AcceptGroup.DTMFKey = dmAgent->CustTypeItemList.GetItemValue(cbCustType->Text);;
  }
  else if (cbDTMFType->Text == "客戶級別")
  {
    AcceptGroup.DTMFType = 2;
    AcceptGroup.DTMFKey = dmAgent->CustLevelItemList.GetItemValue(cbCustLevel->Text);;
  }
  else if (cbDTMFType->Text == "外線迴路")
  {
    AcceptGroup.DTMFType = 3;
    AcceptGroup.DTMFKey = editDTMFKey->Text;
  }
  else
  {
    AcceptGroup.DTMFType = 0;
    AcceptGroup.DTMFKey = editDTMFKey->Text;
  }

  if (cbVocId->Text == "不播報語音直接轉值機員")
    AcceptGroup.VocId = 0;
  else if (cbVocId->Text == "按系統設定參數播報語音")
    AcceptGroup.VocId = 1;
  else if (cbVocId->Text == "播報個性化公司語音")
    AcceptGroup.VocId = 2;
  else if (cbVocId->Text == "播報個性化公司及語音選單")
    AcceptGroup.VocId = 3;
  else if (cbVocId->Text == "播報總機語音選單")
    AcceptGroup.VocId = 4;

  AcceptGroup.CorpVoc = editCorpVoc->Text;
  AcceptGroup.MenuVoc = editMenuVoc->Text;
  if (cbWorkerNoId->Text == "不播報值機員編號")
    AcceptGroup.WorkerNoId = 0;
  else
    AcceptGroup.WorkerNoId = 1;


  AcceptGroup.GroupNo1 = dmAgent->GetGroupNoByName(cbGroupNo->Text, AcceptGroup.GroupNoTel1, GroupNoType);
  if (GroupNoType == 0)
    AcceptGroup.GroupNo1Type = 0;
  else
  {
    if (ckGroupNo1Type->Checked == true)
      AcceptGroup.GroupNo1Type = 1;
    else
      AcceptGroup.GroupNo1Type = 0;
  }

  AcceptGroup.GroupNo2 = dmAgent->GetGroupNoByName(cbGroupNo1->Text, AcceptGroup.GroupNoTel2, GroupNoType);
  if (GroupNoType == 0)
    AcceptGroup.GroupNo2Type = 0;
  else
  {
    if (ckGroupNo2Type->Checked == true)
      AcceptGroup.GroupNo2Type = 1;
    else
      AcceptGroup.GroupNo2Type = 0;
  }

  AcceptGroup.GroupNo3 = dmAgent->GetGroupNoByName(cbGroupNo2->Text, AcceptGroup.GroupNoTel3, GroupNoType);
  if (GroupNoType == 0)
    AcceptGroup.GroupNo3Type = 0;
  else
  {
    if (ckGroupNo3Type->Checked == true)
      AcceptGroup.GroupNo3Type = 1;
    else
      AcceptGroup.GroupNo3Type = 0;
  }

  AcceptGroup.GroupNo4 = dmAgent->GetGroupNoByName(cbGroupNo3->Text, AcceptGroup.GroupNoTel4, GroupNoType);
  if (GroupNoType == 0)
    AcceptGroup.GroupNo4Type = 0;
  else
  {
    if (ckGroupNo4Type->Checked == true)
      AcceptGroup.GroupNo4Type = 1;
    else
      AcceptGroup.GroupNo4Type = 0;
  }

  AcceptGroup.GroupNo5 = dmAgent->GetGroupNoByName(cbGroupNo4->Text, AcceptGroup.GroupNoTel5, GroupNoType);
  if (GroupNoType == 0)
    AcceptGroup.GroupNo5Type = 0;
  else
  {
    if (ckGroupNo5Type->Checked == true)
      AcceptGroup.GroupNo5Type = 1;
    else
      AcceptGroup.GroupNo5Type = 0;
  }

  AcceptGroup.SrvName = editSrvName->Text;
  AcceptGroup.SrvType = dmAgent->ProdTypeItemList.GetItemValue(cbSrvType->Text);
  if (AcceptGroup.SrvType < 0)
    AcceptGroup.SrvType = 0;
  AcceptGroup.MenuKeys = editMenuKeys->Text;

  if (cbMenuDtmfACK->Text == "按1鍵確認")
    AcceptGroup.MenuDtmfACK = 1;
  else if (cbMenuDtmfACK->Text == "按1鍵確認并儲存")
    AcceptGroup.MenuDtmfACK = 2;
  else
    AcceptGroup.MenuDtmfACK = 0;
  AcceptGroup.DtmfACKVoc = editDtmfACKVoc->Text;

  return 0;
}
void TFormAcceptGroup::ClearAcceptGroupData()
{
  cbCalledType->Text = "按撥入號碼";
  editCalledNo->Text = "";

  cbDTMFType->Text = "按鍵選單";
  editDTMFKey->Text = "0";
  cbCustType->Text = dmAgent->CustTypeItemList.GetItemName(0);
  cbCustLevel->Text = dmAgent->CustTypeItemList.GetItemName(0);
  Label2->Visible = true;
  Label2->Caption = "IVR選單按鍵：";
  editDTMFKey->Visible = true;
  Label14->Visible = false;
  cbCustType->Visible = false;
  Label15->Visible = false;
  cbCustLevel->Visible = false;

  cbVocId->Text = "按系統設定參數播報語音";
  editCorpVoc->Text = "";
  editMenuVoc->Text = "";
  cbWorkerNoId->Text = "不播報值機員編號";

  cbGroupNo->Text = "不選擇";
  cbGroupNo1->Text = "不選擇";
  cbGroupNo2->Text = "不選擇";
  cbGroupNo3->Text = "不選擇";
  cbGroupNo4->Text = "不選擇";
  cbSrvType->Text = dmAgent->ProdTypeItemList.GetItemName(1);
  cbMenuDtmfACK->Text = "不按鍵確認";
  editDtmfACKVoc->Text = "";
  editSrvName->Text = "";
}
void __fastcall TFormAcceptGroup::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormAcceptGroup::editCalledNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormAcceptGroup::Button1Click(TObject *Sender)
{
  if (GetAcceptGroupData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertAcceptGroupRecord(&AcceptGroup) == 0)
    {
      MessageBox(NULL,"增加ACD派線方式成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryAcceptGroup->Close();
      dmAgent->adoQryAcceptGroup->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加ACD派線方式失敗!","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateAcceptGroupRecord(&AcceptGroup) == 0)
    {
      MessageBox(NULL,"修改ACD派線方式成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryAcceptGroup->Close();
      dmAgent->adoQryAcceptGroup->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改ACD派線方式失敗!","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormAcceptGroup::cbGroupNoKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptGroup::FormShow(TObject *Sender)
{
  FormAcceptGroup->editCalledNo->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptGroup::cbCalledTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormAcceptGroup::cbDTMFTypeChange(TObject *Sender)
{
  Button1->Enabled = true;
  if (cbDTMFType->Text == "客戶類型")
  {
    //客戶類型
    Label2->Visible = false;
    editDTMFKey->Visible = false;
    Label14->Visible = true;
    cbCustType->Visible = true;
    Label15->Visible = false;
    cbCustLevel->Visible = false;
  }
  else if (cbDTMFType->Text == "客戶級別")
  {
    //客戶級別
    Label2->Visible = false;
    editDTMFKey->Visible = false;
    Label14->Visible = false;
    cbCustType->Visible = false;
    Label15->Visible = true;
    cbCustLevel->Visible = true;
  }
  else if (cbDTMFType->Text == "外線迴路")
  {
    //菜單按鍵
    Label2->Visible = true;
    Label2->Caption = "外線迴路:";
    editDTMFKey->Visible = true;
    Label14->Visible = false;
    cbCustType->Visible = false;
    Label15->Visible = false;
    cbCustLevel->Visible = false;
  }
  else
  {
    //菜單按鍵
    Label2->Visible = true;
    Label2->Caption = "IVR選單按鍵：";
    editDTMFKey->Visible = true;
    Label14->Visible = false;
    cbCustType->Visible = false;
    Label15->Visible = false;
    cbCustLevel->Visible = false;
  }
}
//---------------------------------------------------------------------------

