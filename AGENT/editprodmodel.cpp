//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editprodmodel.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditProdModel *FormEditProdModel;
//---------------------------------------------------------------------------
__fastcall TFormEditProdModel::TFormEditProdModel(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditProdModel::SetDispMsg(bool bInsertId, int nProdType, int nProdModelType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  InsertId = bInsertId;
  ProdType = nProdType;
  if (InsertId == true)
  {
    editItemId->Text = "";
    editItemId->Color = clWindow;
    editItemId->ReadOnly = false;
    editItemName->Text = "";
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = "";
    memoDemo->Lines->Clear();
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editItemId->Text = nProdModelType;
    editItemId->Color = clSilver;
    editItemId->ReadOnly = true;
    editItemName->Text = strItemName;
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = strItemURL;
    memoDemo->Lines->Clear();
    if (strDemo.Length() > 0)
      memoDemo->Lines->Add(strDemo);
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}
void __fastcall TFormEditProdModel::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditProdModel::editItemIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditProdModel::Button1Click(TObject *Sender)
{
  int nProdModelType;

  if (MyIsNumber(editItemId->Text) != 0)
  {
    MessageBox(NULL,"請輸入產品型號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,"請輸入產品型號名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nProdModelType = StrToInt(editItemId->Text);
  if (nProdModelType <= 0)
  {
    MessageBox(NULL,"對不起，產品型號必須大于0！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsProdModelTypeExist(ProdType, nProdModelType) == 1)
    {
      MessageBox(NULL,"對不起，該產品型號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertProdModelTypeRecord(ProdType, nProdModelType, editItemName->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryProdModel->Close();
      dmAgent->adoQryProdModel->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增產品型號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateProdModelTypeRecord(ProdType, nProdModelType, editItemName->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryProdModel->Close();
      dmAgent->adoQryProdModel->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改產品型號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditProdModel::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditProdModel->editItemId->SetFocus();
  else
    FormEditProdModel->editItemName->SetFocus();
}
//---------------------------------------------------------------------------
