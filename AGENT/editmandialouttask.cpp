//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>
#include "editmandialouttask.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditManDialOutTask *FormEditManDialOutTask;

extern CMyWorker MyWorker;
//---------------------------------------------------------------------------
__fastcall TFormEditManDialOutTask::TFormEditManDialOutTask(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void TFormEditManDialOutTask::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormEditManDialOutTask->Caption = "新增人工外撥任務";
  }
  else
  {
    Button1->Caption = "修改";
    FormEditManDialOutTask->Caption = "修改人工外撥任務";
  }
}
void TFormEditManDialOutTask::SetDialOutTaskData(CManDialOutTask& mandialouttask)
{
  ManDialOutTask = mandialouttask;
  editTaskCode->Text = ManDialOutTask.TaskCode;
  editTaskName->Text = ManDialOutTask.TaskName;
  cbStartId->Text = dmAgent->YesNoItemList.GetItemName(ManDialOutTask.StartId);
  dtpStartDate->Date = StrToDateTime(ManDialOutTask.StartDate);
  dtpEndDate->Date = StrToDateTime(ManDialOutTask.EndDate);
  editCallerNo->Text = ManDialOutTask.CallerNo;
  editParam->Text = ManDialOutTask.Param;
  editURLAddr->Text = ManDialOutTask.URLAddr;
  editURLOrder->Text = ManDialOutTask.URLOrder;
  editURLQuery->Text = ManDialOutTask.URLQuery;
  editURLManOrder->Text = ManDialOutTask.URLManOrder;
  editURLPrice->Text = ManDialOutTask.URLPrice;
  editDialPreCode->Text = ManDialOutTask.DialPreCode;
  memoRemark->Lines->Clear();
  memoRemark->Lines->Add(ManDialOutTask.Remark);

  Button1->Enabled = false;
}
int TFormEditManDialOutTask::GetDialOutTaskData()
{
  if (editTaskName->Text == "")
  {
    MessageBox(NULL,"請輸入外撥任務名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  ManDialOutTask.TaskCode = editTaskCode->Text;
  ManDialOutTask.TaskName = editTaskName->Text;
  if (editCallerNo->Text == "")
  {
    MessageBox(NULL,"請輸入主叫號碼！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (MyIsDigits(editCallerNo->Text) != 0)
  {
    MessageBox(NULL,"請輸入正確的主叫號碼！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  ManDialOutTask.CallerNo = editCallerNo->Text;
  ManDialOutTask.StartId = dmAgent->YesNoItemList.GetItemValue(cbStartId->Text);

  ManDialOutTask.StartDate = dtpStartDate->Date.FormatString("yyyy-mm-dd");
  ManDialOutTask.EndDate = dtpEndDate->Date.FormatString("yyyy-mm-dd");
  ManDialOutTask.CreateTime = Now().FormatString("yyyy-mm-dd hh:nn:ss");
  ManDialOutTask.CreateWorker = MyWorker.WorkerNo;
  ManDialOutTask.Param = editParam->Text;
  ManDialOutTask.URLAddr = editURLAddr->Text;
  ManDialOutTask.URLOrder = editURLOrder->Text;
  ManDialOutTask.URLQuery = editURLQuery->Text;
  ManDialOutTask.URLManOrder = editURLManOrder->Text;
  ManDialOutTask.URLPrice = editURLPrice->Text;
  ManDialOutTask.DialPreCode = editDialPreCode->Text;
  ManDialOutTask.Remark = memoRemark->Lines->Text;
  return 0;
}
void TFormEditManDialOutTask::ClearDialOutTaskData()
{
  static int nIndex=0;
  nIndex = (nIndex+1)%9+1;

  editTaskName->Text = "";
  editTaskCode->Text = Now().FormatString("yyyymmddhhnnss")+IntToStr(nIndex);
  editCallerNo->Text = "";
  cbStartId->Text = "是";

  dtpStartDate->Date = Date();
  dtpEndDate->Date = IncMonth(Date(), 6);
  editParam->Text = "";
  editURLAddr->Text = "";
  editURLOrder->Text = "";
  editURLQuery->Text = "";
  editURLManOrder->Text = "";
  editURLPrice->Text = "";
  editDialPreCode->Text = "";
  memoRemark->Lines->Clear();

  Button1->Enabled = false;
}

void __fastcall TFormEditManDialOutTask::cbStartIdKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditManDialOutTask::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditManDialOutTask::Button1Click(TObject *Sender)
{
  if (GetDialOutTaskData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertManDialOutTaskRecord(&ManDialOutTask) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1030, "值機員增加名為：%s的外呼任務資料", DialOutTask.TaskName.c_str());
      //MessageBox(NULL,"新增人工外撥任務成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryDialOutTask->Close();
      dmAgent->adoQryDialOutTask->Open();
      try
      {
        FormMain->QueryManDialOutTele(dmAgent->adoQryDialOutTask->FieldByName("TaskId")->AsInteger);
      }
      catch (...)
      {
      }
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增人工外撥任務資料失敗!","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateManDialOutTaskRecord(&ManDialOutTask) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員修改名為：%s的外呼任務資料", DialOutTask.TaskName.c_str());
      //MessageBox(NULL,"修改人工外撥任務資料成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryDialOutTask->Close();
      dmAgent->adoQryDialOutTask->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改人工外撥任務資料失敗!","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditManDialOutTask::editTaskNameChange(
      TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

