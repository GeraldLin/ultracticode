object FormEditCustExSet: TFormEditCustExSet
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20358#38651#32773#25844#20805#36039#26009#38917#30446#35373#32622
  ClientHeight = 376
  ClientWidth = 507
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 31
    Top = 22
    Width = 60
    Height = 12
    Caption = #38917#30446#24207#34399#65306
  end
  object Label2: TLabel
    Left = 31
    Top = 48
    Width = 60
    Height = 12
    Caption = #36664#20837#39006#22411#65306
  end
  object Label3: TLabel
    Left = 264
    Top = 22
    Width = 60
    Height = 12
    Caption = #38917#30446#21517#31281#65306
  end
  object Label4: TLabel
    Left = 43
    Top = 74
    Width = 48
    Height = 12
    Caption = #38928#35373#20540#65306
  end
  object Label5: TLabel
    Left = 244
    Top = 101
    Width = 48
    Height = 12
    Caption = #26368#22823#20540#65306
  end
  object Label6: TLabel
    Left = 27
    Top = 101
    Width = 48
    Height = 12
    Caption = #26368#23567#20540#65306
  end
  object Label7: TLabel
    Left = 31
    Top = 128
    Width = 60
    Height = 12
    Caption = #21295#20986#38918#24207#65306
  end
  object Label8: TLabel
    Left = 66
    Top = 174
    Width = 258
    Height = 12
    Caption = #20786#23384#26178#33258#21205'COPY'#19968#20221#21040#21463#29702#35352#32160#25844#23637#27396#20301#32232#34399#65306
  end
  object Label9: TLabel
    Left = 7
    Top = 152
    Width = 84
    Height = 12
    Caption = #36039#26009#35712#21462#20358#28304#65306
  end
  object Label10: TLabel
    Left = 238
    Top = 150
    Width = 86
    Height = 12
    Caption = 'CRM'#27396#20301#27161#31805#65306
  end
  object Label13: TLabel
    Left = 264
    Top = 47
    Width = 60
    Height = 12
    Caption = #20803#20214#23532#24230#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object editFieldId: TEdit
    Left = 95
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editFieldIdChange
  end
  object cbControlType: TComboBox
    Left = 95
    Top = 43
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 1
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
  end
  object editLabelCaption: TEdit
    Left = 329
    Top = 17
    Width = 131
    Height = 20
    MaxLength = 20
    TabOrder = 2
    OnChange = editFieldIdChange
  end
  object editDefaultData: TEdit
    Left = 95
    Top = 69
    Width = 365
    Height = 20
    MaxLength = 100
    TabOrder = 3
    OnChange = editFieldIdChange
  end
  object cseMinValue: TCSpinEdit
    Left = 95
    Top = 95
    Width = 131
    Height = 21
    TabOrder = 4
    OnChange = editFieldIdChange
  end
  object cseMaxValue: TCSpinEdit
    Left = 329
    Top = 95
    Width = 131
    Height = 21
    TabOrder = 5
    OnChange = editFieldIdChange
  end
  object ckIsAllowNull: TCheckBox
    Left = 329
    Top = 203
    Width = 123
    Height = 19
    Caption = #20801#35377#28858#31354#30333
    TabOrder = 6
    OnClick = editFieldIdChange
  end
  object ckDispId: TCheckBox
    Left = 95
    Top = 234
    Width = 114
    Height = 18
    Caption = #36039#26009#34920#20801#35377#39023#31034
    Checked = True
    State = cbChecked
    TabOrder = 7
    OnClick = editFieldIdChange
  end
  object ckInputId: TCheckBox
    Left = 329
    Top = 234
    Width = 157
    Height = 18
    Caption = #21463#29702#20171#38754#39023#31034
    Checked = True
    State = cbChecked
    TabOrder = 8
    OnClick = editFieldIdChange
  end
  object ckIsUsed: TCheckBox
    Left = 329
    Top = 284
    Width = 131
    Height = 18
    Caption = #21855#29992#38917#30446
    Checked = True
    State = cbChecked
    TabOrder = 9
    OnClick = editFieldIdChange
  end
  object ckSearchId: TCheckBox
    Left = 95
    Top = 260
    Width = 131
    Height = 18
    Caption = #26597#35426#26781#20214
    TabOrder = 10
    OnClick = editFieldIdChange
  end
  object Button1: TButton
    Left = 121
    Top = 321
    Width = 82
    Height = 27
    Caption = #30906#23450
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 303
    Top = 321
    Width = 82
    Height = 27
    Caption = #21462#28040
    TabOrder = 12
    OnClick = Button2Click
  end
  object ckExportId: TCheckBox
    Left = 95
    Top = 286
    Width = 131
    Height = 18
    Caption = #20801#35377#21295#20986
    TabOrder = 13
    OnClick = editFieldIdChange
  end
  object ckCanEdit: TCheckBox
    Left = 329
    Top = 258
    Width = 157
    Height = 18
    Caption = #20801#35377#20462#25913
    Checked = True
    State = cbChecked
    TabOrder = 14
    OnClick = editFieldIdChange
  end
  object cseExportOrderId: TCSpinEdit
    Left = 95
    Top = 121
    Width = 131
    Height = 21
    MaxValue = 128
    TabOrder = 15
    OnChange = editFieldIdChange
  end
  object cseAcceptExId: TCSpinEdit
    Left = 327
    Top = 169
    Width = 131
    Height = 21
    MaxValue = 128
    TabOrder = 16
    OnChange = editFieldIdChange
  end
  object cbReadDataType: TComboBox
    Left = 95
    Top = 147
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 17
    Text = 'DB'#36039#26009#24235
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
    Items.Strings = (
      'DB'#36039#26009#24235
      'HTTP'#20171#38754
      'WEBSERVICE'#20171#38754
      #21516#27493'CRM'#36039#26009)
  end
  object editReadFieldTag: TEdit
    Left = 329
    Top = 145
    Width = 131
    Height = 20
    MaxLength = 20
    TabOrder = 18
    OnChange = editFieldIdChange
  end
  object ckSaveToDBFlag: TCheckBox
    Left = 95
    Top = 202
    Width = 231
    Height = 18
    Caption = #26159#21542#23559'CRM'#27396#20301#36039#26009#20786#23384#21040#23458#25142#36039#26009#34920
    Checked = True
    State = cbChecked
    TabOrder = 19
    OnClick = editFieldIdChange
  end
  object cbWith: TComboBox
    Left = 328
    Top = 44
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 20
    Text = #27161#20934#23532#24230
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
    Items.Strings = (
      #27161#20934#23532#24230
      #27161#20934#23532#24230#22235#20998#20043#19968
      #27161#20934#23532#24230#20108#20998#20043#19968
      #27161#20934#23532#24230#22235#20998#20043#19977)
  end
end
