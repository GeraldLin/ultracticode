//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editdispitemsel.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditDispItemSel *FormEditDispItemSel;
//---------------------------------------------------------------------------
__fastcall TFormEditDispItemSel::TFormEditDispItemSel(TComponent* Owner)
  : TForm(Owner)
{
  nItemType = 0;
  nItemId = 0;
  nMustInput = 0;
}
//---------------------------------------------------------------------------
void TFormEditDispItemSel::SetCheckBoxVisible(int itemtype, int mustinput)
{
  nItemType = itemtype;
  nMustInput = mustinput;
  if (nItemType == 1)
  {
    //客戶基本資料項目設置：
    CheckBox1->Caption = "是否允許顯示？";
    CheckBox1->Visible = true;

    CheckBox2->Caption = "是否允許受理介面輸入？";
    CheckBox2->Visible = true;

    CheckBox3->Caption = "是否允許導出？";
    CheckBox3->Visible = true;

    CheckBox4->Caption = "";
    CheckBox4->Visible = false;

    Label2->Visible = true;
    cseExportOrderId->Visible = true;
    Label3->Visible = true;
    cseAcceptExId->Visible = true;

    Label9->Visible = true;
    cbReadDataType->Visible = true;
    Label10->Visible = true;
    editReadFieldTag->Visible = true;
    ckSaveToDBFlag->Visible = true;
  }
  else if (nItemType == 2)
  {
    //業務受理記錄顯示項目選擇：
    CheckBox1->Caption = "是否允許顯示？";
    CheckBox1->Visible = true;

    CheckBox2->Caption = "是否允許導出？";
    CheckBox2->Visible = true;

    CheckBox3->Caption = "";
    CheckBox3->Visible = false;

    CheckBox4->Caption = "";
    CheckBox4->Visible = false;

    Label2->Visible = true;
    cseExportOrderId->Visible = true;
    Label3->Visible = false;
    cseAcceptExId->Visible = false;

    Label9->Visible = false;
    cbReadDataType->Visible = false;
    Label10->Visible = false;
    editReadFieldTag->Visible = false;
    ckSaveToDBFlag->Visible = false;
  }
  else if (nItemType == 3)
  {
    //通話記錄顯示項目選擇：
    CheckBox1->Caption = "是否允許顯示？";
    CheckBox1->Visible = true;

    CheckBox2->Caption = "";
    CheckBox2->Visible = false;

    CheckBox3->Caption = "";
    CheckBox3->Visible = false;

    CheckBox4->Caption = "";
    CheckBox4->Visible = false;

    Label2->Visible = false;
    cseExportOrderId->Visible = false;
    Label3->Visible = false;
    cseAcceptExId->Visible = false;

    Label9->Visible = false;
    cbReadDataType->Visible = false;
    Label10->Visible = false;
    editReadFieldTag->Visible = false;
    ckSaveToDBFlag->Visible = false;
  }
}
void TFormEditDispItemSel::SetCheckBoxValue(AnsiString strItemName, int itemid, int nCkValue1, int nCkValue2, int nCkValue3, int nCkValue4, int nExportOrderId, int nAcceptExId)
{
  editItemName->Text = strItemName;
  nItemId = itemid;
  CheckBox1->Checked = (nCkValue1==1) ? true : false;
  CheckBox2->Checked = (nCkValue2==1) ? true : false;
  CheckBox3->Checked = (nCkValue3==1) ? true : false;
  CheckBox4->Checked = (nCkValue4==1) ? true : false;
  cseExportOrderId->Value = nExportOrderId;
  cseAcceptExId->Value = nAcceptExId;
  Button1->Enabled = false;
}

void __fastcall TFormEditDispItemSel::CheckBox1Click(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDispItemSel::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDispItemSel::Button1Click(TObject *Sender)
{
  int nResult=1;
  if (nItemType == 1)
  {
    //客戶基本資料項目設置：
    int nReadDataType, nSaveToDBFlag;
    AnsiString strReadFieldTag;

    if (cbReadDataType->Text == "DB資料庫")
      nReadDataType = 1;
    else if (cbReadDataType->Text == "HTTP介面")
      nReadDataType = 2;
    else if (cbReadDataType->Text == "WEBSERVICE介面")
      nReadDataType = 3;
    else if (cbReadDataType->Text == "同步CRM資料")
      nReadDataType = 4;
    else
      nReadDataType = 1;
    strReadFieldTag = editReadFieldTag->Text;
    if (ckSaveToDBFlag->Checked == true)
      nSaveToDBFlag = 1;
    else
      nSaveToDBFlag = 0;

    if (nMustInput == 1)
    {
      nResult = dmAgent->UpdateCustomSetDispItem(nItemId, true, true, true, cseExportOrderId->Value, cseAcceptExId->Value, nReadDataType, strReadFieldTag, nSaveToDBFlag);
    }
    else
    {
      nResult = dmAgent->UpdateCustomSetDispItem(nItemId, CheckBox1->Checked, CheckBox2->Checked, CheckBox3->Checked, cseExportOrderId->Value, cseAcceptExId->Value, nReadDataType, strReadFieldTag, nSaveToDBFlag);
    }
    dmAgent->adoQryCustomSet->Close();
    dmAgent->adoQryCustomSet->Open();
  }
  else if (nItemType == 2)
  {
    //業務受理記錄顯示項目選擇：
    nResult = dmAgent->UpdateAcceptSetDispItem(nItemId, CheckBox1->Checked, CheckBox2->Checked, cseExportOrderId->Value);
    dmAgent->adoQryAcceptSet->Close();
    dmAgent->adoQryAcceptSet->Open();
  }
  else if (nItemType == 3)
  {
    //通話記錄顯示項目選擇：
    nResult = dmAgent->UpdateCallCDRSetDispItem(nItemId, CheckBox1->Checked);
    dmAgent->adoQryCallCDRSet->Close();
    dmAgent->adoQryCallCDRSet->Open();
  }
  this->Close();
  if (nResult != 0)
    MessageBox(NULL,"參數修改失敗！","訊息提示",MB_OK|MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDispItemSel::FormShow(TObject *Sender)
{
  FormEditDispItemSel->CheckBox1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDispItemSel::cbReadDataTypeKeyPress(
      TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void TFormEditDispItemSel::SetCRMSetValue(int nReadDataType, AnsiString strReadFieldTag, int nSaveToDBFlag)
{
  if (nReadDataType == 1)
    cbReadDataType->Text = "DB資料庫";
  else if (nReadDataType == 2)
    cbReadDataType->Text = "HTTP介面";
  else if (nReadDataType == 3)
    cbReadDataType->Text = "WEBSERVICE介面";
  else if (nReadDataType == 4)
    cbReadDataType->Text = "同步CRM資料";
  else
    cbReadDataType->Text = "DB資料庫";
  editReadFieldTag->Text = strReadFieldTag;
  ckSaveToDBFlag->Checked = nSaveToDBFlag == 1 ? true : false;
  Button1->Enabled = false;
}

