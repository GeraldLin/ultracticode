//---------------------------------------------------------------------------

#ifndef seefaxfileH
#define seefaxfileH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "FAXEDITLib_OCX.h"
#include <OleCtrls.hpp>
//---------------------------------------------------------------------------
class TFormFaxSee : public TForm
{
__published:	// IDE-managed Components
  TFaxEdit *FaxEdit1;
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormFaxSee(TComponent* Owner);

  AnsiString strFaxFileName;
  void SetFaxFileName(AnsiString filename);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormFaxSee *FormFaxSee;
//---------------------------------------------------------------------------
#endif
