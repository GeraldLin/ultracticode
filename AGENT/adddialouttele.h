//---------------------------------------------------------------------------

#ifndef adddialoutteleH
#define adddialoutteleH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormAddDialOutTele : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editMobileNo;
  TEdit *editCustName;
  TEdit *editAccountNo;
  TEdit *editCustAddr;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label5;
  TEdit *editBatchId;
  TLabel *Label253;
  TComboBox *cbTaskId;
  TLabel *Label6;
  TEdit *editTeleNo;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editMobileNoChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAddDialOutTele(TComponent* Owner);

  int nTaskId;
  AnsiString strWorkerNo;
  void ClearAllData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAddDialOutTele *FormAddDialOutTele;
//---------------------------------------------------------------------------
#endif
