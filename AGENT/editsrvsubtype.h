//---------------------------------------------------------------------------

#ifndef editsrvsubtypeH
#define editsrvsubtypeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditSrvSubType : public TForm
{
__published:	// IDE-managed Components
  TLabel *lbItemId;
  TLabel *lbItemName;
  TEdit *editItemId;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label1;
  TEdit *editItemURL;
  TMemo *memoDemo;
  TLabel *Label2;
  TLabel *Label7;
  TCSpinEdit *cseDispOrder;
  TLabel *Label8;
  TEdit *editURLBtnName;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemIdChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSrvSubType(TComponent* Owner);

  int ProdType, SrvType;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetDispMsg(bool bInsertId, int nProdType, int nSrvType, int nSrvSubType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSrvSubType *FormEditSrvSubType;
//---------------------------------------------------------------------------
#endif
