object FormSearchDialoutCust: TFormSearchDialoutCust
  Left = 365
  Top = 221
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #23458#25142#36039#26009#39640#32026#26781#20214#36664#20837
  ClientHeight = 322
  ClientWidth = 981
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object sbDialOutSearch: TScrollBox
    Left = 0
    Top = 0
    Width = 981
    Height = 281
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 32
      Width = 119
      Height = 12
      Caption = '-----'#27169#31946#26597#35426#26781#20214'------:'
    end
    object Label2: TLabel
      Left = 8
      Top = 144
      Width = 123
      Height = 12
      Caption = '------'#31934#28310#26597#35426#26781#20214'------:'
      Visible = False
    end
    object Label255: TLabel
      Left = 286
      Top = 10
      Width = 60
      Height = 12
      Caption = #26371#21729#22995#21517#65306
    end
    object Label312: TLabel
      Left = 537
      Top = 10
      Width = 60
      Height = 12
      Caption = #26371#21729#24115#34399#65306
    end
    object Label294: TLabel
      Left = 37
      Top = 10
      Width = 60
      Height = 12
      Caption = #38651#35441#34399#30908#65306
    end
    object Label252: TLabel
      Left = 786
      Top = 13
      Width = 60
      Height = 12
      Caption = #22519#27231#32232#34399#65306
    end
    object editDialOutCustName: TEdit
      Left = 350
      Top = 6
      Width = 120
      Height = 20
      TabOrder = 0
    end
    object editDialOutAccountNo: TEdit
      Left = 600
      Top = 6
      Width = 120
      Height = 20
      TabOrder = 1
    end
    object editDialOutTeleNo: TEdit
      Left = 100
      Top = 6
      Width = 120
      Height = 20
      TabOrder = 2
    end
    object editDialOutWorkerNo: TEdit
      Left = 850
      Top = 9
      Width = 120
      Height = 20
      TabOrder = 3
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 281
    Width = 981
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object Button1: TButton
      Left = 320
      Top = 8
      Width = 75
      Height = 25
      Caption = #26597#35426
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 584
      Top = 8
      Width = 75
      Height = 25
      Caption = #21462#28040
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
