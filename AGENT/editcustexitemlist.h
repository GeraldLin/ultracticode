//---------------------------------------------------------------------------

#ifndef editcustexitemlistH
#define editcustexitemlistH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditCustExItemList : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editLabelCaption;
  TEdit *editItemNo;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditCustExItemList(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CCustExItemSet CustExItemSet;
  void SetModifyType(bool isinsert);
  void SetItemData(CCustExItemSet& custexitemset, AnsiString strItemNo, AnsiString strItemName);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditCustExItemList *FormEditCustExItemList;
//---------------------------------------------------------------------------
#endif
