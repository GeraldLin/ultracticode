//---------------------------------------------------------------------------

#ifndef acptitemsetH
#define acptitemsetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormAcptItemSet : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TEdit *editTid;
  TComboBox *cbControlType;
  TEdit *editLabelName;
  TEdit *editDefaultData;
  TCSpinEdit *cseTMinValue;
  TCSpinEdit *cseTMaxValue;
  TButton *Button1;
  TButton *Button2;
  TCheckBox *ckIsAllowNull;
  TCheckBox *ckExportId;
  TLabel *Label7;
  TEdit *editMaskEdit;
  TLabel *Label8;
  TEdit *editHintStr;
  TMemo *Memo1;
  TLabel *Label9;
  TComboBox *cbCombineMode;
  TLabel *Label10;
  TCSpinEdit *cseCombineTid;
  TLabel *Label11;
  TEdit *editCombineValue;
  TLabel *Label12;
  TCSpinEdit *cseExportOrderId;
  TLabel *Label13;
  TComboBox *cbWith;
  void __fastcall cbControlTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editTidChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbControlTypeChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAcptItemSet(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  int m_nProdType, m_nSrvType, m_nSrvSubType;
  CAcptItemSet AcptItemSet;
  void SetModifyType(bool isinsert);
  void SetAcptItemSetData(CAcptItemSet& acptitemset);
  int GetAcptItemSetData();
  void ClearAcptItemSetData();
  void SetSrvType(int prodtype, int srvtype, int srvsubtype);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAcptItemSet *FormAcptItemSet;
//---------------------------------------------------------------------------
#endif
