//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editcustlevel.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormCustLevel *FormCustLevel;
//---------------------------------------------------------------------------
__fastcall TFormCustLevel::TFormCustLevel(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormCustLevel::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormCustLevel::editCustLevelIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormCustLevel::Button1Click(TObject *Sender)
{
  int nCustLevelId;

  if (MyIsNumber(editCustLevelId->Text) != 0)
  {
    MessageBox(NULL,"請輸入客戶劃分編號！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editCustLevelName->Text == "")
  {
    MessageBox(NULL,"請輸入客戶劃分名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nCustLevelId = StrToInt(editCustLevelId->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsCustLevelExist(nCustLevelId) == 1)
    {
      MessageBox(NULL,"對不起，該客戶劃分編號已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertCustLevelRecord(nCustLevelId, editCustLevelName->Text) == 0)
    {
      dmAgent->adoQryCustLevel->Close();
      dmAgent->adoQryCustLevel->Open();
      dmAgent->UpdateCustLevelListItems();
      MessageBox(NULL,"增加客戶劃分名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加客戶劃分名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateCustLevelRecord(nCustLevelId, editCustLevelName->Text) == 0)
    {
      dmAgent->adoQryCustLevel->Close();
      dmAgent->adoQryCustLevel->Open();
      dmAgent->UpdateCustLevelListItems();
      MessageBox(NULL,"修改客戶劃分名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改客戶劃分名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
