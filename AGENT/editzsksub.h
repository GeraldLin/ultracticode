//---------------------------------------------------------------------------

#ifndef editzsksubH
#define editzsksubH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormZSKSub : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label11;
  TEdit *editMainTitle;
  TEdit *editRemark;
  TMemo *memoExplain;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label8;
  TEdit *editSubTitle;
  TLabel *Label1;
  TCSpinEdit *cseOrderNo;
  void __fastcall cbIssueStatusKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSubTitleChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormZSKSub(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CZSKSub ZSKSub;
  void SetModifyType(bool isinsert);
  void SetZSKSubData(CZSKSub& zsksub);
  int GetZSKSubData();
  void ClearZSKSubData(CZSKMain& zskmain);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormZSKSub *FormZSKSub;
//---------------------------------------------------------------------------
#endif
