//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editivrmenuset.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormIVRMenuSet *FormIVRMenuSet;

extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormIVRMenuSet::TFormIVRMenuSet(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
    dmAgent->AddWorkerNameCmbItem(FormIVRMenuSet->editWorkerNo);
}
//---------------------------------------------------------------------------
void __fastcall TFormIVRMenuSet::cbWellcomeIdKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormIVRMenuSet::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormIVRMenuSet::editCalledNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void TFormIVRMenuSet::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormIVRMenuSet->Caption = "新增個性化語音選單設置";
  }
  else
  {
    Button1->Caption = "修改";
    FormIVRMenuSet->Caption = "修改個性化語音選單設置";
  }
}
void TFormIVRMenuSet::SetIVRMenuSet(CIVRMenuSet &ivrmenuset)
{
  IVRMenuSet = ivrmenuset;

  editCalledNo->Text = IVRMenuSet.CalledNo;
  if (IVRMenuSet.WellcomeId == 0)
    cbWellcomeId->Text = "不播報";
  else
    cbWellcomeId->Text = "播報個性化歡迎詞";
  if (IVRMenuSet.VocMenuId == 0)
    cbVocMenuId->Text = "不播報";
  else
    cbVocMenuId->Text = "播報個性化語音選單";
  editSeatNo->Text = IVRMenuSet.DIDSeatNo;
  editWorkerNo->Text = dmAgent->GetWorkerName(IVRMenuSet.DIDWorkerNo);
  if (editWorkerNo->Text == "")
    editWorkerNo->Text = IVRMenuSet.DIDWorkerNo;

  Button1->Enabled = false;
}
int TFormIVRMenuSet::GetIVRMenuSet()
{
  if (editCalledNo->Text == "")
  {
    MessageBox(NULL,"請輸入撥入的號碼！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  IVRMenuSet.CalledNo = editCalledNo->Text;
  if (cbWellcomeId->Text == "不播報")
    IVRMenuSet.WellcomeId = 0;
  else
    IVRMenuSet.WellcomeId = 1;
  if (cbVocMenuId->Text == "不播報")
    IVRMenuSet.VocMenuId = 0;
  else
    IVRMenuSet.VocMenuId = 1;
  IVRMenuSet.DIDSeatNo = editSeatNo->Text;

  AnsiString strWorkerNo;

  if (dmAgent->GetWorkerNo(editWorkerNo->Text, strWorkerNo) == 1)
  {
    IVRMenuSet.DIDWorkerNo = strWorkerNo;
  }
  else
  {
    if (MyIsDigits(editWorkerNo->Text) == 0)
    {
      IVRMenuSet.DIDWorkerNo = editWorkerNo->Text;
    }
    else
    {
      MessageBox(NULL,"對不起，值機務員不存在！","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
  }
  return 0;
}
void TFormIVRMenuSet::ClearIVRMenuSet()
{
  editCalledNo->Text = "";
  cbWellcomeId->Text = "播報個性化歡迎詞";
  cbVocMenuId->Text = "播報個性化語音選單";
  editSeatNo->Text = "";
  editWorkerNo->Text = "";
}

void __fastcall TFormIVRMenuSet::Button1Click(TObject *Sender)
{
  if (GetIVRMenuSet() != 0)
  {
    return;
  }
  if (InsertId == true)
  {
    if (dmAgent->InsertIVRMenuSetRecord(&IVRMenuSet) == 0)
    {
      dmAgent->adoQryIVRSet->Close();
      dmAgent->adoQryIVRSet->Open();
      ClearIVRMenuSet();
      this->Close();
      //MessageBox(NULL,"新增個性化語音選單成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增個性化語音選單失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateIVRMenuSetRecord(&IVRMenuSet) == 0)
    {
      dmAgent->adoQryIVRSet->Close();
      dmAgent->adoQryIVRSet->Open();
      ClearIVRMenuSet();
      this->Close();
      //MessageBox(NULL,"個性化語音選單修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"個性化語音選單修改失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormIVRMenuSet::FormShow(TObject *Sender)
{
  FormIVRMenuSet->editCalledNo->SetFocus();
}
//---------------------------------------------------------------------------

