//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "viewfax.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImgeditLibCtl_OCX"
#pragma resource "*.dfm"
TFormViewFax *FormViewFax;

extern CMyWorker MyWorker;
extern AnsiString strAllocFaxURL;
extern int g_nAllocInitProcId;
//---------------------------------------------------------------------------
__fastcall TFormViewFax::TFormViewFax(TComponent* Owner)
  : TForm(Owner)
{
  m_nPageCount = 0;
}
//---------------------------------------------------------------------------
void TFormViewFax::SetFaxFileName(AnsiString filename)
{
  m_strFaxFileName = filename;
  m_nPageCount = 1;
  m_nCurPage = 1;
}
void __fastcall TFormViewFax::FormShow(TObject *Sender)
{
  //int nZoom1, nZoom2;
  WideString wFaxFile;
  BSTR bFaxFile;

  wFaxFile = WideString(m_strFaxFileName);
  bFaxFile = ( wchar_t * )wFaxFile;

  ImgEdit1->Image = bFaxFile;
  //nZoom1 = (ImgEdit1->Width*100)/ImgEdit1->ImageWidth;
  //nZoom2 = (ImgEdit1->Height*100)/ImgEdit1->ImageHeight;
  //if (nZoom1 < nZoom2)
  //  ImgEdit1->Zoom = nZoom1;
  //else
  //  ImgEdit1->Zoom = nZoom2;
  ImgEdit1->Page = 1;
  ImgEdit1->FitTo(1);
  m_nCurPage = 1;
  m_nPageCount = ImgEdit1->PageCount;
  ImgEdit1->Display();
  editFaxPage->Text = IntToStr(m_nCurPage)+"/"+IntToStr(ImgEdit1->PageCount);
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::sbPrevPageClick(TObject *Sender)
{
  if (m_nCurPage > 1 && m_nCurPage <= ImgEdit1->PageCount)
  {
    m_nCurPage --;
    ImgEdit1->Page = m_nCurPage;
    ImgEdit1->Display();
    editFaxPage->Text = IntToStr(m_nCurPage)+"/"+IntToStr(ImgEdit1->PageCount);
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::sbNextPageClick(TObject *Sender)
{
  if (m_nCurPage >= 1 && m_nCurPage < ImgEdit1->PageCount)
  {
    m_nCurPage ++;
    ImgEdit1->Page = m_nCurPage;
    ImgEdit1->Display();
    editFaxPage->Text = IntToStr(m_nCurPage)+"/"+IntToStr(ImgEdit1->PageCount);
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::sbZoomOutClick(TObject *Sender)
{
  if (ImgEdit1->Zoom < 200)
  {
    ImgEdit1->Zoom ++;
    ImgEdit1->Display();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::sbZoomInClick(TObject *Sender)
{
  if (ImgEdit1->Zoom > 5)
  {
    ImgEdit1->Zoom --;
    ImgEdit1->Display();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::spFitWidthClick(TObject *Sender)
{
  ImgEdit1->FitTo(1);
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::sbRotLeftClick(TObject *Sender)
{
  ImgEdit1->RotateLeft();
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::sbRotRightClick(TObject *Sender)
{
  ImgEdit1->RotateRight();
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::SpeedButton1Click(TObject *Sender)
{
  ImgEdit1->FitTo(3);
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::btCloseClick(TObject *Sender)
{
  ImgEdit1->ClearDisplay();
  DeleteFile(m_strFaxFileName);
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::FormResize(TObject *Sender)
{
  btClose->Left = Panel2->Width - 82;
  Panel3->Left = (Panel2->Width - Panel3->Width)/2;
}
//---------------------------------------------------------------------------
void __fastcall TFormViewFax::Button1Click(TObject *Sender)
{
  AnsiString strWorkerNo, strURLTemp;

  if (cbWorkerNo->Text == "")
  {
    MessageBox(NULL,"請選擇值機編號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  strWorkerNo = SplitStringFirstItem(cbWorkerNo->Text, "-");
  m_Accept.WorkerNo = strWorkerNo;

  //http://127.0.0.1:8080/?proctype=fax&id=$S_FaxSerialNo&workerno=$S_WorkerNo&AcceptID=$S_AcptGUID&procworkerno=$S_AdminWorkerNo
  strURLTemp = strAllocFaxURL;
  MyReplace(strURLTemp, "$S_FaxSerialNo", m_Accept.SerialNo, strURLTemp);
  MyReplace(strURLTemp, "$S_WorkerNo", strWorkerNo, strURLTemp);
  MyReplace(strURLTemp, "$S_AcptGUID", m_Accept.GId, strURLTemp);
  MyReplace(strURLTemp, "$S_AdminWorkerNo", MyWorker.WorkerNo, strURLTemp);

  WriteErrprMsg("%s", strURLTemp.c_str());
  try
  {
    if (FormMain->IdHTTP1->Get(strURLTemp) == "OK")
    {
      FormMain->IdHTTP1->DisconnectSocket();
      if (dmAgent->InsertAccept(m_Accept) == 0)
      {
        dmAgent->UpdateRecvFaxProcFlag(m_FaxId, g_nAllocInitProcId, strWorkerNo, "等待二線處理");
        MessageBox(NULL,"派發二線處理成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
        return;
      }
      MessageBox(NULL,"派發二線處理(新增受理工單)失敗!","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"派發二線處理(發email)失敗!","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
  }
  catch (...)
  {
    MessageBox(NULL,"派發二線處理(發email)失敗!","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------

