object FormSeatParam: TFormSeatParam
  Left = 488
  Top = 121
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #22352#24109#21443#25976#35373#23450
  ClientHeight = 479
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object GroupBox2: TGroupBox
    Left = 9
    Top = 263
    Width = 352
    Height = 166
    Caption = #24179#21488#36039#26009#24235#21443#25976#35373#23450#65306
    TabOrder = 0
    object Label15: TLabel
      Left = 9
      Top = 24
      Width = 72
      Height = 12
      Caption = #36039#26009#24235#39006#22411#65306
    end
    object Label37: TLabel
      Left = 9
      Top = 51
      Width = 82
      Height = 12
      Caption = #36039#26009#20282#26381#22120'IP'#65306
    end
    object Label16: TLabel
      Left = 9
      Top = 78
      Width = 72
      Height = 12
      Caption = #36039#26009#24235#21517#31281#65306
    end
    object Label35: TLabel
      Left = 9
      Top = 105
      Width = 60
      Height = 12
      Caption = #30331#37636#24115#34399#65306
    end
    object Label36: TLabel
      Left = 9
      Top = 132
      Width = 60
      Height = 12
      Caption = #30331#37636#23494#30908#65306
    end
    object cbxDBType: TComboBox
      Left = 102
      Top = 18
      Width = 240
      Height = 20
      ItemHeight = 12
      TabOrder = 0
      Text = 'SQLSERVER'
      OnChange = cbxDBTypeChange
      OnKeyPress = cbxDBTypeKeyPress
      Items.Strings = (
        'SQLSERVER'
        'ORACLE'
        'SYBASE'
        'MYSQL'
        'ACCESS'
        'ODBC'
        'NONE')
    end
    object edtServer: TEdit
      Left = 102
      Top = 46
      Width = 240
      Height = 20
      TabOrder = 1
      Text = '127.0.0.1'
      OnChange = cspServerPortChange
    end
    object edtDB: TEdit
      Left = 102
      Top = 73
      Width = 240
      Height = 20
      TabOrder = 2
      Text = 'callcenterdb'
      OnChange = cspServerPortChange
    end
    object edtUSER: TEdit
      Left = 102
      Top = 100
      Width = 240
      Height = 20
      TabOrder = 3
      Text = 'sa'
      OnChange = cspServerPortChange
    end
    object edtPSW: TEdit
      Left = 102
      Top = 127
      Width = 240
      Height = 20
      MaxLength = 50
      PasswordChar = '*'
      TabOrder = 4
      Text = 'sa'
      OnChange = cspServerPortChange
    end
  end
  object btSaveParam: TButton
    Left = 99
    Top = 437
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 1
    OnClick = btSaveParamClick
  end
  object btClose: TButton
    Left = 219
    Top = 437
    Width = 82
    Height = 27
    Caption = #38364#38281
    TabOrder = 2
    OnClick = btCloseClick
  end
  object GroupBox3: TGroupBox
    Left = 9
    Top = 9
    Width = 352
    Height = 216
    Caption = #20282#26381#22120#21443#25976#35373#23450#65306
    TabOrder = 3
    object Label6: TLabel
      Left = 65
      Top = 76
      Width = 49
      Height = 12
      Caption = #20282#26381#22120'IP:'
    end
    object Label7: TLabel
      Left = 65
      Top = 52
      Width = 75
      Height = 12
      Caption = #20282#26381#22120#36899#32080#22496':'
    end
    object Label12: TLabel
      Left = 65
      Top = 102
      Width = 61
      Height = 12
      Caption = #26412#27231'IP'#22320#22336':'
    end
    object Label1: TLabel
      Left = 65
      Top = 128
      Width = 135
      Height = 12
      Caption = #36890#35441#24460#33258#21205#36914#20837#23601#32210#29376#24907':'
    end
    object Label52: TLabel
      Left = 65
      Top = 189
      Width = 75
      Height = 12
      Caption = #24231#24109#20998#27231#34399#30908':'
    end
    object Label2: TLabel
      Left = 65
      Top = 162
      Width = 99
      Height = 12
      Caption = #22352#24109#20998#27231#32129#23450#26041#24335':'
    end
    object Label3: TLabel
      Left = 65
      Top = 28
      Width = 51
      Height = 12
      Caption = #20282#26381#22120'ID:'
    end
    object edServerIP: TEdit
      Left = 182
      Top = 74
      Width = 108
      Height = 20
      TabOrder = 0
      Text = '192.168.0.35'
      OnChange = cspServerPortChange
    end
    object cspServerPort: TCSpinEdit
      Left = 182
      Top = 49
      Width = 108
      Height = 21
      MaxValue = 9999
      MinValue = 1024
      TabOrder = 1
      Value = 5227
      OnChange = cspServerPortChange
    end
    object edSeatIP: TEdit
      Left = 182
      Top = 99
      Width = 108
      Height = 20
      TabOrder = 2
      Text = '192.168.0.138'
    end
    object cbAutoReady: TComboBox
      Left = 246
      Top = 125
      Width = 44
      Height = 20
      ItemHeight = 12
      TabOrder = 3
      Text = #26159
      OnChange = cspServerPortChange
      OnKeyPress = cbxDBTypeKeyPress
      Items.Strings = (
        #26159
        #21542)
    end
    object edSeatNo: TEdit
      Left = 182
      Top = 185
      Width = 108
      Height = 20
      TabOrder = 4
      OnChange = cspServerPortChange
    end
    object cbGetSeatNo: TComboBox
      Left = 182
      Top = 157
      Width = 109
      Height = 20
      ItemHeight = 12
      TabOrder = 5
      Text = #33287'IP'#32129#23450
      OnChange = cbGetSeatNoChange
      OnKeyPress = cbxDBTypeKeyPress
      Items.Strings = (
        #19981#32129#23450
        #33287'IP'#32129#23450
        #25163#21205#32129#23450)
    end
    object cspServerID: TCSpinEdit
      Left = 182
      Top = 25
      Width = 108
      Height = 21
      MaxValue = 255
      MinValue = 1
      TabOrder = 6
      Value = 1
      OnChange = cspServerPortChange
    end
  end
  object ckDBParamType: TCheckBox
    Left = 8
    Top = 232
    Width = 161
    Height = 17
    Caption = #26412#27231#35373#23450#36039#26009#24235#21443#25976'?'
    TabOrder = 4
    OnClick = ckDBParamTypeClick
  end
end
