//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editorderfit.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditOrderFit *FormEditOrderFit;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormEditOrderFit::TFormEditOrderFit(TComponent* Owner)
  : TForm(Owner)
{
  GetDateId = 1;  
}
//---------------------------------------------------------------------------
void TFormEditOrderFit::SetOrderFitData(COrders& orders)
{
  Orders = orders;
  editOrderId->Text = orders.OrderId;
  editPreFitDate->Text = orders.PreFitDate;
  editFactFitDate->Text = orders.FactFitDate;

  cbFitStatus->Text = dmAgent->OrderFitStatusItemList.GetItemName(orders.FitStatus);
  editFitSrvCorp->Text = orders.FitSrvCorp;
  editFitMen->Text = orders.FitMen;
  editFitMenTel->Text = orders.FitMenTel;

  memoFitNote->Lines->Clear();
  memoFitNote->Lines->Add(orders.FitNote);

  Button1->Enabled = false;
}
int TFormEditOrderFit::GetOrderFitData()
{
  Orders.PreFitDate = editPreFitDate->Text;
  Orders.FactFitDate = editFactFitDate->Text;

  Orders.FitStatus = dmAgent->OrderFitStatusItemList.GetItemValue(cbFitStatus->Text);
  Orders.FitSrvCorp = editFitSrvCorp->Text;
  Orders.FitMen = editFitMen->Text;
  Orders.FitMenTel = editFitMenTel->Text;
  Orders.FitNote = memoFitNote->Lines->Text;
  return 0;
}

void __fastcall TFormEditOrderFit::cbFitStatusKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderFit::editPreFitDateChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderFit::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderFit::FormCreate(TObject *Sender)
{
  dmAgent->AddCmbItem(cbFitStatus, 17, "");  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderFit::Button1Click(TObject *Sender)
{
  if (GetOrderFitData() != 0)
    return;

  if (dmAgent->UpdateOrderFitRecord(&Orders) == 0)
  {
    dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改訂單：%s的安裝情況記錄", Orders.OrderId.c_str());
    MessageBox(NULL,"修改訂單安裝情況成功!","信息提示",MB_OK|MB_ICONINFORMATION);
    dmAgent->adoQryOrder->Close();
    dmAgent->adoQryOrder->Open();
    this->Close();
  }
  else
  {
    MessageBox(NULL,"修改訂單安裝情況失敗!","信息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderFit::Button3Click(TObject *Sender)
{
  MonthCalendar1->Visible = true;
  MonthCalendar1->Date = Date();
  GetDateId = 1;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditOrderFit::Button4Click(TObject *Sender)
{
  MonthCalendar1->Visible = true;
  MonthCalendar1->Date = Date();
  GetDateId = 2;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditOrderFit::MonthCalendar1Click(TObject *Sender)
{
  if (GetDateId == 1)
  {
    editPreFitDate->Text = MonthCalendar1->Date.FormatString("yyyy-mm-dd");
  }
  else if (GetDateId == 2)
  {
    editFactFitDate->Text = MonthCalendar1->Date.FormatString("yyyy-mm-dd");
  }
  MonthCalendar1->Visible = false;
}
//---------------------------------------------------------------------------

