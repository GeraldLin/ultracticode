//---------------------------------------------------------------------------

#ifndef dialoutcallbackH
#define dialoutcallbackH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormCallBackPrompt : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TButton *Button1;
  TButton *Button2;
  TButton *Button3;
  TComboBox *ComboBox1;
  TLabel *Label2;
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormCallBackPrompt(TComponent* Owner);

  int Id;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormCallBackPrompt *FormCallBackPrompt;
//---------------------------------------------------------------------------
#endif
