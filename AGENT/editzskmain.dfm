object FormZSKMain: TFormZSKMain
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #31649#29702#26989#21209#30693#35672#24235#20027#20998#39006#35338#24687
  ClientHeight = 247
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label2: TLabel
    Left = 24
    Top = 13
    Width = 72
    Height = 12
    Caption = #20027#20998#39006#27161#38988#65306
  end
  object Label3: TLabel
    Left = 36
    Top = 47
    Width = 60
    Height = 12
    Caption = #35443#32048#35498#26126#65306
  end
  object Label11: TLabel
    Left = 60
    Top = 151
    Width = 36
    Height = 12
    Caption = #20633#35387#65306
  end
  object Label1: TLabel
    Left = 24
    Top = 181
    Width = 72
    Height = 12
    Caption = #37325#35201#24615#24207#34399#65306
  end
  object editMainTitle: TEdit
    Left = 104
    Top = 9
    Width = 325
    Height = 20
    TabOrder = 0
    OnChange = editMainTitleChange
  end
  object editRemark: TEdit
    Left = 104
    Top = 147
    Width = 325
    Height = 20
    TabOrder = 2
    OnChange = editMainTitleChange
  end
  object memoExplain: TMemo
    Left = 104
    Top = 43
    Width = 325
    Height = 97
    MaxLength = 1000
    ScrollBars = ssVertical
    TabOrder = 1
    OnChange = editMainTitleChange
  end
  object Button1: TButton
    Left = 113
    Top = 206
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 295
    Top = 206
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
  object cseOrderNo: TCSpinEdit
    Left = 104
    Top = 176
    Width = 60
    Height = 21
    TabOrder = 5
    OnChange = editMainTitleChange
  end
end
