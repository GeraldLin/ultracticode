//---------------------------------------------------------------------------

#ifndef editselitemH
#define editselitemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditSelItem : public TForm
{
__published:	// IDE-managed Components
  TLabel *lbItemId;
  TLabel *lbItemName;
  TEdit *editItemId;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  TLabel *lbURLAddr;
  TEdit *editURLAddr;
  TLabel *Label1;
  TCSpinEdit *cseSortType;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemIdChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSelItem(TComponent* Owner);

  int SelectTableId;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetDispMsg(int nSelectTableId);
  void SetDispValue(bool bInsertId, int nItemvalue, AnsiString strItemName);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSelItem *FormEditSelItem;
//---------------------------------------------------------------------------
#endif
