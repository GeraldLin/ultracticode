//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editendcode2.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditEndCode2 *FormEditEndCode2;
//---------------------------------------------------------------------------
__fastcall TFormEditEndCode2::TFormEditEndCode2(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditEndCode2::SetDispMsg(bool bInsertId, int nEndCode1, int nEndCode2, AnsiString strItemName, AnsiString strItemURL, AnsiString strColor, int nDispOrder)
{
  InsertId = bInsertId;
  EndCode1 = nEndCode1;
  if (InsertId == true)
  {
    editItemId->Text = "";
    editItemId->Color = clWindow;
    editItemId->ReadOnly = false;
    editItemName->Text = "";
    editItemURL->Text = "";
    cbColor->Text = strColor;
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editItemId->Text = nEndCode2;
    editItemId->Color = clSilver;
    editItemId->ReadOnly = true;
    editItemName->Text = strItemName;
    editItemURL->Text = strItemURL;
    cbColor->Text = strColor;
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}

void __fastcall TFormEditEndCode2::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditEndCode2::editItemIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditEndCode2::Button1Click(TObject *Sender)
{
  char dispbuf[256];

  if (MyIsNumber(editItemId->Text) != 0)
  {
    MessageBox(NULL,"對不起,您輸入的結束碼二編號有誤!","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,"對不起,請輸入結束碼二名稱!","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  int nEndCode2 = StrToInt(editItemId->Text);
  if (InsertId == true)
  {
    if (dmAgent->IsEndCode2Exist(EndCode1, nEndCode2) == 1)
    {
      MessageBox(NULL,"對不起,您輸入的結束碼二編號已存在!","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertEndCode2(EndCode1, nEndCode2, editItemName->Text, editItemURL->Text, cbColor->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryEndCode2->Close();
      dmAgent->adoQryEndCode2->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加結束碼二編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateEndCode2(EndCode1, nEndCode2, editItemName->Text, editItemURL->Text, cbColor->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryEndCode2->Close();
      dmAgent->adoQryEndCode2->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改結束碼二編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditEndCode2::FormShow(TObject *Sender)
{
  if (InsertId == true)
    editItemId->SetFocus();
  else
    editItemName->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditEndCode2::cbColorKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

