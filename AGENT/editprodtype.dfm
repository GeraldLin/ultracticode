object FormEditProdType: TFormEditProdType
  Left = 322
  Top = 208
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #35373#23450#29986#21697#25110#26381#21209#39006#22411
  ClientHeight = 446
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 52
    Top = 28
    Width = 96
    Height = 12
    Caption = #26381#21209#25110#29986#21697#32232#34399#65306
  end
  object Label2: TLabel
    Left = 307
    Top = 29
    Width = 96
    Height = 12
    Caption = #26381#21209#25110#29986#21697#21517#31281#65306
  end
  object Label3: TLabel
    Left = 76
    Top = 61
    Width = 72
    Height = 12
    Caption = #25152#23660#25216#33021#32068#65306
  end
  object Label4: TLabel
    Left = 152
    Top = 80
    Width = 372
    Height = 12
    Caption = #27880#24847#65306#30070#23660#20110#25152#26377#32676#32068#26178#22635'0'#65292#23660#20110#22810#20491#32676#32068#26178#29992#36887#34399#38548#38283#65292#22914#65306'1,2,3'
  end
  object Label5: TLabel
    Left = 73
    Top = 152
    Width = 72
    Height = 12
    Caption = #29986#21697#39006#22411'URL:'
  end
  object Label6: TLabel
    Left = 14
    Top = 184
    Width = 54
    Height = 12
    Caption = #35441#34899#33139#26412':'
  end
  object Label7: TLabel
    Left = 306
    Top = 61
    Width = 96
    Height = 12
    Caption = #19979#25289#26694#39023#31034#38918#24207#65306
  end
  object Label8: TLabel
    Left = 67
    Top = 117
    Width = 78
    Height = 12
    Caption = 'URL'#25353#37397#21517#31281#65306
  end
  object editProdType: TEdit
    Left = 152
    Top = 24
    Width = 145
    Height = 20
    TabOrder = 0
    OnChange = editProdTypeChange
  end
  object editProdName: TEdit
    Left = 407
    Top = 24
    Width = 145
    Height = 20
    TabOrder = 1
    OnChange = editProdTypeChange
  end
  object Button1: TButton
    Left = 176
    Top = 408
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 368
    Top = 408
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editGroupNoList: TEdit
    Left = 152
    Top = 56
    Width = 145
    Height = 20
    TabOrder = 4
    Text = '0'
    OnChange = editProdTypeChange
  end
  object editItemURL: TEdit
    Left = 152
    Top = 148
    Width = 399
    Height = 20
    TabOrder = 5
    OnChange = editProdTypeChange
  end
  object memoDemo: TMemo
    Left = 16
    Top = 200
    Width = 535
    Height = 185
    MaxLength = 3500
    ScrollBars = ssBoth
    TabOrder = 6
    OnChange = editProdTypeChange
  end
  object cseDispOrder: TCSpinEdit
    Left = 408
    Top = 56
    Width = 145
    Height = 21
    TabOrder = 7
    OnChange = editProdTypeChange
  end
  object editURLBtnName: TEdit
    Left = 151
    Top = 112
    Width = 145
    Height = 20
    TabOrder = 8
    OnChange = editProdTypeChange
  end
end
