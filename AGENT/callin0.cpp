//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "callin0.h"
#include "main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormCallin0 *FormCallin0;
extern BSTR bEmpty;
extern CMySeat MySeat;
extern bool FlashId;
//呼叫信息
extern CCallMsg CallMsg;
extern CAccept MyAccept;
//---------------------------------------------------------------------------
__fastcall TFormCallin0::TFormCallin0(TComponent* Owner)
  : TForm(Owner)
{
}

void TFormCallin0::SetCallerNo(AnsiString callerno)
{
  lblCallerNo->Caption = callerno;
}
void TFormCallin0::SetCalledNo(AnsiString calledno, int inout)
{
  if (inout == 2)
  {
    Label3->Visible = false;
    lblCalledNo->Visible = false;
    lblCalledNo->Caption = "";
  }
  else
  {
    Label3->Visible = true;
    lblCalledNo->Visible = true;
    lblCalledNo->Caption = calledno;
  }
}
void TFormCallin0::SetCallerName(AnsiString callername)
{
  if (callername.Length() > 0)
    Label1->Caption = callername;
  else
    Label1->Caption = "來電者號碼：";
}
void TFormCallin0::SetTelOfCity(AnsiString city)
{
  Label12->Caption = "號碼類別："+city;
}
void TFormCallin0::SetTranPhone(int nacdcalltype, const char *transeatno)
{
  AnsiString strTranSeatNo = (char *)transeatno;
  if (nacdcalltype == 1)
  {
    lbTranPhone->Caption = "轉接的電話，轉接的坐席號："+strTranSeatNo;
    lbTranPhone->Visible = true;
  }
  else if (nacdcalltype == 2)
  {
    lbTranPhone->Caption = "轉接返回的電話，轉接的坐席號："+strTranSeatNo;
    lbTranPhone->Visible = true;
  }
  else
  {
    lbTranPhone->Caption = "";
    lbTranPhone->Visible = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin0::SpeedButton2Click(TObject *Sender)
{
  FormMain->MyAgent1->AnswerCall(2, bEmpty, bEmpty);
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin0::FormCreate(TObject *Sender)
{
  if (MySeat.SeatType == 2)
  {
    Button1->Caption = "接聽";
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin0::FormKeyPress(TObject *Sender, char &Key)
{
  if (Key == VK_BACK)
  {
    if ((MySeat.SeatType == 2 && CallMsg.CallInOut == 1) || MyAccept.MediaType == 1)
    {
      FormMain->MyAgent1->AnswerCall(1, bEmpty, bEmpty);
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin0::Button1Click(TObject *Sender)
{
  if ((MySeat.SeatType == 2 && CallMsg.CallInOut == 1) || MyAccept.MediaType == 1)
  {
    FormMain->MyAgent1->AnswerCall(1, bEmpty, bEmpty);
  }
  this->Close();
}
//---------------------------------------------------------------------------
void TFormCallin0::SetSrvType(CAccept &accept)
{
  if (accept.AcptType > 0)
    lbSrvType->Caption = "業務類型："+dmAgent->ProdTypeItemList.GetItemName(accept.ProdType)+"->"+dmAgent->SrvTypeItemList.GetItemName(accept.ProdType, accept.AcptType);
  else
    lbSrvType->Caption = "業務類型："+dmAgent->ProdTypeItemList.GetItemName(accept.ProdType);
}

void TFormCallin0::SetSrvType(int nAcptType, int nAcptSubType)
{
  if (nAcptSubType > 0)
    lbSrvType->Caption = "業務類型："+dmAgent->ProdTypeItemList.GetItemName(nAcptType)+"->"+dmAgent->SrvTypeItemList.GetItemName(nAcptType, nAcptSubType);
  else
    lbSrvType->Caption = "業務類型："+dmAgent->ProdTypeItemList.GetItemName(nAcptType);
}
void TFormCallin0::SetCallType(int nCallType)
{
  if (nCallType == 2)
  {
    Label3->Visible = false;
    lblCalledNo->Visible = false;
  }
  else
  {
    Label3->Visible = true;
    lblCalledNo->Visible = true;
  }
}
void TFormCallin0::SetScreenPop(int nPopId)
{
  lbSrvType->Caption = "業務類型："+dmAgent->GetPopString(nPopId);
}

