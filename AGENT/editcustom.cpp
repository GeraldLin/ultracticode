//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editcustom.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditCustom *FormEditCustom;

extern int g_nBandSeatNoType, CustExOpenId;
//話務員參數
extern CMySeat MySeat;
extern CMyWorker MyWorker;
extern char *MyGetCustomNo();
//2017-10-08
extern AnsiString g_strIDFieldName; //身份證客戶資料擴展欄位字段名
extern int g_nIDFieldIndex; //身份證客戶資料擴展欄位字段索引
extern AnsiString g_strACFieldName; //會員編號客戶資料擴展欄位字段名
extern int g_nACFieldIndex; //會員編號客戶資料擴展欄位字段索引
extern AnsiString g_strTELFieldName; //聯絡號碼客戶資料擴展欄位字段名
extern int g_nTELFieldIndex; //聯絡號碼客戶資料擴展欄位字段索引
extern AnsiString strQueryCustDataURL;
extern int g_nQueryCustDataURLEncodeType;

//---------------------------------------------------------------------------
__fastcall TFormEditCustom::TFormEditCustom(TComponent* Owner)
  : TForm(Owner)
{
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    TCustExLabelList[i] = NULL;
    TCustExInputList[i] = NULL;
  }
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormEditCustom->edtCustType, dmAgent->CustTypeItemList, "");
    dmAgent->AddCmbItem(FormEditCustom->edtCustLevel, dmAgent->CustLevelItemList, "");
    dmAgent->AddCmbItem(FormEditCustom->cbProvince, dmAgent->ProvinceList, "");
    dmAgent->AddCmbItem(FormEditCustom->cbCustClass, dmAgent->CustClassList, "");
    CreateCustExControl();
  }
  CustExChangeId = false;
}
void __fastcall TFormEditCustom::MyOnChangeEvent(TObject *Sender)
{
  BitBtn1->Enabled = true;
  CustExChangeId = true;
}
void TFormEditCustom::CreateCustExControl()
{
  static bool bCreated=false;
  int nShortEditWith=120, nLongEditWith=420;
  int i, nControlCount, nEditLeft=100, nEditTop=6;

  if (bCreated == true)
    return;
  bCreated = true;
  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList->ItemNum == 0)
  {
    gbCustEx->Visible = false;
    btQryAS400->Top = 180;
    BitBtn1->Top = 180;
    BitBtn2->Top = 180;
    FormEditCustom->Height = 270;
    return;
  }

  for (i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].state == 0 ||
      (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType != 1 && FormMain->m_pCustExItemSetList->m_CustExItemSet[i].ReadDataType != 4
      && FormMain->m_pCustExItemSetList->m_CustExItemSet[i].SaveToDBFlag != 1)) //2017-01-02 add
      continue;
    //輸入標簽控件
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType == 3)
    {
      if (nEditLeft == 400)
      {
        nEditLeft = 700;
      }
      else if (nEditLeft == 1000)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
    }
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType != 8)
    {
      TCustExLabelList[i] = new TLabel(this);
      TCustExLabelList[i]->Parent = sbCustEx;
      TCustExLabelList[i]->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"：";
      TCustExLabelList[i]->Left = nEditLeft-TCustExLabelList[i]->Width-2;
      TCustExLabelList[i]->Top = nEditTop+4;
      TCustExLabelList[i]->Show();
    }
    //輸入框控件
    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      TCustExInputList[i] = new TComboBox(this);
      ((TComboBox *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TComboBox *)TCustExInputList[i])->Enabled = true;
        ((TComboBox *)TCustExInputList[i])->Color = clSilver;
      }
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TComboBox *)TCustExInputList[i])->Left = nEditLeft;
      ((TComboBox *)TCustExInputList[i])->Top = nEditTop;
      ((TComboBox *)TCustExInputList[i])->Width = nShortEditWith + 50;
      ((TComboBox *)TCustExInputList[i])->MaxLength = 50;
      ((TComboBox *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      dmAgent->AddCmbItem((TComboBox *)TCustExInputList[i], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "");
      ((TComboBox *)TCustExInputList[i])->Show();
      break;
    case 2: //數值下拉框
      TCustExInputList[i] = new TComboBox(this);
      ((TComboBox *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TComboBox *)TCustExInputList[i])->Enabled = true;
        ((TComboBox *)TCustExInputList[i])->Color = clSilver;
      }
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemName(MyStrToInt(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData));
      ((TComboBox *)TCustExInputList[i])->Left = nEditLeft;
      ((TComboBox *)TCustExInputList[i])->Top = nEditTop;
      ((TComboBox *)TCustExInputList[i])->Width = nShortEditWith;
      ((TComboBox *)TCustExInputList[i])->MaxLength = 50;
      ((TComboBox *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TComboBox *)TCustExInputList[i])->OnKeyPress = edtCustTypeKeyPress;
      dmAgent->AddCmbItem((TComboBox *)TCustExInputList[i], FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList, "");
      ((TComboBox *)TCustExInputList[i])->Show();
      break;
    case 3: //長文本框
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TEdit *)TCustExInputList[i])->ReadOnly = true;
        ((TEdit *)TCustExInputList[i])->Color = clSilver;
      }
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nLongEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 250;
      ((TEdit *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    case 4: //短文本框
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TEdit *)TCustExInputList[i])->ReadOnly = true;
        ((TEdit *)TCustExInputList[i])->Color = clSilver;
      }
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 50;
      ((TEdit *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    case 5: //數值框
      TCustExInputList[i] = new TCSpinEdit(this);
      ((TCSpinEdit *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TCSpinEdit *)TCustExInputList[i])->ReadOnly = true;
        ((TCSpinEdit *)TCustExInputList[i])->Color = clSilver;
      }
      ((TCSpinEdit *)TCustExInputList[i])->MinValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMinValue;
      ((TCSpinEdit *)TCustExInputList[i])->MaxValue = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CMaxValue;
      ((TCSpinEdit *)TCustExInputList[i])->Value = MyStrToInt(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData);
      ((TCSpinEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TCSpinEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TCSpinEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TCSpinEdit *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TCSpinEdit *)TCustExInputList[i])->Show();
      break;
    case 6: //金額框
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TEdit *)TCustExInputList[i])->ReadOnly = true;
        ((TEdit *)TCustExInputList[i])->Color = clSilver;
      }
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 20;
      ((TEdit *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    case 7: //日期框
      TCustExInputList[i] = new TDateTimePicker(this);
      ((TDateTimePicker *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TDateTimePicker *)TCustExInputList[i])->Enabled = true;
        ((TDateTimePicker *)TCustExInputList[i])->Color = clSilver;
      }
      ((TDateTimePicker *)TCustExInputList[i])->Kind = dtkDate;
      ((TDateTimePicker *)TCustExInputList[i])->Date = MyStrToDate(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData);
      ((TDateTimePicker *)TCustExInputList[i])->Left = nEditLeft;
      ((TDateTimePicker *)TCustExInputList[i])->Top = nEditTop;
      ((TDateTimePicker *)TCustExInputList[i])->Width = nShortEditWith;
      ((TDateTimePicker *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TDateTimePicker *)TCustExInputList[i])->Show();
      break;
    case 8: //是非框
      TCustExInputList[i] = new TCheckBox(this);
      ((TCheckBox *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TCheckBox *)TCustExInputList[i])->Enabled = true;
        ((TCheckBox *)TCustExInputList[i])->Color = clSilver;
      }
      ((TCheckBox *)TCustExInputList[i])->Caption = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption;
      ((TCheckBox *)TCustExInputList[i])->Checked = (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData=="1") ? true : false;
      ((TCheckBox *)TCustExInputList[i])->Left = nEditLeft-80;
      ((TCheckBox *)TCustExInputList[i])->Top = nEditTop+4;
      ((TCheckBox *)TCustExInputList[i])->Width = nShortEditWith;
      ((TCheckBox *)TCustExInputList[i])->OnClick = MyOnChangeEvent;
      ((TCheckBox *)TCustExInputList[i])->Show();
      break;
    default:
      TCustExInputList[i] = new TEdit(this);
      ((TEdit *)TCustExInputList[i])->Parent = sbCustEx;
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CInputId == 2)
      {
        ((TEdit *)TCustExInputList[i])->ReadOnly = true;
        ((TEdit *)TCustExInputList[i])->Color = clSilver;
      }
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      ((TEdit *)TCustExInputList[i])->Left = nEditLeft;
      ((TEdit *)TCustExInputList[i])->Top = nEditTop;
      ((TEdit *)TCustExInputList[i])->Width = nShortEditWith;
      ((TEdit *)TCustExInputList[i])->MaxLength = 50;
      ((TEdit *)TCustExInputList[i])->OnChange = MyOnChangeEvent;
      ((TEdit *)TCustExInputList[i])->Show();
      break;
    }
    if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType != 3)
    {
      if (nEditLeft == 1000)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
      else
      {
        nEditLeft = nEditLeft+300;
      }
    }
    else
    {
      if (nEditLeft == 100)
      {
        nEditLeft = 700;
      }
      else if (nEditLeft == 700)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+22;
      }
    }
  }
  if (nEditLeft == 100 && nEditTop == 6)
  {
    gbCustEx->Visible = false;
    btQryAS400->Top = 180;
    BitBtn1->Top = 180;
    BitBtn2->Top = 180;
    FormEditCustom->Height = 270;
    return;
  }
  int nRows = (nEditTop-6)/22+1;
  if (nEditLeft == 100 && nRows > 0)
    nRows--;
  if (nRows < 6)
  {
    gbCustEx->Height = 165-(6-nRows)*22;
    btQryAS400->Top = 348-(6-nRows)*22;
    BitBtn1->Top = 348-(6-nRows)*22;
    BitBtn2->Top = 348-(6-nRows)*22;
    FormEditCustom->Height = 430-(6-nRows)*22;
  }
}
//---------------------------------------------------------------------------
void TFormEditCustom::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    BitBtn1->Caption = "新增";
    FormEditCustom->Caption = "新增來電者資料";
  }
  else
  {
    BitBtn1->Caption = "修改";
    FormEditCustom->Caption = "修改來電者資料";
  }
  CreateCustExControl(); //2017-01-02 add
}

void TFormEditCustom::SetCustom(CCustomer &customer)
{
  Customer = customer;
  CustEx.CustomNo = customer.CustomNo;
  edtCustomNo->Text = customer.CustomNo; //客戶編號
  edtCustName->Text = customer.CustName; //客戶名稱
  edtCorpName->Text = customer.CorpName; //公司名稱

  edtCustType->Text = dmAgent->CustTypeItemList.GetItemName(customer.CustType); //客戶類型
  edtCustLevel->Text = dmAgent->CustLevelItemList.GetItemName(customer.CustLevel); //客戶劃分

  cbCustClass->Text = dmAgent->CustClassList.GetItemName(customer.CustClass);

  edtCustSex->Text = customer.CustSex; //性別

  edtCustSex->Text = GetHidePhoneNum(2, customer.CustSex); //俶梗
  edtMobileNo->Text = GetHidePhoneNum(2, customer.MobileNo); //忒儂
  edtTeleNo->Text = GetHidePhoneNum(2, customer.TeleNo); //嘐趕

  Customer.MobileNoModifyId = false;
  Customer.TeleNoModifyId = false;
  Customer.FaxNoModifyId = false;

  edtSubPhone->Text = customer.SubPhone;
  edtEMail->Text = customer.EMail; //郵箱
  edtContAddr->Text = customer.ContAddr; //地址
  edtZoneName->Text = customer.ZoneName; //地址
  edtPostNo->Text = customer.PostNo; //郵編
  edtRemark->Text = customer.Remark; //備注
  cbProvince->Text = customer.Province;
  cbCityName->Text = customer.CityName;
  cbCountyName->Text = customer.CountyName;
  edtAccountNo->Text = customer.AccountNo;
  int ParentId;
  if (cbProvince->Text.Length() > 0)
  {
    ParentId = dmAgent->ProvinceList.GetItemValue(cbProvince->Text);
    cbCityName->Items->Clear();
    if (ParentId > 0)
    {
      dmAgent->ReadProvidAreaList(dmAgent->CityNameList, ParentId, 2);
      dmAgent->AddCmbItem(cbCityName, dmAgent->CityNameList, "");
    }
  }
  if (cbCityName->Text.Length() > 0)
  {
    ParentId = dmAgent->CityNameList.GetItemValue(cbCityName->Text);
    cbCountyName->Items->Clear();
    if (ParentId > 0)
    {
      dmAgent->ReadProvidAreaList(dmAgent->CountyNameList, ParentId, 3);
      dmAgent->AddCmbItem(cbCountyName, dmAgent->CountyNameList, "");
    }
  }
  CustExChangeId = false;
  BitBtn1->Enabled = false;
}
void TFormEditCustom::SetCustEx(CCustEx &custex)
{
  AnsiString strDT;
  CustExChangeId = false;
  CustEx = custex;
  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList == NULL)
    return;
  if (FormMain->m_pCustExItemSetList->ItemNum == 0)
    return;

  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (TCustExInputList[i] == NULL)
      continue;
    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      ((TComboBox *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 2: //數值下拉框
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemName(MyStrToInt(custex.Itemdata[i]));
      break;
    case 3: //長文本框
      ((TEdit *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 4: //短文本框
      ((TEdit *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 5: //數值框
      ((TCSpinEdit *)TCustExInputList[i])->Value = MyStrToInt(custex.Itemdata[i]);
      break;
    case 6: //金額框
      ((TEdit *)TCustExInputList[i])->Text = custex.Itemdata[i];
      break;
    case 7: //日期框
      strDT = custex.Itemdata[i].SubString(1, 10);
      ((TDateTimePicker *)TCustExInputList[i])->Date = MyStrToDate(strDT);
      break;
    case 8: //是非框
      ((TCheckBox *)TCustExInputList[i])->Checked = (custex.Itemdata[i]=="1") ? true : false;
      break;
    }
  }
}
void TFormEditCustom::ClearCusExData()
{
  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList == NULL)
    return;
  if (FormMain->m_pCustExItemSetList->ItemNum == 0)
    return;
  CustExChangeId = false;
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (TCustExInputList[i] == NULL)
      continue;
    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      break;
    case 2: //數值下拉框
      ((TComboBox *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemName(MyStrToInt(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData));
      break;
    case 3: //長文本框
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      break;
    case 4: //短文本框
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      break;
    case 5: //數值框
      ((TCSpinEdit *)TCustExInputList[i])->Value = MyStrToInt(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData);
      break;
    case 6: //金額框
      ((TEdit *)TCustExInputList[i])->Text = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData;
      break;
    case 7: //日期框
      ((TDateTimePicker *)TCustExInputList[i])->Date = MyStrToDate(FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData);
      break;
    case 8: //是非框
      ((TCheckBox *)TCustExInputList[i])->Checked = (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CDefaultData=="1") ? true : false;
      break;
    }
  }
}
//---------------------------------------------------------------------------
int TFormEditCustom::GetCustom()
{
  Customer.CustomNo = edtCustomNo->Text; //客戶編號
  //if (edtCustName->Text == "")
  //{
  //  MessageBox(NULL,"請輸入來電者姓名！","訊息提示",MB_OK|MB_ICONERROR);
  //  return 1;
  //}
  Customer.CustName = edtCustName->Text; //客戶名稱
  Customer.CorpName = edtCorpName->Text; //公司名稱
  /*if (edtAccountNo->Text != "")
  {
    int r = CheckAccountNo(edtAccountNo->Text);
    if (r == 1 || r == 2)
    {
      MessageBox(NULL,"統一編號必須為8個數字！","訊息提示",MB_OK|MB_ICONWARNING);
      return 1;
    }
    else if (r == 3)
    {
      MessageBox(NULL,"統一編號不符合規範！","訊息提示",MB_OK|MB_ICONWARNING);
      return 1;
    }
  }*/

  Customer.CustType = dmAgent->CustTypeItemList.GetItemValue(edtCustType->Text); //客戶類型
  if (Customer.CustType < 0)
    Customer.CustType = 0;
  Customer.CustLevel = dmAgent->CustLevelItemList.GetItemValue(edtCustLevel->Text);
  if (Customer.CustLevel < 0)
    Customer.CustLevel = 0;
  Customer.CustClass = dmAgent->CustClassList.GetItemValue(cbCustClass->Text);
  if (Customer.CustClass == 1) //限本人查看
  {
    Customer.DepartmentId = 0;
    Customer.WorkerNo = MyWorker.WorkerNo;
  }
  else if (Customer.CustClass == 2) //部門內查看
  {
    Customer.DepartmentId = MyWorker.DepartmentID;
    Customer.WorkerNo = "";
  }
  else
  {
    Customer.DepartmentId = 0;
    Customer.WorkerNo = "";
  }

  Customer.CustSex = edtCustSex->Text; //性別
  //if (edtMobileNo->Text == "" && edtTeleNo->Text == "" && edtEMail->Text == "")
  //{
  //  MessageBox(NULL,"請至少輸入一組電話(手機)號碼！","訊息提示",MB_OK|MB_ICONWARNING);
  //  return 1;
  //}
  if (edtMobileNo->Text != "")
  {
    if (GetBit(MyWorker.HideCallerNo, 2) == 0 || Customer.MobileNoModifyId == true)
    {
    //if (MyIsMobileNo(edtMobileNo->Text) != 0)
    //{
    //  MessageBox(NULL,"輸入的手機號碼有誤！","訊息提示",MB_OK|MB_ICONERROR);
    //  return 1;
    //}
    }
  }
  if (GetBit(MyWorker.HideCallerNo, 2) == 0 || Customer.MobileNoModifyId == true)
    Customer.MobileNo = edtMobileNo->Text.Trim(); //忒儂

  if (edtTeleNo->Text != "")
  {
    if (GetBit(MyWorker.HideCallerNo, 2) == 0 || Customer.TeleNoModifyId == true)
    {
    if (edtTeleNo->Text.Length() < 7 || MyIsDigits(edtTeleNo->Text) != 0)
    {
      MessageBox(NULL,"輸入的市話號碼有誤！","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
    }
  }
  if (GetBit(MyWorker.HideCallerNo, 2) == 0 || Customer.TeleNoModifyId == true)
    Customer.TeleNo = edtTeleNo->Text.Trim(); //嘐趕

  if (edtFaxNo->Text != "")
  {
    if (GetBit(MyWorker.HideCallerNo, 2) == 0 || Customer.FaxNoModifyId == true)
    {
    if (edtFaxNo->Text.Length() < 7 || MyIsDigits(edtFaxNo->Text) != 0)
    {
      MessageBox(NULL,"輸入的傳真號碼有誤！","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
    }
  }
  if (GetBit(MyWorker.HideCallerNo, 2) == 0 || Customer.FaxNoModifyId == true)
    Customer.FaxNo = edtFaxNo->Text.Trim(); //換淩

  Customer.SubPhone = edtSubPhone->Text.Trim(); //分機
  Customer.EMail = edtEMail->Text; //郵箱
  Customer.ContAddr = edtContAddr->Text; //地址
  Customer.ZoneName = edtZoneName->Text; //地址
  Customer.PostNo = edtPostNo->Text; //郵編

  Customer.Province = cbProvince->Text;
  Customer.CityName = cbCityName->Text;
  Customer.CountyName = cbCountyName->Text;

  Customer.Remark = edtRemark->Text; //備注
  Customer.AccountNo = edtAccountNo->Text;
  return 0;
}


void TFormEditCustom::ClearCustom()
{
  Customer.id = 0;
  //edtCustomNo->Text = FormMain->MyGetCustomNo(); //客戶編號
  edtCustName->Text = ""; //客戶名稱
  edtCorpName->Text = ""; //公司名稱
  edtCustType->Text = dmAgent->CustTypeItemList.GetItemName(1); //客戶類型
  edtCustLevel->Text = dmAgent->CustLevelItemList.GetItemName(1); //客戶類型
  edtCustSex->Text = ""; //性別
  edtMobileNo->Text = ""; //手機
  edtTeleNo->Text = ""; //固話
  edtFaxNo->Text = ""; //傳真
  edtSubPhone->Text = ""; //分機
  edtEMail->Text = ""; //郵箱
  edtContAddr->Text = ""; //地址
  edtZoneName->Text = "";
  edtPostNo->Text = ""; //郵編
  cbProvince->Text = "";
  cbCityName->Text = "";
  cbCityName->Items->Clear();
  cbCountyName->Text = "";
  cbCountyName->Items->Clear();
  edtRemark->Text = ""; //備注
  edtAccountNo->Text = "";
  cbCustClass->Text = dmAgent->CustClassList.GetItemName(3);
  BitBtn1->Enabled = false;
}

void __fastcall TFormEditCustom::BitBtn1Click(TObject *Sender)
{
  int nCustomerExist;
  WriteErrprMsg("%s", "BitBtn1Click start");
  if (GetCustom() != 0)
  {
    return;
  }
  if (GetCustExData(CustEx) != 0)
  {
    return;
  }
  //2017-10-08
  AnsiString strCustomNo="", strID="", strAC="", strTel="";
  AnsiString strCustomNo1="", strID1="", strAC1="", strTel1="";

  //if (InsertId == true)
  {
  //跟據:身份證, 會員編號, 聯絡號碼, 客戶編號 判斷客戶資料
  if (g_nTELFieldIndex >= 0 && g_nTELFieldIndex < MAX_CUST_CONTROL_NUM)
  {
    strTel = CustEx.Itemdata[g_nTELFieldIndex];
    strID = CustEx.Itemdata[g_nIDFieldIndex];
    strAC = CustEx.Itemdata[g_nACFieldIndex];
    if (strTel != "")
    {
      if (dmAgent->GetCustomerNoByTEL(strTel, strCustomNo1, strID1, strAC1) == 0)
      {
        WriteErrprMsg("GetCustomerNoByTEL strTel=%s strCustomNo=%s", strTel.c_str(), strCustomNo1.c_str());
        if (strCustomNo1 != Customer.CustomNo)
        {
          MessageBox(NULL,"對不起, 該號碼已存在！","訊息提示",MB_OK|MB_ICONERROR);
          return;
        }
        else
        {
          if (strID != strID1 || strAC != strAC1)
          {
            MessageBox(NULL,"對不起, 該號碼已存在！","訊息提示",MB_OK|MB_ICONERROR);
            return;
          }
        }
      }

      if (strTel.SubString(1,2) == "09")
      {
        Customer.MobileNo = strTel;
      }
      else
      {
        Customer.TeleNo = strTel;
      }
    }
  }

  if (g_nIDFieldIndex >= 0 && g_nIDFieldIndex < MAX_CUST_CONTROL_NUM)
  {
    strID = CustEx.Itemdata[g_nIDFieldIndex];
    if (strID != "")
    {
      if (dmAgent->SaveCustomerDataByID(strID, Customer, CustEx, strCustomNo) == 0)
      {
        MessageBox(NULL,"儲存來電者資料成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
        ClearCustom();
        ClearCusExData();
        this->Close();
      }
      else
      {
        MessageBox(NULL,"儲存來電者資料失敗！","訊息提示",MB_OK|MB_ICONERROR);
      }
      return;
    }
  }
  if (g_nACFieldIndex >= 0 && g_nACFieldIndex < MAX_CUST_CONTROL_NUM)
  {
    strAC = CustEx.Itemdata[g_nACFieldIndex];
    if (strAC != "")
    {
      if (dmAgent->SaveCustomerDataByAC(strAC, Customer, CustEx, strCustomNo) == 0)
      {
        MessageBox(NULL,"儲存來電者資料成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
        ClearCustom();
        ClearCusExData();
        this->Close();
      }
      else
      {
        MessageBox(NULL,"儲存來電者資料失敗！","訊息提示",MB_OK|MB_ICONERROR);
      }
      return;
    }
  }
  if (g_nTELFieldIndex >= 0 && g_nTELFieldIndex < MAX_CUST_CONTROL_NUM)
  {
    strTel = CustEx.Itemdata[g_nTELFieldIndex];
    if (strTel != "")
    {
      if (dmAgent->SaveCustomerDataByTEL(strTel, Customer, CustEx, strCustomNo) == 0)
      {
        MessageBox(NULL,"儲存來電者資料成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
        ClearCustom();
        ClearCusExData();
        this->Close();
      }
      else
      {
        MessageBox(NULL,"儲存來電者資料失敗！","訊息提示",MB_OK|MB_ICONERROR);
      }
      return;
    }
  }
  }

  if (InsertId == true)
  {
    nCustomerExist = dmAgent->IsCustomerExist(Customer.CustName, Customer.MobileNo, Customer.TeleNo, Customer.FaxNo, ckSameCustName->Checked);

    if (nCustomerExist == 1)
    {
      MessageBox(NULL,"對不起，該來電者姓名已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (nCustomerExist == 2)
    {
      MessageBox(NULL,"對不起，輸入的號碼已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->IsCustomNoExist(Customer.CustomNo) == 1)
    {
      MessageBox(NULL,"對不起，該來電者編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertCustom(&Customer) == 0)
    {
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員新增來電者編號為：%s的資料", Customer.CustomNo.c_str());
      if (CustExChangeId == true)
        dmAgent->UpdateCustEx(CustEx);
      dmAgent->adoQryUserBase->Close();
      dmAgent->adoQryUserBase->Open();
      ClearCustom();
      ClearCusExData();
      this->Close();
      //MessageBox(NULL,"新增來電者資料成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增來電者資料失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateCustom(&Customer) == 0)
    {
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員修改來電者編號為：%s的資料", Customer.CustomNo.c_str());
      if (CustExChangeId == true)
        dmAgent->UpdateCustEx(CustEx);
      dmAgent->adoQryUserBase->Close();
      dmAgent->adoQryUserBase->Open();
      ClearCustom();
      ClearCusExData();
      this->Close();
      //MessageBox(NULL,"來電者資料修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"來電者資料修改失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}

//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::BitBtn2Click(TObject *Sender)
{
  if (InsertId == false)
  {
    ClearCustom();
    ClearCusExData();
  }
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::edtCustNameChange(TObject *Sender)
{
  BitBtn1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::edtCustTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::cbProvinceExit(TObject *Sender)
{
  int ParentId;
  ParentId = dmAgent->ProvinceList.GetItemValue(cbProvince->Text);
  cbCityName->Items->Clear();
  cbCountyName->Items->Clear();
  if (ParentId > 0)
  {
    dmAgent->ReadProvidAreaList(dmAgent->CityNameList, ParentId, 2);
    dmAgent->AddCmbItem(cbCityName, dmAgent->CityNameList, "");
  }
  BitBtn1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::FormShow(TObject *Sender)
{
  if (strQueryCustDataURL != "")
    btQryAS400->Visible = true;

  CreateCustExControl(); //2017-01-02 add
  FormEditCustom->edtCustType->SetFocus();
  dmAgent->AddCmbItem(cbProvince, dmAgent->ProvinceList, "");
}
//---------------------------------------------------------------------------
/*
  for (int i=1; i<=Edit1->Text.Length(); i++)
  {
    if ((Edit1->Text[i]>='a' && Edit1->Text[i]>='z') || (Edit1->Text[i]>='A' && Edit1->Text[i]>='Z'))
      continue;
    else
      return;
  }
  dmAgent->AddProvidAreaCmbItem(cbProvince, 0, Edit1->Text);
  cbProvince->DroppedDown = true;
*/
void __fastcall TFormEditCustom::cbCityNameExit(TObject *Sender)
{
  int ParentId;
  ParentId = dmAgent->CityNameList.GetItemValue(cbCityName->Text);
  cbCountyName->Items->Clear();
  if (ParentId > 0)
  {
    dmAgent->ReadProvidAreaList(dmAgent->CountyNameList, ParentId, 3);
    dmAgent->AddCmbItem(cbCountyName, dmAgent->CountyNameList, "");
  }
  BitBtn1->Enabled = true;
}
//---------------------------------------------------------------------------
int TFormEditCustom::GetCustExData(CCustEx &custx, bool bPromptError)
{
  AnsiString strPrompt;
  custx.nItemNum = 0;

  if (CustExOpenId == 0 || FormMain->m_pCustExItemSetList == NULL)
    return 0;
  if (CustExChangeId == false)
    return 0;
  CustEx.CustomNo = edtCustomNo->Text;
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (TCustExInputList[i] == NULL)
      continue;

    switch (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CControlType)
    {
    case 1: //文本下拉框
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CIsAllowNull == 0 && ((TComboBox *)TCustExInputList[i])->Text.Length() == 0)
      {
        if (bPromptError == true)
        {
          strPrompt = "對不起，"+FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"不能為空";
          MessageBox(NULL, strPrompt.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
        }
        return 1;
      }
      custx.Itemdata[i] = ((TComboBox *)TCustExInputList[i])->Text;
      break;
    case 2: //數值下拉框
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CIsAllowNull == 0 && ((TComboBox *)TCustExInputList[i])->Text.Length() == 0)
      {
        if (bPromptError == true)
        {
          strPrompt = "對不起，"+FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"不能為空";
          MessageBox(NULL, strPrompt.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
        }
        return 1;
      }
      custx.Itemdata[i] = FormMain->m_pCustExItemSetList->m_CustExItemSet[i].TLookupItemList.GetItemValue(((TComboBox *)TCustExInputList[i])->Text);
      break;
    case 3: //長文本框
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CIsAllowNull == 0 && ((TComboBox *)TCustExInputList[i])->Text.Length() == 0)
      {
        if (bPromptError == true)
        {
          strPrompt = "對不起，"+FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"不能為空";
          MessageBox(NULL, strPrompt.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
        }
        return 1;
      }
      custx.Itemdata[i] = ((TEdit *)TCustExInputList[i])->Text;
      custx.Itemdata[i] = custx.Itemdata[i].Trim();
      break;
    case 4: //短文本框
      if (FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CIsAllowNull == 0 && ((TComboBox *)TCustExInputList[i])->Text.Length() == 0)
      {
        if (bPromptError == true)
        {
          strPrompt = "對不起，"+FormMain->m_pCustExItemSetList->m_CustExItemSet[i].CLabelCaption+"不能為空";
          MessageBox(NULL, strPrompt.c_str(),"訊息提示",MB_OK|MB_ICONERROR);
        }
        return 1;
      }
      custx.Itemdata[i] = ((TEdit *)TCustExInputList[i])->Text;
      custx.Itemdata[i] = custx.Itemdata[i].Trim();
      break;
    case 5: //數值框
      custx.Itemdata[i] = ((TCSpinEdit *)TCustExInputList[i])->Value;
      break;
    case 6: //金額框
      if (MyIsFloat(((TEdit *)TCustExInputList[i])->Text) != 0)
      {
        if (bPromptError == true)
          MessageBox(NULL,"對不起，金額框預設值必須為整數或小數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      custx.Itemdata[i] = ((TEdit *)TCustExInputList[i])->Text;
      custx.Itemdata[i] = custx.Itemdata[i].Trim();
      break;
    case 7: //日期框
      custx.Itemdata[i] = ((TDateTimePicker *)TCustExInputList[i])->Date.FormatString("yyyy-mm-dd");
      break;
    case 8: //是非框
      custx.Itemdata[i] = ((TCheckBox *)TCustExInputList[i])->Checked == true ? "1" : "0";
      break;
    }
  }
  return 0;
}

void __fastcall TFormEditCustom::FormDestroy(TObject *Sender)
{
  for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
  {
    if (TCustExLabelList[i] != NULL)
    {
      delete TCustExLabelList[i];
      TCustExLabelList[i] = NULL;
    }
    if (TCustExInputList[i] != NULL)
    {
      delete TCustExInputList[i];
      TCustExInputList[i] = NULL;
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::edtMobileNoChange(TObject *Sender)
{
  BitBtn1->Enabled = true;
  Customer.MobileNoModifyId = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::edtTeleNoChange(TObject *Sender)
{
  BitBtn1->Enabled = true;
  Customer.TeleNoModifyId = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::edtFaxNoChange(TObject *Sender)
{
  BitBtn1->Enabled = true;
  Customer.FaxNoModifyId = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustom::btQryAS400Click(TObject *Sender)
{
  if (GetCustom() != 0)
  {
    return;
  }
  if (GetCustExData(CustEx) != 0)
  {
    return;
  }
  if (CustEx.Itemdata[0] == "" && CustEx.Itemdata[1] == "")
  {
    MessageBox(NULL,"對不起, 身份證字號和會員編號不能全為空白！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (dmAgent->QueryCustExDataFromCRM(&Customer, &CustEx) == 1)
  {
    SetCustom(Customer);
    SetCustEx(CustEx);
  }
}
//---------------------------------------------------------------------------

