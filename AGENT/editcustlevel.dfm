object FormCustLevel: TFormCustLevel
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'FormCustLevel'
  ClientHeight = 118
  ClientWidth = 246
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 15
    Top = 20
    Width = 84
    Height = 12
    Caption = #23458#25143#21010#20998#32534#21495#65306
  end
  object Label2: TLabel
    Left = 15
    Top = 53
    Width = 84
    Height = 12
    Caption = #23458#25143#21010#20998#21517#31216#65306
  end
  object editCustLevelId: TEdit
    Left = 104
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editCustLevelIdChange
  end
  object editCustLevelName: TEdit
    Left = 104
    Top = 48
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editCustLevelIdChange
  end
  object Button1: TButton
    Left = 32
    Top = 80
    Width = 75
    Height = 25
    Caption = #30830#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 144
    Top = 80
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
end
