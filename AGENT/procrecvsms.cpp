//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "procrecvsms.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormRecvSmsProc *FormRecvSmsProc;
extern int g_nBandSeatNoType;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormRecvSmsProc::TFormRecvSmsProc(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType > 0)
  {
    dmAgent->AddCmbItem(FormRecvSmsProc->cbReadFlag, dmAgent->SmsProcTypeItemList, "");
    dmAgent->AddCmbItem(FormRecvSmsProc->cbProcSrvType, dmAgent->RecvSmsTypeItemList, "");
  }
}
//---------------------------------------------------------------------------
void TFormRecvSmsProc::SetRecvSmsData(CRecvSms& recvsms)
{
  RecvSms = recvsms;
  editMobile->Text = recvsms.Mobile;
  editRecvTime->Text = recvsms.RecvTime;
  memoSmsMsg->Lines->Clear();
  memoSmsMsg->Lines->Add(recvsms.SmsMsg);
  cbReadFlag->Text = dmAgent->SmsProcTypeItemList.GetItemName(recvsms.ReadFlag);
  editReadTime->Text = recvsms.ReadTime;
  cbProcSrvType->Text = dmAgent->RecvSmsTypeItemList.GetItemName(recvsms.ProcSrvType);
  memoProcSrvData->Lines->Clear();
  memoProcSrvData->Lines->Add(recvsms.ProcSrvData);
  editProcBy->Text = recvsms.ProcBy;
  editRemark->Text = recvsms.Remark;
  Button1->Enabled = false;
}
int TFormRecvSmsProc::GetRecvSmsData()
{
  RecvSms.ReadFlag = dmAgent->SmsProcTypeItemList.GetItemValue(cbReadFlag->Text);
  RecvSms.ReadTime = Now().FormatString("yyyy-mm-dd hh:nn:ss");
  RecvSms.ProcSrvType = dmAgent->RecvSmsTypeItemList.GetItemValue(cbProcSrvType->Text);
  RecvSms.ProcSrvData = memoProcSrvData->Lines->Text;
  RecvSms.ProcBy = MyWorker.WorkerNo;
  RecvSms.Remark = editRemark->Text;
  return 0;
}

void __fastcall TFormRecvSmsProc::cbReadFlagKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvSmsProc::cbReadFlagChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvSmsProc::Button1Click(TObject *Sender)
{
  if (GetRecvSmsData() != 0)
    return;

  if (dmAgent->UpdateRecvSmsRecord(&RecvSms) == 0)
  {
    dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員處理%s的簡訊記錄", RecvSms.Mobile.c_str());
    MessageBox(NULL,"處理簡訊記錄成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
    dmAgent->adoQryRecvSms->Close();
    dmAgent->adoQryRecvSms->Open();
    this->Close();
  }
  else
  {
    MessageBox(NULL,"處理簡訊記失敗!","訊息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvSmsProc::Button2Click(TObject *Sender)
{
  this->Close(); 
}
//---------------------------------------------------------------------------
