object FormEditSubPhone: TFormEditSubPhone
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20225#26989#20998#27231#36039#26009#31649#29702
  ClientHeight = 239
  ClientWidth = 518
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 44
    Top = 22
    Width = 64
    Height = 14
    AutoSize = False
    Caption = #20998#27231#34399#30908#65306
  end
  object Label2: TLabel
    Left = 44
    Top = 49
    Width = 65
    Height = 14
    AutoSize = False
    Caption = #30331#37636#23494#30908#65306
  end
  object Label3: TLabel
    Left = 304
    Top = 49
    Width = 65
    Height = 14
    AutoSize = False
    Caption = #21729#24037#22995#21517#65306
  end
  object Label4: TLabel
    Left = 304
    Top = 22
    Width = 65
    Height = 14
    AutoSize = False
    Caption = #22806#25765#27402#38480#65306
  end
  object Label5: TLabel
    Left = 44
    Top = 82
    Width = 65
    Height = 14
    AutoSize = False
    Caption = #25163#27231#34399#30908#65306
  end
  object Label6: TLabel
    Left = 20
    Top = 108
    Width = 91
    Height = 14
    AutoSize = False
    Caption = #36935#24537#36681#31227#34399#30908#65306
  end
  object Label7: TLabel
    Left = 256
    Top = 108
    Width = 117
    Height = 14
    AutoSize = False
    Caption = #28961#20154#25509#32893#36681#31227#34399#30908#65306
  end
  object Label8: TLabel
    Left = 280
    Top = 82
    Width = 91
    Height = 14
    AutoSize = False
    Caption = #25163#27231#32129#23450#27169#24335#65306
  end
  object Label9: TLabel
    Left = 68
    Top = 143
    Width = 39
    Height = 14
    AutoSize = False
    Caption = #25391#37428#65306
  end
  object Label10: TLabel
    Left = 256
    Top = 143
    Width = 117
    Height = 14
    AutoSize = False
    Caption = #30041#35328#20449#31665#38283#36890#27161#35468#65306
  end
  object Label11: TLabel
    Left = 68
    Top = 169
    Width = 39
    Height = 14
    AutoSize = False
    Caption = #20633#35387#65306
  end
  object editSubPhone: TEdit
    Left = 113
    Top = 17
    Width = 131
    Height = 21
    TabOrder = 0
    OnChange = editSubPhoneChange
  end
  object editPassword: TEdit
    Left = 113
    Top = 43
    Width = 131
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnChange = editSubPhoneChange
  end
  object editUserName: TEdit
    Left = 373
    Top = 43
    Width = 131
    Height = 21
    TabOrder = 2
    OnChange = editSubPhoneChange
  end
  object cbCallRight: TComboBox
    Left = 373
    Top = 17
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    OnChange = editSubPhoneChange
    OnKeyPress = cbCallRightKeyPress
  end
  object editMobile: TEdit
    Left = 113
    Top = 78
    Width = 131
    Height = 21
    TabOrder = 4
    OnChange = editSubPhoneChange
  end
  object editBusyTran: TEdit
    Left = 113
    Top = 104
    Width = 131
    Height = 21
    TabOrder = 5
    OnChange = editSubPhoneChange
  end
  object editNoAnsTran: TEdit
    Left = 373
    Top = 104
    Width = 131
    Height = 21
    TabOrder = 6
    OnChange = editSubPhoneChange
  end
  object cbBangMode: TComboBox
    Left = 373
    Top = 78
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 7
    OnChange = editSubPhoneChange
    OnKeyPress = cbCallRightKeyPress
  end
  object cbCRBT: TComboBox
    Left = 113
    Top = 139
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 8
    OnChange = editSubPhoneChange
    Items.Strings = (
      'song01.voc'
      'song02.voc'
      'song03.voc'
      'song04.voc'
      'song05.voc'
      'song06.voc'
      'song07.voc'
      'song08.voc'
      'song09.voc'
      'song10.voc'
      'song11.voc'
      'song12.voc'
      'song13.voc'
      'song14.voc'
      'song15.voc'
      'song16.voc'
      'song17.voc'
      'song18.voc'
      'song19.voc'
      'song20.voc')
  end
  object cbVoiceBox: TComboBox
    Left = 373
    Top = 139
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 9
    OnChange = editSubPhoneChange
    OnKeyPress = cbCallRightKeyPress
  end
  object editRemark: TEdit
    Left = 113
    Top = 165
    Width = 391
    Height = 21
    TabOrder = 10
    OnChange = editSubPhoneChange
  end
  object Button1: TButton
    Left = 113
    Top = 199
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 321
    Top = 199
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 12
    OnClick = Button2Click
  end
end
