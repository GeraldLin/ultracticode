object FormEditEndCode2: TFormEditEndCode2
  Left = 192
  Top = 114
  BorderStyle = bsSingle
  Caption = #32080#26463#30908#20108#35373#23450
  ClientHeight = 242
  ClientWidth = 292
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lbItemId: TLabel
    Left = 41
    Top = 21
    Width = 84
    Height = 12
    Caption = #32080#26463#30908#20108#32232#34399#65306
  end
  object lbItemName: TLabel
    Left = 41
    Top = 56
    Width = 84
    Height = 12
    Caption = #32080#26463#30908#20108#21517#31281#65306
  end
  object Label1: TLabel
    Left = 59
    Top = 88
    Width = 66
    Height = 12
    Caption = #32080#26463#30908'URL'#65306
  end
  object Label2: TLabel
    Left = 65
    Top = 120
    Width = 60
    Height = 12
    Caption = #39023#31034#38991#33394#65306
  end
  object Label7: TLabel
    Left = 29
    Top = 157
    Width = 96
    Height = 12
    Caption = #19979#25289#26694#39023#31034#38918#24207#65306
  end
  object editItemId: TEdit
    Left = 130
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editItemIdChange
  end
  object editItemName: TEdit
    Left = 130
    Top = 52
    Width = 131
    Height = 20
    TabOrder = 1
    OnChange = editItemIdChange
  end
  object Button1: TButton
    Left = 35
    Top = 191
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 173
    Top = 191
    Width = 82
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editItemURL: TEdit
    Left = 130
    Top = 84
    Width = 131
    Height = 20
    TabOrder = 4
    OnChange = editItemIdChange
  end
  object cbColor: TComboBox
    Left = 130
    Top = 116
    Width = 133
    Height = 20
    ItemHeight = 12
    TabOrder = 5
    Text = 'clBlack'
    OnChange = editItemIdChange
    OnKeyPress = cbColorKeyPress
    Items.Strings = (
      'clBlack'
      'clMaroon'
      'clGreen'
      'clOlive'
      'clNavy'
      'clPurple'
      'clTeal'
      'clGray'
      'clSilver'
      'clRed'
      'clLime'
      'clYellow'
      'clBlue'
      'clFuchsia'
      'clAqua'
      'clLtGray'
      'clDkGray'
      'clWhite'
      'clMoneyGreen'
      'clSkyBlue'
      'clCream'
      'clMedGray')
  end
  object cseDispOrder: TCSpinEdit
    Left = 130
    Top = 152
    Width = 133
    Height = 21
    TabOrder = 6
    OnChange = editItemIdChange
  end
end
