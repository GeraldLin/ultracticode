//---------------------------------------------------------------------------

#ifndef editivrmenusetH
#define editivrmenusetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormIVRMenuSet : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TEdit *editCalledNo;
  TComboBox *cbWellcomeId;
  TComboBox *cbVocMenuId;
  TEdit *editSeatNo;
  TComboBox *editWorkerNo;
  TButton *Button1;
  TButton *Button2;
  void __fastcall cbWellcomeIdKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editCalledNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormIVRMenuSet(TComponent* Owner);

  CIVRMenuSet IVRMenuSet;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetModifyType(bool isinsert);
  void SetIVRMenuSet(CIVRMenuSet &ivrmenuset);
  int GetIVRMenuSet();
  void ClearIVRMenuSet();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormIVRMenuSet *FormIVRMenuSet;
//---------------------------------------------------------------------------
#endif
