//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "updateversion.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormUpdateVersion *FormUpdateVersion;

extern bool g_bUpdatePrompt;
extern AnsiString strCurVersion, strSrvVersion, strUpdateVersion;
//---------------------------------------------------------------------------
__fastcall TFormUpdateVersion::TFormUpdateVersion(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormUpdateVersion::ckUpdatePromptClick(TObject *Sender)
{
  g_bUpdatePrompt = !ckUpdatePrompt->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TFormUpdateVersion::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormUpdateVersion::Button1Click(TObject *Sender)
{
  if (dmAgent->DownloadMyAgent() == 0)
  {
    strUpdateVersion = strSrvVersion;
    MessageBox(NULL,"更新電腦坐席程式成功，請重啟坐席程式！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
  else
  {
    MessageBox(NULL,"更新電腦坐席程式失敗！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
  this->Close();
}
//---------------------------------------------------------------------------
