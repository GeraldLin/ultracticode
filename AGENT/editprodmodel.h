//---------------------------------------------------------------------------

#ifndef editprodmodelH
#define editprodmodelH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditProdModel : public TForm
{
__published:	// IDE-managed Components
  TLabel *lbItemId;
  TLabel *lbItemName;
  TEdit *editItemId;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label1;
  TEdit *editItemURL;
  TMemo *memoDemo;
  TLabel *Label2;
  TLabel *Label7;
  TCSpinEdit *cseDispOrder;
  TLabel *Label8;
  TEdit *editURLBtnName;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemIdChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditProdModel(TComponent* Owner);

  int  ProdType;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetDispMsg(bool bInsertId, int nProdType, int nProdModelType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditProdModel *FormEditProdModel;
//---------------------------------------------------------------------------
#endif
