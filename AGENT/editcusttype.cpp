//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editcusttype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditCustType *FormEditCustType;
//---------------------------------------------------------------------------
__fastcall TFormEditCustType::TFormEditCustType(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustType::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustType::editCustTypeChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustType::Button1Click(TObject *Sender)
{
  int nCustType;

  if (MyIsNumber(editCustType->Text) != 0)
  {
    MessageBox(NULL,"請輸入客戶類型編號！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editCustTypeName->Text == "")
  {
    MessageBox(NULL,"請輸入客戶類型名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nCustType = StrToInt(editCustType->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsCustTypeExist(nCustType) == 1)
    {
      MessageBox(NULL,"對不起，該客戶類型編號已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertCustTypeRecord(nCustType, editCustTypeName->Text) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員話務技能組編號為：%d的資料", nSrvType);
      dmAgent->adoQryCustType->Close();
      dmAgent->adoQryCustType->Open();
      dmAgent->UpdateCustTypeListItems();
      MessageBox(NULL,"增加客戶類型名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加客戶類型名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateCustTypeRecord(nCustType, editCustTypeName->Text) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改話務技能組編號為：%d的資料", nSrvType);
      dmAgent->adoQryCustType->Close();
      dmAgent->adoQryCustType->Open();
      dmAgent->UpdateCustTypeListItems();
      MessageBox(NULL,"修改客戶類型名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改客戶類型名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
