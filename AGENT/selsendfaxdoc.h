//---------------------------------------------------------------------------

#ifndef selsendfaxdocH
#define selsendfaxdocH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <FileCtrl.hpp>
//---------------------------------------------------------------------------
class TFormSendFaxDocList : public TForm
{
__published:	// IDE-managed Components
  TFileListBox *FileListBox1;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSendFaxDocList(TComponent* Owner);

  int m_nDocId;
  AnsiString m_strSendFaxDoc;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSendFaxDocList *FormSendFaxDocList;
//---------------------------------------------------------------------------
#endif
