//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editpassword.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditPassword *FormEditPassword;
extern CMyWorker MyWorker;
//2017-09-05 新增密碼策略
extern int g_nPWDPolicyID;
extern int g_nPWDModifyPeriod;
extern int g_nPWDExpirePeriod;
extern int g_nPWDPolicyAdmin;
extern int CheckPWDPolicy(AnsiString strPWD);
//---------------------------------------------------------------------------
__fastcall TFormEditPassword::TFormEditPassword(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormEditPassword::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditPassword::editOldPswChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditPassword::Button1Click(TObject *Sender)
{
  AnsiString strOldPsw;
  int nWorkerType;
  if (dmAgent->GetWorkerPsw(MyWorker.WorkerNo, strOldPsw, nWorkerType) == 0)
  {
    MessageBox(NULL,"對不起，該工號不存在！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (strOldPsw != editOldPsw->Text)
  {
    MessageBox(NULL,"對不起，您輸入的原密碼不正確！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editNewPsw1->Text == "")
  {
    MessageBox(NULL,"對不起，密碼不能為空！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editNewPsw1->Text != editNewPsw2->Text)
  {
    MessageBox(NULL,"對不起，確認的新密碼與新密碼不相同！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (g_nPWDPolicyID == 1)
  {
    if (nWorkerType < 9 || (nWorkerType == 9 && g_nPWDPolicyAdmin == 1))
    {
      if (CheckPWDPolicy(editNewPsw1->Text) != 0)
      {
        MessageBox(NULL,"對不起，密碼至少要包含一個數字,一個大寫英文字母,一個小寫英文字母,至少有8碼！","訊息提示",MB_OK|MB_ICONERROR);
        return;
      }
    }
  }
  if (dmAgent->UpdateWorkerPsw(MyWorker.WorkerNo, editNewPsw1->Text) == 0)
  {
    MessageBox(NULL,"值機員登錄密碼修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    this->Close();
  }
  else
  {
    MessageBox(NULL,"對不起，值機員登錄密碼修改失敗！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------
