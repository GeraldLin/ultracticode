object FormAllocDialTask: TFormAllocDialTask
  Left = 192
  Top = 114
  BorderStyle = bsSingle
  Caption = #20998#27966#22806#25765#20219#21209
  ClientHeight = 161
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label3: TLabel
    Left = 36
    Top = 31
    Width = 60
    Height = 12
    Caption = #22519#27231#32232#34399#65306
  end
  object cbWorkerNo: TComboBox
    Left = 100
    Top = 27
    Width = 205
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    OnKeyPress = cbWorkerNoKeyPress
  end
  object Button1: TButton
    Left = 64
    Top = 64
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 224
    Top = 64
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 2
    OnClick = Button2Click
  end
  object Panel1: TPanel
    Left = 16
    Top = 96
    Width = 313
    Height = 41
    Caption = #27491#22312#20998#37197#22806#25765#34399#30908'(0/0'#26781')'#65292#35531#31245#31561'......'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
  end
end
