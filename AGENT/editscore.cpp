//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editscore.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditScore *FormEditScore;
//---------------------------------------------------------------------------
__fastcall TFormEditScore::TFormEditScore(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormEditScore::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditScore::editScoreIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditScore::Button1Click(TObject *Sender)
{
  int nScoreId;

  if (MyIsNumber(editScoreId->Text) != 0)
  {
    MessageBox(NULL,"請輸入服務評分分值！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editScoreName->Text == "")
  {
    MessageBox(NULL,"請輸入服務評分名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nScoreId = StrToInt(editScoreId->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsScoreTypeExist(nScoreId) == 1)
    {
      MessageBox(NULL,"對不起，該服務評分分值已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertScoreTypeRecord(nScoreId, editScoreName->Text) == 0)
    {
      dmAgent->adoQryScore->Close();
      dmAgent->adoQryScore->Open();
      dmAgent->UpdateScoreTypeListItems();
      MessageBox(NULL,"增加服務評分分值成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加服務評分分值失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateScoreTypeRecord(nScoreId, editScoreName->Text) == 0)
    {
      dmAgent->adoQryScore->Close();
      dmAgent->adoQryScore->Open();
      dmAgent->UpdateScoreTypeListItems();
      MessageBox(NULL,"修改服務評分分值成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改服務評分分值失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
