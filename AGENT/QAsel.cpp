//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "QAsel.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormQASel *FormQASel;
//---------------------------------------------------------------------------
__fastcall TFormQASel::TFormQASel(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormQASel::Button3Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormQASel::Button2Click(TObject *Sender)
{
  FormMain->MyAgent1->Hangon(1);
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormQASel::Button1Click(TObject *Sender)
{
  AnsiString TranParam;
  WideString wTranParam;
  BSTR bTranParam;

  TranParam = "QUESTCODE="+IntToStr(RadioGroup1->ItemIndex+1)+";"+IntToStr(RadioGroup2->ItemIndex+1);
  wTranParam = WideString(TranParam);
  bTranParam = ( wchar_t * )wTranParam;
  FormMain->MyAgent1->TranIVR(1, 0, bTranParam);
  this->Close();
}
//---------------------------------------------------------------------------
