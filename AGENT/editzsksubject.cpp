//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editzsksubject.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormZSKSubject *FormZSKSubject;
extern int g_nBandSeatNoType;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
AnsiString ImageFile;
AnsiString AffixFile;
AnsiString strSelId="";
TListItem *SelListItem=NULL;

//---------------------------------------------------------------------------
__fastcall TFormZSKSubject::TFormZSKSubject(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
  bModify = false;
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormZSKSubject->cbIssueStatus, dmAgent->ZSKIssueStatusItemList, "");
    dmAgent->AddCmbItem(FormZSKSubject->cbSecrecyId, dmAgent->CustClassList, "");
    dmAgent->AddCmbItem(FormZSKSubject->cbDepartmentId, dmAgent->DepartmentItemList, "");
    FormZSKSubject->cbDepartmentId->Items->Add("所有部門");
  }
}
//---------------------------------------------------------------------------
void TFormZSKSubject::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  bModify=false;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormZSKSubject->Caption = "新增業務知識庫題庫訊息";
  }
  else
  {
    Button1->Caption = "修改";
    FormZSKSubject->Caption = "修改業務知識庫題庫訊息";
  }
  if (MyWorker.WorkType > 2)
  {
    Label5->Visible = true;
    cbDepartmentId->Visible = true;
    Label4->Visible = true;
    cbIssueStatus->Visible = true;
  }
  else
  {
    Label5->Visible = false;
    cbDepartmentId->Visible = false;
    Label4->Visible = false;
    cbIssueStatus->Visible = false;
  }
}
void TFormZSKSubject::SetZSKSubjectData(CZSKSubject& zsksubject)
{
  ZSKSubject = zsksubject;
  editMainTitle->Text = zsksubject.MainTitle;
  editSubTitle->Text = zsksubject.SubTitle;
  editSubjectTitle->Text = zsksubject.SubjectTitle;
  memoExplainCont->Lines->Clear();
  memoExplainCont->Lines->Add(zsksubject.ExplainCont);
  editRemark->Text = zsksubject.Remark;
  editVersionNo->Text = zsksubject.VersionNo;

  cbIssueStatus->Text = dmAgent->ZSKIssueStatusItemList.GetItemName(zsksubject.IssueStatus);
  cbSecrecyId->Text = dmAgent->CustClassList.GetItemName(zsksubject.SecrecyId);
  cbDepartmentId->Text = dmAgent->DepartmentItemList.GetItemName(zsksubject.DepartmentId);

  cseOrderNo->Value = zsksubject.orderno3;

  Button1->Enabled = false;
}
int TFormZSKSubject::GetZSKSubjectData()
{
  //ZSKSubject.SubjectNo;
  //ZSKSubject.SubNo;
  //ZSKSubject.MainNo;
  ZSKSubject.MainTitle = editMainTitle->Text;
  ZSKSubject.SubTitle = editSubTitle->Text;
  if (editSubjectTitle->Text == "")
  {
    MessageBox(NULL,"請輸入題庫標題！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  ZSKSubject.SubjectTitle = editSubjectTitle->Text;
  ZSKSubject.ExplainCont = memoExplainCont->Lines->Text;
  ZSKSubject.IssueStatus = dmAgent->ZSKIssueStatusItemList.GetItemValue(cbIssueStatus->Text);
  ZSKSubject.SecrecyId = dmAgent->CustClassList.GetItemValue(cbSecrecyId->Text);
  if (cbDepartmentId->Text == "所有部門")
    ZSKSubject.DepartmentId = 0;
  else
    ZSKSubject.DepartmentId = dmAgent->DepartmentItemList.GetItemValue(cbDepartmentId->Text);
  ZSKSubject.Remark = editRemark->Text;
  ZSKSubject.VersionNo = editVersionNo->Text;
  ZSKSubject.orderno3 = cseOrderNo->Value;
  return 0;
}
void TFormZSKSubject::ClearZSKSubjectData(CZSKSub& zsksub)
{
  ZSKSubject.SubjectNo = 0;
  ZSKSubject.Gid = dmAgent->MyGetGIUD();
  ZSKSubject.SubNo = zsksub.SubNo;
  ZSKSubject.MainNo = zsksub.MainNo;
  editMainTitle->Text = zsksub.MainTitle;
  editSubTitle->Text = zsksub.SubTitle;
  editSubjectTitle->Text = "";
  memoExplainCont->Lines->Clear();
  editRemark->Text = "";
  editVersionNo->Text = "1.0.0";
  ZSKSubject.IssueMan = MyWorker.WorkerNo;
  cbIssueStatus->Text = dmAgent->ZSKIssueStatusItemList.GetItemName(2);
  cbSecrecyId->Text = dmAgent->CustClassList.GetItemName(3);
  cbDepartmentId->Text = dmAgent->DepartmentItemList.GetItemName(MyWorker.DepartmentID);
  lvAppendix->Items->Clear();

  Button1->Enabled = false;
}

void __fastcall TFormZSKSubject::cbIssueStatusKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::editSubjectTitleChange(TObject *Sender)
{
  bModify = true;
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::Button1Click(TObject *Sender)
{
  TListItem  *ListItem1;

  if (bModify == true)
  {
  if (GetZSKSubjectData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertZSKSubjectRecord(ZSKSubject) == 0)
    {
      for (int i=0; i<lvAppendix->Items->Count; i++)
      {
        ListItem1 = lvAppendix->Items->Item[i];
        if (ListItem1->Caption == "0-0")
          dmAgent->InsertZSKAppendix(ZSKSubject.Gid, ListItem1->SubItems->Strings[1].c_str(), ListItem1->SubItems->Strings[0].c_str());
      }

      dmAgent->RefreshZSKSubject();
      dmAgent->InsertOPLog(MySeat, MyWorker, 1030, "值機員新增業務知識庫為：%s的題庫訊息資料", ZSKSubject.SubjectTitle.c_str());
      //MessageBox(NULL,"新增業務知識庫題庫訊息成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增業務知識庫題庫訊息失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateZSKSubjectRecord(ZSKSubject) == 0)
    {
      for (int i=0; i<lvAppendix->Items->Count; i++)
      {
        ListItem1 = lvAppendix->Items->Item[i];
        if (ListItem1->Caption == "0-0")
          dmAgent->InsertZSKAppendix(ZSKSubject.Gid, ListItem1->SubItems->Strings[1].c_str(), ListItem1->SubItems->Strings[0].c_str());
      }

      dmAgent->RefreshZSKSubject();
      dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "值機員修改業務知識庫為：%s的題庫訊息資料", ZSKSubject.SubjectTitle.c_str());
      //MessageBox(NULL,"修改業務知識庫題庫訊息成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改業務知識庫題庫訊息失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  }
  else
  {
    this->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::Button3Click(TObject *Sender)
{
  TListItem  *ListItem1;

  if (OpenDialog2->Execute())
  {
    ListItem1 = lvAppendix->Items->Add();
    ListItem1->Caption = "0-0";
    ListItem1->SubItems->Insert(0, ExtractFileName(OpenDialog2->FileName));
    ListItem1->SubItems->Insert(1, ExtractFilePath(OpenDialog2->FileName));

    bModify = true;
    Button1->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::cbIssueStatusChange(TObject *Sender)
{
  ZSKSubject.CheckTime = DateTimeToStr(Now());
  ZSKSubject.CheckMan = MyWorker.WorkerNo;
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::FormShow(TObject *Sender)
{
  FormZSKSubject->editSubjectTitle->SetFocus();  
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::N1Click(TObject *Sender)
{
  int nType, nId;
  char szType[4]={0}, szId[32]={0};
  char sqlbuf[256], sqlbuf1[256];

  if ( MessageBox( NULL, "您真的要刪除選擇的附件嗎?", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
    return;

  SelListItem->Delete();
  if (strSelId == "" || strSelId == "0-0")
    return;

  sscanf(strSelId.c_str(), "%4[0-9]-%32[0-9]", szType, szId);
  nId = atoi(szId);
  nType = atoi(szType);
  if (nId == 0 || nType == 0)
    return;
  if (nType == 1)
  {
    ZSKSubject.ExplainImage = "";
    sprintf( sqlbuf, "update tbZSKSubject set ExplainImage=NULL where SubjectNo=%d", nId);
    sprintf( sqlbuf1, "update tbZSKSubject set ImageFileName='' where SubjectNo=%d", nId);
  }
  else if (nType == 2)
  {
    ZSKSubject.ExplainFile = "";
    sprintf( sqlbuf, "update tbZSKSubject set ExplainFile=NULL where SubjectNo=%d", nId);
    sprintf( sqlbuf1, "update tbZSKSubject set ExplainFileName='' where SubjectNo=%d", nId);
  }
  else if (nType == 3)
  {
    sprintf( sqlbuf, "delete from tbZSKAppendix where id=%d", nId);
    memset(sqlbuf1, 0, 256);
  }
  else
  {
    return;
  }
  if (strlen(sqlbuf1) > 0)
  {
    try
    {
      dmAgent->adoqueryPub->SQL->Clear();
      dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf1 );
      dmAgent->adoqueryPub->ExecSQL();
    }
    catch ( ... )
    {
    }
  }
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
  }
  catch ( ... )
  {
  }
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormZSKSubject::lvAppendixSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == true)
  {
    strSelId = Item->Caption;
    SelListItem = Item;
  }
}
//---------------------------------------------------------------------------

