//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editsrvtype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditSrvType *FormEditSrvType;
//---------------------------------------------------------------------------
__fastcall TFormEditSrvType::TFormEditSrvType(TComponent* Owner)
  : TForm(Owner)
{
  ProdType = 0;
}
//---------------------------------------------------------------------------
void TFormEditSrvType::SetDispMsg(bool bInsertId, int nProdType, int nSrvType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  InsertId = bInsertId;
  ProdType = nProdType;
  if (InsertId == true)
  {
    editSrvType->Text = "";
    editSrvType->Color = clWindow;
    editSrvType->ReadOnly = false;
    editSrvName->Text = "";
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = "";
    memoDemo->Lines->Clear();
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editSrvType->Text = nSrvType;
    editSrvType->Color = clSilver;
    editSrvType->ReadOnly = true;
    editSrvName->Text = strItemName;
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = strItemURL;
    memoDemo->Lines->Clear();
    if (strDemo.Length() > 0)
      memoDemo->Lines->Add(strDemo);
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}
void __fastcall TFormEditSrvType::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSrvType::editSrvTypeChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSrvType::Button1Click(TObject *Sender)
{
  int nSrvType;

  if (MyIsNumber(editSrvType->Text) != 0)
  {
    MessageBox(NULL,"請輸入業務代碼！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editSrvName->Text == "")
  {
    MessageBox(NULL,"請輸入業務名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nSrvType = StrToInt(editSrvType->Text);
  if (nSrvType <= 0)
  {
    MessageBox(NULL,"對不起，業務代碼必須大于0！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsSrvTypeExist(ProdType, nSrvType) == 1)
    {
      MessageBox(NULL,"對不起，該業務編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertSrvTypeRecord(ProdType, nSrvType, editSrvName->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQrySrvType->Close();
      dmAgent->adoQrySrvType->Open();
      dmAgent->UpdateSrvTypeListItems();
      this->Close();
      //MessageBox(NULL,"新增業務編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增業務編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateSrvTypeRecord(ProdType, nSrvType, editSrvName->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQrySrvType->Close();
      dmAgent->adoQrySrvType->Open();
      dmAgent->UpdateSrvTypeListItems();
      this->Close();
      //MessageBox(NULL,"修改業務編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改業務編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSrvType::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditSrvType->editSrvType->SetFocus();
  else
    FormEditSrvType->editSrvName->SetFocus();
}
//---------------------------------------------------------------------------

