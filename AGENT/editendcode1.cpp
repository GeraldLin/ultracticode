//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editendcode1.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditEndCode1 *FormEditEndCode1;
//---------------------------------------------------------------------------
__fastcall TFormEditEndCode1::TFormEditEndCode1(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void TFormEditEndCode1::SetDispMsg(bool bInsertId, int nEndCode1, AnsiString strItemName, int nDispOrder)
{
  InsertId = bInsertId;
  if (InsertId == true)
  {
    editEndCode1->Text = "";
    editEndCode1->Color = clWindow;
    editEndCode1->ReadOnly = false;
    editEndCodeName->Text = "";
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editEndCode1->Text = nEndCode1;
    editEndCode1->Color = clSilver;
    editEndCode1->ReadOnly = true;
    editEndCodeName->Text = strItemName;
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}
void __fastcall TFormEditEndCode1::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditEndCode1::editEndCode1Change(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditEndCode1::Button1Click(TObject *Sender)
{
  int nEndCode1;

  if (MyIsNumber(editEndCode1->Text) != 0)
  {
    MessageBox(NULL,"請輸入結束碼一編號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editEndCodeName->Text == "")
  {
    MessageBox(NULL,"請輸入結束碼一名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nEndCode1 = StrToInt(editEndCode1->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsEndCode1Exist(nEndCode1) == 1)
    {
      MessageBox(NULL,"對不起，該結束碼一編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertEndCode1Record(nEndCode1, editEndCodeName->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryEndCode1->Close();
      dmAgent->adoQryEndCode1->Open();
      dmAgent->UpdateEndCode1ListItems();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"新增結束碼一編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateEndCode1Record(nEndCode1, editEndCodeName->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQryEndCode1->Close();
      dmAgent->adoQryEndCode1->Open();
      dmAgent->UpdateEndCode1ListItems();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改結束碼一編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditEndCode1::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditEndCode1->editEndCode1->SetFocus();
  else
    FormEditEndCode1->editEndCodeName->SetFocus();
}
//---------------------------------------------------------------------------
