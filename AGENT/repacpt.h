//---------------------------------------------------------------------------

#ifndef repacptH
#define repacptH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <QuickRpt.hpp>
#include <QRCtrls.hpp>
//---------------------------------------------------------------------------
class TFormRepAcpt : public TForm
{
__published:	// IDE-managed Components
  TQuickRep *QuickRep1;
  TQRBand *PageHeaderBand1;
  TQRBand *DetailBand1;
  TQRBand *PageFooterBand1;
  TQRSysData *QRSysData2;
  TQRLabel *QRLabel1;
  TQRLabel *QRLabelTotal;
  TQRLabel *QRLabel2;
  TQRLabel *QRLabel3;
  TQRLabel *QRLabel4;
  TQRLabel *QRLabel5;
  TQRLabel *QRLabel6;
  TQRLabel *QRLabel7;
  TQRLabel *QRLabel8;
  TQRDBText *QRDBText1;
  TQRSysData *QRSysData1;
  TQRDBText *QRDBText2;
  TQRDBText *QRDBText3;
  TQRDBText *QRDBText4;
  TQRDBText *QRDBText5;
  TQRDBText *QRDBText6;
  TQRDBText *QRDBText7;
private:	// User declarations
public:		// User declarations
  __fastcall TFormRepAcpt(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRepAcpt *FormRepAcpt;
//---------------------------------------------------------------------------
#endif
