//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editsrvsubtype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditSrvSubType *FormEditSrvSubType;
//---------------------------------------------------------------------------
__fastcall TFormEditSrvSubType::TFormEditSrvSubType(TComponent* Owner)
  : TForm(Owner)
{
  ProdType = 0;
}
//---------------------------------------------------------------------------
void TFormEditSrvSubType::SetDispMsg(bool bInsertId, int nProdType, int nSrvType, int nSrvSubType, AnsiString strItemName, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder)
{
  InsertId = bInsertId;
  ProdType = nProdType;
  SrvType = nSrvType;
  if (InsertId == true)
  {
    editItemId->Text = "";
    editItemId->Color = clWindow;
    editItemId->ReadOnly = false;
    editItemName->Text = "";
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = "";
    memoDemo->Lines->Clear();
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "新增";
    Button1->Enabled = false;
  }
  else
  {
    editItemId->Text = nSrvSubType;
    editItemId->Color = clSilver;
    editItemId->ReadOnly = true;
    editItemName->Text = strItemName;
    editURLBtnName->Text = strURLBtnName;
    editItemURL->Text = strItemURL;
    memoDemo->Lines->Clear();
    if (strDemo.Length() > 0)
      memoDemo->Lines->Add(strDemo);
    cseDispOrder->Value = nDispOrder;
    Button1->Caption = "修改";
    Button1->Enabled = false;
  }
}

void __fastcall TFormEditSrvSubType::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSrvSubType::editItemIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSrvSubType::FormShow(TObject *Sender)
{
  if (InsertId == true)
    editItemId->SetFocus();
  else
    editItemName->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSrvSubType::Button1Click(TObject *Sender)
{
  char dispbuf[256];

  if (MyIsNumber(editItemId->Text) != 0)
  {
    MessageBox(NULL,"對不起,您輸入的子業務編號有誤!","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,"對不起,請輸入子業務名稱!","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  int nSrvSubType = StrToInt(editItemId->Text);
  if (nSrvSubType <= 0)
  {
    MessageBox(NULL,"對不起，子業務編號必須大于0！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsSrvSubTypeExist(ProdType, SrvType, nSrvSubType) == 1)
    {
      MessageBox(NULL,"對不起,您輸入的子業務編號已存在!","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertSrvSubType(ProdType, SrvType, nSrvSubType, editItemName->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQrySrvSubType->Close();
      dmAgent->adoQrySrvSubType->Open();
      this->Close();
      //MessageBox(NULL,"增加子業務編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"增加子業務編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateSrvSubType(ProdType, SrvType, nSrvSubType, editItemName->Text, editURLBtnName->Text, editItemURL->Text, memoDemo->Lines->Text, cseDispOrder->Value) == 0)
    {
      dmAgent->adoQrySrvSubType->Close();
      dmAgent->adoQrySrvSubType->Open();
      this->Close();
      //MessageBox(NULL,"修改子業務編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改子業務編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

