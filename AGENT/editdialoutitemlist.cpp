//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editdialoutitemlist.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditDialOutItemList *FormEditDialOutItemList;
//---------------------------------------------------------------------------
__fastcall TFormEditDialOutItemList::TFormEditDialOutItemList(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void TFormEditDialOutItemList::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    editItemNo->ReadOnly = false;
    editItemNo->Color = clWindow;
  }
  else
  {
    Button1->Caption = "修改";
    editItemNo->ReadOnly = true;
    editItemNo->Color = clSilver;
  }
}
void TFormEditDialOutItemList::SetItemData(CDialOutItemSet& dialoutitemset, AnsiString strTaskName, AnsiString strItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo)
{
  DialOutItemSet = dialoutitemset;
  editTaskName->Text = strTaskName;
  editLabelCaption->Text = DialOutItemSet.TLabelCaption;
  editItemNo->Text = strItemNo;
  editItemName->Text = strItemName;
  editItemURL->Text = strItemURL;
  memoItemDemo->Lines->Clear();
  if (strItemDemo.Length() > 0)
    memoItemDemo->Lines->Add(strItemDemo);
  Button1->Enabled = false;
}


void __fastcall TFormEditDialOutItemList::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDialOutItemList::editItemNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDialOutItemList::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditDialOutItemList->editItemNo->SetFocus();
  else
    FormEditDialOutItemList->editItemName->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDialOutItemList::Button1Click(TObject *Sender)
{
  int nItemNo;

  if (MyIsNumber(editItemNo->Text) != 0)
  {
    MessageBox(NULL,"請輸入下拉框選擇項編號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,"請輸入下拉框選擇項名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  nItemNo = StrToInt(editItemNo->Text);
  if (DialOutItemSet.TMinValue != 0 && DialOutItemSet.TMaxValue != 0 && (nItemNo < DialOutItemSet.TMinValue || nItemNo > DialOutItemSet.TMaxValue))
  {
    MessageBox(NULL,"對不起，您輸入的下拉框選擇項編號超出允許範圍！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsDialOutItemListExist(DialOutItemSet.TaskId, DialOutItemSet.Tid, nItemNo) == 1)
    {
      MessageBox(NULL,"對不起，該下拉框選擇項編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertDialOutItemListRecord(DialOutItemSet.TaskId, DialOutItemSet.Tid, nItemNo, editItemName->Text, editItemURL->Text, memoItemDemo->Lines->Text) == 0)
    {
      dmAgent->adoQryDialOutItemList->Close();
      dmAgent->adoQryDialOutItemList->Open();
      this->Close();
      //MessageBox(NULL,"新增下拉框選擇項編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增下拉框選擇項編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateDialOutItemListRecord(DialOutItemSet.TaskId, DialOutItemSet.Tid, nItemNo, editItemName->Text, editItemURL->Text, memoItemDemo->Lines->Text) == 0)
    {
      dmAgent->adoQryDialOutItemList->Close();
      dmAgent->adoQryDialOutItemList->Open();
      this->Close();
      //MessageBox(NULL,"修改下拉框選擇項編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改下拉框選擇項編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

