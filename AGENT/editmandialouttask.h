//---------------------------------------------------------------------------

#ifndef editmandialouttaskH
#define editmandialouttaskH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditManDialOutTask : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editTaskName;
  TLabel *Label8;
  TEdit *editCallerNo;
  TLabel *Label6;
  TComboBox *cbStartId;
  TLabel *Label2;
  TDateTimePicker *dtpStartDate;
  TLabel *Label3;
  TDateTimePicker *dtpEndDate;
  TLabel *Label4;
  TEdit *editParam;
  TLabel *Label5;
  TMemo *memoRemark;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label7;
  TEdit *editURLAddr;
  TLabel *Label9;
  TEdit *editTaskCode;
  TLabel *Label10;
  TEdit *editURLOrder;
  TLabel *Label11;
  TEdit *editURLQuery;
  TLabel *Label12;
  TEdit *editURLManOrder;
  TLabel *Label13;
  TEdit *editURLPrice;
  TLabel *Label14;
  TEdit *editDialPreCode;
  void __fastcall cbStartIdKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall editTaskNameChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditManDialOutTask(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CManDialOutTask ManDialOutTask;
  void SetModifyType(bool isinsert);
  void SetDialOutTaskData(CManDialOutTask& mandialouttask);
  int GetDialOutTaskData();
  void ClearDialOutTaskData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditManDialOutTask *FormEditManDialOutTask;
//---------------------------------------------------------------------------
#endif
