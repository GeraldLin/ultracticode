//---------------------------------------------------------------------------

#ifndef editorderpayH
#define editorderpayH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditOrderPay : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label167;
  TComboBox *cbOIsNeedBill;
  TLabel *Label168;
  TEdit *edtOBillName;
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TEdit *edtOProductTotal;
  TEdit *edtOOrderAmount;
  TEdit *edtOExpressFee;
  TEdit *edtOPayAmount;
  TComboBox *cbOPayState;
  TEdit *edtOPayMen;
  TEdit *edtOPayedAmount;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label8;
  TEdit *edtOOrderId;
  TLabel *Label9;
  TEdit *edtOPayedTime;
  TButton *Button3;
  TLabel *Label11;
  TComboBox *cbOOrderState;
  TLabel *Label12;
  TEdit *edtODiscountAmount;
  TMonthCalendar *MonthCalendar1;
  TLabel *Label13;
  TEdit *editONoPayAmount;
  void __fastcall cbOIsNeedBillKeyPress(TObject *Sender, char &Key);
  void __fastcall cbOIsNeedBillChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall MonthCalendar1Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbOPayStateChange(TObject *Sender);
  void __fastcall edtOPayedAmountExit(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditOrderPay(TComponent* Owner);

  //int GetDateId;
  int oldOrderStateId;
  int oldPayStateId;

  CMyOrder MyOrder;
  void SetOrderPayData(CMyOrder &myorder);
  int GetOrderPayData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditOrderPay *FormEditOrderPay;
//---------------------------------------------------------------------------
#endif
