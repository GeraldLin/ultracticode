object FormEditEndCode1: TFormEditEndCode1
  Left = 192
  Top = 114
  BorderStyle = bsSingle
  Caption = #22806#25765#32080#26463#30908#19968#35373#23450
  ClientHeight = 176
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 19
    Top = 13
    Width = 84
    Height = 13
    Caption = #32080#26463#30908#19968#32232#34399#65306
  end
  object Label2: TLabel
    Left = 19
    Top = 47
    Width = 84
    Height = 13
    Caption = #32080#26463#30908#19968#21517#31281#65306
  end
  object Label7: TLabel
    Left = 7
    Top = 85
    Width = 96
    Height = 13
    Caption = #19979#25289#26694#39023#31034#38918#24207#65306
  end
  object editEndCodeName: TEdit
    Left = 111
    Top = 44
    Width = 131
    Height = 21
    TabOrder = 0
    OnChange = editEndCode1Change
  end
  object Button1: TButton
    Left = 43
    Top = 127
    Width = 82
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 156
    Top = 127
    Width = 81
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 2
    OnClick = Button2Click
  end
  object editEndCode1: TEdit
    Left = 111
    Top = 9
    Width = 131
    Height = 21
    TabOrder = 3
    OnChange = editEndCode1Change
  end
  object cseDispOrder: TCSpinEdit
    Left = 111
    Top = 80
    Width = 133
    Height = 22
    TabOrder = 4
    OnChange = editEndCode1Change
  end
end
