//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "callin1.h"
#include "main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormCallin1 *FormCallin1;
extern BSTR bEmpty;
extern CMySeat MySeat;
extern bool FlashId;
//呼叫信息
extern CCallMsg CallMsg;
extern CAccept MyAccept;
//---------------------------------------------------------------------------
__fastcall TFormCallin1::TFormCallin1(TComponent* Owner)
  : TForm(Owner)
{
}

void TFormCallin1::SetCallerNo(AnsiString callerno)
{
  lblCallerNo->Caption = callerno;
}
void TFormCallin1::SetCalledNo(AnsiString calledno, int inout)
{
  if (inout == 2)
  {
    Label7->Visible = false;
    lblCalledNo->Visible = false;
    lblCalledNo->Caption = "";
  }
  else
  {
    Label7->Visible = true;
    lblCalledNo->Visible = true;
    lblCalledNo->Caption = calledno;
  }
}
void TFormCallin1::SetTelOfCity(AnsiString city)
{
  Label12->Caption = "號碼類別："+city;
}
void TFormCallin1::SetTranPhone(int nacdcalltype, const char *transeatno)
{
  AnsiString strTranSeatNo = (char *)transeatno;
  if (nacdcalltype == 1)
  {
    lbTranPhone->Caption = "轉接的電話，轉接的坐席號："+strTranSeatNo;
    lbTranPhone->Visible = true;
  }
  else if (nacdcalltype == 2)
  {
    lbTranPhone->Caption = "轉接返回的電話，轉接的坐席號："+strTranSeatNo;
    lbTranPhone->Visible = true;
  }
  else
  {
    lbTranPhone->Caption = "";
    lbTranPhone->Visible = false;
  }
}

void TFormCallin1::SetCustom(CCustomer *customer)
{
  edtCustomNo->Text = customer->CustomNo; //客戶編號
  edtCustName->Text = customer->CustName; //客戶名稱
  edtCorpName->Text = customer->CorpName; //公司名稱
  edtCustType->Text = dmAgent->CustTypeItemList.GetItemName(customer->CustType); //客戶類型
  edtCustSex->Text = customer->CustSex; //性別
  if (customer->MobileNo != "")
    edtMobileNo->Text = GetHidePhoneNum(0, customer->MobileNo); //手機
  else
    edtMobileNo->Text = GetHidePhoneNum(0, customer->TeleNo); //固話
  //edtFaxNo->Text = customer->FaxNo; //傳真
  //edtEMail->Text = customer->EMail; //郵箱
  edtContAddr->Text = customer->ContAddr; //地址
  edtProvince->Text = customer->Province;
  edtCityName->Text = customer->CityName;
  //edtCountyName->Text = customer->CountyName;
  edtAccountNo->Text = customer->AccountNo;
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin1::SpeedButton2Click(TObject *Sender)
{
  FormMain->MyAgent1->AnswerCall(2, bEmpty, bEmpty);
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin1::FormCreate(TObject *Sender)
{
  if (MySeat.SeatType == 2)
  {
    Button1->Caption = "接聽";
  }
}
//---------------------------------------------------------------------------


void __fastcall TFormCallin1::FormKeyPress(TObject *Sender, char &Key)
{
  if (Key == VK_BACK)
  {
    if ((MySeat.SeatType == 2 && CallMsg.CallInOut == 1) || MyAccept.MediaType == 1)
    {
      FormMain->MyAgent1->AnswerCall(1, bEmpty, bEmpty);
    }
    this->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormCallin1::Button1Click(TObject *Sender)
{
  if ((MySeat.SeatType == 2 && CallMsg.CallInOut == 1) || MyAccept.MediaType == 1)
  {
    FormMain->MyAgent1->AnswerCall(1, bEmpty, bEmpty);
  }
  this->Close();
}
//---------------------------------------------------------------------------
void TFormCallin1::SetSrvType(CAccept &accept)
{
  if (accept.AcptType > 0)
    lbSrvType->Caption = "業務類型："+dmAgent->ProdTypeItemList.GetItemName(accept.ProdType)+"->"+dmAgent->SrvTypeItemList.GetItemName(accept.ProdType, accept.AcptType);
  else
    lbSrvType->Caption = "業務類型："+dmAgent->ProdTypeItemList.GetItemName(accept.ProdType);
}
void TFormCallin1::SetScreenPop(int nPopId)
{
  lbSrvType->Caption = "業務類型："+dmAgent->GetPopString(nPopId);
}

