//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "u_About.h"
//--------------------------------------------------------------------- 
#pragma resource "*.dfm"
TAboutBox *AboutBox;
//--------------------------------------------------------------------- 
__fastcall TAboutBox::TAboutBox(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void TAboutBox::GetVersionInformation()
{
  static bool bGetid=false;

  if (bGetid == true)
    return;
  bGetid = true;
  //首先獲得版本信息資源的長度
  DWORD dwHandle,InfoSize;
  InfoSize = GetFileVersionInfoSize(Application->ExeName.c_str(),&dwHandle);
  //將版本信息資源讀入緩沖區
  char *InfoBuf = new char[InfoSize];
  GetFileVersionInfo(Application->ExeName.c_str(),0,InfoSize,InfoBuf);
  //獲得生成文件使用的代碼頁及字符集信息
  char *pInfoVal;
  unsigned int dwInfoValSize;
  VerQueryValue(InfoBuf,"\\VarFileInfo\\Translation",&((void *)pInfoVal), &dwInfoValSize);

  AnsiString V;
  AnsiString ver = "\\StringFileInfo\\"+IntToHex(*((unsigned short int *)pInfoVal),4)+IntToHex(*((unsigned short int *)&pInfoVal[2]),4)+"\\";
  Memo1->Lines->Clear();
  //獲得產品名稱
  V=ver+"ProductName";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("產品名稱："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  //獲得產品版本
  V=ver+"ProductVersion";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("產品版本："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  //獲得文件版本
  V=ver+"FileVersion";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("文件版本："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  //獲得內部名稱
  V=ver+"InternalName";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("內部名稱："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));
  /*
  //獲得原始文件名
  V=ver+"OriginalFilename";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("原文件名："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  //獲得文件描述
  V=ver+"FileDescription";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("文件描述："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  //獲得合法版權
  V=ver+"LegalCopyright";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("合法版權："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  //獲得公司名稱
  V=ver+"CompanyName";
  VerQueryValue(InfoBuf, V.c_str(),&((void *)pInfoVal),&dwInfoValSize);
  Memo1->Lines->Add("公司名稱："+AnsiString(pInfoVal).SetLength(dwInfoValSize-1));

  Memo1->Lines->Add("公司網址：www.mitaccomm.com.tw");
  */
  //delete InfoBuf;
}
void __fastcall TAboutBox::OKButtonClick(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

