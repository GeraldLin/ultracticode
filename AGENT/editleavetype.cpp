//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editleavetype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditLeaveType *FormEditLeaveType;
//---------------------------------------------------------------------------
__fastcall TFormEditLeaveType::TFormEditLeaveType(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditLeaveType::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditLeaveType::editLeaveIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditLeaveType::Button1Click(TObject *Sender)
{
  int nLeaveId;

  if (MyIsNumber(editLeaveId->Text) != 0)
  {
    MessageBox(NULL,"請輸入離席狀態標志！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editLeavename->Text == "")
  {
    MessageBox(NULL,"請輸入離席狀態名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nLeaveId = StrToInt(editLeaveId->Text);
  if (nLeaveId == 0 || nLeaveId > 9)
  {
    MessageBox(NULL,"對不起，離席狀態標志范圍為1-9！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsLeaveTypeExist(nLeaveId) == 1)
    {
      MessageBox(NULL,"對不起，該離席狀態標志已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertLeaveTypeRecord(nLeaveId, editLeavename->Text) == 0)
    {
      dmAgent->adoQryLeaveType->Close();
      dmAgent->adoQryLeaveType->Open();
      dmAgent->UpdateLeaveTypeListItems();
      MessageBox(NULL,"增加離席狀態成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加離席狀態失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateLeaveTypeRecord(nLeaveId, editLeavename->Text) == 0)
    {
      dmAgent->adoQryLeaveType->Close();
      dmAgent->adoQryLeaveType->Open();
      dmAgent->UpdateLeaveTypeListItems();
      MessageBox(NULL,"修改離席狀態成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改離席狀態失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
