//---------------------------------------------------------------------------

#ifndef seatparamH
#define seatparamH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormSeatParam : public TForm
{
__published:	// IDE-managed Components
  TGroupBox *GroupBox2;
  TLabel *Label15;
  TLabel *Label37;
  TLabel *Label16;
  TLabel *Label35;
  TLabel *Label36;
  TComboBox *cbxDBType;
  TEdit *edtServer;
  TEdit *edtDB;
  TEdit *edtUSER;
  TEdit *edtPSW;
  TButton *btSaveParam;
  TButton *btClose;
  TGroupBox *GroupBox3;
  TLabel *Label6;
  TLabel *Label7;
  TEdit *edServerIP;
  TCSpinEdit *cspServerPort;
  TLabel *Label12;
  TEdit *edSeatIP;
  TLabel *Label1;
  TComboBox *cbAutoReady;
  TLabel *Label52;
  TEdit *edSeatNo;
  TLabel *Label2;
  TComboBox *cbGetSeatNo;
  TCheckBox *ckDBParamType;
  TLabel *Label3;
  TCSpinEdit *cspServerID;
  void __fastcall btCloseClick(TObject *Sender);
  void __fastcall btSaveParamClick(TObject *Sender);
  void __fastcall cbxDBTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall cspServerPortChange(TObject *Sender);
  void __fastcall cbGetSeatNoChange(TObject *Sender);
  void __fastcall ckDBParamTypeClick(TObject *Sender);
  void __fastcall cbxDBTypeChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSeatParam(TComponent* Owner);

  void ReadIniFile();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSeatParam *FormSeatParam;
//---------------------------------------------------------------------------
#endif
