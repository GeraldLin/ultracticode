//---------------------------------------------------------------------------

#ifndef dispzsksubjectH
#define dispzsksubjectH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormDispZSKSubject : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label11;
  TLabel *Label8;
  TLabel *Label12;
  TLabel *Label13;
  TLabel *Label14;
  TEdit *editMainTitle;
  TEdit *editRemark;
  TMemo *memoExplainCont;
  TButton *Button2;
  TEdit *editSubTitle;
  TEdit *editSubjectTitle;
  TEdit *editExplainImage;
  TEdit *editExplainFile;
  TButton *Button3;
  TButton *Button4;
  TLabel *Label15;
  TEdit *editVersionNo;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button4Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDispZSKSubject(TComponent* Owner);

  int SubjectNo;
  void SetZSKSubjectData(CZSKSubject& zsksubject);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDispZSKSubject *FormDispZSKSubject;
//---------------------------------------------------------------------------
#endif
