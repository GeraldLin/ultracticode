//---------------------------------------------------------------------------

#ifndef editworkgroupH
#define editworkgroupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormWorkerGroup : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editGroupNo;
  TEdit *editGroupName;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label3;
  TCSpinEdit *cseACWTimer;
  TLabel *Label4;
  TEdit *editDialoutPreCode;
  TLabel *Label16;
  TComboBox *cbDepartmentID;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editGroupNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormWorkerGroup(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
};
//---------------------------------------------------------------------------
extern PACKAGE TFormWorkerGroup *FormWorkerGroup;
//---------------------------------------------------------------------------
#endif
