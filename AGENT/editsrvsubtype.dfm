object FormEditSrvSubType: TFormEditSrvSubType
  Left = 187
  Top = 114
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #23376#26989#21209#39006#22411#35373#23450
  ClientHeight = 371
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lbItemId: TLabel
    Left = 20
    Top = 21
    Width = 63
    Height = 12
    Caption = #23376#26989#21209#32232#34399':'
  end
  object lbItemName: TLabel
    Left = 270
    Top = 21
    Width = 63
    Height = 12
    Caption = #23376#26989#21209#21517#31281':'
  end
  object Label1: TLabel
    Left = 22
    Top = 85
    Width = 62
    Height = 12
    Caption = #23376#26989#21209'URL:'
  end
  object Label2: TLabel
    Left = 8
    Top = 112
    Width = 51
    Height = 12
    Caption = #35441#34899#33139#26412':'
  end
  object Label7: TLabel
    Left = 237
    Top = 107
    Width = 96
    Height = 12
    Caption = #19979#25289#26694#39023#31034#38918#24207#65306
  end
  object Label8: TLabel
    Left = 5
    Top = 53
    Width = 83
    Height = 12
    Caption = 'URL'#25353#37397#21517#31281#65306
  end
  object editItemId: TEdit
    Left = 90
    Top = 17
    Width = 150
    Height = 20
    TabOrder = 0
    OnChange = editItemIdChange
  end
  object editItemName: TEdit
    Left = 338
    Top = 17
    Width = 150
    Height = 20
    TabOrder = 1
    OnChange = editItemIdChange
  end
  object Button1: TButton
    Left = 83
    Top = 327
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 349
    Top = 327
    Width = 82
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editItemURL: TEdit
    Left = 90
    Top = 80
    Width = 399
    Height = 20
    TabOrder = 4
    OnChange = editItemIdChange
  end
  object memoDemo: TMemo
    Left = 8
    Top = 128
    Width = 481
    Height = 185
    MaxLength = 3500
    ScrollBars = ssBoth
    TabOrder = 5
    OnChange = editItemIdChange
  end
  object cseDispOrder: TCSpinEdit
    Left = 339
    Top = 102
    Width = 75
    Height = 21
    TabOrder = 6
    OnChange = editItemIdChange
  end
  object editURLBtnName: TEdit
    Left = 90
    Top = 48
    Width = 150
    Height = 20
    TabOrder = 7
    OnChange = editItemIdChange
  end
end
