//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editcustexset.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditCustExSet *FormEditCustExSet;

extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormEditCustExSet::TFormEditCustExSet(TComponent* Owner)
  : TForm(Owner)
{
  InsertId = false;
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormEditCustExSet->cbControlType, dmAgent->ControlTypeItemList, "");
  }
}
//---------------------------------------------------------------------------
void TFormEditCustExSet::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    editFieldId->ReadOnly = false;
    editFieldId->Color = clWindow;
    FormEditCustExSet->Caption = "新增來電者資料自定義項目";
  }
  else
  {
    Button1->Caption = "修改";
    editFieldId->ReadOnly = true;
    editFieldId->Color = clSilver;
    FormEditCustExSet->Caption = "修改來電者資料自定義項目";
  }
}
void TFormEditCustExSet::SetCustExItemSetData(CCustExItemSet& custexitemset)
{
  CustExItemSet = custexitemset;

  editFieldId->Text = CustExItemSet.CFieldId;
  editLabelCaption->Text = CustExItemSet.CLabelCaption;
  cbControlType->Text = dmAgent->ControlTypeItemList.GetItemName(CustExItemSet.CControlType);

  switch (custexitemset.CWidth)
  {
    case 0: cbWith->Text = "標准寬度"; break;
    case 1: cbWith->Text = "標准寬度四分之一"; break;
    case 2: cbWith->Text = "標准寬度二分之一"; break;
    case 3: cbWith->Text = "標准寬度四分之三"; break;
    default: cbWith->Text = "標准寬度"; break;
  }

  editDefaultData->Text = CustExItemSet.CDefaultData;
  cseMinValue->Value = CustExItemSet.CMinValue;
  cseMaxValue->Value = CustExItemSet.CMaxValue;
  ckIsAllowNull->Checked = (CustExItemSet.CIsAllowNull == 0) ? false : true;
  ckDispId->Checked = (CustExItemSet.CDispId == 0) ? false : true;
  if (CustExItemSet.CInputId == 0)
  {
    ckInputId->Checked = false;
    ckCanEdit->Checked = false;
  }
  else if (CustExItemSet.CInputId == 1)
  {
    ckInputId->Checked = true;
    ckCanEdit->Checked = true;
  }
  else
  {
    ckInputId->Checked = true;
    ckCanEdit->Checked = false;
  }
  ckSearchId->Checked = (CustExItemSet.CSearchId == 0) ? false : true;
  ckIsUsed->Checked = (CustExItemSet.CIsUsed == 0) ? false : true;
  ckExportId->Checked = (CustExItemSet.CExportId == 0) ? false : true;
  if (cbControlType->Text == "短文本框" || cbControlType->Text == "長文本框")
  {
    if (CustExItemSet.CLabelCaption == "生日")
    {
      Label6->Caption = "最小字位數：";
      Label5->Caption = "提醒最大年齡：";
    }
    else
    {
      Label6->Caption = "最小字位數：";
      Label5->Caption = "最大字位數：";
    }
  }
  else
  {
    Label6->Caption = "最小值：";
    Label5->Caption = "最大值：";
  }
  cseExportOrderId->Value = CustExItemSet.ExportOrderId;
  cseAcceptExId->Value = CustExItemSet.AcceptExId;

  if (CustExItemSet.ReadDataType == 1)
    cbReadDataType->Text = "DB資料庫";
  else if (CustExItemSet.ReadDataType == 2)
    cbReadDataType->Text = "HTTP介面";
  else if (CustExItemSet.ReadDataType == 3)
    cbReadDataType->Text = "WEBSERVICE介面";
  else if (CustExItemSet.ReadDataType == 4)
    cbReadDataType->Text = "同步CRM資料";
  else
    cbReadDataType->Text = "DB資料庫";
  editReadFieldTag->Text = CustExItemSet.ReadFieldTag;
  ckSaveToDBFlag->Checked = CustExItemSet.SaveToDBFlag == 1 ? true : false;
}
int TFormEditCustExSet::GetCustExItemSetData()
{
  if (MyIsNumber(editFieldId->Text) != 0)
  {
    MessageBox(NULL,"請輸入來電者資料項目序號！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  int nFieldId = StrToInt(editFieldId->Text);
  if (nFieldId < 1 || nFieldId > MAX_CUST_CONTROL_NUM)
  {
    MessageBox(NULL,"對不起，來電者資料項目序號取值範圍為1-30","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }

  if (editLabelCaption->Text == "")
  {
    MessageBox(NULL,"請輸入來電者資料項目名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  CustExItemSet.CFieldId = nFieldId;
  CustExItemSet.CFieldName = "ItemData"+IntToStr(nFieldId);
  CustExItemSet.CLabelCaption = editLabelCaption->Text;
  CustExItemSet.CControlType = dmAgent->ControlTypeItemList.GetItemValue(cbControlType->Text);

  if (cbWith->Text == "標准寬度")
    CustExItemSet.CWidth = 0;
  else if (cbWith->Text == "標准寬度四分之一")
    CustExItemSet.CWidth = 1;
  else if (cbWith->Text == "標准寬度二分之一")
    CustExItemSet.CWidth = 2;
  else if (cbWith->Text == "標准寬度四分之三")
    CustExItemSet.CWidth = 3;

  CustExItemSet.CDefaultData = editDefaultData->Text;
  if (CustExItemSet.CControlType == 2)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsNumber(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，選項類型預設值必須為整數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      if (cseMinValue->Value != 0 && cseMaxValue->Value != 0 &&
        (StrToInt(editDefaultData->Text) < cseMinValue->Value || StrToInt(editDefaultData->Text) > cseMaxValue->Value))
      {
        MessageBox(NULL,"對不起，選項類型預設值不在設置的最小最大值範圍內","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      CustExItemSet.CDefaultData = IntToStr(cseMinValue->Value);
    }
  }
  else if (CustExItemSet.CControlType == 5)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsNumber(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，數值類型預設值必須為整數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
      if (cseMinValue->Value != 0 && cseMaxValue->Value != 0 &&
        (StrToInt(editDefaultData->Text) < cseMinValue->Value || StrToInt(editDefaultData->Text) > cseMaxValue->Value))
      {
        MessageBox(NULL,"對不起，數值類型預設值不在設置的最小最大值範圍內","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      CustExItemSet.CDefaultData = IntToStr(cseMinValue->Value);
    }
  }
  else if (CustExItemSet.CControlType == 6)
  {
    if (editDefaultData->Text != "")
    {
      if (MyIsFloat(editDefaultData->Text) != 0)
      {
        MessageBox(NULL,"對不起，金額類型預設值必須為整數或小數","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      CustExItemSet.CDefaultData = "0";
    }
  }
  else if (CustExItemSet.CControlType == 7)
  {
    if (editDefaultData->Text != "" && MyIsDate(editDefaultData->Text) != 0)
    {
      MessageBox(NULL,"對不起，日期類型預設值必須為正確的日期","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
  }
  else if (CustExItemSet.CControlType == 8)
  {
    if (editDefaultData->Text != "")
    {
      if (editDefaultData->Text != "1" && editDefaultData->Text != "0")
      {
        MessageBox(NULL,"對不起，是非類型預設值必須為0或1","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
    else
    {
      CustExItemSet.CDefaultData = "1";
    }
  }
  CustExItemSet.CMinValue = cseMinValue->Value;
  CustExItemSet.CMaxValue = cseMaxValue->Value;

  (ckIsAllowNull->Checked == true) ? CustExItemSet.CIsAllowNull=1 : CustExItemSet.CIsAllowNull=0;
  (ckDispId->Checked == true) ? CustExItemSet.CDispId=1 : CustExItemSet.CDispId=0;

  if (ckInputId->Checked == true && ckCanEdit->Checked == true)
    CustExItemSet.CInputId = 1;
  else if (ckInputId->Checked == true && ckCanEdit->Checked == false)
    CustExItemSet.CInputId = 2;
  else
    CustExItemSet.CInputId = 0;

  (ckSearchId->Checked == true) ? CustExItemSet.CSearchId=1 : CustExItemSet.CSearchId=0;
  (ckIsUsed->Checked == true) ? CustExItemSet.CIsUsed=1 : CustExItemSet.CIsUsed=0;
  (ckExportId->Checked == true) ? CustExItemSet.CExportId=1 : CustExItemSet.CExportId=0;
  CustExItemSet.ExportOrderId = cseExportOrderId->Value;
  CustExItemSet.AcceptExId = cseAcceptExId->Value;

  if (cbReadDataType->Text == "DB資料庫")
    CustExItemSet.ReadDataType = 1;
  else if (cbReadDataType->Text == "HTTP介面")
    CustExItemSet.ReadDataType = 2;
  else if (cbReadDataType->Text == "WEBSERVICE介面")
    CustExItemSet.ReadDataType = 3;
  else if (cbReadDataType->Text == "同步CRM資料")
    CustExItemSet.ReadDataType = 4;
  else
    CustExItemSet.ReadDataType = 1;
  CustExItemSet.ReadFieldTag = editReadFieldTag->Text;
  if (ckSaveToDBFlag->Checked == true)
    CustExItemSet.SaveToDBFlag = 1;
  else
    CustExItemSet.SaveToDBFlag = 0;
  return 0;
}
void TFormEditCustExSet::ClearCustExtItemSetData()
{
  editFieldId->Text = "";
  editLabelCaption->Text = "";
  cbControlType->Text = dmAgent->ControlTypeItemList.GetItemName(4);
  cbWith->Text = "標准寬度";
  editDefaultData->Text = "";
  cseMinValue->Value = 0;
  cseMaxValue->Value = 0;
  ckIsAllowNull->Checked = true;
  ckDispId->Checked = true;
  ckInputId->Checked = true;
  ckCanEdit->Checked = true;
  ckSearchId->Checked = false;
  ckIsUsed->Checked = true;
  ckExportId->Checked = false;
  cseExportOrderId->Value = 0;
  cseAcceptExId->Value = 0;
  cbReadDataType->Text = "DB資料庫";
  editReadFieldTag->Text = "";
  ckSaveToDBFlag->Checked = true;
}
void __fastcall TFormEditCustExSet::cbControlTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExSet::Button2Click(TObject *Sender)
{
  if (InsertId == false)
    ClearCustExtItemSetData();
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExSet::editFieldIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExSet::Button1Click(TObject *Sender)
{
  CCustExItemSet custexitemset;
  if (GetCustExItemSetData() != 0)
  {
    return;
  }
  if (InsertId == true)
  {
    if (dmAgent->IsCustExItemSetFieldIdExist(CustExItemSet.CFieldId) == 1)
    {
      MessageBox(NULL,"對不起，該來電者資料項目序號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertCustExItemSet(&CustExItemSet) == 0)
    {
      dmAgent->adoQryCustExSet->Close();
      dmAgent->adoQryCustExSet->Open();
      if (dmAgent->GetCustExItemSetRecord(dmAgent->adoQryCustExSet, &custexitemset) == 0)
      {
        FormMain->QueryCurCustExItemList(custexitemset);
      }
      else
      {
        FormMain->gbCustExItemList->Visible = false;
        dmAgent->adoQryCustExFieldList->Close();
      }
      ClearCustExtItemSetData();
      this->Close();
      //MessageBox(NULL,"新增來電者資料項目成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增來電者資料項目失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateCustExItemSet(&CustExItemSet) == 0)
    {
      dmAgent->adoQryCustExSet->Close();
      dmAgent->adoQryCustExSet->Open();
      if (dmAgent->GetCustExItemSetRecord(dmAgent->adoQryCustExSet, &custexitemset) == 0)
      {
        FormMain->QueryCurCustExItemList(custexitemset);
      }
      else
      {
        FormMain->gbCustExItemList->Visible = false;
        dmAgent->adoQryCustExFieldList->Close();
      }
      ClearCustExtItemSetData();
      this->Close();
      //MessageBox(NULL,"來電者資料項目修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"來電者資料項目修改失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExSet::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditCustExSet->editFieldId->SetFocus();
  else
    FormEditCustExSet->editLabelCaption->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditCustExSet::cbControlTypeChange(TObject *Sender)
{
  Button1->Enabled = true;
  if (cbControlType->Text == "短文本框" || cbControlType->Text == "長文本框")
  {
    Label6->Caption = "最小字位數：";
    Label5->Caption = "最大字位數：";
  }
  else
  {
    Label6->Caption = "最小值：";
    Label5->Caption = "最大值：";
  }
}
//---------------------------------------------------------------------------

