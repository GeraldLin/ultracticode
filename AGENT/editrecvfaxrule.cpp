//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editrecvfaxrule.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormRecvFaxRule *FormRecvFaxRule;
extern int g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormRecvFaxRule::TFormRecvFaxRule(TComponent* Owner)
  : TForm(Owner)
{
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddWorkerNameCmbItem(FormRecvFaxRule->cbWorkerNo);
    dmAgent->AddDepartmentCmbItem(FormRecvFaxRule->cbDepartment);
  }
  nRuleType = 3;
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvFaxRule::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvFaxRule::editRuleItemChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvFaxRule::cbRuleTypeChange(TObject *Sender)
{
  if (cbRuleType->Text == "公司公共傳真")
  {
    nRuleType = 1;
    Label2->Visible = false;
    cbDepartment->Visible = false;
    Label3->Visible = false;
    cbWorkerNo->Visible = false;
  }
  else if (cbRuleType->Text == "部門公共傳真")
  {
    nRuleType = 2;
    Label2->Visible = true;
    cbDepartment->Visible = true;
    Label3->Visible = false;
    cbWorkerNo->Visible = false;
  }
  else if (cbRuleType->Text == "個人傳真")
  {
    nRuleType = 3;
    Label2->Visible = false;
    cbDepartment->Visible = false;
    Label3->Visible = true;
    cbWorkerNo->Visible = true;
  }
}
//---------------------------------------------------------------------------
void TFormRecvFaxRule::SetModifyType(int nruleid, bool isinsert)
{
  AnsiString itemname;

  InsertId = isinsert;
  nSelectRuleId = nruleid;
  if (nruleid == 1)
    itemname = "通道號";
  else if (nruleid == 2)
    itemname = "被叫號碼";
  else if (nruleid == 3)
    itemname = "主叫號碼";
  else if (nruleid == 4)
    itemname = "傳真標識";

  Label1->Caption = itemname+"：";
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    FormRecvFaxRule->Caption = "新增按"+itemname+"分發傳真規則";
  }
  else
  {
    Button1->Caption = "修改";
    FormRecvFaxRule->Caption = "修改按"+itemname+"分發傳真規則";
  }
}
void TFormRecvFaxRule::SetRecvFaxRuleSet(CRecvFaxRuleSet &ruleset)
{
  RecvFaxRuleSet = ruleset;

  editRuleItem->Text = RecvFaxRuleSet.RuleItem;
  if (RecvFaxRuleSet.DepartmentId == 0 && (RecvFaxRuleSet.WorkerNo == "0" || RecvFaxRuleSet.WorkerNo == ""))
  {
    cbRuleType->Text = "公司公共傳真";
    nRuleType = 1;
    Label2->Visible = false;
    cbDepartment->Visible = false;
    Label3->Visible = false;
    cbWorkerNo->Visible = false;
  }
  else if (RecvFaxRuleSet.DepartmentId != 0 && (RecvFaxRuleSet.WorkerNo == "0" || RecvFaxRuleSet.WorkerNo == ""))
  {
    cbRuleType->Text = "部門公共傳真";
    nRuleType = 2;
    Label2->Visible = true;
    cbDepartment->Visible = true;
    cbDepartment->Text = dmAgent->DepartmentItemList.GetItemName(RecvFaxRuleSet.DepartmentId);
    Label3->Visible = false;
    cbWorkerNo->Visible = false;
  }
  else
  {
    cbRuleType->Text = "個人傳真";
    nRuleType = 3;
    Label2->Visible = false;
    cbDepartment->Visible = false;
    Label3->Visible = true;
    cbWorkerNo->Visible = true;
    cbWorkerNo->Text = dmAgent->GetWorkerName(RecvFaxRuleSet.WorkerNo);
    if (cbWorkerNo->Text == "")
      cbWorkerNo->Text = RecvFaxRuleSet.WorkerNo;
  }
}
int TFormRecvFaxRule::GetRecvFaxRuleSet()
{
  AnsiString strWorkerNo;
  
  if (nSelectRuleId == 1)
  {
    if (MyIsNumber(editRuleItem->Text) != 0)
    {
      MessageBox(NULL,"對不起，通道號輸入錯誤！","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
  }
  else if (nSelectRuleId == 2 || nSelectRuleId == 3)
  {
    if (MyIsDigits(editRuleItem->Text) != 0)
    {
      MessageBox(NULL,"對不起，號碼輸入錯誤！","訊息提示",MB_OK|MB_ICONERROR);
      return 1;
    }
  }
  RecvFaxRuleSet.RuleItem = editRuleItem->Text;
  if (nRuleType == 1)
  {
    RecvFaxRuleSet.DepartmentId = 0;
    RecvFaxRuleSet.WorkerNo = "0";
  }
  else if (nRuleType == 2)
  {
    RecvFaxRuleSet.DepartmentId = dmAgent->DepartmentItemList.GetItemValue(cbDepartment->Text);
    RecvFaxRuleSet.WorkerNo = "0";
  }
  else if (nRuleType == 3)
  {
    RecvFaxRuleSet.DepartmentId = 0;

    if (dmAgent->GetWorkerNo(cbWorkerNo->Text, strWorkerNo) == 1)
    {
      RecvFaxRuleSet.WorkerNo = strWorkerNo;
    }
    else
    {
      if (MyIsDigits(cbWorkerNo->Text) == 0)
      {
        RecvFaxRuleSet.WorkerNo = cbWorkerNo->Text;
      }
      else
      {
        MessageBox(NULL,"對不起，該值機員不存在！","訊息提示",MB_OK|MB_ICONERROR);
        return 1;
      }
    }
  }
  return 0;
}
void TFormRecvFaxRule::ClearRecvFaxRuleSet()
{
  editRuleItem->Text = "";
  cbRuleType->Text = "個人傳真";
  nRuleType = 3;
  Label2->Visible = false;
  cbDepartment->Visible = false;
  Label3->Visible = true;
  cbWorkerNo->Visible = true;
  cbWorkerNo->Text = "";
}
void __fastcall TFormRecvFaxRule::Button1Click(TObject *Sender)
{
  if (GetRecvFaxRuleSet() != 0)
  {
    return;
  }
  if (InsertId == true)
  {
    if (dmAgent->IsRecvFaxRuleItemIdExist(nSelectRuleId, RecvFaxRuleSet.RuleItem) == 1)
    {
      MessageBox(NULL,"該規則參數已存在","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertRecvFaxRuleRecord(nSelectRuleId, &RecvFaxRuleSet) == 0)
    {
      dmAgent->adoQryRecvFaxRule->Close();
      dmAgent->adoQryRecvFaxRule->Open();
      ClearRecvFaxRuleSet();
      this->Close();
      //MessageBox(NULL,"新增傳真分發規則成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增傳真分發規則失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateRecvFaxRuleRecord(nSelectRuleId, &RecvFaxRuleSet) == 0)
    {
      dmAgent->adoQryRecvFaxRule->Close();
      dmAgent->adoQryRecvFaxRule->Open();
      ClearRecvFaxRuleSet();
      this->Close();
      //MessageBox(NULL,"傳真分發規則修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"傳真分發規則修改失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormRecvFaxRule::FormShow(TObject *Sender)
{
  FormRecvFaxRule->editRuleItem->SetFocus();  
}
//---------------------------------------------------------------------------

