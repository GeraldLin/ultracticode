//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "selsendfaxdoc.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormSendFaxDocList *FormSendFaxDocList;

extern AnsiString strSendFaxTaskId;
//---------------------------------------------------------------------------
__fastcall TFormSendFaxDocList::TFormSendFaxDocList(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSendFaxDocList::Button2Click(TObject *Sender)
{
  m_strSendFaxDoc = ""; 
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSendFaxDocList::Button1Click(TObject *Sender)
{
  m_strSendFaxDoc = FileListBox1->FileName;
  int nResult = FormMain->UpLoadSendFaxDoc(m_nDocId, m_strSendFaxDoc);
  if (nResult == 0)
  {
    switch (m_nDocId)
    {
    case 1: FormMain->btDelUpLoaddoc1->Visible = true; break;
    case 2: FormMain->btDelUpLoaddoc2->Visible = true; break;
    case 3: FormMain->btDelUpLoaddoc3->Visible = true; break;
    case 4: FormMain->btDelUpLoaddoc4->Visible = true; break;
    case 5: FormMain->btDelUpLoaddoc5->Visible = true; break;
    }
    strSendFaxTaskId = "";
    MessageBox(NULL,"上傳文件成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
    this->Close();
  }
  else if (nResult == 1)
  {
    switch (m_nDocId)
    {
    case 1: FormMain->btDelUpLoaddoc1->Visible = false; break;
    case 2: FormMain->btDelUpLoaddoc2->Visible = false; break;
    case 3: FormMain->btDelUpLoaddoc3->Visible = false; break;
    case 4: FormMain->btDelUpLoaddoc4->Visible = false; break;
    case 5: FormMain->btDelUpLoaddoc5->Visible = false; break;
    }
    MessageBox(NULL,"上傳文件失敗!","訊息提示",MB_OK|MB_ICONERROR);
  }
  else if (nResult == 2)
  {
    switch (m_nDocId)
    {
    case 1: FormMain->btDelUpLoaddoc1->Visible = false; break;
    case 2: FormMain->btDelUpLoaddoc2->Visible = false; break;
    case 3: FormMain->btDelUpLoaddoc3->Visible = false; break;
    case 4: FormMain->btDelUpLoaddoc4->Visible = false; break;
    case 5: FormMain->btDelUpLoaddoc5->Visible = false; break;
    }
    MessageBox(NULL,"登綠伺服器失敗!","訊息提示",MB_OK|MB_ICONERROR);
  }
  else if (nResult == 3)
  {
    switch (m_nDocId)
    {
    case 1: FormMain->btDelUpLoaddoc1->Visible = false; break;
    case 2: FormMain->btDelUpLoaddoc2->Visible = false; break;
    case 3: FormMain->btDelUpLoaddoc3->Visible = false; break;
    case 4: FormMain->btDelUpLoaddoc4->Visible = false; break;
    case 5: FormMain->btDelUpLoaddoc5->Visible = false; break;
    }
    MessageBox(NULL,"打開文件失敗!","訊息提示",MB_OK|MB_ICONERROR);
  }
  else if (nResult == 4)
  {
    switch (m_nDocId)
    {
    case 1: FormMain->btDelUpLoaddoc1->Visible = false; break;
    case 2: FormMain->btDelUpLoaddoc2->Visible = false; break;
    case 3: FormMain->btDelUpLoaddoc3->Visible = false; break;
    case 4: FormMain->btDelUpLoaddoc4->Visible = false; break;
    case 5: FormMain->btDelUpLoaddoc5->Visible = false; break;
    }
    MessageBox(NULL,"文件太大超過8M!","訊息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------
