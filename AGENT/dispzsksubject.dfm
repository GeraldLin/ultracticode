object FormDispZSKSubject: TFormDispZSKSubject
  Left = 192
  Top = 114
  Width = 688
  Height = 477
  Caption = #39023#31034#30693#35672#24235#38988#24235#35338#24687
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 13
    Width = 90
    Height = 14
    AutoSize = False
    Caption = #20027#20998#39006#27161#38988#65306
  end
  object Label3: TLabel
    Left = 28
    Top = 116
    Width = 71
    Height = 14
    AutoSize = False
    Caption = #35443#32048#35498#26126#65306
  end
  object Label11: TLabel
    Left = 55
    Top = 376
    Width = 44
    Height = 14
    AutoSize = False
    Caption = #20633#35387#65306
  end
  object Label8: TLabel
    Left = 12
    Top = 48
    Width = 90
    Height = 14
    AutoSize = False
    Caption = #23376#20998#39006#27161#38988#65306
  end
  object Label12: TLabel
    Left = 27
    Top = 82
    Width = 75
    Height = 14
    AutoSize = False
    Caption = #38988#24235#27161#38988#65306
  end
  object Label13: TLabel
    Left = 27
    Top = 316
    Width = 75
    Height = 14
    AutoSize = False
    Caption = #35299#31572#22294#29255#65306
  end
  object Label14: TLabel
    Left = 27
    Top = 351
    Width = 75
    Height = 14
    AutoSize = False
    Caption = #35299#31572#38468#20214#65306
  end
  object Label15: TLabel
    Left = 470
    Top = 82
    Width = 55
    Height = 14
    AutoSize = False
    Caption = #29256#26412#34399#65306
  end
  object editMainTitle: TEdit
    Left = 104
    Top = 9
    Width = 556
    Height = 21
    ReadOnly = True
    TabOrder = 0
  end
  object editRemark: TEdit
    Left = 104
    Top = 373
    Width = 556
    Height = 21
    ReadOnly = True
    TabOrder = 1
  end
  object memoExplainCont: TMemo
    Left = 104
    Top = 113
    Width = 556
    Height = 191
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object Button2: TButton
    Left = 303
    Top = 407
    Width = 82
    Height = 27
    Caption = #38364#38281
    TabOrder = 3
    OnClick = Button2Click
  end
  object editSubTitle: TEdit
    Left = 104
    Top = 43
    Width = 556
    Height = 21
    ReadOnly = True
    TabOrder = 4
  end
  object editSubjectTitle: TEdit
    Left = 104
    Top = 78
    Width = 356
    Height = 21
    ReadOnly = True
    TabOrder = 5
  end
  object editExplainImage: TEdit
    Left = 104
    Top = 312
    Width = 521
    Height = 21
    ReadOnly = True
    TabOrder = 6
  end
  object editExplainFile: TEdit
    Left = 104
    Top = 347
    Width = 521
    Height = 21
    ReadOnly = True
    TabOrder = 7
  end
  object Button3: TButton
    Left = 633
    Top = 312
    Width = 27
    Height = 22
    Caption = '...'
    TabOrder = 8
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 633
    Top = 347
    Width = 27
    Height = 22
    Caption = '...'
    TabOrder = 9
    OnClick = Button4Click
  end
  object editVersionNo: TEdit
    Left = 529
    Top = 78
    Width = 131
    Height = 21
    TabStop = False
    ReadOnly = True
    TabOrder = 10
  end
end
