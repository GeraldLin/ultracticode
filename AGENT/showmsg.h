//---------------------------------------------------------------------------

#ifndef showmsgH
#define showmsgH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormMsg : public TForm
{
__published:	// IDE-managed Components
  TLabel *lblMsg;
  TButton *Button1;
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormMsg(TComponent* Owner);

  void SetShowMsg(char *msg);
  void SetShowMsg(AnsiString msg);
  void SetShowMsg(LPCTSTR lpszFormat, ...);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMsg *FormMsg;
//---------------------------------------------------------------------------
#endif
