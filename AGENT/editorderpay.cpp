//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "editorderpay.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditOrderPay *FormEditOrderPay;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormEditOrderPay::TFormEditOrderPay(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditOrderPay::SetOrderPayData(CMyOrder &myorder)
{
  MyOrder = myorder;
  edtOOrderId->Text = myorder.m_Orders.OrderId;
  edtOProductTotal->Text = myorder.m_Orders.ProductTotal;
  edtOOrderAmount->Text = myorder.m_Orders.OrderAmount;
  edtODiscountAmount->Text = myorder.m_Orders.DiscountAmount;
  edtOExpressFee->Text = myorder.m_Orders.ExpressFee;
  edtOPayAmount->Text = myorder.m_Orders.PayAmount;

  cbOIsNeedBill->Text = dmAgent->YesNoItemList.GetItemName(myorder.m_Orders.IsNeedBill);
  edtOBillName->Text = myorder.m_Orders.BillName;

  cbOPayState->Text = dmAgent->PayStateItemList.GetItemName(myorder.m_Orders.PayState);
  edtOPayedAmount->Text = myorder.m_Orders.PayedAmount;
  editONoPayAmount->Text = myorder.m_Orders.NoPayAmount;
  edtOPayedTime->Text = myorder.m_Orders.PayedTime;
  edtOPayMen->Text = myorder.m_Orders.PayMen;
  cbOOrderState->Text = dmAgent->OrderStateItemList.GetItemName(myorder.m_Orders.OrderState);
  oldOrderStateId = MyOrder.m_Orders.OrderState;
  oldPayStateId = MyOrder.m_Orders.PayState;

  if (myorder.m_Orders.OrderState == 6)
  {
    cbOOrderState->Enabled = false;
  }
  else
  {
    cbOOrderState->Enabled = true;
  }
  Button1->Enabled = false;
}
int TFormEditOrderPay::GetOrderPayData()
{
  MyOrder.m_Orders.IsNeedBill = dmAgent->YesNoItemList.GetItemValue(cbOIsNeedBill->Text);
  MyOrder.m_Orders.BillName = edtOBillName->Text;
  MyOrder.m_Orders.PayState = dmAgent->PayStateItemList.GetItemValue(cbOPayState->Text);
  try
  {
    MyOrder.m_Orders.PayedAmount = StrToFloat(edtOPayedAmount->Text);
  }
  catch (...)
  {
    MessageBox(NULL,"已付金額輸入錯誤！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }

  MyOrder.m_Orders.PayedTime = edtOPayedTime->Text;
  MyOrder.m_Orders.PayMen = edtOPayMen->Text;
  MyOrder.m_Orders.OrderState = dmAgent->OrderStateItemList.GetItemValue(cbOOrderState->Text);
  return 0;
}
void __fastcall TFormEditOrderPay::cbOIsNeedBillKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderPay::cbOIsNeedBillChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderPay::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderPay::FormCreate(TObject *Sender)
{
  dmAgent->AddCmbItem(cbOIsNeedBill, dmAgent->YesNoItemList, "");
  dmAgent->AddCmbItem(cbOPayState, dmAgent->PayStateItemList, "");
  dmAgent->AddCmbItem(cbOOrderState, dmAgent->OrderStateItemList, "");
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderPay::Button3Click(TObject *Sender)
{
  MonthCalendar1->Visible = true;
  MonthCalendar1->Date = Date();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderPay::MonthCalendar1Click(TObject *Sender)
{
  edtOPayedTime->Text = MonthCalendar1->Date.FormatString("yyyy-mm-dd");
  MonthCalendar1->Visible = false;
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderPay::Button1Click(TObject *Sender)
{
  char dispbuf[256];

  if (GetOrderPayData() != 0)
    return;

  if (dmAgent->UpdateOrderPayRecord(&MyOrder.m_Orders) == 0)
  {
    dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改訂單：%s的付款情況記錄", MyOrder.m_Orders.OrderId.c_str());
    MessageBox(NULL,"修改訂單付款情況成功!","信息提示",MB_OK|MB_ICONINFORMATION);
    dmAgent->adoQryOrder->Close();
    dmAgent->adoQryOrder->Open();
    this->Close();
  }
  else
  {
    MessageBox(NULL,"修改訂單付款情況失敗!","信息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditOrderPay::cbOPayStateChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditOrderPay::edtOPayedAmountExit(TObject *Sender)
{
  try
  {
    MyOrder.m_Orders.PayedAmount = StrToFloat(edtOPayedAmount->Text);
  }
  catch (...)
  {
    MessageBox(NULL,"已付金額輸入錯誤！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  MyOrder.m_Orders.NoPayAmount = MyOrder.m_Orders.PayAmount-MyOrder.m_Orders.PayedAmount;
  editONoPayAmount->Text = MyOrder.m_Orders.NoPayAmount;
}
//---------------------------------------------------------------------------

