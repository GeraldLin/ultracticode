//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "viewemail.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormViewEmail *FormViewEmail;

extern CMyWorker MyWorker;
extern AnsiString strAllocEmailURL;
extern int g_nAllocInitProcId;
//---------------------------------------------------------------------------
__fastcall TFormViewEmail::TFormViewEmail(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFormViewEmail::FormShow(TObject *Sender)
{
  editEmailFrom->Text = m_strEmailFrom;
  editEmailTo->Text = m_strEmailTo;
  editEmailSubject->Text = m_strEmailSubject;
  memoEmailContent->Lines->Clear();
  memoEmailContent->Lines->LoadFromFile(m_strEmailFileName);
}
//---------------------------------------------------------------------------

void __fastcall TFormViewEmail::btCloseClick(TObject *Sender)
{
  DeleteFile(m_strEmailFileName);
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormViewEmail::Button1Click(TObject *Sender)
{
  AnsiString strWorkerNo, strURLTemp;

  if (cbWorkerNo->Text == "")
  {
    MessageBox(NULL,"請選擇值機編號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  strWorkerNo = SplitStringFirstItem(cbWorkerNo->Text, "-");
  m_Accept.WorkerNo = strWorkerNo;

  //http://127.0.0.1:8080/?proctype=email&id=$S_EmailUIDL&workerno=$S_WorkerNo&AcceptID=$S_AcptGUID&procworkerno=$S_AdminWorkerNo
  strURLTemp = strAllocEmailURL;
  MyReplace(strURLTemp, "$S_EmailUIDL", m_Accept.SerialNo, strURLTemp);
  MyReplace(strURLTemp, "$S_WorkerNo", strWorkerNo, strURLTemp);
  MyReplace(strURLTemp, "$S_AcptGUID", m_Accept.GId, strURLTemp);
  MyReplace(strURLTemp, "$S_AdminWorkerNo", MyWorker.WorkerNo, strURLTemp);

  WriteErrprMsg("%s", strURLTemp.c_str());
  try
  {
    if (FormMain->IdHTTP1->Get(strURLTemp) == "OK")
    {
      FormMain->IdHTTP1->DisconnectSocket();
      if (dmAgent->InsertAccept(m_Accept) == 0)
      {
        dmAgent->UpdateRecvEmailProcFlag(m_EmailId, g_nAllocInitProcId, strWorkerNo, "等待二線處理");
        MessageBox(NULL,"派發二線處理成功!","訊息提示",MB_OK|MB_ICONINFORMATION);
        return;
      }
      MessageBox(NULL,"派發二線處理(新增受理工單)失敗!","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"派發二線處理(發email)失敗!","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
  }
  catch (...)
  {
    MessageBox(NULL,"派發二線處理(發email)失敗!","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------

