//---------------------------------------------------------------------------
#ifndef __PUBLIC_H_
#define __PUBLIC_H_
//---------------------------------------------------------------------------
#include <time.h>

#define MAX_SEAT_NUM            128
#define MAX_ORDERLIST_NUM       50

class CSysVar
{
public:
  AnsiString S_CallerNo; //��������
  AnsiString S_CalledNo; //��������
  AnsiString S_OrgCalledNo;
  int S_ChanType; //�o��F��
  int S_ChanNo; //�o����
  AnsiString S_DTMF; //���vټӨ
  AnsiString S_SeatNo; //�N����
  AnsiString S_WorkerNo; //�ϕ^������
  AnsiString S_WorkerName;
  AnsiString S_WorkerPwd; //�ϕ^�����̳���
  int S_GroupNo; //�ϕ^���V��
  AnsiString S_SerialNo; //�����Ե���
  AnsiString S_CustomNo; //���c����
  AnsiString S_RecdFile; //�̶x�`����
  AnsiString S_RecdPath; //�̶x�H����
  AnsiString S_RecdFilePath; //�̶x?����`����
  AnsiString S_RecdFTPPath; //�̶x�`��FTP�B�C����
  int S_AcceptSrvType; //���^�F��
  AnsiString S_AcceptSrvName;
  AnsiString S_AcceptSrvNameTag;
  int S_AcceptSrvSubType; //���^�F��
  AnsiString S_AcceptSrvSubName;
  AnsiString S_AcceptSrvSubNameTag;
  int S_AcceptProcType; //�b���^�F��
  int S_CallInOut; //��?����
  AnsiString S_TranSeatNo; //�v��ǻ�N����
  AnsiString S_URLParam;
  AnsiString S_URLAddr;
  AnsiString S_AccountNo;
  AnsiString S_UUID;
  AnsiString S_EmployeeID;
  int S_ProdType;
  AnsiString S_ProdName;
  AnsiString S_ProdNameTag;
  int S_ProdModelType;
  AnsiString S_ProdModelName;
  AnsiString S_ProdModelNameTag;
  AnsiString S_EMAILID;
public:
  CSysVar()
  {
    S_CallerNo = "";
    S_CalledNo = "";
    S_OrgCalledNo = "";
    S_ChanType = 0;
    S_ChanNo = 0;
    S_DTMF = "";
    S_SeatNo = "";
    S_WorkerNo = "";
    S_WorkerName = "";
    S_WorkerPwd = "";
    S_GroupNo = 0;
    S_SerialNo = "";
    S_CustomNo = "";
    S_RecdFile = "";
    S_RecdPath = "";
    S_AcceptSrvType = 0;
    S_AcceptSrvName = "";
    S_AcceptSrvNameTag = "";
    S_AcceptSrvSubType = 0;
    S_AcceptSrvSubName = "";
    S_AcceptSrvSubNameTag = "";
    S_AcceptProcType = 0;
    S_CallInOut = 0;
    S_TranSeatNo = "";
    S_URLParam = "";
    S_URLAddr = "";
    S_AccountNo = "";
    S_UUID = "";
    S_ProdType = 0;
    S_ProdName = "";
    S_ProdNameTag = "";
    S_ProdModelType = 0;
    S_ProdModelName = "";
    S_ProdModelNameTag = "";
    S_EMAILID = "";
  }
  const CSysVar& operator=(const CSysVar& sysvar)
  {
    S_CallerNo = sysvar.S_CallerNo;
    S_CalledNo = sysvar.S_CalledNo;
    S_OrgCalledNo = sysvar.S_OrgCalledNo;
    S_ChanType = sysvar.S_ChanType;
    S_ChanNo = sysvar.S_ChanNo;
    S_DTMF = sysvar.S_DTMF;
    S_SeatNo = sysvar.S_SeatNo;
    S_WorkerNo = sysvar.S_WorkerNo;
    S_WorkerName = sysvar.S_WorkerName;
    S_WorkerPwd = sysvar.S_WorkerPwd;
    S_GroupNo = sysvar.S_GroupNo;
    S_SerialNo = sysvar.S_SerialNo;
    S_CustomNo = sysvar.S_CustomNo;
    S_RecdFile = sysvar.S_RecdFile;
    S_RecdPath = sysvar.S_RecdPath;
    S_AcceptSrvType = sysvar.S_AcceptSrvType;
    S_AcceptSrvName = sysvar.S_AcceptSrvName;
    S_AcceptSrvNameTag = sysvar.S_AcceptSrvNameTag;
    S_AcceptSrvSubType = sysvar.S_AcceptSrvSubType;
    S_AcceptSrvSubName = sysvar.S_AcceptSrvSubName;
    S_AcceptSrvSubNameTag = sysvar.S_AcceptSrvSubNameTag;
    S_AcceptProcType = sysvar.S_AcceptProcType;
    S_CallInOut = sysvar.S_CallInOut;
    S_TranSeatNo = sysvar.S_TranSeatNo;
    S_URLParam = sysvar.S_URLParam;
    S_URLAddr = sysvar.S_URLAddr;
    S_AccountNo = sysvar.S_AccountNo;
    S_UUID = sysvar.S_UUID;
    S_ProdType = sysvar.S_ProdType;
    S_ProdName = sysvar.S_ProdName;
    S_ProdNameTag = sysvar.S_ProdNameTag;
    S_ProdModelType = sysvar.S_ProdModelType;
    S_ProdModelName = sysvar.S_ProdModelName;
    S_ProdModelNameTag = sysvar.S_ProdModelNameTag;
    S_EMAILID = sysvar.S_EMAILID;
    return *this;
  }
};

class CMySeat
{
public:
  bool TcpLinkId;
  bool LoginId;

	short ServerId;
	AnsiString  ServerIP;
	short ServerPort;
	
	short ClientId;
	short SeatType;
	short ChType;
	short ChIndex;
	
	AnsiString  SeatIP;
	AnsiString  SeatNo;
  bool AutoReady;

public:
  CMySeat()
  {
    TcpLinkId = false;
    LoginId = false;

	  ServerId = 1;
	  ServerIP = "127.0.0.1";
	  ServerPort = 1227;
	
	  ClientId = 1;
	  SeatType = 2;
	  ChType = 18;
	  ChIndex = 0;

	  SeatIP = "127.0.0.1";
	  SeatNo = "0";
    AutoReady = true;
  }

  virtual ~CMySeat(){}
  
};

class CMyWorker
{
public:
  bool  LoginId;
  AnsiString  LoginTime;
  short CallInCount;
  short AnsCount;
  int   TalkCount;
  short CallOutCount;
  short CallOutAns;

  int Id;
  AnsiString WorkerNo; //�ϕ^������
  short WorkType; //����׫����1-?�o�ϕ^�� 2-?�o���� 3-�ϕ^�� 4-������∸ 9-��∸��
  short DepartmentID;
  short RealGroupNo; //2016-12-19��ڪ��s�ո�
  short GroupNo; //�����V���1-31��
  AnsiString GroupName; //2016-12-07
  short GroupNo1; //�����V���1-31��
  short GroupNo2; //�����V���1-31��
  short GroupNo3; //�����V���1-31��
  short GroupNo4; //�����V���1-31��
  short GroupNo5; //�����V���1-31��
  short GroupNo6; //�����V���1-31��
  short GroupNo7; //�����V���1-31��
  short GroupNo8; //�����V���1-31��
  short GroupNo9; //�����V���1-31��
  short GroupNo10; //�����V���1-31��
  short GroupNo11; //�����V���1-31��
  short GroupNo12; //�����V���1-31��
  short GroupNo13; //�����V���1-31��
  short GroupNo14; //�����V���1-31��
  short GroupNo15; //�����V���1-31��
  short wLevel; //����׫����1-15��
  short wLevel1; //����׫����1-15��
  short wLevel2; //����׫����1-15��
  short wLevel3; //����׫����1-15��
  short wLevel4; //����׫����1-15��
  short wLevel5; //����׫����1-15��
  short wLevel6; //����׫����1-15��
  short wLevel7; //����׫����1-15��
  short wLevel8; //����׫����1-15��
  short wLevel9; //����׫����1-15��
  short wLevel10; //����׫����1-15��
  short wLevel11; //����׫����1-15��
  short wLevel12; //����׫����1-15��
  short wLevel13; //����׫����1-15��
  short wLevel14; //����׫����1-15��
  short wLevel15; //����׫����1-15��
  short CallRight; //����?�j 0-�Û��ɸ� 1-�Ϻ������ɸ� 2-�Ϻ��һ��ܸ� 3-��Ϻ��һ�߯ٯ 4-�Ϻ��������� 5-�Ϻ���������
  AnsiString Password; //���̳���
  AnsiString WorkerName; //�ϕ^���n��
  AnsiString Tele; //�����ɸ�
  AnsiString Mobile; //߯ٯ����
  AnsiString Address; //�r�u
  AnsiString Email; //�Y�g
  AnsiString SubPhone;
  AnsiString EmployeeID; //Ա�����
  AnsiString IVRNO;
  int HideCallerNo; //��0bit-5bit��0bit-����������룬1bit-���غ������룬2bit-���ؿͻ����Ϻ��룬3bit-�����Ⲧ���Ϻ���
  int AutoAnswerId;
  int ProcDialoutNextType;
  AnsiString SWTGroupID; //2017-07-31 pbx�s�ո�
  AnsiString SWTGroupPWD;
  int PWDModifyFlag;
  AnsiString PWDModifyDate;

public:
  CMyWorker()
  {
    LoginId = false;
    WorkerNo = "";
    WorkType = 1;
    RealGroupNo=1;
    GroupNo = 1;
    GroupName = "";
    GroupNo1 = 0;
    GroupNo2 = 0;
    GroupNo3 = 0;
    GroupNo4 = 0;
    GroupNo5 = 0;
    GroupNo6 = 0;
    GroupNo7 = 0;
    GroupNo8 = 0;
    GroupNo9 = 0;
    GroupNo10 = 0;
    GroupNo11 = 0;
    GroupNo12 = 0;
    GroupNo13 = 0;
    GroupNo14 = 0;
    GroupNo15 = 0;
    wLevel = 1;
    wLevel1 = 0;
    wLevel2 = 0;
    wLevel3 = 0;
    wLevel4 = 0;
    wLevel5 = 0;
    wLevel6 = 0;
    wLevel7 = 0;
    wLevel8 = 0;
    wLevel9 = 0;
    wLevel10 = 0;
    wLevel11 = 0;
    wLevel12 = 0;
    wLevel13 = 0;
    wLevel14 = 0;
    wLevel15 = 0;
    CallRight = 4;
    WorkerName = "none";
    CallInCount = 0;
    AnsCount = 0;
    TalkCount = 0;
    Email = "";
    Address = "";
    Mobile = "";
    SubPhone = "";
    EmployeeID = "";
    IVRNO = "";
    HideCallerNo = 0x00;
    AutoAnswerId = 0;
    ProcDialoutNextType = 0;
    SWTGroupID = "";
    SWTGroupPWD = "";
    PWDModifyFlag = 0;
    PWDModifyDate = "";
  }

  virtual ~CMyWorker(){}
  //2108-05-15
  bool isGroupNo(int nGroupNo)
  {
    if (nGroupNo == 0)
      return false;
    if (nGroupNo == GroupNo)
      return true;
    if (nGroupNo == GroupNo1)
      return true;
    if (nGroupNo == GroupNo2)
      return true;
    if (nGroupNo == GroupNo3)
      return true;
    if (nGroupNo == GroupNo4)
      return true;
    if (nGroupNo == GroupNo5)
      return true;
    if (nGroupNo == GroupNo6)
      return true;
    if (nGroupNo == GroupNo7)
      return true;
    if (nGroupNo == GroupNo8)
      return true;
    if (nGroupNo == GroupNo9)
      return true;
    if (nGroupNo == GroupNo10)
      return true;
    if (nGroupNo == GroupNo11)
      return true;
    if (nGroupNo == GroupNo12)
      return true;
    if (nGroupNo == GroupNo13)
      return true;
    if (nGroupNo == GroupNo14)
      return true;
    if (nGroupNo == GroupNo15)
      return true;
    else
      return false;
  }
};

class CDeptSeat
{
public:
  int state;
  int DeptNo;
  AnsiString SeatNo;

public:
  CDeptSeat()
  {
    state = 0;
    DeptNo = 0;
    SeatNo = "";
  }

  virtual ~CDeptSeat(){}
};

class CSeat
{
public:
  int state;
  AnsiString SeatNo;
  AnsiString WorkerNo;
  AnsiString WorkerName;

public:
  CSeat()
  {
    Init();
  }

  virtual ~CSeat(){}

  void Init()
  {
    state = 0;
    SeatNo = "";
    WorkerNo = "";
  }
};

class CSeatList
{
public:
  short DeptSeatNum;
  CDeptSeat DeptSeat[MAX_SEAT_NUM];
  CSeat Seat[MAX_SEAT_NUM];

public:
  CSeatList()
  {
    DeptSeatNum = 0;
  }
  virtual ~CSeatList(){}
  inline void SetSeatData(AnsiString SeatNo, AnsiString WorkerNo, AnsiString WorkerName)
  {
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].SeatNo == SeatNo)
      {
        Seat[i].WorkerNo = WorkerNo;
        Seat[i].WorkerName = WorkerName;
        return;
      }
    }
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].state == 0)
      {
        Seat[i].state = 1;
        Seat[i].SeatNo = SeatNo;
        Seat[i].WorkerNo = WorkerNo;
        Seat[i].WorkerName = WorkerName;
        return;
      }
    }
  }
  void ClearDeptSeatData()
  {
    DeptSeatNum = 0;
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      DeptSeat[i].state = 0;
      DeptSeat[i].SeatNo = "";

      Seat[i].state = 0;
      Seat[i].SeatNo = "";
      Seat[i].WorkerNo = "";
      Seat[i].WorkerName = "";
    }
  }
  inline void SetDeptSeatData(int DeptNo, AnsiString SeatNo)
  {
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (DeptSeat[i].SeatNo == SeatNo)
      {
        return;
      }
    }
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (DeptSeat[i].state == 0)
      {
        DeptSeatNum ++;
        DeptSeat[i].state = 1;
        DeptSeat[i].DeptNo = DeptNo;
        DeptSeat[i].SeatNo = SeatNo;

        Seat[i].state = 1;
        Seat[i].SeatNo = SeatNo;
        return;
      }
    }
  }
  AnsiString GetSeatNo(AnsiString WorkerNo)
  {
    if (WorkerNo == "0" || WorkerNo == "")
      return "";
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].WorkerNo == WorkerNo)
      {
        return Seat[i].SeatNo;
      }
    }
    return "";
  }
  bool isDeptSeatNoExist(AnsiString SeatNo)
  {
    if (DeptSeatNum == 0)
      return true;
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (DeptSeat[i].SeatNo == SeatNo)
      {
        return true;
      }
    }
    return false;
  }
  int GetAgNo(AnsiString SeatNo)
  {
    if (SeatNo == "")
      return -1;
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].SeatNo == SeatNo)
      {
        return i;
      }
    }
    return -1;
  }
  AnsiString GetWorkerNo(AnsiString SeatNo)
  {
    if (SeatNo == "")
      return 0;
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].SeatNo == SeatNo)
      {
        return Seat[i].WorkerNo;
      }
    }
    return 0;
  }
  AnsiString GetWorkerNameByWorkerNo(AnsiString WorkerNo)
  {
    if (WorkerNo == "0" || WorkerNo == "")
      return "";
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].WorkerNo == WorkerNo)
      {
        return Seat[i].WorkerName;
      }
    }
    return "";
  }
  AnsiString GetWorkerName(AnsiString SeatNo)
  {
    if (SeatNo == "")
      return "";
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].SeatNo == SeatNo)
      {
        return Seat[i].WorkerName;
      }
    }
    return "";
  }
  bool IsSeatNoExist(AnsiString SeatNo)
  {
    if (SeatNo == "")
      return false;
    for (int i=0; i<MAX_SEAT_NUM; i++)
    {
      if (Seat[i].SeatNo == SeatNo)
      {
        return true;
      }
    }
    return false;
  }
};
class CAcceptGroup
{
public:
  int id;
  AnsiString CalledNo;
  AnsiString DTMFKey;
  int GroupNo1; //���壺-1��ʾת�ӵ������ߺ���GroupNoTel1��0-32 ��ʾת�ӵ��ǻ����飬32-99 ��ʾת�ӵ������⹦�ܣ�100-65535 ��ʾת�ӵ��ǻ���Ա����
  //32-���Խ��飬33-IVR��ʾ������34-���մ��棬35�绰����,36��ȡ���棬37-����IVR�Ӳ˵���38����ת����")
  AnsiString GroupNoTel1; //���ߺ���
  int GroupNo1Type;

  int GroupNo2;
  AnsiString GroupNoTel2;
  int GroupNo2Type;
  int GroupNo3;
  AnsiString GroupNoTel3;
  int GroupNo3Type;
  int GroupNo4;
  AnsiString GroupNoTel4;
  int GroupNo4Type;
  int GroupNo5;
  AnsiString GroupNoTel5;
  int GroupNo5Type;

  int SrvType;
  AnsiString SrvName;
  int CalledType;
  int DTMFType;
  int VocId;
  AnsiString CorpVoc;
  AnsiString MenuVoc;
  int WorkerNoId;
  AnsiString MenuKeys;

  int MenuDtmfACK;
  AnsiString DtmfACKVoc;

public:
  CAcceptGroup()
  {
    id = 0;
    CalledNo = "";
    DTMFKey = "";
    GroupNo1 = 0;
    GroupNoTel1 = "0";
    GroupNo1Type = 0;
    GroupNo2 = 0;
    GroupNoTel2 = "0";
    GroupNo2Type = 0;
    GroupNo3 = 0;
    GroupNoTel3 = "0";
    GroupNo3Type = 0;
    GroupNo4 = 0;
    GroupNoTel4 = "0";
    GroupNo4Type = 0;
    GroupNo5 = 0;
    GroupNoTel5 = "0";
    GroupNo5Type = 0;
    SrvType = 0;
    SrvName = "";
    CalledType = 0;
    DTMFType = 0;
    VocId = 1;
    CorpVoc = "";
    MenuVoc = "";
    WorkerNoId = 1;
    MenuKeys = "";
    MenuDtmfACK = 0;
    DtmfACKVoc = "";
  }

  virtual ~CAcceptGroup(){}

  const CAcceptGroup& operator=(const CAcceptGroup& acceptgroup)
  {
    id = acceptgroup.id;
    CalledNo = acceptgroup.CalledNo;
    DTMFKey = acceptgroup.DTMFKey;
    GroupNo1 = acceptgroup.GroupNo1;
    GroupNo2 = acceptgroup.GroupNo2;
    GroupNo3 = acceptgroup.GroupNo3;
    GroupNo4 = acceptgroup.GroupNo4;
    GroupNo5 = acceptgroup.GroupNo5;
    GroupNoTel1 = acceptgroup.GroupNoTel1;
    GroupNoTel2 = acceptgroup.GroupNoTel2;
    GroupNoTel3 = acceptgroup.GroupNoTel3;
    GroupNoTel4 = acceptgroup.GroupNoTel4;
    GroupNoTel5 = acceptgroup.GroupNoTel5;
    GroupNo1Type = acceptgroup.GroupNo1Type;
    GroupNo2Type = acceptgroup.GroupNo2Type;
    GroupNo3Type = acceptgroup.GroupNo3Type;
    GroupNo4Type = acceptgroup.GroupNo4Type;
    GroupNo5Type = acceptgroup.GroupNo5Type;
    SrvType = acceptgroup.SrvType;
    SrvName = acceptgroup.SrvName;
    CalledType = acceptgroup.CalledType;
    DTMFType = acceptgroup.DTMFType;
    VocId = acceptgroup.VocId;
    CorpVoc = acceptgroup.CorpVoc;
    MenuVoc = acceptgroup.MenuVoc;
    WorkerNoId = acceptgroup.WorkerNoId;
    MenuKeys = acceptgroup.MenuKeys;
    MenuDtmfACK = acceptgroup.MenuDtmfACK;
    DtmfACKVoc = acceptgroup.DtmfACKVoc;
    return *this;
  }
};

class CCallMsg
{
public:
  bool state;
  int CallInOut; //1-��? 2-����
  bool AnsId;
  AnsiString CallerNo;
  AnsiString DispCallerNo;
  AnsiString CalledNo;
  AnsiString ChnMsg;
  AnsiString CallTime;
  time_t st;
  int WaitTimer;
  int TalkTimer;
  AnsiString QYBH;
  bool SumTimer;

public:
  CCallMsg()
  {
    Init();
  }

  virtual ~CCallMsg(){}

  void Init()
  {
    state = false;
    CallInOut = 0;
    AnsId = false;
    CallerNo = "";
    DispCallerNo = "";
    CalledNo = "";
    ChnMsg = "";
    CallTime = "";
    time(&st);
    WaitTimer = 0;
    TalkTimer = 0;
    QYBH = "";
    SumTimer = true;
  }
};

class CLookupItem
{
public:
  int state;
  int ItemValue;
  int ItemSubValue;
  int ItemSubExValue;
  AnsiString ItemName;

  AnsiString ItemDemo;
  AnsiString ItemURL;
  AnsiString ItemURLBtnName;

public:
  CLookupItem()
  {
    state = 0;
    ItemValue = -1;
    ItemSubValue = -1;
    ItemSubExValue = -1;
    ItemName = "";
    ItemDemo = "";
    ItemURL = "";
    ItemURLBtnName = "";
  }
  virtual ~CLookupItem()
  {
    state = 0;
    ItemValue = -1;
    ItemSubValue = -1;
    ItemSubExValue = -1;
    ItemName = "";
    ItemDemo = "";
    ItemURL = "";
    ItemURLBtnName = "";
  }
};

class CLookupItemList
{
public:
  int ItemNum;
  CLookupItem *pLookupItem;

public:
  CLookupItemList()
  {
    ItemNum = 0;
    pLookupItem = NULL;
  }
  virtual ~CLookupItemList()
  {
    if (pLookupItem != NULL)
    {
      delete []pLookupItem;
      pLookupItem = NULL;
    }
  }
  int InitData(int itemnum)
  {
    if (itemnum <= 0)
      return 1;
    if (pLookupItem != NULL)
    {
      delete []pLookupItem;
      pLookupItem = NULL;
      ItemNum = 0;
    }
    pLookupItem = new CLookupItem[itemnum];
    if (pLookupItem == NULL)
      return 1;
    ItemNum = itemnum;
    return 0;
  }
  //----------------------------------------------------------------------------
  void AddItem(int itemvalue, AnsiString itemname)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = -1;
        pLookupItem[i].ItemSubExValue = -1;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = "";
        pLookupItem[i].ItemURL = "";
        break;
      }
    }
  }
  void AddItem(int itemvalue, AnsiString itemname, AnsiString itemdemo, AnsiString itemurl)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = -1;
        pLookupItem[i].ItemSubExValue = -1;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = itemdemo;
        pLookupItem[i].ItemURL = itemurl;
        break;
      }
    }
  }
  void AddItem(int itemvalue, AnsiString itemname, AnsiString itemdemo, AnsiString itemurl, AnsiString itemurlbtnname)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = -1;
        pLookupItem[i].ItemSubExValue = -1;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = itemdemo;
        pLookupItem[i].ItemURL = itemurl;
        pLookupItem[i].ItemURLBtnName = itemurlbtnname;
        break;
      }
    }
  }
  //----
  void AddItem(int itemvalue, int itemsubvalue, AnsiString itemname)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = itemsubvalue;
        pLookupItem[i].ItemSubExValue = -1;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = "";
        pLookupItem[i].ItemURL = "";
        break;
      }
    }
  }
  void AddItem(int itemvalue, int itemsubvalue, AnsiString itemname, AnsiString itemdemo, AnsiString itemurl)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = itemsubvalue;
        pLookupItem[i].ItemSubExValue = -1;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = itemdemo;
        pLookupItem[i].ItemURL = itemurl;
        break;
      }
    }
  }
  void AddItem(int itemvalue, int itemsubvalue, AnsiString itemname, AnsiString itemdemo, AnsiString itemurl, AnsiString itemurlbtnname)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = itemsubvalue;
        pLookupItem[i].ItemSubExValue = -1;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = itemdemo;
        pLookupItem[i].ItemURL = itemurl;
        pLookupItem[i].ItemURLBtnName = itemurlbtnname;
        break;
      }
    }
  }
  //----
  void AddItem(int itemvalue, int itemsubvalue, int itemsubexvalue, AnsiString itemname)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = itemsubvalue;
        pLookupItem[i].ItemSubExValue = itemsubexvalue;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = "";
        pLookupItem[i].ItemURL = "";
        break;
      }
    }
  }
  void AddItem(int itemvalue, int itemsubvalue, int itemsubexvalue, AnsiString itemname, AnsiString itemdemo, AnsiString itemurl)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = itemsubvalue;
        pLookupItem[i].ItemSubExValue = itemsubexvalue;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = itemdemo;
        pLookupItem[i].ItemURL = itemurl;
        break;
      }
    }
  }
  void AddItem(int itemvalue, int itemsubvalue, int itemsubexvalue, AnsiString itemname, AnsiString itemdemo, AnsiString itemurl, AnsiString itemurlbtnname)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].state == 0)
      {
        pLookupItem[i].state = 1;
        pLookupItem[i].ItemValue = itemvalue;
        pLookupItem[i].ItemSubValue = itemsubvalue;
        pLookupItem[i].ItemSubExValue = itemsubexvalue;
        pLookupItem[i].ItemName = itemname;
        pLookupItem[i].ItemDemo = itemdemo;
        pLookupItem[i].ItemURL = itemurl;
        pLookupItem[i].ItemURLBtnName = itemurlbtnname;
        break;
      }
    }
  }
  //----------------------------------------------------------------------------
  AnsiString GetItemName(int itemvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        return pLookupItem[i].ItemName;
      }
    }
    return "";
  }
  AnsiString GetItemName(int itemvalue, int itemsubvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        return pLookupItem[i].ItemName;
      }
    }
    return "";
  }
  AnsiString GetItemName(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        return pLookupItem[i].ItemName;
      }
    }
    return "";
  }
  //---------------------------------------------------------------------------
  bool IsItemExist(int itemvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        return true;
      }
    }
    return false;
  }
  bool IsItemExist(int itemvalue, int itemsubvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        return true;
      }
    }
    return false;
  }
  bool IsItemExist(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        return true;
      }
    }
    return false;
  }
  //---------------------------------------------------------------------------
  AnsiString GetItemNameTag(int itemvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        int nPos = pLookupItem[i].ItemName.AnsiPos("-");
        if (nPos < 1)
          return pLookupItem[i].ItemName;
        else
          return pLookupItem[i].ItemName.SubString(1,nPos-1);
      }
    }
    return "";
  }
  AnsiString GetItemNameTag(int itemvalue, int itemsubvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        int nPos = pLookupItem[i].ItemName.AnsiPos("-");
        if (nPos < 1)
          return pLookupItem[i].ItemName;
        else
          return pLookupItem[i].ItemName.SubString(1,nPos-1);
      }
    }
    return "";
  }
  AnsiString GetItemNameTag(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        int nPos = pLookupItem[i].ItemName.AnsiPos("-");
        if (nPos < 1)
          return pLookupItem[i].ItemName;
        else
          return pLookupItem[i].ItemName.SubString(1,nPos-1);
      }
    }
    return "";
  }
  //----------------------------------------------------------------------------
  AnsiString GetItemDemo(int itemvalue)
  {
    int i;
    AnsiString strDemo="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        strDemo = pLookupItem[i].ItemDemo;
      }
    }
    if (ItemNum > 0 && strDemo == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == 0)
        {
          strDemo = pLookupItem[i].ItemDemo;
        }
      }
    }
    return strDemo;
  }
  AnsiString GetItemDemo(int itemvalue, int itemsubvalue)
  {
    int i;
    AnsiString strDemo="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        strDemo = pLookupItem[i].ItemDemo;
      }
    }
    if (ItemNum > 0 && strDemo == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == 0)
        {
          strDemo = pLookupItem[i].ItemDemo;
        }
      }
    }
    return strDemo;
  }
  AnsiString GetItemDemo(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    int i;
    AnsiString strDemo="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        strDemo = pLookupItem[i].ItemDemo;
      }
    }
    if (ItemNum > 0 && strDemo == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == 0)
        {
          strDemo = pLookupItem[i].ItemDemo;
        }
      }
    }
    return strDemo;
  }
  AnsiString GetItemDemoByItemName(AnsiString itemname, AnsiString &strUrl)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemName == itemname)
      {
        strUrl = pLookupItem[i].ItemURL;
        return pLookupItem[i].ItemDemo;
      }
    }
    return "";
  }
  //----------------------------------------------------------------------------
  AnsiString GetItemURL(int itemvalue)
  {
    int i;
    AnsiString strUrl="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        strUrl = pLookupItem[i].ItemURL;
      }
    }
    if (ItemNum > 0 && strUrl == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == 0)
        {
          strUrl = pLookupItem[i].ItemURL;
        }
      }
    }
    return strUrl;
  }
  AnsiString GetItemURL(int itemvalue, int itemsubvalue)
  {
    int i;
    AnsiString strUrl="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        strUrl = pLookupItem[i].ItemURL;
      }
    }
    if (ItemNum > 0 && strUrl == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == 0)
        {
          strUrl = pLookupItem[i].ItemURL;
        }
      }
    }
    return strUrl;
  }
  AnsiString GetItemURL(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    int i;
    AnsiString strUrl="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        strUrl = pLookupItem[i].ItemURL;
      }
    }
    if (ItemNum > 0 && strUrl == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == 0)
        {
          strUrl = pLookupItem[i].ItemURL;
        }
      }
    }
    return strUrl;
  }
  //----------------------------------------------------------------------------
  AnsiString GetItemURLBtnName(int itemvalue)
  {
    int i;
    AnsiString strUrlBtnName="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        strUrlBtnName = pLookupItem[i].ItemURLBtnName;
      }
    }
    if (ItemNum > 0 && strUrlBtnName == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == 0)
        {
          strUrlBtnName = pLookupItem[i].ItemURLBtnName;
        }
      }
    }
    return strUrlBtnName;
  }
  AnsiString GetItemURLBtnName(int itemvalue, int itemsubvalue)
  {
    int i;
    AnsiString strUrlBtnName="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        strUrlBtnName = pLookupItem[i].ItemURLBtnName;
      }
    }
    if (ItemNum > 0 && strUrlBtnName == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == 0)
        {
          strUrlBtnName = pLookupItem[i].ItemURLBtnName;
        }
      }
    }
    return strUrlBtnName;
  }
  AnsiString GetItemURLBtnName(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    int i;
    AnsiString strUrlBtnName="";
    for (i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        strUrlBtnName = pLookupItem[i].ItemURLBtnName;
      }
    }
    if (ItemNum > 0 && strUrlBtnName == "")
    {
      for (i = 0; i < ItemNum; i ++)
      {
        if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == 0)
        {
          strUrlBtnName = pLookupItem[i].ItemURLBtnName;
        }
      }
    }
    return strUrlBtnName;
  }
  //----------------------------------------------------------------------------
  AnsiString GetItemNameEx(int itemvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue)
      {
        return pLookupItem[i].ItemName;
      }
    }
    return IntToStr(itemvalue);
  }
  AnsiString GetItemNameEx(int itemvalue, int itemsubvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue)
      {
        return pLookupItem[i].ItemName;
      }
    }
    return IntToStr(itemsubvalue);
  }
  AnsiString GetItemNameEx(int itemvalue, int itemsubvalue, int itemsubexvalue)
  {
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemSubExValue == itemsubexvalue)
      {
        return pLookupItem[i].ItemName;
      }
    }
    return IntToStr(itemsubexvalue);
  }
  //----------------------------------------------------------------------------
  int GetItemValue(AnsiString itemname)
  {
    if (itemname == "")
      return -1;
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemName == itemname)
      {
        return pLookupItem[i].ItemValue;
      }
    }
    return -1;
  }
  int GetItemSubValue(int itemvalue, AnsiString itemname)
  {
    if (itemname == "")
      return -1;
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemName == itemname)
      {
        return pLookupItem[i].ItemSubValue;
      }
    }
    return -1;
  }
  int GetItemSubExValue(int itemvalue, int itemsubvalue, AnsiString itemname)
  {
    if (itemname == "")
      return -1;
    for (int i = 0; i < ItemNum; i ++)
    {
      if (pLookupItem[i].ItemValue == itemvalue && pLookupItem[i].ItemSubValue == itemsubvalue && pLookupItem[i].ItemName == itemname)
      {
        return pLookupItem[i].ItemSubExValue;
      }
    }
    return -1;
  }
};
//�����XͰ
class CSelectTable
{
public:
  int nSelectTableId; //����
  AnsiString strTableName; //�f��Ͱ��
	AnsiString strItemId; //���v�X�f�t
	AnsiString strItemName; //���v�X�ұ�

  AnsiString strCaption; //��������
	AnsiString strItemIdDisp; //���v�X�f�t��?�ū}��ǻ�ұ�
	AnsiString strItemNameDisp; //���v�X�ұ���?�ū}��ǻ�ұ�

	AnsiString strInpuItemIdErrMsg; //��?���v�X�f�t���d�C����ǻ�|��
	AnsiString strInputItemNameErrMsg; //��?���v�X�ұ����d�C����ǻ�|��

  AnsiString strExistMsg; //���v�X�f�t�Պ�C����ǻ�|��
  AnsiString strInsertSuccMsg; //��?���v�X�f�t���ՊC����ǻ�|��
  AnsiString strInsertFailMsg; //��?���v�X�f�t��ࢊC����ǻ�|��
  AnsiString strUpdateSuccMsg; //�������v�X�f�t���ՊC����ǻ�|��
  AnsiString strUpdateFailMsg; //�������v�X�f�t��ࢊC����ǻ�|��
  AnsiString strDeletePromptMsg; //�W�����v�X�f�t?����ǻ�|��
  AnsiString strDeleteSuccMsg; //�W�����v�X�f�t���ՊC����ǻ�|��

  int nMinValue; //�Ƅ�����ǻ�I�ֳt
  int nMaxValue; //�Ƅ�����ǻ�I�γt  쨍R0�CͰ����j��

  bool bModifyId;

public:
  CSelectTable()
  {
    nSelectTableId = -1;
    nMinValue = 0;
    nMaxValue = 0;
    bModifyId = false;
  }
};

class CSelectRecord
{
public:
  int nItemIdValue;
  AnsiString strItemNameValue;

public:
  CSelectRecord()
  {
    nItemIdValue = 0;
    strItemNameValue = "";
  }
};

class CCustDataToAcceptExId
{
public:
  int AcceptExId_id;
  int AcceptExId_CustomNo;
  int AcceptExId_CustName;
  int AcceptExId_CorpName;
  int AcceptExId_CustType;
  int AcceptExId_CustClass;
  int AcceptExId_PriceModule;
  int AcceptExId_Discount;
  int AcceptExId_CustSex;
  int AcceptExId_MobileNo;
  int AcceptExId_TeleNo;
  int AcceptExId_FaxNo;
  int AcceptExId_EMail;
  int AcceptExId_ContAddr;
  int AcceptExId_PostNo;
  int AcceptExId_Province;
  int AcceptExId_CityName;
  int AcceptExId_CountyName;
  int AcceptExId_ZoneName;
  int AcceptExId_EnsureMoney;
  int AcceptExId_RegTime;
  int AcceptExId_RegWorker;
  int AcceptExId_MdfDate;
  int AcceptExId_MdfWorker;
  int AcceptExId_TotalPoints;
  int AcceptExId_Points;
  int AcceptExId_Remark;
  int AcceptExId_SubPhone;
  int AcceptExId_NewId;
  int AcceptExId_CustLevel;
  int AcceptExId_Password;
  int AcceptExId_AccountNo;
  int AcceptExId_ItemData[32];
public:
  CCustDataToAcceptExId()
  {
    AcceptExId_id = 0;
    AcceptExId_CustomNo = 0;
    AcceptExId_CustName = 0;
    AcceptExId_CorpName = 0;
    AcceptExId_CustType = 0;
    AcceptExId_CustClass = 0;
    AcceptExId_PriceModule = 0;
    AcceptExId_Discount = 0;
    AcceptExId_CustSex = 0;
    AcceptExId_MobileNo = 0;
    AcceptExId_TeleNo = 0;
    AcceptExId_FaxNo = 0;
    AcceptExId_EMail = 0;
    AcceptExId_ContAddr = 0;
    AcceptExId_PostNo = 0;
    AcceptExId_Province = 0;
    AcceptExId_CityName = 0;
    AcceptExId_CountyName = 0;
    AcceptExId_ZoneName = 0;
    AcceptExId_EnsureMoney = 0;
    AcceptExId_RegTime = 0;
    AcceptExId_RegWorker = 0;
    AcceptExId_MdfDate = 0;
    AcceptExId_MdfWorker = 0;
    AcceptExId_TotalPoints = 0;
    AcceptExId_Points = 0;
    AcceptExId_Remark = 0;
    AcceptExId_SubPhone = 0;
    AcceptExId_NewId = 0;
    AcceptExId_CustLevel = 0;
    AcceptExId_Password = 0;
    AcceptExId_AccountNo = 0;
    for (int i=0; i<32; i++)
      AcceptExId_ItemData[i] = 0;
  }
};
class CJSONField
{
public:
  int m_FieldNum;
  AnsiString m_FieldName[64];
  AnsiString m_FieldValue[64];

public:
  CJSONField()
  {
    m_FieldNum = 0;
    for (int i=0; i<64; i++)
    {
      m_FieldName[i] = "";
      m_FieldValue[i] = "";
    }
  }
};
class CCustomerItemSet
{
public:
  int state;

  int CFieldId;
  AnsiString CFieldName;
  AnsiString CLabelCaption;
  int CControlType;
  
  int CDispId;
  int CInputId;
  int CDispOrder;
  int CDispWidth;
  int CExportId;
  int ExportOrderId;
  int AcceptExId;

  int ReadDataType; //���Ū���ӷ�:1-DB��Ʈw,2-HTTP����,3-WEBSERVICE����,4-�P�BCRM��� 2016-12-25
  AnsiString ReadFieldTag; //���Ū����ñ(���f�������W��) 2016-12-25
  int SaveToDBFlag; //�O�_�N��ƫO�s��DB��Ʈw
  int ReadDataMinLen; //2022-01-21 ���w��Ʀ��ĳ̤p����
  int ReadDataMaxLen; //2022-01-21 ���w��Ʀ��ĳ̤j����

public:
  void Init()
  {
    state = 0;

    CFieldId = 0;
    CFieldName = "";
	  CLabelCaption = "";
    CControlType = 0;

    CDispId = 0;
    CInputId = 0;
    CDispOrder = 0;
	  CDispWidth = 100;
    CExportId = 0;
    ExportOrderId = 0;
    AcceptExId = 0;

    ReadDataType = 1;
    ReadFieldTag = "";
    SaveToDBFlag = 1;

    ReadDataMinLen = 0;
    ReadDataMaxLen = 65535;
  }
  CCustomerItemSet()
  {
    Init();
  }

  virtual ~CCustomerItemSet(){}

  const CCustomerItemSet& operator=(const CCustomerItemSet& customeritemset)
  {
    CFieldId = customeritemset.CFieldId;
    CFieldName = customeritemset.CFieldName;
    CControlType = customeritemset.CControlType;
	  CLabelCaption = customeritemset.CLabelCaption;
    CDispId = customeritemset.CDispId;
    CInputId = customeritemset.CInputId;
    CDispOrder = customeritemset.CDispOrder;
    CDispWidth = customeritemset.CDispWidth;
    CExportId = customeritemset.CExportId;
    ExportOrderId = customeritemset.ExportOrderId;
    AcceptExId = customeritemset.AcceptExId;
    ReadDataType = customeritemset.ReadDataType;
    ReadFieldTag = customeritemset.ReadFieldTag;
    SaveToDBFlag = customeritemset.SaveToDBFlag;
    ReadDataMinLen = customeritemset.ReadDataMinLen;
    ReadDataMaxLen = customeritemset.ReadDataMaxLen;
    return *this;
  }
};

#define   MAX_CUSTOMER_CONTROL_NUM          32

class CCustomerItemSetList
{
public:
  int ItemNum;
  CCustomerItemSet m_CustomerItemSet[MAX_CUSTOMER_CONTROL_NUM];

public:
  CCustomerItemSetList()
  {
    ItemNum = 0;
  }
  virtual ~CCustomerItemSetList()
  {
  }
  int InitData(int itemnum)
  {
    ItemNum = itemnum;
    return 0;
  }
};

//���c�۹��Ị
class CCustomer
{
public:
	int id;
	AnsiString CustomNo;
	AnsiString CustName;
	AnsiString CorpName;
	int CustType;
  AnsiString CustTypeName;
	int CustLevel;
  AnsiString CustLevelName;
	int CustClass;
  AnsiString CustClassName;
	int PriceModule;
	int Discount;
	AnsiString CustSex;
	AnsiString MobileNo;
	AnsiString TeleNo;
	AnsiString FaxNo;
	AnsiString EMail;
	AnsiString ContAddr;
	AnsiString PostNo;
  AnsiString Province; //��׫
  AnsiString CityName; //����׫
  AnsiString CountyName; //��?׫
  AnsiString ZoneName; //���^˭����?������`
  float EnsureMoney;
	AnsiString RegTime;
	AnsiString RegWorker;
	AnsiString RegWorkerName;
  int DepartmentId; //2018-06-03 ���ݳ���
	AnsiString DepartmentName; //2018-06-03
  AnsiString WorkerNo; //2018-06-03 ���ݭȾ���
	AnsiString MdfDate;
	AnsiString MdfWorker;
	int TotalPoints;
	int Points;
	AnsiString Remark;
  int NewId;
  AnsiString SubPhone;
  AnsiString Password;
  AnsiString AccountNo;

  AnsiString ItemData[32];

  bool MobileNoModifyId;
  bool TeleNoModifyId;
  bool FaxNoModifyId;

public:
  void Init()
  {
	  id = 0;
	  CustomNo = "";
	  CustName = "";
	  CorpName = "";
	  CustType = 0;
    CustTypeName = "";
    CustLevel = 0;
    CustLevelName = "";
	  CustClass = 3;
    CustClassName = "";
	  PriceModule = 0;
	  Discount = 100;
	  CustSex = "";
	  MobileNo = "";
	  TeleNo = "";
	  FaxNo = "";
	  EMail = "";
	  ContAddr = "";
	  PostNo = "";
    Province = "";
    CityName = "";
    CountyName = "";
    ZoneName = "";
    EnsureMoney = 0;
	  RegTime = "";
	  RegWorker = "";
    RegWorkerName = "";
    DepartmentId = 0;
    DepartmentName = "";
    WorkerNo = "";
	  MdfDate = "";
	  MdfWorker = "";
	  TotalPoints = 0;
	  Points = 0;
	  Remark = "";
    NewId = 1;
    SubPhone = "";
    Password = "";
    AccountNo = "";
    for (int i=0; i<32; i++)
      ItemData[i] = "";

    MobileNoModifyId = false;
    TeleNoModifyId = false;
    FaxNoModifyId = false;
  }
  CCustomer()
  {
    Init();
  }
  virtual ~CCustomer(){}
  const CCustomer& operator=(const CCustomer& customer)
  {
	  id = customer.id;
	  CustomNo = customer.CustomNo;
	  CustName = customer.CustName;
	  CorpName = customer.CorpName;
	  CustType = customer.CustType;
	  CustClass = customer.CustClass;
	  PriceModule = customer.PriceModule;
	  Discount = customer.Discount;
	  CustSex = customer.CustSex;
	  MobileNo = customer.MobileNo;
	  TeleNo = customer.TeleNo;
	  FaxNo = customer.FaxNo;
	  EMail = customer.EMail;
	  ContAddr = customer.ContAddr;
	  PostNo = customer.PostNo;
    Province = customer.Province;
    CityName = customer.CityName;
    CountyName = customer.CountyName;
    ZoneName = customer.ZoneName;
    EnsureMoney = customer.EnsureMoney;
	  RegTime = customer.RegTime;
	  RegWorker = customer.RegWorker;
    DepartmentId = customer.DepartmentId;
    DepartmentName = customer.DepartmentName;
    WorkerNo = customer.WorkerNo;
	  MdfDate = customer.MdfDate;
	  MdfWorker = customer.MdfWorker;
	  TotalPoints = customer.TotalPoints;
	  Points = customer.Points;
	  Remark = customer.Remark;
    NewId = customer.NewId;
    SubPhone = customer.SubPhone;
    Password = customer.Password;
    AccountNo = customer.AccountNo;

    for (int i=0; i<32; i++)
      ItemData[i] = customer.ItemData[i];

    MobileNoModifyId = customer.MobileNoModifyId;
    TeleNoModifyId = customer.TeleNoModifyId;
    FaxNoModifyId = customer.FaxNoModifyId;
    return *this;
  }
};

class CAccept
{
public:
  AnsiString GId; //����t��(�`�ۍ�¥)
  AnsiString SerialNo; //�Ե���
  AnsiString CustomTel; //��������
  AnsiString SeatNo; //�N����
  AnsiString WorkerNo; //�ϕ^������
  AnsiString WorkerName; //�ϕ^������
  int CustomType; //�������c�F��
  AnsiString CustomNo; //�������c����
  AnsiString CustName; //�������c�ұ�
  int AcptType; //���⫋�^�F����0-?�Զ��^ 1-���˫��^�y2-བྷr����y3-���Ʊ����
  int SrvSubType;
  AnsiString SrvName;
  int AcptSubType; //�����ԵȞF��
  AnsiString AcptSubTypeName;
  AnsiString ACDTime; //�������ЊC��
  AnsiString AcptTime; //����C��
  AnsiString PutTime; //�����C��
  int AcptLen; //����C��
  AnsiString AcptCont; //������?ĺ��
  AnsiString DealTime; //�һ�C��
  AnsiString DealWorker; //�һ�ǻ����
  int DealState; //�һ�О�F��1-���һ� 2-�B��һ� 3-������� 4-�U����
  AnsiString DealResult;
  AnsiString DealLog; //�һ�?��ĺ��
  AnsiString RecdRootPath;
  AnsiString RecdFile; //�̶x�`���Ҩ�NULLͰ�����̶x��
  AnsiString RecdCallID;
  int MediaType; //ý������:0-�绰,1-IM
  int ProdType;
  int ProdModelType;
  int CallBackId;
  AnsiString CallBackTime;
  AnsiString CallBackWorkerNo;

  bool nWaitSaveId;

public:
  void Init()
  {
    GId = ""; //����t��(�`�ۍ�¥)
    SerialNo = ""; //�Ե���
    CustomTel = ""; //��������
    SeatNo = ""; //�N����
    WorkerNo = ""; //�ϕ^������
    WorkerName = ""; //�ϕ^������
    CustomType = 0; //�������c�F��
    CustomNo = ""; //�������c����
    CustName = ""; //�������c�ұ�
    AcptType = 1; //���⫋�^�F����0-?�Զ��^ 1-���˫��^�y2-བྷr����y3-���Ʊ����
    SrvSubType = 1;
    SrvName = "";
    AcptSubType = 0; //�����ԵȞF��
    AcptSubTypeName = "";
    ACDTime = ""; //�������ЊC��
    AcptTime = ""; //����C��
    PutTime = ""; //�����C��
    AcptLen = 0; //����C��
    AcptCont = ""; //������?ĺ��
    DealTime = ""; //�һ�C��
    DealWorker = ""; //�һ�ǻ����
    DealState = 0; //�һ�О�F��1-���һ� 2-�B��һ� 3-������� 4-�U����
    DealResult = "";
    DealLog = ""; //�һ�?��ĺ��
    RecdRootPath = "";
    RecdFile = ""; //�̶x�`���Ҩ�NULLͰ�����̶x��
    RecdCallID = "";
    MediaType = 0;
    ProdType = 0;
    ProdModelType = 0;
    CallBackId = 0;
    CallBackTime = "";
    CallBackWorkerNo = "";

    nWaitSaveId = false;
  }
  CAccept(){}
  virtual ~CAccept(){}
};

#define MAX_ZSK_MAIN_NUM      128
#define MAX_ZSK_SUB_NUM       256
#define MAX_ZSK_SUBJECT_NUM   512

//���^���o������Ͱ
class CZSKSubject
{
public:
  AnsiString Gid;
	int SubjectNo; //��������(�`�ۍ�¥)
	int MainNo; //�����^��F����
	int SubNo; //�b���^��F����
  AnsiString MainTitle; //������
	AnsiString SubTitle; //�b����
	AnsiString SubjectTitle; //��������
	AnsiString ExplainCont; //����`����?
	AnsiString ExplainImage; //�����Z?
	AnsiString ExplainFile; //��������
	int IssueStatus; //��ƱО�F 0-����� 1-�m��Ʊ 2-�m���� 3-��¡��
	AnsiString IssueDate; //��Ʊ�C��
	AnsiString IssueMan; //��Ʊ?
	int AccessRight; //ʪ�^?�j
	AnsiString CheckTime; //����C��
	AnsiString CheckMan; //����?
	AnsiString VersionNo; //হ���
	int QueryCount; //Ì����f
  int SecrecyId;
  int DepartmentId;
  int orderno3;
	AnsiString Remark; //���q

  int nRootIndex; //�~�Y�H������l��
  int nChildIndex; //�~�Y�b������l��
  int nSubjectIndex; //���������l��
  int nImageIndex; //�Z����l

public:
  CZSKSubject()
  {
    SubjectNo = 0;
    MainNo = 0;
    SubNo = 0;
    IssueStatus = 0;
    AccessRight = 0;
    QueryCount = 0;
    orderno3 = 0;
  }
  virtual ~CZSKSubject(){}

  const CZSKSubject& operator=(const CZSKSubject& zsksubject)
  {
    Gid = zsksubject.Gid;
	  SubjectNo = zsksubject.SubjectNo;
	  MainNo = zsksubject.MainNo;
	  SubNo = zsksubject.SubNo;
	  MainTitle = zsksubject.MainTitle;
	  SubTitle = zsksubject.SubTitle;
	  SubjectTitle = zsksubject.SubjectTitle;
	  ExplainCont = zsksubject.ExplainCont;
	  ExplainImage = zsksubject.ExplainImage;
	  ExplainFile = zsksubject.ExplainFile;
	  IssueStatus = zsksubject.IssueStatus;
	  IssueDate = zsksubject.IssueDate;
	  IssueMan = zsksubject.IssueMan;
	  AccessRight = zsksubject.AccessRight;
	  CheckTime = zsksubject.CheckTime;
	  CheckMan = zsksubject.CheckMan;
	  VersionNo = zsksubject.VersionNo;
	  QueryCount = zsksubject.QueryCount;
	  SecrecyId = zsksubject.SecrecyId;
	  DepartmentId = zsksubject.DepartmentId;
    orderno3 = zsksubject.orderno3;
	  Remark = zsksubject.Remark;

    nRootIndex = zsksubject.nRootIndex;
    nChildIndex = zsksubject.nChildIndex;
    nSubjectIndex = zsksubject.nSubjectIndex;
    nImageIndex = zsksubject.nImageIndex;

    return *this;
  }
};

//���^���o���b��FͰ
class CZSKSub
{
public:
	int SubNo; //�b���^��F����(�`�ۍ�¥)
	int MainNo; //�����^��F����
  AnsiString MainTitle; //������
	AnsiString SubTitle; //�b����
	AnsiString Explain; //�Q����¤
	int IssueStatus; //��ƱО�F 0-����� 1-�m��Ʊ 2-�m���� 3-��¡��
	AnsiString IssueDate; //��Ʊ�C��
	AnsiString IssueMan; //��Ʊ?
	int AccessRight; //ʪ�^?�j
	AnsiString CheckTime; //����C��
	AnsiString CheckMan; //����?
  int orderno2;
	AnsiString Remark; //���q

  int nRootIndex; //�~�Y�H������l��
  int nChildIndex; //�b������l��
  int nImageIndex; //�Z����l

  int nSubjectNum; //���ǻ����f
  CZSKSubject *pZSKSubject[MAX_ZSK_SUBJECT_NUM];

public:
  CZSKSub()
  {
    SubNo = 0;
    MainNo = 0;
    IssueStatus = 2;
    AccessRight = 0;
    nSubjectNum = 0;
    orderno2 = 0;
    for (int i= 0; i < MAX_ZSK_SUBJECT_NUM; i ++)
    {
      pZSKSubject[i] = NULL;
    }
  }
  virtual ~CZSKSub()
  {
    for (int i= 0; i < MAX_ZSK_SUBJECT_NUM; i ++)
    {
      if (pZSKSubject[i] != NULL)
      {
        delete pZSKSubject[i];
        pZSKSubject[i] = NULL;
      }
    }
  }
  const CZSKSub& operator=(const CZSKSub& zsksub)
  {
	  SubNo = zsksub.SubNo;
	  MainNo = zsksub.MainNo;
    MainTitle = zsksub.MainTitle;
	  SubTitle = zsksub.SubTitle;
	  Explain = zsksub.Explain;
	  IssueStatus = zsksub.IssueStatus;
	  IssueDate = zsksub.IssueDate;
	  IssueMan = zsksub.IssueMan;
	  AccessRight = zsksub.AccessRight;
	  CheckTime = zsksub.CheckTime;
	  CheckMan = zsksub.CheckMan;
    orderno2 = zsksub.orderno2;
	  Remark = zsksub.Remark;
    nRootIndex = zsksub.nRootIndex;
    nChildIndex = zsksub.nChildIndex;
    nImageIndex = zsksub.nImageIndex;
    nSubjectNum = zsksub.nSubjectNum;
    for (int i= 0; i < MAX_ZSK_SUBJECT_NUM; i ++)
    {
      if (zsksub.pZSKSubject[i])
      {
        if (pZSKSubject[i] == NULL)
        {
          pZSKSubject[i] = new CZSKSubject;
        }
        memcpy(pZSKSubject[i], zsksub.pZSKSubject[i], sizeof(CZSKSubject));
      }
      else
      {
        if (pZSKSubject[i] != NULL)
        {
          delete pZSKSubject[i];
          pZSKSubject[i] = NULL;
        }
      }
    }
    return *this;
  }
};

//���^���o������FͰ
class CZSKMain
{
public:
  int MainNo; //�����^��F����(�`�ۍ�¥)
	AnsiString MainTitle; //����
	AnsiString Explain; //�Q����¤
	int IssueStatus; //��ƱО�F 0-����� 1-�m��Ʊ 2-�m���� 3-��¡��
	AnsiString IssueDate; //��Ʊ�C��
	AnsiString IssueMan; //��Ʊ?
	int AccessRight; //ʪ�^?�j
	AnsiString CheckTime; //����C��
	AnsiString CheckMan; //����?
  int orderno1;
	AnsiString Remark; //���q

  int nRootIndex; //�H������l��
  int nImageIndex; //�Z����l

  int nZSKSubNum; //���ǻ���^���o���b��F�f
  CZSKSub *pZSKSub[MAX_ZSK_SUB_NUM];

public:
  CZSKMain()
  {
    MainNo = 0;
    IssueStatus = 2;
    AccessRight = 0;
    nZSKSubNum = 0;
    orderno1 = 0;
    for (int i= 0; i < MAX_ZSK_SUB_NUM; i ++)
    {
      pZSKSub[i] = NULL;
    }
  }
  virtual ~CZSKMain()
  {
    for (int i= 0; i < MAX_ZSK_SUB_NUM; i ++)
    {
      if (pZSKSub[i] != NULL)
      {
        delete pZSKSub[i];
        pZSKSub[i] = NULL;
      }
    }
  }
  const CZSKMain& operator=(const CZSKMain& zskmain)
  {
	  MainNo = zskmain.MainNo;
	  MainTitle = zskmain.MainTitle;
	  Explain = zskmain.Explain;
	  IssueStatus = zskmain.IssueStatus;
	  IssueDate = zskmain.IssueDate;
	  IssueMan = zskmain.IssueMan;
	  AccessRight = zskmain.AccessRight;
	  CheckTime = zskmain.CheckTime;
	  CheckMan = zskmain.CheckMan;
    orderno1 = zskmain.orderno1;
	  Remark = zskmain.Remark;

    nRootIndex = zskmain.nRootIndex;
    nImageIndex = zskmain.nImageIndex;
    nZSKSubNum = zskmain.nZSKSubNum;
    for (int i= 0; i < MAX_ZSK_SUB_NUM; i ++)
    {
      if (zskmain.pZSKSub[i])
      {
        if (pZSKSub[i] == NULL)
        {
          pZSKSub[i] = new CZSKSub;
        }
        memcpy(pZSKSub[i], zskmain.pZSKSub[i], sizeof(CZSKSub));
      }
      else
      {
        if (pZSKSub[i] != NULL)
        {
          delete pZSKSub[i];
          pZSKSub[i] = NULL;
        }
      }
    }
    return *this;
  }
};

//���o��
class CZSK
{
public:
  int nZSKMainNum;
  CZSKMain *pZSKMain[MAX_ZSK_MAIN_NUM];

public:
  CZSK()
  {
    nZSKMainNum = 0;
    for (int i= 0; i < MAX_ZSK_MAIN_NUM; i ++)
    {
      pZSKMain[i] = NULL;
    }
  }
  virtual ~CZSK()
  {
    for (int i= 0; i < MAX_ZSK_MAIN_NUM; i ++)
    {
      if (pZSKMain[i] != NULL)
      {
        delete pZSKMain[i];
        pZSKMain[i] = NULL;
      }
    }
  }
  const CZSK& operator=(const CZSK& zsk)
  {
    nZSKMainNum = zsk.nZSKMainNum;
    for (int i= 0; i < MAX_ZSK_MAIN_NUM; i ++)
    {
      if (zsk.pZSKMain[i])
      {
        if (pZSKMain[i] == NULL)
        {
          pZSKMain[i] = new CZSKMain;
        }
        memcpy(pZSKMain[i], zsk.pZSKMain[i], sizeof(CZSKMain));
      }
      else
      {
        if (pZSKMain[i] != NULL)
        {
          delete pZSKMain[i];
          pZSKMain[i] = NULL;
        }
      }
    }
    return *this;
  }
};

class CRecvSms
{
public:
  int id;
  AnsiString Mobile;
  AnsiString SmsMsg;
  AnsiString RecvTime;
  int ReadFlag;
  AnsiString ReadTime;
  int ProcSrvType;
  AnsiString ProcSrvData;
  AnsiString ProcBy;
  AnsiString Remark;

public:
  CRecvSms(){}
  virtual ~CRecvSms(){}

  const CRecvSms& operator=(const CRecvSms& recvsms)
  {
    id = recvsms.id;
    Mobile = recvsms.Mobile;
    SmsMsg = recvsms.SmsMsg;
    RecvTime = recvsms.RecvTime;
    ReadFlag = recvsms.ReadFlag;
    ReadTime = recvsms.ReadTime;
    ProcSrvType = recvsms.ProcSrvType;
    ProcSrvData = recvsms.ProcSrvData;
    ProcBy = recvsms.ProcBy;
    Remark = recvsms.Remark;
    return *this;
  }
};

class CDialOutTask
{
public:
  int TaskId;
  AnsiString TaskName;
  AnsiString StartDate;
  AnsiString EndDate;
  AnsiString StartTime;
  AnsiString EndTime;
  AnsiString StartTime2;
  AnsiString EndTime2;
  int StartId;
  AnsiString StartMem;
  AnsiString StopMem;
  int TotalTeles;
  int CalledCount;
  int AnsCount;
  int DeviceNo;
  int RouteNo;
  int MaxChnNum;
  AnsiString CallerNo;
  int MaxCalledTimes;
  int MaxAnsTimes;
  int NoAnsDelay;
  int BusyDelay;
  AnsiString FileName;
  int MixerType;
  AnsiString FileName2;
  int GroupNo;
  int FuncNo;
  AnsiString Param;
  AnsiString Remark;
  int ExportId;
  int CallType;
  int PlayDelay;
  int TTSType;
  int TTSUpdateId;
  AnsiString TTSData;
  AnsiString TTSFileName;
  int ConfirmType;
  AnsiString ConfirmVoc;
  AnsiString ConfirmDTMF;
  AnsiString TranVDN;
  AnsiString DialPreCode;
  int ListenTimeLen;

public:
  CDialOutTask()
  {
    TaskId = 0;
    TaskName = "";
    StartDate = "2011-01-01";
    EndDate = "2020-12-31";
    StartTime = "08:30";
    EndTime = "12:00";
    StartTime2 = "14:30";
    EndTime2 = "18:00";
    StartId = 1;
    StartMem = "";
    StopMem = "";
    TotalTeles = 0;
    CalledCount = 0;
    AnsCount = 0;
    DeviceNo = 1;
    RouteNo = 1;
    MaxChnNum = 5;
    CallerNo = "";
    MaxCalledTimes = 3;
    MaxAnsTimes = 1;
    NoAnsDelay = 300;
    BusyDelay = 300;
    FileName = "";
    MixerType = 0;
    FileName2 = "";
    GroupNo = 0;
    FuncNo = 0;
    Param = "";
    Remark = "";
    ExportId = 0;
    CallType = 0;
    PlayDelay = 3;
    TTSType = 0;
    TTSUpdateId = 1;
    TTSData = "";
    TTSFileName = "";
    ConfirmType = 0;
    ConfirmVoc = "";
    ConfirmDTMF = "";
    TranVDN = "";
    DialPreCode = "";
    ListenTimeLen = 0;
  }
  virtual ~CDialOutTask(){}

  const CDialOutTask& operator=(const CDialOutTask& dialouttask)
  {
    TaskId = dialouttask.TaskId;
    TaskName = dialouttask.TaskName;
    StartDate = dialouttask.StartDate;
    EndDate = dialouttask.EndDate;
    StartTime = dialouttask.StartTime;
    EndTime = dialouttask.EndTime;
    StartTime2 = dialouttask.StartTime2;
    EndTime2 = dialouttask.EndTime2;
    StartId = dialouttask.StartId;
    StartMem = dialouttask.StartMem;
    StopMem = dialouttask.StopMem;
    TotalTeles = dialouttask.TotalTeles;
    CalledCount = dialouttask.CalledCount;
    AnsCount = dialouttask.AnsCount;
    DeviceNo = dialouttask.DeviceNo;
    RouteNo = dialouttask.RouteNo;
    MaxChnNum = dialouttask.MaxChnNum;
    CallerNo = dialouttask.CallerNo;
    MaxCalledTimes = dialouttask.MaxCalledTimes;
    MaxAnsTimes = dialouttask.MaxAnsTimes;
    NoAnsDelay = dialouttask.NoAnsDelay;
    BusyDelay = dialouttask.BusyDelay;
    FileName = dialouttask.FileName;
    MixerType = dialouttask.MixerType;
    FileName2 = dialouttask.FileName2;
    GroupNo = dialouttask.GroupNo;
    FuncNo = dialouttask.FuncNo;
    Param = dialouttask.Param;
    Remark = dialouttask.Remark;
    ExportId = dialouttask.ExportId;
    CallType = dialouttask.CallType;
    PlayDelay = dialouttask.PlayDelay;
    TTSType = dialouttask.TTSType;
    TTSUpdateId = dialouttask.TTSUpdateId;
    TTSData = dialouttask.TTSData;
    TTSFileName = dialouttask.TTSFileName;
    ConfirmType = dialouttask.ConfirmType;
    ConfirmVoc = dialouttask.ConfirmVoc;
    ConfirmDTMF = dialouttask.ConfirmDTMF;
    TranVDN = dialouttask.TranVDN;
    DialPreCode = dialouttask.DialPreCode;
    ListenTimeLen = dialouttask.ListenTimeLen;
    return *this;
  }
};

class CSubPhone
{
public:
	int id; //�t��(�`�ۍ�¥)
  AnsiString SubPhone; //��ٯ��
  AnsiString Password; //���̳���
	AnsiString UserName; //���Ԃn��
	int CallRight; //����?�j 0���ϰj�Û��ɸ� 1��Ϻ������ɸ� 2��Ϻ��һ��E�� 3��Ϻ��һ��E��ֿ߯ٯ 4��Ϻ��������� 5��Ϻ��ϻːL���� 6��Ϻ���������
	AnsiString Mobile; //߯ٯ����
	AnsiString BusyTran; //�R���v������
	AnsiString NoAnsTran; //��?�Û��v������
	int BangMode; //߯ٯ����ҫ�d 0-������ 1-����ҫ�d 2-�X��ҫ�d
	AnsiString CRBT; //����
	int VoiceBox; //�����|�g�˻o���� 0-��?�[�����|�g 1-�R���ᒏ?�ÊC�v���� 2-�����v�����|�g
	AnsiString Remark; //���q�|��

public:
  CSubPhone(){}
  virtual ~CSubPhone(){}
  const CSubPhone& operator=(const CSubPhone& subphone)
  {
	  id = subphone.id;
    SubPhone = subphone.SubPhone;
    Password = subphone.Password;
	  UserName = subphone.UserName;
	  CallRight = subphone.CallRight;
	  Mobile = subphone.Mobile;
	  BusyTran = subphone.BusyTran;
	  NoAnsTran = subphone.NoAnsTran;
	  BangMode = subphone.BangMode;
	  CRBT = subphone.CRBT;
	  VoiceBox = subphone.VoiceBox;
	  Remark = subphone.Remark;
    return *this;
  }
};

class CRecdRecord
{
public:
	int id;
	AnsiString SerialNo;
	AnsiString CallerNo;
	AnsiString CustNo;
	AnsiString CalledNo;
	AnsiString CallTime;
	AnsiString RecdFile;
	int ListenId;
	AnsiString RecdContent;
	AnsiString ListenWorkerNo;
	AnsiString ListenTime;
	int ProcId;
	AnsiString ProcNote;
	AnsiString ProcWorkerNo;
	AnsiString ProcTime;

  AnsiString CustName;
  AnsiString ContAddr;

public:
  CRecdRecord()
  {
    id = 0;
	  SerialNo = "";
	  CallerNo = "";
	  CustNo = "";
	  CalledNo = "";
	  CallTime = "";
	  RecdFile = "";
	  ListenId = 0;
	  RecdContent = "";
	  ListenWorkerNo = "";
	  ListenTime = "";
	  ProcId = 0;
	  ProcNote = "";
	  ProcWorkerNo = "";
	  ProcTime = "";

    CustName = "";
    ContAddr = "";
  }

  virtual ~CRecdRecord(){}

  const CRecdRecord& operator=(const CRecdRecord& recd)
  {
    id = recd.id;
	  SerialNo = recd.SerialNo;
	  CallerNo = recd.CallerNo;
	  CustNo = recd.CustNo;
	  CalledNo = recd.CalledNo;
	  CallTime = recd.CallTime;
	  RecdFile = recd.RecdFile;
	  ListenId = recd.ListenId;
	  RecdContent = recd.RecdContent;
	  ListenWorkerNo = recd.ListenWorkerNo;
	  ListenTime = recd.ListenTime;
	  ProcId = recd.ProcId;
	  ProcNote = recd.ProcNote;
	  ProcWorkerNo = recd.ProcWorkerNo;
	  ProcTime = recd.ProcTime;

    CustName = recd.CustName;
    ContAddr = recd.ContAddr;
    return *this;
  }
};

class CAgStatus
{
public:
	short LogState; //����О�F
	short svState; //���^О�F
	short Disturbid; //��?О�F
	short Leaveid; //���О�F
public:
  CAgStatus()
  {
    LogState = -1;
    svState = 0;
    Disturbid = 0;
    Leaveid = 0;
  }
};

#define   MAX_ACPT_CONTROL_NUM         32
#define   MAX_CHECKBOX_CONTROL_NUM     50

class CAcptItemSet
{
public:
  int id;
  int SrvType;
	int Tid;
  AnsiString TFieldName;
  int TControlType;
	AnsiString TLabelCaption;
  int TXpos;
  int TYpos;
  int THeigth;
  int TWith;
  int TDataType;
	AnsiString TDefaultData;
  int TMinValue;
  int TMaxValue;
  int TIsAllowNull;
  int TExportId;
  int SrvSubType;
  AnsiString TLinkId;
  AnsiString TMaskEditStr;
  AnsiString THintStr;
  int ExportOrderId;
  int CombineMode;
  int CombineTid;
  AnsiString CombineValue;
  int ProdType;

  CLookupItemList TLookupItemList;

public:
  void Init()
  {
    id = -1;
    SrvType = -1;
	  Tid = 0;
    TFieldName = "";
    TControlType = 0;
	  TLabelCaption = "";
    TXpos = 0;
    TYpos = 0;
    THeigth = 0;
    TWith = 0;
    TDataType = 0;
	  TDefaultData = "";
    TMinValue = 0;
    TMaxValue = 0;
    TIsAllowNull = 0;
    TExportId = 0;
    SrvSubType = -1;
    TLinkId = "";
    TMaskEditStr = "";
    THintStr = "";
    ExportOrderId = 0;
    CombineMode = 0;
    CombineTid = 0;
    CombineValue = "";
    ProdType = -1;
  }
  CAcptItemSet()
  {
    Init();
  }

  virtual ~CAcptItemSet(){}

  const CAcptItemSet& operator=(const CAcptItemSet& acptitemset)
  {
    id = acptitemset.id;
	  SrvType = acptitemset.SrvType;
    Tid = acptitemset.Tid;
    TFieldName = acptitemset.TFieldName;
    TControlType = acptitemset.TControlType;
	  TLabelCaption = acptitemset.TLabelCaption;
    TXpos = acptitemset.TXpos;
    TYpos = acptitemset.TYpos;
    THeigth = acptitemset.THeigth;
    TWith = acptitemset.TWith;
    TDataType = acptitemset.TDataType;
	  TDefaultData = acptitemset.TDefaultData;
    TMinValue = acptitemset.TMinValue;
    TMaxValue = acptitemset.TMaxValue;
    TIsAllowNull = acptitemset.TIsAllowNull;
    TExportId = acptitemset.TExportId;
    SrvSubType = acptitemset.SrvSubType;
    TLinkId = acptitemset.TLinkId;
    TMaskEditStr = acptitemset.TMaskEditStr;
    THintStr = acptitemset.THintStr;
    ExportOrderId = acptitemset.ExportOrderId;
    CombineMode = acptitemset.CombineMode;
    CombineTid = acptitemset.CombineTid;
    CombineValue = acptitemset.CombineValue;
    ProdType = acptitemset.ProdType;
    return *this;
  }
};

class CAcptItemSetList
{
public:
  int ItemNum;
  int nProdType;
  int nSrvType;
  int nSrvSubType;
  CAcptItemSet *pAcptItemSet;

public:
  CAcptItemSetList()
  {
    ItemNum = 0;
    nProdType = -1;
    nSrvType = -1;
    nSrvSubType = -1;
    pAcptItemSet = NULL;
  }
  virtual ~CAcptItemSetList()
  {
    if (pAcptItemSet != NULL)
    {
      delete []pAcptItemSet;
      pAcptItemSet = NULL;
    }
  }
  int InitData(int itemnum)
  {
    if (itemnum <= 0)
      return 1;
    if (pAcptItemSet != NULL)
    {
      delete []pAcptItemSet;
      pAcptItemSet = NULL;
      ItemNum = 0;
    }
    pAcptItemSet = new CAcptItemSet[itemnum];
    if (pAcptItemSet == NULL)
      return 1;
    ItemNum = itemnum;
    return 0;
  }
};

class CAcceptEx
{
public:
  AnsiString GId; //����t��
  AnsiString SerialNo; //�Ե���
  AnsiString Itemdata[MAX_ACPT_CONTROL_NUM];

  int nItemNum;

public:
  void Init()
  {
    GId = "";
    SerialNo = "";
    for (int i=0; i<MAX_ACPT_CONTROL_NUM; i++)
      Itemdata[i] = "";
    nItemNum = 0;
  }
  CAcceptEx()
  {
    Init();
  }
};

//2014-11-09 update
class CExportField
{
public:
  int FieldType; //�������Ӷ�����: 1-ҵ�������������ֶ�,2-ҵ����������չ�ֶ�,3-�ͻ����ϱ������ֶ�,4-3-�ͻ����ϱ���չ�ֶ�
  int ControlType;
  int FieldId;
  int ExportOrderId; //������˳��
  AnsiString FieldName; //��¾�ұ�
  AnsiString FieldExplain; //��¾���S

  int CombineMode;
  int CombineTid;
  AnsiString CombineValue;

  bool bOrdered; //�������־

  bool bCanExport;
  int nDefaultItemCount;

public:
  void InitData()
  {
    FieldType = 0;
    ControlType = 0;
    FieldId = 0;
    ExportOrderId = 0;
    FieldName = "";
    FieldExplain = "";

    CombineMode = 0;
    CombineTid = 0;
    CombineValue = "";

    bOrdered = false;
    bCanExport = true;

    nDefaultItemCount = 0;
  }
  CExportField()
  {
    InitData();
  }
  void SetData(int fieldtype, int controltype, int fieldid, AnsiString strFieldName, AnsiString strFieldExplain, int exportorderid, int defaultitemcount=0)
  {
    FieldType = fieldtype;
    ControlType = controltype;
    FieldId = fieldid;
    FieldName = strFieldName;
    FieldExplain = strFieldExplain;
    ExportOrderId = exportorderid;
    nDefaultItemCount = defaultitemcount;
  }
  void SetCombineData(int combinemode, int combinetid, AnsiString combinevalue)
  {
    CombineMode = combinemode;
    CombineTid = combinetid;
    CombineValue = combinevalue;
  }
  const CExportField& operator=(const CExportField& exportfield)
  {
    FieldType = exportfield.FieldType;
    ControlType = exportfield.ControlType;
    FieldId = exportfield.FieldId;
    ExportOrderId = exportfield.ExportOrderId;
    FieldName = exportfield.FieldName;
    FieldExplain = exportfield.FieldExplain;

    CombineMode = exportfield.CombineMode;
    CombineTid = exportfield.CombineTid;
    CombineValue = exportfield.CombineValue;

    bOrdered = exportfield.bOrdered;
    bCanExport = exportfield.bCanExport;
    nDefaultItemCount = exportfield.nDefaultItemCount;
    return *this;
  }
};

class CAcceptExportField
{
public:
  int ControlType;
  int FieldId;
  AnsiString FieldName; //��¾�ұ�
  AnsiString FieldExplain; //��¾���S

public:
  CAcceptExportField()
  {
    ControlType = 0;
    FieldId = 0;
    FieldName = "";
    FieldExplain = "";
  }
  void SetData(int controltype, int fieldid, AnsiString strFieldName, AnsiString strFieldExplain)
  {
    ControlType = controltype;
    FieldId = fieldid;
    FieldName = strFieldName;
    FieldExplain = strFieldExplain;
  }
};

#define   MAX_ACCEPT_EXPORT_FIELD_NUM          64

class CAcceptExportFieldList
{
public:
  int FieldNum;
  int ProdType;
  int SrvType;
  int SrvSubType;
  bool bCreatedSheet;
  AnsiString SrvName;
  CExportField m_AcceptExportField[MAX_ACCEPT_EXPORT_FIELD_NUM]; //2014-11-09 update

public:
  CAcceptExportFieldList()
  {
    FieldNum = 0;
    ProdType = -1;
    SrvType = -1;
    SrvSubType = -1;
    SrvName = "";
    bCreatedSheet = false;
  }
};

#define   MAX_ALL_ACCEPT_EXPORT_FIELD_NUM          128
//�����˵Ļ��������λ�б�
class CAcceptExportFieldOrderList
{
public:
  int FieldNum;
  int ProdType;
  int SrvType;
  int SrvSubType;
  bool bCreatedSheet;
  AnsiString SrvName;
  CExportField m_AcceptExportField[MAX_ALL_ACCEPT_EXPORT_FIELD_NUM]; //2014-11-09 update

public:
  CAcceptExportFieldOrderList()
  {
    FieldNum = 0;
    ProdType = -1;
    SrvType = -1;
    SrvSubType = -1;
    SrvName = "";
    bCreatedSheet = false;
  }
  void InitData()
  {
    FieldNum = 0;
    ProdType = -1;
    SrvType = -1;
    SrvSubType = -1;
    SrvName = "";
    bCreatedSheet = false;
    for (int i=0; i<MAX_ALL_ACCEPT_EXPORT_FIELD_NUM; i++)
    {
      m_AcceptExportField[i].InitData();
    }
  }
};

class CIVRMenuSet
{
public:
	int id; //�t��(�`�ۍ�¥)
  AnsiString CalledNo; //��?ǻ����
	int WellcomeId; //��ƙ������Դ�d 0��ټ��?Դ�d���� 1�����¶�m����ƙ��
	int VocMenuId; //�̶x�ɵ�����Դ�d 0��ټ��?Դ�d���� 1�����¶�m���̶x�ɵ�
	AnsiString DIDSeatNo; //�����v��ǻ�N����ٯ��
	AnsiString DIDWorkerNo; //�����v��ǻ�ϕ^������
	AnsiString WorkerName; //�����v��ǻ�ϕ^���n��

public:
  CIVRMenuSet()
  {
    id = 0;
    CalledNo = "";
    WellcomeId = 1;
    VocMenuId = 1;
    DIDSeatNo= "";
    DIDWorkerNo = "";
    WorkerName = "";
  }
  virtual ~CIVRMenuSet(){}
  const CIVRMenuSet& operator=(const CIVRMenuSet& menuset)
  {
    id = menuset.id;
    CalledNo = menuset.CalledNo;
    WellcomeId = menuset.WellcomeId;
    VocMenuId = menuset.VocMenuId;
    DIDSeatNo = menuset.DIDSeatNo;
    DIDWorkerNo = menuset.DIDWorkerNo;
    WorkerName = menuset.WorkerName;
    return *this;
  }
};

class CRecvFaxRuleSet
{
public:
  int nRuleId;

	int id; //�t��(�`�ۍ�¥)
  AnsiString RuleItem; //į�uͳ�f�t
	int DepartmentId; //�㿬ǻ��������
	AnsiString DepartmentName; //�㿬ǻ�����ұ�
	AnsiString WorkerNo; //�㿬ǻ�ϕ^������
	AnsiString WorkerName; //�㿬ǻ�ϕ^���n��
  //���q���DepartmentId��R0�CͰ���R�������ͻ��R�۽�DepartmentId�R0��WorkerNo�Rŵ��0�CͰ���R�����ͻ��R

public:
  CRecvFaxRuleSet()
  {
    nRuleId = 0;

    id = 0;
    RuleItem = "";
    DepartmentId = 0;
    DepartmentName = "";
    WorkerNo = "0";
    WorkerName = "";
  }
  virtual ~CRecvFaxRuleSet(){}
  const CRecvFaxRuleSet& operator=(const CRecvFaxRuleSet& ruleset)
  {
    nRuleId = ruleset.nRuleId;

    id = ruleset.id;
    RuleItem = ruleset.RuleItem;
    DepartmentId = ruleset.DepartmentId;
    DepartmentName = ruleset.DepartmentName;
    WorkerNo = ruleset.WorkerNo;
    WorkerName = ruleset.WorkerName;
    return *this;
  }
};

class CSendFaxFileList
{
public:
  int nFileNum;
  AnsiString SendFaxFileDoc[5];

public:
  void Init()
  {
    nFileNum = 0;
    SendFaxFileDoc[0] = "";
    SendFaxFileDoc[1] = "";
    SendFaxFileDoc[2] = "";
    SendFaxFileDoc[3] = "";
    SendFaxFileDoc[4] = "";
  }
  CSendFaxFileList()
  {
    Init();
  }
};

class CRecvFaxRecord
{
public:
  int id;
  AnsiString SerialNo;
  AnsiString CallerNo;
  AnsiString CustomNo;
  AnsiString CalledNo;
  AnsiString RecvTime;
  AnsiString RecvRootPath;
  AnsiString RecvFile;
  int ReadId;
  AnsiString FaxContent;
  AnsiString ReadWorkerNo;
  AnsiString ReadTime;
  int ProcId;
  AnsiString ProcNote;
  AnsiString ProcWorkerNo;
  AnsiString ProcTime;
  AnsiString FaxCSID;
  int RecvDepartmentId;
  AnsiString RecvWorkerNo;
  int RecvPages;
  int RecvSizes;
  int RecvSpeed;
  //FileContent
  int SourceId;
  int SortType;
  int ChnType;
  int ChnNo;
};


#define   MAX_CUST_CONTROL_NUM          32
#define   MAX_CUST_QUERYCONTROL_NUM     8

class CCustExItemSet
{
public:
  int state;

	int CFieldId;
  AnsiString CFieldName;
  int CControlType;
	AnsiString CLabelCaption;
  int CXpos;
  int CYpos;
  int CHeigth;
  int CWidth;
  int CDataType;
	AnsiString CDefaultData;
  int CMinValue;
  int CMaxValue;
  int CIsAllowNull;
  int CDispId;
  int CInputId;
  int CDispOrder;
  int CDispWidth;
  int CSearchId;
  int CIsUsed;
  int CExportId;
  int ExportOrderId;
  int AcceptExId;

  int ReadDataType; //���Ū���ӷ�:1-DB��Ʈw,2-HTTP����,3-WEBSERVICE����,4-�P�BCRM��� 2016-12-25
  AnsiString ReadFieldTag; //���Ū����ñ(���f�������W��) 2016-12-25
  int SaveToDBFlag; //�O�_�N��ƫO�s��DB��Ʈw
  int ReadDataMinLen; //2022-01-21 ���w��Ʀ��ĳ̤p����
  int ReadDataMaxLen; //2022-01-21 ���w��Ʀ��ĳ̤j����

  CLookupItemList TLookupItemList;

public:
  void Init()
  {
    state = 0;

    CFieldId = 0;
    CFieldName = "";
    CControlType = 0;
	  CLabelCaption = "";
    CXpos = 0;
    CYpos = 0;
    CHeigth = 0;
    CWidth = 0;
    CDataType = 0;
	  CDefaultData = "";
    CMinValue = 0;
    CMaxValue = 0;
    CIsAllowNull = 0;
    CDispId = 0;
    CInputId = 0;
    CDispOrder = 0;
    CDispWidth = 80;
    CSearchId = 0;
    CIsUsed = 0;
    CExportId = 0;
    ExportOrderId = 0;
    AcceptExId = 0;

    ReadDataType = 1;
    ReadFieldTag = "";
    SaveToDBFlag = 1;
    ReadDataMinLen = 0;
    ReadDataMaxLen = 65535;
  }
  CCustExItemSet()
  {
    Init();
  }

  virtual ~CCustExItemSet(){}

  const CCustExItemSet& operator=(const CCustExItemSet& custitemset)
  {
    CFieldId = custitemset.CFieldId;
    CFieldName = custitemset.CFieldName;
    CControlType = custitemset.CControlType;
	  CLabelCaption = custitemset.CLabelCaption;
    CXpos = custitemset.CXpos;
    CYpos = custitemset.CYpos;
    CHeigth = custitemset.CHeigth;
    CWidth = custitemset.CWidth;
    CDataType = custitemset.CDataType;
	  CDefaultData = custitemset.CDefaultData;
    CMinValue = custitemset.CMinValue;
    CMaxValue = custitemset.CMaxValue;
    CIsAllowNull = custitemset.CIsAllowNull;
    CDispId = custitemset.CDispId;
    CInputId = custitemset.CInputId;
    CDispOrder = custitemset.CDispOrder;
    CDispWidth = custitemset.CDispWidth;
    CSearchId = custitemset.CSearchId;
    CIsUsed = custitemset.CIsUsed;
    CExportId = custitemset.CExportId;
    ExportOrderId = custitemset.ExportOrderId;
    AcceptExId = custitemset.AcceptExId;
    ReadDataType = custitemset.ReadDataType;
    ReadFieldTag = custitemset.ReadFieldTag;
    SaveToDBFlag = custitemset.SaveToDBFlag;
    ReadDataMinLen = custitemset.ReadDataMinLen;
    ReadDataMaxLen = custitemset.ReadDataMaxLen;
    return *this;
  }
};

class CCustExItemSetList
{
public:
  int ItemNum;
  CCustExItemSet m_CustExItemSet[MAX_CUST_CONTROL_NUM];

public:
  CCustExItemSetList()
  {
    ItemNum = 0;
  }
  virtual ~CCustExItemSetList()
  {
  }
  int InitData(int itemnum)
  {
    ItemNum = itemnum;
    return 0;
  }
};

class CCustEx
{
public:
  AnsiString CustomNo; //���c����
  AnsiString Itemdata[MAX_CUST_CONTROL_NUM];

  int nItemNum;

public:
  void Init()
  {
    CustomNo = "";
    for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
      Itemdata[i] = "";
    nItemNum = 0;
  }
  CCustEx()
  {
    Init();
  }
  const CCustEx& operator=(const CCustEx& custex)
  {
    CustomNo = custex.CustomNo;
    nItemNum = custex.nItemNum;
    for (int i=0; i<MAX_CUST_CONTROL_NUM; i++)
      Itemdata[i] = custex.Itemdata[i];
    return *this;
  }
};

class CCustomExportField
{
public:
  int ControlType;
  int FieldId;
  AnsiString FieldName; //��¾�ұ�
  AnsiString FieldExplain; //��¾���S

public:
  CCustomExportField()
  {
    ControlType = 0;
    FieldId = 0;
    FieldName = "";
    FieldExplain = "";
  }
  const CCustomExportField& operator=(const CCustomExportField& customexportfield)
  {
    ControlType = customexportfield.ControlType;
    FieldId = customexportfield.FieldId;
    FieldName = customexportfield.FieldName;
    FieldExplain = customexportfield.FieldExplain;
    return *this;
  }
  void SetData(int controltype, int fieldid, AnsiString strFieldName, AnsiString strFieldExplain)
  {
    ControlType = controltype;
    FieldId = fieldid;
    FieldName = strFieldName;
    FieldExplain = strFieldExplain;
  }
};

#define   MAX_CUST_EXPORT_FIELD_NUM          64

class CCustomExportFieldList
{
public:
  int FieldNum;
  CExportField m_CustomExportField[MAX_CUST_EXPORT_FIELD_NUM]; //2014-11-09 update

public:
  CCustomExportFieldList()
  {
    FieldNum = 0;
  }
};

class CCustomAllFieldList
{
public:
  int FieldNum;
  CCustomExportField m_CustomExportField[MAX_CUST_EXPORT_FIELD_NUM];

public:
  CCustomAllFieldList()
  {
    FieldNum = 0;
  }
};

class CCustExDispField
{
public:
  int ControlType;
  int FieldId;
  AnsiString FieldName; //��¾�ұ�
  AnsiString FieldExplain; //��¾���S

public:
  CCustExDispField()
  {
    ControlType = 0;
    FieldId = 0;
    FieldName = "";
    FieldExplain = "";
  }
  void SetData(int controltype, int fieldid, AnsiString strFieldName, AnsiString strFieldExplain)
  {
    ControlType = controltype;
    FieldId = fieldid;
    FieldName = strFieldName;
    FieldExplain = strFieldExplain;
  }
};

#define   MAX_CUST_DISP_FIELD_NUM          32

class CCustExDispFieldList
{
public:
  int FieldNum;
  CCustExDispField m_CustExDispField[MAX_CUST_DISP_FIELD_NUM];

public:
  CCustExDispFieldList()
  {
    FieldNum = 0;
  }
};

class CTabAuth
{
public:
  int MainTabID;
  int SubTabID;
  AnsiString TabName;
  int AdminAuth;
  int MaintAuth;
  int OpertAuth;
  int MonitAuth;
  int AgentAuth;

public:
  CTabAuth()
  {
    MainTabID = 0;
    SubTabID = 0;
    TabName = "";
    AdminAuth = 0;
    MaintAuth = 0;
    OpertAuth = 0;
    MonitAuth = 0;
    AgentAuth = 0;
  }
  const CTabAuth& operator=(const CTabAuth& tabauth)
  {
    MainTabID = tabauth.MainTabID;
    SubTabID = tabauth.SubTabID;
    TabName = tabauth.TabName;
    AdminAuth = tabauth.AdminAuth;
    MaintAuth = tabauth.MaintAuth;
    OpertAuth = tabauth.OpertAuth;
    MonitAuth = tabauth.MonitAuth;
    AgentAuth = tabauth.AgentAuth;
    return *this;
  }
};

#define   MAX_TAB_PAGE_NUM          64

class CTabAuthList
{
public:
  int TabNum;
  CTabAuth m_CTabAuth[MAX_TAB_PAGE_NUM];

public:
  CTabAuthList()
  {
    TabNum = 0;
  }
  int GetAuthByTabID(int workertype, int maintabid, int subtabid)
  {
    int authid=0;

    for (int i = 0; i < TabNum; i ++)
    {
      if (m_CTabAuth[i].MainTabID == maintabid && m_CTabAuth[i].SubTabID == 0)
      {
        switch (workertype)
        {
        case 1:
          authid = m_CTabAuth[i].AgentAuth;
          break;
        case 2:
          authid = m_CTabAuth[i].OpertAuth;
          break;
        case 3:
          authid = m_CTabAuth[i].MonitAuth;
          break;
        case 4:
          authid = m_CTabAuth[i].MaintAuth;
          break;
        case 9:
          authid = m_CTabAuth[i].AdminAuth;
          break;
        default:
          authid = m_CTabAuth[i].AgentAuth;
          break;
        }
      }
    }
    if (subtabid == 0)
    {
      return authid;
    }
    else
    {
      if (authid == 0)
        return 0;
    }
    authid = 0;
    for (int i = 0; i < TabNum; i ++)
    {
      if (m_CTabAuth[i].MainTabID == maintabid && m_CTabAuth[i].SubTabID == subtabid)
      {
        switch (workertype)
        {
        case 1:
          authid = m_CTabAuth[i].AgentAuth;
          break;
        case 2:
          authid = m_CTabAuth[i].OpertAuth;
          break;
        case 3:
          authid = m_CTabAuth[i].MonitAuth;
          break;
        case 4:
          authid = m_CTabAuth[i].MaintAuth;
          break;
        case 9:
          authid = m_CTabAuth[i].AdminAuth;
          break;
        default:
          authid = m_CTabAuth[i].AgentAuth;
          break;
        }
      }
    }
    return authid;
  }
};

class CManDialOutTask
{
public:
  int TaskId;
  AnsiString TaskCode;
  AnsiString TaskName;
  int StartId;
  AnsiString StartDate;
  AnsiString EndDate;
  AnsiString CallerNo;
  AnsiString CreateTime;
  AnsiString CreateWorker;
  AnsiString Param;
  AnsiString URLAddr;
  AnsiString URLOrder;
  AnsiString URLQuery;
  AnsiString URLManOrder;
  AnsiString URLPrice;
  AnsiString Remark;
  AnsiString DialPreCode;

public:
  CManDialOutTask()
  {
    TaskId = 0;
    TaskCode = Now().FormatString("yyyymmddhhnnss");
    TaskName = "";
    StartId = 1;
    StartDate = Now().FormatString("yyyy-mm-dd");
    EndDate = IncMonth(Date(), 6).FormatString("yyyy-mm-dd");
    CallerNo = "";
    CreateTime = Now().FormatString("yyyy-mm-dd hh:nn:ss");
    CreateWorker = "";
    Param = "";
    URLAddr = "";
    URLOrder = "";
    URLQuery = "";
    URLManOrder = "";
    URLPrice = "";
    Remark = "";
    DialPreCode = "";
  }
  virtual ~CManDialOutTask(){}

  const CManDialOutTask& operator=(const CManDialOutTask& dialouttask)
  {
    TaskId = dialouttask.TaskId;
    TaskCode = dialouttask.TaskCode;
    TaskName = dialouttask.TaskName;
    StartId = dialouttask.StartId;
    StartDate = dialouttask.StartDate;
    EndDate = dialouttask.EndDate;
    CallerNo = dialouttask.CallerNo;
    CreateTime = dialouttask.CreateTime;
    CreateWorker = dialouttask.CreateWorker;
    Param = dialouttask.Param;
    URLAddr = dialouttask.URLAddr;
    URLOrder = dialouttask.URLOrder;
    URLQuery = dialouttask.URLQuery;
    URLManOrder = dialouttask.URLManOrder;
    URLPrice = dialouttask.URLPrice;
    Remark = dialouttask.Remark;
    DialPreCode = dialouttask.DialPreCode;
    return *this;
  }
};

#define   MAX_DIALOUT_CONTROL_NUM     32

class CDialOutItemSet
{
public:
  int id;
  int TaskId;
	int Tid;
  AnsiString TFieldName;
  int TControlType;
	AnsiString TLabelCaption;
  int TXpos;
  int TYpos;
  int THeigth;
  int TWith;
  int TDataType;
	AnsiString TDefaultData;
  int TMinValue;
  int TMaxValue;
  int TIsAllowNull;
  int TExportId;
  int TDispId;
  int TModifyId;
  AnsiString TLinkId;
  AnsiString TMaskEditStr;
  AnsiString THintStr;
  int ExportOrderId;
  int CombineMode;
  int CombineTid;
  AnsiString CombineValue;

  int SearchId; //2019-05-27 ����d�߼Ч�: 0-�L,1-�ҽk�d��,2-��Ǭd��
  int SearchOrder; //2019-05-27  ����d�߶���

  int DispOrder; //2019-07-30 ��ܶ���

  CLookupItemList TLookupItemList;

public:
  void Init()
  {
    id = -1;
    TaskId = -1;
	  Tid = 0;
    TFieldName = "";
    TControlType = 0;
	  TLabelCaption = "";
    TXpos = 0;
    TYpos = 0;
    THeigth = 0;
    TWith = 0;
    TDataType = 0;
	  TDefaultData = "";
    TMinValue = 0;
    TMaxValue = 0;
    TIsAllowNull = 0;
    TExportId = 0;
    TDispId = 0;
    TModifyId = 0;
    TLinkId = "";
    TMaskEditStr = "";
    THintStr = "";
    ExportOrderId = 0;
    CombineMode = 0;
    CombineTid = 0;
    CombineValue = "";
    SearchId = 0;
    SearchOrder = 0;
    DispOrder = 0;
  }
  CDialOutItemSet()
  {
    Init();
  }

  virtual ~CDialOutItemSet(){}

  const CDialOutItemSet& operator=(const CDialOutItemSet& dialoutitemset)
  {
    id = dialoutitemset.id;
	  TaskId = dialoutitemset.TaskId;
    Tid = dialoutitemset.Tid;
    TFieldName = dialoutitemset.TFieldName;
    TControlType = dialoutitemset.TControlType;
	  TLabelCaption = dialoutitemset.TLabelCaption;
    TXpos = dialoutitemset.TXpos;
    TYpos = dialoutitemset.TYpos;
    THeigth = dialoutitemset.THeigth;
    TWith = dialoutitemset.TWith;
    TDataType = dialoutitemset.TDataType;
	  TDefaultData = dialoutitemset.TDefaultData;
    TMinValue = dialoutitemset.TMinValue;
    TMaxValue = dialoutitemset.TMaxValue;
    TIsAllowNull = dialoutitemset.TIsAllowNull;
    TExportId = dialoutitemset.TExportId;
    TDispId = dialoutitemset.TDispId;
    TModifyId = dialoutitemset.TModifyId;
    TLinkId = dialoutitemset.TLinkId;
    TMaskEditStr = dialoutitemset.TMaskEditStr;
    THintStr = dialoutitemset.THintStr;
    ExportOrderId = dialoutitemset.ExportOrderId;
    CombineMode = dialoutitemset.CombineMode;
    CombineTid = dialoutitemset.CombineTid;
    CombineValue = dialoutitemset.CombineValue;
    SearchId = dialoutitemset.SearchId; //2019-05-27
    SearchOrder = dialoutitemset.SearchOrder; //2019-05-27
    DispOrder = dialoutitemset.DispOrder;
    return *this;
  }
};

class CDialOutItemSetList
{
public:
  int ItemNum;
  int TaskId;
  CDialOutItemSet *pDialOutItemSet;

public:
  CDialOutItemSetList()
  {
    ItemNum = 0;
    TaskId = -1;
    pDialOutItemSet = NULL;
  }
  virtual ~CDialOutItemSetList()
  {
    if (pDialOutItemSet != NULL)
    {
      delete []pDialOutItemSet;
      pDialOutItemSet = NULL;
    }
  }
  int InitData(int itemnum)
  {
    if (itemnum <= 0)
      return 1;
    if (pDialOutItemSet != NULL)
    {
      delete []pDialOutItemSet;
      pDialOutItemSet = NULL;
      ItemNum = 0;
    }
    pDialOutItemSet = new CDialOutItemSet[itemnum];
    if (pDialOutItemSet == NULL)
      return 1;
    ItemNum = itemnum;
    return 0;
  }
};

class CDialOutTele
{
public:
  int Id;
  AnsiString DialId;
  int TaskId;
  AnsiString TaskCode;
  AnsiString TaskName;
  AnsiString BatchId;
  AnsiString CallerNo;
  AnsiString CalledNo;
  AnsiString TeleNo;
  AnsiString CustName;
  AnsiString CustAddr;
  AnsiString CustomNo;
  AnsiString AccountNo;
  AnsiString InsertTime;
  int CalledTimes;
  int AnsTimes;
  AnsiString LastCallTime;
  AnsiString AnsTime;
  AnsiString RelTime;
  int LastResult;
  int EndCode1;
  int EndCode2;
  AnsiString OrderCode;
  int CallBackId;
  AnsiString CallBackTime;
  AnsiString TaskWorkerNo;
  AnsiString TalkWorkerNo;
  int Priority;
  AnsiString SerialNo;
  AnsiString RecdRootPath;
  AnsiString RecdFile;
  AnsiString DialLog;
  AnsiString URLAddr;
  AnsiString URLOrder;
  AnsiString URLQuery;
  AnsiString URLManOrder;
  AnsiString URLPrice;

  int ItemNum;
  AnsiString ItemData[32];
public:
  CDialOutTele()
  {
    Id = 0;
    DialId = "";
    TaskId = 0;
    TaskCode = "";
    TaskName = "";
    BatchId = "";
    CallerNo = "";
    CalledNo = "";
    TeleNo = "";
    CustName = "";
    CustAddr = "";
    CustomNo = "";
    AccountNo = "";
    InsertTime = "";
    CalledTimes = 0;
    AnsTimes = 0;
    LastCallTime = "";
    AnsTime = "";
    RelTime = "";
    LastResult = 0;
    EndCode1 = 0;
    EndCode2 = 0;
    OrderCode = "";
    CallBackId = 0;
    CallBackTime = "";
    TaskWorkerNo = "";
    TalkWorkerNo = "";
    Priority = 0;
    SerialNo = "";
    RecdRootPath = "";
    RecdFile = "";
    DialLog = "";
    URLAddr = "";
    URLOrder = "";
    URLQuery = "";
    URLManOrder = "";
    URLPrice = "";

    ItemNum = 0;
    for (int i=0; i<32; i++)
      ItemData[i] = "";
  }
  virtual ~CDialOutTele(){}
  const CDialOutTele& operator=(const CDialOutTele& dialouttele)
  {
    Id = dialouttele.Id;
    DialId = dialouttele.DialId;
    TaskId = dialouttele.TaskId;
    TaskCode = dialouttele.TaskCode;
    TaskName = dialouttele.TaskName;
    BatchId = dialouttele.BatchId;
    CallerNo = dialouttele.CallerNo;
    CalledNo = dialouttele.CalledNo;
    TeleNo = dialouttele.TeleNo;
    CustName = dialouttele.CustName;
    CustAddr = dialouttele.CustAddr;
    CustomNo = dialouttele.CustomNo;
    AccountNo = dialouttele.AccountNo;
    InsertTime = dialouttele.InsertTime;
    CalledTimes = dialouttele.CalledTimes;
    AnsTimes = dialouttele.AnsTimes;
    LastCallTime = dialouttele.LastCallTime;
    AnsTime = dialouttele.AnsTime;
    RelTime = dialouttele.RelTime;
    LastResult = dialouttele.LastResult;
    EndCode1 = dialouttele.EndCode1;
    EndCode2 = dialouttele.EndCode2;
    OrderCode = dialouttele.OrderCode;
    CallBackId = dialouttele.CallBackId;
    CallBackTime = dialouttele.CallBackTime;
    TaskWorkerNo = dialouttele.TaskWorkerNo;
    TalkWorkerNo = dialouttele.TalkWorkerNo;
    Priority = dialouttele.Priority;
    SerialNo = dialouttele.SerialNo;
    RecdRootPath = dialouttele.RecdRootPath;
    RecdFile = dialouttele.RecdFile;
    DialLog = dialouttele.DialLog;
    URLAddr = dialouttele.URLAddr;
    URLOrder = dialouttele.URLOrder;
    URLQuery = dialouttele.URLQuery;
    URLManOrder = dialouttele.URLManOrder;
    URLPrice = dialouttele.URLPrice;
    ItemNum = dialouttele.ItemNum;
    for (int i=0; i<32; i++)
      ItemData[i] = dialouttele.ItemData[i];
    return *this;
  }
};

class CDialOutImportField
{
public:
  int ControlType;
  int FieldId;
  int ExcelFieldId;
  AnsiString FieldName; //��¾�ұ�
  AnsiString FieldExplain; //��¾���S

public:
  CDialOutImportField()
  {
    ControlType = 0;
    FieldId = 0;
    ExcelFieldId = -1;
    FieldName = "";
    FieldExplain = "";
  }
  const CDialOutImportField& operator=(const CDialOutImportField& dialoutimportfield)
  {
    ControlType = dialoutimportfield.ControlType;
    FieldId = dialoutimportfield.FieldId;
    ExcelFieldId = dialoutimportfield.ExcelFieldId;
    FieldName = dialoutimportfield.FieldName;
    FieldExplain = dialoutimportfield.FieldExplain;
    return *this;
  }
  void SetData(int fieldtype, int controltype, int fieldid, AnsiString strFieldName, AnsiString strFieldExplain)
  {
    ControlType = controltype;
    FieldId = fieldid;
    FieldName = strFieldName;
    FieldExplain = strFieldExplain;
  }
};

#define   MAX_DIALOUT_FIELD_NUM     64

class CDialOutImportFieldList
{
public:
  int FieldNum;
  int TaskId;
  AnsiString TaskName;
  CDialOutImportField m_DialOutImportField[MAX_DIALOUT_FIELD_NUM];

public:
  CDialOutImportFieldList()
  {
    FieldNum = 0;
    TaskId = -1;
    TaskName = "";
  }
};

class CDialOutExportField
{
public:
  int ControlType;
  int FieldId;
  int ExcelFieldId;
  AnsiString FieldName; //��¾�ұ�
  AnsiString FieldExplain; //��¾���S

public:
  CDialOutExportField()
  {
    ControlType = 0;
    FieldId = 0;
    ExcelFieldId = -1;
    FieldName = "";
    FieldExplain = "";
  }
  const CDialOutExportField& operator=(const CDialOutExportField& dialoutexportfield)
  {
    ControlType = dialoutexportfield.ControlType;
    FieldId = dialoutexportfield.FieldId;
    ExcelFieldId = dialoutexportfield.ExcelFieldId;
    FieldName = dialoutexportfield.FieldName;
    FieldExplain = dialoutexportfield.FieldExplain;
    return *this;
  }
  void SetData(int fieldtype, int controltype, int fieldid, AnsiString strFieldName, AnsiString strFieldExplain)
  {
    ControlType = controltype;
    FieldId = fieldid;
    FieldName = strFieldName;
    FieldExplain = strFieldExplain;
  }
};

class CDialOutExportFieldList
{
public:
  int FieldNum;
  int TaskId;
  AnsiString TaskName;
  CDialOutExportField m_DialOutExportField[MAX_DIALOUT_FIELD_NUM];

public:
  CDialOutExportFieldList()
  {
    FieldNum = 0;
    TaskId = -1;
    TaskName = "";
  }
};

//------------------------------------------------------------------------------
class CDutyTel
{
public:
  int id;
  int DayType;
  AnsiString StartDayTime;
  AnsiString EndDayTime;
  AnsiString StartTime1;
  AnsiString EndTime1;
  AnsiString StartTime2;
  AnsiString EndTime2;
  AnsiString DutyTele1;
  AnsiString DutyTele2;
  int OpenFlag;
  AnsiString WorkerName;

public:
  CDutyTel()
  {
    id = 0;
    DayType = 1;
    StartDayTime = 0;
    EndDayTime = 0;
    StartTime1 = "09:00";
    EndTime1 = "12:00";
    StartTime2 = "14:00";
    EndTime2 = "18:00";
    DutyTele1 = "";
    DutyTele2 = "";
    OpenFlag = 1;
    WorkerName = "";
  }

  virtual ~CDutyTel(){}

  const CDutyTel& operator=(const CDutyTel& dutytel)
  {
    id = dutytel.id;
    DayType = dutytel.DayType;
    StartDayTime = dutytel.StartDayTime;
    EndDayTime = dutytel.EndDayTime;
    StartTime1 = dutytel.StartTime1;
    EndTime1 = dutytel.EndTime1;
    StartTime2 = dutytel.StartTime2;
    EndTime2 = dutytel.EndTime2;
    DutyTele1 = dutytel.DutyTele1;
    DutyTele2 = dutytel.DutyTele2;
    OpenFlag = dutytel.OpenFlag;
    WorkerName = dutytel.WorkerName;
    return *this;
  }
};

class CWorkTimeSet
{
public:
  int id;
  AnsiString CTID;
  AnsiString HotLine;
  AnsiString DTMFKeys;
  int DayType;
  AnsiString StartDay;
  AnsiString EndDay;
  AnsiString StartTime1;
  AnsiString EndTime1;
  AnsiString StartTime2;
  AnsiString EndTime2;
  int WorkType;
  int OpenFlag;

public:
  CWorkTimeSet()
  {
    id = 0;
    CTID = "";
    HotLine = "*";
    DTMFKeys = "*";
    DayType = 1;
    StartDay = "0";
    EndDay = "6";
    StartTime1 = "09:00";
    EndTime1 = "12:00";
    StartTime2 = "12:00";
    EndTime2 = "18:00";
    WorkType = 1;
    OpenFlag = 1;
  }

  virtual ~CWorkTimeSet(){}

  const CWorkTimeSet& operator=(const CWorkTimeSet& worktimeset)
  {
    id = worktimeset.id;
    CTID = worktimeset.CTID;
    HotLine = worktimeset.HotLine;
    DTMFKeys = worktimeset.DTMFKeys;
    DayType = worktimeset.DayType;
    StartDay = worktimeset.StartDay;
    EndDay = worktimeset.EndDay;
    StartTime1 = worktimeset.StartTime1;
    EndTime1 = worktimeset.EndTime1;
    StartTime2 = worktimeset.StartTime2;
    EndTime2 = worktimeset.EndTime2;
    WorkType = worktimeset.WorkType;
    OpenFlag = worktimeset.OpenFlag;
    return *this;
  }
};
//2019-07-31
class CDialOutDispFieldItemSet
{
public:
  int Tid;
  AnsiString TFieldName;
	AnsiString TLabelCaption;
  int TWith;
  int TDispId;
  int TDispOrder;

public:
  void Init()
  {
	  Tid = -1;
    TFieldName = "";
	  TLabelCaption = "";
    TWith = 0;
    TDispId = 0;
    TDispOrder = 0;
  }
  CDialOutDispFieldItemSet()
  {
    Init();
  }

  virtual ~CDialOutDispFieldItemSet(){}
};


class CWebURL
{
public:
  int state;
  AnsiString WebName;
  AnsiString WebURL;
  TMenuItem *pMenuItem;

public:
  CWebURL()
  {
    state = 0;
    WebName = "";
    WebURL = "";
    pMenuItem = NULL;
  }

  virtual ~CWebURL(){}
};
//---------------------------------------------------------------------------
#endif

