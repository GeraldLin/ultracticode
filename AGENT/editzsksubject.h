//---------------------------------------------------------------------------

#ifndef editzsksubjectH
#define editzsksubjectH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include <Dialogs.hpp>
#include "CSPIN.h"
#include <ComCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFormZSKSubject : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label11;
  TEdit *editMainTitle;
  TComboBox *cbIssueStatus;
  TEdit *editRemark;
  TMemo *memoExplainCont;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label8;
  TEdit *editSubTitle;
  TLabel *Label12;
  TEdit *editSubjectTitle;
  TLabel *Label14;
  TButton *Button3;
  TOpenDialog *OpenDialog2;
  TLabel *Label15;
  TEdit *editVersionNo;
  TLabel *Label1;
  TComboBox *cbSecrecyId;
  TLabel *Label5;
  TComboBox *cbDepartmentId;
  TLabel *Label6;
  TCSpinEdit *cseOrderNo;
  TListView *lvAppendix;
  TPopupMenu *PopupMenu1;
  TMenuItem *N1;
  void __fastcall cbIssueStatusKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSubjectTitleChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall cbIssueStatusChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall N1Click(TObject *Sender);
  void __fastcall lvAppendixSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
private:	// User declarations
public:		// User declarations
  __fastcall TFormZSKSubject(TComponent* Owner);

  bool bModify;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CZSKSubject ZSKSubject;
  void SetModifyType(bool isinsert);
  void SetZSKSubjectData(CZSKSubject& zsksubject);
  int GetZSKSubjectData();
  void ClearZSKSubjectData(CZSKSub& zsksub);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormZSKSubject *FormZSKSubject;
//---------------------------------------------------------------------------
#endif
