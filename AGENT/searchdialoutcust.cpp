//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "searchdialoutcust.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormSearchDialoutCust *FormSearchDialoutCust;

int g_nPreCreateQryTaskId=-1;

extern AnsiString DIAL1WhereSql;
extern AnsiString DIAL1OrderFieldName;
extern AnsiString g_CurDialListId, g_CurDialId;

extern int GetControlWith(int nDefaultWith, int nType);

//---------------------------------------------------------------------------
__fastcall TFormSearchDialoutCust::TFormSearchDialoutCust(TComponent* Owner)
  : TForm(Owner)
{
  DialOutQryItemNum = 0;
}
//---------------------------------------------------------------------------
void TFormSearchDialoutCust::CreateDialOutExQryControl(int nTaskId)
{
  int nShortEditWith=120;
  int nEditLeft=100, nEditTop=52;
  int i, j;

  if (FormMain->m_pDialOutItemSetList == NULL)
    return;
  if (FormMain->m_pDialOutItemSetList[0].ItemNum == 0)
    return;

  for (i=0; i<FormMain->DialOutItemSetListCount; i++)
  {
    if (nTaskId == FormMain->m_pDialOutItemSetList[i].TaskId)
    {
      break;
    }
  }
  if (i >= FormMain->DialOutItemSetListCount)
  {
    for (i=0; i<FormMain->DialOutItemSetListCount; i++)
    {
      if (0 == FormMain->m_pDialOutItemSetList[i].TaskId)
      {
        break;
      }
    }
  }
  if (i >= FormMain->DialOutItemSetListCount)
  {
    for (i=0; i<MAX_DIALOUT_CONTROL_NUM; i++)
    {
      if (TDialOutQryLabelList[i] != NULL)
      {
        delete TDialOutQryLabelList[i];
        TDialOutQryLabelList[i] = NULL;
      }
      if (TDialOutQryInputList[i] != NULL)
      {
        delete TDialOutQryInputList[i];
        TDialOutQryInputList[i] = NULL;
      }
    }
    g_nPreCreateQryTaskId = -1;
    m_pSelDialOutItemSetList = NULL;
    return;
  }
  m_pSelDialOutItemSetList = &FormMain->m_pDialOutItemSetList[i];

  if (g_nPreCreateQryTaskId == nTaskId)
  {
    editDialOutTeleNo->Text = "";
    editDialOutCustName->Text = "";
    editDialOutAccountNo->Text = "";

    for (int i=0; i<MAX_DIALOUT_CONTROL_NUM; i++)
    {
      if (TDialOutQryInputList[i] == NULL)
        continue;
      switch (m_pSelDialOutItemSetList->pDialOutItemSet[DialOutQryItemId[i]].TControlType)
      {
      case 1: //文本下拉框
        ((TComboBox *)TDialOutQryInputList[i])->Text = "不選擇";
        break;
      case 2: //數值下拉框
        ((TComboBox *)TDialOutQryInputList[i])->Text = "不選擇";
        break;
      case 3: //長文本框
        ((TEdit *)TDialOutQryInputList[i])->Text = "";
        break;
      case 4: //短文本框
        ((TEdit *)TDialOutQryInputList[i])->Text = "";
        break;
      case 5: //數值框
        ((TEdit *)TDialOutQryInputList[i])->Text = "";
        break;
      case 6: //金額框
        ((TEdit *)TDialOutQryInputList[i])->Text = "";
        break;
      case 7: //日期框
        //((TDateTimePicker *)TDialOutQryInputList[i])->Date
        break;
      case 8: //是非框
        ((TComboBox *)TDialOutQryInputList[i])->Text = "不選擇";
        break;
      }
    }
    return;
  }

  g_nPreCreateQryTaskId = nTaskId;

  //模糊查詢
  j = 0;
  for (i=0; i<m_pSelDialOutItemSetList->ItemNum && i<MAX_DIALOUT_CONTROL_NUM; i++)
  {
    if (m_pSelDialOutItemSetList->pDialOutItemSet[i].SearchId != 1)
      continue;
    switch (m_pSelDialOutItemSetList->pDialOutItemSet[i].TControlType)
    {
    case 1: //文本下拉框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 2: //數值下拉框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = /FormKeyDown;
      ((TComboBox *)TDialOutQryInputList[j])->OnKeyPress = FormMain->MyComboBoxKeyPressEvent;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 3: //長文本框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = "";
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+" like ";
      j++;
      break;
    case 4: //短文本框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = "";
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+" like ";
      j++;
      break;
    case 5: //數值框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+">=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TCSpinEdit(this);
      ((TCSpinEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TCSpinEdit *)TDialOutQryInputList[j])->MinValue = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMinValue;
      ((TCSpinEdit *)TDialOutQryInputList[j])->MaxValue = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMaxValue;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Value = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMinValue;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      //((TCSpinEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+">=";
      j++;
      if (j >= MAX_DIALOUT_CONTROL_NUM)
        break;
      if (nEditLeft == 850)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+25;
      }
      else
      {
        nEditLeft = nEditLeft+250;
      }
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"<=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TCSpinEdit(this);
      ((TCSpinEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TCSpinEdit *)TDialOutQryInputList[j])->MinValue = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMinValue;
      ((TCSpinEdit *)TDialOutQryInputList[j])->MaxValue = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMaxValue;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Value = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMaxValue;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      //((TCSpinEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TCSpinEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"<=";
      j++;
      break;
    case 6: //金額框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+">=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMinValue;
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 20;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+">=";
      j++;
      if (j >= MAX_DIALOUT_CONTROL_NUM)
        break;
      if (nEditLeft == 850)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+25;
      }
      else
      {
        nEditLeft = nEditLeft+250;
      }
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"<=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMaxValue;
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 20;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"<=";
      j++;
      break;
    case 7: //日期框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+">=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TDateTimePicker(this);
      ((TDateTimePicker *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Kind = dtkDate;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Date = IncMonth(Date(),-1);
      ((TDateTimePicker *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      //((TDateTimePicker *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+">=";
      j++;
      if (j >= MAX_DIALOUT_CONTROL_NUM)
        break;
      if (nEditLeft == 850)
      {
        nEditLeft = 100;
        nEditTop = nEditTop+25;
      }
      else
      {
        nEditLeft = nEditLeft+250;
      }
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"<=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TDateTimePicker(this);
      ((TDateTimePicker *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Kind = dtkDate;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Date = Date();
      ((TDateTimePicker *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      //((TDateTimePicker *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"<=";
      j++;
      break;
    case 8: //是非框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TComboBox *)TDialOutQryInputList[j])->OnKeyPress = FormMain->MyComboBoxKeyPressEvent;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], dmAgent->YesNoItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 9: //長文本下拉框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    }
    if (j >= MAX_DIALOUT_CONTROL_NUM)
      break;
    if (nEditLeft == 850)
    {
      nEditLeft = 100;
      nEditTop = nEditTop+25;
    }
    else
    {
      nEditLeft = nEditLeft+250;
    }
  }

  //精準查詢
  DialOutQryItemNum = j;
  if (nEditLeft == 100)
  {
    Label2->Top = nEditTop+8;
    nEditTop = nEditTop+30;
  }
  else
  {
    Label2->Top = nEditTop+30;
    nEditTop = nEditTop+52;
  }

  nEditLeft = 100;

  for (i=0; i<m_pSelDialOutItemSetList->ItemNum && i<MAX_DIALOUT_CONTROL_NUM; i++)
  {
    if (m_pSelDialOutItemSetList->pDialOutItemSet[i].SearchId != 2)
      continue;
    switch (m_pSelDialOutItemSetList->pDialOutItemSet[i].TControlType)
    {
    case 1: //文本下拉框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 2: //數值下拉框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = /FormKeyDown;
      ((TComboBox *)TDialOutQryInputList[j])->OnKeyPress = FormMain->MyComboBoxKeyPressEvent;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 3: //長文本框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = "";
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 4: //短文本框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = "";
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 5: //數值框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = "";
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 6: //金額框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TEdit(this);
      ((TEdit *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TEdit *)TDialOutQryInputList[j])->Text = m_pSelDialOutItemSetList->pDialOutItemSet[i].TMinValue;
      ((TEdit *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TEdit *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TEdit *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TEdit *)TDialOutQryInputList[j])->MaxLength = 20;
      //((TEdit *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TEdit *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 7: //日期框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"=";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TDateTimePicker(this);
      ((TDateTimePicker *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Kind = dtkDate;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Date = IncMonth(Date(),-1);
      ((TDateTimePicker *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      //((TDateTimePicker *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TDateTimePicker *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 8: //是非框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      ((TComboBox *)TDialOutQryInputList[j])->OnKeyPress = FormMain->MyComboBoxKeyPressEvent;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], dmAgent->YesNoItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    case 9: //長文本下拉框
      //輸入標簽控件
      TDialOutQryLabelList[j] = new TLabel(this);
      TDialOutQryLabelList[j]->Parent = sbDialOutSearch;
      TDialOutQryLabelList[j]->Caption = m_pSelDialOutItemSetList->pDialOutItemSet[i].TLabelCaption+"：";
      TDialOutQryLabelList[j]->Left = nEditLeft-TDialOutQryLabelList[j]->Width-2;
      TDialOutQryLabelList[j]->Top = nEditTop+4;
      TDialOutQryLabelList[j]->Show();
      //輸入框控件
      TDialOutQryInputList[j] = new TComboBox(this);
      ((TComboBox *)TDialOutQryInputList[j])->Parent = sbDialOutSearch;
      ((TComboBox *)TDialOutQryInputList[j])->Text = "不選擇";
      ((TComboBox *)TDialOutQryInputList[j])->Left = nEditLeft;
      ((TComboBox *)TDialOutQryInputList[j])->Top = nEditTop;
      ((TComboBox *)TDialOutQryInputList[j])->Width = GetControlWith(nShortEditWith, m_pSelDialOutItemSetList->pDialOutItemSet[i].TWith);
      ((TComboBox *)TDialOutQryInputList[j])->MaxLength = 50;
      //((TComboBox *)TDialOutQryInputList[j])->OnKeyDown = FormKeyDown;
      dmAgent->AddCmbItem((TComboBox *)TDialOutQryInputList[j], m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList, "不選擇");
      ((TComboBox *)TDialOutQryInputList[j])->Show();
      DialOutQryItemId[j] = i;
      DialOutQrySql[j] = "ItemData"+IntToStr(i+1)+"=";
      j++;
      break;
    }
    if (j >= MAX_DIALOUT_CONTROL_NUM)
      break;
    if (nEditLeft == 850)
    {
      nEditLeft = 100;
      nEditTop = nEditTop+25;
    }
    else
    {
      nEditLeft = nEditLeft+250;
    }
  }
  if (j > DialOutQryItemNum)
    Label2->Visible = true;
  else
    Label2->Visible = false;

  DialOutQryItemNum = j;
}

void __fastcall TFormSearchDialoutCust::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormSearchDialoutCust::Button1Click(TObject *Sender)
{
  DIAL1WhereSql = "";
  if (editDialOutTeleNo->Text != "")
  {
    DIAL1WhereSql = "(CalledNo like '" + editDialOutTeleNo->Text + "%' or TeleNo like '"+editDialOutTeleNo->Text + "%')";
  }
  if (editDialOutCustName->Text != "")
  {
    if (DIAL1WhereSql == "")
      DIAL1WhereSql = "CustName like '%" + editDialOutCustName->Text + "%'";
    else
      DIAL1WhereSql += " and CustName like '%" + editDialOutCustName->Text + "%'";
  }
  if (editDialOutWorkerNo->Text != "")
  {
    if (DIAL1WhereSql == "")
      DIAL1WhereSql = "TaskWorkerNo='" + editDialOutWorkerNo->Text + "'";
    else
      DIAL1WhereSql += " and TaskWorkerNo='" + editDialOutWorkerNo->Text + "'";
  }
  if (editDialOutAccountNo->Text != "")
  {
    if (DIAL1WhereSql == "")
      DIAL1WhereSql = "AccountNo='" + editDialOutAccountNo->Text + "'";
    else
      DIAL1WhereSql += " and AccountNo='" + editDialOutAccountNo->Text + "'";
  }

  for (int i=0; i<MAX_DIALOUT_CONTROL_NUM; i++)
  {
    if (TDialOutQryInputList[i] == NULL)
      continue;
    switch (m_pSelDialOutItemSetList->pDialOutItemSet[DialOutQryItemId[i]].TControlType)
    {
    case 1: //文本下拉框
      if (((TComboBox *)TDialOutQryInputList[i])->Text != "不選擇")
      {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'"+((TComboBox *)TDialOutQryInputList[i])->Text+"'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+((TComboBox *)TDialOutQryInputList[i])->Text+"'";
      }
      break;
    case 2: //數值下拉框
      if (((TComboBox *)TDialOutQryInputList[i])->Text != "不選擇")
      {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'"+IntToStr(m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList.GetItemValue(((TComboBox *)TDialOutQryInputList[i])->Text))+"'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+IntToStr(m_pSelDialOutItemSetList->pDialOutItemSet[i].TLookupItemList.GetItemValue(((TComboBox *)TDialOutQryInputList[i])->Text))+"'";
      }
      break;
    case 3: //長文本框
      if (((TEdit *)TDialOutQryInputList[i])->Text != "")
      {
        if (m_pSelDialOutItemSetList->pDialOutItemSet[DialOutQryItemId[i]].SearchId == 1)
        {
          if (DIAL1WhereSql == "")
            DIAL1WhereSql = DialOutQrySql[i]+"'%"+((TEdit *)TDialOutQryInputList[i])->Text+"%'";
          else
            DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'%"+((TEdit *)TDialOutQryInputList[i])->Text+"%'";
        }
        else if (m_pSelDialOutItemSetList->pDialOutItemSet[DialOutQryItemId[i]].SearchId == 2)
        {
          if (DIAL1WhereSql == "")
            DIAL1WhereSql = DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
          else
            DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
        }
      }
      break;
    case 4: //短文本框
      if (((TEdit *)TDialOutQryInputList[i])->Text != "")
      {
        if (m_pSelDialOutItemSetList->pDialOutItemSet[DialOutQryItemId[i]].SearchId == 1)
        {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'%"+((TEdit *)TDialOutQryInputList[i])->Text+"%'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'%"+((TEdit *)TDialOutQryInputList[i])->Text+"%'";
        }
        else if (m_pSelDialOutItemSetList->pDialOutItemSet[DialOutQryItemId[i]].SearchId == 2)
        {
          if (DIAL1WhereSql == "")
            DIAL1WhereSql = DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
          else
            DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
        }
      }
      break;
    case 5: //數值框
      if (((TEdit *)TDialOutQryInputList[i])->Text != "")
      {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
      }
      break;
    case 6: //金額框
      if (((TEdit *)TDialOutQryInputList[i])->Text != "")
      {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+((TEdit *)TDialOutQryInputList[i])->Text+"'";
      }
      break;
    case 7: //日期框
      if (DIAL1WhereSql == "")
        DIAL1WhereSql = DialOutQrySql[i]+"'"+((TDateTimePicker *)TDialOutQryInputList[i])->Date.FormatString("yyyy-mm-dd")+"'";
      else
        DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'"+((TDateTimePicker *)TDialOutQryInputList[i])->Date.FormatString("yyyy-mm-dd")+"'";
      break;
    case 8: //是非框
      if (((TComboBox *)TDialOutQryInputList[i])->Text == "是")
      {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'1'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'1'";
      }
      else if (((TComboBox *)TDialOutQryInputList[i])->Text == "否")
      {
        if (DIAL1WhereSql == "")
          DIAL1WhereSql = DialOutQrySql[i]+"'0'";
        else
          DIAL1WhereSql = DIAL1WhereSql+" and "+DialOutQrySql[i]+"'0'";
      }
      break;
    }
  }

  FormMain->QueryDialOutProcData();
  this->Close();
}
//---------------------------------------------------------------------------

