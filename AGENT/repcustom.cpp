//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "repcustom.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormRepCustomList *FormRepCustomList;
//---------------------------------------------------------------------------
__fastcall TFormRepCustomList::TFormRepCustomList(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormRepCustomList::ADOQuery1CalcFields(TDataSet *DataSet)
{
  ADOQuery1CustTypeName->AsString = dmAgent->CustTypeItemList.GetItemName(ADOQuery1CustType->AsInteger);
  ADOQuery1DispMobileNo->AsString = GetHidePhoneNum(2, ADOQuery1MobileNo->AsString);
  ADOQuery1DispTeleNo->AsString = GetHidePhoneNum(2, ADOQuery1TeleNo->AsString);
  ADOQuery1DispFaxNo->AsString = GetHidePhoneNum(2, ADOQuery1FaxNo->AsString);
}
//---------------------------------------------------------------------------

