object FormEditDialOutItemSet: TFormEditDialOutItemSet
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20154#24037#22806#21628#26989#21209#23450#21046#38917#30446#35373#23450
  ClientHeight = 336
  ClientWidth = 448
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 12
    Top = 20
    Width = 72
    Height = 12
    Caption = #21463#29702#26694#24207#34399#65306
  end
  object Label2: TLabel
    Left = 12
    Top = 44
    Width = 72
    Height = 12
    Caption = #21463#29702#26694#39006#22411#65306
  end
  object Label3: TLabel
    Left = 228
    Top = 20
    Width = 72
    Height = 12
    Caption = #21463#29702#38917#21517#31281#65306
  end
  object Label4: TLabel
    Left = 36
    Top = 68
    Width = 48
    Height = 12
    Caption = #38928#35373#20540#65306
  end
  object Label5: TLabel
    Left = 228
    Top = 93
    Width = 48
    Height = 12
    Caption = #26368#22823#20540#65306
  end
  object Label6: TLabel
    Left = 12
    Top = 93
    Width = 48
    Height = 12
    Caption = #26368#23567#20540#65306
  end
  object Label7: TLabel
    Left = 240
    Top = 236
    Width = 60
    Height = 12
    Caption = #36664#20837#35373#23450#65306
  end
  object Label8: TLabel
    Left = 23
    Top = 121
    Width = 60
    Height = 12
    Caption = #35215#23450#26684#24335#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 23
    Top = 145
    Width = 60
    Height = 12
    Caption = #24171#21161#25552#31034#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 239
    Top = 262
    Width = 60
    Height = 12
    Caption = #20803#20214#23532#24230#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 12
    Top = 172
    Width = 60
    Height = 12
    Caption = #26597#35426#26781#20214#65306
  end
  object Label11: TLabel
    Left = 228
    Top = 173
    Width = 60
    Height = 12
    Caption = #26781#20214#38918#24207#65306
  end
  object Label12: TLabel
    Left = 228
    Top = 200
    Width = 60
    Height = 12
    Caption = #23637#31034#38918#24207#65306
  end
  object editTid: TEdit
    Left = 88
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editTidChange
  end
  object cbControlType: TComboBox
    Left = 88
    Top = 40
    Width = 121
    Height = 20
    ItemHeight = 12
    TabOrder = 1
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
  end
  object editLabelName: TEdit
    Left = 304
    Top = 16
    Width = 121
    Height = 20
    MaxLength = 20
    TabOrder = 2
    OnChange = editTidChange
  end
  object editDefaultData: TEdit
    Left = 88
    Top = 64
    Width = 337
    Height = 20
    MaxLength = 100
    TabOrder = 3
    OnChange = editTidChange
  end
  object cseTMinValue: TCSpinEdit
    Left = 88
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 4
    OnChange = editTidChange
  end
  object cseTMaxValue: TCSpinEdit
    Left = 304
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 5
    OnChange = editTidChange
  end
  object Button1: TButton
    Left = 96
    Top = 293
    Width = 75
    Height = 25
    Caption = #30906#23450
    Default = True
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 280
    Top = 293
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 7
    OnClick = Button2Click
  end
  object ckIsAllowNull: TCheckBox
    Left = 304
    Top = 40
    Width = 121
    Height = 17
    Caption = #26159#21542#20801#35377#28858#31354#65311
    TabOrder = 8
    OnClick = editTidChange
  end
  object ckExportId: TCheckBox
    Left = 88
    Top = 232
    Width = 121
    Height = 17
    Caption = #26159#21542#20801#35377#21295#20986#65311
    Checked = True
    State = cbChecked
    TabOrder = 9
    OnClick = editTidChange
  end
  object ckDispId: TCheckBox
    Left = 88
    Top = 256
    Width = 113
    Height = 17
    Caption = #26159#21542#20801#35377#39023#31034#65311
    Checked = True
    State = cbChecked
    TabOrder = 10
    OnClick = editTidChange
  end
  object cbModifyId: TComboBox
    Left = 304
    Top = 232
    Width = 121
    Height = 20
    ItemHeight = 12
    TabOrder = 11
    Text = #21463#29702#20171#38754#36664#20837
    OnChange = editTidChange
    OnKeyPress = cbControlTypeKeyPress
    Items.Strings = (
      #21463#29702#20171#38754#19981#39023#31034
      #21463#29702#20171#38754#39023#31034
      #21463#29702#20171#38754#36664#20837)
  end
  object editMaskEdit: TEdit
    Left = 87
    Top = 116
    Width = 338
    Height = 20
    TabOrder = 12
    OnChange = editTidChange
  end
  object editHintStr: TEdit
    Left = 87
    Top = 141
    Width = 339
    Height = 20
    MaxLength = 100
    TabOrder = 13
    OnChange = editTidChange
  end
  object Memo1: TMemo
    Left = 2
    Top = 344
    Width = 442
    Height = 305
    Lines.Strings = (
      #35215#23450#26684#24335#65306#32232#30908#26684#24335#21487#20197#20998#28858#19977#37096#20998#27599#20491#37096#20998#20043#38291#29992#20998#34399#8220';'#8221#20998#38283': A;B;C'
      ''
      'A-'#37096#20998':'#22312#36889#19968#37096#20998#29992#19968#20123#29305#27530#30340#26684#24335#31526#20358#34920#31034#25033#36664#20837#30340#23383#31526#39006#22411#21450#26684#24335#65292#24120#29992
      #30340#29305#27530#26684#24335#31526#26377#65306
      '    !   '#21435#25481#36664#20837#25976#25818#38283#38957#30340#31354#26684#31526
      '    >   '#35731#36664#20837#25976#25818#30340#23383#27597#37117#35722#25104#22823#23531#65292#30452#21040#36935#19978#25513#30908#23383#31526'<'
      '    <   '#35731#36664#20837#25976#25818#30340#23383#27597#37117#35722#25104#23567#23531#65292#30452#21040#36935#19978#25513#30908#23383#31526'>'
      '    <>  '#19981#38480#21046#36664#20837#25976#25818#23383#27597#26159#22823#23531#25110#23567#23531
      '    \   '#33509#22312#36664#20837#26684#24335#20839#21152#20837#26576#20491#29305#27530#23383#31526#65292#21482#35201#22312#29305#27530#23383#31526#30340#21069#38754#21152#19978#27492#25513#30908#23601
      #21487#20197#12290#65288#20854#23526#19981#21152#20063#21487#20197#65289
      '    L   '#20801#35377#36664#20837#33521#25991#23383#27597#65292#32780#19988#19968#23450#35201#36664#20837
      '    l   '#20801#35377#36664#20837#33521#25991#23383#27597#65292#19981#19968#23450#35201#36664#20837
      '    A   '#20801#35377#36664#20837#33521#25991#23383#27597#21644#38463#25289#20271#25976#23383#65292#19968#23450#35201#36664#20837
      '    a   '#20801#35377#36664#20837#33521#25991#23383#27597#21644#38463#25289#20271#25976#23383#65292#19981#19968#23450#35201#36664#20837
      '    C   '#20801#35377#36664#20837#20219#20309#23383#31526#65292#19968#23450#35201#36664#20837
      '    c   '#20801#35377#36664#20837#20219#20309#23383#31526#65292#19981#19968#23450#35201#36664#20837
      '    0   '#20801#35377#36664#20837#38463#25289#20271#25976#23383#65292#19968#23450#35201#36664#20837
      '    9   '#20801#35377#36664#20837#38463#25289#20271#25976#23383#65292#19981#19968#23450#35201#36664#20837
      '    #   '#20801#35377#36664#20837#38463#25289#20271#25976#23383#25110#27491#12289#36000#34399#23383#31526
      '    :  '#65288#20882#34399#65289#29992#20358#20998#38548#26178#38291#25976#25818#20013#30340#26178#12289#20998#12289#31186
      '    /   '#29992#20358#20998#38548#26085#26399#25976#25818#20013#30340#24180#12289#26376#12289#26085
      ''
      'B-'#37096#20998':'#21482#26377'0'#21644'1'#20841#31278#36984#25799#12290#22914#28858'1'#65292#21063#25513#30908#20013#30340#38750#29992#25142#36664#20837#25976#25818#21644#27161#28310#20998#38548#31526
      #31561#20854#23427#21508#31278#23383#31526#26371#20316#28858#25976#25818#30340
      #19968#37096#20998#20445#23384#65307#28858'0'#21063#19981#20445#23384#12290
      ''
      'C-'#37096#20998':'#29992#20110#34920#31034#25976#25818#20013#30340#31354#20301#29992#21738#20491#23383#31526#20195#26367#39023#31034#12290
      ''
      #22914': !AAAAA-AAAAAA;1;_  '#34920#31034#21069#38754#26159'5'#30908#33521#25991#23383#27597#21644#38463#25289#20271#25976#23383'('#24517#38656#35201#36664#20837'),'
      #20013#38291#29992#28187#34399#38548#38283','#21518#38754#26159'5'#30908#33521
      #25991#23383#27597#21644#38463#25289#20271#25976#23383'('#24517#38656#35201#36664#20837')')
    ScrollBars = ssVertical
    TabOrder = 14
  end
  object cbWith: TComboBox
    Left = 304
    Top = 256
    Width = 121
    Height = 20
    ItemHeight = 12
    TabOrder = 15
    Text = #27161#20934#23532#24230
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
    Items.Strings = (
      #27161#20934#23532#24230
      #27161#20934#23532#24230#22235#20998#20043#19968
      #27161#20934#23532#24230#20108#20998#20043#19968
      #27161#20934#23532#24230#22235#20998#20043#19977)
  end
  object cbSearchId: TComboBox
    Left = 88
    Top = 168
    Width = 121
    Height = 20
    ItemHeight = 12
    TabOrder = 16
    Text = #28961
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
    Items.Strings = (
      #28961
      #27169#31946#26597#35426
      #31934#28310#26597#35426)
  end
  object cseSearchOrder: TCSpinEdit
    Left = 304
    Top = 168
    Width = 121
    Height = 21
    MaxValue = 32
    TabOrder = 17
    OnChange = editTidChange
  end
  object cseDispOrder: TCSpinEdit
    Left = 304
    Top = 195
    Width = 121
    Height = 21
    MaxValue = 32
    TabOrder = 18
    OnChange = editTidChange
  end
end
