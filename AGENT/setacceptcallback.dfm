object FormSetAcceptCallBack: TFormSetAcceptCallBack
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #35373#23450#26989#21209#21463#29702#38928#32004#22238#25765#26178#38291
  ClientHeight = 268
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 95
    Top = 180
    Width = 12
    Height = 13
    Caption = #40670
  end
  object Label2: TLabel
    Left = 159
    Top = 180
    Width = 12
    Height = 13
    Caption = #20998
  end
  object MonthCalendar1: TMonthCalendar
    Left = 48
    Top = 16
    Width = 267
    Height = 153
    Date = 42018.9283474537
    TabOrder = 0
  end
  object CSpinEdit1: TCSpinEdit
    Left = 48
    Top = 176
    Width = 45
    Height = 22
    MaxValue = 23
    TabOrder = 1
  end
  object CSpinEdit2: TCSpinEdit
    Left = 112
    Top = 176
    Width = 45
    Height = 22
    MaxValue = 59
    TabOrder = 2
  end
  object Button1: TButton
    Left = 176
    Top = 174
    Width = 65
    Height = 25
    Caption = #30906#23450#38928#32004
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 248
    Top = 174
    Width = 65
    Height = 25
    Caption = #21462#28040#38928#32004
    TabOrder = 4
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 128
    Top = 216
    Width = 75
    Height = 25
    Caption = #36864#20986
    TabOrder = 5
    OnClick = Button3Click
  end
end
