//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dispproduct.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormPrdDetail *FormPrdDetail;
//---------------------------------------------------------------------------
__fastcall TFormPrdDetail::TFormPrdDetail(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormPrdDetail::SetProductData(CProducts &products)
{
  editProductNo->Text = products.ProductNo;
  editName->Text = products.ProductName;
  editProductType->Text = products.ProductType;
  editBrand->Text = products.Brand;
  editSupplierName->Text = products.SupplierName;
  editSupplierMem->Text = products.SupplierMem;
  editSupplierTel->Text = products.SupplierTel;
  editSupplierAddr->Text = products.SupplierAddr;
  editShortDesc->Text = products.ShortDesc;
  memoDescription->Lines->Clear();
  memoDescription->Lines->Add(products.Description);
  editInFocusImageUrl->Text = products.ProductImg;
}
void __fastcall TFormPrdDetail::Button1Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormPrdDetail::Button2Click(TObject *Sender)
{
  if (editInFocusImageUrl->Text.Length() > 0)
    ShellExecute(Handle, "open", editInFocusImageUrl->Text.c_str(), NULL, NULL, SW_SHOWDEFAULT);
}
//---------------------------------------------------------------------------

