//---------------------------------------------------------------------------

#ifndef dispcustomerH
#define dispcustomerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include "public.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormDispCustom : public TForm
{
__published:	// IDE-managed Components
  TButton *Button1;
  TGroupBox *gbCustBase;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label1;
  TLabel *Label14;
  TLabel *Label15;
  TLabel *Label16;
  TLabel *Label13;
  TLabel *Label17;
  TLabel *Label18;
  TEdit *edtCustomNo;
  TEdit *edtCustName;
  TEdit *edtCorpName;
  TEdit *edtMobileNo;
  TEdit *edtTeleNo;
  TEdit *edtFaxNo;
  TEdit *edtEMail;
  TEdit *edtContAddr;
  TEdit *edtPostNo;
  TEdit *edtRemark;
  TComboBox *edtCustSex;
  TComboBox *edtCustType;
  TComboBox *cbProvince;
  TComboBox *cbCityName;
  TComboBox *cbCountyName;
  TComboBox *cbCustClass;
  TEdit *edtZoneName;
  TComboBox *edtCustLevel;
  TGroupBox *gbCustEx;
  TScrollBox *sbCustEx;
  TLabel *Label19;
  TEdit *edtSubPhone;
  TLabel *lbAccountNo;
  TEdit *edtAccountNo;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDispCustom(TComponent* Owner);

  //客户扩展资料输入控件
  TLabel *TCustExLabelList[MAX_CUST_CONTROL_NUM];
  TControl *TCustExInputList[MAX_CUST_CONTROL_NUM];

  void CreateCustExControl();
  void SetCustom(CCustomer &customer);
  void SetCustEx(CCustEx &custex);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDispCustom *FormDispCustom;
//---------------------------------------------------------------------------
#endif
