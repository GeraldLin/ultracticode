//---------------------------------------------------------------------------

#ifndef editorderexpressH
#define editorderexpressH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditOrderExpress : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label166;
  TLabel *Label209;
  TLabel *Label210;
  TLabel *Label211;
  TLabel *Label212;
  TLabel *Label213;
  TLabel *Label214;
  TLabel *Label215;
  TLabel *Label218;
  TComboBox *cbOExpressType;
  TEdit *edtOExpressNo;
  TEdit *edtOExpressCorp;
  TEdit *edtOExpressTele;
  TEdit *edtORecvName;
  TEdit *edtORecvAddr;
  TEdit *edtORecvPost;
  TEdit *edtORecvTele;
  TEdit *edtORemark;
  TLabel *Label8;
  TEdit *edtOOrderId;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label11;
  TComboBox *cbOOrderState;
  void __fastcall cbOExpressTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall cbOExpressTypeChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditOrderExpress(TComponent* Owner);

  COrders Orders;
  void SetOrderExpData(COrders& orders);
  int GetOrderExpData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditOrderExpress *FormEditOrderExpress;
//---------------------------------------------------------------------------
#endif
