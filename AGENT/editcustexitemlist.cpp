//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editcustexitemlist.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditCustExItemList *FormEditCustExItemList;
//---------------------------------------------------------------------------
__fastcall TFormEditCustExItemList::TFormEditCustExItemList(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditCustExItemList::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "新增";
    editItemNo->ReadOnly = false;
    editItemNo->Color = clWindow;
    FormEditCustExItemList->Caption = "新增來電者資料擴展項目選擇項資料";
  }
  else
  {
    Button1->Caption = "修改";
    editItemNo->ReadOnly = true;
    editItemNo->Color = clSilver;
    FormEditCustExItemList->Caption = "修改來電者資料擴展項目選擇項資料";
  }
}
void TFormEditCustExItemList::SetItemData(CCustExItemSet& custexitemset, AnsiString strItemNo, AnsiString strItemName)
{
  CustExItemSet = custexitemset;
  editLabelCaption->Text = CustExItemSet.CLabelCaption;
  editItemNo->Text = strItemNo;
  editItemName->Text = strItemName;
  Button1->Enabled = false;
}

void __fastcall TFormEditCustExItemList::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExItemList::editItemNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExItemList::Button1Click(TObject *Sender)
{
  int nItemNo;

  if (MyIsNumber(editItemNo->Text) != 0)
  {
    MessageBox(NULL,"請輸入下拉框選擇項編號！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editItemName->Text == "")
  {
    MessageBox(NULL,"請輸入下拉框選擇項名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  nItemNo = StrToInt(editItemNo->Text);
  if (CustExItemSet.CMinValue != 0 && CustExItemSet.CMaxValue != 0 && (nItemNo < CustExItemSet.CMinValue || nItemNo > CustExItemSet.CMaxValue))
  {
    MessageBox(NULL,"對不起，您輸入的下拉框選擇項編號超出允許範圍！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }

  if (InsertId == true)
  {
    if (dmAgent->IsCustExItemListExist(CustExItemSet.CFieldId, nItemNo) == 1)
    {
      MessageBox(NULL,"對不起，該下拉框選擇項編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertCustExItemListRecord(CustExItemSet.CFieldId, nItemNo, editItemName->Text) == 0)
    {
      dmAgent->adoQryCustExFieldList->Close();
      dmAgent->adoQryCustExFieldList->Open();
      this->Close();
      //MessageBox(NULL,"新增下拉框選擇項編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"新增下拉框選擇項編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateCustExItemListRecord(CustExItemSet.CFieldId, nItemNo, editItemName->Text) == 0)
    {
      dmAgent->adoQryCustExFieldList->Close();
      dmAgent->adoQryCustExFieldList->Open();
      this->Close();
      //MessageBox(NULL,"修改下拉框選擇項編號成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
    }
    else
    {
      MessageBox(NULL,"修改下拉框選擇項編號失敗！","訊息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditCustExItemList::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormEditCustExItemList->editItemNo->SetFocus();
  else
    FormEditCustExItemList->editItemName->SetFocus();
}
//---------------------------------------------------------------------------

