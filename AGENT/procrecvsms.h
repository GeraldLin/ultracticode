//---------------------------------------------------------------------------

#ifndef procrecvsmsH
#define procrecvsmsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormRecvSmsProc : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TEdit *editMobile;
  TEdit *editRecvTime;
  TMemo *memoSmsMsg;
  TEdit *editReadTime;
  TEdit *editRemark;
  TMemo *memoProcSrvData;
  TButton *Button1;
  TButton *Button2;
  TComboBox *cbProcSrvType;
  TComboBox *cbReadFlag;
  TLabel *Label9;
  TEdit *editProcBy;
  void __fastcall cbReadFlagKeyPress(TObject *Sender, char &Key);
  void __fastcall cbReadFlagChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormRecvSmsProc(TComponent* Owner);

  CRecvSms RecvSms;
  void SetRecvSmsData(CRecvSms& recvsms);
  int GetRecvSmsData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRecvSmsProc *FormRecvSmsProc;
//---------------------------------------------------------------------------
#endif
