//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "edittabauthset.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormTabAuthSet *FormTabAuthSet;

extern int g_nDeptId; //2019-08-04 增加按部?查?限

//---------------------------------------------------------------------------
__fastcall TFormTabAuthSet::TFormTabAuthSet(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormTabAuthSet::SetTabAuth(CTabAuth &tabauth)
{
  curTabAuth = tabauth;
  editTabName->Text = curTabAuth.TabName;
  ckAdmin->Checked = curTabAuth.AdminAuth;
  ckMaint->Checked = curTabAuth.MaintAuth;
  ckMonit->Checked = curTabAuth.MonitAuth;
  ckOpert->Checked = curTabAuth.OpertAuth;
  ckAgent->Checked = curTabAuth.AgentAuth;
  Button1->Enabled = false;
}

void __fastcall TFormTabAuthSet::ckAdminClick(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormTabAuthSet::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormTabAuthSet::Button1Click(TObject *Sender)
{
  char sqlbuf[256];

  //2019-08-04
  if (g_nDeptId <= 0)
    sprintf(sqlbuf, "update tbtabauth set AdminAuth=%d,MaintAuth=%d,OpertAuth=%d,MonitAuth=%d,AgentAuth=%d where MainTabID=%d and SubTabID=%d",
    ckAdmin->Checked, ckMaint->Checked, ckOpert->Checked, ckMonit->Checked, ckAgent->Checked,
    curTabAuth.MainTabID, curTabAuth.SubTabID);
  else
    sprintf(sqlbuf, "update tbtabauth_Dept%d set AdminAuth=%d,MaintAuth=%d,OpertAuth=%d,MonitAuth=%d,AgentAuth=%d where MainTabID=%d and SubTabID=%d",
    g_nDeptId, ckAdmin->Checked, ckMaint->Checked, ckOpert->Checked, ckMonit->Checked, ckAgent->Checked,
    curTabAuth.MainTabID, curTabAuth.SubTabID);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->ExecSQL();
    dmAgent->adoQryTabAuthSet->Close();
    dmAgent->adoQryTabAuthSet->Open();
    dmAgent->QryTabPageAuth();
    this->Close();
    //MessageBox(NULL,"介面操作權限修改成功！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
  catch ( ... )
  {
    MessageBox(NULL,"介面操作權限修改失敗！","訊息提示",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------

