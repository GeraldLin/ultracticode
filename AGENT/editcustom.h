//---------------------------------------------------------------------------

#ifndef editcustomH
#define editcustomH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "public.h"
#include <ComCtrls.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditCustom : public TForm
{
__published:	// IDE-managed Components
  TBitBtn *BitBtn1;
  TBitBtn *BitBtn2;
  TGroupBox *gbCustBase;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label1;
  TLabel *Label14;
  TLabel *Label15;
  TLabel *Label16;
  TLabel *Label13;
  TLabel *Label17;
  TLabel *Label18;
  TEdit *edtCustomNo;
  TEdit *edtCustName;
  TEdit *edtCorpName;
  TEdit *edtMobileNo;
  TEdit *edtTeleNo;
  TEdit *edtFaxNo;
  TEdit *edtEMail;
  TEdit *edtContAddr;
  TEdit *edtPostNo;
  TEdit *edtRemark;
  TComboBox *edtCustSex;
  TComboBox *edtCustType;
  TComboBox *cbProvince;
  TComboBox *cbCityName;
  TComboBox *cbCountyName;
  TComboBox *cbCustClass;
  TEdit *edtZoneName;
  TComboBox *edtCustLevel;
  TGroupBox *gbCustEx;
  TScrollBox *sbCustEx;
  TCheckBox *ckSameCustName;
  TLabel *Label19;
  TEdit *edtSubPhone;
  TLabel *lbAccountNo;
  TEdit *edtAccountNo;
  TButton *btQryAS400;
  void __fastcall BitBtn1Click(TObject *Sender);
  void __fastcall BitBtn2Click(TObject *Sender);
  void __fastcall edtCustNameChange(TObject *Sender);
  void __fastcall edtCustTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall cbProvinceExit(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbCityNameExit(TObject *Sender);
  void __fastcall FormDestroy(TObject *Sender);
  void __fastcall edtMobileNoChange(TObject *Sender);
  void __fastcall edtTeleNoChange(TObject *Sender);
  void __fastcall edtFaxNoChange(TObject *Sender);
  void __fastcall btQryAS400Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditCustom(TComponent* Owner);

  void __fastcall MyOnChangeEvent(TObject *Sender);
  //客户扩展资料输入控件
  TLabel *TCustExLabelList[MAX_CUST_CONTROL_NUM];
  TControl *TCustExInputList[MAX_CUST_CONTROL_NUM];

  CCustomer Customer;
  CCustEx CustEx;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  bool CustExChangeId;
  void SetModifyType(bool isinsert);
  void SetCustom(CCustomer &customer);
  void SetCustEx(CCustEx &custex);
  int GetCustom();
  void ClearCustom();
  void ClearCusExData();

  void CreateCustExControl();
  int GetCustExData(CCustEx &custx, bool bPromptError=true);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditCustom *FormEditCustom;
//---------------------------------------------------------------------------
#endif
