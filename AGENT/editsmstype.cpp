//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editsmstype.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditSmsType *FormEditSmsType;
//---------------------------------------------------------------------------
__fastcall TFormEditSmsType::TFormEditSmsType(TComponent* Owner)
  : TForm(Owner)
{
  SmsType = 0;
}
//---------------------------------------------------------------------------
void TFormEditSmsType::SetSmsTypeData(int nSmsType, bool bInsertId, AnsiString strSmsTypeId, AnsiString strSmsTypeName)
{
  SmsType = nSmsType;
  if (nSmsType == 1)
  {
    FormEditSmsType->Caption = "設置接收短信業務類型";
    Label1->Caption = "接收短信業務類型編號";
    Label2->Caption = "接收短信業務類型名稱";
  }
  else
  {
    FormEditSmsType->Caption = "設置發送短信業務類型";
    Label1->Caption = "發送短信業務類型編號";
    Label2->Caption = "發送短信業務類型名稱";
  }
  InsertId = bInsertId;
  if (bInsertId == true)
  {
    editSmsTypeId->Text = "";
    editSmsTypeId->Color = clWindow;
    editSmsTypeId->ReadOnly = false;
    editSmsTypeName->Text = "";
  }
  else
  {
    editSmsTypeId->Text = strSmsTypeId;
    editSmsTypeId->Color = clGrayText;
    editSmsTypeId->ReadOnly = true;
    editSmsTypeName->Text = strSmsTypeName;
  }
  Button1->Enabled = false;
}
void __fastcall TFormEditSmsType::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditSmsType::editSmsTypeIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditSmsType::Button1Click(TObject *Sender)
{
  int nSmsTypeId;

  if (SmsType == 1)
  {
    if (MyIsNumber(editSmsTypeId->Text) != 0)
    {
      MessageBox(NULL,"請輸入接收短信業務類型編號！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (editSmsTypeName->Text == "")
    {
      MessageBox(NULL,"請輸入接收短信業務類型名稱！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }

    nSmsTypeId = StrToInt(editSmsTypeId->Text);

    if (InsertId == true)
    {
      if (dmAgent->IsRecvSmsTypeExist(nSmsTypeId) == 1)
      {
        MessageBox(NULL,"對不起，該接收短信業務類型編號已存在！","信息提示",MB_OK|MB_ICONERROR);
        return;
      }
      if (dmAgent->InsertRecvSmsTypeRecord(nSmsTypeId, editSmsTypeName->Text) == 0)
      {
        dmAgent->adoQryRecvSmsType->Close();
        dmAgent->adoQryRecvSmsType->Open();
        dmAgent->UpdateRecvSmsTypeListItems();
        MessageBox(NULL,"增加接收短信業務類型名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
        this->Close();
      }
      else
      {
        MessageBox(NULL,"增加接收短信業務類型名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
      }
    }
    else
    {
      if (dmAgent->UpdateRecvSmsTypeRecord(nSmsTypeId, editSmsTypeName->Text) == 0)
      {
        dmAgent->adoQryRecvSmsType->Close();
        dmAgent->adoQryRecvSmsType->Open();
        dmAgent->UpdateRecvSmsTypeListItems();
        MessageBox(NULL,"修改接收短信業務類型名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
        this->Close();
      }
      else
      {
        MessageBox(NULL,"修改接收短信業務類型名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
      }
    }
  }
  else
  {
    if (MyIsNumber(editSmsTypeId->Text) != 0)
    {
      MessageBox(NULL,"請輸入發送短信業務類型編號！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (editSmsTypeName->Text == "")
    {
      MessageBox(NULL,"請輸入發送短信業務類型名稱！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }

    nSmsTypeId = StrToInt(editSmsTypeId->Text);

    if (InsertId == true)
    {
      if (dmAgent->IsSendSmsTypeExist(nSmsTypeId) == 1)
      {
        MessageBox(NULL,"對不起，該發送短信業務類型編號已存在！","信息提示",MB_OK|MB_ICONERROR);
        return;
      }
      if (dmAgent->InsertSendSmsTypeRecord(nSmsTypeId, editSmsTypeName->Text) == 0)
      {
        dmAgent->adoQrySendSMSType->Close();
        dmAgent->adoQrySendSMSType->Open();
        dmAgent->UpdateSendSmsTypeListItems();
        MessageBox(NULL,"增加發送短信業務類型名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
        this->Close();
      }
      else
      {
        MessageBox(NULL,"增加發送短信業務類型名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
      }
    }
    else
    {
      if (dmAgent->UpdateSendSmsTypeRecord(nSmsTypeId, editSmsTypeName->Text) == 0)
      {
        dmAgent->adoQrySendSMSType->Close();
        dmAgent->adoQrySendSMSType->Open();
        dmAgent->UpdateSendSmsTypeListItems();
        MessageBox(NULL,"修改發送短信業務類型名稱成功！","信息提示",MB_OK|MB_ICONINFORMATION);
        this->Close();
      }
      else
      {
        MessageBox(NULL,"修改發送短信業務類型名稱失敗！","信息提示",MB_OK|MB_ICONERROR);
      }
    }
  }
}
//---------------------------------------------------------------------------
