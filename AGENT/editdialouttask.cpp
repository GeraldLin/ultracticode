//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>
#include "editdialouttask.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditDialOutTask *FormEditDialOutTask;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;
extern g_nBandSeatNoType;
//---------------------------------------------------------------------------
__fastcall TFormEditDialOutTask::TFormEditDialOutTask(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditDialOutTask::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "增加";
    FormEditDialOutTask->Caption = "增加外撥任務";
  }
  else
  {
    Button1->Caption = "修改";
    FormEditDialOutTask->Caption = "修改外撥任務";
  }
}
void TFormEditDialOutTask::SetDialOutTaskData(CDialOutTask& dialouttask)
{
  DialOutTask = dialouttask;
  editTaskName->Text = DialOutTask.TaskName;
  if (DialOutTask.FuncNo == 0)
    cbFuncNo->Text = "系統預設流程";
  else
    cbFuncNo->Text = dmAgent->FlwFuncNoList.GetItemName(DialOutTask.FuncNo);

  editCallerNo->Text = DialOutTask.CallerNo;
  cbStartId->Text = dmAgent->YesNoItemList.GetItemName(DialOutTask.StartId);

  dtpStartDate->Date = StrToDateTime(DialOutTask.StartDate);
  dtpEndDate->Date = StrToDateTime(DialOutTask.EndDate);
  dtpStartTime->Time = StrToDateTime(DialOutTask.StartTime+":00");
  dtpEndTime->Time = StrToDateTime(DialOutTask.EndTime+":00");
  dtpStartTime2->Time = StrToDateTime(DialOutTask.StartTime2+":00");
  dtpEndTime2->Time = StrToDateTime(DialOutTask.EndTime2+":00");

  cseMaxCalledTimes->Value = DialOutTask.MaxCalledTimes;
  cseMaxAnsTimes->Value = DialOutTask.MaxAnsTimes;
  cseNoAnsDelay->Value = DialOutTask.NoAnsDelay;
  cseRouteNo->Value = DialOutTask.RouteNo;
  cseMaxChnNum->Value = DialOutTask.MaxChnNum;

  if (DialOutTask.GroupNo <= 0)
    cbGroupNo->Text = "不轉值機坐席";
  else
    cbGroupNo->Text = dmAgent->WorkerGroupList.GetItemName(DialOutTask.GroupNo);

  editFileName->Text = DialOutTask.FileName;
  if (DialOutTask.MixerType == 0)
    cbMixerType->Text = "不合成";
  else if (DialOutTask.MixerType == 1)
    cbMixerType->Text = "播報數字";
  else if (DialOutTask.MixerType == 2)
    cbMixerType->Text = "播報金額";
  else if (DialOutTask.MixerType == 3)
    cbMixerType->Text = "播報數值";
  else if (DialOutTask.MixerType == 4)
    cbMixerType->Text = "語音文件";
  else if (DialOutTask.MixerType == 5)
    cbMixerType->Text = "字符串TTS";
  else if (DialOutTask.MixerType == 6)
    cbMixerType->Text = "TXT文件TTS";

  editFileName2->Text = DialOutTask.FileName2;

  if (DialOutTask.TTSType == 0)
    cbTTSType->Text = "不合成";
  else if (DialOutTask.TTSType == 1)
    cbTTSType->Text = "字符串TTS";
  else if (DialOutTask.TTSType == 2)
    cbTTSType->Text = "TXT文件TTS";
  editTTSData->Text = DialOutTask.TTSData;

  if (DialOutTask.ConfirmType == 0)
    cbConfirmType->Text = "放音后挂斷";
  else if (DialOutTask.ConfirmType == 1)
    cbConfirmType->Text = "放音前詢問是否按1鍵收聽";
  else if (DialOutTask.ConfirmType == 2)
    cbConfirmType->Text = "放音后詢問是否按1鍵證實";
  else if (DialOutTask.ConfirmType == 3)
    cbConfirmType->Text = "業務調查按鍵";
  else if (DialOutTask.ConfirmType == 4)
    cbConfirmType->Text = "放音后詢問是否按1鍵轉客服";
  else if (DialOutTask.ConfirmType == 5)
    cbConfirmType->Text = "放音后詢問是否按1鍵重聽";

  editConfirmDTMF->Text = DialOutTask.ConfirmDTMF;
  editConfirmVoc->Text = DialOutTask.ConfirmVoc;

  switch (DialOutTask.ExportId)
  {
  case 0:
    cbExportId->Text = "不處理";
    break;
  case 1:
    cbExportId->Text = "自動刪除成功記錄";
    break;
  case 2:
    cbExportId->Text = "自動刪除失敗記錄";
    break;
  case 3:
    cbExportId->Text = "自動刪除成功及失敗記錄";
    break;
  case 4:
    cbExportId->Text = "自動將成功記錄匯入歷史表";
    break;
  case 5:
    cbExportId->Text = "自動將失敗記錄匯入歷史表";
    break;
  case 6:
    cbExportId->Text = "自動將成功及失敗記錄匯入歷史表";
    break;
  default:
    cbExportId->Text = "不處理";
    break;
  }
  switch (DialOutTask.CallType)
  {
  case 0:
    cbCallType->Text = "外撥后宣告語音";
    break;
  case 1:
    cbCallType->Text = "外撥后發送傳真";
    break;
  case 2:
    cbCallType->Text = "外撥成功后轉坐席";
    break;
  case 3:
    cbCallType->Text = "先撥座席然后外撥";
    break;
  case 4:
    cbCallType->Text = "外撥后即刻轉座席";
    break;
  default:
    cbCallType->Text = "外撥后宣告語音";
    break;
  }
  csePlayDelay->Value = DialOutTask.PlayDelay;
  cseListenTimeLen->Value = DialOutTask.ListenTimeLen;
  editTranVDN->Text = DialOutTask.TranVDN;
  editDialPreCode->Text = DialOutTask.DialPreCode;

  Button1->Enabled = false;
}
int TFormEditDialOutTask::GetDialOutTaskData()
{
  if (editTaskName->Text == "")
  {
    MessageBox(NULL,"請輸入外撥任務名稱！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  DialOutTask.TaskName = editTaskName->Text;
  if (editCallerNo->Text == "")
  {
    MessageBox(NULL,"請輸入主叫號碼！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (MyIsDigits(editCallerNo->Text) != 0)
  {
    MessageBox(NULL,"請輸入正確的主叫號碼！","信息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (cbFuncNo->Text == "系統預設流程")
    DialOutTask.FuncNo = 0;
  else
    DialOutTask.FuncNo = dmAgent->FlwFuncNoList.GetItemValue(cbFuncNo->Text);

  DialOutTask.CallerNo = editCallerNo->Text;
  DialOutTask.StartId = dmAgent->YesNoItemList.GetItemValue(cbStartId->Text);

  DialOutTask.StartDate = dtpStartDate->Date.FormatString("yyyy-mm-dd");
  DialOutTask.EndDate = dtpEndDate->Date.FormatString("yyyy-mm-dd");
  DialOutTask.StartTime = dtpStartTime->Date.FormatString("hh:nn");
  DialOutTask.EndTime = dtpEndTime->Date.FormatString("hh:nn");
  DialOutTask.StartTime2 = dtpStartTime2->Date.FormatString("hh:nn");
  DialOutTask.EndTime2 = dtpEndTime2->Date.FormatString("hh:nn");

  DialOutTask.MaxCalledTimes = cseMaxCalledTimes->Value;
  DialOutTask.MaxAnsTimes = cseMaxAnsTimes->Value;
  DialOutTask.NoAnsDelay = cseNoAnsDelay->Value;
  DialOutTask.BusyDelay = cseNoAnsDelay->Value;
  DialOutTask.RouteNo = cseRouteNo->Value;
  DialOutTask.MaxChnNum = cseMaxChnNum->Value;

  if (cbGroupNo->Text == "不轉值機坐席")
    DialOutTask.GroupNo = 0;
  else
    DialOutTask.GroupNo = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo->Text);
  DialOutTask.FileName = editFileName->Text;
  if (cbMixerType->Text == "不合成")
    DialOutTask.MixerType = 0;
  else if (cbMixerType->Text == "播報數字")
    DialOutTask.MixerType = 1;
  else if (cbMixerType->Text == "播報金額")
    DialOutTask.MixerType = 2;
  else if (cbMixerType->Text == "播報數值")
    DialOutTask.MixerType = 3;
  else if (cbMixerType->Text == "語音文件")
    DialOutTask.MixerType = 4;
  else if (cbMixerType->Text == "字符串TTS")
    DialOutTask.MixerType = 5;
  else if (cbMixerType->Text == "TXT文件TTS")
    DialOutTask.MixerType = 6;
  DialOutTask.FileName2 = editFileName2->Text;

  if (cbTTSType->Text == "不合成")
    DialOutTask.TTSType = 0;
  else if (cbTTSType->Text == "字符串TTS")
    DialOutTask.TTSType = 1;
  else if (cbTTSType->Text == "TXT文件TTS")
    DialOutTask.TTSType = 2;
  DialOutTask.TTSData = editTTSData->Text;

  if (cbConfirmType->Text == "放音后挂斷")
    DialOutTask.ConfirmType = 0;
  else if (cbConfirmType->Text == "放音前詢問是否按1鍵收聽")
    DialOutTask.ConfirmType = 1;
  else if (cbConfirmType->Text == "放音后詢問是否按1鍵證實")
    DialOutTask.ConfirmType = 2;
  else if (cbConfirmType->Text == "業務調查按鍵")
    DialOutTask.ConfirmType = 3;
  else if (cbConfirmType->Text == "放音后詢問是否按1鍵轉客服")
    DialOutTask.ConfirmType = 4;
  else if (cbConfirmType->Text == "放音后詢問是否按1鍵重聽")
    DialOutTask.ConfirmType = 5;

  DialOutTask.ConfirmDTMF = editConfirmDTMF->Text;
  DialOutTask.ConfirmVoc = editConfirmVoc->Text;

  if (cbExportId->Text == "不處理")
    DialOutTask.ExportId = 0;
  else if (cbExportId->Text == "自動刪除成功記錄")
    DialOutTask.ExportId = 1;
  else if (cbExportId->Text == "自動刪除失敗記錄")
    DialOutTask.ExportId = 2;
  else if (cbExportId->Text == "自動刪除成功及失敗記錄")
    DialOutTask.ExportId = 3;
  else if (cbExportId->Text == "自動將成功記錄匯入歷史表")
    DialOutTask.ExportId = 4;
  else if (cbExportId->Text == "自動將失敗記錄匯入歷史表")
    DialOutTask.ExportId = 5;
  else if (cbExportId->Text == "自動將成功及失敗記錄匯入歷史表")
    DialOutTask.ExportId = 6;
  else
    DialOutTask.ExportId = 0;

  if (cbCallType->Text == "外撥后宣告語音")
    DialOutTask.CallType = 0;
  else if (cbCallType->Text == "外撥后發送傳真")
    DialOutTask.CallType = 1;
  else if (cbCallType->Text == "外撥成功后轉坐席")
    DialOutTask.CallType = 2;
  else if (cbCallType->Text == "先撥坐席然后外撥")
    DialOutTask.CallType = 3;
  else if (cbCallType->Text == "外撥后即刻轉座席")
    DialOutTask.CallType = 4;
  else
    DialOutTask.CallType = 0;

  DialOutTask.PlayDelay = csePlayDelay->Value;
  DialOutTask.ListenTimeLen = cseListenTimeLen->Value;
  DialOutTask.TranVDN = editTranVDN->Text;
  DialOutTask.DialPreCode = editDialPreCode->Text;

  return 0;
}
void TFormEditDialOutTask::ClearDialOutTaskData()
{
  editTaskName->Text = "";
  cbFuncNo->Text = "系統預設流程";
  editCallerNo->Text = "";
  cbStartId->Text = "是";

  dtpStartDate->Date = Date();
  dtpEndDate->Date = IncDay(Date()+7);
  dtpStartTime->Time = StrToDateTime("08:30:00");
  dtpEndTime->Time = StrToDateTime("12:00:00");
  dtpStartTime2->Time = StrToDateTime("14:30:00");
  dtpEndTime2->Time = StrToDateTime("18:00:00");

  cseMaxCalledTimes->Value = 3;
  cseMaxAnsTimes->Value = 1;
  cseNoAnsDelay->Value = 300;
  cseRouteNo->Value = 1;
  cseMaxChnNum->Value = 5;

  cbGroupNo->Text = "不轉值機坐席";
  editFileName->Text = "";
  cbMixerType->Text = "不合成";
  editFileName2->Text = "";
  editDialPreCode->Text = "";

  Button1->Enabled = false;
}

void __fastcall TFormEditDialOutTask::cbStartIdKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutTask::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutTask::editTaskNameChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutTask::Button1Click(TObject *Sender)
{
  if (GetDialOutTaskData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertDialOutTaskRecord(&DialOutTask) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1030, "話務員增加名為：%s的外呼任務資料", DialOutTask.TaskName.c_str());
      MessageBox(NULL,"增加外撥任務成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryCalloutTask->Close();
      dmAgent->adoQryCalloutTask->Open();
      try
      {
        FormMain->QueryDialOutTele(dmAgent->adoQryCalloutTask->FieldByName("TaskId")->AsInteger);
      }
      catch (...)
      {
      }
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加外撥任務資料失敗!","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateDialOutTaskRecord(&DialOutTask) == 0)
    {
      //dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改名為：%s的外呼任務資料", DialOutTask.TaskName.c_str());
      MessageBox(NULL,"修改外撥任務資料成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryCalloutTask->Close();
      dmAgent->adoQryCalloutTask->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改外撥任務資料失敗!","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditDialOutTask::FormCreate(TObject *Sender)
{
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(cbGroupNo, dmAgent->WorkerGroupList, "播放提示語音");
    dmAgent->AddCmbItem(cbFuncNo, dmAgent->FlwFuncNoList, "系統預設流程");
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditDialOutTask::editTTSDataChange(TObject *Sender)
{
  DialOutTask.TTSUpdateId = 1;
  DialOutTask.TTSFileName = "";
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

