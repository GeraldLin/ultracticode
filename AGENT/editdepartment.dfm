object FormDepartment: TFormDepartment
  Left = 316
  Top = 211
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#37096#38376#21517#31216
  ClientHeight = 129
  ClientWidth = 237
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 60
    Height = 12
    Caption = #37096#38376#32534#21495#65306
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 60
    Height = 12
    Caption = #37096#38376#21517#31216#65306
  end
  object editDepartmentID: TEdit
    Left = 88
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editDepartmentIDChange
  end
  object editDepartmentName: TEdit
    Left = 88
    Top = 48
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editDepartmentIDChange
  end
  object Button1: TButton
    Left = 24
    Top = 88
    Width = 75
    Height = 25
    Caption = #30830#23450
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 128
    Top = 88
    Width = 75
    Height = 25
    Cancel = True
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
end
