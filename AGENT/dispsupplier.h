//---------------------------------------------------------------------------

#ifndef dispsupplierH
#define dispsupplierH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormDispSupplier : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TEdit *editSupplierId;
  TEdit *editName;
  TEdit *editTelPhone;
  TEdit *editCellPhone;
  TEdit *editAddress;
  TEdit *editContact;
  TMemo *memoRemark;
  TButton *Button1;
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDispSupplier(TComponent* Owner);

  void SetSupplierData(CShopSupplier &shopsupplier);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDispSupplier *FormDispSupplier;
//---------------------------------------------------------------------------
#endif
