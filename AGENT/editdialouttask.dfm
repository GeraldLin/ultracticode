object FormEditDialOutTask: TFormEditDialOutTask
  Left = 327
  Top = 155
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #22806#25765#20219#21209
  ClientHeight = 603
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 128
    Top = 11
    Width = 60
    Height = 12
    Caption = #20219#21209#21517#31281#65306
  end
  object Label2: TLabel
    Left = 104
    Top = 84
    Width = 84
    Height = 12
    Caption = #38283#22987#21628#21483#26085#26399#65306
  end
  object Label3: TLabel
    Left = 288
    Top = 84
    Width = 84
    Height = 12
    Caption = #32080#26463#21628#21483#26085#26399#65306
  end
  object Label4: TLabel
    Left = 80
    Top = 107
    Width = 108
    Height = 12
    Caption = #19978#21320#38283#22987#22806#25765#26178#38291#65306
  end
  object Label5: TLabel
    Left = 296
    Top = 108
    Width = 108
    Height = 12
    Caption = #19978#21320#32080#26463#22806#25765#26178#38291#65306
  end
  object Label6: TLabel
    Left = 319
    Top = 36
    Width = 84
    Height = 12
    Caption = #21855#21205#22806#25765#27161#24535#65306
  end
  object Label7: TLabel
    Left = 332
    Top = 181
    Width = 72
    Height = 12
    Caption = #22806#25765#36335#30001#34399#65306
  end
  object Label8: TLabel
    Left = 80
    Top = 60
    Width = 108
    Height = 12
    Caption = #22806#25765#36865#30340#20027#21483#34399#30908#65306
  end
  object Label9: TLabel
    Left = 104
    Top = 156
    Width = 84
    Height = 12
    Caption = #26368#22823#22806#25765#27425#25976#65306
  end
  object Label10: TLabel
    Left = 80
    Top = 180
    Width = 108
    Height = 12
    Caption = #22806#25765#38291#38548#26178#38291'('#31186')'#65306
  end
  object Label12: TLabel
    Left = 92
    Top = 300
    Width = 96
    Height = 12
    Caption = #39318#26781#35486#38899#25991#20214#21517#65306
  end
  object Label13: TLabel
    Left = 92
    Top = 276
    Width = 96
    Height = 12
    Caption = #36681#25509#30340#35441#21209#21729#32068#65306
  end
  object Label14: TLabel
    Left = 80
    Top = 131
    Width = 108
    Height = 12
    Caption = #19979#21320#38283#22987#22806#25765#26178#38291#65306
  end
  object Label17: TLabel
    Left = 296
    Top = 132
    Width = 108
    Height = 12
    Caption = #19979#21320#32080#26463#22806#25765#26178#38291#65306
  end
  object Label11: TLabel
    Left = 92
    Top = 396
    Width = 96
    Height = 12
    Caption = #26411#26781#35486#38899#25991#20214#21517#65306
  end
  object Label15: TLabel
    Left = 8
    Top = 372
    Width = 180
    Height = 12
    Caption = #22806#25765#35352#37636#20013#23459#21578#36039#26009#30340#21512#25104#26041#24335#65306
  end
  object Label16: TLabel
    Left = 320
    Top = 157
    Width = 84
    Height = 12
    Caption = #26368#22823#25033#31572#27425#25976#65306
  end
  object Label18: TLabel
    Left = 80
    Top = 204
    Width = 108
    Height = 12
    Caption = #26368#22823#21516#26178#22806#25765#36335#25976#65306
  end
  object Label19: TLabel
    Left = 92
    Top = 37
    Width = 96
    Height = 12
    Caption = #35519#29992#30340#26989#21209#27969#31243#65306
  end
  object Label20: TLabel
    Left = 104
    Top = 228
    Width = 84
    Height = 12
    Caption = #27511#21490#35352#37636#34389#29702#65306
  end
  object Label21: TLabel
    Left = 260
    Top = 205
    Width = 144
    Height = 12
    Caption = #25033#31572#21518#25918#38899#24310#26178#26178#38263'('#31186')'#65306
  end
  object Label22: TLabel
    Left = 92
    Top = 252
    Width = 96
    Height = 12
    Caption = #22806#25765#21518#34389#29702#26041#24335#65306
  end
  object Label23: TLabel
    Left = 62
    Top = 324
    Width = 126
    Height = 12
    Caption = #26412#20219#21209#30340'TTS'#21512#25104#20839#23481#65306
  end
  object Label24: TLabel
    Left = 62
    Top = 348
    Width = 126
    Height = 12
    Caption = #26412#20219#21209#30340'TTS'#21512#25104#26041#24335#65306
  end
  object Label25: TLabel
    Left = 16
    Top = 576
    Width = 402
    Height = 12
    Caption = #27880#65306#35531#23559#35486#38899#25991#20214#21450#38656#35201#21512#25104#30340'TXT'#25991#20214#25335#35997#21040'/ivrvoc/callcenter'#36335#24465#19979#12290
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label26: TLabel
    Left = 56
    Top = 420
    Width = 132
    Height = 12
    Caption = #22806#25765#25910#32893#35486#38899#35657#23526#26041#24335#65306
  end
  object Label27: TLabel
    Left = 44
    Top = 492
    Width = 144
    Height = 12
    Caption = #26989#21209#35519#26597#25552#31034#35486#38899#25991#20214#21517#65306
  end
  object Label28: TLabel
    Left = 128
    Top = 516
    Width = 60
    Height = 12
    Caption = #35519#26597#25353#37749#65306
  end
  object Label29: TLabel
    Left = 68
    Top = 468
    Width = 120
    Height = 12
    Caption = #36681#25509#23458#26381#30340#32676#32068#34399#30908#65306
  end
  object Label30: TLabel
    Left = 8
    Top = 444
    Width = 180
    Height = 12
    Caption = #22806#25765#25910#32893#35486#38899#26368#30701#26377#25928#26178#38263'('#31186')'#65306
  end
  object Label31: TLabel
    Left = 319
    Top = 59
    Width = 66
    Height = 12
    Caption = #22806#25765#25235#21462#30908':'
  end
  object editTaskName: TEdit
    Left = 192
    Top = 8
    Width = 121
    Height = 20
    MaxLength = 50
    TabOrder = 0
    OnChange = editTaskNameChange
  end
  object dtpStartDate: TDateTimePicker
    Left = 192
    Top = 80
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 3
    OnChange = editTaskNameChange
  end
  object dtpEndDate: TDateTimePicker
    Left = 376
    Top = 80
    Width = 85
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 4
    OnChange = editTaskNameChange
  end
  object dtpStartTime: TDateTimePicker
    Left = 192
    Top = 104
    Width = 54
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 5
    OnChange = editTaskNameChange
  end
  object dtpEndTime: TDateTimePicker
    Left = 408
    Top = 104
    Width = 53
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 6
    OnChange = editTaskNameChange
  end
  object cbStartId: TComboBox
    Left = 407
    Top = 32
    Width = 53
    Height = 20
    ItemHeight = 12
    TabOrder = 2
    Text = #26159
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #26159
      #21542)
  end
  object cseRouteNo: TCSpinEdit
    Left = 408
    Top = 176
    Width = 55
    Height = 21
    MaxValue = 64
    MinValue = 1
    TabOrder = 7
    Value = 1
    OnChange = editTaskNameChange
  end
  object cseMaxCalledTimes: TCSpinEdit
    Left = 192
    Top = 152
    Width = 55
    Height = 21
    MaxValue = 9999
    TabOrder = 8
    Value = 3
    OnChange = editTaskNameChange
  end
  object cseNoAnsDelay: TCSpinEdit
    Left = 192
    Top = 176
    Width = 55
    Height = 21
    MaxValue = 9999
    TabOrder = 9
    Value = 300
    OnChange = editTaskNameChange
  end
  object editCallerNo: TEdit
    Left = 192
    Top = 56
    Width = 121
    Height = 20
    Hint = #21482#36969#29992#19968#39636#27231
    MaxLength = 20
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = editTaskNameChange
  end
  object editFileName: TEdit
    Left = 192
    Top = 296
    Width = 271
    Height = 20
    MaxLength = 100
    TabOrder = 10
    OnChange = editTaskNameChange
  end
  object Button1: TButton
    Left = 120
    Top = 544
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 296
    Top = 544
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 12
    OnClick = Button2Click
  end
  object dtpStartTime2: TDateTimePicker
    Left = 192
    Top = 128
    Width = 54
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 13
    OnChange = editTaskNameChange
  end
  object dtpEndTime2: TDateTimePicker
    Left = 408
    Top = 128
    Width = 53
    Height = 20
    CalAlignment = dtaLeft
    Date = 39977.4104922569
    Time = 39977.4104922569
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkTime
    ParseInput = False
    TabOrder = 14
    OnChange = editTaskNameChange
  end
  object editFileName2: TEdit
    Left = 192
    Top = 392
    Width = 272
    Height = 20
    MaxLength = 100
    TabOrder = 15
    OnChange = editTaskNameChange
  end
  object cbGroupNo: TComboBox
    Left = 192
    Top = 272
    Width = 272
    Height = 20
    ItemHeight = 12
    TabOrder = 16
    Text = #19981#36681#20540#27231#22352#24109
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
  end
  object cbMixerType: TComboBox
    Left = 192
    Top = 368
    Width = 273
    Height = 20
    ItemHeight = 12
    TabOrder = 17
    Text = #19981#21512#25104
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #19981#21512#25104
      #25773#22577#25976#23383
      #25773#22577#37329#38989
      #25773#22577#25976#20540
      #35486#38899#25991#20214
      #23383#31526#20018'TTS'
      'TXT'#25991#20214'TTS')
  end
  object cseMaxAnsTimes: TCSpinEdit
    Left = 408
    Top = 152
    Width = 55
    Height = 21
    MaxValue = 9999
    TabOrder = 18
    Value = 1
    OnChange = editTaskNameChange
  end
  object cseMaxChnNum: TCSpinEdit
    Left = 192
    Top = 200
    Width = 55
    Height = 21
    MaxValue = 9999
    TabOrder = 19
    Value = 5
    OnChange = editTaskNameChange
  end
  object cbFuncNo: TComboBox
    Left = 192
    Top = 32
    Width = 121
    Height = 20
    ItemHeight = 12
    TabOrder = 20
    Text = #31995#32113#38928#35373#27969#31243
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
  end
  object cbExportId: TComboBox
    Left = 192
    Top = 224
    Width = 271
    Height = 20
    ItemHeight = 12
    TabOrder = 21
    Text = #19981#34389#29702
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #19981#34389#29702
      #33258#21205#21034#38500#25104#21151#35352#37636
      #33258#21205#21034#38500#22833#25943#35352#37636
      #33258#21205#21034#38500#25104#21151#21450#22833#25943#35352#37636
      #33258#21205#23559#25104#21151#35352#37636#21295#20837#27511#21490#34920
      #33258#21205#23559#22833#25943#35352#37636#21295#20837#27511#21490#34920
      #33258#21205#23559#25104#21151#21450#22833#25943#35352#37636#21295#20837#27511#21490#34920)
  end
  object csePlayDelay: TCSpinEdit
    Left = 408
    Top = 200
    Width = 55
    Height = 21
    MaxValue = 64
    MinValue = 1
    TabOrder = 22
    Value = 1
    OnChange = editTaskNameChange
  end
  object cbCallType: TComboBox
    Left = 192
    Top = 248
    Width = 271
    Height = 20
    ItemHeight = 12
    TabOrder = 23
    Text = #22806#25765#21518#23459#21578#35486#38899
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #22806#25765#21518#23459#21578#35486#38899
      #22806#25765#21518#30332#36865#20659#30495
      #22806#25765#25104#21151#21518#36681#22352#24109
      #20808#25765#22352#24109#28982#21518#22806#25765
      #22806#25765#21518#21363#21051#36681#24231#24109)
  end
  object editTTSData: TEdit
    Left = 192
    Top = 320
    Width = 271
    Height = 20
    Hint = #22914#26524#38656#35201#21512#25104#36229#36942'250'#23383#31526#30340#35531#36890#36942'TXT'#25991#20214#21512#25104
    MaxLength = 248
    ParentShowHint = False
    ShowHint = True
    TabOrder = 24
    OnChange = editTTSDataChange
  end
  object cbTTSType: TComboBox
    Left = 192
    Top = 344
    Width = 272
    Height = 20
    ItemHeight = 12
    TabOrder = 25
    Text = #19981#21512#25104
    OnChange = editTTSDataChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #19981#21512#25104
      #23383#31526#20018'TTS'
      'TXT'#25991#20214'TTS')
  end
  object cbConfirmType: TComboBox
    Left = 192
    Top = 416
    Width = 273
    Height = 20
    ItemHeight = 12
    TabOrder = 26
    Text = #25918#38899#21518#25346#26039
    OnChange = editTaskNameChange
    OnKeyPress = cbStartIdKeyPress
    Items.Strings = (
      #25918#38899#21518#25346#26039
      #25918#38899#21069#35426#21839#26159#21542#25353'1'#37749#25910#32893
      #25918#38899#21518#35426#21839#26159#21542#25353'1'#37749#35657#23526
      #26989#21209#35519#26597#25353#37749
      #25918#38899#21518#35426#21839#26159#21542#25353'1'#37749#36681#23458#26381
      #25918#38899#21518#35426#21839#26159#21542#25353'1'#37749#37325#32893)
  end
  object editConfirmVoc: TEdit
    Left = 192
    Top = 488
    Width = 272
    Height = 20
    MaxLength = 100
    TabOrder = 27
    OnChange = editTaskNameChange
  end
  object editConfirmDTMF: TEdit
    Left = 192
    Top = 512
    Width = 273
    Height = 20
    MaxLength = 100
    TabOrder = 28
    OnChange = editTaskNameChange
  end
  object editTranVDN: TEdit
    Left = 192
    Top = 464
    Width = 273
    Height = 20
    MaxLength = 100
    TabOrder = 29
    OnChange = editTaskNameChange
  end
  object cseListenTimeLen: TCSpinEdit
    Left = 192
    Top = 440
    Width = 55
    Height = 21
    MaxValue = 9999
    TabOrder = 30
    Value = 300
    OnChange = editTaskNameChange
  end
  object editDialPreCode: TEdit
    Left = 391
    Top = 55
    Width = 68
    Height = 20
    TabOrder = 31
    OnChange = editTaskNameChange
  end
end
