//---------------------------------------------------------------------------

#ifndef editorderlistH
#define editorderlistH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditOrderList : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TEdit *editOrderId;
  TEdit *editProductName;
  TEdit *editProductNo;
  TCSpinEdit *cseExpressTotals;
  TEdit *editBarCode;
  TLabel *Label6;
  TEdit *editTotals;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label7;
  void __fastcall cseExpressTotalsChange(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditOrderList(TComponent* Owner);

  int id;
  COrderlist Orderlist;
  void SetOrderListData(COrderlist &orderlist);
  int GetOrderListData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditOrderList *FormEditOrderList;
//---------------------------------------------------------------------------
#endif
