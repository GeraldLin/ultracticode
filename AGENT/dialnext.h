//---------------------------------------------------------------------------

#ifndef dialnextH
#define dialnextH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormDialNext : public TForm
{
__published:	// IDE-managed Components
  TButton *Button1;
  TLabel *Label1;
  TEdit *editCustName;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editCustSex;
  TEdit *editCustAge;
  TEdit *editCustSource;
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDialNext(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDialNext *FormDialNext;
//---------------------------------------------------------------------------
#endif
