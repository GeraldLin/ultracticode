//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "adddialouttele.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAddDialOutTele *FormAddDialOutTele;
//---------------------------------------------------------------------------
__fastcall TFormAddDialOutTele::TFormAddDialOutTele(TComponent* Owner)
  : TForm(Owner)
{
  nTaskId = 0;
}
//---------------------------------------------------------------------------
void TFormAddDialOutTele::ClearAllData()
{
  editTeleNo->Text = "";
  editBatchId->Text = "";
  editCustName->Text = "";
  editAccountNo->Text = "";
  editCustAddr->Text = "";
  Button1->Enabled = false;
}

void __fastcall TFormAddDialOutTele::Button1Click(TObject *Sender)
{
  if (editMobileNo->Text == "")
  {
    MessageBox(NULL,"請輸入手機號碼！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editCustName->Text == "")
  {
    MessageBox(NULL,"請輸入客戶名稱！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  nTaskId = dmAgent->DialOutTaskItemList.GetItemValue(cbTaskId->Text);
  if (nTaskId < 1)
  {
    MessageBox(NULL,"請選擇外撥任務！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (dmAgent->IsDialOutCalledNoCanImport(nTaskId, editMobileNo->Text, false) == 1)
  {
    MessageBox(NULL,"對不起,存在相同的手機號碼！","訊息提示",MB_OK|MB_ICONERROR);
    return;
  }
  int nAllocId = dmAgent->GetManDialOutTeleAllocId(nTaskId);
  if (dmAgent->InsertManDialOutTeleRecord(nTaskId, editBatchId->Text, editMobileNo->Text, editTeleNo->Text, editCustName->Text, editCustAddr->Text, editAccountNo->Text, nAllocId, strWorkerNo) == 0)
  {
    FormMain->QueryManDialOutTele(nTaskId);
    this->Close();
  }
  else
  {
    MessageBox(NULL,"對不起,新增號碼失敗！","訊息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormAddDialOutTele::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormAddDialOutTele::editMobileNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
