//---------------------------------------------------------------------------

#ifndef sendsmsH
#define sendsmsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormSendSms : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TEdit *editMobileNo;
  TLabel *Label2;
  TEdit *editSms;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label113;
  TComboBox *cbSendSmsSrvType;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSendSms(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSendSms *FormSendSms;
//---------------------------------------------------------------------------
#endif
