//---------------------------------------------------------------------------

#ifndef searchcustH
#define searchcustH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormSeachCustomer : public TForm
{
__published:	// IDE-managed Components
  TPanel *plCustExQry;
  TLabel *Label1;
  TEdit *Edit1;
  TButton *Button1;
  TButton *Button2;
  TDBGrid *dgCustom;
  TButton *Button3;
  TButton *Button4;
  TCheckBox *ckCustExSearch;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button4Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSeachCustomer(TComponent* Owner);

  int CustExQryItemNum;
  int CustExQryItemId[MAX_CUST_QUERYCONTROL_NUM];
  AnsiString CustExQrySql[MAX_CUST_QUERYCONTROL_NUM];
  TLabel *TCustExQryLabelList[MAX_CUST_QUERYCONTROL_NUM];
  TControl *TCustExQryInputList[MAX_CUST_QUERYCONTROL_NUM];

  void CreateCustExQryControl();
  void SetDispCustomItem();
  void SetDispCustExItem();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSeachCustomer *FormSeachCustomer;
//---------------------------------------------------------------------------
#endif
