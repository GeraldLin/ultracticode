object FormSeachCustomer: TFormSeachCustomer
  Left = 331
  Top = 100
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #37325#26032#26597#25214#20358#38651#32773#36039#26009
  ClientHeight = 579
  ClientWidth = 981
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object plCustExQry: TPanel
    Left = 0
    Top = 0
    Width = 981
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 16
      Width = 224
      Height = 12
      Caption = #35531#36664#20837#27169#31946#26597#35426#26781#20214'('#20358#38651#32773#22995#21517#25110#34399#30908')'#65306
    end
    object Edit1: TEdit
      Left = 240
      Top = 13
      Width = 131
      Height = 20
      TabOrder = 0
    end
    object Button1: TButton
      Left = 563
      Top = 9
      Width = 81
      Height = 27
      Caption = #26597#35426
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 891
      Top = 9
      Width = 81
      Height = 27
      Caption = #38364#38281
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 649
      Top = 9
      Width = 105
      Height = 27
      Caption = #35373#23450#28858#30446#21069#20358#38651#32773
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 768
      Top = 9
      Width = 75
      Height = 27
      Caption = #25033#31572
      TabOrder = 4
      OnClick = Button4Click
    end
    object ckCustExSearch: TCheckBox
      Left = 393
      Top = 13
      Width = 104
      Height = 18
      Caption = #21855#29992#25844#20805#26781#20214
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
  end
  object dgCustom: TDBGrid
    Left = 0
    Top = 45
    Width = 981
    Height = 534
    Align = alClient
    DataSource = dmAgent.dsSearchCust
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #26032#32048#26126#39636
    TitleFont.Style = []
    OnDblClick = Button3Click
    Columns = <
      item
        Expanded = False
        FieldName = 'CustomNo'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CustName'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CustTypeName'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CustLevelName'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CustSex'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispMobileNo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispTeleNo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DispFaxNo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ContAddr'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PostNo'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EMail'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Province'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CityName'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CountyName'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ZoneName'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CorpName'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SubPhone'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Remark'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AccountNo'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RegTime'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ItemData1'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData1'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData2'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData2'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData3'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData3'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData4'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData4'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData5'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData5'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData6'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData6'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData7'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData7'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData8'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData8'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData9'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData9'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData10'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData10'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData11'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData11'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData12'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData12'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData13'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData13'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData14'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData14'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData15'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData15'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData16'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData16'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData17'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData17'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData18'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData18'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData19'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData19'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData20'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData20'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData21'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData21'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData22'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData22'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData23'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData23'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData24'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData24'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData25'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData25'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData26'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData26'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData27'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData27'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData28'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData28'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData29'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData29'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData30'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData30'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData31'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData31'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ItemData32'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DispItemData32'
        Visible = False
      end>
  end
end
