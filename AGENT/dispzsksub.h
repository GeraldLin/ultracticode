//---------------------------------------------------------------------------

#ifndef dispzsksubH
#define dispzsksubH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormDispZSKSub : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label11;
  TLabel *Label8;
  TEdit *editMainTitle;
  TEdit *editRemark;
  TMemo *memoExplain;
  TButton *Button2;
  TEdit *editSubTitle;
  void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDispZSKSub(TComponent* Owner);

  void SetZSKSubData(CZSKSub& zsksub);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDispZSKSub *FormDispZSKSub;
//---------------------------------------------------------------------------
#endif
