//---------------------------------------------------------------------------

#ifndef editprodtypeH
#define editprodtypeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditProdType : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editProdType;
  TEdit *editProdName;
  TButton *Button1;
  TButton *Button2;
  TEdit *editGroupNoList;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TEdit *editItemURL;
  TMemo *memoDemo;
  TLabel *Label7;
  TCSpinEdit *cseDispOrder;
  TLabel *Label8;
  TEdit *editURLBtnName;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editProdTypeChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbGroupNoKeyPress(TObject *Sender, char &Key);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditProdType(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示囊改记录
  void SetDispMsg(bool bInsertId, int nProdType, AnsiString strItemName, AnsiString strGroupNo, AnsiString strURLBtnName, AnsiString strItemURL, AnsiString strDemo, int nDispOrder);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditProdType *FormEditProdType;
//---------------------------------------------------------------------------
#endif
