//---------------------------------------------------------------------------

#ifndef seeimageH
#define seeimageH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormImageSee : public TForm
{
__published:	// IDE-managed Components
  TImage *Image1;
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormImageSee(TComponent* Owner);

  AnsiString strImgFileName;
  void SetImagFileName(AnsiString filename);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormImageSee *FormImageSee;
//---------------------------------------------------------------------------
#endif
