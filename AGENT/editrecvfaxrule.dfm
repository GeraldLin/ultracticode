object FormRecvFaxRule: TFormRecvFaxRule
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #25509#25910#20659#30495#20998#30332#35215#21063#35373#23450
  ClientHeight = 169
  ClientWidth = 260
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 22
    Width = 60
    Height = 13
    Caption = #35215#21063#21443#25976#65306
  end
  object Label2: TLabel
    Left = 9
    Top = 74
    Width = 60
    Height = 13
    Caption = #25910#20214#37096#38272#65306
    Visible = False
  end
  object Label3: TLabel
    Left = 9
    Top = 73
    Width = 48
    Height = 13
    Caption = #25910#20214#20154#65306
  end
  object Label4: TLabel
    Left = 9
    Top = 47
    Width = 60
    Height = 13
    Caption = #20998#30332#26041#24335#65306
  end
  object editRuleItem: TEdit
    Left = 95
    Top = 17
    Width = 131
    Height = 21
    TabOrder = 0
    OnChange = editRuleItemChange
  end
  object cbRuleType: TComboBox
    Left = 95
    Top = 43
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = #20491#20154#20659#30495
    OnChange = cbRuleTypeChange
    Items.Strings = (
      #20844#21496#20844#20849#20659#30495
      #37096#38272#20844#20849#20659#30495
      #20491#20154#20659#30495)
  end
  object cbDepartment: TComboBox
    Left = 95
    Top = 69
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Visible = False
    OnChange = editRuleItemChange
  end
  object cbWorkerNo: TComboBox
    Left = 95
    Top = 69
    Width = 131
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    OnChange = editRuleItemChange
  end
  object Button1: TButton
    Left = 26
    Top = 130
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 156
    Top = 130
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 5
    OnClick = Button2Click
  end
end
