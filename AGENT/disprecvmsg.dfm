object FormRecvMsg: TFormRecvMsg
  Left = 399
  Top = 238
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #25910#21040#20839#37096#31777#35338
  ClientHeight = 172
  ClientWidth = 590
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 7
    Top = 22
    Width = 66
    Height = 14
    AutoSize = False
    Caption = #24231#24109#32232#34399#65306
  end
  object Label2: TLabel
    Left = 210
    Top = 22
    Width = 63
    Height = 14
    AutoSize = False
    Caption = #22519#27231#32232#34399#65306
  end
  object Label3: TLabel
    Left = 400
    Top = 22
    Width = 39
    Height = 14
    AutoSize = False
    Caption = #22995#21517#65306
  end
  object Label4: TLabel
    Left = 36
    Top = 56
    Width = 39
    Height = 14
    AutoSize = False
    Caption = #31777#35338#65306
  end
  object Label5: TLabel
    Left = 36
    Top = 100
    Width = 39
    Height = 14
    AutoSize = False
    Caption = #22238#24489#65306
  end
  object editSeatNo: TEdit
    Left = 78
    Top = 17
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
  end
  object editWorkerNo: TEdit
    Left = 280
    Top = 17
    Width = 111
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 1
  end
  object editWorkerName: TEdit
    Left = 442
    Top = 17
    Width = 131
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 2
  end
  object editMsg: TEdit
    Left = 78
    Top = 52
    Width = 495
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 3
  end
  object Button1: TButton
    Left = 139
    Top = 130
    Width = 81
    Height = 27
    Caption = #22238#24489
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 381
    Top = 130
    Width = 82
    Height = 27
    Caption = #38364#38281
    TabOrder = 5
    OnClick = Button2Click
  end
  object editSendMsg: TEdit
    Left = 78
    Top = 95
    Width = 495
    Height = 21
    TabOrder = 6
  end
end
