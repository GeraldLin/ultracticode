object FormEditProduct: TFormEditProduct
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20462#25913#20135#21697#36164#26009
  ClientHeight = 439
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 23
    Top = 12
    Width = 60
    Height = 13
    Caption = #21830#21697#32534#21495#65306
  end
  object Label2: TLabel
    Left = 239
    Top = 12
    Width = 60
    Height = 13
    Caption = #21830#21697#21517#31216#65306
  end
  object Label3: TLabel
    Left = 19
    Top = 36
    Width = 60
    Height = 13
    Caption = #21830#21697#22411#21495#65306
  end
  object Label4: TLabel
    Left = 267
    Top = 36
    Width = 36
    Height = 13
    Caption = #21697#29260#65306
  end
  object Label5: TLabel
    Left = 51
    Top = 140
    Width = 36
    Height = 13
    Caption = #31616#20171#65306
  end
  object Label6: TLabel
    Left = 51
    Top = 172
    Width = 36
    Height = 13
    Caption = #21333#20301#65306
  end
  object Label7: TLabel
    Left = 249
    Top = 172
    Width = 54
    Height = 13
    Caption = #37325#37327'(kg)'#65306
  end
  object Label8: TLabel
    Left = 37
    Top = 196
    Width = 48
    Height = 13
    Caption = #24066#22330#20215#65306
  end
  object Label9: TLabel
    Left = 253
    Top = 196
    Width = 48
    Height = 13
    Caption = #20250#21592#20215#65306
  end
  object Label10: TLabel
    Left = 24
    Top = 228
    Width = 60
    Height = 13
    Caption = #20135#21697#22270#29255#65306
  end
  object Label11: TLabel
    Left = 24
    Top = 251
    Width = 60
    Height = 13
    Caption = #35814#32454#20171#32461#65306
  end
  object Label12: TLabel
    Left = 23
    Top = 348
    Width = 60
    Height = 13
    Caption = #24211#23384#25968#37327#65306
  end
  object Label13: TLabel
    Left = 51
    Top = 380
    Width = 36
    Height = 13
    Caption = #22791#27880#65306
  end
  object Label14: TLabel
    Left = 11
    Top = 60
    Width = 72
    Height = 13
    Caption = #20379#24212#21830#21517#31216#65306
  end
  object Label15: TLabel
    Left = 227
    Top = 60
    Width = 72
    Height = 13
    Caption = #20379#24212#21830#21495#30721#65306
  end
  object Label16: TLabel
    Left = 3
    Top = 84
    Width = 84
    Height = 13
    Caption = #20379#24212#21830#32852#31995#20154#65306
  end
  object Label17: TLabel
    Left = 11
    Top = 108
    Width = 72
    Height = 13
    Caption = #20379#24212#21830#22320#22336#65306
  end
  object editProductNo: TEdit
    Left = 96
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    OnChange = editProductNoChange
  end
  object editName: TEdit
    Left = 312
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = editProductNoChange
  end
  object editProductType: TEdit
    Left = 96
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 2
    OnChange = editProductNoChange
  end
  object editBrand: TEdit
    Left = 312
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 3
    OnChange = editProductNoChange
  end
  object editShortDesc: TEdit
    Left = 96
    Top = 136
    Width = 337
    Height = 21
    TabOrder = 4
    OnChange = editProductNoChange
  end
  object editUnit: TEdit
    Left = 96
    Top = 168
    Width = 121
    Height = 21
    TabOrder = 5
    OnChange = editProductNoChange
  end
  object editWeight: TEdit
    Left = 312
    Top = 168
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '0'
    OnChange = editProductNoChange
  end
  object editMarketPrice: TEdit
    Left = 96
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '0'
    OnChange = editProductNoChange
  end
  object editMemberPrice: TEdit
    Left = 312
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 8
    Text = '0'
    OnChange = editProductNoChange
  end
  object editProductImg: TEdit
    Left = 96
    Top = 224
    Width = 313
    Height = 21
    TabOrder = 9
    OnChange = editProductNoChange
  end
  object editStorage: TEdit
    Left = 96
    Top = 344
    Width = 121
    Height = 21
    TabOrder = 10
    Text = '0'
    OnChange = editProductNoChange
  end
  object editRemark: TEdit
    Left = 96
    Top = 376
    Width = 337
    Height = 21
    TabOrder = 11
    OnChange = editProductNoChange
  end
  object memoDescription: TMemo
    Left = 96
    Top = 248
    Width = 337
    Height = 89
    TabOrder = 12
    OnChange = editProductNoChange
  end
  object Button1: TButton
    Left = 112
    Top = 408
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 13
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 272
    Top = 408
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 14
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 411
    Top = 224
    Width = 23
    Height = 21
    Caption = '...'
    TabOrder = 15
    OnClick = Button3Click
  end
  object editSupplierName: TEdit
    Left = 96
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 16
    OnChange = editProductNoChange
  end
  object editSupplierTel: TEdit
    Left = 312
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 17
    OnChange = editProductNoChange
  end
  object editSupplierMem: TEdit
    Left = 96
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 18
    OnChange = editProductNoChange
  end
  object editSupplierAddr: TEdit
    Left = 96
    Top = 104
    Width = 337
    Height = 21
    TabOrder = 19
    OnChange = editProductNoChange
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.jpg'
    Filter = 'BMP'#25991#20214'(*.bmp)|*.bmp|JPG'#25991#20214'(*.jpg)|*.jpg'
    Left = 8
    Top = 272
  end
end
