//---------------------------------------------------------------------------

#ifndef addcustomerH
#define addcustomerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include "public.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormAddCustom : public TForm
{
__published:	// IDE-managed Components
  TButton *Button1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TLabel *Label12;
  TEdit *edtCustomNo;
  TEdit *edtCustName;
  TEdit *edtCustSex;
  TEdit *edtCorpName;
  TEdit *edtMobileNo;
  TEdit *edtTeleNo;
  TEdit *edtFaxNo;
  TEdit *edtEMail;
  TEdit *edtContAddr;
  TEdit *edtPostNo;
  TEdit *edtRemark;
  TLabel *Label1;
  TEdit *edtCustType;
  TLabel *Label13;
  TEdit *edtPoints;
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAddCustom(TComponent* Owner);
  CCustomer Customer;
  void SetCustom(CCustomer *customer);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAddCustom *FormAddCustom;
//---------------------------------------------------------------------------
#endif
