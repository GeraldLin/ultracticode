object FormAddDialOutTele: TFormAddDialOutTele
  Left = 192
  Top = 114
  BorderStyle = bsSingle
  Caption = #26032#22686#22806#25765#34399#30908
  ClientHeight = 189
  ClientWidth = 433
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 31
    Top = 45
    Width = 51
    Height = 12
    Caption = #25163#27231#34399#30908':'
  end
  object Label2: TLabel
    Left = 31
    Top = 77
    Width = 51
    Height = 12
    Caption = #26371#21729#22995#21517':'
  end
  object Label3: TLabel
    Left = 31
    Top = 108
    Width = 51
    Height = 12
    Caption = #23458#25142#20301#22336':'
  end
  object Label4: TLabel
    Left = 223
    Top = 76
    Width = 51
    Height = 12
    Caption = #26371#21729#24115#34399':'
  end
  object Label5: TLabel
    Left = 223
    Top = 13
    Width = 51
    Height = 12
    Caption = #25209#27425#34399#30908':'
  end
  object Label253: TLabel
    Left = 30
    Top = 15
    Width = 51
    Height = 12
    Caption = #22806#25765#20219#21209':'
  end
  object Label6: TLabel
    Left = 223
    Top = 45
    Width = 51
    Height = 12
    Caption = #24066#35441#34399#30908':'
  end
  object editMobileNo: TEdit
    Left = 88
    Top = 40
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editMobileNoChange
  end
  object editCustName: TEdit
    Left = 88
    Top = 72
    Width = 121
    Height = 20
    TabOrder = 1
    OnChange = editMobileNoChange
  end
  object editAccountNo: TEdit
    Left = 280
    Top = 72
    Width = 121
    Height = 20
    TabOrder = 2
    OnChange = editMobileNoChange
  end
  object editCustAddr: TEdit
    Left = 88
    Top = 104
    Width = 313
    Height = 20
    TabOrder = 3
    OnChange = editMobileNoChange
  end
  object Button1: TButton
    Left = 96
    Top = 144
    Width = 75
    Height = 25
    Caption = #26032#22686
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 280
    Top = 144
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 5
    OnClick = Button2Click
  end
  object editBatchId: TEdit
    Left = 280
    Top = 8
    Width = 121
    Height = 20
    TabOrder = 6
    OnChange = editMobileNoChange
  end
  object cbTaskId: TComboBox
    Left = 89
    Top = 11
    Width = 120
    Height = 20
    ItemHeight = 12
    TabOrder = 7
  end
  object editTeleNo: TEdit
    Left = 280
    Top = 40
    Width = 121
    Height = 20
    TabOrder = 8
    OnChange = editMobileNoChange
  end
end
