//---------------------------------------------------------------------------

#ifndef dialerH
#define dialerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <MPlayer.hpp>
//---------------------------------------------------------------------------
class TFormDial : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TComboBox *cbDialNum;
  TButton *Button1;
  TButton *Button2;
  TButton *Button3;
  TButton *btDTMF1;
  TButton *btDTMF2;
  TButton *btDTMF3;
  TButton *btDTMF4;
  TButton *btDTMF5;
  TButton *btDTMF6;
  TButton *btDTMF7;
  TButton *btDTMF8;
  TButton *btDTMF9;
  TButton *btDTMFX;
  TButton *btDTM0;
  TButton *btDTMFJ;
  TCheckBox *ckMobileId;
  TGroupBox *gbFastDial;
  TButton *btFuncF1;
  TButton *btFuncF2;
  TButton *btFuncF3;
  TButton *btFuncF4;
  TButton *btFuncF5;
  TButton *btFuncF6;
  TButton *btFuncF7;
  TButton *btFuncF8;
  TButton *btFuncF9;
  TButton *btFuncF10;
  TButton *btFuncF11;
  TButton *btFuncF12;
  TLabel *lbFuncF1;
  TLabel *lbFuncF2;
  TLabel *lbFuncF3;
  TLabel *lbFuncF4;
  TLabel *lbFuncF5;
  TLabel *lbFuncF6;
  TLabel *lbFuncF7;
  TLabel *lbFuncF8;
  TLabel *lbFuncF9;
  TLabel *lbFuncF10;
  TLabel *lbFuncF11;
  TLabel *lbFuncF12;
  TCheckBox *ckTranType;
  TButton *btUpDown;
  TMediaPlayer *MediaPlayer1;
  TCheckBox *ckAutoAdd9OnDial;
  TCheckBox *ckAutoAdd9OnTran;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall btDTMF1Click(TObject *Sender);
  void __fastcall btDTMF2Click(TObject *Sender);
  void __fastcall btDTMF3Click(TObject *Sender);
  void __fastcall btDTMF4Click(TObject *Sender);
  void __fastcall btDTMF5Click(TObject *Sender);
  void __fastcall btDTMF6Click(TObject *Sender);
  void __fastcall btDTMF7Click(TObject *Sender);
  void __fastcall btDTMF8Click(TObject *Sender);
  void __fastcall btDTMF9Click(TObject *Sender);
  void __fastcall btDTMFXClick(TObject *Sender);
  void __fastcall btDTM0Click(TObject *Sender);
  void __fastcall btDTMFJClick(TObject *Sender);
  void __fastcall ckMobileIdClick(TObject *Sender);
  void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
  void __fastcall btFuncF1Click(TObject *Sender);
  void __fastcall btFuncF2Click(TObject *Sender);
  void __fastcall btFuncF3Click(TObject *Sender);
  void __fastcall btFuncF4Click(TObject *Sender);
  void __fastcall btFuncF5Click(TObject *Sender);
  void __fastcall btFuncF6Click(TObject *Sender);
  void __fastcall btFuncF7Click(TObject *Sender);
  void __fastcall btFuncF8Click(TObject *Sender);
  void __fastcall btFuncF9Click(TObject *Sender);
  void __fastcall btFuncF10Click(TObject *Sender);
  void __fastcall btFuncF11Click(TObject *Sender);
  void __fastcall btFuncF12Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall btUpDownClick(TObject *Sender);
  void __fastcall ckAutoAdd9OnDialClick(TObject *Sender);
  void __fastcall ckAutoAdd9OnTranClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormDial(TComponent* Owner);

  int DialFlag; //1-���� 2-ת�Ӻ���
  void FuncKeyDialOut(int nFuncKeyId);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormDial *FormDial;
//---------------------------------------------------------------------------
#endif
