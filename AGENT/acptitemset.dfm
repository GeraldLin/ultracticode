object FormAcptItemSet: TFormAcptItemSet
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #26989#21209#21463#29702#23450#32681#38917#30446#21443#25976#35373#23450
  ClientHeight = 564
  ClientWidth = 610
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 78
    Top = 22
    Width = 72
    Height = 12
    Caption = #21463#29702#26694#24207#34399#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 78
    Top = 48
    Width = 72
    Height = 12
    Caption = #21463#29702#26694#39006#22411#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 304
    Top = 22
    Width = 84
    Height = 12
    Caption = #21463#29702#38917#30446#21517#31281#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 103
    Top = 75
    Width = 48
    Height = 12
    Caption = #38928#35373#20540#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 324
    Top = 101
    Width = 48
    Height = 12
    Caption = #26368#22823#20540#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 86
    Top = 102
    Width = 48
    Height = 12
    Caption = #26368#23567#20540#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 90
    Top = 129
    Width = 60
    Height = 12
    Caption = #35215#23450#26684#24335#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 90
    Top = 154
    Width = 60
    Height = 12
    Caption = #24171#21161#25552#31034#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 66
    Top = 184
    Width = 84
    Height = 12
    Caption = #21295#20986#21512#24182#26041#24335#65306
  end
  object Label10: TLabel
    Left = 293
    Top = 181
    Width = 96
    Height = 12
    Caption = #21512#24182#21463#29702#26694#24207#34399#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 72
    Top = 210
    Width = 78
    Height = 12
    Caption = #21512#24182#26781#20214#28858'='#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 91
    Top = 237
    Width = 60
    Height = 12
    Caption = #21295#20986#38918#24207#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 331
    Top = 237
    Width = 60
    Height = 12
    Caption = #20803#20214#23532#24230#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
  end
  object editTid: TEdit
    Left = 159
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editTidChange
  end
  object cbControlType: TComboBox
    Left = 159
    Top = 43
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 1
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
  end
  object editLabelName: TEdit
    Left = 393
    Top = 17
    Width = 131
    Height = 20
    MaxLength = 20
    TabOrder = 2
    OnChange = editTidChange
  end
  object editDefaultData: TEdit
    Left = 159
    Top = 70
    Width = 365
    Height = 20
    MaxLength = 100
    TabOrder = 3
    OnChange = editTidChange
  end
  object cseTMinValue: TCSpinEdit
    Left = 159
    Top = 96
    Width = 131
    Height = 21
    TabOrder = 4
    OnChange = editTidChange
  end
  object cseTMaxValue: TCSpinEdit
    Left = 393
    Top = 96
    Width = 131
    Height = 21
    TabOrder = 5
    OnChange = editTidChange
  end
  object Button1: TButton
    Left = 152
    Top = 269
    Width = 81
    Height = 27
    Caption = #30906#23450
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 351
    Top = 269
    Width = 82
    Height = 27
    Caption = #21462#28040
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = Button2Click
  end
  object ckIsAllowNull: TCheckBox
    Left = 305
    Top = 43
    Width = 80
    Height = 19
    Caption = #20801#35377#28858#31354'?'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object ckExportId: TCheckBox
    Left = 443
    Top = 46
    Width = 81
    Height = 18
    Caption = #20801#35377#21295#20986'?'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 9
  end
  object editMaskEdit: TEdit
    Left = 159
    Top = 124
    Width = 365
    Height = 20
    TabOrder = 10
    OnChange = editTidChange
  end
  object editHintStr: TEdit
    Left = 159
    Top = 149
    Width = 365
    Height = 20
    MaxLength = 100
    TabOrder = 11
    OnChange = editTidChange
  end
  object Memo1: TMemo
    Left = 8
    Top = 328
    Width = 593
    Height = 177
    Lines.Strings = (
      #35215#23450#26684#24335#65306#32232#30908#26684#24335#21487#20197#20998#28858#19977#37096#20998#27599#20491#37096#20998#20043#38291#29992#20998#34399#8220';'#8221#20998#38283': A;B;C'
      ''
      'A-'#37096#20998':'#22312#36889#19968#37096#20998#29992#19968#20123#29305#27530#30340#26684#24335#31526#20358#34920#31034#25033#36664#20837#30340#23383#31526#39006#22411#21450#26684#24335#65292#24120#29992#30340#29305#27530#26684#24335#31526#26377#65306
      '    !   '#21435#25481#36664#20837#25976#25818#38283#38957#30340#31354#26684#31526
      '    >   '#35731#36664#20837#25976#25818#30340#23383#27597#37117#35722#25104#22823#23531#65292#30452#21040#36935#19978#25513#30908#23383#31526'<'
      '    <   '#35731#36664#20837#25976#25818#30340#23383#27597#37117#35722#25104#23567#23531#65292#30452#21040#36935#19978#25513#30908#23383#31526'>'
      '    <>  '#19981#38480#21046#36664#20837#25976#25818#23383#27597#26159#22823#23531#25110#23567#23531
      '    \   '#33509#22312#36664#20837#26684#24335#20839#21152#20837#26576#20491#29305#27530#23383#31526#65292#21482#35201#22312#29305#27530#23383#31526#30340#21069#38754#21152#19978#27492#25513#30908#23601#21487#20197#12290#65288#20854#23526#19981#21152#20063#21487#20197#65289
      '    L   '#20801#35377#36664#20837#33521#25991#23383#27597#65292#32780#19988#19968#23450#35201#36664#20837
      '    l   '#20801#35377#36664#20837#33521#25991#23383#27597#65292#19981#19968#23450#35201#36664#20837
      '    A   '#20801#35377#36664#20837#33521#25991#23383#27597#21644#38463#25289#20271#25976#23383#65292#19968#23450#35201#36664#20837
      '    a   '#20801#35377#36664#20837#33521#25991#23383#27597#21644#38463#25289#20271#25976#23383#65292#19981#19968#23450#35201#36664#20837
      '    C   '#20801#35377#36664#20837#20219#20309#23383#31526#65292#19968#23450#35201#36664#20837
      '    c   '#20801#35377#36664#20837#20219#20309#23383#31526#65292#19981#19968#23450#35201#36664#20837
      '    0   '#20801#35377#36664#20837#38463#25289#20271#25976#23383#65292#19968#23450#35201#36664#20837
      '    9   '#20801#35377#36664#20837#38463#25289#20271#25976#23383#65292#19981#19968#23450#35201#36664#20837
      '    #   '#20801#35377#36664#20837#38463#25289#20271#25976#23383#25110#27491#12289#36000#34399#23383#31526
      '    :  '#65288#20882#34399#65289#29992#20358#20998#38548#26178#38291#25976#25818#20013#30340#26178#12289#20998#12289#31186
      '    /   '#29992#20358#20998#38548#26085#26399#25976#25818#20013#30340#24180#12289#26376#12289#26085
      ''
      'B-'#37096#20998':'#21482#26377'0'#21644'1'#20841#31278#36984#25799#12290#22914#28858'1'#65292#21063#25513#30908#20013#30340#38750#29992#25142#36664#20837#25976#25818#21644#27161#28310#20998#38548#31526#31561#20854#23427#21508#31278#23383#31526#26371#20316#28858#25976#25818#30340
      #19968#37096#20998#20445#23384#65307#28858'0'#21063#19981#20445#23384#12290
      ''
      'C-'#37096#20998':'#29992#20110#34920#31034#25976#25818#20013#30340#31354#20301#29992#21738#20491#23383#31526#20195#26367#39023#31034#12290
      ''
      #22914': !AAAAA-AAAAAA;1;_  '#34920#31034#21069#38754#26159'5'#30908#33521#25991#23383#27597#21644#38463#25289#20271#25976#23383'('#24517#38656#35201#36664#20837'),'#20013#38291#29992#28187#34399#38548#38283','#21518#38754#26159'5'#30908#33521
      #25991#23383#27597#21644#38463#25289#20271#25976#23383'('#24517#38656#35201#36664#20837')')
    ScrollBars = ssVertical
    TabOrder = 12
  end
  object cbCombineMode: TComboBox
    Left = 160
    Top = 176
    Width = 129
    Height = 20
    ItemHeight = 12
    TabOrder = 13
    Text = #19981#21512#24182
    Items.Strings = (
      #19981#21512#24182
      #26367#25563
      #21512#24182)
  end
  object cseCombineTid: TCSpinEdit
    Left = 394
    Top = 176
    Width = 131
    Height = 21
    MaxValue = 32
    TabOrder = 14
    OnChange = editTidChange
  end
  object editCombineValue: TEdit
    Left = 159
    Top = 205
    Width = 365
    Height = 20
    MaxLength = 100
    TabOrder = 15
    OnChange = editTidChange
  end
  object cseExportOrderId: TCSpinEdit
    Left = 160
    Top = 232
    Width = 131
    Height = 21
    MaxValue = 128
    TabOrder = 16
    OnChange = editTidChange
  end
  object cbWith: TComboBox
    Left = 394
    Top = 232
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 17
    Text = #27161#20934#23532#24230
    OnChange = cbControlTypeChange
    OnKeyPress = cbControlTypeKeyPress
    Items.Strings = (
      #27161#20934#23532#24230
      #27161#20934#23532#24230#22235#20998#20043#19968
      #27161#20934#23532#24230#20108#20998#20043#19968
      #27161#20934#23532#24230#22235#20998#20043#19977)
  end
end
