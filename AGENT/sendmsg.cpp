//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <inifiles.hpp>
#include "DataModule.h"
#include "sendmsg.h"
#include "main.h"
#include "showmsg.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormSendMsg *FormSendMsg;
extern BSTR bEmpty;
extern CMyWorker MyWorker;
//---------------------------------------------------------------------------
__fastcall TFormSendMsg::TFormSendMsg(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSendMsg::Button1Click(TObject *Sender)
{
  AnsiString SeatNo="", strWorkerName, strWorkerNo;
  WideString wSeatNo, wMsg;
  BSTR bSeatNo, bMsg;
  AnsiString workerno;

  if (editSendMsg->Text.Length() == 0)
  {
    MessageBox(NULL,"請輸入需要發送的訊息內容！","訊息提示",MB_OK|MB_ICONWARNING);
    return;
  }

  if (RadioGroup1->ItemIndex == 0)
  {
    if (cbSeatNo->Text == "群發")
    {
      SeatNo = "0";
      workerno = "0";
    }
    else
    {
      if (cbSeatNo->Text == "")
      {
        MessageBox(NULL,"請輸入分機號碼！","訊息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      SeatNo = cbSeatNo->Text;
      workerno = FormMain->SeatList.GetWorkerNo(SeatNo);
    }
  }
  else
  {
    if (cbWorkerNo->Text == "群發")
    {
      SeatNo = "0";
      workerno = "0";
    }
    else
    {
      if (cbWorkerNo->Text == "")
      {
        MessageBox(NULL,"請輸入值機編號！","訊息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      try
      {
        workerno = cbWorkerNo->Text;
        SeatNo = FormMain->SeatList.GetSeatNo(workerno);
      }
      catch (...)
      {
        MessageBox(NULL,"請輸入正確的工號！","訊息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }
  }
  if (workerno == "0")
  {
    dmAgent->InsertSendMsgData(0, "群發", editSendMsg->Text);

    try
    {
      dmAgent->adoqueryPub1->SQL->Clear();
      dmAgent->adoqueryPub1->SQL->Add("Select WorkerNo,WorkerName from tbWorker order by WorkerNo");
      dmAgent->adoqueryPub1->Open();
      while ( !dmAgent->adoqueryPub1->Eof )
      {
        strWorkerNo = dmAgent->adoqueryPub1->FieldByName("WorkerNo")->AsString;
        if (FormMain->SeatList.GetSeatNo(strWorkerNo) == "")
          dmAgent->InsertRecvMsgData(strWorkerNo, MyWorker.WorkerNo, MyWorker.WorkerName, editSendMsg->Text, 0);
        else
          dmAgent->InsertRecvMsgData(strWorkerNo, MyWorker.WorkerNo, MyWorker.WorkerName, editSendMsg->Text);
        dmAgent->adoqueryPub1->Next();
      }
      dmAgent->adoqueryPub1->Close();
    }
    catch ( ... )
    {
    }
  }
  else
  {
    if (SeatNo.Length() > 0)
      dmAgent->InsertRecvMsgData(workerno, MyWorker.WorkerNo, MyWorker.WorkerName, editSendMsg->Text);
    else
      dmAgent->InsertRecvMsgData(workerno, MyWorker.WorkerNo, MyWorker.WorkerName, editSendMsg->Text, 0);
    dmAgent->InsertSendMsgData(workerno, dmAgent->GetWorkerName(workerno), editSendMsg->Text);
  }

  if (SeatNo.Length() > 0)
  {
    wSeatNo = WideString(SeatNo);
    bSeatNo = ( wchar_t * )wSeatNo;

    wMsg = WideString(editSendMsg->Text);
    bMsg = ( wchar_t * )wMsg;

    FormMain->MyAgent1->SendMessage(bSeatNo, 0, 0, bMsg);
  }
  FormMsg->SetShowMsg("訊息已發送!!!");
  FormMsg->Show();
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSendMsg::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormSendMsg::RadioGroup1Click(TObject *Sender)
{
  if (RadioGroup1->ItemIndex == 0)
  {
    Label1->Visible = true;
    cbSeatNo->Visible = true;
    Label3->Visible = false;
    cbWorkerNo->Visible = false;
  }
  else
  {
    Label1->Visible = false;
    cbSeatNo->Visible = false;
    Label3->Visible = true;
    cbWorkerNo->Visible = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormSendMsg::FormShow(TObject *Sender)
{
  FormSendMsg->editSendMsg->SetFocus();  
}
//---------------------------------------------------------------------------

