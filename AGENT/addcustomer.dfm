object FormAddCustom: TFormAddCustom
  Left = 409
  Top = 119
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #23458#25143#36164#26009
  ClientHeight = 254
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 20
    Width = 60
    Height = 13
    Caption = #23458#25143#32534#21495#65306
  end
  object Label3: TLabel
    Left = 16
    Top = 44
    Width = 60
    Height = 13
    Caption = #23458#25143#21517#31216#65306
  end
  object Label4: TLabel
    Left = 16
    Top = 67
    Width = 60
    Height = 13
    Caption = #20844#21496#21517#31216#65306
  end
  object Label5: TLabel
    Left = 218
    Top = 20
    Width = 60
    Height = 13
    Caption = #23458#25143#31867#22411#65306
  end
  object Label6: TLabel
    Left = 248
    Top = 43
    Width = 36
    Height = 13
    Caption = #24615#21035#65306
  end
  object Label7: TLabel
    Left = 40
    Top = 92
    Width = 36
    Height = 13
    Caption = #25163#26426#65306
  end
  object Label8: TLabel
    Left = 248
    Top = 92
    Width = 36
    Height = 13
    Caption = #22266#35805#65306
  end
  object Label9: TLabel
    Left = 40
    Top = 116
    Width = 36
    Height = 13
    Caption = #20256#30495#65306
  end
  object Label10: TLabel
    Left = 248
    Top = 115
    Width = 36
    Height = 13
    Caption = #37038#31665#65306
  end
  object Label11: TLabel
    Left = 40
    Top = 139
    Width = 36
    Height = 13
    Caption = #22320#22336#65306
  end
  object Label12: TLabel
    Left = 40
    Top = 164
    Width = 36
    Height = 13
    Caption = #37038#32534#65306
  end
  object Label1: TLabel
    Left = 40
    Top = 187
    Width = 36
    Height = 13
    Caption = #22791#27880#65306
  end
  object Label13: TLabel
    Left = 248
    Top = 164
    Width = 36
    Height = 13
    Caption = #31215#20998#65306
  end
  object Button1: TButton
    Left = 176
    Top = 216
    Width = 75
    Height = 25
    Caption = #20851#38381
    TabOrder = 0
    OnClick = Button1Click
  end
  object edtCustomNo: TEdit
    Left = 80
    Top = 16
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 1
  end
  object edtCustName: TEdit
    Left = 80
    Top = 40
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 2
  end
  object edtCustSex: TEdit
    Left = 282
    Top = 40
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 3
  end
  object edtCorpName: TEdit
    Left = 80
    Top = 64
    Width = 323
    Height = 21
    ReadOnly = True
    TabOrder = 4
  end
  object edtMobileNo: TEdit
    Left = 80
    Top = 88
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 5
  end
  object edtTeleNo: TEdit
    Left = 282
    Top = 88
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 6
  end
  object edtFaxNo: TEdit
    Left = 80
    Top = 112
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 7
  end
  object edtEMail: TEdit
    Left = 282
    Top = 112
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 8
  end
  object edtContAddr: TEdit
    Left = 80
    Top = 136
    Width = 323
    Height = 21
    ReadOnly = True
    TabOrder = 9
  end
  object edtPostNo: TEdit
    Left = 80
    Top = 160
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 10
  end
  object edtRemark: TEdit
    Left = 80
    Top = 184
    Width = 324
    Height = 21
    ReadOnly = True
    TabOrder = 11
  end
  object edtCustType: TEdit
    Left = 282
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 12
  end
  object edtPoints: TEdit
    Left = 282
    Top = 160
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 13
    Text = '0'
  end
end
