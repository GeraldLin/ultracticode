object FormEditOrderFit: TFormEditOrderFit
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #35746#21333#23433#35013#24773#20917#30331#35760
  ClientHeight = 308
  ClientWidth = 511
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 43
    Width = 92
    Height = 13
    AutoSize = False
    Caption = #39044#35745#23433#35013#26102#38388#65306
  end
  object Label2: TLabel
    Left = 257
    Top = 44
    Width = 91
    Height = 13
    AutoSize = False
    Caption = #23454#38469#23433#35013#26102#38388#65306
  end
  object Label4: TLabel
    Left = 9
    Top = 92
    Width = 91
    Height = 13
    AutoSize = False
    Caption = #23433#35013#26381#21153#21830#21517#65306
  end
  object Label5: TLabel
    Left = 37
    Top = 116
    Width = 63
    Height = 13
    AutoSize = False
    Caption = #23433#35013#20154#21592#65306
  end
  object Label6: TLabel
    Left = 229
    Top = 116
    Width = 121
    Height = 13
    AutoSize = False
    Caption = #23433#35013#20154#21592#32852#31995#30005#35805#65306
  end
  object Label7: TLabel
    Left = 36
    Top = 68
    Width = 65
    Height = 13
    AutoSize = False
    Caption = #23433#35013#29366#24577#65306
  end
  object Label8: TLabel
    Left = 9
    Top = 137
    Width = 92
    Height = 13
    AutoSize = False
    Caption = #23433#35013#24773#20917#35760#24405#65306
  end
  object Label10: TLabel
    Left = 50
    Top = 12
    Width = 50
    Height = 13
    AutoSize = False
    Caption = #35746#21333#21495#65306
  end
  object editPreFitDate: TEdit
    Left = 104
    Top = 40
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 0
    OnChange = editPreFitDateChange
  end
  object editFactFitDate: TEdit
    Left = 352
    Top = 40
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 1
    OnChange = editPreFitDateChange
  end
  object editFitSrvCorp: TEdit
    Left = 104
    Top = 88
    Width = 369
    Height = 21
    TabOrder = 2
    OnChange = editPreFitDateChange
  end
  object editFitMen: TEdit
    Left = 104
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 3
    OnChange = editPreFitDateChange
  end
  object editFitMenTel: TEdit
    Left = 352
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 4
    OnChange = editPreFitDateChange
  end
  object cbFitStatus: TComboBox
    Left = 104
    Top = 64
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    OnChange = editPreFitDateChange
    OnKeyPress = cbFitStatusKeyPress
  end
  object memoFitNote: TMemo
    Left = 104
    Top = 136
    Width = 369
    Height = 129
    ScrollBars = ssBoth
    TabOrder = 6
    OnChange = editPreFitDateChange
  end
  object Button1: TButton
    Left = 112
    Top = 272
    Width = 75
    Height = 25
    Caption = #20462#25913
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 288
    Top = 272
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 8
    OnClick = Button2Click
  end
  object editOrderId: TEdit
    Left = 104
    Top = 8
    Width = 121
    Height = 21
    Color = clGrayText
    ReadOnly = True
    TabOrder = 9
  end
  object MonthCalendar1: TMonthCalendar
    Left = 105
    Top = 62
    Width = 309
    Height = 153
    AutoSize = True
    Date = 39967.8576676389
    TabOrder = 10
    Visible = False
    OnClick = MonthCalendar1Click
  end
  object Button3: TButton
    Left = 227
    Top = 40
    Width = 25
    Height = 21
    Caption = '...'
    TabOrder = 11
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 475
    Top = 40
    Width = 25
    Height = 21
    Caption = '...'
    TabOrder = 12
    OnClick = Button4Click
  end
end
