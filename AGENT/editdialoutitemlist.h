//---------------------------------------------------------------------------

#ifndef editdialoutitemlistH
#define editdialoutitemlistH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditDialOutItemList : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editLabelCaption;
  TEdit *editItemNo;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  TEdit *editTaskName;
  TLabel *Label5;
  TLabel *Label6;
  TEdit *editItemURL;
  TMemo *memoItemDemo;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemNoChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditDialOutItemList(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CDialOutItemSet DialOutItemSet;
  void SetModifyType(bool isinsert);
  void SetItemData(CDialOutItemSet& dialoutitemset, AnsiString strTaskName, AnsiString strItemNo, AnsiString strItemName, AnsiString strItemURL, AnsiString strItemDemo);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditDialOutItemList *FormEditDialOutItemList;
//---------------------------------------------------------------------------
#endif
