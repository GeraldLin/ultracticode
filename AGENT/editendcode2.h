//---------------------------------------------------------------------------

#ifndef editendcode2H
#define editendcode2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditEndCode2 : public TForm
{
__published:	// IDE-managed Components
  TLabel *lbItemId;
  TLabel *lbItemName;
  TLabel *Label1;
  TEdit *editItemId;
  TEdit *editItemName;
  TButton *Button1;
  TButton *Button2;
  TEdit *editItemURL;
  TLabel *Label2;
  TComboBox *cbColor;
  TLabel *Label7;
  TCSpinEdit *cseDispOrder;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editItemIdChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbColorKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditEndCode2(TComponent* Owner);

  int EndCode1;
  bool InsertId; //true-表示是增加记录 false-表示修改记录
  void SetDispMsg(bool bInsertId, int nEndCode1, int nEndCode2, AnsiString strItemName, AnsiString strItemURL, AnsiString strColor, int nDispOrder);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditEndCode2 *FormEditEndCode2;
//---------------------------------------------------------------------------
#endif
