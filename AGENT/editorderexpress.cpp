//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editorderexpress.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditOrderExpress *FormEditOrderExpress;

//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormEditOrderExpress::TFormEditOrderExpress(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditOrderExpress::SetOrderExpData(COrders& orders)
{
  Orders = orders;
  edtOOrderId->Text = orders.OrderId;

  cbOExpressType->Text = dmAgent->ExpressTypeItemList.GetItemName(orders.ExpressType);
  edtOExpressNo->Text = orders.ExpressNo;
  edtOExpressTele->Text = orders.ExpressTele;
  edtOExpressCorp->Text = orders.ExpressCorp;
  edtORecvName->Text = orders.RecvName;
  edtORecvTele->Text = orders.RecvTele;
  edtORecvPost->Text = orders.RecvPost;
  edtORecvAddr->Text = orders.RecvAddr;
  edtORemark->Text = orders.Remark;
  cbOOrderState->Text = dmAgent->OrderStateItemList.GetItemName(orders.OrderState);

  Button1->Enabled = false;
}
int TFormEditOrderExpress::GetOrderExpData()
{
  Orders.ExpressType = dmAgent->ExpressTypeItemList.GetItemValue(cbOExpressType->Text);
  Orders.ExpressNo = edtOExpressNo->Text;
  Orders.ExpressTele = edtOExpressTele->Text;
  Orders.ExpressCorp = edtOExpressCorp->Text;
  Orders.RecvName = edtORecvName->Text;
  Orders.RecvTele = edtORecvTele->Text;
  Orders.RecvPost = edtORecvPost->Text;
  Orders.RecvAddr = edtORecvAddr->Text;
  Orders.Remark = edtORemark->Text;
  Orders.OrderState = dmAgent->OrderStateItemList.GetItemValue(cbOOrderState->Text);
  return 0;
}
void __fastcall TFormEditOrderExpress::cbOExpressTypeKeyPress(
      TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderExpress::cbOExpressTypeChange(
      TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderExpress::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderExpress::FormCreate(TObject *Sender)
{
  dmAgent->AddCmbItem(cbOExpressType, dmAgent->ExpressTypeItemList, "");
  dmAgent->AddCmbItem(cbOOrderState, dmAgent->OrderStateItemList, "");  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditOrderExpress::Button1Click(TObject *Sender)
{
  if (GetOrderExpData() != 0)
    return;

  if (dmAgent->UpdateOrderExpRecord(&Orders) == 0)
  {
    dmAgent->InsertOPLog(MySeat, MyWorker, 1031, "話務員修改訂單：%s的快遞發貨情況記錄", Orders.OrderId.c_str());
    MessageBox(NULL,"修改訂單快遞發貨情況成功!","信息提示",MB_OK|MB_ICONINFORMATION);
    dmAgent->adoQryOrder->Close();
    dmAgent->adoQryOrder->Open();
    this->Close();
  }
  else
  {
    MessageBox(NULL,"修改訂單快遞發貨情況失敗!","信息提示",MB_OK|MB_ICONERROR);
  }
}
//---------------------------------------------------------------------------
