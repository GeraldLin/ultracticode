//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editworktimeset.h"
#include "main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormEditWorkTimeSet *FormEditWorkTimeSet;
//---------------------------------------------------------------------------
__fastcall TFormEditWorkTimeSet::TFormEditWorkTimeSet(TComponent* Owner)
  : TForm(Owner)
{
  dtpStartDate->Date = Date();
  dtpEndDate->Date = Date();
}
//---------------------------------------------------------------------------
void TFormEditWorkTimeSet::SetModifyType(bool isinsert)
{
  InsertId = isinsert;
  if (InsertId == true)
  {
    Button1->Caption = "增加";
  }
  else
  {
    Button1->Caption = "修改";
  }
}
void TFormEditWorkTimeSet::SetWorkTimeSetData(CWorkTimeSet& worktimeset)
{
  WorkTimeSet = worktimeset;

  editCTID->Text = WorkTimeSet.CTID;
  editHotLine->Text = WorkTimeSet.HotLine;
  switch (WorkTimeSet.DayType)
  {
  case 1:
    cbDayType->Text = "每周某天";

    cbStartWeek->Visible = true;
    cbEndWeek->Visible = true;
    if (WorkTimeSet.StartDay == "0")
      cbStartWeek->Text = "星期日";
    else if (WorkTimeSet.StartDay == "1")
      cbStartWeek->Text = "星期一";
    else if (WorkTimeSet.StartDay == "2")
      cbStartWeek->Text = "星期二";
    else if (WorkTimeSet.StartDay == "3")
      cbStartWeek->Text = "星期三";
    else if (WorkTimeSet.StartDay == "4")
      cbStartWeek->Text = "星期四";
    else if (WorkTimeSet.StartDay == "5")
      cbStartWeek->Text = "星期五";
    else if (WorkTimeSet.StartDay == "6")
      cbStartWeek->Text = "星期六";

    if (WorkTimeSet.EndDay == "0")
      cbEndWeek->Text = "星期日";
    else if (WorkTimeSet.EndDay == "1")
      cbEndWeek->Text = "星期一";
    else if (WorkTimeSet.EndDay == "2")
      cbEndWeek->Text = "星期二";
    else if (WorkTimeSet.EndDay == "3")
      cbEndWeek->Text = "星期三";
    else if (WorkTimeSet.EndDay == "4")
      cbEndWeek->Text = "星期四";
    else if (WorkTimeSet.EndDay == "5")
      cbEndWeek->Text = "星期五";
    else if (WorkTimeSet.EndDay == "6")
      cbEndWeek->Text = "星期六";

    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
    break;
  case 2:
    cbDayType->Text = "每月某天";

    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = true;
    cbEndDay->Visible = true;
    cbStartDay->Text = "每月"+WorkTimeSet.StartDay+"日";
    cbEndDay->Text = "每月"+WorkTimeSet.EndDay+"日";

    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
    break;
  case 3:
    cbDayType->Text = "具體某天";

    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = true;
    dtpEndDate->Visible = true;

    try
    {
      dtpStartDate->Date = StrToDate(WorkTimeSet.StartDay);
      dtpEndDate->Date = StrToDate(WorkTimeSet.EndDay);
    }
    catch (...)
    {
      try
      {
        dtpStartDate->Date = MyStrToDateEx(WorkTimeSet.StartDay);
        dtpEndDate->Date = MyStrToDateEx(WorkTimeSet.EndDay);
      }
      catch (...)
      {

      }
    }
    break;
  }
  dtpAMStartTime->Time = StrToDateTime(WorkTimeSet.StartTime1+":00");
  dtpAMEndTime->Time = StrToDateTime(WorkTimeSet.EndTime1+":59");
  dtpPMStartTime->Time = StrToDateTime(WorkTimeSet.StartTime2+":00");
  dtpPMEndTime->Time = StrToDateTime(WorkTimeSet.EndTime2+":59");

  switch (WorkTimeSet.WorkType)
  {
  case 0:
    cbWorkType->Text = "下班時間";
    break;
  case 1:
    cbWorkType->Text = "上班時間";
    break;
  case 2:
    cbWorkType->Text = "放假時間";
    break;
  case 3:
    cbWorkType->Text = "午休時間";
    break;
  case 4:
    cbWorkType->Text = "值班時間";
    break;
  case 5:
    cbWorkType->Text = "晚餐時間";
    break;
  }

  if (WorkTimeSet.OpenFlag == 0)
    cbOpenFlag->Text = "停用";
  else
    cbOpenFlag->Text = "啟用";

  Button1->Enabled = false;
}
int TFormEditWorkTimeSet::GetWorkTimeSetData()
{
  if (editCTID->Text == "")
    editCTID->Text = "*";
  WorkTimeSet.CTID = editCTID->Text;
  if (editHotLine->Text == "")
    editHotLine->Text = "*";
  WorkTimeSet.HotLine = editHotLine->Text;
  if (cbDayType->Text == "每周某天")
  {
    WorkTimeSet.DayType = 1;
    if (cbStartWeek->Text == "星期日")
      WorkTimeSet.StartDay = "0";
    else if (cbStartWeek->Text == "星期一")
      WorkTimeSet.StartDay = "1";
    else if (cbStartWeek->Text == "星期二")
      WorkTimeSet.StartDay = "2";
    else if (cbStartWeek->Text == "星期三")
      WorkTimeSet.StartDay = "3";
    else if (cbStartWeek->Text == "星期四")
      WorkTimeSet.StartDay = "4";
    else if (cbStartWeek->Text == "星期五")
      WorkTimeSet.StartDay = "5";
    else if (cbStartWeek->Text == "星期六")
      WorkTimeSet.StartDay = "6";
    if (cbEndWeek->Text == "星期日")
      WorkTimeSet.EndDay = "0";
    else if (cbEndWeek->Text == "星期一")
      WorkTimeSet.EndDay = "1";
    else if (cbEndWeek->Text == "星期二")
      WorkTimeSet.EndDay = "2";
    else if (cbEndWeek->Text == "星期三")
      WorkTimeSet.EndDay = "3";
    else if (cbEndWeek->Text == "星期四")
      WorkTimeSet.EndDay = "4";
    else if (cbEndWeek->Text == "星期五")
      WorkTimeSet.EndDay = "5";
    else if (cbEndWeek->Text == "星期六")
      WorkTimeSet.EndDay = "6";
  }
  else if (cbDayType->Text == "每月某天")
  {
    WorkTimeSet.DayType = 2;
    if (cbStartDay->Text.Length() == 7)
      WorkTimeSet.StartDay = cbStartDay->Text.SubString(5,1);
    else
      WorkTimeSet.StartDay = cbStartDay->Text.SubString(5,2);
    if (cbEndDay->Text.Length() == 7)
      WorkTimeSet.EndDay = cbEndDay->Text.SubString(5,1);
    else
      WorkTimeSet.EndDay = cbEndDay->Text.SubString(5,2);
  }
  else if (cbDayType->Text == "具體某天")
  {
    WorkTimeSet.DayType = 3;
    WorkTimeSet.StartDay = dtpStartDate->Date.FormatString("yyyy-mm-dd");
    WorkTimeSet.EndDay = dtpEndDate->Date.FormatString("yyyy-mm-dd");
  }
  WorkTimeSet.StartTime1 = dtpAMStartTime->Date.FormatString("hh:nn");
  WorkTimeSet.EndTime1 = dtpAMEndTime->Date.FormatString("hh:nn");
  WorkTimeSet.StartTime2 = dtpPMStartTime->Date.FormatString("hh:nn");
  WorkTimeSet.EndTime2 = dtpPMEndTime->Date.FormatString("hh:nn");

  if (cbWorkType->Text == "上班時間")
    WorkTimeSet.WorkType = 1;
  else if (cbWorkType->Text == "下班時間")
    WorkTimeSet.WorkType = 0;
  else if (cbWorkType->Text == "放假時間")
    WorkTimeSet.WorkType = 2;
  else if (cbWorkType->Text == "午休時間")
    WorkTimeSet.WorkType = 3;
  else if (cbWorkType->Text == "值班時間")
    WorkTimeSet.WorkType = 4;
  else if (cbWorkType->Text == "晚餐時間")
    WorkTimeSet.WorkType = 5;

  if (cbOpenFlag->Text == "啟用")
    WorkTimeSet.OpenFlag = 1;
  else
    WorkTimeSet.OpenFlag = 0;
  return 0;
}
void TFormEditWorkTimeSet::ClearWorkTimeSetData()
{
}

void __fastcall TFormEditWorkTimeSet::cbDayTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditWorkTimeSet::editHotLineChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormEditWorkTimeSet::cbDayTypeChange(TObject *Sender)
{
  Button1->Enabled = true;
  if (cbDayType->Text == "每周某天")
  {
    cbStartWeek->Visible = true;
    cbEndWeek->Visible = true;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
  }
  else if (cbDayType->Text == "每月某天")
  {
    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = true;
    cbEndDay->Visible = true;
    dtpStartDate->Visible = false;
    dtpEndDate->Visible = false;
  }
  else if (cbDayType->Text == "具體某天")
  {
    cbStartWeek->Visible = false;
    cbEndWeek->Visible = false;
    cbStartDay->Visible = false;
    cbEndDay->Visible = false;
    dtpStartDate->Visible = true;
    dtpEndDate->Visible = true;
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormEditWorkTimeSet::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormEditWorkTimeSet::Button1Click(TObject *Sender)
{
  if (GetWorkTimeSetData() != 0)
    return;
  if (InsertId == true)
  {
    if (dmAgent->InsertWorkTimeSetRecord(&WorkTimeSet) == 0)
    {
      //MessageBox(NULL,"增加值班時間記錄成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryWorkTimeSet->Close();
      dmAgent->adoQryWorkTimeSet->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加值班時間記錄失敗!","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateWorkTimeSetRecord(&WorkTimeSet) == 0)
    {
      //MessageBox(NULL,"修改值班時間記錄成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      dmAgent->adoQryWorkTimeSet->Close();
      dmAgent->adoQryWorkTimeSet->Open();
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改值班時間記錄失敗!","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------

