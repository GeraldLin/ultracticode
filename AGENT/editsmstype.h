//---------------------------------------------------------------------------

#ifndef editsmstypeH
#define editsmstypeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormEditSmsType : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editSmsTypeId;
  TEdit *editSmsTypeName;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSmsTypeIdChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSmsType(TComponent* Owner);

  int SmsType; //1-接收 2-发送
  bool InsertId; //true-表示是增加记录 false-表示修改记录

  void SetSmsTypeData(int nSmsType, bool bInsertId, AnsiString strSmsTypeId, AnsiString strSmsTypeName);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSmsType *FormEditSmsType;
//---------------------------------------------------------------------------
#endif
