//---------------------------------------------------------------------------

#ifndef editcustexsetH
#define editcustexsetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditCustExSet : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TEdit *editFieldId;
  TComboBox *cbControlType;
  TEdit *editLabelCaption;
  TEdit *editDefaultData;
  TCSpinEdit *cseMinValue;
  TCSpinEdit *cseMaxValue;
  TCheckBox *ckIsAllowNull;
  TCheckBox *ckDispId;
  TCheckBox *ckInputId;
  TCheckBox *ckIsUsed;
  TCheckBox *ckSearchId;
  TButton *Button1;
  TButton *Button2;
  TCheckBox *ckExportId;
  TCheckBox *ckCanEdit;
  TLabel *Label7;
  TCSpinEdit *cseExportOrderId;
  TLabel *Label8;
  TCSpinEdit *cseAcceptExId;
  TLabel *Label9;
  TComboBox *cbReadDataType;
  TLabel *Label10;
  TEdit *editReadFieldTag;
  TCheckBox *ckSaveToDBFlag;
  TLabel *Label13;
  TComboBox *cbWith;
  void __fastcall cbControlTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editFieldIdChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall cbControlTypeChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditCustExSet(TComponent* Owner);

  bool InsertId; //true-表示是增加记录 false-表示修改记录
  CCustExItemSet CustExItemSet;
  void SetModifyType(bool isinsert);
  void SetCustExItemSetData(CCustExItemSet& custexitemset);
  int GetCustExItemSetData();
  void ClearCustExtItemSetData();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditCustExSet *FormEditCustExSet;
//---------------------------------------------------------------------------
#endif
