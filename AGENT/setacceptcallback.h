//---------------------------------------------------------------------------

#ifndef setacceptcallbackH
#define setacceptcallbackH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFormSetAcceptCallBack : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TMonthCalendar *MonthCalendar1;
  TCSpinEdit *CSpinEdit1;
  TCSpinEdit *CSpinEdit2;
  TButton *Button1;
  TButton *Button2;
  TButton *Button3;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSetAcceptCallBack(TComponent* Owner);

  AnsiString GId;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSetAcceptCallBack *FormSetAcceptCallBack;
//---------------------------------------------------------------------------
#endif
