//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <inifiles.hpp>
#include "crmdbparam.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormCRMDBParam *FormCRMDBParam;
//---------------------------------------------------------------------------
__fastcall TFormCRMDBParam::TFormCRMDBParam(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormCRMDBParam::ReadIniFile()
{
  TIniFile *IniKey    = new TIniFile ( ExtractFilePath(Application->ExeName)+"agent.ini" );

  ckCRMDBParam->Checked = IniKey->ReadBool ( "CRMDATABASE", "OpenFlag", false );

  cbxDBType->Text = IniKey->ReadString ( "CRMDATABASE", "DBTYPE", "SQLSERVER" );
  edtServer->Text = IniKey->ReadString ( "CRMDATABASE", "DBSERVER", "127.0.0.1" );
  edtDB->Text = IniKey->ReadString ( "CRMDATABASE", "DATABASE", "callcenterdb" );
  edtUSER->Text = IniKey->ReadString ( "CRMDATABASE", "USERID", "sa" );
  edtPSW->Text = DecodePass(IniKey->ReadString ( "CRMDATABASE", "PASSWORD", "pebpyhns{khc_s" ));

  delete IniKey;
  btSaveParam->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormCRMDBParam::btCloseClick(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormCRMDBParam::cbxDBTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormCRMDBParam::cbxDBTypeChange(TObject *Sender)
{
  btSaveParam->Enabled = true;
  if (cbxDBType->Text == "ORACLE")
  {
    Label37->Caption = "TNS Service Name:";
    Label37->Visible = true;
    edtServer->Visible = true;

    Label16->Visible = false;
    edtDB->Visible = false;
  }
  else if (cbxDBType->Text == "ODBC")
  {
    Label37->Visible = false;
    edtServer->Visible = false;

    Label16->Caption = "DNS Name:";
    Label16->Visible = true;
    edtDB->Visible = true;
  }
  else
  {
    Label37->Caption = "資料庫伺服器IP位址:";
    Label37->Visible = true;
    edtServer->Visible = true;

    Label16->Caption = "資料庫名稱:";
    Label16->Visible = true;
    edtDB->Visible = true;
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormCRMDBParam::btSaveParamClick(TObject *Sender)
{
  if ( MessageBox( NULL, "您真的要修改第三方資料庫參數嗎？", "系統提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    TIniFile *IniKey    = new TIniFile ( ExtractFilePath(Application->ExeName)+"agent.ini" ) ;
    IniKey->WriteBool( "CRMDATABASE", "OpenFlag", ckCRMDBParam->Checked );

    IniKey->WriteString( "CRMDATABASE", "DBTYPE", cbxDBType->Text );
    IniKey->WriteString( "CRMDATABASE", "DBSERVER", edtServer->Text );
    IniKey->WriteString( "CRMDATABASE", "DATABASE", edtDB->Text );
    IniKey->WriteString( "CRMDATABASE", "USERID", edtUSER->Text );
    IniKey->WriteString( "CRMDATABASE", "PASSWORD", EncodePass(edtPSW->Text));

    delete IniKey;
    btSaveParam->Enabled = false;
    this->Close();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormCRMDBParam::edtServerChange(TObject *Sender)
{
  btSaveParam->Enabled = true;
}
//---------------------------------------------------------------------------

