//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editacceptprocid.h"
#include "DataModule.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAcceptProcId *FormAcceptProcId;
//---------------------------------------------------------------------------
__fastcall TFormAcceptProcId::TFormAcceptProcId(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormAcceptProcId::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormAcceptProcId::editAcceptProcIdChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormAcceptProcId::Button1Click(TObject *Sender)
{
  int nProcType;

  if (MyIsNumber(editAcceptProcId->Text) != 0)
  {
    MessageBox(NULL,"請輸入工單處理結果代碼！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }
  if (editAcceptProcName->Text == "")
  {
    MessageBox(NULL,"請輸入工單處理結果名稱！","信息提示",MB_OK|MB_ICONERROR);
    return;
  }

  nProcType = StrToInt(editAcceptProcId->Text);

  if (InsertId == true)
  {
    if (dmAgent->IsAcptProcTypeExist(nProcType) == 1)
    {
      MessageBox(NULL,"對不起，該工單處理結果編號已存在！","信息提示",MB_OK|MB_ICONERROR);
      return;
    }
    if (dmAgent->InsertAcptProcTypeRecord(nProcType, editAcceptProcName->Text) == 0)
    {
      dmAgent->adoQryAcceptProcType->Close();
      dmAgent->adoQryAcceptProcType->Open();
      dmAgent->UpdateAcptProcTypeListItems();
      MessageBox(NULL,"增加工單處理結果編號成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"增加工單處理結果編號失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
  else
  {
    if (dmAgent->UpdateAcptProcTypeRecord(nProcType, editAcceptProcName->Text) == 0)
    {
      dmAgent->adoQryAcceptProcType->Close();
      dmAgent->adoQryAcceptProcType->Open();
      dmAgent->UpdateAcptProcTypeListItems();
      MessageBox(NULL,"修改工單處理結果編號成功！","信息提示",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
    else
    {
      MessageBox(NULL,"修改工單處理結果編號失敗！","信息提示",MB_OK|MB_ICONERROR);
    }
  }
}
//---------------------------------------------------------------------------
