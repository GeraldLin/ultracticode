//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "worker.h"
#include "main.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormWorker *FormWorker;

extern int g_nBandSeatNoType;
//座席參數
extern CMySeat MySeat;
//話務員參數
extern CMyWorker MyWorker;

//---------------------------------------------------------------------------
__fastcall TFormWorker::TFormWorker(TComponent* Owner)
  : TForm(Owner)
{
  bPwdModify = false;
  InsertId = false;
  if (g_nBandSeatNoType == 0)
  {
    dmAgent->AddCmbItem(FormWorker->cbDepartmentID, dmAgent->DepartmentItemList, "");
    dmAgent->AddCmbItem(FormWorker->cmbWWorkType, dmAgent->WorkerRoleList, "");
    dmAgent->AddCmbItem(FormWorker->cmbWCallRight, dmAgent->CallRightItemList, "");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo1, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo2, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo3, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo4, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo5, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo6, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo7, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo8, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo9, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo10, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo11, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo12, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo13, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo14, dmAgent->WorkerGroupList, "不選擇");
    dmAgent->AddCmbItem(FormWorker->cbGroupNo15, dmAgent->WorkerGroupList, "不選擇");
  }
}
//---------------------------------------------------------------------------
void TFormWorker::SetWorker(CMyWorker *worker)
{
  tbId = worker->Id;
  edtWWorkerNo->Text = worker->WorkerNo; //話務員工號
  cmbWWorkType->Text = dmAgent->WorkerRoleList.GetItemName(worker->WorkType);
  cbDepartmentID->Text = dmAgent->DepartmentItemList.GetItemName(worker->DepartmentID);
  if (worker->GroupNo == 0)
    cbGroupNo->Text = "不選擇";
  else
    cbGroupNo->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo);

  if (worker->GroupNo1 == 0)
    cbGroupNo1->Text = "不選擇";
  else
    cbGroupNo1->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo1);

  if (worker->GroupNo2 == 0)
    cbGroupNo2->Text = "不選擇";
  else
    cbGroupNo2->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo2);

  if (worker->GroupNo3 == 0)
    cbGroupNo3->Text = "不選擇";
  else
    cbGroupNo3->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo3);

  if (worker->GroupNo4 == 0)
    cbGroupNo4->Text = "不選擇";
  else
    cbGroupNo4->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo4);

  if (worker->GroupNo5 == 0)
    cbGroupNo5->Text = "不選擇";
  else
    cbGroupNo5->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo5);

  if (worker->GroupNo6 == 0)
    cbGroupNo6->Text = "不選擇";
  else
    cbGroupNo6->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo6);

  if (worker->GroupNo7 == 0)
    cbGroupNo7->Text = "不選擇";
  else
    cbGroupNo7->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo7);

  if (worker->GroupNo8 == 0)
    cbGroupNo8->Text = "不選擇";
  else
    cbGroupNo8->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo8);

  if (worker->GroupNo9 == 0)
    cbGroupNo9->Text = "不選擇";
  else
    cbGroupNo9->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo9);

  if (worker->GroupNo10 == 0)
    cbGroupNo10->Text = "不選擇";
  else
    cbGroupNo10->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo10);

  if (worker->GroupNo11 == 0)
    cbGroupNo11->Text = "不選擇";
  else
    cbGroupNo11->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo11);

  if (worker->GroupNo12 == 0)
    cbGroupNo12->Text = "不選擇";
  else
    cbGroupNo12->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo12);

  if (worker->GroupNo13 == 0)
    cbGroupNo13->Text = "不選擇";
  else
    cbGroupNo13->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo13);

  if (worker->GroupNo14 == 0)
    cbGroupNo14->Text = "不選擇";
  else
    cbGroupNo14->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo14);

  if (worker->GroupNo15 == 0)
    cbGroupNo15->Text = "不選擇";
  else
    cbGroupNo15->Text = dmAgent->WorkerGroupList.GetItemName(worker->GroupNo15);

  cspWwLevel->Value = worker->wLevel; //技能級別
  cspWwLevel1->Value = worker->wLevel1; //技能級別
  cspWwLevel2->Value = worker->wLevel2; //技能級別
  cspWwLevel3->Value = worker->wLevel3; //技能級別
  cspWwLevel4->Value = worker->wLevel4; //技能級別
  cspWwLevel5->Value = worker->wLevel5; //技能級別
  cspWwLevel6->Value = worker->wLevel6; //技能級別
  cspWwLevel7->Value = worker->wLevel7; //技能級別
  cspWwLevel8->Value = worker->wLevel8; //技能級別
  cspWwLevel9->Value = worker->wLevel9; //技能級別
  cspWwLevel10->Value = worker->wLevel10; //技能級別
  cspWwLevel11->Value = worker->wLevel11; //技能級別
  cspWwLevel12->Value = worker->wLevel12; //技能級別
  cspWwLevel13->Value = worker->wLevel13; //技能級別
  cspWwLevel14->Value = worker->wLevel14; //技能級別
  cspWwLevel15->Value = worker->wLevel15; //技能級別

  edtSWTGroupID->Text = worker->SWTGroupID;
  edtSWTGroupPWD->Text = worker->SWTGroupPWD;

  cmbWCallRight->Text = dmAgent->CallRightItemList.GetItemName(worker->CallRight);
  edtWPassword->Text = worker->Password; //登錄密碼
  edtWWorkerName->Text =  worker->WorkerName; //話務員姓名
  edtWTele->Text =  worker->Tele; //聯系電話
  edtWMobile->Text =  worker->Mobile; //手機號碼
  edtWAddress->Text =  worker->Address; //住址
  edtWEmail->Text =  worker->Email; //郵箱
  edtSubPhone->Text =  worker->SubPhone;
  edtEmployeeID->Text =  worker->EmployeeID;

  if (GetBit(worker->HideCallerNo, 0) == 0)
    CheckBox1->Checked = false;
  else
    CheckBox1->Checked = true;
  if (GetBit(worker->HideCallerNo, 1) == 0)
    CheckBox2->Checked = false;
  else
    CheckBox2->Checked = true;
  if (GetBit(worker->HideCallerNo, 2) == 0)
    CheckBox3->Checked = false;
  else
    CheckBox3->Checked = true;
  if (GetBit(worker->HideCallerNo, 3) == 0)
    CheckBox4->Checked = false;
  else
    CheckBox4->Checked = true;

  if (worker->AutoAnswerId == 1)
    ckAutoAnswerId->Checked = true;
  else
    ckAutoAnswerId->Checked = false;

  if (worker->ProcDialoutNextType == 1)
    cbProcDialoutNextType->Text = "鎖定";
  else
    cbProcDialoutNextType->Text = "不鎖定";
  bPwdModify = false;
  Button1->Enabled = false;
}

int TFormWorker::GetWorker(CMyWorker *worker)
{
  worker->Id = tbId;
  if (edtWWorkerNo->Text == "")
  {
    MessageBox(NULL,"請輸入值機編號！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  if (MyIsNumber(edtWWorkerNo->Text) == 1) //話務員工號
  {
    MessageBox(NULL,"值機編號必須為數字！","訊息提示",MB_OK|MB_ICONERROR);
    return 1;
  }
  worker->WorkerNo = edtWWorkerNo->Text;
  worker->WorkType = dmAgent->WorkerRoleList.GetItemValue(cmbWWorkType->Text);
  worker->DepartmentID = dmAgent->DepartmentItemList.GetItemValue(cbDepartmentID->Text);

  worker->GroupNo = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo->Text);
  if (worker->GroupNo < 0 || worker->GroupNo > 31)
    worker->GroupNo = 0;

  worker->GroupNo1 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo1->Text);
  if (worker->GroupNo1 < 0 || worker->GroupNo1 > 31)
    worker->GroupNo1 = 0;

  worker->GroupNo2 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo2->Text);
  if (worker->GroupNo2 < 0 || worker->GroupNo2 > 31)
    worker->GroupNo2 = 0;

  worker->GroupNo3 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo3->Text);
  if (worker->GroupNo3 < 0 || worker->GroupNo3 > 31)
    worker->GroupNo3 = 0;

  worker->GroupNo4 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo4->Text);
  if (worker->GroupNo4 < 0 || worker->GroupNo4 > 31)
    worker->GroupNo4 = 0;

  worker->GroupNo5 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo5->Text);
  if (worker->GroupNo5 < 0 || worker->GroupNo5 > 31)
    worker->GroupNo5 = 0;

  worker->GroupNo6 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo6->Text);
  if (worker->GroupNo6 < 0 || worker->GroupNo6 > 31)
    worker->GroupNo6 = 0;

  worker->GroupNo7 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo7->Text);
  if (worker->GroupNo7 < 0 || worker->GroupNo7 > 31)
    worker->GroupNo7 = 0;

  worker->GroupNo8 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo8->Text);
  if (worker->GroupNo8 < 0 || worker->GroupNo8 > 31)
    worker->GroupNo8 = 0;

  worker->GroupNo9 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo9->Text);
  if (worker->GroupNo9 < 0 || worker->GroupNo9 > 31)
    worker->GroupNo9 = 0;

  worker->GroupNo10 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo10->Text);
  if (worker->GroupNo10 < 0 || worker->GroupNo10 > 31)
    worker->GroupNo10 = 0;

  worker->GroupNo11 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo11->Text);
  if (worker->GroupNo11 < 0 || worker->GroupNo11 > 31)
    worker->GroupNo11 = 0;

  worker->GroupNo12 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo12->Text);
  if (worker->GroupNo12 < 0 || worker->GroupNo12 > 31)
    worker->GroupNo12 = 0;

  worker->GroupNo13 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo13->Text);
  if (worker->GroupNo13 < 0 || worker->GroupNo13 > 31)
    worker->GroupNo13 = 0;

  worker->GroupNo14 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo14->Text);
  if (worker->GroupNo14 < 0 || worker->GroupNo14 > 31)
    worker->GroupNo14 = 0;

  worker->GroupNo15 = dmAgent->WorkerGroupList.GetItemValue(cbGroupNo15->Text);
  if (worker->GroupNo15 < 0 || worker->GroupNo15 > 31)
    worker->GroupNo15 = 0;

  worker->SWTGroupID = edtSWTGroupID->Text;
  worker->SWTGroupPWD = edtSWTGroupPWD->Text;

  worker->wLevel = cspWwLevel->Value; //技能級別
  worker->wLevel1 = cspWwLevel1->Value; //技能級別
  worker->wLevel2 = cspWwLevel2->Value; //技能級別
  worker->wLevel3 = cspWwLevel3->Value; //技能級別
  worker->wLevel4 = cspWwLevel4->Value; //技能級別
  worker->wLevel5 = cspWwLevel5->Value; //技能級別
  worker->wLevel6 = cspWwLevel6->Value; //技能級別
  worker->wLevel7 = cspWwLevel7->Value; //技能級別
  worker->wLevel8 = cspWwLevel8->Value; //技能級別
  worker->wLevel9 = cspWwLevel9->Value; //技能級別
  worker->wLevel10 = cspWwLevel10->Value; //技能級別
  worker->wLevel11 = cspWwLevel11->Value; //技能級別
  worker->wLevel12 = cspWwLevel12->Value; //技能級別
  worker->wLevel13 = cspWwLevel13->Value; //技能級別
  worker->wLevel14 = cspWwLevel14->Value; //技能級別
  worker->wLevel15 = cspWwLevel15->Value; //技能級別

  //呼出權限 0-接聽電話 1-撥打內部電話 2-撥打本地市話 3-：撥打本地手機 4-撥打國內長途 5-撥打國際長途
  worker->CallRight = dmAgent->CallRightItemList.GetItemValue(cmbWCallRight->Text);
  if (worker->Password != edtWPassword->Text)
    bPwdModify = true;
  worker->Password = edtWPassword->Text; //登錄密碼
  worker->WorkerName = edtWWorkerName->Text; //話務員姓名
  worker->Tele = edtWTele->Text; //聯系電話
  worker->Mobile = edtWMobile->Text; //手機號碼
  worker->Address = edtWAddress->Text; //住址
  worker->Email = edtWEmail->Text; //郵箱
  worker->SubPhone =  edtSubPhone->Text;
  worker->EmployeeID =  edtEmployeeID->Text;

  if (ckAutoAnswerId->Checked == true)
    worker->AutoAnswerId = 1;
  else
    worker->AutoAnswerId = 0;

  if (cbProcDialoutNextType->Text == "鎖定")
    worker->ProcDialoutNextType = 1;
  else
    worker->ProcDialoutNextType = 0;

  worker->HideCallerNo = 0;
  if (CheckBox1->Checked == true)
    worker->HideCallerNo |= 1;
  if (CheckBox2->Checked == true)
    worker->HideCallerNo |= 2;
  if (CheckBox3->Checked == true)
    worker->HideCallerNo |= 4;
  if (CheckBox4->Checked == true)
    worker->HideCallerNo |= 8;

  return 0;
}
void TFormWorker::SetAddEnable()
{
  edtWWorkerNo->Enabled = true;
  cmbWWorkType->Enabled = true;
  cbDepartmentID->Enabled = true;
  cbGroupNo->Enabled = true;
  cbGroupNo1->Enabled = true;
  cbGroupNo2->Enabled = true;
  cbGroupNo3->Enabled = true;
  cbGroupNo4->Enabled = true;
  cbGroupNo5->Enabled = true;
  cbGroupNo6->Enabled = true;
  cbGroupNo7->Enabled = true;
  cbGroupNo8->Enabled = true;
  cbGroupNo9->Enabled = true;
  cbGroupNo10->Enabled = true;
  cbGroupNo11->Enabled = true;
  cbGroupNo12->Enabled = true;
  cbGroupNo13->Enabled = true;
  cbGroupNo14->Enabled = true;
  cbGroupNo15->Enabled = true;
  edtSWTGroupID->Enabled = true;
  edtSWTGroupPWD->Enabled = true;

  cspWwLevel->Enabled = true;
  cspWwLevel1->Enabled = true;
  cspWwLevel2->Enabled = true;
  cspWwLevel3->Enabled = true;
  cspWwLevel4->Enabled = true;
  cspWwLevel5->Enabled = true;
  cspWwLevel6->Enabled = true;
  cspWwLevel7->Enabled = true;
  cspWwLevel8->Enabled = true;
  cspWwLevel9->Enabled = true;
  cspWwLevel10->Enabled = true;
  cspWwLevel11->Enabled = true;
  cspWwLevel12->Enabled = true;
  cspWwLevel13->Enabled = true;
  cspWwLevel14->Enabled = true;
  cspWwLevel15->Enabled = true;

  cmbWCallRight->Enabled = true;
  edtWPassword->Enabled = true;
  edtWWorkerName->Enabled = true;
  edtWTele->Enabled = true;
  edtWMobile->Enabled = true;
  edtWAddress->Enabled = true;
  edtWEmail->Enabled = true;
  edtEmployeeID->Enabled = true;
  CheckBox1->Enabled = true;
  CheckBox2->Enabled = true;
  CheckBox3->Enabled = true;
  CheckBox4->Enabled = true;

  ckAutoAnswerId->Enabled = true;
  cbProcDialoutNextType->Enabled = true;
}
void TFormWorker::SetAllEnable()
{
  edtWWorkerNo->Enabled = false;
  cmbWWorkType->Enabled = true;
  cbDepartmentID->Enabled = true;
  cbGroupNo->Enabled = true;
  cbGroupNo1->Enabled = true;
  cbGroupNo2->Enabled = true;
  cbGroupNo3->Enabled = true;
  cbGroupNo4->Enabled = true;
  cbGroupNo5->Enabled = true;
  cbGroupNo6->Enabled = true;
  cbGroupNo7->Enabled = true;
  cbGroupNo8->Enabled = true;
  cbGroupNo9->Enabled = true;
  cbGroupNo10->Enabled = true;
  cbGroupNo11->Enabled = true;
  cbGroupNo12->Enabled = true;
  cbGroupNo13->Enabled = true;
  cbGroupNo14->Enabled = true;
  cbGroupNo15->Enabled = true;
  edtSWTGroupID->Enabled = true;
  edtSWTGroupPWD->Enabled = true;

  cspWwLevel->Enabled = true;
  cspWwLevel1->Enabled = true;
  cspWwLevel2->Enabled = true;
  cspWwLevel3->Enabled = true;
  cspWwLevel4->Enabled = true;
  cspWwLevel5->Enabled = true;
  cspWwLevel6->Enabled = true;
  cspWwLevel7->Enabled = true;
  cspWwLevel8->Enabled = true;
  cspWwLevel9->Enabled = true;
  cspWwLevel10->Enabled = true;
  cspWwLevel11->Enabled = true;
  cspWwLevel12->Enabled = true;
  cspWwLevel13->Enabled = true;
  cspWwLevel14->Enabled = true;
  cspWwLevel15->Enabled = true;

  cmbWCallRight->Enabled = true;
  edtWPassword->Enabled = true;
  edtWWorkerName->Enabled = true;
  edtWTele->Enabled = true;
  edtWMobile->Enabled = true;
  edtWAddress->Enabled = true;
  edtWEmail->Enabled = true;
  edtEmployeeID->Enabled = true;
  CheckBox1->Enabled = true;
  CheckBox2->Enabled = true;
  CheckBox3->Enabled = true;
  CheckBox4->Enabled = true;

  ckAutoAnswerId->Enabled = true;
  cbProcDialoutNextType->Enabled = true;
}

void TFormWorker::SetMyEnable()
{
  edtWWorkerNo->Enabled = false;
  cmbWWorkType->Enabled = false;
  cbDepartmentID->Enabled = false;
  cbGroupNo->Enabled = false;
  cbGroupNo1->Enabled = false;
  cbGroupNo2->Enabled = false;
  cbGroupNo3->Enabled = false;
  cbGroupNo4->Enabled = false;
  cbGroupNo5->Enabled = false;
  cbGroupNo6->Enabled = false;
  cbGroupNo7->Enabled = false;
  cbGroupNo8->Enabled = false;
  cbGroupNo9->Enabled = false;
  cbGroupNo10->Enabled = false;
  cbGroupNo11->Enabled = false;
  cbGroupNo12->Enabled = false;
  cbGroupNo13->Enabled = false;
  cbGroupNo14->Enabled = false;
  cbGroupNo15->Enabled = false;
  edtSWTGroupID->Enabled = false;
  edtSWTGroupPWD->Enabled = false;

  cspWwLevel->Enabled = false;
  cspWwLevel1->Enabled = false;
  cspWwLevel2->Enabled = false;
  cspWwLevel3->Enabled = false;
  cspWwLevel4->Enabled = false;
  cspWwLevel5->Enabled = false;
  cspWwLevel6->Enabled = false;
  cspWwLevel7->Enabled = false;
  cspWwLevel8->Enabled = false;
  cspWwLevel9->Enabled = false;
  cspWwLevel10->Enabled = false;
  cspWwLevel11->Enabled = false;
  cspWwLevel12->Enabled = false;
  cspWwLevel13->Enabled = false;
  cspWwLevel14->Enabled = false;
  cspWwLevel15->Enabled = false;

  cmbWCallRight->Enabled = false;
  edtWPassword->Enabled = true;
  edtWWorkerName->Enabled = false;
  edtWTele->Enabled = true;
  edtWMobile->Enabled = true;
  edtWAddress->Enabled = true;
  edtWEmail->Enabled = true;
  edtEmployeeID->Enabled = true;
  CheckBox1->Enabled = false;
  CheckBox2->Enabled = false;
  CheckBox3->Enabled = false;
  CheckBox4->Enabled = false;

  ckAutoAnswerId->Enabled = true;
  cbProcDialoutNextType->Enabled = false;
}

void TFormWorker::SetDispEnable()
{
  edtWWorkerNo->Enabled = false;
  cmbWWorkType->Enabled = false;
  cbDepartmentID->Enabled = false;
  cbGroupNo->Enabled = false;
  cbGroupNo1->Enabled = false;
  cbGroupNo2->Enabled = false;
  cbGroupNo3->Enabled = false;
  cbGroupNo4->Enabled = false;
  cbGroupNo5->Enabled = false;
  cbGroupNo6->Enabled = false;
  cbGroupNo7->Enabled = false;
  cbGroupNo8->Enabled = false;
  cbGroupNo9->Enabled = false;
  cbGroupNo10->Enabled = false;
  cbGroupNo11->Enabled = false;
  cbGroupNo12->Enabled = false;
  cbGroupNo13->Enabled = false;
  cbGroupNo14->Enabled = false;
  cbGroupNo15->Enabled = false;
  edtSWTGroupID->Enabled = false;
  edtSWTGroupPWD->Enabled = false;

  cspWwLevel->Enabled = false;
  cspWwLevel1->Enabled = false;
  cspWwLevel2->Enabled = false;
  cspWwLevel3->Enabled = false;
  cspWwLevel4->Enabled = false;
  cspWwLevel5->Enabled = false;
  cspWwLevel6->Enabled = false;
  cspWwLevel7->Enabled = false;
  cspWwLevel8->Enabled = false;
  cspWwLevel9->Enabled = false;
  cspWwLevel10->Enabled = false;
  cspWwLevel11->Enabled = false;
  cspWwLevel12->Enabled = false;
  cspWwLevel13->Enabled = false;
  cspWwLevel14->Enabled = false;
  cspWwLevel15->Enabled = false;

  cmbWCallRight->Enabled = false;
  edtWPassword->Enabled = false;
  edtWWorkerName->Enabled = false;
  edtWTele->Enabled = false;
  edtWMobile->Enabled = false;
  edtWAddress->Enabled = false;
  edtWEmail->Enabled = false;
  edtEmployeeID->Enabled = false;
  CheckBox1->Enabled = false;
  CheckBox2->Enabled = false;
  CheckBox3->Enabled = false;
  CheckBox4->Enabled = false;

  ckAutoAnswerId->Enabled = false;
  cbProcDialoutNextType->Enabled = false;
}

void __fastcall TFormWorker::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormWorker::Button1Click(TObject *Sender)
{
  CMyWorker worker;
  if (GetWorker(&worker) == 0)
  {
    if (InsertId == false)
    {
      dmAgent->UpdateWorker(worker, bPwdModify);
      if (cmbWWorkType->Enabled == false)
        dmAgent->InsertOPLog(MySeat, MyWorker, 1018, "值機員修改本人資料");
      else
        dmAgent->InsertOPLog(MySeat, MyWorker, 1020, "班長修改值機編號=%s的資料", worker.WorkerNo.c_str());
    }
    else
    {
      if (dmAgent->IsWorkerNoExist(worker.WorkerNo) == 1)
      {
        MessageBox(NULL,"該值機編號已存在！","訊息提示",MB_OK|MB_ICONERROR);
        return;
      }
      dmAgent->InsertWorker(worker);
      dmAgent->InsertOPLog(MySeat, MyWorker, 1019, "班長新增值機編號=%s的資料", worker.WorkerNo.c_str());
    }
    dmAgent->UpdateWorkerGroupListItems();
    dmAgent->adotbWorker->Close();
    dmAgent->adotbWorker->Open();
  }
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormWorker::edtWWorkerNoChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormWorker::cmbWWorkTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------


void __fastcall TFormWorker::FormShow(TObject *Sender)
{
  if (InsertId == true)
    FormWorker->edtWWorkerNo->SetFocus();
  else
    FormWorker->edtWWorkerName->SetFocus();
}
//---------------------------------------------------------------------------

