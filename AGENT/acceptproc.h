//---------------------------------------------------------------------------

#ifndef acceptprocH
#define acceptprocH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFormAcceptProc : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel1;
  TLabel *Label1;
  TEdit *editCustName;
  TLabel *Label2;
  TEdit *editCustomTel;
  TGroupBox *GroupBox1;
  TDBGrid *DBGrid1;
  TPanel *Panel2;
  TButton *Button2;
  TMemo *memoAcceptProc;
  TButton *Button1;
  TLabel *Label4;
  TComboBox *cbNextProcType;
  TLabel *Label5;
  TComboBox *cbNextProcSel;
  TLabel *Label3;
  TComboBox *cmbDealState;
  TButton *Button4;
  TLabel *Label6;
  TComboBox *cbNextProcResult;
  TPopupMenu *PopupMenu1;
  TMenuItem *N1;
  TMenuItem *N2;
  TMenuItem *N3;
  TMenuItem *N4;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall DBGrid1CellClick(TColumn *Column);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall memoAcceptProcChange(TObject *Sender);
  void __fastcall Button4Click(TObject *Sender);
  void __fastcall cmbDealStateChange(TObject *Sender);
  void __fastcall cmbDealStateKeyPress(TObject *Sender, char &Key);
  void __fastcall cbNextProcTypeChange(TObject *Sender);
  void __fastcall cbNextProcSelChange(TObject *Sender);
  void __fastcall N4Click(TObject *Sender);
  void __fastcall N1Click(TObject *Sender);
  void __fastcall N2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAcceptProc(TComponent* Owner);

  bool InsertId;
  int ProcId;
  AnsiString GID;
  void SetAcceptdata(AnsiString gid, AnsiString custname, AnsiString customtel);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAcceptProc *FormAcceptProc;
//---------------------------------------------------------------------------
#endif
