//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "disprecvmsg.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormRecvMsg *FormRecvMsg;
extern BSTR bEmpty;
//---------------------------------------------------------------------------
__fastcall TFormRecvMsg::TFormRecvMsg(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormRecvMsg::SetMsgData(AnsiString seatno, AnsiString workerno, AnsiString workername, AnsiString msg)
{
  editSeatNo->Text = seatno;
  editWorkerNo->Text = workerno;
  editWorkerName->Text = workername;
  editMsg->Text = msg;
  editSendMsg->Text = "已收到";
}
void __fastcall TFormRecvMsg::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormRecvMsg::Button1Click(TObject *Sender)
{
  AnsiString SeatNo;
  WideString wSeatNo, wMsg;
  BSTR bSeatNo, bMsg;

  SeatNo = editSeatNo->Text;
  if (SeatNo.Length() > 0 && editSendMsg->Text.Length() > 0)
  {
    wSeatNo = WideString(SeatNo);
    bSeatNo = ( wchar_t * )wSeatNo;

    wMsg = WideString(editSendMsg->Text);
    bMsg = ( wchar_t * )wMsg;

    if (FormMain->MyAgent1->SendMessage(bSeatNo, 0, 0, bMsg) == 0)
    {
      MessageBox(NULL,"簡訊已發送!!!","警告",MB_OK|MB_ICONINFORMATION);
      this->Close();
    }
  }
}
//---------------------------------------------------------------------------

