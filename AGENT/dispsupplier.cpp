//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dispsupplier.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDispSupplier *FormDispSupplier;
//---------------------------------------------------------------------------
__fastcall TFormDispSupplier::TFormDispSupplier(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormDispSupplier::SetSupplierData(CShopSupplier &shopsupplier)
{
  editSupplierId->Text = shopsupplier.SupplierId;
  editName->Text = shopsupplier.Name;
  editTelPhone->Text = shopsupplier.TelPhone;
  editCellPhone->Text = shopsupplier.CellPhone;
  editAddress->Text = shopsupplier.Address;
  editContact->Text = shopsupplier.Contact;
  memoRemark->Lines->Clear();
  memoRemark->Lines->Add(shopsupplier.Remark);
}

void __fastcall TFormDispSupplier::Button1Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

