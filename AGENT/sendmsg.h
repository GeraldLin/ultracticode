//---------------------------------------------------------------------------

#ifndef sendmsgH
#define sendmsgH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormSendMsg : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TComboBox *cbSeatNo;
  TLabel *Label2;
  TEdit *editSendMsg;
  TButton *Button1;
  TButton *Button2;
  TRadioGroup *RadioGroup1;
  TLabel *Label3;
  TComboBox *cbWorkerNo;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall RadioGroup1Click(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSendMsg(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSendMsg *FormSendMsg;
//---------------------------------------------------------------------------
#endif
