//---------------------------------------------------------------------------

#ifndef repcdrlistH
#define repcdrlistH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <ExtCtrls.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
//---------------------------------------------------------------------------
class TFormRepCDRList : public TForm
{
__published:	// IDE-managed Components
  TQuickRep *QuickRep1;
  TADOQuery *ADOQuery1;
  TQRBand *PageHeaderBand1;
  TQRBand *DetailBand1;
  TQRBand *PageFooterBand1;
  TQRSysData *QRSysData2;
  TQRLabel *QRLabel1;
  TQRLabel *QRLabelTotal;
  TQRLabel *QRLabel3;
  TQRLabel *QRLabel4;
  TQRLabel *QRLabel5;
  TQRLabel *QRLabel6;
  TQRLabel *QRLabel7;
  TQRLabel *QRLabel8;
  TQRLabel *QRLabel9;
  TQRLabel *QRLabel10;
  TQRDBText *QRDBText2;
  TQRDBText *QRDBText3;
  TQRDBText *QRDBText4;
  TQRDBText *QRDBText5;
  TQRDBText *QRDBText6;
  TQRDBText *QRDBText7;
  TQRDBText *QRDBText8;
  TQRDBText *QRDBText9;
  TQRSysData *QRSysData1;
  TQRLabel *QRLabel2;
  TQRDBText *QRDBText1;
  TQRLabel *QRLabel11;
  TQRDBText *QRDBText10;
  TIntegerField *ADOQuery1Id;
  TWideStringField *ADOQuery1SerialNo;
  TIntegerField *ADOQuery1ChnType;
  TIntegerField *ADOQuery1ChnNo;
  TWideStringField *ADOQuery1CallerNo;
  TWideStringField *ADOQuery1CalledNo;
  TWideStringField *ADOQuery1CustomNo;
  TWordField *ADOQuery1CallInOut;
  TWordField *ADOQuery1CallType;
  TWordField *ADOQuery1SrvType;
  TDateTimeField *ADOQuery1CallTime;
  TDateTimeField *ADOQuery1AnsTime;
  TWordField *ADOQuery1AnsId;
  TWideStringField *ADOQuery1SelCode;
  TDateTimeField *ADOQuery1AcdTime;
  TDateTimeField *ADOQuery1WaitTime;
  TDateTimeField *ADOQuery1RingTime;
  TWideStringField *ADOQuery1RingSeats;
  TWideStringField *ADOQuery1RingWorkerNos;
  TDateTimeField *ADOQuery1SeatAnsTime;
  TWordField *ADOQuery1SeatAnsId;
  TWideStringField *ADOQuery1SeatNo;
  TWideStringField *ADOQuery1WorkerNo;
  TIntegerField *ADOQuery1GroupNo;
  TDateTimeField *ADOQuery1SeatRelTime;
  TDateTimeField *ADOQuery1RelTime;
  TDateTimeField *ADOQuery1ACWTime;
  TIntegerField *ADOQuery1Score;
  TIntegerField *ADOQuery1TalkScore;
  TWideStringField *ADOQuery1RecdRootPath;
  TWideStringField *ADOQuery1RecdFile;
  TWideStringField *ADOQuery1Remark;
  TIntegerField *ADOQuery1RecdFileSize;
  TIntegerField *ADOQuery1RecdTimeLen;
  TWideStringField *ADOQuery1CustName;
  TWideStringField *ADOQuery1WorkerName;
  TStringField *ADOQuery1DispCallInOut;
  TStringField *ADOQuery1ScoreName;
  TStringField *ADOQuery1DispCallerNo;
  void __fastcall ADOQuery1CalcFields(TDataSet *DataSet);
private:	// User declarations
public:		// User declarations
  __fastcall TFormRepCDRList(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRepCDRList *FormRepCDRList;
//---------------------------------------------------------------------------
#endif
