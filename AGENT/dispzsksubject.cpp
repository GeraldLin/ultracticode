//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "dispzsksubject.h"
#include "DataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormDispZSKSubject *FormDispZSKSubject;
//---------------------------------------------------------------------------
__fastcall TFormDispZSKSubject::TFormDispZSKSubject(TComponent* Owner)
  : TForm(Owner)
{
  SubjectNo = 0;
}
//---------------------------------------------------------------------------
void TFormDispZSKSubject::SetZSKSubjectData(CZSKSubject& zsksubject)
{
  SubjectNo = zsksubject.SubjectNo;
  editMainTitle->Text = zsksubject.MainTitle;
  editSubTitle->Text = zsksubject.SubTitle;
  editSubjectTitle->Text = zsksubject.SubjectTitle;
  memoExplainCont->Lines->Clear();
  memoExplainCont->Lines->Add(zsksubject.ExplainCont);
  editExplainImage->Text = zsksubject.ExplainImage;
  editExplainFile->Text = zsksubject.ExplainFile;
  editRemark->Text = zsksubject.Remark;
  editVersionNo->Text = zsksubject.VersionNo;
}
void __fastcall TFormDispZSKSubject::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormDispZSKSubject::Button3Click(TObject *Sender)
{
  TADOBlobStream* pBlobRead;
  TMemoryStream *pMemStream;
  char sqlbuf[256];
  AnsiString strFileName;

  sprintf( sqlbuf, "Select ExplainImage from tbZSKSubject where SubjectNo=%d", SubjectNo);
  try
  {
    dmAgent->adoqueryPub->SQL->Clear();
    dmAgent->adoqueryPub->SQL->Add((char *)sqlbuf );
    dmAgent->adoqueryPub->Open();
  }
  catch ( ... )
  {
    return;
  }

  if ( !dmAgent->adoqueryPub->Eof )
  {
    pBlobRead = new TADOBlobStream((TBlobField*)dmAgent->adoqueryPub->FieldByName("ExplainImage"),bmRead);
    pMemStream = new TMemoryStream();
    pMemStream->LoadFromStream(pBlobRead);
    strFileName = ExtractFilePath(Application->ExeName)+"temp\\"+editExplainImage->Text;
    pMemStream->SaveToFile(strFileName.c_str());
    ShellExecute(Handle, "open", strFileName.c_str(), NULL, NULL, SW_SHOWDEFAULT);
  }
  dmAgent->adoqueryPub->Close();
  if (editExplainImage->Text.Length() > 0)
    ShellExecute(Handle, "open", editExplainImage->Text.c_str(), NULL, NULL, SW_SHOWDEFAULT);
}
//---------------------------------------------------------------------------
void __fastcall TFormDispZSKSubject::Button4Click(TObject *Sender)
{
  if (editExplainFile->Text.Length() > 0)
    ShellExecute(Handle, "open", editExplainFile->Text.c_str(), NULL, NULL, SW_SHOWDEFAULT);
}
//---------------------------------------------------------------------------

