object FormDial: TFormDial
  Left = 299
  Top = 109
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #36664#20837#22806#25765#34399#30908
  ClientHeight = 545
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 54
    Top = 14
    Width = 39
    Height = 16
    AutoSize = False
    Caption = #34399#30908#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbDialNum: TComboBox
    Left = 105
    Top = 9
    Width = 240
    Height = 32
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ItemHeight = 24
    ParentFont = False
    TabOrder = 0
    OnKeyDown = FormKeyDown
  end
  object Button1: TButton
    Left = 33
    Top = 223
    Width = 81
    Height = 27
    Caption = #30906#23450
    Default = True
    TabOrder = 1
    OnClick = Button1Click
    OnKeyDown = FormKeyDown
  end
  object Button2: TButton
    Left = 310
    Top = 223
    Width = 81
    Height = 27
    Cancel = True
    Caption = #21462#28040
    TabOrder = 2
    OnClick = Button2Click
    OnKeyDown = FormKeyDown
  end
  object Button3: TButton
    Left = 173
    Top = 223
    Width = 82
    Height = 27
    Caption = #36681#26371#35696
    TabOrder = 3
    OnClick = Button3Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF1: TButton
    Left = 104
    Top = 43
    Width = 65
    Height = 38
    Caption = '&1'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = btDTMF1Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF2: TButton
    Left = 191
    Top = 43
    Width = 65
    Height = 38
    Caption = '&2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = btDTMF2Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF3: TButton
    Left = 277
    Top = 43
    Width = 65
    Height = 38
    Caption = '&3'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    OnClick = btDTMF3Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF4: TButton
    Left = 104
    Top = 87
    Width = 65
    Height = 38
    Caption = '&4'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = btDTMF4Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF5: TButton
    Left = 191
    Top = 87
    Width = 65
    Height = 38
    Caption = '&5'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = btDTMF5Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF6: TButton
    Left = 277
    Top = 87
    Width = 65
    Height = 38
    Caption = '&6'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    OnClick = btDTMF6Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF7: TButton
    Left = 104
    Top = 130
    Width = 65
    Height = 38
    Caption = '&7'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    OnClick = btDTMF7Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF8: TButton
    Left = 191
    Top = 130
    Width = 65
    Height = 38
    Caption = '&8'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    OnClick = btDTMF8Click
    OnKeyDown = FormKeyDown
  end
  object btDTMF9: TButton
    Left = 277
    Top = 130
    Width = 65
    Height = 38
    Caption = '&9'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    OnClick = btDTMF9Click
    OnKeyDown = FormKeyDown
  end
  object btDTMFX: TButton
    Left = 104
    Top = 173
    Width = 65
    Height = 38
    Caption = '&*'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    OnClick = btDTMFXClick
    OnKeyDown = FormKeyDown
  end
  object btDTM0: TButton
    Left = 191
    Top = 173
    Width = 65
    Height = 38
    Caption = '&0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    OnClick = btDTM0Click
    OnKeyDown = FormKeyDown
  end
  object btDTMFJ: TButton
    Left = 277
    Top = 173
    Width = 65
    Height = 38
    Caption = '&#'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    OnClick = btDTMFJClick
    OnKeyDown = FormKeyDown
  end
  object ckMobileId: TCheckBox
    Left = 9
    Top = 494
    Width = 252
    Height = 18
    Caption = #25765#20986#26178#26159#21542#22312#30064#22320#25163#27231#21069#33258#21205#21152#25765'0'#65311
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clFuchsia
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    Visible = False
    OnClick = ckMobileIdClick
    OnKeyDown = FormKeyDown
  end
  object gbFastDial: TGroupBox
    Left = 23
    Top = 259
    Width = 377
    Height = 227
    Caption = #24555#36895#21151#33021#37749#23565#25033#30340#22806#32218#34399#30908#65306
    TabOrder = 17
    object lbFuncF1: TLabel
      Left = 9
      Top = 44
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF2: TLabel
      Left = 135
      Top = 44
      Width = 109
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF3: TLabel
      Left = 260
      Top = 44
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF4: TLabel
      Left = 9
      Top = 96
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF5: TLabel
      Left = 135
      Top = 96
      Width = 109
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF6: TLabel
      Left = 260
      Top = 96
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF7: TLabel
      Left = 9
      Top = 148
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF8: TLabel
      Left = 135
      Top = 148
      Width = 109
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF9: TLabel
      Left = 260
      Top = 148
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF10: TLabel
      Left = 9
      Top = 200
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF11: TLabel
      Left = 135
      Top = 200
      Width = 109
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbFuncF12: TLabel
      Left = 260
      Top = 200
      Width = 108
      Height = 20
      Alignment = taCenter
      AutoSize = False
      Color = clInactiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object btFuncF1: TButton
      Left = 9
      Top = 17
      Width = 108
      Height = 27
      Caption = 'F1'
      Enabled = False
      TabOrder = 0
      OnClick = btFuncF1Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF2: TButton
      Left = 135
      Top = 17
      Width = 109
      Height = 27
      Caption = 'F2'
      Enabled = False
      TabOrder = 1
      OnClick = btFuncF2Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF3: TButton
      Left = 260
      Top = 17
      Width = 108
      Height = 27
      Caption = 'F3'
      Enabled = False
      TabOrder = 2
      OnClick = btFuncF3Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF4: TButton
      Left = 9
      Top = 69
      Width = 108
      Height = 27
      Caption = 'F4'
      Enabled = False
      TabOrder = 3
      OnClick = btFuncF4Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF5: TButton
      Left = 135
      Top = 69
      Width = 109
      Height = 27
      Caption = 'F5'
      Enabled = False
      TabOrder = 4
      OnClick = btFuncF5Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF6: TButton
      Left = 260
      Top = 69
      Width = 108
      Height = 27
      Caption = 'F6'
      Enabled = False
      TabOrder = 5
      OnClick = btFuncF6Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF7: TButton
      Left = 9
      Top = 121
      Width = 108
      Height = 27
      Caption = 'F7'
      Enabled = False
      TabOrder = 6
      OnClick = btFuncF7Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF8: TButton
      Left = 135
      Top = 121
      Width = 109
      Height = 27
      Caption = 'F8'
      Enabled = False
      TabOrder = 7
      OnClick = btFuncF8Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF9: TButton
      Left = 260
      Top = 121
      Width = 108
      Height = 27
      Caption = 'F9'
      Enabled = False
      TabOrder = 8
      OnClick = btFuncF9Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF10: TButton
      Left = 9
      Top = 173
      Width = 108
      Height = 27
      Caption = 'F10'
      Enabled = False
      TabOrder = 9
      OnClick = btFuncF10Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF11: TButton
      Left = 135
      Top = 173
      Width = 109
      Height = 27
      Caption = 'F11'
      Enabled = False
      TabOrder = 10
      OnClick = btFuncF11Click
      OnKeyDown = FormKeyDown
    end
    object btFuncF12: TButton
      Left = 260
      Top = 173
      Width = 108
      Height = 27
      Caption = 'F12'
      Enabled = False
      TabOrder = 11
      OnClick = btFuncF12Click
      OnKeyDown = FormKeyDown
    end
  end
  object ckTranType: TCheckBox
    Left = 295
    Top = 494
    Width = 87
    Height = 18
    Caption = #24555#36895#36681#25509#65311
    Enabled = False
    TabOrder = 18
    Visible = False
  end
  object btUpDown: TButton
    Left = 3
    Top = 225
    Width = 24
    Height = 22
    Hint = #38577#34255#24555#36895#25765#34399#21151#33021#37749
    Caption = #923
    ParentShowHint = False
    ShowHint = True
    TabOrder = 19
    OnClick = btUpDownClick
  end
  object MediaPlayer1: TMediaPlayer
    Left = 61
    Top = 355
    Width = 271
    Height = 33
    AutoRewind = False
    Visible = False
    TabOrder = 20
  end
  object ckAutoAdd9OnDial: TCheckBox
    Left = 9
    Top = 520
    Width = 200
    Height = 18
    Caption = #26159#21542#22312#25765#20986#34399#30908#21069#33258#21205#21152#25765'9'#65311
    Enabled = False
    TabOrder = 21
    Visible = False
    OnClick = ckAutoAdd9OnDialClick
  end
  object ckAutoAdd9OnTran: TCheckBox
    Left = 217
    Top = 520
    Width = 200
    Height = 18
    Caption = #26159#21542#22312#36681#25509#34399#30908#21069#33258#21205#21152#25765'9'#65311
    Enabled = False
    TabOrder = 22
    Visible = False
    OnClick = ckAutoAdd9OnTranClick
  end
end
