//---------------------------------------------------------------------------

#ifndef crmdbparamH
#define crmdbparamH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormCRMDBParam : public TForm
{
__published:	// IDE-managed Components
  TGroupBox *GroupBox2;
  TLabel *Label15;
  TLabel *Label37;
  TLabel *Label16;
  TLabel *Label35;
  TLabel *Label36;
  TComboBox *cbxDBType;
  TEdit *edtServer;
  TEdit *edtDB;
  TEdit *edtUSER;
  TEdit *edtPSW;
  TCheckBox *ckCRMDBParam;
  TButton *btSaveParam;
  TButton *btClose;
  void __fastcall btCloseClick(TObject *Sender);
  void __fastcall cbxDBTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall cbxDBTypeChange(TObject *Sender);
  void __fastcall btSaveParamClick(TObject *Sender);
  void __fastcall edtServerChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormCRMDBParam(TComponent* Owner);

  void ReadIniFile();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormCRMDBParam *FormCRMDBParam;
//---------------------------------------------------------------------------
#endif
