// TCPClient.h: interface for the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_)
#define AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct TServer
{
	char  ServerIP[32];
	short ServerPort;
	unsigned short Serverid;
  bool  IsConnected;
};

class CTCPClient  
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnAppLogin,
                          ONAPPCLOSE OnAppClose,
                          ONAPPRECEIVEDATA OnAppReceiveData
                          );
private:
	TServer  IVRServer;		//IVR流程服務器

  void ReleaseDll();
public:
	CTCPClient();
	virtual ~CTCPClient();
  unsigned short ClientId;
  unsigned long SessionId;

  unsigned short GetIVRServerId()
  {return IVRServer.Serverid;}
  void SetIVRLinked() {IVRServer.IsConnected=true;}
  void SetIVRUnlink() {IVRServer.IsConnected=false;}
  bool IsIVRConnected()
  {return IVRServer.IsConnected;}

	char DllFile[64];
  bool LoadDll();
  bool IsOpen; //dll is opend

  bool ReadIni(const char *filename);
  bool ReadWebSetupIni(const char *filename);

  int ConnectIVRServer();
  int ConnectServer();

	//申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;

  int SendMessage(unsigned short ServerId, unsigned short MsgId, int MsgLen, const unsigned char *MsgBuf);
  int IVRSendMessage(unsigned short MsgId, const unsigned char *MsgBuf, int len);

  //組合對象編碼
  unsigned short CombineObjectID( unsigned char ObjectType, unsigned char ObjectNo );
	//組合消息類型及消息編號
  unsigned short CombineMessageID(unsigned char MsgType, unsigned char MsgNo);
  //分解對象標識及編號
  void GetClientID(unsigned short Msg_wParamHi, unsigned char &ClientType, unsigned char &ClientNo);
  //分解消息類型及消息編號
  void GetMsgID(unsigned short Msg_wParamLo, unsigned char &MsgType, unsigned char &MsgNo);
};
//tcllinkc.dll callback function,
extern void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnIVRLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

//---------------------------------------------------------------------------------
#endif // !defined(AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_)
