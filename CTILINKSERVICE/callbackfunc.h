//-----------------------------------------------------------------------------
#ifndef __CALLBACKFUNC_H__
#define __CALLBACKFUNC_H__
//-----------------------------------------------------------------------------

#include "CTILink.h"

//事件函數原型
void tmOnConnect(short nSwitchId, short nSwitchType, short nResult, LPCTSTR pszError);
void tmOnLinkStatus(short nSwitchId, short nLinkId, short nStatus, LPCTSTR pszError);
void tmOnGetDevNum(short nSwitchId, short nPortNum);
void tmOnGetDevParam(short nPortID, const char *pszDeviceID, short nChStyle, short nChStyleIndex, short nChType, short nChIndex, short nLgChType, short nLgChIndex);
void tmOnPortStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
void tmOnAgentStatus(short nPortID, LPCTSTR pszAgentID, short nStatus);
void tmOnRoomStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
void tmOnLampStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
void tmOnWakeStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
void tmOnACDQueueStatus(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszQueue);
void tmOnRecvBill(short nSwitchId, LPCTSTR pszBillRecord);
void tmOnCCBStatus(short nConnID, short nStatus);
void tmOnCallEvent(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nStatus, short nDirect, short nDesPortID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, LPCTSTR pszOrgCallerNo, LPCTSTR pszOrgCalledNo, LPCTSTR pszCallData, LPCTSTR pszCustomPhone, short nCallType);
void tmOnCallSam(short nPortID, LPCTSTR pszDeviceID, long nConnID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, short nSamType, LPCTSTR pszCallData, LPCTSTR pszCustomPhone);
void tmOnCallAcm(short nPortID, LPCTSTR pszDeviceID, long nConnID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo);
void tmOnCallRing(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult);
void tmOnCallFail(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult);
void tmOnCallAck(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, LPCTSTR pszCallData, LPCTSTR pszCustomPhone);
void tmOnCallRelease(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult);
void tmOnAnswerCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnRefuseCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnMakeCall(short nPortID, LPCTSTR pszDeviceID, long nCallID, long nConnID, short nResult);
void tmOnBandCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnBandVoice(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnHoldCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnUnHoldCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnTransferCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult);
void tmOnStopTransfer(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnSwapCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnPickupCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnBreakinCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnListenCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnRemoveCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnThreeConf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnConsoleHold(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnConsolePickup(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult);
void tmOnSendSwitchCMD(short nPortID, LPCTSTR pszDeviceID, short nResult);
void tmOnCallConnected(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult);
//ver:3.2版本增加
void tmOnRecordResult(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszReason, long nRecBytes, long nRecTimeLen);
void tmOnPlayResult(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszReason);
void tmOnRecvDTMF(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszDtmf);
//ver:3.3版本增加
void tmOnQueryACDSplit(short nSwitchId, LPCTSTR pszACDSplit, short nAvailAgents, short nCallsInQueue, short nLoginAgents, LPCTSTR pszACDParam);
void tmOnAgentLoggedOn(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, LPCTSTR pszPassword, short nStatus);
void tmOnAgentLoggedOff(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, short nStatus);

void tmOnSetTranPhone(short nPortID, LPCTSTR pszDeviceID, short nForwardType, short nForwardOnOff, LPCTSTR pszForwardDeviceID, short nResult);

//設置所有事件回調函數
void SetAllCallBackFunc();
//-----------------------------------------------------------------------------
#endif
