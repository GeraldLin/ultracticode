//---------------------------------------------------------------------------

#ifndef msgfifoH
#define msgfifoH

//---------------------------------------------------------------------------
//接收消息緩沖結構
typedef struct
{
  US ClientId;
	US MsgId; //指令編號
  CH MsgBuf[2048];

}VXML_RECV_MSG_STRUCT;

class CMsgfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_RECV_MSG_STRUCT      *pMsgs;

public:
  CMsgfifo()
	{
		Head=0;
		Tail=0;
		resv=0;
		Len=128;
		pMsgs=new VXML_RECV_MSG_STRUCT[128];
	}
	
	~CMsgfifo()
	{
		delete []pMsgs;
	}
	
	bool	IsFull()
	{
		unsigned short temp=Tail+1;
		return temp==Len?Head==0:temp==Head;
	}
	bool 	IsEmpty();
  VXML_RECV_MSG_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
  	if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(US ClientId, US MsgId, const char *MsgBuf);
  unsigned short  Read(US &ClientId, US &MsgId, char *MsgBuf);
  unsigned short  Read(VXML_RECV_MSG_STRUCT *RecvMsg);
};

//---------------------------------------------------------------------------
#endif
