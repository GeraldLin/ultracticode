//---------------------------------------------------------------------------
#ifndef __VARDEF_H__
#define __VARDEF_H__
//---------------------------------------------------------------------------
short g_nMultiRunMode=0; //雙機熱備模式（0-否，1-是）
short g_nActiveHost=1; //0-主機有效，1-備機有效

//全局變量、類定義
int  g_nMainCTILinkServiceID=1; //是否為主CTILinkService：0-否 1-是
int  g_nTAPICTILinkType=1; //TAPI CTI-LINK授權類型:0-未授權（CTI Lite） 1-已授權（CTI Pro）
int  g_nCTILinkstatus=0;

bool g_bSaveId=false;
bool g_bSaveCommId=false;
bool g_bSaveLinkId=false;
bool g_bInitSuccId=false;
bool g_bConnectIVR=false;

int g_nINIFileType=0; //INI配置文件：0-默認的舊的格式 2-新的web配置格式

char g_szRootPath[MAX_PATH_LEN]; //平臺安裝根路徑
//日志文件路徑
char g_szLogPath[MAX_PATH_LEN]; 
//執行文件路徑
char g_szAppPath[MAX_PATH_LEN]; 
//程序配置文件
char g_szCTIConfigFileName[MAX_PATH_FILE_LEN];

char g_szUnimeINIFileName[MAX_PATH_FILE_LEN];

CTCPClient g_cTcpClient;
CMsgfifo g_cMsgFifo;
CFlwRule g_cCTLLinkRule;
CXMLMsg g_cRecvMsg;
CXMLMsg g_cSendMsg;

CSwitchPortList g_cSwitchPortList;

CCriticalSection g_cCriticalSection;

//---------------------------------------------------------------------------

#endif
