// TCPClient.cpp: implementation of the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TCPClient.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTCPClient::CTCPClient()
{
	hdll = NULL;
  strcpy(DllFile, "tmtcpserver.dll");
  ClientId = 0x0101;
  LoadDll();
}

CTCPClient::~CTCPClient()
{
  ReleaseDll();
}

bool CTCPClient::LoadDll()
{
	hdll = LoadLibrary(DllFile);
	if ( hdll != NULL )
	{
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(  !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MessageBoxA(NULL, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MessageBoxA(NULL, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		return false;
	}
}

void CTCPClient::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}
//組合對象編碼
unsigned short CTCPClient::CombineObjectID( unsigned char ObjectType, unsigned char ObjectNo )
{
  US o_i1, o_i2;
  o_i1 = ( ObjectType << 8 ) & 0xFF00;
  o_i2 = ObjectNo & 0x00FF;
  return (o_i1 | o_i2);
}
//組合消息類型及消息編號
unsigned short CTCPClient::CombineMessageID(unsigned char MsgType, unsigned char MsgNo)
{
  US m_i1, m_i2;
  m_i1 = ( MsgType << 8 ) & 0xFF00;
  m_i2 = MsgNo & 0x00FF;
  return (m_i1 | m_i2);
}
//分解對象標識及編號
void CTCPClient::GetClientID(unsigned short Msg_wParamHi, unsigned char &ClientType, unsigned char &ClientNo)
{
  UC C_T, C_N;
  C_T = ( Msg_wParamHi >> 8 ) & 0x00FF;
  C_N = Msg_wParamHi & 0x00FF;
  ClientType = C_T;
  ClientNo = C_N;
}
//分解消息類型及消息編號
void CTCPClient::GetMsgID(unsigned short Msg_wParamLo, unsigned char &MsgType, unsigned char &MsgNo)
{
  UC M_T, M_N;
  M_T = ( Msg_wParamLo >> 8 ) & 0x00FF;
  M_N = Msg_wParamLo & 0x00FF;
  MsgType = M_T;
  MsgNo = M_N;
}
//發送消息
int CTCPClient::SendMessage(unsigned short ServerId, unsigned short MsgId, int MsgLen, const unsigned char *MsgBuf)
{
	int SendedLen;

  SendedLen = AppSendData(ServerId, MsgId, MsgBuf, MsgLen);
//   if (SendedLen != MsgLen)
//     TRACE("發送消息錯誤 ServerId=%04x MsgId=%04x MsgLen=%d SendedLen=%d\n", ServerId, MsgId, MsgLen, SendedLen);
	return SendedLen;
}
int CTCPClient::IVRSendMessage(unsigned short MsgId, const unsigned char *MsgBuf, int len)
{
  //MyTrace(2, MsgBuf);
  return SendMessage(IVRServer.Serverid, MsgId, len, MsgBuf);
}
bool CTCPClient::ReadIni(const char *filename)
{
  ClientId = CombineObjectID(NODE_SWT, GetPrivateProfileInt("IVRSERVER", "ClientId", 1, filename));
  SessionId = (ClientId<<16)&0xFFFF0000;

  IVRServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "IVRServerIP", "127.0.0.1", IVRServer.ServerIP, 30, filename);
  IVRServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "IVRServerPort", 5227, filename);
  IVRServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "IVRServerID", 1, filename));
  
  return true;
}    
bool CTCPClient::ReadWebSetupIni(const char *filename)
{
  ClientId = CombineObjectID(NODE_SWT, GetPrivateProfileInt("TCPLINK", "CTILINKServerID", 1, filename));
  SessionId = (ClientId<<16)&0xFFFF0000;
  
  IVRServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "IVRServerIP", "127.0.0.1", IVRServer.ServerIP, 30, filename);
  IVRServer.ServerPort = GetPrivateProfileInt("TCPLINK", "IVRServerPort", 5227, filename);
  IVRServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("TCPLINK", "IVRServerID", 1, filename));
  
  return true;
}    
int CTCPClient::ConnectIVRServer()
{
  if (IsOpen == true)
  {
    AppInit(IVRServer.Serverid,
            ClientId,
            IVRServer.ServerIP,
            IVRServer.ServerPort,
            OnIVRLogin,
            OnIVRClose,
            OnIVRReceiveData);
  printf("ConnectIVRServer ClientId=0x%04x ServerIP=%s ServerPort=%d Serverid=%d\n",ClientId,
    IVRServer.ServerIP,
    IVRServer.ServerPort,
    IVRServer.Serverid);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPClient::ConnectServer()
{
  int result = 0;

  result = ConnectIVRServer();
  return result;
}