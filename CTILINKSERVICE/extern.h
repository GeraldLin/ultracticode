//-----------------------------------------------------------------------------
#ifndef __externdef__h_
#define __externdef__h_
//-----------------------------------------------------------------------------
extern short g_nMultiRunMode; //雙機熱備模式（0-否，1-是）
extern short g_nActiveHost; //0-主機有效，1-備機有效

extern int  g_nMainCTILinkServiceID;
extern int  g_nTAPICTILinkType;
extern int  g_nCTILinkstatus;

extern bool g_bSaveId;
extern bool g_bSaveCommId;
extern bool g_bSaveLinkId;
extern bool g_bInitSuccId;
extern bool g_bConnectIVR;

extern int g_nINIFileType;

extern char g_szRootPath[MAX_PATH_LEN];
//日志文件路徑
extern char g_szLogPath[MAX_PATH_LEN]; 
//執行文件路徑
extern char g_szAppPath[MAX_PATH_LEN]; 
//程序配置文件
extern char g_szCTIConfigFileName[MAX_PATH_FILE_LEN];

extern char g_szUnimeINIFileName[MAX_PATH_FILE_LEN];

extern CTCPClient g_cTcpClient;
extern CMsgfifo g_cMsgFifo;
extern CFlwRule g_cCTLLinkRule;
extern CXMLMsg g_cRecvMsg;
extern CXMLMsg g_cSendMsg;

extern CSwitchPortList g_cSwitchPortList;

extern CCriticalSection g_cCriticalSection;

//-----------------------------------------------------------------------------
#endif