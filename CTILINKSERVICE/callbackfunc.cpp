//-----------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"
#include "callbackfunc.h"
#include "mainfunc.h"
//-----------------------------------------------------------------------------

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

//事件函數原型
void tmOnConnectSwitch(short nSwitchId, short nSwitchType, short nResult, LPCTSTR pszError)
{
	MyCriticalSectionLock();

  //在此添加響應事件代碼
  if (nResult == 0)
  {
    g_cSwitchPortList.m_nSwitchID = nSwitchId;
    g_cTcpClient.SessionId |= nSwitchId;
    g_cSwitchPortList.m_nSwitchType = nSwitchType;
    g_cSwitchPortList.m_bConnectSwitch = true;
  }
  else
  {
    g_cSwitchPortList.m_bConnectSwitch = false;
  }

  AddMsgHeader(SWTMSG_onconnectlink);
  AddIntItem(SWTMSG_onconnectlink, 2, nSwitchType);
  AddIntItem(SWTMSG_onconnectlink, 3, nResult);
  AddStrItem(SWTMSG_onconnectlink, 4, pszError);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onconnectlink, g_cSendMsg.GetBuf());

  MyTrace(3, "OnConnectSwitch nSwitchId=%d nSwitchType=%d nResult=%d pszError=%s", 
    nSwitchId, nSwitchType, nResult, pszError);
  
  MyCriticalSectionUnlock();
}
void tmOnLinkStatus(short nSwitchId, short nLinkId, short nStatus, LPCTSTR pszError)
{
  MyCriticalSectionLock();
  //在此添加響應事件代碼
  
  AddMsgHeader(SWTMSG_onlinkstatus);
  AddIntItem(SWTMSG_onlinkstatus, 2, nLinkId);
  AddIntItem(SWTMSG_onlinkstatus, 3, nStatus);
  AddStrItem(SWTMSG_onlinkstatus, 4, pszError);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onlinkstatus, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnLinkStatus nSwitchId=%d nLinkId=%d nStatus=%d pszError=%s", 
//     nSwitchId, nLinkId, nStatus, pszError);
  
  MyCriticalSectionUnlock();
}
void tmOnGetDevNum(short nSwitchId, short nPortNum)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  g_cSwitchPortList.InitData(nPortNum);

//   AddMsgHeader(SWTMSG_ongetdevnnum);
//   AddIntItem(SWTMSG_ongetdevnnum, 2, nPortNum);
//   AddMsgTail();
//   SendMsg2IVR(SWTMSG_ongetdevnnum, g_cSendMsg.GetBuf());

  //MyTrace(3, "OnGetDevNum nSwitchId=%d nPortNum=%d", nSwitchId, nPortNum);
  
  MyCriticalSectionUnlock();
}
void tmOnGetDevParam(short nPortID, const char *pszDeviceID, short nChStyle, short nChStyleIndex, short nChType, short nChIndex, short nLgChType, short nLgChIndex)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  //g_cSwitchPortList.SetPortData(nPortID, pszDeviceID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex, 0);
  
//   MyTrace(3, "OnGetDevParam nPortID=%d pszDeviceID=%s nChStyle=%d nChStyleIndex=%d nLgChType=%d nLgChIndex=%d", 
//     nPortID, pszDeviceID, nChStyle, nChStyleIndex, nLgChType, nLgChIndex);

  MyCriticalSectionUnlock();
}
void tmOnPortStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onportstatus);
  AddIntItem(SWTMSG_onportstatus, 2, nPortID);
  AddStrItem(SWTMSG_onportstatus, 3, pszDeviceID);
  AddIntItem(SWTMSG_onportstatus, 4, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onportstatus, g_cSendMsg.GetBuf());

  MyCriticalSectionUnlock();
}
void tmOnAgentStatus(short nPortID, LPCTSTR pszAgentID, short nStatus)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  
  AddMsgHeader(SWTMSG_onagentstatus);
  AddIntItem(SWTMSG_onagentstatus, 2, nPortID);
  AddStrItem(SWTMSG_onagentstatus, 3, pszAgentID);
  AddIntItem(SWTMSG_onagentstatus, 4, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onagentstatus, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnAgentStatus nPortID=%d pszAgentID=%s nStatus=%d", 
//     nPortID, pszAgentID, nStatus);

  MyCriticalSectionUnlock();
}
void tmOnRoomStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onroomstatus);
  AddIntItem(SWTMSG_onroomstatus, 2, nPortID);
  AddStrItem(SWTMSG_onroomstatus, 3, pszDeviceID);
  AddIntItem(SWTMSG_onroomstatus, 4, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onroomstatus, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnRoomStatus nPortID=%d pszDeviceID=%s nStatus=%d", 
//     nPortID, pszDeviceID, nStatus);
  
  MyCriticalSectionUnlock();
}
void tmOnLampStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onlampevent);
  AddIntItem(SWTMSG_onlampevent, 2, nPortID);
  AddStrItem(SWTMSG_onlampevent, 3, pszDeviceID);
  AddIntItem(SWTMSG_onlampevent, 4, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onlampevent, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnLampStatus nPortID=%d pszDeviceID=%s nStatus=%d", 
//     nPortID, pszDeviceID, nStatus);
  
  MyCriticalSectionUnlock();
}
void tmOnWakeStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onwakeevent);
  AddIntItem(SWTMSG_onwakeevent, 2, nPortID);
  AddStrItem(SWTMSG_onwakeevent, 3, pszDeviceID);
  AddIntItem(SWTMSG_onwakeevent, 4, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onwakeevent, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnWakeStatus nPortID=%d pszDeviceID=%s nStatus=%d", 
//     nPortID, pszDeviceID, nStatus);
  
  MyCriticalSectionUnlock();
}
void tmOnACDQueueStatus(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszQueue)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onconsoleevent);
  AddIntItem(SWTMSG_onconsoleevent, 2, nPortID);
  AddStrItem(SWTMSG_onconsoleevent, 3, pszDeviceID);
  AddStrItem(SWTMSG_onconsoleevent, 4, pszQueue);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onconsoleevent, g_cSendMsg.GetBuf());

  //MyTrace(3, "OnACDQueueStatus nPortID=%d pszDeviceID=%s pszQueue=%s", 
  //  nPortID, pszDeviceID, pszQueue);
  
  MyCriticalSectionUnlock();
}
void tmOnRecvBill(short nSwitchId, LPCTSTR pszBillRecord)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onbillevent);
  AddStrItem(SWTMSG_onbillevent, 2, pszBillRecord);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onbillevent, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnRecvBill nSwitchId=%d pszBillRecord=%s", 
//     nSwitchId, pszBillRecord);
  
  MyCriticalSectionUnlock();
}
void tmOnCCBStatus(short nConnID, short nStatus)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
 
  MyTrace(3, "OnCCBStatus nConnID=%d nStatus=%d", nConnID, nStatus);

  MyCriticalSectionUnlock();
}
void tmOnCallEvent(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nStatus, short nDirect, short nDesPortID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, LPCTSTR pszOrgCallerNo, LPCTSTR pszOrgCalledNo, LPCTSTR pszCallData, LPCTSTR pszCustomPhone, short nCallType)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_oncallevent);
  AddIntItem(SWTMSG_oncallevent, 2, nPortID);
  AddStrItem(SWTMSG_oncallevent, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallevent, 4, nConnID);
  AddIntItem(SWTMSG_oncallevent, 5, nDirect);
  AddIntItem(SWTMSG_oncallevent, 6, nDesPortID);
  AddStrItem(SWTMSG_oncallevent, 7, pszCallerNo);
  AddStrItem(SWTMSG_oncallevent, 8, pszCalledNo);
  AddStrItem(SWTMSG_oncallevent, 9, pszOrgCallerNo);
  AddStrItem(SWTMSG_oncallevent, 10, pszOrgCalledNo);
  AddIntItem(SWTMSG_oncallevent, 11, nStatus);
  AddStrItem(SWTMSG_oncallevent, 12, pszCallData);
  AddStrItem(SWTMSG_oncallevent, 13, pszCustomPhone);
  AddIntItem(SWTMSG_oncallevent, 14, nCallType);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallevent, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnCallEvent nPortID=%d pszDeviceID=%s nConnID=%d nStatus=%d nDirect=%d nDesPortID=%d pszCallerNo=%s pszCalledNo=%s pszOrgCallerNo=%s pszOrgCalledNo=%s", 
//     nPortID, pszDeviceID, nConnID, nStatus, nDirect, nDesPortID, pszCallerNo, pszCalledNo, pszOrgCallerNo, pszOrgCalledNo);

  MyCriticalSectionUnlock();
}
void tmOnCallSam(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, short nSamType, LPCTSTR pszCallData, LPCTSTR pszCustomPhone)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼

  AddMsgHeader(SWTMSG_oncallsam);
  AddIntItem(SWTMSG_oncallsam, 2, nPortID);
  AddStrItem(SWTMSG_oncallsam, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallsam, 4, nConnID);
  AddIntItem(SWTMSG_oncallsam, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallsam, 6, pszDesDeviceID);
  AddStrItem(SWTMSG_oncallsam, 7, pszCallerNo);
  AddStrItem(SWTMSG_oncallsam, 8, pszCalledNo);
  AddIntItem(SWTMSG_oncallsam, 9, nSamType);
  AddStrItem(SWTMSG_oncallsam, 10, pszCallData);
  AddStrItem(SWTMSG_oncallsam, 11, pszCustomPhone);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallsam, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnCallSam nPortID=%d pszDeviceID=%s nConnID=%d pszCallerNo=%s pszCalledNo=%s", 
//     nPortID, pszDeviceID, nConnID, pszCallerNo, pszCalledNo);
  
  MyCriticalSectionUnlock();
}
void tmOnCallAcm(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼

  AddMsgHeader(SWTMSG_oncallacm);
  AddIntItem(SWTMSG_oncallacm, 2, nPortID);
  AddStrItem(SWTMSG_oncallacm, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallacm, 4, nConnID);
  AddIntItem(SWTMSG_oncallsam, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallsam, 6, pszDesDeviceID);
  AddStrItem(SWTMSG_oncallacm, 7, pszCallerNo);
  AddStrItem(SWTMSG_oncallacm, 8, pszCalledNo);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallacm, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnCallAcm nPortID=%d pszDeviceID=%s nConnID=%d pszCallerNo=%s pszCalledNo=%s", 
//     nPortID, pszDeviceID, nConnID, pszCallerNo, pszCalledNo);
  
  MyCriticalSectionUnlock();
}
void tmOnCallRing(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  
  AddMsgHeader(SWTMSG_oncallring);
  AddIntItem(SWTMSG_oncallring, 2, nPortID);
  AddStrItem(SWTMSG_oncallring, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallring, 4, nConnID);
  AddIntItem(SWTMSG_oncallring, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallring, 6, pszDesDeviceID);
  AddIntItem(SWTMSG_oncallring, 7, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallring, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnCallRing nPortID=%d pszDeviceID=%s nConnID=%d nDesPortID=%d pszDesDeviceID=%s nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nDesPortID, pszDesDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnCallFail(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_oncallfail);
  AddIntItem(SWTMSG_oncallfail, 2, nPortID);
  AddStrItem(SWTMSG_oncallfail, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallfail, 4, nConnID);
  AddIntItem(SWTMSG_oncallring, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallring, 6, pszDesDeviceID);
  AddIntItem(SWTMSG_oncallfail, 7, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallfail, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnCallFail nPortID=%d pszDeviceID=%s nConnID=%d nDesPortID=%d pszDesDeviceID=%s nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nDesPortID, pszDesDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnCallAck(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, LPCTSTR pszCallData, LPCTSTR pszCustomPhone)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼

  AddMsgHeader(SWTMSG_oncallack);
  AddIntItem(SWTMSG_oncallack, 2, nPortID);
  AddStrItem(SWTMSG_oncallack, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallack, 4, nConnID);
  AddIntItem(SWTMSG_oncallack, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallack, 6, pszDesDeviceID);
  AddIntItem(SWTMSG_oncallack, 7, nResult);
  AddStrItem(SWTMSG_oncallack, 8, pszCallerNo);
  AddStrItem(SWTMSG_oncallack, 9, pszCalledNo);
  AddStrItem(SWTMSG_oncallack, 10, pszCallData);
  AddStrItem(SWTMSG_oncallack, 11, pszCustomPhone);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallack, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnCallAns nPortID=%d pszDeviceID=%s nConnID=%d nDesPortID=%d pszDesDeviceID=%s nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nDesPortID, pszDesDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnCallRelease(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼

  AddMsgHeader(SWTMSG_oncallrelease);
  AddIntItem(SWTMSG_oncallrelease, 2, nPortID);
  AddStrItem(SWTMSG_oncallrelease, 3, pszDeviceID);
  AddLongItem(SWTMSG_oncallrelease, 4, nConnID);
  AddIntItem(SWTMSG_oncallrelease, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallrelease, 6, pszDesDeviceID);
  AddIntItem(SWTMSG_oncallrelease, 7, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallrelease, g_cSendMsg.GetBuf());

//   MyTrace(3, "OnCallRelease nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnAnswerCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onanswercall);
  AddIntItem(SWTMSG_onanswercall, 2, nPortID);
  AddStrItem(SWTMSG_onanswercall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onanswercall, 4, nConnID);
  AddIntItem(SWTMSG_onanswercall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onanswercall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnAnswerCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnRefuseCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onrefusecall);
  AddIntItem(SWTMSG_onrefusecall, 2, nPortID);
  AddStrItem(SWTMSG_onrefusecall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onrefusecall, 4, nConnID);
  AddIntItem(SWTMSG_onrefusecall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onrefusecall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnRefuseCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnMakeCall(short nPortID, LPCTSTR pszDeviceID, long nCallID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onmakecall);
  AddIntItem(SWTMSG_onmakecall, 2, nPortID);
  AddStrItem(SWTMSG_onmakecall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onmakecall, 4, nCallID);
  AddLongItem(SWTMSG_onmakecall, 5, nConnID);
  AddIntItem(SWTMSG_onmakecall, 6, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onmakecall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnMakeCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnBandCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onbandcall);
  AddIntItem(SWTMSG_onbandcall, 2, nPortID);
  AddStrItem(SWTMSG_onbandcall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onbandcall, 4, nConnID);
  AddIntItem(SWTMSG_onbandcall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onbandcall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnBandCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnBandVoice(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onbandvoice);
  AddIntItem(SWTMSG_onbandvoice, 2, nPortID);
  AddStrItem(SWTMSG_onbandvoice, 3, pszDeviceID);
  AddLongItem(SWTMSG_onbandvoice, 4, nConnID);
  AddIntItem(SWTMSG_onbandvoice, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onbandcall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnBandVoice nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnHoldCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onholdcall);
  AddIntItem(SWTMSG_onholdcall, 2, nPortID);
  AddStrItem(SWTMSG_onholdcall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onholdcall, 4, nConnID);
  AddIntItem(SWTMSG_onholdcall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onholdcall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnHoldCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnUnHoldCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onunholdcall);
  AddIntItem(SWTMSG_onunholdcall, 2, nPortID);
  AddStrItem(SWTMSG_onunholdcall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onunholdcall, 4, nConnID);
  AddIntItem(SWTMSG_onunholdcall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onunholdcall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnUnHoldCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnTransferCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_ontransfercall);
  AddIntItem(SWTMSG_ontransfercall, 2, nPortID);
  AddStrItem(SWTMSG_ontransfercall, 3, pszDeviceID);
  AddLongItem(SWTMSG_ontransfercall, 4, nConnID);
  AddIntItem(SWTMSG_ontransfercall, 5, nDesPortID);
  AddStrItem(SWTMSG_ontransfercall, 6, pszDesDeviceID);
  AddIntItem(SWTMSG_ontransfercall, 7, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_ontransfercall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnTransferCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnStopTransfer(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onstoptransfer);
  AddIntItem(SWTMSG_onstoptransfer, 2, nPortID);
  AddStrItem(SWTMSG_onstoptransfer, 3, pszDeviceID);
  AddLongItem(SWTMSG_onstoptransfer, 4, nConnID);
  AddIntItem(SWTMSG_onstoptransfer, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onstoptransfer, g_cSendMsg.GetBuf());
  
  //   MyTrace(3, "OnStopTransfer nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
  //     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnSwapCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onswapcall);
  AddIntItem(SWTMSG_onswapcall, 2, nPortID);
  AddStrItem(SWTMSG_onswapcall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onswapcall, 4, nConnID);
  AddIntItem(SWTMSG_onswapcall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onswapcall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnSwapCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnPickupCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onpickupcall);
  AddIntItem(SWTMSG_onpickupcall, 2, nPortID);
  AddStrItem(SWTMSG_onpickupcall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onpickupcall, 4, nConnID);
  AddIntItem(SWTMSG_onpickupcall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onpickupcall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnPickupCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnBreakinCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onbreakincall);
  AddIntItem(SWTMSG_onbreakincall, 2, nPortID);
  AddStrItem(SWTMSG_onbreakincall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onbreakincall, 4, nConnID);
  AddIntItem(SWTMSG_onbreakincall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onbreakincall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnBreakinCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnListenCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onlistencall);
  AddIntItem(SWTMSG_onlistencall, 2, nPortID);
  AddStrItem(SWTMSG_onlistencall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onlistencall, 4, nConnID);
  AddIntItem(SWTMSG_onlistencall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onlistencall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnListenCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnRemoveCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onremovecall);
  AddIntItem(SWTMSG_onremovecall, 2, nPortID);
  AddStrItem(SWTMSG_onremovecall, 3, pszDeviceID);
  AddLongItem(SWTMSG_onremovecall, 4, nConnID);
  AddIntItem(SWTMSG_onremovecall, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onremovecall, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnRemoveCall nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnThreeConf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onthreeconf);
  AddIntItem(SWTMSG_onthreeconf, 2, nPortID);
  AddStrItem(SWTMSG_onthreeconf, 3, pszDeviceID);
  AddLongItem(SWTMSG_onthreeconf, 4, nConnID);
  AddIntItem(SWTMSG_onthreeconf, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onthreeconf, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnThreeConf nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnConsoleHold(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onconsolehold);
  AddIntItem(SWTMSG_onconsolehold, 2, nPortID);
  AddStrItem(SWTMSG_onconsolehold, 3, pszDeviceID);
  AddLongItem(SWTMSG_onconsolehold, 4, nConnID);
  AddIntItem(SWTMSG_onconsolehold, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onconsolehold, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnConsoleHold nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnConsolePickup(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult)
{
  MyCriticalSectionLock();
  
	//在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onconsolepickup);
  AddIntItem(SWTMSG_onconsolepickup, 2, nPortID);
  AddStrItem(SWTMSG_onconsolepickup, 3, pszDeviceID);
  AddLongItem(SWTMSG_onconsolepickup, 4, nConnID);
  AddIntItem(SWTMSG_onconsolepickup, 5, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onconsolepickup, g_cSendMsg.GetBuf());
  
//   MyTrace(3, "OnConsolePickup nPortID=%d pszDeviceID=%s nConnID=%d nResult=%d", 
//     nPortID, pszDeviceID, nConnID, nResult);

  MyCriticalSectionUnlock();
}
void tmOnSendSwitchCMD(short nPortID, LPCTSTR pszDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onsendswitchcmd);
  AddIntItem(SWTMSG_onsendswitchcmd, 2, nPortID);
  AddStrItem(SWTMSG_onsendswitchcmd, 3, pszDeviceID);
  AddIntItem(SWTMSG_onsendswitchcmd, 4, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onsendswitchcmd, g_cSendMsg.GetBuf());
  
  //   MyTrace(3, "OnSendSwitchCMD nPortID=%d pszDeviceID=%s nResult=%d", 
  //     nPortID, pszDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnCallConnected(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_oncallconnected);
  AddIntItem(SWTMSG_oncallconnected, 2, nPortID);
  AddStrItem(SWTMSG_oncallconnected, 3, pszDeviceID);
  AddIntItem(SWTMSG_oncallconnected, 4, nConnID);
  AddIntItem(SWTMSG_oncallconnected, 5, nDesPortID);
  AddStrItem(SWTMSG_oncallconnected, 6, pszDesDeviceID);
  AddIntItem(SWTMSG_oncallconnected, 7, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_oncallconnected, g_cSendMsg.GetBuf());
  
  //   MyTrace(3, "OnCallConnected nPortID=%d pszDeviceID=%s nResult=%d", 
  //     nPortID, pszDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
//ver:3.2版本增加
void tmOnRecordResult(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszReason, long nRecBytes, long nRecTimeLen)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onrecordresult);
  AddIntItem(SWTMSG_onrecordresult, 2, nPortID);
  AddStrItem(SWTMSG_onrecordresult, 3, pszDeviceID);
  AddIntItem(SWTMSG_onrecordresult, 4, nConnID);
  AddIntItem(SWTMSG_onrecordresult, 5, nResult);
  AddStrItem(SWTMSG_onrecordresult, 6, pszReason);
  AddIntItem(SWTMSG_onrecordresult, 7, nRecBytes);
  AddIntItem(SWTMSG_onrecordresult, 8, nRecTimeLen);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onrecordresult, g_cSendMsg.GetBuf());
  
  //   MyTrace(3, "OnCallConnected nPortID=%d pszDeviceID=%s nResult=%d", 
  //     nPortID, pszDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnPlayResult(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszReason)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onplayresult);
  AddIntItem(SWTMSG_onplayresult, 2, nPortID);
  AddStrItem(SWTMSG_onplayresult, 3, pszDeviceID);
  AddIntItem(SWTMSG_onplayresult, 4, nConnID);
  AddIntItem(SWTMSG_onplayresult, 5, nResult);
  AddStrItem(SWTMSG_onplayresult, 6, pszReason);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onplayresult, g_cSendMsg.GetBuf());
  
  //   MyTrace(3, "OnCallConnected nPortID=%d pszDeviceID=%s nResult=%d", 
  //     nPortID, pszDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnRecvDTMF(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszDtmf)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onrecvdtmf);
  AddIntItem(SWTMSG_onrecvdtmf, 2, nPortID);
  AddStrItem(SWTMSG_onrecvdtmf, 3, pszDeviceID);
  AddIntItem(SWTMSG_onrecvdtmf, 4, nConnID);
  AddIntItem(SWTMSG_onrecvdtmf, 5, nResult);
  AddStrItem(SWTMSG_onrecvdtmf, 6, pszDtmf);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onrecvdtmf, g_cSendMsg.GetBuf());
  
  //   MyTrace(3, "OnCallConnected nPortID=%d pszDeviceID=%s nResult=%d", 
  //     nPortID, pszDeviceID, nResult);
  
  MyCriticalSectionUnlock();
}
void tmOnQueryACDSplit(short nSwitchId, LPCTSTR pszACDSplit, short nAvailAgents, short nCallsInQueue, short nLoginAgents, LPCTSTR pszACDParam)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onqueryacdsplit);
  AddStrItem(SWTMSG_onqueryacdsplit, 2, pszACDSplit);
  AddIntItem(SWTMSG_onqueryacdsplit, 3, nAvailAgents);
  AddIntItem(SWTMSG_onqueryacdsplit, 4, nCallsInQueue);
  AddIntItem(SWTMSG_onqueryacdsplit, 5, nLoginAgents);
  AddStrItem(SWTMSG_onqueryacdsplit, 6, pszACDParam);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onqueryacdsplit, g_cSendMsg.GetBuf());
  
  MyCriticalSectionUnlock();
}
void tmOnAgentLoggedOn(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, LPCTSTR pszPassword, short nStatus)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onagentloggedon);
  AddIntItem(SWTMSG_onagentloggedon, 2, nPortID);
  AddStrItem(SWTMSG_onagentloggedon, 3, pszDeviceID);
  AddStrItem(SWTMSG_onagentloggedon, 4, pszAgentID);
  AddStrItem(SWTMSG_onagentloggedon, 5, pszGroupID);
  AddStrItem(SWTMSG_onagentloggedon, 6, pszPassword);
  AddIntItem(SWTMSG_onagentloggedon, 7, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onagentloggedon, g_cSendMsg.GetBuf());
  
  MyCriticalSectionUnlock();
}
void tmOnAgentLoggedOff(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, short nStatus)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onagentloggedoff);
  AddIntItem(SWTMSG_onagentloggedoff, 2, nPortID);
  AddStrItem(SWTMSG_onagentloggedoff, 3, pszDeviceID);
  AddStrItem(SWTMSG_onagentloggedoff, 4, pszAgentID);
  AddStrItem(SWTMSG_onagentloggedoff, 5, pszGroupID);
  AddIntItem(SWTMSG_onagentloggedoff, 6, nStatus);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onagentloggedoff, g_cSendMsg.GetBuf());
  
  MyCriticalSectionUnlock();
}
void tmOnSetTranPhone(short nPortID, LPCTSTR pszDeviceID, short nForwardType, short nForwardOnOff, LPCTSTR pszForwardDeviceID, short nResult)
{
  MyCriticalSectionLock();
  
  //在此添加響應事件代碼
  AddMsgHeader(SWTMSG_onsettranphone);
  AddIntItem(SWTMSG_onsettranphone, 2, nPortID);
  AddStrItem(SWTMSG_onsettranphone, 3, pszDeviceID);
  AddIntItem(SWTMSG_onsettranphone, 4, nForwardType);
  AddIntItem(SWTMSG_onsettranphone, 5, nForwardOnOff);
  AddStrItem(SWTMSG_onsettranphone, 6, pszForwardDeviceID);
  AddIntItem(SWTMSG_onsettranphone, 7, nResult);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onsettranphone, g_cSendMsg.GetBuf());
  
  MyCriticalSectionUnlock();
}

//設置所有事件回調函數
void SetAllCallBackFunc()
{
	tmSetFuncOnConnect(tmOnConnectSwitch);
	tmSetFuncOnLinkStatus(tmOnLinkStatus);
	tmSetFuncOnGetDevNum(tmOnGetDevNum);
	tmSetFuncOnGetDevParam(tmOnGetDevParam);
	tmSetFuncOnPortStatus(tmOnPortStatus);
	tmSetFuncOnAgentStatus(tmOnAgentStatus);
	tmSetFuncOnRoomStatus(tmOnRoomStatus);
	tmSetFuncOnLampStatus(tmOnLampStatus);
	tmSetFuncOnWakeStatus(tmOnWakeStatus);
	tmSetFuncOnACDQueueStatus(tmOnACDQueueStatus);
	tmSetFuncOnRecvBill(tmOnRecvBill);
  tmSetFuncOnCCBStatus(tmOnCCBStatus);
	tmSetFuncOnCallEvent(tmOnCallEvent);
	tmSetFuncOnCallSam(tmOnCallSam);
	tmSetFuncOnCallAcm(tmOnCallAcm);
	tmSetFuncOnCallRing(tmOnCallRing);
	tmSetFuncOnCallFail(tmOnCallFail);
	tmSetFuncOnCallAck(tmOnCallAck);
	tmSetFuncOnCallRelease(tmOnCallRelease);
	tmSetFuncOnAnswerCall(tmOnAnswerCall);
	tmSetFuncOnRefuseCall(tmOnRefuseCall);
	tmSetFuncOnMakeCall(tmOnMakeCall);
	tmSetFuncOnBandCall(tmOnBandCall);
  tmSetFuncOnBandVoice(tmOnBandVoice);
	tmSetFuncOnHoldCall(tmOnHoldCall);
	tmSetFuncOnUnHoldCall(tmOnUnHoldCall);
	tmSetFuncOnTransferCall(tmOnTransferCall);
  tmSetFuncOnStopTransfer(tmOnStopTransfer);
	tmSetFuncOnSwapCall(tmOnSwapCall);
	tmSetFuncOnPickupCall(tmOnPickupCall);
	tmSetFuncOnBreakinCall(tmOnBreakinCall);
	tmSetFuncOnListenCall(tmOnListenCall);
	tmSetFuncOnRemoveCall(tmOnRemoveCall);
	tmSetFuncOnThreeConf(tmOnThreeConf);
	tmSetFuncOnConsoleHold(tmOnConsoleHold);
	tmSetFuncOnConsolePickup(tmOnConsolePickup);
  tmSetFuncOnSendSwitchCMD(tmOnSendSwitchCMD);
  tmSetFuncOnCallConnected(tmOnCallConnected);
  tmSetFuncOnRecordResult(tmOnRecordResult);
  tmSetFuncOnPlayResult(tmOnPlayResult);
  tmSetFuncOnRecvDTMF(tmOnRecvDTMF);
  tmSetFuncOnQueryACDSplit(tmOnQueryACDSplit);
  tmSetFuncOnAgentLoggedOn(tmOnAgentLoggedOn);
  tmSetFuncOnAgentLoggedOff(tmOnAgentLoggedOff);
  tmSetFuncOnSetTranPhone(tmOnSetTranPhone);
}
