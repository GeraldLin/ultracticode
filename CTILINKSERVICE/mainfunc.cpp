//
//主處理函數部分
//

#include "stdafx.h"
#include "odsLog.h"
#include "vardef.h"
#include "main.h"
#include "mainfunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#define CONSOLE_RUN_MODE

//檢查文件是否存在
int MyCheckFileExist(const char *FilePath)
{
#ifdef WIN32 //win32環境
  CFileFind tempFind;
  
  BOOL IsFinded=(BOOL)tempFind.FindFile(FilePath); 
  if(IsFinded) 
  { 
    IsFinded=(BOOL)tempFind.FindNextFile(); 
    if(!tempFind.IsDots()) 
    { 
      if(!tempFind.IsDirectory()) 
      { 
        return 1;
      } 
    } 
  } 
  tempFind.Close(); 
  return 0; //文件不存在
#else //LINUX環境
  if (access(FilePath, F_OK) == 0)
    return 1;
  else
    return 0;
#endif
}

//-----------------------------------------------------------------------------
//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-CTILinkDLL消息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...)
{
  if ((g_bSaveId == TRUE && MsgType == 3) || MsgType == 0 || ((MsgType == 1 || MsgType == 2) && g_bSaveCommId == true))
  {
    char buf[2048], msgtype[16];
    char LogFile[256];
    FILE	*Ftrace;	//跟蹤日志文件
    SYSTEMTIME sysTime;
    
    va_list		ArgList;
    va_start(ArgList, lpszFormat);
    _vsnprintf(buf, 2048, lpszFormat, ArgList);
    va_end (ArgList);
    CString dispbuf;
    
    switch (MsgType)
    {
    case 0:
      strcpy(msgtype, "ALRM: ");
      break;
    case 1:
      strcpy(msgtype, "RECV: ");
      break;
    case 2:
      strcpy(msgtype, "SEND: ");
      break;
    case 3:
      strcpy(msgtype, "LINK: ");
      break;
    default:
      strcpy(msgtype, "DEBG: ");
      break;
    }

    GetLocalTime(&sysTime);
    
    sprintf(LogFile, "%s\\CTI_%04d%02d%02d.log", g_szLogPath, sysTime.wYear, sysTime.wMonth, sysTime.wDay);
    Ftrace = fopen(LogFile, "a+t");
    if (Ftrace != NULL)
    {
      fprintf(Ftrace, "%s >> %s%s\n", GetNowTime(), msgtype, buf);
    }
    if (Ftrace != NULL)
    {
      fclose(Ftrace);
      Ftrace = NULL;
    }
#ifdef CONSOLE_RUN_MODE
    printf("%s >> %s%s\n", GetNowTime(), msgtype, buf);
#endif
  }
}

char *GetNowTime() 
{
  SYSTEMTIME sysTime;
  static CH nowtime[25];
  GetLocalTime(&sysTime);
  
  sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
  return nowtime;
}
void CreateAllDirectories(CString strDir)
{
  // remove ending / if exists
  if (strDir.GetLength() == 0)
    return;
  if(strDir.Right(1)=="\\" || strDir.Right(1)=="/")
    strDir=strDir.Left(strDir.GetLength()-1);
  
  // base case . . .if directory exists
  if(GetFileAttributes(strDir)!=-1)
    return;
  
  // recursive call, one less directory
  int nFound1 = strDir.ReverseFind('\\');
  int nFound2 = strDir.ReverseFind('/');
  if (nFound1>nFound2)
    CreateAllDirectories(strDir.Left(nFound1));
  else
    CreateAllDirectories(strDir.Left(nFound2));
  
  // actual work
  if (strDir.GetLength() > 0)
  {
    CreateDirectory(strDir,NULL);
  }
}
void MyCriticalSectionLock()
{
  //g_cCriticalSection.Lock();
}
void MyCriticalSectionUnlock()
{
  //g_cCriticalSection.Unlock();
}

//收到IVR服務器數據消息事件回調函數
void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                      const unsigned char *buf,
                      unsigned long len)
{
  ((unsigned char *)buf)[len] = 0;
  MyTrace(1, "%s", (char *)buf);
  if (g_cMsgFifo.Write(remoteid, msgtype, (char *)buf) == 0)
    MyTrace(0, "接收消息緩沖溢出!");
}

//登錄IVR服務器成功事件回調函數
void OnIVRLogin(unsigned short serverid,unsigned short clientid)
{
  short nPortNum = 0;
  g_bConnectIVR = true;
#ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, "Connect IVRserver success!!!", EVENTLOG_INFORMATION_TYPE );
#endif
  MyTrace(0, "Connect IVRserver success!!!");

  if (g_nMainCTILinkServiceID == 1)
  {
    AddMsgHeader(SWTMSG_onconnectlink);
    AddIntItem(SWTMSG_onconnectlink, 2, 1);
    AddIntItem(SWTMSG_onconnectlink, 3, 1);
    AddStrItem(SWTMSG_onconnectlink, 4, "");
    AddMsgTail();
    SendMsg2IVR(SWTMSG_onconnectlink, g_cSendMsg.GetBuf());
    
    tmGetDevNum(1, nPortNum);
    AddMsgHeader(SWTMSG_ongetdevnnum);
    AddIntItem(SWTMSG_ongetdevnnum, 2, nPortNum);
    AddMsgTail();
    SendMsg2IVR(SWTMSG_ongetdevnnum, g_cSendMsg.GetBuf());
    
    Send_SWTMSG_ongetdevparam();
  }
  else
  {
    Send_SWTMSG_onlinkstatus();
  }
  tmConnectIVRStatus(g_cSwitchPortList.m_nSwitchID, 0);
}

//與IVR服務器斷開事件回調函數
void OnIVRClose(unsigned short serverid,unsigned short clientid)
{
  g_bConnectIVR = false;
#ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, "Disconnect IVRservice!!!", EVENTLOG_WARNING_TYPE );
#endif
  MyTrace(0, "Disconnect IVRservice");
  tmConnectIVRStatus(g_cSwitchPortList.m_nSwitchID, 1);
}

int Proc_Msg_From_IVR(CXMLMsg &IVRMsg)
{
  unsigned short MsgId=IVRMsg.GetMsgId();
  
  if(MsgId >= MAX_IVRSWTMSG_NUM)	//IVR-->SWITCH
  {
    //指令編號超出范圍
    MyTrace(0, "IVRmsgid = %d is out of range", MsgId);
    return 1;
  }
  
  if(0!=IVRMsg.ParseSndMsg(g_cCTLLinkRule.IVRSWTMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(0, "parse IVR msg error: MsgId = %d MsgBuf = %s", MsgId, IVRMsg.GetBuf().C_Str());
    return 2;
  }
  return 0;
}

void ProcRecvMsg()
{
  unsigned short count = 0, ClientId, MsgId;
  static char MsgBuf[2048];
  
  while(!g_cMsgFifo.IsEmpty() && count < 64) //一次最多處理64條信息
  {
    if (g_cMsgFifo.Read(ClientId, MsgId, MsgBuf) != 0)
    {
      g_cRecvMsg.SetMsgBufId(MsgId&0xFF, (char *)MsgBuf);  //裝載到Cstring中
      if (Proc_Msg_From_IVR(g_cRecvMsg) == 0)
      {
        Proc_SWICTH_MSG(g_cRecvMsg);
      }
    }
    count ++;
  }
}

//發送消息
void SendMsg2IVR(US MsgId, const char *msg)
{
  int MsgLen, SendLen;

  if (g_bConnectIVR == false)
  {
    return;
  }
  
  //if (MsgId != SWTMSG_ongetdevparam && MsgId != SWTMSG_onportstatus)
    MyTrace(2, "%s", msg);
  MsgLen = strlen(msg);
  SendLen = g_cTcpClient.IVRSendMessage((MSGTYPE_IVRSWT<<8)|MsgId, (unsigned char *)msg, MsgLen);
  if (SendLen != MsgLen)
    MyTrace(0, "發送消息錯誤!");
}
void SendMsg2IVR(US MsgId, const CStringX msg)
{
  SendMsg2IVR(MsgId, msg.C_Str());
}

//組合發送消息
void AddMsgHeader(US MsgId)
{
  g_cSendMsg.GetBuf().Format("<%s sessionid='%ld' switchid='%d'", 
    g_cCTLLinkRule.SWTIVRMsgRule[MsgId].MsgNameEng, g_cTcpClient.SessionId, g_cSwitchPortList.m_nSwitchID);
}
void AddStrItem(US MsgId, US AttrId, const char *value)
{
  g_cSendMsg.AddItemToBuf(g_cCTLLinkRule.SWTIVRMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void AddStrItem(US MsgId, US AttrId, const CString value)
{
  g_cSendMsg.AddItemToBuf(g_cCTLLinkRule.SWTIVRMsgRule[MsgId].Attribute[AttrId].AttrNameEng, (LPCTSTR)value);
}
void AddIntItem(US MsgId, US AttrId, int value)
{
  g_cSendMsg.AddIntItemToBuf(g_cCTLLinkRule.SWTIVRMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void AddLongItem(US MsgId, US AttrId, long value)
{
  g_cSendMsg.AddLongItemToBuf(g_cCTLLinkRule.SWTIVRMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void AddMsgTail()
{
  g_cSendMsg.AddTailToBuf();
}

void Send_SWTMSG_ongetdevparam()
{
  char pszDeviceID[32];
  short nPortNum = 0;
  short nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex;

  tmGetDevNum(1, nPortNum);
  g_cSwitchPortList.InitData(nPortNum);
  for (short nPortID = 0; nPortID < nPortNum; nPortID ++)
  {
    if (tmGetDevParam(nPortID, pszDeviceID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex) == 0)
    {
      g_cSwitchPortList.SetPortData(nPortID, pszDeviceID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex, 0);
      AddMsgHeader(SWTMSG_ongetdevparam);
      AddIntItem(SWTMSG_ongetdevparam, 2, nPortID);
      AddStrItem(SWTMSG_ongetdevparam, 3, pszDeviceID);
      AddIntItem(SWTMSG_ongetdevparam, 4, nChStyle);
      AddIntItem(SWTMSG_ongetdevparam, 5, nChStyleIndex);
      AddIntItem(SWTMSG_ongetdevparam, 6, nChType);
      AddIntItem(SWTMSG_ongetdevparam, 7, nChIndex);
      AddIntItem(SWTMSG_ongetdevparam, 8, nLgChType);
      AddIntItem(SWTMSG_ongetdevparam, 9, nLgChIndex);
      AddMsgTail();
      SendMsg2IVR(SWTMSG_ongetdevparam, g_cSendMsg.GetBuf());
    }
  }
}

void Send_SWTMSG_onlinkstatus()
{
  char pszDeviceID[32];
  short nPortNum = 0;
  short nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex;
  
  tmGetDevNum(1, nPortNum);
  g_cSwitchPortList.InitData(nPortNum);
  for (short nPortID = 0; nPortID < nPortNum; nPortID ++)
  {
    if (tmGetDevParam(nPortID, pszDeviceID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex) == 0)
    {
      g_cSwitchPortList.SetPortData(nPortID, pszDeviceID, nChStyle, nChStyleIndex, nChType, nChIndex, nLgChType, nLgChIndex, 0);
    }
  }
  AddMsgHeader(SWTMSG_onlinkstatus);
  AddIntItem(SWTMSG_onlinkstatus, 2, g_cSwitchPortList.m_pSwitchPort[0].m_nPortID);
  AddIntItem(SWTMSG_onlinkstatus, 3, g_nCTILinkstatus);
  AddStrItem(SWTMSG_onlinkstatus, 4, g_cSwitchPortList.m_pSwitchPort[0].m_szDeviceID);
  AddMsgTail();
  SendMsg2IVR(SWTMSG_onlinkstatus, g_cSendMsg.GetBuf());
}

void Send_AllPortState()
{
//   short nPortNum = 0;
// 
//   tmGetDevNum(1, nPortNum);
//   for (short nPortID = 0; nPortID < nPortNum; nPortID ++)
//   {
//     if (g_cSwitchPortList.m_pSwitchPort[nPortID].m_nValid == 1)
//     {
//       AddMsgHeader(SWTMSG_onportstatus);
//       AddIntItem(SWTMSG_onportstatus, 2, nPortID);
//       AddStrItem(SWTMSG_onportstatus, 3, "");
//       AddIntItem(SWTMSG_onportstatus, 4, 0);
//       AddMsgTail();
//       SendMsg2IVR(SWTMSG_onportstatus, g_cSendMsg.GetBuf());
//     }
//   }
}

//自定義一個函數指針類型
typedef int (*Proc_SwitchMSG)(CXMLMsg &SWTMsg);
Proc_SwitchMSG proc_switchmsg[]=
{
  Proc_SWTMSG_connectlink,	//連接CTILinkService
  
  Proc_SWTMSG_setportstatus,	//設置端口狀態
  Proc_SWTMSG_setagentstatus,	//設置坐席狀態
  Proc_SWTMSG_setroomstatus,	//設置房間狀態
  Proc_SWTMSG_setlampstatus,	//設置留言指示燈
  Proc_SWTMSG_setwakestatus,	//設置叫醒
  
  Proc_SWTMSG_setcalllevel,	//設置呼出權限
  Proc_SWTMSG_settranphone,	//設置轉移號碼
  
  Proc_SWTMSG_answercall,	//應答來話
  Proc_SWTMSG_refusecall,	//拒絕來話
  Proc_SWTMSG_makecall,	//發起呼叫
  Proc_SWTMSG_bandcall,	//將來中繼來話綁定到分機
  Proc_SWTMSG_bandvoice,	//將來中繼來話綁定到提示語音
  Proc_SWTMSG_holdcall,	//保持電話
  Proc_SWTMSG_unholdcall,	//取消保持電話
  Proc_SWTMSG_transfercall,	//轉接電話
  Proc_SWTMSG_stoptransfer,	//停止轉接電話
  Proc_SWTMSG_swapcall,	//穿梭通話
  Proc_SWTMSG_pickupcall,	//代接電話
  Proc_SWTMSG_breakincall,	//強插通話
  Proc_SWTMSG_listencall,	//監聽電話
  Proc_SWTMSG_removecall,	//強拆電話
  Proc_SWTMSG_threeconf,	//三方通話
  Proc_SWTMSG_releasecall,	//釋放呼叫
  Proc_SWTMSG_hangup,	//分機掛機
  Proc_SWTMSG_putacdqueuecall,	//將電話保持到話務臺隊列
  Proc_SWTMSG_getacdqueuecall,	//從話務臺隊列提取電話
  Proc_SWTMSG_sendswitchcmd,	//發送交換機操作指令
  Proc_SWTMSG_senddtmf,	//發送DTMF指令
  Proc_SWTMSG_takeovercall,	//接管電話
  Proc_SWTMSG_redirectcall,	//電話重新定向振鈴
  Proc_SWTMSG_addtoconference,	//加入會議
  Proc_SWTMSG_removefromconference,	//退出會議
  Proc_SWTMSG_sendivrstatus, //發送IVR節點的狀態到交換機
  Proc_SWTMSG_sendswitchover, //發送主備切換標志
  Proc_SWTMSG_recordfile,	//文件錄音 //ver:3.2版本修改
  Proc_SWTMSG_stoprecord,	//停止錄音
  Proc_SWTMSG_playfile,	//文件放音
  Proc_SWTMSG_playindex,	//索引放音
  Proc_SWTMSG_stopplay,	//停止放音
};

void Proc_SWICTH_MSG(CXMLMsg &XMLMsg)
{
  if (XMLMsg.GetMsgId() < MAX_IVRSWTMSG_NUM)
  {
    proc_switchmsg[XMLMsg.GetMsgId()](XMLMsg);
  }
}

//連接CTILinkService
int Proc_SWTMSG_connectlink(CXMLMsg &SWTMsg)
{
  return 0;
}
//設置端口狀態
int Proc_SWTMSG_setportstatus(CXMLMsg &SWTMsg)
{
  return 0;
}
//設置坐席狀態
int Proc_SWTMSG_setagentstatus(CXMLMsg &SWTMsg)
{
  tmSetAgentStatus(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), SWTMsg.GetAttrValue(4).C_Str(), SWTMsg.GetAttrValue(5).C_Str(), SWTMsg.GetAttrValue(6).C_Str(), atoi(SWTMsg.GetAttrValue(7).C_Str()));
  return 0;
}
//設置房間狀態	
int Proc_SWTMSG_setroomstatus(CXMLMsg &SWTMsg)
{
  tmSetPortStatus(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()));
  return 0;
}
//設置留言指示燈
int Proc_SWTMSG_setlampstatus(CXMLMsg &SWTMsg)
{
  tmSetLampStatus(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()));
  return 0;
}
//設置叫醒
int Proc_SWTMSG_setwakestatus(CXMLMsg &SWTMsg)
{
  tmSetWakeStatus(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), SWTMsg.GetAttrValue(4).C_Str());
  return 0;
}
//設置呼出權限
int Proc_SWTMSG_setcalllevel(CXMLMsg &SWTMsg)
{
  tmSetCallLevel(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()));
  return 0;
}
//設置轉移號碼
int Proc_SWTMSG_settranphone(CXMLMsg &SWTMsg)
{
  int nResult = tmSetTranPhone(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()), SWTMsg.GetAttrValue(5).C_Str());
  if (nResult != 0)
  {
    tmOnSetTranPhone(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()), 0, SWTMsg.GetAttrValue(5).C_Str(), 1);
  }
  else
  {
    tmOnSetTranPhone(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()), 0, SWTMsg.GetAttrValue(5).C_Str(), 0);
  }
  return 0;
}
//應答來話
int Proc_SWTMSG_answercall(CXMLMsg &SWTMsg)
{
  int nResult = tmAnswerCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 0);
  if (nResult != 0)
  {
    tmOnAnswerCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//拒絕來話
int Proc_SWTMSG_refusecall(CXMLMsg &SWTMsg)
{
  int nResult = tmRefuseCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()));
  if (nResult != 0)
  {
    tmOnRefuseCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//發起呼叫
int Proc_SWTMSG_makecall(CXMLMsg &SWTMsg)
{
  int nResult = tmMakeCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    SWTMsg.GetAttrValue(5).C_Str(), SWTMsg.GetAttrValue(6).C_Str(), atoi(SWTMsg.GetAttrValue(7).C_Str()), SWTMsg.GetAttrValue(8).C_Str());
  if (nResult != 0)
  {
    tmOnMakeCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 0, 1);
  }
  return 0;
}
//將來中繼來話綁定到分機
int Proc_SWTMSG_bandcall(CXMLMsg &SWTMsg)
{
  int nResult = tmBandCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  if (nResult != 0)
  {
    tmOnBandCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//將來中繼來話綁定到提示語音
int Proc_SWTMSG_bandvoice(CXMLMsg &SWTMsg)
{
  int nResult = tmBandVoice(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), atoi(SWTMsg.GetAttrValue(5).C_Str()));
  if (nResult != 0)
  {
    tmOnBandVoice(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//保持電話
int Proc_SWTMSG_holdcall(CXMLMsg &SWTMsg)
{
  int nResult = tmHoldCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()));
  if (nResult != 0)
  {
    tmOnHoldCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//取消保持電話
int Proc_SWTMSG_unholdcall(CXMLMsg &SWTMsg)
{
  int nResult = tmUnHoldCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()));
  if (nResult != 0)
  {
    tmOnUnHoldCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//轉接電話
int Proc_SWTMSG_transfercall(CXMLMsg &SWTMsg)
{
  int nResult = tmTransferCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str(), atoi(SWTMsg.GetAttrValue(7).C_Str()), SWTMsg.GetAttrValue(8).C_Str());
  if (nResult != 0)
  {
    tmOnTransferCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), 
      atol(SWTMsg.GetAttrValue(4).C_Str()), 
      atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str(), 1);
  }
  return 0;
}
//停止轉接電話
int Proc_SWTMSG_stoptransfer(CXMLMsg &SWTMsg)
{
  int nResult = tmStopTransfer(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()));
  if (nResult != 0)
  {
    tmOnStopTransfer(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//穿梭通話
int Proc_SWTMSG_swapcall(CXMLMsg &SWTMsg)
{
  int nResult = tmSwapCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()));
  if (nResult != 0)
  {
    tmOnSwapCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//代接電話
int Proc_SWTMSG_pickupcall(CXMLMsg &SWTMsg)
{
  int nResult = tmPickupCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  if (nResult != 0)
  {
    tmOnPickupCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//強插通話
int Proc_SWTMSG_breakincall(CXMLMsg &SWTMsg)
{
  int nResult = tmBreakinCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  if (nResult != 0)
  {
    tmOnBreakinCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//監聽電話
int Proc_SWTMSG_listencall(CXMLMsg &SWTMsg)
{
  int nResult = tmListenCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  if (nResult != 0)
  {
    tmOnListenCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//強拆電話
int Proc_SWTMSG_removecall(CXMLMsg &SWTMsg)
{
  int nResult = tmRemoveCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  if (nResult != 0)
  {
    tmOnRemoveCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//三方通話
int Proc_SWTMSG_threeconf(CXMLMsg &SWTMsg)
{
  int nResult = tmThreeConf(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atol(SWTMsg.GetAttrValue(5).C_Str()));
  if (nResult != 0)
  {
    tmOnThreeConf(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 1);
  }
  return 0;
}
//釋放呼叫
int Proc_SWTMSG_releasecall(CXMLMsg &SWTMsg)
{
  tmReleaseCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()));
  return 0;
}
//分機掛機
int Proc_SWTMSG_hangup(CXMLMsg &SWTMsg)
{
  tmHangup(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str());
  return 0;
}
//將電話保持到話務臺隊列
int Proc_SWTMSG_putacdqueuecall(CXMLMsg &SWTMsg)
{
  return 0;
}
//從話務臺隊列提取電話
int Proc_SWTMSG_getacdqueuecall(CXMLMsg &SWTMsg)
{
  return 0;
}
//發送交換機操作指令
int Proc_SWTMSG_sendswitchcmd(CXMLMsg &SWTMsg)
{
  int nResult = tmSendSwitchCMD(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()));
  if (nResult != 0)
  {
    tmOnSendSwitchCMD(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), 1);
  }
  return 0;
}
//發送DTMF指令
int Proc_SWTMSG_senddtmf(CXMLMsg &SWTMsg)
{
  tmSendDTMF(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()), SWTMsg.GetAttrValue(5).C_Str());
  return 0;
}
//接管電話
int Proc_SWTMSG_takeovercall(CXMLMsg &SWTMsg)
{
  tmTakeOverCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  return 0;
}
//電話重新定向振鈴
int Proc_SWTMSG_redirectcall(CXMLMsg &SWTMsg)
{
  tmRedirectCall(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), SWTMsg.GetAttrValue(6).C_Str());
  return 0;
}
//加入會議
int Proc_SWTMSG_addtoconference(CXMLMsg &SWTMsg)
{
  return 0;
}
//退出會議
int Proc_SWTMSG_removefromconference(CXMLMsg &SWTMsg)
{
  return 0;
}
//發送IVR節點的狀態到交換機
int Proc_SWTMSG_sendivrstatus(CXMLMsg &SWTMsg)
{
  tmSendIVRStatus(atoi(SWTMsg.GetAttrValue(1).C_Str()), SWTMsg.GetAttrValue(2).C_Str(), atoi(SWTMsg.GetAttrValue(3).C_Str()));
  return 0;
}
//發送主備切換標志
int Proc_SWTMSG_sendswitchover(CXMLMsg &SWTMsg)
{
  tmSendSwitchover(atoi(SWTMsg.GetAttrValue(1).C_Str()), atoi(SWTMsg.GetAttrValue(2).C_Str()),
    atoi(SWTMsg.GetAttrValue(3).C_Str()), atoi(SWTMsg.GetAttrValue(4).C_Str()),
    SWTMsg.GetAttrValue(5).C_Str());
  return 0;
}
//ver:3.2版本增加
//文件錄音
int Proc_SWTMSG_recordfile(CXMLMsg &SWTMsg)
{
  int nResult = tmRecordFile(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()),
    SWTMsg.GetAttrValue(5).C_Str(), atoi(SWTMsg.GetAttrValue(6).C_Str()), atoi(SWTMsg.GetAttrValue(7).C_Str()), SWTMsg.GetAttrValue(8).C_Str());
  if (nResult != 0)
  {
    tmOnRecordResult(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 10, "", 0, 0);
  }
  return 0;
}
//停止錄音
int Proc_SWTMSG_stoprecord(CXMLMsg &SWTMsg)
{
  tmStopRecord(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()));
  return 0;
}
//文件放音
int Proc_SWTMSG_playfile(CXMLMsg &SWTMsg)
{
  int nResult = tmPlayFile(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()),
    SWTMsg.GetAttrValue(5).C_Str(), atoi(SWTMsg.GetAttrValue(6).C_Str()), SWTMsg.GetAttrValue(7).C_Str());
  if (nResult != 0)
  {
    tmOnPlayResult(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 10, "");
  }
  return 0;
}
//索引放音
int Proc_SWTMSG_playindex(CXMLMsg &SWTMsg)
{
  int nResult = tmPlayIndex(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()), atoi(SWTMsg.GetAttrValue(6).C_Str()), SWTMsg.GetAttrValue(7).C_Str());
  if (nResult != 0)
  {
    tmOnPlayResult(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atol(SWTMsg.GetAttrValue(4).C_Str()), 10, "");
  }
  return 0;
}
//停止放音
int Proc_SWTMSG_stopplay(CXMLMsg &SWTMsg)
{
  tmStopPlay(atoi(SWTMsg.GetAttrValue(2).C_Str()), SWTMsg.GetAttrValue(3).C_Str(), atoi(SWTMsg.GetAttrValue(4).C_Str()),
    atoi(SWTMsg.GetAttrValue(5).C_Str()));
  return 0;
}

//-----------------------------------------------------------------------------
int InitCTIDLL()
{
  SetAllCallBackFunc();
  tmConnectSwitch(g_szCTIConfigFileName);
  return 0;
}

void CloseCTIDLL()
{
  tmDisConnectSwitch(0);
}

int InitTcpLink()
{
  if (g_nINIFileType == 0)
    g_cTcpClient.ReadIni(g_szCTIConfigFileName);
  else
    g_cTcpClient.ReadWebSetupIni(g_szCTIConfigFileName);
  
  //初始化服務端
  if(g_cTcpClient.ConnectServer())
  {
    return 1;
  }
  MyTrace(0, "Connecting IVRSERVICE ...");
  return 0;
}

void CloseTcpLink()
{
  g_cTcpClient.AppCloseAll();
}

bool Init()
{
  int nInitResult=0;
  TCHAR szFull[_MAX_PATH];
  TCHAR szDrive[_MAX_DRIVE];
  TCHAR szDir[_MAX_DIR];
  GetModuleFileName(NULL, szFull, sizeof(szFull)/sizeof(TCHAR));
  _tsplitpath(szFull, szDrive, szDir, NULL, NULL);
  _tcscpy(szFull, szDrive);
  _tcscat(szFull, szDir);
  strcpy(g_szAppPath, szFull);
  
  memset(g_szRootPath, 0, MAX_PATH_LEN);
  strncpy(g_szRootPath, g_szAppPath, strlen(g_szAppPath)-16);

  sprintf(g_szUnimeINIFileName, "%s\\unimeconfig.ini", g_szRootPath);
  sprintf(g_szCTIConfigFileName, "%sCTILINKSERVICE.ini", g_szAppPath);
  
  if (MyCheckFileExist(g_szCTIConfigFileName) == 0)
  {
    g_nINIFileType = 1;
  }
  else
  {
    g_nINIFileType = GetPrivateProfileInt("QUARKCALL","INIType", 0, g_szCTIConfigFileName);
  }
  if (g_nINIFileType == 1)
  {
    sprintf(g_szCTIConfigFileName, g_szUnimeINIFileName);
  }

#ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "Init Msg: CTILINK INIFile=%s", g_szCTIConfigFileName);
#endif

  //g_nMainCTILinkServiceID = GetPrivateProfileInt("IVRSERVER", "MainCTILinkServiceID", 1, g_szCTIConfigFileName);
  g_nTAPICTILinkType = GetPrivateProfileInt("PORT", "TAPICTILinkType", 1, g_szCTIConfigFileName);
  
  //讀記錄日志參數設置
  if (g_nINIFileType == 0)
  {
    GetPrivateProfileString("LOG", "LogPath", "", g_szLogPath, 128, g_szCTIConfigFileName);
    if (GetPrivateProfileInt("LOG", "SaveId", 0, g_szCTIConfigFileName) == 1)
    {
      g_bSaveId = true;
    }
    if (GetPrivateProfileInt("LOG", "CommId", 0, g_szCTIConfigFileName) == 1)
    {
      g_bSaveCommId = true;
    }
    if (GetPrivateProfileInt("LOG", "LinkId", 0, g_szCTIConfigFileName) == 1)
    {
      g_bSaveLinkId = true;
    }
  }
  else
  {
    GetPrivateProfileString("CTILINKLOG", "LogPath", "", g_szLogPath, 128, g_szCTIConfigFileName);
    if (GetPrivateProfileInt("CTILINKLOG", "SaveId", 0, g_szCTIConfigFileName) == 1)
    {
      g_bSaveId = true;
    }
    if (GetPrivateProfileInt("CTILINKLOG", "CommId", 0, g_szCTIConfigFileName) == 1)
    {
      g_bSaveCommId = true;
    }
    if (GetPrivateProfileInt("CTILINKLOG", "LinkId", 0, g_szCTIConfigFileName) == 1)
    {
      g_bSaveLinkId = true;
    }
  }
  if (strlen(g_szLogPath)==0)
    sprintf(g_szLogPath, "%s\\msg", g_szAppPath);
  CreateAllDirectories((CString)g_szLogPath);

  if (InitCTIDLL() != 0)
  {
    nInitResult = 1;
  }
  if (InitTcpLink() != 0)
  {
    nInitResult = 1;
  }
  if (nInitResult == 0)
  {
    g_bInitSuccId = true;
  }
  return g_bInitSuccId;
}

void APPLoop()
{
  if (g_bInitSuccId == true)
  {
    if(g_cTcpClient.IsOpen)
      g_cTcpClient.AppMainLoop();
    ProcRecvMsg();
    MyCriticalSectionLock();
    //if (g_bConnectIVR == true)
      tmMainLoop();
    MyCriticalSectionUnlock();
  }	
}
