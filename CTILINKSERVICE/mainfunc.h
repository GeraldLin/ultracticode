//---------------------------------------------------------------------------
#ifndef MainfuncH
#define MainfuncH
//---------------------------------------------------------------------------
//取當前日期時間字符串
char *GetNowTime();
void CreateAllDirectories(CString strDir);

void MyCriticalSectionLock();
void MyCriticalSectionUnlock();

//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-流程指令 4-變量跟蹤 5-內部調試信息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

void OnIVRLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnIVRClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function

void SendMsg2IVR(US MsgId, const char *msg);
void SendMsg2IVR(US MsgId, const CStringX msg);
void AddMsgHeader(US MsgId);
void AddStrItem(US MsgId, US AttrId, const char *value);
void AddStrItem(US MsgId, US AttrId, const CString value);
void AddIntItem(US MsgId, US AttrId, int value);
void AddLongItem(US MsgId, US AttrId, long value);
void AddMsgTail();
void Send_SWTMSG_ongetdevparam();
void Send_SWTMSG_onlinkstatus();
void Send_AllPortState();

void Proc_SWICTH_MSG(CXMLMsg &XMLMsg); //處理消息

//處理從IVR接收的消息
void ProcRecvMsg();

int Proc_SWTMSG_connectlink(CXMLMsg &SWTMsg);	//連接CTILinkService

int Proc_SWTMSG_setportstatus(CXMLMsg &SWTMsg);	//設置端口狀態
int Proc_SWTMSG_setagentstatus(CXMLMsg &SWTMsg);	//設置坐席狀態
int Proc_SWTMSG_setroomstatus(CXMLMsg &SWTMsg);	//設置房間狀態
int Proc_SWTMSG_setlampstatus(CXMLMsg &SWTMsg);	//設置留言指示燈
int Proc_SWTMSG_setwakestatus(CXMLMsg &SWTMsg);	//設置叫醒

int Proc_SWTMSG_setcalllevel(CXMLMsg &SWTMsg);	//設置呼出權限
int Proc_SWTMSG_settranphone(CXMLMsg &SWTMsg);	//設置轉移號碼

int Proc_SWTMSG_answercall(CXMLMsg &SWTMsg);	//應答來話
int Proc_SWTMSG_refusecall(CXMLMsg &SWTMsg);	//拒絕來話
int Proc_SWTMSG_makecall(CXMLMsg &SWTMsg);	//發起呼叫
int Proc_SWTMSG_bandcall(CXMLMsg &SWTMsg);	//將來中繼來話綁定到分機
int Proc_SWTMSG_bandvoice(CXMLMsg &SWTMsg);	//將來中繼來話綁定到提示語音
int Proc_SWTMSG_holdcall(CXMLMsg &SWTMsg);	//保持電話
int Proc_SWTMSG_unholdcall(CXMLMsg &SWTMsg);	//取消保持電話
int Proc_SWTMSG_transfercall(CXMLMsg &SWTMsg);	//轉接電話
int Proc_SWTMSG_stoptransfer(CXMLMsg &SWTMsg);	//停止轉接電話
int Proc_SWTMSG_swapcall(CXMLMsg &SWTMsg);	//穿梭通話
int Proc_SWTMSG_pickupcall(CXMLMsg &SWTMsg);	//代接電話
int Proc_SWTMSG_breakincall(CXMLMsg &SWTMsg);	//強插通話
int Proc_SWTMSG_listencall(CXMLMsg &SWTMsg);	//監聽電話
int Proc_SWTMSG_removecall(CXMLMsg &SWTMsg);	//強拆電話
int Proc_SWTMSG_threeconf(CXMLMsg &SWTMsg);	//三方通話
int Proc_SWTMSG_releasecall(CXMLMsg &SWTMsg);	//釋放呼叫
int Proc_SWTMSG_hangup(CXMLMsg &SWTMsg);	//分機掛機
int Proc_SWTMSG_putacdqueuecall(CXMLMsg &SWTMsg);	//將電話保持到話務臺隊列
int Proc_SWTMSG_getacdqueuecall(CXMLMsg &SWTMsg);	//從話務臺隊列提取電話
int Proc_SWTMSG_sendswitchcmd(CXMLMsg &SWTMsg);	//發送交換機操作指令
int Proc_SWTMSG_senddtmf(CXMLMsg &SWTMsg);	//發送DTMF指令

int Proc_SWTMSG_takeovercall(CXMLMsg &SWTMsg);	//接管電話
int Proc_SWTMSG_redirectcall(CXMLMsg &SWTMsg);	//電話重新定向振鈴
int Proc_SWTMSG_addtoconference(CXMLMsg &SWTMsg);	//加入會議
int Proc_SWTMSG_removefromconference(CXMLMsg &SWTMsg);	//退出會議

int Proc_SWTMSG_sendivrstatus(CXMLMsg &SWTMsg);	//發送IVR節點的狀態到交換機
int Proc_SWTMSG_sendswitchover(CXMLMsg &SWTMsg);	//發送主備切換標志

int Proc_SWTMSG_routeselectinv(CXMLMsg &SWTMsg);	//發送選擇外部路由給交換機

int Proc_SWTMSG_recordfile(CXMLMsg &SWTMsg);	//文件錄音 //ver:3.2版本修改
int Proc_SWTMSG_stoprecord(CXMLMsg &SWTMsg);	//停止錄音
//ver:3.2版本增加
int Proc_SWTMSG_playfile(CXMLMsg &SWTMsg);	//文件放音
int Proc_SWTMSG_playindex(CXMLMsg &SWTMsg);	//索引放音
int Proc_SWTMSG_stopplay(CXMLMsg &SWTMsg);	//停止放音

//初始化CTIDLL
int InitCTIDLL();

//初始化TTS代理與IVR的連接
int InitTcpLink();

//關閉CTIDLL資源
void CloseCTIDLL();

//斷開CTIDLL與IVR的連接
void CloseTcpLink();

bool Init();
void APPLoop();

//---------------------------------------------------------------------------
#endif
