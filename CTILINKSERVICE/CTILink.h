//-----------------------------------------------------------------------------
#ifndef __CTILINK_H_
#define __CTILINK_H_
//-----------------------------------------------------------------------------
/*! @file  
********************************************************************************  
<PRE>  
模塊名       : CTILinkDLL  
文件名       : CTILink.h    
相關文件     :   
文件實現功能 :   
作者         : zgj  
版本         : 1.0  
--------------------------------------------------------------------------------  
備注         :   
--------------------------------------------------------------------------------  
修改記錄 :   
日 期        版本     修改人              修改內容  
2010/05/15   1.0      zgj                 創建了此文件  
</PRE>  
*******************************************************************************/  

// #ifdef __cplusplus
// extern "C" {
// #endif

/******************************************************************************
//函數名：ConnectSwitch
//作者： zgj
//日期： 2010-05-15
//功能： 連接交換機
//輸入參數：

//返回值： 

//修改記錄：
//日 期        版本     修改人              修改內容

******************************************************************************/
//函數定義如下：
int WINAPI tmConnectSwitch(LPCTSTR pszINIFileName);
//=============================================================================
int WINAPI tmDisConnectSwitch(short nSwitchId);
int WINAPI tmGetDevNum(short nSwitchId, short &nPortNum);
int WINAPI tmGetDevParam(short nPortID, char *pszDeviceID, short &nChStyle, short &nChStyleIndex, short &nChType, short &nChIndex, short &nLgChType, short &nLgChIndex);
int WINAPI tmSetPortStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
int WINAPI tmSetAgentStatus(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, LPCTSTR pszPassword, short nStatus);
int WINAPI tmSetRoomStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
int WINAPI tmSetLampStatus(short nPortID, LPCTSTR pszDeviceID, short nStatus);
int WINAPI tmSetWakeStatus(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszWakeTime);
int WINAPI tmSetCallLevel(short nPortID, LPCTSTR pszDeviceID, short nCallLevel);
int WINAPI tmSetTranPhone(short nPortID, LPCTSTR pszDeviceID, short nTranType, LPCTSTR pszTranPhone);
int WINAPI tmAnswerCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nAnsparam);
int WINAPI tmRefuseCall(short nPortID, LPCTSTR pszDeviceID, long nConnID);
int WINAPI tmMakeCall(short nPortID, LPCTSTR pszDeviceID, long nCallID, LPCTSTR pszCallerID, LPCTSTR pszCalledID, short nRouteNo, LPCTSTR pszCallData);
int WINAPI tmBandCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmBandVoice(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nVoiceID);
int WINAPI tmHoldCall(short nPortID, LPCTSTR pszDeviceID, long nConnID);
int WINAPI tmUnHoldCall(short nPortID, LPCTSTR pszDeviceID, long nConnID);
int WINAPI tmTransferCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nTransferType, LPCTSTR pszCallData);
int WINAPI tmStopTransfer(short nPortID, LPCTSTR pszDeviceID, long nConnID);
int WINAPI tmSwapCall(short nPortID, LPCTSTR pszDeviceID, long nConnID);
int WINAPI tmPickupCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmBreakinCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmListenCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmRemoveCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmThreeConf(short nPortID, LPCTSTR pszDeviceID, long nActConnID, long nHeldConnID);
int WINAPI tmReleaseCall(short nPortID, LPCTSTR pszDeviceID, long nConnID);
int WINAPI tmHangup(short nPortID, LPCTSTR pszDeviceID);
int WINAPI tmGetConsoleQueueCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nQueueNo);
int WINAPI tmPutConsoleQueueCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nQueueNo);
int WINAPI tmSendSwitchCMD(short nPortID, LPCTSTR pszDeviceID, short nCmdID);
int WINAPI tmSendDTMF(short nPortID, LPCTSTR pszDeviceID, long nConnID, LPCTSTR pszDTMF);
int WINAPI tmTakeOverCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmRedirectCall(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID);
int WINAPI tmAddToConf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, long nDesConnID, long nConfID);
int WINAPI tmRemoveFromConf(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, long nDesConnID, long nConfID);
int WINAPI tmSendIVRStatus(short nSwitchId, LPCTSTR pszIVRHostId, short nStatus);
int WINAPI tmSendSwitchover(short nSwitchId, short nMasterId, short nActiveId, short nReasonId, LPCTSTR pszReasonMsg);
//ver:3.2版本增加
int WINAPI tmRecordFile(short nPortID, LPCTSTR pszDeviceID, long nConnID, LPCTSTR pszFileName, long nRecdLength, short nRecdParam, LPCTSTR pszRule);
int WINAPI tmStopRecord(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nStopFlag);
int WINAPI tmPlayFile(short nPortID, LPCTSTR pszDeviceID, long nConnID, LPCTSTR pszFileName, short nPlayParam, LPCTSTR pszRule);
int WINAPI tmPlayIndex(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nIndex, short nPlayParam, LPCTSTR pszRule);
int WINAPI tmStopPlay(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nStopFlag);
int WINAPI tmConnectIVRStatus(short nSwitchId, short nStatus);

//設置事件回調函數
void WINAPI tmSetFuncOnConnect(void (*tmonConnectSwitch)(short nSwitchId, short nSwitchType, short nResult, LPCTSTR pszError));
void WINAPI tmSetFuncOnLinkStatus(void (*tmonLinkStatus)(short nSwitchId, short nLinkId, short nStatus, LPCTSTR pszError));
void WINAPI tmSetFuncOnGetDevNum(void (*tmonGetDevNum)(short nSwitchId, short nPortNum));
void WINAPI tmSetFuncOnGetDevParam(void (*tmonGetDevParam)(short nPortID, const char *pszDeviceID, short nChStyle, short nChStyleIndex, short nChType, short nChIndex, short nLgChType, short nLgChIndex));
void WINAPI tmSetFuncOnPortStatus(void (*tmonPortStatus)(short nPortID, LPCTSTR pszDeviceID, short nStatus));
void WINAPI tmSetFuncOnAgentStatus(void (*tmonAgentStatus)(short nPortID, LPCTSTR pszAgentID, short nStatus));
void WINAPI tmSetFuncOnRoomStatus(void (*tmonRoomStatus)(short nPortID, LPCTSTR pszDeviceID, short nStatus));
void WINAPI tmSetFuncOnLampStatus(void (*tmonLampStatus)(short nPortID, LPCTSTR pszDeviceID, short nStatus));
void WINAPI tmSetFuncOnWakeStatus(void (*tmonWakeStatus)(short nPortID, LPCTSTR pszDeviceID, short nStatus));
void WINAPI tmSetFuncOnACDQueueStatus(void (*tmonACDQueueStatus)(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszQueue));
void WINAPI tmSetFuncOnRecvBill(void (*tmonRecvBill)(short nSwitchId, LPCTSTR pszBillRecord));
void WINAPI tmSetFuncOnCCBStatus(void (*tmonCCBStatus)(short nConnID, short nStatus));
void WINAPI tmSetFuncOnCallEvent(void (*tmonCallEvent)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nStatus, short nDirect, short nDesPortID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, LPCTSTR pszOrgCallerNo, LPCTSTR pszOrgCalledNo, LPCTSTR pszCallData, LPCTSTR pszCustomPhone, short nCallType));
void WINAPI tmSetFuncOnCallSam(void (*tmonCallSam)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, short nSamType, LPCTSTR pszCallData, LPCTSTR pszCustomPhone));
void WINAPI tmSetFuncOnCallAcm(void (*tmonCallAcm)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo));
void WINAPI tmSetFuncOnCallRing(void (*tmonCallRing)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult));
void WINAPI tmSetFuncOnCallFail(void (*tmonCallFail)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult));
void WINAPI tmSetFuncOnCallAck(void (*tmonCallAck)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult, LPCTSTR pszCallerNo, LPCTSTR pszCalledNo, LPCTSTR pszCallData, LPCTSTR pszCustomPhone));
void WINAPI tmSetFuncOnCallRelease(void (*tmonCallRelease)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult));
void WINAPI tmSetFuncOnAnswerCall(void (*tmonAnswerCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnRefuseCall(void (*tmonRefuseCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnMakeCall(void (*tmonMakeCall)(short nPortID, LPCTSTR pszDeviceID, long nCallID, long nConnID, short nResult));
void WINAPI tmSetFuncOnBandCall(void (*tmonBandCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnBandVoice(void (*tmonBandVoice)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnHoldCall(void (*tmonHoldCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnUnHoldCall(void (*tmonUnHoldCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnTransferCall(void (*tmonTransferCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult));
void WINAPI tmSetFuncOnStopTransfer(void (*tmonStopTransfer)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnSwapCall(void (*tmonSwapCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnPickupCall(void (*tmonPickupCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnBreakinCall(void (*tmonBreakinCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnListenCall(void (*tmonListenCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnRemoveCall(void (*tmonRemoveCall)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnThreeConf(void (*tmonThreeConf)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnConsoleHold(void (*tmonConsoleHold)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnConsolePickup(void (*tmonConsolePickup)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult));
void WINAPI tmSetFuncOnSendSwitchCMD(void (*tmonSendSwitchCMD)(short nPortID, LPCTSTR pszDeviceID, short nResult));
void WINAPI tmSetFuncOnCallConnected(void (*tmonCallConnected)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nDesPortID, LPCTSTR pszDesDeviceID, short nResult));
//ver:3.2版本增加
void WINAPI tmSetFuncOnRecordResult(void (*tmOnRecordResult)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszReason, long nRecBytes, long nRecTimeLen));
void WINAPI tmSetFuncOnPlayResult(void (*tmOnPlayResult)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszReason));
void WINAPI tmSetFuncOnRecvDTMF(void (*tmOnRecvDTMF)(short nPortID, LPCTSTR pszDeviceID, long nConnID, short nResult, LPCTSTR pszDtmf));
//ver:3.3版本增加
void WINAPI tmSetFuncOnQueryACDSplit(void (*tmonQueryACDSplit)(short nSwitchId, LPCTSTR pszACDSplit, short nAvailAgents, short nCallsInQueue, short nLoginAgents, LPCTSTR pszACDParam));
void WINAPI tmSetFuncOnAgentLoggedOn(void (*tmOnAgentLoggedOn)(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, LPCTSTR pszPassword, short nStatus));
void WINAPI tmSetFuncOnAgentLoggedOff(void (*tmOnAgentLoggedOff)(short nPortID, LPCTSTR pszDeviceID, LPCTSTR pszAgentID, LPCTSTR pszGroupID, short nStatus));

void WINAPI tmSetFuncOnSetTranPhone(void (*tmonSetTranPhone)(short nPortID, LPCTSTR pszDeviceID, short nForwardType, short nForwardOnOff, LPCTSTR pszForwardDeviceID, short nResult));

void WINAPI tmMainLoop();

//-----------------------------------------------------------------------------
// #ifdef __cplusplus
// }
// #endif

//-----------------------------------------------------------------------------
#endif
