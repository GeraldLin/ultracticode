// PortStatus.h: interface for the CPortStatus class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SWITCHPORT_H__F4D253DC_2C48_4416_973B_892F85F3B117__INCLUDED_)
#define AFX_SWITCHPORT_H__F4D253DC_2C48_4416_973B_892F85F3B117__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSwitchPort
{
public:
  short m_nValid; //該端口是否有效
  short m_nPortID; //端口編號
  char m_szDeviceID[32]; //設備號碼:m_nPortType=2時表示為分機號 m_nPortType=1時表示為中繼接入號
  
  short m_nChStyle;     //通道信令類型
  short m_nChStyleIndex;//按信令類型排列的通道序號
  short m_nChType;      //硬件邏輯通道類型
  short m_nChIndex;     //硬件邏輯通道號
  short m_nLgChType;    //業務邏輯通道類型: 1-中繼 2-坐席 3-IVR 4-錄音 5-傳真 6-會議 7-VOIP
  short m_nLgChIndex;   //業務邏輯通道號

  DWORD m_dwPhysPort;   //實際的物理端口號

  short m_nCTILinkStatus;

public:
  CSwitchPort()
  {
    m_nValid = 0;
    m_nPortID = -1;
    memset(m_szDeviceID, 0, 32);
    
    m_nChStyle = 0;
    m_nChStyleIndex = 0;
    m_nChType = 0;
    m_nChIndex = 0;
    m_nLgChType = 0;
    m_nLgChIndex = 0;

    m_nCTILinkStatus = 0;
  }
  virtual ~CSwitchPort(){}
};

class CSwitchPortList
{
public:
  short m_nSwitchID;
  short m_nSwitchType;
  bool  m_bConnectSwitch;

  short m_nMaxPortNum;
  CSwitchPort *m_pSwitchPort;

public:
  CSwitchPortList()
  {
    m_nSwitchID = 1;
    m_nSwitchType = 1;
    m_nMaxPortNum = 0;
    m_pSwitchPort = NULL;
  }
  virtual ~CSwitchPortList()
  {
    if (m_pSwitchPort)
    {
      delete []m_pSwitchPort;
      m_pSwitchPort = NULL;
    }
  }
  bool InitData(short nPortNum)
  {
    if (m_pSwitchPort)
    {
      delete []m_pSwitchPort;
      m_pSwitchPort = NULL;
    }
    m_pSwitchPort = new CSwitchPort[nPortNum];
    m_nMaxPortNum = nPortNum;
    if (m_pSwitchPort)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  bool isPortIDValid(short nPortID)
  {
    if (nPortID >= 0  && nPortID < m_nMaxPortNum)
    {
      return true;
    } 
    else
    {
      return false;
    }
  }
  short GetPortIDByDeviceID(const char *pszDeviceID)
  {
    if (strlen(pszDeviceID) == 0)
    {
      return -1;
    }
    for (int i=0; i<m_nMaxPortNum; i++)
    {
      if (strcmp(pszDeviceID, m_pSwitchPort[i].m_szDeviceID) == 0)
      {
        return i;
      }
    }
    return -1;
  }
  char *GetDeviceIDByPortID(short nPortID)
  {
    if (isPortIDValid(nPortID) == true)
    {
      return m_pSwitchPort[nPortID].m_szDeviceID;
    }
    else
    {
      return "";
    }
  }
  short GetPortIDByPhysPort(DWORD dwPhysPort)
  {
    if (dwPhysPort == 0xFFFFFFFF)
    {
      return -1;
    }
    for (int i=0; i<m_nMaxPortNum; i++)
    {
      if (m_pSwitchPort[i].m_dwPhysPort == dwPhysPort)
      {
        return i;
      }
    }
    return -1;
  }
  void SetPortData(short nPortID, const char *pszDeviceID, short nChStyle, short nChStyleIndex, short nChType, short nChIndex, short nLgChType, short nLgChIndex, DWORD dwPhysPort)
  {
    if (isPortIDValid(nPortID) == true)
    {
      m_pSwitchPort[nPortID].m_nValid = 1;
      m_pSwitchPort[nPortID].m_nPortID = nPortID;
      
      m_pSwitchPort[nPortID].m_nChStyle = nChStyle;
      m_pSwitchPort[nPortID].m_nChStyleIndex = nChStyleIndex;
      m_pSwitchPort[nPortID].m_nChType = nChType;
      m_pSwitchPort[nPortID].m_nChIndex = nChIndex;
      m_pSwitchPort[nPortID].m_nLgChType = nLgChType;
      m_pSwitchPort[nPortID].m_nLgChIndex = nLgChIndex;
      m_pSwitchPort[nPortID].m_dwPhysPort = dwPhysPort;

      strncpy(m_pSwitchPort[nPortID].m_szDeviceID, pszDeviceID, 32);
    }
  }
  bool isThePortExist(short nPortID, const char *pszDeviceID)
  {
    if (isPortIDValid(nPortID) == true)
    {
      if (m_pSwitchPort[nPortID].m_nLgChType == 1)
      {
        return true;
      }
      if (strcmp(pszDeviceID, m_pSwitchPort[nPortID].m_szDeviceID) == 0)
      {
        return true;
      }
    }
    return false;
  }
};
#endif // !defined(AFX_SWITCHPORT_H__F4D253DC_2C48_4416_973B_892F85F3B117__INCLUDED_)
