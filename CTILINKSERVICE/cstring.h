#ifndef __CSTRING_H__
#define __CSTRING_H__
 
typedef char TCHARx, *PTCHARx;
typedef const char *LPCSTRx, *PCSTRx;
typedef LPCSTRx LPCTSTRx;
typedef char *LPSTRx, *PSTRx;
typedef LPSTRx PTSTRx, LPTSTRx;

#define MAXCSTRINGLEN		16300		//最大字符串長度(包括結尾0)	

  
  //本文件實現TSTRING類(注意不能賦值給自己(緩沖區不能重疊))
class CStringX
{
	 LPTSTRx   pC;   // pointer to ref counted string data
	 unsigned int DataLength;        // length of data (unincluding terminator)
	 unsigned int AllocLength;       // length of allocation
private:
	 void Init();
	 void Release();
	 unsigned int AllocBuffer(unsigned int nLen);	
	 void AssignCopy( LPCTSTRx lpszSrcData,unsigned int nSrcLen);
	 void AppendDataWithCheck( LPCTSTRx lpszSrcData,unsigned int nSrcLen) ; 
	 void AllocAndCopy(LPCSTRx lpch, unsigned int nLength);
public:
	~CStringX();		
	 // constructs empty CStringX
	CStringX();
	// copy constructor
	CStringX(const CStringX& stringSrc);
	// from a single character
	CStringX(TCHARx ch, unsigned int nRepeat = 1);
	// from an ANSI string (converts to TCHARx)
	CStringX(LPCSTRx lpsz);
	// subset of characters from an ANSI string (converts to TCHARx)
	CStringX(LPCSTRx lpch, unsigned int nLength);
	// from unsigned characters
	CStringX(const unsigned char* psz);
	 	
	// get data length
	unsigned int GetLength() const //常用
	{return DataLength;}
	// TRUE if zero length
	bool IsEmpty() const //常用
	{return DataLength==0;}
	// clear contents to empty
	void Empty(); //常用
	
	//返回0結尾的C字符串
	LPCSTRx C_Str() const
	{return pC?pC:"";}
	
	// return single character at zero-based index(注意沒有保護)
	TCHARx operator[](int nIndex) const
	{return pC[nIndex];}
	
	// set a single character at zero-based index(注意沒有保護)
	void SetAt(int nIndex, TCHARx ch)
	{pC[nIndex]=ch;}
	
	// overloaded assignment

	// ref-counted copy from another CStringX
	const CStringX& operator=(const CStringX& stringSrc);
	// set string content to single character
	const CStringX& operator=(TCHARx ch);
	
		// copy string content from ANSI string (converts to TCHAR)
	const CStringX& operator=(LPCSTRx lpsz);
	// copy string content from unsigned chars
	const CStringX& operator=(const unsigned char* psz)
	{	return operator=((LPCSTRx) psz);}
	// string concatenation

	// replace pC by string's nCount characters starting at zero-based nFirst
	void CopyMidFrom(const CStringX &string, unsigned int nFirst, unsigned int nCount)//常用
	{	Release();AppendMidFrom(string, nFirst, nCount);}
	
	// printf-like formatting using passed string
	void Format(LPCTSTRx lpszFormat, ...);
	
	// concatenate from another CStringX
	const CStringX& operator+=(const CStringX& string);

	// concatenate a single character
	const CStringX& operator+=(TCHARx ch);
		// concatenate a UNICODE character after converting it to TCHAR
	const CStringX& operator+=(LPCTSTRx lpsz);	

	// append pC by string's nCount characters starting at zero-based nFirst
	void AppendMidFrom(const CStringX &string, unsigned int nFirst, unsigned int nCount); //常用
	
	
	// 將內部數據從pos開始復制到buf,并添加結尾0,注意nBufSize為包括0在內的緩沖區buf大小,返回包括0在內的復制數據長度
	int CopyOutWithTerm(LPTSTRx buf, unsigned int nBufSize, unsigned int pos = 0) const ;

	// 將內部數據從0開始復制到buf,并添加結尾0,注意必須保證buf有足夠長度保存所有數據
	void CopyOutWithTerm(LPTSTRx buf) const ;
		 	
public:
	 	// straight character comparison
	int Compare(LPCTSTRx lpsz) const; //常用
	 	// 只比較開始的n個字節
	int CompareN(LPCTSTRx lpsz,unsigned int nCount) const; //常用
	// compare ignoring case
	int CompareNoCase(LPCTSTRx lpsz) const; //常用

	// simple sub-string extraction

	
		// upper/lower/reverse conversion

	// NLS aware conversion to uppercase
	void MakeUpper(); //常用
	// NLS aware conversion to lowercase
	void MakeLower(); //常用


	// trimming whitespace (either side)

	// remove whitespace starting from right edge
	void TrimRight(); //常用
	// remove whitespace starting from left side
	void TrimLeft(); //常用


	// searching

	// find character starting at left, -1 if not found
	int Find(TCHARx ch) const //常用
	{	return Find(ch,0);	}
	// find character starting at right
	int ReverseFind(TCHARx ch) const; //常用
	// find character starting at zero-based index and going right
	int Find(TCHARx ch, unsigned int nStart) const; //常用
	// find space char from nStart,return pos
	int FindSpace(unsigned int nStart) const; //常用
	// find not space char from nStart,return pos
	int FindNotSpace(unsigned int nStart) const; //常用
	// find first instance of any character in passed string
	int FindOneOf(LPCTSTRx lpszCharSet) const; //常用
	// find first instance of substring
	int Find(LPCTSTRx lpszSub) const //常用
	{return Find(lpszSub,0);}	
	// find first instance of substring starting at zero-based index
	int Find(LPCTSTRx lpszSub, unsigned int nStart) const; //常用
};

// Compare helpers
/*bool operator==(const CStringX& s1, const CStringX& s2);
bool operator==(const CStringX& s1, LPCTSTR s2);
bool operator==(LPCTSTR s1, const CStringX& s2);
bool operator!=(const CStringX& s1, const CStringX& s2);
bool operator!=(const CStringX& s1, LPCTSTR s2);
bool operator!=(LPCTSTR s1, const CStringX& s2);
bool operator<(const CStringX& s1, const CStringX& s2);
bool operator<(const CStringX& s1, LPCTSTR s2);
bool operator<(LPCTSTR s1, const CStringX& s2);
bool operator>(const CStringX& s1, const CStringX& s2);
bool operator>(const CStringX& s1, LPCTSTR s2);
bool operator>(LPCTSTR s1, const CStringX& s2);
bool operator<=(const CStringX& s1, const CStringX& s2);
bool operator<=(const CStringX& s1, LPCTSTR s2);
bool operator<=(LPCTSTR s1, const CStringX& s2);
bool operator>=(const CStringX& s1, const CStringX& s2);
bool operator>=(const CStringX& s1, LPCTSTR s2);
bool operator>=(LPCTSTR s1, const CStringX& s2);
*/
#endif
