//字符串宏定義-(5-枚舉字符串)
//注意字符串定義長度不得超過 MAX_MACRO_STRING_LEN-1
const VXML_MACRO_STR_RANGE_RULE_STRUCT    MacroStrRangeArray[MAX_MACRO_STR_NUM]=
{
{2	,
				{
					{"english"},
					{"chinese"},
				},
													},//langueType
{7,	
				{
					{"application"		},
	        {"description"		},
	        {"version"		},
	        {"copyright"	},
	        {"author"		},
	        {"maintainer"	},
	        {"editdate"			},
	      }, 
													},//metanameType
{3	,
				{
					{"r"	},
					{"w"},
					{"a"	},
				},
													},//fopen mode
};
													
