//多個DTMF有效按鍵定義-(3-字符串中允許出現的字符)
//注意字符串定義長度不得超過 MAX_VAR_DATA_LEN-1
const VXML_STRING_RANGE_RULE_STRUCT StrRangeArray[MAX_STR_RANGE_NUM]=
{
{12,	"0123456789,;"					,0},
{14,	"0123456789,;*#"      	,0},
{22,	"0123456789,;*#ABCDabcd",0},
};
