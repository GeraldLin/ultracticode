//整數范圍定義-(1-整數范圍)
const VXML_INTEGER_RANGE_RULE_STRUCT IntRangeArray[MAX_INT_RANGE_NUM]=
{
{0,	1					},//bool
{0,	4					},//4
{0,	8					},//8
{0,	16				},//16
{0,	64				},//64
{0,	128				},//128
{0,	255				},//UC
{0,	65535			},//US
{0,	4026531839UL},//UL
};
