//單個DTMF有效按鍵定義-(2-單字符范圍)
//注意字符串定義長度不得超過 MAX_VAR_DATA_LEN-1
const VXML_CHAR_RANGE_RULE_STRUCT CharRangeArray[MAX_CHAR_RANGE_NUM]=
{
{12,	"0123456789*#"				},
{12,	"0123456789*#"      },
{20,	"0123456789*#ABCDabcd"},
{4,		"ynYN"              },
};
