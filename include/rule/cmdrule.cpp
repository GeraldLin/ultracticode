//指令規則定義
//MsgId	MsgName		AttrNum	Note
//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1
VXML_CMD_RULE_STRUCT       CmdRuleArray[MAX_CMDS_NUM]=
{
	{/* 0*/	0	,"mainform"	,"mainform"	,2	,//主窗口定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"startid"		,"startid"	,5	,0	,0	,0},
		},      
														},
	{/* 1*/	0	,"subform"	,"subform"	,2	,//子窗口定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"startid"	,"startid"	,5	,0	,0	,0},
		},
														},
	{/* 2*/	1	,"callsubform"	,"callsubform"	,4	,//子窗口調用
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"subformid"	,"subformid"	,5	,0	,0	,0},
			{40	,20	,"subformaddr"	,"subformaddr"	,5	,0	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/* 3*/	1	,"callextform"	,"callextform"	,4	,//外部窗口調用
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"extvxml"	,"extvxml"	,5	,0	,0	,0},
			{170	,20	,"formid"		,"formid"		,5	,0	,0	,0},
			{190	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/* 4*/	1	,"assign"	,"assign"	,4	,//變量賦值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"name"		,"name"		,5	,1	,0	,0},
			{40	,250	,"expr"		,"expr"		,5	,7	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/* 5*/	1	,"getvalue"	,"getvalue"	,6	,//取其他會話通道變量值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid1"	,"sessionid1"	,2	,6	,0	,0},
			{40	,20	,"varname1"	,"varname1"	,5	,1	,0	,0},
			{60	,20	,"sessionid2"	,"sessionid2"	,2	,6	,0	,0},
			{80	,20	,"varname2"	,"varname2"	,5	,6	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/* 6*/	2	,"answer"	,"answer"	,3	,//來話應答
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"answertype"	,"answertype"	,2	,2	,4	,11},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},                                                                              
														},
	{/* 7*/	2	,"sendsignal"	,"sendsignal"	,5	,//發送信令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"signaltype"	,"signaltype"	,2	,0	,4	,12},
			{40	,20	,"signal"		,"signal"		,2	,0	,4	,13},
			{60	,20	,"cause"		,"cause"		,2	,2	,1	,7 },
			{80	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/* 8*/	2	,"hangon"	,"hangon"	,5	,//掛機釋放
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"sessionid"	,"sessionid"	,2	,6	,0	,0 },
			{40	,20	,"chntype"	,"chntype"	,2	,6	,4	,24},
			{60	,20	,"chnno"		,"chnno"		,2	,6	,0	,0 },
			{80	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/* 9*/	2	,"dtmfrule"	,"dtmfrule"	,11	,//DTMF按鍵規則定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"ruleid"		,"ruleid"		,2	,0	,1	,6},
			{40	,20	,"digits"		,"digits"		,5	,7	,3	,2},
			{60	,20	,"break"		,"break"		,2	,0	,4	,0},
			{80	,20	,"transtype"	,"transtype"	,2	,0	,4	,0},
			{100	,20	,"minlength"	,"minlength"	,2	,2	,1	,4},
			{120	,20	,"maxlength"	,"maxlength"	,2	,2	,1	,4},
			{140	,20	,"timeout"	,"timeout"	,2	,2	,1	,6},
			{160	,20	,"termdigits"	,"termdigits"	,5	,0	,3	,2},
			{180	,20	,"termid"		,"termid"		,2	,0	,4	,0},
			{200	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*10*/	2	,"getdtmf"	,"getdtmf"	,6	,//收碼定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6 },
			{40	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6 },
			{60	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0 },
			{80	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0 },
			{100	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*11*/	2	,"playrule"	,"playrule"	,11	,//放音規則定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"ruleid"		,"ruleid"		,2	,0	,1	,4},
			{40	,20	,"format"		,"format"		,2	,2	,4	,4},
			{60	,20	,"repeat"		,"repeat"		,2	,0	,1	,6},
			{80	,20	,"broadcast"	,"broadcast"	,2	,0	,4	,0},
			{100	,20	,"volume"		,"volume"		,2	,2	,1	,6},
			{120	,20	,"speech"		,"speech"		,2	,2	,4	,2},
			{140	,20	,"voice"		,"voice"		,2	,2	,4	,3},
			{160	,20	,"pause"		,"pause"		,2	,0	,1	,6},
			{180	,20	,"error"		,"error"		,2	,0	,4	,0},
			{200	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*12*/	2	,"asrrule"	,"asrrule"	,4	,//ASR規則定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"ruleid"		,"ruleid"		,2	,0	,1	,4},
			{40	,150	,"grammar"	,"grammar"	,5	,0	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*13*/	2	,"setvolume"	,"setvolume"	,3	,//改變放音音量
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"volume"		,"volume"		,5	,0	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*14*/	2	,"playfile"	,"playfile"	,10	,//單文件放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{230	,20	,"startpos"	,"startpos"	,2	,2	,0	,0},
			{250	,20	,"length"		,"length"		,2	,2	,0	,0},
			{270	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{290	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{310	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*15*/	2	,"setplaypos"	,"setplaypos"	,3	,//改變放音指針位置
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"pos"		,"pos"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*16*/	2	,"playfiles"	,"playfiles"	,10	,//多文件放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,150	,"filename1"	,"filename1"	,5	,7	,0	,0},
			{230	,150	,"filename2"	,"filename2"	,5	,7	,0	,0},
			{380	,150	,"filename3"	,"filename3"	,5	,7	,0	,0},
			{530	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{550	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{570	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*17*/	2	,"playcompose"	,"playcompose"	,9	,//合成串放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,20	,"comptype"	,"comptype"	,2	,0	,4	,5},
			{100	,250	,"string"		,"string"		,5	,7	,0	,0},
			{250	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{270	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*18*/	2	,"multiplay"	,"multiplay"	,13	,//多語音合成放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,20	,"mixtype1"	,"mixtype1"	,2	,0	,4	,5},
			{100	,150	,"content1"	,"content1"	,5	,7	,0	,0},
			{250	,20	,"mixtype2"	,"mixtype2"	,2	,0	,4	,5},
			{270	,150	,"content2"	,"content2"	,5	,7	,0	,0},
			{420	,20	,"mixtype3"	,"mixtype3"	,2	,0	,4	,5},
			{440	,150	,"content3"	,"content3"	,5	,7	,0	,0},
			{590	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{610	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{630	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*19*/	2	,"ttsstring"	,"ttsstring"	,14	,//文本串轉語音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,20	,"ttstype"	,"ttstype"	,5	,7	,0	,0},
			{100	,30	,"ttsgrammar"	,"ttsgrammar"	,5	,7	,0	,0},
			{130	,100	,"ttsparam"	,"ttsparam"	,5	,7	,0	,0},
			{230	,20	,"ttsformat"	,"ttsformat"	,2	,2	,4	,4},
			{250	,100	,"ttsfilename"	,"ttsfilename"	,5	,7	,0	,0},
			{350	,250	,"string"		,"string"		,5	,7	,0	,0},
			{600	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{620	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{640	,20	,"returntts"	,"returntts"	,10	,1	,0	,0},
			{660	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*20*/	2	,"ttsfile"	,"ttsfile"	,14	,//文本文件轉語音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,20	,"ttstype"	,"ttstype"	,5	,7	,0	,0},
			{100	,30	,"ttsgrammar"	,"ttsgrammar"	,5	,7	,0	,0},
			{130	,100	,"ttsparam"	,"ttsparam"	,5	,7	,0	,0},
			{230	,20	,"ttsformat"	,"ttsformat"	,2	,2	,4	,4},
			{250	,100	,"ttsfilename"	,"ttsfilename"	,5	,7	,0	,0},
			{350	,150	,"filename"		,"filename"		,5	,7	,0	,0},
			{500	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{520	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{540	,20	,"returntts"	,"returntts"	,10	,1	,0	,0},
			{560	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*21*/	2	,"senddtmf"	,"senddtmf"	,5	,//發送DTMF
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"duration"	,"duration"	,2	,0	,1	,6},
			{40	,20	,"pause"		,"pause"		,2	,0	,1	,6},
			{60	,20	,"dtmfs"		,"dtmfs"		,5	,7	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*22*/	2	,"sendfsk"	,"sendfsk"	,7	,//發送FSK
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"fskheader"	,"fskheader"	,2	,2	,1	,6},
			{40	,20	,"fskcmd"	,"fskcmd"	,2	,2	,1	,6},
			{60	,500,"fskdata"		,"fskdata"		,5	,7	,0	,0},
			{560,20	,"fskdatalen"	,"fskdatalen"	,2	,2	,1	,7},
			{580,20	,"crctype"	,"crctype"	,2	,2	,1	,6},
			{600,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*23*/	2	,"playcfc"	,"playcfc"	,7	,//會議室放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{40	,20	,"cfcno"		,"cfcno"		,2	,2	,1	,7},
			{60	,150	,"filename1"	,"filename1"	,5	,7	,0	,0},
			{210	,150	,"filename2"	,"filename2"	,5	,7	,0	,0},
			{360	,150	,"filename3"	,"filename3"	,5	,7	,0	,0},
			{510	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*24*/	2	,"playtone"	,"playtone"	,6	,//播放信號音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"tonetype"	,"tonetype"	,2	,2	,1	,6},
			{60	,20	,"repeat"		,"repeat"		,2	,0	,1	,6},
			{80	,20	,"release"	,"release"	,2	,0	,4	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*25*/	2	,"recordfile"	,"recordfile"	,10	,//錄音定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0                 },
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0                 },
			{170	,20	,"beep"		,"beep"		,2	,0	,4	,0                 },
			{190	,20	,"maxtime"		,"maxtime"		,2	,2	,1	,7                 },
			{210	,20	,"termchar"	,"termchar"	,5	,0	,2	,1                 },
			{230	,20	,"format"		,"format"		,2	,2	,4	,4                 },
			{250	,20	,"writemode"	,"writemode"	,2	,0	,4	,19                },
			{270	,20	,"returnrecordedlen"	,"returnrecordedlen"	,10	,1	,0	,0 },
			{290	,20	,"returnrecordedsize"	,"returnrecordedsize"	,10	,1	,0	,0 },
			{310	,20	,"next"		,"next"		,5	,0	,0	,0                 },
		},
														},
	{/*26*/	2	,"recordcfc"	,"recordcfc"	,10	,//會議錄音定義
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0                },
			{20	,20	,"cfcno"		,"cfcno"		,2	,2	,1	,7                },
			{40	,150	,"filename"	,"filename"	,5	,7	,0	,0                },
			{190	,20	,"maxtime"	,"maxtime"	,2	,2	,1	,7                },
			{210	,20	,"format"		,"format"		,2	,2	,4	,4                },
			{230	,20	,"writemode"	,"writemode"	,2	,0	,4	,19               },
			{250	,20	,"returncfcno"	,"returncfcno"	,10	,1	,0	,0                },
			{270	,20	,"returnrecordedlen"	,"returnrecordedlen"	,10	,1	,0	,0},
			{290	,20	,"returnrecordedsize"	,"returnrecordedsize"	,10	,1	,0	,0},
			{310	,20	,"next"		,"next"		,5	,0	,0	,0                },
		},
														},
	{/*27*/	2	,"stop"		,"stop"		,3	,//停止放音錄音收碼
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"stopsessionid"	,"stopsessionid"	,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*28*/	2	,"callout"	,"callout"	,24	,//電話呼出
		{
			{0,20,		"id","id"		,5	,0	,0	,0},
			{20,20,		"serialno","serialno"	,2	,0	,1	,6},
			{40,20,		"callertype","callertype"	,2	,2	,1	,6},
			{60,20,		"calltype","calltype"	,2	,2	,1	,6},
			{80,60,		"callerno","callerno"	,5	,7	,0	,0},
			{140,100,	"calledno","calledno"	,5	,7	,0	,0},
			{240,20,	"waittime","waittime"	,2	,2	,1	,6},
			{260,20,	"routeno","routeno"	,5	,7	,0	,0},
			{280,20,	"vxmlid","vxmlid"		,2	,6	,1	,6},
			{300,20,	"funcno","funcno"		,2	,2	,1	,7},
			{320,20,	"times","times"		,2	,2	,1	,6},
			{340,100,	"param","param"		,5	,7	,0	,0},
			{440,20,	"returnchntype","returnchntype"	,10	,1	,0	,0},
			{460,20,	"returnchnno","returnchnno"	,10	,1	,0	,0},
			{480,20,	"returncalledno","returncalledno"	,10	,1	,0	,0},
			{500,20,	"returnfuncno","returnfuncno"	,10	,1	,0	,0},
			{520,20,	"returnpoint","returnpoint"	,10	,1	,0	,0},
			{540,20,	"outchntype","outchntype"		,2	,6	,4	,24},
			{560,20,	"outchnno","outchnno"		,2	,6	,1	,7},
			{580,20,	"callmode","callmode"		,2	,6	,1	,6},
			{600,20,	"callpoint","callpoint"		,2	,6	,1	,6},
			{620,20,	"callinterval","callinterval"		,2	,6	,1	,6},
			{640,20,	"calledtype","calledtype"		,5	,7	,0	,0},
			{660,20,	"next","next"		,5	,0	,0	,0},
		},
														},
	{/*29*/	2	,"cancelcallout","cancelcallout",2	,//取消呼出
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*30*/	2	,"sendcalledno"	,"sendcalledno"	,3	,//發送被叫號碼
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,30	,"calledno"	,"calledno"	,5	,7	,0	,0},
			{50	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*31*/	2	,"transfer"	,"transfer"	,18	,//電話轉接
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20,20	,"sourcechntype"		,"sourcechntype"		,2	,6	,1	,6},
			{40,20	,"sourcechnno"		,"sourcechnno"		,2	,6	,1	,7},
			{60	,20	,"tranmode"	,"tranmode"	,2	,2	,1	,6},
			{80	,20	,"callertype"	,"callertype"	,2	,2	,1	,6},
			{100	,20	,"calledtype"	,"callertype"	,2	,2	,1	,6},
			{120	,20	,"calltype"	,"calltype"	,2	,2	,1	,6},
			{140,20	,"callerno"	,"callerno"	,5	,7	,0	,0},
			{160,20	,"calledno"	,"calledno"	,5	,7	,0	,0},
			{180,20	,"waittime"	,"waittime"	,2	,2	,1	,6},
			{200,20	,"routeno"	,"routeno"	,2	,6	,1	,6},
			{220,20	,"outchntype"		,"outchntype"		,2	,6	,1	,6},
			{240,20	,"outchnno"		,"outchnno"		,2	,6	,1	,7},
			{260,100,"waitvoc"	,"waitvoc"	,5	,7	,0	,0},
			{360,100,"param"	,"param"	,5	,7	,0	,0},
			{460,20,"returnchntype","returnchntype"	,10	,1	,0	,0},
			{480,20,"returnchnno","returnchnno"	,10	,1	,0	,0},
			{500,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*32*/	2	,"callseat"	,"callseat"	,31	,//分配坐席
		{
			{0	  ,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	  ,20	,"callerno"	,"callerno"	,5	,7	,0	,0},
			{40	  ,20	,"calledno"	,"calledno"	,5	,7	,0	,0},
			{60	  ,20	,"seatno"		,"seatno"		,5	,7	,0	,0},
			{80	  ,20	,"workerno"		,"workerno"		,2	,6	,0	,0},
			{100	,20	,"seattype"	,"seattype"	,2	,6	,1	,6},
			{120	,20	,"acdrule"		,"acdrule"		,2	,6	,1	,6},
			{140	,20	,"callmode"		,"callmode"		,2	,6	,1	,6},
			{160	,50	,"groupno"	,"groupno"	,5	,7	,0	,0},
			{210	,20	,"calltype"	,"calltype"	,2	,6	,1	,7},
			{230	,20	,"seatgroupno"	,"seatgroupno"	,2	,6	,1	,6},
			{250	,20	,"level"		,"level"		,5	,7	,0	,0},
			{270	,20	,"levelrule"		,"levelrule"		,2	,6	,1	,6},
			{290	,20	,"waittime"	,"waittime"	,2	,6	,1	,6},
			{310	,20	,"ringtime"		,"ringtime"		,2	,6	,1	,6},
			{330	,20	,"vxmlid"		,"vxmlid"		,2	,6	,1	,6},
			{350	,20	,"funcno"		,"funcno"		,2	,6	,1	,7},
			{370	,20	,"busywaitid"	,"busywaitid"	,2	,6	,1	,6},
			{390	,20	,"priority"		,"priority"		,2	,6	,1	,6},
			{410	,20,"servicetype"		,"servicetype"		,5	,7	,0	,0},
			{430	,20,"cdrserialno"		,"cdrserialno"		,5	,7	,0	,0},
			{450	,100,"param"		,"param"		,5	,7	,0	,0},
			{550	,20	,"returnchntype"	,"returnchntype"	,10	,1	,0	,0},
			{570	,20	,"returnchnno"	,"returnchnno"	,10	,1	,0	,0},
			{590	,20	,"returnseatno"	,"returnseatno"	,10	,1	,0	,0},
			{610	,20	,"returnworkerno"	,"returnworkerno"	,10	,1	,0	,0},
			{630	,20	,"returngroupno"	,"returngroupno"	,10	,1	,0	,0},
			{650	,20	,"returnqueueno"	,"returnqueueno"	,10	,1	,0	,0},
			{670	,20	,"returnserialno"	,"returnserialno"	,10	,1	,0	,0},
			{690	,20	,"returnparam"	,"returnparam"	,10	,1	,0	,0},
			{710	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*33*/	2	,"stopcallseat"	,"stopcallseat"	,2	,//取消分配坐席
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*34*/	2	,"createcfc"	,"createcfc"	,12	,//創建會議
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"cfcno"		,"cfcno"		,2	,2	,0	,0},
			{40	,50	,"name"		,"name"		,5	,2	,0	,0},
			{90	,20	,"onwer"		,"onwer"		,5	,2	,0	,0},
			{110	,20	,"presiders"	,"presiders"	,2	,2	,0	,0},
			{130	,20	,"talkers"	,"talkers"	,2	,2	,0	,0},
			{150	,20	,"listeners"	,"listeners"	,2	,2	,0	,0},
			{170	,20	,"password"	,"password"	,5	,2	,0	,0},
			{190	,20	,"autorecord"	,"autorecord"	,2	,2	,0	,0},
			{210	,20	,"autofree"	,"autofree"	,2	,2	,0	,0},
			{230	,20	,"returncfcno"	,"returncfcno"	,10	,1	,0	,0},
			{250	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*35*/	2	,"destroycfc"	,"destroycfc"	,4	,//釋放會議資源
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"cfcno"		,"cfcno"		,2	,2	,0	,0},
			{40	,20	,"destroyid"		,"destroyid"		,2	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*36*/	2	,"joincfc"	,"joincfc"	,6	,//加入會議
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"cfcno"		,"cfcno"		,2	,2	,0	,0 },
			{40	,20	,"jointype"	,"jointype"	,2	,2	,4	,10},
			{60	,20	,"knocktone"	,"knocktone"	,2	,2	,1	,3 },
			{80	,20	,"emptyevent"	,"emptyevent"	,2	,2	,0	,0 },
			{100	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*37*/	2	,"unjoincfc"	,"unjoincfc"	,3	,//退出會議
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"cfcno"		,"cfcno"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*38*/	2	,"link"		,"link"		,5	,//通道交換
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"linkchantype"	,"linkchantype"	,2	,6	,4	,24},
			{40	,20	,"linkchanno"	,"linkchanno"	,2	,6	,1	,7 },
			{60	,20	,"linkmode"	,"linkmode"	,2	,2	,4	,7 },
			{80	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*39*/	2	,"talkwith"	,"talkwith"	,3	,//與對方通話
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*40*/	2	,"listenfrom"	,"listenfrom"	,3	,//監聽對方通話
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*41*/	2	,"inserttalk"	,"inserttalk"	,5	,//強插對方通話
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"linkchantype"	,"linkchantype"	,2	,6	,4	,24},
			{60	,20	,"linkchanno"	,"linkchanno"	,2	,6	,1	,7 },
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*42*/	2	,"threetalk"	,"threetalk"	,8	,//三方通話
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid1"	,"sessionid1"	,2	,2	,0	,0},
			{40	,20	,"linkchantype1"	,"linkchantype1"	,2	,6	,4	,24},
			{60	,20	,"linkchanno1"	,"linkchanno1"	,2	,6	,1	,7 },
			{80	,20	,"sessionid2"	,"sessionid2"	,2	,2	,0	,0},
			{100	,20	,"linkchantype2"	,"linkchantype2"	,2	,6	,4	,24},
			{120	,20	,"linkchanno2"	,"linkchanno2"	,2	,6	,1	,7 },
			{140	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*43*/	2	,"stoptalk"	,"stoptalk"	,2	,//結束通話
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*44*/	2	,"sendfax"	,"sendfax"	,13	,//發送傳真
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"faxcsid"	,"faxcsid"	,5	,7	,0	,0},
			{40	,20	,"resolution"	,"resolution"	,5	,7	,0	,0},
			{60 ,20	,"totalpages"	,"totalpages"	,2	,6	,1	,6},
			{80 ,20	,"pagelist"	,"pagelist"	,5	,7	,0	,0},
			{100,100,"faxheader"	,"faxheader"	,5	,7	,0	,0},
			{200,100,"faxfooter"	,"faxfooter"	,5	,7	,0	,0},
			{300,50	,"barcode"		,"barcode"		,5	,7	,0	,0},
			{350,250,"filename"	,"filename"	,5	,7	,0	,0 },
			{600,20	,"returnsendpages"	,"returnsendpages"	,10	,1	,0	,0},
			{620,20	,"returnsendbytes"	,"returnsendbytes"	,10	,1	,0	,0},
			{640,20	,"returnsendspeed"	,"returnsendspeed"	,10	,1	,0	,0},
			{660,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*45*/	2	,"recvfax"	,"recvfax"	,10	,//接收傳真
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20,20,"faxcsid"	,"faxcsid"	,5	,7	,0	,0},
			{40,50,"barcode"	,"barcode"	,5	,7	,0	,0},
			{90,250,"filename"	,"filename"	,5	,7	,0	,0},
			{340,20	,"returnfaxcsid"	,"returnfaxcsid"	,10	,1	,0	,0},
			{360,20	,"returnbarcode"		,"returnbarcode"		,10	,1	,0	,0},
			{380,20	,"returnrecvpages"	,"returnrecvpages"	,10	,1	,0	,0},
			{400,20	,"returnrecvbytes"	,"returnrecvbytes"	,10	,1	,0	,0},
			{420,20	,"returnrecvspeed"	,"returnrecvspeed"	,10	,1	,0	,0},
			{440,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*46*/	2	,"checkpath"	,"checkpath"	,3	,//檢查文件路徑
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"path"		,"path"		,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*47*/	2	,"createpath"	,"createpath"	,3	,//創建文件路徑
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"path"		,"path"		,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*48*/	2	,"deletepath"	,"deletepath"	,3	,//刪除文件路徑
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"path"		,"path"		,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*49*/	2	,"getfilenum"	,"getfilenum"	,5	,//取文件個數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"path"		,"path"		,5	,7	,0	,0},
			{170	,50	,"filespec"	,"filespec"	,5	,7	,0	,0},
			{220	,20	,"returnfilenums"	,"returnfilenums"	,10	,1	,0	,0},
			{240	,20	,"next"		,"next"		,5	,0	,0	,0},
		},                                                                              
														},
	{/*50*/	2	,"getfilename"	,"getfilename"	,7	,//取文件名
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"path"		,"path"		,5	,7	,0	,0},
			{170	,50	,"filespec"	,"filespec"	,5	,7	,0	,0},
			{220	,20	,"returnfilenums"	,"returnfilenums"	,10	,1	,0	,0},
			{240	,20	,"maxfiles"	,"maxfiles"	,2	,2	,0	,0},
			{260	,20	,"returnfilename"	,"returnfilename"	,10	,1	,0	,0},
			{280	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*51*/	2	,"renname"	,"renname"	,4	,//改文件名
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"oldfilename"	,"oldfilename"	,5	,7	,0	,0},
			{170	,150	,"newfilename"	,"newfilename"	,5	,7	,0	,0},
			{320	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*52*/	2	,"copyfile"	,"copyfile"	,4	,//拷貝文件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"soufilename"	,"soufilename"	,5	,7	,0	,0},
			{170	,150	,"desfilename"	,"desfilename"	,5	,7	,0	,0},
			{320	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*53*/	2	,"deletefile"	,"deletefile"	,3	,//刪除文件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*54*/	1	,"fopen"	,"fopen"	,6	,//打開文本文件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"mode"		,"mode"		,5	,0	,5	,2},
			{190	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{210	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{230	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*55*/	1	,"fread"	,"fread"	,6	,//讀文本文件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"returnstring"	,"returnstring"	,10	,1	,0	,0},
			{190	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{210	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{230	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*56*/	1	,"fwrite"	,"fwrite"	,6	,//寫文本文件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,250	,"txtline"	,"txtline"	,5	,7	,0	,0},
			{420	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{440	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{460	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*57*/	1	,"fclose"	,"fclose"	,3	,//關閉文本文件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*58*/	3	,"dbquery"	,"dbquery"	,5	,//查詢數據操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,500	,"sqls"		,"sqls"		,9	,10	,0	,0},
			{520	,20	,"returnrecords"	,"returnrecords"	,10	,1	,0	,0},
			{540	,20	,"returnfields"	,"returnfields"	,10	,1	,0	,0},
			{560	,20	,"next"		,"next"		,5	,0	,0	,0},
		},                                                                              
														},
	{/*59*/	3	,"dbfieldno"	,"dbfieldno"	,6	,//取數據字段操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"fieldno"	,"fieldno"	,2	,2	,0	,0},
			{40	,20	,"returnvalue"	,"returnvalue"	,10	,1	,0	,0},
			{60	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{80	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*60*/	3	,"dbfirst"	,"dbfirst"	,5	,//首記錄
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returnrecordno"	,"returnrecordno"	,10	,1	,0	,0},
			{40	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{60	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*61*/	3	,"dbnext"	,"dbnext"	,5	,//下一記錄
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returnrecordno"	,"returnrecordno"	,10	,1	,0	,0},
			{40	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{60	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*62*/	3	,"dbprior"	,"dbprior"	,5	,//前一記錄
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returnrecordno"	,"returnrecordno"	,10	,1	,0	,0},
			{40	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{60	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*63*/	3	,"dblast"	,"dblast"	,5	,//末記錄
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returnrecordno"	,"returnrecordno"	,10	,1	,0	,0},
			{40	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{60	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*64*/	3	,"dbgoto"	,"dbgoto"	,6	,//移動記錄指針
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returnrecordno"	,"returnrecordno"	,10	,1	,0	,0}, //edit by zgj
			{40	,20	,"recordno"	,"recordno"	,2	,2	,0	,0},
			{60	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{80	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*65*/	3	,"dbinsert"	,"dbinsert"	,3	,//插入記錄操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,500	,"sqls"		,"sqls"		,9	,11	,0	,0},
			{520	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*66*/	3	,"dbupdate"	,"dbupdate"	,3	,//修改記錄操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,500	,"sqls"		,"sqls"		,9	,12	,0	,0},
			{520	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*67*/	3	,"dbdelete"	,"dbdelete"	,3	,//刪除記錄操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,500	,"sqls"		,"sqls"		,9	,13	,0	,0},
			{520	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*68*/	1	,"startbill"	,"startbill"	,3	,//開始計費
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"writeid"		,"writeid"		,1	,2	,1	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*69*/	3	,"writebill"	,"writebill"	,11	,//終止計費并寫話單
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"tollid"		,"tollid"		,2	,2	,0	,0},
			{40	,20	,"funcgroup"		,"funcgroup"		,5	,6	,0	,0},
			{60	,20	,"funcno"		,"funcno"		,5	,6	,0	,0},
			{80	,20	,"callerno"		,"callerno"		,5	,6	,0	,0},
			{100	,20	,"calledno"		,"calledno"		,5	,6	,0	,0},
			{120	,20	,"account"		,"account"		,5	,6	,0	,0},
			{140	,20	,"starttime"		,"starttime"		,5	,6	,0	,0},
			{160	,20	,"endtime"		,"endtime"		,5	,6	,0	,0},
			{180	,20	,"returntollfee"		,"returntollfee"		,10	,1	,0	,0},
			{200	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*70*/	2	,"oncallin"	,"oncallin"	,6	,//響應電話呼入事件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returncallerno"	,"returncallerno"	,5	,1	,0	,0},
			{40	,20	,"returncalledno"	,"returncalledno"	,5	,1	,0	,0},
			{60	,20	,"returncallerno"	,"returncallerno"	,5	,1	,0	,0},
			{80	,20	,"returncalledno"	,"returncalledno"	,5	,1	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*71*/	2	,"onrecvsam"	,"onrecvsam"	,6	,//收到后續地址事件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returncallerno"	,"returncallerno"	,5	,1	,0	,0},
			{40	,20	,"returncalledno"	,"returncalledno"	,5	,1	,0	,0},
			{60	,20	,"returncallerno"	,"returncallerno"	,5	,1	,0	,0},
			{80	,20	,"returncalledno"	,"returncalledno"	,5	,1	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*72*/	2	,"onsmsin"	,"onsmsin"	,5	,//短信呼入事件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"returncallerno"	,"returncallerno"	,5	,1	,0	,0},
			{40	,20	,"returncalledno"	,"returncalledno"	,5	,1	,0	,0},
			{60	,20	,"returnsms"	,"returnsms"	,5	,1	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},                                                                              
														},
	{/*73*/	4	,"catch"	,"catch"	,2	,//響應事件指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*74*/	9	,"onevent"	,"onevent"	,5	,//響應事件指令判斷事件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"event"		,"event"		,2	,0	,4	,23},
			{40	,20	,"goto"		,"goto"		,5	,0	,0	,0 },
			{60	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0 },
			{80	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*75*/	5	,"switch"	,"switch"	,2	,//條件跳轉指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*76*/	9	,"case"		,"case"		,5	,//條件跳轉指令判斷條件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,250	,"cond"		,"cond"		,7	,8	,0	,0},
			{270	,20	,"goto"		,"goto"		,5	,0	,0	,0},
			{290	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{310	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*77*/	1	,"findsessionid","findsessionid",6	,//查找目標會話通道標志
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,250	,"cond"		,"cond"		,7	,8	,0	,0},
			{270	,20	,"maxnum"		,"maxnum"		,2	,2	,0	,0},
			{290	,20	,"returnnums"	,"returnnums"	,10	,1	,0	,0},
			{310	,20	,"returnsessionid","returnsessionid"	,10	,1	,0	,0},
			{330	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*78*/	1	,"goto"		,"goto"		,5	,//跳轉指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"goto"		,"goto"		,5	,0	,0	,0},
			{60	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{80	,20	,"next"		,"next"		,5	,2	,0	,0},
		},
														},
	{/*79*/	1	,"return"	,"return"	,2	,//子窗口調用結束返回
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*80*/	1	,"throw"	,"throw"	,5	,//產生事件
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"event"		,"event"		,11	,0	,0	,0},
			{60	,20	,"eventid"	,"eventid"		,2	,0	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*81*/	7	,"stradd"	,"stradd"	,4	,//字符串相加
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"expr"		,"expr"		,5	,7	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*82*/	8	,"len"		,"len"		,4	,//取字符串長度
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*83*/	8	,"substring"	,"substring"	,6	,//取子字符串
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"startpos"	,"startpos"	,2	,2	,0	,0},
			{310	,20	,"length"		,"length"		,2	,2	,0	,0},
			{330	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*84*/	8	,"trim"		,"trim"		,4	,//去掉字符串兩頭空格
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*85*/	8	,"lefttrim"	,"lefttrim"	,4	,//去掉字符串左空格
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,6	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*86*/	8	,"righttrim"	,"righttrim"	,4	,//去掉字符串右邊空格
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*87*/	8	,"left"		,"left"		,5	,//取左字符串
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"length"		,"length"		,2	,2	,0	,0},
			{310	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*88*/	8	,"right"	,"right"	,5	,//取右字符串
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"length"		,"length"		,2	,2	,0	,0},
			{310	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*89*/	8	,"strcmp"	,"strcmp"	,5	,//區分大小寫的字符串比較
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"	,5	,1	,0	,0},
			{40	,250	,"string1"	,"string1"	,5	,7	,0	,0},
			{290	,250	,"string2"	,"string2"	,5	,7	,0	,0},
			{540	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*90*/	8	,"strcmpi"	,"strcmpi"	,5	,//不區分大小寫的字符串比較
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"	,"varname"		,5	,1	,0	,0},
			{40	,250	,"string1"	,"string1"		,5	,7	,0	,0},
			{290	,250	,"string2"	,"string2"		,5	,7	,0	,0},
			{540	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*91*/	8	,"like"		,"like"		,5	,//字符串相似比較
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,250	,"string1"		,"string1"		,5	,7	,0	,0},
			{290	,250	,"string2"		,"string2"		,5	,7	,0	,0},
			{540	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*92*/	8	,"lower"	,"lower"	,4	,//將字符串小寫化
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*93*/	8	,"upper"	,"upper"	,4	,//將字符串大寫化
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,250	,"string"		,"string"		,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*94*/	8	,"strfmt"	,"strfmt"	,6	,//將字符填充空格
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0 },
			{40	,250	,"string"		,"string"		,5	,7	,0	,0 },
			{290	,250	,"formatstr"	,"formatstr"	,5	,7	,0	,0},
			{540	,20	,"formattype"		,"formattype"		,2	,2	,0	,0 },
			{560	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*95*/	8	,"split"	,"split"	,7	,//根據分隔符分隔子字符串
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"startvarname"	,"startvarname"	,5	,1	,0	,0},
			{40	,20	,"numvarname"	,"numvarname"	,5	,1	,0	,0},
			{60	,250	,"string"		,"string"		,5	,7	,0	,0},
			{310	,20	,"separator"	,"separator"	,4	,0	,0	,0},
			{330	,20	,"splitnum"	,"splitnum"	,2	,2	,0	,0},
			{350	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*96*/	8	,"join"		,"join"		,6	,//將數組連成一字符串，并用分隔符分開
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"separator"	,"separator"	,4	,0	,0	,0},
			{60	,20	,"joinnum"		,"joinnum"		,2	,2	,0	,0},
			{80	,20	,"startvarnum"	,"startvarnum"	,5	,1	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*97*/	8	,"strpos"	,"strpos"	,5	,//搜索子字符串位置
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,250	,"string1"		,"string1"		,5	,7	,0	,0},
			{290	,250	,"string2"		,"string2"		,5	,7	,0	,0},
			{540	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*98*/	8	,"getnow"	,"getnow"	,3	,//取當前日期時間函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*99*/	8	,"getdate"	,"getdate"	,4	,//取日期函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*100*/8	,"gettime"	,"gettime"	,4	,//取時間函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*101*/8	,"getyear"	,"getyear"	,4	,//取年函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*102*/8	,"getmonth"	,"getmonth"	,4	,//取月函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*103*/8	,"getday"	,"getday"	,4	,//取日函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*104*/8	,"gethour"	,"gethour"	,4	,//取時函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*105*/8	,"getminute"	,"getminute"	,4	,//取分函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*106*/8	,"getsecond"	,"getsecond"	,4	,//取秒函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*107*/8	,"getmsecond"	,"getmsecond"	,3	,//取毫秒函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*108*/8	,"getweekday"	,"getweekday"	,4	,//取星期函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*109*/8	,"starttimer"	,"starttimer"	,4	,//啟動定時器
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"timerid"		,"timerid"		,2	,0	,4	,20},
			{40	,20	,"timelen"		,"timelen"		,2	,2	,0	,0 },
			{60	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*110*/8	,"isdatetime"	,"isdatetime"	,4	,//檢查日期時間的合法性
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*111*/8	,"getinterval"	,"getinterval"	,6	,//取日期或時間間隔
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0 },
			{40	,20	,"type"		,"type"		,2	,0	,4	,15},
			{60	,20	,"datetime1"	,"datetime1"	,5	,7	,0	,0 },
			{80	,20	,"datetime2"	,"datetime2"	,5	,7	,0	,0 },
			{100	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*112*/8	,"incdatetime"	,"incdatetime"	,6	,//日期時間增量
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0 },
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0 },
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0 },
			{60	,20	,"type"		,"type"		,2	,0	,4	,15},
			{80	,20	,"num"		,"num"		,2	,2	,0	,0 },
			{100	,20	,"next"		,"next"		,5	,0	,0	,0 },
		},
														},
	{/*113*/8	,"indatetime"	,"indatetime"	,6	,//是否在時間段內
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{60	,20	,"datetime1"	,"datetime1"	,5	,7	,0	,0},
			{80	,20	,"datetime2"	,"datetime2"	,5	,7	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*114*/8	,"daysinyear"	,"daysinyear"	,4	,//指定年有多少天
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"year"		,"year"		,2	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*115*/8	,"daysinmonth"	,"daysinmonth"	,5	,//指定月有多少天
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"year"		,"year"		,2	,2	,0	,0},
			{60	,20	,"month"		,"month"		,2	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*116*/8	,"isleapyear"	,"isleapyear"	,4	,//指定年是否為閏年
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"year"		,"year"		,2	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*117*/1	,"readinifile"	,"readinifile"	,7	,//讀配置文件函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,150	,"inifile"		,"inifile"		,5	,7	,0	,0},
			{190	,50	,"section"		,"section"		,5	,7	,0	,0},
			{240	,50	,"paramer"		,"paramer"		,5	,7	,0	,0},
			{290	,50	,"default"		,"default"		,5	,7	,0	,0},
			{340	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*118*/8	,"add"		,"add"		,5	,//算術相加函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*119*/8	,"sub"		,"sub"		,5	,//算術相減函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*120*/8	,"mul"		,"mul"		,5	,//算術相乘函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*121*/8	,"div"		,"div"		,5	,//算術除函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*122*/8	,"mod"		,"mod"		,5	,//算術取模函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,2	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,2	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*123*/8	,"bitand"	,"bitand"	,5	,//位與
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,2	,2	,1	,6},
			{60	,20	,"numeric2"	,"numeric2"	,2	,2	,1	,6},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*124*/8	,"bitor"	,"bitor"	,5	,//位或
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,2	,2	,1	,6},
			{60	,20	,"numeric2"	,"numeric2"	,2	,2	,1	,6},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*125*/8	,"bitnot"	,"bitnot"	,4	,//位非
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,2	,2	,1	,6},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*126*/8	,"bitlmove"	,"bitlmove"	,5	,//位左移
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,2	,2	,1	,6},
			{60	,20	,"bits"		,"bits"		,2	,2	,1	,2},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*127*/8	,"bitrmove"	,"bitrmove"	,5	,//位右移
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,2	,2	,1	,6},
			{60	,20	,"bits"		,"bits"		,2	,2	,1	,2},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*128*/8	,"getbit"	,"getbit"	,5	,//取位值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,2	,2	,1	,6},
			{60	,20	,"bits"		,"bits"		,2	,2	,1	,2},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*129*/8	,"random"	,"random"	,5	,//產生隨機數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"lownum"		,"lownum"		,2	,2	,1	,8},
			{60	,20	,"upnum"		,"upnum"		,2	,2	,1	,8},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*130*/8	,"abs"		,"abs"		,4	,//取絕對值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*131*/8	,"pow"		,"pow"		,5	,//乘方
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"power"		,"power"		,2	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*132*/8	,"sqrt"		,"sqrt"		,4	,//平方根
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*133*/8	,"sin"		,"sin"		,4	,//正弦值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*134*/8	,"asin"		,"asin"		,4	,//反正弦值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*135*/8	,"cos"		,"cos"		,4	,//余弦值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*136*/8	,"acos"		,"acos"		,4	,//反余弦值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*137*/8	,"tan"		,"tan"		,4	,//正切值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*138*/8	,"atan"		,"atan"		,4	,//反正切值
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*139*/8	,"max"		,"max"		,5	,//取最大數函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*140*/8	,"min"		,"min"		,5	,//取最小數函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*141*/6	,"math"		,"math"		,4	,//算術運算表達式
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,250	,"expr"		,"expr"		,8	,9	,0	,0},
			{290	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*142*/8	,"fmtdecimal"	,"fmtdecimal"	,5	,//浮點小數小數位數處理
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"decimals"	,"decimals"	,2	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*143*/8	,"isint"	,"isint"	,4	,//整數有效判斷
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*144*/8	,"isnumeric"	,"isnumeric"	,4	,//數值有效判斷
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric"		,"numeric"		,3	,2	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*145*/10	,"default"	,"default"	,4	,//默認條件跳轉指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"goto"		,"goto"		,5	,0	,0	,0},
			{40	,20	,"gotoaddr"	,"gotoaddr"	,5	,0	,0	,0},
			{60	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*146*/3	,"dbfieldname"	,"dbfieldname"	,6	,//取數據字段操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"fieldname"	,"fieldname"	,5	,7	,0	,0},
			{40	,20	,"returnvalue"	,"returnvalue"	,10	,1	,0	,0},
			{60	,20	,"failgoto"	,"failgoto"	,5	,0	,0	,0},
			{80	,20	,"gotoaddr"	,"gotoaddr"	,2	,0	,0	,0},
			{100	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*147*/3	,"dbsp"		,"dbsp"		,4	,//執行存儲過程操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,100	,"sp"		,"sp"		,5	,7	,0	,0},
			{120	,880	,"params"		,"params"		,6	,7	,0	,0},
			{1000	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*148*/8	,"idiv"		,"idiv"		,5	,//算術整除函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"numeric1"	,"numeric1"	,3	,2	,0	,0},
			{60	,20	,"numeric2"	,"numeric2"	,3	,2	,0	,0},
			{80	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*149*/1	,"waiting"	,"waiting"	,3	,//條件判斷等待
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,2	,0	,0},
		},
														},
	{/*150*/3	,"dbclose"	,"dbclose"	,2	,//關閉查詢的數據表
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,2	,0	,0},
		},
														},
	{/*151*/2	,"checkfile"	,"checkfile"	,4	,//檢查文件是否存在
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"returnsize"	,"returnsize"	,10	,1	,0	,0},
			{190	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*152*/2	,"clearmixer"	,"clearmixer"	,2	,//清除放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*153*/2	,"addfile"	,"addfile"	,3	,//增加文件到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*154*/2	,"adddatetime"	,"adddatetime"	,3	,//增加日期時間到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"datetime"	,"datetime"	,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*155*/2	,"addmoney"	,"addmoney"	,3	,//增加金額到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"money"		,"money"		,3	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*156*/2	,"addnumber"	,"addnumber"	,3	,//增加數值到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"number"		,"number"		,3	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*157*/2	,"adddigits"	,"adddigits"	,3	,//增加數字串到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"digits"		,"digits"		,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*158*/2	,"addchars"	,"addchars"	,3	,//增加字符串到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"chars"	,"chars"	,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*159*/2	,"playmixer"	,"playmixer"	,7	,//播放合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{100	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{120	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*160*/2	,"addttsstr"	,"addttsstr"	,3	,//增加TTS字符串到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"string"		,"string"		,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*161*/2	,"addttsfile"	,"addttsfile"	,3	,//增加TTS文件到放音合成緩沖區
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"filename"	,"filename"	,5	,7	,0	,0},
			{170	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*162*/1	,"gotomainform"	,"gotomainform"	,3	,//由子窗口直接跳轉到主窗口指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"sessionid"	,"sessionid"	,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,2	,0	,0},
		},
														},
	{/*163*/1	,"md5"		,"md5"		,4	,//MD5加密指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,100	,"string"		,"string"		,5	,7	,0	,0},
			{140	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*164*/2	,"flash"	,"flash"	,2	,//模擬外線拍叉簧指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*165*/2,"gwquery"	,"gwquery"	,13	,//查詢外部數據網關
		{
			{0,   20	,"id",        		"id",           5,  0,  0,  0},
			{20,  20	,"gwid",      		"gwid",         2,  6,  1,  6},
			{40,  20	,"msgtype",   		"msgtype",      2,  6,  1,  7},
			{60,  150	,"inputitem0",    "inputitem0",   5,  7,  0,  0},
			{210, 150	,"inputitem1",    "inputitem1",   5,  7,  0,  0},
			{360, 150	,"inputitem2",    "inputitem2",   5,  7,  0,  0},
			{510, 150	,"inputitem3",    "inputitem3",   5,  7,  0,  0},
			{660, 20	,"returnmsgtype", "returnmsgtype",10, 1,  0,  0},
			{680, 20	,"returnitem0",   "returnitem0",  10, 1,  0,  0},
			{700, 20	,"returnitem1",   "returnitem1",  10, 1,  0,  0},
			{720, 20	,"returnitem2",   "returnitem2",  10, 1,  0,  0},
			{740, 20	,"returnitem3",   "returnitem3",  10, 1,  0,  0},
			{760, 20	,"next",          "next",         5,  0,  0,  0},
		},
	},
	{/*166*/1,"fork"	,"newsession"	,10	,//克窿會話
		{
			{0,   20, "id",         "id",         5,  0,  0,  0},
			{20,  20, "returnsession", "returnsession",  10, 1,  0,  0}, //返回的新會話標志
			{40,  20, "goto",       "goto",       5,  0,  0,  0}, //新會話的起始id
			{60,  20, "gotoaddr",   "gotoaddr",   5,  0,  0,  0},
			{80,  20	,"chntype",   "chntype",    2,  6,  4,  24},
			{100,  20	,"chnno",     "chnno",      2,  6,  1,  7},
			{120,  20	,"callerno",  "callerno",   5,  6,  0,  0},
			{140,  20	,"calledno",  "calledno",   5,  6,  0,  0},
			{160,  20	,"inout",     "inout",      2,  6,  1,  6},
			{180,  20, "next",      "next",       5,  0,  0,  0},
		},
	},
	{/*167*/1	,"writeinifile"	,"writeinifile"	,6	,//讀配置文件函數
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,150	,"inifile"		,"inifile"		,5	,7	,0	,0},
			{170	,50	,"section"		,"section"		,5	,7	,0	,0},
			{220	,50	,"paramer"		,"paramer"		,5	,7	,0	,0},
			{270	,100	,"value"		,"value"		,5	,7	,0	,0},
			{370	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*168*/1	,"inc"	,"inc"	,3	,//變量加1
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*169*/1	,"dec"	,"dec"	,3	,//變量減1
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*170*/1	,"if"	,"if"	,7	,//IF條件判斷
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,250	,"cond"		,"cond"		,7	,8	,0	,0},
			{270	,20	,"thengoto"		,"thengoto"		,5	,0	,0	,0},
			{290	,20	,"thenaddr"		,"thenaddr"		,2	,0	,0	,0},
			{310	,20	,"elsegoto"		,"elsegoto"		,5	,0	,0	,0},
			{330	,20	,"elseaddr"		,"elseaddr"		,2	,0	,0	,0},
			{350	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*171*/1	,"createmutex"	,"createmutex"	,3	,//建立互斥
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"mutexid"		,"mutexid"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*172*/1	,"releasemutex"	,"releasemutex"	,3	,//釋放互斥
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"mutexid"		,"mutexid"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*173*/1	,"crypt"		,"crypt"		,6	,//密碼加密指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,5	,1	,0	,0},
			{40	,20	,"crypttype"		,"crypttype"		,5	,7	,0	,0},
			{60	,100	,"key"		,"key"		,5	,7	,0	,0},
			{160	,250	,"salt"		,"salt"		,5	,7	,0	,0},
			{410	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*174*/1	,"setdbgateway"	,"setdbgateway"	,3	,//設置數據網關指令
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"gwid"		,"gwid"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*175*/2	,"setvcparam"	,"setvcparam"	,7	,//設置變聲參數指令
		{
			{0,20	,"id",       "id"		,5	,0	,0	,0},
			{20,20	,"vcmode",   "vcmode"		,2	,2	,1	,3},
			{40,20	,"vcparam",  "vcparam"		,2	,2	,1	,6},
			{60,20	,"vcmix",    "vcmix"		,2	,2	,1	,6},
			{80,20	,"noisemode","noisemode"		,2	,2	,1	,6},
			{100,20	,"noiseparam","noiseparam"		,2	,2	,1	,0},
			{120,20	,"next","next"		,5	,0	,0	,0},
		},
	},
	{/*176*/	3	,"gettolltime"	,"gettolltime"	,11	,//根據余額取最大通話時長
		{
			{0	,20	,"id"		,"id"		            ,5	,0	,0	,0},
			{20	,20	,"tollid"		,"tollid"		    ,2	,2	,0	,0},
			{40	,20	,"funcgroup"		,"funcgroup",5	,6	,0	,0},
			{60	,20	,"funcno"		,"funcno"		    ,5	,6	,0	,0},
			{80	,20	,"callerno"		,"callerno"		,5	,6	,0	,0},
			{100,20	,"calledno"		,"calledno"		,5	,6	,0	,0},
			{120,20	,"account"		,"account"		,5	,6	,0	,0},
			{140,20	,"starttime"		,"starttime",5	,6	,0	,0},
			{160,20	,"balance"		,"balance"		,3	,2	,0	,0},
			{180,20	,"returntolltime"		,"returntolltime"		,10	,1	,0	,0},
			{200,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*177*/	2	,"pause"		,"pause"		,2	,//暫停放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*178*/	2	,"replay"		,"replay"		,2	,//重新放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*179*/	2	,"fastplay"		,"fastplay"		,4	,//快速放音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"direction"	,"direction"	,2	,2	,1	,0},
			{40	,20	,"length"	,"length"	,2	,2	,1	,7},
			{50	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*180*/	2	,"checktone"		,"checktone"		,5	,//檢測信號音
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"tonetype"	,"tonetype"	,2	,2	,4	,6},
			{40	,100	,"toneparam"	,"toneparam"	,5	,7	,0	,0},
			{140,20	,"returntonetype"		,"returntonetype"		,10	,1	,0	,0},
			{160,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*181*/2,"setchannel"	,"setchannel",4,//設置會話通道號
		{
			{0,20	,"id",       "id"		,5	,0	,0	,0},
			{20,20	,"chntype",   "chntype"		,2	,6	,4	,24},
			{40,20	,"chnno",  "chnno"		,2	,6	,0	,0},
			{60,20	,"next","next"		,5	,0	,0	,0},
		},
	},
	{/*182*/3	,"dbrecord","dbrecord",37	,//讀數據表當前記錄
		{
			{0,   20, "id",             "id",            5,0,0,0},
			{20,  20, "recordno",       "recordno",      5,2,0,0},
			{40,  20, "autogotonext",   "autogotonext",  2,2,0,0},
			{60,	20,	"returnrecordno", "returnrecordno",10,1,0,0},
			{80,	20,	"returnfield0",	  "returnfield0",  10,1,0,0},
			{100,	20,	"returnfield1",	  "returnfield1",  10,1,0,0},
			{120,	20,	"returnfield2",	  "returnfield2",  10,1,0,0},
			{140,	20,	"returnfield3",	  "returnfield3",  10,1,0,0},
			{160,	20,	"returnfield4",	  "returnfield4",  10,1,0,0},
			{180,	20,	"returnfield5",	  "returnfield5",  10,1,0,0},
			{200,	20,	"returnfield6",	  "returnfield6",  10,1,0,0},
			{220,	20,	"returnfield7",	  "returnfield7",  10,1,0,0},
			{240,	20,	"returnfield8",	  "returnfield8",  10,1,0,0},
			{260,	20,	"returnfield9",	  "returnfield9",  10,1,0,0},
			{280,	20,	"returnfield10",	"returnfield10", 10,1,0,0},
			{300,	20,	"returnfield11",	"returnfield11", 10,1,0,0},
			{320,	20,	"returnfield12",	"returnfield12", 10,1,0,0},
			{340,	20,	"returnfield13",	"returnfield13", 10,1,0,0},
			{360,	20,	"returnfield14",	"returnfield14", 10,1,0,0},
			{380,	20,	"returnfield15",	"returnfield15", 10,1,0,0},
			{400,	20,	"returnfield16",	"returnfield16", 10,1,0,0},
			{420,	20,	"returnfield17",	"returnfield17", 10,1,0,0},
			{440,	20,	"returnfield18",	"returnfield18", 10,1,0,0},
			{460,	20,	"returnfield19",	"returnfield19", 10,1,0,0},
			{480,	20,	"returnfield20",	"returnfield20", 10,1,0,0},
			{500,	20,	"returnfield21",	"returnfield21", 10,1,0,0},
			{520,	20,	"returnfield22",	"returnfield22", 10,1,0,0},
			{540,	20,	"returnfield23",	"returnfield23", 10,1,0,0},
			{560,	20,	"returnfield24",	"returnfield24", 10,1,0,0},
			{580,	20,	"returnfield25",	"returnfield25", 10,1,0,0},
			{600,	20,	"returnfield26",	"returnfield26", 10,1,0,0},
			{620,	20,	"returnfield27",	"returnfield27", 10,1,0,0},
			{640,	20,	"returnfield28",	"returnfield28", 10,1,0,0},
			{660,	20,	"returnfield29",	"returnfield29", 10,1,0,0},
			{680,	20,	"returnfield30",	"returnfield30", 10,1,0,0},
			{700,	20,	"returnfield31",	"returnfield31", 10,1,0,0},
			{720, 20, "next",           "next",          5,0,0,0},
		},
	},
	{/*183*/2,"workerlogin"	,"workerlogin"	,9	,//話務員登錄
		{
			{0,   20	,"id",            "id",            5,  0,  0,  0},
			{20,  20	,"seatno",      	"seatno",      	 5,  6,  0,  0},
			{40,  20	,"workerno",      "workerno",      2,  6,  0,  0},
			{60,  50	,"workername",    "workername",    5,  7,  0,  0},
			{110, 50	,"password",      "password",      5,  7,  0,  0},
			{160, 20	,"groupno",       "groupno",       5,  7,  0,  0},
			{180, 20	,"class",         "class",         2,  6,  0,  0},
			{200, 20	,"level",         "level",         5,  7,  0,  0},
			{220, 20	,"next",          "next",          5,  0,  0,  0},
		},
	},
	{/*184*/2,"workerlogout"	,"workerlogout"	,3	,//話務員退出
		{
			{0,   20	,"id",            "id",            5,  0,  0,  0},
			{20,  20	,"seatno",      	"seatno",      	 5,  6,  0,  0},
			{40,  20	,"next",          "next",          5,  0,  0,  0},
		},
	},
	{/*185*/3	,"dbspoutparam","dbspoutparam",25	,//取存儲過程輸出參數值
		{
			{0,   20, "id",             "id",            5,0,0,0},
			{20,  200,"params",         "params",        5,7,0,0},
			{220,	20,	"outparam0",	    "outparam0",     10,1,0,0},
			{240,	20,	"outparam1",	    "outparam1",     10,1,0,0},
			{260,	20,	"outparam2",	    "outparam2",     10,1,0,0},
			{280,	20,	"outparam3",	    "outparam3",     10,1,0,0},
			{300,	20,	"outparam4",	    "outparam4",     10,1,0,0},
			{320,	20,	"outparam5",	    "outparam5",     10,1,0,0},
			{340,	20,	"outparam6",	    "outparam6",     10,1,0,0},
			{360,	20,	"outparam7",	    "outparam7",     10,1,0,0},
			{380,	20,	"outparam8",	    "outparam8",     10,1,0,0},
			{400,	20,	"outparam9",	    "outparam9",     10,1,0,0},
			{420,	20,	"outparam10",	    "outparam10",    10,1,0,0},
			{440,	20,	"outparam11",	    "outparam11",    10,1,0,0},
			{460,	20,	"outparam12",	    "outparam12",    10,1,0,0},
			{480,	20,	"outparam13",	    "outparam13",    10,1,0,0},
			{500,	20,	"outparam14",	    "outparam14",    10,1,0,0},
			{520,	20,	"outparam15",	    "outparam15",    10,1,0,0},
			{540,	20,	"outparam16",	    "outparam16",    10,1,0,0},
			{560,	20,	"outparam17",	    "outparam17",    10,1,0,0},
			{580,	20,	"outparam18",	    "outparam18",    10,1,0,0},
			{600,	20,	"outparam19",	    "outparam19",    10,1,0,0},
			{620,	20,	"outparam20",	    "outparam20",    10,1,0,0},
			{640,	20,	"outparam21",	    "outparam21",    10,1,0,0},
			{660, 20, "next",           "next",          5,0,0,0},
		},
	},
	{/*186*/3,"gwmakecallresult"	,"gwmakecallresult"	,8	,//將呼叫結果發送給外部數據網關
		{
			{0,   20	,"id",        "id",         5,  0,  0,  0},
			{20,  20	,"gwid",      "gwid",       2,  2,  0,  0},
			{40,  20	,"gwserialno","gwserialno", 2,  6,  0,  0},
			{60,  20	,"callerno",  "callerno",   5,  7,  0,  0},
			{80,  20	,"calledno",  "calledno",   5,  7,  0,  0},
			{100, 20	,"account",   "account",    5,  7,  0,  0},
			{120, 20	,"result",    "result",     2,  2,  0,  0},
			{140, 100	,"param",     "param",      5,  7,  0,  0},
			{240, 20	,"next",      "next",       5,  0,  0,  0},
		},
	},
	{/*187*/3,"gwmsgeventresult"	,"gwmsgeventresult"	,6	,//向外部數據網關發送消息事件處理結果
		{
			{0,   20	,"id",        "id",         5,  0,  0,  0},
			{20,  20	,"gwid",      "gwid",       2,  2,  0,  0},
			{40,  20	,"gwserialno","gwserialno", 2,  6,  0,  0},
			{60,  20	,"result",    "result",     2,  2,  0,  0},
			{80,  100	,"param",     "param",      5,  7,  0,  0},
			{100, 20	,"next",      "next",       5,  0,  0,  0},
		},
	},
	{/*188*/2,"agtrancallresult"	,"agtrancallresult"	,14	,//向坐席發送轉接呼叫結果
		{
			{0,   20	,"id",        "id",         5,  0,  0,  0},
			{20,  20	,"seatno",    "seatno",     5,  7,  0,  0},
			{40,  20	,"workerno",  "workerno",   2,  6,  0,  0},
			{60,  20	,"trantype",  "trantype",   2,  6,  1,  6},
			{80,  20	,"sessionno", "sessionno",  2,  6,  0,  0},
			{100, 20	,"chntype",   "chntype",    2,  6,  4,  24},
			{120, 20	,"chnno",     "chnno",      2,  6,  1,  7},
			{140, 20	,"cfcno",     "cfcno",      2,  6,  1,  6},
			{160, 20	,"callerno",  "callerno",   5,  7,  0,  0},
			{180, 20	,"calledno",  "calledno",   5,  7,  0,  0},
			{200, 100	,"param",     "param",      5,  7,  0,  0},
			{300, 20	,"result",    "result",     2,  2,  0,  0},
			{320, 100	,"errorbuf",  "errorbuf",   5,  7,  0,  0},
			{420, 20	,"next",      "next",       5,  0,  0,  0},
		},
	},
	{/*189*/3,"agtranivrresult"	,"agtranivrresult"	,8	,//向坐席發送坐席將來話轉接到ivr流程結果
		{
			{0,   20	,"id",        "id",         5,  0,  0,  0},
			{20,  20	,"seatno",    "seatno",     5,  7,  0,  0},
			{40,  20	,"workerno",  "workerno",   2,  6,  0,  0},
			{60,  20	,"returnflag","returnflag", 2,  2,  0,  0},
			{80,  100	,"param",     "param",      5,  7,  0,  0},
			{180, 20	,"result",    "result",     2,  2,  0,  0},
			{200, 100	,"errorbuf",  "errorbuf",   5,  7,  0,  0},
			{300, 20	,"next",      "next",       5,  0,  0,  0},
		},
	},
	{/*190*/3,"agjoinconfresult"	,"agjoinconfresult"	,10	,//向坐席發送指定通道參加會議結果
		{
			{0,   20	,"id",        "id",         5,  0,  0,  0},
			{20,  20	,"seatno",    "seatno",     5,  7,  0,  0},
			{40,  20	,"workerno",  "workerno",   2,  6,  0,  0},
			{60,  20	,"destchntype","destchntype",2, 6,  4,  24},
			{80,  20	,"destchnno", "destchnno",  2,  6,  1,  7},
			{100, 20	,"confno", 		"confno",  		2,  6,  1,  6},
			{120, 20	,"jointype", 	"jointype",  	2,  6,  1,  6},
			{140, 20	,"result",    "result",     2,  2,  0,  0},
			{160, 100	,"errorbuf",  "errorbuf",   5,  7,  0,  0},
			{260, 20	,"next",      "next",       5,  0,  0,  0},
		},
	},
	{/*191*/1,"sendagentmsg"	,"sendagentmsg"	,9	,//發送消息到坐席
		{
			{0,   20	,"id",        "id",         5,  0,  0,  0},
			{20,  20	,"seatno",      "seatno",       5,  6,  0,  0},
			{40,  20	,"workerno",      "workerno",       2,  6,  0,  0},
			{60,  20	,"msgtype",   "msgtype",    2,  2,  1,  7},
			{80,  100	,"item0",     "item0",      5,  7,  0,  0},
			{180,  100	,"item1",     "item1",      5,  7,  0,  0},
			{280,  100	,"item2",     "item2",      5,  7,  0,  0},
			{380,  100	,"item3",     "item3",      5,  7,  0,  0},
			{480,  20	,"next",      "next",       5,  0,  0,  0},
		},
	},
	{/*192*/2,"pickupcall"	,"pickupcall"	,12	,//代接電話
		{
			{0,   20	,"id",             "id",           5,  0,  0,  0},
			{20,  100	,"groupno",        "groupno",      5,  7,  0,  0},
			{120, 20	,"seatno",         "seatno",       5,  6,  0,  0},
			{140, 20	,"workerno",       "workerno",     2,  6,  0,  0},
			{160, 20	,"selectmode",     "selectmode",   2,  2,  0,  0},
			{180, 20	,"returncallerno", "returncallerno",10, 1,  0,  0},
			{200, 20	,"returncalledno", "returncalledno",10, 1,  0,  0},
			{220, 20	,"returnchntype",  "returnchntype", 10, 1,  0,  0},
			{240, 20	,"returnchnno",    "returnchnno",  10, 1,  0,  0},
			{260, 20	,"returnseatno",   "returnseatno", 10, 1,  0,  0},
			{280, 20	,"returnworkerno", "returnworkerno", 10, 1,  0,  0},
			{300, 20	,"next",           "next",         5,  0,  0,  0},
		},
	},
	{/*193*/2,"getacdqueue"	,"getacdqueue"	,5	,//取ACD隊列信息
		{
			{0,   20	,"id",             "id",           5,  0,  0,  0},
			{20,  50	,"groupno",        "groupno",      5,  7,  0,  0},
			{70,  20	,"returnwaitacd",  "returnwaitacd",10, 1,  0,  0},
			{90,  20	,"returnwaitans",  "returnwaitans",10, 1,  0,  0},
			{110,  20	,"next",           "next",         5,  0,  0,  0},
		},
	},
	{/*194*/2,"getacdqueue"	,"getacdqueue"	,3	,//設置通道流程業務狀態信息
		{
			{0,   20	,"id",             "id",           5,  0,  0,  0},
			{20,  100	,"flwstate",       "flwstate",     5,  7,  0,  0},
			{120, 20	,"next",           "next",         5,  0,  0,  0},
		},
	},
	{/*195*/	2	,"swapcallresult"	,"swapcallresult"	,3	,//將呼出或呼叫坐席結果改發送到指定的會話
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20,	"sessionid",				"sessionid",		2,6,0,0},
			{40,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*196*/	2	,"recvfsk"	,"recvfsk"	,7	,//接收FSK
		{
			{0,		20,	"id",								"id",						5,	0,	0,	0},
			{20,	20,	"fskheader",				"fskheader",		2,	2,	1,	6},
			{40,	20,	"crctype",					"crctype",			2,	2,	1,	6},
			{60,	20,	"returnfskcmd",			"returnfskcmd",	10,	1,	0,	0},
			{80,	20,	"returnfskdata",		"returnfskdata",10,	1,	0,	0},
			{100,	20,	"returndatalen",		"returndatalen",10,	1,	0,	0},
			{120,	20,	"next",							"next",					5,	0,	0,	0},
		},
	},
	{/*197*/	2	,"transferivr","transferivr"	,5	,//轉接到IVR
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20,	"vopno",						"vopno",				2,6,0,0},
			{40,	20,	"ivrno",						"ivrno",				5,7,0,0},
			{60,	250,"param",						"param",				5,7,0,0},
			{310,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*198*/	2	,"releaseivr","releaseivr"	,3	,//釋放IVR資源
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*199*/	2	,"playswitchvoice","playswitchvoice"	,3	,//播放交換機內置語音
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20,	"voiceid",					"voiceid",			2,6,0,0},
			{40,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*200*/	2	,"sendswitchcmd","sendswitchcmd"	,4	,//發送交換機操作命令
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20,	"cmdid",					  "cmdid",			  2,6,0,0},
			{40,	100,"cmdparam",					"cmdparam",			5,7,0,0},
			{140,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*201*/	2	,"dial","dial"	,5	,//撥打電話
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20, "callerno",					"callerno",			5,7,0,0},
			{40,	20, "calledno",					"calledno",			5,7,0,0},
			{60,	20,	"routeno",					"routeno",			2,6,0,0},
			{80,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*202*/3	,"dbfieldtofile"	,"dbfieldtofile"	,6	,//取查詢的字段值并存放到指定的文件
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	50,	"fieldname",				"fieldname",		5,7,0,0},
			{70,	100,"pathname",					"pathname",			5,7,0,0},
			{170,	100,"filename",					"filename",			5,7,0,0},
			{270,	20,	"returnfilename",		"returnfilename",10,1,0,0},
			{290,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*203*/3	,"dbfiletofield"	,"dbfiletofield"	,7	,//將文件內容存入數據表
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	100,"pathname",					"pathname",			5,7,0,0},
			{120,	100,"filename",					"filename",			5,7,0,0},
			{220,	50, "tablename",				"tablename",		5,7,0,0},
			{270,	50,	"fieldname",				"fieldname",		5,7,0,0},
			{320,	100,"wheresql",					"wheresql",			9,12,0,0},
			{420,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*204*/2	,"appendfaxfile"	,"appendfaxfile"	,10	,//追加發送的傳真文件
		{
			{0,		20,	"id",								"id",						5,0,0,0},
			{20,	20,	"faxcsid",					"faxcsid",			5,7,0,0},
			{40,	20,	"resolution",				"resolution",		5,7,0,0},
			{60,	20,	"totalpages",				"totalpages",		2,6,1,6},
			{80,	20,	"pagelist",					"pagelist",			5,7,0,0},
			{100,	100,"faxheader",				"faxheader",		5,7,0,0},
			{200,	100,"faxfooter",				"faxfooter",		5,7,0,0},
			{300,	50,	"barcode",					"barcode",			5,7,0,0},
			{350,	250,"filename",					"filename",			5,7,0,0},
			{600,	20,	"next",							"next",					5,0,0,0},
		},
	},
	{/*205*/1	,"getguid"	,"getguid"	,3	,//產生GUID碼
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"varname"		,"varname"		,2	,2	,0	,0},
			{40	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
	{/*206*/	3	,"dbinsertex"	,"dbinsertex"	,3	,//插入記錄操作（針對長度>500的sql語句）
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,900	,"sqls"		,"sqls"		,9	,11	,0	,0},
			{920	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
													},
	{/*207*/	3	,"dbupdateex"	,"dbupdateex"	,3	,//修改記錄操作（針對長度>500的sql語句）
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,900	,"sqls"		,"sqls"		,9	,12	,0	,0},
			{920	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
														},
	{/*208*/	3	,"dbqueryex"	,"dbqueryex"	,5	,//查詢數據操作
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,900	,"sqls"		,"sqls"		,9	,10	,0	,0},
			{920	,20	,"returnrecords"	,"returnrecords"	,10	,1	,0	,0},
			{940	,20	,"returnfields"	,"returnfields"	,10	,1	,0	,0},
			{960	,20	,"next"		,"next"		,5	,0	,0	,0},
		},                                                                              
														},
	{/*209*/2,"bandagentchn"	,"bandagentchn",7,//綁定坐席通道
		{
			{0,20	  ,"id",       "id"		    ,5	,0	,0	,0},
			{20,20	,"seatno",   "seatno"		,5	,6	,0	,0},
			{40,20	,"phoneno",  "phoneno"	,5	,6	,0	,0},
			{60,20	,"chntype",  "chntype"	,2	,6	,4	,24},
			{80,20	,"chnno",    "chnno"		,2	,6	,0	,0},
			{100,20	,"state",    "state"		,2	,6	,0	,0},
			{120,20	,"next",     "next"		  ,5	,0	,0	,0},
		},
	},
	{/*210*/2,"sendcalltoagent"	,"sendcalltoagent",9,//發送電話分配到電腦坐席
		{
			{0,20	  ,"id",       "id"		    ,5	,0	,0	,0},
			{20,20	,"seatno",   "seatno"		,5	,6	,0	,0},
			{40,20	,"workerno", "workerno"	,5	,6	,0	,0},
			{60,20	,"callinout","callinout",2	,6	,0	,0},
			{80,20	,"calltype", "calltype"	,2	,6	,0	,0},
			{100,20	,"callerno", "callerno"	,5	,6	,0	,0},
			{120,20	,"calledno", "calledno"	,5	,6	,0	,0},
			{140,100,"param",    "param"	  ,5	,7	,0	,0},
			{240,20	,"next",     "next"		  ,5	,0	,0	,0},
		},
	},
	{/*211*/3,"querywebservice"	,"querywebservice",11,//執行查詢外部webservice接口數據
		{
			{0,20	  ,"id",              "id"		    ,5	,0	,0	,0},
			{20, 20	,"gwid",      		"gwid",         2,  6,  1,  6},
			{40,100	,"wsdladdress",     "wsdladdress"		,5	,7	,0	,0},
			{140,100,"soapaction",      "soapaction"	,5	,7	,0	,0},
			{240,50	,"startenvelope",   "startenvelope",5	,7	,0	,0},
			{290,50	,"startbody",       "startbody"	,5	,7	,0	,0},
			{340,100,"interfacestring", "interfacestring"	,5	,7	,0	,0},
			{440,20	,"paramcount",      "paramcount"	,2	,2	,0	,0},
			{460,500,"paramstring",     "paramstring"	  ,5	,7	,0	,0},
			{960,20	,"returnstring",    "returnstring"	,10	,1	,0	,0},
			{980,20	,"next",            "next"		  ,5	,0	,0	,0},
		},
	},
	{/*212*/3,"getwebservicexml"	,"getwebservicexml",7,//從外部webservice接口查詢的XML串中取數據
		{
			{0,20	  ,"id",              "id"		    ,5	,0	,0	,0},
			{20, 20	,"gwid",      		  "gwid",         2,  6,  1,  6},
			{40,200	,"noderoot",        "noderoot"		,5	,7	,0	,0},
			{240,20 ,"nodeindex",       "nodeindex"	,2	,2	,0	,0},
			{260,100,"attrname",        "attrname",5	,7	,0	,0},
			{360,20	,"returnstring",    "returnstring"	,10	,1	,0	,0},
			{380,20	,"next",            "next"		  ,5	,0	,0	,0},
		},
	},
	{/*213*/1,"getxmlstringdata"	,"getxmlstringdata",9,//取xml串的節點數據
		{
			{0,20	  ,"id",              "id"		    ,5	,0	,0	,0},
			{20,200	,"noderoot",        "noderoot"		,5	,7	,0	,0},
			{220,20,"nodeindex",        "nodeindex"	,2	,2	,0	,0},
			{240,100,"attrname",        "attrname",5	,7	,0	,0},
			{340,500,"xmlstring",       "xmlstring",5	,7	,0	,0},
			{840,20	,"nodesvarname",    "nodesvarname"	,5	,1	,0	,0},
			{860,20	,"valuevarname",    "valuevarname"	,5	,1	,0	,0},
			{880,20	,"existvarname",    "existvarname"	,5	,1	,0	,0},
			{900,20	,"next",            "next"		  ,5	,0	,0	,0},
		},
	},
	{/*214*/1,"getxmlfiledata"	,"getxmlfiledata",9,//取xml文件的節點數據
		{
			{0,20	  ,"id",              "id"		    ,5	,0	,0	,0},
			{20,200	,"noderoot",        "noderoot"		,5	,7	,0	,0},
			{220,20,"nodeindex",        "nodeindex"	,2	,2	,0	,0},
			{240,100,"attrname",        "attrname",5	,7	,0	,0},
			{340,100,"xmlfile",         "xmlfile",5	,7	,0	,0},
			{440,20	,"nodesvarname",    "nodesvarname"	,5	,1	,0	,0},
			{460,20	,"valuevarname",    "valuevarname"	,5	,1	,0	,0},
			{480,20	,"existvarname",    "existvarname"	,5	,1	,0	,0},
			{500,20	,"next",            "next"		  ,5	,0	,0	,0},
		},
	},
	{/*215*/3,"closewebservice","closewebservice",3,//關閉外部webservice接口
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"gwid",      			"gwid",							2,	6,	1,	6},
			{40,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*216*/1,"strreplace",	"strreplace",	8,	//字符串替換
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"varname",      	"varname",					5,	1,	0,	0},
			{40, 	500,"string",      		"string",						5,	7,	0,	0},
			{540, 100,"oldstr",      		"oldstr",						5,	7,	0,	0},
			{640, 100,"newstr",      		"newstr",						5,	7,	0,	0},
			{740, 20,	"startindex",     "startindex",				2,	2,	0,	0},
			{760, 20,	"replacenums",    "replacenums",			2,	2,	0,	0},
			{780,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*217*/1,"getmarkerstring",	"getmarkerstring",	8,	//取特殊標簽位置的字符串
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"strvarname",     "strvarname",				5,	1,	0,	0},
			{40, 	20,	"existvarname",   "existvarname",			5,	1,	0,	0},
			{60, 	500,"string",      		"string",						5,	7,	0,	0},
			{560, 100,"startmarker",    "startmarker",			5,	7,	0,	0},
			{660, 100,"endmarker",      "endmarker",				5,	7,	0,	0},
			{760, 20,	"getindex",     	"getindex",					2,	2,	0,	0},
			{780,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*218*/1,"base64encode",	"base64encode",	4,	//字符串base64編碼
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"varname",      	"varname",					5,	1,	0,	0},
			{40, 	500,"string",      		"string",						5,	7,	0,	0},
			{540,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*219*/1,"base64decode",	"base64decode",	4,	//字符串base64解碼
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"varname",      	"varname",					5,	1,	0,	0},
			{40, 	500,"string",      		"string",						5,	7,	0,	0},
			{540,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*220*/3,"getwebservicestr"	,"getwebservicestr",7,//從外部webservice接口查詢的字符串中取數據
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20,	20,	"gwid",      		  "gwid",         		2,  6,  1,  6},
			{40,	250,"startmarker",    "startmarker",			5,	7,	0,	0},
			{290,	250,"endmarker",      "endmarker",				5,	7,	0,	0},
			{540,	20,	"getindex",     	"getindex",					2,	2,	0,	0},
			{560,	20,	"returnstring",   "returnstring",			10,	1,	0,	0},
			{580,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*221*/1,"leftstrreplace",	"leftstrreplace",	6,	//替換字符串左邊指定的子字符串
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"varname",      	"varname",					5,	1,	0,	0},
			{40, 	500,"string",      		"string",						5,	7,	0,	0},
			{540, 100,"leftstr",      	"leftstr",					5,	7,	0,	0},
			{640, 100,"newstr",      		"newstr",						5,	7,	0,	0},
			{740,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*222*/1,"rightstrreplace",	"rightstrreplace",	6,	//替換字符串右邊指定的子字符串
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20, 	20,	"varname",      	"varname",					5,	1,	0,	0},
			{40, 	500,"string",      		"string",						5,	7,	0,	0},
			{540, 100,"rightstr",      	"rightstr",					5,	7,	0,	0},
			{640, 100,"newstr",      		"newstr",						5,	7,	0,	0},
			{740,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*223*/2,"play",								"play",							15,	//通用放音指令
		{
			{0,		20,	"id",							"id",								5,0,0,0},
			{20,	150,"dtmfrule",				"dtmfrule",					5,7,0,0},
			{170,	150,"playrule",				"playrule",					5,7,0,0},
			{320,	20,	"asrrule",				"asrrule",					5,7,0,0},
			{340,	20,	"chntype",				"chntype",					2,6,4,24},
			{360,	20,	"chnno",					"chnno",						2,6,0,0},
			{380,	100,"content1",				"content1",					5,7,0,0},
			{480,	100,"content2",				"content2",					5,7,0,0},
			{580,	100,"content3",				"content3",					5,7,0,0},
			{680,	100,"content4",				"content4",					5,7,0,0},
			{780,	100,"content5",				"content5",					5,7,0,0},
			{880,	80,	"content6",				"content6",					5,7,0,0},
			{960,	20,	"returndtmfs",		"returndtmfs",			10,1,0,0},
			{980,	20,	"returnasr",			"returnasr",				10,1,0,0},
			{1000,20,	"next",						"next",							5,0,0,0},
		},
	},
	{/*224*/3,"getwebserviceindex"	,"getwebserviceindex",7,//從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20,	20,	"gwid",      		  "gwid",         		2,  6,  1,  6},
			{40,	250,"startmarker",    "startmarker",			5,	7,	0,	0},
			{290,	250,"endmarker",      "endmarker",				5,	7,	0,	0},
			{540,	100,"string",     	  "string",					  5,	7,	0,	0},
			{640,	20,	"returnindex",    "returnindex",			10,	1,	0,	0},
			{660,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*225*/2,"workerloginex"	,"workerloginex"	,16	,//話務員登錄擴展指令
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  20	,"seatno",      	"seatno",      	 		5,  6,  0,  0},
			{40,  20	,"workerno",      "workerno",      		2,  6,  0,  0},
			{60,  50	,"workername",    "workername",    		5,  7,  0,  0},
			{110, 50	,"password",      "password",      		5,  7,  0,  0},
			{160, 50	,"groupno",       "groupno",       		5,  7,  0,  0},
			{210, 20	,"class",         "class",         		2,  6,  0,  0},
			{230, 20	,"level",         "level",         		5,  7,  0,  0},
			{250, 20	,"clearid",       "clearid",       		2,  6,  0,  0},
			{270, 20	,"state",      	  "state",       			2,  6,  0,  0},
			{290, 20	,"dutyno",        "dutyno",      			2,  6,  0,  0},
			{310, 20	,"autorecord",    "autorecord",      	2,  6,  0,  0},
			{330, 20	,"calldirection", "calldirection",    2,  6,  0,  0},
			{350, 20	,"accountno",     "accountno",      	5,  7,  0,  0},
			{370, 20	,"departmentno",  "departmentno",     2,  6,  0,  0},
			{390, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*226*/2,"workerlogoutex"	,"workerlogoutex"	,4	,//話務員退出擴展指令
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  20	,"seatno",      	"seatno",      	 		5,  6,  0,  0},
			{40,  20	,"workerno",      "workerno",      		2,  6,  0,  0},
			{60,  20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*227*/2,"getseatlogindata"	,"getseatlogindata"	,13	,//取坐席登錄的話務員信息
		{
			{0,   20, "id",            "id",            		5,  0,  0,  0},
			{20,  20, "seatno",      	 "seatno",      	 		5,  6,  0,  0},
			{40,  20, "workerno",      "workerno",      		2,  6,  0,  0},
			{60,	20,	"returnseatno",   "returnseatno",		  10,	1,	0,	0},
			{80,	20,	"returnworkerno", "returnworkerno",		10,	1,	0,	0},
			{100,	20,	"returnworkername","returnworkername",10,	1,	0,	0},
			{120,	20,	"returngroupno",  "returngroupno",		10,	1,	0,	0},
			{140,	20,	"returndisturbid","returndisturbid",	10,	1,	0,	0},
			{160,	20,	"returnleaveid",  "returnleaveid",		10,	1,	0,	0},
			{180,	20,	"returndutyno",   "returndutyno",			10,	1,	0,	0},
			{200,	20,	"returnaccountno","returnaccountno",	10,	1,	0,	0},
			{220,	20,	"returnlogtime",  "returnlogtime",		10,	1,	0,	0},
			{240, 20, "next",           "next",          		5,  0,  0,  0},
		},
	},
	{/* 228*/	1	,"callsubformex"	,"callsubformex"	,3	,//子窗口調用擴展指令
		{
			{0,		20,	"id",							"id",								5,	0,	0,	0},
			{20,	50,	"subformname",		"subformname",			5,	7,	0,	0},
			{70,	20,	"next",						"next",							5,	0,	0,	0},
		},
	},
	{/*229*/3,"httprequest"	,"httprequest",7,//發送HTTP協議請求
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20,	20,	"gwid",      		  "gwid",         		2,  6,  1,  6},
			{40,	250,"httpurl",    		"httpurl",					5,	7,	0,	0},
			{290,	250,"httpparam",      "httpparam",				5,	7,	0,	0},
			{540,	20,	"requesttype",   	"requesttype",			5,	6,	0,	0},
			{560,	20,	"returnstring",   "returnstring",			10,	1,	0,	0},
			{580,	20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*230*/	2	,"getidleseat"	,"getidleseat"	,15	,//取一空閑坐席并臨時鎖定，并返回該組登錄座席總數和空閑座席總數
		{
			{0,		20,	"id",							"id",								5,	0,	0,	0},
			{20,	20,	"seatno",					"seatno",						5,	7,	0,	0},
			{40,	20,	"workerno",				"workerno",					2,	6,	0,	0},
			{60,	20,	"seattype",				"seattype",					2,	6,	1,	6},
			{80,	20,	"acdrule",				"acdrule",					2,	6,	1,	6},
			{100,	50,	"groupno",				"groupno",					5,	7,	0,	0},
			{150,	20,	"seatgroupno",		"seatgroupno",			2,	6,	1,	6},
			{170,	20,	"level",					"level",						5,	7,	0,	0},
			{190,	20,	"levelrule",			"levelrule",				2,	6,	1,	6},
			{210,	20,	"lock",						"lock",							2,	6,	1,	6},
			{230,	20,	"returnseatno",		"returnseatno",			10,	1,	0,	0},
			{250,	20,	"returnworkerno",	"returnworkerno",		10,	1,	0,	0},
			{270,	20,	"returnlogins",		"returnlogins",			10,	1,	0,	0},
			{290,	20,	"returnidles",		"returnidles",			10,	1,	0,	0},
			{310,	20,	"next",						"next",							5,	0,	0,	0},
		},
	},
	{/*231*/2,"unlockseat"	,"unlockseat"	,4	,//解除臨時鎖定的座席
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  20	,"seatno",      	"seatno",      	 		5,  6,  0,  0},
			{40,  20	,"workerno",      "workerno",      		2,  6,  0,  0},
			{60,  20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*232*/2,"updateservicescore"	,"updateservicescore"	,6	,//更新服務評價到后臺IVR服務
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  20	,"seatno",      	"seatno",      	 		5,  6,  0,  0},
			{40,  20	,"workerno",      "workerno",      		2,  6,  0,  0},
			{60,  50	,"cdrserialno",   "cdrserialno",      5,  6,  0,  0},
			{110, 20	,"score",      		"score",      			2,  6,  0,  0},
			{130, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*233*/2,"updatecdrfield"	,"updatecdrfield"	,5	,//更新CDR字段數據
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  50	,"cdrserialno",   "cdrserialno",      5,  6,  0,  0},
			{70,  50	,"fieldname",     "fieldname",      	5,  7,  0,  0},
			{120, 250	,"fieldvalue",    "fieldvalue",      	5,  7,  0,  0},
			{370, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*234*/2,"updatesysparam"	,"updatesysparam"	,4	,//更新平臺系統參數
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  50	,"paramname",     "paramname",      	5,  7,  0,  0},
			{70, 250	,"paramvalue",    "paramvalue",      	5,  7,  0,  0},
			{320, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*235*/3,"shellexecute"	,"shellexecute"	,8	,//執行外部程序
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,	20  ,"gwid",      		"gwid",         		2,  6,  1,  6},
			{40,  100	,"cmdname",     	"cmdname",      		5,  7,  0,  0},
			{140, 250	,"cmdparam",    	"cmdparam",      		5,  7,  0,  0},
			{390, 100	,"readfile",     	"readfile",      		5,  7,  0,  0},
			{490, 20	,"timeout",      	"timeout",      		2,  6,  0,  0},
			{510,	20  ,"returndata",		"returndata",				10,	1,	0,	0},
			{530, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*236*/3,"gettxtlinestr"	,"gettxtlinestr"	,10	,//取文本行某段字符串
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,	20  ,"gwid",      		"gwid",         		2,  6,  1,  6},
			{40,  100	,"readfile",     	"readfile",      		5,  7,  0,  0},
			{140, 20	,"lineno",      	"lineno",      		  2,  6,  0,  0},
			{160, 20	,"separator",     "separator",        4,  0,  0,  0},
			{180, 20	,"fieldno",      	"fieldno",      		2,  6,  0,  0},
			{200, 20	,"startpos",      "startpos",      		2,  6,  0,  0},
			{220, 20	,"length",      	"length",      		  2,  6,  0,  0},
			{240,	20  ,"returndata",		"returndata",				10,	1,	0,	0},
			{260, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*237*/2,"setforwarding"	,"setforwarding"	,9	,//設置分機呼轉
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  50	,"deviceno",   		"deviceno",      		5,  7,  0,  0},
			{70,  20	,"forwardtype",   "forwardtype",      2,  2,  1,  6},
			{90,  20	,"forwardonoff",  "forwardonoff",     2,  2,  1,  6},
			{110, 50	,"forwarddeviceno","forwarddeviceno", 5,  7,  0,  0},
			{160,	20	,"returntype",		"returntype",			  10,	1,	0,	0},
			{180,	20	,"returnonoff",		"returnonoff",			10,	1,	0,	0},
			{200,	20	,"returndeviceno","returndeviceno",		10,	1,	0,	0},
			{220, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*238*/3,"socketrequest"	,"socketrequest"	,9	,//通過socket通信發送查詢請求
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,	20  ,"gwid",      		"gwid",         		2,  6,  1,  6},
			{40,  50	,"serverip",   		"serverip",      		5,  7,  0,  0},
			{90,  20	,"serverport",  	"serverport",     	2,  6,  0,  0},
			{110, 20	,"streamtype",		"streamtype", 			5,  6,  0,  0},
			{130,	100	,"recvprotocol",	"recvprotocol",			5,	7,	0,	0},
			{230,	500	,"sendstring",		"sendstring",				5,	7,	0,	0},
			{730,	20	,"returndeviceno","returndeviceno",		10,	1,	0,	0},
			{750, 20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*239*/3,"dbrecorddump"	,"dbrecorddump"	,8	,//數據表記錄轉存
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,	20  ,"gwid",      		"gwid",         		2,  6,  1,  6},
			{40,  20	,"sourdbid",   		"sourdbid",      		5,  6,  0,  0},
			{60,  450	,"querysqls",  		"querysqls",     		5,  7,  0,  0},
			{510, 20	,"destdbid",   		"destdbid",      		5,  6,  0,  0},
			{530, 450	,"insertsqls",  	"insertsqls",     	5,  7,  0,  0},
			{980,	20	,"returnrecords",	"returnrecords",		10,	1,	0,	0},
			{1000,20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*240*/3,"httprequestex"	,"httprequestex",9,//發送HTTP協議請求擴展指令
		{
			{0,		20,	"id",             "id",								5,	0,	0,	0},
			{20,	20,	"gwid",      		  "gwid",         		2,  6,  1,  6},
			{40,	250,"httpurl",    		"httpurl",					5,	7,	0,	0},
			{290,	400,"httpparam",      "httpparam",				5,	7,	0,	0},
			{690,	250,"httpcookies",    "httpcookies",			5,	7,	0,	0},
			{940,	20,	"requesttype",   	"requesttype",			5,	6,	0,	0},
			{960,	20,	"returnstring",   "returnstring",			10,	1,	0,	0},
			{980,	20,	"returncookies",  "returncookies",		10,	1,	0,	0},
			{1000,20,	"next",           "next",							5,	0,	0,	0},
		},
	},
	{/*241*/2,"callfunction"	,"callfunction"	,5	,//特殊功能呼叫，如：監聽、強插
		{
			{0,   20	,"id",            "id",            		5,  0,  0,  0},
			{20,  20	,"calltype",   		"calltype",      		2,  2,  1,  6},
			{40,  20	,"seatno",      	"seatno",      	 		5,  6,  0,  0},
			{60,  20	,"workerno",      "workerno",      		2,  6,  0,  0},
			{80,  20	,"next",          "next",          		5,  0,  0,  0},
		},
	},
	{/*242*/3	,"dbselect"	,"dbselect"	,27	,//詢數據表記錄記錄字段值操作擴展指令
		{
			{0,		20,	"id",							"id",						 5,0,0,0},
			{20,	500,"sqls",						"sqls",					 9,10,0,0},
			{520,	20,	"returnrecords",	"returnrecords", 10,1,0,0},
			{540,	20,	"returnfields",		"returnfields",	 10,1,0,0},
			{560,	20,	"returnfield0",	  "returnfield0",  10,1,0,0},
			{580,	20,	"returnfield1",	  "returnfield1",  10,1,0,0},
			{600,	20,	"returnfield2",	  "returnfield2",  10,1,0,0},
			{620,	20,	"returnfield3",	  "returnfield3",  10,1,0,0},
			{640,	20,	"returnfield4",	  "returnfield4",  10,1,0,0},
			{660,	20,	"returnfield5",	  "returnfield5",  10,1,0,0},
			{680,	20,	"returnfield6",	  "returnfield6",  10,1,0,0},
			{700,	20,	"returnfield7",	  "returnfield7",  10,1,0,0},
			{720,	20,	"returnfield8",	  "returnfield8",  10,1,0,0},
			{740,	20,	"returnfield9",	  "returnfield9",  10,1,0,0},
			{760,	20,	"returnfield10",	"returnfield10", 10,1,0,0},
			{780,	20,	"returnfield11",	"returnfield11", 10,1,0,0},
			{800,	20,	"returnfield12",	"returnfield12", 10,1,0,0},
			{820,	20,	"returnfield13",	"returnfield13", 10,1,0,0},
			{840,	20,	"returnfield14",	"returnfield14", 10,1,0,0},
			{860,	20,	"returnfield15",	"returnfield15", 10,1,0,0},
			{880,	20,	"returnfield16",	"returnfield16", 10,1,0,0},
			{900,	20,	"returnfield17",	"returnfield17", 10,1,0,0},
			{920,	20,	"returnfield18",	"returnfield18", 10,1,0,0},
			{940,	20,	"returnfield19",	"returnfield19", 10,1,0,0},
			{960,	20,	"returnfield20",	"returnfield20", 10,1,0,0},
			{980,	20,	"returnfield21",	"returnfield21", 10,1,0,0},
			{1000,20,	"next",						"next",					 5,0,0,0},
		},                                                                              
	},
	{/*243*/2,"playex",							"playex",						13,	//增強型放音指令
		{
			{0,		20,	"id",							"id",								5,0,0,0},
			{20,	200,"dtmfrule",				"dtmfrule",					5,7,0,0},
			{220,	200,"playrule",				"playrule",					5,7,0,0},
			{420,	20,	"asrrule",				"asrrule",					5,7,0,0},
			{440,	100,"content1",				"content1",					5,7,0,0},
			{540,	100,"content2",				"content2",					5,7,0,0},
			{640,	80, "content3",				"content3",					5,7,0,0},
			{720,	80, "content4",				"content4",					5,7,0,0},
			{800,	80, "invalidvoc",			"invalidvoc",				5,7,0,0},
			{880,	80,	"timeoutvoc",			"timeoutvoc",				5,7,0,0},
			{960,	20,	"returndtmfs",		"returndtmfs",			10,1,0,0},
			{980,	20,	"returnasr",			"returnasr",				10,1,0,0},
			{1000,20,	"next",						"next",							5,0,0,0},
		},
	},
	{/*244*/3,"getwebservicejson"	,"getwebservicejson",	23,//從外部webservice接口查詢的JSON串中取數據
		{
			{0,		20,	"id",             "id",								5,0,0,0},
			{20, 	20,	"gwid",      		  "gwid",							2,6,1,6},
			{40,	250,"jsonnodetree",   "jsonnodetree",		  5,7,0,0},
			{290,	300,"jsonnamelist",   "jsonnamelist",			5,7,0,0},
			{590,	50, "jsonfilter",   	"jsonfilter",				5,7,0,0},
			{640,	20,	"returnrecords",	"returnrecords", 		10,1,0,0},
			{660,	20,	"returnfield0",	  "returnfield0",  		10,1,0,0},
			{680,	20,	"returnfield1",	  "returnfield1",  		10,1,0,0},
			{700,	20,	"returnfield2",	  "returnfield2",  		10,1,0,0},
			{720,	20,	"returnfield3",	  "returnfield3",  		10,1,0,0},
			{740,	20,	"returnfield4",	  "returnfield4",  		10,1,0,0},
			{760,	20,	"returnfield5",	  "returnfield5",  		10,1,0,0},
			{780,	20,	"returnfield6",	  "returnfield6",  		10,1,0,0},
			{800,	20,	"returnfield7",	  "returnfield7",  		10,1,0,0},
			{820,	20,	"returnfield8",	  "returnfield8",  		10,1,0,0},
			{840,	20,	"returnfield9",	  "returnfield9",  		10,1,0,0},
			{860,	20,	"returnfield10",	"returnfield10", 		10,1,0,0},
			{880,	20,	"returnfield11",	"returnfield11", 		10,1,0,0},
			{900,	20,	"returnfield12",	"returnfield12", 		10,1,0,0},
			{920,	20,	"returnfield13",	"returnfield13", 		10,1,0,0},
			{940,	20,	"returnfield14",	"returnfield14", 		10,1,0,0},
			{960,	20,	"returnfield15",	"returnfield15", 		10,1,0,0},
			{980,	20,	"next",           "next",							5,0,0,0},
		},
	},
	{/*245*/	2	,"ttsstringex"	,"ttsstringex"	,14	,//文本串轉語音擴展指令 2017-04-01
		{
			{0	,20	,"id"		,"id"		,5	,0	,0	,0},
			{20	,20	,"dtmfruleid"	,"dtmfruleid"	,2	,2	,1	,6},
			{40	,20	,"playruleid"	,"playruleid"	,2	,2	,1	,6},
			{60	,20	,"asrruleid"	,"asrruleid"	,2	,2	,1	,6},
			{80	,20	,"ttstype"	,"ttstype"	,5	,7	,0	,0},
			{100	,30	,"ttsgrammar"	,"ttsgrammar"	,5	,7	,0	,0},
			{130	,100	,"ttsparam"	,"ttsparam"	,5	,7	,0	,0},
			{230	,20	,"ttsformat"	,"ttsformat"	,2	,2	,4	,4},
			{250	,100	,"ttsfilename"	,"ttsfilename"	,5	,7	,0	,0},
			{350	,500	,"string"		,"string"		,5	,7	,0	,0},
			{850	,20	,"returndtmfs"	,"returndtmfs"	,10	,1	,0	,0},
			{870	,20	,"returnasr"	,"returnasr"	,10	,1	,0	,0},
			{890	,20	,"returntts"	,"returntts"	,10	,1	,0	,0},
			{910	,20	,"next"		,"next"		,5	,0	,0	,0},
		},
	},
};
