//---------------------------------------------------------------------------
#ifndef __IVRPARSER_H__
#define __IVRPARSER_H__
//---------------------------------------------------------------------------
#define MSGTYPE_IVRXML					0	//用于在IVR和解析器之間傳遞的消息

//其他消息類型定義
//#define MSGTYPE_IVRVOP					0x01	//用于在IVR和Voice語音機之間傳遞的消息
//#define MSGTYPE_IVRSWT					0x02	//用于在IVR和交換機之間傳遞的消息
//#define MSGTYPE_IVRMON					0x03	//用于在ivr和監控席之間傳遞的消息
//#define MSGTYPE_IVRIPREC				0x04	//用于在IVR和外部ip錄音系統之間傳遞的消息
//#define MSGTYPE_IVRSIPMON				0x05	//用于在IVR和外部sip server狀態監視之間傳遞的消息
//#define MSGTYPE_IVRCOM					0x06	//用于在IVR和rs232串口之間傳遞的消息
//#define MSGTYPE_IVRWEB					0x07	//用于在IVR和WEB坐席之間傳遞的消息(IVR->WS: MsgId:0普通消息編號，1-注冊結果消息編號，2-狀態消息)
//#define MSGTYPE_IVREMAIL				0x08	//用于在IVR和EMAIL網關之間傳遞的消息
//#define MSGTYPE_HAIVR						0x09	//用于在IVR之間傳遞的心跳檢測消息
//#define MSGTYPE_DBXML						0x10	//用于在DB和解析器之間傳遞的消息
//#define MSGTYPE_IVRREC					0x0f	//用于在IVR和外部錄音系統之間傳遞的消息
//#define MSGTYPE_IVRLOG  				0x20	//用于在ivr和監控之間傳遞的消息
//#define MSGTYPE_AGIVR						0x30	//用于在ivr和坐席之間傳遞的消息
//#define MSGTYPE_IVRTTS					0x40	//用于在IVR和TTS之間傳遞的消息
//#define MSGTYPE_CFG						  0x50	//用于在CFG和平臺服務之間傳遞的消息

//---------------------------------------------------------------------------
//IVR之間傳遞的心跳檢測消息定義
#define MSGTYPE_HAIVR						0x09	//用于在IVR之間傳遞的心跳檢測消息

#define HAMSG_standbystatus     0 //備用機將運行狀態發到主用機

#define HAMSG_onmasterstatus    0 //主用機將運行狀態發到備用機
#define HAMSG_onaglogindata     1 //同步坐席登錄數據及狀態
#define HAMSG_onsysrunstate     2 //系統運行狀態

//---------------------------------------------------------------------------
#define MAX_IVRXMLMSG_NUM       114//128 //最大
#define MAX_XMLIVRMSG_NUM       100//128 //最大

//---------------------------------------------------------------------------
//ivr服務器-->vxml解析器(parser)
//---------------------------------------------------------------------------
#define MSG_onlogin		          0	//節點注冊結果
#define MSG_onsetaccessno	      1 //設置接入號碼結果
#define MSG_oncallin	          2	//電話呼入事件
#define MSG_onrecvsam	          3	//收到后續地址事件
#define MSG_onsmsin		          4	//短信呼入事件
#define MSG_oncallflw	          5	//調用指定流程
#define MSG_onanswer	          6	//來話應答結果
#define MSG_onsendsignal	      7	//發送信令結果
#define MSG_onhangon	          8	//掛機釋放結果
#define MSG_ondtmfrule	        9	//DTMF按鍵規則定義結果
#define MSG_ongetdtmf	          10	//收碼結果
#define MSG_onplayrule	        11	//放音規則定義結果
#define MSG_onasrrule	          12  //ASR規則定義結果
#define MSG_onsetvolume	        13	//改變放音音量結果
#define MSG_onplayfile	        14  //單文件放音結果
#define MSG_onsetplaypos	      15	//改變放音指針位置結果
#define MSG_onplayfiles	        16	//多文件放音結果
#define MSG_onplaycompose	      17	//合成串放音結果
#define MSG_onmultiplay	        18	//多語音合成放音結果
#define MSG_onttsstring	        19	//文本串轉語音結果
#define MSG_onttsfile	          20	//文本文件轉語音結果
#define MSG_onsenddtmf	        21	//發送DTMF結果
#define MSG_onsendfsk	          22	//發送FSK結果
#define MSG_onplaycfc	          23	//會議室放音結果
#define MSG_onplaytone	        24	//播放信號音結果
#define MSG_onrecordfile	      25	//錄音結果
#define MSG_onrecordcfc	        26	//會議錄音結果
#define MSG_onstop		          27	//停止放音錄音收碼結果
#define MSG_oncallout	          28	//電話呼出結果
#define MSG_oncancelcallout	    29	//取消呼出結果
#define MSG_onsendsam	          30	//發送被叫號碼結果
#define MSG_ontransfer	        31	//電話轉接結果
#define MSG_oncallseat	        32	//分配坐席結果
#define MSG_onstopcallseat      33	//取消分配坐席結果
#define MSG_oncreatecfc	        34	//創建會議結果
#define MSG_ondestroycfc	      35	//釋放會議資源結果
#define MSG_onjoincfc	          36	//加入會議結果
#define MSG_onunjoincfc	        37	//退出會議結果
#define MSG_onlink		          38	//通道交換結果
#define MSG_ontalkwith	        39	//與對方通話結果
#define MSG_onlistenfrom	      40	//監聽對方通話結果
#define MSG_oninserttalk	      41	//強插對方通話結果
#define MSG_onthreetalk	        42	//三方通話結果
#define MSG_onstoptalk	        43	//結束通話結果
#define MSG_onsendfax	          44  //發送傳真結果
#define MSG_onrecvfax	          45	//接收傳真結果
#define MSG_oncheckpath	        46	//檢查文件路徑結果
#define MSG_oncreatepath	      47	//創建文件路徑結果
#define MSG_ondeletepath	      48	//刪除文件結果
#define MSG_ongetfilenum	      49	//取文件個數結果
#define MSG_ongetfilename	      50	//取文件名結果
#define MSG_onrenname	          51	//改文件名結果
#define MSG_oncopyfile	        52	//拷貝文件結果
#define MSG_ondeletefile	      53	//刪除文件結果
#define MSG_oncheckfile	        54	//檢查文件是否存在
#define MSG_onclearmixer	      55	//清除放音合成緩沖區
#define MSG_onaddfile	          56	//增加文件到放音合成緩沖區
#define MSG_onadddatetime	      57	//增加日期時間到放音合成緩沖區
#define MSG_onaddmoney	        58	//增加金額到放音合成緩沖區
#define MSG_onaddnumber	        59	//增加數值到放音合成緩沖區
#define MSG_onadddigits	        60	//增加數字串到放音合成緩沖區
#define MSG_onaddchars	        61	//增加字符串到放音合成緩沖區
#define MSG_onplaymixer	        62	//播放合成緩沖區
#define MSG_onaddttsstr	        63	//增加TTS字符串到放音合成緩沖區
#define MSG_onaddttsfile        64	//增加TTS字符串到放音合成緩沖區
#define MSG_onplay				      65  //放音結果
#define MSG_onlinkevent         66	//通道交換對方掛機、斷開事件
#define MSG_onrecvevent         67	//收到消息證實事件
#define MSG_ondialout           68	//撥號器呼出返回事件
#define MSG_onflash             69	//模擬外線拍叉簧結果
#define MSG_onsetvcparam        70	//設置變聲參數結果
#define MSG_onpause             71	//暫停放音結果
#define MSG_onreplay            72	//重新放音結果
#define MSG_onfastplay          73	//快速放音結果
#define MSG_onchecktone         74	//檢測信號音結果
#define MSG_onexecsql           75	//執行SQL語句
#define MSG_onworkerlogin       76	//話務員登錄消息結果
#define MSG_onworkerlogout      77	//話務員退出消息結果
#define MSG_onagblindtrancall   78	//電腦坐席快速轉接電話
#define MSG_onagconsulttrancall 79	//電腦坐席協商轉接電話
#define MSG_onagconftrancall    80	//電腦坐席會議轉接電話
#define MSG_onagstoptrancall    81	//電腦坐席停止轉接電話
#define MSG_onagconfspeak     	82	//電腦坐席指定通道會議發言
#define MSG_onagconfaudit     	83	//電腦坐席指定通道旁聽會議
#define MSG_onagtranivr         84	//坐席將來話轉接到ivr流程
#define MSG_onaghold        		85	//電腦坐席發給流程會話的保持/取消保持消息
#define MSG_onagmute        		86	//電腦坐席發給流程會話的靜音/取消靜音消息
#define MSG_onagflwmsg        	87	//電腦坐席發給流程會話的事件消息
#define MSG_onpickupcall        88	//代接電話結果
#define MSG_ongetacdqueue       89	//取ACD隊列信息結果
#define MSG_ongetdialdtmf       90	//內線分機后續按鍵事件
#define MSG_onrecvfsk	          91	//接收FSK結果
#define MSG_ontransferivr		    92	//轉接到IVR結果
#define MSG_onreleaseivr		    93	//釋放IVR資源結果
#define MSG_onplayswitchvoice		94	//播放交換機內置語音結果
#define MSG_onsendswitchcmd 		95	//發送交換機操作命令結果
#define MSG_ondial 		  			  96	//撥打電話結果
#define MSG_onappendfaxfile			97	//追加發送的傳真文件結果
#define MSG_onrouteideltrks			98	//路由空閑中繼數
#define MSG_onbandagentchn			99	//綁定坐席通道結果
#define MSG_onsendcalltoagent		100	//發送電話分配到電腦坐席結果
#define MSG_oncallconnected     101	//收到交換機呼叫通話連接事件
#define MSG_onnewcallparam      102	//變化了的新的呼叫相關參數
#define MSG_ongetseatlogindata  103	//取坐席登錄的話務員信息結果
#define MSG_onnodestate         104	//節點狀態
#define MSG_onrouteselectreq 	  105	//交換機請求選擇外部路由
#define MSG_ongetidleseat       106	//取一空閑坐席并臨時鎖定結果
#define MSG_onunlockseat        107	//解除臨時鎖定結果
#define MSG_onupdatesrvcore     108	//更新服務評價到后臺IVR服務結果
#define MSG_onupdatecdrfield    109	//更新CDR字段數據結果
#define MSG_onupdatesysparam    110	//更新平臺系統參數結果
#define MSG_onsetforwarding     111	//設置分機呼轉結果
#define MSG_oncallfunction      112	//特殊功能呼叫結果，如：監聽、強插
#define MSG_onplayex						113	//增強型放音結果

//////////////////////////////////////////////////////
//vxml解析器(parser)-->ivr服務器
#define MSG_login		            0	//節點注冊
#define MSG_setaccessno	        1	//設置接入號碼
#define MSG_answer		          2	//來話應答
#define MSG_sendsignal	        3	//發送信令
#define MSG_hangon		          4	//掛機釋放
#define MSG_dtmfrule	          5	//DTMF按鍵規則定義
#define MSG_getdtmf		          6	//收碼結果
#define MSG_playrule	          7	//放音規則定義
#define MSG_asrrule		          8	//ASR規則定義
#define MSG_setvolume	          9	//改變放音音量
#define MSG_playfile	          10	//單文件放音結果
#define MSG_setplaypos	        11	//改變放音指針位置
#define MSG_playfiles	          12	//多文件放音
#define MSG_playcompose	        13	//合成串放音
#define MSG_multiplay	          14	//多語音合成放音
#define MSG_ttsstring	          15	//文本串轉語音
#define MSG_ttsfile		          16	//文本文件轉語音
#define MSG_senddtmf	          17	//發送DTMF
#define MSG_sendfsk		          18	//發送FSK
#define MSG_playcfc		          19	//會議室放音
#define MSG_playtone	          20	//播放信號音
#define MSG_recordfile	        21	//錄音
#define MSG_recordcfc	          22	//會議錄音
#define MSG_stop		            23	//停止放音錄音收碼
#define MSG_callout		          24	//電話呼出
#define MSG_cancelcallout	      25	//取消呼出
#define MSG_sendsam				      26	//發送被叫號碼
#define MSG_transfer	          27	//電話轉接
#define MSG_callseat	          28	//分配坐席
#define MSG_stopcallseat	      29	//取消分配坐席
#define MSG_createcfc	          30	//創建會議
#define MSG_destroycfc	        31	//釋放會議資源
#define MSG_joincfc		          32	//加入會議
#define MSG_unjoincfc	          33	//退出會議
#define MSG_link		            34	//通道交換
#define MSG_talkwith	          35	//與對方通話
#define MSG_listenfrom	        36	//監聽對方通話
#define MSG_inserttalk	        37	//強插對方通話
#define MSG_threetalk	          38	//三方通話
#define MSG_stoptalk	          39	//結束通話
#define MSG_sendfax		          40	//發送傳真
#define MSG_recvfax		          41	//接收傳真
#define MSG_checkpath	          42	//檢查文件路徑
#define MSG_createpath	        43	//創建文件路徑
#define MSG_deletepath	        44  //刪除文件路徑
#define MSG_getfilenum	        45	//取文件個數
#define MSG_getfilename	        46	//取文件名
#define MSG_renname		          47	//改文件名結果
#define MSG_copyfile	          48	//拷貝文件結果
#define MSG_deletefile	        49	//刪除文件結果
#define MSG_checkfile	          50	//檢查文件
#define MSG_clearmixer	        51	//清除放音合成緩沖區
#define MSG_addfile				      52	//增加文件到放音合成緩沖區
#define MSG_adddatetime	        53	//增加日期時間到放音合成緩沖區
#define MSG_addmoney	          54	//增加金額到放音合成緩沖區
#define MSG_addnumber	          55	//增加數值到放音合成緩沖區
#define MSG_adddigits	          56	//增加數字串到放音合成緩沖區
#define MSG_addchars	          57	//增加字符串到放音合成緩沖區
#define MSG_playmixer	          58	//播放合成緩沖區
#define MSG_addttsstr	          59	//增加TTS字符串到放音合成緩沖區
#define MSG_addttsfile          60	//增加TTS文件到放音合成緩沖區
#define MSG_dialout             61	//撥號器呼出
#define MSG_flash               62	//模擬外線拍叉簧
#define MSG_setvcparam          63	//設置變聲參數
#define MSG_pause               64	//暫停放音
#define MSG_replay              65	//重新放音
#define MSG_fastplay            66	//快速放音
#define MSG_checktone           67	//檢測信號音
#define MSG_workerlogin         68	//話務員登錄消息
#define MSG_workerlogout        69	//話務員退出消息
#define MSG_agtrancallresult    70	//向坐席發送轉接呼叫結果
#define MSG_agjoinconfresult 		71	//向坐席發送指定通道參加會議結果
#define MSG_agtranivrresult     72	//坐席將來話轉接到ivr流程結果
#define MSG_sendagentmsg        73	//發送消息到坐席
#define MSG_pickupcall          74	//代接電話
#define MSG_getacdqueue         75	//取ACD隊列信息
#define MSG_setflwstate         76	//設置通道流程業務狀態信息
#define MSG_swapcallresult      77	//將呼出或呼叫坐席結果改發送到指定的會話
#define MSG_recvfsk		          78	//接收FSK
#define MSG_transferivr		      79	//轉接到IVR
#define MSG_releaseivr		      80	//釋放IVR資源
#define MSG_playswitchvoice		  81	//播放交換機內置語音
#define MSG_sendswitchcmd 		  82	//發送交換機操作命令
#define MSG_dial 		  					83	//撥打電話
#define MSG_appendfaxfile				84	//追加發送的傳真文件
#define MSG_bandagentchn				85	//綁定坐席通道
#define MSG_sendcalltoagent			86	//發送電話分配到電腦坐席
#define MSG_play								87	//通用放音指令
#define MSG_workerloginex       88	//話務員登錄擴展指令消息
#define MSG_workerlogoutex      89	//話務員退出擴展指令消息
#define MSG_getseatlogindata    90	//取坐席登錄的話務員信息消息
#define MSG_getidleseat         91	//取一空閑坐席并臨時鎖定，并返回該組登錄座席總數和空閑座席總數
#define MSG_unlockseat          92	//解除臨時鎖定的座席
#define MSG_updateservicescore  93	//更新服務評價到后臺IVR服務
#define MSG_updatecdrfield      94	//更新CDR字段數據
#define MSG_updatesysparam      95	//更新平臺系統參數
#define MSG_setforwarding       96	//設置分機呼轉
#define MSG_callfunction        97	//特殊功能呼叫，如：監聽、強插
#define MSG_playex							98	//增強型放音指令
#define MSG_ttsstringex	        99	//文本串轉語音擴展指令

//-------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
