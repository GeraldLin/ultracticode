//---------------------------------------------------------------------------
#ifndef __CONFIG_H__
#define __CONFIG_H__

#define MSGTYPE_CFG						  0x50	//用于在CFG和平臺服務之間傳遞的消息
#define MAX_CFGSRVMSG_NUM       2//128 //最大
#define MAX_SRVCFGMSG_NUM       2//128 //最大

//CFG服務-->其他SRV服務程序
#define CFGMSG_onlogin          0	//登錄結果
#define CFGMSG_oniniparam       1	//INI參數

//////////////////////////////////////////////////////
//其他SRV服務程序-->CFG服務
#define CFGMSG_login           	0	//登錄
#define CFGMSG_getiniparam      1	//取INI參數

//-------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
