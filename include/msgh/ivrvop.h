//---------------------------------------------------------------------------
#ifndef __IVRVOICE_H__
#define __IVRVOICE_H__
//---------------------------------------------------------------------------
#define MSGTYPE_IVRVOP					0x01	//用于在IVR和Voice語音機之間傳遞的消息
#define MAX_IVRVOPMSG_NUM       24//128 //最大
#define MAX_VOPIVRMSG_NUM       17//128 //最大

#define MSGTYPE_IVRREC					0x0f	//用于在IVR和外部錄音系統之間傳遞的消息
#define MSGTYPE_IVRIPREC				0x04	//用于在IVR和外部ip錄音系統之間傳遞的消息
#define MSGTYPE_IVRSIPMON				0x05	//用于在IVR和外部sip server狀態監視之間傳遞的消息

//---------------------------------------------------------------------------
//ivr服務器-->voice語音機
//---------------------------------------------------------------------------
#define VOPMSG_loginresult          0	//語音節點注冊結果
#define VOPMSG_addvocindex     		  1	//增加內存放音索引
#define VOPMSG_playfile          		2	//文件放音
#define VOPMSG_playindex          	3	//內存索引放音
#define VOPMSG_stopplay          		4	//停止放音
#define VOPMSG_recordfile          	5	//文件錄音
#define VOPMSG_recordmem          	6	//內存錄音
#define VOPMSG_stoprecord          	7	//停止錄音
#define VOPMSG_sendfax          		8	//發送傳真
#define VOPMSG_recvfax          		9	//接收傳真
#define VOPMSG_stopfax          		10//停止收發傳真
#define VOPMSG_senddtmf          		11//發送DTMF
#define VOPMSG_sendfsk          		12//發送FSK
#define VOPMSG_release          		13//釋放通道
#define VOPMSG_appendfax          	14//追加發送的傳真文件
#define VOPMSG_sendacm          		15//發送ACM
#define VOPMSG_sendack          		16//發送ACK
#define VOPMSG_callout          		17//呼出
#define VOPMSG_sendsam          		18//呼出發送后續號碼
#define VOPMSG_stopcallout          19//停止呼出
#define VOPMSG_sendflash            20//模擬拍叉簧
#define VOPMSG_transfer             21//拍叉簧轉接電話
#define VOPMSG_stoptransfer         22//停止拍叉簧轉接電話
#define VOPMSG_sendswitchover 			23//發送主備切換標志

//////////////////////////////////////////////////////
//voice語音機-->ivr服務器
#define VOPMSG_onlogin		          0	//語音節點資源注冊
#define VOPMSG_onlinealarm		      1	//收到線路告警消息
#define VOPMSG_onplayresult		      2	//放音結果消息
#define VOPMSG_onrecordresult		    3	//錄音結果消息
#define VOPMSG_onrecvdtmf		        4	//收到DTMF消息
#define VOPMSG_onsendfaxresult		  5	//發送傳真結果消息
#define VOPMSG_onrecvfaxresult		  6	//接收傳真結果消息
#define VOPMSG_onrelease          	7 //通道釋放消息
#define VOPMSG_oncallin          	  8 //電話呼入消息
#define VOPMSG_onrecvsam          	9 //收到電話呼入后續號碼
#define VOPMSG_onsendacm            10//發送ACM結果
#define VOPMSG_onsendack          	11//發送ACK結果
#define VOPMSG_onrecvacm          	12//呼出收到ACM
#define VOPMSG_onrecvack          	13//呼出收到ACK
#define VOPMSG_ontransfer           14//拍叉簧轉接電話結果
#define VOPMSG_onivrstatus       		15//語音機將一方的IVR服務狀態發給另一方
#define VOPMSG_onfaxstatus       		16//收發傳真狀態信息

//-------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
