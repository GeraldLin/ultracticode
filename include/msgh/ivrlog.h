//---------------------------------------------------------------------------
#ifndef __IVRLOG_H__
#define __IVRLOG_H__

#define MAX_LOGNODE_NUM           8 //最大監控節點數

#define MSGTYPE_IVRLOG  				  0x20	//用于在ivr和監控之間傳遞的消息
#define MAX_IVRLOGMSG_NUM         50//128 //最大
#define MAX_LOGIVRMSG_NUM         50//128 //最大

//-----------------------------------------------------------------------------
//IVR服務器-->LOG監控客戶端
#define LOGMSG_onlogin       				0	  //登錄結果

#define LOGMSG_onivronoff						1	  //IVR監控信息開關狀態
#define LOGMSG_onflwonoff						2	  //flw監控信息開關狀態

#define LOGMSG_onivralarmmsg		  	3	  //取ivr告警跟蹤信息結果
#define LOGMSG_onflwalarmmsg		  	4	  //取flw告警跟蹤信息結果
#define LOGMSG_ondbalarmmsg		  	  5	  //取db告警跟蹤信息結果

#define LOGMSG_ongetqueueinfo 			6	  //取ACD分配隊列信息結果
#define LOGMSG_ongetagentstatus	    7	  //取所有坐席的狀態信息結果
#define LOGMSG_ongetchnstatus 			8	  //取所有通道的狀態信息結果
#define LOGMSG_ongetivrtracemsg		  9	  //取ivr日志跟蹤信息結果
#define LOGMSG_ongetloadedflws		  10  //取已加載的流程信息結果
#define LOGMSG_ongetflwtracemsg		  11	//取flw日志跟蹤信息結果

#define LOGMSG_onreleasechn     	  12  //強行釋放通道結果
#define LOGMSG_onforceclear     	  13  //強拆其他坐席的電話結果
#define LOGMSG_onforcelogout     	  14  //強制將其他話務員退出結果
#define LOGMSG_onforcedisturb       15  //強制其他坐席免打擾結果
#define LOGMSG_onforceready     	  16	//強制其他坐席空閑結果

#define LOGMSG_onmodifyroutedata    17	//修改路由參數結果
#define LOGMSG_onmodifyseatno       18	//修改坐席分機號結果
#define LOGMSG_onmodifyextline      19	//修改模擬外線接入號碼參數結果

#define LOGMSG_onloadflw            20	//加載業務流程結果
#define LOGMSG_onaddaccesscode   		21	//添加接入號碼結果
#define LOGMSG_ondelaccesscode   		22	//刪除接入號碼結果

#define LOGMSG_onsetivrsavemsgid 		23	//當前設置的IVR服務自動保存日志標志
#define LOGMSG_onsetflwsavemsgid 		24	//當前設置的FLW服務自動保存日志標志

#define LOGMSG_onrecviniparam 		  25	//收到的ini參數
#define LOGMSG_onrecvtxtline 		    26	//收到的ini文件行
#define LOGMSG_onmodifyseatdata     27	//修改坐席參數結果

#define LOGMSG_ontrkcountdata       28	//中繼話務實時統計數據
#define LOGMSG_onagcountdata        29	//坐席話務實時統計數據

#define LOGMSG_onmodifypassword 	  30	//修改登錄密碼結果

#define LOGMSG_ongetvopstatus 			31	//取語音機通道狀態結果

#define LOGMSG_onseatstatusdata     32	//坐席狀態實時統計數據
#define LOGMSG_ongroupstatusdata    33	//話務組狀態實時統計數據

#define LOGMSG_onsessionscount      34	//流程會話session實時統計數據

#define LOGMSG_onharunstatus      	35	//本機在雙機熱備系統中的運行狀態

#define LOGMSG_onquerytrunkgroup   	36	//查詢中繼群占用數狀態
#define LOGMSG_onqueryacdsplit      37	//查詢ACD數結果

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//LOG監控客戶端-->IVR服務器
#define LOGMSG_login       				0	  //登錄
#define LOGMSG_logout      				1	  //登出

#define LOGMSG_getqueueinfo 			2	  //取ACD分配隊列信息
#define LOGMSG_getagentstatus	    3	  //取所有坐席的狀態信息
#define LOGMSG_getchnstatus 			4	  //取所有通道的狀態信息
#define LOGMSG_getivrtracemsg		  5	  //取ivr日志跟蹤信息
#define LOGMSG_getloadedflws		  6	  //取已加載的流程信息
#define LOGMSG_getflwtracemsg		  7	  //取flw日志跟蹤信息

#define LOGMSG_releasechn     	  8	  //強行釋放通道
#define LOGMSG_forceclear     	  9	  //強拆其他坐席的電話
#define LOGMSG_forcelogout     	  10  //強制將其他話務員退出
#define LOGMSG_forcedisturb     	11  //強制其他坐席免打擾
#define LOGMSG_forceready     	  12	//強制其他坐席空閑

#define LOGMSG_modifyroutedata    13	//修改路由參數
#define LOGMSG_modifyseatno       14	//修改坐席分機號
#define LOGMSG_modifyextline      15	//修改模擬外線接入號碼參數

#define LOGMSG_loadflw            16	//加載業務流程
#define LOGMSG_addaccesscode   		17	//添加接入號碼
#define LOGMSG_delaccesscode   		18	//刪除接入號碼

#define LOGMSG_setivrsavemsgid 		19	//設置IVR服務自動保存日志標志
#define LOGMSG_setflwsavemsgid 		20	//設置FLW服務自動保存日志標志

#define LOGMSG_modifyiniparam 		21	//修改INI配置文件參數
#define LOGMSG_modifyseatdata     22	//修改坐席參數

#define LOGMSG_gettrkcallcount	  23	//取中繼實時話務統計數據
#define LOGMSG_getagcallcount	    24	//取坐席實時話務統計數據

#define LOGMSG_autoflwonoff 		  25	//自動運行流程啟動及停止

#define LOGMSG_modifypassword 	  26	//修改登錄密碼

#define LOGMSG_modifyctilinkdata  27	//修改CTILINK參數
//-----------------------------------------------------------------------------
#endif

