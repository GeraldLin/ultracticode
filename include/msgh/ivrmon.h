//---------------------------------------------------------------------------
#ifndef __IVRMONITOR_H__
#define __IVRMONITOR_H__

#define MAX_MONNODE_NUM           64 //最大監控席節點數

#define MSGTYPE_IVRMON					  0x03	//用于在ivr和監控席之間傳遞的消息
#define MAX_IVRMONMSG_NUM         16//128 //最大
#define MAX_MONIVRMSG_NUM         14//128 //最大

//-----------------------------------------------------------------------------
//IVR服務器-->監控坐席
#define MONMSG_onmonitorlogin	    0	//監控坐席登錄結果

#define MONMSG_onlistencall     	1	//監聽電話結果
#define MONMSG_onbreakincall     	2	//強插電話結果
#define MONMSG_ontakeovercall    	3	//強接電話結果（就是把其他話務員正在通話的線路強接過來）
#define MONMSG_onsendmessage     	4	//發送文字消息結果

#define MONMSG_onforceclear      	5	//強拆電話結果
#define MONMSG_onforcelogout     	6	//強制將其他話務員退出結果
#define MONMSG_onforcebusy        7	//強制坐席示忙結果
#define MONMSG_onforceready     	8	//強制坐席空閑結果

#define MONMSG_ongetagentstatus	  9	//收到的坐席狀態信息
#define MONMSG_ongetchnstatus 		10	//收到的通道狀態信息
#define MONMSG_onswttrkgrpstatus  11	//收到的交換機中繼組占用狀態
#define MONMSG_onswtacdgrpstatus  12	//收到的交換機ACD組登錄占用狀態
#define MONMSG_ongetqueueinfo 		13	//收到的平臺ACD分配簡單隊列信息

#define MONMSG_ongetagenttotal    14	//坐席狀態實時統計數據
#define MONMSG_ongetgrouptotal    15	//話務組狀態實時統計數據

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//>監控坐席-->IVR服務器
#define MONMSG_minitorlogin       0	//監控坐席登錄

#define MONMSG_listencall     	  1	//監聽電話
#define MONMSG_breakincall     	  2	//強插電話
#define MONMSG_takeovercall    	  3	//強接電話（就是把其他話務員正在通話的線路強接過來）
#define MONMSG_sendmessage     	  4	//發送文字消息

#define MONMSG_forceclear      	  5	//強拆電話
#define MONMSG_forcelogout     	  6	//強制將其他話務員退出
#define MONMSG_forcebusy        	7	//強制坐席示忙
#define MONMSG_forceready     	  8	//強制坐席空閑

#define MONMSG_getagentstatus	    9 //取坐席的狀態信息
#define MONMSG_getchnstatus 			10 //取通道的狀態信息
#define MONMSG_getacdqueueinfo 		11 //取ACD分配隊列信息

#define MONMSG_getagenttotal 			12 //取坐席實時統計話務數據
#define MONMSG_getgrouptotal 			13 //取坐席組實時統計話務數據

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
#endif

