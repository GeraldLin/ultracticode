//---------------------------------------------------------------------------
#ifndef __DBPARSER_H__
#define __DBPARSER_H__

#define MSGTYPE_DBXML						0x10	//用于在DB和解析器之間傳遞的消息
#define MAX_DBXMLMSG_NUM        35//128 //最大 //edit 2008-09-30 change
#define MAX_XMLDBMSG_NUM        43//128 //最大

//db服務器-->vxml解析器(parser)
#define MSG_ongwquery         	0	//查詢外部數據網關結果
#define MSG_ondbquery           1	//查詢數據表操作結果
#define MSG_ondbfield           2	//取數據字段值操作結果
#define MSG_ondbmove            3	//記錄指針移動結果
#define MSG_ondbcmd             4	//插入、修改、刪除記錄結果
#define MSG_onwritebill         5	//寫話單結果
#define MSG_ongettolltime       6	//返回最大通話時長
#define MSG_ongwmakecall        7	//外部數據網關向流程發送啟動呼叫事件
#define MSG_ondbrecord          8	//讀數據表當前記錄結果
#define MSG_ondbsp              9	//執行存儲過程結果
#define MSG_ondbspoutparam      10//取存儲過程輸出參數值結果
#define MSG_ongwmsgevent        11//外部數據網關向流程發送消息事件
#define MSG_ondbfieldtofile    	12//取查詢的字段值并存放到指定的文件結果
#define MSG_ondbfiletofield    	13//將文件內容存入數據表結果
#define MSG_onquerywebservice   14//執行查詢外部webservice接口數據結果
#define MSG_ongetwebservicexml  15//從外部webservice接口查詢的XML串中取數據結果
#define MSG_ongetwebservicestr  16//從外部webservice接口查詢的字符串取數據結果
#define MSG_ongetwebserviceindex 17//從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號結果
#define MSG_onhttprequest   		18//流程發送HTTP協議請求結果
#define MSG_onwebhttprequest 		19//外部web界面發來的http請求消息
#define MSG_onshellexecute   		20 //執行外部程序結果
#define MSG_ongettxtlinestr   	21 //取文本行某段字符串結果
#define MSG_onsocketrequest     22 //通過socket通信發送查詢請求結果
#define MSG_onconnectdbfail  		23//連接數據庫失敗事件
#define MSG_onconnectdbsucc  		24//連接數據庫失敗事件
#define MSG_ondbrecorddump   		25//數據表記錄轉存結果
#define MSG_onimcallin   				26//IM呼入
#define MSG_onemailcallin   		27//Email呼入
#define MSG_onfaxcallin   			28//Fax呼入
#define MSG_onimcancel   				29//IM取消
#define MSG_onemcancel   				30//Email取消
#define MSG_onfaxcancel   			31//Fax取消
#define MSG_onhttprequestex   	32//流程發送HTTP協議請求擴展指令結果
#define MSG_ondbselect          33//查詢數據表記錄擴展指令結果
#define MSG_ongetwebservicejson 34//從外部webservice接口查詢的JSON串中取數據結果

//////////////////////////////////////////////////////
//vxml解析器(parser)-->db服務器
#define MSG_gwquery           	0	//查詢外部數據網關
#define MSG_dbquery             1	//查詢數據操作
#define MSG_dbfieldno           2	//根據字段序號取字段值操作
#define MSG_dbfieldname         3	//根據字段名取字段值操作
#define MSG_dbfirst             4	//記錄指針移到首記錄
#define MSG_dbnext              5	//記錄指針移到下一記錄
#define MSG_dbprior             6	//記錄指針移到前一記錄
#define MSG_dblast              7	//記錄指針移到末記錄
#define MSG_dbgoto              8	//記錄指針移到指定記錄
#define MSG_dbinsert            9	//插入記錄操作
#define MSG_dbupdate            10	//修改記錄操作
#define MSG_dbdelete            11	//刪除記錄操作
#define MSG_writebill           12	//寫計費話單
#define MSG_dbsp                13	//執行存儲過程操作
#define MSG_dbclose             14	//關閉查詢的數據表
#define MSG_gettolltime         15	//根據余額取最大通話時長
#define MSG_dbrecord            16	//讀數據表當前記錄
#define MSG_dbspoutparam        17	//取存儲過程輸出參數值
#define MSG_execsql             18	//執行SQL語句
#define MSG_gwmakecallresult    19	//外部數據網關向流程發送啟動呼叫事件結果
#define MSG_gwmsgeventresult    20	//外部數據網關向流程發送消息事件執行結果
#define MSG_dbfieldtofile    		21	//取查詢的字段值并存放到指定的文件
#define MSG_dbfiletofield    		22	//將文件內容存入數據表
#define MSG_dbinsertex          23	//插入記錄操作（針對長度>500的sql語句）
#define MSG_dbupdateex          24	//修改記錄操作（針對長度>500的sql語句）
#define MSG_dbqueryex           25	//查詢數據操作（針對長度>500的sql語句）
#define MSG_querywebservice     26	//執行查詢外部webservice接口數據
#define MSG_getwebservicexml    27	//從外部webservice接口查詢的XML串中取數據
#define MSG_getwebservicestr    28	//從外部webservice接口查詢的字符串取數據
#define MSG_closewebservice     29	//關閉外部webservice接口
#define MSG_getwebserviceindex  30	//從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號
#define MSG_httprequest   			31  //流程發送HTTP協議請求
#define MSG_shellexecute   			32  //執行外部程序
#define MSG_gettxtlinestr   		33  //取文本行某段字符串
#define MSG_socketrequest   		34  //通過socket通信發送查詢請求
#define MSG_dbrecorddump   			35  //數據表記錄轉存
#define MSG_imcallinresult			36	//IM呼入結果
#define MSG_emcallinresult			37	//Email呼入結果
#define MSG_faxcallinresult			38	//Fax呼入結果
#define MSG_querycustomer				39	//通過webservice接口查詢客戶資料
#define MSG_httprequestex  			40  //流程發送HTTP協議請求擴展指令
#define MSG_dbselect            41	//查詢數據表記錄擴展指令
#define MSG_getwebservicejson   42	//從外部webservice接口查詢的JSON串中取數據

//-------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
