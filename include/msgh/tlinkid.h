#ifndef __TLINKID_H__
#define __TLINKID_H__

//TLINK通訊節點類型定義
#define NODE_IVR				        0  //IVR服務器
#define NODE_SWT                1  //交換接入
#define NODE_XML                2  //流程解析
#define NODE_VOP                3  //外置語音
#define NODE_FAX                4  //傳真
#define NODE_CFG                5  //配置服務
#define NODE_CSC				        6  //坐席
#define NODE_WS			            7  //WebSocket,WebHTTP服務
#define NODE_TTS 				        8  //TTS代理客戶端
#define NODE_SMS 				        9  //SMS短信網關代理客戶端
#define NODE_LOG 				        10  //平臺配置監控客戶端
#define NODE_CTRL				        11  //LINUX下平臺服務運行監控程序
#define NODE_MON 				        12  //話務監控客戶端
#define NODE_SIPMON  		        13  //Sipserver狀態監視
#define NODE_IPREC  		        14  //ip錄音平臺客戶端
#define NODE_REC  		          15  //外部錄音平臺客戶端
#define NODE_DB 				        16  //DB服務器代理
#define NODE_COM				        17  //RS232串口接入
#define NODE_EMAILGW		        18  //EMAIL網關

//組合對象編碼 (節點類型 + 節點編號)
static inline unsigned short CombineObjectID( unsigned char ObjectType, unsigned char ObjectNo )
{
    return (unsigned short)(((unsigned int)ObjectNo) | ((unsigned int)ObjectType<<8));
}

#ifdef __cplusplus

//分解對象標識及編號
static inline void GetClientID(unsigned short Msg_wParamHi, unsigned char &ObjectType, unsigned char &ObjectNo)
{
    ObjectType = (unsigned char)( Msg_wParamHi >> 8 ) ;
    ObjectNo = (unsigned char)Msg_wParamHi ;
}
#else
//分解對象標識及編號
static inline void GetClientID(unsigned short Msg_wParamHi, unsigned char *ObjectType, unsigned char *ObjectNo)
{
    *ObjectType = (unsigned char)( Msg_wParamHi >> 8 ) ;
    *ObjectNo = (unsigned char)Msg_wParamHi ;
}

#endif



#endif
