//---------------------------------------------------------------------------
#ifndef __IVRSWICTH_H__
#define __IVRSWICTH_H__
//---------------------------------------------------------------------------
#define MSGTYPE_IVRSWT					    0x02	//用于在IVR和交換機之間傳遞的消息
#define MAX_IVRSWTMSG_NUM           40//128 //最大
#define MAX_SWTIVRMSG_NUM           44//128 //最大

//---------------------------------------------------------------------------
//ivr服務器-->swt交換機(switch)
//---------------------------------------------------------------------------
#define SWTMSG_connectlink          0	//連接CTILinkService

#define SWTMSG_setportstatus        1	//設置端口狀態
#define SWTMSG_setagentstatus       2	//設置坐席狀態
#define SWTMSG_setroomstatus        3	//設置房間狀態
#define SWTMSG_setlampstatus 				4	//設置留言指示燈
#define SWTMSG_setwakestatus 				5	//設置叫醒

#define SWTMSG_setcalllevel         6	//設置呼出權限
#define SWTMSG_settranphone         7	//設置轉移號碼

#define SWTMSG_answercall           8	//應答來話
#define SWTMSG_refusecall           9	//拒絕來話
#define SWTMSG_makecall             10	//發起呼叫
#define SWTMSG_bandcall             11	//將來中繼來話綁定到分機
#define SWTMSG_bandvoice            12	//將來中繼來話綁定到提示語音
#define SWTMSG_holdcall             13	//保持電話
#define SWTMSG_unholdcall           14	//取消保持電話
#define SWTMSG_transfercall         15	//轉接電話
#define SWTMSG_stoptransfer         16	//停止轉接電話
#define SWTMSG_swapcall             17	//穿梭通話
#define SWTMSG_pickupcall           18	//代接電話
#define SWTMSG_breakincall          19	//強插通話
#define SWTMSG_listencall           20	//監聽電話
#define SWTMSG_removecall           21	//強拆電話
#define SWTMSG_threeconf            22	//三方通話
#define SWTMSG_releasecall          23	//釋放呼叫
#define SWTMSG_hangup               24	//分機掛機
#define SWTMSG_putacdqueuecall      25	//將電話保持到話務臺隊列
#define SWTMSG_getacdqueuecall      26	//從話務臺隊列提取電話
#define SWTMSG_sendswitchcmd        27	//發送交換機操作指令
#define SWTMSG_senddtmf             28	//發送DTMF指令
#define SWTMSG_takeovercall         29	//接管電話
#define SWTMSG_redirectcall         30	//電話重新定向振鈴
#define SWTMSG_addtoconference      31	//加入會議
#define SWTMSG_removefromconference 32	//退出會議
#define SWTMSG_sendivrstatus 				33	//發送IVR節點的狀態到交換機
#define SWTMSG_sendswitchover 			34	//發送主備切換標志
//ver:3.2版本增加
#define SWTMSG_recordfile 					35	//文件錄音
#define SWTMSG_stoprecord 			    36	//停止錄音
#define SWTMSG_playfile 					  37	//文件放音
#define SWTMSG_playindex 					  38	//索引放音
#define SWTMSG_stopplay 					  39	//停止放音

//---------------------------------------------------------------------------
//swt交換機(switch)-->ivr服務器
#define SWTMSG_onconnectlink        0	//連接CTILinkService結果
#define SWTMSG_onlinkstatus         1	//返回CTILink接口與交換機的連接狀態
#define SWTMSG_ongetdevnnum         2	//返回交換機設備配置數量
#define SWTMSG_ongetdevparam        3	//返回交換機設備的配置參數
#define SWTMSG_onportstatus         4	//端口狀態事件

#define SWTMSG_onagentstatus        5	//坐席狀態事件
#define SWTMSG_onroomstatus    			6	//房間狀態事件
#define SWTMSG_onlampevent          7	//留言燈狀態事件
#define SWTMSG_onwakeevent          8	//叫醒狀態事件
#define SWTMSG_onconsoleevent       9	//話務臺隊列事件
#define SWTMSG_onbillevent          10 //收到呼叫話單事件

#define SWTMSG_oncallevent          11 //呼叫事件
#define SWTMSG_oncallsam        		12 //收到后續地址事件
#define SWTMSG_oncallacm        		13 //號碼收全事件
#define SWTMSG_oncallring           14 //收到振鈴事件
#define SWTMSG_oncallfail           15 //收到呼叫失敗事件
#define SWTMSG_oncallack        		16 //收到應答事件
#define SWTMSG_oncallrelease        17 //收到釋放事件

#define SWTMSG_onanswercall         18	//應答來話結果
#define SWTMSG_onrefusecall         19	//拒絕來話結果
#define SWTMSG_onmakecall           20	//發起呼叫結果
#define SWTMSG_onbandcall           21	//將來中繼來話綁定到分機結果
#define SWTMSG_onbandvoice          22	//將來中繼來話綁定到提示語音結果
#define SWTMSG_onholdcall           23	//保持電話結果
#define SWTMSG_onunholdcall      		24	//取消保持電話結果
#define SWTMSG_ontransfercall       25	//轉接電話結果
#define SWTMSG_onstoptransfer       26	//停止轉接電話結果
#define SWTMSG_onswapcall           27	//穿梭通話結果
#define SWTMSG_onpickupcall         28	//代接電話結果
#define SWTMSG_onbreakincall        29	//強插通話結果
#define SWTMSG_onlistencall         30	//監聽電話結果
#define SWTMSG_onremovecall         31	//強拆電話結果
#define SWTMSG_onthreeconf          32	//三方通話結果
#define SWTMSG_onconsolehold        33	//將電話保持到話務臺隊列結果
#define SWTMSG_onconsolepickup      34	//從話務臺隊列提取電話結果
#define SWTMSG_onsendswitchcmd      35	//發送交換機操作指令結果
#define SWTMSG_oncallconnected      36  //收到呼叫通話連接事件
//ver:3.2版本增加
#define SWTMSG_onrecordresult			  37	//錄音結果
#define SWTMSG_onplayresult			    38	//放音結果
#define SWTMSG_onrecvdtmf 			    39	//收碼結果
//ver:3.3版本增加
#define SWTMSG_onqueryacdsplit      40  //查詢ACD話務組統計狀態結果
#define SWTMSG_onagentloggedon   		41  //agent登錄交換機事件
#define SWTMSG_onagentloggedoff     42  //agent登出交換機事件

#define SWTMSG_onsettranphone       43	//設置轉移號碼結果

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif

