//xml通訊消息結構規則定義
#ifndef __vmsgruleH__
#define __vmsgruleH__

#define MAX_MSG_ATTRIBUTE_NUM 32 //消息中最多帶的參數個數
////////////////////////////////////////////////////////////////////////////////////////


//返回消息屬性規則結構定義
typedef struct
{
    const char *AttrNameEng;//[MAX_ATTRIBUTE_LEN]; //屬性名稱
    unsigned char VarType; //屬性參數類型
    unsigned char MacroType; //屬性結果參數值范圍限制類型：0-不限制 1-整數范圍 2-單字符范圍 3-字符串中允許出現的字符 4-整數枚舉字符串 5-枚舉字符串
    unsigned short MacroNo; //屬性結果參數值范圍限制: 0xFFFF表示沒有限制 >=0表示限制的規則編號
    unsigned char ReturnType; //返回值類型：0-未定義 1-會話標志參數 2-消息結果參數 3-返回值參數 4-返回的錯誤消息 5-返回的系統變量值
    unsigned short ReturnVar; //返回值存放的變量地址：
                  //1-會話標志參數（1：sessionid 2：cmdaddr 3：chantype 4：channo）
                  //2-消息結果參數（1：MsgEvent 2：PlayEvent 3：TTSEvent 4：ASREvent 5：RecordEvent 6：CalloutEvent 7：ACDEvent 8：DBEvent 9: FAXEvent 10: ToneDetectedEvent 11: LinkEvent 12: FlashEvent 13: CfcEvent）
                  //3-返回值參數（對應指令規則中的屬性編號）
                  //4-返回的錯誤消息（ErrorBuf）
                  //5-返回的系統變量地址編號

}VXML_RECV_MSG_ATTR_RULE_STRUCT;

//返回消息規則結構定義
typedef struct
{
    const char *MsgNameEng;//[MAX_CMD_NAME_LEN]; //消息英文名稱
    unsigned short AttrNum; //屬性參數值個數
    VXML_RECV_MSG_ATTR_RULE_STRUCT Attribute[MAX_MSG_ATTRIBUTE_NUM]; //指令中屬性定義

}VXML_RECV_MSG_CMD_RULE_STRUCT;

//發送命令屬性規則結構定義
typedef struct
{
  const char *AttrNameEng;//[MAX_ATTRIBUTE_LEN]; //屬性名稱
  unsigned char VarType; //屬性參數類型 0-常量 1-BOOL型 2-整數型 3-浮點數型 4-單字符型 5-字符串型 6-長整型
  unsigned char MacroType; //屬性結果參數值范圍限制類型：0-不限制 1-整數范圍 2-單字符范圍 3-字符串中允許出現的字符 4-整數枚舉字符串 5-枚舉字符串
  unsigned short MacroNo; //屬性結果參數值范圍限制: 0xFFFF表示沒有限制 >=0表示限制的規則編號
  const char *Default;//[MAX_ATTRIBUTE_LEN]; //缺省值

}VXML_SEND_MSG_ATTR_RULE_STRUCT;

//發送消息規則結構定義
typedef struct
{
    const char *MsgNameEng;//[MAX_CMD_NAME_LEN]; //消息英文名稱
    unsigned short AttrNum; //屬性參數值個數
    VXML_SEND_MSG_ATTR_RULE_STRUCT Attribute[MAX_MSG_ATTRIBUTE_NUM]; //指令中屬性定義

}VXML_SEND_MSG_CMD_RULE_STRUCT;

#endif
