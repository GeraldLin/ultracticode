//---------------------------------------------------------------------------
#ifndef __IVRAGENT_H__
#define __IVRAGENT_H__

#define MSGTYPE_AGIVR						  0x30	//用于在ivr和坐席之間傳遞的消息
#define MAX_IVRAGMSG_NUM          52//128 //最大
#define MAX_AGIVRMSG_NUM          53//128 //最大

#define MSGTYPE_IVRWEB						0x07	//用于在IVR和WEB坐席之間傳遞的消息

//-----------------------------------------------------------------------------
//IVR服務器-->AGENT坐席
#define AGMSG_onseatlogin	        0	  //坐席登錄結果
#define AGMSG_onworkerlogin	      1	  //話務員登錄結果
#define AGMSG_onworkerlogout	    2	  //話務員退出結果
#define AGMSG_ondisturb	          3	  //免打擾設置結果
#define AGMSG_onleval		          4	  //離席/在席設置結果
#define AGMSG_onsetforward     		5	  //設置呼叫轉移結果
#define AGMSG_oncancelforward 	  6	  //取消呼叫轉移結果
#define AGMSG_onacdcallin	        7	  //呼入分配坐席消息
#define AGMSG_onstopacdcallin	    8	  //停止呼入分配坐席消息
#define AGMSG_onanswercall        9 	//應答來話結果
#define AGMSG_onmakecall          10  //發起呼叫結果
#define AGMSG_onhangon         		11  //坐席掛機消息

#define AGMSG_ontrancall     			12	//轉接電話結果
#define AGMSG_onstoptrancall      13	//停止轉接結果
#define AGMSG_onjoinconf      		14	//指定通道參加會議結果

#define AGMSG_ontranivr    	      15	//轉接ivr流程結果

#define AGMSG_onsenddtmf     			16	//發送DTMF按鍵結果
#define AGMSG_onplay        			17	//播放語音文件結果
#define AGMSG_onstopplay     			18	//停止放音結果

#define AGMSG_onpickup        	  19	//代接電話結果
#define AGMSG_onpickupqueuecall   20	//搶接ACD隊列里的呼叫結果
#define AGMSG_ontakeover       	  21	//接管其他坐席的電話結果
#define AGMSG_onlisten        	  22	//監聽結果
#define AGMSG_oninsert        	  23	//強插通話結果

#define AGMSG_onhold          	  24  //保持/取消保持結果
#define AGMSG_onmute          	  25	//靜音/取消靜音結果
#define AGMSG_onreleasecall       26	//收到通道釋放

#define AGMSG_onforceclear     	  27	//強拆其他坐席的電話
#define AGMSG_onforcelogout     	28	//強制將其他話務員退出
#define AGMSG_onforcedisturb     	29	//強制其他坐席免打擾
#define AGMSG_onforceready     	  30	//強制其他坐席空閑

#define AGMSG_ongetqueueinfo 			31	//ACD分配簡單隊列信息
#define AGMSG_ongetagentstatus	  32	//坐席的狀態信息
#define AGMSG_ongetchnstatus 			33	//通道的狀態信息

#define AGMSG_onrecvmsgfromflw 		34	//接收流程發來的消息
#define AGMSG_onsendfax     			35	//發送傳真
#define AGMSG_onrecvfax     			36	//接收傳真,通話后接收傳真
#define AGMSG_onsendmessage  			37	//發送文字信息
#define AGMSG_onsendsms     			38	//發送短信

#define AGMSG_ongetqueueinfoEx		39	//ACD分配詳細隊列信息
#define AGMSG_ongetconfmember 		40	//會議成員狀態結果

#define AGMSG_oncreateconf 				41	//創建會議結果
#define AGMSG_ondestroyconf   		42	//釋放會議結果

#define AGMSG_onsetacwtimer   		43	//設置事后處理時長(秒)結果
#define AGMSG_ongetdbparam   		  44	//取數據庫登錄信息結果
#define AGMSG_ongetauthkey   		  45	//獲取的授權信息結果

#define AGMSG_onswaphold   		    46	//穿梭保持通話結果

#define AGMSG_onredirectcall   	  47	//電話重新定向振鈴結果

#define AGMSG_onsetgoouttel 			48	//設置外出值班電話結果

#define AGMSG_onswitchover 			  49	//收到的主備切換標志

#define AGMSG_onswttrkgrpstatus   50	//收到的交換機的中繼組占用狀態
#define AGMSG_onswtacdgrpstatus   51	//收到的交換機的ACD組登錄占用狀態

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//AGENT坐席-->IVR服務器
#define AGMSG_seatlogin           0	  //電腦坐席登錄
#define AGMSG_workerlogin         1	  //話務員登錄
#define AGMSG_workerlogout        2	  //話務員退出
#define AGMSG_disturb             3	  //免打擾設置
#define AGMSG_leval               4	  //離席/在席設置
#define AGMSG_setforward     			5	  //設置呼叫轉移
#define AGMSG_cancelforward 			6	  //取消呼叫轉移
#define AGMSG_answercall          7	  //應答來話
#define AGMSG_makecall          	8	  //發起呼叫
#define AGMSG_hangon              9	  //坐席掛機

#define AGMSG_blindtrancall     	10	//快速轉接電話
#define AGMSG_consulttrancall   	11	//協商轉接電話
#define AGMSG_conftrancall   			12	//會議轉接電話
#define AGMSG_stoptrancall      	13	//停止轉接
#define AGMSG_confspeak      			14	//指定通道會議發言
#define AGMSG_confaudit      			15	//指定通道旁聽會議

#define AGMSG_tranivr       	    16	//轉接ivr流程

#define AGMSG_senddtmf     				17	//發送DTMF按鍵
#define AGMSG_play        				18	//播放語音文件
#define AGMSG_stopplay     				19	//停止放音

#define AGMSG_pickup        	    20	//代接電話
#define AGMSG_pickupqueuecall    	21	//搶接ACD隊列里的呼叫
#define AGMSG_takeover       	    22	//接管其他坐席的電話
#define AGMSG_listen        	    23	//監聽
#define AGMSG_insert        	    24	//強插通話

#define AGMSG_hold          	    25  //保持/取消保持
#define AGMSG_mute          	    26	//靜音/取消靜音
#define AGMSG_releasecall         27	//釋放一方通話,

#define AGMSG_forceclear     	    28	//強拆其他坐席的電話
#define AGMSG_forcelogout     	  29	//強制將其他話務員退出
#define AGMSG_forcedisturb     	  30	//強制其他坐席免打擾
#define AGMSG_forceready     	    31	//強制其他坐席空閑

#define AGMSG_getqueueinfo 			  32	//取ACD分配隊列信息
#define AGMSG_getagentstatus	    33	//取所有坐席的狀態信息
#define AGMSG_getchnstatus 			  34	//取所有通道的狀態信息

#define AGMSG_sendmsgtoflw   			35	//發送消息到流程
#define AGMSG_sendfax     				36	//發送傳真
#define AGMSG_recvfax     				37	//接收傳真,通話后接收傳真
#define AGMSG_sendmessage  				38	//發送文字信息
#define AGMSG_sendsms     				39	//發送短信

#define AGMSG_createconf 					40	//創建會議
#define AGMSG_destroyconf   			41	//釋放會議

#define AGMSG_setacwtimer   			42	//設置事后處理時長(秒)
#define AGMSG_setacwend   				43	//事后處理完成

#define AGMSG_swaphold   				  44	//穿梭保持通話

#define AGMSG_setgroupname			  45	//設置技能組名稱

#define AGMSG_startrecord			  	46	//開始錄音
#define AGMSG_stoprecord			  	47	//停止錄音
#define AGMSG_setcalldirection  	48	//設置呼叫方向(0-雙向服務 1-呼入 2-呼出)

#define AGMSG_pickupex      	    49	//代接電話擴展指令
#define AGMSG_redirectcall   	    50	//電話重新定向振鈴

#define AGMSG_setgoouttel 			  51	//設置外出值班電話

#define AGMSG_queryivrstatus		  52	//查詢IVR運行狀態

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
#endif

