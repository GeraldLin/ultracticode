//---------------------------------------------------------------------------
#ifndef __IVRTTS_H__
#define __IVRTTS_H__
//---------------------------------------------------------------------------
#define MSGTYPE_IVRTTS					0x40	//用于在IVR和TTS之間傳遞的消息
#define MAX_IVRTTSMSG_NUM       5//128 //最大
#define MAX_TTSIVRMSG_NUM       3//128 //最大

//---------------------------------------------------------------------------
//ivr服務器-->TTS服務器
//---------------------------------------------------------------------------
#define TTSMSG_ttsstring		    0	//字符串合成
#define TTSMSG_ttsfile		      1	//文本文件合成
#define TTSMSG_ttsnextstream    2	//請求下一語音流
#define TTSMSG_ttsstop		      3	//停止合成

#define TTSMSG_ttsauthdata      4	//TTS授權數據

#define MAX_TTS_STRING_LEN      2048 //最大文本長度
#define MAX_TTS_FILENAME_LEN    128 //TTS文本名長度

//字符串合成結構 (MsgId = 0)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  CmdAddr;   //流程指令地址
	unsigned short ChnType;   //通道類型
	unsigned short ChnNo;     //通道號
	
	unsigned char DtmfRuleId; //收碼DTMF規則
	unsigned char PlayRuleId; //放音規則
	unsigned char AsrRuleId;  //備用
	
	unsigned char ttsFormat;  //語音編碼格式: 0-表示由TTS配置文件定 >0 參考 macrodef.h 文件中語音格式定義 
	unsigned char ttsStream;  //返回語音流方式: 1-文件 2-數據流
	unsigned char resv1;      //備用
	unsigned char resv2;      //備用
	unsigned char resv3;      //備用

	char ttsType[32];         //TTS引擎類型
	char ttsGrammar[64];      //TTS語法規則文件
	char ttsParam[128];       //TTS附加參數
	char ttsFileName[MAX_TTS_FILENAME_LEN]; //合成后保存的語音文件名

	char ttsString[MAX_TTS_STRING_LEN];//需要合成的字符串
	
}VXML_TTSSTRING_STRUCT;

//文本文件合成結構 (MsgId = 1)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  CmdAddr;   //流程指令地址
	unsigned short ChnType;   //通道類型
	unsigned short ChnNo;     //通道號
	
	unsigned char DtmfRuleId; //收碼DTMF規則
	unsigned char PlayRuleId; //放音規則
	unsigned char AsrRuleId;  //備用
	
	unsigned char ttsFormat;  //語音編碼格式: 0-表示由TTS配置文件定 >0 參考 macrodef.h 文件中語音格式定義 
	unsigned char ttsStream;  //返回語音流方式: 1-文件 2-數據流
	unsigned char resv1;      //備用
	unsigned char resv2;      //備用
	unsigned char resv3;      //備用

	char ttsType[32];         //TTS引擎類型
	char ttsGrammar[64];      //TTS語法規則文件
	char ttsParam[128];       //TTS附加參數
	char ttsFileName[MAX_TTS_FILENAME_LEN]; //合成后保存的語音文件名

	char FileName[MAX_TTS_FILENAME_LEN];//需要合成的文件名
	
}VXML_TTSFILE_STRUCT;

//取下一合成的語音流結構（當有多個語音流時） (MsgId = 2)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  CmdAddr;   //流程指令地址
	unsigned short ChnType;  //通道類型
	unsigned short ChnNo;    //通道號
	unsigned short VocIndex;  //語音流索引號,0表示從頭開始取
	
}VXML_TTSNEXTSTREAM_STRUCT;

//停止合成結構 (MsgId = 3)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  CmdAddr;   //流程指令地址
	unsigned short ChnType;  //通道類型
	unsigned short ChnNo;    //通道號
	
}VXML_TTSSTOP_STRUCT;

//TTS授權數據結構 (MsgId = 4)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  TTSLineNum;//授權的TTS路數
	char ttsAuthData[128];    //TTS授權的附加參數
	
}VXML_TTSAUTHDATA_STRUCT;

//---------------------------------------------------------------------------
//TTS服務器-->ivr服務器
//---------------------------------------------------------------------------
#define TTSMSG_onttsstring		  0	//字符串合成結果
#define TTSMSG_onttsfile		    1	//文本文件合成結果

#define TTSMSG_ongetauthdata    2	//取TTS授權數據

#define MAX_TTS_STREAM_LEN       0x3F00 //最大語音流長度

//TTS返回合成的語音流結構 (MsgId = 0)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  CmdAddr;   //流程指令地址
	unsigned short ChnType;   //通道類型
	unsigned short ChnNo;     //通道號
	
	unsigned char DtmfRuleId; //收碼DTMF規則
	unsigned char PlayRuleId; //放音規則
	unsigned char AsrRuleId;  //備用

	unsigned char Result;     //合成結果 0-合成成功,返回數據流(可能還有后續流) 1-合成成功完成,返回合成的數據 2-合成失敗
	unsigned char VocFormat;  //語音流格式
	unsigned char VocIndex;   //語音流索引號
	unsigned char resv2;      //備用
	unsigned char resv3;      //備用

	unsigned long StreamLen;  //實際的語音流長度
	unsigned char VocStream[MAX_TTS_STREAM_LEN];//返回合成的語音數據流
	
}VXML_ONTTSSTREAM_STRUCT;

//TTS返回合成的語音文件結構 (MsgId = 1)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  CmdAddr;   //流程指令地址
	unsigned short ChnType;   //通道類型
	unsigned short ChnNo;     //通道號
	
	unsigned char DtmfRuleId; //收碼DTMF規則
	unsigned char PlayRuleId; //放音規則
	unsigned char AsrRuleId;  //備用

	unsigned char Result;     //合成結果 0-合成成功,返回數據(還有后續流) 1-合成成功完成,返回合成的數據 2-合成失敗
	unsigned char VocFormat;  //語音文件格式
	unsigned char resv2;      //備用
	unsigned char resv3;      //備用
	unsigned char resv4;      //備用
	char VocFile[MAX_TTS_FILENAME_LEN];//返回合成的語音文件名
	
}VXML_ONTTSSFILE_STRUCT;

//取TTS授權數據結構 (MsgId = 2)
typedef struct
{
	unsigned long  SessionId; //會話序列號
	unsigned long  TTSLicence;//第三方TTS授權的Licence并發路數
	char TTSAuthData[128];
	
}VXML_ONGETTTSSAUTHDATA_STRUCT;

//---------------------------------------------------------------------------
#endif
