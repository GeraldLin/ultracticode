//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1
//坐席到IVR的消息規則
const VXML_SEND_MSG_CMD_RULE_STRUCT       MONIVRMsgRuleArray[MAX_MONIVRMSG_NUM]=
{
  //MsgId = 0 					功能描述：監控登錄
  //消息名							消息參數個數
  {"monitorlogin",			5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //標志(0xTTNNnnnn TT:06電腦坐席類型 NN:電腦坐席客戶端編號 nnnn:對話消息序列號)
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",			  5,			0,				0,			"" }, //登錄的坐席號
			{"workerno",			5,			0,				0,			"" }, //登錄的話務員工號
			{"password",			5,			0,				0,			"" }, //登錄密碼
		},
	},//end of MsgId = 0

  //MsgId = 1 					功能描述：監聽電話
  //消息名							消息參數個數
  {"listencall",		    4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //被監聽的坐席號:為0時則按工號監聽
			{"workerno",			2,			0,				0,			"" }, //被監聽的話務員工號，不指定工號則為空或0
		},
	},//end of MsgId = 1

  //MsgId = 2 					功能描述：強插電話
  //消息名							消息參數個數
  {"breakincall",		    4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //被強插的坐席號:為0時則按工號強插
			{"workerno",			2,			0,				0,			"" }, //被強插的話務員工號，不指定工號則為空或0
		},
	},//end of MsgId = 2

  //MsgId = 3 					功能描述：強接電話
  //消息名							消息參數個數
  {"takeovercall",		    4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //被強接的坐席號:為0時則按工號強接
			{"workerno",			2,			0,				0,			"" }, //被強接的話務員工號，不指定工號則為空或0
		},
	},//end of MsgId = 3

  //MsgId = 4 					功能描述：發送文字消息
  //消息名							消息參數個數
  {"sendmessage",		    6,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //接收消息的坐席號:為0時則按工號強接
			{"workerno",			2,			0,				0,			"" }, //接收消息的話務員工號，不指定工號則為空或0
			{"groupno",				5,			0,				0,			"" }, //接收消息的話務員組號:0-不指定組號
			{"message",			  5,			0,				0,			"" }, //消息內容（不超過256字節）
		},
	},//end of MsgId = 4

  //MsgId = 5 					功能描述：強拆其他坐席的電話
  //消息名							消息參數個數
  {"forceclear",		    4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 5

  //MsgId = 6 					功能描述：強制將其他話務員退出
  //消息名							消息參數個數
  {"forcelogout",		    4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 6

  //MsgId = 7 					功能描述：強制其他坐席免打擾
  //消息名							消息參數個數
  {"forcebusy",		      4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 7

  //MsgId = 8 					功能描述：強制其他坐席空閑
  //消息名							消息參數個數
  {"forceready",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 8

  //MsgId = 9 					功能描述：取所有坐席的狀態信息
  //消息名							消息參數個數
  {"getagentstatus",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"sessionid",			2,			0,				0,			"" }, //ACD標志
			{"monid",					2,			1,				6,			"" }, //監控標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示取所有坐席狀態
		},
	},//end of MsgId = 9

  //MsgId = 10 					功能描述：取所有通道的狀態信息
  //消息名							消息參數個數
  {"getchnstatus",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開
		},
	},//end of MsgId = 10

  //MsgId = 11 					功能描述：取ACD分配隊列信息
  //消息名							消息參數個數
  {"getacdqueueinfo",		3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示取簡單排隊信息 2-表示取詳細排隊信息
		},
	},//end of MsgId = 11

  //MsgId = 12 					功能描述：取坐席實時統計數據
  //消息名							消息參數個數
  {"getagenttotal",		  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示取簡單排隊信息 2-表示取詳細排隊信息
		},
	},//end of MsgId = 12

  //MsgId = 13 					功能描述：取坐席組實時統計數據
  //消息名							消息參數個數
  {"getgrouptotal",		  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示取簡單排隊信息 2-表示取詳細排隊信息
		},
	},//end of MsgId = 13
};
