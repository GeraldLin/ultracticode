//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//db服務器-->vxml解析器(parser)
//MsgId	MsgName		AttrNum	Note
const VXML_RECV_MSG_CMD_RULE_STRUCT       DBXMLMsgRuleArray[MAX_DBXMLMSG_NUM]=
{
{/*0*/	"ongwquery",10,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"result"	    ,2	,1		,6	,2		,8},
					{"msgtype"	  ,2	,1		,7	,3		,7},
					{"returnitem0",5	,0		,0	,3		,8},
					{"returnitem1",5	,0		,0	,3		,9},
					{"returnitem2",5	,0		,0	,3		,10},
					{"returnitem3",5	,0		,0	,3		,11},
				},        
													},//查詢外部數據網關結果
{/*1*/	"ondbquery",8	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"records"	  ,2	,1		,7	,3		,2},
					{"fields"   	,2	,1		,7	,3		,3},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//查詢數據表操作結果
{/*2*/	"ondbfield",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"value"	    ,5	,0		,0	,3		,2},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//取數據字段值操作結果
{/*3*/	"ondbmove",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"recordno"	  ,2	,1		,7	,3		,1},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//記錄指針移動結果
{/*4*/	"ondbcmd",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"records"	  ,2	,1		,7	,5		,126},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//插入、修改、刪除記錄結果
{/*5*/	"onwritebill",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"tollfee"    ,3	,0		,0	,3		,9}, //計費金額
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//寫話單結果
{/*6*/	"ongettolltime",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"tolltime"	  ,2	,0		,0	,3		,9}, //秒數
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//返回最大通話時長
{/*7*/	"ongwmakecall",7	,
				{
					{"gwserialno", 2,  0,   0,   0,   0}, //發起呼叫者的序列號0x10NNxxxx(NN表示數據網關編號1-8,xxxx由外部數據網關產生，必須唯一)
					{"callerno",   5,  0,   0,   0,   0}, //主叫號碼
					{"calledno",   5,  0,   0,   0,   0}, //被叫號碼
					{"account",    5,  0,   0,   0,   0}, //賬號
					{"funcno",     2,  1,   6,   0,   0}, //執行的流程功能號
					{"msgtype",    2,  1,   7,   0,   0}, //呼叫數據類型
					{"msg",        5,  0,   0,   0,   0}, //呼叫附加消息
				},
													},//外部數據網關向流程發送呼叫事件
{/*8*/	"ondbrecord",8	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"recordno"	  ,2	,1		,7	,3		,3},
					{"fieldlist"  ,5	,0		,0	,3		,4}, //字段值用`隔開
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//返回讀數據表當前記錄字段值
{/*9*/	"ondbsp",6	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//執行存儲過程結果 //edit 2009-02-15 add
{/*10*/	"ondbspoutparam",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"outparamlist",5	,0		,0	,3		,2}, //輸出參數值用`隔開
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//返回讀數據表當前記錄字段值 //edit 2009-02-15 add
{/*11*/	"ongwmsgevent",4	,
				{
					{"sessionid",  2,  0,   0,   1,   1}, //流程會話序列號
					{"gwserialno", 2,  0,   0,   0,   0}, //發起控制的序列號0x10NNxxxx(NN表示數據網關編號1-8,xxxx由外部數據網關產生，必須唯一)
					{"msgtype",    2,  1,   6,   0,   0}, //事件類型
					{"msg",        5,  0,   0,   0,   0}, //附加信息
				},
													},//外部數據網關向流程發送消息事件
{/*12*/	"ondbfieldtofile",7	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"filename"	  ,5	,0		,0	,3		,4},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//取查詢的字段值并存放到指定的文件結果
{/*13*/	"ondbfiletofield",6	,
				{
					{"sessionid"	,2	,0		,0	,1		,1},
					{"cmdaddr"	  ,2	,1		,8	,1		,2},
					{"chantype"	  ,2	,1		,2	,1		,3},
					{"channo"	    ,2	,1		,7	,1		,4},
					{"result"	    ,2	,1		,6	,2		,8},
					{"errorbuf"	  ,5	,0		,0	,4		,1},
				},
													},//將文件內容存入數據表結果
	
	{/*14*/	"onquerywebservice",7	, //執行查詢外部webservice接口數據結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"xmlstring"	,5	,0		,0	,3		,9},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	
	{/*15*/	"ongetwebservicexml",7	, //取外部webservice接口查詢的節點數據
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"string"			,5	,0		,0	,3		,5},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	
	{/*16*/	"ongetwebservicestr",7	, //從外部webservice接口查詢的字符串取數據
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"string"			,5	,0		,0	,3		,5},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	
	{/*17*/	"ongetwebserviceidx",7	, //從外部webservice接口查詢的字符串取數據
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"index"			,2	,1		,7	,3		,5},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},

	{/*18*/	"onhttprequest",7	, //發送HTTP協議請求結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"returnstring",5	,0		,0	,3		,5},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},

  {/*19*/	"onwebhttprequest",3	, //外部web界面發來的http請求消息
		{
			{"sessionid",  2,  0,   0,   0,   0}, //
			{"command",    5,  0,   0,   0,   0}, //GET, POST, or HEAD
			{"commatext",  5,  0,   0,   0,   0}, //被叫號碼
	  },
	},

	{/*20*/	"onshellexecute",7	, //執行外部程序結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"returndata"	,5	,0		,0	,3		,6},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},

	{/*21*/	"ongettxtlinestr",7	, //取文本行某段字符串結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"returndata"	,5	,0		,0	,3		,8},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	{/*22*/	"onsocketrequest",7	, //通過socket通信發送查詢請求結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"returnstring",5	,0		,0	,3		,7},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	{/*23*/	"onconnectdbfail",6	, //連接數據庫失敗
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	{/*24*/	"onconnectdbsucc",6	, //連接數據庫成功
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	{/*25*/	"ondbrecorddump",7	, //數據表記錄轉存結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
			{"records"	  ,2	,1		,7	,3		,6},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
	{/*26*/	"onimcallin",12	, //IM呼入
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"callerno",		5,	0,	0,	0,	0}, //1主叫號碼
			{"calledno",		5,	0,	0,	0,	0}, //2被叫號碼
			{"seatno",			5,	0,	0,	0,	0}, //3坐席號碼
			{"workerno",		2,	0,	0,	0,	0}, //4話務員工號
			{"acdrule",			2,	1,	6,	0,	0}, //5分配規則
			{"groupno",			5,	0,	0,	0,	0}, //6分配組號
			{"servicetype",	5,	0,	0,	0,	0}, //7服務類型
			{"imserialno",	5,	0,	0,	0,	0}, //8im會話id
			{"waittime",		2,	0,	0,	0,	0}, //9總等待時長
			{"ringtime",		2,	0,	0,	0,	0}, //10每個話務員等待時長
			{"param",				5,	0,	0,	0,	0}, //11其他參數
		},
	},
	{/*27*/	"onemailcallin",12	, //Email呼入
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"callerno",		5,	0,	0,	0,	0}, //主叫號碼
			{"calledno",		5,	0,	0,	0,	0}, //被叫號碼
			{"seatno",			5,	0,	0,	0,	0}, //坐席號碼
			{"workerno",		2,	0,	0,	0,	0}, //話務員工號
			{"acdrule",			2,	1,	6,	0,	0}, //分配規則
			{"groupno",			5,	0,	0,	0,	0}, //分配組號
			{"servicetype",	5,	0,	0,	0,	0}, //服務類型
			{"emserialno",	5,	0,	0,	0,	0}, //email會話id
			{"waittime",		2,	0,	0,	0,	0}, //總等待時長
			{"ringtime",		2,	0,	0,	0,	0}, //每個話務員等待時長
			{"param",				5,	0,	0,	0,	0}, //其他參數
		},
	},
	{/*28*/	"onfaxcallin",12	, //Fax呼入
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"callerno",		5,	0,	0,	0,	0}, //主叫號碼
			{"calledno",		5,	0,	0,	0,	0}, //被叫號碼
			{"seatno",			5,	0,	0,	0,	0}, //坐席號碼
			{"workerno",		2,	0,	0,	0,	0}, //話務員工號
			{"acdrule",			2,	1,	6,	0,	0}, //分配規則
			{"groupno",			5,	0,	0,	0,	0}, //分配組號
			{"servicetype",	5,	0,	0,	0,	0}, //服務類型
			{"fxserialno",	5,	0,	0,	0,	0}, //fax會話id
			{"waittime",		2,	0,	0,	0,	0}, //總等待時長
			{"ringtime",		2,	0,	0,	0,	0}, //每個話務員等待時長
			{"param",				5,	0,	0,	0,	0}, //其他參數
		},
	},
	{/*29*/	"onimcancel",2	, //IM取消
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"imserialno",	5,	0,	0,	0,	0}, //im會話id
		},
	},
	{/*30*/	"onemcancel",2	, //email取消
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"emserialno",	5,	0,	0,	0,	0}, //email會話id
		},
	},
	{/*31*/	"onfaxcancel",2	, //fax取消
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"fxserialno",	5,	0,	0,	0,	0}, //fax會話id
		},
	},

	{/*32*/	"onhttprequestex",8	, //發送HTTP協議請求擴展指令結果
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
  		{"returnstring",5	,0		,0	,3		,6},
  		{"returncookies",5,0		,0	,3		,7},
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},

	{/*33*/	"ondbselect",9	,//返回查詢數據表記錄擴展指令當前記錄字段值
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
			{"records"	  ,2	,1		,7	,3		,2},
			{"fields"   	,2	,1		,7	,3		,3},
			{"fieldlist"  ,5	,0		,0	,3		,4}, //字段值用`隔開
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},

	{/*34*/	"ongetwebservicejson",8	, //取外部webservice接口查詢的JSON節點數據
		{
			{"sessionid"	,2	,0		,0	,1		,1},
			{"cmdaddr"	  ,2	,1		,8	,1		,2},
			{"chantype"	  ,2	,1		,2	,1		,3},
			{"channo"	    ,2	,1		,7	,1		,4},
			{"records"	  ,2	,1		,7	,3		,5},
  		{"fieldlist"  ,5	,0		,0	,3		,6}, //字段值用`隔開
			{"result"	    ,2	,1		,6	,2		,8},
			{"errorbuf"	  ,5	,0		,0	,4		,1},
		},
	},
};
													
