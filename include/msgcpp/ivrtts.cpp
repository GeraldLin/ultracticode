
//消息格式：
//IVR服務器-->TTS服務器

const VXML_SEND_MSG_CMD_RULE_STRUCT       IVRTTSMsgRuleArray[MAX_IVRTTSMSG_NUM]=
{
  //MsgId = 0 			功能描述：字符串合成
  //消息名					消息參數個數
  {"ttsstring",			10,
		{					
			//AttrNameEng	VarType	MacroType	MacroNo	Default
			{"sessionid", 2,      0,        0,      ""}, //會話序列號
			{"cmdaddr",   2,      1,        8,      ""}, //流程指令地址
			{"chantype",  2,      1,        2,      ""}, //通道類型
			{"channo",    2,      1,        7,      ""}, //通道號
			{"lang",      2,      1,        7,      "0"},//語言
			{"gender",    2,      1,        7,      "0"},//聲音性別
			{"age",       2,      1,        7,      "0"},//聲音年齡
			{"format",    2,      1,        7,      "0"},//語音編碼格式
			{"stream",    2,      1,        7,      "0"},//返回語音流方式
			{"grammar",   5,      0,        0,      ""}, //TTS基本語法
			{"string",    5,      0,        0,      ""}, //文本字符串
		},
	},//end of MsgId = 0
	
  //MsgId = 1 			功能描述：文本文件合成
  //消息名					消息參數個數
  {"ttsfile",			  10,
		{					
			//AttrNameEng	VarType	MacroType	MacroNo	Default
			{"sessionid", 2,      0,        0,      ""}, //會話序列號
			{"cmdaddr",   2,      1,        8,      ""}, //流程指令地址
			{"chantype",  2,      1,        2,      ""}, //通道類型
			{"channo",    2,      1,        7,      ""}, //通道號
			{"lang",      2,      1,        7,      "0"},//語言
			{"gender",    2,      1,        7,      "0"},//聲音性別
			{"age",       2,      1,        7,      "0"},//聲音年齡
			{"format",    2,      1,        7,      "0"},//語音編碼格式
			{"stream",    2,      1,        7,      "0"},//返回語音流方式
			{"grammar",   5,      0,        0,      ""}, //TTS基本語法
			{"filename",  5,      0,        0,      ""}, //文本文件名
		},
	},//end of MsgId = 1
	
  //MsgId = 2 			功能描述：停止TTS合成
  //消息名					消息參數個數
  {"stoptts",			  4,
		{					
			//AttrNameEng	VarType	MacroType	MacroNo	Default
			{"sessionid", 2,      0,        0,      ""}, //會話序列號
			{"cmdaddr",   2,      1,        8,      ""}, //流程指令地址
			{"chantype",  2,      1,        2,      ""}, //通道類型
			{"channo",    2,      1,        7,      ""}, //通道號
		},
	},//end of MsgId = 2
};
