//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//IVR服務器-->AGENT坐席
//MsgId	MsgName		AttrNum	Note
const VXML_RECV_MSG_CMD_RULE_STRUCT       IVRAGMsgRuleArray[MAX_IVRAGMSG_NUM]=
{
  //MsgId = 0 					功能描述：坐席登錄結果
  //消息名							消息參數個數
	{"onseatlogin",				9,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"agentno",				2,			1,				7,			0,          0}, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			0,					0}, //返回的坐席號
			{"seattype",			2,			1,				6,			0,					0}, //返回的坐席類型
			{"chantype",			2,			1,				2,			0,					0}, //返回的坐席電話綁定的通道類型
			{"channo",				2,			1,				7,			0,					0}, //返回的坐席電話綁定的通道號
			{"result",				2,			1,				6,			0,					0}, //返回的結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
 			{"hosttype",			2,			1,				6,			0,					0}, //坐席端口所屬的服務器：0-主機，1-備機
		},        
	},//end of MsgId = 0

  //MsgId = 1 					功能描述：話務員登錄結果
  //消息名							消息參數個數
	{"onworkerlogin",				4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"inform",				5,			0,				0,			0,					0}, //通知消息
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 1

  //MsgId = 2 					功能描述：話務員退出結果
  //消息名							消息參數個數
	{"onworkerlogout",		3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 2

  //MsgId = 3 					功能描述：免打擾設置結果
  //消息名							消息參數個數
	{"ondisturb",					3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 3

  //MsgId = 4 					功能描述：離席/在席設置結果
  //消息名							消息參數個數
	{"onleval",						3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 4

  //MsgId = 5 					功能描述：設置呼叫轉移結果
  //消息名							消息參數個數
	{"onsetforward",			4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"forwardtype",		2,			1,				6,			0,					0}, //呼叫轉移類型: 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 5

  //MsgId = 6 					功能描述：取消呼叫轉移結果
  //消息名							消息參數個數
	{"oncancelforward",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"forwardtype",		2,			1,				6,			0,					0}, //取消呼叫轉移類型: 0-取消所有轉移設置 1-取消無條件轉移 2-取消遇忙轉移 3-取消久叫不應轉移
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 6

  //MsgId = 7 					功能描述：呼入分配坐席消息
  //消息名							消息參數個數
	{"onacdcallin",				11,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //呼入方的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼入方的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
			{"orgcallerno",		5,			0,				0,			0,					0}, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			0,					0}, //原被叫號碼
			{"calltype",			2,			1,				6,			0,					0}, //呼叫類型: 0-呼入來電 1-轉接來電 2-呼出電話 3-代接電話
 			{"funcno",				2,			1,				6,			0,					0}, //業務功能號
 			{"param",					5,			0,				0,			0,					0}, //附加參數
		},        
	},//end of MsgId = 7

  //MsgId = 8 					功能描述：停止呼入分配坐席消息
  //消息名							消息參數個數
	{"onstopacdcallin",			3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果 0-成功 1-失敗
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 8

  //MsgId = 9 					功能描述：坐席應答結果
  //消息名							消息參數個數
	{"onanswercall",			3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果 0-成功 1-失敗 2-坐席電話未摘機(針對純電腦坐席)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 9

  //MsgId = 10 					功能描述：發起呼叫結果
  //消息名							消息參數個數
	{"onmakecall",				9,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				6,			0,					0}, //呼出的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼出的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 10

  //MsgId = 11 					功能描述：坐席掛機消息
  //消息名							消息參數個數
	{"onhangon",					2,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"flag",					2,			1,				6,			0,					0}, //
		},        
	},//end of MsgId = 11

  //MsgId = 12 					功能描述：轉接電話結果
  //消息名							消息參數個數
	{"ontrancall",	11,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
 			{"TranType",			2,			1,				6,			0,					0}, //轉接類型: 1-快速轉接電話 2-協商轉接電話 3-會議轉接電話
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				6,			0,					0}, //轉接呼出的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //轉接呼出的通道號
 			{"confno",				2,			1,				6,			0,					0}, //會議號,會議轉接時用
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 12

  //MsgId = 13 					功能描述：停止轉接結果
  //消息名							消息參數個數
	{"onstoptrancall",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果 0-成功 1-失敗
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 13

  //MsgId = 14 					功能描述：參加會議結果
  //消息名							消息參數個數
	{"onjoinconf",	7,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"destchntype",		2,			1,				2,			0,					0}, //對方的通道類型
 			{"destchnno",			2,			1,				7,			0,					0}, //對方的通道號
 			{"confno",				2,			1,				6,			0,					0}, //會議號
 			{"jointype",			2,			1,				6,			0,					0}, //參見會議類型 1-發言 2-旁聽
			{"result",				2,			1,				6,			0,					0}, //結果 0-成功 1-失敗
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 14

  //MsgId = 15 					功能描述：轉接ivr流程結果
  //消息名							消息參數個數
	{"ontranivr",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
 			{"returnflag",		2,			1,				6,			0,					0}, //流程返回的標志（0-返回轉接IVR結果 1-重新返回坐席服務）
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 15

  //MsgId = 16 					功能描述：發送DTMF按鍵結果
  //消息名							消息參數個數
	{"onsenddtmf",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 16

  //MsgId = 17 					功能描述：播放語音文件結果
  //消息名							消息參數個數
	{"onplay",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 17

  //MsgId = 18 					功能描述：停止放音結果
  //消息名							消息參數個數
	{"onstopplay",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 18

  //MsgId = 19 					功能描述：代接結果
  //消息名							消息參數個數
	{"onpickup",	12,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //呼入方的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼入方的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
			{"orgcallerno",		5,			0,				0,			0,					0}, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			0,					0}, //原被叫號碼
 			{"funcno",				2,			1,				6,			0,					0}, //業務功能號
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 19

  //MsgId = 20 					功能描述：搶接ACD隊列里的呼叫結果
  //消息名							消息參數個數
	{"onpickupqueuecall",	12,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //呼入方的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼入方的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
			{"orgcallerno",		5,			0,				0,			0,					0}, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			0,					0}, //原被叫號碼
 			{"funcno",				2,			1,				6,			0,					0}, //業務功能號
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 20

  //MsgId = 21 					功能描述：接管其他坐席的電話結果
  //消息名							消息參數個數
	{"ontakeover",	12,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //呼入方的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼入方的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
			{"orgcallerno",		5,			0,				0,			0,					0}, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			0,					0}, //原被叫號碼
 			{"funcno",				2,			1,				6,			0,					0}, //業務功能號
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 21

  //MsgId = 22 					功能描述：監聽結果
  //消息名							消息參數個數
	{"onlisten",	12,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //呼入方的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼入方的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
			{"orgcallerno",		5,			0,				0,			0,					0}, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			0,					0}, //原被叫號碼
 			{"funcno",				2,			1,				6,			0,					0}, //業務功能號
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 22

  //MsgId = 23 					功能描述：強插通話結果
  //消息名							消息參數個數
	{"oninsert",	12,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //呼入方的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //呼入方的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //被叫號碼
			{"orgcallerno",		5,			0,				0,			0,					0}, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			0,					0}, //原被叫號碼
 			{"funcno",				2,			1,				6,			0,					0}, //業務功能號
 			{"param",					5,			0,				0,			0,					0}, //附加參數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 23

  //MsgId = 24 					功能描述：保持/取消保持結果
  //消息名							消息參數個數
	{"onhold",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 24

  //MsgId = 25 					功能描述：靜音/取消靜音結果
  //消息名							消息參數個數
	{"onmute",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 25

  //MsgId = 26 					功能描述：收到關聯的通道釋放消息
  //消息名							消息參數個數
	{"onrelease",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 26

  //MsgId = 27 					功能描述：強拆其他坐席結果
  //消息名							消息參數個數
	{"onforceclear",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 27

  //MsgId = 28 					功能描述：強制將其他話務員退出結果
  //消息名							消息參數個數
	{"onforcelogout",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 28

  //MsgId = 29 					功能描述：強制其他坐席免打擾結果
  //消息名							消息參數個數
	{"onforcedisturb",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 29

  //MsgId = 30 					功能描述：強制其他坐席空閑結果
  //消息名							消息參數個數
	{"onforceready",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 30

  //MsgId = 31 					功能描述：收到ACD分配簡單隊列信息
  //消息名							消息參數個數
	{"ongetqueueinfo",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"queuenum",			2,			1,				7,			0,					0}, //隊列等待數
			{"queueinfo",			5,			0,				0,			0,					0}, //隊列信息:ACD編號1,主叫號碼1,排隊時間,等待狀態acdState,已分配的坐席號,組號;ACD編號2,主叫號碼2,排隊時間;......
			{"waitacdnum",		2,			1,				7,			0,					0}, //等待分配空閑坐席數
			{"waitansnum",		2,			1,				7,			0,					0}, //已分配到空閑坐席等待應答數
		},
	},//end of MsgId = 31

  //MsgId = 32 					功能描述：收到坐席的狀態信息
  //消息名							消息參數個數
	{"ongetagentstatus",	18,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //0 ACD標志
			{"agno",		  		2,			0,				0,			0,					0}, //1 序號
			{"seatno",				5,			0,				0,			0,					0}, //2 坐席分機號
			{"clientid",		  2,			0,				0,			0,					0}, //3 客戶端編號
			{"seattype",		  2,			0,				0,			0,					0}, //4 坐席類型
			{"seatgroupno",		2,			0,				0,			0,					0}, //5 坐席組號
			{"seatip",				5,			0,				0,			0,					0}, //6 坐席ip
			{"workerno",			2,			0,				0,			0,					0}, //7 話務員工號
			{"logintime",			5,			0,				0,			0,					0}, //8 話務員登錄時間
			{"workername",		5,			0,				0,			0,					0}, //9 話務員姓名
			{"groupno",				5,			0,				0,			0,					0}, //10 話務員組號
			{"svstate",				2,			1,				6,			0,					0}, //11 服務狀態
			{"disturbid",			2,			1,				6,			0,					0}, //12 0-可打擾 1-免打擾
			{"leaveid",				2,			1,				6,			0,					0}, //13 在席(0)/離席(>0)標志
			{"anscount",			2,			1,				7,			0,					0}, //14 應答總次數
			{"noanscount",	  2,			1,				7,			0,					0}, //15 未應答總次數
			{"leavereason",		5,			0,				0,			0,					0}, //16 離席原因
 			{"callmsg",				5,			0,				0,			0,					0}, //17 電話呼叫信息 inout;callerno;calltime
		},        
	},//end of MsgId = 32

  //MsgId = 33 					功能描述：收到通道的狀態信息
  //消息名							消息參數個數
	{"ongetchnstatus",	16,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",			2,			0,				0,			0,					0}, //ACD標志
			{"chnno",		  2,			0,				0,			0,					0}, //序號
			{"chtype",	  2,			0,				0,			0,					0}, //硬件通道類型
			{"chindex",		2,			0,				0,			0,					0}, //硬件通道號
			{"lgchntype",	2,			0,				0,			0,					0}, //業務通道類型
			{"lgchnno",		2,			0,				0,			0,					0}, //業務通道號
			{"hwstate",		2,			0,				0,			0,					0}, //硬件狀態
			{"lnstate",	  2,			0,				0,			0,					0}, //線路狀態
			{"ssstate",		2,			0,				0,			0,					0}, //信令狀態
			{"inout",			2,			0,				0,			0,					0}, //呼叫方向
 			{"callerno",	5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",	5,			0,				0,			0,					0}, //被叫號碼
 			{"calltime",	5,			0,				0,			0,					0}, //呼叫時間
 			{"anstime",	  5,			0,				0,			0,					0}, //應答時間
 			{"svstate",	  5,			0,				0,			0,					0}, //業務狀態信息
 			{"deviceid",	5,			0,				0,			0,					0}, //交換機對應的分機號
		},        
	},//end of MsgId = 33

  //MsgId = 34 					功能描述：收到流程發來的消息
  //消息名							消息參數個數
	{"onrecvmsgfromflw",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",			2,			0,				0,			0,					0}, //ACD標志
 			{"msgtype",	  2,			0,				0,			0,					0}, //消息類型
 			{"item0",	    5,			0,				0,			0,					0}, //消息參數0
 			{"item1",	    5,			0,				0,			0,					0}, //消息參數0
 			{"item2",	    5,			0,				0,			0,					0}, //消息參數0
 			{"item3",	    5,			0,				0,			0,					0}, //消息參數0
		},        
	},//end of MsgId = 34

  //MsgId = 35 					功能描述：發送傳真結果
  //消息名							消息參數個數
	{"onsendfax",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 35

  //MsgId = 36 					功能描述：接收傳真,通話后接收傳真結果
  //消息名							消息參數個數
	{"onrecvfax",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 36

  //MsgId = 37 					功能描述：收到文字信息
  //消息名							消息參數個數
	{"onrecvmessage",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"seatno",				5,			0,				0,			0,					0}, //發送信息的坐席號
			{"workerno",			2,			1,				7,			0,					0}, //發送信息的工號號
			{"workername",	  5,			0,				0,			0,					0}, //發送信息的話務員姓名
 			{"msg",			      5,			0,				0,			0,					0}, //文字信息
		},        
	},//end of MsgId = 37

  //MsgId = 38 					功能描述：收到手機短信
  //消息名							消息參數個數
	{"onrecvsms",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 38
  
  //MsgId = 39 					功能描述：收到ACD分配詳細隊列信息
  //消息名							消息參數個數
	{"ongetqueueinfoex",	16,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"queueno",				2,			1,				6,			0,					0}, //隊列位置
			{"acdno",				  2,			1,				6,			0,					0}, //ACD排隊記錄號
			{"state",				  2,			1,				6,			0,					0}, //等待標志()
			{"sessionno",			2,			0,				0,			0,					0}, //流程會話序列號
			{"chantype",			2,			1,				2,			0,					0}, //等待者的通道類型
 			{"channo",				2,			1,				7,			0,					0}, //等待者的通道號
 			{"callerno",			5,			0,				0,			0,					0}, //等待者的主叫號碼
 			{"calledno",			5,			0,				0,			0,					0}, //等待者的被叫號碼
			{"acdtime",	  		5,			0,				0,			0,					0}, //排隊時間
			{"waitlen",	  		2,			0,				0,			0,					0}, //等待時長秒
			{"seatno",				5,			0,				0,			0,					0}, //需要分配的座席號
			{"workerno",			2,			0,				0,			0,					0}, //需要分配的話務員工號
			{"groupno",				5,			0,				0,			0,					0}, //需要分配的話務員組號
			{"acdseatno",			5,			0,				0,			0,					0}, //已分配的座席號
			{"acdworkerno",		2,			0,				0,			0,					0}, //已分配的工號
		},
	},//end of MsgId = 39

  //MsgId = 40 			功能描述：會議成員狀態結果
  //消息名					消息參數個數
	{"ongetconfmember",		7,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",			2,			0,				0,			0,					0}, //ACD標志
			{"confno",	  2,			0,				0,			0,					0}, //會議編號
			{"state",     2,			0,				0,			0,					0}, //加入狀態 //0-退出 1-主持 2-加入 3-旁聽 4-清空所有成員
			{"chntype",	  2,			0,				0,			0,					0}, //通道類型
			{"chnno",     2,		  0,				0,			0,					0}, //通道號
			{"callerno",  5,			0,				0,			0,					0}, //主叫號碼
			{"jointime",  5,		  0,				0,			0,					0}, //加入時間
		},        
	},//end of MsgId = 40

  //MsgId = 41 					功能描述：創建會議結果
  //消息名							消息參數個數
	{"oncreateconf",	4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"confno",				2,			1,				6,			0,					0}, //創建的會議室編號
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 41

  //MsgId = 42 					功能描述：釋放會議結果
  //消息名							消息參數個數
	{"ondestroyconf",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 42
  
  //MsgId = 43 					功能描述：設置事后處理時長(秒)結果
  //消息名							消息參數個數
	{"onsetacwtimer",					3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 43
  
  //MsgId = 44 					功能描述：取數據庫登錄信息結果
  //消息名							消息參數個數
	{"ongetdbparam",					7,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"dbtype",			  5,			0,				0,			0,					0}, //數據庫類型：SQLSERVER，MYSQL
 			{"dbserver",			5,			0,				0,			0,					0}, //數據庫服務器IP
 			{"database",			5,			0,				0,			0,					0}, //數據庫名稱
 			{"userid",			  5,			0,				0,			0,					0}, //登錄用戶名
 			{"password",			5,			0,				0,			0,					0}, //登錄密碼
		},        
	},//end of MsgId = 44
  
  //MsgId = 45 					功能描述：獲取的授權信息結果
  //消息名							消息參數個數
	{"ongetauthkey",			3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"authkey",			  5,			0,				0,			0,					0}, //授權信息用分號隔開
		},        
	},//end of MsgId = 45
  
  //MsgId = 46 					功能描述：穿梭保持通話結果
  //消息名							消息參數個數
	{"onswaphold",				3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 46
  
  //MsgId = 47 					功能描述：電話重新定向振鈴結果
  //消息名							消息參數個數
	{"onredirectcall",		3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 47

  //MsgId = 48 					功能描述：設置外出值班電話結果
  //消息名							消息參數個數
	{"onsetgoouttel",			3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 48

  //MsgId = 49 					功能描述：主備切換標志
  //消息名							消息參數個數
	{"sendswitchover",		7,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"sessionid",			2,			0,				0,			0,					0}, //
			{"switchid",			2,			1,				6,			0,					0}, //
			{"hosttype",			2,			1,				6,			0,					0}, //本機類型：0-主機 1-備機
			{"masterstatus",	2,			1,				6,			0,					0}, //主機狀態：0-為待機狀態,1-為工作狀態,2-接管狀態
			{"standbystatus",	2,			1,				6,			0,					0}, //備機狀態：0-為待機狀態,1-為工作狀態,2-接管狀態
 			{"reason",			  2,			1,				7,			0,					0}, //切換原因值
 			{"reasonmsg",			5,			0,				0,			0,					0}, //切換原因信息
		},        
	},//end of MsgId = 49

	//start 50
	{"onquerytrunkgroup",	6,	//查詢中繼群占用數結果
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"switchid",		2,	1,	6,	0,	0}, //交換機節點標志
			{"trunkgroup",	5,	0,	0,	0,	0}, //中繼線路群
			{"idletrunks",	2,	1,	7,	0,	0}, //空閑中繼線路數
			{"usedtrunks",	2,	1,	7,	0,	0}, //占用中繼線路數
			{"trunkparam",	5,	0,	0,	0,	0}, //中繼擴展參數
		},
	}, //end 50
	
	//start 51
	{"onqueryacdsplit",	7,	//查詢ACD數結果
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"switchid",		2,	1,	6,	0,	0}, //交換機節點標志
			{"acdsplit",		5,	0,	0,	0,	0}, //ACD群組
			{"availagents",	2,	1,	7,	0,	0}, //有效的坐席數
			{"callsinqueue",2,	1,	7,	0,	0}, //隊列等待數
			{"loginagents", 2,	1,	7,	0,	0}, //登錄坐席數
			{"acdparam",	  5,	0,	0,	0,	0}, //ACD擴展參數
		},
	}, //end 51
};
