//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1
//坐席到IVR的消息規則
const VXML_SEND_MSG_CMD_RULE_STRUCT       AGIVRMsgRuleArray[MAX_AGIVRMSG_NUM]=
{
  //MsgId = 0 					功能描述：坐席登錄
  //消息名							消息參數個數
  {"seatlogin",					9,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志(0xTTNNnnnn TT:06電腦坐席類型 NN:電腦坐席客戶端編號 nnnn:對話消息序列號)
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號,有IVR返回
			{"seatno",				5,			0,				0,			"" }, //坐席號,當為0時表示根據IP地址綁定
			{"phoneno",				5,			0,				0,			"" }, //遠端坐席的電話號碼(遠端坐席有用,不填號碼則用配置文件設置的外線號碼)
			{"seatip",				5,			0,				0,			"" }, //坐席電腦IP地址
			{"clearid",				2,			1,				6,			"" }, //清理服務累計標記
			{"groupno",				2,			1,				6,			"0"}, //坐席組號
			{"langid",				2,			1,				6,			"0"}, //語言類型 0-默認 1-簡體中文 2-繁體中文 3-美國英文
			{"seattype",			2,			1,				6,			"0"}, //坐席類型 
		},
	},//end of MsgId = 0
	
  //MsgId = 1 					功能描述：話務員登錄
  //消息名							消息參數個數
  {"workerlogin",					15,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //0 ACD標志
			{"agentno",				2,			1,				7,			"" }, //1 坐席統一邏輯編號
			{"workerno",			2,			0,				0,			"0" },//2 話務員工號
			{"name",					5,			0,				0,			"" }, //3 話務員姓名
			{"psw",						5,			0,				0,			"" }, //4 話務員密碼
			{"groupno",				5,			0,				0,			"" }, //5 話務員組號
			{"grade",					2,			1,				6,			"" }, //6 話務員類別:(1-普通話務員席、2-班長席、3-管理員席)
			{"level",					5,			0,				0,			"" }, //7 話務員級別
			{"clearid",				2,			1,				6,			"" }, //8 清理服務累計標記
			{"state",				  2,			1,				6,			"0" },//9 登錄后初始狀態(0-本次登錄前的狀態 1-示忙)
			{"dutyno",				2,			1,				6,			"0" },//10 上班班次
			{"autorecord",		2,			1,				6,			"1" },//11 自動對通話錄音
			{"calldirection", 2,			1,				6,			"0" },//12 呼叫方向(0-雙向服務 1-呼入 2-呼出)
			{"accountno",     5,			0,				0,			"" }, //13 登錄的賬號
			{"departmentno",  5,			0,				0,			"0" },//14 部門編號
		},
	},//end of MsgId = 1
	
  //MsgId = 2 					功能描述：話務員退出
  //消息名							消息參數個數
  {"workerlogout",				2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
		},
	},//end of MsgId = 2
	
  //MsgId = 3 					功能描述：免打擾設置
  //消息名							消息參數個數
  {"disturb",						3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //1-免打擾 0-可打擾
		},
	},//end of MsgId = 3
	
  //MsgId = 4 					功能描述：離席/在席設置
  //消息名							消息參數個數
  {"leval",							4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //0-在席 >1-離席
			{"reason",				5,			0,				0,			"" }, //離席時的原因
		},
	},//end of MsgId = 4
	
  //MsgId = 5 					功能描述：設置呼叫轉移
  //消息名							消息參數個數
  {"setforward",				4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"forwardtype",		2,			1,				6,			"" }, //呼叫轉移類型: 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移
			{"phoneno",			  5,			0,				0,			"" }, //設置的呼叫轉移號碼
		},
	},//end of MsgId = 5
	
  //MsgId = 6 					功能描述：取消呼叫轉移
  //消息名							消息參數個數
  {"cancelforward",			3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"forwardtype",		2,			1,				6,			"" }, //取消呼叫轉移類型: 0-取消所有轉移設置 1-取消無條件轉移 2-取消遇忙轉移 3-取消久叫不應轉移
		},
	},//end of MsgId = 6

  //MsgId = 7 					功能描述：坐席應答
  //消息名							消息參數個數
  {"answercall",				5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //0-收到呼入正在振鈴 1-應答來話 2-拒絕來話 3-傳遞參數
			{"serialno",			5,			0,				0,			"" }, //電腦坐席傳遞給流程的業務受理工單號
			{"param",					5,			0,				0,			"" }, //電腦坐席傳遞給流程的附加參數（可自定義格式）
		},
	},//end of MsgId = 7
  
  //MsgId = 8 					功能描述：發起呼叫
  //消息名							消息參數個數
  {"makecall",					11,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"callerno",			5,			0,				0,			"" }, //主叫號碼
			{"callertype",		2,			1,				6,			"" }, //主叫類別
			{"calledno",			5,			0,				0,			"" }, //被叫號碼
			{"calledtype",		2,			1,				6,			"" }, //被叫類別: 0-外線呼入號碼(外線有效) 1-呼出的外線號碼 2-呼叫的內線坐席號 3-呼叫的坐席工號
			{"orgcallerno",		5,			0,				0,			"" }, //原主叫號碼
			{"orgcalledno",		5,			0,				0,			"" }, //原被叫號碼
			{"routeno",				2,			1,				6,			"" }, //呼出路由
			{"funcno",			  2,			1,				6,			"" }, //業務功能號
			{"param",					5,			0,				0,			"" }, //呼出附加參數（可自定義格式）
		},
	},//end of MsgId = 8
  
  //MsgId = 9 					功能描述：坐席掛機
  //消息名							消息參數個數
  {"hangon",						3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //掛機標志
		},
	},//end of MsgId = 9
  
  //MsgId = 10 					功能描述：快速轉接電話
  //消息名							消息參數個數
  {"blindtrancall",		5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"phonetype",			2,			1,				6,			"" }, //轉接號碼類型: 1-外線號碼 2-坐席分機號 3-話務員工號
			{"tranphone",			5,			0,				0,			"" }, //轉接號碼
			{"tranparam",			5,			0,				0,			"" }, //轉接的附加參數（可自定義格式）
		},
	},//end of MsgId = 10
  
  //MsgId = 11 					功能描述：協商轉接電話
  //消息名							消息參數個數
  {"consulttrancall",		5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"phonetype",			2,			1,				6,			"" }, //轉接號碼類型: 1-外線號碼 2-坐席分機號 3-話務員工號
			{"tranphone",			5,			0,				0,			"" }, //轉接號碼
			{"tranparam",			5,			0,				0,			"" }, //轉接的附加參數（可自定義格式）
		},
	},//end of MsgId = 11
  
  //MsgId = 12 					功能描述：會議轉接電話
  //消息名							消息參數個數
  {"conftrancall",		6,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"phonetype",			2,			1,				6,			"" }, //轉接號碼類型: 0-直接將保持的1方及通話的1方加入3方會議 1-外線號碼 2-坐席分機號 3-話務員工號
			{"tranphone",			5,			0,				0,			"" }, //轉接號碼
			{"confno",				2,			1,				6,			"0" }, //會議號 0-表示由流程創建會議 >0-表示指定會議號
			{"tranparam",			5,			0,				0,			"" }, //轉接的附加參數（可自定義格式）
		},
	},//end of MsgId = 12
  
  //MsgId = 13 					功能描述：停止轉接
  //消息名							消息參數個數
  {"stoptrancall",		  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //停止標志
		},
	},//end of MsgId = 13

  //MsgId = 14 					功能描述：指定通道會議發言
  //消息名							消息參數個數
  {"confspeak",		     5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"destchntype",		2,			1,				6,			"" }, //通話的通道類型
			{"destchnno",			2,			1,				7,			"" }, //通話的通道號
			{"confno",				2,			1,				6,			"0" }, //會議號 0-表示由流程創建會議 >0-表示指定會議號
		},
	},//end of MsgId = 14

  //MsgId = 15 					功能描述：指定通道旁聽會議
  //消息名							消息參數個數
  {"confaudit",		     5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"destchntype",		2,			1,				6,			"" }, //通話的通道類型
			{"destchnno",			2,			1,				7,			"" }, //通話的通道號
			{"confno",				2,			1,				6,			"0" }, //會議號 0-表示由流程創建會議 >0-表示指定會議號
		},
	},//end of MsgId = 15

  //MsgId = 16 					功能描述：轉接ivr流程
  //消息名							消息參數個數
  {"tranivr",		     5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"tranivrid",			2,			1,				7,			"" }, //轉接的ivr標志,流程通過該標志約定來判斷跳轉到哪里:1-評分,2-密碼驗證
			{"returnid",  		2,			1,				6,			"" }, //是否返回原服務坐席標志,如：要求客戶輸入密碼認證后,返回坐席繼續服務等業務
			{"tranparam",			5,			0,				0,			"" }, //傳遞的附加參數（可自定義格式）
		},
	},//end of MsgId = 16

  //MsgId = 17 					功能描述：發送DTMF按鍵
  //消息名							消息參數個數
  {"senddtmf",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"dtmf",	    		5,			0,				0,			"" }, //DTMF碼
		},
	},//end of MsgId = 17

  //MsgId = 18 					功能描述：播放語音文件
  //消息名							消息參數個數
  {"play",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"filename",	    5,			0,				0,			"" }, //語音文件名
			{"offset",		    2,			0,				0,			"0" }, //放音起始位(按百分比)
		},
	},//end of MsgId = 18

  //MsgId = 19 					功能描述：停止放音
  //消息名							消息參數個數
  {"stopplay",		     2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
		},
	},//end of MsgId = 19

  //MsgId = 20 					功能描述：代接電話
  //消息名							消息參數個數
  {"pickup",		     5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
			{"groupno",				2,			1,				7,			"" }, //話務員組號:0-不指定組號
		},
	},//end of MsgId = 20

  //MsgId = 21 					功能描述：搶接ACD隊列里的呼叫
  //消息名							消息參數個數
  {"pickupqueuecall",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"acdno",			    2,			1,				6,			"" }, //ACD編號,通過ongetqueueinfo或得
		},
	},//end of MsgId = 21

  //MsgId = 22 					功能描述：接管其他坐席的電話
  //消息名							消息參數個數
  {"takeover",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 22

  //MsgId = 23 					功能描述：監聽
  //消息名							消息參數個數
  {"listen",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 23

  //MsgId = 24 					功能描述：強插通話
  //消息名							消息參數個數
  {"insert",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 24

  //MsgId = 25 					功能描述：保持/取消保持
  //消息名							消息參數個數
  {"hold",		     6,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"chantype",			2,			1,				6,			"" }, //通話的通道類型
			{"channo",				2,			1,				7,			"" }, //通話的通道號
			{"flag",					2,			1,				6,			"" }, //標志: 1-保持 0-取消保持
			{"waitvoc",				5,			0,				0,			"" }, //等待音樂
		},
	},//end of MsgId = 25

  //MsgId = 26 					功能描述：靜音/取消靜音
  //消息名							消息參數個數
  {"mute",		     5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"chantype",			2,			1,				6,			"" }, //通話的通道類型
			{"channo",				2,			1,				7,			"" }, //通話的通道號
			{"flag",					2,			1,				6,			"" }, //標志: 1-靜音 0-取消靜音
		},
	},//end of MsgId = 26

  //MsgId = 27 					功能描述：釋放一方通話
  //消息名							消息參數個數
  {"releasecall",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",			    2,			1,				6,			"" }, //0-釋放主通話方，1-釋放被求助方
		},
	},//end of MsgId = 27

  //MsgId = 28 					功能描述：強拆其他坐席的電話
  //消息名							消息參數個數
  {"forceclear",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 28

  //MsgId = 29 					功能描述：強制將其他話務員退出
  //消息名							消息參數個數
  {"forcelogout",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 29

  //MsgId = 30 					功能描述：強制其他坐席免打擾
  //消息名							消息參數個數
  {"forcedisturb",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 30

  //MsgId = 31 					功能描述：強制其他坐席空閑
  //消息名							消息參數個數
  {"forceready",		     4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 31

  //MsgId = 32 					功能描述：取ACD分配隊列信息
  //消息名							消息參數個數
  {"getqueueinfo",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示取簡單排隊信息 2-表示取詳細排隊信息
		},
	},//end of MsgId = 32

  //MsgId = 33 					功能描述：取所有坐席的狀態信息
  //消息名							消息參數個數
  {"getagentstatus",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示只取本坐席狀態 1表示取所有坐席狀態
		},
	},//end of MsgId = 33

  //MsgId = 34 					功能描述：取所有通道的狀態信息
  //消息名							消息參數個數
  {"getchnstatus",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開
		},
	},//end of MsgId = 34

  //MsgId = 35 					功能描述：發送消息到流程
  //消息名							消息參數個數
  {"sendmsgtoflw",		     7,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"chntype",				2,			1,				6,			"" }, //連接坐席電話的通道邏輯類型
			{"chnno",					2,			1,				7,			"" }, //連接坐席電話的通道邏輯號(0xFFFF表示不規定)
			{"eventtype",			2,			0,				0,			"" }, //事件類型
			{"msgtype",				2,			0,				0,			"" }, //消息類型
			{"msg",						5,			0,				0,			"" }, //消息內容
		},
	},//end of MsgId = 35

  //MsgId = 36 					功能描述：發送傳真
  //消息名							消息參數個數
  {"sendfax",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"filename",		  5,			0,				0,			"" }, //發送的傳真文件名
		},
	},//end of MsgId = 36

  //MsgId = 37 					功能描述：接收傳真,通話后接收傳真
  //消息名							消息參數個數
  {"recvfax",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"filename",		  5,			0,				0,			"" }, //接收的傳真文件名
		},
	},//end of MsgId = 37

  //MsgId = 38 					功能描述：發送文字信息
  //消息名							消息參數個數
  {"sendmessage",		     6,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			0,				0,			"" }, //話務員工號:0-不指定工號
			{"groupno",				5,			0,				0,			"" }, //話務員組號:0-不指定組號
			{"msg",				    5,			0,				0,			"" }, //消息內容（不超過256字節）
		},
	},//end of MsgId = 38

  //MsgId = 39 					功能描述：發送短信
  //消息名							消息參數個數
  {"sendsms",		     2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"mobile",				5,			0,				0,			"" }, //手機號碼
			{"msg",				    5,			0,				0,			"" }, //消息內容（不超過256字節）
		},
	},//end of MsgId = 39

  //MsgId = 40 					功能描述：創建會議
  //消息名							消息參數個數
  {"createconf",		     10,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"confno",				2,			0,				0,			"0" }, //會議編號
			{"confname",			5,			0,				0,			"" }, //會議名稱
			{"presiders",			2,			0,				0,			"" }, //會議主持人數
			{"talkers",				2,			0,				0,			"" }, //會議發言人數
			{"listeners",			2,			0,				0,			"" }, //會議旁聽人數
			{"password",			5,			0,				0,			"" }, //會議密碼
			{"recordfile",		5,			0,				0,			"" }, //會議錄音文件名
			{"autofree",			2,			0,				0,			"" }, //所有成員退出后是否自動釋放
		},
	},//end of MsgId = 40

  //MsgId = 41 					功能描述：釋放會議
  //消息名							消息參數個數
  {"destroyconf",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"confno",				2,			0,				0,			"0" }, //會議編號
		},
	},//end of MsgId = 41

  //MsgId = 42 					功能描述：設置事后處理時長(秒)
  //消息名							消息參數個數
  {"setacwtimer",		     3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"timer",				  2,			0,				0,			"0"}, //處理時長(秒)
		},
	},//end of MsgId = 42
	
  //MsgId = 43 					功能描述：事后處理完成
  //消息名							消息參數個數
  {"setacwend",					3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //1-完成
		},
	},//end of MsgId = 43
	
  //MsgId = 44 					功能描述：穿梭保持通話
  //消息名							消息參數個數
  {"swaphold",					3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"1"}, //穿梭標志
		},
	},//end of MsgId = 44
	
  //MsgId = 45 					功能描述：設置技能組名稱
  //消息名							消息參數個數
  {"setgroupname",			4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"groupno",				2,			1,				6,			"" }, //話務技能組號
			{"groupname",			5,			0,				0,			"" }, //話務技能組名稱
		},
	},//end of MsgId = 45
	
  //MsgId = 46 					功能描述：開始錄音
  //消息名							消息參數個數
  {"startrecord",			  4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"filename",			5,			0,				0,			"" }, //錄音文件名,為空時有系統自動取文件名
			{"recordlen",			2,			0,				0,			"0" }, //錄音時長秒,0表示不限制時長
		},
	},//end of MsgId = 46
	
  //MsgId = 47 					功能描述：停止錄音
  //消息名							消息參數個數
  {"stoprecord",			  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //1-只停止本次錄音,不修改自動錄音標志 0-停止錄音,并修改自動錄音標志為0
		},
	},//end of MsgId = 47
	
  //MsgId = 48 					功能描述：設置呼叫方向
  //消息名							消息參數個數
  {"setcalldirection",	3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"direction",			2,			1,				6,			"0" }, //呼叫方向(0-雙向服務 1-呼入 2-呼出)
		},
	},//end of MsgId = 48

  //MsgId = 49 					功能描述：代接電話
  //消息名							消息參數個數
  {"pickupex",		      4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"pickuptype",		2,			1,				6,			"" }, //代接目的類型 1-坐席分機號，2-話務員工號，3-話務員組號，4-話務員登錄賬號，5-話務員登錄姓名
			{"pickupdest",		5,			0,				0,			"" }, //代接目的號碼、賬號或名稱
		},
	},//end of MsgId = 49

  //MsgId = 50 					功能描述：電話重新定向振鈴
  //消息名							消息參數個數
  {"redirectcall",		  6,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"sourseatno",		5,			0,				0,			"" }, //源頭坐席號:0-不指定坐席號
			{"sourworkerno",	2,			0,				0,			"" }, //源頭話務員工號:0-不指定工號
			{"destseatno",		5,			0,				0,			"" }, //目的坐席號:0-不指定坐席號
			{"destworkerno",	2,			0,				0,			"" }, //目的話務員工號:0-不指定工號
		},
	},//end of MsgId = 50
	
  //MsgId = 51 					功能描述：設置外出值班電話
  //消息名							消息參數個數
  {"setgoouttel",				4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"flag",					2,			1,				6,			"" }, //0-取消 1-設置
			{"dutytel",				5,			0,				0,			"" }, //外出值班的電話
		},
	},//end of MsgId = 51
	
  //MsgId = 52 					功能描述：查詢IVR運行狀態
  //消息名							消息參數個數
  {"queryivrstatus",		3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"acdid",					2,			0,				0,			"" }, //ACD標志
			{"agentno",				2,			1,				7,			"" }, //坐席統一邏輯編號
			{"hosttype",			2,			1,				6,			"" }, //0-主機 1-備機 2-全部
		},
	},//end of MsgId = 52
};
