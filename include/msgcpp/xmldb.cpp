//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1
												
const VXML_SEND_MSG_CMD_RULE_STRUCT       XMLDBMsgRuleArray[MAX_XMLDBMSG_NUM]=
{
{/*0*/	"gwquery"	,9,	//發送消息到外部數據網關
	{					
		{"sessionid", 2,0,0,""},         
		{"cmdaddr",   2,1,8,""}, 
		{"chantype",  2,1,2,""},        
		{"channo",    2,1,7,""},
    {"msgtype",   2,1,7,""},
    {"inputitem0",5,0,0,""},
    {"inputitem1",5,0,0,""},
    {"inputitem2",5,0,0,""},
    {"inputitem3",5,0,0,""},
  },
},//
{/*1*/	"dbquery"	,5,	//查詢數據操作
				{					
					{"sessionid"	,2	,0		,0	,""    },         
					{"cmdaddr"		,2	,1		,8       ,""   }, 
					{"chantype"	,2	,1		,2       ,""    },        
					{"channo"		,2	,1		,7       ,""    },
					{"sqls"		,5	,0		,0       ,""    },        
				},
													},//
{/*2*/	"dbfieldno"	,5,	//根據字段序號取字段值操作
				{                   					
					{"sessionid"	,2	,0		,0    	,"" },          
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2       ,"" },         
					{"channo"		,2	,1		,7       ,"" }, 
					{"fieldno"		,2	,0		,0       ,"" }, 
				},
													},//
{/*3*/	"dbfieldname"	,5,	//根據字段名取字段值操作
				{					
					{"sessionid"	,2	,0		,0	,"" },          
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2       ,"" },         
					{"channo"		,2	,1		,7       ,"" }, 
					{"fieldname"	,5	,0		,0       ,"" },         
				},
													},//
{/*4*/	"dbfirst"	,4,	//記錄指針移到首記錄
				{                      					
					{"sessionid"	,2	,0		,0	,""},          
					{"cmdaddr"		,2	,1		,8       ,"" },
					{"chantype"	,2	,1		,2       ,"" },        
					{"channo"		,2	,1		,7       ,"" },
				},
													},//
{/*5*/	"dbnext"	,4,	//記錄指針移到下一記錄
				{                  					
					{"sessionid"	,2	,0		,0	,""},           
					{"cmdaddr"		,2	,1		,8       ,"" }, 
					{"chantype"	,2	,1		,2       ,"" },         
					{"channo"		,2	,1		,7       ,"" }, 
				},
													},//
{/*6*/	"dbprior"	,4,	//記錄指針移到前一記錄
				{                       					
					{"sessionid"	,2	,0		,0	,""  },           
					{"cmdaddr"		,2	,1		,8       ,""   }, 
					{"chantype"	,2	,1		,2       ,""   },         
					{"channo"		,2	,1		,7       ,""   }, 
				},
													},//
{/*7*/	"dblast"	,4,	//記錄指針移到末記錄
				{                          					
					{"sessionid"	,2	,0		,0	,""},          
					{"cmdaddr"		,2	,1		,8       ,"" },
					{"chantype"	,2	,1		,2       ,"" },        
					{"channo"		,2	,1		,7       ,"" },
				},        
													},//
{/*8*/	"dbgoto"	,5,	//記錄指針移到指定記錄
				{                				
					{"sessionid"	,2	,0		,0	,"" },          
					{"cmdaddr"		,2	,1		,8       ,"" }, 
					{"chantype"	,2	,1		,2       ,"" },         
					{"channo"		,2	,1		,7       ,"" }, 
					{"recordno"	,2	,0		,0	,"" },          
				},
													},//
{/*9*/	"dbinsert"	,5,	//插入記錄操作
				{                      					
					{"sessionid"	,2	,0		,0  	,"" },         
					{"cmdaddr"		,2	,1		,8       ,"" },
					{"chantype"	,2	,1		,2       ,"" },        
					{"channo"		,2	,1		,7       ,"" },
					{"sqls"		,5	,0		,0       ,"" },        
				},
													},//
{/*10*/	"dbupdate"	,5,	//修改記錄操作
				{              					
					{"sessionid"	,2	,0		,0	,""},         
					{"cmdaddr"		,2	,1		,8       ,""},
					{"chantype"	,2	,1		,2       ,""},        
					{"channo"		,2	,1		,7       ,""},
					{"sqls"		,5	,0		,0       ,""},        
				},
													},//
{/*11*/	"dbdelete"	,5,	//刪除記錄操作
				{					
					{"sessionid"	,2	,0		,0	,""},         
					{"cmdaddr"		,2	,1		,8       ,""},
					{"chantype"	,2	,1		,2       ,""},        
					{"channo"		,2	,1		,7       ,""},
					{"sqls"		,5	,0		,0       ,""},        
				},
													},//
{/*12*/	"writebill"	,14,	//寫計費話單
				{                					
					{"sessionid",		2,0,0,""},
					{"cmdaddr",			2,1,8,""},
					{"chantype",		2,1,2,""},
					{"channo",			2,1,7,""},
					{"deviceid",	  2,1,7,""},
					{"funcgroup",		2,1,7,""},
					{"funcno",			2,1,7,""},
					{"callerno",		5,0,0,""},
					{"calledno",		5,0,0,""},
					{"account",			5,0,0,""},
					{"starttime",		5,0,0,""},
					{"endtime",			5,0,0,""},
					{"callinout",		2,1,2,""},
					{"tollid",			2,1,2,""},
				},
													},//
{/*13*/	"dbsp"		,6,	//執行存儲過程操作 //edit 2008-09-30 change
				{                       					
					{"sessionid"	,2	,0		,0	,""},            
					{"cmdaddr"		,2	,1		,8       ,""},   
					{"chantype"	,2	,1		,2       ,""},           
					{"channo"		,2	,1		,7       ,""},   
					{"sp"		,5	,0		,0       ,""},           
					{"params"		,5	,0		,0       ,""},           
				},
													},//
{/*14*/	"dbclose"	,4,	//關閉查詢的數據表
				{                					
					{"sessionid"	,2	,0		,0	,""},         
					{"cmdaddr"		,2	,1		,8       ,""},
					{"chantype"	,2	,1		,2       ,""},        
					{"channo"		,2	,1		,7       ,""},
				},
													},//
  {/*15*/	"gettolltime"	,12,	//根據余額取最大通話時長
		{                					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"tollid",			2,1,2,""},
			{"funcgroup",		2,1,7,""},
			{"funcno",			2,1,7,""},
			{"callerno",		5,0,0,""},
			{"calledno",		5,0,0,""},
			{"account",			5,0,0,""},
			{"starttime",		5,0,0,""},
			{"balance",     3,0,0,""},
		},
	},//
  {/*16*/	"dbrecord"	,7,	//讀數據表當前記錄
		{                					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"recordno",		5,0,0,""},
			{"autogotonext",2,1,7,"1"},
			{"getidlist",		5,0,0,""},
		},
	},//
  {/*17*/	"dbspoutparam"	,5,	//取存儲過程輸出參數值
		{                					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"params",		  5,0,0,""},
		},
	},//
{/*18*/	"execsql"	,5,	//IVR執行SQL語句
				{					
					{"sessionid"	,2	,0		,0	,""},         
					{"cmdaddr"		,2	,1		,8       ,""},
					{"chantype"	,2	,1		,2       ,""},        
					{"channo"		,2	,1		,7       ,""},
					{"sqls"		,5	,0		,0       ,""},        
				},
													},//
  {/*19*/	"gwmakecallresult"	,9,	//向外部數據網關發送器發起呼叫的結果
		{                					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"gwserialno",  2,0,0,""}, //返回由外部數據網關發來的序列號
			{"callerno",    5,0,0,""},
			{"calledno",    5,0,0,""},
			{"result",		  2,1,7,""}, //呼叫結果
			{"param",		    5,0,0,""},
		},
	},//
  {/*20*/	"gwmsgeventresult",	7,	//向外部數據網關發送控制事件結果
		{                					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"gwserialno",  2,0,0,""}, //返回由外部數據網關發來的序列號
			{"result",		  2,1,7,""},
			{"param",		    5,0,0,""},
		},
	},//
	{/*21*/	"dbfieldtofile",		7,	//取查詢的字段值并存放到指定的文件
		{					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"fieldname",		5,0,0,""},
			{"pathname",		5,0,0,""},
			{"filename",		5,0,0,""},
		},
	},//
	{/*22*/	"dbfiletofield",		9,	//將文件內容存入數據表
		{					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"pathname",		5,0,0,""},
			{"filename",		5,0,0,""},
			{"tablename",		5,0,0,""},
			{"fieldname",		5,0,0,""},
			{"wheresql",		5,0,0,""},
		},
	},//
{/*23*/	"dbinsertex"	,5,	//插入記錄操作（針對長度>500的sql語句）
				{                      					
					{"sessionid"	,2	,0		,0  	,"" },         
					{"cmdaddr"		,2	,1		,8       ,"" },
					{"chantype"	,2	,1		,2       ,"" },        
					{"channo"		,2	,1		,7       ,"" },
					{"sqls"		,5	,0		,0       ,"" },        
				},
													},//
{/*24*/	"dbupdateex"	,5,	//修改記錄操作（針對長度>500的sql語句）
				{              					
					{"sessionid"	,2	,0		,0	,""},         
					{"cmdaddr"		,2	,1		,8       ,""},
					{"chantype"	,2	,1		,2       ,""},        
					{"channo"		,2	,1		,7       ,""},
					{"sqls"		,5	,0		,0       ,""},        
				},
													},//
{/*25*/	"dbqueryex"	,5,	//查詢數據操作（針對長度>500的sql語句）
				{					
					{"sessionid"	,2	,0		,0	,""    },         
					{"cmdaddr"		,2	,1		,8       ,""   }, 
					{"chantype"	,2	,1		,2       ,""    },        
					{"channo"		,2	,1		,7       ,""    },
					{"sqls"		,5	,0		,0       ,""    },        
				},
													},//
	{/*26*/	"querywebservice"	,11,	//執行查詢外部webservice接口數據
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"wsdladdress",     5,0,0,""},
	    {"soapaction",      5,0,0,""},
	    {"startenvelope",   5,0,0,""},
	    {"startbody",       5,0,0,""},
	    {"interfacestring", 5,0,0,""},
	    {"paramcount",      2,1,7,""},
	    {"paramstring",     5,0,0,""},
	  },
	},//
	{/*27*/	"getwebservicexml"	,7,	//從外部webservice接口查詢的XML串中取數據
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"noderoot",        5,0,0,""},
	    {"nodeindex",       2,1,7,""},
	    {"attrname",        5,0,0,""},
	  },
	},//
	{/*28*/	"getwebservicestr"	,7,	//從外部webservice接口查詢的字符串取數據
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"startmarker",     5,0,0,""},
	    {"endmarker",       5,0,0,""},
	    {"getindex",        2,1,7,""},
	  },
	},//
	{/*29*/	"getwebservicestr"	,4,	//關閉外部webservice接口
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	  },
	},//
	{/*30*/	"getwebserviceindex"	,7,	//從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"startmarker",     5,0,0,""},
	    {"endmarker",       5,0,0,""},
	    {"string",          5,0,0,""},
	  },
	},//
	{/*31*/	"httprequest"	,7,	//發送HTTP協議請求
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"httpurl",   			5,0,0,""},
	    {"httpparam",       5,0,0,""},
	    {"requesttype", 		5,0,0,""},
	  },
	},//
	{/*32*/	"shellexecute"	,8,	//執行外部程序
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"cmdname",   			5,0,0,""},
	    {"cmdparam",        5,0,0,""},
	    {"readfile",        5,0,0,""},
	    {"timeout", 		    2,1,7,""},
	  },
	},//
	{/*33*/	"gettxtlinestr"	,10,	//取文本行某段字符串
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"readfile",        5,0,0,""},
	    {"lineno", 		      2,1,7,""},
	    {"separator",       5,0,0,""},
	    {"fieldno", 		    2,1,7,""},
	    {"startpos", 		    2,1,7,""},
	    {"length", 		      2,1,7,""},
	  },
	},//
	{/*34*/	"socketrequest"	,9,	//通過socket通信發送查詢請求
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"serverip",   			5,0,0,""},
	    {"serverport",      2,0,0,""},
	    {"streamtype", 			5,0,0,""},
	    {"recvprotocol", 		5,0,0,""},
	    {"sendstring", 			5,0,0,""},
	  },
	},//
	{/*35*/	"dbrecorddump"	,8,	//數據表記錄轉存
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"sourdbid",   			2,1,6,""},
	    {"querysqls",      	5,0,0,""},
	    {"destdbid",   			2,1,6,""},
	    {"insertsqls",      5,0,0,""},
	  },
	},//
	{/*36*/	"imcallinresult"	,12,	//IM呼入分配結果
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
			{"result",      		2,1,6,""},
			{"seatno",      		5,0,0,""},
			{"workerno",    		2,1,7,""},
			{"groupno",     		2,1,7,""},
			{"queueno",     		2,1,7,""},
			{"imserialno",    	5,0,0,""},
			{"param",       		5,0,0,""},
			{"errorbuf",    		5,0,0,""},
	  },
	},//
	{/*37*/	"emcallinresult"	,12,	//email呼入分配結果
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
			{"result",      		2,1,6,""},
			{"seatno",      		5,0,0,""},
			{"workerno",    		2,1,7,""},
			{"groupno",     		2,1,7,""},
			{"queueno",     		2,1,7,""},
			{"emserialno",    	5,0,0,""},
			{"param",       		5,0,0,""},
			{"errorbuf",    		5,0,0,""},
	  },
	},//
	{/*38*/	"faxcallinresult"	,12,	//fax呼入分配結果
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
			{"result",      		2,1,6,""},
			{"seatno",      		5,0,0,""},
			{"workerno",    		2,1,7,""},
			{"groupno",     		2,1,7,""},
			{"queueno",     		2,1,7,""},
			{"fxserialno",    	5,0,0,""},
			{"param",       		5,0,0,""},
			{"errorbuf",    		5,0,0,""},
	  },
	},//
	{/*39*/	"querycustomer"	,8,	//通過webservice接口查詢客戶資料
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"callerno",     		5,0,0,""},
	    {"calledno",      	5,0,0,""},
	    {"webserviceaddr",  5,0,0,""},
	    {"param",     			5,0,0,""},
	  },
	},//
	{/*40*/	"httprequestex"	,8,	//發送HTTP協議請求擴展指令
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"httpurl",   			5,0,0,""},
	    {"httpparam",       5,0,0,""},
	    {"httpcookies",     5,0,0,""},
	    {"requesttype", 		5,0,0,""},
	  },
	},//
  {/*41*/	"dbselect"	,6,	//查詢數據表記錄擴展指令
		{                					
			{"sessionid",   2,0,0,""},         
			{"cmdaddr",     2,1,8,""},
			{"chantype",    2,1,2,""},        
			{"channo",      2,1,7,""},
			{"sqls",				5,0,0,""},
			{"getidlist",		5,0,0,""},
		},
	},//
	{/*42*/	"getwebservicejson"	,7,	//從外部webservice接口查詢的JSON串中取數據
		{					
			{"sessionid",       2,0,0,""},         
			{"cmdaddr",         2,1,8,""}, 
			{"chantype",        2,1,2,""},        
			{"channo",          2,1,7,""},
	    {"jsonnodetree",    5,0,0,""},
	    {"jsonnamelist",    5,0,0,""},
	    {"jsonfilter",    	5,0,0,""},
	  },
	},//
};

