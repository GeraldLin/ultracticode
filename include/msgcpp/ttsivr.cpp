
//TTS服務器-->IVR服務器
//MsgId	MsgName		AttrNum	Note

const VXML_RECV_MSG_CMD_RULE_STRUCT       TTSIVRMsgRuleArray[MAX_TTSIVRMSG_NUM]=
{
  //MsgId = 0 			功能描述：字符串合成結果
  //消息名					消息參數個數
	{"onttsstring",		7,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"sessionid", 2,	    0,		    0,	    1,		      1}, //會話序列號
			{"cmdaddr",   2,	    1,		    8,	    1,		      2}, //流程指令地址
			{"chantype",  2,	    1,		    2,	    1,		      3}, //通道類型
			{"channo",    2,	    1,		    7,	    1,		      4}, //通道號
			{"result",		2,			1,				6,			0,					0}, //結果
 			{"vocstream", 5,			0,				0,			0,					0}, //合成的語音流
 			{"errorbuf",	5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 0

  //MsgId = 1 			功能描述：文本文件合成結果
  //消息名					消息參數個數
	{"onttsfile",		4,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"sessionid", 2,	    0,		    0,	    1,		      1}, //會話序列號
			{"cmdaddr",   2,	    1,		    8,	    1,		      2}, //流程指令地址
			{"chantype",  2,	    1,		    2,	    1,		      3}, //通道類型
			{"channo",    2,	    1,		    7,	    1,		      4}, //通道號
			{"result",		2,			1,				6,			0,					0}, //結果
 			{"vocfile",	  5,			0,				0,			0,					0}, //合成的語音文件名
 			{"errorbuf",	5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 1
};
