//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//IVR服務器到Voice語音機的消息
const VXML_SEND_MSG_CMD_RULE_STRUCT       IVRVOPMsgRuleArray[MAX_IVRVOPMSG_NUM]=
{
	//0-語音節點注冊結果
	{"loginresult",				5,
		{
			{"sessionid",			2,	0,	0,	"" }, //會話序列號
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"result",				2,	1,	7,	"1"}, //注冊結果
			{"hosttype",			2,	1,	6,	"" }, //0-主用機，1-備用機
			{"activeid",			2,	1,	6,	"" }, //0-待機，1-啟用
		},
	},
	//1-增加內存放音索引
	{"addvocindex",			  4,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vocindex",			2,	1,	7,	"" }, //語音通道索引
			{"filename",			5,	0,	0,	"" }, //內存語音文件名
		},
	},
	//2-文件放音
	{"playfile",					10,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"filename",			5,	0,	0,	"" }, //語音文件名
			{"offset",				2,	1,	7,	"" }, //放音起始位置
			{"length",				2,	1,	7,	"" }, //放音長度
			{"param",					2,	1,	7,	"" }, //放音參數
			{"rule",					5,	0,	0,	"" }, //允許收碼串
		},
	},
	//3-內存索引放音
	{"playindex",					8,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"index",					2,	1,	7,	"" }, //內存索引號
			{"param",					2,	1,	7,	"" }, //放音參數
			{"rule",					5,	0,	0,	"" }, //允許收碼串
		},
	},
	//4-停止放音
	{"stopplay",				  6,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"stopparam",			2,	1,	6,	"" }, //停止標志
		},
	},
	//5-文件錄音
	{"recordfile",				11,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"filename",			5,	0,	0,	"" }, //錄音的文件名
			{"offset",				2,	1,	7,	"" }, //錄音文件起始位(0表示從文件頭開始 -1表示從文件尾開始)
			{"length",				2,	1,	7,	"" }, //錄音長度(0表示不限制長度)
			{"param",					2,	1,	7,	"" }, //錄音參數
			{"rule",					5,	0,	0,	"" }, //結束錄音按鍵
			{"phoneip",				5,	0,	0,	"" }, //IP話機的IP地址
		},
	},
	//6-內存錄音
	{"recordmem",					9,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"filename",			5,	0,	0,	"" }, //錄音的文件名
			{"length",				2,	1,	7,	"" }, //錄音長度(0表示不限制長度)
			{"param",					2,	1,	7,	"" }, //錄音參數
			{"rule",					5,	0,	0,	"" }, //結束錄音按鍵
		},
	},
	//7-停止錄音
	{"stoprecord",				7,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"stopparam",			2,	1,	6,	"" }, //停止標志
			{"phoneip",				5,	0,	0,	"" }, //IP話機的IP地址
		},
	},
	//8-發送傳真
	{"sendfax",						10,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"faxchindex",		2,	1,	7,	"0"}, //傳真資源通道號
			{"filename",			5,	0,	0,	"" }, //發送的傳真文件名(tiff格式）
			{"faxcsid",				5,	0,	0,	"" }, //傳真標識
			{"param",					2,	1,	7,	"0"}, //傳真參數
			{"rule",					5,	0,	0,	"" }, //傳真規則
		},
	},
	//9-接收傳真
	{"recvfax",						10,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"faxchindex",		2,	1,	7,	"0"}, //傳真資源通道號
			{"filename",			5,	0,	0,	"" }, //接收的傳真文件名(tiff格式）
			{"faxcsid",				5,	0,	0,	"" }, //傳真標識
			{"param",					2,	1,	7,	"0"}, //傳真參數
			{"rule",					5,	0,	0,	"" }, //傳真規則
		},
	},
	//10-停止收發傳真
	{"stopfax",						7,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"faxchindex",		2,	1,	7,	"0"}, //傳真資源通道號
			{"stopparam",			2,	1,	6,	"" }, //停止標志
		},
	},
	//11-發送DTMF
	{"senddtmf",					8,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"dtmfstr",			  5,	0,	0,	"" }, //DTMF
			{"param",					2,	1,	7,	"0"}, //傳真參數
			{"rule",					5,	0,	0,	"" }, //傳真規則
		},
	},
	//12-發送FSK
	{"sendfsk",						8,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"fskstr",			  5,	0,	0,	"" }, //DTMF
			{"param",					2,	1,	7,	"0"}, //傳真參數
			{"rule",					5,	0,	0,	"" }, //傳真規則
		},
	},
	//13-釋放通道
	{"release",						6,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"relparam",			2,	1,	6,	"" }, //釋放標志
		},
	},
	//14-發送FSK
	{"appendfax",					10,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"faxchindex",		2,	1,	7,	"0"}, //傳真資源通道號
			{"filename",			5,	0,	0,	"" }, //發送的傳真文件名(tiff格式）
			{"faxcsid",				5,	0,	0,	"" }, //傳真標識
			{"param",					2,	1,	7,	"0"}, //傳真參數
			{"rule",					5,	0,	0,	"" }, //傳真規則
		},
	},
	//15-發送ACM
	{"sendacm",						6,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"acmparam",			2,	1,	6,	"" }, //ACM參數
		},
	},
	//16-發送ACK
	{"sendack",						6,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"ackparam",			2,	1,	6,	"" }, //ACK參數
		},
	},
	//17-呼出
	{"callout",						10,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"callparam",			2,	1,	7,	"" }, //呼叫參數
			{"callflag",			2,	1,	7,	"" }, //呼叫標志
			{"callerno",			5,	0,	0,	"" }, //主叫號碼
			{"calledno",			5,	0,	0,	"" }, //被叫號碼
			{"orgcalledno",		5,	0,	0,	"" }, //原被叫號碼
		},
	},
	//18-呼出發送后續號碼
	{"sendsam",						9,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"callparam",			2,	1,	7,	"" }, //呼叫參數
			{"callflag",			2,	1,	7,	"" }, //呼叫標志
			{"callerno",			5,	0,	0,	"" }, //主叫號碼
			{"calledno",			5,	0,	0,	"" }, //被叫號碼
		},
	},
	//19-停止呼出
	{"stopcallout",				7,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"callparam",			2,	1,	7,	"" }, //呼叫參數
			{"callflag",			2,	1,	7,	"" }, //呼叫標志
		},
	},
	//20-模擬拍叉簧
	{"sendflash",				  6,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"flashparam",		2,	1,	6,	"" }, //FLASH參數
		},
	},
	//21-拍叉簧轉接電話
	{"transfer",					8,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"trantype",			2,	1,	6,	"" }, //轉接方式：1-協商轉接 2-快速轉接 3-會議轉接
			{"calledno",			5,	0,	0,	"" }, //被叫號碼
			{"trandata",			5,	0,	0,	"" }, //轉接附帶數據
		},
	},
	//22-停止拍叉簧轉接電話
	{"stoptransfer",			6,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"vopno",					2,	1,	6,	"1"}, //語音節點號
			{"vopchnno",			2,	1,	7,	"" }, //語音通道索引
			{"chantype",			2,	1,	6,	"" }, //邏輯通道類型
			{"channo",				2,	1,	7,	"" }, //邏輯通道號
			{"reason",			  2,	1,	6,	"" }, //停止原因 0-正常停止轉接 1-因為檢測到摘機而停止轉接
		},
	},
	//23-發送主備切換標志
	{"sendswitchover",		7,
		{
			{"sessionid",			2,	0,	0,	"" },
			{"switchid",			2,	1,	6,	"1"},
			{"hosttype",			2,	1,	6,	"" }, //本機類型：0-主機 1-備機
			{"masterstatus",	2,	1,	6,	"" }, //主機狀態：0-為待機狀態,1-為工作狀態,2-接管狀態
			{"standbystatus",	2,	1,	6,	"" }, //備機狀態：0-為待機狀態,1-為工作狀態,2-接管狀態
			{"reason",				2,	1,	6,	"" },
			{"reasonmsg",			5,	0,	0,	"" },
		},
	},
};
