//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//其他SRV服務程序-->CFG服務
const VXML_SEND_MSG_CMD_RULE_STRUCT       SRVCFGMsgRuleArray[MAX_SRVCFGMSG_NUM]=
{
	{/*0*/"login",			9,	//登錄
		{					
			{"nodetype", 		2,	1,	6,	""}, //登錄的服務節點類型
			{"nodeid",   		2,	1,	7,	""}, //登錄的服務節點編號
	  },
	},//
	{/*1*/"getiniparam",9,	//取INI參數
		{					
			{"nodetype", 		2,	1,	6,	""}, //登錄的服務節點類型
			{"nodeid",   		2,	1,	7,	""}, //登錄的服務節點編號
	  },
	},//
};

