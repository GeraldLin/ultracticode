//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1
												
const VXML_SEND_MSG_CMD_RULE_STRUCT       XMLIVRMsgRuleArray[MAX_XMLIVRMSG_NUM]=
{
{/*0*/	"login"		,6,	//節點注冊	,
				{
					{"sessionid"	,2	,0		,0	,"" },          
					{"nodetype"	,2	,1		,3	,"2"},          
					{"nodeno"		,2	,1		,4	,"1"},  
					{"name"		,5	,0		,0	,"" },          
					{"psw"		,5	,0		,0	,"" },          
					{"logid"		,2	,1		,1	,"" },  
				},      
													},
{/*1*/	"setaccessno"	,5,	//設置接入號碼
				{
					{"sessionid"	,2	,0		,0	,"" },	          
					{"settype"		,2	,1		,0	,"1"},    
					{"accessno"	,5	,0		,0       ,"" },           
					{"minlen"		,2	,1		,4	,"1"},    
					{"maxlen"		,2	,1		,4	,"1"},    
				},
													},
{/*2*/	"answer"	,5,	//來話應答
				{
					{"sessionid"	,2	,0		,0      	,""},    
					{"cmdaddr"		,2	,1		,8       ,""},   
					{"chantype"	,2	,1		,2       ,""},           
					{"channo"		,2	,1		,7       ,""},   
					{"answertype"	,2	,1		,0       ,""},           
				},
													},
{/*3*/	"sendsignal"	,7,	//發送信令
				{
					{"sessionid"	,2	,0		,0	,""},               
					{"cmdaddr"		,2	,1		,8       ,""},      
					{"chantype"	,2	,1		,2       ,""},              
					{"channo"		,2	,1		,7       ,""},      
					{"signaltype"	,2	,1		,6       ,""},              
					{"signal"		,2	,1		,6    	,""},       
					{"cause"		,3	,1		,7       ,""},      
				},
													},
{/*4*/	"hangon"	,4,	//掛機釋放
				{
					{"sessionid"	,2	,0		,0	,""},           			
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2	,""},           
					{"channo"		,2	,1		,7       ,""},  
				},
													},
{/*5*/	"dtmfrule"	,13,	//DTMF按鍵規則定義
				{
					{"sessionid"	,2	,0		,0	,""},                         
					{"cmdaddr"		,2	,1		,8       ,""},                
					{"chantype"	,2	,1		,2	,""},                         
					{"channo"		,2	,1		,7       ,""},                
					{"ruleid"		,2	,1		,6	,"1"   },             
					{"digits"		,5	,3		,2	,"0123456789*#"},     
					{"break"		,2	,1		,0	,"1"},                
					{"transtype"	,2	,1		,0	,"0"     },                   
					{"minlength"	,2	,1		,4	,"1"},                        
					{"maxlength"	,2	,1		,4	,"1"    },                    
					{"timeout"		,2	,1		,7	,"5"},                
					{"termdigits"	,5	,3		,2	,"#"},                        
					{"termid"		,2	,1		,0	,"1"  },              
				},
													},
{/*6*/	"getdtmf"	,6	,//收碼結果
				{
					{"sessionid"	,2	,0		,0      	,""},   
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2	,""},           
					{"channo"		,2	,1		,7       ,""},  
					{"dtmfruleid"	,2	,1		,6	,"0"},          
					{"asrruleid"	,2	,1		,6	,"0"},          
				},
													},
{/*7*/	"playrule"	,14,	//放音規則定義
				{
					{"sessionid"	,2	,0		,0	,""},            
					{"cmdaddr"		,2	,1		,8       ,""},   
					{"chantype"	,2	,1		,2	,""},            
					{"channo"		,2	,1		,7       ,""},   
					{"ruleid"		,2	,1		,6	,"1"},   
					{"format"		,2	,4		,4	,"1"},   
					{"repeat"		,2	,1		,6	,"1"},   
					{"duration"	,2	,1		,7	,"0"},           
					{"broadcast"	,2	,1		,0	,"0"  },         
					{"volume"		,2	,0		,0	,"255"}, 
					{"speech"		,2	,4		,2	,"2"},   
					{"voice"		,2	,4		,3	,"1"},   
					{"pause"		,2	,1		,6	,"3"},   
					{"error"		,2	,1		,0	,"1"},   
				},
													},
{/*8*/	"asrrule"	,6	,//ASR規則定義
				{
					{"sessionid"	,2	,0		,0	,""},           
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2	,""},           
					{"channo"		,2	,1		,7       ,""},  
					{"ruleid"		,2	,1		,6	,"1"},  
					{"grammar"		,5	,0		,0       ,"" }, 
				},
													},
{/*9*/	"setvolume"	,5	,//改變放音音量
				{
					{"sessionid"	,2	,0		,0	,""},          
					{"cmdaddr"		,2	,1		,8       ,""}, 				
					{"chantype"	,2	,1		,2	,""},          
					{"channo"		,2	,1		,7       ,""}, 
					{"volume"		,5	,0		,0	,"0"}, 
				},
													},
{/*10*/	"playfile"	,10,	//單文件放音結果
				{
					{"sessionid"	,2	,0		,0	,""},            
					{"cmdaddr"		,2	,1		,8       ,""},   
					{"chantype"	,2	,1		,2	,""},            
					{"channo"		,2	,1		,7       ,""},   
					{"dtmfruleid"	,2	,1		,6	,"0"},           
					{"playruleid"	,2	,1		,6	,"0"},           
					{"asrruleid"	,2	,1		,4	,"0"},           
					{"filename"	,5	,0		,0       ,"" },          
					{"startpos"	,2	,0		,0	,"0"},           
					{"length"		,2	,0		,0	,"0"},   
				},
													},
{/*11*/	"setplaypos"	,5	,//改變放音指針位置
				{					
					{"sessionid"	,2	,0		,0	,""   },          
					{"cmdaddr"		,2	,1		,8       ,""   }, 
					{"chantype"	,2	,1		,2	,""   },          
					{"channo"		,2	,1		,7       ,""   }, 
					{"pos"		,2	,0		,0	,"0"  },          
				},
													},
{/*12*/	"playfiles"	,10,	//多文件放音
				{
					{"sessionid"	,2	,0		,0	,"" },         
					{"cmdaddr"		,2	,1		,8       ,"" },
					{"chantype"	,2	,1		,2	,"" },         
					{"channo"		,2	,1		,7       ,"" },
					{"dtmfruleid"	,2	,1		,6	,"0"},         
					{"playruleid"	,2	,1		,6	,"0"},         
					{"asrruleid"	,2	,1		,6	,"0"},         
					{"filename1"	,5	,0		,0       ,"" },        
					{"filename2"	,5	,0		,0	,"" },         
					{"filename3"	,5	,0		,0       ,"" },        
				},
													},
{/*13*/	"playcompose"	,9	,//合成串放音
				{
					{"sessionid"	,2	,0		,0	,"" },          
					{"cmdaddr"		,2	,1		,8       ,"" }, 
					{"chantype"	,2	,1		,2	,"" },          
					{"channo"		,2	,1		,7       ,"" }, 
					{"dtmfruleid"	,2	,1		,6	,"0"},          
					{"playruleid"	,2	,1		,6	,"0"},          
					{"asrruleid"	,2	,1		,6	,"0"},          
					{"comptype"	,2	,4		,5       ,"" },         
					{"string"		,5	,0		,0	,"" },  
				},
													},
{/*14*/	"multiplay"	,13,	//多語音合成放音
				{
					{"sessionid"	,2	,0		,0	,"" },           
					{"cmdaddr"		,2	,1		,8       ,"" },  
					{"chantype"	,2	,1		,2	,"" },           
					{"channo"		,2	,1		,7       ,"" },  
					{"dtmfruleid"	,2	,1		,6	,"0"},           
					{"playruleid"	,2	,1		,6	,"0"},           
					{"asrruleid"	,2	,1		,6	,"0"},           
					{"mixtype1"	,2	,4		,5       ,"" },          
					{"content1"	,5	,0		,0	,"" },           
					{"mixtype2"	,2	,4		,5       ,"" },          
					{"content2"	,5	,0		,0	,""},            
					{"mixtype3"	,2	,4		,5       ,""},           
					{"content3"	,5	,0		,0       ,""},           
				},
													},
{/*15*/	"ttsstring"	,13	,//文本串轉語音
				{
					{"sessionid", 2, 0, 0,""},          
					{"cmdaddr",   2, 1, 8,""},  
					{"chantype",  2, 1, 2,""},           
					{"channo",    2, 1, 7,""}, 
					{"dtmfruleid",2, 1, 6,"0"},          
					{"playruleid",2, 1, 6,"0"},          
					{"asrruleid", 2, 1, 6,"0"},          
					{"ttstype",  5, 0, 0,"" }, 
					{"ttsgrammar",   5, 0, 0,"" }, 
					{"ttsparam",   5, 0, 0,"" }, 
					{"ttsformat",  2, 4, 4,"1" }, 
					{"ttsfilename",5, 0, 0,"" },  
					{"string",    5, 0, 0,"" },  
				},
													},
{/*16*/	"ttsfile"	,13	,//文本文件轉語音
				{
					{"sessionid"	,2	,0		,0	,""},          
					{"cmdaddr"		,2	,1		,8       ,""}, 
					{"chantype"	,2	,1		,2	,""},          
					{"channo"		,2	,1		,7       ,"" },
					{"dtmfruleid"	,2	,1		,6	,"0"},         
					{"playruleid"	,2	,1		,6	,"0"},         
					{"asrruleid"	,2	,1		,6	,"0"},         
					{"ttstype",  5, 0, 0,"" }, 
					{"ttsgrammar",   5, 0, 0,"" }, 
					{"ttsparam",   5, 0, 0,"" }, 
					{"ttsformat",  2, 4, 4,"1" }, 
					{"ttsfilename",5, 0, 0,"" },  
					{"filename"	,5	,0		,0	,"" },         
				},
													},
{/*17*/	"senddtmf"	,7	,//發送DTMF
				{
					{"sessionid"	,2	,0		,0	,""},            
					{"cmdaddr"		,2	,1		,8       ,""},   
					{"chantype"	,2	,1		,2    	,""},            			
					{"channo"		,2	,1		,7       ,""  }, 
					{"duration"	,2	,1		,7	,"50"},          
					{"pause"		,2	,1		,6	,"50"},  
					{"dtmfs"		,5	,3		,1	,""  },  
				},
													},
{/*18*/	"sendfsk"	,9	,//發送FSK
				{				
					{"sessionid"	,2	,0		,0	,""},           
					{"cmdaddr"		,2	,1		,8  ,""},  
					{"chantype"	  ,2	,1		,2  ,""},   
					{"channo"		  ,2	,1		,7  ,""},  
					{"fskheader"	,2	,1		,6	,""},
					{"fskcmd"		  ,2	,1		,6	,""},
					{"fskdata"		,5	,0		,0	,""},           
					{"fskdatalen"	,2	,1		,7	,""},
					{"crctype"		,2	,1		,6	,""},
				},
													},
{/*19*/	"playcfc"	,9	,//會議室放音
				{
					{"sessionid"	,2	,0		,0	,"" },            
					{"cmdaddr"		,2	,1		,8       ,"" },   
					{"chantype"	,2	,1		,2	,"" },            
					{"channo"		,2	,1		,7       ,"" },   
					{"playruleid"	,2	,1		,4	,"0"},            
					{"cfcno"		,2	,1		,7       ,""   }, 
					{"filename1"	,5	,0		,0	,""   },          
					{"filename2"	,5	,0		,0       ,""   },         
					{"filename3"	,5	,0		,0       ,""   },         
				},
													},
{/*20*/	"playtone"	,8 ,	//播放信號音
				{
					{"sessionid"	,2	,0		,0	,"" },          
					{"cmdaddr"		,2	,1		,8       ,"" }, 
					{"chantype"	,2	,1		,2	,"" },          
					{"channo"		,2	,1		,7       ,"" }, 
					{"dtmfruleid"	,2	,1		,6	,"0"},          
					{"tonetype"	,2	,1		,6       ,"" },         
					{"repeat"		,2	,1		,6	,"0"},  
					{"release"		,2	,1		,0	,"0"},  
				},
													},
{/*21*/	"recordfile"	,10,	//錄音
				{
					{"sessionid"	,2	,0		,0	,"" },          
					{"cmdaddr"		,2	,1		,8       ,"" }, 
					{"chantype"	,2	,1		,2	,"" },          
					{"channo"		,2	,1		,7       ,"" }, 
					{"filename"	,5	,0		,0     	,""  },         
					{"beep"		,2	,1		,0	,"1" },         
					{"maxtime"		,2	,1		,6	,"30"}, 
					{"termchar"	,5	,2		,1	,"#" },         
					{"format"		,2	,4		,4	,"1" }, 
					{"writemode"	,2	,4		,19	,"0" },         
				},      
													},
{/*22*/	"recordcfc"	,9	,//監視錄音
				{
					{"sessionid"	,2	,0		,0      	,""  },   
					{"cmdaddr"		,2	,1		,8       ,""  },  
					{"chantype"	,2	,1		,2	,""},             
					{"channo"		,2	,1		,7       ,""},    
					{"cfcno"	,2	,1		,7	,""    },         
					{"filename"	,5	,0		,0       ,""    },        
					{"maxtime"		,2	,1		,6	,"30"},   
					{"format"		,2	,4		,4	,"1" },   
					{"writemode"	,2	,4		,19	,"0"  },          
				},
													},
{/*23*/	"stop"		,4	,//停止放音錄音收碼
				{       				
					{"sessionid"	,2	,0		,0	,""},           
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2	,""},           
					{"channo"		,2	,1		,7       ,""},  
				},
													},
{/*24*/	"callout"	,22,	//電話呼出
				{
					{"sessionid",		2,0,0,""},
					{"cmdaddr",			2,1,8,""},
					{"chantype",		2,1,2,""},
					{"channo",			2,1,7,""},
					{"serialno",		2,1,6,"1"},
					{"callertype",	2,1,7,"0"},
					{"calltype",		2,1,7,"0"},
					{"callerno",		5,0,0,""},
					{"calledno",		5,0,0,""},
					{"waittime",		2,1,6,"60"},
					{"routeno",			5,0,0,"0"},
					{"vxmlid",			2,1,4,"1"},
					{"funcno",			2,1,4,"0"},
					{"times",				2,1,6,"1"},
					{"sessionno",		2,0,0,""},
					{"param",				5,0,0,""},
					{"outchntype",	2,1,6,"0"},
					{"outchnno",    2,1,7,"0"},
					{"callmode",		2,1,6,"0"},
					{"callpoint",		2,1,6,"0"},
					{"callinterval",2,1,6,"0"},
					{"calledtype",  5,0,0,"1"},
				},
},
{/*25*/	"cancelcallout"	,5	,//取消呼出
				{              					
					{"sessionid"	,2	,0		,0	,""},          
					{"cmdaddr"		,2	,1		,8       ,""}, 
					{"chantype"	,2	,1		,2	,""},          
					{"channo"		,2	,1		,7       ,""}, 
					{"sessionno"	,2	,0		,0	,""},          
				},
													},//
{/*26*/	"sendsam"	,6	,//發送被叫號碼
				{                       				
					{"sessionid"	,2	,0		,0	,""   },         
					{"cmdaddr"		,2	,1		,8       ,""   },
					{"chantype"	,2	,1		,2	,""   },         
					{"channo"		,2	,1		,7       ,""   },
					{"calledno"	,5	,0		,0	,""   },         
					{"sessionno"	,2	,0		,0       ,""},           
				},
													},//
{/*27*/	"transfer",			19,	//電話轉接
				{
					{"sessionid",		2,0,0,""},  //0
					{"cmdaddr",			2,1,8,""},  //1
					{"chantype",		2,1,2,""},  //2
					{"channo",			2,1,7,""},  //3
					{"sourcechntype",2,1,6,"0"},//4
					{"sourcechnno",	2,1,7,"0"}, //5
					{"tranmode",		2,1,2,"1"}, //6 轉接類型:0-板卡版本拍叉轉接 1-協商轉接 2-盲轉 3-會議轉接 4-轉接外部路由 5-流程發起坐席轉接電話指令(2016-01-17) 6-流程發起坐席停止轉接電話指令
					{"callertype",	2,1,6,"0"}, //7
					{"calledtype",	2,1,6,"1"}, //8 轉接的號碼類型:1-外線號碼 2-坐席號(判斷是否存在) 3-話務員工號 4-分機號(不判斷是否存在)
					{"calltype",		2,1,6,"0"}, //9 當tranmode=0時：1-協商轉接，2-快速轉接
					{"callerno",		5,0,0,""},  //10 當tranmode=5,6時表示為發起轉接的分機號碼(2016-01-17)
					{"calledno",		5,0,0,""},  //11 當tranmode=4時，表示為外部路由VDN，為空時表示按原路由返回
					{"waittime",		2,1,6,"30"},//12
					{"routeno",			2,1,6,"1"}, //13
					{"outchntype",	2,1,6,"0"}, //14
					{"outchnno",		2,1,7,"0"}, //15
					{"waitvoc",			5,0,0,""},  //16
					{"param",				5,0,0,""},  //17
					{"sessionno",		2,0,0,""},  //18
				},
													},//
{/*28*/	"callseat"	,26,	//呼叫坐席
				{
					{"sessionid",	2	,0	,0	,""	}, //0
					{"cmdaddr",		2	,1	,8  ,"" }, //1
					{"chantype",	2	,1	,2	,"" }, //2
					{"channo",		2	,1	,7  ,"" }, //3
					{"callerno",	5	,0	,0	,"" }, //4
					{"calledno",	5	,0	,0  ,"" }, //5
					{"seatno",		5	,0	,0	,"0"}, //6
					{"workerno",	2	,0	,0	,"0"}, //7
					{"seattype",	2	,1	,6	,"0"}, //8
					{"acdrule",		2	,1	,6	,"2"}, //9
					{"callmode",	2	,1	,6	,"0"}, //10
					{"groupno",		5	,0	,0	,"1"}, //11
					{"calltype",	2	,1	,7	,"1"}, //12
					{"seatgroupno",2,0	,0	,"0"}, //13
					{"level",			5	,0	,0	,"1"}, //14
					{"levelrule",	2	,1	,6	,"0"}, //15
					{"waittime",	2	,1	,6	,"60"},//16
					{"ringtime",	2	,1	,6	,"30"},//17
					{"vxmlid",		2	,1	,4	,"1"}, //18
					{"funcno",		2	,1	,4	,"0"}, //19
					{"busywaitid",5	,0	,0	,"1"}, //20
					{"priority",	2	,1	,6	,"0"}, //21
					{"servicetype",5,0	,0	,""	}, //22
					{"cdrserialno",5, 0	,0	,""	}, //23
					{"param",			5	,0	,0	,""	}, //24
					{"sessionno",	2	,0	,0  ,"" }, //25
				},
													},//
{/*29*/	"stopcallseat"	,5	,//停止呼叫坐席
				{
					{"sessionid"	,2	,0		,0	,""     	},   			
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2	,""      },          
					{"channo"		,2	,1		,7       ,""      }, 
					{"sessionno"	,2	,0		,0	,""      },          
				},
													},//
{/*30*/	"createcfc"	,13,	//創建會議
				{
					{"sessionid"	,2	,0		,0	,""      },  	     
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2	,""      },          
					{"channo"		,2	,1		,7       ,""      }, 
					{"cfcno"		,2	,1		,7	,""      },  
					{"name"		,5	,0		,0       ,""	},           
					{"owner"		,5	,0		,0	,""      },  
					{"presiders"	,2	,1		,6	,"1"     },          
					{"talkers"		,2	,1		,6	,"5"     },  
					{"listeners"	,2	,1		,7	,"30"    },          
					{"password"	,5	,0		,0	,""	},           
					{"autorecord"	,2	,0		,0	,"0"     },          
					{"autofree"	,2	,0		,0	,"0"     },          
				},
													},//
{/*31*/	"destroycfc"	,6	,//釋放會議資源
				{                     					
					{"sessionid"	,2	,0		,0	,""	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2	,""      },         
					{"channo"		,2	,1		,7       ,""      },
					{"cfcno"		,2	,1		,7	,""      }, 
					{"destroyid"		,2	,1		,7	,"0"      }, 
				},
													},//
{/*32*/	"joincfc"	,8	,//加入會議
				{				
					{"sessionid"	,2	,0		,0	,""	},            
					{"cmdaddr"		,2	,1		,8       ,""      },  
					{"chantype"	,2	,1		,2	,""      },           
					{"channo"		,2	,1		,7       ,""      },  
					{"cfcno"		,2	,1		,7	,""      },   
					{"jointype"	,2	,4		,10      ,""      },	      
					{"knocktone"	,2	,1		,3	,"1"     },           
					{"emptyevent"	,2	,1		,7	,"0"     },           
				},
													},//
{/*33*/	"unjoincfc"	,5	,//退出會議
				{					
					{"sessionid"	,2	,0		,0	,""     	},   
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2	,""      },          
					{"channo"		,2	,1		,7       ,""      }, 
					{"cfcno"		,2	,1		,7     	,""      },  
				},
													},//

{/*34*/	"link"		,7	,//通道交換
				{
					{"sessionid"	,2	,0		,0	,""	},           
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2	,""      },          
					{"channo"		,2	,1		,7       ,""      }, 
					{"linkchantype"	,2	,1		,2    	,""      },          
					{"linkchanno"	,2	,1		,7       ,""      },	     
					{"linkmode"	,2	,4		,7	,""      },          
				},
													},//
{/*35*/	"talkwith"	,6,	//與對方通話
				{
					{"sessionid"	,2	,0		,0    	,""     	},  
					{"cmdaddr"		,2	,1		,8     	,""      }, 
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"linkchantype"	,2	,1		,2       ,""      },        
					{"linkchanno"	,2	,1		,7       ,""      },        
				},
													},//
{/*36*/	"listenfrom"	,6,	//監聽對方通話
				{
					{"sessionid"	,2	,0		,0	,""     	},  
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"linkchantype"	,2	,1		,2       ,""      },        
					{"linkchanno"	,2	,1		,7       ,""      },        
				},
													},//
{/*37*/	"inserttalk"	,6,	//強插對方通話
				{
					{"sessionid"	,2	,0		,0	,""	},           
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2       ,""      },         
					{"channo"		,2	,1		,7       ,""      }, 
					{"linkchantype"	,2	,1		,2       ,""      },         
					{"linkchanno"	,2	,1		,7       ,""      },         
				},
													},//
{/*38*/	"threetalk"	,8,	//三方通話
				{
					{"sessionid"	,2	,0		,0	,""	},            
					{"cmdaddr"		,2	,1		,8       ,""      },  
					{"chantype"	,2	,1		,2       ,""      },          
					{"channo"		,2	,1		,7       ,""      },  
					{"linkchantype1"	,2	,1		,2       ,""      },  
					{"linkchanno1"	,2	,1		,7       ,""      },          
					{"linkchantype2"	,2	,1		,2    	,""      },   
					{"linkchanno2"	,2	,1		,7       ,""      },          
				},
													},//
{/*39*/	"stoptalk"	,4,	//結束通話
				{                        					
					{"sessionid"	,2	,0		,0	,""     	},   
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2	,""      },          
					{"channo"		,2	,1		,7       ,""      }, 
				},
													},//
{/*40*/	"sendfax"	,12,	//發送傳真
				{
					{"sessionid",		2,	0,	0,	""},          
					{"cmdaddr",			2,	1,	8,	""},
					{"chantype",		2,	1,	2,	""},         
					{"channo",			2,	1,	7,	""},
					{"faxcsid",			5,	0,	0,	""}, //4 本端傳真標識號
					{"resolution",	5,	0,	0,	""}, //5 為空時則采用默認分辨率，格式為：X:分辨率數值;Y:分辨率數值
					{"totalpages",	2,	1,	6,	"0"},//6 總頁數
					{"pagelist",		5,	0,	0,	""}, //7 發送的頁碼范圍，為空時表示發送所有頁面，格式為：a-z (a表示起始頁號，z表示終止頁號)
					{"faxheader",		5,	0,	0,	""}, //8 添加的頁眉，為空時則不添加頁眉，格式為：XPos=起始X坐標;YPos=起始Y坐標;FontName:字體名稱;FontSize:字體大小;From:發件人;Subject:主題;To:收件人;Time:發送時間;website:公司網址;
					{"faxfooter",		5,	0,	0,	""}, //9 添加的頁腳，為空時則不添加頁腳，格式為：XPos=起始X坐標;YPos=起始Y坐標;FontName:字體名稱;FontSize:字體大小;From:發件人;Subject:主題;To:收件人;Time:發送時間;website:公司網址;
					{"barcode",		  5,	0,	0,	""}, //10 是否加注條碼
					{"filename",		5,	0,	0,	""}, //11 傳真文件名
				},
													},//
{/*41*/	"recvfax"	,7,	//接收傳真
				{
					{"sessionid",		2,	0,	0,	""},           
					{"cmdaddr",			2,	1,	8,	""}, 
					{"chantype",		2,	1,	2,	""},          
					{"channo",			2,	1,	7,	""}, 
					{"faxcsid",			5,	0,	0,	""}, //本端傳真標識號
					{"barcode",			5,	0,	0,	""}, //加注的條碼，不為空且收到的傳真無條碼時增加的條碼
					{"filename",		5,	0,	0,	""}, //發送的傳真文件名
				},
													},//
{/*42*/	"checkpath"	,5,	//檢查文件路徑
				{					
					{"sessionid"	,2	,0		,0	,""    	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2	,""      },         
					{"channo"		,2	,1		,7       ,""      },
					{"path"		,5	,0		,0	,""      },         
				},
													},//
{/*43*/	"createpath"	,5,	//創建文件路徑
				{					
					{"sessionid"	,2	,0		,0    	,""	},            
					{"cmdaddr"		,2	,1		,8       ,""      },  
					{"chantype"	,2	,1		,2       ,""      },          
					{"channo"		,2	,1		,7       ,""      },  
					{"path"		,5	,0		,0       ,""      },          
				},
													},//
{/*44*/	"deletepath"	,5,	//刪除文件路徑
				{					
					{"sessionid"	,2	,0		,0	,""	},           
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2       ,""      },         
					{"channo"		,2	,1		,7       ,""      }, 
					{"path"		,5	,0		,0       ,""      },         
				},
													},//
{/*45*/	"getfilenum"	,7,	//取文件個數
				{
					{"sessionid"	,2	,0		,0	,""	},          
				        {"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"path"		,5	,0		,0       ,""      },        
					{"filespec"	,5	,0		,0	,""      },	    
					{"maxfiles"	,2	,1		,7       ,""      },        
				},
													},//
{/*46*/	"getfilename"	,7,	//取文件名
				{
					{"sessionid"	,2	,0		,0     	,""    	},           
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2       ,""      },         
					{"channo"		,2	,1		,7       ,""      }, 
					{"path"		,5	,0		,0       ,""      },         
					{"filespec"	,5	,0		,0       ,""      },         
					{"maxfiles"	,2	,1		,7       ,""      },         
				},
													},//
{/*47*/	"renname"	,6,	//改文件名
				{
					{"sessionid"	,2	,0		,0	,""	},           
					{"cmdaddr"		,2	,1		,8       ,""      }, 
					{"chantype"	,2	,1		,2       ,""      },         
					{"channo"		,2	,1		,7       ,""      }, 
					{"oldfilename"	,5	,0		,0       ,""      },         
					{"newfilename"	,5	,0		,0       ,""      },         
				},
													},//
{/*48*/	"copyfile"	,6,	//拷貝文件
				{
					{"sessionid"	,2	,0		,0	,""	 },           
					{"cmdaddr"		,2	,1		,8       ,""       }, 
					{"chantype"	,2	,1		,2       ,""       },         
					{"channo"		,2	,1		,7       ,""       }, 
					{"soufilename"	,5	,0		,0       ,""       },         
					{"desfilename"	,5	,0		,0       ,""       },         
				},
													},//
{/*49*/	"deletefile"	,5,	//刪除文件
				{                        					
					{"sessionid"	,2	,0		,0  	,""	},               
					{"cmdaddr"		,2	,1		,8       ,""      },     
					{"chantype"	,2	,1		,2       ,""      },             
					{"channo"		,2	,1		,7       ,""      },     
					{"filename"	,5	,0		,0       ,""      },             
				},
													},//
{/*50*/	"checkfile"	,5,	//檢查文件是否存在
				{                       				
					{"sessionid"	,2	,0		,0	,""     	},       
					{"cmdaddr"		,2	,1		,8       ,""      },     
					{"chantype"	,2	,1		,2       ,""      },             
					{"channo"		,2	,1		,7       ,""      },     
					{"filename"	,5	,0		,0       ,""      },             
				},
													},//
{/*51*/	"clearmixer"	,4,	//清除放音合成緩沖區
				{                       					
					{"sessionid"	,2	,0		,0    	,""    	},             
					{"cmdaddr"		,2	,1		,8       ,""      },   
					{"chantype"	,2	,1		,2       ,""      },           
					{"channo"		,2	,1		,7       ,""      },   
				},
													},//
{/*52*/	"addfile"	,5,	//增加文件到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0  	,""	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"filename"	,5	,0		,0       ,""	},          
				},
													},//
{/*53*/	"adddatetime"	,5,	//增加日期時間到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0	,""	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"datetime"	,5	,0		,0       ,""      },        
				},
													},//
{/*54*/	"addmoney"	,5,	//增加金額到放音合成緩沖區
				{                            					
					{"sessionid"	,2	,0		,0	,""	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"money"	,5	,0		,0       ,""      },        
				},
													},//
{/*55*/	"addnumber"	,5,	//增加數值到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0	,""	},              
					{"cmdaddr"		,2	,1		,8       ,""      },    
					{"chantype"	,2	,1		,2       ,""      },            
					{"channo"		,2	,1		,7       ,""      },    
					{"number"		,5	,0		,0       ,""      },    
				},
													},//
{/*56*/	"adddigits"	,5,	//增加數字串到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0	,""	},             					
					{"cmdaddr"		,2	,1		,8       ,""      },   
					{"chantype"	,2	,1		,2       ,""      },           
					{"channo"		,2	,1		,7       ,""      },   
					{"digits"		,5	,0		,0       ,""      },   
				},
													},//
{/*57*/	"addchars"	,5,	//增加字母串到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0	,""	},              
					{"cmdaddr"		,2	,1		,8       ,""      },    
					{"chantype"	,2	,1		,2       ,""      },            
					{"channo"		,2	,1		,7       ,""      },    
					{"chars"		,5	,0		,0       ,""      },    
				},
													},//
{/*58*/	"playmixer"	,7,	//播放合成緩沖區
				{
					{"sessionid"	,2	,0		,0	,""	},                 
					{"cmdaddr"		,2	,1		,8       ,""      },       
					{"chantype"	,2	,1		,2       ,""      },               
					{"channo"		,2	,1		,7       ,""      },       
					{"dtmfruleid"	,2	,1		,6	,"0"	},                 
					{"playruleid"	,2	,1		,6	,"0"     },                
					{"asrruleid"	,2	,1		,6	,"0"     },                
				},
													},//
{/*59*/	"addttsstr"	,5,	//增加TTS字符串到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0    	,""    	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"string"		,5	,0		,0       ,""      },
				},
													},//
{/*60*/	"addttsfile"	,5,	//增加TTS文件到放音合成緩沖區
				{					
					{"sessionid"	,2	,0		,0	,""	},          
					{"cmdaddr"		,2	,1		,8       ,""      },
					{"chantype"	,2	,1		,2       ,""      },        
					{"channo"		,2	,1		,7       ,""      },
					{"filename"	,5	,0		,0       ,""      },        
				},
													},//
{/*61*/	"dialout"	,8,	//撥號器呼出
				{
					{"sessionid"	,2	,0		,0	,""	},         
					{"callerno"	,5	,0		,0       ,""      },       
					{"calledno"	,5	,0		,0       ,""      },       
					{"waittime"	,2	,1		,6	,"60"    },        
					{"routeno"		,2	,1		,6	,"0"     },
					{"vxmlid"		,2	,1		,4	,"1"	}, 
					{"funcno"		,2	,1		,4	,"0"     },
					{"param"		,5	,0		,0	,""      },
				},
													},//
{/*62*/	"flash"		,4,	//模擬外線拍叉簧
				{               					
					{"sessionid"	,2	,0		,0   	,"" },          
					{"cmdaddr"		,2	,1		,8       ,""},  
					{"chantype"	,2	,1		,2       ,"" },         
					{"channo"		,2	,1		,7       ,"" }, 
				},
													},//
  {/*63*/	"setvcparam", 9,	//設置變聲參數
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"vcmode",    2,1,3,"1"},
			{"vcparam",   2,1,6,"0"}, 
			{"vcmix",     2,1,6,"0"},
			{"noisemode", 2,1,6,"0"},
			{"noiseparam",5,0,0,"0"},
		},
	},//
  {/*64*/	"pause"		,4	,//暫停放音
		{       				
			{"sessionid", 2,0,0,""},
			{"cmdaddr",   2,1,8,""},
			{"chantype",  2,1,2,""},
			{"channo",    2,1,7,""},
		},
  },
  {/*65*/	"replay"  ,4	,//重新放音
		{       				
			{"sessionid", 2,0,0,""},
			{"cmdaddr",   2,1,8,""},
			{"chantype",  2,1,2,""},
			{"channo",    2,1,7,""},
		},
  },
  {/*66*/	"fastplay"  ,6	,//快速放音
		{       				
			{"sessionid", 2,0,0,""},
			{"cmdaddr",   2,1,8,""},
			{"chantype",  2,1,2,""},
			{"channo",    2,1,7,""},
			{"direction", 2,1,0,"0"},
			{"length",    2,1,7,""},
		},
  },
  {/*67*/	"checktone"  ,6	,//檢測信號音
		{       				
			{"sessionid", 2,0,0,""},
			{"cmdaddr",   2,1,8,""},
			{"chantype",  2,1,2,""},
			{"channo",    2,1,7,""},
			{"tonetype",  2,1,6,"0"},
			{"toneparam", 5,0,0,""},
		},
  },
  {/*68*/	"workerlogin", 11,	//話務員登錄消息
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" },
			{"seatno",    5,0,0,"" }, 
			{"workerno",  2,0,0,"" },
			{"workername",5,0,0,"" },
			{"password",  5,0,0,"" },
			{"groupno",   2,1,6,"1"},
			{"class",     2,1,6,"1"},
			{"level",     2,1,6,"1"},
		},
	},//
  {/*69*/	"workerlogout", 5,	//話務員退出消息
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,"" },
		},
	},//
  {/*70*/	"agtrancallresult", 16,	//向坐席發送轉接呼叫結果
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,"" },
			{"workerno",  2,0,0,"" },
			{"trantype",  2,1,6,"" },
			{"sessionno", 2,0,0,"" }, 
			{"chntype",   2,1,2,"" },
			{"chnno",     2,1,7,"" },
			{"cfcno",     2,1,6,"" },
			{"callerno",  5,0,0,"" },
			{"calledno",  5,0,0,"" },
			{"param",     5,0,0,"" },
			{"result",    2,1,6,"" },
			{"errorbuf",  5,0,0,"" },
		},
	},//
  {/*71*/	"agjoinconfresult", 12,	//向坐席發送指定通道參加會議結果
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,"" },
			{"workerno",  2,0,0,"" },
			{"destchntype",2,1,2,"" },
			{"destchnno", 2,1,7,"" },
			{"confno", 		2,1,7,"" },
			{"jointype", 	2,1,7,"" },
			{"result",    2,1,6,"" },
			{"errorbuf",  5,0,0,"" },
		},
	},//
  {/*72*/	"agtranivrresult", 10,	//坐席將來話轉接到ivr流程結果
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,"" },
			{"workerno",  2,0,0,"" },
			{"returnflag",2,1,6,"" }, //流程返回的標志
			{"param",     5,0,0,"" }, //返回附加參數
			{"result",    2,1,6,"" },
			{"errorbuf",  5,0,0,"" },
		},
	},//
  {/*73*/	"sendagentmsg", 11,	//發送消息到電腦坐席
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,"0"},
			{"workerno",  2,0,0,"0"}, 
			{"msgtype",   2,1,6,"0"},
			{"item0",     5,0,0,"" },
			{"item1",     5,0,0,"" },
			{"item2",     5,0,0,"" },
			{"item3",     5,0,0,"" },
		},
	},//
	{/*74*/	"pickupcall"	,8,	//代接電話
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"groupno",   5,0,0,"0" },
			{"seatno",    5,0,0,"0" },
			{"workerno",  2,0,0,"0" },
			{"selectmode",2,1,6,"0" },
		},
	},
	{/*75*/	"getacdqueue"	,5,	//取ACD隊列信息
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"groupno",   5,0,0,"0" },
		},
	},
	{/*76*/	"setflwstate"	,5,	//設置通道流程業務狀態信息
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"flwstate",  5,0,0,"" },
		},
	},
	{/*77*/	"swapcallresult"	,8,	//將呼出或呼叫坐席結果改發送到指定的會話
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"sessionno", 2,0,0,"" },
			{"sessionid1",2,0,0,"" },
			{"chantype1", 2,1,2,"" },           
			{"channo1",   2,1,7,"" },  
		},
	},
  {/*78*/	"recvfsk"	,6	,//接收FSK
		{				
			{"sessionid"	,2	,0		,0	,""},           
			{"cmdaddr"		,2	,1		,8  ,""},  
			{"chantype"	  ,2	,1		,2  ,""},   
			{"channo"		  ,2	,1		,7  ,""},  
			{"fskheader"	,2	,1		,6	,""},
			{"crctype"		,2	,1		,6	,""},
		},
   },
	{/*79*/	"transferivr",7,	//分配IVR資源
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"vopno",     2,1,6,"" },
			{"ivrno",     5,0,0,"" },
			{"param",     5,0,0,"" },
		},
	},
	{/*80*/	"releaseivr",4,	//釋放IVR資源
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
		},
	},
	{/*81*/	"playswitchvoice",5,	//播放交換機內置語音
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"voiceid",   2,1,6,"" },
		},
	},
	{/*82*/	"sendswitchcmd",6,	//發送交換機操作命令
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"cmdid",     2,1,6,"" }, //1-拍叉簧 2-保持 3-三方會議 4-切換通話 5-掛斷 6-完成轉接 100-將cmdparam設置的工號從等待IVR返回狀態置閑
			{"cmdparam",  5,0,0,"" },
		},
	},
	{/*83*/	"dial",6,	//撥打電話
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"callerno",  5,0,0,"" },
			{"calledno",  5,0,0,"" },
			{"routeno",   2,1,6,"" },
		},
	},
	{/*84*/	"appendfaxfile",12,	//追加發送的傳真文件
		{
			{"sessionid",		2,	0,	0,	""},
			{"cmdaddr",			2,	1,	8,	""},
			{"chantype",		2,	1,	2,	""},
			{"channo",			2,	1,	7,	""},
			{"faxcsid",			5,	0,	0,	""}, //4 本端傳真標識號
			{"resolution",	5,	0,	0,	""}, //5 為空時則采用默認分辨率，格式為：X:分辨率數值;Y:分辨率數值
			{"totalpages",	2,	1,	6,	"0"},//6 總頁數
			{"pagelist",		5,	0,	0,	""}, //7 發送的頁碼范圍，為空時表示發送所有頁面，格式為：a-z (a表示起始頁號，z表示終止頁號)
			{"faxheader",		5,	0,	0,	""}, //8 添加的頁眉，為空時則不添加頁眉，格式為：XPos=起始X坐標;YPos=起始Y坐標;FontName=字體名稱;FontSize=字體大小;From=發件人;Subject=主題;To=收件人;Time=發送時間;website=公司網址;
			{"faxfooter",		5,	0,	0,	""}, //9 添加的頁腳，為空時則不添加頁腳，格式為：XPos=起始X坐標;YPos=起始Y坐標;FontName=字體名稱;FontSize=字體大小;From=發件人;Subject=主題;To=收件人;Time=發送時間;website=公司網址;
			{"barcode",		  5,	0,	0,	""}, //10 是否加注條碼
			{"filename",		5,	0,	0,	""}, //11 傳真文件名
		},
	},
	{/*85*/	"bandagentchn",9,	//綁定坐席通道
		{
			{"sessionid", 2,0,0,"" },           
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"seatno",    5,0,0,"" },
			{"phoneno",   5,0,0,"" },
			{"chntype",   2,1,2,"" },           
			{"chnno",     2,1,7,"" },  
			{"state",     2,1,6,"0"},
		},
	},
	{/*86*/	"sendcalltoagent",11,	//發送電話分配到電腦坐席
		{
			{"sessionid", 2,0,0,"" },
			{"cmdaddr",   2,1,8,"" },
			{"chantype",  2,1,2,"" },           
			{"channo",    2,1,7,"" },  
			{"seatno",    5,0,0,"" },
			{"workerno",  5,0,0,"" },
			{"callinout", 2,1,6,"" },           
			{"calltype",  2,1,6,"" },  
			{"callerno",  5,0,0,"" },
			{"calledno",  5,0,0,"" },
			{"param",     5,0,0,"" },
		},
	},
	{/*87*/	"play",			15,	//多文件放音
		{
			{"sessionid",		2,0,0,"" }, //0
			{"cmdaddr",			2,1,8,"" }, //1
			{"chantype",		2,1,2,"" }, //2
			{"channo",			2,1,7,"" }, //3
			{"dtmfrule",		2,1,6,"0"}, //4
			{"playrule",		2,1,6,"0"}, //5
			{"asrrule",			2,1,6,"0"}, //6
			{"chntype",  		2,1,6,"0"}, //7
			{"chnno",    		2,1,7,"0"}, //8
			{"content1",		5,0,0,"" }, //9
			{"content2",		5,0,0,"" }, //10
			{"content3",		5,0,0,"" }, //11
			{"content4",		5,0,0,"" }, //12
			{"content5",		5,0,0,"" }, //13
			{"content6",		5,0,0,"" }, //14
		},
	},
  {/*88*/	"workerloginex", 18,	//話務員登錄消息
	  {
			{"sessionid", 2,0,0,"" }, //0         
			{"cmdaddr",   2,1,8,"" }, //1
			{"chantype",  2,1,2,"" }, //2
			{"channo",    2,1,7,"" }, //3
			{"seatno",    5,0,0,""},  //4
			{"workerno",  2,0,0,""},  //5
			{"workername",5,0,0,""},  //6
			{"password",  5,0,0,""},  //7
			{"groupno",   5,0,0,"1" },//8
			{"class",     2,1,6,"1" },//9
			{"level",     5,0,0,"1" },//10
			{"clearid",   2,1,6,"1" },//11
			{"state",     2,1,6,"0" },//12
			{"dutyno",    2,1,6,"0" },//13
			{"autorecord",2,1,6,"1" },//14
			{"calldirection",2,1,6,"0" },//15
			{"accountno", 5,0,0,"" }, //16
			{"departmentno",2,1,7,"0" },//17
		},
	},//
  {/*89*/	"workerlogoutex", 6,	//話務員退出消息
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,""},
			{"workerno",  2,0,0,""},
		},
	},//
  {/*90*/	"getseatlogindata", 6,	//取坐席登錄的話務員信息
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,""},
			{"workerno",  2,0,0,""},
		},
	},//
	{/*91*/	"getidleseat"	,13,	//取一空閑坐席并臨時鎖定
		{
			{"sessionid",	2	,0	,0	,""	}, //0
			{"cmdaddr",		2	,1	,8  ,"" }, //1
			{"chantype",	2	,1	,2	,"" }, //2
			{"channo",		2	,1	,7  ,"" }, //3
			{"seatno",		5	,0	,0	,"0"}, //4
			{"workerno",	2	,0	,0	,"0"}, //5
			{"seattype",	2	,1	,6	,"0"}, //6
			{"acdrule",		2	,1	,6	,"2"}, //7
			{"groupno",		5	,0	,0	,"1"}, //8
			{"seatgroupno",2,0	,0	,"0"}, //9
			{"level",			5	,0	,0	,"0"}, //10
			{"levelrule",	2	,1	,6	,"0"}, //11
			{"lock",	    2	,1	,6	,"1"}, //12 是否臨時鎖定
		},
	},//
  {/*92*/	"unlockseat", 6,	//解除臨時鎖定
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,"0"},
			{"workerno",  2,0,0,"0"},
		},
	},//
  {/*93*/	"updateservicescore", 8,	//更新服務評價到后臺IVR服務
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"seatno",    5,0,0,""},
			{"workerno",  2,0,0,""},
			{"cdrserialno",5,0,0,""},
			{"score",     2,0,0,""},
		},
	},//
  {/*94*/	"updatecdrfield", 7,	//更新CDR字段數據
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"cdrserialno",5,0,0,""},
			{"fieldname", 5,0,0,""},
			{"fieldvalue",5,0,0,""},
		},
	},//
  {/*95*/	"updatesysparam", 6,	//更新平臺系統參數
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"paramname", 5,0,0,""},
			{"paramvalue",5,0,0,""},
		},
	},//
  {/*96*/	"setforwarding", 8,	//設置分機呼轉
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" }, 
			{"deviceno",  5,0,0,""},
			{"forwardtype",2,1,6,""},
			{"forwardonoff",2,1,6,""},
			{"forwarddeviceno",5,0,0,""},
		},
	},//
  {/*97*/	"callfunction", 7,	//特殊功能呼叫，如：監聽、強插
	  {
			{"sessionid", 2,0,0,"" },          
			{"cmdaddr",   2,1,8,"" },  
			{"chantype",  2,1,2,"" },         
			{"channo",    2,1,7,"" },
			{"calltype",	2,1,6,"1"}, //1-監聽，2-強插
			{"seatno",    5,0,0,"0"},
			{"workerno",  2,0,0,"0"},
		},
	},//
	{/*98*/	"playex",			13,	//增強型放音指令
		{
			{"sessionid",		2,0,0,"" }, //0
			{"cmdaddr",			2,1,8,"" }, //1
			{"chantype",		2,1,2,"" }, //2
			{"channo",			2,1,7,"" }, //3
			{"dtmfrule",		2,1,6,"0"}, //4
			{"playrule",		2,1,6,"0"}, //5
			{"asrrule",			2,1,6,"0"}, //6
			{"content1",		5,0,0,"" }, //7
			{"content2",		5,0,0,"" }, //8
			{"content3",		5,0,0,"" }, //9
			{"content4",		5,0,0,"" }, //10
			{"invalidvoc",	5,0,0,"" }, //11
			{"timeoutvoc",	5,0,0,"" }, //12
		},
	},
	{/*99*/	"ttsstringex"	,13	,//文本串轉語音
		{
			{"sessionid", 2, 0, 0,""},          
			{"cmdaddr",   2, 1, 8,""},  
			{"chantype",  2, 1, 2,""},           
			{"channo",    2, 1, 7,""}, 
			{"dtmfruleid",2, 1, 6,"0"},          
			{"playruleid",2, 1, 6,"0"},          
			{"asrruleid", 2, 1, 6,"0"},          
			{"ttstype",  5, 0, 0,"" }, 
			{"ttsgrammar",   5, 0, 0,"" }, 
			{"ttsparam",   5, 0, 0,"" }, 
			{"ttsformat",  2, 4, 4,"1" }, 
			{"ttsfilename",5, 0, 0,"" },  
			{"string",    5, 0, 0,"" },  
		},
	},
};

//----------------------------------------------------------------
//備ivr服務器-->主ivr服務器
const VXML_SEND_MSG_CMD_RULE_STRUCT S_TO_MMsgRuleArray[1]=
{
	{"standbystatus",			6,//備用機將運行狀態發到主用機
		{
			{"sessionid",			2,	0,	0,	"" },          
			{"nodetype",			2,	1,	3,	"0"},          
			{"nodeno",				2,	1,	4,	"" },  
			{"masterstatus",	2,	1,	1,	"" },  
			{"standbystatus",	2,	1,	1,	"" },  
			{"reason",				5,	0,	0,	"" },  
		},
	},
};
