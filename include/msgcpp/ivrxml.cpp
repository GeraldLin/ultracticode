//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//ivr服務器-->vxml解析器(parser)
//MsgId	MsgName		AttrNum	Note
const VXML_RECV_MSG_CMD_RULE_STRUCT       IVRXMLMsgRuleArray[MAX_IVRXMLMSG_NUM]=
{
{/*0*/	"onlogin",10,
				{
					{"sessionid",2,	0,		0,	1,		1},
					{"nodetype"	,2,	1,		3,	0,		0},
					{"nodeno"	,2,	1,		4,	0,		0},
					{"maxtrunk"	,2,	1,		7,	0,		0},
					{"maxseat"	,2,	1,		7,	0,		0},
					{"cardtype"	,2,	1,		6,	0,		0},
					{"result"	,2,	1,		6,	0,		0},
					{"errorbuf"	,5,	0,		0,	0,		0},
					{"switchtype"	,2,	1,		6,	0,		0},
					{"dbtype"	,5,	0,		0,	0,		0},
				},
													},//節點注冊結果
{/*1*/	"onsetaccessno",5	,
				{
					{"sessionid",2,	0,		0,	1,		1},
					{"settype"	,2,	1,		0,	0,		0},
					{"accessno"	,5,	0,		0,	0,		0},
					{"result"	,2,	1,		6,	0,		0},
					{"errorbuf"	,5,	0,		0,	0,		0},
				},
													},//設置接入號碼結果
{/*2*/	"oncallin",				23,
				{
					{"chantype"	    ,2,	1,		2,	1,		3}, //0  業務通道類型: 1-中繼 2-坐席
					{"channo"		    ,2,	1,		7,	1,		4}, //1  業務通道編號
					{"callerno"	    ,5,	0,		0,	0,		0}, //2  主叫號碼
					{"callertype"	  ,2,	1,		6,	0,		0}, //3  主叫號碼類型
					{"calledno"	    ,5,	0,		0,	0,		0}, //4  被叫號碼
					{"calledtype"	  ,2,	1,		6,	0,		0}, //5  被叫號碼類型: 0-外線呼入號碼(外線有效) 1-呼出的外線號碼 2-呼叫的內線坐席號 3-呼叫的坐席工號
					{"orgcallerno"	,5,	0,		0,	0,		0}, //6  原主叫號碼
					{"orgcalledno"	,5,	0,		0,	0,		0}, //7  原被叫號碼
					{"inrouteno"	  ,2,	1,		6,	0,		0}, //8  呼入中繼的路由號
					{"chhwtype"	    ,2,	1,		6,	0,		0}, //9  通道硬件類型
					{"chhwindex"    ,2,	1,		7,	0,		0}, //10 按通道硬件類型排列的通道序號
					{"seattype"	    ,2,	1,		6,	0,		0}, //11 對應的坐席類型
					{"seatno"	      ,5,	0,		0,	0,		0}, //12 坐席號,對于坐席電話呼入時用
					{"workerno"	    ,5,	0,		0,	0,		0}, //13 工號,對于坐席電話呼入時用
					{"param"	      ,5,	0,		0,	0,		0}, //14 傳遞的參數,對于電腦坐席電話呼入時,傳遞給流程的參數
					{"groupno"	    ,2,	0,		0,	0,		0}, //15 話務員組號,對于坐席電話呼入時用
					{"bandagfirst"	,2,	1,		6,	0,		0}, //16 先要綁定呼叫坐席標志(1-是 0-否)
					{"calltype"			,2,	1,		6,	0,		0}, //17 呼叫類型(0-普通呼叫 1-直接呼入 2-直接呼出 3-轉移呼入 4-代接呼入 5-強插監聽呼出)
					{"answerid"			,2,	1,		6,	0,		0}, //18 已應答標志標志(1-是 0-否)
					{"cdrserialno"	,5,	0,		0,	0,		0}, //19 詳細話單號
					{"recdrootpath"	,5,	0,		0,	0,		0}, //20 坐席錄音文件根路徑
					{"recdfilename"	,5,	0,		0,	0,		0}, //21 坐席錄音文件名
					{"dtmfbuf"			,5,	3,		2,	3,		7},
				},
													},//電話呼入事件
{/*3*/	"onrecvsam",		8,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"callerno"	  ,5,	0,		0,	0,		0},
					{"calledno"	  ,5,	0,		0,	0,		0},
					{"orgcalledno",5,	0,		0,	0,		0},
					{"orgcallerno",5,	0,		0,	0,		0},
				},
													},//收到后續地址事件
{/*4*/	"onsmsin",			5,
				{
					{"chantype"  ,	2,	1,		2,	1,		3},
					{"channo"	,	2,	1,		7,	1,		4},
					{"callerno"  ,	5,	0,		0,	0,		0},
					{"calledno"  ,	5,	0,		0,	0,		0},
					{"sms"		,	5,	0,		0,	0,		0    },
				},
													},//短信呼入事件
{/*5*/	"oncallflw",15	,
				{
					{"funcno"		    ,2,	1,		6,	0,		0  },
					{"chantype"	    ,2,	1,		2,	1,		3  },
					{"channo"		    ,2,	1,		7,	1,		4  },
					{"callerno"	    ,5,	0,		0,	0,		0  },
					{"calledno"	    ,5,	0,		0,	0,		0  },
					{"orgcalledno"	,5,	0,		0,	0,		0}, //原主叫號碼
					{"orgcallerno"	,5,	0,		0,	0,		0}, //原被叫號碼
					{"inrouteno"	  ,2,	1,		6,	0,		0}, //呼入中繼的路由號
					{"chhwtype"	    ,2,	1,		6,	0,		0}, //通道硬件類型
					{"chhwindex"    ,2,	1,		7,	0,		0}, //按通道硬件類型排列的通道序號
					{"seattype"	    ,2,	1,		6,	0,		0}, //對應的坐席類型
					{"seatno"	      ,5,	0,		0,	0,		0}, //坐席號,對于坐席電話呼入時用
					{"workerno"	    ,5,	0,		0,	0,		0}, //工號,對于坐席電話呼入時用
					{"serialno"	    ,2,	0,		0,	5,		101},
					{"param"		    ,5,	0,		0,	5,		102},
				},
													},//調用指定流程
{/*6*/	"onanswer",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//來話應答結果
{/*7*/	"onsendsignal",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//發送信令結果
{/*8*/	"onhangon",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//掛機釋放結果
{/*9*/	"ondtmfrule",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//DTMF按鍵規則定義結果
{/*10*/	"ongetdtmf",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		3},
					{"asrbuf"		,5,	0,		0,	3,		4},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//收碼結果
{/*11*/	"onplayrule",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//放音規則定義結果
{/*12*/	"onasrrule",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//ASR規則定義結果
{/*13*/	"onsetvolume",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//改變放音音量結果
{/*14*/	"onplayfile",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		7},
					{"asrbuf"		,5,	0,		0,	3,		8},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//單文件放音結果
{/*15*/	"onsetplaypos",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//改變放音指針位置結果
{/*16*/	"onplayfiles",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		7},
					{"asrbuf"		,5,	0,		0,	3,		8},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//多文件放音結果
{/*17*/	"onplaycompose",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		6},
					{"asrbuf"		,5,	0,		0,	3,		7},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//合成串放音結果
{/*18*/	"onmultiplay",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1 },
					{"cmdaddr"		,2,	1,		8,	1,		2 },
					{"chantype"	    ,2,	1,		2,	1,		3 },
					{"channo"		,2,	1,		7,	1,		4 },
					{"result"		,2,	1,		6,	2,		2 },
					{"dtmfbuf"		,5,	3,		2,	3,		10},
					{"asrbuf"		,5,	0,		0,	3,		11},
					{"errorbuf"	    ,5,	0,		0,	4,		1 },
				},
													},//多語音合成放音結果
{/*19*/	"onttsstring",9	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		10},
					{"asrbuf"		,5,	0,		0,	3,		11},
					{"ttsbuf"		,5,	0,		0,	3,		12},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//文本串轉語音結果
{/*20*/	"onttsfile",9	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		10},
					{"asrbuf"		,5,	0,		0,	3,		11},
					{"ttsbuf"		,5,	0,		0,	3,		12},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//文本文件轉語音結果
{/*21*/	"onsenddtmf",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	0,		0},
					{"asrbuf"		,5,	0,		0,	0,		0},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//發送DTMF結果
{/*22*/	"onsendfsk",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//發送FSK結果
{/*23*/	"onplaycfc",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//會議室放音結果
{/*24*/	"onplaytone",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//播放信號音結果
{/*25*/	"onrecordfile",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1    },
					{"cmdaddr"		,2,	1,		8,	1,		2    },
					{"chantype"	    ,2,	1,		2,	1,		3    },
					{"channo"		,2,	1,		7,	1,		4    },
					{"result"		,2,	1,		6,	2,		5    },
					{"recordedlen"	,2,	1,		8,	3,		7    },
					{"recordedsize"  ,2,	1,		8,	3,		8	 },
					{"errorbuf"	    ,5,	0,		0,	4,		1    },
				},
													},//錄音結果
{/*26*/	"onrecordcfc",9	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1    },
					{"cmdaddr"		,2,	1,		8,	1,		2    },
					{"chantype"	    ,2,	1,		2,	1,		3    },
					{"channo"		,2,	1,		7,	1,		4    },
					{"result"		,2,	1,		6,	2,		5    },
					{"cfcno"	    ,2,	1,		7,	3,		6    },
					{"recordedlen"	,2,	1,		8,	3,		7    },
					{"recordedsize"  ,2,	1,		8,	3,		8	 },
					{"errorbuf"	    ,5,	0,		0,	4,		1    },
				},
													},//會議錄音結果
{/*27*/	"onstop",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//停止放音錄音收碼結果
{/*28*/	"oncallout",11	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1 },
					{"cmdaddr"		,2,	1,		8,	1,		2 },
					{"chantype"	    ,2,	1,		2,	1,		3 },
					{"channo"		,2,	1,		7,	1,		4 },
					{"result"		,2,	1,		6,	2,		6 },
					{"outchantype"	    ,2,	1,		7,	3,		12},
					{"outchanno"	    ,2,	1,		7,	3,		13},
					{"calledno"	    ,5,	0,		0,	3,		14},
					{"funcno"		,2,	1,		6,	3,		15},
					{"point"		,5,	0,		0,	3,		16},
					{"errorbuf"	    ,5,	0,		0,	4,		1 },
				},
													},//電話呼出結果
{/*29*/	"oncancelcallout",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//取消呼出結果
{/*30*/	"onsendsam",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//發送被叫號碼結果
{/*31*/	"ontransfer",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1 },
					{"cmdaddr"		,2,	1,		8,	1,		2 },
					{"chantype"	    ,2,	1,		2,	1,		3 },
					{"channo"		,2,	1,		7,	1,		4 },
					{"result"		,2,	1,		6,	2,		6 },
					{"outchantype"	    ,2,	1,		7,	3,		15},
					{"outchanno"	    ,2,	1,		7,	3,		16},
					{"errorbuf"	    ,5,	0,		0,	4,		1 },
				},
													},//電話轉接結果
{/*32*/	"oncallseat",  14	,
				{
					{"sessionid",   2,	0,		0,	1,		1 },
					{"cmdaddr",     2,	1,		8,	1,		2 },
					{"chantype",    2,	1,		2,	1,		3 },
					{"channo",      2,	1,		7,	1,		4 },
					{"result",      2,	1,		6,	2,		7 },
					{"outchantype", 2,	1,		7,	3,		22},
					{"outchanno",   2,	1,		7,	3,		23},
					{"seatno",      5,	0,		0,	3,		24},
					{"workerno",    2,	1,		7,	3,		25},
					{"groupno",     2,	1,		7,	3,		26},
					{"queueno",     2,	1,		7,	3,		27},
					{"serialno",    5,	0,		0,	3,		28},
					{"param",       5,	0,		0,	3,		29},
					{"errorbuf",    5,	0,		0,	4,		1 },
				},
													},//分配坐席結果
{/*33*/	"onstopcallseat",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//取消分配坐席結果
{/*34*/	"oncreatecfc",7	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1 },
					{"cmdaddr"		,2,	1,		8,	1,		2 },
					{"chantype"	    ,2,	1,		2,	1,		3 },
					{"channo"		,2,	1,		7,	1,		4 },
					{"result"		,2,	1,		6,	2,		1 },
					{"cfcno"		    ,2,	1,		7,	3,		10},
					{"errorbuf"	    ,5,	0,		0,	4,		1 },
				},
													},//創建會議結果
{/*35*/	"ondestroycfc",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//釋放會議資源結果
{/*36*/	"onjoincfc",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		13},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//加入會議結果
{/*37*/	"onunjoincfc",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//退出會議結果
{/*38*/	"onlink",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//通道交換結果
{/*39*/	"ontalkwith",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//與對方通話結果
{/*40*/	"onlistenfrom",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//監聽對方通話結果
{/*41*/	"oninserttalk",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//強插對方通話結果
{/*42*/	"onthreetalk",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//三方通話結果
{/*43*/	"onstoptalk",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//結束通話結果
{/*44*/	"onsendfax",9	,
				{
					{"sessionid",		2,	0,	0,	1,	1},
					{"cmdaddr",			2,	1,	8,	1,	2},
					{"chantype",		2,	1,	2,	1,	3},
					{"channo",			2,	1,	7,	1,	4},
					{"result",			2,	1,	6,	2,	9},
					{"sendpages",		2,	1,	6,	3,	9},
					{"sendbytes",		2,	0,	0,	3,	10},
					{"sendspeed",		2,	0,	0,	3,	11},
					{"errorbuf",		5,	0,	0,	4,	1},
				},
													},//發送傳真結果
{/*45*/	"onrecvfax",11	,
				{
					{"sessionid",		2,	0,	0,	1,	1},
					{"cmdaddr",			2,	1,	8,	1,	2},
					{"chantype",		2,	1,	2,	1,	3},
					{"channo",			2,	1,	7,	1,	4},
					{"result",			2,	1,	6,	2,	9},
					{"faxcsid",			5,	0,	0,	3,	4},
					{"barcode",			5,	0,	0,	3,	5},
					{"recvpages",		2,	1,	6,	3,	6},
					{"recvbytes",		2,	0,	0,	3,	7},
					{"recvspeed",		2,	0,	0,	3,	8},
					{"errorbuf",		5,	0,	0,	4,	1},
				},
													},//接收傳真結果
{/*46*/	"oncheckpath",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//檢查文件路徑結果
{/*47*/	"oncreatepath",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//創建文件路徑結果
{/*48*/	"ondeletepath",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//刪除文件路徑結果
{/*49*/	"ongetfilenum",7	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"filenums"	    ,2,	1,		7,	3,		3},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//取文件個數結果
{/*50*/	"ongetfilename",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"serialno"	    ,2,	1,		7,	0,		0},
					{"filenums"	    ,2,	1,		7,	3,		3},
					{"filename"	    ,5,	0,		0,	3,		5},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//取文件名結果
{/*51*/	"onrenname",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//改文件名結果
{/*52*/	"oncopyfile",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//拷貝文件結果
{/*53*/	"ondeletefile",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//刪除文件結果
{/*54*/	"oncheckfile",7	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"size"		    ,2,	1,		8,	3,		2},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//檢查文件是否存在
{/*55*/	"onclearmixer",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//清除放音合成緩沖區
{/*56*/	"onaddfile",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加文件到放音合成緩沖區
{/*57*/	"onadddatetime",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加日期時間到放音合成緩沖區
{/*58*/	"onaddmoney",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加金額到放音合成緩沖區
{/*59*/	"onaddnumber",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加數值到放音合成緩沖區
{/*60*/	"onadddigits",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加數字串到放音合成緩沖區
{/*61*/	"onaddchars",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加字符串到放音合成緩沖區
{/*62*/	"onplaymixer",8	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		1,	3,		4},
					{"asrbuf"		,5,	0,		0,	3,		5},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//播放合成緩沖區
{/*63*/	"onaddttsstr",6	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加TTS字符串到放音合成緩沖區
{/*64*/	"onaddttsfile",6	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//增加TTS字符串到放音合成緩沖區
{/*65*/	"onplay",8	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		2},
					{"dtmfbuf"		,5,	3,		2,	3,		12},
					{"asrbuf"		,5,	0,		0,	3,		13},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//通用放音結果
{/*66*/	"onlinkevent",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		11},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//通道交換對方掛機、斷開事件
{/*67*/	"onrecvevent",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//收到消息證實事件
{/*68*/	"ondialout",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		6},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//撥號器呼出返回事件
{/*69*/	"onflash",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//模擬外線拍叉簧結果
{/*70*/	"onsetvcparam",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//設置變聲參數結果
{/*71*/	"onpause",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//暫停放音結果
{/*72*/	"onreplay",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//重新放音結果
{/*73*/	"onfastplay",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//快速放音結果
{/*74*/	"onchecktone",7	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1    },
					{"cmdaddr"		,2,	1,		8,	1,		2    },
					{"chantype"	    ,2,	1,		2,	1,		3    },
					{"channo"		,2,	1,		7,	1,		4    },
					{"result"		,2,	1,		6,	2,		10    },
					{"tonetype"	    ,2,	1,		7,	3,		3    }, //檢測信號音 1-撥號音，2-回鈴音，3-忙音，4-應答音，5-F1傳真信號音(發送傳真的信號音)，6-F2傳真信號音(等待接收傳真的信號音)
					{"errorbuf"	    ,5,	0,		0,	4,		1    },
				},
													},//檢測信號音結果
{/*75*/	"execsql",5	,
				{
					{"sessionid",  2,0,0,1,1},
					{"cmdaddr",    2,1,8,1,2},
					{"chantype",   2,1,2,1,3},
					{"channo",     2,1,7,1,4},
					{"sqls",       5,0,0,0,0},
				},
													},//IVR執行SQL語句
{/*76*/	"onworkerlogin",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//話務員登錄消息結果
{/*77*/	"onworkerlogout",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//話務員退出消息結果
{/*78*/	"onagblindtrancall",7	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"phonetype"	,2,	1,		6,	0,		0},
					{"tranphone"	,5,	0,		0,	0,		0},
					{"tranparam"	,5,	0,		0,	0,		0},
				},
													},//電腦坐席快速轉接電話
{/*79*/	"onagconsulttrancall",7	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"phonetype"	,2,	1,		6,	0,		0},
					{"tranphone"	,5,	0,		0,	0,		0},
					{"tranparam"	,5,	0,		0,	0,		0},
				},
													},//電腦坐席協商轉接電話
{/*80*/	"onagconftrancall",8	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"phonetype"	,2,	1,		6,	0,		0},
					{"tranphone"	,5,	0,		0,	0,		0},
					{"confno"			,2,	1,		6,	0,		0},
					{"tranparam"	,5,	0,		0,	0,		0},
				},
													},//電腦坐席會議轉接電話
{/*81*/	"onagstoptrancall",4	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
				},
													},//坐席停止轉接來話
{/*82*/	"onagconfspeak",7	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"destchntype",2,	1,		2,	0,		0}, //目的通道類型 0-表示自動選擇
					{"destchnno"	,2,	1,		7,	0,		0}, //目的通道號
					{"confno"			,2,	1,		6,	0,		0},
				},
													},//電腦坐席指定通道會議發言
{/*83*/	"onagconfaudit",7	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"destchntype",2,	1,		2,	0,		0}, //目的通道類型 0-表示自動選擇
					{"destchnno"	,2,	1,		7,	0,		0}, //目的通道號
					{"confno"			,2,	1,		6,	0,		0},
				},
													},//電腦坐席指定通道旁聽會議
{/*84*/	"onagtranivr",7	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"tranivrid"  ,2,	1,		7,	0,		0}, //轉接的ivr標志,流程通過該標志約定來判斷跳轉到哪里
					{"returnid"		,2, 1,		7,	0,		0}, //是否返回坐席標志
					{"tranparam"	,5,	0,		0,	0,		0}, //傳遞的附加參數
				},
													},//坐席將來話轉接到ivr流程
{/*85*/	"onaghold",6	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"flag"       ,2,	1,		7,	0,		0}, //1-保持 0-取消保持
					{"waitvoc"    ,5,	0,		0,	0,		0}, //等待音樂
				},
													},//坐席將來話保持/取消保持消息
{/*86*/	"onagmute",5	,
				{
					{"sessionid"	,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	  ,2,	1,		2,	1,		3},
					{"channo"		  ,2,	1,		7,	1,		4},
					{"flag"       ,2,	1,		7,	0,		0}, //1-靜音 0-取消靜音
				},
													},//坐席將來話靜音/取消靜音消息
{/*87*/	"onagflwmsg",7	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"eventtype"		,2,	1,		7,	0,		0},
					{"msgtype"		,2,	1,		7,	0,		0},
					{"msg"	    ,5,	0,		0,	0,		0},
				},
													},//收到電腦坐席發送給流程的消息
{/*88*/	"onpickupcall",12	,
				{
					{"sessionid"	  ,2,	0,		0,	1,		1},
					{"cmdaddr"		  ,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		    ,2,	1,		7,	1,		4},
					{"result"		    ,2,	1,		6,	2,		1},
					{"callerno"	    ,5,	0,		0,	3,		5},
					{"calledno"	    ,5,	0,		0,	3,		6},
					{"chntype"	    ,2,	1,		7,	3,		7},
					{"chnno"	      ,2,	1,		7,	3,		8},
					{"seatno"	      ,5,	0,		0,	3,		9},
					{"workerno"	    ,2,	1,		7,	3,		10},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//代接電話結果
{/*89*/	"ongetacdqueue",8	,
				{
					{"sessionid"	  ,2,	0,		0,	1,		1},
					{"cmdaddr"		  ,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		    ,2,	1,		7,	1,		4},
					{"result"		    ,2,	1,		6,	2,		1},
					{"waitacd"	    ,2,	1,		7,	3,		2},
					{"waitans"	    ,2,	1,		7,	3,		3},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//取ACD隊列信息結果
{/*90*/	"ondialdtmf",			5,
				{
					{"sessionid"	  ,2,	0,		0,	1,		1},
					{"cmdaddr"		  ,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		    ,2,	1,		7,	1,		4},
					{"dialdtmf"		  ,5, 0,		0,	0,		0},
				},
													},//內線分機后續按鍵事件
{/*91*/	"onrecvfsk",9	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"fskcmd",       2,	1,		6,	3,		3},
					{"fskdata",       5,	0,		0,	3,		4},
					{"datalen",       2,	1,		7,	3,		5},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//接收FSK結果
{/*92*/	"ontransferivr",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//分配IVR資源結果
{/*93*/	"onreleaseivr",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//釋放IVR資源結果
{/*94*/	"onplayswitchvoice",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//播放交換機內置語音結果
{/*95*/	"onsendswitchcmd",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		1},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//發送交換機操作命令結果
{/*96*/	"ondial",6	,
				{
					{"sessionid"	    ,2,	0,		0,	1,		1},
					{"cmdaddr"		,2,	1,		8,	1,		2},
					{"chantype"	    ,2,	1,		2,	1,		3},
					{"channo"		,2,	1,		7,	1,		4},
					{"result"		,2,	1,		6,	2,		6},
					{"errorbuf"	    ,5,	0,		0,	4,		1},
				},
													},//撥打電話結果
	{/*97*/	"onappendfaxfile",6	,
		{
			{"sessionid",		2,0,0,1,1},
			{"cmdaddr",			2,1,8,1,2},
			{"chantype",		2,1,2,1,3},
			{"channo",			2,1,7,1,4},
			{"result",			2,1,6,2,1},
			{"errorbuf",		5,0,0,4,1},
		},
	},//追加發送的傳真文件結果
	{/*98*/	"onrouteideltrks",6	,
		{
					{"sessionid",  2,0,0,1,1},
					{"cmdaddr",    2,1,8,1,2},
					{"chantype",   2,1,2,1,3},
					{"channo",     2,1,7,1,4},
					{"routeno",		 2,1,6,0,0}, //路由號
					{"idelnum",    2,1,7,0,0}, //空閑中繼數
		},
	},//路由空閑中繼數
	{/*99*/	"onbandagentchn",12	,
		{
			{"sessionid",		2,0,0,1,1}, //0
			{"cmdaddr",			2,1,8,1,2}, //1
			{"chantype",		2,1,2,1,3}, //2
			{"channo",			2,1,7,1,4}, //3
			{"chhwtype",		2,1,6,0,0}, //4 通道硬件類型
			{"chhwindex",		2,1,7,0,0}, //5 按通道硬件類型排列的通道序號
			{"seattype",		2,1,6,0,0}, //6 對應的坐席類型
			{"seatno",			5,0,0,0,0}, //7 坐席號
			{"workerno",		5,0,0,0,0}, //8 工號
			{"groupno",			2,0,0,0,0}, //9 話務員組號
			{"result",			2,1,6,2,1}, //10
			{"errorbuf",		5,0,0,4,1}, //11
		},
	},//綁定坐席通道結果
	{/*100*/	"onsendcalltoagent",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//發送電話分配到電腦坐席結果
	{/*101*/	"oncallconnected",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		11},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//收到交換機呼叫通話連接事件
	{/*102*/	"onnewcallparam",9,
		{
			{"sessionid"	  ,2,	0,		0,	1,		1},
			{"cmdaddr"		  ,2,	1,		8,	1,		2},
			{"chantype"	    ,2,	1,		2,	1,		3},
			{"channo"		    ,2,	1,		7,	1,		4},
			{"callerno"	    ,5,	0,		0,	0,		0},
			{"calledno"	    ,5,	0,		0,	0,		0},
			{"workerno"	    ,5,	0,		0,	0,		0},
			{"cdrserialno"	,5,	0,		0,	0,		0},
			{"param"	      ,5,	0,		0,	0,		0},
		},
	},//收到變化了的新的呼叫相關參數
  {/*103*/	"ongetseatlogindata",15	,
		{
			{"sessionid",		2,	0,		0,	1,		1},
			{"cmdaddr",			2,	1,		8,	1,		2},
			{"chantype",		2,	1,		2,	1,		3},
			{"channo",			2,	1,		7,	1,		4},
			{"seatno",      5,	0,		0,	3,		3},
			{"workerno",    5,	0,		0,	3,		4},
			{"workername",  5,	0,		0,	3,		5},
			{"groupno",     5,	0,		0,	3,		6},
			{"disturbid",   5,	0,		0,	3,		7},
			{"leaveid",     5,	0,		0,	3,		8},
			{"dutyno",     	5,	0,		0,	3,		9},
			{"accountno",   5,	0,		0,	3,		10},
			{"logtime",     5,	0,		0,	3,		11},
			{"result",			2,	1,		6,	2,		1},
			{"errorbuf",		5,	0,		0,	4,		1},
		},
	},//話務員登錄消息結果
	
	{/*104*/	"onnodestate",4,
		{
			{"sessionid",		2,	0,		0,	1,		1},
			{"nodetype",		2,	1,		6,	0,		0},
			{"nodeno",			2,	1,		6,	0,		0},
			{"state",			  2,	1,		6,	0,		0},
		},
	},//節點登錄狀態
	
	//start 105
	{"onrouteselectreq",		8,	//交換機請求選擇外部路由
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"cmdaddr",			2,	1,	8,	1,	2},
			{"chantype",		2,	1,	2,	1,	3},
			{"channo",			2,	1,	7,	1,	4},
			{"result"		   ,2,	1,	6,	2,	1},
			{"routeregid",	2,	0,	0,	0,	0}, //路由注冊id
			{"routerefid",	2,	0,	0,	0,	0}, //請求參考id
			{"reqparam",	  5,	0,	0,	0,	0}, //請求附加參數
		},
	},//end 105
  
  {/*106*/	"ongetidleseat",10	,
		{
			{"sessionid",		2,	0,		0,	1,		1},
			{"cmdaddr",			2,	1,		8,	1,		2},
			{"chantype",		2,	1,		2,	1,		3},
			{"channo",			2,	1,		7,	1,		4},
			{"seatno",      5,	0,		0,	3,		10}, //返回的鎖定座席號
			{"workerno",    5,	0,		0,	3,		11}, //返回的鎖定的工號
			{"logins",      5,	0,		0,	3,		12}, //該組已登錄的座席總數
			{"idels",       5,	0,		0,	3,		13}, //該組空閑的座席數
			{"result",			2,	1,		6,	2,		1},
			{"errorbuf",		5,	0,		0,	4,		1},
		},
	},//取一空閑坐席并臨時鎖定結果
	
	{/*107*/	"onunlockseat",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//解除臨時鎖定結果

	
	{/*108*/	"onupdatesrvcore",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//更新服務評價到后臺IVR服務結果
	
	{/*109*/	"onupdatecdrfield",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//更新CDR字段數據結果
	
	{/*110*/	"onupdatesysparam",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//更新平臺系統參數結果
	
	{/*111*/	"onsetforwarding",9	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"forwardtype", 2,	0,		0,	3,		5},
			{"forwardonoff",2,	0,		0,	3,		6},
			{"forwarddeviceno",5,0,		0,	3,		7},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//設置分機呼轉結果
	{/*112*/	"oncallfunction",6	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		1},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//特殊功能呼叫結果，如：監聽、強插
	{/*113*/	"onplayex",8	,
		{
			{"sessionid"	 ,2,	0,		0,	1,		1},
			{"cmdaddr"		 ,2,	1,		8,	1,		2},
			{"chantype"	   ,2,	1,		2,	1,		3},
			{"channo"		   ,2,	1,		7,	1,		4},
			{"result"		   ,2,	1,		6,	2,		2},
			{"dtmfbuf"		 ,5,	3,		2,	3,		10},
			{"asrbuf"		   ,5,	0,		0,	3,		11},
			{"errorbuf"	   ,5,	0,		0,	4,		1},
		},
	},//增強型放音結果
};

//----------------------------------------------------------------
//主ivr服務器-->備ivr服務器
const VXML_RECV_MSG_CMD_RULE_STRUCT M_TO_SMsgRuleArray[4]=
{
	//0
	{"onmasterstatus",		6, //主用機將運行狀態發到備用機
		{
			{"sessionid",			2,	0,	0,	1,	1},
			{"nodetype",			2,	1,	3,	0,	0},
			{"nodeno",				2,	1,	4,	0,	0},
			{"masterstatus",	2,	1,	6,	0,	0}, //主機狀態：0-待機，1-運行，2-接管運行
			{"standbystatus",	2,	1,	6,	0,	0}, //備機狀態：0-待機，1-運行，2-接管運行
			{"reason",				5,	0,	0,	0,	0},
		},
	},
	//1
	{"onaglogindata",			12, //同步坐席登錄數據及狀態
		{
			{"agno",		  		2,	0,	0,	0,	0}, //0 序號
			{"seatno",	  	  5,	0,	0,	0,	0}, //1 座席分機號
			{"workerno",	  	2,	0,	0,	0,	0}, //2 話務員工號
			{"workername",	  5,	0,	0,	0,	0}, //3 話務員姓名
			{"groupno",	  	  5,	0,	0,	0,	0}, //4 話務組號
			{"groupname",	  	5,	0,	0,	0,	0}, //5 話務組名稱
			{"dutyno",	  	  2,	1,	6,	0,	0}, //6 值班號
			{"svstate",				2,	1,	6,	0,	0}, //7 服務狀態 21//話務員登錄 22//話務員退出
			{"disturbid",			2,	1,	6,	0,	0}, //8 忙閑 0-示閑 1-示忙
			{"leaveid",				2,	1,	6,	0,	0}, //9 在席(0)/離席(>0)標志
			{"reason",	  	  5,	0,	0,	0,	0}, //10 離席原因
			{"callmsg",				5,	0,	0,	0,	0}, //11 呼叫信息
		},
	},
	//2
	{"onsysrunstate",			6, //系統運行狀態
		{
			{"sessionid",			2,	0,	0,	1,	1},
			{"nodetype",			2,	1,	3,	0,	0},
			{"nodeno",				2,	1,	4,	0,	0},
			{"masterstatus",	2,	1,	6,	0,	0}, //主機狀態：0-待機，1-運行，2-接管運行
			{"standbystatus",	2,	1,	6,	0,	0}, //備機狀態：0-待機，1-運行，2-接管運行
			{"runstate",			2,	0,	0,	0,	0}, //告警bit定義：bit0-FLW,bit1-SWT,bit2-DB,bit3-VOP,bit4-CTILINK,bit5-DB CONECT,bit6-WS,bit7-ALLIVR,bit8-IVR ch,bit9-REC ch
		},
	},
};
