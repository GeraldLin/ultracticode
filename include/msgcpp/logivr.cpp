//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1
//坐席到IVR的消息規則
const VXML_SEND_MSG_CMD_RULE_STRUCT       LOGIVRMsgRuleArray[MAX_LOGIVRMSG_NUM]=
{
  //MsgId = 0 					功能描述：登錄
  //消息名							消息參數個數
  {"login",					    4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"username",			5,			0,				0,			"" }, //登錄用戶名:administrator,manager,operator,board
			{"password",			5,			0,				0,			"" }, //登錄密碼，默認密碼：administrator,manager,operator,board
			{"getiniid",		  2,			1,				6,			"1" },//是否取ini配置文件
		},
	},//end of MsgId = 0

  //MsgId = 1 					功能描述：登出
  //消息名							消息參數個數
  {"logout",					    3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
		},
	},//end of MsgId = 1

  //MsgId = 2 					功能描述：取ACD分配隊列信息
  //消息名							消息參數個數
  {"getqueueinfo",		  2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示取簡單排隊信息 2-表示取詳細排隊信息
		},
	},//end of MsgId = 2

  //MsgId = 3 					功能描述：取所有坐席的狀態信息
  //消息名							消息參數個數
  {"getagentstatus",		2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示只取本坐席狀態 1表示取所有坐席狀態
		},
	},//end of MsgId = 3

  //MsgId = 4 					功能描述：取所有通道的狀態信息
  //消息名							消息參數個數
  {"getchnstatus",		  2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開
		},
	},//end of MsgId = 4

  //MsgId = 5 					功能描述：取ivr日志跟蹤信息
  //消息名							消息參數個數
  {"getivrtracemsg",		2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開
		},
	},//end of MsgId = 5

  //MsgId = 6 					功能描述：取已加載的流程信息
  //消息名							消息參數個數
  {"getloadedflws",		  2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開
		},
	},//end of MsgId = 6

  //MsgId = 7 					功能描述：取flw日志跟蹤信息
  //消息名							消息參數個數
  {"getflwtracemsg",		2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開
		},
	},//end of MsgId = 7

  //MsgId = 8 					功能描述：強行釋放通道
  //消息名							消息參數個數
  {"releasechn",		    2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"chnno",			    2,			1,				7,			"" }, //釋放的通道號
		},
	},//end of MsgId = 8

  //MsgId = 9 					功能描述：強拆其他坐席的電話
  //消息名							消息參數個數
  {"forceclear",		    3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			1,				7,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 9

  //MsgId = 10 					功能描述：強制將其他話務員退出
  //消息名							消息參數個數
  {"forcelogout",		    3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			1,				7,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 10

  //MsgId = 11 					功能描述：強制其他坐席免打擾
  //消息名							消息參數個數
  {"forcedisturb",		  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			1,				7,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 11

  //MsgId = 12 					功能描述：強制其他坐席空閑
  //消息名							消息參數個數
  {"forceready",		    3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"seatno",				5,			0,				0,			"" }, //坐席號:0-不指定坐席號
			{"workerno",			2,			1,				7,			"" }, //話務員工號:0-不指定工號
		},
	},//end of MsgId = 12

  //MsgId = 13 					功能描述：修改路由參數
  //消息名							消息參數個數
  {"modifyroutedata",		5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"routeno",			  2,			1,				7,			"" }, //路由號
			{"chnnolist",		  5,			0,				0,			"" }, //路由對應的中繼線序列
			{"selmode",			  2,			1,				7,			"0"}, //路由1中繼優先選澤方式(0-按順序循環選擇(默認值) 1-先選奇數電路，全忙時再選偶數電路(數字中繼有效) 2-先選偶數電路，全忙時再選奇數電路(數字中繼有效) 3-按順序
			{"ipprecode",		  5,			0,				0,			"" }, //長途呼出時插撥的IP字冠
		},
	},//end of MsgId = 13

  //MsgId = 14 					功能描述：修改坐席號
  //消息名							消息參數個數
  {"modifyseatdata",		3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"agentno",		    2,			1,				6,			"" }, //坐席編號
			{"seatno",		    5,			0,				0,			"" }, //新的坐席號
		},
	},//end of MsgId = 14

  //MsgId = 15 					功能描述：修改模擬外線接入號碼參數
  //消息名							消息參數個數
  {"modifyextline",		6,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"extlineno",			2,			1,				7,			"" }, //模擬外線序號
			{"accesscode",		5,			0,				0,			"" }, //接入號碼
			{"precode",		    5,			0,				0,			"" }, //呼出是加撥的號碼
			{"ipprecode",		  5,			0,				0,			"" }, //長途呼出時插撥的IP字冠
			{"notaddprecode", 5,			0,				0,			"" }, //呼出不加撥字冠的例外情況
		},
	},//end of MsgId = 15

  //MsgId = 16 					功能描述：加載業務流程
  //消息名							消息參數個數
  {"loadflw",		        4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"flwfilename",	  5,			0,				0,			"" }, //流程文件名
			{"funcgroup",		  2,			1,				7,			"" }, //功能組號
			{"funcno",		    2,			1,				7,			"" }, //流程功能號
		},
	},//end of MsgId = 16

  //MsgId = 17 					功能描述：添加接入號碼
  //消息名							消息參數個數
  {"addaccesscode",		  5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"flwno",	        2,			1,				7,			"" }, //流程存放序號
			{"accesscode",		5,			0,				0,			"" }, //接入號碼
			{"minlen",		    2,			1,				7,			"" }, //最小長度
			{"maxlen",		    2,			1,				7,			"" }, //最大長度
		},
	},//end of MsgId = 17

  //MsgId = 18 					功能描述：刪除接入號碼
  //消息名							消息參數個數
  {"addaccesscode",		  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"flwno",	        2,			1,				7,			"" }, //流程存放序號
			{"accesscode",		5,			0,				0,			"" }, //接入號碼
		},
	},//end of MsgId = 18

  //MsgId = 19 					功能描述：設置IVR服務自動保存日志標志
  //消息名							消息參數個數
  {"setivrsavemsgid",		  5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"alartid",	      2,			1,				6,			"" }, //告警聲音
			{"commid",	      2,			1,				6,			"" }, //通信消息
			{"debugid",	      2,			1,				6,			"" }, //內部調試信息
			{"drvid",	        2,			1,				6,			"" }, //底層驅動信息
		},
	},//end of MsgId = 19

  //MsgId = 20 					功能描述：設置FLW服務自動保存日志標志
  //消息名							消息參數個數
  {"setivrsavemsgid",		  5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"alartid",	      2,			1,				6,			"" }, //告警聲音
			{"commid",	      2,			1,				6,			"" }, //通信消息
			{"cmdsid",	      2,			1,				6,			"" }, //指令跟蹤信息
			{"varsd",	        2,			1,				6,			"" }, //變量跟蹤信息
		},
	},//end of MsgId = 20

  //MsgId = 21 					功能描述：修改INI配置文件參數
  //消息名							消息參數個數
  {"modifyiniparam",		  5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"inifilename",	  5,			0,				0,			"" }, //ini文件名
			{"sectname",	    5,			0,				0,			"" }, //section
			{"keyname",	      5,			0,				0,			"" }, //key
			{"value",	        5,			0,				0,			"" }, //參數
		},
	},//end of MsgId = 21

  //MsgId = 22 					功能描述：修改坐席參數
  //消息名							消息參數個數
  {"modifyseatdata",		4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"porttype",		  2,			1,				6,			"" }, //邏輯端口類型(固定端口坐席: 1-內線電話座席 2-內線電腦座席 3-數字線電話座席 4-數字線電腦座席) (不固定端口坐席: 5-遠程電話座席)
			{"portno",		    2,			1,				7,			"" }, //邏輯端口序號
			{"seatdata",		  5,			0,				0,			"" }, //參數：坐席號,類型(1-內線電話座席 2-內線電腦座席),坐席組號,默認登錄的話務員組號,默認登錄的話務員級別,默認登錄的話務員工號(工號為空時表示未登錄)
		},
	},//end of MsgId = 22

  //MsgId = 23 					功能描述：取中繼實時話務統計數據
  //消息名							消息參數個數
  {"gettrkcallcount",  2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開 2表示取當前統計數據
		},
	},//end of MsgId = 23

  //MsgId = 24 					功能描述：取坐席實時話務統計數據
  //消息名							消息參數個數
  {"getagcallcount",  2,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"openflag",	    2,			0,				0,			"0" }, //0表示停止 1表示打開 2表示取當前統計數據
		},
	},//end of MsgId = 24

  //MsgId = 25 					功能描述：啟動或停止自動流程
  //消息名							消息參數個數
  {"autoflwonoff",		  3,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"flwno",	        2,			1,				7,			"" }, //流程存放序號
			{"onoff",					2,			1,				6,			"" }, //1-啟動 0-停止
		},
	},//end of MsgId = 25

  //MsgId = 26 					功能描述：修改登錄密碼
  //消息名							消息參數個數
  {"modifypassword",	  4,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"username",			5,			0,				0,			"" }, //登錄用戶名:administrator,manager,operator
			{"oldpassword",		5,			0,				0,			"" }, //原登錄密碼
			{"newpassword",		5,			0,				0,			"" }, //新登錄密碼
		},
	},//end of MsgId = 26

  //MsgId = 27 					功能描述：修改CTILINK參數
  //消息名							消息參數個數
  {"modifyctilinkdata",	5,
		{					
			//AttrNameEng			VarType	MacroType	MacroNo	Default
			{"logid",					2,			0,				0,			"" }, //監控客戶端標志
			{"inifilename",	  5,			0,				0,			"" }, //ini文件名
			{"sectname",	    5,			0,				0,			"" }, //section
			{"keyname",	      5,			0,				0,			"" }, //key
			{"value",	        5,			0,				0,			"" }, //參數
		},
	},//end of MsgId = 27
};

