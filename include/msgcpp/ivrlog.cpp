//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//IVR服務器-->LOG監控客戶端
//MsgId	MsgName		AttrNum	Note
const VXML_RECV_MSG_CMD_RULE_STRUCT       IVRLOGMsgRuleArray[MAX_IVRLOGMSG_NUM]=
{
  //MsgId = 0 					功能描述：登錄結果
  //消息名							消息參數個數
	{"onlogin",						4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"serviceid",			2,			1,				6,			0,					0}, //登錄的服務類型（1-IVR 2-FLW 3-DB 4-CTRL）
			{"result",				2,			1,				6,			0,					0}, //登錄結果(0-成功 1-失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 0

  //MsgId = 1 					功能描述：ivr監控信息開關狀態
  //消息名							消息參數個數
	{"onivronoff",		    10,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"maxchnnum",			2,			1,				7,			0,					0}, //最大通道數
			{"maxagnnum",			2,			1,				7,			0,					0}, //最大坐席數
			{"queueinfo",			2,			1,				6,			0,					0}, //ACD分配隊列信息開關
			{"agentstatus",	  2,			1,				6,			0,					0}, //坐席的狀態信息開關
			{"chnstatus",	    2,			1,				6,			0,					0}, //通道的狀態信息開關
			{"ivrtracemsg",	  2,			1,				6,			0,					0}, //ivr日志跟蹤信息開關
			{"trkcallcount",	2,			1,				6,			0,					0}, //中繼話務實時統計數據開關
			{"agcallcount",	  2,			1,				6,			0,					0}, //坐席話務實時統計數據開關
			{"maxvopnnum",	  2,			1,				7,			0,					0}, //最大語音資源數
		},        
	},//end of MsgId = 1

  //MsgId = 2 					功能描述：flw監控信息開關狀態
  //消息名							消息參數個數
	{"onflwonoff",		3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"loadedflws",	  2,			1,				6,			0,					0}, //已加載的流程信息開關
			{"flwtracemsg",	  2,			1,				6,			0,					0}, //flw日志跟蹤信息開關
		},        
	},//end of MsgId = 2

  //MsgId = 3 					功能描述：ivr告警跟蹤信息
  //消息名							消息參數個數
	{"onivralarmmsg",			5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"alarmcode",		  2,			1,				6,			0,					0}, //告警編碼(1-255)(0-板卡初始化告警 1-交換機連接告警 2-數字中繼電路告警 3-模擬中繼電路告警 4-流程解析器節點告警 5-IVR語音節點告警)
			{"alarmlevel",		2,			1,				6,			0,					0}, //告警級別()
			{"alarmonoff",		2,			1,				6,			0,					0}, //告警開關狀態(0-告警消除 1-告警產生)
 			{"alarmmsg",			5,			0,				0,			0,					0}, //告警信息
		},        
	},//end of MsgId = 3

  //MsgId = 4 					功能描述：flw告警跟蹤信息
  //消息名							消息參數個數
	{"onflwalarmmsg",			5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"alarmcode",		  2,			1,				6,			0,					0}, //告警編碼(1-255)(0-連接IVR服務告警 1-連接數據庫網關告警 2-數據庫操作告警)
			{"alarmlevel",		2,			1,				6,			0,					0}, //告警級別
			{"alarmonoff",		2,			1,				6,			0,					0}, //告警開關狀態(0-告警消除 1-告警產生)
 			{"alarmmsg",			5,			0,				0,			0,					0}, //告警信息
		},        
	},//end of MsgId = 4

  //MsgId = 5 					功能描述：db告警跟蹤信息
  //消息名							消息參數個數
	{"ondbalarmmsg",			3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"alarmcode",		  2,			1,				6,			0,					0}, //告警編碼(1-255)(0-連接IVR服務告警 1-連接數據庫網關告警 2-數據庫操作告警)
			{"alarmlevel",		2,			1,				6,			0,					0}, //告警級別
			{"alarmonoff",		2,			1,				6,			0,					0}, //告警開關狀態(0-告警消除 1-告警產生)
 			{"alarmmsg",			5,			0,				0,			0,					0}, //告警信息
		},        
	},//end of MsgId = 5

  //MsgId = 6 					功能描述：收到ACD分配簡單隊列信息
  //消息名							消息參數個數
	{"ongetqueueinfo",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"queuenum",			2,			1,				7,			0,					0}, //隊列等待數
			{"queueinfo",			5,			0,				0,			0,					0}, //隊列信息:ACD編號1,主叫號碼1,排隊時間;ACD編號2,主叫號碼2,排隊時間;......
			{"waitacdnum",		2,			1,				7,			0,					0}, //等待分配空閑坐席數
			{"waitansnum",		2,			1,				7,			0,					0}, //已分配到空閑坐席等待應答數
		},
	},//end of MsgId = 6

  //MsgId = 7 					功能描述：收到坐席的狀態信息
  //消息名							消息參數個數
	{"ongetagentstatus",	22,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"agno",		  		2,			0,				0,			0,					0}, //序號
			{"seatno",				5,			0,				0,			0,					0}, //坐席分機號
			{"clientid",		  2,			0,				0,			0,					0}, //客戶端編號
			{"seattype",		  2,			0,				0,			0,					0}, //坐席類型
			{"seatgroupno",		2,			0,				0,			0,					0}, //坐席組號
			{"seatip",				5,			0,				0,			0,					0}, //坐席ip
			{"workerno",			2,			0,				0,			0,					0}, //話務員工號
			{"logintime",			5,			0,				0,			0,					0}, //話務員登錄時間
			{"workername",		5,			0,				0,			0,					0}, //話務員姓名
			{"groupno",				5,			0,				0,			0,					0}, //話務員組號
			{"svstate",				2,			1,				6,			0,					0}, //服務狀態
			{"disturbid",			2,			1,				6,			0,					0}, //0-可打擾 1-免打擾 2-等待IVR返回 3-外出值班模式
			{"leaveid",				2,			1,				6,			0,					0}, //在席(0)/離席(>0)標志
			{"anscount",			2,			1,				7,			0,					0}, //應答總次數
			{"noanscount",	  2,			1,				7,			0,					0}, //未應答總次數
			{"leavereason",		5,			0,				0,			0,					0}, //離席原因
			{"chantype",			2,			0,				0,			0,					0}, //綁定的通道類型
			{"channo",				2,			0,				0,			0,					0}, //綁定的通道號
			{"accountno",		  5,			0,				0,			0,					0}, //話務員登錄賬號
			{"departmentno",  5,			0,				0,			0,					0}, //話務員部門編號
			{"callmsg",       5,			0,				0,			0,					0}, //電話呼叫信息 inout;callerno;calltime
		},        
	},//end of MsgId = 7

  //MsgId = 8 					功能描述：收到通道的狀態信息
  //消息名							消息參數個數
	{"ongetchnstatus",	16,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",			2,			0,				0,			0,					0}, //ACD標志
			{"chnno",		  2,			0,				0,			0,					0}, //序號
			{"chtype",	  2,			0,				0,			0,					0}, //硬件通道類型
			{"chindex",		2,			0,				0,			0,					0}, //硬件通道號
			{"lgchntype",	2,			0,				0,			0,					0}, //業務通道類型
			{"lgchnno",		2,			0,				0,			0,					0}, //業務通道號
			{"hwstate",		2,			0,				0,			0,					0}, //硬件狀態
			{"lnstate",	  2,			0,				0,			0,					0}, //線路狀態
			{"ssstate",		2,			0,				0,			0,					0}, //信令狀態
			{"inout",			2,			0,				0,			0,					0}, //呼叫方向
 			{"callerno",	5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",	5,			0,				0,			0,					0}, //被叫號碼
 			{"calltime",	5,			0,				0,			0,					0}, //呼叫時間
 			{"anstime",	  5,			0,				0,			0,					0}, //應答時間
 			{"svstate",	  5,			0,				0,			0,					0}, //業務狀態信息
 			{"deviceid",	5,			0,				0,			0,					0}, //交換機對應的分機號
		},        
	},//end of MsgId = 8

  //MsgId = 9 					功能描述：取ivr日志跟蹤信息結果
  //消息名							消息參數個數
	{"ongetivrtracemsg",	2,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"tracemsg",			5,			0,				0,			0,					0}, //日志消息
		},
	},//end of MsgId = 9

  //MsgId = 10 					功能描述：取已加載的流程信息結果
  //消息名							消息參數個數
	{"ongetloadedflws",	  8,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"flwno",	        2,			1,				7,			0,					0}, //流程序號
			{"flwfilename",	  5,			0,				0,			0,					0}, //流程文件名
			{"groupno",	      2,			1,				7,			0,					0}, //組號
			{"funcno",	      2,			1,				7,			0,					0}, //流程功能號
			{"cmdcount",	    2,			1,				7,			0,					0}, //流程指令條數
			{"accesscodes",	  5,			0,				0,			0,					0}, //接入號碼
			{"loadtime",	    5,			0,				0,			0,					0}, //加載的時間
		},
	},//end of MsgId = 10

  //MsgId = 11 					功能描述：取flw日志跟蹤信息結果
  //消息名							消息參數個數
	{"ongetflwtracemsg",	2,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"tracemsg",			5,			0,				0,			0,					0}, //日志消息
		},
	},//end of MsgId = 11

  //MsgId = 12 					功能描述：強行釋放通道結果
  //消息名							消息參數個數
	{"onreleasechn",	    3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 12

  //MsgId = 13 					功能描述：強拆其他坐席的電話結果
  //消息名							消息參數個數
	{"onforceclear",	    3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 13

  //MsgId = 14 					功能描述：強制將其他話務員退出結果
  //消息名							消息參數個數
	{"onforcelogout",	    3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 14

  //MsgId = 15 					功能描述：強制其他坐席免打擾結果
  //消息名							消息參數個數
	{"onforcedisturb",	  3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 15

  //MsgId = 16 					功能描述：強制其他坐席空閑結果
  //消息名							消息參數個數
	{"onforceready",	    3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 16

  //MsgId = 17 					功能描述：修改路由參數結果
  //消息名							消息參數個數
	{"onmodifyroutedata",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 17

  //MsgId = 18 					功能描述：修改坐席參數結果
  //消息名							消息參數個數
	{"onmodifyseatdata",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 18

  //MsgId = 19 					功能描述：修改模擬外線接入號碼參數結果
  //消息名							消息參數個數
	{"onmodifyextline",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 19

  //MsgId = 20 					功能描述：加載業務流程結果
  //消息名							消息參數個數
	{"onloadflw",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 20

  //MsgId = 21 					功能描述：添加接入號碼結果
  //消息名							消息參數個數
	{"onaddaccesscode",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 21

  //MsgId = 22 					功能描述：刪除接入號碼結果
  //消息名							消息參數個數
	{"ondelaccesscode",	3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 22

  //MsgId = 23 					功能描述：當前設置的IVR服務自動保存日志標志
  //消息名							消息參數個數
	{"onsetivrsavemsgid",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"alartonoff",		2,			1,				6,			0,					0}, //設置的告警聲音開關標志
			{"commonoff",		  2,			1,				6,			0,					0}, //設置的通信消息開關標志
			{"debugonoff",		2,			1,				6,			0,					0}, //設置的內部調試信息開關標志
			{"drvonoff",		  2,			1,				6,			0,					0}, //設置的底層驅動信息開關標志
		},
	},//end of MsgId = 23

  //MsgId = 24 					功能描述：當前設置的flw服務自動保存日志標志
  //消息名							消息參數個數
	{"onsetflwsavemsgid",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"alartonoff",		2,			1,				6,			0,					0}, //設置的告警聲音開關標志
			{"commonoff",		  2,			1,				6,			0,					0}, //設置的通信消息開關標志
			{"cmdsonoff",		  2,			1,				6,			0,					0}, //設置的指令跟蹤開關標志
			{"varsonoff",		  2,			1,				6,			0,					0}, //設置的變量跟蹤開關標志
		},
	},//end of MsgId = 24

  //MsgId = 25 					功能描述：收到的ini參數
  //消息名							消息參數個數
	{"onrecviniparam",	  7,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"serviceid",			2,			1,				6,			0,					0}, //登錄的服務類型（1-IVR 2-FLW 3-DB 4-CTILINK）
			{"inifilename",		5,			0,				0,			0,					0}, //ini文件名
			{"sectname",		  5,			0,				0,			0,					0}, //section
			{"keyname",		    5,			0,				0,			0,					0}, //key
			{"value",		      5,			0,				0,			0,					0}, //參數
			{"overid",		    2,			1,				6,			0,					0}, //發送完標志
		},
	},//end of MsgId = 25

  //MsgId = 26 					功能描述：收到的ini文件行
  //消息名							消息參數個數
	{"onrecvtxtline",	    4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"serviceid",			2,			1,				6,			0,					0}, //登錄的服務類型（1-IVR 2-FLW 3-DB 4-CTILINK 5-/etc/sysconfig/network-scripts/ifcfg-eth0 6-/etc/sysconfig/network 7-/etc/resolv.conf）
			{"linemsg",		    5,			0,				0,			0,					0}, //ini文件名
			{"lineid",		    2,			1,				6,			0,					0}, //行標志（0-開始行 1-中間行 2-結束行）
		},
	},//end of MsgId = 26

  //MsgId = 27 					功能描述：修改坐席參數結果
  //消息名							消息參數個數
	{"onmodifyseatdata",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"agno",					2,			1,				7,			0,					0}, //坐席邏輯序號
			{"maxagnnum",			2,			1,				7,			0,					0}, //最大坐席數
			{"result",				2,			1,				6,			0,					0}, //結果
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},
	},//end of MsgId = 27

  //MsgId = 28 					功能描述：中繼話務實時統計數據
  //消息名							消息參數個數
	{"ontrkcountdata",	  3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"counttimeid",		2,			1,				6,			0,					0}, //統計時間點標志
			{"countdata",			2,			1,				6,			0,					0}, //統計數據，數據間用英文逗號,隔開
		},
	},//end of MsgId = 28

  //MsgId = 29 					功能描述：坐席話務實時統計數據
  //消息名							消息參數個數
	{"onagcountdata",	    3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"counttimeid",		2,			1,				6,			0,					0}, //統計時間點標志
			{"countdata",			2,			1,				6,			0,					0}, //統計數據，數據間用英文逗號,隔開
		},
	},//end of MsgId = 29

  //MsgId = 30 					功能描述：修改登錄密碼結果
  //消息名							消息參數個數
	{"onmodifypassword",						3,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"result",				2,			1,				6,			0,					0}, //結果(0-成功 1-失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 30

  //MsgId = 31 					功能描述：取語音機通道狀態結果
  //消息名							消息參數個數
	{"ongetvopstatus",	14,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",			2,			0,				0,			0,					0}, //0-ACD標志
			{"vopindex",	2,			0,				0,			0,					0}, //1-索引序號
			{"vopno",		  2,			0,				0,			0,					0}, //2-語音機序號
			{"vopchnno",	2,			0,				0,			0,					0}, //3-語音通道號
			{"vopchntype",2,			0,				0,			0,					0}, //4-資源類型
			{"chstyle",	  2,			0,				0,			0,					0}, //5-語音卡的通道類型
			{"chindex",		2,			0,				0,			0,					0}, //6-語音卡的通道號
			{"hwstate",		2,			0,				0,			0,					0}, //7-硬件狀態
			{"lnstate",	  2,			0,				0,			0,					0}, //8-線路狀態
			{"ssstate",		2,			0,				0,			0,					0}, //9-信令狀態
 			{"svstate",	  2,			0,				0,			0,					0}, //10-業務狀態信息 0-空閑 1-正在放音 2-正在錄音 3-正在發送傳真 4-正在接收傳真
 			{"deviceid",	5,			0,				0,			0,					0}, //11-連接的交換機的分機號
 			{"callerno",	5,			0,				0,			0,					0}, //12-主叫號碼
 			{"calltime",	5,			0,				0,			0,					0}, //13-呼叫時間
		},        
	},//end of MsgId = 31
  
  //MsgId = 32 					功能描述：坐席狀態實時統計數據
  //消息名							消息參數個數
	{"onseatstatusdata",	8,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"agno",		  		2,			0,				0,			0,					0}, //序號
			{"seatno",				5,			0,				0,			0,					0}, //坐席分機號
			{"workerno",			2,			0,				0,			0,					0}, //話務員工號
			{"workername",		5,			0,				0,			0,					0}, //話務員姓名
			{"curstatus",			2,			0,				0,			0,					0}, //當前狀態
			{"curtimelen",		2,			0,				0,			0,					0}, //當前狀態累計時長秒
			{"statusdata",	  5,			0,				0,			0,					0}, //統計數據，數據間用英文逗號,隔開
		},
	},//end of MsgId = 32
  
  //MsgId = 33 					功能描述：話務組狀態實時統計數據
  //消息名							消息參數個數
	{"ongroupstatusdata",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"groupno",				2,			0,				0,			0,					0}, //話務組號 0-表示為整個系統
			{"groupname",			5,			0,				0,			0,					0}, //話務組名稱
			{"statecount",	  5,			0,				0,			0,					0}, //狀態統計數據，數據間用英文逗號,隔開
			{"statusdata",	  5,			0,				0,			0,					0}, //話務統計數據，數據間用英文逗號,隔開
		},
	},//end of MsgId = 33

  //MsgId = 34 					功能描述：流程會話session實時統計數據
  //消息名							消息參數個數
	{"onsessionscount",	  4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"flwno",	        2,			1,				7,			0,					0}, //流程序號 65535-表示所有的
			{"onlines",	      2,			1,				7,			0,					0}, //在線數
			{"autoid",	      2,			1,				6,			0,					0}, //自動流程啟動標志
		},
	},//end of MsgId = 34

  //MsgId = 35 					功能描述：本機在雙機熱備系統中的運行狀態
  //消息名							消息參數個數
	{"onharunstatus",	  5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"hosttype",			2,			1,				6,			0,					0}, //本機類型：0-主機 1-備機
			{"activeid",			2,			1,				6,			0,					0}, //本機狀態：0-為待機狀態,1-為工作狀態,2-接管狀態
			{"switchtime",	  5,			0,				0,			0,					0}, //切換的時間
			{"reason",	  		5,			0,				0,			0,					0}, //切換原因
		},
	},//end of MsgId = 35

  //MsgId = 36 					功能描述：查詢中繼群占用數狀態
  //消息名							消息參數個數
	{"onquerytrunkgroup",	7,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"switchid",			2,			1,				6,			0,					0}, //交換機節點標志
			{"trunkgroup",	  5,			0,				0,			0,					0}, //中繼線路群
			{"idletrunks",	  2,			1,				7,			0,					0}, //空閑中繼線路數
			{"usedtrunks",	  2,			1,				7,			0,					0}, //占用中繼線路數
			{"blocktrunks",	  2,			1,				7,			0,					0}, //阻塞中繼線路數
			{"trunkparam",	  5,			0,				0,			0,					0}, //中繼擴展參數
		},
	},//end of MsgId = 36

  //MsgId = 37 					功能描述：查詢ACD數結果
  //消息名							消息參數個數
	{"onqueryacdsplit",	  7,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"switchid",			2,			1,				6,			0,					0}, //交換機節點標志
			{"acdsplit",	    5,			0,				0,			0,					0}, //ACD群組
			{"availagents",	  2,			1,				7,			0,					0}, //有效的坐席數
			{"callsinqueue",	2,			1,				7,			0,					0}, //隊列等待數
			{"loginagents",	  2,			1,				7,			0,					0}, //登錄坐席數
			{"acdparam",	    5,			0,				0,			0,					0}, //ACD擴展參數
		},
	},//end of MsgId = 37
};
