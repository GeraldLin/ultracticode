//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

const VXML_SEND_MSG_CMD_RULE_STRUCT       IVRSWTMsgRuleArray[MAX_IVRSWTMSG_NUM]=
{
	//start 0
	{"connect",			5,	//連接CTILinkService
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"switchtype",	2,	1,	6,	"1"}, //1-avaya ipo 2-avaya sxxxx
			{"logid",				2,	1,	1,	"" },
			{"password",		5,	0,	0,	"" },
		},
	},//end 0

	//start 1
	{"setportstatus",	  5,	//設置端口狀態
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"status",		  2,	1,	6,	"1"},
		},
	},//end 1

	//start 2
	{"setagentstatus",	8,	//設置坐席狀態
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"agentid",		  5,	0,	0,	"" },
			{"groupid",		  5,	0,	0,	"" },
			{"password",	  5,	0,	0,	"" },
			{"status",		  2,	1,	6,	"0"}, //AM_LOG_IN = 0,AM_LOG_OUT = 1,AM_NOT_READY = 2,AM_READY = 3,AM_WORK_NOT_READY = 4,AM_WORK_READY = 5,AG_AM_LOG_IN_NOTREADY = 9(登錄后直接進入NotReady狀態),AG_AM_LOG_IN_ACW = 10(登錄后直接進入整理狀態),
		},
	},//end 2

	//start 3
	{"setroomstatus",		5,	//設置房間狀態
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"roomid",		  5,	0,	0,	"" },
			{"status",		  2,	1,	6,	"1"}, //checkin,checkout
		},
	},//end 3

	//start 4
	{"setlampstatus",		5,	//設置留言指示燈
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"status",		  2,	1,	6,	"1"}, //>=1-亮 0-滅
		},
	},//end 4

	//start 5
	{"setwakestatus",		5,	//設置叫醒
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"waketime",		5,	0,	0,	"" },
		},
	},//end 5

	//start 6
	{"setcalllevel",		5,	//設置呼出權限
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"calllevel",	  2,	1,	6,	"" },
		},
	},//end 6

	//start 7
	{"settranphone",		6,	//設置轉移號碼
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"trantype",		2,	1,	6,	"1"}, //0-取消所有轉移 1-無條件轉移 2-遇忙轉移 3-久叫不應轉移 4-同振綁定 5-遇忙和久叫不應轉移 10-取消無條件轉移 20-取消遇忙轉移 30-取消久叫不應轉移 50-取消遇忙和久叫不應轉移
			{"tranphone",		5,	0,	0,	"" },
		},
	},//end 7

	//start 8
	{"answercall",			5,	//應答來話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"1"},
		},
	},//end 8

	//start 9
	{"refusecall",			5,	//拒絕來話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"1"},
		},
	},//end 9

	//start 10
	{"makecall",				9,	//發起呼叫
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"callid",		  2,	1,	8,	"" },
			{"callerid",		5,	0,	0,	"" },
			{"calledid",		5,	0,	0,	"" },
			{"routeno",		  2,	1,	7,	"1"},
			{"calldata",		5,	0,	0,	"" },
		},
	},//end 10

	//start 11
	{"bandcall",				7,	//將來中繼來話綁定到分機
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"1"},
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 11

	//start 12
	{"bandvoice",				6,	//將來中繼來話綁定到提示語音
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"1"},
			{"voiceid",		  2,	1,	6,	"1"},
		},
	},//end 12

	//start 13
	{"holdcall",				5,	//保持電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"1"},
		},
	},//end 13

	//start 14
	{"unholdcall",			5,	//取消保持電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"1"},
		},
	},//end 14

	//start 15
	{"transfercall",		9,	//轉接電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
			{"trantype",    2,	1,	6,	"1"}, //轉接方式：1-協商轉接 2-快速轉接 3-會議轉接
			{"calldata",		5,	0,	0,	"" },
		},
	},//end 15

	//start 16
	{"stoptransfer",		5,	//停止轉接電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
		},
	},//end 16

	//start 17
	{"swapcall",				5,	//穿梭通話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
		},
	},//end 17

	//start 18
	{"pickupcall",			7,	//代接電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 18

	//start 19
	{"breakincall",			7,	//強插通話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 19

	//start 20
	{"listencall",			7,	//監聽電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 20

	//start 21
	{"removecall",			7,	//強拆電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 21

	//start 22
	{"threeconf",				6,	//三方通話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"heldconnid",  2,	1,	8,	"" },
			{"actconnid",   2,	1,	8,	"" },
		},
	},//end 22

	//start 23
	{"releasecall",			5,	//釋放呼叫
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
		},
	},//end 23

	//start 24
	{"hangup",					4,	//分機掛機
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
		},
	},//end 24

	//start 25
	{"putacdqueuecall",	6,	//將電話保持到話務臺隊列
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"queueno",		  2,	1,	6,	"" },
		},
	},//end 25

	//start 26
	{"getacdqueuecall",	6,	//從話務臺隊列提取電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"queueno",		  2,	1,	6,	"" },
		},
	},//end 26

	//start 27
	{"sendswitchcmd",	  5,	//發送交換機操作指令
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"cmdid",		    2,	1,	6,	"" }, //1-拍叉簧 2-保持 3-三方會議 4-切換通話 5-掛斷
		},
	},//end 27

	//start 28
	{"senddtmf",	      6,	//發送DTMF指令
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"dtmf",		    5,	0,	0,	"" },
		},
	},//end 28
	
	//start 29
	{"takeovercall",		7,	//接管電話
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 29
	
	//start 30
	{"redirectcall",		7,	//電話重新定向振鈴
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
		},
	},//end 30
	
	//start 31
	{"addtoconf",				9,	//加入會議
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
			{"destconnid",  2,	1,	8,	"" },
			{"confid",      2,	1,	8,	"" },
		},
	},//end 31
	
	//start 32
	{"removefromconf",	9,	//退出會議
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",      2,	1,	8,	"" },
			{"desportid",		2,	1,	7,	"" },
			{"desdeviceid", 5,	0,	0,	"" },
			{"destconnid",  2,	1,	8,	"" },
			{"confid",      2,	1,	8,	"" },
		},
	},//end 32
	
	//start 33
	{"sendivrstatus",	  4,	//發送IVR節點的狀態到交換機
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"ivrhostid",		5,	0,	0,	"" },
			{"status",			2,	1,	6,	"0" }, //VOC語音機狀態 0-工作 1-故障
		},
	},//end 33
	
	//start 34
	{"sendswitchover",	6,	//發送主備切換標志
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"hosttype",		2,	1,	6,	"" }, //本機類型：0-主機 1-備機
			{"activeid",		2,	1,	6,	"" }, //本機狀態：0-為待機狀態,1-為工作狀態,2-接管狀態
			{"reason",			2,	1,	6,	"" },
			{"reasonmsg",		5,	0,	0,	"" },
		},
	},//end 34

	//ver:3.2版本增加
	//start 35
	{"recordfile",			9,	//文件錄音 //ver:3.2版本修改
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"filename", 		5,	0,	0,	"" }, //錄音文件名
			{"recdlength",  2,	1,	7,	"0"}, //錄音長度秒
			{"recdparam",   2,	1,	7,	"0"}, //錄音參數
			{"rule", 		    5,	0,	0,	"" }, //附加規則參數
		},
	},//end 35

	//start 36
	{"stoprecord",			6,	//停止錄音
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"stopflag",    2,	1,	7,	"0"},
		},
	},//end 36
	
	//start 37
	{"playfile",			  8,	//文件放音
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"filename", 		5,	0,	0,	"" }, //放音文件名
			{"playparam",   2,	1,	7,	"0"}, //放音參數
			{"rule", 		    5,	0,	0,	"" }, //附加規則參數
		},
	},//end 37

	//start 38
	{"playindex",			  8,	//索引放音
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"index", 		  2,	1,	7,	"" }, //索引號
			{"playparam",   2,	1,	7,	"0"}, //放音參數
			{"rule", 		    5,	0,	0,	"" }, //附加規則參數
		},
	},//end 38

	//start 39
	{"stopplay",			  6,	//停止放音
		{
			{"sessionid",		2,	0,	0,	"" },
			{"switchid",		2,	1,	6,	"1"},
			{"portid",		  2,	1,	7,	"" },
			{"deviceid",		5,	0,	0,	"" },
			{"connid",		  2,	1,	8,	"" },
			{"stopflag",    2,	1,	7,	"0"},
		},
	},//end 39
};
