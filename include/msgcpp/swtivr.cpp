//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

const VXML_RECV_MSG_CMD_RULE_STRUCT       SWTIVRMsgRuleArray[MAX_SWTIVRMSG_NUM]=
{
	//start 0
	{"onconnect",		5,	//連接CTILinkService結果
		{
			{"sessionid",		2,	0,	0,	1,	1}, //會話標志
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"switchtype",  2,	1,	4,	0,	0}, //交換機類型 //1-avaya ipo 2-avaya sxxxx
			{"result",			2,	1,	6,	0,	0}, //連接結果
			{"errorbuf",		5,	0,	0,	0,	0},
		},
	}, //end 0
	
	//start 1
	{"onlinkstatus",		5,	//CTILink接口與交換機的連接狀態
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"switchid",		2,	1,	6,	0,	0}, //交換機節點標志
			{"linkid",			2,	1,	6,	0,	0}, //CTILink Id標志 與邏輯端口號相同
			{"status",			2,	1,	6,	0,	0}, //連接狀態值 0-斷開 1-連接
			{"errorbuf",		5,	0,	0,	0,	0}, //傳遞對應的端口分機號
		},
	}, //end 1
	
	//start 2
	{"ongetdevnum",			3,	//返回交換機設備配置數量
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portnum",	    2,	1,	7,	0,	0}, //配置的端口總數
			//{"trunknum",	  2,	1,	7,	0,	0}, //配置的中繼端口數
			//{"extnnum",	    2,	1,	7,	0,	0}, //配置的分機端口數
		},
	},//end 2

	//start 3
	{"ongetdevparam",		11,	//返回交換機設備配置數量
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"chstyle",		  2,	1,	6,	0,	0}, //
			{"chstyleindex",2,	1,	7,	0,	0}, //
			{"chtype",		  2,	1,	6,	0,	0}, //
			{"chindex",		  2,	1,	7,	0,	0}, //
			{"lgchtype",		2,	1,	6,	0,	0}, //
			{"lgchindex",		2,	1,	7,	0,	0}, //
			{"hosttype",		2,	1,	6,	0,	0}, //
		},
	},//end 3
	
	//start 4
	{"onportstatus",	  5,	//端口狀態事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"status",	    2,	1,	6,	0,	0}, //端口狀態值：0-空閑 1-摘機 2-振鈴 3-通話 4-忙音
		},
	},//end 4
	
	//start 5
	{"onagentstatus",		5,	//坐席狀態事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的工號
			{"status",	    2,	1,	6,	0,	0}, //在席狀態值：AM_LOG_IN = 0,AM_LOG_OUT = 1,AM_NOT_READY = 2,AM_READY = 3,AM_WORK_NOT_READY = 4,AM_WORK_READY = 5 FAIL=9
		},
	},//end 5
	
	//start 6
	{"onroomstatus",		5,	//房間狀態事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"status",	    2,	1,	6,	0,	0}, //狀態值
		},
	},//end 6
	
	//start 7
	{"onlampevent",			5,	//留言燈狀態事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"status",	    2,	1,	6,	0,	0}, //狀態值
		},
	},//end 7
	
	//start 8
	{"onwakeevent",			5,	//叫醒狀態事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"status",	    2,	1,	6,	0,	0}, //狀態值
		},
	},//end 8
	
	//start 9
	{"onacdqueueevent",	5,	//話務臺隊列事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"queue",	      5,	0,	0,	0,	0}, //隊列信息串
		},
	},//end 9
	
	//start 10
	{"onbillevent",			3,	//收到呼叫話單事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"billrecord",	5,	0,	0,	0,	0}, //話單信息串
		},
	},//end 10

	//start 11
	{"oncallevent",			17,	//呼叫事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //1 交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //2 端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //3 配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //4 呼叫會話id
			{"direct",			2,	1,	6,	0,	0}, //5 呼叫方向 1-電話呼入 2-內線呼出
			{"desportid",		2,	1,	7,	0,	0}, //6 接收呼叫的邏輯通道號
			{"callerno",	  5,	0,	0,	0,	0}, //7 主叫號碼
			{"calledno",	  5,	0,	0,	0,	0}, //8 被叫號碼
			{"orgcallerno",	5,	0,	0,	0,	0}, //9 原主叫號碼
			{"orgcalledno",	5,	0,	0,	0,	0}, //10 原被叫號碼
			{"status",	  	2,	1,	7,	0,	0}, //11 呼叫狀態 1-電話呼入振鈴 2-電話呼入應答 3-呼出聽撥號音 4-呼出正在撥號 5-呼出聽回鈴音 6-呼出聽忙音
			{"calldata",	  5,	0,	0,	0,	0}, //12 傳遞的呼叫數據
			{"customphone",	5,	0,	0,	0,	0}, //13 傳遞的客戶號碼
			{"calltype",	  2,	1,	7,	0,	0}, //14 呼叫類型 1-直接呼入 2-直接呼出 3-轉移呼入 4-代接呼入 5-強插監聽呼出
		},
	},//end 11
	
	//start 12
	{"oncallrecvsam",		12,	//收到后續地址事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"callerno",	  5,	0,	0,	0,	0}, //主叫號碼
			{"calledno",	  5,	0,	0,	0,	0}, //被叫號碼
			{"samtype",	    2,	1,	6,	0,	0}, //后續地址類型：0-收到主叫號碼 1-呼入收到撥的被叫號碼 2-呼出收到撥的轉接號碼 3-呼出收到撥的外線號碼
			{"calldata",	  5,	0,	0,	0,	0}, //傳遞的呼叫數據
			{"customphone",	5,	0,	0,	0,	0}, //傳遞的客戶號碼
		},
	},//end 12
	
	//start 13
	{"oncallrecvacm",		9,	//收到后續地址全事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"callerno",	  5,	0,	0,	0,	0}, //主叫號碼
			{"calledno",	  5,	0,	0,	0,	0}, //被叫號碼
		},
	},//end 13
	
	//start 14
	{"oncallring",		  8,	//收到振鈴事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"result",	    2,	1,	6,	0,	0}, //轉呼結果：1-入局呼叫轉到分機振鈴 2-分機呼叫分機振鈴 3-分機呼出振鈴
		},
	},//end 14
	
	//start 15
	{"oncallfail",		  8,	//收到呼叫失敗事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"result",	    2,	1,	6,	0,	0}, //入局呼叫轉分機結果 1-轉到忙分機 2-分機無人應答 3-分機不存在
		},
	},//end 15
	
	//start 16
	{"oncallack",				14,	//收到應答事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //1交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //2端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //3配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //4呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //5關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //6關聯的號碼
			{"result",	    2,	1,	6,	0,	0}, //7轉呼結果：1-入局呼叫轉到分機應答 2-分機呼叫分機應答 3-分機呼出應答
			{"callerno",	  5,	0,	0,	0,	0}, //8主叫號碼
			{"calledno",	  5,	0,	0,	0,	0}, //9被叫號碼
			{"calldata",	  5,	0,	0,	0,	0}, //10傳遞的呼叫數據
			{"customphone",	5,	0,	0,	0,	0}, //11傳遞的客戶號碼
		},
	},//end 16
	
	//start 17
	{"oncallrelease",		8,	//收到釋放事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"result",	    2,	1,	6,	0,	0}, //釋放標志 0-通話的對方釋放 1-本方掛機
		},
	},//end 17
	
	//start 18
	{"onanswercall",		6,	//應答來話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 18
	
	//start 19
	{"onrefusecall",		6,	//拒絕來話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 19
	
	//start 20
	{"onmakecall",		  7,	//發起呼叫結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"callid",	    2,	1,	7,	0,	0}, //呼叫會話id
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 20
	
	//start 21
	{"onbandcall",		  6,	//將來中繼來話綁定到分機結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 21
	
	//start 22
	{"onbandvoice",		  6,	//將來中繼來話綁定到提示語音結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 22
	
	//start 23
	{"onholdcall",		  6,	//保持電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 23
	
	//start 24
	{"onunholdcall",		6,	//取消保持電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 24
	
	//start 25
	{"ontransfercall",  8,	//轉接電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"reault",	    2,	1,	7,	0,	0}, //結果: 0-轉接成功 1-轉接失敗 2-正在振鈴 3-轉接撥號失敗 9-盲轉成功
		},
	},//end 25
	
	//start 26
	{"onstoptransfer",  6,	//停止轉接電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 26
	
	//start 27
	{"onswapcall",	    6,	//穿梭通話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 27
	
	//start 28
	{"onpickupcall",		6,	//代接電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 28
	
	//start 29
	{"onbreakincall",		6,	//強插通話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 29
	
	//start 30
	{"onlistencall",		6,	//監聽電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 30
	
	//start 31
	{"onremovecall",		6,	//強拆電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 31
	
	//start 32
	{"onthreeconf",		  6,	//三方通話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 32
	
	//start 33
	{"onconsolehold",		6,	//將電話保持到話務臺隊列結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 33
	
	//start 34
	{"onconsolepickup",	6,	//從話務臺隊列提取電話結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 34
	
	//start 35
	{"onsendswitchcmd",	5,	//發送交換機操作指令結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"reault",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 35
	
	//start 36
	{"oncallconnected",		  8,	//收到呼叫通話連接事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"desportid",	  2,	1,	7,	0,	0}, //關聯的端口編號
			{"desdeviceid", 5,	0,	0,	0,	0}, //關聯的號碼
			{"result",	    2,	1,	6,	0,	0}, //結果
		},
	},//end 36
	
	//ver:3.2版本增加	
	//start 37
	{"onstartrecord",		9,	//錄音結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"result",	    2,	1,	7,	0,	0}, //結果
			{"reason",			5,	0,	0,	0,	0}, //原因
			{"recbytes",		2,	0,	0,	0,	0}, //錄音字節數
			{"rectimelen",	2,	0,	0,	0,	0}, //錄音時長秒
		},
	},//end 37

	//start 38
	{"onplayresult",		7,	//放音結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"result",	    2,	1,	7,	0,	0}, //結果
			{"reason",			5,	0,	0,	0,	0}, //原因
		},
	},//end 38
	
	//start 39
	{"onrecvdtmf",		  7,	//收碼結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //配置的通道對應的分機號或外線號碼
			{"connid",	    2,	1,	8,	0,	0}, //呼叫會話id
			{"result",	    2,	1,	7,	0,	0}, //結果
			{"dtmf",			  5,	0,	0,	0,	0}, //DTMF碼
		},
	},//end 39
	
	//ver:3.3版本增加
	//start 40
	{"onqueryacdsplit",	7,	//查詢ACD數結果
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"switchid",		2,	1,	6,	0,	0}, //交換機節點標志
			{"acdsplit",		5,	0,	0,	0,	0}, //ACD群組
			{"availagents",	2,	1,	7,	0,	0}, //有效的坐席數
			{"callsinqueue",2,	1,	7,	0,	0}, //隊列等待數
			{"loginagents", 2,	1,	7,	0,	0}, //登錄坐席數
			{"acdparam",	  5,	0,	0,	0,	0}, //ACD擴展參數
		},
	}, //end 40
	
	//start 41
	{"onagentloggedon",		8,	//agent登錄交換機事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //登錄的分機號
			{"agentid",     5,	0,	0,	0,	0}, //登錄的話務員工號AgentID
			{"groupid",     5,	0,	0,	0,	0}, //登錄的組號
			{"password",    5,	0,	0,	0,	0}, //登錄的密碼
			{"status",	    2,	1,	6,	0,	0}, //在席狀態值：AM_LOG_IN = 0,AM_LOG_OUT = 1,AM_NOT_READY = 2,AM_READY = 3,AM_WORK_NOT_READY = 4,AM_WORK_READY = 5 FAIL=9
		},
	},//end 41
	
	//start 42
	{"onagentloggedoff",		7,	//agent登出交換機事件
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //登出的分機號
			{"agentid",     5,	0,	0,	0,	0}, //登出的話務員工號AgentID
			{"groupid",     5,	0,	0,	0,	0}, //登出的組號
			{"status",	    2,	1,	6,	0,	0}, //在席狀態值：AM_LOG_IN = 0,AM_LOG_OUT = 1,AM_NOT_READY = 2,AM_READY = 3,AM_WORK_NOT_READY = 4,AM_WORK_READY = 5 FAIL=9
		},
	},//end 42

	//start 43
	{"onsettranphone",	8,	//設置轉移號碼結果
		{
			{"sessionid",		2,	0,	0,	0,	0},
			{"switchid",		2,	1,	4,	0,	0}, //交換機節點標志
			{"portid",			2,	1,	7,	0,	0}, //端口編號
			{"deviceid",    5,	0,	0,	0,	0}, //分機號
			{"forwardtype", 5,	0,	0,	0,	0}, //轉移類型
			{"forwardonoff",2,	1,	6,	0,	0}, //轉移開關
			{"forwarddeviceno",5,	0,	0,	0,	0}, //轉移的目的號碼
			{"result",	    2,	1,	7,	0,	0}, //結果
		},
	},//end 43
};
