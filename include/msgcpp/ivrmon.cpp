//注意命令字符串定義長度不得超過 MAX_CMD_NAME_LEN-1
//注意屬性字符串定義長度不得超過 MAX_ATTRIBUTE_LEN-1

//IVR服務器-->監控坐席
//MsgId	MsgName		AttrNum	Note
const VXML_RECV_MSG_CMD_RULE_STRUCT       IVRMONMsgRuleArray[MAX_IVRMONMSG_NUM]=
{
  //MsgId = 0 					功能描述：坐席登錄結果
  //消息名							消息參數個數
	{"onmonitorlogin",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 0

  //MsgId = 1 					功能描述：監聽電話結果
  //消息名							消息參數個數
	{"onlistencall",		  4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 1

  //MsgId = 2 					功能描述：強插電話結果
  //消息名							消息參數個數
	{"onbreakincall",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 2

  //MsgId = 3 					功能描述：強接電話結果
  //消息名							消息參數個數
	{"ontakeovercall",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 3

  //MsgId = 4 					功能描述：發送文字消息結果
  //消息名							消息參數個數
	{"onsendmessage",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 4

  //MsgId = 5 					功能描述：強拆電話結果
  //消息名							消息參數個數
	{"onforceclear",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 5

  //MsgId = 6 					功能描述：強制將其他話務員退出結果
  //消息名							消息參數個數
	{"onforcelogout",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 6

  //MsgId = 7 					功能描述：強制坐席示忙結果
  //消息名							消息參數個數
	{"onforcebusy",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 7

  //MsgId = 8 					功能描述：強制坐席空閑結果
  //消息名							消息參數個數
	{"onforceready",		4,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"monid",					2,			1,				6,			0,          0}, //坐席統一邏輯編號
			{"result",				2,			1,				6,			0,					0}, //返回的結果(0-成功，其他值失敗)
 			{"errorbuf",			5,			0,				0,			0,					0}, //錯誤信息
		},        
	},//end of MsgId = 8

  //MsgId = 9 					功能描述：收到坐席的狀態信息
  //消息名							消息參數個數
	{"ongetagentstatus",	18,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //0 ACD標志
			{"agno",		  		2,			0,				0,			0,					0}, //1 序號
			{"seatno",				5,			0,				0,			0,					0}, //2 坐席分機號
			{"clientid",		  2,			0,				0,			0,					0}, //3 客戶端編號
			{"seattype",		  2,			0,				0,			0,					0}, //4 坐席類型
			{"seatgroupno",		2,			0,				0,			0,					0}, //5 坐席組號
			{"seatip",				5,			0,				0,			0,					0}, //6 坐席ip
			{"workerno",			2,			0,				0,			0,					0}, //7 話務員工號
			{"logintime",			5,			0,				0,			0,					0}, //8 話務員登錄時間
			{"workername",		5,			0,				0,			0,					0}, //9 話務員姓名
			{"groupno",				5,			0,				0,			0,					0}, //10 話務員組號
			{"svstate",				2,			1,				6,			0,					0}, //11 服務狀態
			{"disturbid",			2,			1,				6,			0,					0}, //12 0-可打擾 1-免打擾
			{"leaveid",				2,			1,				6,			0,					0}, //13 在席(0)/離席(>0)標志
			{"anscount",			2,			1,				7,			0,					0}, //14 應答總次數
			{"noanscount",	  2,			1,				7,			0,					0}, //15 未應答總次數
			{"leavereason",		5,			0,				0,			0,					0}, //16 離席原因
 			{"callmsg",				5,			0,				0,			0,					0}, //17 電話呼叫信息 inout;callerno;calltime
		},        
	},//end of MsgId = 9

  //MsgId = 10 					功能描述：收到通道的狀態信息
  //消息名							消息參數個數
	{"ongetchnstatus",	16,
		{
			//AttrNameEng	VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",			2,			0,				0,			0,					0}, //ACD標志
			{"chnno",		  2,			0,				0,			0,					0}, //序號
			{"chtype",	  2,			0,				0,			0,					0}, //硬件通道類型
			{"chindex",		2,			0,				0,			0,					0}, //硬件通道號
			{"lgchntype",	2,			0,				0,			0,					0}, //業務通道類型
			{"lgchnno",		2,			0,				0,			0,					0}, //業務通道號
			{"hwstate",		2,			0,				0,			0,					0}, //硬件狀態
			{"lnstate",	  2,			0,				0,			0,					0}, //線路狀態
			{"ssstate",		2,			0,				0,			0,					0}, //信令狀態
			{"inout",			2,			0,				0,			0,					0}, //呼叫方向
 			{"callerno",	5,			0,				0,			0,					0}, //主叫號碼
 			{"calledno",	5,			0,				0,			0,					0}, //被叫號碼
 			{"calltime",	5,			0,				0,			0,					0}, //呼叫時間
 			{"anstime",	  5,			0,				0,			0,					0}, //應答時間
 			{"svstate",	  5,			0,				0,			0,					0}, //業務狀態信息
 			{"deviceid",	5,			0,				0,			0,					0}, //交換機對應的分機號
		},        
	},//end of MsgId = 10

	//start 11
	{"onswttrkgrpstatus",	6,	//收到的交換機中繼組占用狀態
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"switchid",		2,	1,	6,	0,	0}, //交換機節點標志
			{"trunkgroup",	5,	0,	0,	0,	0}, //中繼線路群
			{"idletrunks",	2,	1,	7,	0,	0}, //空閑中繼線路數
			{"usedtrunks",	2,	1,	7,	0,	0}, //占用中繼線路數
			{"trunkparam",	5,	0,	0,	0,	0}, //中繼擴展參數
		},
	}, //end 11
	
	//start 12
	{"onswtacdgrpstatus",	7,	//收到的交換機ACD組登錄占用狀態
		{
			{"sessionid",		2,	0,	0,	1,	1},
			{"switchid",		2,	1,	6,	0,	0}, //交換機節點標志
			{"acdsplit",		5,	0,	0,	0,	0}, //ACD群組
			{"availagents",	2,	1,	7,	0,	0}, //有效的坐席數
			{"callsinqueue",2,	1,	7,	0,	0}, //隊列等待數
			{"loginagents", 2,	1,	7,	0,	0}, //登錄坐席數
			{"acdparam",	  5,	0,	0,	0,	0}, //ACD擴展參數
		},
	}, //end 12

  //MsgId = 13 					功能描述：收到的平臺ACD分配簡單隊列信息
  //消息名							消息參數個數
	{"ongetqueueinfo",	13,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"acdid",					2,			0,				0,			0,					0}, //ACD標志
			{"queuenum",			2,			1,				7,			0,					0}, //隊列等待數
			{"queueinfo",			5,			0,				0,			0,					0}, //隊列信息:ACD編號1,主叫號碼1,排隊時間,等待狀態acdState,已分配的坐席號;ACD編號2,主叫號碼2,排隊時間;......
			{"waitacdnum",		2,			1,				7,			0,					0}, //等待分配空閑坐席數
			{"waitansnum",		2,			1,				7,			0,					0}, //已分配到空閑坐席等待應答數
		},
	},//end of MsgId = 5
  
  //MsgId = 14 					功能描述：坐席狀態實時統計數據
  //消息名							消息參數個數
	{"onseatstatusdata",	8,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"agno",		  		2,			0,				0,			0,					0}, //序號
			{"seatno",				5,			0,				0,			0,					0}, //坐席分機號
			{"workerno",			2,			0,				0,			0,					0}, //話務員工號
			{"workername",		5,			0,				0,			0,					0}, //話務員姓名
			{"curstatus",			2,			0,				0,			0,					0}, //當前狀態
			{"curtimelen",		2,			0,				0,			0,					0}, //當前狀態累計時長秒
			{"statusdata",	  5,			0,				0,			0,					0}, //統計數據，數據間用英文逗號,隔開
		},
	},//end of MsgId = 14
  
  //MsgId = 15 					功能描述：話務組狀態實時統計數據
  //消息名							消息參數個數
	{"ongroupstatusdata",	5,
		{
			//AttrNameEng			VarType	MacroType	MacroNo	ReturnType	ReturnVar
			{"logid",					2,			0,				0,			0,					0}, //監控客戶端標志
			{"groupno",				2,			0,				0,			0,					0}, //話務組號 0-表示為整個系統
			{"groupname",			5,			0,				0,			0,					0}, //話務組名稱
			{"statecount",	  5,			0,				0,			0,					0}, //狀態統計數據，數據間用英文逗號,隔開
			{"statusdata",	  5,			0,				0,			0,					0}, //話務統計數據，數據間用英文逗號,隔開
		},
	},//end of MsgId = 15
};
