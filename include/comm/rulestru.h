//系統變量名結構定義
#ifndef __rulestruH__
#define __rulestruH__

typedef struct
{
    const CH *VarName;//[MAX_VARNAME_LEN]; //變量名稱
    UC AreaType; //變量作用域: 1系統全局變量 2-流程全局變量 3-流程局部變量 4-系統固定變量名 5-流程固定變量名
    UC VarType; //變量類型: 0-any 1-bool 2-int 3-float 4-char 5-string
    US Index; //邏輯序號

}VXML_SYSVAR_STRUCT;

//函數參數語法結構定義
typedef struct
{    
    const CH *FuncName;//[MAX_VARNAME_LEN]; //函數名
    UC ParaNums; //參數個數
    CH ParaType[MAX_FUNC_PARA_NUM]; //參數值類型：0-any 1-bool 2-int 3-float 4-char 5-string
 //   UC ParaValues[MAX_FUNC_PARA_NUM][MAX_VAR_DATA_LEN]; //參數值

}VXML_FUNC_RULE_STRUCT;


//---------------------------------------------------------------------------
//流程腳本規則結構定義
//---------------------------------------------------------------------------


//屬性規則結構定義
typedef struct
{
    US Address; //指令屬性值在存放的地址
    US Size; //占用地址長度
    const CH *AttrNameEng;//[MAX_ATTRIBUTE_LEN]; //指令屬性英文名稱
    const CH *AttrNameChn;//[MAX_ATTRIBUTE_LEN]; //指令屬性中文名稱
    UC VarType; //屬性值類型
                //0未定義(OperType=1)
                //1布爾型(OperType=0,1,2)
                //2整數(OperType=0,1,2,3,4,5,6)
                //3浮點數(OperType=0,1,2)
                //4字符(OperType=0,1,2,)
                //5字符串(OperType=0,1,2,3,4,5,6)
                //6字符串運算表達式(包括常量、變量、系統變量)
                //7邏輯運算表達式(包括常量、變量、系統變量)
                //8算術運算表達式(包括常量、變量、系統變量)、
                //9 SQL語句
                //10 返回值存儲變量名(為空時表示不賦值)
                //11 自定義事件名稱
    UC OperType; //屬性賦值方式：
                //0常量、
                //1可被賦值變量(包括自定義變量、間接地址變量、系統全局變量)
                //2常量與可被賦值變量、
                //3系統固定變量、
                //4常量與系統固定變量、
                //5系統所有變量、
                //6常量與系統所有變量、
                //7字符串運算表達式(包括常量、變量、系統變量)、
                //8邏輯運算表達式(包括常量、變量、系統變量)
                //9算術運算表達式(包括常量、變量、系統變量)、
                //10 select查詢語句(當VarType=9時)
                //11 insert插入語句(當VarType=9時)
                //12 update修改語句(當VarType=9時)
                //13 delete刪除語句(當VarType=9時)
                //14 exec執行存儲過程語句(當VarType=9時)
    UC MacroType; //屬性結果參數值范圍限制類型：0-不限制 1-整數范圍 2-單字符范圍 3-字符串中允許出現的字符 4-整數枚舉字符串 5-枚舉字符串
    US MacroNo; //屬性結果參數值范圍限制: 0xFFFF表示沒有限制 >=0表示限制的規則編號

}VXML_ATTR_RULE_STRUCT;


//指令規則結構定義
typedef struct //__attribute__((packed))
{
    UC CmdType; //指令類型: 0聲明指令、1內部操作指令、2通信指令、3數據庫指令、4事件響應指令、5邏輯跳轉指令、6算術運算指令、7字符串運算指令、8函數操作指令、9跳轉判斷指令、10跳轉判斷末指令
    const CH *CmdNameEng;//[MAX_CMD_NAME_LEN]; //指令英文名稱
    const CH *CmdNameChn;//[MAX_CMD_NAME_LEN]; //指令中文名稱
    US AttrNum; //屬性參數值個數
    VXML_ATTR_RULE_STRUCT Attribute[MAX_ATTRIBUTE_NUM]; //指令中屬性定義

}VXML_CMD_RULE_STRUCT;




//---------------------------------------------------------------------------
//屬性參數范圍結構定義
//---------------------------------------------------------------------------


//整數范圍定義
typedef struct
{
  
    UL lownum; //整數最小值
    UL upnum; //整數最大值

}VXML_INTEGER_RANGE_RULE_STRUCT;


//單字符范圍定義
typedef struct
{
    US len; //字符個數
    const CH *chars;//[MAX_VAR_DATA_LEN]; //整數最小值

}VXML_CHAR_RANGE_RULE_STRUCT;


//字符串中允許出現的字符定義
typedef struct
{
    US len; //串實際長度
    const CH *chars;//[MAX_VAR_DATA_LEN]; //允許出現的字符,為空時表示所有可見字符
    US maxlen; //字符串允許的最大長度 0不限制

}VXML_STRING_RANGE_RULE_STRUCT;

//整數枚舉字符串定義
typedef struct
{    
    UC stringnum; //枚舉字符串個數
    struct
    {
    	const CH *str;//[MAX_MACRO_STRING_LEN]; //允許出現的字符串
    	US value;
    }Es[MAX_MACRO_STRING_NUM];
}VXML_MACRO_INT_RANGE_RULE_STRUCT;



//枚舉字符串定義
typedef struct
{    
    UC stringnum; //枚舉字符串個數
    struct 
    {
    	const CH *str;//[MAX_MACRO_STRING_LEN]; //允許出現的字符串
		}Es[MAX_MACRO_STRING_NUM];
}VXML_MACRO_STR_RANGE_RULE_STRUCT;

////////////////////////////////////////////////////////////////////////////////////////



#endif
