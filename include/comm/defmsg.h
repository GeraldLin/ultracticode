#ifndef __DEFMSG_H__
#define __DEFMSG_H__

#define BOXTYPERBOX							1		//320模擬引擎
#define BOXTYPEDTBOX						2   //960混合引擎
#define BOXTYPEMBOX							3		//2560數字引擎

#define RBOXCONFGROUPSIZE				32  //RBOX會議組大小
#define DTMBOXCONFGROUPSIZE			80  //DTMBOX會議組大小

#define MAXPATHLEN			  256     //最大路徑長度
#define MAXVOCINDEX				2800  	//系統最大內存索引放音的索引數目
#define USERMAXVOCINDEX		1024   	//用戶可使用最大內存索引放音的索引數目,用戶使用Voc0--Voc8191
#define VOCDBINDEX  		  1024 		//語音合成庫的索引,1675個發音 1024--1024+1674
#define DTMFINDEX   			2700 		//DTMF放音索引,16個碼 9900--9915,9916 PAUSE 靜音

#define MAXCALLLEN     		30  	  //最大主叫被叫號碼長度(包括字符串結尾0)
#define MAXNAMELEN				50			//最大名字信息長度(包括字符串結尾0)


/////////////////定義通道類型
#define MAXCHTYPENUM		32		//所有通道類型的數目

#define CHCLASS_NONE    0x00  //空線路類型																			

#define CHTYPE_FAX     	0x01   //傳真通道																				
#define CHTYPE_VOIP    	0x02   //VOIP通訊通道																		
#define CHTYPE_FSK      0x03   //fsk通訊通道																		
#define CHTYPE_MODEM   	0x04   //modem通訊通道																	
#define CHTYPE_PVOIP  	0x05   //PCM VOIP通訊通道(語音不壓縮)

#define CHTYPE_PHYDT    0x08  //數字中繼物理PCM 
#define CHTYPE_DTNO1    0x09  //數字中繼中國一號信令 
#define CHTYPE_DTNO7    0x0A  //數字中繼中國七號信令(TUP)
#define CHTYPE_DTDSS1   0x0B  //數字中繼ISDN PRI(30B+D)
#define CHTYPE_DTNO7ISUP 0x0C  //數字中繼中國七號信令(ISUP)
#define CHTYPE_DTM      0x0E  //數字中繼高阻監控通道 

#define CHTYPE_PHYAT    0x10  //模擬中繼物理線端口								
#define CHTYPE_TB       0x11  //模擬中繼外線(普通市話線),接電信局								
#define CHTYPE_AGENT    0x12  //模擬中繼內線(座席線)，接電話機									
#define CHTYPE_HB       0x13  //模擬中繼高阻線(監控線)，與電話線/電話機相并聯		
#define CHTYPE_LOOP     0x14  //自環線路(放音后能夠原樣錄音回來)               	
#define CHTYPE_SPKMIC   0x15  //音箱/MIC模塊(只有語音功能，無摘掛機振鈴等功能)	
#define CHTYPE_MONITOR  0x16  //內線監聽模塊(只有語音/DTMF功能，無摘掛機振鈴等功能)	

#define CHTYPE_MTP3			0x18  //七號信令MTP傳輸層通道
#define CHTYPE_CAS			0x19   //cas通道																	
#define CHTYPE_MFC			0x1A   //mfc通道																	

#define CHTYPE_CONFCH		0x1E   //會議通道(可交換/收發碼等)																	
#define CHTYPE_CONFROOM	0x1F   //會議室(可錄放音)	

/////////////////定義資源參數 注意Param只有5bits
////////for 放音
#define RESPAR_DTMFSTOPPLAY 0x01  //收到DTMF時停止放音(必須設置RESPAR_FIRSTPLAY才起作用)
#define RESPAR_VOCSTOPPLAY 	0x02  //檢測到線路有語音時停止放音(必須設置RESPAR_FIRSTPLAY才起作用)
#define RESPAR_LOOPLAY  		0x04  //這是一個循環放音消息,如果沒有同時設置RESPAR_PLASTMSG,則放完后重新追加到隊列尾
#define RESPAR_FIRSTPLAY 		0x08  //這是第一個放音消息
#define RESPAR_LASTPLAY  		0x10  //這是最后一個放音消息
////////for 錄音
#define RESPAR_DTMFSTOPREC 	0x01  //收到DTMF時停止錄音
#define RESPAR_RECAFTERPLAY	0x08  //收到放音結束后才自動開始錄音(放音結束原因為打斷或自然結束)

                               
////////定義傳真數據的寬度
#define FAXVRES_LOW      0x00  //普通垂直分辨率(98/英寸)
#define FAXVRES_HIGH     0x01  //高垂直分辨率(196/英寸)

#define FAXWIDTH_A4      0x00  //1728點寬
#define FAXWIDTH_A3      0x01  //2432點寬
#define FAXWIDTH_B4      0x02  //2048點寬
                
/////////////////定義消息,可用于下傳也用于上傳
#define MSG_NULL              0     //空消息,消息無意義,DataLen仍舊有意義
                                    
#define MSG_RELEASE           1     //通道釋放
 #define F_R_NORMAL           	0   //通話后正常釋放
 #define F_R_COUTFAIL           1   //呼出失敗釋放
 #define F_R_CINFAIL           	2   //呼入失敗釋放 	
 	#define P_R_NORMAL           	 0   //正常釋放
 	#define P_R_COUTNODIALTONE     1   //呼出沒有檢測到撥號音 （只用于外線TB）
 	#define P_R_COUTLINEBUSY       2   //呼出線路占用中
 	#define P_R_COUTNOSOUND        3   //呼出后沒有聲音
 	#define P_R_COUTRINGTIMEOUT    4   //呼出振鈴超時 （對方無人接）
 	#define P_R_COUTSTOP    			 5   //呼出被用戶中斷  	
 	#define P_R_CINNOACK       		 6   //呼入未接超時 （程序沒有及時發送摘機）
 	#define P_R_CINREFUSE      		 7   //呼入拒絕 （程序收到呼入后發釋放命令）
  #define P_R_COUTCONGESTION     12  //設備擁塞
  #define P_R_COUTINCOMPLETE     15  //地址不全
  #define P_R_COUTFAILURE        16  //呼叫故障
  #define P_R_COUTCALLBARRING    18  //對方接入拒絕
  #define P_R_COUTBUSYLOCAL      19  //市話忙 slb
  #define P_R_COUTBUSYDDD        20  //長話忙 stb
  #define P_R_BUSYUSER           21  //用戶忙 ssb
  #define P_R_COUTNULLDN         22  //空號
  #define P_R_SPECTONE           23  //專用信號音 sst
  #define P_R_CGC								 24  //電路群擁塞cgc
  #define P_R_POWEROFF					 25  //用戶關機(ISUP)
  #define P_R_COUTNODIGITALPATH  26  //沒有數字通路
  #define P_R_SIGNFAIL					 30  //信令故障
  #define P_R_PHYFAIL						 31  //線路硬件物理故障
 #define F_R_RESET	            3    //線路復原

#define MSG_CALL              2     //通道呼出/或有呼入
 #define F_C_NORMAL            	0    //普通呼叫
  //對于TB 和 AGENT 呼出時
  #define P_C_NORMAL            	0    //TB=檢測dialtone,AGENT=標準振鈴聲+FSK主叫發送                     
  #define P_C_1                 	1    //TB=不檢測dialtone;AGENT=標準振鈴聲+DTMF主叫發送
  #define P_C_2                 	2    //AGENT=第2種振鈴聲+DTMF主叫發送
  #define P_C_3                 	3    //AGENT=第3種振鈴聲+DTMF主叫發送
  //對于TB 和 AGENT 呼入時,Param=0
 	//對于數字線路呼出,Param=0,實際參數位于 msg.call.xxx
 #define F_C_OVLP              	1    //重疊呼叫
  
#define MSG_ACK         			3     //被叫摘機應答
 #define  F_ACK_ANC            0    //無指示(默認狀態)
 #define  F_ACK_ANN            1    //不計費
 #define  F_ACK_ANCFORCE       2    //計費
 #define  F_ACK_RESERVE        3    //保留
 
#define MSG_FLASH             4     //拍叉簧

#define MSG_ACM               5    //上傳消息本方呼出成功(對方已經振鈴),有可能收不到該消息而直接收到Connect消息
					               //下傳消息給對方(呼叫方)發地址全(特殊情況下用)
 #define F_ACM_IDLE            0   //空閑可用
 #define F_ACM_BUSY            1   //線路忙
 #define F_ACM_NULL            2   //線路空號
 #define F_ACM_FAULT           3   //線路呼叫失敗
               
#define DOWNMSG_CONF        	6    //會議命令(一個會議中所有會議人+旁聽人總數不能超過80)

#define DOWNMSG_ROUTER       	7    //交換命令
 #define F_R_SILENCE           0   //通道輸入/輸出靜音 ,不能用于會議資源
 #define F_R_MONITOR           1   //單向監聽					 ,不能用于會議資源
 #define F_R_TALK              2   //雙向通話  
  
#define MSG_ONOFF             8     //通道關閉/打開(不許使用/可以使用)
 #define F_OF_OFF              0   //關閉
 #define F_OF_ON               1   //打開

#define DOWNMSG_SETPARAM      9    //下傳消息設置通道參數
 #define F_PM_DEVICE            0  //每個設備參數
 #define F_PM_GLOBAL	          1  //全局參數
 #define F_PM_CHANNEL			 			2  //通道參數
    
#define UPMSG_ALARM           10    //上傳消息線路告警(軟故障)
 #define F_AM_CH								0   //通道告警
  #define P_AM_OK  		     				0    //通道正常，可以使用
  #define P_AM_DT_LINKFAIL				1    //由于鏈路失敗，本通道不能使用
  #define P_AM_DT_MSYN						2    //由于16時隙復貞同步錯誤（只在NO1方式下），本通道不能使用
  #define P_AM_DT_BLOCK						3    //由于話路閉塞，本通道不能呼出使用
  #define P_AM_TB_NOPOWER		 			6    //外線上沒有直流電壓(掉線或短路)
 #define F_AM_PCMSYN						1   //PCM同步告警
  #define P_AM_SYNOK             	0    //同步正常
  #define P_AM_SYNNOCARRY		 			1    //接收無載波
  #define P_AM_SYNFAS		     			2    //接收FAS同步錯誤
  #define P_AM_SYNCAS			 				3    //接收CAS MF同步錯誤（只在NO1方式下）
  #define P_AM_SYNSLIP			 			4    //彈性緩沖區滑動
  #define P_AM_SYNALARMA		 			5    //遠端A位告警
  #define P_AM_SYNALARMY		 			6    //遠端CAS MF Y位告警（只在NO1方式下）
  #define P_AM_SYNALLONE		 			7    //接收全1錯誤
  #define P_AM_SYNJITTER		 			8    //抖動消除電路告警
 #define F_AM_MTP								2    //七號信令MTP告警
  #define P_L3L4_STOP			 				1    //所有鏈路中斷
  #define P_L3L4_LINKOK				 		2    //鏈路開通，開通鏈路號	  
  #define P_L3L4_LINKFAIL        	3    //鏈路中斷，失敗鏈路號	  
  #define P_L3L4_DPCOK           	4    //DPC 可到達	  
  #define P_L3L4_DPCNOLINK       	5    //DPC 不可到達
 #define F_AM_REF								3    //數字卡同步參考源信息
  #define P_SYN_OK			    			1   //參考有
  #define P_SYN_FAIL			    		0   //參考無
 #define F_AM_CHCAS							4   //通道CAS信令告警(只在CAS方式下)
  #define P_AM_CASCHREOK        	0   //線路正常（空閑）
  #define P_AM_CASCHREMOTEBLOCK 	1   //遠端閉塞
  #define P_AM_CASCHCONFUSE     	2   //100:呼出線路收到對方占用
  #define P_AM_CASCHNOCONFIRM   	3   //101:呼出時對方占用證實時間太長(>200ms),實際為>800ms
  #define P_AM_CASCHERRORREL1   	4   //102:呼出時對方占用證實后并且在摘機前不正常地釋放了線路(>2s)
  #define P_AM_CASCHERRORREL2   	5   //103:呼出時對方摘機后不正常地釋放了線路
 #define F_AM_DEVICE						99   //USE設備拔出
   
#define MSG_USERDATA		  		12    //用戶直接收發信令相關的消息包
   
#define DOWNMSG_ADDINDEX      13    //添加一條內存索引(文件形式)

 
/////////////////特殊多媒體通道命令FAX,FSK,MODEM
#define MSG_SPECMEDIA        	14    //特殊媒體收發消息FAX,FSK,MODEM
 #define F_SMEDIA_SetParam      0    //設置指定資源通道的參數(下發)
 #define F_SMEDIA_SetStart      1    //啟動資源 (下發),必須在Idle狀態下發送
 	#define P_SMEDIA_StartDefault  0    //默認方式(下發)
 	#define P_SMEDIA_StartSend     1    //發送文件(下發)
  #define P_SMEDIA_StartRecv     2    //接收到文件(下發)
 #define F_SMEDIA_SetStop  	    2    //命令停止收發 (下發),收到Idle消息后不用再發本命令
 #define F_SMEDIA_SendMemory    3    //發送內存 (下發)
/////////////////特殊多媒體通道命令事件FAX,FSK,MODEM
 #define F_SMEDIA_MemoryRecv    13   //收到內存錄音塊(上傳) 
 #define F_SMEDIA_Idle    			14   //資源關閉后空閑 (上傳),收到此消息才能認為資源空閑
  #define P_SMEDIASTOP_FINISH    	0   //正確結束后空閑
  #define P_SMEDIASTOP_FILERR   	1   //文件錯誤后空閑
  #define P_SMEDIASTOP_COMMERR   	2   //通訊錯誤后空閑
  #define P_SMEDIASTOP_USR   	    3   //用戶停止后空閑
  
/////////////////語音多媒體命令
#define MSG_VOC          			15    //語音/DTMF多媒體消息
 #define F_VOC_SetParam         0    //設置指定資源通道的參數(下發)

 #define F_VOC_StopPlay    		  1    //停止放音 (下發)
 #define F_VOC_PlayFile         2    //播放文件(下發)
 #define F_VOC_PlayMemory    	  3    //播放內存 (下發)
 #define F_VOC_PlayIndex     	  4    //播放內存索引號指定的語音(下發)
 #define F_VOC_PlayTTStr        5    //播放文本串(下發)
 #define F_VOC_PlayDTMFStr      6   //發送 dtmf(下發)

 #define F_VOC_StopRec   			  7    //停止錄音 (下發)
 #define F_VOC_PauseRec         8    //暫停錄音(下發)
 #define F_VOC_ResumeRec        9    //重新開始錄音 (下發)
 #define F_VOC_RecFile          10   //錄音到文件(下發)
 #define F_VOC_RecMemory        11   //錄音到內存(下發) 
/////////////////語音多媒體事件
 #define F_VOC_InfoRecv         12   //收到 信息碼(包括dtmf等)(上傳) 
 #define F_VOC_MemoryRecv       13   //收到內存錄音塊(上傳) 
 #define F_VOC_PlayStop    		  14   //放音被停止 (上傳)
 #define F_VOC_RecStop   			  15   //錄音被停止 (上傳)
  #define P_VOCSTOP_FINISH       	0   //自然結束停止
  #define P_VOCSTOP_RECERR       	1   //錄音文件操作錯誤或內存錄音參數錯誤（錄音）
  #define P_VOCSTOP_PLAYERR      	2   //播放內容不存在錯誤（放音）
  

static inline const char *GetChTypeNameStr(unsigned short chtype)//取通道類型的名字
{
 switch (chtype)
 {
  case CHCLASS_NONE:		return "空類型";							
  case CHTYPE_FAX   :  	return "傳真通道";
  case CHTYPE_VOIP  :  	return "VOIP通道";
  case CHTYPE_FSK   :  	return "fsk通道";
  case CHTYPE_MODEM :  	return "modem通道";
  case CHTYPE_PVOIP	:  	return "PVOIP通道";
	
	case CHTYPE_PHYDT	:		return "物理PCM";
	case CHTYPE_DTNO1:		return "一號通道";
	case CHTYPE_DTNO7:		return "七號通道";
	case CHTYPE_DTDSS1:		return "ISDN通道";
	case CHTYPE_DTNO7ISUP:		return "ISUP通道";
	case CHTYPE_DTM:			return "數字高阻通道";
 
	case CHTYPE_PHYAT:		return "物理模擬線";
  case CHTYPE_TB:				return "外線通道";
  case CHTYPE_AGENT:		return "內線通道";
  case CHTYPE_HB:				return "高阻通道";
  case CHTYPE_LOOP:     return "自環通道";
  case CHTYPE_SPKMIC:   return "音箱麥克";
	case CHTYPE_MONITOR:  return "監聽通道";
		
  case CHTYPE_MTP3:			return "MTP3傳輸層通道";	
  case CHTYPE_CAS:			return "CAS傳輸通道";	
  case CHTYPE_MFC:			return "MFC互控通道";	
  case CHTYPE_CONFCH	:	return "會議通道";																	
	case CHTYPE_CONFROOM:	return "會議室";	
 default:							return "未知類型";
 }
}
 
static inline const char *GetMsgTypeNameStr(unsigned short msgtype)
{
	switch(msgtype)
	{
		case(MSG_RELEASE):		return "釋放通道";
		case(MSG_CALL):				return "通道呼叫";
		case(MSG_ACM):				return "地址收全";
		case(MSG_ACK):				return "摘機應答";
		case(MSG_FLASH):			return "拍叉簧";
		case(DOWNMSG_CONF):		return "會議/交換";
		case(DOWNMSG_ROUTER):	return "通道交換";	
		case(MSG_ONOFF):			return "通道開關";	
		case(UPMSG_ALARM):    return "上傳告警";		
		case(MSG_USERDATA):		return "用戶數據";	
		case(DOWNMSG_ADDINDEX): return "添加語音索引";
		case(MSG_SPECMEDIA):		return "特殊媒體";
		case(MSG_VOC):				return "語音/DTMF";
		default:				return "未知消息"; 
	}
}

static inline const char *GetReleaseParamNameStr(unsigned short func,unsigned short param)
{
	switch(param)
	{
	case(P_R_NORMAL):					return "正常釋放";
	case(P_R_COUTNODIALTONE):	return "呼出無撥號音";
	case(P_R_COUTLINEBUSY):		return "呼出線路忙";
	case(P_R_COUTNOSOUND):		return "呼叫后無聲";
	case(P_R_COUTRINGTIMEOUT):	return "呼出振鈴超時";
  case(P_R_COUTSTOP):    			return "呼出被用戶中止";  
	case(P_R_CINNOACK):					return "呼入未接";
	case(P_R_COUTCONGESTION):   return "設備擁塞";
  case(P_R_COUTINCOMPLETE):   return "地址不全";
  case(P_R_COUTFAILURE):      return "呼叫故障";
  case(P_R_COUTCALLBARRING):  return "接入拒絕";
  case(P_R_COUTBUSYLOCAL):    return "市話忙";
  case(P_R_COUTBUSYDDD):      return "長話忙";
  case(P_R_BUSYUSER):         return "用戶忙";
  case(P_R_COUTNULLDN):       return "空號";
  case(P_R_SPECTONE):         return "專用信號音";
  case(P_R_CGC):							return "電路群擁塞";
  case(P_R_POWEROFF):					return "用戶關機";
  case(P_R_COUTNODIGITALPATH):  return "沒有數字通路";
  case(P_R_SIGNFAIL):					 return "信令故障";
  case(P_R_PHYFAIL):					return "硬件故障";	
	default:										return "未知";
	}
}

static inline const char *GetCallFuncNameStr(unsigned short func)
{
	switch(func)
	{
	case(F_C_NORMAL):		return "普通呼叫";
	case(F_C_OVLP):			return "重疊呼叫";
	default:				return "未知功能";
	}
}

static inline const char *GetCallParamNameStr(unsigned short chtype, unsigned short param)
{
	switch(param)
	{
	case(P_C_NORMAL):
		switch(chtype)
		{
			case(CHTYPE_AGENT):		return "標準振鈴聲+FSK主叫發送";
			case(CHTYPE_TB):		return "檢測撥號音";
		}
		break;
	case(P_C_1):
		switch(chtype)
		{
			case(CHTYPE_AGENT):		return "DTMF主叫發送+標準振鈴聲";
			case(CHTYPE_TB):		return "不檢測撥號音";
		}
		break;
	case(P_C_2):
		switch(chtype)
		{
			case(CHTYPE_AGENT):		return "DTMF主叫發送+第2種振鈴聲";
		}
		break;
	case(P_C_3):
		switch(chtype)
		{
		case(CHTYPE_AGENT):			return "DTMF主叫發送+第3種振鈴聲";
		}
		break;
			
	}
	return "未知參數";

}

static inline const char *GetAlarmParamNameStr(unsigned short func, unsigned short param)
{
	switch(func)
	{
	case(F_AM_CH):
		switch(param)
		{
			case(P_AM_OK):					return "線路正常";
			case(P_AM_TB_NOPOWER):	return "外線無直流電壓";
		}
		break;
	}
	return "未知參數";
}

static inline const char *GetVocFuncNameStr(unsigned short func)
{
	switch(func)
	{
  case(F_VOC_SetParam):     return "設置參數";		
	case(F_VOC_StopPlay):			return "停止放音";
	case(F_VOC_StopRec):			return "停止錄音";
	case(F_VOC_PauseRec):			return "暫停錄音";
	case(F_VOC_ResumeRec):		return "重新開始錄音";		
	case(F_VOC_PlayFile):			return "播放文件";
	case(F_VOC_PlayMemory):		return "播放內存";
	case(F_VOC_PlayIndex):		return "播放內存索引";
	case(F_VOC_PlayTTStr):		return "播放文本串";
	case(F_VOC_PlayDTMFStr):	return "播放DTMF串";
	case(F_VOC_RecFile):			return "錄音到文件";
	case(F_VOC_RecMemory):		return "錄音到內存";
 
  case(F_VOC_InfoRecv):     return "收到信息碼";
  case(F_VOC_MemoryRecv):   return "收到內存錄音塊";   	
  case(F_VOC_PlayStop):    	return "放音被停止";
  case(F_VOC_RecStop):   		return "錄音被停止";
	default:					return "未知功能";
	}
}

 
static inline const char *GetSpecMediaFuncNameStr(unsigned short func)
{
	switch(func)
	{
	case(F_SMEDIA_SetParam):			return "設置參數";
	case(F_SMEDIA_SetStart):			return "啟動資源";
	case(F_SMEDIA_SetStop):				return "停止資源";
	case(F_SMEDIA_SendMemory):		return "發送內存";
 
  case(F_SMEDIA_MemoryRecv):   return "收到內存數據";
  case(F_SMEDIA_Idle):    	   return "停止后空閑";
  default:										 return "未知功能";
	}
}

 
static inline const char *GetStopReason(unsigned char rsn)//返回媒體停止原因
{
  switch(rsn)
  {
  case P_VOCSTOP_FINISH: 	return "成功完成";
  case P_VOCSTOP_RECERR: 	return "錄音失敗錯誤";
  case P_VOCSTOP_PLAYERR: return "播放內容錯誤";
  default:return "未知錯誤";
  }
}



static inline const char *GetVocFileFormatNameStr(unsigned short fmt)
{
	switch(fmt)
	{
    case 0:return "ALAW PCM";
		case 1:return "WAVE";
		case 2:return "CIRRUS ADPCM";
		case 3:return "G.723.1";
		case 4:return "Dialogic 6K";
		case 5:return "Dialogic 8K";
		default:return "未知格式";
	}
}                

#endif
