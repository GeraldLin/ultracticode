#ifndef __DEFMNG_H__
#define __DEFMNG_H__

//注意libmng.so必須使用靜態連接共享庫的方式使用 



//定義RBOXSENDMSG返回的錯誤碼 (返回0成功)
#define SENDMSGERR_EXCEPTION	  -999							//程序異常錯誤，需要檢查代碼
#define SENDMSGERR_LOGICCH			-100							//邏輯通道錯誤
#define SENDMSGERR_LOGICCHFUN		-101							//邏輯通道不支持本功能
#define SENDMSGERR_MSGTYPE			-200							//消息類型錯誤
#define SENDMSGERR_MSGFUNC			-300							//消息功能錯誤
#define SENDMSGERR_MSGPARAM			-400							//消息參數錯誤(包括union中擴展參數)
#define SENDMSGERR_STATUS				-500							//當前通道狀態不能接受本命令(如對不空閑通道呼出),強烈建議呼出時判斷
#define SENDMSGERR_FILE					-600							//指定的文件打開失敗
#define SENDMSGERR_FILEFMT			-700							//指定的文件格式錯誤
#define SENDMSGERR_ADDINDEX			-800							//添加語音索引錯誤
#define SENDMSGERR_FULL					-900							//底層接收消息隊列滿錯誤
#define SENDMSGERR_ALLOCRES			-901							//分配資源錯誤（語音/會議等）
#define SENDMSGERR_UNLINK				-902							//底層接收消息鏈路沒有建立錯誤(TCPLINK)


//定義RBOXMAININIT返回的錯誤碼 (返回0成功)
#define MAININITERR_ADDINDEX 		SENDMSGERR_ADDINDEX		//添加語音索引錯誤
#define MAININITERR_ALLOCMEM		-1000							//系統分配內存失敗
#define MAININITERR_INIFILEDIR 	-1101							//配置文件中配置的目錄錯誤
#define MAININITERR_INIFILECARD	-1102							//配置文件中配置的板類型錯誤
#define MAININITERR_INIFILECH		-1103							//配置文件中配置的通道錯誤
#define MAININITERR_INIFILENOCH	-1104							//配置文件中沒有配置的通道類型錯誤
#define MAININITERR_INIFILETELS	-1105							//配置文件中配置的合群號碼錯誤

#define MAININITERR_CREATEDTMF	-1201							//創建dtmf音失敗
#define MAININITERR_CREATETONE	-1202							//創建信號音失敗
#define MAININITERR_VOCDBFILE		-1301							//語音合成庫文件不存在或打開失敗
#define MAININITERR_VOCDBFORMAT -1302							//語音合成庫文件格式/內容錯誤
#define MAININITERR_SLATDRVOPEN	-1401							//打開slat驅動失敗
#define MAININITERR_SLATDRVMAP	-1402							//映射slat驅動失敗
#define MAININITERR_SLATDRVER		-1403							//slat驅動版本不匹配
#define MAININITERR_DSP0DRVOPEN	-1501							//打開dsp0驅動失敗
#define MAININITERR_DSP0DRVMAP	-1502							//映射dsp0驅動失敗
#define MAININITERR_DSP0DRVER		-1503							//dsp0驅動版本不匹配
#define MAININITERR_DSP1DRVOPEN	-1601							//打開dsp1驅動失敗
#define MAININITERR_DSP1DRVMAP	-1602							//映射dsp1驅動失敗
#define MAININITERR_DSP1DRVER		-1603							//dsp1驅動版本不匹配
#define MAININITERR_MUTEX				-1701							//mutex初始化失敗

#define MAININITERR_OPENDRV			-1900							//打開某個驅動程序失敗
#define MAININITERR_LOADSLIB		-2000							//mng加載內部共享庫(libfax/libvoip等)失敗
#define MAININITERR_NOCHANNEL		-3000							//沒有配置成功任何通道類型

#endif
