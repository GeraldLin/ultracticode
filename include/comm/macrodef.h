//---------------------------------------------------------------------------
#ifndef MacroDefH
#define MacroDefH
//---------------------------------------------------------------------------
//定時器單位(ms)
#define Timer_Interval          5 //5ms
#define TIMERS                  1 //1s

#define TIMER0					        1 //10 ms
#define TIMER1					        2 //500 ms
#define TIMER2					        3 //50 ms

//線程定義標志
#define THREADID


//數據庫類型
#define DB_NONE		              0
#define DB_SQLSERVER		        1
#define DB_ORACLE			          2
#define DB_SYBASE			          3
#define DB_ACCESS			          4
#define DB_ODBC				          5
#define DB_MYSQL			          6
#define DB_SQLITE3		          7

//數據庫操作超時時長（毫秒）
#define DB_PROC_TIMEOUT		      5000

//流程變量類型定義
#define anyType	                0  //任意類型
#define boolType                1  //BOOL
#define intType	                2  //integer
#define floatType               3  //float
#define charType                4  //char
#define stringType              5  //string

//語言宏定義常量
#define english	                1  //英語
#define chinese	                2  //普通話
#define localism	              3  //地方話

//聲音宏定義
#define female	                1  //女聲
#define male	                  2  //男聲
#define child					          3  //童聲

//放音內容類型定義
#define PLAYTYPE_file	          1  //文件
#define PLAYTYPE_index          2  //內存索引
#define PLAYTYPE_ttsfile        3  //TTS文件
#define PLAYTYPE_ttsstr         4  //TTS字符串
#define PLAYTYPE_tone	          5  //信號音
#define PLAYTYPE_dtmf	          6  //dtmf

//語音格式定義
#define AUDIO_FORMAT_default	  0  //板卡默認的格式
#define AUDIO_FORMAT_pcma			  1  //PCM a律格式
#define AUDIO_FORMAT_pcmmu		  2  //PCM mu律格式
#define AUDIO_FORMAT_adpcm	    3  //ADPCM格式
#define AUDIO_FORMAT_wave				4  //WAVE格式
#define AUDIO_FORMAT_mp3				5  //MP3格式
#define AUDIO_FORMAT_gsm				6  //gsm格式
#define AUDIO_FORMAT_vox				7  //vox格式
#define AUDIO_FORMAT_g729a  		8  //g729a格式

//信號音標志定義
#define TONE_dialtone	          1	//撥號音
#define TONE_ringtone	          2	//回鈴音
#define TONE_busytone	          3	//忙音
#define TONE_unntone	          4	//空號音
#define TONE_conftone	          5	//證實音
#define TONE_hangtone	          6	//催掛音
#define TONE_restdial	          7	//限制撥打音
#define TONE_notopen            8 //業務未開通音

#define disconn					        0  //斷開連接
#define connboth	              1  //sou<->obj雙向連接
#define connfrom				        2  //sou<-obj單向連接
#define connto					        3  //sou->obj單向連接
#define conncbm					        4  //彩話雙向連接
#define connvcfrom			        5  //來話變聲
#define connvcto				        6  //去話變聲
#define connvcboth			        7  //雙向變聲

#define serial					        1  //順序呼叫
#define simult					        2  //同時呼叫

#define blind					          1  //直接轉接
#define bridge					        2  //呼通后轉接
#define consultation			      3  //通話后轉接

#define presider				        1  //主持
#define talker					        2  //加入
#define listener				        3  //旁聽

#define anc						          1  //應答計費
#define ann						          2  //應答不計費

//信令類型
#define SS1						          1  //
#define TUP						          2  //
#define ISUP					          3  //
#define Q931					          4  //
#define SIP						          5  //
#define H323					          6  //
#define POT						          7  //
#define CAS						          8  //
#define CSTA					          9  //

//信令消息類型
#define ACM						          1  //
#define GRQ						          2  //
#define ANN						          3  //
#define ANC						          4  //
#define CFL						          5  //
#define SLB						          6  //
#define STB						          7  //
#define SEC						          8  //
#define UNN						          9  //
#define REL						          10  //

#define LEFT					          1  //從字符串左開始
#define RIGHT					          2  //從字符串右開始

#define DT_yyyy					        1  //年
#define DT_mm						        2  //月
#define DT_dd						        3  //日
#define DT_hh						        4  //時
#define DT_mi						        5  //分
#define DT_ss						        6  //秒
#define DT_ms						        7  //豪秒
#define DT_wd						        8  //星期

//通道按硬件端口分類定義類型
#define PORT_SANHUI_TYPE        0	//三匯板卡端口類型
#define PORT_AVAYA_IPO_TYPE     1	//AVAYA-IPO交換機端口類型
#define PORT_AVAYA_SXXXX_TYPE   2	//AVAYA-S系列媒體交換機端口類型
#define PORT_ALCATEL_OXO_TYPE   3	//ALCATEL_OXO系列媒體交換機端口類型
#define PORT_ALCATEL_OXE_TYPE   4	//ALCATEL_OXE系列媒體交換機端口類型
#define PORT_SIMENS_TYPE        5	//simens系列交換機端口類型
#define PORT_Panasonic_TYPE     6	//Panasonic交換機端口類型
#define PORT_NEC_TYPE           7	//NEC交換機端口類型
#define PORT_HUAWEI_TYPE        8	//HUAWEI交換機端口類型
#define PORT_STARTNET_TYPE      9	//星網銳捷IPPBX交換機端口類型
#define PORT_NOCTI_PBX_TYPE     10//無CTI接口的交換機端口類型
#define PORT_ZL_PBX_TYPE     		11//無CTI接口的交換機端口類型
#define PORT_CISCO_TYPE     		12//CISCO交換機端口類型
#define PORT_FREESWITCH_TYPE    13//FREESWITCH交換機端口類型

//通道按業務分類定義類型
#define CHANNEL_NONE            0	//無
#define CHANNEL_TRUNK           1	//中繼
#define CHANNEL_SEAT            2	//坐席
#define CHANNEL_IVR             3	//IVR
#define CHANNEL_REC             4	//錄音
#define CHANNEL_FAX             5	//傳真
#define CHANNEL_CFC             6	//會議
#define CHANNEL_VOIP            7	//VOIP
#define CHANNEL_VDN             8	//VDN FOR AVAYA
#define CHANNEL_ACD             9	//ACD Splite for AVAYA

//通道按信令分類定義類型
#define CH_NONE			  					0 //無
#define CH_A_SEAT								1 //模擬坐席通道
#define CH_A_EXTN								2 //模擬外線通道
#define CH_D_SEAT								3 //ISDN坐席通道
#define CH_D_EXTN								4 //ISDN外線通道
#define CH_A_RECD								5 //模擬電話錄音通道
#define CH_D_RECD								6 //ISDN電話錄音通道
#define CH_E_RECD								7 //數字中繼錄音通道
#define CH_E_SS1								8 //數字通道（中國No.1信令）
#define CH_E_TUP								9 //數字通道（中國No.7信令TUP）
#define CH_E_ISUP								10 //數字通道（中國No.7信令ISUP）
#define CH_E_ISDN_U							11 //數字通道（ISDN用戶側）
#define CH_E_ISDN_N							12 //數字通道（ISDN網絡側）
#define CH_D_FAX								13 //傳真資源通道
#define CH_S_FAX								14 //軟傳真資源通道
#define CH_D_VOIP								15 //VOIP資源通道H323
#define CH_S_VOIP								16 //VOIP資源通道SIP
#define CH_A_EM4								17 //EM4線中繼
#define CH_S_TRUNK							18 //SIP中繼
#define CH_H_TRUNK							19 //H323中繼
#define CH_A_NONE								20 //未插模塊
#define CH_E_LINESIDE						21 //LineSide信令
#define CH_V_VDNROUTE						22 //VDN

//呼叫方向
#define CALL_IN                 1	//呼入
#define CALL_OUT                2	//呼出
#define CALL_TRAN_IN            3	//轉接呼入
#define CALL_TRAN_OUT           4	//轉接呼出
#define CALL_AUTO               5	//自動流程

//---------------------------------------------------------------------------
//消息事件定義
//---------------------------------------------------------------------------
//通用操作返回事件
#define OnSuccess				        0x00 //成功(或不存在、或收到消息)
#define OnFail					        0x01 //失敗或存在

#define OnInAnswer							0x02 //電話呼入后分機應答（針對交換機版本）

#define OnHangon				        0x09 //通道釋放事件
#define OnLinkHangon				    0x0A //連接通道釋放事件
#define OnUnLink				        0x0B //連接通道斷開事件
#define OnFlash				          0x0C //坐席通道拍叉簧事件
#define OnCallConnected         0x0D //交換機通話連接事件
#define OnCallUnConnected       0x0E //交換機通話斷開事件
#define OnSwtRouteReq       		0x0F //交換機請求選擇外部路由事件

//放音收碼事件
#define OnPlaying				        0x10 //16-正在放音
#define OnPlayEnd				        0x11 //17-放音結束
#define OnRecvDTMF				      0x12 //18-收到按鍵
#define OnRecvASR				        0x13 //19-收到識別字符串
#define OnPlayError				      0x14 //20-放音錯誤
#define OnErrDTMF				        0x15 //21-收收到錯誤的DTMF

#define OnDialDTMF				      0x1A //內線分機撥后續按鍵事件

//TTS事件
#define OnTTSing				        0x20 //正在文本轉語音操作
#define OnTTSEnd				        0x21 //文本轉語音操作結束
#define OnTTSError				      0x22 //文本轉語音操作錯誤

//ASR事件
#define OnASRing				        0x30 //正在語音識別操作
#define OnASREnd				        0x31 //語音識別結束
#define OnASRError				      0x32 //語音識別錯誤

//錄音事件
#define OnRecording				      0x40 //64-正在錄音
#define OnRecordEnd				      0x41 //65-結束錄音
#define OnRecordError			      0x42 //66-錄音錯誤

//呼出事件
#define OnOutCalling			      0x50 //80-正在呼出
#define OnOutRing				        0x51 //81-被叫振鈴
#define OnOutBusy				        0x52 //82-被叫占線
#define OnOutTimeout			      0x53 //83-無人應答或超時
#define OnOutUNN				        0x54 //84-空號
#define OnOutFail				        0x55 //85-呼叫失敗
#define OnOutAnswer				      0x56 //86-呼出應答
#define OnTranTalk				      0x57 //87-轉接成功通話
#define OnOutRel				        0x58 //88-呼出通道通話后釋放
#define OnOutNoTrk			        0x59 //89-呼出中繼全忙
#define OnOutBAR		            0x5A //90-接入拒絕
#define OnOutCGC		            0x5B //91-設備擁塞
#define OnOutADI		            0x5C //92-地址不全
#define OnOutSST		            0x5D //93-專用信號音
#define OnOutPWROFF	   	        0x5E //94-用戶關機
#define OnOutOUTSRV 		        0x5F //95-用戶不在服務區

//坐席分配事件
#define OnACDWait			          0x60 //96-正在分配
#define OnACDRing			          0x61 //97-坐席振鈴
#define OnACDBusy				        0x62 //98-坐席全忙
#define OnACDTimeOut			      0x63 //99-無人應答或超時
#define OnACDUNN		        		0x64 //100-空號
#define OnACDFail				        0x65 //101-分配失敗
#define OnACDAnswer			        0x66 //102-坐席應答
#define OnACDRefuse	        		0x67 //103-拒絕
#define OnACDGetSeat         		0x68 //104-返回取的坐席數
#define OnACDOUTSRV		          0x69 //105-無空閑坐席
#define OnACDTakeover	          0x6A //106-坐席接管事件

//數據庫操作事件
#define OnDBSuccess				      0x70 //112-數據庫操作成功
#define OnDBFail				        0x71 //113-數據庫操作失敗
#define OnDBBOF					        0x72 //114-到第一條記錄
#define OnDBEOF					        0x73 //115-到最后一條記錄

//收到外部數據網關消息事件
#define OnDWMsg        		      0x78 //收到外部數據網關消息事件

//定時器事件
#define OnTimerOver			        0x80 //定時器溢出事件
#define OnTimer0Over			      0x81 //定時器0溢出事件
#define OnTimer1Over			      0x82 //定時器1溢出事件
#define OnTimer2Over			      0x83 //定時器2溢出事件
#define OnTimer3Over			      0x84 //定時器3溢出事件
#define OnTimer4Over			      0x85 //定時器4溢出事件

//傳真收發事件
#define OnsFaxing			          0x90 //144-正在發送
#define OnsFaxend			          0x91 //145-發送結束
#define OnsFaxFail		          0x92 //146-發送失敗
#define OnrFaxing			          0x93 //147-正在接收
#define OnrFaxend			          0x94 //148-接收結束
#define OnrFaxFail		          0x95 //149-接收失敗
#define OnsFaxNoSignal		      0x96 //150-發送傳真時未檢測到對方的信號音
#define OnrFaxNoSignal          0x97 //151-接收傳真時未檢測到對方的信號音

#define OnToneDetected          0x98 //檢測到信號音

//會議操作返回事件
#define OnCfcFirst			        0xA0 //第1個加入會議
#define OnCfcJoin			          0xA1 //加入會議通話成功
#define OnCfcListen		          0xA2 //旁聽會議成功
#define OnCfcNotOpen	          0xA3 //會議未開通失敗
#define OnCfcJoinFail           0xA4 //加入會議失敗
#define OnCfcListenFail         0xA5 //旁聽會議失敗
#define OnCfcFull               0xA6 //人滿失敗

//會議室人數變化事件
#define OnCfcOne2Many           0xB0 //會議加入人數從1人變多人
#define OnCfcMany2One           0xB1 //會議加入人數從多人變1人
#define OnCfcBusy2Idle          0xB2 //會議加入人數從忙變有空位

//收到座席消息事件
#define OnAGBlindTran     		  0xC0 //收到電腦坐席快速轉接電話事件
#define OnAGConsultTran     		0xC1 //收到電腦坐席協商轉接電話事件
#define OnAGConfTran     		    0xC2 //收到電腦坐席會議轉接電話事件
#define OnAGStopTran        		0xC3 //收到電腦坐席停止轉接電話事件
#define OnAGConfSpeak  		    	0xC4 //收到電腦坐席指定通道會議發言事件
#define OnAGConfAudit  		    	0xC5 //收到電腦坐席指定通道旁聽會議事件
#define OnAGTranIVR        		  0xC6 //收到電腦坐席將來話轉到IVR事件
#define OnAGHold       		    	0xC7 //收到電腦坐席保持事件
#define OnAGUnHold       		    0xC8 //收到電腦坐席取消事件
#define OnAGMute        		    0xC9 //收到電腦坐席靜音事件
#define OnAGUnMute       		    0xCA //收到電腦坐席取消靜音事件
#define OnAGMsg11        		    0xCB //收到電腦坐席消息事件11
#define OnAGMsg12        		    0xCC //收到電腦坐席消息事件12
#define OnAGMsg13        		    0xCD //收到電腦坐席消息事件13
#define OnAGMsg14        		    0xCE //收到電腦坐席消息事件14
#define OnAGMsg15        		    0xCF //收到電腦坐席消息事件15

//自定義事件
#define OnSelfEvent0            0xE0 //自定義事件0
#define OnSelfEvent1            0xE1 //自定義事件1
#define OnSelfEvent2            0xE2 //自定義事件2
#define OnSelfEvent3            0xE3 //自定義事件3
#define OnSelfEvent4            0xE4 //自定義事件4
#define OnSelfEvent5            0xE5 //自定義事件5
#define OnSelfEvent6            0xE6 //自定義事件6
#define OnSelfEvent7            0xE7 //自定義事件7
#define OnSelfEvent8            0xE8 //自定義事件8
#define OnSelfEvent9            0xE9 //自定義事件9
#define OnSelfEvent10           0xEA //自定義事件10
#define OnSelfEvent11           0xEB //自定義事件11
#define OnSelfEvent12           0xEC //自定義事件12
#define OnSelfEvent13           0xED //自定義事件13
#define OnSelfEvent14           0xEE //自定義事件14
#define OnSelfEvent15           0xEF //自定義事件15

#define OnGotoEvent             0xF0 //跳轉事件

#define WAIT_EVENT			        0xFF //等待返回事件
//---------------------------------------------------------------------------
//最大值定義
//---------------------------------------------------------------------------
#define MAX_NODE_XML            10  //最大XML流程解析節點數
#define MAX_NODE_VOX            8   //最大語音節點數

#define MAX_CHAR32_LEN		      32	//32個字符串長度
#define MAX_CHAR64_LEN		      64	//64個字符串長度
#define MAX_CHAR128_LEN		      128	//128個字符串長度
#define MAX_CHAR256_LEN		      256	//256個字符串長度

#define MAX_ACCESSCODES_NUM		  256	//接入號碼最大個數
#define MAX_TELECODE_LEN		    32	//最長的電話號碼長度
#define MAX_CALLED_NUM          8   //呼出指令中最大被叫號碼個數

#define MAX_PATH_LEN			      128	//最長的路徑長度
#define MAX_FILENAME_LEN		    64	//最長的文件名長度(不可修改)
#define MAX_PATH_FILE_LEN		    164	//全路徑文件名最大長度

#define MAX_DTMFBUF_LEN			    64	//收碼緩沖最大長度
#define MAX_DTMFRULE_LEN		    16	//收碼規則串最大長度
#define MAX_ASRBUF_LEN			    64	//ASR緩沖最大長度
#define MAX_FSKBUF_LEN			    64	//FSK緩沖最大長度
#define MAX_SMS_LEN				      200	//短消息最大長度
#define MAX_COMP_LEN			      200	//合成串最大長度
#define MAX_TTS_LEN				      200	//文本串最大長度

#define MAX_USERNAME_LEN        32	//最長的用戶名長度(不可修改)
#define MAX_PASSWORD_LEN        32	//最長的密碼長度

#define MAX_ERRORBUF_LEN        1024 //最長的錯誤信息長度

#define MAX_VARNAME_LEN         25  //變量名的最長長度(不可修改)
#define MAX_VAR_DATA_LEN        1024 //256 //每個變量最大數據長度(不可修改)
#define MAX_CMD_ATTR_LEN        1024 //680 指令屬性區最大長度(不可修改) edit 2010-10-08
#define MAX_PRI_NUM             200 //局部變量最大地址數(范圍：通道)
#define MAX_PUB_NUM             100 //公共變量最大地址數(范圍：流程)
#define MAX_SYS_NUM             150 //全局變量最大地址數(范圍：系統) 按文件sysvar.txt,sysvar.cpp實際定義的數目定
#define MAX_FUNC_PARA_NUM       16  //函數最大參數個數
#define MAX_FUNC_NUM            21//50  //函數最大個數

#define MAX_MARKER_LEN          21  //通用標簽最大長度
#define MAX_CMD_LABEL_LEN       21  //每個指令行號標識最大長度(不可修改)
#define MAX_CMD_NAME_LEN        21  //每個指令名稱最大長度
#define MAX_ATTRIBUTE_NUM       48  //每條指令最大屬性個數 //edit 2010-0=10-22 從32改到48
#define MAX_ATTRIBUTE_LEN       21  //屬性名最大長度
//#define MAX_CMD_LINES           5000 //每個流程最大的指令行數(備注：移到cmd.h文件中定義2007-01-19)
#define MAX_MACRO_STRING_NUM    128  //每個宏定義最大字符串個數 EDIT 2009-04-17 從100改到128
#define MAX_MACRO_STRING_LEN    32  //每個宏定義字符串最大長度

//#define MAX_SESSION_NUM         240 //最大會話資源個數(備注：移到session.h文件中定義2007-01-19)
#define MAX_CHN_NUM				      240 //最大通道數
#define MAX_CONF_NUM			      128 //最大會議數
#define MAX_FLOW_NUM            64  //最大流程個數
//#define MAX_CMDS_NUM            177//200 //最大指令個數(備注：移到cmd.h文件中定義2007-01-19)
#define MAX_COMMENT_NUM         4   //最大注釋定義個數
#define MAX_REPLACE_NUM         16  //最大替換定義個數
#define MAX_MACRO_NUM           100 //最大宏定義個數
#define MAX_REGULA_NUM          10  //最大正則法定義個數
#define MAX_TIMER_NUM           5   //最大定時器個數

//#define MAX_KEYWORDS_NUM        256//最大關鍵字數
#define MAX_INT_RANGE_NUM       9//16 //最大個數
#define MAX_CHAR_RANGE_NUM      4//16 //最大個數
#define MAX_STR_RANGE_NUM       3//16 //最大個數
#define MAX_MACRO_INT_NUM       26//64 //最大個數
#define MAX_MACRO_STR_NUM       3//16 //最大個數


#define MAX_EVENT_NUMS			    20	//自定義事件個數

#define MAX_FLW_RETURN_LEVEL    16	//子流程調用嵌套層數

#define MAX_MSG_BUF_LEN         2048 //1024//消息最大長度

#define MAX_EXP_STRING_LEN      2048 //1024//表達式最大長度

#define MAX_SQL_BUF_NUM         256//數據庫操作最大條數
#define MAX_SQLS_LEN            1024 //SQL語句最大長度

#define MAX_SQLS_BUF_LEN        512 //SQL語句緩沖最大長度

#define MAX_STRING_SPLIT_NUM    64	//字符串分離及組合最大子串數

#define MAX_BILL_FEE_LEN        250 //話單費率最大記錄條數

#define MAX_MEM_PLAY_LIST_NUM	  1024 //內存放音文件列表最大數
#define MAX_PLAY_INDEX_NUM		  128 //放音緩沖區最大索引數

#define MAX_GET_FILE_NUM		    255 //取文件最大數

#define MAX_ROUTE_NUM			      128 //最大路由數 2013-09-22路由數從32增加到128
#define MAX_ROUTE_TRUNK_NUM     512 //每條路由最大中繼數
#define MAX_CHANGE_NUM			    32 //最大號碼轉換數
#define MAX_CHANGE_EXT_NUM      64 
#define MAX_CHANGE_VOIP_NUM     128 

#define MAX_DTMFRULE_BUF		    128	//最大收碼規則緩沖個數
#define MAX_PLAYRULE_BUF		    128	//最大放音規則緩沖個數
#define MAX_ASRRULE_BUF			    128	//最大asr規則緩沖個數
#define MAX_LINK_CHN_NUM		    32	//最大交換的通道個數

#define MAX_CALLOUT_BUF_NUM		  240	//最大呼出緩沖取記錄數

#define MAX_MSG_QUEUE_NUM	      1024	//最大接收消息隊列個數

#define MAX_CONFGROUP_NUM       8 //最大會議組數
#define MAX_ROOM_INGROUP_NUM    8 //每個會議組最大會議室數
#define MAX_TOTAL_ROOM_NUM      32 //最大總會議室數
#define MAX_CONFCHN_INGROUP_NUM 32 //每個會議組最大會議通道數

#define MAX_PRESIDER_NUM        5  //每個會議室最大主持人數
#define MAX_TALKERS_NUM	        25  //每個會議室最大加入人數
#define MAX_LISTENERS_NUM				240  //每個會議室最大旁聽人數

#define LANGUAGE_ENG						1 //英語
#define LANGUAGE_CHN						2 //普通話
#define LANGUAGE_LOC						3 //本地話
#define LANGUAGE_GDH						4 //廣東話
#define LANGUAGE_MNH						5 //閩南話
#define LANGUAGE_FRC						6 //法語
#define LANGUAGE_PTG						7 //葡萄牙語
#define LANGUAGE_ALB						8 //阿拉伯語

//---------------------------------------------------------------------------
//系統固定變量名
#define R_NULL					        50 //空值
#define R_DateTime				      51 //當前日期時間YYYY-MM-DD HH-MI-SS
#define R_Date     				      52 //當前日期YYYY-MM-DD
#define R_Time     				      53 //當前時間HH-MI-SS
#define R_Year        			    54 //當前年(yyyy)
#define R_Month       			    55 //當前月(01-12)
#define R_Day         			    56 //當前天(01-31)
#define R_Hour        			    57 //當前時(00-23)
#define R_Minute      			    58 //當前分(00-59)
#define R_Second				        59 //當前秒(00-59)
#define R_Msecond     			    60 //當前毫秒(00-999)
#define R_Weekday  				      61 //當前星期幾(1-7)
#define R_True					        62 //是
#define R_False					        63 //否
#define R_Yes					          64 //是
#define R_No					          65 //否
#define R_AllOnLines 			      66 //系統在線數
#define R_VoxPath				        67 //語音文件路徑
#define R_Langue				        68 //語言
#define R_Voice					        69 //聲音

//流程固定變量名
#define R_SessionId				      70 //當前呼叫會話業務序列號
#define R_StatusId				      71 //當前呼叫會話狀態標志(0-空閑，1-占用)
#define R_CallInOut				      72 //呼叫方向 0: idle, 1: in, 2: out
#define R_VxmlPoint				      73 //解析器節點號
#define R_ServerType			      74 //業務腳本功能服務類型
#define R_ServerNo				      75 //業務腳本功能服務號(業務流程編號)
#define R_SngDevice				      76 //信令控制設備號
#define R_SwtDevice				      77 //交換控制設備號
#define R_VoxDevice				      78 //語音控制設備號
#define R_CfcDevice				      79 //會議控制設備號
#define R_ChanType 				      80 //通道類型
#define R_ChanNo				        81 //通道號
#define R_CallerNo				      82 //主叫號碼
#define R_OrgCallerNo			      83 //原主叫號碼
#define R_CalledNo				      84 //被叫號碼
#define R_OrgCalledNo			      85 //原被叫號碼
#define R_CallTime				      86 //呼叫時間
#define R_AnsTime   			      87 //應答時間
#define R_RelTime   			      88 //釋放時間
#define R_Timer0				        89 //定時器0
#define R_Timer1				        90 //定時器1
#define R_Timer2				        91 //定時器2
#define R_Timer3				        92 //定時器3
#define R_Timer4				        93 //定時器4
#define R_SeatNo				        94 //坐席號
#define R_WorkerNo				      95 //話務員工號
#define R_CmdAddr				        96 //指令地址
#define R_OnLines				        97 //本流程在線人數
#define R_OnEvent				        98 //事件
#define R_DtmfBuf				        99 //收碼緩沖
#define R_AsrBuf				        100 //Asr緩沖
#define R_DialSerialNo	        101 //撥號器呼叫序列號
#define R_DialParam			        102 //撥號器呼叫參數
#define R_Error			            103 //錯誤信息
#define R_SessionNo             104 //當前呼叫會話業務序列號
#define R_ChHWType							105	//通道硬件類型
#define R_ChHWNo								106	//硬件通道序號
#define R_SeatType							107	//該通道綁定的坐席類型
#define R_CallerType						108	//主叫號碼類型
#define R_CalledType						109	//被叫號碼類型
#define R_RouteNo								110	//呼入通道的路由號
#define R_GWSerialNo            111 //外部數據網關發來的呼叫序列號
#define R_GWAccount	            112 //外部數據網關發來的賬號
#define R_GWMsgType	            113 //外部數據網關發來的數據類型
#define R_GWMsg		              114 //外部數據網關發來的數據消息
#define R_AGMsgType							115	//坐席電腦發來的消息類型
#define R_AGMsg									116	//坐席電腦發來的消息內容
#define R_SMSType								117	//收到的短消息類型
#define R_SMSMsg								118	//收到的短消息內容
#define R_AGPhoneType						119	//電腦坐席轉接的號碼類型: 1-外線號碼 2-坐席分機號 3-話務員工號
#define R_AGTranPhone						120	//電腦坐席轉接的號碼
#define R_AGTranParam						121	//電腦坐席傳遞給流程的附加參數
#define R_AGTranIVRId						122	//轉接的ivr標志,流程通過該標志約定來判斷跳轉到哪里
#define R_AGReturnId						123	//是否返回原服務坐席標志
#define R_GroupNo 							124	//呼入坐席所在的話務員組號
#define R_AGTranConfNo					125	//電腦坐席轉接的會議號
#define R_DBRecordRows  				126	//返回操作數據表影響的行數
#define R_DialDTMF							127	//內線分機撥打的號碼
#define R_Route0Idels					  128	//所有中繼空閑數
#define R_Route1Idels						129	//路由1中繼空閑數
#define R_Route2Idels						130	//路由2中繼空閑數
#define R_Route3Idels						131	//路由3中繼空閑數
#define R_Route4Idels						132	//路由4中繼空閑數
#define R_Route5Idels						133	//路由5中繼空閑數
#define R_Route6Idels						134	//路由6中繼空閑數
#define R_Route7Idels						135	//路由7中繼空閑數
#define R_RunOS							    136	//系統運行環境WIN32,LINUX
#define R_BandAGFirst						137	//先要綁定呼叫坐席標志(1-是 0-否)
#define R_CallType							138	//呼叫類型(0-普通呼叫 1-直接呼入 2-直接呼出 3-轉移呼入 4-代接呼入 5-強插監聽呼出)
#define R_AnswerId							139	//已應答標志(1-是 0-否)
#define R_CDRSerialNo						140	//詳細話單號
#define R_RecdRootPath					141	//坐席錄音文件根路徑
#define R_RecdFileName					142	//坐席錄音文件名
#define R_SwitchType						143	//硬件平臺類型：0：板卡模式 1：AVAYA-IPO系列 2：AVAYA-S系列 3: Alcatel-OmniPCX
#define R_DBType							  144	//數據庫類型：SQLSERVER，ORACLE，SYBASE，MYSQL，ACCESS，ODBC，NONE
#define R_RemoteFaxSID					145	//遠端傳真標識號
#define R_RecSysState						146	//第三方錄音系統登錄狀態：0-斷開，1-已登錄
#define R_SysVar147							147	//系統變量名147
#define R_SysVar148							148	//系統變量名148
#define R_SysVar149							149	//系統變量名149

//---------------------------------------------------------------------------
//錯誤信息定義
//---------------------------------------------------------------------------
//函數標志定義
#define FUNC_len		            0	//取字符串長度
#define FUNC_substring	        1   //取子字符串
#define FUNC_trim	              2	//去掉字符串兩頭空格
#define FUNC_lefttrim           3	//去掉字符串左空格
#define FUNC_righttrim          4	//去掉字符串右邊空格
#define FUNC_left	              5	//取左字符串
#define FUNC_right	            6	//取右字符串
#define FUNC_lower	            7	//將字符串小寫化
#define FUNC_upper	            8	//將字符串大寫化
#define FUNC_strfmt	            9	//將字符填充空格
#define FUNC_getnow	            10	//取當前日期時間函數
#define FUNC_getdate	          11	//取日期函數
#define FUNC_gettime	          12  //取時間函數
#define FUNC_getyear	          13	//取年函數
#define FUNC_getmonth	          14  //取月函數
#define FUNC_getday	            15	//取日函數
#define FUNC_gethour	          16	//取時函數
#define FUNC_getminute	        17	//取分函數
#define FUNC_getsecond	        18	//取秒函數
#define FUNC_getmsecond	        19	//取毫秒函數
#define FUNC_getweekday	        20	//取星期函數

//通道硬件狀態
#define CHN_HW_NOINIT		        0xFF	//未初始化
#define CHN_HW_VALID		        0x00	//有效
#define CHN_HW_BLOCK1		        0x01	//閉塞
#define CHN_HW_BLOCK2		        0x10	//閉塞
#define CHN_HW_BLOCK3		        0x11	//閉塞
//#define CHN_HW_CLOSE		        3	//關閉

//線路狀態定義
#define CHN_LN_IDLE		          0	//空閑
#define CHN_LN_SEIZE		        1	//占用
#define CHN_LN_RELEASE	        2	//釋放
#define CHN_LN_WAIT_REL	        3	//發送釋放后等待回應

//通道信令狀態
#define CHN_SNG_IDLE		        0	//空閑
#define CHN_SNG_DELAY		        1	//由忙轉空閑延時態
#define CHN_SNG_IN_ARRIVE	      2	//呼入到達
#define CHN_SNG_IN_WAIT		      3	//呼入等待后續號碼
#define CHN_SNG_IN_RING		      4	//呼入振鈴
#define CHN_SNG_IN_TALK		      5	//呼入通話
#define CHN_SNG_OT_READY	      6	//準備呼出
#define CHN_SNG_OT_HANGOFF	    7	//呼出摘機
#define CHN_SNG_OT_DTMF		      8	//呼出發送號碼
#define CHN_SNG_OT_WAIT		      9	//呼出等待結果
#define CHN_SNG_OT_RING		      10	//呼出振鈴
#define CHN_SNG_OT_TALK		      11	//呼出通話
#define CHN_SNG_IN_FAIL		      12	//呼入失敗
#define CHN_SNG_OT_FAIL		      13	//呼出失敗
#define CHN_SNG_OT_WAITREL      14	//呼出等待取消呼出結果

#define CHN_SNG_FOR_VOX     		16	//語音機狀態

#define CHN_SNG_IN_TEST		      20	//呼入大話務量測試

//業務狀態
#define CHN_SV_IDEL			        0	//空閑
#define CHN_SV_PLAYING		      1	//正在放音
#define CHN_SV_PLAYEND		      2	//放音結束
#define CHN_SV_PLAYFAIL		      3	//放音失敗
#define CHN_SV_RECORDING	      4	//正在錄音
#define CHN_SV_RECORDEND	      5	//錄音結束
#define CHN_SV_RECORDFAIL	      6	//錄音失敗
#define CHN_SV_DTMFRECV		      7	//接受DTMF
#define CHN_SV_DTMFFAIL		      8	//接受DTMF失敗
#define CHN_SV_DTMFEND		      9	//DTMF按鍵結束
#define CHN_SV_TALKING		      10	//正在通話
#define CHN_SV_MEETING		      11	//正在與會

// 呼出結果
#define CALLOUT_NOCALL		      0   // 還未呼出
#define CALLOUT_CALLING		      1   // 正在呼出(可發送后續地址)
#define CALLOUT_RING			      2   // 正在振鈴
#define CALLOUT_CONN			      3   // 用戶應答
#define CALLOUT_SLB			        4   // 用戶市忙
#define CALLOUT_STB			        5   // 用戶長忙
#define CALLOUT_NOBODY		      6   // 無人應答
#define CALLOUT_NOTRUNK		      7   // 無空閑出中繼
#define CALLOUT_UNN				      8   // 空號
#define CALLOUT_FAIL			      9	  // 失敗
#define CALLOUT_CNG			        10	// 設備擁塞
#define CALLOUT_CGC			        11	// 電路群擁塞
#define CALLOUT_ADI			        12	// 地址不全
#define CALLOUT_SST			        13	// 專用信號音
#define CALLOUT_OFF			        14	// 用戶關機
#define CALLOUT_BAR			        15	// 接入拒絕
#define CALLOUT_REFUSE			    16	// 坐席拒絕來電

//坐席相關定義
#define MAX_AG_NUM			        1024 //最大坐席終端數
#define MAX_AG_GROUP		        128 //最大話務員技能組數 2015-10-07 該宏定義從32改為128
#define MAX_ACD_GROUP		        256 //最大的ACD隊列數
#define MAX_AG_LEVEL            16 //最大話務員技能級別數
#define MAX_GROUP_NUM_FORWORKER 32  //每個話務員最大處理的業務功能組數  2015-10-07 該宏定義從8改為32
#define MAX_ACD_NUM			        64 //最大ACD分配隊列
#define MAX_AG_SRVLINE_NUM      4 //每個坐席同時服務的最大線路數

//坐席類型
#define SEATTYPE_AG_TEL		  		1	//內線電話電話(來話時電話機會振鈴,通過摘機接聽電話,無電腦也可工作,也可以登錄電腦)
#define SEATTYPE_AG_PC		    	2	//內線電腦座席(來話時電話機不會振鈴,通過電腦提示來話,所有操作全部通過電腦,必須電腦登陸才能使用)
#define SEATTYPE_DT_TEL	    		3	//數字線電話座席
#define SEATTYPE_DT_PC	    		4	//數字線電腦座席
#define SEATTYPE_RT_TEL	      	5	//遠程座席
#define SEATTYPE_RT_VOIP      	6	//遠程voip座席
#define SEATTYPE_SWT_IVR      	7	//交換機IVR端口

//坐席對應的坐席通道狀態
//注: 參照"通道信令狀態"的定義

//坐席服務狀態
#define AG_SV_IDEL		        	0	//空閑
#define AG_SV_DELAY		        	1	//由忙轉空閑延時態
#define AG_SV_INSEIZE 	      	2	//呼入占用
#define AG_SV_OUTSEIZE 	      	3	//呼出占用
#define AG_SV_INRING		      	4	//呼入振鈴
#define AG_SV_OUTRING		      	5	//呼出振鈴
#define AG_SV_CONN		      		6	//電話接通狀態
#define AG_SV_WAIT_REL		      7	//等待坐席電話掛機
#define AG_SV_PLAY							8 //放音狀態
#define AG_SV_ACW 							9 //話后處理
#define AG_SV_HOLD 							10//保持狀態
#define AG_SV_WAIT_RETURN				11//等待IVR返回狀態
#define AG_SV_HANGUP				    12//摘機狀態
#define AG_SV_CONSULT				    13//求助狀態
#define AG_SV_IMCHAT				    14//IM交談
#define AG_SV_PROC_EMAIL				15//處理郵件
#define AG_SV_PROC_FAX				  16//處理傳真
#define AG_SV_ACDLOCK				    17//ACD分配

//AVAYA坐席狀態
#define AG_AM_LOG_IN						0 //登錄后直接進入Ready狀態
#define AG_AM_LOG_OUT						1
#define AG_AM_NOT_READY					2
#define AG_AM_READY							3
#define AG_AM_WORK_NOT_READY		4
#define AG_AM_WORK_READY				5
#define AG_AM_LOG_IN_NOTREADY  	9 //登錄后直接進入NotReady狀態
#define AG_AM_LOG_IN_ACW  	    10 //登錄后直接進入整理狀態
#define AG_AM_LOG_IN_GROUP 	    98 //登記到話務組
#define AG_AM_LOG_OUT_GROUP 	  99 //登記到話務組

//坐席話務統計狀態
#define AG_STATUS_LOGOUT		    0	//未登錄
#define AG_STATUS_LOGIN		      1	//登錄
#define AG_STATUS_BUSY		      2	//示忙態
#define AG_STATUS_IDEL		  		3	//空閑態
#define AG_STATUS_INRING		    4	//呼入振鈴
#define AG_STATUS_OUTSEIZE 	    5	//呼出占用
#define AG_STATUS_OUTRING		    6	//呼出振鈴
#define AG_STATUS_TALK		      7	//說話狀態
#define AG_STATUS_HOLD		      8	//保持狀態
#define AG_STATUS_ACW 					9 //話后處理
#define AG_STATUS_LEAVE 				10 //離席小休態
#define AG_STATUS_ABAND 				11 //來話放棄
#define AG_STATUS_IVR 				  12 //占用IVR
#define AG_STATUS_CONSULT			  13 //咨詢狀態
#define AG_STATUS_CONF			    14 //會議狀態
#define AG_STATUS_CONNECT		    15 //電話持續接通時長
#define AG_STATUS_HANDLECALL    16 //整個呼叫處理時長
#define AG_STATUS_TRANSFER    	17 //轉接電話 2017-04-29
#define AG_STATUS_WORK		  		99 //工作態

//放音狀態
#define PLAY_NONE       				0	//沒有放音
#define PLAY_PLAYING		        1	//正在放音
#define PLAY_PLAYEND_OVER		    2	//全部內容完畢結束放音
#define PLAY_PLAYEND_DTMF		    3	//DTMF按鍵打斷結束放音
#define PLAY_PLAYEND_VOICE	    4	//語音打斷結束放音
#define PLAY_PLAYEND_APP	      5	//主動停止結束放音
#define PLAY_PLAYEND_HANGON     6	//掛機結束放音
#define PLAY_PLAYEND_ERROR      7	//其他錯誤結束放音

//錄音狀態
#define REOCRD_NONE       			0	//沒有錄音
#define REOCRD_RECING		      	1	//正在錄音
#define REOCRD_RECEND_OVER		  2	//全部內容完畢停止錄音
#define REOCRD_RECEND_DTMF		  3	//DTMF按鍵打斷停止錄音
#define REOCRD_RECEND_APP	    	4	//主動停止錄音
#define REOCRD_RECEND_HANGON  	5	//掛機停止錄音
#define REOCRD_RECEND_ERROR   	6	//其他錯誤停止錄音

//告警級別定義
#define ALARM_LEVEL_MAJOR       1 //重要告警
#define ALARM_LEVEL_MINOR       2 //次要告警
#define ALARM_LEVEL_WARN        3 //閥值提醒
#define ALARM_LEVEL_INFO        4 //提示信息

/*
平臺告警代碼
1001：CTI鏈路斷開
1002：CTI鏈路恢復
1003：數據庫連接斷開
1004：數據庫連接恢復
1005：CTILINK服務網絡斷開
1006：CTILINK服務網絡恢復
1007：DB服務網絡斷開
1008：DB服務網絡恢復
1009：FLW服務網絡斷開
1010：FLW服務網絡恢復
1011：TTS服務網絡斷開
1012：TTS服務網絡連接
1013：WebSocket網絡斷開
1014：WebSocket網絡恢復
1015：RS232串服務網絡斷開
1016：RS232串服務網絡連接
1017：VOP服務網絡斷開
1018：VOP服務網絡連接
1019：IPREC服務網絡斷開
1020：IPREC服務網絡連接
1021：通道阻塞
1022：通道恢復正常
*/

//-------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
