//---------------------------------------------------------------------------
#ifndef TypeDefH
#define TypeDefH
//---------------------------------------------------------------------------

#define CH		                char
#define SC                    signed char
#define UC		                unsigned char
#define US		                unsigned short
#define UI		                unsigned int
#define SI                    signed short
#define UL                    unsigned long
#define DF                    double



//---------------------------------------------------------------------------
#endif
