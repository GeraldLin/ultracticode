//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <SvcMgr.hpp>
#include <vcl.h>
#include <ADODB.hpp>
#include <DB.hpp>
#include "TypeDef.h"
#include "boxinc.h"
#include "ThreadProc.h"
#include "ThreadConnDB.h"
#include "ThreadProcTCP.h"
#include "ThreadProcCDR.h"
#include "FlwRule.h"
#include "FlwData.h"
#include "MsgProc.h"
#include <ExtCtrls.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTPServer.hpp>
#include <IdTCPServer.hpp>
//---------------------------------------------------------------------------
class TQuarkCallDBServer : public TService
{
__published:    // IDE-managed Components
  TADOConnection *ADOConnection1;
  TADOConnection *ADOConnection2;
  TADOQuery *adoPublic2;
  TADOQuery *adoPublic1;
  TIdHTTPServer *IdHTTPServer1;
  void __fastcall ServiceCreate(TObject *Sender);
  void __fastcall ServiceStart(TService *Sender, bool &Started);
  void __fastcall ServiceStop(TService *Sender, bool &Stopped);
  void __fastcall ServicePause(TService *Sender, bool &Paused);
  void __fastcall ServiceContinue(TService *Sender, bool &Continued);
  void __fastcall ServiceDestroy(TObject *Sender);
  void __fastcall ServiceBeforeInstall(TService *Sender);
  void __fastcall IdHTTPServer1CommandGet(TIdPeerThread *AThread,
          TIdHTTPRequestInfo *RequestInfo,
          TIdHTTPResponseInfo *ResponseInfo);
private:        // User declarations
public:         // User declarations
	__fastcall TQuarkCallDBServer(TComponent* Owner);
	TServiceController __fastcall GetServiceController(void);

	friend void __stdcall ServiceController(unsigned CtrlCode);

  void WriteParserStatus(int logid);
  void WriteConnDBStatus(int connid);
  
  CThreadProc *ThreadProc;
  CThreadConnDB *ThreadConnDB;
  CThreadProcTCP *ThreadProcTCP;
  CThreadProcCDR *ThreadProcCDR;

  US CombineID(UC IdType, UC IdNo);
  //重新連接數據庫
  int ReConnectDB(void);
  void __fastcall OnAppLoginx(unsigned short serverid,unsigned short clientid);
  void __fastcall OnAppClosex(unsigned short serverid,unsigned short clientid);
  void __fastcall OnAppReceiveDatax(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len);
  //解密密碼
  char *DecodePass(const char *Pass);
  AnsiString DecodePass(AnsiString Pass);

  //顯示節點登錄及退出信息
  void Disp_LoginMsg(UC LogId, UC NodeType, UC NodeNo, const CH *LogMsg);
  //顯示執行指令的錯誤及告警信息
  void Disp_ErrMsg(UC MsgType, UL SessionId, UL CmdAddr, UC ChnType, US ChnNo, const CH *ErrMsg);
  //顯示通信消息
  void Disp_Comm_Msg(UC MsgType, const CH *CommMsg);
  void WriteSQLs(UC SqlType, const CH *sqls);
  void MainLoop();
  void ReConnectDBLoop();

  int CreateCDRMonthTable(const char *pszMonth, bool bDelBefore);
  int CreateSTATUSMonthTable(const char *pszMonth, bool bDelBefore);
  void ProcCDR();
};
//---------------------------------------------------------------------------
extern PACKAGE TQuarkCallDBServer *QuarkCallDBServer;
//---------------------------------------------------------------------------
#endif
