//---------------------------------------------------------------------------

#ifndef ThreadProcCDRH
#define ThreadProcCDRH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class CThreadProcCDR : public TThread
{            
private:
protected:
  void __fastcall Execute();
public:
  __fastcall CThreadProcCDR(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
