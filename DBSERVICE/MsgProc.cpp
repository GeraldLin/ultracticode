//---------------------------------------------------------------------------


#pragma hdrstop

#include <dbtables.hpp>
#include "MsgProc.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

extern HANDLE Mutex_RecvMsg;

extern US gHTTPServerId;
extern CFlwData *FlwData;
extern CFlwRule FlwRule;
extern CTCPServer *TcpServer;
extern CMsgfifo *Msgfifo;
extern CXMLRcvMsg RcvMsg;
extern AnsiString g_strServiceINIFileName;
extern unsigned short IVRServerID;
//---------------------------------------------------------------------------
//---------------類型轉換函數--------------------------------------------------
//整數字符串有效判斷(0-有效的整數字符串、并返回具體數值，1-超出范圍，2-無效的整數字符串)
int Check_Int_Str(CH *ValueStr, int &IntValue)
{
    try
    {
        IntValue = StrToInt((char*)ValueStr);
        return 0;
    }
    catch(EConvertError &exception)
    {
        //不是有效的整數錯誤
        return 1;
    }
}
int Check_long_Str(CH *ValueStr, long &IntValue)
{
    try
    {
        IntValue = StrToInt64((char*)ValueStr);
        return 0;
    }
    catch(EConvertError &exception)
    {
        //不是有效的整數錯誤
        return 1;
    }
}
//整數轉字符串
int Int_To_Str( int IntValue, UC *ValueStr )
{
    AnsiString result;
    result = IntToStr(IntValue);
    strcpy(ValueStr, result.c_str());
    return 0;
}
//浮點數字符串有效判斷
int Check_Float_Str( UC *ValueStr, double &FloatValue )
{
    try
    {
        FloatValue = StrToFloat((char*)ValueStr);
        return 0;
    }
    catch(EConvertError &exception)
    {
        //不是有效的浮點數錯誤
        return 1;
    }
}
//浮點數轉字符串
int Float_To_Str( int IntValue, UC *ValueStr )
{
    AnsiString result;
    result = CurrToStr(IntValue);
    strcpy(ValueStr, result.c_str());
    return 0;
}
//分離文本行(,) //edit 2009-02-15 add
int SplitTxtLine(const char *txtline, int maxnum, CString *arrstring)
{
  int len, i, k = 0, num = 0;
  char ch, ch1, ch2 = 0, strtemp[64];

  memset(strtemp, 0, 64);
  len = strlen(txtline);
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    ch1 = ch2;
    ch2 = ch;
    if (ch1 == '/' && ch2 == '/')
    {
      //備注標志  
      if (k > 0)
      {
        strtemp[k] = '\0';
        arrstring[num] = strtemp;
        num ++;
      }
      break;
    }
    if (ch == ',' || ch == ';'   || ch == ' ' || ch == '\n' || ch == '\t' || ch == 0)
    {
      //間隔標志
      if (k == 0 && (ch != ',' && ch != ';'))
        continue;
      strtemp[k] = '\0';
      arrstring[num] = strtemp;  
      num ++;
      k = 0;
      if (num >= maxnum)
        break;
    }
    else
    {
      if (k < 64)
      {
        strtemp[k] = ch;
        k ++;
      }
    }
  }
  return num;
}
//根據分離字符分割字符串 //edit 2009-02-15 add
int SplitString(const char *txtline, const char splitchar, int maxnum, CString *arrstring)
{
  int len, i, k = 0, num = 0;
  char ch, strtemp[64];

  memset(strtemp, 0, 64);
  len = strlen(txtline);
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    if (ch == splitchar || ch == 0)
    {
      //間隔標志
      strtemp[k] = '\0';
      arrstring[num] = strtemp;  
      num ++;
      k = 0;
      if (num >= maxnum)
        break;
    }
    else
    {
      if (k < 64)
      {
        strtemp[k] = ch;
        k ++;
      }
    }
  }
  return num;
}
//根據字符串名取參數類型  //edit 2009-02-15 add
TFieldType GetFieldTypeByString(const char *fieldtype)
{
  if (strcmpi(fieldtype, "String") == 0)
    return ftString;
  else if (strcmpi(fieldtype, "Smallint") == 0)
    return ftSmallint;
  else if (strcmpi(fieldtype, "Integer") == 0 || strcmpi(fieldtype, "Int") == 0)
    return ftInteger;
  else if (strcmpi(fieldtype, "Word") == 0)
    return ftWord;
  else if (strcmpi(fieldtype, "Boolean") == 0)
    return ftBoolean;
  else if (strcmpi(fieldtype, "Float") == 0)
    return ftFloat;
  else if (strcmpi(fieldtype, "Currency") == 0)
    return ftCurrency;
  else if (strcmpi(fieldtype, "BCD") == 0)
    return ftBCD;
  else if (strcmpi(fieldtype, "Date") == 0)
    return ftDate;
  else if (strcmpi(fieldtype, "Time") == 0)
    return ftTime;
  else if (strcmpi(fieldtype, "DateTime") == 0)
    return ftDateTime;
  else if (strcmpi(fieldtype, "Bytes") == 0)
    return ftBytes;
  else
    return ftUnknown;
}
//根據字符串名取參數輸出方向 //edit 2009-02-15 add
TParameterDirection GetDirectionByString(const char *directionstr)
{
  if (strcmpi(directionstr, "in") == 0)
    return pdInput;
  else if (strcmpi(directionstr, "out") == 0)
    return pdOutput;
  else if (strcmpi(directionstr, "inout") == 0)
    return pdInputOutput;
  else if (strcmpi(directionstr, "return") == 0)
    return pdReturnValue;
  else
    return pdUnknown;
}
//設置存儲過程參數  //edit 2009-02-15 add
void CreateParameters(const char *params, TADOStoredProc *sdosp)
{
  char ParamStr[32];
  CString Param1CStr[20], Param2CStr[4];
  int Param1Num;

  sdosp->Parameters->Clear();
  Param1Num = SplitString(params, ';', 20, Param1CStr);
  for (int i=0; i<Param1Num; i++)
  {
    Param2CStr[0] = "";
    Param2CStr[1] = "";
    Param2CStr[2] = "";
    Param2CStr[3] = "";
    SplitString(Param1CStr[i].C_Str(), ',', 4, Param2CStr);
    if (Param2CStr[0].GetLength() > 0)
    {
      sdosp->Parameters->CreateParameter(Param2CStr[0].C_Str(),
                                         GetFieldTypeByString(Param2CStr[1].C_Str()),
                                         GetDirectionByString(Param2CStr[2].C_Str()),
                                         128,
                                         Param2CStr[3].C_Str());
    }
  }
}
//---------------------------------------------------------------------------
//增加XML消息頭名稱
void Add_XML_Msg_Header(UC *MsgBuf, US MsgId, UC *MsgName, UL SerialNo, UL CurrCmdAddr, UC ChnType, US ChnNo)
{
    sprintf(MsgBuf, "<%s sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\"", MsgName, SerialNo, CurrCmdAddr, ChnType, ChnNo);
}
//增加XML消息屬性內容
void Add_XML_Msg_Attr(UC *MsgBuf, UC *AttrName, UC *AttrValue)
{
    UC MsgTemp[MAX_MSG_BUF_LEN];

    sprintf(MsgTemp, "%s %s=\"%s\"", MsgBuf, AttrName, AttrValue);
    strcpy(MsgBuf, MsgTemp);
}
//增加XML消息尾
void Add_XML_Msg_Trail(UC *MsgBuf)
{
    strcat(MsgBuf, "/>");
}
void Get_Chn_Data(CXMLRcvMsg &XMLMsg, UL &SessionId, UL &CmdAddr, UC &ChnType, US &ChnNo)
{
  SessionId = atol(XMLMsg.GetAttrValue(0).C_Str());
  CmdAddr = atol(XMLMsg.GetAttrValue(1).C_Str());
  ChnType = atoi(XMLMsg.GetAttrValue(2).C_Str());
  ChnNo = atoi(XMLMsg.GetAttrValue(3).C_Str());
}
//---------------------------------------------------------------------------
//發送消息
void SendMessage(US ClientId, US MsgId, long MsgLen, UC *msg)
{
  CH DispMsg[MAX_EXP_STRING_LEN];

  if (WaitForSingleObject(Mutex_RecvMsg, 100) == WAIT_OBJECT_0)
  {
    if (TcpServer->SendMessage(ClientId, (MSGTYPE_DBXML<<8)|MsgId, MsgLen, msg) > 0)
    {
      ReleaseMutex(Mutex_RecvMsg);
      if (FlwData->DispComId == 1 || FlwData->WriteComId == 1)
      {
        sprintf(DispMsg, "remoteid=0x%04x, msgtype=0x%04x, msg=%s", ClientId, (MSGTYPE_DBXML<<8)|MsgId, (char *)msg);
        QuarkCallDBServer->Disp_Comm_Msg(1, DispMsg);
      }
    }
    else
    {
      ReleaseMutex(Mutex_RecvMsg);
    }
  }
}
void Send_GWQuery_Result(CXMLRcvMsg &XMLMsg, US Result)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ongwquery sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" result=\"%d\" msgtype=\"%s\" item0=\"\" item1=\"\" item2=\"\" item3=\"\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            Result,
            XMLMsg.GetAttrValue(4).C_Str()
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ongwquery, strlen(MsgBuf), MsgBuf);
}
//發送查詢數據表結果消息
void Send_DBQuery_Result(US SqlBufId)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbquery sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" records=\"%d\" fields=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->QuerySQLBuf[SqlBufId].SessionId,
            FlwData->QuerySQLBuf[SqlBufId].CmdAddr,
            FlwData->QuerySQLBuf[SqlBufId].ChnType,
            FlwData->QuerySQLBuf[SqlBufId].ChnNo,
            FlwData->QuerySQLBuf[SqlBufId].RecordCount,
            FlwData->QuerySQLBuf[SqlBufId].FieldCount,
            FlwData->QuerySQLBuf[SqlBufId].DBEvent,
            FlwData->QuerySQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->QuerySQLBuf[SqlBufId].ClientId, MSG_ondbquery, strlen(MsgBuf), MsgBuf);
}
void Send_DBQuery_Result(CXMLRcvMsg &XMLMsg, int records, int fields, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbquery sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" records=\"%d\" fields=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            records,
            fields,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbquery, strlen(MsgBuf), MsgBuf);
}
//發送查詢數據表記錄擴展指令消息
void Send_DBSelect_Result(US SqlBufId, CH *ValueList)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbselect sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" records=\"%d\" fields=\"%d\" fieldlist=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->QuerySQLBuf[SqlBufId].SessionId,
            FlwData->QuerySQLBuf[SqlBufId].CmdAddr,
            FlwData->QuerySQLBuf[SqlBufId].ChnType,
            FlwData->QuerySQLBuf[SqlBufId].ChnNo,
            FlwData->QuerySQLBuf[SqlBufId].RecordCount,
            FlwData->QuerySQLBuf[SqlBufId].FieldCount,
            ValueList,
            FlwData->QuerySQLBuf[SqlBufId].DBEvent,
            FlwData->QuerySQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->QuerySQLBuf[SqlBufId].ClientId, MSG_ondbselect, strlen(MsgBuf), MsgBuf);
}
void Send_DBSelect_FailResult(US SqlBufId, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbselect sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" records=\"%d\" fields=\"%d\" fieldlist=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->QuerySQLBuf[SqlBufId].SessionId,
            FlwData->QuerySQLBuf[SqlBufId].CmdAddr,
            FlwData->QuerySQLBuf[SqlBufId].ChnType,
            FlwData->QuerySQLBuf[SqlBufId].ChnNo,
            0,
            0,
            "",
            OnDBFail,
            ErrorBuf
            );
    SendMessage(FlwData->QuerySQLBuf[SqlBufId].ClientId, MSG_ondbselect, strlen(MsgBuf), MsgBuf);
}
void Send_DBSelect_Result(CXMLRcvMsg &XMLMsg, int records, int fields, CH *ValueList, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbselect sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" records=\"%d\" fields=\"%d\" fieldlist=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            records,
            fields,
            ValueList,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbselect, strlen(MsgBuf), MsgBuf);
}
//發送取字段值結果消息
void Send_DBField_Value(US SqlBufId, CH *Value)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbfield sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" value=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->QuerySQLBuf[SqlBufId].SessionId,
            FlwData->QuerySQLBuf[SqlBufId].CmdAddr,
            FlwData->QuerySQLBuf[SqlBufId].ChnType,
            FlwData->QuerySQLBuf[SqlBufId].ChnNo,
            FlwData->QuerySQLBuf[SqlBufId].Value,
            FlwData->QuerySQLBuf[SqlBufId].DBEvent,
            FlwData->QuerySQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->QuerySQLBuf[SqlBufId].ClientId, MSG_ondbfield, strlen(MsgBuf), MsgBuf);
}
void Send_DBField_Value(CXMLRcvMsg &XMLMsg, CH *Value, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbfield sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" value=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            Value,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbfield, strlen(MsgBuf), MsgBuf);
}
//發送移動記錄指針結果消息
void Send_DBMove_Result(US SqlBufId)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbmove sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" recordno=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->QuerySQLBuf[SqlBufId].SessionId,
            FlwData->QuerySQLBuf[SqlBufId].CmdAddr,
            FlwData->QuerySQLBuf[SqlBufId].ChnType,
            FlwData->QuerySQLBuf[SqlBufId].ChnNo,
            FlwData->QuerySQLBuf[SqlBufId].CurrRecdNo,
            FlwData->QuerySQLBuf[SqlBufId].DBEvent,
            FlwData->QuerySQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->QuerySQLBuf[SqlBufId].ClientId, MSG_ondbmove, strlen(MsgBuf), MsgBuf);
}
void Send_DBMove_Result(CXMLRcvMsg &XMLMsg, int RecdNo, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbmove sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" recordno=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            RecdNo,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbmove, strlen(MsgBuf), MsgBuf);
}
//發送插入、修改、刪除記錄結果消息
void Send_DBCmd_Result(US SqlBufId)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbcmd sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" records=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->ExecSQLBuf[SqlBufId].SessionId,
            FlwData->ExecSQLBuf[SqlBufId].CmdAddr,
            FlwData->ExecSQLBuf[SqlBufId].ChnType,
            FlwData->ExecSQLBuf[SqlBufId].ChnNo,
            FlwData->ExecSQLBuf[SqlBufId].RecordCount,
            FlwData->ExecSQLBuf[SqlBufId].DBEvent,
            FlwData->ExecSQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->ExecSQLBuf[SqlBufId].ClientId, MSG_ondbcmd, strlen(MsgBuf), MsgBuf);
}
void Send_DBCmd_Result(CXMLRcvMsg &XMLMsg, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbcmd sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" records=\"0\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbcmd, strlen(MsgBuf), MsgBuf);
}
//發送寫話單結果
void Send_WriteBill_Result(US SqlBufId)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<onwritebill sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" tollfee=\"%f\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->ExecSQLBuf[SqlBufId].SessionId,
            FlwData->ExecSQLBuf[SqlBufId].CmdAddr,
            FlwData->ExecSQLBuf[SqlBufId].ChnType,
            FlwData->ExecSQLBuf[SqlBufId].ChnNo,
            FlwData->ExecSQLBuf[SqlBufId].TollFee,
            FlwData->ExecSQLBuf[SqlBufId].DBEvent,
            FlwData->ExecSQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->ExecSQLBuf[SqlBufId].ClientId, MSG_onwritebill, strlen(MsgBuf), MsgBuf);
}
void Send_WriteBill_Result(CXMLRcvMsg &XMLMsg, double tollfee, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<onwritebill sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" tollfee=\"%f\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            tollfee,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_onwritebill, strlen(MsgBuf), MsgBuf);
}
//發送通話時長結果
void Send_TollTime_Result(CXMLRcvMsg &XMLMsg, int tolltime, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ongettolltime sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" tolltime=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            tolltime,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ongettolltime, strlen(MsgBuf), MsgBuf);
}
//發送讀數據表當前記錄字段值結果
void Send_DBRecord_Result(CXMLRcvMsg &XMLMsg, int RecdNo, CH *ValueList, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbrecord sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" recordno=\"%d\" fieldlist=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            RecdNo,
            ValueList,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbrecord, strlen(MsgBuf), MsgBuf);
}

//發送執行存儲過程操作結果消息 //edit 2009-02-15 add
void Send_DBSP_Result(US SqlBufId)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbsp sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->SPBuf[SqlBufId].SessionId,
            FlwData->SPBuf[SqlBufId].CmdAddr,
            FlwData->SPBuf[SqlBufId].ChnType,
            FlwData->SPBuf[SqlBufId].ChnNo,
            FlwData->SPBuf[SqlBufId].DBEvent,
            FlwData->SPBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->SPBuf[SqlBufId].ClientId, MSG_ondbsp, strlen(MsgBuf), MsgBuf);
}
void Send_DBSP_Result(CXMLRcvMsg &XMLMsg, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbsp sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbsp, strlen(MsgBuf), MsgBuf);
}
//發送取存儲過程輸出參數值結果 //edit 2009-02-15 add
void Send_DBSPoutparam_Result(CXMLRcvMsg &XMLMsg, CH *ValueList, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbspoutparam sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" outparamlist=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            ValueList,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbspoutparam, strlen(MsgBuf), MsgBuf);
}
//發送將字段值存入文件結果消息
void Send_DBFieldToFile_Result(US SqlBufId, CH *Value)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbfieldtofile sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" value=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->QuerySQLBuf[SqlBufId].SessionId,
            FlwData->QuerySQLBuf[SqlBufId].CmdAddr,
            FlwData->QuerySQLBuf[SqlBufId].ChnType,
            FlwData->QuerySQLBuf[SqlBufId].ChnNo,
            FlwData->QuerySQLBuf[SqlBufId].Value,
            FlwData->QuerySQLBuf[SqlBufId].DBEvent,
            FlwData->QuerySQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->QuerySQLBuf[SqlBufId].ClientId, MSG_ondbfieldtofile, strlen(MsgBuf), MsgBuf);
}
void Send_DBFieldToFile_Result(CXMLRcvMsg &XMLMsg, CH *Value, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbfieldtofile sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" value=\"%s\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            Value,
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbfieldtofile, strlen(MsgBuf), MsgBuf);
}
//發送將文件內容存入數據表結果消息
void Send_DBFileToField_Result(US SqlBufId)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbfiletofield sessionid=\"%d\" cmdaddr=\"%d\" chantype=\"%d\" channo=\"%d\" records=\"%d\" result=\"%d\" errorbuf=\"%s\"/>",
            FlwData->ExecSQLBuf[SqlBufId].SessionId,
            FlwData->ExecSQLBuf[SqlBufId].CmdAddr,
            FlwData->ExecSQLBuf[SqlBufId].ChnType,
            FlwData->ExecSQLBuf[SqlBufId].ChnNo,
            FlwData->ExecSQLBuf[SqlBufId].RecordCount,
            FlwData->ExecSQLBuf[SqlBufId].DBEvent,
            FlwData->ExecSQLBuf[SqlBufId].ErrorBuf
            );
    SendMessage(FlwData->ExecSQLBuf[SqlBufId].ClientId, MSG_ondbfiletofield, strlen(MsgBuf), MsgBuf);
}
void Send_DBFileToField_Result(CXMLRcvMsg &XMLMsg, US Result, CH *ErrorBuf)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<ondbfiletofield sessionid=\"%s\" cmdaddr=\"%s\" chantype=\"%s\" channo=\"%s\" records=\"0\" result=\"%d\" errorbuf=\"%s\"/>",
            XMLMsg.GetAttrValue(0).C_Str(),
            XMLMsg.GetAttrValue(1).C_Str(),
            XMLMsg.GetAttrValue(2).C_Str(),
            XMLMsg.GetAttrValue(3).C_Str(),
            Result,
            ErrorBuf
            );
    SendMessage(XMLMsg.GetClientId(), MSG_ondbfiletofield, strlen(MsgBuf), MsgBuf);
}
void Send_WEBRequestEvent(const CH *Command, const CH *CommaText)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<onwebhttprequest sessionid=\"0\" command=\"%s\" commatext=\"%s\"/>",
            Command,
            CommaText
            );
    SendMessage(gHTTPServerId, MSG_onwebhttprequest, strlen(MsgBuf), MsgBuf);
}
//發送連接數據庫失敗消息到IVR
void Send_ConnectDBFail(int nMsgType)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<onconnectdbfail sessionid=\"0\" cmdaddr=\"0\" chantype=\"0\" channo=\"0\" records=\"0\" result=\"1\" errorbuf=\"Connect DB fail\"/>");
    SendMessage(IVRServerID, nMsgType, strlen(MsgBuf), MsgBuf);
}
//發送連接數據庫成功消息到IVR //2016-10-05
void Send_ConnectDBSucc(int nMsgType)
{
    UC MsgBuf[MAX_MSG_BUF_LEN];

    sprintf(MsgBuf, "<onconnectdbsucc sessionid=\"0\" cmdaddr=\"0\" chantype=\"0\" channo=\"0\" records=\"0\" result=\"0\" errorbuf=\"Connect DB success\"/>");
    SendMessage(IVRServerID, nMsgType, strlen(MsgBuf), MsgBuf);
}
//---------------------------------------------------------------------------
//組合類型、編號
US CombineID(UC IdType, UC IdNo)
{
    US m_i1, m_i2;
    m_i1 = ( IdType << 8 ) & 0xFF00;
    m_i2 = IdNo & 0x00FF;
    return (m_i1 | m_i2);
}
//分解類型、編號
void DisCombineID(US ID, UC &IdType, UC &IdNo)
{
    IdType = ( ID >> 8 ) & 0x00FF;
    IdNo = ID & 0x00FF;
}
//處理消息隊列緩沖區
void ProcQueue(void)
{
	US result, count = 0, ClientId, MsgId;
  CH MsgBuf[2048];

  while(!Msgfifo->IsEmpty() && (count++) < 256)
  {
    result = 0;
    if (WaitForSingleObject(Mutex_RecvMsg, 100) == WAIT_OBJECT_0)
    {
      result = Msgfifo->Read(ClientId, MsgId, MsgBuf);
      ReleaseMutex(Mutex_RecvMsg);
    }
    if (result != 0)
    {
      MsgId = MsgId & 0xFF;
      RcvMsg.SetMsgBufId(MsgId,(char *)MsgBuf);  //裝載到Cstring中
      RcvMsg.SetClientId(ClientId);
      /*if(0==RcvMsg.ParseSndMsgWithCheck(FlwRule.XMLDBMsgRule[MsgId]))
      switch (MsgId)
      {
        case MSG_gwquery:	//查詢外部數據網關
        {
            Proc_MSG_gwquery(RcvMsg);
            break;
        }
        case MSG_dbquery: //查詢數據操作
        {
            Proc_MSG_dbquery(RcvMsg);
            break;
        }
        case MSG_dbfieldno: //根據字段序號取字段值操作
        {
            Proc_MSG_dbfieldno(RcvMsg);
            break;
        }
        case MSG_dbfieldname: //根據字段名取字段值操作
        {
            Proc_MSG_dbfieldname(RcvMsg);
            break;
        }
        case MSG_dbfirst: //記錄指針移到首記錄
        {
            Proc_MSG_dbfirst(RcvMsg);
            break;
        }
        case MSG_dbnext: //記錄指針移到下一記錄
        {
            Proc_MSG_dbnext(RcvMsg);
            break;
        }
        case MSG_dbprior: //記錄指針移到前一記錄
        {
            Proc_MSG_dbprior(RcvMsg);
            break;
        }
        case MSG_dblast: //記錄指針移到末記錄
        {
            Proc_MSG_dblast(RcvMsg);
            break;
        }
        case MSG_dbgoto: //記錄指針移到指定記錄
        {
            Proc_MSG_dbgoto(RcvMsg);
            break;
        }
        case MSG_dbinsert: //插入記錄操作
        {
            Proc_MSG_dbinsert(RcvMsg);
            break;
        }
        case MSG_dbupdate: //修改記錄操作
        {
            Proc_MSG_dbupdate(RcvMsg);
            break;
        }
        case MSG_dbdelete: //刪除記錄操作
        {
            Proc_MSG_dbdelete(RcvMsg);
            break;
        }
        case MSG_writebill:	//寫計費話單
        {
            Proc_MSG_writebill(RcvMsg);
            break;
        }
        case MSG_dbsp: //執行存儲過程操作
        {
            Proc_MSG_dbsp(RcvMsg);
            break;
        }
        case MSG_dbclose: //關閉查詢的數據表
        {
            Proc_MSG_dbclose(RcvMsg);
            break;
        }
        case MSG_gettolltime: //根據余額取最大通話時長
        {
            Proc_MSG_gettolltime(RcvMsg);
            break;
        }
        case MSG_dbrecord: //讀數據表當前記錄
        {
            Proc_MSG_dbrecord(RcvMsg);
            break;
        }
        case MSG_dbspoutparam: //取存儲過程輸出參數值
        {
            Proc_MSG_dbspoutparam(RcvMsg);
            break;
        }
        case MSG_execsql: //執行SQL語句
        {
            Proc_MSG_execsql(RcvMsg);
            break;
        }
        case MSG_dbfieldtofile: //取查詢的字段值并存放到指定的文件
        {
            Proc_MSG_dbfieldtofile(RcvMsg);
            break;
        }
        case MSG_dbfiletofield: //將文件內容存入數據表
        {
            Proc_MSG_dbfiletofield(RcvMsg);
            break;
        }
        case MSG_dbinsertex: //插入記錄操作
        {
            Proc_MSG_dbinsertex(RcvMsg);
            break;
        }
        case MSG_dbupdateex: //修改記錄操作
        {
            Proc_MSG_dbupdateex(RcvMsg);
            break;
        }
        case MSG_dbqueryex: //查詢數據操作
        {
            Proc_MSG_dbqueryex(RcvMsg);
            break;
        }
        case MSG_dbselect: //查詢數據表記錄擴展指令操作
        {
            Proc_MSG_dbselect(RcvMsg);
            break;
        }
        default:
        {
            break;
        }
      }*/
    }
  }
}
//處理查詢數據操作
int Proc_MSG_gwquery(CXMLRcvMsg &XMLMsg)
{
    Send_GWQuery_Result(XMLMsg, OnDBFail);
    return 0;
}
//查詢數據操作
int Proc_MSG_dbquery(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  //CH sqls[MAX_SQLS_LEN];
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBQuery_Result(XMLMsg, 0, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Get_Idel_QuerySQL_Buf(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBQuery_Result(XMLMsg, 0, 0, OnDBFail, "Query buf over!");
      return 0;
  }
  FlwData->QuerySQLBuf[SqlBufId].state = 1;
  FlwData->QuerySQLBuf[SqlBufId].timer = 0;

  FlwData->QuerySQLBuf[SqlBufId].SessionId = SessionId;
  FlwData->QuerySQLBuf[SqlBufId].ClientId = XMLMsg.GetClientId();
  FlwData->QuerySQLBuf[SqlBufId].CmdAddr = CmdAddr;
  FlwData->QuerySQLBuf[SqlBufId].DeviceNo = 1;
  FlwData->QuerySQLBuf[SqlBufId].ChnType = ChnType;
  FlwData->QuerySQLBuf[SqlBufId].ChnNo = ChnNo;

  FlwData->QuerySQLBuf[SqlBufId].QueryType = 1;
  memset(FlwData->QuerySQLBuf[SqlBufId].GetFieldIdList, 0, 64);

  strncpy(FlwData->QuerySQLBuf[SqlBufId].sqls, XMLMsg.GetAttrValue(4).C_Str(), MAX_SQLS_LEN);
  FlwData->QuerySQLBuf[SqlBufId].ExecResult = 1;
  FlwData->QuerySQLBuf[SqlBufId].RecordCount = 0;
  FlwData->QuerySQLBuf[SqlBufId].FieldCount = 0;
  FlwData->QuerySQLBuf[SqlBufId].CurrRecdNo = 0;
  FlwData->QuerySQLBuf[SqlBufId].DBEvent = 0xFFFF;
  memset(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
  return 0;
}
//根據字段序號取字段值操作
int Proc_MSG_dbfieldno(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  int FieldNo;
  US SqlBufId;
  AnsiString value;
  static char pszValues[MAX_VAR_DATA_LEN];
  int j = 0;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBField_Value(XMLMsg, "", OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBField_Value(XMLMsg, "", OnDBFail, "have not query before!");
      return 0;
  }
  FieldNo = atoi(XMLMsg.GetAttrValue(4).C_Str());

  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL)
  {
      Send_DBField_Value(XMLMsg, "", OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "AdoQuery = dsInactive!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "query no in success!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Eof)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "none record or eof!");
      return 0;
  }
  if (FieldNo >= FlwData->QuerySQLBuf[SqlBufId].FieldCount)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "fieldno id out of range!");
      return 0;
  }
  try
  {
      value = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Fields->Fields[FieldNo]->AsString;

      memset(pszValues, 0, MAX_VAR_DATA_LEN);
      for (int i = 1; i < MAX_VAR_DATA_LEN && i <= value.Length(); i ++)
      {
        pszValues[j] = value[i];
        if (pszValues[i] == '\"')
        {
          j ++;
          pszValues[j] = '\"';
        }
        j ++;
        if (j >= MAX_VAR_DATA_LEN)
          break;
      }
      Send_DBField_Value(XMLMsg, pszValues, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBField_Value(XMLMsg, "", OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//根據字段名取字段值操作
int Proc_MSG_dbfieldname(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  CH FieldName[MAX_VAR_DATA_LEN];
  US SqlBufId;
  AnsiString value;
  static char pszValues[MAX_VAR_DATA_LEN];
  int j = 0;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBField_Value(XMLMsg, "", OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBField_Value(XMLMsg, "", OnDBFail, "have not query before!");
      return 0;
  }
  strncpy(FieldName,XMLMsg.GetAttrValue(4).C_Str(), MAX_VAR_DATA_LEN);

  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL)
  {
      Send_DBField_Value(XMLMsg, "", OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "AdoQuery = dsInactive!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "query no in success!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Eof)
  {
      //數據集未建立
      Send_DBField_Value(XMLMsg, "", OnDBFail, "none record or eof!");
      return 0;
  }
  try
  {
      value = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FieldByName(StrPas(FieldName))->AsString;
      memset(pszValues, 0, MAX_VAR_DATA_LEN);
      for (int i = 1; i < MAX_VAR_DATA_LEN && i <= value.Length(); i ++)
      {
        pszValues[j] = value[i];
        if (pszValues[i] == '\"')
        {
          j ++;
          pszValues[j] = '\"';
        }
        j ++;
        if (j >= MAX_VAR_DATA_LEN)
          break;
      }
      Send_DBField_Value(XMLMsg, pszValues, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBField_Value(XMLMsg, "", OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//記錄指針移到首記錄
int Proc_MSG_dbfirst(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  int RecordNo;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive ||
    FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3 ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount == 0)
  {
      //數據集未建立
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  try
  {
    if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FindFirst() == false)
    {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not records!");
      return 0;
    }
    RecordNo = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo;
    Send_DBMove_Result(XMLMsg, RecordNo, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//記錄指針移到下一記錄
int Proc_MSG_dbnext(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  int RecordNo;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive ||
    FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3 ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount == 0 ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Eof)
  {
      //數據集未建立
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  try
  {
    if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FindNext() == false)
    {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not records!");
      return 0;
    }
    RecordNo = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo;
    Send_DBMove_Result(XMLMsg, RecordNo, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//記錄指針移到前一記錄
int Proc_MSG_dbprior(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  int RecordNo;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive ||
    FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3 ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount == 0 ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Bof)
  {
      //數據集未建立
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  try
  {
    if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FindPrior() == false)
    {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not records!");
      return 0;
    }
    RecordNo = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo;
    Send_DBMove_Result(XMLMsg, RecordNo, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//記錄指針移到末記錄
int Proc_MSG_dblast(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  int RecordNo;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive ||
    FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3 ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount == 0)
  {
      //數據集未建立
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  try
  {
    if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FindLast() == false)
    {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not records!");
      return 0;
    }
    RecordNo = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo;
    Send_DBMove_Result(XMLMsg, RecordNo, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//記錄指針移到指定記錄
int Proc_MSG_dbgoto(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  int RecordNo;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  RecordNo = atoi(XMLMsg.GetAttrValue(4).C_Str());
  
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive ||
    FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3)
  {
      //數據集未建立
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount < RecordNo)
  {
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, "recordno is out of range!");
      return 0;
  }
  try
  {
    if (RecordNo == FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount)
    {
      if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FindLast() == true)
        Send_DBMove_Result(XMLMsg, RecordNo, OnDBSuccess, "");
      else
        Send_DBMove_Result(XMLMsg, RecordNo, OnDBFail, "");
    }
    else
    {
      FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo = RecordNo;
      Send_DBMove_Result(XMLMsg, RecordNo, OnDBSuccess, "");
    }
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBMove_Result(XMLMsg, 0, OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//插入記錄操作
int Proc_MSG_dbinsert(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);

  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 1, XMLMsg.GetAttrValue(4).C_Str(), 0);
  }
  else
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "sql exec buf is over!");
  }
  return 0;
}
//修改記錄操作
int Proc_MSG_dbupdate(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);

  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 2, XMLMsg.GetAttrValue(4).C_Str(), 0);
  }
  else
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "sql exec buf is over!");
  }
  return 0;
}
//刪除記錄操作
int Proc_MSG_dbdelete(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);

  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 3, XMLMsg.GetAttrValue(4).C_Str(), 0);
  }
  else
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "sql exec buf is over!");
  }
  return 0;
}
//寫計費話單
int Proc_MSG_writebill(CXMLRcvMsg &XMLMsg)
{
  Send_WriteBill_Result(XMLMsg, 0, OnDBFail, "this cmd had abolished!");
  return 0;
}
//執行存儲過程操作
int Proc_MSG_dbsp(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  char ParamStr[32];
  CString Param1CStr[20], Param2CStr[4];
  int Param1Num;
  TParameterDirection direct;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      Send_DBSP_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Get_Idel_SP_Buf(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBSP_Result(XMLMsg, OnDBFail, "sp buf over!");
      return 0;
  }

  FlwData->SPBuf[SqlBufId].AdoSp->Close();
  FlwData->SPBuf[SqlBufId].AdoSp->Parameters->Clear();
  strncpy(FlwData->SPBuf[SqlBufId].spname, XMLMsg.GetAttrValue(4).C_Str(), MAX_PATH_FILE_LEN);
  FlwData->SPBuf[SqlBufId].AdoSp->ProcedureName = FlwData->SPBuf[SqlBufId].spname;
  try
  {
    Param1Num = SplitString(XMLMsg.GetAttrValue(5).C_Str(), ';', 20, Param1CStr);
    for (int i=0; i<Param1Num; i++)
    {
      Param2CStr[0] = ""; //參數名
      Param2CStr[1] = ""; //參數類型
      Param2CStr[2] = ""; //傳遞方向
      Param2CStr[3] = ""; //輸入參數值
      SplitString(Param1CStr[i].C_Str(), ',', 4, Param2CStr);
      if (Param2CStr[0].GetLength() > 0)
      {
        direct = GetDirectionByString(Param2CStr[2].C_Str());
        FlwData->SPBuf[SqlBufId].AdoSp->Parameters->CreateParameter((char *)Param2CStr[0].C_Str(),
                                         GetFieldTypeByString(Param2CStr[1].C_Str()),
                                         direct,
                                         128,
                                         NULL);
        if (direct == pdInput || direct == pdInputOutput)
          FlwData->SPBuf[SqlBufId].AdoSp->Parameters->ParamByName((char *)Param2CStr[0].C_Str())->Value = (char *)Param2CStr[3].C_Str();
      }
    }
  }
  catch (Exception &exception)
  {
      Send_DBSP_Result(XMLMsg, OnDBFail, exception.Message.c_str());
      return 0;
  }

  FlwData->SPBuf[SqlBufId].state = 1;
  FlwData->SPBuf[SqlBufId].timer = 0;

  FlwData->SPBuf[SqlBufId].SessionId = SessionId;
  FlwData->SPBuf[SqlBufId].ClientId = XMLMsg.GetClientId();
  FlwData->SPBuf[SqlBufId].CmdAddr = CmdAddr;
  FlwData->SPBuf[SqlBufId].DeviceNo = 1;
  FlwData->SPBuf[SqlBufId].ChnType = ChnType;
  FlwData->SPBuf[SqlBufId].ChnNo = ChnNo;

  FlwData->SPBuf[SqlBufId].ExecResult = 1;
  FlwData->SPBuf[SqlBufId].DBEvent = 0xFFFF;
  memset(FlwData->SPBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
  return 0;
}
//關閉查詢的數據表
int Proc_MSG_dbclose(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  
  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not query before!");
      return 0;
  }

  FlwData->Init_QuerySQL_Buf(SqlBufId);
  Send_DBCmd_Result(XMLMsg, OnDBSuccess, "");
  return 0;
}
//根據余額取最大通話時長
int Proc_MSG_gettolltime(CXMLRcvMsg &XMLMsg)
{
  Send_WriteBill_Result(XMLMsg, 0, OnDBFail, "this cmd had abolished!");
  return 0;
}
//讀數據表當前記錄
int Proc_MSG_dbrecord(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  int FieldNo, RecordNo;
  int AutoGotoNext;
  US SqlBufId;
  AnsiString value;
  static char pszValues[1024], FieldList[1920];
  char szGetIdList[64];
  int i, j, len=0;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, "have not query before!");
      return 0;
  }
  if (Check_Int_Str((char *)XMLMsg.GetAttrValue(4).C_Str(), RecordNo) != 0)
    RecordNo = -1;
  AutoGotoNext = atoi(XMLMsg.GetAttrValue(5).C_Str());

  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL ||
    FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive ||
    FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3)
  {
      //數據集未建立
      Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Eof)
  {
      //數據集未建立
      Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, "none record or eof!");
      return 0;
  }
  if (RecordNo > 0)
  {
      if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecordCount < RecordNo)
      {
          Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, "recordno is out of range!");
          return 0;
      }
      try
      {
          FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo = RecordNo;
      }
      catch (Exception &exception)
      {
          strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
          Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
          return 0;
      }
  }
  try
  {
      memset(FieldList, 0, 1920);
      memset(szGetIdList, 0, 64);
      strncpy(szGetIdList, XMLMsg.GetAttrValue(6).C_Str(), 32);
      for (FieldNo = 0; FieldNo < FlwData->QuerySQLBuf[SqlBufId].FieldCount; FieldNo ++)
      {
        if (szGetIdList[FieldNo] == '0')
        {
          memset(pszValues, 0, 1024);
          j = 0;
        }
        else
        {
          value = FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Fields->Fields[FieldNo]->AsString;

          memset(pszValues, 0, 1024);
          j = 0;
          for (i = 1; i < 1024 && i <= value.Length(); i ++)
          {
            pszValues[j] = value[i];
            if (pszValues[j] == '\"')
            {
              j ++;
              pszValues[j] = '\"';
            }
            j ++;
            if (j >= 1024)
              break;
          }
        }
        len = len+j+1;
        if (len >= 1920)
          break;
        strcat(FieldList, pszValues);
        strcat(FieldList, "`");
      }
      Send_DBRecord_Result(XMLMsg, FlwData->QuerySQLBuf[SqlBufId].AdoQuery->RecNo, FieldList, OnDBSuccess, "");
      if (AutoGotoNext == 1)
      {
        try
        {
          FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Next();
        }
        catch (...)
        {
        }
      }
  }
  catch (Exception &exception)
  {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBRecord_Result(XMLMsg, 0, "", OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
  }
  return 0;
}
//取存儲過程輸出參數值 //edit 2009-02-15 add
int Proc_MSG_dbspoutparam(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  int FieldNo, RecordNo;
  int AutoGotoNext;
  US SqlBufId;
  AnsiString value;
  static char ParamList[1920];

  char ParamStr[32];
  CString ParamCStr[21];
  int ParamNum;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      Send_DBSPoutparam_Result(XMLMsg, "", OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_SPBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBSPoutparam_Result(XMLMsg, "", OnDBFail, "have not exec sp before!");
      return 0;
  }

  if (FlwData->SPBuf[SqlBufId].AdoSp == NULL ||
    FlwData->SPBuf[SqlBufId].ExecResult != 3)
  {
      //數據集未建立
      Send_DBSPoutparam_Result(XMLMsg, "", OnDBFail, "have not exec sp before!");
      return 0;
  }
  try
  {
      memset(ParamList, 0, 1920);
      ParamNum = SplitString(XMLMsg.GetAttrValue(4).C_Str(), ';', 21, ParamCStr);
      for (int i=0; i<ParamNum&&i<22; i++)
      {
        if (!FlwData->SPBuf[SqlBufId].AdoSp->Parameters->ParamByName((char *)ParamCStr[i].C_Str())->Value.IsNull())
          value = FlwData->SPBuf[SqlBufId].AdoSp->Parameters->ParamByName((char *)ParamCStr[i].C_Str())->Value;
        else
          value = "";
        if ((strlen(ParamList)+value.Length()+1) >= 1920)
          break;
        strcat(ParamList, value.c_str());
        strcat(ParamList, "`");
      }
      Send_DBSPoutparam_Result(XMLMsg, ParamList, OnDBSuccess, "");
  }
  catch (Exception &exception)
  {
      Send_DBSPoutparam_Result(XMLMsg, "", OnDBFail, exception.Message.c_str());
  }
  return 0;
}
//執行SQL語句
int Proc_MSG_execsql(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  
  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 6, XMLMsg.GetAttrValue(4).C_Str(), 0);
  }
  else
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
  }
  return 0;
}
//取查詢的字段值并存放到指定的文件
int Proc_MSG_dbfieldtofile(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  CH FieldName[MAX_VAR_DATA_LEN];
  CH FileName[MAX_VAR_DATA_LEN];
  US SqlBufId;
  AnsiString value;
  TADOBlobStream* pBlobRead=NULL;
  TMemoryStream *pMemStream=NULL;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Fine_QueryBuf_by_SessionId(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "have not query before!");
      return 0;
  }
  strncpy(FieldName,XMLMsg.GetAttrValue(4).C_Str(), MAX_VAR_DATA_LEN);

  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery == NULL)
  {
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "have not query before!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->State == dsInactive)
  {
      //數據集未建立
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "AdoQuery = dsInactive!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].ExecResult != 3)
  {
      //數據集未建立
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "query no in success!");
      return 0;
  }
  if (FlwData->QuerySQLBuf[SqlBufId].AdoQuery->Eof)
  {
      //數據集未建立
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "none record or eof!");
      return 0;
  }
  try
  {
    try
    {
      //都取2進制字段值然后寫入文件
      pBlobRead = new TADOBlobStream((TBlobField*)FlwData->QuerySQLBuf[SqlBufId].AdoQuery->FieldByName(StrPas(FieldName)),bmRead);
      pMemStream = new TMemoryStream();
      pMemStream->LoadFromStream(pBlobRead);
      if (pMemStream->Size > 0)
      {
        AnsiString strTemPath = (AnsiString)(XMLMsg.GetAttrValue(5).C_Str());
        if (DirectoryExists(strTemPath) == false)
          CreateDir(strTemPath);
        snprintf(FileName, MAX_VAR_DATA_LEN, "%s/%s", XMLMsg.GetAttrValue(5).C_Str(), XMLMsg.GetAttrValue(6).C_Str());
        pMemStream->SaveToFile(FileName);

        Send_DBFieldToFile_Result(XMLMsg, FileName, OnDBSuccess, "");
      }
      else
      {
        Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, "the field is 0 byte");
      }
    }
    catch (Exception &exception)
    {
      strncpy(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
      Send_DBFieldToFile_Result(XMLMsg, "", OnDBFail, FlwData->QuerySQLBuf[SqlBufId].ErrorBuf);
    }
  }
  __finally
  {
    pBlobRead->Free();
    pMemStream->Free();
  }
  return 0;
}
//將文件內容存入數據表
int Proc_MSG_dbfiletofield(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;
  CH sqls[MAX_SQLS_LEN]; //sql語句

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);

  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      if (XMLMsg.GetAttrValue(8).GetLength() > 0)
      {
        sprintf(sqls, "update %s set %s=:MyblobData where %s",
          XMLMsg.GetAttrValue(6).C_Str(),
          XMLMsg.GetAttrValue(7).C_Str(),
          XMLMsg.GetAttrValue(8).C_Str());
      }
      else
      {
        sprintf(sqls, "update %s set %s=:MyblobData",
          XMLMsg.GetAttrValue(6).C_Str(),
          XMLMsg.GetAttrValue(7).C_Str());
      }

      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 7, sqls, 0);
      //把文件名暫存在ErrorBuf
      snprintf(FlwData->ExecSQLBuf[SqlBufId].ErrorBuf, MAX_ERRORBUF_LEN, "%s/%s",
        XMLMsg.GetAttrValue(4).C_Str(), XMLMsg.GetAttrValue(5).C_Str());
  }
  else
  {
      Send_DBCmd_Result(XMLMsg, OnDBFail, "sql exec buf is over!");
  }
  return 0;
}
//插入記錄操作（針對長度>500的sql語句）
int Proc_MSG_dbinsertex(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);

  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 1, XMLMsg.GetAttrValue(4).C_Str(), 0);
  }
  else
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "sql exec buf is over!");
  }
  return 0;
}
//修改記錄操作
int Proc_MSG_dbupdateex(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo = 1; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);

  SqlBufId = FlwData->Get_Idel_ExecSQL_Buf(SessionId);
  if (SqlBufId < MAX_SESSION_NUM)
  {
      FlwData->Add_SQL_Buf(SqlBufId, SessionId, XMLMsg.GetClientId(), CmdAddr,
        DeviceNo, ChnType, ChnNo, 2, XMLMsg.GetAttrValue(4).C_Str(), 0);
  }
  else
  {
      QuarkCallDBServer->WriteSQLs(1, XMLMsg.GetAttrValue(4).C_Str());
      Send_DBCmd_Result(XMLMsg, OnDBFail, "sql exec buf is over!");
  }
  return 0;
}
//查詢數據操作
int Proc_MSG_dbqueryex(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  //CH sqls[MAX_SQLS_LEN];
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      Send_DBQuery_Result(XMLMsg, 0, 0, OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Get_Idel_QuerySQL_Buf(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBQuery_Result(XMLMsg, 0, 0, OnDBFail, "Query buf over!");
      return 0;
  }
  FlwData->QuerySQLBuf[SqlBufId].state = 1;
  FlwData->QuerySQLBuf[SqlBufId].timer = 0;

  FlwData->QuerySQLBuf[SqlBufId].SessionId = SessionId;
  FlwData->QuerySQLBuf[SqlBufId].ClientId = XMLMsg.GetClientId();
  FlwData->QuerySQLBuf[SqlBufId].CmdAddr = CmdAddr;
  FlwData->QuerySQLBuf[SqlBufId].DeviceNo = 1;
  FlwData->QuerySQLBuf[SqlBufId].ChnType = ChnType;
  FlwData->QuerySQLBuf[SqlBufId].ChnNo = ChnNo;

  strncpy(FlwData->QuerySQLBuf[SqlBufId].sqls, XMLMsg.GetAttrValue(4).C_Str(), MAX_SQLS_LEN);
  FlwData->QuerySQLBuf[SqlBufId].ExecResult = 1;
  FlwData->QuerySQLBuf[SqlBufId].RecordCount = 0;
  FlwData->QuerySQLBuf[SqlBufId].FieldCount = 0;
  FlwData->QuerySQLBuf[SqlBufId].CurrRecdNo = 0;
  FlwData->QuerySQLBuf[SqlBufId].DBEvent = 0xFFFF;
  memset(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
  return 0;
}
//查詢數據表記錄擴展指令
int Proc_MSG_dbselect(CXMLRcvMsg &XMLMsg)
{
  UL SessionId;
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
  UC ChnType; //通道類型
  US ChnNo; //通道號
  //CH sqls[MAX_SQLS_LEN];
  US SqlBufId;

  if (FlwData->DBConnectId != 1 || FlwData->DBType == DB_NONE)
  {
      QuarkCallDBServer->ADOConnection1->Connected = false;
      Send_DBSelect_Result(XMLMsg, 0, 0, "", OnDBFail, "have not connected DataBase!");
      return 0;
  }
  Get_Chn_Data(XMLMsg, SessionId, CmdAddr, ChnType, ChnNo);
  SqlBufId = FlwData->Get_Idel_QuerySQL_Buf(SessionId);
  if (SqlBufId >= MAX_SESSION_NUM)
  {
      Send_DBSelect_Result(XMLMsg, 0, 0, "", OnDBFail, "Query buf over!");
      return 0;
  }
  FlwData->QuerySQLBuf[SqlBufId].state = 1;
  FlwData->QuerySQLBuf[SqlBufId].timer = 0;

  FlwData->QuerySQLBuf[SqlBufId].SessionId = SessionId;
  FlwData->QuerySQLBuf[SqlBufId].ClientId = XMLMsg.GetClientId();
  FlwData->QuerySQLBuf[SqlBufId].CmdAddr = CmdAddr;
  FlwData->QuerySQLBuf[SqlBufId].DeviceNo = 1;
  FlwData->QuerySQLBuf[SqlBufId].ChnType = ChnType;
  FlwData->QuerySQLBuf[SqlBufId].ChnNo = ChnNo;

  FlwData->QuerySQLBuf[SqlBufId].QueryType = 2;
  memset(FlwData->QuerySQLBuf[SqlBufId].GetFieldIdList, 0, 64);
  strncpy(FlwData->QuerySQLBuf[SqlBufId].GetFieldIdList, XMLMsg.GetAttrValue(5).C_Str(), 63);

  strncpy(FlwData->QuerySQLBuf[SqlBufId].sqls, XMLMsg.GetAttrValue(4).C_Str(), MAX_SQLS_LEN);
  FlwData->QuerySQLBuf[SqlBufId].ExecResult = 1;
  FlwData->QuerySQLBuf[SqlBufId].RecordCount = 0;
  FlwData->QuerySQLBuf[SqlBufId].FieldCount = 0;
  FlwData->QuerySQLBuf[SqlBufId].CurrRecdNo = 0;
  FlwData->QuerySQLBuf[SqlBufId].DBEvent = 0xFFFF;
  memset(FlwData->QuerySQLBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
  return 0;
}
//----------------------------------------------------------------------------
void SendLineMsg(int nLOG, const char *pszLineMsg, int nLineId)
{
  char strMsg[1024];

  sprintf(strMsg, "<onrecvtxtline acdid='%d' serviceid='3' linemsg='%s' lineid='%d'/>",
    nLOG, pszLineMsg, nLineId);
  TcpServer->SendMessage((NODE_LOG<<8)|nLOG, (MSGTYPE_IVRLOG<<8)|LOGMSG_onrecvtxtline, strlen(strMsg), strMsg);
}
void SendINIFile(int nLOG)
{
  FILE *fp1;
  char szLineMsg[256];

  fp1 = fopen(g_strServiceINIFileName.c_str(), "r");
  if (fp1 != NULL)
  {
    SendLineMsg(nLOG, "", 0);
    while (!feof(fp1))
    {
      if (fgets(szLineMsg, 256, fp1) > 0)
      {
        SendLineMsg(nLOG, szLineMsg, 1);
      }
    }
    SendLineMsg(nLOG, "", 2);
    fclose(fp1);
  }
}
//LOG登錄
int Proc_LOGMSG_login(int nLOG, CXMLRcvMsg &LOGMsg)
{
  char strMsg[1024];
  sprintf(strMsg, "<onlogin logid='%d' serviceid='3' result='0' errorbuf=''/>", nLOG);
  TcpServer->SendMessage((NODE_LOG<<8)|nLOG, (MSGTYPE_IVRLOG<<8)|0, strlen(strMsg), strMsg);
  //SendINIFile(nLOG);
  return 0;
}
//修改INI配置文件參數
int Proc_LOGMSG_modifyiniparam(CXMLRcvMsg &LOGMsg)
{
  WritePrivateProfileString(LOGMsg.GetAttrValue(2).C_Str(),
    LOGMsg.GetAttrValue(3).C_Str(),
    LOGMsg.GetAttrValue(4).C_Str(), g_strServiceINIFileName.c_str());
  return 0;
}


