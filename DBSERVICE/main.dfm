object QuarkCallDBServer: TQuarkCallDBServer
  OldCreateOrder = False
  OnCreate = ServiceCreate
  OnDestroy = ServiceDestroy
  DisplayName = 'QuarkCallDBService'
  BeforeInstall = ServiceBeforeInstall
  OnContinue = ServiceContinue
  OnPause = ServicePause
  OnStart = ServiceStart
  OnStop = ServiceStop
  Left = 192
  Top = 114
  Height = 114
  Width = 563
  object ADOConnection1: TADOConnection
    ConnectionTimeout = 30
    LoginPrompt = False
    Left = 40
    Top = 16
  end
  object ADOConnection2: TADOConnection
    CommandTimeout = 300
    ConnectionTimeout = 30
    LoginPrompt = False
    Left = 216
    Top = 16
  end
  object adoPublic2: TADOQuery
    Connection = ADOConnection2
    Parameters = <>
    Left = 304
    Top = 16
  end
  object adoPublic1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    SQL.Strings = (
      'select getdate() as nowtime')
    Left = 128
    Top = 16
  end
  object IdHTTPServer1: TIdHTTPServer
    Bindings = <>
    DefaultPort = 5220
    OnCommandGet = IdHTTPServer1CommandGet
    Left = 400
    Top = 16
  end
end
