//---------------------------------------------------------------------------

#ifndef ThreadConnDBH
#define ThreadConnDBH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class CThreadConnDB : public TThread
{            
private:
protected:
  void __fastcall Execute();
public:
  __fastcall CThreadConnDB(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
