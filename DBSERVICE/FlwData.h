//---------------------------------------------------------------------------
#ifndef FlwDataH
#define FlwDataH
//---------------------------------------------------------------------------
#include <stdio.h>
#include <inifiles.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <SyncObjs.hpp>
#include "boxinc.h"
#define MAX_SESSION_NUM   1024 //最大會話資源個數
//---------------------------------------------------------------------------
//查詢記錄指令結構定義
typedef struct
{
    UC state; //數據狀態 0: 無 1：有
    US timer;

    UL SessionId; //會話序列號
    US ClientId; //客戶端編號
    UL CmdAddr; //指令地址
    UC DeviceNo; //接入設備號
	  UC ChnType; //通道類型
	  US ChnNo; //通道號

    UC QueryType; //查詢指令類型：1-dbquery,2-dbselect
    CH GetFieldIdList[64];

    TADOQuery *AdoQuery; //數據表查詢
    //TADOStoredProc *AdoSp; //存儲過程
    CH sqls[MAX_SQLS_LEN]; //sql語句
    UC ExecResult; //操作結果：0-未操作 1-等待查詢 2-正在查詢 3-查詢成功 4-查詢失敗
    US RecordCount; //返回的記錄數
    US FieldCount; //返回的字段數
    US CurrRecdNo; //當前記錄指針
    CH Value[MAX_VAR_DATA_LEN];
    US DBEvent; //查詢結果
    CH ErrorBuf[MAX_ERRORBUF_LEN]; //返回的錯誤信息

}VXML_DB_QUERY_STRUCT;

//存儲過程指令結構定義   //edit 2008-09-30 add
typedef struct
{
    UC state; //數據狀態 0: 無 1：有
    US timer;

    UL SessionId; //會話序列號
    US ClientId; //客戶端編號
    UL CmdAddr; //指令地址
    UC DeviceNo; //接入設備號
	  UC ChnType; //通道類型
	  US ChnNo; //通道號

    TADOStoredProc *AdoSp; //存儲過程
    CH spname[MAX_PATH_FILE_LEN]; //sp名
    UC ExecResult; //操作結果：0-未操作 1-等待執行 2-正在執行 3-執行成功 4-執行失敗
    US DBEvent; //執行結果
    CH ErrorBuf[MAX_ERRORBUF_LEN]; //返回的錯誤信息

}VXML_DB_SP_STRUCT;

//插入、修改、刪除記錄指令結構定義
typedef struct
{
  UC state; //數據狀態 0: 無 1：有
  US timer;

  UL SessionId; //會話序列號
  US ClientId; //客戶端編號
  UL CmdAddr; //指令地址
  UC DeviceNo; //接入設備號
	UC ChnType; //通道類型
	US ChnNo; //通道號

  UC ExecType; //操作類型：0-無 1-插入 2-修改 3-刪除 4-寫計費話單 5-存儲過程 6-執行通用數據操作 7-將文件寫到數據表某字段
  CH sqls[MAX_SQLS_LEN]; //sql語句
  UC ExecResult; //操作結果：0-未操作 1-正在操作 2-成功操作 3-操作失敗
  US RecordCount; //返回操作數據表影響的行數
  US DBEvent; //查詢結果
  double TollFee;
  CH ErrorBuf[MAX_ERRORBUF_LEN]; //返回的錯誤信息
	UI prior; //對應的前一序號(當為0節點時，該值表示隊尾節點號)
	UI next; //對應的下一序號, 0xFFFF表示結束

}VXML_DB_EXECSQL_STRUCT;

//費率結構定義
typedef struct
{
    UC state; //數據狀態 0: 無 1：有
    UC FuncGroup; /*業務腳本功能服務組號*/
    US FuncNo; /*業務腳本功能服務號*/
    CH CallerCode[MAX_TELECODE_LEN]; //主叫號碼字冠(*表示所有號碼)
    CH CalledCode[MAX_TELECODE_LEN]; //被叫號碼字冠(*表示所有號碼)
    US FreeTime; //不計費通話時長（秒）
    UC BillType; //話單類型
    double AfficFee; //每次通話附加費
    UC FeeType; //計費公式：計費公式0：免費 1：單次計費（A）2：復式計費（A+B）3：IP電話計費((A+B)|C)4：按首次一個單位時間費用
    US FirstLen; //單次（首次）計費單位時間（秒）
    double FirstFee; //單次（首次）計費費率(A)
    US LaterLen; //以后每次計費單位時間（秒）
    double LaterFee; //以后每次計費費率(B)
    US IPLen; //IP電話計費單位時間（秒）
    double IPFee; //IP電話計費費率(C)
    US InfoLen; //信息費計費時間單位（秒）
    double InfoFee; //信息費費率
    UC BftId; //優惠標志
    UC BftPercent; //優惠比例（百分數）
    CH Bfttime1[10]; //優惠時段1（HHMM-HHMM）
    CH Bfttime2[10]; //優惠時段1（HHMM-HHMM）

}VXML_FEE_RATE_STRUCT;

//話單結構定義
typedef struct
{
    UC DeviceNo;
    UC ChnType;
    US ChnNo;
    CH CallerNo[MAX_TELECODE_LEN]; //主叫號碼
    CH CalledNo[MAX_TELECODE_LEN]; //被叫號碼
    CH OrgCallerNo[MAX_TELECODE_LEN]; //原主叫號碼(也可作為其他用)
    CH OrgCalledNo[MAX_TELECODE_LEN]; //原被叫號碼(也可作為其他用)
    CH Account[MAX_TELECODE_LEN]; //帳號
    UC CallInOut; //呼叫方向
    UC FuncGroup; //業務腳本功能服務組號
    US FuncNo; //業務腳本功能服務號
    CH StartTime[25]; //起始時間
    CH EndTime[25]; //終止時間
    US MinuteTimes; //分時長
    US SecondTimes; //秒時長
    UC BillType; //話單類型(0-免費話單 1-......)
    UC FeeType; //計費方式
    double Rate1; //費率1
    double Rate2; //費率2
    double TeleCharge; //電話費
    double InfoCharge; //信息費
    double TotalCharge; //總話費
    CH Remark[50]; //備注

}VXML_BILL_RECORD_STRUCT;

typedef struct
{
    UC BftId; //優惠標志 1-是 0-否
    TDateTime StartTime; //起始時間
    TDateTime EndTime; //終止時間
    int TotalLen; //實際總計時長（秒）
    int OverFlowLen; //溢出部分的時長（秒），是由于按一個單位時長而向后不足的時長

}VXML_BILL_TIME_STRUCT;

typedef struct
{
  UC TimePhaseNum; //實際的計費時間段數
  VXML_BILL_TIME_STRUCT TimePhaseList[8]; //計費時間段

}VXML_BILL_TIME_PHASE_STRUCT;

class CFlwData
{
private:

public:
	  CFlwData();
	  virtual ~CFlwData();

    //計費費率數據區
    VXML_FEE_RATE_STRUCT FeeRate[MAX_BILL_FEE_LEN];
    //變量數據區
    VXML_DB_QUERY_STRUCT   QuerySQLBuf[MAX_SESSION_NUM]; //數據庫操作緩沖
    VXML_DB_EXECSQL_STRUCT ExecSQLBuf[MAX_SESSION_NUM]; //數據庫操作緩沖

    VXML_DB_SP_STRUCT      SPBuf[MAX_SESSION_NUM]; //存儲過程操作緩沖  //edit 2008-09-30 add

    TADOConnection *AdoConnection; //ADO數據庫連接
    TADOQuery *PubQuery1; //公共通用數據集
    TADOQuery *ExecQuery; //插入、修改、刪除數據集

    UC INIFileType;
    AnsiString DBINIFileName;
    //數據庫參數
    UC DBType; //連接的數據庫類型(1:SQLSERVER 2:ORACLE 3:SYBASE 4:ACCESS 5:ODBC)
	  CH DBServer[MAX_USERNAME_LEN]; //服務器名或IP地址
    CH DataBase[MAX_PATH_FILE_LEN]; //需要連接的數據庫
    CH UserId[MAX_USERNAME_LEN]; //數據庫用戶名
    CH Password[64]; //數據庫用戶密碼（加密顯示）
    UC DBConnectId; //數據庫連接標志(0:未連接 1:連接成功 2:連接斷開)

    //話單數據表參數
    UC BillTableId; //每月計費是否分表
    UC BillDay; //每月計費起始日期
    UC WriteBillTimeId; //寫話單表參考時間(0-按應答時間 1-按掛機時間)
    UC WriteTxtBillId; //是否寫文本話單記錄(0-不寫 1-寫)

    UC ExecDir[MAX_PATH_LEN]; //執行文件目錄

    UC DispComId; //顯示通信消息 0：不顯示 1：顯示
    UC WriteComId; //寫通信消息標志 0：不寫 1：寫
    UC AutoRunId;

    AnsiString ConnFailErrStr;

public:
    void Timer_Add(void);
    //讀配置文件
    void Read_IniFile(void);
    //初始化一數據庫緩沖
    int Init_QuerySQL_Buf(US SqlBufId);
    //取空閑的查詢緩沖
    int Get_Idel_QuerySQL_Buf(UL SessionId);
    US Fine_QueryBuf_by_SessionId(UL SessionId);

    //edit 2008-09-30 add
    //初始化一存儲過程緩沖
    int Init_SP_Buf(US SqlBufId);
    //取空閑的存儲過程緩沖
    int Get_Idel_SP_Buf(UL SessionId);
    //根據會話標志查找存儲過程緩沖序號
    US Fine_SPBuf_by_SessionId(UL SessionId);

    int Init_ExecSQL_Buf(US SqlBufId);
    void Init_All(void);
    //取一空閑數據庫緩沖
    int Get_Idel_ExecSQL_Buf(UL SessionId);
    //增加數據庫操作
    int Add_SQL_Buf(US SqlBufId, UL SessionId, US ClientId, UL CmdAddr, UC DeviceNo, UC ChnType, US ChnNo, UC ExecType, const CH *sqls, double TollFee);
    //刪除數據庫操作
    int Del_SQL_Buf(US SqlBufId);

    //解密密碼
    char *DecodePass(const char *Pass);
    AnsiString DecodePass(AnsiString Pass);
    //執行查詢操作
    void Proc_Query(void);
    //執行存儲過程操作
    void Proc_SP(void); //edit 2008-09-30 add
    //執行插入、修改、刪除
    void Proc_Exec(void);
};
//接收到的消息的封裝類
class CXMLRcvMsg : public CXMLMsg
{
	public:
		int ParseRevMsgWithCheck(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule);
		int ParseSndMsgWithCheck(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule);
};
//---------------------------------------------------------------------------
#endif

