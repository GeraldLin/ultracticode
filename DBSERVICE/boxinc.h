//---------------------------------------------------------------------------
#ifndef BoxIncludeH
#define BoxIncludeH
//---------------------------------------------------------------------------

#include "../include/msgh/tlinkid.h"
#include "../include/msgh/vmsgrule.h"
#include "../include/msgh/ivrlog.h"

#include "../include/comm/typedef.h"
#include "../include/comm/macrodef.h"
#include "../include/comm/rulestru.h"

#include "../comm/TcpServer.h"
#include "../comm/msgfifo.h"
#include "../comm/cstring.h"
#include "../comm/parxml.h"
//---------------------------------------------------------------------------
#endif

 