//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ThreadProc.h"
#include "main.h"
#include "FlwRule.h"
#include "FlwData.h"
#include "MsgProc.h"

#pragma package(smart_init)
extern CFlwData *FlwData;
extern CFlwRule *FlwRule;
extern CTCPServer *TcpServer;

//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CThreadProc::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall CThreadProc::CThreadProc(bool CreateSuspended)
    : TThread(CreateSuspended)
{
    //Priority = tpNormal;
    Priority = tpLower;
    FreeOnTerminate = true;
}
//---------------------------------------------------------------------------
void __fastcall CThreadProc::Execute()
{
    while ( !Terminated )
    {
      //QuarkCallDBServer->MainLoop();
      try
      {
        ProcQueue();
        FlwData->Proc_Query();
        FlwData->Proc_SP();
        FlwData->Proc_Exec();
      }
      catch (...)
      {
      }
      ::Sleep(10);
    }
}
//---------------------------------------------------------------------------
