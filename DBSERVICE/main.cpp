//---------------------------------------------------------------------------
//#include <mmsystem.h>
#include <DateUtils.hpp>
#include <stdio.h>
#include <time.h>
#include <inifiles.hpp>
#include "main.h"
#include "../comm/base64.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TQuarkCallDBServer *QuarkCallDBServer;

bool SystemStartId;
bool AutoRunId;
bool isDBConnected;
int ParserLoginId=0; //0-初始值 1-流程解析器已登錄 2-流程解析器已斷開

CFlwRule FlwRule; //流程規則類
CFlwData *FlwData; //配置、參數、變量數據類
CTCPServer *TcpServer; //
int gHTTPServerPort=5220;
US gHTTPServerId = 0;

CMsgfifo *Msgfifo; //接收消息隊列
CXMLRcvMsg RcvMsg; //保存接收到的通訊消息，靜態變量防止頻繁分配內存
CXMLRcvMsg LogRcvMsg;

HANDLE Mutex_DispComm;
HANDLE Mutex_ExecDB;
HANDLE Mutex_RecvMsg;

int LOGLoginId[MAX_LOGNODE_NUM];

int g_nINIFileType=0;
bool g_bBackDump=false;
bool g_bBackDumpDel=false;
int g_nBackDumpHour=2;
int g_nWatchDogId=1;
AnsiString g_strDBType="SQLSERVER";
AnsiString g_strWatchDogSqls="Select sysdate from dual;";
AnsiString g_strAppPath;
AnsiString g_strRootPath;
AnsiString g_strDBINIFileName;
AnsiString g_strServiceINIFileName;
AnsiString g_strUnimeINIFileName;

unsigned short IVRServerID=0;
//---------------------------------------------------------------------------

void OnAppReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //tcllinkc.dll callback function
{
   QuarkCallDBServer->OnAppReceiveDatax(remoteid, msgtype,buf,len);
}

void OnAppLogin(unsigned short serverid,unsigned short clientid)     //tcllinkc.dll callback function
{
   QuarkCallDBServer->OnAppLoginx(serverid,clientid);
   ParserLoginId = 1;
}

void OnAppClose(unsigned short serverid,unsigned short clientid)    //tcllinkc.dll callback function
{
   QuarkCallDBServer->OnAppClosex(serverid,clientid);
   ParserLoginId = 2;
}
//解密密碼
char *TQuarkCallDBServer::DecodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  //Disp_Comm_Msg(0, Pass);
  Base64Decode(key, Pass);
  //Disp_Comm_Msg(0, key);
  for (unsigned int i=1; i<strlen(key)-2; i++)
  {
    char Inc = key[i] - 'a';
    i++;
    rtn[j++] = char(key[i] - Inc + 15);
  }
  //Disp_Comm_Msg(0, key);
  return rtn;
}
AnsiString TQuarkCallDBServer::DecodePass(AnsiString Pass)
{
  return (char *)DecodePass(Pass.c_str());
}
US TQuarkCallDBServer::CombineID(UC IdType, UC IdNo)
{
    US m_i1, m_i2;
    m_i1 = ( IdType << 8 ) & 0xFF00;
    m_i2 = IdNo & 0x00FF;
    return (m_i1 | m_i2);
}

//---------------------------------------------------------------------------
void __fastcall TQuarkCallDBServer::OnAppReceiveDatax(unsigned short remoteid,
                                             unsigned short msgtype,
                                             const unsigned char *buf,
                                             unsigned long len)
{
  CH ErrMsg[MAX_EXP_STRING_LEN];

  ((unsigned char *)buf)[len] = 0;
  if (FlwData->DispComId == 1 || FlwData->WriteComId == 1)
  {
      sprintf(ErrMsg, "remoteid=0x%04x, msgtype=0x%04x, msg = %s", remoteid, msgtype, buf);
      Disp_Comm_Msg(2, ErrMsg);
  }
  if((msgtype>>8)!=MSGTYPE_DBXML) //消息類型不正確
  {
    LogRcvMsg.SetMsgBufId(msgtype&0xFF,(char *)buf);
    switch (msgtype&0xFF)
    {
    case LOGMSG_login:
      Proc_LOGMSG_login(remoteid&0xFF, LogRcvMsg);
      break;
    case LOGMSG_modifyiniparam:
      //Proc_LOGMSG_modifyiniparam(LogRcvMsg);
      break;
    }
    return;
  }
  if (WaitForSingleObject(Mutex_RecvMsg, 100) == WAIT_OBJECT_0)
  {
    if (Msgfifo->Write(remoteid, msgtype, (char *)buf) == 0)
    {
      char szBuf[2048];
      sprintf(szBuf, "recv msgbuf overflow [Head=%d Tail=%d] %s", Msgfifo->Head, Msgfifo->Tail, buf);
      Disp_Comm_Msg(0, szBuf);
    }
    ReleaseMutex(Mutex_RecvMsg);
  }
}

void __fastcall TQuarkCallDBServer::OnAppLoginx(unsigned short serverid,unsigned short clientid)
{
    UC ClientType, ClientNo;
    char msg[256];

    TcpServer->DisCombineID(clientid, ClientType, ClientNo);
    if (ClientType == NODE_XML)
    {
      WriteParserStatus(1);
      sprintf(msg, "FLWSERVICE[%d] login", ClientNo);
    }
    else if (ClientType == NODE_LOG)
    {
      //if (ClientNo < MAX_LOGNODE_NUM)
      //{
      //  LOGLoginId[ClientNo] = 1;
      //}
      sprintf(msg, "CONFIG[%d] login", ClientNo);
    }
    else if (ClientType == NODE_IVR)
    {
      IVRServerID = ClientNo;
      sprintf(msg, "IVRSERVICE[%d] login", ClientNo);
    }
    else
    {
      sprintf(msg, "Other[%d] login", ClientNo);
    }

    Disp_Comm_Msg(0, msg);
    //UcommDBService->LogMessage((char *)msg, EVENTLOG_INFORMATION_TYPE);
}

void __fastcall TQuarkCallDBServer::OnAppClosex(unsigned short serverid,unsigned short clientid)
{
    UC ClientType, ClientNo;
    char msg[256];

    TcpServer->DisCombineID(clientid, ClientType, ClientNo);
    if (ClientType == NODE_XML)
    {
      WriteParserStatus(2);
      FlwData->Init_All();
      sprintf(msg, "FLWSERVICE[%d] logout", ClientNo);
    }
    else if (ClientType == NODE_LOG)
    {
      //if (ClientNo < MAX_LOGNODE_NUM)
      //{
      //  LOGLoginId[ClientNo] = 0;
      //}
      sprintf(msg, "CONFIG[%d] logout", ClientNo);
    }
    else if (ClientType == NODE_IVR)
    {
      IVRServerID = 0;
      sprintf(msg, "IVRSERVICE[%d] logout", ClientNo);
    }
    else
    {
      sprintf(msg, "Other[%d] logout", ClientNo);
    }

    Disp_Comm_Msg(0, msg);
    //UcommDBService->LogMessage((char *)msg, EVENTLOG_ERROR_TYPE);
}
//---------------------------------------------------------------------------
//重新連接數據庫
int TQuarkCallDBServer::ReConnectDB(void)
{
    try
    {
        ADOConnection1->Connected = true;
        ::Sleep(2000);

        adoPublic1->Open();
        adoPublic1->Close();

        FlwData->DBConnectId = 1;
        WriteConnDBStatus(1);
        Disp_Comm_Msg(0, "ReConnectDB success");

        Send_ConnectDBSucc(MSG_onconnectdbsucc); //2016-10-05
        return 1;
    }
    catch (Exception &exception)
    {
        Disp_Comm_Msg(0, "ReConnectDB fail");
        Disp_Comm_Msg(0, exception.Message.c_str());
        ADOConnection1->Close();
        ADOConnection1->Connected = false;
        FlwData->DBConnectId = 2;
        return 0;
    }
}

//顯示節點登錄及退出信息
void TQuarkCallDBServer::Disp_LoginMsg(UC LogId, UC NodeType, UC NodeNo, const CH *LogMsg)
{
}
//顯示執行指令的錯誤及告警信息
void TQuarkCallDBServer::Disp_ErrMsg(UC MsgType, UL SessionId, UL CmdAddr, UC ChnType, US ChnNo, const CH *ErrMsg)
{
}
//顯示通信消息(MsgType: 1-發送 2-接收 3-SQL命令 )
void TQuarkCallDBServer::Disp_Comm_Msg(UC MsgType, const CH *CommMsg)
{
    if (FlwData->WriteComId == 0 && MsgType != 0)
      return;
    if (WaitForSingleObject(Mutex_DispComm, 50) == WAIT_OBJECT_0)
    {
      FILE *LogFile;
      AnsiString datetime;
      UC filename[MAX_PATH_FILE_LEN];
      Word Year, Month, Day, Hour, Min, Sec, MSec;

      datetime = DateTimeToStr(Now());
      //if (FlwData->WriteComId == 1)
      {
          LogFile = NULL;
          if (LogFile == NULL)
          {
              DecodeDate(Now(), Year, Month, Day );
              DecodeTime(Now(), Hour, Min, Sec, MSec );
              if (MsgType == 0)
                sprintf(filename, "%s\\err\\dbe_%04d%02d%02d.log", FlwData->ExecDir, Year, Month, Day);
              else
                sprintf(filename, "%s\\comm\\dbc_%04d%02d%02d%02d.log", FlwData->ExecDir, Year, Month, Day, Hour);
              LogFile = fopen( filename, "a+t");
          }
          if (LogFile != NULL)
          {
              if (MsgType == 0)
                  fprintf(LogFile, "%s ALRM: %s\n", datetime.c_str(), CommMsg);
              else if (MsgType == 1)
                  fprintf(LogFile, "%s SEND: %s\n", datetime.c_str(), CommMsg);
              else if (MsgType == 2)
                  fprintf(LogFile, "%s RECV: %s\n", datetime.c_str(), CommMsg);
              else if (MsgType == 3)
                  fprintf(LogFile, "%s SQLS: %s\n", datetime.c_str(), CommMsg);
              else
                  fprintf(LogFile, "%s PROC: %s\n", datetime.c_str(), CommMsg);
              fclose(LogFile);
          }
      }

      ReleaseMutex(Mutex_DispComm);
    }
}
//SqlType 0-表示查詢語句 1-表示插入、修改、刪除語句
void TQuarkCallDBServer::WriteSQLs(UC SqlType, const CH *sqls)
{
  if (strlen(sqls) == 0)
    return;
  if (WaitForSingleObject(Mutex_DispComm, 50) == WAIT_OBJECT_0)
  {
    FILE *LogFile;
    AnsiString datetime;
    UC filename[MAX_PATH_FILE_LEN];
    Word Year, Month, Day;

    DecodeDate(Now(), Year, Month, Day );
    if (SqlType == 0)
      sprintf(filename, "%s\\sql\\SQL_%04d%02d%02d.log", FlwData->ExecDir, Year, Month, Day);
    else
      sprintf(filename, "%s\\sql\\QUERY_%04d%02d%02d.log", FlwData->ExecDir, Year, Month, Day);

    LogFile = fopen( filename, "a+t");
    if (LogFile != NULL)
    {
      fprintf(LogFile, "%s\n", sqls);
      fclose(LogFile);
    }
    ReleaseMutex(Mutex_DispComm);
  }
}
//---------------------------------------------------------------------------
__fastcall TQuarkCallDBServer::TQuarkCallDBServer(TComponent* Owner)
	: TService(Owner)
{
  ThreadProc = NULL;
  ThreadConnDB = NULL;
  ThreadProcTCP = NULL;
  ThreadProcCDR = NULL;
}

TServiceController __fastcall TQuarkCallDBServer::GetServiceController(void)
{
	return (TServiceController) ServiceController;
}

void __stdcall ServiceController(unsigned CtrlCode)
{
	QuarkCallDBServer->Controller(CtrlCode);
}
//---------------------------------------------------------------------------
void TQuarkCallDBServer::WriteParserStatus(int logid)
{
  TIniFile *IniKey = new TIniFile (g_strDBINIFileName) ;
  IniKey->WriteInteger( "STATUS", "PARSERLOGID", logid ); //0-未登錄 1-已登錄 2-退出
  IniKey->WriteString( "STATUS", "PARSERLOGTIME", DateTimeToStr(Now()) );
  delete IniKey;
}
void TQuarkCallDBServer::WriteConnDBStatus(int connid)
{
  TIniFile *IniKey = new TIniFile (g_strDBINIFileName) ;
  IniKey->WriteInteger( "STATUS", "CONNECTDBID", connid ); //0-未連接 1-已連接 2-斷開
  IniKey->WriteString( "STATUS", "CONNECTDBTIME", DateTimeToStr(Now()) );
  delete IniKey;
}

void __fastcall TQuarkCallDBServer::ServiceCreate(TObject *Sender)
{
    CH ConnectionString[256];

    for (int i=0; i<MAX_LOGNODE_NUM; i++)
    {
      LOGLoginId[i] = 0;
    }

    Mutex_DispComm = CreateMutex(NULL, false, NULL);
    Mutex_ExecDB = CreateMutex(NULL, false, NULL);
    Mutex_RecvMsg = CreateMutex(NULL, false, NULL);

    g_strAppPath = ExtractFilePath(Forms::Application->ExeName);

    g_strRootPath = g_strAppPath.SubString(1, g_strAppPath.Length()-11);

    g_strUnimeINIFileName = g_strAppPath + "unimeconfig.ini";
    g_strDBINIFileName = g_strAppPath + "DBSERVICE.ini";

    g_nINIFileType = GetPrivateProfileInt("QUARKCALL","INIType", 0, g_strDBINIFileName.c_str());
    if (g_nINIFileType == 0)
      g_strServiceINIFileName = g_strDBINIFileName;
    else
      g_strServiceINIFileName = g_strUnimeINIFileName;

    TIniFile *IniKey = new TIniFile (g_strServiceINIFileName) ;

    UC ServerNo;
    int ServerPort;

    if (g_nINIFileType == 0)
    {
      ServerNo = IniKey->ReadInteger ( "CONFIGURE", "ServerId", 1 );
      ServerPort = IniKey->ReadInteger ( "CONFIGURE", "ServerPort", 5228 );
      gHTTPServerPort = IniKey->ReadInteger ( "CONFIGURE", "HTTPServerPort", 5221 );
      gHTTPServerId = 0;//IniKey->ReadInteger ( "CONFIGURE", "HTTPServerId", 0 );
    }
    else
    {
      ServerNo = IniKey->ReadInteger ( "TCPLINK", "DBServerID", 1 );
      ServerPort = IniKey->ReadInteger ( "TCPLINK", "DBServerPort", 5228 );
      gHTTPServerPort = IniKey->ReadInteger ( "TCPLINK", "HTTPServerPort", 5221 );
      gHTTPServerId = 0;//IniKey->ReadInteger ( "TCPLINK", "HTTPServerId", 0 );
    }
    g_strDBType = IniKey->ReadString ( "CONFIGURE", "DBType", "SQLSERVER" );
    adoPublic1->SQL->Clear();
    if (g_strDBType == "SQLSERVER")
      g_strWatchDogSqls = "select getdate() as nowtime";
    else if (g_strDBType == "MYSQL")
      g_strWatchDogSqls = "select NOW() as nowtime";
    else if (g_strDBType == "ORACLE")
      g_strWatchDogSqls = "select TO_CHAR(SYSDATE(),'yyyy-mm-dd') as nowtime";
    else if (g_strDBType == "ODBC")
      g_strWatchDogSqls = IniKey->ReadString ( "CONFIGURE", "WatchDogSqls", "select id from tbsysset where id=1" );
    adoPublic1->SQL->Add(g_strWatchDogSqls);

    g_bBackDump = false; //IniKey->ReadBool ( "CONFIGURE", "CDRBackDump", false );
    g_bBackDumpDel = false; //IniKey->ReadBool ( "CONFIGURE", "BackDumpDel", false );
    g_nBackDumpHour = IniKey->ReadInteger ( "CONFIGURE", "BackDumpHour", 2 );

    US ServerId = CombineID(16, ServerNo);

    delete IniKey;

    Msgfifo = new CMsgfifo(MAX_MSG_QUEUE_NUM*8);
    FlwData = new CFlwData;
    if (FlwData != NULL)
    {
        FlwData->INIFileType = g_nINIFileType;
        FlwData->DBINIFileName = g_strServiceINIFileName;
        strcpy(FlwData->ExecDir, g_strAppPath.c_str());
        FlwData->Read_IniFile();
        Disp_Comm_Msg(0, "system data init success");
        if (!DirectoryExists( g_strAppPath + "comm" ))
        {
            if (!CreateDir(g_strAppPath + "comm"))
            {
                //MessageBox(NULL,"創建comm目錄失敗!!!","警告",MB_OK|MB_ICONWARNING);
            }
        }
        if (!DirectoryExists( g_strAppPath + "err" ))
        {
            if (!CreateDir(g_strAppPath + "err"))
            {
                //MessageBox(NULL,"創建err目錄失敗!!!","警告",MB_OK|MB_ICONWARNING);
            }
        }
        if (!DirectoryExists( g_strAppPath + "sql" ))
        {
            if (!CreateDir(g_strAppPath + "sql"))
            {
                //MessageBox(NULL,"創建bill目錄失敗!!!","警告",MB_OK|MB_ICONWARNING);
            }
        }
        if (FlwData->DBType != DB_NONE)
        {
          try
          {
              FlwData->AdoConnection = ADOConnection1;
              switch (FlwData->DBType)
              {
                  case DB_SQLSERVER:
                  {
                      sprintf(ConnectionString,
                          "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s",
                          FlwData->Password,
                          FlwData->UserId,
                          FlwData->DataBase,
                          FlwData->DBServer);
                      //UcommDBService->Dependencies->Items[0]->Name = "MSSQLSERVER";
                      break;
                  }
                  case DB_ORACLE:
                  {
                      sprintf(ConnectionString,
                          "Provider=OraOLEDB.Oracle.1;Password=%s;Persist Security Info=True;User ID=%s;Data Source=%s",
                          FlwData->Password,
                          FlwData->UserId,
                          FlwData->DBServer );
                      break;
                  }
                  case DB_SYBASE:
                  {
                      sprintf(ConnectionString,
                          "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s",
                          FlwData->Password,
                          FlwData->UserId,
                          FlwData->DataBase,
                          FlwData->DBServer);
                      break;
                  }
                  case DB_ACCESS:
                  {
                      sprintf(ConnectionString,
                          "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%s",
                          FlwData->DataBase);
                      break;
                  }
                  case DB_MYSQL:
                  {
                      sprintf(ConnectionString, "Driver={MySQL ODBC 5.1 Driver};server=%s;uid=%s;pwd=%s;database=%s;",
                        FlwData->DBServer, FlwData->UserId, FlwData->Password, FlwData->DataBase);
                      //UcommDBService->Dependencies->Items[0]->Name = "MySQL";
                      break;
                  }
                  case DB_ODBC:
                  {
                      sprintf(ConnectionString,
                          "Data Source=%s;UID=%s;PWD=%s;",
                          FlwData->DataBase,
                          FlwData->UserId,
                          FlwData->Password);
                      break;
                  }
              }
              Disp_Comm_Msg(0, ConnectionString);
              ADOConnection1->ConnectionString = StrPas(ConnectionString);
              ADOConnection2->ConnectionString = StrPas(ConnectionString);
              //UcommDBService->LogMessage("Init Msg: ADO ConnectionString="+StrPas(ConnectionString), EVENTLOG_INFORMATION_TYPE);

              for (int i = 0; i < MAX_SESSION_NUM; i ++)
              {
                  if (FlwData->QuerySQLBuf[i].AdoQuery != NULL)
                  {
                      FlwData->QuerySQLBuf[i].AdoQuery->Connection = ADOConnection1;
                      FlwData->SPBuf[i].AdoSp->Connection = ADOConnection1;
                  }
              }
              FlwData->PubQuery1->Connection = ADOConnection1;
              FlwData->ExecQuery->Connection = ADOConnection1;

              ADOConnection1->Connected = true;
              FlwData->DBConnectId = 1;

              WriteConnDBStatus(1);
              Disp_Comm_Msg(0, "Connected db success");
          }
          catch (Exception &exception)
          {
              FlwData->DBConnectId = 2;
              ADOConnection1->Connected = false;
              WriteConnDBStatus(2);
              Disp_Comm_Msg(0, exception.Message.c_str());
              //StatusBar1->Panels->Items[3]->Text = "失敗!";
          }
        }
    }
    else
    {
        WriteConnDBStatus(2);
        Disp_Comm_Msg(0, "Init data error");
    }
    try
    {
      if (gHTTPServerId > 0)
      {
        IdHTTPServer1->DefaultPort = gHTTPServerPort;
        IdHTTPServer1->Active = true;
        Disp_Comm_Msg(0, "ServiceCreate HTTPServer start success");
      }
    }
    catch (...)
    {
    }
    TcpServer = new CTCPServer;
    if (TcpServer != NULL)
    {
        if (TcpServer->Run(ServerId, ServerPort) == 0)
        {
            //UcommDBService->LogMessage("Init Msg: Start TCPserver success Port="+IntToStr(ServerPort), EVENTLOG_INFORMATION_TYPE);
            Disp_Comm_Msg(0, "Start TcpServer success");
            //StatusBar1->Panels->Items[1]->Text = "服務已啟動!";
        }
        else
        {
            Disp_Comm_Msg(0, "Start TcpServer error");
        }
    }
    else
    {
        Disp_Comm_Msg(0, "load dll error");
    }

    SystemStartId = true;
}
//---------------------------------------------------------------------------
void __fastcall TQuarkCallDBServer::ServiceStart(TService *Sender, bool &Started)
{
  try
  {
    ADOConnection1->Connected = true;
    FlwData->DBConnectId = 1;
  }
  catch (...)
  {
    FlwData->DBConnectId = 2;
    ADOConnection1->Connected = false;
  }
  if (FlwData->DBType == DB_SQLSERVER)
    g_strWatchDogSqls = "select getdate() as nowtime";
  else if (FlwData->DBType == DB_MYSQL)
    g_strWatchDogSqls = "select NOW() as nowtime";
  else if (FlwData->DBType == DB_ORACLE)
    g_strWatchDogSqls = "Select sysdate from dual;";
  else if (FlwData->DBType == DB_ODBC)
    g_strWatchDogSqls = "Select sysdate from dual;"; //IniKey->ReadString ( "CONFIGURE", "WatchDogSqls", "select id from tbsysset where id=1" );
  adoPublic1->SQL->Clear();
  adoPublic1->SQL->Add(g_strWatchDogSqls);
  try
  {
    if (gHTTPServerId > 0)
    {
      IdHTTPServer1->DefaultPort = gHTTPServerPort;
      IdHTTPServer1->Active = true;
      Disp_Comm_Msg(0, "ServiceStart HTTPServer start success");
    }
  }
  catch (...)
  {
  }

  ThreadProc = new CThreadProc( false );
  ThreadConnDB = new CThreadConnDB( false );
  ThreadProcTCP = new CThreadProcTCP( false );
  if (g_bBackDump == true)
    ThreadProcCDR = new CThreadProcCDR( false );
  Started = true;
}
//---------------------------------------------------------------------------

void __fastcall TQuarkCallDBServer::ServiceStop(TService *Sender, bool &Stopped)
{
  ThreadProc->Terminate();
  ThreadConnDB->Terminate();
  ThreadProcTCP->Terminate();
  if (ThreadProcCDR)
    ThreadProcCDR->Terminate();
  Stopped = true;
}
//---------------------------------------------------------------------------

void __fastcall TQuarkCallDBServer::ServicePause(TService *Sender, bool &Paused)
{
  ThreadProc->Suspend();
  ThreadConnDB->Suspend();
  ThreadProcTCP->Suspend();
  if (ThreadProcCDR)
    ThreadProcCDR->Suspend();
  Paused = true;
}
//---------------------------------------------------------------------------

void __fastcall TQuarkCallDBServer::ServiceContinue(TService *Sender,
      bool &Continued)
{
  ThreadProc->Resume();
  ThreadConnDB->Resume();
  ThreadProcTCP->Resume();
  if (ThreadProcCDR)
    ThreadProcCDR->Resume();
  Continued = true;
}

//---------------------------------------------------------------------------
void TQuarkCallDBServer::MainLoop()
{
  //循環調用動態連接庫主循環函數
	if(TcpServer->IsOpen)
  {
    TcpServer->AppMainLoop();
  }
}
void TQuarkCallDBServer::ReConnectDBLoop()
{
  char szDisp[128];
  static int Conncount = 0, Conncount1=0;
  //判斷數據庫是否關閉否則重連
  if (FlwData->DBConnectId == 2 && FlwData->DBType != DB_NONE)
  {
      if (Conncount ++ > 500)
      {
          Conncount = 0;
          ReConnectDB();
      }
  }
  if (g_nWatchDogId == 1 && FlwData->DBConnectId == 1 && ADOConnection1->Connected == true)
  {
      if (Conncount1 ++ > 15000)
      {
          Conncount1 = 0;
          try
          {
            adoPublic1->Open();
            adoPublic1->Close();
          }
          catch(...)
          {
            sprintf(szDisp, "execsqls WatchDogSqls %s error", g_strWatchDogSqls.c_str());
            Disp_Comm_Msg(0, szDisp);
          }
      }
  }
}
void __fastcall TQuarkCallDBServer::ServiceDestroy(TObject *Sender)
{
  /*if (ThreadProc)
    ThreadProc->Terminate();
  if (TcpServer != NULL)
  {
      delete TcpServer;
      TcpServer = NULL;
  }
  if (Msgfifo != NULL)
  {
      delete Msgfifo;
      Msgfifo = NULL;
  }
  if (FlwData !=NULL)
  {
      delete FlwData;
      FlwData = NULL;
  }
  ADOConnection1->Connected = false;*/
}
//---------------------------------------------------------------------------


void __fastcall TQuarkCallDBServer::ServiceBeforeInstall(TService *Sender)
{
  g_strAppPath = ExtractFilePath(Forms::Application->ExeName);
  g_strServiceINIFileName = g_strAppPath + "DBSERVICE.ini";
  TIniFile *IniKey = new TIniFile (g_strServiceINIFileName);

  AnsiString strDependService = IniKey->ReadString ( "CONFIGURE", "DependServiceName", "" );

  if (strDependService.Length() > 0)
  {
    QuarkCallDBServer->Dependencies->Clear();
    QuarkCallDBServer->Dependencies->Add();
    QuarkCallDBServer->Dependencies->Items[0]->Name = strDependService;
  }

  delete IniKey;
}
//---------------------------------------------------------------------------
int TQuarkCallDBServer::CreateCDRMonthTable(const char *pszMonth, bool bDelBefore)
{
  char sqls[8000];

  memset(sqls, 0, 8000);

  if (g_strDBType == "SQLSERVER")
  {
    if (bDelBefore == true)
    {
      sprintf(sqls, "if exists(select * from sysobjects where name='tbcallcdr_%s') drop table tbcallcdr_%s \
CREATE TABLE [dbo].[tbcallcdr_%s](\
	[Id] [int] NOT NULL,\
	[SerialNo] [nvarchar](50) NOT NULL,\
	[ChnType] [int] NULL DEFAULT ('0'),\
	[ChnNo] [int] NULL DEFAULT ('0'),\
	[CallerNo] [nvarchar](50) NULL DEFAULT (N''),\
	[CalledNo] [nvarchar](50) NULL DEFAULT (N''),\
	[CustomNo] [nvarchar](50) NULL DEFAULT (N''),\
	[CallInOut] [tinyint] NULL DEFAULT ('0'),\
	[CallType] [tinyint] NULL DEFAULT ('1'),\
	[SrvType] [tinyint] NULL DEFAULT ('1'),\
	[SrvSubType] [int] NULL DEFAULT ((0)),\
	[CallTime] [datetime] NOT NULL,\
	[AnsTime] [datetime] NULL DEFAULT (NULL),\
	[AnsId] [tinyint] NULL DEFAULT ('0'),\
	[SelCode] [nvarchar](100) NULL DEFAULT (NULL),\
	[AcdTime] [datetime] NULL DEFAULT (NULL),\
	[WaitTime] [datetime] NULL DEFAULT (NULL),\
	[RingTime] [datetime] NULL DEFAULT (NULL),\
	[RingSeats] [nvarchar](100) NULL DEFAULT (N''),\
	[RingWorkerNos] [nvarchar](100) NULL DEFAULT (N''),\
	[SeatAnsTime] [datetime] NULL DEFAULT (NULL),\
	[SeatAnsId] [tinyint] NULL DEFAULT ('0'),\
	[SeatNo] [nvarchar](50) NULL DEFAULT (N''),\
	[WorkerNo] [nvarchar](50) NULL DEFAULT (N''),\
	[GroupNo] [int] NULL DEFAULT ('0'),\
	[SeatRelTime] [datetime] NULL DEFAULT (NULL),\
	[RelTime] [datetime] NULL DEFAULT (NULL),\
	[ACWTime] [datetime] NULL DEFAULT (NULL),\
	[Score] [int] NULL DEFAULT ('0'),\
	[TalkScore] [int] NULL DEFAULT ('0'),\
	[RecdRootPath] [nvarchar](100) NULL DEFAULT (N''),\
	[RecdFile] [nvarchar](100) NULL DEFAULT (N''),\
	[Remark] [nvarchar](100) NULL DEFAULT (N''),\
	[RecdFileSize] [int] NULL DEFAULT ('0'),\
	[RecdTimeLen] [int] NULL DEFAULT ('0'),\
	[RelReason] [int] NOT NULL DEFAULT ((0)),\
	[DutyNo] [int] NOT NULL DEFAULT ((0)),\
	[TranWorkerNo] [nvarchar](50) NULL,\
	[UpdateFlag] [int] NOT NULL DEFAULT ((0)),\
	[FlagReport] [int] NOT NULL DEFAULT ((1)),\
	[AccountNo] [varchar](50) NULL,\
	[OrgCallerNo] [nvarchar](50) NULL,\
	[OrgCalledNo] [nvarchar](50) NULL,\
	[areaname] [nvarchar](50) NULL,\
 CONSTRAINT [PK_tbcallcdr_%s_Id] PRIMARY KEY CLUSTERED\
(\
	[Id] ASC,\
	[SerialNo] ASC,\
	[CallTime] ASC\
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],\
 CONSTRAINT [tbcallcdr_%s$PK_tbCallcdr_%s] UNIQUE NONCLUSTERED\
(\
	[Id] ASC\
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]\
) ON [PRIMARY]\
", pszMonth, pszMonth, pszMonth, pszMonth, pszMonth, pszMonth);
    }
    else
    {
      sprintf(sqls, "if not exists(select * from sysobjects where name='tbcallcdr_%s') \
CREATE TABLE [dbo].[tbcallcdr_%s](\
	[Id] [int] NOT NULL,\
	[SerialNo] [nvarchar](50) NOT NULL,\
	[ChnType] [int] NULL DEFAULT ('0'),\
	[ChnNo] [int] NULL DEFAULT ('0'),\
	[CallerNo] [nvarchar](50) NULL DEFAULT (N''),\
	[CalledNo] [nvarchar](50) NULL DEFAULT (N''),\
	[CustomNo] [nvarchar](50) NULL DEFAULT (N''),\
	[CallInOut] [tinyint] NULL DEFAULT ('0'),\
	[CallType] [tinyint] NULL DEFAULT ('1'),\
	[SrvType] [tinyint] NULL DEFAULT ('1'),\
	[SrvSubType] [int] NULL DEFAULT ((0)),\
	[CallTime] [datetime] NOT NULL,\
	[AnsTime] [datetime] NULL DEFAULT (NULL),\
	[AnsId] [tinyint] NULL DEFAULT ('0'),\
	[SelCode] [nvarchar](100) NULL DEFAULT (NULL),\
	[AcdTime] [datetime] NULL DEFAULT (NULL),\
	[WaitTime] [datetime] NULL DEFAULT (NULL),\
	[RingTime] [datetime] NULL DEFAULT (NULL),\
	[RingSeats] [nvarchar](100) NULL DEFAULT (N''),\
	[RingWorkerNos] [nvarchar](100) NULL DEFAULT (N''),\
	[SeatAnsTime] [datetime] NULL DEFAULT (NULL),\
	[SeatAnsId] [tinyint] NULL DEFAULT ('0'),\
	[SeatNo] [nvarchar](50) NULL DEFAULT (N''),\
	[WorkerNo] [nvarchar](50) NULL DEFAULT (N''),\
	[GroupNo] [int] NULL DEFAULT ('0'),\
	[SeatRelTime] [datetime] NULL DEFAULT (NULL),\
	[RelTime] [datetime] NULL DEFAULT (NULL),\
	[ACWTime] [datetime] NULL DEFAULT (NULL),\
	[Score] [int] NULL DEFAULT ('0'),\
	[TalkScore] [int] NULL DEFAULT ('0'),\
	[RecdRootPath] [nvarchar](100) NULL DEFAULT (N''),\
	[RecdFile] [nvarchar](100) NULL DEFAULT (N''),\
	[Remark] [nvarchar](100) NULL DEFAULT (N''),\
	[RecdFileSize] [int] NULL DEFAULT ('0'),\
	[RecdTimeLen] [int] NULL DEFAULT ('0'),\
	[RelReason] [int] NOT NULL DEFAULT ((0)),\
	[DutyNo] [int] NOT NULL DEFAULT ((0)),\
	[TranWorkerNo] [nvarchar](50) NULL,\
	[UpdateFlag] [int] NOT NULL DEFAULT ((0)),\
	[FlagReport] [int] NOT NULL DEFAULT ((1)),\
	[AccountNo] [varchar](50) NULL,\
	[OrgCallerNo] [nvarchar](50) NULL,\
	[OrgCalledNo] [nvarchar](50) NULL,\
	[areaname] [nvarchar](50) NULL,\
 CONSTRAINT [PK_tbcallcdr_%s_Id] PRIMARY KEY CLUSTERED\
(\
	[Id] ASC,\
	[SerialNo] ASC,\
	[CallTime] ASC\
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],\
 CONSTRAINT [tbcallcdr_%s$PK_tbCallcdr_%s] UNIQUE NONCLUSTERED\
(\
	[Id] ASC\
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]\
) ON [PRIMARY]\
", pszMonth, pszMonth, pszMonth, pszMonth, pszMonth);
    }
    try
    {
      adoPublic2->SQL->Clear();
      adoPublic2->SQL->Add((char *)sqls);
      adoPublic2->ExecSQL();
      return 0;
    }
    catch ( ... )
    {
      return 1;
    }
  }
  else
  {
    int nExist = 0;
    if (bDelBefore == true)
    {
      sprintf(sqls, "DROP TABLE IF EXISTS `tbcallcdr_%s`;", pszMonth);
      try
      {
        adoPublic2->SQL->Clear();
        adoPublic2->SQL->Add((char *)sqls);
        adoPublic2->ExecSQL();
      }
      catch ( ... )
      {
      }
    }
    else
    {
      sprintf(sqls, "SELECT COUNT(*) as n FROM information_schema.tables WHERE TABLE_SCHEMA='ccdb' AND TABLE_NAME='tbcallcdr_%s`;", pszMonth);
      try
      {
        adoPublic2->SQL->Clear();
        adoPublic2->SQL->Add((char *)sqls);
        adoPublic2->Open();
        if (!adoPublic2->Eof)
        {
          nExist = adoPublic2->FieldByName("n")->AsInteger;
        }
      }
      catch ( ... )
      {
      }
    }
    if (nExist == 0)
    {
      sprintf(sqls, "CREATE TABLE `tbcallcdr_%s` (\
  `Id` int(11) unsigned NOT NULL,\
  `SerialNo` varchar(50) NOT NULL,\
  `ChnType` int(10) DEFAULT '0',\
  `ChnNo` int(10) DEFAULT '0',\
  `CallerNo` varchar(50) DEFAULT '',\
  `CalledNo` varchar(50) DEFAULT '',\
  `CustomNo` varchar(50) DEFAULT '',\
  `CallInOut` tinyint(3) unsigned DEFAULT '0',\
  `CallType` tinyint(3) unsigned DEFAULT '1',\
  `SrvType` tinyint(3) unsigned DEFAULT '1',\
  `SrvSubType` int(11) DEFAULT '1',\
  `CallTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',\
  `AnsTime` timestamp NULL DEFAULT NULL,\
  `AnsId` tinyint(3) unsigned DEFAULT '0',\
  `SelCode` varchar(100) DEFAULT NULL,\
  `AcdTime` timestamp NULL DEFAULT NULL,\
  `WaitTime` timestamp NULL DEFAULT NULL,\
  `RingTime` timestamp NULL DEFAULT NULL,\
  `RingSeats` varchar(100) DEFAULT '',\
  `RingWorkerNos` varchar(100) DEFAULT '',\
  `SeatAnsTime` timestamp NULL DEFAULT NULL,\
  `SeatAnsId` tinyint(3) unsigned DEFAULT '0',\
  `SeatNo` varchar(20) DEFAULT '',\
  `WorkerNo` varchar(20) DEFAULT '',\
  `GroupNo` int(10) DEFAULT '0',\
  `SeatRelTime` timestamp NULL DEFAULT NULL,\
  `RelTime` timestamp NULL DEFAULT NULL,\
  `ACWTime` timestamp NULL DEFAULT NULL,\
  `Score` int(10) DEFAULT '0',\
  `TalkScore` int(10) DEFAULT '0',\
  `RecdRootPath` varchar(100) DEFAULT '',\
  `RecdFile` varchar(100) DEFAULT '',\
  `Remark` varchar(100) DEFAULT '',\
  `RecdFileSize` int(11) DEFAULT '0',\
  `RecdTimeLen` int(11) DEFAULT '0',\
  `RelReason` int(11) DEFAULT '0',\
  `DutyNo` int(11) DEFAULT '0',\
  `TranWorkerNo` varchar(20) DEFAULT NULL,\
  `FlagReport` int(10) unsigned DEFAULT '1',\
  `updateflag` int(11) DEFAULT '0',\
  `AccountNo` varchar(50) DEFAULT NULL,\
  `OrgCallerNo` varchar(50) DEFAULT NULL,\
  `OrgCalledNo` varchar(50) DEFAULT NULL,\
  `areaname` varchar(50) DEFAULT NULL,\
  PRIMARY KEY (`Id`,`SerialNo`,`CallTime`)\
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8", pszMonth);

      try
      {
        adoPublic2->SQL->Clear();
        adoPublic2->SQL->Add((char *)sqls);
        adoPublic2->ExecSQL();
        return 0;
      }
      catch ( ... )
      {
        return 1;
      }
    }
    else
    {
      return 0;
    }
  }
}
int TQuarkCallDBServer::CreateSTATUSMonthTable(const char *pszMonth, bool bDelBefore)
{
  char sqls[8000];

  memset(sqls, 0, 8000);

  if (g_strDBType == "SQLSERVER")
  {
    if (bDelBefore == true)
    {
      sprintf(sqls, "if exists(select * from sysobjects where name='tbseatstatus_%s') drop table tbseatstatus_%s \
CREATE TABLE [dbo].[tbseatstatus_%s](\
	[Gid] [char](36) NOT NULL,\
	[WorkerNo] [varchar](20) NULL,\
	[SeatNo] [varchar](20) NULL,\
	[STATUS] [int] NULL DEFAULT ((0)),\
	[DutyNo] [int] NULL DEFAULT ((0)),\
	[StartTime] [int] NULL DEFAULT ((0)),\
	[StartTimeLoca] [datetime] NULL,\
	[EndTime] [int] NULL DEFAULT ((0)),\
	[EndTimeLoca] [datetime] NULL,\
	[Duration] [int] NULL DEFAULT ((0)),\
	[SerialNo] [varchar](50) NULL,\
	[Flag] [int] NULL DEFAULT ((0)),\
  [FlagReport] [int] NULL DEFAULT ((1)),\
	[Id] [int] IDENTITY(1,1) NOT NULL,\
  CONSTRAINT [PK_tbseatstatus_%s] PRIMARY KEY CLUSTERED\
(\
	[Gid] ASC\
	[StartTimeLoca] ASC,\
	[Id] ASC\
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]\
) ON [PRIMARY]\
", pszMonth, pszMonth, pszMonth, pszMonth);
    }
    else
    {
      sprintf(sqls, "if not exists(select * from sysobjects where name='tbseatstatus_%s') \
CREATE TABLE [dbo].[tbseatstatus_%s](\
	[Gid] [char](36) NOT NULL,\
	[WorkerNo] [varchar](20) NULL,\
	[SeatNo] [varchar](20) NULL,\
	[STATUS] [int] NULL DEFAULT ((0)),\
	[DutyNo] [int] NULL DEFAULT ((0)),\
	[StartTime] [int] NULL DEFAULT ((0)),\
	[StartTimeLoca] [datetime] NULL,\
	[EndTime] [int] NULL DEFAULT ((0)),\
	[EndTimeLoca] [datetime] NULL,\
	[Duration] [int] NULL DEFAULT ((0)),\
	[SerialNo] [varchar](50) NULL,\
	[Flag] [int] NULL DEFAULT ((0)),\
  [FlagReport] [int] NULL DEFAULT ((1)),\
	[Id] [int] IDENTITY(1,1) NOT NULL,\
  CONSTRAINT [PK_tbseatstatus_%s] PRIMARY KEY CLUSTERED\
(\
	[Gid] ASC\
	[StartTimeLoca] ASC,\
	[Id] ASC\
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]\
) ON [PRIMARY]\
", pszMonth, pszMonth, pszMonth);
    }
    try
    {
      adoPublic2->SQL->Clear();
      adoPublic2->SQL->Add((char *)sqls);
      adoPublic2->ExecSQL();
      return 0;
    }
    catch ( ... )
    {
      return 1;
    }
  }
  else
  {
    int nExist = 0;
    if (bDelBefore == true)
    {
      sprintf(sqls, "DROP TABLE IF EXISTS `tbseatstatus_%s`;", pszMonth);
      try
      {
        adoPublic2->SQL->Clear();
        adoPublic2->SQL->Add((char *)sqls);
        adoPublic2->ExecSQL();
      }
      catch ( ... )
      {
      }
    }
    else
    {
      sprintf(sqls, "SELECT COUNT(*) as n FROM information_schema.tables WHERE TABLE_SCHEMA='ccdb' AND TABLE_NAME='tbseatstatus_%s';", pszMonth);
      try
      {
        adoPublic2->SQL->Clear();
        adoPublic2->SQL->Add((char *)sqls);
        adoPublic2->Open();
        if (!adoPublic2->Eof)
        {
          nExist = adoPublic2->FieldByName("n")->AsInteger;
        }
      }
      catch ( ... )
      {
      }
    }
    if (nExist == 0)
    {
      sprintf(sqls, "CREATE TABLE `tbseatstatus_%s` (\
  `Gid` char(36) NOT NULL,\
  `WorkerNo` varchar(20) DEFAULT NULL,\
  `SeatNo` varchar(20) DEFAULT NULL,\
  `Status` int(11) DEFAULT '0',\
  `DutyNo` int(11) DEFAULT '0',\
  `StartTime` int(11) DEFAULT NULL,\
  `StartTimeLoca` datetime DEFAULT NULL,\
  `EndTime` int(11) DEFAULT NULL,\
  `EndTimeLoca` datetime DEFAULT NULL,\
  `Duration` int(11) DEFAULT '0',\
  `SerialNo` varchar(50) DEFAULT NULL,\
  `Flag` int(11) DEFAULT '0',\
  `FlagReport` int(10) unsigned DEFAULT '1',\
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,\
  PRIMARY KEY (`Gid`,`StartTimeLoca`,`Id`)\
) ENGINE=InnoDB DEFAULT CHARSET=utf8", pszMonth);

      try
      {
        adoPublic2->SQL->Clear();
        adoPublic2->SQL->Add((char *)sqls);
        adoPublic2->ExecSQL();
        return 0;
      }
      catch ( ... )
      {
        return 1;
      }
    }
    return 0;
  }
}
void TQuarkCallDBServer::ProcCDR()
{
  static bool ProcId = false;
  Word Hour, Min, Sec, MSec;
  DecodeTime(Now(), Hour, Min, Sec, MSec);

  if (g_bBackDump == true && Hour == 23)
  {
    if (Min <= 2 && ProcId == false)
    {
      ProcId = true;
    }
    if (Min > 5 && ProcId == true)
    {
      int failcount=0;
      try
      {
        ADOConnection2->Connected = true;
      }
      catch (...)
      {
        ::Sleep(5000);
        if (failcount++ > 3)
        {
          Disp_Comm_Msg(0, "導出話單記錄時連接數據庫超過5次失敗");
          return;
        }
      }
      char szStartTime[20], szEndTime[20], szMonth[10], sqls[512], szTemp[256];
      TDateTime tdTemp = Yesterday();
      strcpy(szMonth, tdTemp.FormatString("yyyymm").c_str());
      strcpy(szStartTime, tdTemp.FormatString("yyyy-mm-dd 00:00:00").c_str());
      strcpy(szEndTime, tdTemp.FormatString("yyyy-mm-dd 23:59:59").c_str());

      ::Sleep(100);
      //將昨天的話單分到月表
      if (CreateCDRMonthTable(szMonth, false) == 0)
      {
        sprintf(sqls, "insert into tbcallcdr_%s select * from tbcallcdr where CallTime>='%s' and CallTime<'%s'",
          szMonth, szStartTime, szEndTime);
        try
        {
          adoPublic2->SQL->Clear();
          adoPublic2->SQL->Add((char *)sqls);
          adoPublic2->ExecSQL();
          sprintf(szTemp, "導出昨天的話單成功(共%d條) %s", adoPublic2->RowsAffected, sqls);
          Disp_Comm_Msg(0, szTemp);
          ::Sleep(500);
          if (g_bBackDumpDel == true)
          {
            sprintf(sqls, "delete from tbcallcdr where CallTime>='%s' and CallTime<'%s'",
              szStartTime, szEndTime);
            try
            {
              adoPublic2->SQL->Clear();
              adoPublic2->SQL->Add((char *)sqls);
              adoPublic2->ExecSQL();
              sprintf(szTemp, "刪除昨天的話單成功(共%d條) %s", adoPublic2->RowsAffected, sqls);
              Disp_Comm_Msg(0, szTemp);
            }
            catch (...)
            {
              sprintf(szTemp, "刪除昨天的話單失敗 %s", sqls);
              Disp_Comm_Msg(0, szTemp);
            }
          }
        }
        catch (...)
        {
          sprintf(szTemp, "導出昨天的話單失敗 %s", sqls);
          Disp_Comm_Msg(0, szTemp);
        }
      }
      ::Sleep(1000);
      //將昨天的座席狀態記錄分到月表
      if (CreateSTATUSMonthTable(szMonth, false) == 0)
      {
        sprintf(sqls, "insert into tbseatstatus_%s select * from tbseatstatus where CallTime>='%s' and CallTime<'%s'",
          szMonth, szStartTime, szEndTime);
        try
        {
          adoPublic2->SQL->Clear();
          adoPublic2->SQL->Add((char *)sqls);
          adoPublic2->ExecSQL();
          sprintf(szTemp, "導出昨天的座席狀態記錄成功(共%d條) %s", adoPublic2->RowsAffected, sqls);
          Disp_Comm_Msg(0, szTemp);
          ::Sleep(500);
          if (g_bBackDumpDel == true)
          {
            sprintf(sqls, "delete from tbseatstatus where CallTime>='%s' and CallTime<'%s'",
              szStartTime, szEndTime);
            try
            {
              adoPublic2->SQL->Clear();
              adoPublic2->SQL->Add((char *)sqls);
              adoPublic2->ExecSQL();
              sprintf(szTemp, "刪除昨天的座席狀態記錄成功(共%d條) %s", adoPublic2->RowsAffected, sqls);
              Disp_Comm_Msg(0, szTemp);
            }
            catch (...)
            {
              sprintf(szTemp, "刪除昨天的座席狀態記錄失敗 %s", sqls);
              Disp_Comm_Msg(0, szTemp);
            }
          }
        }
        catch (...)
        {
          sprintf(szTemp, "導出昨天的座席狀態記錄失敗 %s", sqls);
          Disp_Comm_Msg(0, szTemp);
        }
      }

      ProcId = false;
    }
  }
}

void __fastcall TQuarkCallDBServer::IdHTTPServer1CommandGet(
      TIdPeerThread *AThread, TIdHTTPRequestInfo *RequestInfo,
      TIdHTTPResponseInfo *ResponseInfo)
{
  char szTemp[256];
  if (RequestInfo->Params->CommaText.Length() > 0)
  {
    sprintf(szTemp, "Recv HTTP RequestInfo Command=%s CommaText=%s", RequestInfo->Command.c_str(), RequestInfo->Params->CommaText.c_str());
    Disp_Comm_Msg(0, szTemp);
  }

  ResponseInfo->ResponseNo = 200;
  ResponseInfo->WriteHeader();

  ResponseInfo->ContentText = "OK";
  ResponseInfo->WriteContent();

  if (RequestInfo->Params->CommaText.Length() > 0)
    Send_WEBRequestEvent(RequestInfo->Command.c_str(), RequestInfo->Params->CommaText.c_str());
}
//---------------------------------------------------------------------------

