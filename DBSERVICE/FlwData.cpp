//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>
#include <dbtables.hpp>
#include "../comm/base64.h"
#include "FlwData.h"
#include "main.h"
#include "MsgProc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
extern HANDLE Mutex_DispComm;
extern HANDLE Mutex_ExecDB;
extern HANDLE Mutex_RecvMsg;

CFlwData::CFlwData()
{
  AdoConnection = NULL;
  DBConnectId = 0;

  DispComId = 0;
  WriteComId = 0;

	//初始化數據庫操作緩沖
	for (int i = 0; i < MAX_SQL_BUF_NUM; i++)
	{
        Init_ExecSQL_Buf(i);
	}
	ExecSQLBuf[0].state = 1;
	ExecSQLBuf[0].prior = 0xFFFF;
	ExecSQLBuf[0].next = 0xFFFF;

	for (int i = 0; i < MAX_SESSION_NUM; i ++)
	{
    Init_QuerySQL_Buf(i);
    QuerySQLBuf[i].AdoQuery = new TADOQuery(Forms::Application);

    Init_SP_Buf(i);  //edit 2008-09-30 add
    SPBuf[i].AdoSp = new TADOStoredProc(Forms::Application);
    SPBuf[i].AdoSp->Prepared = true;
	}
  PubQuery1 = new TADOQuery(Forms::Application);
  ExecQuery = new TADOQuery(Forms::Application);

  BillTableId = 0;
  BillDay = 1;
  WriteBillTimeId = 0;
  WriteTxtBillId = 0;
  INIFileType = 0;
  //讀配置文件
  //Read_IniFile();
}
CFlwData::~CFlwData()
{
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
      if (QuerySQLBuf[i].AdoQuery != NULL)
      {
          delete QuerySQLBuf[i].AdoQuery;
      }
      if (SPBuf[i].AdoSp != NULL) //edit 2008-09-30 add
      {
          delete SPBuf[i].AdoSp;
      }
  }
  if (PubQuery1 != NULL)
  {
      delete PubQuery1;
  }
  if (ExecQuery != NULL)
  {
      delete ExecQuery;
  }
}
//定時計數器
void CFlwData::Timer_Add(void)
{
    for (int i = 0; i < MAX_SESSION_NUM; i ++)
    {
        QuerySQLBuf[i].timer ++;
        SPBuf[i].timer ++; //edit 2008-09-30 add
        ExecSQLBuf[i].timer ++;
    }
}
//讀配置文件
void CFlwData::Read_IniFile(void)
{
    AnsiString temp, strSection;

    TIniFile *IniKey = new TIniFile (DBINIFileName) ;

    if (INIFileType == 0)
      strSection = "CONFIGURE";
    else
      strSection = "DATABASE";

    temp = IniKey->ReadString ( strSection, "DBType", "SQLSERVER" );
    if (temp == "SQLSERVER")
        DBType = DB_SQLSERVER;
    else if (temp == "ORACLE")
        DBType = DB_ORACLE;
    else if (temp == "SYBASE")
        DBType = DB_SYBASE;
    else if (temp == "MYSQL")
        DBType = DB_MYSQL;
    else if (temp == "ACCESS")
        DBType = DB_ACCESS;
    else if (temp == "ODBC")
        DBType = DB_ODBC;
    else
        DBType = DB_NONE;
    temp = IniKey->ReadString ( strSection, "DBServer", "127.0.0.1" );
    strncpy(DBServer, temp.c_str(), MAX_USERNAME_LEN);

    temp = IniKey->ReadString ( strSection, "DataBase", "callcenterdb" );
    strncpy(DataBase, temp.c_str(), MAX_USERNAME_LEN);

    temp = IniKey->ReadString ( strSection, "UserId", "sa" );
    strncpy(UserId, temp.c_str(), MAX_USERNAME_LEN);

    temp = DecodePass(IniKey->ReadString ( strSection, "Password", "cGJmbWxlYnBpaGtzcmthY2dz" ));
    //temp = IniKey->ReadString ( strSection, "Password", "cGJmbWxlYnBpaGtzcmthY2dz" );
    memset(Password, 0, 64);
    strncpy(Password, temp.c_str(), 63);

    BillTableId = IniKey->ReadInteger ( strSection, "BillTableId", 0 );
    WriteTxtBillId = IniKey->ReadInteger ( strSection, "WriteTxtBillId", 0 );

    ConnFailErrStr = IniKey->ReadString ( strSection, "ConnFailErrStr", "連接失敗" );
    if (ConnFailErrStr == "")
      ConnFailErrStr = "連接失敗";
    delete IniKey;
}
//初始化查詢緩沖
int CFlwData::Init_QuerySQL_Buf(US SqlBufId)
{
    if (SqlBufId >= MAX_SESSION_NUM) return 1;

    QuerySQLBuf[SqlBufId].state = 0;
    QuerySQLBuf[SqlBufId].timer = 0;

    QuerySQLBuf[SqlBufId].SessionId = 0xFFFFFFFF;
    QuerySQLBuf[SqlBufId].ClientId = 0xFFFF;
    QuerySQLBuf[SqlBufId].CmdAddr = 0xFFFF;
    QuerySQLBuf[SqlBufId].DeviceNo = 0xFF;
    QuerySQLBuf[SqlBufId].ChnType = 0xFF;
    QuerySQLBuf[SqlBufId].ChnNo = 0xFFFF;

    QuerySQLBuf[SqlBufId].QueryType = 0;
    memset(QuerySQLBuf[SqlBufId].GetFieldIdList, 0, 64);

    if (QuerySQLBuf[SqlBufId].AdoQuery != NULL)
    {
        QuerySQLBuf[SqlBufId].AdoQuery->Close();
    }
    memset(QuerySQLBuf[SqlBufId].sqls, 0, MAX_SQLS_LEN);
    QuerySQLBuf[SqlBufId].ExecResult = 0;
    QuerySQLBuf[SqlBufId].RecordCount = 0;
    QuerySQLBuf[SqlBufId].FieldCount = 0;
    QuerySQLBuf[SqlBufId].CurrRecdNo = 0;
    QuerySQLBuf[SqlBufId].DBEvent = 0xFFFF;
    memset(QuerySQLBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
    return 0;
}
//取空閑的查詢緩沖
int CFlwData::Get_Idel_QuerySQL_Buf(UL SessionId)
{
  CH DispMsg[MAX_EXP_STRING_LEN];

  int result = 0xFFFF;
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
    if (QuerySQLBuf[i].SessionId == SessionId)
    {
      Init_QuerySQL_Buf(i);
      result = i;
      //if (DispComId == 1 || WriteComId == 1)
      //{
      //  sprintf(DispMsg, "Get_Idel_QuerySQL_Buf SessionId = %d had exesit SqlBufId = %d", SessionId, result);
      //  FormMain->Disp_ErrMsg(1, SessionId, 0, 0, 0, DispMsg);
      //}
      break;
    }
  }
  if (result >= MAX_SESSION_NUM)
  {
    for (int i = 0; i < MAX_SESSION_NUM; i ++)
    {
        if (QuerySQLBuf[i].state == 0)
        {
            result = i;
            break;
        }
    }
  }
  if (DispComId == 1 || WriteComId == 1)
  {
    sprintf(DispMsg, "Get_Idel_QuerySQL_Buf = %d for SessionId = %d", result, SessionId);
    QuarkCallDBServer->Disp_Comm_Msg(4, DispMsg);
  }
  return result;
}
US CFlwData::Fine_QueryBuf_by_SessionId(UL SessionId)
{
  CH DispMsg[MAX_EXP_STRING_LEN];
  int result = 0xFFFF;
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
    if (QuerySQLBuf[i].SessionId == SessionId)
    {
      result = i;
    }
  }
  if (DispComId == 1 || WriteComId == 1)
  {
    sprintf(DispMsg, "Find_QueryBuf_by_SessionId = %d for SessionId = %d", result, SessionId);
    QuarkCallDBServer->Disp_Comm_Msg(1, DispMsg);
  }
  return result;
}
//初始化一存儲過程緩沖 //edit 2008-09-30 add
int CFlwData::Init_SP_Buf(US SqlBufId)
{
    if (SqlBufId >= MAX_SESSION_NUM) return 1;

    SPBuf[SqlBufId].state = 0;
    SPBuf[SqlBufId].timer = 0;

    SPBuf[SqlBufId].SessionId = 0xFFFFFFFF;
    SPBuf[SqlBufId].ClientId = 0xFFFF;
    SPBuf[SqlBufId].CmdAddr = 0xFFFF;
    SPBuf[SqlBufId].DeviceNo = 0xFF;
    SPBuf[SqlBufId].ChnType = 0xFF;
    SPBuf[SqlBufId].ChnNo = 0xFFFF;

    if (SPBuf[SqlBufId].AdoSp != NULL)
    {
        SPBuf[SqlBufId].AdoSp->Close();
    }
    memset(SPBuf[SqlBufId].spname, 0, MAX_PATH_FILE_LEN);
    SPBuf[SqlBufId].ExecResult = 0;
    SPBuf[SqlBufId].DBEvent = 0xFFFF;
    memset(QuerySQLBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
    return 0;
}
//取空閑的存儲過程緩沖 //edit 2008-09-30 add
int CFlwData::Get_Idel_SP_Buf(UL SessionId)
{
  CH DispMsg[MAX_EXP_STRING_LEN];

  int result = 0xFFFF;
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
    if (SPBuf[i].SessionId == SessionId)
    {
      Init_SP_Buf(i);
      result = i;
      break;
    }
  }
  if (result >= MAX_SESSION_NUM)
  {
    for (int i = 0; i < MAX_SESSION_NUM; i ++)
    {
        if (SPBuf[i].state == 0)
        {
            result = i;
            break;
        }
    }
  }
  if (DispComId == 1 || WriteComId == 1)
  {
    sprintf(DispMsg, "Get_Idel_SP_Buf = %d for SessionId = %d", result, SessionId);
    QuarkCallDBServer->Disp_Comm_Msg(4, DispMsg);
  }
  return result;
}
//根據會話標志查找存儲過程緩沖序號 //edit 2008-09-30 add
US CFlwData::Fine_SPBuf_by_SessionId(UL SessionId)
{
  CH DispMsg[MAX_EXP_STRING_LEN];
  int result = 0xFFFF;
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
    if (SPBuf[i].SessionId == SessionId)
    {
      result = i;
    }
  }
  if (DispComId == 1 || WriteComId == 1)
  {
    sprintf(DispMsg, "Fine_SPBuf_by_SessionId = %d for SessionId = %d", result, SessionId);
    QuarkCallDBServer->Disp_Comm_Msg(1, DispMsg);
  }
  return result;
}
//初始化插入、修改、刪除緩沖
int CFlwData::Init_ExecSQL_Buf(US SqlBufId)
{
    if (SqlBufId >= MAX_SESSION_NUM) return 1;

    ExecSQLBuf[SqlBufId].state = 0;
    ExecSQLBuf[SqlBufId].timer = 0;
    
    ExecSQLBuf[SqlBufId].SessionId = 0xFFFFFFFF;
    ExecSQLBuf[SqlBufId].ClientId = 0xFFFF;
    ExecSQLBuf[SqlBufId].CmdAddr = 0xFFFF;
    ExecSQLBuf[SqlBufId].DeviceNo = 0xFF;
    ExecSQLBuf[SqlBufId].ChnType = 0xFF;
    ExecSQLBuf[SqlBufId].ChnNo = 0xFFFF;

    ExecSQLBuf[SqlBufId].ExecType = 0;
    memset(ExecSQLBuf[SqlBufId].sqls, 0, MAX_SQLS_LEN);
    ExecSQLBuf[SqlBufId].ExecResult = 0;
    ExecSQLBuf[SqlBufId].RecordCount = 0;
    ExecSQLBuf[SqlBufId].DBEvent = 0xFFFF;
    ExecSQLBuf[SqlBufId].TollFee = 0;
    memset(ExecSQLBuf[SqlBufId].ErrorBuf, 0, MAX_ERRORBUF_LEN);
    ExecSQLBuf[SqlBufId].prior = 0xFFFF;
    ExecSQLBuf[SqlBufId].next = 0xFFFF;
    return 0;
}
void CFlwData::Init_All(void)
{
  for (int i = 0; i < MAX_SQL_BUF_NUM; i++)
  {
      Init_ExecSQL_Buf(i);
  }
  ExecSQLBuf[0].state = 1;
  ExecSQLBuf[0].prior = 0xFFFF;
  ExecSQLBuf[0].next = 0xFFFF;

  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
      Init_QuerySQL_Buf(i);
      Init_SP_Buf(i); //edit 2008-09-30 add
  }
}
//取一空閑數據庫緩沖
int CFlwData::Get_Idel_ExecSQL_Buf(UL SessionId)
{
  int result = 0xFFFF;
  CH DispMsg[MAX_EXP_STRING_LEN];

	for (int i = 1; i < MAX_SESSION_NUM; i++)
	{
    if (ExecSQLBuf[i].state == 0)
    {
      result = i;
      break;
    }
	}
  if (DispComId == 1 || WriteComId == 1)
  {
    sprintf(DispMsg, "Get_Idel_ExecSQL_Buf = %d for SessionId = %d", result, SessionId);
    QuarkCallDBServer->Disp_Comm_Msg(4, DispMsg);
  }
  return result;
}
//增加數據庫操作
int CFlwData::Add_SQL_Buf(US SqlBufId, UL SessionId, US ClientId, UL CmdAddr, UC DeviceNo, UC ChnType, US ChnNo, UC ExecType, const CH *sqls, double TollFee)
{
    US TailSqlBufId;

    if (SqlBufId >= MAX_SESSION_NUM) return 1;

    ExecSQLBuf[SqlBufId].state = 1;
    ExecSQLBuf[SqlBufId].timer = 0;

    ExecSQLBuf[SqlBufId].SessionId = SessionId;
    ExecSQLBuf[SqlBufId].ClientId = ClientId;
    ExecSQLBuf[SqlBufId].CmdAddr = CmdAddr;
    ExecSQLBuf[SqlBufId].DeviceNo = DeviceNo;
    ExecSQLBuf[SqlBufId].ChnType = ChnType;
    ExecSQLBuf[SqlBufId].ChnNo = ChnNo;

    ExecSQLBuf[SqlBufId].ExecType = ExecType;
    strncpy(ExecSQLBuf[SqlBufId].sqls, sqls, MAX_SQLS_LEN);
    ExecSQLBuf[SqlBufId].ExecResult = 0;
    ExecSQLBuf[SqlBufId].RecordCount = 0;
    ExecSQLBuf[SqlBufId].DBEvent = 0xFFFF;
    ExecSQLBuf[SqlBufId].TollFee = TollFee;
    TailSqlBufId = ExecSQLBuf[0].prior;

    if (TailSqlBufId >= MAX_SESSION_NUM)
    {   //隊列為空時
        ExecSQLBuf[0].prior = SqlBufId;
        ExecSQLBuf[0].next = SqlBufId;
        ExecSQLBuf[SqlBufId].prior = 0;
        ExecSQLBuf[SqlBufId].next = 0xFFFF;
    }
    else
    {
        ExecSQLBuf[0].prior = SqlBufId;
        ExecSQLBuf[TailSqlBufId].next = SqlBufId;
        ExecSQLBuf[SqlBufId].prior = TailSqlBufId;
        ExecSQLBuf[SqlBufId].next = 0xFFFF;
    }
    return 0;
}
//刪除數據庫操作
int CFlwData::Del_SQL_Buf(US SqlBufId)
{
    US PriorId, NextId;

    PriorId = ExecSQLBuf[SqlBufId].prior;
    NextId = ExecSQLBuf[SqlBufId].next;
    if (NextId >= MAX_SESSION_NUM)
    {
        if (PriorId == 0)
        {
            ExecSQLBuf[0].prior = 0xFFFF;
            ExecSQLBuf[0].next = 0xFFFF;
        }
        else
        {
            ExecSQLBuf[0].prior = PriorId;
            ExecSQLBuf[PriorId].next = 0xFFFF;
        }
    }
    else
    {
        ExecSQLBuf[PriorId].next = NextId;
    }
    Init_ExecSQL_Buf(SqlBufId);
    return 0;
}
//解密密碼
char *CFlwData::DecodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  Base64Decode(key, Pass);
  for (unsigned int i=1; i<strlen(key)-2; i++)
  {
    char Inc = key[i] - 'a';
    i++;
    rtn[j++] = char(key[i] - Inc + 15);
  }
  return rtn;
}
AnsiString CFlwData::DecodePass(AnsiString Pass)
{
  return (char *)DecodePass(Pass.c_str());
}
//執行查詢操作
void CFlwData::Proc_Query(void)
{
  AnsiString value;
  static char pszValues[1024], FieldList[1920];
  int n, m, len=0;

  //static int startpos=0;
  //int count=0;

  //if (startpos >= MAX_SESSION_NUM)
  //  startpos = 0;
  //for (int i = startpos; i < MAX_SESSION_NUM; i ++)
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
    //startpos=i+1;
    if (QuerySQLBuf[i].state == 0)
        continue;
    if (QuerySQLBuf[i].ExecResult == 1)
    {
        //count++;
        QuerySQLBuf[i].ExecResult = 2;
        try
        {
            QuerySQLBuf[i].AdoQuery->Close();
            QuerySQLBuf[i].AdoQuery->SQL->Clear();
            QuerySQLBuf[i].AdoQuery->SQL->Add((char *)(QuerySQLBuf[i].sqls));
            QuerySQLBuf[i].AdoQuery->Open();
            if (DispComId == 1 || WriteComId == 1)
            {
              QuarkCallDBServer->Disp_Comm_Msg(3, QuerySQLBuf[i].sqls);
            }
            QuerySQLBuf[i].RecordCount = QuerySQLBuf[i].AdoQuery->RecordCount;
            QuerySQLBuf[i].FieldCount = QuerySQLBuf[i].AdoQuery->FieldCount;
            QuerySQLBuf[i].AdoQuery->First();
            QuerySQLBuf[i].CurrRecdNo = 1;
            QuerySQLBuf[i].DBEvent = OnDBSuccess;
            QuerySQLBuf[i].ExecResult = 3; //3-查詢成功
            memset(QuerySQLBuf[i].ErrorBuf, 0, MAX_ERRORBUF_LEN);
            sprintf(QuerySQLBuf[i].ErrorBuf, "return rows=%d fields=%d", QuerySQLBuf[i].RecordCount, QuerySQLBuf[i].FieldCount);

            if (QuerySQLBuf[i].QueryType == 2)  //2-dbselect
            {
              if (QuerySQLBuf[i].RecordCount == 0)
              {
                Send_DBSelect_FailResult(i, "have not record!");
              }
              else
              {
                try
                {
                  memset(FieldList, 0, 1920);
                  for (int FieldNo = 0; FieldNo < QuerySQLBuf[i].FieldCount; FieldNo ++)
                  {
                    if (QuerySQLBuf[i].GetFieldIdList[FieldNo] == '0')
                    {
                      memset(pszValues, 0, 1024);
                      m = 0;
                    }
                    else
                    {
                      value = QuerySQLBuf[i].AdoQuery->Fields->Fields[FieldNo]->AsString;

                      memset(pszValues, 0, 1024);
                      m = 0;
                      for (n = 1; n < 1024 && n <= value.Length(); n ++)
                      {
                        pszValues[m] = value[n];
                        if (pszValues[m] == '\"')
                        {
                          m ++;
                          pszValues[m] = '\"';
                        }
                        m ++;
                        if (m >= 1024)
                          break;
                      }
                    }
                    len = len+m+1;
                    if (len >= 1920)
                      break;
                    strcat(FieldList, pszValues);
                    strcat(FieldList, "`");
                  }
                  Send_DBSelect_Result(i, FieldList);
                }
                catch (Exception &exception)
                {
                  Send_DBSelect_FailResult(i, exception.Message.c_str());
                }
              }
            }
            else
            {
              Send_DBQuery_Result(i);
            }
        }
        catch (Exception &exception)
        {
            QuarkCallDBServer->WriteSQLs(0, QuerySQLBuf[i].sqls);
            if (exception.Message.AnsiPos(ConnFailErrStr) > 0
                || exception.Message.AnsiPos("連接失敗") > 0
                || exception.Message.AnsiPos("超時已過期") > 0
                || exception.Message.AnsiPos("Missing Connection or ConnectionString") > 0
                || exception.Message.AnsiPos("gone away") > 0
                || exception.Message.AnsiPos("not connected to ORACLE") > 0
                || exception.Message.AnsiPos("超出打開游標的最大數") > 0
                || exception.Message.AnsiPos("一般性網絡錯誤") > 0
                || exception.Message.AnsiPos("未連接到") > 0
                || exception.Message.AnsiPos("硈??毖") > 0
                || exception.Message.AnsiPos("?�Z�I??蕩�Z����") > 0
                || exception.Message.AnsiPos("筄��?戳") > 0
                || exception.Message.AnsiPos("ORA-03114") > 0
                )
            {
                QuarkCallDBServer->ADOConnection1->Close();
                QuarkCallDBServer->ADOConnection1->Connected = false;
                QuarkCallDBServer->WriteConnDBStatus(2);
                //FormMain->StatusBar1->Panels->Items[3]->Text = "失敗!";
                DBConnectId = 2;
                Send_ConnectDBFail(MSG_onconnectdbfail);
            }
            strncpy(QuerySQLBuf[i].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
            QuerySQLBuf[i].RecordCount = 0;
            QuerySQLBuf[i].FieldCount = 0;
            QuerySQLBuf[i].CurrRecdNo = 0;
            QuerySQLBuf[i].DBEvent = OnDBFail;
            QuerySQLBuf[i].ExecResult = 4; //4-查詢失敗
            QuarkCallDBServer->Disp_Comm_Msg(0, QuerySQLBuf[i].ErrorBuf);
            Send_DBQuery_Result(i);
            Init_QuerySQL_Buf(i);
        }
    }
    //if (count > 16)
    //{
    //  break;
    //}
  }
}
//執行存儲過程操作 //edit 2008-09-30 add
void CFlwData::Proc_SP(void)
{
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
    if (SPBuf[i].state == 0)
        continue;
    if (SPBuf[i].ExecResult == 1)
    {
        SPBuf[i].ExecResult = 2;
        try
        {
            //SPBuf[i].AdoSp->Close();
            //SPBuf[i].AdoSp->ProcedureName = SPBuf[i].spname;
            SPBuf[i].AdoSp->ExecProc();
            if (DispComId == 1 || WriteComId == 1)
            {
              QuarkCallDBServer->Disp_Comm_Msg(3, SPBuf[i].spname);
            }
            SPBuf[i].DBEvent = OnDBSuccess;
            SPBuf[i].ExecResult = 3; //3-查詢成功
            memset(SPBuf[i].ErrorBuf, 0, MAX_ERRORBUF_LEN);
            Send_DBSP_Result(i);
        }
        catch (Exception &exception)
        {
              if (exception.Message.AnsiPos(ConnFailErrStr) > 0
                || exception.Message.AnsiPos("連接失敗") > 0
                || exception.Message.AnsiPos("超時已過期") > 0
                || exception.Message.AnsiPos("Missing Connection or ConnectionString") > 0
                || exception.Message.AnsiPos("gone away") > 0
                || exception.Message.AnsiPos("not connected to ORACLE") > 0
                || exception.Message.AnsiPos("超出打開游標的最大數") > 0
                || exception.Message.AnsiPos("一般性網絡錯誤") > 0
                || exception.Message.AnsiPos("未連接到") > 0
                || exception.Message.AnsiPos("硈??毖") > 0
                || exception.Message.AnsiPos("?�Z�I??蕩�Z����") > 0
                || exception.Message.AnsiPos("筄��?戳") > 0
                || exception.Message.AnsiPos("ORA-03114") > 0
                )
            {
                QuarkCallDBServer->ADOConnection1->Close();
                QuarkCallDBServer->ADOConnection1->Connected = false;
                QuarkCallDBServer->WriteConnDBStatus(2);
                DBConnectId = 2;
                Send_ConnectDBFail(MSG_onconnectdbfail);
                //FormMain->StatusBar1->Panels->Items[3]->Text = "失敗!";
            }
            strncpy(SPBuf[i].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
            SPBuf[i].DBEvent = OnDBFail;
            SPBuf[i].ExecResult = 4; //4-查詢失敗
            QuarkCallDBServer->Disp_Comm_Msg(0, SPBuf[i].ErrorBuf);
            Send_DBSP_Result(i);
            Init_SP_Buf(i);
        }
    }
  }
}
//執行插入、修改、刪除
void CFlwData::Proc_Exec(void)
{
  //CH DispMsg[MAX_EXP_STRING_LEN];
  US prior, next;
  TMemoryStream *pMemStream=NULL;

  prior;
  next = ExecSQLBuf[0].next;
  while (next < MAX_SESSION_NUM)
  {
      if (ExecSQLBuf[next].state == 1 &&
          ExecSQLBuf[next].ExecType > 0 &&
          ExecSQLBuf[next].ExecResult == 0)
      {
          ExecSQLBuf[next].ExecResult = 1;
          try
          {
              ExecQuery->SQL->Clear();
              if (ExecSQLBuf[next].ExecType == 7)
              {
                //將文件保存到數據表
                try
                {
                  QuarkCallDBServer->Disp_Comm_Msg(0, ExecSQLBuf[next].ErrorBuf);
                  QuarkCallDBServer->Disp_Comm_Msg(0, ExecSQLBuf[next].sqls);

                  pMemStream = new TMemoryStream();
			            pMemStream->LoadFromFile((char *)ExecSQLBuf[next].ErrorBuf);
			            pMemStream->Position = 0;

			            ExecQuery->SQL->Add((char *)(ExecSQLBuf[next].sqls));
			            ExecQuery->Parameters->ParamByName("MyblobData")->DataType = ftBlob;
                  ExecQuery->Parameters->ParamByName("MyblobData")->LoadFromStream(pMemStream, ftBlob);
			            ExecQuery->ExecSQL();
                }
                __finally
                {
                  pMemStream->Free();
                }
              }
              else
              {
                ExecQuery->SQL->Add((char *)(ExecSQLBuf[next].sqls));
                ExecQuery->ExecSQL();
              }

              ExecSQLBuf[next].DBEvent = OnDBSuccess;
              ExecSQLBuf[next].ExecResult = 2;
              ExecSQLBuf[next].RecordCount = ExecQuery->RowsAffected;
              memset(ExecSQLBuf[next].ErrorBuf, 0, MAX_ERRORBUF_LEN);
              sprintf(ExecSQLBuf[next].ErrorBuf, "%d rows affect", ExecQuery->RowsAffected);
              if (ExecSQLBuf[next].ExecType == 4)
                Send_WriteBill_Result(next);
              else
                Send_DBCmd_Result(next);
          }
          catch (Exception &exception)
          {
              QuarkCallDBServer->WriteSQLs(1, ExecSQLBuf[next].sqls);
              if (exception.Message.AnsiPos(ConnFailErrStr) > 0
                || exception.Message.AnsiPos("連接失敗") > 0
                || exception.Message.AnsiPos("超時已過期") > 0
                || exception.Message.AnsiPos("Missing Connection or ConnectionString") > 0
                || exception.Message.AnsiPos("gone away") > 0
                || exception.Message.AnsiPos("not connected to ORACLE") > 0
                || exception.Message.AnsiPos("超出打開游標的最大數") > 0
                || exception.Message.AnsiPos("一般性網絡錯誤") > 0
                || exception.Message.AnsiPos("未連接到") > 0
                || exception.Message.AnsiPos("硈??毖") > 0
                || exception.Message.AnsiPos("?�Z�I??蕩�Z����") > 0
                || exception.Message.AnsiPos("筄��?戳") > 0
                || exception.Message.AnsiPos("ORA-03114") > 0
                )
              {
                QuarkCallDBServer->ADOConnection1->Close();
                QuarkCallDBServer->ADOConnection1->Connected = false;
                QuarkCallDBServer->WriteConnDBStatus(2);
                //FormMain->StatusBar1->Panels->Items[3]->Text = "失敗!";
                DBConnectId = 2;
                Send_ConnectDBFail(MSG_onconnectdbfail);
              }
              strncpy(ExecSQLBuf[next].ErrorBuf, exception.Message.c_str(), MAX_ERRORBUF_LEN);
              ExecSQLBuf[next].DBEvent = OnDBFail;
              ExecSQLBuf[next].ExecResult = 3;
              QuarkCallDBServer->Disp_Comm_Msg(0, ExecSQLBuf[next].ErrorBuf);
              if (ExecSQLBuf[next].ExecType == 4)
                Send_WriteBill_Result(next);
              else
                Send_DBCmd_Result(next);
          }
      }
      prior = next;
      next = ExecSQLBuf[next].next;
      Init_ExecSQL_Buf(prior);

      ExecSQLBuf[0].next = next;
      if (next < MAX_SESSION_NUM)
          ExecSQLBuf[next].prior = 0;
      else
          ExecSQLBuf[0].prior = next;
  }
}
//接收XML解析
int CXMLRcvMsg::ParseRevMsgWithCheck(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule)
{
  int result=ParseRevMsg(MsgRule);

  return result;
}
//發送XML解析
int CXMLRcvMsg::ParseSndMsgWithCheck(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule)
{
	int result=ParseSndMsg(MsgRule);
  
   return result;	
}

