//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ThreadProcCDR.h"
#include "main.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CThreadProcCDR::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall CThreadProcCDR::CThreadProcCDR(bool CreateSuspended)
  : TThread(CreateSuspended)
{
    Priority = tpNormal;
    FreeOnTerminate = true;
}
//---------------------------------------------------------------------------
void __fastcall CThreadProcCDR::Execute()
{
  while ( !Terminated )
  {
    QuarkCallDBServer->ProcCDR();
    ::Sleep(60);
  }
}
//---------------------------------------------------------------------------
