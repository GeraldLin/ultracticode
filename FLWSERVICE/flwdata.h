//---------------------------------------------------------------------------
#ifndef FlwDataH
#define FlwDataH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//接入字冠結構(采用隊列方式保存，增加字冠時，按字冠長度從大到小排)
typedef struct
{
	UC state;		                //數據狀態 0: 無 1：有
  UC FuncGroup;                   //業務腳本功能服務組號
	US FuncNo;	                    //流程功能號
	
	CStringX PreCaller; //接入主叫號碼字冠
  CStringX PreCalled; //接入被叫號碼字冠
	US MinLen;		                //接入字冠最少長度
	US MaxLen;		                //接入字冠最大長度
  US FlwNo;	                    //流程地址序號
	US prior;		                //對應的前一序號
	US next;		                //對應的下一序號, 0xFFFF表示結束
	unsigned short resv;
}VXML_PRECODE_STRUCT;


class CGData
{
private:
   //客戶端號
  UC ClientId;
public:
	CGData();
	virtual ~CGData();

  
  CStringX SysVar[MAX_SYS_NUM];


  //接入字冠
  VXML_PRECODE_STRUCT PreCodeNo[MAX_ACCESSCODES_NUM];
 
public:
  //讀配置文件
  void ReadIni(char *filename);
 
  //增加接入字冠
  int AddPreCode(char *Caller, const char *Called, short MinLen, short MaxLen, UC FuncGroup, US FuncNo, US FlwNo);
  //刪除接入字冠
  int DelPreCode(char *Caller, const char *Called);
  //根據主被叫號碼取流程序號
  int Get_FlwNo_By_CalledNo(const CStringX &CallerNo, const CStringX &CalledNo, US &FlwNo);
   //查詢呼叫限制表(0-沒有記錄 1-限制撥打)
  int Query_RestTel(UC FuncGroup, US FuncNo, const CStringX &CallerNo, const CStringX &CalledNo);
  
  ////////////////////////////////
  //發送接入字冠
  int Send_AccessNo_To_IVR(UC settype, const CH *accessno, UC minlen, UC maxlen);

  void SetClientId(UC nClientId);
  UC GetClientId()
  {return ClientId;}
  
};

//Parser接收到的消息的封裝類
class CXMLRcvMsg : public CXMLMsg
{
	public:
		int ParseRevMsgWithCheck(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule);
		int ParseSndMsgWithCheck(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule);
}	;

//Parser發送的消息的封裝類
class CXMLSndMsg : public CXMLMsg
{
public:	
	void SetHeadToBuf(const char *msgname,CSession *pSession); //在buf中設置頭部
}	;



//---------------------------------------------------------------------------
#endif

