//
// service.h - 服務程序框架
//


#if !defined(AFX_SERVICE_H__40095FEA_5F70_11D4_B3A2_0080C88FC26A__INCLUDED_)
#define AFX_SERVICE_H__40095FEA_5F70_11D4_B3A2_0080C88FC26A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "odsError.h"

////////////////////////////////////////////////////////////////////////
// 和服務相關的宏定義，具體的服務程序實現應當修改這幾個宏的內容

#define QUARKCALL_PLATFORM

#ifdef QUARKCALL_PLATFORM
// 服務程序名稱
#define SRV_APPNAME						  _T("QuarkCallFLWService")
// 要安裝的 NT 服務名稱
#define SRV_SERVICENAME			    _T("QuarkCallFLWService")
// 要安裝的 NT 服務的顯示名稱
#define SRV_SERVICEDISPLAYNAME	_T("QuarkCallFLWService")
#else
// 服務程序名稱
#define SRV_APPNAME						  _T("CTICallFLWService")
// 要安裝的 NT 服務名稱
#define SRV_SERVICENAME			    _T("CTICallFLWService")
// 要安裝的 NT 服務的顯示名稱
#define SRV_SERVICEDISPLAYNAME	_T("CTICallFLWService")
#endif

// 服務程序的依賴關系
#define SRV_DEPENDENCIES				_T("\0\0")

// 函數定義

void WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv);
void WINAPI service_ctrl(DWORD dwCtrlCode);
void CmdInstallService();
void CmdRemoveService();
void CmdDebugService(int argc, TCHAR ** argv);
BOOL WINAPI ControlHandler ( DWORD dwCtrlType );
void ServiceStart (DWORD dwArgc, LPTSTR *lpszArgv, STODSERROR* pstError);
void ServiceCleanup();
void ServiceStop();

#endif // !defined(AFX_SERVICE_H__40095FEA_5F70_11D4_B3A2_0080C88FC26A__INCLUDED_)
