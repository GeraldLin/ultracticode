//////////////////////////////////////////////////////////////
//
// odsError.cpp
//
//		和錯誤處理相關的函數
//
//
//////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "odsError.h"

//-------------------------------------------------------------
//	用缺省的語言取得系統錯誤碼對應的錯誤信息( 格式 1 )
//-------------------------------------------------------------
void odsGetSysErrMsg( DWORD dwErrCode, CString& strMsg )
{
	LANGID dwDefaultLanguageID;		//系統缺省的語言ID
	TCHAR strBuf[ LEN_ERR_MSG + 1 ];
	memset( strBuf, 0, sizeof(TCHAR) * ( LEN_ERR_MSG + 1 ) );

	dwDefaultLanguageID = GetSystemDefaultLangID();
	
	DWORD dwReturnCode = FormatMessage
				(
				 FORMAT_MESSAGE_FROM_SYSTEM |
				 FORMAT_MESSAGE_MAX_WIDTH_MASK,	//Flags
				 NULL,							//No message source
				 dwErrCode,						//Message ID
				 dwDefaultLanguageID,			//Language ID
				 strBuf,						//Pointer of message buffer
				 LEN_ERR_MSG,					//Message buffer size
				 NULL							//No arguments
				);

	if( dwReturnCode == 0 )
		strMsg = _T("取得系統錯誤信息失敗，或沒有相應的錯誤信息!");
	else
		strMsg = strBuf;
}

//-------------------------------------------------------------
//	用缺省的語言取得系統錯誤碼對應的錯誤信息( 格式 2 )
//-------------------------------------------------------------
void odsGetSysErrMsg( DWORD dwErrCode, LPTSTR strBuf, int lenMsg )
{
	LANGID dwDefaultLanguageID;		//系統缺省的語言ID
	if (strBuf == NULL) 
		return;

	memset( strBuf, 0, lenMsg );

	dwDefaultLanguageID = GetSystemDefaultLangID();
	
	DWORD dwReturnCode = FormatMessage
				(
				 FORMAT_MESSAGE_FROM_SYSTEM |
				 FORMAT_MESSAGE_MAX_WIDTH_MASK,	//Flags
				 NULL,							//No message source
				 dwErrCode,						//Message ID
				 dwDefaultLanguageID,			//Language ID
				 strBuf,						//Pointer of message buffer
				 lenMsg,						//Message buffer size
				 NULL							//No arguments
				);

	if( dwReturnCode == 0 )
		lstrcpyn( strBuf, _T("取得系統錯誤信息失敗，或沒有相應的錯誤信息!"), lenMsg );	
}

//-------------------------------------------------------------
// 在錯誤信息中填充錯誤代碼和錯誤信息，strMsg為NULL時只填充錯誤代碼
//-------------------------------------------------------------
void odsMakeError( STODSERROR* pstError, DWORD nCode, LPCTSTR strMsg )
{
	if (pstError == NULL)
		return;
	pstError->nCode = nCode;
	if (strMsg != NULL)
		lstrcpyn( pstError->strMsg, strMsg, LEN_ERR_MSG );
}

//-------------------------------------------------------------
// 按指定的方式，用錯誤信息來填充錯誤信息結構
//
// uFlag 可以取的值有：
//		FORMAT_WITH_SYSTEM						- 取系統錯誤信息
//												- ( pstError->nCode 將被置換成系統錯誤代碼 )
//		FORMAT_WITH_STRING						- 自己生成錯誤信息
//												- ( pstError->nCode 不變 )
//		FORMAT_WITH_SYSTEM_AND_STRING			- 將系統錯誤信息和自己生成的錯誤信息結合起來
//												- ( pstError->nCode 將被置換成系統錯誤代碼 )
// 參數 lpszFormat, ... 的定義同 printf 中的格式串定義
//-------------------------------------------------------------
void odsFormatError( STODSERROR* pstError, UINT uFlag, LPCTSTR lpszFormat, ...)
{ 
	TCHAR lpszTempBuffer[LEN_ERR_MSG + 1];
	TCHAR lpszTempSystemBuffer[LEN_ERR_MSG + 1];
	
	va_list arg_list;

	if (pstError == NULL)
		return;

	// pstError->nCode = 0;
	pstError->strMsg[0] = _T('\0');

	lpszTempBuffer[0] = _T('\0');
	lpszTempSystemBuffer[0] = _T('\0');

	if (uFlag & FORMAT_WITH_STRING)
	{
		if (lpszFormat != NULL)
		{
			va_start(arg_list, lpszFormat);
			_vsntprintf(lpszTempBuffer, LEN_ERR_MSG, lpszFormat, arg_list);
			lpszTempBuffer[LEN_ERR_MSG] = _T('\0');
			va_end(arg_list);
		}
		if (uFlag & FORMAT_WITH_SYSTEM)			
			lstrcat(lpszTempBuffer, _T(":"));
	}

	if (uFlag & FORMAT_WITH_SYSTEM)
	{
		pstError->nCode = GetLastError();
		odsGetSysErrMsg( pstError->nCode, lpszTempSystemBuffer, LEN_ERR_MSG );				
	}

	if ( lstrlen( lpszTempBuffer ) + lstrlen( lpszTempSystemBuffer ) > LEN_ERR_MSG )
		lpszTempSystemBuffer[LEN_ERR_MSG - lstrlen(lpszTempBuffer)] = _T('\0');	
	lstrcat(lpszTempBuffer, lpszTempSystemBuffer);	

	lstrcpyn(pstError->strMsg, lpszTempBuffer, LEN_ERR_MSG);
}
