//---------------------------------------------------------------------------
#include "stdafx.h"
#include "msgfifo.h"

//---------------------------------------------------------------------------
unsigned short CMsgfifo::Write(US ClientId, US MsgId, const char *MsgBuf)
{
	  if(IsFull()) //full
			return 0;

    pMsgs[Tail].ClientId = ClientId;
    pMsgs[Tail].MsgId = MsgId;
    strncpy(pMsgs[Tail].MsgBuf, MsgBuf, 2048);

	  if(++Tail >= Len)
         Tail=0;
	  return 1;
}

unsigned short CMsgfifo::Read(US &ClientId, US &MsgId, char *MsgBuf)
{  	
  if(IsEmpty())
	  return 0;//Buf Null
	
  ClientId = pMsgs[Head].ClientId;
	MsgId = pMsgs[Head].MsgId;
  strcpy(MsgBuf, pMsgs[Head].MsgBuf);

	DiscardHead();
	return 1;  
}
unsigned short CMsgfifo::Read(VXML_RECV_MSG_STRUCT *RecvMsg)
{
  if(IsEmpty())
	  return 0;//Buf Null
	
  RecvMsg->ClientId = pMsgs[Head].ClientId;
	RecvMsg->MsgId = pMsgs[Head].MsgId;
  strcpy(RecvMsg->MsgBuf, pMsgs[Head].MsgBuf);

	DiscardHead();
	return 1;  
}

