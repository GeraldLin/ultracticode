//---------------------------------------------------------------------------
#ifndef __SessionH__
#define __SessionH__
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#define MAX_EVENT_VAR_NUM 32
 //事件變量下標定義
#define 	UC_HangOn		    0 //掛機事件 edit 2007-09-15
#define 	UC_OnEvent 		  1 //通用事件
#define   UC_MsgEvent		  2 //通信處理消息響應事件(65535表示未返回事件)(OnSuccess|OnFail)
#define   UC_PlayEvent	  3 //放音收碼事件(65535表示未返回事件)(OnPlaying|OnPlayEnd|OnRecvDTMF|OnRecvASR|OnPlayError)
#define   UC_TTSEvent		  4 //TTS事件(65535表示未返回事件)(OnTTSing|OnTTSEnd|OnTTSError)
#define   UC_ASREvent		  5 //ASR事件(65535表示未返回事件)(OnASRing|OnASREnd|OnASRError)
#define   UC_RecordEvent	6 //錄音事件(65535表示未返回事件)(OnRecording|OnRecordEnd|OnRecordError)
#define   UC_CalloutEvent	7 //呼出事件(65535表示未返回事件)(OnOutCalling|OnOutRing|OnOutBusy|OnOutTimeout|OnOutUNN|OnOutFail|OnOutAnswer|OnTranTalk)
#define   UC_ACDEvent			8 //坐席分配事件(65535表示未返回事件)(OnACDWait|OnACDRing|OnACDBusy|OnACDTimeOut|OnACDAns|OnACDFail)
#define   UC_DBEvent			9 //數據庫操作事件(65535表示未返回事件)(OnDBSuccess|OnDBFail|OnDBBOF|OnDBEOF)
#define   UC_AGMSGEvent		10 //座席消息事件(65535表示未返回事件)(OnAGMsg)
#define   UC_DBGWMSGEvent	11 //外部數據網關消息事件(65535表示未返回事件)(OnDBGWMsg)
#define   UC_SelfEvent		12 //自定義事件

#define   UC_Timer0Event  13
#define   UC_Timer1Event  14
#define   UC_Timer2Event  15
#define   UC_Timer3Event  16
#define   UC_Timer4Event  17

#define   UC_DialDTMFEvent 18 //內線分機撥后續按鍵事件(65535表示未返回事件)(OnDBGWMsg)
#define   UC_FaxEvent			19 //傳真操作事件(=通用事件)(65535表示未返回事件)(OnsFaxing|OnsFaxend|OnsFaxFail|OnrFaxing|OnrFaxend|OnrFaxFail)
#define   UC_ToneDetectedEvent	20 //檢測到信號音事件(65535表示未返回事件)(OnToneDetected)

#define   UC_LinkEvent    21
#define   UC_FlashEvent   22
#define   UC_CfcEvent			23 //會議操作事件(=通用事件)(65535表示未返回事件)(OnCfcFirst|OnCfcTalk|OnCfcListen|OnCfcTalkFail|OnCfcListenFail|OnCfcOne2Many|OnCfcMany2One|OnCfcBusy2Idle)

#define MAX_SESSION_NUM   960 //最大會話資源個數
  
//會話結構定義
class CSession
{
private:  
  //嵌套子流程的定義
  US ReturnFlwNo[MAX_FLW_RETURN_LEVEL]; //執行子流程返回后執行的流程序號
  US ReturnFlwId[MAX_FLW_RETURN_LEVEL]; //
  UC ReturnCallId[MAX_FLW_RETURN_LEVEL]; //流程調用標志(0-內部調用 1-外部調用)
  US ReturnCmdAddr[MAX_FLW_RETURN_LEVEL]; //執行子流程返回后執行的指令地址
  UC ReturnLevel;

	unsigned int Waiting; //>0表示當指令等待執行中,防止catch指令頻繁執行
public:	
  US FlwNo; //流程序號
  US FlwId; //流程標志(針對在線加載)

  UC MutexId; //互斥變量(當為1時指令繼續執行，直到釋放該變量)
  
  UC DBGwId; //當前選擇的數據網關
  UC StartBillId; //啟動計費標志(0-未啟動 1-以啟動)
  US SqlFlwNo; //調用SQL時的流程序號
  US SqlFlwId; //流程標志
  US RecordNo; //記錄指鐘
  US DBQueryResult; //查詢操作結果：0-未操作 1-正在操作 2-成功操作 3-操作失敗
  US DBFieldResult; //取值結果：0-未操作 1-正在操作 2-成功操作 3-操作失敗
  int DBTimer; //數據庫操作計時器

  FILE *Ftext; //文本文件句柄
  US RWTextResult; //文本文件操作結果：0-未操作 1-正在操作 2-成功操作 3-操作失敗
  US CurrCmdAddr; //當前執行的指令地址

  UC RecvHangonId; //已收到掛機消息
  bool Disp; //跟蹤session活動	
  
  bool bWaiting; //進入catch或switch指令標志 //edit 2009-06-15
  int WaitingCount; //等待計時器 //edit 2009-06-15

public:  //流程系統變量
  UL SerialNo; //會話序列號(十六進制高4位為客戶端號，低4位為會話編號)
	UC CallInOut; //呼入/呼出
  UC FaxSndRcv; //傳真收發方向 （0-無 1-接收 2-發送）
	UC ChnType; //通道類型
	US ChnNo; //通道號
	
  UC ChHWType; //通道硬件類型
	US ChHWNo; //硬件通道序號
  UC SeatType; //該通道綁定的坐席類型

  US RouteNo; //呼入中繼的路由號
  
  CStringX CallerNo;//主叫號碼
  UC CallerType; //主叫號碼類型
  CStringX CalledNo;//被叫號碼
  CStringX DialDTMF; //內線分機撥打的號碼
  UC CalledType; //被叫號碼類型
  CStringX OrgCallerNo;//原主叫號碼
  CStringX OrgCalledNo;//原被叫號碼
  
  time_t BillTime; //開始計費時間
  time_t RelTime; //釋放時間
	time_t CallTime; //呼叫時間
  time_t AnsTime; //應答時間
  
  UL TimerCount[MAX_TIMER_NUM]; //定時器記數器(單位秒),定時遞減，當為0時不再遞減，當為0時表示時間到,并將對應的TimerId置為0
  
  CStringX SeatNo;  //坐席號
  US WorkerNo;  //話務員工號
  US GroupNo; //話務員組號
  
  CStringX RecvDtmf; //DTMF收碼緩沖
  CStringX AsrBuf; //asr語音識別結果

  CStringX ErrorMsg; //錯誤消息

  US DBRecordRows; //返回操作數據表影響的行數

  UL GWSerialNo; //外部數據網關發來的呼叫序列號
  CStringX GWAccount; //外部數據網關發來的賬號
  US GWMsgType; //外部數據網關發來的數據類型
  CStringX GWMsg; //外部數據網關發來的數據消息
  
  US AGMsgType; //對應的電腦坐席發來的消息類型
  CStringX AGMsg; //對應的電腦坐席發來的消息
  US AGPhoneType; //電腦坐席轉接的號碼類型: 1-外線號碼 2-坐席分機號 3-話務員工號
  CStringX AGTranPhone; //電腦坐席轉接的號碼
  CStringX AGTranParam; //電腦坐席傳遞給流程的附加參數
  US AGTranConfNo; //電腦坐席轉接的會議號
  US AGTranIVRId; //轉接的ivr標志
  US AGReturnId; //是否返回原服務坐席標志
  US BandAGFirst; //是否需要先呼叫綁定坐席通道
  US CallType; //呼叫類型(0-普通呼叫 1-直接呼入 2-直接呼出 3-轉移呼入 4-代接呼入 5-強插監聽呼出)
  US AnswerId; //已應答標志(1-是 0-否)

  US SMSType; //收到的短消息類型
  CStringX SMSMsg; //收到的短消息內容

  UL DialSerialNo; //callout,callseat呼出序列號(HHMMSS+SessionId),沒有呼出時為0
  CStringX DialParam; //callout,callseat,或由電腦坐席呼出時傳遞的附加參數param

  CStringX CDRSerialNo; //詳細話單號
  CStringX RecdRootPath; //坐席錄音文件根路徑
  CStringX RecdFileName; //坐席錄音文件名
  CStringX RemoteFaxSID; //遠端傳真標識號

  CStringX DataBaseType; //數據庫類型：SQLSERVER，ORACLE，SYBASE，MYSQL，ACCESS，ODBC，NONE
  US SwitchType; //硬件平臺類型：0：板卡模式 1：AVAYA-IPO系列 2：AVAYA-S系列 3: Alcatel-OmniPCX

public:	
  UC EventVars[MAX_EVENT_VAR_NUM]; //所有事件變量
  CStringX PriVar[MAX_PRI_NUM];  //每個session的局部變量,通過下標訪問

public:
  CSession(UL clientid,US sessionid);
  ~CSession();
  
  void CloneTo(CSession &Session,US FirstCmdAddr,UC chntype=0,US chnno=0,const char *callerno="", const char *calledno="",UC inout=1) const; //復制內容到新session(不復制sessionid)
  
	void Init_PriVar_InitData( const CFlw &CurFlw);

	void Init(
					bool disp,
					UC chntype,US chnno, UC callinout,
					const CStringX &caller,
					const CStringX &called,
					const CStringX &orgcalled,
					
					UL dialserialno,const CStringX &dialparam,
					
					US flwno,US flwid,US FirstCmdAddr=0
					);

  void SetWaitNum(unsigned int num) //設置當前指令等待周期
  {Waiting=num;}
  bool IsCheckWaiting() //如果等待中，變量-1
  //{return Waiting?Waiting--;:false;}
  {
    if (Waiting>0)
    {
      Waiting--;
      return true;
    }
    else
    {
      return false;
    }
  }
  
  UL GetFlw_CmdAddr(); //獲取當前的流程號/地址組合
  
  US GetSessionId()
  {return (US)SerialNo;}
  	
  const CRunFlw &GetRunFlw()const; //返回執行的流程管理
  CFlw &GetFlw()const ; //返回執行的流程
  int GetCmdAddrByCmdIdName(const char *id);
  
 	US GetFuncNo() const ; /*業務腳本功能服務號*/
  UC GetFuncGroup() const; /*業務腳本功能服務組號*/
public:
	int PushCall(UC SubCallID,US SubFlwNo,US SubFlwId,UL SubFormAddr); //壓棧準備調用子form
	int PopReturn();  //出棧返回
	
	void ClearStack(); //由于需要結束本session,清除引用
	  

  void ClearAllEvents();//清除所有事件
	void SetOnHangOnEvent(); //設置掛機事件
	void SetOnDBFailEvent(); //設置數據庫失敗事件
	void InitDBEvent(int timecount=15); //設置數據庫開始操作前的事件狀態
	void InitIVREvent(); //設置IVR開始操作前的事件狀態
  void CreateMutex() {MutexId=1;};
  void ReleaseMutex() {MutexId=0;};
  bool isMutex() {return MutexId==1;}
  void SetDBGwId(UC gwid) {DBGwId=gwid;};
  UC GetDBGwId() {return DBGwId;}
};


class CSessionIdMng //管理SessionId分配，實現先釋放的先分配策略
{
	unsigned int Head;
	unsigned int Tail;
	US Buf[MAX_SESSION_NUM]; //實際保存 MAX_SESSION_NUM-1個數字
public:
	CSessionIdMng();
  bool IsAllSessionBusy();
	US Alloc(); //分配一個，返回0失敗
	void Free(US Id);//釋放一個,注意沒有檢查，外部必須正確釋放
};

class CSessionMng
{
private:
  //客戶端號
  UL ClientId;
  unsigned int UsedNum; //分配的數目
  CSession *pPriVarDatas[MAX_SESSION_NUM];
	CSessionIdMng SessionIdMng;
public:
	  //其他參數
  US RestLines; //限制呼叫線數 0: 不限制 >0限制
  //路由空閑中繼數
  int RouteIdelTrkNum[MAX_ROUTE_NUM];

  CStringX DataBaseType; //數據庫類型：SQLSERVER，ORACLE，SYBASE，MYSQL，ACCESS，ODBC，NONE
  US SwitchType; //硬件平臺類型：0：板卡模式 1：AVAYA-IPO系列 2：AVAYA-S系列 3: Alcatel-OmniPCX
  US RecSysState; //第三方錄音系統登錄狀態：0-斷開，1-已登錄

public:
	CSessionMng();
	virtual ~CSessionMng();
	void Clear();

 	CSession &operator[](unsigned int nIndex) 
	{return *pPriVarDatas[nIndex];}
  
  bool IsSessionNoAvail(unsigned short SessionId);//檢查session是否合法(SessionId==0也非法)
  //獲取使用的Session數目
	unsigned int GetUsedSessionNum()
	{	return UsedNum; }

public:
  //讀配置文件
  void ReadIni(char *filename);
  void SetClientId(UC nClientId);

  //強制復位會話通道
  void Free_SessionId(US SessionId);
  ////////////////////////////////
  //定時器計數1s調用一次
  void Timer1s();
  void MainLoop();
  
  bool IsAllSessionBusy()
  {return SessionIdMng.IsAllSessionBusy();}

  //取空閑的會話序列號,沒有找到返回0,否則返回SessionId
  US Alloc_Idel_SessionId( );

  //根據通道號取會話標志號,沒有找到返回0,否則返回SessionId
  US Get_SessionId_By_Chn(UC ChnType, US ChnNo);

	void pCurrentFindSession(  US CmdAddr,    CH *AttrValue, 
			US MaxNum, const CH *NumVarName, const CH *StartVarName);


public:
  void StopAutoFlw(US flwno); //停止自動流程的會話
	void SetAllHangOnEvent(); //設置所有通道掛機事件 
	void SetAllDBFailEvent(); //設置所有通道數據庫失敗事件
	
  void LocaDisp_AllSessions();//本地顯示所有在線session
};

class CLogerStatusSet
{
public:
  short nLoginId; //0-斷開 1-連接 2-已登錄

  unsigned char LogerClientId;
  
  short nGetLoadFLW;
  short nGetFLWMsg;
public:
  CLogerStatusSet()
  {
    nLoginId = 0;
    LogerClientId = 0;
    
    nGetLoadFLW = 1;
    nGetFLWMsg = 0;
  }
  virtual ~CLogerStatusSet(){}
  
};
//---------------------------------------------------------------------------
#endif

