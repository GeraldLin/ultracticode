//
//主處理函數部分
//

#include "stdafx.h"
#include "odsLog.h"
#include "vardef.h"
#include "main.h"
#include "mainfunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#define CONSOLE_RUN_MODE

//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-流程指令 4-變量跟蹤 5-內部調試信息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...)
{
  if (MsgType != 0 && g_nSendTraceMsgToLOG == false && g_bServiceDebugId == false)
  {
    if (g_bLogId == false && g_bSaveId == false) return;
    if ((MsgType == 1 || MsgType == 2) && g_bCommId == false) return;
    if (MsgType == 3 && g_bCmdId == false) return;
    if (MsgType == 4 && g_bVarId == false) return;
    if (MsgType == 5 && g_bDebugId == false) return;
  }
  
  char buf[2048], msgtype[16], buf1[4096];
  char LogFile[256];
  FILE	*Ftrace;	//跟蹤日志文件
  SYSTEMTIME sysTime;
  
  va_list		ArgList;
  va_start(ArgList, lpszFormat);
  _vsnprintf(buf, 2048, lpszFormat, ArgList);
  va_end (ArgList);
  CString dispbuf;
  
  switch (MsgType)
  {
  case 0:
    strcpy(msgtype, "ALRM: ");
    break;
  case 1:
    strcpy(msgtype, "RECV: ");
    break;
  case 2:
    strcpy(msgtype, "SEND: ");
    break;
  case 3:
    strcpy(msgtype, "CMDS: ");
    break;
  case 4:
    strcpy(msgtype, "VARS: ");
    break;
  case 5:
    strcpy(msgtype, "DEBG: ");
    break;
  default:
    strcpy(msgtype, "REST: ");
    break;
  }
  if (g_nSendTraceMsgToLOG == true || MsgType == 0)
  {
    sprintf(buf1, "%s >> %s%s", MyGetNow(), msgtype, buf);
    for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
    {
      if (LogerStatusSet[nLOG].nLoginId == 2 && (LogerStatusSet[nLOG].nGetFLWMsg == 1  || MsgType == 0))
        g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_ongetflwtracemsg, buf1);
    }
  }
  if (g_bSaveId == TRUE || MsgType == 0)
  {
    GetLocalTime(&sysTime);
    
    sprintf(LogFile, "%s\\FLW_%04d%02d%02d%02d%d.log", g_szLogPath, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute/10);
    Ftrace = fopen(LogFile, "a+t");
    if (Ftrace != NULL)
    {
      fprintf(Ftrace, "%s >> %s%s\n", MyGetNow(), msgtype, buf);
    }
    if (Ftrace != NULL)
    {
      fclose(Ftrace);
      Ftrace = NULL;
    }
  }
  if (g_bServiceDebugId == TRUE)
  {
    printf("%s >> %s%s\n", MyGetNow(), msgtype, buf);
    return;
  }
#ifdef CONSOLE_RUN_MODE
  printf("%s >> %s%s\n", MyGetNow(), msgtype, buf);
#endif
}
//-----------------------------------------------------------------------------
void OnCFGReceiveData(unsigned short remoteid,unsigned short msgtype,
                     const unsigned char *buf,
                     unsigned long len) //need define in main.cpp
{
  if (msgtype == 0x5001)
  {
    ((unsigned char *)buf)[len] = 0;
    MyTrace(1, (char *)buf);
    ProcCFGMsg(msgtype, (char *)buf);
  }
}

void OnCFGLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  g_TcpServer.SetCFGLinked();
  MyTrace(0, "Connect CFGserver success!!!");
  SendLoginCFGMsg();
}

void OnCFGClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  g_TcpServer.SetCFGUnlink();
  MyTrace(0, "Disconnect CFGserver!!!");
}

void OnIVRLogin(unsigned short serverid,unsigned short clientid)     //tcllinkc.dll callback function
{
  //int i,j;
  g_TcpServer.SetIVRLinked();	
  //與服務器TCP連接成功

  SendLoginMsg();
  g_RunFlwMng.Send_AccessNo_To_IVR();
  g_RunFlwMng.ReRunAutoRunFlw();
  MyTrace(0, "Connect IVRserver success!!!");
  #ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, "Connect IVRserver success!!!", EVENTLOG_INFORMATION_TYPE );
  #endif
  SendAllFLWSessionsToAllLOG();
  SendTotalSessionsToAllLOG();
  g_bAlarmId = false;
}

void OnIVRClose(unsigned short serverid,unsigned short clientid)    //tcllinkc.dll callback function
{
  g_TcpServer.SetIVRUnlink();	
	
  g_SessionMng.SetAllHangOnEvent(); //設置為掛機事件發生，等待流程自己掛掉
  MyTrace(0, "Disconnect IVRserver!!!");
  #ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, "Disconnect IVRserver!!!", EVENTLOG_WARNING_TYPE );
  #endif
  g_bAlarmId = true;
}

void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //tcllinkc.dll callback function
{
  ((unsigned char *)buf)[len] = 0;
  if (g_Msgfifo.Write(remoteid, msgtype, (char *)buf) == 0)
    MyTrace(0, "Recv Buffer overflow!");
  if ((msgtype&0xFF) == MSG_onexecsql && g_bDispIVRSQLId == false)
    return;
  MyTrace(1, (char *)buf);
}

void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //need define in main.cpp
{
  ((unsigned char *)buf)[len] = 0;
  if (g_Msgfifo.Write(remoteid, msgtype, (char *)buf) == 0)
    MyTrace(0, "Recv Buffer overflow!");
  
  if (g_bCommId == false)
    return;
  unsigned short msgid = msgtype&0xFF;
  if (msgid != MSG_ondbquery && msgid != MSG_ondbcmd)
  {
    MyTrace(1, (char *)buf);
    return;
  }
  if (g_bDispIVRSQLId == false && msgid == MSG_ondbcmd)
  {
    return;
  }
  
  CH *pos1, *pos2, AttrValue[MAX_VAR_DATA_LEN];
  int nLen;
  
  pos1 = strstr((char *)buf, " cmdaddr='");
  if (pos1)
  {
    pos2 = strstr(pos1, "' ");
    if (pos2)
    {
      nLen = pos2 - pos1;
      if (nLen >= MAX_VAR_DATA_LEN)
      {
        nLen = MAX_VAR_DATA_LEN-1;
      }
      memset(AttrValue, 0, MAX_VAR_DATA_LEN);
      strncpy(AttrValue, pos1, nLen);
      UL Flw_CmdAddr = atol(AttrValue);
      US FlwNo = (UC)(Flw_CmdAddr>>24);
      if (FlwNo>=0 && FlwNo<MAX_FLOW_NUM)
      {
        if ((g_RunFlwMng[FlwNo].DispLogId == 1 && (msgtype&0xFF) == MSG_ondbquery) 
          || (g_bDispIVRSQLId == true && (msgtype&0xFF) == MSG_ondbcmd))
          MyTrace(1, (char *)buf);
      }
    }
  }
}
	
void OnDBLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
	g_TcpServer.SetDBLinked();
  g_bAlarmId = false;
  MyTrace(0, "Connect DBserver success!!!");
  #ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, "Connect DBserver success!!!", EVENTLOG_INFORMATION_TYPE );
  #endif
}

void OnDBClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  g_TcpServer.SetDBUnlink();
  g_SessionMng.SetAllDBFailEvent(); //設置所有通道數據庫失敗事件，等待流程自己掛掉
  g_bAlarmId = true;
  MyTrace(0, "DisConnect DBserver!!!");
  #ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, "DisConnect DBserver!!!", EVENTLOG_WARNING_TYPE );
  #endif
}

void OnGWReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //need define in main.cpp
{
  ((unsigned char *)buf)[len] = 0;
  if (g_Msgfifo.Write(remoteid, msgtype, (char *)buf) == 0)
    MyTrace(0, "Recv Buffer overflow!");
  MyTrace(1, (char *)buf);
}
	
void OnGWLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
	for (int i = 0; i < MAX_GW_NUM; i ++)  
  {
	  if (serverid == g_TcpServer.GetGWServerId(i))
    {
      g_TcpServer.SetGWLinked(i);
      g_bAlarmId = false;
      MyTrace(0, "GWserver Id=%d connect success!!!", i);
      #ifndef CONSOLE_RUN_MODE
      odsAddToSystemLog( SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "GWserver Id=%d connect success!!!", i );
      #endif
      break;
    }
  }
}

void OnGWClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
	for (int i = 0; i < MAX_GW_NUM; i ++)  
  {
	  if (serverid == g_TcpServer.GetGWServerId(i))
    {
      g_TcpServer.SetGWUnlink(i);
      g_bAlarmId = true;
      g_SessionMng.SetAllDBFailEvent(); //設置所有通道數據庫失敗事件，等待流程自己掛掉
      MyTrace(0, "GWserver Id=%d connect break!!!", i);
      #ifndef CONSOLE_RUN_MODE
      odsAddToSystemLog( SRV_APPNAME, EVENTLOG_WARNING_TYPE, "GWserver Id=%d connect break!!!", i );
      #endif
      break;
    }
  }
}

void OnLOGLogin(unsigned short serverid,unsigned short clientid)     //tcllinkc.dll callback function
{
  UC NodeType, NodeId;
  g_TcpServer.GetClientID(clientid, NodeType, NodeId);
  if (NodeType == NODE_LOG)
  {
    for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
    {
      if (LogerStatusSet[nLOG].nLoginId == 0 && LogerStatusSet[nLOG].LogerClientId == 0)
      {
        LogerStatusSet[nLOG].nLoginId = 1;
        LogerStatusSet[nLOG].LogerClientId = NodeId;
        CheckLOGTraceMsgId();
        MyTrace(0, "LOGLogin nLOG=%d ClientId=%d", nLOG, NodeId);
        #ifndef CONSOLE_RUN_MODE
        odsAddToSystemLog(SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "CONFIG[%d] Login", NodeId);
        #endif
        break;
      }
    }
  }
}
void OnLOGReceiveData(unsigned short remoteid,unsigned short msgtype,
                      const unsigned char *buf,
                      unsigned long len) //tcllinkc.dll callback function
{
  ((unsigned char *)buf)[len] = 0;
  //printf("recv log %s\n", buf);
  if (g_Msgfifo.Write(remoteid, msgtype, (char *)buf) == 0)
    MyTrace(0, "Recv Buffer overflow!");
  MyTrace(1, (char *)buf);
}
void OnLOGClose(unsigned short serverid,unsigned short clientid)    //tcllinkc.dll callback function
{
  UC NodeType, NodeId;
  g_TcpServer.GetClientID(clientid, NodeType, NodeId);
  if (NodeType == NODE_LOG)
  {
    for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
    {
      if (LogerStatusSet[nLOG].LogerClientId == NodeId)
      {
        LogerStatusSet[nLOG].nLoginId = 0;
        LogerStatusSet[nLOG].LogerClientId = 0;
        MyTrace(0, "LOGLogout nLOG=%d ClientId=%d", nLOG, NodeId);
        CheckLOGTraceMsgId();
        #ifndef CONSOLE_RUN_MODE
        odsAddToSystemLog(SRV_APPNAME, EVENTLOG_WARNING_TYPE, "CONFIG[%d] Logout", NodeId);
        #endif
      }
    }
  }
}

//處理接收到的消息
void ProcRecvMsg()
{
	US count = 0, ClientId, MsgId, ClientType, ClientNo;
  static CH MsgBuf[2048];

  while(!g_Msgfifo.IsEmpty() && count < 128) //一次最多處理128條信息
  {
    if (g_Msgfifo.Read(ClientId, MsgId, MsgBuf) != 0)
    {
      //MsgId = MsgId & 0xFF;
      ClientType = ClientId>>8;
      ClientNo = ClientId & 0xFF;
      RcvMsg.SetMsgBufId(MsgId & 0xFF,(char *)MsgBuf);  //裝載到Cstring中
      if((MsgId>>8)==MSGTYPE_IVRXML)
      {
        Proc_Msg_From_IVRServer(RcvMsg);
      }
      else if((MsgId>>8)==MSGTYPE_DBXML)
      {
        Proc_Msg_From_DBServer(RcvMsg);
      }
      else if((MsgId>>8)==MSGTYPE_IVRLOG)
      {
        //printf("recv log %s\n", MsgBuf);
        Proc_Msg_From_LOG(RcvMsg);
      }
      else
      {
        MyTrace(0, "Recv MsgType error!");
      }
    }
    count ++;
  }
}

void APPLoop() 
{
	static time_t oldt=0;
	
	time_t newt;
	
	time(&newt);
	if(newt!=oldt)
	{
		oldt=newt; //每秒一次
	
		g_SessionMng.Timer1s();

	}
		
	g_SessionMng.MainLoop();
}
int Init()
{
  char szTemp[32];
  TCHAR szFull[_MAX_PATH];
  TCHAR szDrive[_MAX_DRIVE];
  TCHAR szDir[_MAX_DIR];
  
  LANGID seatlangid = GetSystemDefaultLangID();
  if (seatlangid == 0x0804)
  {
    g_LangID = 1; //簡體中文
  } 
  else if (seatlangid == 0x0404 || seatlangid == 0x0c04 || seatlangid == 0x1004)
  {
    g_LangID = 2; //繁體中文
  }
  else
  {
    g_LangID = 3; //美國英文
  }
  
  GetModuleFileName(NULL, szFull, sizeof(szFull)/sizeof(TCHAR));
  _tsplitpath(szFull, szDrive, szDir, NULL, NULL);
  _tcscpy(szFull, szDrive);
  _tcscat(szFull, szDir);
  strcpy(g_szAppPath, szFull);
  
  memset(g_szRootPath, 0, MAX_PATH_FILE_LEN);
  strncpy(g_szRootPath, g_szAppPath, strlen(g_szAppPath)-12);

  sprintf(g_szUnimeINIFileName, "%s\\unimeconfig.ini", g_szRootPath);
  sprintf(g_szServiceINIFileName, "%sFLWSERVICE.ini", g_szAppPath);
  
  if (MyCheckFileExist(g_szServiceINIFileName) == 0)
  {
    g_nINIFileType = 1;
  }
  else
  {
    g_nINIFileType = GetPrivateProfileInt("QUARKCALL","INIType", 0, g_szServiceINIFileName);
  }
  if (g_nINIFileType == 1)
  {
    sprintf(g_szServiceINIFileName, g_szUnimeINIFileName);
  }

  //讀記錄日志參數設置
  if (g_nINIFileType == 0)
  {
    strcpy(szTemp, "LOG");
  }
  else
  {
    strcpy(szTemp, "FLWLOG");
  }
  GetPrivateProfileString(szTemp, "LogPath", "", g_szLogPath, 128, g_szServiceINIFileName);
  if (strlen(g_szLogPath)==0)
    sprintf(g_szLogPath, "%smsg", g_szAppPath);
  CreateAllDirectories((CString)g_szLogPath);
  if (GetPrivateProfileInt(szTemp,"SaveId", 0, g_szServiceINIFileName) == 1)
  {
    g_bSaveId = true;
  }
  if (GetPrivateProfileInt(szTemp,"CommId", 0, g_szServiceINIFileName) == 1)
  {
    g_bCommId = true;
  }
  if (GetPrivateProfileInt(szTemp,"CmdId", 0, g_szServiceINIFileName) == 1)
  {
    g_bCmdId = true;
  }
  if (GetPrivateProfileInt(szTemp,"VarId", 0, g_szServiceINIFileName) == 1)
  {
    g_bVarId = true;
  }
  if (GetPrivateProfileInt(szTemp,"DebugId", 0, g_szServiceINIFileName) == 1)
  {
    g_bDebugId = true;
  }
  if (GetPrivateProfileInt(szTemp,"AlartId", 0, g_szServiceINIFileName) == 1)
  {
    g_bAlartId = true;
  }
  if (GetPrivateProfileInt(szTemp,"SQLId", 0, g_szServiceINIFileName) == 1)
  {
    g_bDispIVRSQLId = true;
  }
  (g_bCommId==true || g_bCmdId==true || g_bVarId==true || g_bDebugId==true) ? g_bSaveId = true : g_bSaveId = false;
  
  //g_GData.ReadIni(g_szServiceINIFileName);
  //g_SessionMng.ReadIni(g_szServiceINIFileName);
  
  if (g_nINIFileType == 0)
  {
    strcpy(szTemp, "PATH");
  }
  else
  {
    strcpy(szTemp, "FLWPATH");
  }
  GetPrivateProfileString(szTemp, "FlwPath", "", g_szFlwPath, MAX_PATH_LEN, g_szServiceINIFileName);
 	if (strlen(g_szFlwPath)==0)
    sprintf(g_szFlwPath, "%soml", g_szAppPath);
  GetPrivateProfileString(szTemp, "IniPath", "", g_szIniPath, MAX_PATH_LEN, g_szServiceINIFileName);
 	if (strlen(g_szIniPath)==0)
    sprintf(g_szIniPath, "%sini", g_szAppPath);
  GetPrivateProfileString(szTemp, "DatPath", "", g_szDatPath, MAX_PATH_LEN, g_szServiceINIFileName);
 	if (strlen(g_szDatPath)==0)
    sprintf(g_szDatPath, "%sdb", g_szAppPath);

  MyTrace(0, "FlwIniFile=%s FlwPath=%s IniPath=%s DatPath=%s", 
    g_szServiceINIFileName, g_szFlwPath, g_szIniPath, g_szDatPath);

  g_nDBTimeOut = GetPrivateProfileInt("DBSERVER","DBTimeOut", 15, g_szServiceINIFileName);
  g_nGWTimeOut = GetPrivateProfileInt("DBGW1","GWTimeOut", 30, g_szServiceINIFileName);

  g_nParserAccessCodeType = GetPrivateProfileInt("FLW","ParserAccessCodeType", 1, g_szServiceINIFileName); //2016-03-31
  
#ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "Init Msg: FLW FlwPath=%s", g_szFlwPath);
#endif

  if (g_nINIFileType == 0)
  {
    if(!g_TcpServer.ReadIni(g_szServiceINIFileName))
    {
      return 1;
    }     
  }
  else
  {
    if(!g_TcpServer.ReadWebSetupIni(g_szServiceINIFileName))
    {
      return 1;
    }     
  }
  
  g_GData.SetClientId((UC)g_TcpServer.ClientId&0xFF);
  g_SessionMng.SetClientId((UC)g_TcpServer.ClientId&0xFF);
  g_RunFlwMng.AutoLoadFlw(g_szServiceINIFileName);//在session初始化后調用

  if (g_TcpServer.ConnectServer() )
  {
    //printf("Run g_TcpServer fail\n");
    return 2;
  }
#ifndef CONSOLE_RUN_MODE
  odsAddToSystemLog( SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "Init Msg: init success");
#endif
  return 0;
}
void SendLoginCFGMsg()
{
  char msg[512];
  sprintf(msg, "<onlogin nodetype='%d' nodeid='%d'/>", NODE_XML, g_TcpServer.ClientId&0xff);
  g_TcpServer.CFGSendMessage(CFGMSG_login, msg);
}
void ProcCFGMsg(unsigned short MsgId, const char *msg)
{
  CXMLRcvMsg CfgMsg;

  CfgMsg.SetMsgBufId(MsgId, (char *)msg);
  if(0!=CfgMsg.ParseRevMsgWithCheck(g_FlwRuleMng.CFGSRVMsgRule[MsgId&0xFF]))
  {
    return;
  }
  int iniparamflag = atoi(CfgMsg.GetAttrValue(2).C_Str());
  switch (iniparamflag)
  {
  case 2000: //保存日志總開關
    if (atoi(CfgMsg.GetAttrValue(6).C_Str()) == 0)
    {
      g_bCommId = false;
      g_bCmdId = false;
      g_bVarId = false;
      g_bDebugId = false;
      g_bSaveId = false;
    }
    else
    {
      g_bCommId = true;
      g_bCmdId = true;
      g_bVarId = true;
      g_bDebugId = true;
      g_bSaveId = true;
    }
  	break;
  case 2001: //保存通信消息
    if (atoi(CfgMsg.GetAttrValue(6).C_Str()) == 0)
    {
      g_bCommId = false;
    }
    else
    {
      g_bCommId = true;
    }
    (g_bCommId==true || g_bCmdId==true || g_bVarId==true || g_bDebugId==true) ? g_bSaveId = true : g_bSaveId = false;
    break;
  case 2002: //保存流程指令消息
    if (atoi(CfgMsg.GetAttrValue(6).C_Str()) == 0)
    {
      g_bCmdId = false;
    }
    else
    {
      g_bCmdId = true;
    }
    (g_bCommId==true || g_bCmdId==true || g_bVarId==true || g_bDebugId==true) ? g_bSaveId = true : g_bSaveId = false;
    break;
  case 2003: //保存變量跟蹤消息
    if (atoi(CfgMsg.GetAttrValue(6).C_Str()) == 0)
    {
      g_bVarId = false;
    }
    else
    {
      g_bVarId = true;
    }
    (g_bCommId==true || g_bCmdId==true || g_bVarId==true || g_bDebugId==true) ? g_bSaveId = true : g_bSaveId = false;
    break;
  case 2004: //保存內部調試消息
    if (atoi(CfgMsg.GetAttrValue(6).C_Str()) == 0)
    {
      g_bDebugId = false;
    }
    else
    {
      g_bDebugId = true;
    }
    (g_bCommId==true || g_bCmdId==true || g_bVarId==true || g_bDebugId==true) ? g_bSaveId = true : g_bSaveId = false;
    break;
  }
}
