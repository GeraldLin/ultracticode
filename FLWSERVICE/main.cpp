//
// service.cpp - 服務程序框架
//

#include "stdafx.h"
#include "main.h"
#include "odsService.h"
#include "odsError.h"
#include "odsLog.h"
#include "extern.h"
#include "mainfunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CWinApp theApp;

static SERVICE_STATUS_HANDLE g_sshStatusHandle;
static BOOL g_bDebug = FALSE;
static DWORD g_dwCurrentState;

//
// 主函數，如不帶參數，則以服務方式運行；
// 也可以帶以下參數而以命令行方式運行
//
// options:
//		-install -remove -debug
//
//		-install 表示在 NT 系統中安裝服務
//		-remove 表示在 NT 系統中刪除服務
//		-debug 表示以命令行方式運行此程序，以便于調試
//
void _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{		
		odsAddToSystemLog( SRV_APPNAME, ODS_MSG_INITMFC, EVENTLOG_ERROR_TYPE );
		return;
	}

	// 參數判斷
	if ( (argc > 1) && ( *argv[1] == _T('-') || *argv[1] == _T('/') ) )
    {
        if ( _tcsicmp( _T("install"), argv[1] + 1 ) == 0 )
        {
            CmdInstallService();
        }
        else if ( _tcsicmp( _T("remove"), argv[1] + 1 ) == 0 || _tcsicmp( _T("uninstall"), argv[1] + 1 ) == 0)
        {
            CmdRemoveService();
        }
        else if ( _tcsicmp( _T("debug"), argv[1] + 1 ) == 0 )
        {
          g_bServiceDebugId = true;  
          g_bDebug = TRUE;
          CmdDebugService(argc, argv);
        }
        else
        {
            goto dispatch;
        }
        return;
    }

    // 如果沒有參數，則以服務方式運行
dispatch:
	SERVICE_TABLE_ENTRY dispatchTable[] =
    {
        { SRV_SERVICENAME, (LPSERVICE_MAIN_FUNCTION)service_main },
        { NULL, NULL }
    };

    if (!StartServiceCtrlDispatcher(dispatchTable))
	{
        odsAddToSystemLog( SRV_APPNAME, ODS_MSG_STARTSVCDISP, EVENTLOG_ERROR_TYPE );
		return;
	}
}

void WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv)
{
	STODSERROR err;
	err.nCode = 0;

    // register our service control handler:
    //
    g_sshStatusHandle = RegisterServiceCtrlHandler( SRV_SERVICENAME, service_ctrl );

    if (!g_sshStatusHandle)
	{
		odsAddToSystemLog( SRV_APPNAME, ODS_MSG_REGSVCHCTRL, EVENTLOG_ERROR_TYPE );
        return;
	}

    // report the status to the service control manager.
    //
	g_dwCurrentState = SERVICE_START_PENDING;
    if (!odsReportStatusToSCMgr( g_sshStatusHandle, 
			g_dwCurrentState, 0, 5000, NULL ))
        goto cleanup;

    ServiceStart( dwArgc, lpszArgv, &err );

cleanup:

	ServiceCleanup();

    // try to report the stopped status to the service control manager.
    //
	g_dwCurrentState = SERVICE_STOPPED;
    odsReportStatusToSCMgr( g_sshStatusHandle, 
                            g_dwCurrentState, err.nCode, 0, NULL );

    return;
}

void WINAPI service_ctrl(DWORD dwCtrlCode)
{
    // Handle the requested control code.
    //
    switch(dwCtrlCode)
    {
        // Stop the service.
        //
        case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN:

			g_dwCurrentState = SERVICE_STOP_PENDING;
            odsReportStatusToSCMgr(g_sshStatusHandle, g_dwCurrentState, 0, 5000, NULL);
            ServiceStop();
            return;

        // Update the service status.
        //
        case SERVICE_CONTROL_INTERROGATE:
            break;

        // invalid control code
        //
        default:
            break;

    }    
	odsReportStatusToSCMgr(g_sshStatusHandle, g_dwCurrentState, 0, 5000, NULL);
}

void CmdInstallService()
{
    TCHAR szPath[512];
	STODSERROR err;

    if ( GetModuleFileName( NULL, szPath, 512 ) == 0 )
    {
        _tprintf(_T("Unable to install %s - can not get moudle file name\n"), SRV_SERVICEDISPLAYNAME );
        return;
    }

    if (!odsInstallService( NULL, SRV_SERVICENAME, szPath, SRV_SERVICEDISPLAYNAME, 
							SERVICE_AUTO_START, SRV_DEPENDENCIES, &err) )
	{
		_tprintf(_T("(%d)%s\n"), err.nCode, err.strMsg );
		return;
	}
}

void CmdRemoveService()
{
	STODSERROR err;

	if (!odsRemoveService( NULL, SRV_SERVICENAME, 30000, &err ))
	{
		_tprintf(_T("(%d)%s\n"), err.nCode, err.strMsg );
		return;
	}    
}

void CmdDebugService(int argc, TCHAR ** argv)
{
	STODSERROR err;

    _tprintf(_T("Debugging %s.\n"), SRV_SERVICEDISPLAYNAME);

    SetConsoleCtrlHandler( ControlHandler, TRUE );

    ServiceStart( (DWORD) argc, argv, &err );

	ServiceCleanup();
}

BOOL WINAPI ControlHandler ( DWORD dwCtrlType )
{
    switch( dwCtrlType )
    {
        case CTRL_BREAK_EVENT:  // use Ctrl+C or Ctrl+Break to simulate
        case CTRL_C_EVENT:      // SERVICE_CONTROL_STOP in debug mode
            _tprintf(_T("Stopping %s.\n"), SRV_SERVICEDISPLAYNAME);
            ServiceStop();
            return TRUE;
            break;
    }
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////
// ServiceStart, ServiceCleanup and ServiceStop


static HANDLE hStopFLWEvent;

void ServiceStart (DWORD dwArgc, LPTSTR *lpszArgv, STODSERROR* pstError)
{	
	// 服務初始化

	// 每一個初始化步驟前，都向服務控制器報告下一步操作最長要多長時間完成，
	// 以便服務控制器判斷服務程序是否已失去響應
	if (!g_bDebug)
	{
		g_dwCurrentState = SERVICE_START_PENDING;
		if (!odsReportStatusToSCMgr(g_sshStatusHandle, g_dwCurrentState, 0, 5000, NULL ))            
			return;
	}

	// …………………………………………
	// 初始化終止事件
#ifdef QUARKCALL_PLATFORM
	hStopFLWEvent = CreateEvent( NULL, FALSE, FALSE, "STOP_QuarkCallFLWService" );
#else
  hStopFLWEvent = CreateEvent( NULL, FALSE, FALSE, "STOP_CTICallFLWService" );
#endif
	

	// 進入服務程序的主流程

	// 向服務控制器報告服務程序已經處于正常運行的狀態
	if (!g_bDebug)
	{
		g_dwCurrentState = SERVICE_RUNNING;
		if (!odsReportStatusToSCMgr(g_sshStatusHandle, g_dwCurrentState, 0, 5000, NULL ))            
			 return;
	}

	// ………………………………………… 

	// 下面是主流程
	// 并檢查退出標記，如果已發出終止服務的命令，就停止循環
	static int AlarmCount = 0;

  int SystemStartId = Init();
  if (SystemStartId == 0)
  {
    odsAddToSystemLog( SRV_APPNAME, "start success", EVENTLOG_INFORMATION_TYPE );
  }
  else
  {
    odsAddToSystemLog( SRV_APPNAME, "start fail", EVENTLOG_WARNING_TYPE );
  }
	// 服務程序的主流程通常都是含有某個特定終止條件的無限循環結構
	while (1)
	{
    if (SystemStartId == 0)
    {
      g_TcpServer.AppMainLoop();
      ProcRecvMsg();
      APPLoop();
    }
    if (g_bAlarmId == true && g_bAlartId == true)
    {
      if (AlarmCount ++ > 20)
      {
        Beep( 1000, 200 );
        AlarmCount = 0;
      }
    }
		// 如果 hStopEvent 被激活，則停止循環
		if ( WaitForSingleObject( hStopFLWEvent, 0 ) != WAIT_TIMEOUT )
			break;
    Sleep(20);
	}    
  g_TcpServer.AppCloseAll();
  Sleep(100);
  g_SessionMng.Clear();
  odsAddToSystemLog( SRV_APPNAME, "stop", EVENTLOG_INFORMATION_TYPE );
	return;
}

void ServiceCleanup()
{
	// 做服務終止時的系統清理工作

	// 每一個清理步驟前，都向服務控制器報告下一步操作最長要多長時間完成，
	// 以便服務控制器判斷服務程序是否已失去響應
	if (!g_bDebug)
	{
		g_dwCurrentState = SERVICE_STOP_PENDING;
		odsReportStatusToSCMgr(g_sshStatusHandle, g_dwCurrentState, 0, 5000, NULL );
	}    

	// …………………………………………
	CloseHandle( hStopFLWEvent );
}

void ServiceStop()
{
    // 停止服務，在這里激活終止主流程的特定條件

	// …………………………………………
	SetEvent( hStopFLWEvent );
}

//--------------------------------------------------------------------------
