//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"
//---------------------------------------------------------------------------

CGData::CGData()
{
  int i;//, j;
  

	//初始化接入字冠入口
	for (i = 0; i < MAX_ACCESSCODES_NUM; i++)
	{
		PreCodeNo[i].state = 0;
	//	memset(PreCodeNo[i].PreCaller, 0, MAX_TELECODE_LEN);
	//	memset(PreCodeNo[i].PreCalled, 0, MAX_TELECODE_LEN);
		PreCodeNo[i].MinLen = 0xFF;
		PreCodeNo[i].MaxLen = 0xFF;
		PreCodeNo[i].FuncNo = 0xFFFF;
		PreCodeNo[i].FlwNo = 0xFFFF;
		PreCodeNo[i].prior = 0xFFFF;
		PreCodeNo[i].next = 0xFFFF;
	}
	PreCodeNo[0].state = 1;
	PreCodeNo[0].prior = 0;
	PreCodeNo[0].next = 0xFFFF;
	ClientId = 0;
	
}
CGData::~CGData()
{

}
//讀客戶端號
void CGData::ReadIni(char *filename)
{
	ClientId =  GetPrivateProfileInt("TCPLINK", "FLWServerID", 1, filename);
}

void CGData::SetClientId(UC nClientId)
{
  ClientId = nClientId;
}

//增加接入字冠
int CGData::AddPreCode(char *Caller, const char *Called, short MinLen, short MaxLen, UC FuncGroup, US FuncNo, US FlwNo)
{
	int i;
	UI p = 0, prior = 0, next = PreCodeNo[0].next;

	if (strlen(Called) == 0) return 1;
	for (i = 1; i < MAX_ACCESSCODES_NUM; i ++)
	{
		if (PreCodeNo[i].state == 0 && p == 0)
			p = i;
		if (PreCodeNo[i].PreCalled.Compare(Called) == 0)
		{
			//接入碼已經存在
			return 1;
		}
	}
  if (p == 0)
  {
      //接入字冠緩沖區已滿
      return 1;
  }
	if (p < MAX_ACCESSCODES_NUM)
	{
		PreCodeNo[p].state = 1;
		PreCodeNo[p].PreCalled=Called;
	//	printf("%d,%d\n",strlen(Called),PreCodeNo[p].PreCalled.GetLength());
		PreCodeNo[p].MinLen = MinLen;
		PreCodeNo[p].MaxLen = MaxLen;
		PreCodeNo[p].FuncNo = FuncNo;
		PreCodeNo[p].FlwNo = FlwNo;
		
		while (next < MAX_ACCESSCODES_NUM)
		{
      if (strlen(Called) >= PreCodeNo[next].PreCalled.GetLength())
			{
				PreCodeNo[prior].next = p;
				PreCodeNo[next].prior = p;

				PreCodeNo[p].prior = prior;
				PreCodeNo[p].next = next;
				return 0;
			}
			prior = next;
			next = PreCodeNo[prior].next;
		}
		PreCodeNo[prior].next = p;
		PreCodeNo[p].prior = prior;
		PreCodeNo[p].next = 0xFFFF;
		return 0;
	}
	return 1;
}
//刪除接入字冠
int CGData::DelPreCode(char *Caller, const char *Called)
{
	US prior = 0, next = PreCodeNo[0].next;

  if (strlen(Called) == 0) return 1;
  while (next < MAX_ACCESSCODES_NUM)
  {
    if (PreCodeNo[next].PreCalled.Compare(Called) == 0)
    {
      PreCodeNo[prior].next = PreCodeNo[next].next;

      PreCodeNo[next].state = 0;
      PreCodeNo[next].PreCalled.Empty();
      PreCodeNo[next].MinLen = 0;
      PreCodeNo[next].MaxLen = 0;
      PreCodeNo[next].FlwNo = 0xFFFF;
      PreCodeNo[next].prior = 0xFFFF;
      PreCodeNo[next].next = 0xFFFF;
      return 0;
    }
    prior = next;
    next = PreCodeNo[prior].next;
  }
  return 1;
}
//根據主被叫號碼取流程序號
int CGData::Get_FlwNo_By_CalledNo(const CStringX &CallerNo, const CStringX &CalledNo, US &FlwNo)
{
  UI next = PreCodeNo[0].next;

  //printf("search called=%s ... ",CalledNo.C_Str());

  if (CalledNo.GetLength() == 0) 
  	return 1;
  while (next < MAX_ACCESSCODES_NUM)
  {  
  	unsigned int len=PreCodeNo[next].PreCalled.GetLength();//edwin 是否需要判斷len==0?
    
    //printf("i=%d,len=%d,code=%s\n",next,len,PreCodeNo[next].PreCalled.C_Str());
  	
    if(len>0)
    if(PreCodeNo[next].PreCalled.CompareN(CalledNo.C_Str(),len) == 0)
    {
      FlwNo = PreCodeNo[next].FlwNo;
      return 0;
    }
    next = PreCodeNo[next].next;
  }
 // printf("failed\n");
  return 1;
}

//查詢呼叫限制表(0-沒有記錄)
int CGData::Query_RestTel(UC FuncGroup, US FuncNo, const CStringX &CallerNo, const CStringX &CalledNo)
{
  //UC sqlbuf[600];
  int RestType = 0;

  //if (DBType == DB_NONE) return 0;
  /*
  if (WaitForSingleObject(Mutex_QuerySqls, 50) == WAIT_OBJECT_0)
  {
      //sprintf( sqlbuf, "Select RestType from ivr_RestTel where OpenFlag=1 and (FuncGroup=%d or FuncGroup=0) and (FuncNo=%d or FuncNo=0) and (CallerCode='*' or CallerCode = substring('%s',1,len(CallerCode))) and (CalledCode='*' or CalledCode = substring('%s',1,len(CalledCode))) order by FuncGroup desc,FuncNo desc,CallerCode desc,CalledCode desc",
      if (DBType == DB_ORACLE)
      {
         sprintf( sqlbuf, "Select RestType from ivr_RestTel where OpenFlag=1 and (FuncGroup=%d or FuncGroup=0) and (FuncNo=%d or FuncNo=0) and (CallerCode='*' or CallerCode = substr('%s',1,length(CallerCode))) and (CalledCode='*' or CalledCode = substr('%s',1,length(CalledCode))) order by FuncGroup desc,FuncNo desc,CallerCode desc,CalledCode desc",
           FuncGroup, FuncNo, CallerNo, CalledNo);
      }
      else if (DBType == DB_MYSQL)
      {
         sprintf( sqlbuf, "Select RestType from ivr_RestTel where OpenFlag=1 and (FuncGroup=%d or FuncGroup=0) and (FuncNo=%d or FuncNo=0) and (CallerCode='*' or CallerCode = left('%s',length(CallerCode))) and (CalledCode='*' or CalledCode = left('%s',length(CalledCode))) order by FuncGroup desc,FuncNo desc,CallerCode desc,CalledCode desc",
           FuncGroup, FuncNo, CallerNo, CalledNo);
      }
      else
      {
         sprintf( sqlbuf, "Select RestType from ivr_RestTel where OpenFlag=1 and (FuncGroup=%d or FuncGroup=0) and (FuncNo=%d or FuncNo=0) and (CallerCode='*' or CallerCode = left('%s',len(CallerCode))) and (CalledCode='*' or CalledCode = left('%s',len(CalledCode))) order by FuncGroup desc,FuncNo desc,CallerCode desc,CalledCode desc",
           FuncGroup, FuncNo, CallerNo, CalledNo);
      }
      try
      {
          PubQuery1->SQL->Clear();
          PubQuery1->SQL->Add((char *)sqlbuf );
          PubQuery1->Open();
      }
      catch ( ... )
      {
          ReleaseMutex(Mutex_QuerySqls);
          return 0;
      }
      PubQuery1->First();
      if ( !PubQuery1->Eof )
      {
          RestType = PubQuery1->FieldByName("RestType")->AsInteger;
      }
      PubQuery1->Close();
      ReleaseMutex(Mutex_QuerySqls);
  }
  */
  return RestType;
}

int CGData::Send_AccessNo_To_IVR(UC settype, const CH *accessno, UC minlen, UC maxlen)
{
		CStringX Msg; 
    Msg.Format("<setaccessno sessionid='%d' settype='%d' accessno='%s' minlen='%d' maxlen='%d'/>",
        0x02000000 | ((ClientId << 16) & 0x00FF0000), settype, accessno, minlen, maxlen);
       
    g_TcpServer.IVRSendMessage(MSG_setaccessno, Msg);
    return 0;
}


int CXMLRcvMsg::ParseRevMsgWithCheck(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule)
{
  int result=ParseRevMsg(MsgRule);
  
  if(result==0)//消息屬性參數值檢查,并替換相應的宏	
		for (int i = 0; i < AttrNum; i ++)
		{
				result=g_FlwRuleMng.Parse_Msg_Attr_Value(MsgRule.Attribute[i],AttrValues[i]);
  			if (result)
  				break;
  	}		
  return result;
}

int CXMLRcvMsg::ParseSndMsgWithCheck(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule)
{
	int result=ParseSndMsg(MsgRule);
  
   return result;	
  
}

void CXMLSndMsg::SetHeadToBuf(const char *msgname,CSession *pSession) //在buf中設置頭部
{
	Buf.Format("<%s sessionid='%ld' cmdaddr='%d' chantype='%d' channo='%d'", 
			msgname, pSession->SerialNo, pSession->GetFlw_CmdAddr(), pSession->ChnType, pSession->ChnNo);
}

