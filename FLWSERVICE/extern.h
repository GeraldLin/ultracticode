//-----------------------------------------------------------------------------
#ifndef __externdef__h_
#define __externdef__h_
//-----------------------------------------------------------------------------

extern int g_LangID;

extern bool	g_bLogId;
extern bool	g_bSaveId;
extern bool	g_bCommId;
extern bool	g_bCmdId;
extern bool	g_bVarId;
extern bool	g_bDebugId;
extern bool g_bAlarmId;
extern bool	g_bAlartId;
extern bool	g_bDispIVRSQLId;

extern bool g_bServiceDebugId;
extern int g_nParserAccessCodeType;

extern int g_nDBTimeOut; //數據庫操作超時時長s
extern int g_nGWTimeOut; //數據網關操作超時時長s

extern CMsgfifo g_Msgfifo;
extern CTCPServer g_TcpServer; //通信消息類
extern CRunFlwMng g_RunFlwMng; //加載的流程類
extern CSessionMng g_SessionMng; //session管理類
extern CGData 		g_GData; //配置、參數、變量數據類
extern CFlwRuleMng g_FlwRuleMng; //流程規則類

extern CSession *pCurrent; //當前處理的Session指針
extern const RRunCmd *pCurrentCmd; //指向當前Session執行的指令
extern int preCmdOperId;
extern CStringX ErrVar; //全局用錯誤信息變量

extern int g_nINIFileType;

extern char g_szServiceINIFileName[MAX_PATH_FILE_LEN];
extern char g_szUnimeINIFileName[MAX_PATH_FILE_LEN];

extern char g_szRootPath[MAX_PATH_LEN];
extern char g_szAppPath[MAX_PATH_LEN]; //執行文件路徑
extern char g_szFlwPath[MAX_PATH_LEN]; //流程文件路徑
extern char g_szIniPath[MAX_PATH_LEN]; //配置文件路徑
extern char g_szDatPath[MAX_PATH_LEN]; //數據文件路徑

extern int AuthMaxTrunk;
extern int AuthMaxSeat;
extern int AuthMaxIVR;
extern int AuthCardType;
extern int LocaCardType;
extern CStringX MatchCaller;
extern CStringX MatchCalled;

extern int RunDays;
extern int RestDays;
extern bool AuthKeyId;
extern bool RestDayId;

extern bool g_nSendTraceMsgToLOG;
extern CLogerStatusSet LogerStatusSet[MAX_LOGNODE_NUM];
//-----------------------------------------------------------------------------
#endif