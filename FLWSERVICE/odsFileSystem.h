////////////////////////////////////////////////////////////////////////
//
// odsFileSystem.h
//
//		ODS 有關文件系統操作的定義文件
//
// by Wang Yong Gang, 1999-10-16
//
////////////////////////////////////////////////////////////////////////


#if !defined(_ODS_20_FILESYSTEM_H_)
#define _ODS_20_FILESYSTEM_H_

#include "odsError.h"

////////////////////////////////////////////////////////////////////////
// 類型定義

//用于 odsBrowseDirectory 的回調函數定義
typedef BOOL (*FILEBROWSEFUNC) (LPCTSTR lpszFileName, STODSERROR* pstError );

//目錄屬性結構定義
typedef struct tagSTDIRPROPERTY
{
	 DWORD		dwDirNum;			//包含子目錄的個數
	 DWORD		dwFileNum;			//包含文件的個數
	 __int64	i64Size;			//目錄的大小，以字節為單位
}STDIRPROPERTY;


////////////////////////////////////////////////////////////////////////
// 函數說明

//------------------------------------------------------------
// 刪除路徑名最后的反斜線(格式1)
//------------------------------------------------------------
void odsRemoveLastSlash( LPTSTR lpszDirectory );

//------------------------------------------------------------
// 刪除路徑名最后的反斜線(格式2)
//------------------------------------------------------------
void odsRemoveLastSlash( CString& strDirectory );

//------------------------------------------------------------
// 判斷由路徑名和文件名合成的全稱文件名長度是否
// 超過了系統允許的最大長度
//------------------------------------------------------------
BOOL odsIsFullNameOverflow( LPCTSTR lpszDirectoryName, LPCTSTR lpszFileName );

//------------------------------------------------------------
// 從文件全路徑名中分離出不包含路徑的文件名
//------------------------------------------------------------ 
void odsGetFileNameFromPath( LPCTSTR lpszFileFullPath, LPTSTR lpszFileName );

//------------------------------------------------------------
// 判斷兩個目錄是否在同一個驅動器上
//------------------------------------------------------------
BOOL odsIsDirInSameDriver( LPCTSTR lpszDirectory1, LPCTSTR lpszDirectory2 );

//------------------------------------------------------------
// 取得指定路徑的根目錄
//------------------------------------------------------------
void odsGetRootDir( LPCTSTR lpszDirectoryName, LPTSTR lpszRootDirectory );

//------------------------------------------------------------
// 判斷指定的文件是否存在
//------------------------------------------------------------
BOOL odsIsFileExist( LPCTSTR lpszFileName );

//------------------------------------------------------------
// 判斷指定的目錄是否存在
//------------------------------------------------------------
BOOL odsIsDirExist( LPCTSTR lpszDirectoryName );

//------------------------------------------------------------
// 取指定文件的大小( 返回 0xFFFFFFFF 表示失敗 )，
// lpFileSizeHigh 可以為 NULL
//------------------------------------------------------------
DWORD odsGetFileSize( LPCTSTR lpszFileName, LPDWORD lpFileSizeHigh );

//------------------------------------------------------------
// 達到目錄所在的硬盤剩余空間的大小，失敗則返回負數
//------------------------------------------------------------
__int64 odsGetDiskFreeSpace( LPCTSTR strDir );

//------------------------------------------------------------
// 將目錄中的內容從原始目錄拷貝到目標目錄，且可包含子目錄
//------------------------------------------------------------
BOOL odsCopyDirectory
	( 
		LPCTSTR		lpszSourceDirectory,			// 原始目錄
		LPCTSTR		lpszDestinationDirectory,		// 目的目錄
		BOOL		bIncludeSubDir,					// 是否包含子目錄
		BOOL		bOverwriteExist,				// 是否覆蓋已有的目錄及文件
		STODSERROR* pstError						// 返回錯誤信息
	);

//------------------------------------------------------------
// 刪除指定的目錄
//	bIncludeSubDir = TRUE,  刪除指定的目錄及其所有子目錄（包括所有文件）
//	bIncludeSubDir = FALSE, 僅刪除指定目錄中的文件，不刪除子目錄和子目錄
//							內的文件，也不刪除指定目錄本身。
//------------------------------------------------------------
BOOL odsRemoveDirectory( LPCTSTR lpszDirectoryName, BOOL bIncludeSubDir, STODSERROR* pstError );

//------------------------------------------------------------
// 移動目錄 - bDirect 指明是直接移動還是先復制再刪除
//------------------------------------------------------------
BOOL odsMoveDirectory
		( 
			LPCTSTR lpszSourceDirectory, 
			LPCTSTR lpszDestinationDirectory, 
			BOOL bDirect,
			STODSERROR* pstError 
		);

//------------------------------------------------------------ 
// 設置目錄內所有文件的屬性
//	bIncludeSubDir			是否包含子目錄內的文件
//------------------------------------------------------------ 
BOOL odsSetFileAttributesInDir( LPCTSTR lpszDirectoryName, DWORD dwFileAttributes, BOOL bIncludeSubDir, STODSERROR* pstError );

//------------------------------------------------------------ 
// 取得指定目錄的屬性
//		包括此目錄包含的子目錄的個數，文件的個數，以及目錄以字節為單位的大小
//------------------------------------------------------------ 
BOOL odsGetDirProperty( LPCTSTR lpszDirectoryName, BOOL bIncludeSubDir, 
					 STDIRPROPERTY* lpDirectoryProperty, STODSERROR* pstError );

//------------------------------------------------------------
// 瀏覽目錄及其子目錄中指定類型的文件
// 參數說明:
// 	lpszDirectoryName	要瀏覽的目錄名稱
//	dwFileType			要瀏覽文件的類型，可為以下值或它們的組合
//							FILE_ATTRIBUTE_ARCHIVE 
//							FILE_ATTRIBUTE_COMPRESSED 
//							FILE_ATTRIBUTE_DIRECTORY 
//							FILE_ATTRIBUTE_HIDDEN 
//							FILE_ATTRIBUTE_NORMAL 
//							FILE_ATTRIBUTE_OFFLINE 
//							FILE_ATTRIBUTE_READONLY 
//							FILE_ATTRIBUTE_SYSTEM 
//							FILE_ATTRIBUTE_TEMPORARY 
//	bIncludeSubDir		是否包含子目錄中文件的標志
//							TRUE	包含子目錄中的文件
//							FALSE	不包含子目錄中的文件
//	FileBrowseFunc		回調函數的指針，在瀏覽過程中，瀏覽函數會將找到符合條件的
//						文件名傳給此函數，此回調函數可做相應的操作，不可以為 NULL
//	pstError			錯誤信息結構指針，可以為 NULL
//------------------------------------------------------------ 
BOOL odsBrowseDirectory
		(
			LPCTSTR lpszDirectoryName, 
			DWORD dwFileType, 
			BOOL bIncludeSubDir, 
			FILEBROWSEFUNC FileBrowseFunc, 
			STODSERROR* pstError
		);


#endif // _ODS_20_FILESYSTEM_H_