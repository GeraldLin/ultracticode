//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"
//---------------------------------------------------------------------------
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
	
CSession::CSession(UL clientid,US sessionid)
{
	UL NodeType, NodeId;
	Ftext = 0;
	 NodeType = NODE_XML<<24;
  NodeId = (clientid & 0x00FF) << 16;
  SerialNo = (UL)sessionid | (NodeType | NodeId);
}  
  
CSession::~CSession()
{
	if (Ftext) fclose(Ftext);
}
  
//對自定義的局部變量進行初始化
void CSession::Init_PriVar_InitData( const CFlw &CurFlw)
{
    for (int i = 0; i < MAX_PRI_NUM; i ++)
    {
        PriVar[i]=CurFlw.PriVar[i].Data;
    }
}

void CSession::Init(
					bool disp,
					UC chntype,US chnno, UC callinout,
					const CStringX &caller,
					const CStringX &called,
					const CStringX &orgcalled,
								
					UL dialserialno,const CStringX &dialparam,
								
					US flwno,US flwid,US FirstCmdAddr
					)
{
  int i;

	Waiting=0;
  //edit 2009-06-15
  bWaiting=false;
  WaitingCount=0;
	
  //會話通道局部變量
  Disp=disp;
  
  DialSerialNo = 0;

  DBTimer = 0;
  MutexId = 0;
  DBGwId = 0;

  DBRecordRows = 0;

  RouteNo = 0;
  CallInOut = callinout;
  FaxSndRcv = 0;
 
	RelTime=
  BillTime=
  CallTime=
  AnsTime=time(0);
        
  if(CallInOut==CALL_OUT)          
  	StartBillId=1;
  else            
  	StartBillId = 0;
  
  for ( i = 0; i < MAX_TIMER_NUM; i ++ )
  {
    TimerCount[i] = 0;
  }
  
 	ClearAllEvents();

  DialSerialNo = dialserialno;
  DialParam=dialparam;
  
  RecordNo = 0;
  DBQueryResult = 0;
  DBFieldResult = 0;

  RWTextResult = 0;

  ReturnLevel = 0;
  for ( i = 0; i < MAX_FLW_RETURN_LEVEL; i ++ )
  {
    ReturnFlwNo[i] = 0xFFFF;
    ReturnFlwId[i] = 0;
    ReturnCallId[i] = 0;
    ReturnCmdAddr[i] = 0xFFFF;
  }
	
	ChnType = chntype;
  ChnNo = chnno;
  SeatType = 0;
  RouteNo = 0;

  RecvHangonId = 0;
  CallerNo=caller;
  CallerType = 0;
  CalledNo=called;	
  DialDTMF = "";
  CalledType = 0;
  OrgCallerNo="";
	OrgCalledNo=orgcalled;

  RecvDtmf = "";
  SeatNo = "0";
  WorkerNo = 0;
  GroupNo = 0;
  
  GWSerialNo = 0;
  GWAccount = "";
  GWMsgType = 0;
  GWMsg = "";
  
  AGMsgType = 0;
  AGMsg = "";
  AGPhoneType = 0;
  AGTranPhone = "";
  AGTranParam = "";
  AGTranConfNo = 0;
  AGTranIVRId = 0;
  AGReturnId = 0;
  BandAGFirst = 0;
  
  SMSType = 0;
  SMSMsg = "";

  CDRSerialNo = "";
  RecdRootPath = "";
  RecdFileName = "";
  RemoteFaxSID = "";

  DataBaseType = "MYSQL";
  SwitchType = 0;
	
  FlwNo = flwno;
  FlwId = flwid;

  CallTime=time(0);
  CurrCmdAddr = FirstCmdAddr;
  Init_PriVar_InitData(g_RunFlwMng[FlwNo].Flw[g_RunFlwMng[FlwNo].FlwId]);
  g_RunFlwMng[FlwNo].Flw[g_RunFlwMng[FlwNo].FlwId].Onlines =g_RunFlwMng[FlwNo].Flw[g_RunFlwMng[FlwNo].FlwId].Onlines + 1;

  if(Disp)
 		 DispCmd(this);			 //顯示第一條指令
  
}					

//返回對應的流程
CFlw &CSession::GetFlw()const  
{
	return g_RunFlwMng[FlwNo].Flw[FlwId];
}	
int CSession::GetCmdAddrByCmdIdName(const char *id)
{
  CFlw &CurFlw=GetFlw();
  return CurFlw.GetCmdAddrByCmdIdName(id);
}
const CRunFlw &CSession::GetRunFlw() const //返回執行的流程管理
{
	return g_RunFlwMng[FlwNo];
}
 	
US CSession::GetFuncNo() const  /*業務腳本功能服務號*/
{
  return g_RunFlwMng[FlwNo].FuncNo;
}
	
UC CSession::GetFuncGroup() const /*業務腳本功能服務組號*/
{
	return g_RunFlwMng[FlwNo].FuncGroup;
}	

int CSession::PushCall(UC SubCallID,US SubFlwNo,US SubFlwId,UL SubFormAddr) //壓棧準備調用子form
{
	if (ReturnLevel >= MAX_FLW_RETURN_LEVEL-1)
    return 1;
        
  ReturnCallId[ReturnLevel] = SubCallID;
  ReturnFlwNo[ReturnLevel] = FlwNo;
  ReturnFlwId[ReturnLevel] = FlwId;
  ReturnCmdAddr[ReturnLevel] = g_RunFlwMng[FlwNo].Flw[FlwId].RunCmds[CurrCmdAddr].NextCmdAddr;
  ReturnLevel ++;
  
  FlwNo=SubFlwNo;
  FlwId=SubFlwId;
	CurrCmdAddr = (US)SubFormAddr;
  
  if(SubCallID)
  	g_RunFlwMng[FlwNo].Flw[FlwId].Onlines = g_RunFlwMng[FlwNo].Flw[FlwId].Onlines+1; //外部調用需要增加引用
  return 0;                   
}

int CSession::PopReturn()  //出棧返回
{
	 if (ReturnLevel == 0)
        return 1;
      
   ReturnLevel --;
   
   if(ReturnCallId[ReturnLevel])
   {
  	 if (g_RunFlwMng[FlwNo].Flw[FlwId].Onlines>0)
      g_RunFlwMng[FlwNo].Flw[FlwId].Onlines=g_RunFlwMng[FlwNo].Flw[FlwId].Onlines-1; //外部調用需要減少引用
   }
  
   FlwNo = ReturnFlwNo[ReturnLevel];
   FlwId = ReturnFlwId[ReturnLevel];
   CurrCmdAddr = ReturnCmdAddr[ReturnLevel];   
   return 0;
}
  
UL CSession::GetFlw_CmdAddr() //獲取當前的流程號/地址組合
{
	UL Flw_CmdAddr=(FlwNo<<8) | FlwId;
	Flw_CmdAddr<<=16;
	Flw_CmdAddr |= (UL)CurrCmdAddr ;
	return Flw_CmdAddr;
}  

void CSession::ClearStack() //由于需要結束本session,清除引用
{
  for (unsigned int i = 0 ; i < ReturnLevel; i ++)
  {
    if (ReturnCallId[i] == 1)
    {
      if (g_RunFlwMng[ReturnFlwNo[i]].Flw[ReturnFlwId[i]].Onlines>0)  
        g_RunFlwMng[ReturnFlwNo[i]].Flw[ReturnFlwId[i]].Onlines=g_RunFlwMng[ReturnFlwNo[i]].Flw[ReturnFlwId[i]].Onlines-1;
    }
  }
  if (g_RunFlwMng[FlwNo].Flw[FlwId].Onlines>0)
    g_RunFlwMng[FlwNo].Flw[FlwId].Onlines=g_RunFlwMng[FlwNo].Flw[FlwId].Onlines-1; //當前子form
   
  //theApp.m_StatusDialog->DispStatus(true, this->SerialNo, 0, 0, "", "", 0, "", "");
}


void CSession::CloneTo(CSession &Session,
                       US FirstCmdAddr,
                       UC chntype,US chnno, 
                       const char *callerno,
                       const char *calledno,
                       UC inout) const //復制內容到新session(不復制sessionid)
{
	//UC chntype1;
  //US chnno1;
  UC inout1;
  CStringX callerno1, calledno1;

  /*if (chntype == 0)
  {
    chntype1 = ChnType;
    chnno1 = ChnNo;
  }
  else
  {
    chntype1 = chntype;
    chnno1 = chnno;
  }*/

  if (inout == 0)
    inout1 = CallInOut;
  else
    inout1 = inout;

  if (strlen(callerno) == 0)
    callerno1 = CallerNo;
  else
    callerno1 = callerno;

  if (strlen(calledno) == 0)
    calledno1 = CalledNo;
  else
    calledno1 = calledno;

  Session.Init(
					g_bCmdId,
					chntype, chnno, inout1,
					callerno1,
					calledno1,
					OrgCalledNo,
					DialSerialNo,
					DialParam,
					FlwNo,FlwId,FirstCmdAddr
							);
	for(int i=0;i<MAX_PRI_NUM;i++)
		Session.PriVar[i]=PriVar[i]; //復制變量				

  Session.GWAccount = GWAccount;
  Session.GWMsgType = GWMsgType;
  Session.GWMsg = GWMsg;
  Session.AGMsgType = AGMsgType;
  Session.AGMsg = AGMsg;
  Session.AGPhoneType = AGPhoneType;
  Session.AGTranPhone = AGTranPhone;
  Session.AGTranParam = AGTranParam;
  Session.AGTranConfNo = AGTranConfNo;
  Session.AGTranIVRId = AGTranIVRId;
  Session.AGReturnId = AGReturnId;
  Session.SMSType = SMSType;
  Session.SMSMsg = SMSMsg;
  Session.CDRSerialNo = CDRSerialNo;
  Session.RecdRootPath = RecdRootPath;
  Session.RecdFileName = RecdFileName;
  Session.DataBaseType = DataBaseType;
  Session.SwitchType = SwitchType;
  Session.RemoteFaxSID = RemoteFaxSID;
}

void CSession::ClearAllEvents()//清除所有事件
{
	for(int i=0;i<MAX_EVENT_VAR_NUM;i++)
  	EventVars[i]=WAIT_EVENT;
}

void CSession::SetOnHangOnEvent() //設置掛機事件
{
	 EventVars[UC_HangOn] = OnHangon; //edit 2007-09-15
   RelTime=time(0);
}
	
void CSession::SetOnDBFailEvent() //設置數據庫失敗事件
{
	 EventVars[UC_DBEvent] = OnDBFail;
}

void CSession::InitDBEvent(int timecount) //設置數據庫開始操作前的事件狀態
{
	EventVars[UC_DBEvent] = WAIT_EVENT;
  DBTimer = timecount;
  //EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
  //EventVars[UC_MsgEvent] = WAIT_EVENT; //edit 2007-09-15 del
}
	
void CSession::InitIVREvent() //設置IVR開始操作前的事件狀態
{	
	EventVars[UC_OnEvent] = WAIT_EVENT;
  EventVars[UC_MsgEvent] = WAIT_EVENT;
} 

///////////////////////////////////////////////////////
CSessionIdMng::CSessionIdMng()
{
	for(US i=0;i<MAX_SESSION_NUM;i++)
		Buf[i]=i; //1--MAX_SESSION_NUM-1有效
	Head=1; //從1開始
	Tail=0; //滿狀態
}
bool CSessionIdMng::IsAllSessionBusy()
{
	if(Head==Tail)
		return true;
  else
    return false;
}
US CSessionIdMng::Alloc() //分配一個，返回0失敗
{
	US Id=Buf[Head];
	if(Head==Tail)
		return 0;
	
	if(++Head	>= MAX_SESSION_NUM)
		Head=0;
		

	return 	Id;
}

void CSessionIdMng::Free(US Id)//釋放一個,注意沒有檢查，外部必須正確釋放
{
	Buf[Tail]=Id;
	if(++Tail	>= MAX_SESSION_NUM)
		Tail=0;	
}
   	
////////////////////////////////////////////////////   	
CSessionMng::CSessionMng()
{
  int i;

	UsedNum=0;
	for(i=0;i<MAX_SESSION_NUM;i++)
  {
		pPriVarDatas[i]=0;
  }
	 
  RestLines = 0;
  for (i=0; i<MAX_ROUTE_NUM; i++)
  {
    RouteIdelTrkNum[i] = 1024;
  }

  DataBaseType = "MYSQL";
  SwitchType = 0;
  RecSysState = 0;
}

CSessionMng::~CSessionMng()
{
	Clear();
}

void CSessionMng::Clear()
{	
  for (int i = 0; i < MAX_SESSION_NUM; i ++)
  {
      if (pPriVarDatas[i])
      {          
//          Del_Disp_ChnState(i);
          Free_SessionId(i);   //釋放
      }
  }
 // Onlines = 0;
}  
  
//讀配置文件
void CSessionMng::ReadIni(char *filename)
{
  //需要改從配置文件FLWSERVICE.ini中讀取  
	ClientId = GetPrivateProfileInt("TCPLINK", "FLWServerID", 1, filename);
  RestLines = 0;     
}

void CSessionMng::SetClientId(UC nClientId)
{
  ClientId = nClientId;
  RestLines = 0;
}

US CSessionMng::Get_SessionId_By_Chn(UC ChnType, US ChnNo)
{
	for (int i = 1; i < MAX_SESSION_NUM; i ++)
	{
		if (!pPriVarDatas[i])
			continue;
			
        if (pPriVarDatas[i]->ChnType == ChnType && pPriVarDatas[i]->ChnNo == ChnNo )
        {        	
          return (US)i;
        }
	}
  return 0;
}



//取空閑的會話序列號,沒有找到返回0,否則返回SessionId
US CSessionMng::Alloc_Idel_SessionId( )
{
	US Id=SessionIdMng.Alloc();
		
	if(Id) //分配到了
  {
    if (pPriVarDatas[Id] == 0)
    {       
      pPriVarDatas[Id]=new CSession(ClientId,Id);
      UsedNum++;	 		 
    }
    else
    {
      MyTrace(5, "SessionID conflict can not alloc SessionID");          	
    }
  }
  return Id;
}

//強制復位會話通道數據
void CSessionMng::Free_SessionId(US SessionId)
{
  if(pPriVarDatas[SessionId])
  {
  	pPriVarDatas[SessionId]->ClearStack();
  	SessionIdMng.Free(SessionId); //注意沒有檢查
  	if(pPriVarDatas[SessionId]==pCurrent)
  		pCurrent=0;
  	delete pPriVarDatas[SessionId];
  	pPriVarDatas[SessionId]=0;
  	UsedNum--;
  }
  
  
  else 
  {
  	MyTrace(5, "%s Free SessionId = %d Error",MyGetNow(),SessionId);
	}
 	//printf("%s SessionIdnum= %d!\n",MyGetNow(),UsedNum);
  
  //if(UsedNum	==0 )
 // 	printf("%s All SessionId freed!\n",MyGetNow());
}

bool CSessionMng::IsSessionNoAvail(unsigned short SessionId)//檢查session是否合法(SessionId==0也非法)
{
		bool result=false;//=	(SessionId < MAX_SESSION_NUM)? pPriVarDatas[SessionId]:false;
    if (SessionId < MAX_SESSION_NUM)
    {
      if (pPriVarDatas[SessionId] != NULL)
        result = true;
    }
	//	if(!result)	
	//		printf("I find session == %d is unavaible !!!\n",SessionId);
		//if(	SessionId==0)
		//	printf("!!!SessionId=0\n");
		return  result;
}

void 	CSessionMng::LocaDisp_AllSessions()//本地顯示所有在線session
{
// 	for (int i = 1; i < MAX_SESSION_NUM; i ++)
//   {
//     if(pPriVarDatas[i])
//       theApp.m_StatusDialog->DispStatus(false, pPriVarDatas[i]->SerialNo, 
//         pPriVarDatas[i]->ChnType, pPriVarDatas[i]->ChnNo, 
//         pPriVarDatas[i]->CallerNo.C_Str(), pPriVarDatas[i]->CalledNo.C_Str(),
//         pPriVarDatas[i]->CallInOut, 
//         pPriVarDatas[i]->GetRunFlw().FlwName.C_Str(), ConverTimeToStr(pPriVarDatas[i]->CallTime));
// 
//  	}   		
}

void CSessionMng::Timer1s() //1s
{
    int timer;

    for (int i = 1; i < MAX_SESSION_NUM; i ++)
    {
    	if(!pPriVarDatas[i])
    		continue;
    		
      //edit 2009-06-15
      if (pPriVarDatas[i]->bWaiting == true)
      {
        pPriVarDatas[i]->WaitingCount++;
      }

      if (pPriVarDatas[i]->DBTimer > 0)
      {
          pPriVarDatas[i]->DBTimer -= 1;
          if (pPriVarDatas[i]->DBTimer <= 0)
          {
              pPriVarDatas[i]->SetOnDBFailEvent();
          }
      }

      for (int j = 0; j < MAX_TIMER_NUM; j ++)
      {
          if (pPriVarDatas[i]->TimerCount[j] > 0)
          {
              timer = pPriVarDatas[i]->TimerCount[j];
              pPriVarDatas[i]->TimerCount[j] = timer - 1;
              if ((pPriVarDatas[i]->TimerCount[j]) == 0)
              {
                	pPriVarDatas[i]->EventVars[UC_Timer0Event+j]=OnTimer0Over+j;
              }
          }
      }
    }
}

void CSessionMng::StopAutoFlw(US flwno)//停止自動流程的會話
{		
  for (int i = 1; i < MAX_SESSION_NUM; i ++)
	{
		if(pPriVarDatas[i])
    {
      if(pPriVarDatas[i]->FlwNo == flwno 
        && pPriVarDatas[i]->ChnType == 0 && pPriVarDatas[i]->ChnNo == 0)
			  Free_SessionId(i);
    }
	}
}
	 
void CSessionMng::SetAllHangOnEvent()//設置所有通道掛機事件 
{		
	for (int i = 1; i < MAX_SESSION_NUM; i ++)
	{
		if(pPriVarDatas[i])
			pPriVarDatas[i]->SetOnHangOnEvent();
	}		
}

void CSessionMng::SetAllDBFailEvent() //設置所有通道數據庫失敗事件
{
	for (int i = 1; i < MAX_SESSION_NUM; i ++)
	{
		if(pPriVarDatas[i])
			pPriVarDatas[i]->SetOnDBFailEvent();
	}	
}

void CSessionMng::MainLoop() 
{		
	
	for (int i = 1; i < MAX_SESSION_NUM; i ++)
	{
		if(!pPriVarDatas[i])
			continue;
				
		//if(pPriVarDatas[i]->IsCheckWaiting())
		//	continue; //當前指令正在等待中	
		pCurrent=pPriVarDatas[i];
    for (int j = 0; j < 20; j ++)
    {
      if (pCurrent == NULL) break;
      
      Proc_Cmd(); //執行一條指令
      if (preCmdOperId == CMD_waiting)
        break;
    }
    while (pCurrent != NULL && preCmdOperId != CMD_waiting && pCurrent->isMutex())
    {
      Proc_Cmd(); //執行一條指令
    }
	
	}
  pCurrent=0;
}



//查找符合條件的通道
void CSessionMng::pCurrentFindSession( US CmdAddr,   CH *AttrValue, 
			US MaxNum, const CH *NumVarName, const CH *StartVarName)
{
    CH ReturnExp[MAX_EXP_STRING_LEN];
    CH Result[MAX_EXP_STRING_LEN];
    int LogicResult, FindNum = 0;
    US i;//, fFlwNo;
		
		CSession *pSave=pCurrent;

    for (i = 1; i < MAX_SESSION_NUM; i ++)
    {
    	if (FindNum >= MaxNum) 
    		break;
    		
    	if(!pPriVarDatas[i])	
      	continue;
    	
    	if(pSave==pPriVarDatas[i])
    		continue;
    	   
    	//if(&pCurrent->GetFlw() != &pPriVarDatas[i]->GetFlw()) //edit 2008-06-22 del
    	//  continue;
    	            				
			pCurrent=pPriVarDatas[i];
				
			try	
			{
        Replace_pCurrent_Exp_VarValues(  4, AttrValue, ReturnExp);
        //TRACE("AttrValue=%s ReturnExp=%s\n",AttrValue,ReturnExp);
            if (Compare_Operation(ReturnExp, Result) == 0)
            {
              //TRACE("Result=%s\n",Result);  
              LogicResult = Operation_Logic_Exp(Result);
                if (LogicResult == 1)
                {
                    //找到符合條件的通道
                    pCurrent=pSave;
                    
									try
									{                    
                    Assign_pCurrent_intValue_To_Var(   FindNum, StartVarName, i) ;
                    FindNum ++;
                    //TRACE("FindNum=%d\n",FindNum);
                  }  
                  catch (...)
                  {
                  	
                  }
                  
                }
                else
                  continue;                
            }
            else            
                continue;           
      }    
      catch (...)
  		{
  			continue; //繼續尋找
  		}     
        
    }
    
    pCurrent=pSave;
    
    Assign_pCurrent_intValue_To_Var( NumVarName, FindNum);
}


