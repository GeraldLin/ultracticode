//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

//事件對應的事件變量表
static const unsigned char EventMap[256]=
{
	UC_OnEvent,UC_OnEvent,UC_OnEvent,0,0,0,0,0,0,UC_HangOn,UC_LinkEvent,UC_LinkEvent,UC_LinkEvent,UC_LinkEvent,UC_LinkEvent,0, //edit 2007-09-15
	UC_PlayEvent,UC_PlayEvent,UC_PlayEvent,UC_PlayEvent,UC_PlayEvent,UC_PlayEvent,0,0,0,0,UC_DialDTMFEvent,0,0,0,0,0,
	UC_TTSEvent,UC_TTSEvent,UC_TTSEvent,0,0,0,0,0,0,0,0,0,0,0,0,0,
	UC_ASREvent,UC_ASREvent,UC_ASREvent,0,0,0,0,0,0,0,0,0,0,0,0,0,
	UC_RecordEvent,UC_RecordEvent,UC_RecordEvent,0,0,0,0,0,0,0,0,0,0,0,0,0,
	UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,
	UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,UC_CalloutEvent,
	UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,UC_ACDEvent,0,0,0,0,0,
	UC_DBEvent,UC_DBEvent,UC_DBEvent,UC_DBEvent,0,0,0,0,UC_DBGWMSGEvent,0,0,0,0,0,0,0,
	
  0,UC_Timer0Event,UC_Timer1Event,UC_Timer2Event,UC_Timer3Event,UC_Timer4Event,0,0,0,0,0,0,0,0,0,0,
	UC_FaxEvent,UC_FaxEvent,UC_FaxEvent,UC_FaxEvent,UC_FaxEvent,UC_FaxEvent,UC_FaxEvent,UC_FaxEvent,UC_ToneDetectedEvent,0,0,0,0,0,0,0,
	
  UC_CfcEvent,UC_CfcEvent,UC_CfcEvent,UC_CfcEvent,UC_CfcEvent,UC_CfcEvent,UC_CfcEvent,0,0,0,0,0,0,0,0,0,
	UC_CfcEvent,UC_CfcEvent,UC_CfcEvent,0,0,0,0,0,0,0,0,0,0,0,0,0,
	
  UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,UC_AGMSGEvent,
	
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	
  UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,
	UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,UC_SelfEvent,
	
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};

unsigned int GetEventVarIndexOfEvent(int event )
{
	return EventMap[event & 0xff];
}

//根據事件標志取事件名稱
char *Get_EventName_By_EventId(US EventId)
{
  if (g_LangID == 1)
  {
    switch (EventId)
    {
    //通用操作返回事件
    case OnSuccess:				        return "OnSuccess (成功)"; break;
    case OnFail:					        return "OnFail (失敗或存在)"; break;

    case OnInAnswer:					    return "OnInAnswer (呼入應答)"; break;

    case OnHangon:				        return "OnHangon (通道釋放事件)"; break;
    case OnLinkHangon:				    return "OnLinkHangon (連接通道釋放事件)"; break;
    case OnUnLink:				        return "OnUnLink (連接通道斷開事件)"; break;
    case OnFlash:				          return "OnFlash (坐席通道拍叉簧事件)"; break;

    case OnCallConnected:				  return "OnCallConnected (交換機連接通道事件)"; break;
    case OnCallUnConnected:				return "OnCallUnConnected (交換機斷開通道事件)"; break;

    //放音收碼事件
    case OnPlaying:				        return "OnPlaying (正在放音)"; break;
    case OnPlayEnd:				        return "OnPlayEnd (放音結束)"; break;
    case OnRecvDTMF:				      return "OnRecvDTMF (收到DTMF按鍵)"; break;
    case OnRecvASR:				        return "OnRecvASR (收到識別字符串)"; break;
    case OnPlayError:				      return "OnPlayError (放音錯誤)"; break;
    case OnErrDTMF:				        return "OnErrDTMF (收收到錯誤的DTMF按鍵)"; break;

    case OnDialDTMF:				      return "OnDialDTMF (分機撥后續按鍵事件)"; break;

      //TTS事件
    case OnTTSing:				        return "OnTTSing (正在文本轉語音操作)"; break;
    case OnTTSEnd:				        return "OnTTSEnd (文本轉語音操作結束)"; break;
    case OnTTSError:				      return "OnTTSError (文本轉語音操作錯誤)"; break;

    //ASR事件
    case OnASRing:				        return "OnASRing (正在語音識別操作)"; break;
    case OnASREnd:				        return "OnASREnd (語音識別結束)"; break;
    case OnASRError:				      return "OnASRError (語音識別錯誤)"; break;

    //錄音事件
    case OnRecording:				      return "OnRecording (正在錄音)"; break;
    case OnRecordEnd:				      return "OnRecordEnd (結束錄音)"; break;
    case OnRecordError:			      return "OnRecordError (錄音錯誤)"; break;

    //呼出事件
    case OnOutCalling:			      return "OnOutCalling (正在呼出)"; break;
    case OnOutRing:				        return "OnOutRing (被叫振鈴)"; break;
    case OnOutBusy:				        return "OnOutBusy (被叫占線)"; break;
    case OnOutTimeout:			      return "OnOutTimeout (無人應答或超時)"; break;
    case OnOutUNN:				        return "OnOutUNN (空號)"; break;
    case OnOutFail:				        return "OnOutFail (呼叫失敗)"; break;
    case OnOutAnswer:				      return "OnOutAnswer (呼出應答)"; break;
    case OnTranTalk:				      return "OnTranTalk (轉接成功通話)"; break;
    case OnOutRel:				        return "OnOutRel (呼出通道通話后釋放)"; break;
    case OnOutNoTrk:			        return "OnOutNoTrk (呼出中繼全忙)"; break;
    case OnOutBAR:		            return "OnOutBAR (接入拒絕)"; break;
    case OnOutCGC:		            return "OnOutCGC (設備擁塞)"; break;
    case OnOutADI:		            return "OnOutADI (地址不全)"; break;
    case OnOutSST:		            return "OnOutSST (專用信號音)"; break;
    case OnOutPWROFF:	   	        return "OnOutPWROFF (用戶關機)"; break;
    case OnOutOUTSRV: 		        return "OnOutOUTSRV (用戶不在服務區)"; break;

    //坐席分配事件
    case OnACDWait:			          return "OnACDWait (正在分配)"; break;
    case OnACDRing:			          return "OnACDRing (坐席振鈴)"; break;
    case OnACDBusy:				        return "OnACDBusy (坐席全忙)"; break;
    case OnACDTimeOut:			      return "OnACDTimeOut (無人應答或超時)"; break;
    case OnACDUNN:		        		return "OnACDUNN (空號)"; break;
    case OnACDFail:				        return "OnACDFail (分配失敗)"; break;
    case OnACDAnswer:			        return "OnACDAnswer (坐席應答)"; break;
    case OnACDRefuse:	        		return "OnACDRefuse (拒絕)"; break;
    case OnACDGetSeat:        		return "OnACDGetSeat (返回取的空閑坐席)"; break;
    case OnACDOUTSRV:		          return "OnACDOUTSRV (無空閑坐席)"; break;
    case OnACDTakeover:		        return "OnACDTakeover (坐席接管事件)"; break;

    //數據庫操作事件
    case OnDBSuccess:				      return "OnDBSuccess (數據庫操作成功)"; break;
    case OnDBFail:				        return "OnDBFail (數據庫操作失敗)"; break;
    case OnDBBOF:					        return "OnDBBOF (到第一條記錄)"; break;
    case OnDBEOF:					        return "OnDBEOF (到最后一條記錄)"; break;
    
    //收到外部數據網關消息事件
    case OnDWMsg:        		      return "OnDWMsg (收到外部數據網關消息事件)"; break;

    //定時器事件
    case OnTimerOver:			        return "OnTimerOver (定時器溢出事件)"; break;
    case OnTimer0Over:			      return "OnTimer0Over (定時器0溢出事件)"; break;
    case OnTimer1Over:			      return "OnTimer1Over (定時器1溢出事件)"; break;
    case OnTimer2Over:			      return "OnTimer2Over (定時器2溢出事件)"; break;
    case OnTimer3Over:			      return "OnTimer3Over (定時器3溢出事件)"; break;
    case OnTimer4Over:			      return "OnTimer4Over (定時器4溢出事件)"; break;

    //傳真收發事件
    case OnsFaxing:			          return "OnsFaxing (正在發送)"; break;
    case OnsFaxend:			          return "OnsFaxend (發送結束)"; break;
    case OnsFaxFail:		          return "OnsFaxFail (發送失敗)"; break;
    case OnrFaxing:			          return "OnrFaxing (正在接收)"; break;
    case OnrFaxend:			          return "OnrFaxend (接收結束)"; break;
    case OnrFaxFail:		          return "OnrFaxFail (接收失敗)"; break;
    case OnsFaxNoSignal:		      return "OnsFaxNoSignal (發送傳真時未檢測到對方的信號音)"; break;
    case OnrFaxNoSignal:          return "OnrFaxNoSignal (接收傳真時未檢測到對方的信號音)"; break;

    case OnToneDetected:          return "OnToneDetected (檢測到信號音)"; break;

    //會議操作返回事件
    case OnCfcFirst:			        return "OnCfcFirst (第1個加入會議)"; break;
    case OnCfcJoin:			          return "OnCfcJoin (加入會議通話成功)"; break;
    case OnCfcListen:		          return "OnCfcListen (旁聽會議成功)"; break;
    case OnCfcNotOpen:	          return "OnCfcNotOpen (會議未開通失敗)"; break;
    case OnCfcJoinFail:           return "OnCfcJoinFail (加入會議失敗)"; break;
    case OnCfcListenFail:         return "OnCfcListenFail (旁聽會議失敗)"; break;
    case OnCfcFull:               return "OnCfcFull (人滿失敗)"; break;

    //會議室人數變化事件
    case OnCfcOne2Many:           return "OnCfcOne2Many (會議加入人數從1人變多人)"; break;
    case OnCfcMany2One:           return "OnCfcMany2One (會議加入人數從多人變1人)"; break;
    case OnCfcBusy2Idle:          return "OnCfcBusy2Idle (會議加入人數從忙變有空位)"; break;

    //收到電腦座席消息事件
    case OnAGBlindTran:        		return "OnAGBlindTran (電腦坐席快速轉接電話事件)"; break;
    case OnAGConsultTran:        	return "OnAGConsultTran (電腦坐席協商轉接電話事件)"; break;
    case OnAGConfTran:        		return "OnAGConfTran (電腦坐席會議轉接電話事件)"; break;
    case OnAGStopTran:            return "OnAGStopTran (電腦坐席停止轉接電話事件)"; break;
    case OnAGConfSpeak:        		return "OnAGConfSpeak (電腦坐席指定通道會議發言事件)"; break;
    case OnAGConfAudit:        		return "OnAGConfAudit (電腦坐席指定通道旁聽會議事件)"; break;
    case OnAGTranIVR:        		  return "OnAGTranIVR (電腦坐席將來話轉到IVR事件)"; break;

    default:                      return "OtherEvent (其它事件)"; break;
    }
  }
  else if (g_LangID == 2)
  {
    switch (EventId)
    {
    //??巨�明��Eㄆ?
    case OnSuccess:				        return "OnSuccess (Θ��)"; break;
    case OnFail:					        return "OnFail (?毖?�Z�I)"; break;

    case OnInAnswer:					    return "OnInAnswer (㊣�芚傽�)"; break;

    case OnHangon:				        return "OnHangon (?笵睦�Iㄆ?)"; break;
    case OnLinkHangon:				    return "OnLinkHangon (硈鋇?笵睦�Iㄆ?)"; break;
    case OnUnLink:				        return "OnUnLink (硈鋇?笵耞?ㄆ?)"; break;
    case OnFlash:				          return "OnFlash (???笵?�餖fㄆ?)"; break;

    case OnCallConnected:				  return "OnCallConnected (?傳訣硈鋇?笵ㄆ?)"; break;
    case OnCallUnConnected:				return "OnCallUnConnected (?傳訣耞??笵ㄆ?)"; break;

    //�I�鞶O絏ㄆ?
    case OnPlaying:				        return "OnPlaying (?�I�I��)"; break;
    case OnPlayEnd:				        return "OnPlayEnd (�I�騞���)"; break;
    case OnRecvDTMF:				      return "OnRecvDTMF (Μ�崮TMF�V齡)"; break;
    case OnRecvASR:				        return "OnRecvASR (Μ�淟����Y才﹃)"; break;
    case OnPlayError:				      return "OnPlayError (�I�黼�?)"; break;
    case OnErrDTMF:				        return "OnErrDTMF (ΜΜ�淴�?�YDTMF�V齡)"; break;

    case OnDialDTMF:				      return "OnDialDTMF (?訣擠�A尿�V齡ㄆ?)"; break;

      //TTSㄆ?
    case OnTTSing:				        return "OnTTSing (?�I??鑼?�韞���)"; break;
    case OnTTSEnd:				        return "OnTTSEnd (??鑼?�韞��忠���)"; break;
    case OnTTSError:				      return "OnTTSError (??鑼?�韞��昔�?)"; break;

    //ASRㄆ?
    case OnASRing:				        return "OnASRing (?�I?�鵹��艇���)"; break;
    case OnASREnd:				        return "OnASREnd (?�鵹��蜀���)"; break;
    case OnASRError:				      return "OnASRError (?�鵹��跳�?)"; break;

    //魁�鞶u?
    case OnRecording:				      return "OnRecording (?�I魁��)"; break;
    case OnRecordEnd:				      return "OnRecordEnd (擋�溶蕻�)"; break;
    case OnRecordError:			      return "OnRecordError (魁�黼�?)"; break;

    //㊣�|ㄆ?
    case OnOutCalling:			      return "OnOutCalling (?�I㊣�|)"; break;
    case OnOutRing:				        return "OnOutRing (砆�彰絡c)"; break;
    case OnOutBusy:				        return "OnOutBusy (砆����?)"; break;
    case OnOutTimeout:			      return "OnOutTimeout (?�佽傽�?禬��)"; break;
    case OnOutUNN:				        return "OnOutUNN (�d腹)"; break;
    case OnOutFail:				        return "OnOutFail (㊣��?毖)"; break;
    case OnOutAnswer:				      return "OnOutAnswer (㊣�|萊氮)"; break;
    case OnTranTalk:				      return "OnTranTalk (鑼鋇Θ��?杠)"; break;
    case OnOutRel:				        return "OnOutRel (㊣�|?笵?杠�A睦�I)"; break;
    case OnOutNoTrk:			        return "OnOutNoTrk (㊣�|??�hΓ)"; break;
    case OnOutBAR:		            return "OnOutBAR (鋇��?蕩)"; break;
    case OnOutCGC:		            return "OnOutCGC (?稱局峨)"; break;
    case OnOutADI:		            return "OnOutADI (�H��?�h)"; break;
    case OnOutSST:		            return "OnOutSST (盡?獺腹��)"; break;
    case OnOutPWROFF:	   	        return "OnOutPWROFF (??閩訣)"; break;
    case OnOutOUTSRV: 		        return "OnOutOUTSRV (???�I?叭跋)"; break;

    //????ㄆ?
    case OnACDWait:			          return "OnACDWait (?�I??)"; break;
    case OnACDRing:			          return "OnACDRing (??�絡c)"; break;
    case OnACDBusy:				        return "OnACDBusy (??�hΓ)"; break;
    case OnACDTimeOut:			      return "OnACDTimeOut (?�佽傽�?禬��)"; break;
    case OnACDUNN:		        		return "OnACDUNN (�d腹)"; break;
    case OnACDFail:				        return "OnACDFail (???毖)"; break;
    case OnACDAnswer:			        return "OnACDAnswer (??萊氮)"; break;
    case OnACDRefuse:	        		return "OnACDRefuse (?蕩)"; break;
    case OnACDGetSeat:        		return "OnACDGetSeat (���E�裾Y�d秪??)"; break;
    case OnACDOUTSRV:		          return "OnACDOUTSRV (?�d秪??)"; break;
    case OnACDTakeover:		        return "OnACDTakeover (??鋇恨ㄆ?)"; break;

    //計沮?巨�岡u?
    case OnDBSuccess:				      return "OnDBSuccess (計沮?巨�岡K��)"; break;
    case OnDBFail:				        return "OnDBFail (計沮?巨��?毖)"; break;
    case OnDBBOF:					        return "OnDBBOF (�敓��癟L癘魁)"; break;
    case OnDBEOF:					        return "OnDBEOF (�桮{�A�癟L癘魁)"; break;
    
    //Μ���議鶩p沮呼閩�襴忙u?
    case OnDWMsg:        		      return "OnDWMsg (Μ���議鶩p沮呼閩�襴忙u?)"; break;

    //﹚��竟ㄆ?
    case OnTimerOver:			        return "OnTimerOver (﹚��竟犯�|ㄆ?)"; break;
    case OnTimer0Over:			      return "OnTimer0Over (﹚��竟0犯�|ㄆ?)"; break;
    case OnTimer1Over:			      return "OnTimer1Over (﹚��竟1犯�|ㄆ?)"; break;
    case OnTimer2Over:			      return "OnTimer2Over (﹚��竟2犯�|ㄆ?)"; break;
    case OnTimer3Over:			      return "OnTimer3Over (﹚��竟3犯�|ㄆ?)"; break;
    case OnTimer4Over:			      return "OnTimer4Over (﹚��竟4犯�|ㄆ?)"; break;

    //肚痷Μ只ㄆ?
    case OnsFaxing:			          return "OnsFaxing (?�I只?)"; break;
    case OnsFaxend:			          return "OnsFaxend (只?擋��)"; break;
    case OnsFaxFail:		          return "OnsFaxFail (只??毖)"; break;
    case OnrFaxing:			          return "OnrFaxing (?�I鋇Μ)"; break;
    case OnrFaxend:			          return "OnrFaxend (鋇Μ擋��)"; break;
    case OnrFaxFail:		          return "OnrFaxFail (鋇Μ?毖)"; break;
    case OnsFaxNoSignal:		      return "OnsFaxNoSignal (只?肚痷��?浪代�晙�?�Y獺腹��)"; break;
    case OnrFaxNoSignal:          return "OnrFaxNoSignal (鋇Μ肚痷��?浪代�晙�?�Y獺腹��)"; break;

    case OnToneDetected:          return "OnToneDetected (浪代�桼☆↘�)"; break;

    //?某巨�明��Eㄆ?
    case OnCfcFirst:			        return "OnCfcFirst (材1�R����?某)"; break;
    case OnCfcJoin:			          return "OnCfcJoin (����?某?杠Θ��)"; break;
    case OnCfcListen:		          return "OnCfcListen (�げs?某Θ��)"; break;
    case OnCfcNotOpen:	          return "OnCfcNotOpen (?某????毖)"; break;
    case OnCfcJoinFail:           return "OnCfcJoinFail (����?某?毖)"; break;
    case OnCfcListenFail:         return "OnCfcListenFail (�げs?某?毖)"; break;
    case OnCfcFull:               return "OnCfcFull (�侚e?毖)"; break;

    //?某���阰p跑?ㄆ?
    case OnCfcOne2Many:           return "OnCfcOne2Many (?某�����阰p?1�侀]�O��)"; break;
    case OnCfcMany2One:           return "OnCfcMany2One (?某�����阰p?�O�侀]1��)"; break;
    case OnCfcBusy2Idle:          return "OnCfcBusy2Idle (?某�����阰p?Γ跑Τ�d�x)"; break;

    //Μ��?福??�襴忙u?
    case OnAGBlindTran:        		return "OnAGBlindTran (?福???硉鑼鋇?杠ㄆ?)"; break;
    case OnAGConsultTran:        	return "OnAGConsultTran (?福??�凗藫r鋇?杠ㄆ?)"; break;
    case OnAGConfTran:        		return "OnAGConfTran (?福???某鑼鋇?杠ㄆ?)"; break;
    case OnAGStopTran:            return "OnAGStopTran (?福??氨?鑼鋇?杠ㄆ?)"; break;
    case OnAGConfSpeak:        		return "OnAGConfSpeak (?福??�\﹚?笵?某只?ㄆ?)"; break;
    case OnAGConfAudit:        		return "OnAGConfAudit (?福??�\﹚?笵�げs?某ㄆ?)"; break;
    case OnAGTranIVR:        		  return "OnAGTranIVR (?福???ㄓ杠鑼�庹VRㄆ?)"; break;

    default:                      return "OtherEvent (ㄤ?ㄆ?)"; break;
    }
  }
  else
  {
    switch (EventId)
    {
    //??巨�明��Eㄆ?
    case OnSuccess:				        return "OnSuccess"; break;
    case OnFail:					        return "OnFail"; break;

    case OnInAnswer:					    return "OnInAnswer"; break;

    case OnHangon:				        return "OnHangon"; break;
    case OnLinkHangon:				    return "OnLinkHangon"; break;
    case OnUnLink:				        return "OnUnLink"; break;
    case OnFlash:				          return "OnFlash"; break;

    case OnCallConnected:				  return "OnCallConnected"; break;
    case OnCallUnConnected:				return "OnCallUnConnected"; break;

    //�I�鞶O絏ㄆ?
    case OnPlaying:				        return "OnPlaying"; break;
    case OnPlayEnd:				        return "OnPlayEnd"; break;
    case OnRecvDTMF:				      return "OnRecvDTMF"; break;
    case OnRecvASR:				        return "OnRecvASR"; break;
    case OnPlayError:				      return "OnPlayError"; break;
    case OnErrDTMF:				        return "OnErrDTMF"; break;

    case OnDialDTMF:				      return "OnDialDTMF"; break;

      //TTSㄆ?
    case OnTTSing:				        return "OnTTSing"; break;
    case OnTTSEnd:				        return "OnTTSEnd"; break;
    case OnTTSError:				      return "OnTTSError"; break;

    //ASRㄆ?
    case OnASRing:				        return "OnASRing"; break;
    case OnASREnd:				        return "OnASREnd"; break;
    case OnASRError:				      return "OnASRError"; break;

    //魁�鞶u?
    case OnRecording:				      return "OnRecording"; break;
    case OnRecordEnd:				      return "OnRecordEnd"; break;
    case OnRecordError:			      return "OnRecordError"; break;

    //㊣�|ㄆ?
    case OnOutCalling:			      return "OnOutCalling"; break;
    case OnOutRing:				        return "OnOutRing"; break;
    case OnOutBusy:				        return "OnOutBusy"; break;
    case OnOutTimeout:			      return "OnOutTimeout"; break;
    case OnOutUNN:				        return "OnOutUNN"; break;
    case OnOutFail:				        return "OnOutFail"; break;
    case OnOutAnswer:				      return "OnOutAnswer"; break;
    case OnTranTalk:				      return "OnTranTalk"; break;
    case OnOutRel:				        return "OnOutRel"; break;
    case OnOutNoTrk:			        return "OnOutNoTrk"; break;
    case OnOutBAR:		            return "OnOutBAR"; break;
    case OnOutCGC:		            return "OnOutCGC"; break;
    case OnOutADI:		            return "OnOutADI"; break;
    case OnOutSST:		            return "OnOutSST"; break;
    case OnOutPWROFF:	   	        return "OnOutPWROFF"; break;
    case OnOutOUTSRV: 		        return "OnOutOUTSRV"; break;

    //????ㄆ?
    case OnACDWait:			          return "OnACDWait"; break;
    case OnACDRing:			          return "OnACDRing"; break;
    case OnACDBusy:				        return "OnACDBusy"; break;
    case OnACDTimeOut:			      return "OnACDTimeOut"; break;
    case OnACDUNN:		        		return "OnACDUNN"; break;
    case OnACDFail:				        return "OnACDFail"; break;
    case OnACDAnswer:			        return "OnACDAnswer"; break;
    case OnACDRefuse:	        		return "OnACDRefuse"; break;
    case OnACDGetSeat:        		return "OnACDGetSeat"; break;
    case OnACDOUTSRV:		          return "OnACDOUTSRV"; break;
    case OnACDTakeover:		        return "OnACDTakeover"; break;

    //計沮?巨�岡u?
    case OnDBSuccess:				      return "OnDBSuccess"; break;
    case OnDBFail:				        return "OnDBFail"; break;
    case OnDBBOF:					        return "OnDBBOF"; break;
    case OnDBEOF:					        return "OnDBEOF"; break;
    
    //Μ���議鶩p沮呼閩�襴忙u?
    case OnDWMsg:        		      return "OnDWMsg"; break;

    //﹚��竟ㄆ?
    case OnTimerOver:			        return "OnTimerOver"; break;
    case OnTimer0Over:			      return "OnTimer0Over"; break;
    case OnTimer1Over:			      return "OnTimer1Over"; break;
    case OnTimer2Over:			      return "OnTimer2Over"; break;
    case OnTimer3Over:			      return "OnTimer3Over"; break;
    case OnTimer4Over:			      return "OnTimer4Over"; break;

    //肚痷Μ只ㄆ?
    case OnsFaxing:			          return "OnsFaxing"; break;
    case OnsFaxend:			          return "OnsFaxend"; break;
    case OnsFaxFail:		          return "OnsFaxFail"; break;
    case OnrFaxing:			          return "OnrFaxing"; break;
    case OnrFaxend:			          return "OnrFaxend"; break;
    case OnrFaxFail:		          return "OnrFaxFail"; break;
    case OnsFaxNoSignal:		      return "OnsFaxNoSignal"; break;
    case OnrFaxNoSignal:          return "OnrFaxNoSignal"; break;

    case OnToneDetected:          return "OnToneDetected"; break;

    //?某巨�明��Eㄆ?
    case OnCfcFirst:			        return "OnCfcFirst"; break;
    case OnCfcJoin:			          return "OnCfcJoin"; break;
    case OnCfcListen:		          return "OnCfcListen"; break;
    case OnCfcNotOpen:	          return "OnCfcNotOpen"; break;
    case OnCfcJoinFail:           return "OnCfcJoinFail"; break;
    case OnCfcListenFail:         return "OnCfcListenFail"; break;
    case OnCfcFull:               return "OnCfcFull"; break;

    //?某���阰p跑?ㄆ?
    case OnCfcOne2Many:           return "OnCfcOne2Many"; break;
    case OnCfcMany2One:           return "OnCfcMany2One"; break;
    case OnCfcBusy2Idle:          return "OnCfcBusy2Idle"; break;

    //Μ��?福??�襴忙u?
    case OnAGBlindTran:        		return "OnAGBlindTran"; break;
    case OnAGConsultTran:        	return "OnAGConsultTran"; break;
    case OnAGConfTran:        		return "OnAGConfTran"; break;
    case OnAGStopTran:            return "OnAGStopTran"; break;
    case OnAGConfSpeak:        		return "OnAGConfSpeak"; break;
    case OnAGConfAudit:        		return "OnAGConfAudit"; break;
    case OnAGTranIVR:        		  return "OnAGTranIVR"; break;

    default:                      return "OtherEvent"; break;
    }
  }
}
int Proc_onlogin_Msg(CXMLRcvMsg &IvrMsg)
{
  AuthMaxTrunk = atoi(IvrMsg.GetAttrValue(3).C_Str());
  AuthMaxSeat = atoi(IvrMsg.GetAttrValue(4).C_Str());
  AuthCardType = atoi(IvrMsg.GetAttrValue(5).C_Str());
  g_SessionMng.SwitchType = atoi(IvrMsg.GetAttrValue(8).C_Str());
  g_SessionMng.DataBaseType = IvrMsg.GetAttrValue(9);
  MyTrace(0, "AuthMaxTrunk=%d AuthMaxSeat=%d SwitchType=%d DataBaseType=%s", 
    AuthMaxTrunk, AuthMaxSeat, g_SessionMng.SwitchType, g_SessionMng.DataBaseType.C_Str());
  return 0;
}
int Proc_onnodestate_Msg(CXMLRcvMsg &IvrMsg)
{
  int nodetype = atoi(IvrMsg.GetAttrValue(1).C_Str());
  if (nodetype == 15)
  {
    g_SessionMng.RecSysState = atoi(IvrMsg.GetAttrValue(3).C_Str());
    MyTrace(0, "$S_RecSysState=%d", g_SessionMng.RecSysState);
  }
  return 0;
}
//處理消息
void Proc_Msg_From_IVRServer(  CXMLRcvMsg &IvrMsg)
{

		unsigned short MsgId=IvrMsg.GetMsgId();

    if (MsgId >= MAX_IVRXMLMSG_NUM)
    {
      MyTrace(5, "Recv msgid=%d from IVR is out of range", MsgId);
      return ;
    }
  
  	if(0!=IvrMsg.ParseRevMsgWithCheck(g_FlwRuleMng.IVRXMLMsgRule[MsgId]))
    {
        //接收消息錯誤
        MyTrace(5, "Recv msg From ivr is error: MsgId = %d MsgBuf = %s\n", MsgId, IvrMsg.GetBuf().C_Str());
        return;
    }

  	
    switch (MsgId)
    {
        case MSG_onlogin:       //節點注冊結果
          Proc_onlogin_Msg(  IvrMsg);  
          break;
        case MSG_onsetaccessno:	//設置接入號碼結果
            break;
        case MSG_oncallin:      //電話呼入事件
            Proc_oncallin_Msg(  IvrMsg);
            break;
        case MSG_onrecvsam:      //收到后續地址事件
            Proc_onrecvsam_Msg(  IvrMsg);
            break;
        case MSG_onsmsin:		//短信呼入事件
            break;
        case MSG_oncallflw:	    //調用指定流程
            Proc_oncallflw_Msg(  IvrMsg);
            break;
        case MSG_onanswer:	    //來話應答結果
        case MSG_onsendsignal:	//發送信令結果
        case MSG_onhangon:	    //掛機釋放結果
        case MSG_ondtmfrule:	//DTMF按鍵規則定義結果
        case MSG_ongetdtmf:	    //收碼結果
        case MSG_onplayrule:	//放音規則定義結果
        case MSG_onasrrule:	    //ASR規則定義結果
        case MSG_onsetvolume:	//改變放音音量結果
        case MSG_onplayfile:	//單文件放音結果
        case MSG_onsetplaypos:	//改變放音指針位置結果
        case MSG_onplayfiles:	//多文件放音結果
        case MSG_onplaycompose:	//合成串放音結果
        case MSG_onmultiplay:	//多語音合成放音結果
        case MSG_onttsstring:	//文本串轉語音結果
        case MSG_onttsfile:	    //文本文件轉語音結果
        case MSG_onsenddtmf:	//發送DTMF結果
        case MSG_onsendfsk:	    //發送FSK結果
        case MSG_onplaycfc:	    //會議室放音結果
        case MSG_onplaytone:	//播放信號音結果
        case MSG_onrecordfile:	//錄音結果
        case MSG_onrecordcfc:	//監視錄音結果
        case MSG_onstop:		//停止放音錄音收碼結果
        case MSG_oncallout:	    //電話呼出結果
        case MSG_ondialout:
        case MSG_oncancelcallout://取消呼出結果
        case MSG_onsendsam://發送被叫號碼結果
        case MSG_ontransfer:	//電話轉接結果
        case MSG_oncallseat:	//分配坐席結果
        case MSG_onstopcallseat:	//取消分配坐席結果
        case MSG_oncreatecfc:	//創建會議結果
        case MSG_ondestroycfc:	//釋放會議資源結果
        case MSG_onjoincfc:	    //加入會議結果
        case MSG_onunjoincfc:	//退出會議結果
        case MSG_onlink:		//通道交換結果
        case MSG_ontalkwith:	//與對方通話結果
        case MSG_onlistenfrom:	//監聽對方通話結果
        case MSG_oninserttalk:	//強插對方通話結果
        case MSG_onthreetalk:	//三方通話結果
        case MSG_onstoptalk:	//結束通話結果
        case MSG_onsendfax:	    //發送傳真結果
        case MSG_onrecvfax:	    //接收傳真結果
        case MSG_oncheckpath:	//檢查文件路徑結果
        case MSG_oncreatepath:	//創建文件路徑結果
        case MSG_ondeletepath:	//刪除文件路徑結果
        case MSG_ongetfilenum:	//取文件個數結果
        case MSG_ongetfilename:	//取文件名結果
        case MSG_onrenname:	    //改文件名結果
        case MSG_oncopyfile:	//拷貝文件結果
        case MSG_ondeletefile:	//刪除文件結果
        case MSG_oncheckfile:	//檢查文件是否存在
        case MSG_onclearmixer:	//清除放音合成緩沖區
        case MSG_onaddfile:	//增加文件到放音合成緩沖區
        case MSG_onadddatetime:	//增加日期時間到放音合成緩沖區
        case MSG_onaddmoney:	//增加金額到放音合成緩沖區
        case MSG_onaddnumber:	//增加數值到放音合成緩沖區
        case MSG_onadddigits:	//增加數字串到放音合成緩沖區
        case MSG_onaddchars:	//增加字符串到放音合成緩沖區
        case MSG_onplaymixer:	//播放合成緩沖區
        case MSG_onaddttsstr:	//增加TTS字符串到放音合成緩沖區
        case MSG_onaddttsfile:	//增加TTS字符串到放音合成緩沖區
        case MSG_onplay:	//放音結果
        case MSG_onlinkevent:	//通道交換對方掛機、斷開事件
        case MSG_onflash:	//模擬內線拍叉簧結果
        case MSG_onsetvcparam:	//設置變聲參數結果事件
        case MSG_onpause:	//暫停放音結果事件
        case MSG_onreplay:	//重新放音結果事件
        case MSG_onfastplay:	//快速放音結果事件
        case MSG_onchecktone:	//檢測信號音結果事件
        case MSG_onworkerlogin:	//話務員登錄消息結果事件
        case MSG_onworkerlogout:	//話務員退出消息結果事件
        case MSG_onpickupcall:	    //代接電話結果
        case MSG_ongetacdqueue:	//取ACD隊列信息結果事件
        case MSG_onrecvfsk:	    //接收FSK結果
        case MSG_ontransferivr:	    //分配IVR資源結果
        case MSG_onreleaseivr:	    //釋放IVR資源結果
        case MSG_onplayswitchvoice:	    //播放交換機內置語音結果
        case MSG_onsendswitchcmd:	    //發送交換機操作命令結果
        case MSG_ondial:	    //撥打電話結果結果
        case MSG_onappendfaxfile:	    //追加發送的傳真文件結果
        case MSG_onsendcalltoagent:	    //發送電話分配到電腦坐席結果
        case MSG_oncallconnected:	    //收到交換機呼叫通話連接事件
        case MSG_ongetseatlogindata:	    //收到取坐席登錄的話務員信息結果事件
        case MSG_ongetidleseat:	    //取一空閑坐席并臨時鎖定結果
        case MSG_onunlockseat:	    //解除臨時鎖定結果
        case MSG_onupdatesrvcore:	    //更新服務評價到后臺IVR服務結果
        case MSG_onupdatecdrfield:	    //更新CDR字段數據結果
        case MSG_onupdatesysparam:	    //更新平臺系統參數結果
        case MSG_onsetforwarding:	    //設置分機呼轉結果
        case MSG_oncallfunction:	    //特殊功能呼叫結果，如：監聽、強插
        case MSG_onplayex:	    //增強型放音指令結果
          Proc_onIVRcommon_Msg(IvrMsg);
          break;
        case MSG_onrecvevent:	//通道交換對方掛機、斷開事件(收到時session已經不可使用)
            //Proc_onIVRcommon_Msg(IvrMsg);
            break;
        case MSG_onexecsql:	//IVR執行SQL語句
          g_TcpServer.DBSendMessage(MSG_execsql, IvrMsg.GetBuf());
          break;
        case MSG_onagblindtrancall:	//電腦坐席快速轉接電話
          Proc_onagblindtrancall_Msg(IvrMsg);
          break;
        case MSG_onagconsulttrancall:	//電腦坐席協商轉接電話
          Proc_onagconsulttrancall_Msg(IvrMsg);
          break;
        case MSG_onagconftrancall:	//電腦坐席會議轉接電話
          Proc_onagconftrancall_Msg(IvrMsg);
          break;
        case MSG_onagstoptrancall:	//電腦坐席停止轉接來話事件
          Proc_onagstoptrancall_Msg(IvrMsg);
          break;
        case MSG_onagconfspeak:	//電腦坐席指定通道會議發言
          Proc_onagconfspeak_Msg(IvrMsg);
          break;
        case MSG_onagconfaudit:	//電腦坐席指定通道旁聽會議
          Proc_onagconfaudit_Msg(IvrMsg);
          break;
        case MSG_onagtranivr:	//坐席將來話轉接到ivr流程
          Proc_onagtranivr_Msg(IvrMsg);
          break;
        case MSG_onaghold:	//電腦坐席發給流程會話的保持/取消保持消息
          Proc_onaghold_Msg(IvrMsg);
          break;
        case MSG_onagmute:	//電腦坐席發給流程會話的靜音/取消靜音消息
          Proc_onagmute_Msg(IvrMsg);
          break;
        case MSG_onagflwmsg:	//電腦坐席發給流程會話的消息
          Proc_onsendflwmsg_Msg(IvrMsg);
          break;
        case MSG_ongetdialdtmf:	//內線分機后續按鍵事件
          Proc_ongetdialdtmf_Msg(IvrMsg);
          break;
        case MSG_onrouteideltrks:	//路由空閑中繼數
          Proc_onrouteideltrks(IvrMsg);
          break;
        case MSG_onbandagentchn:	//綁定坐席通道結果
          Proc_onbandagentchn(IvrMsg);
          break;
        case MSG_onnewcallparam:	//變化了的新的呼叫相關參數
          Proc_onnewcallparam(IvrMsg);
          break;
        case MSG_onnodestate:       //節點狀態
          Proc_onnodestate_Msg(IvrMsg);  
          break;
        default:
          break;        
    }
}

void Proc_Msg_From_DBServer(  CXMLRcvMsg &DBMsg)
{		  
	  unsigned short MsgId=DBMsg.GetMsgId();

    if (MsgId >= MAX_DBXMLMSG_NUM)
    {
    //指令編號超出范圍   
      MyTrace(5, "DBmsgid = %d is out of range", MsgId);
      return ;
    }
  
  	if(0!=DBMsg.ParseRevMsgWithCheck(g_FlwRuleMng.DBXMLMsgRule[MsgId]))
    {
        //接收消息錯誤
        MyTrace(5, "Recv msg From dbproxy is error: MsgId = %d MsgBuf = %s", MsgId, DBMsg.GetBuf().C_Str());
        return;
    }

    switch (MsgId)
    {
        case MSG_ongwquery:	//查詢外部數據網關結果
        case MSG_ondbquery:	//查詢數據表操作結果
        case MSG_ondbfield:	//取數據字段值操作結果
        case MSG_ondbmove:	//記錄指針移動結果
        case MSG_ondbcmd:	//插入、修改、刪除記錄結果
        case MSG_onwritebill:	//寫話單結果
        case MSG_ongettolltime:	//返回最大通話時長
        case MSG_ondbrecord:	//讀數據表當前記錄結果
        case MSG_ondbsp: //執行存儲過程結果
        case MSG_ondbspoutparam:	//取存儲過程輸出參數值結果
        case MSG_ondbfieldtofile:	//取查詢的字段值并存放到指定的文件結果
        case MSG_ondbfiletofield:	//將文件內容存入數據表結果
        case MSG_onquerywebservice:	//執行查詢外部webservice接口數據結果
        case MSG_ongetwebservicexml:	//從外部webservice接口查詢的XML串中取數據結果
        case MSG_ongetwebservicestr:	//從外部webservice接口查詢的字符串取數據結果
        case MSG_ongetwebserviceindex:	//從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號結果
        case MSG_onhttprequest:	//執行發送HTTP協議請求結果
        case MSG_onshellexecute:	//執行外部程序結果
        case MSG_ongettxtlinestr:	//取文本行某段字符串結果
        case MSG_onsocketrequest:	//通過socket通信發送查詢請求結果
        case MSG_ondbrecorddump:	//數據表記錄轉存結果
        case MSG_onhttprequestex:	//執行發送HTTP協議請求結果
        case MSG_ondbselect:	    //查詢數據表記錄字段值操作結果
        case MSG_ongetwebservicejson:	//從外部webservice接口查詢的JSON串中取數據結果
          Proc_onDBcommon_Msg(DBMsg);
          break;
        case MSG_ongwmakecall:	//外部數據網關啟動呼叫事件
          Proc_ongwmakecall_Msg(DBMsg);
          break;
        case MSG_ongwmsgevent:	//外部數據網關向流程發送消息事件
          Proc_ongwmsgevent_Msg(DBMsg);
          break;
        default:
            break;        
    }
}

//電話呼入后續地址事件消息
int Proc_onrecvsam_Msg( CXMLRcvMsg &IvrMsg)
{
    US SessionId;
    UC ChnType;
    US ChnNo;
    
    ChnType = atoi(IvrMsg.GetAttrValue(2).C_Str());
    ChnNo = atoi(IvrMsg.GetAttrValue(3).C_Str());
    
    SessionId=g_SessionMng.Get_SessionId_By_Chn(ChnType, ChnNo);
    if (SessionId)
    {
    	  pCurrent=&g_SessionMng[SessionId];
        if (IvrMsg.GetAttrValue(4).GetLength() > 0)
        {
          pCurrent->CallerNo = IvrMsg.GetAttrValue(4);
          if (g_bVarId || g_nSendTraceMsgToLOG)
          {
            MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallerNo; Value=%s", 
              pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallerNo.C_Str());
          }
        }
        if (pCurrent->CalledNo.GetLength()+IvrMsg.GetAttrValue(5).GetLength() < MAX_TELECODE_LEN)
        {
          pCurrent->CalledNo+=IvrMsg.GetAttrValue(5);
          if (g_bVarId || g_nSendTraceMsgToLOG)
          {
            MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CalledNo; Value=%s", 
              pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CalledNo.C_Str());
          }
        }
        if (IvrMsg.GetAttrValue(6).GetLength() > 0)
        {
          pCurrent->OrgCallerNo = IvrMsg.GetAttrValue(6);
          if (g_bVarId || g_nSendTraceMsgToLOG)
          {
            MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_OrgCallerNo; Value=%s", 
              pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->OrgCallerNo.C_Str());
          }
        }
        if (IvrMsg.GetAttrValue(7).GetLength() > 0)
        {
          pCurrent->OrgCalledNo = IvrMsg.GetAttrValue(7);
          if (g_bVarId || g_nSendTraceMsgToLOG)
          {
            MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_OrgCalledNo; Value=%s", 
              pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->OrgCalledNo.C_Str());
          }
        }
    }
    else
    {
        MyTrace(5, "ChnType = %d ChnNo = %d sam = %s recv sam but the chn is not callin",
        	 ChnType, ChnNo, IvrMsg.GetAttrValue(4).C_Str());        
    }
    return 0;
}
int Proc_ongetdialdtmf_Msg( CXMLRcvMsg &IvrMsg)
{
  US SessionId;
  UC ChnType;
  US ChnNo;
  
  ChnType = atoi(IvrMsg.GetAttrValue(2).C_Str());
  ChnNo = atoi(IvrMsg.GetAttrValue(3).C_Str());
  
  SessionId=g_SessionMng.Get_SessionId_By_Chn(ChnType, ChnNo);
  if (SessionId)
  {
    pCurrent=&g_SessionMng[SessionId];
    if (IvrMsg.GetAttrValue(4).GetLength() > 0)
    {
      pCurrent->DialDTMF = IvrMsg.GetAttrValue(4);
      if (g_bVarId || g_nSendTraceMsgToLOG)
      {
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_DialDTMF; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->DialDTMF.C_Str());
      }
      pCurrent->EventVars[UC_DialDTMFEvent] = OnDialDTMF;
    }
  }
  else
  {
    MyTrace(5, "ChnType = %d ChnNo = %d dialdtmf = %s recv dialdtmf but the chn is not callin",
      ChnType, ChnNo, IvrMsg.GetAttrValue(4).C_Str());        
  }
  return 0;
}
//電話呼入事件消息
int Proc_oncallin_Msg(CXMLRcvMsg &IvrMsg)
{
    US OldSessionId, SessionId;
    UC ChnType;
    US ChnNo;
    US FlwNo;
    
    //CH ErrMsg[MAX_MSG_BUF_LEN];

    //if (AuthCardType != LocaCardType) return 1;
    
    ChnType = atoi(IvrMsg.GetAttrValue(0).C_Str());
    ChnNo = atoi(IvrMsg.GetAttrValue(1).C_Str());
    const CStringX &Caller=IvrMsg.GetAttrValue(2);
    const CStringX &Called=IvrMsg.GetAttrValue(4);
    const CStringX &OrgCalled=IvrMsg.GetAttrValue(7);

    if ((ChnType == 1 && ChnNo >= AuthMaxTrunk) || (ChnType == 2 && ChnNo >= AuthMaxSeat))
    {
      Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_restdial, 3, 1);
      return 1;
    }

    //edit 2007-08-09
    OldSessionId=g_SessionMng.Get_SessionId_By_Chn(ChnType, ChnNo);
    if (OldSessionId)
    {
      MyTrace(5, "ChnType=%d ChnNo=%d is online before SessionId=%d oncallin SetOnHangOnEvent",ChnType, ChnNo, OldSessionId);
      g_SessionMng[OldSessionId].RecvHangonId = 1;
      g_SessionMng[OldSessionId].SetOnHangOnEvent();
    }
		if (g_SessionMng.IsAllSessionBusy())
    {
      if (OldSessionId)
      {
          //該通道正在會話中
        US CallFlwNo = pCurrent->FlwNo;
        US CallFlwId = pCurrent->FlwId;
        if (g_RunFlwMng[CallFlwNo].AutoStartId == OldSessionId)
        {
          g_RunFlwMng[CallFlwNo].AutoStartId = 0;
        }
        
        g_SessionMng.Free_SessionId(OldSessionId);
        SendTotalSessionsToAllLOG();
        SendOneFLWSessionsToAllLOG(CallFlwNo);

        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s SessionId=%d oncallin force release", 
        		ChnType, ChnNo, Caller.C_Str(), Called.C_Str(), OldSessionId);
      }
    }
     
    pCurrent=0; 
    //根據主被叫號碼查詢流程功能號
    if (g_nParserAccessCodeType == 1)
    {
      if (g_GData.Get_FlwNo_By_CalledNo(Caller, Called, FlwNo) != 0)
      {
        //該被叫號碼對應的業務暫未開通(播放業務暫未開通提示音，然后釋放通道)
        Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_notopen, 3, 1);
        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s but the service is not open", 
          ChnType, ChnNo, Caller.C_Str(), Called.C_Str());
        return 3;
      }
    }
    else if (g_nParserAccessCodeType == 2)
    {
      if (g_GData.Get_FlwNo_By_CalledNo(Caller, OrgCalled, FlwNo) != 0)
      {
        //該被叫號碼對應的業務暫未開通(播放業務暫未開通提示音，然后釋放通道)
        Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_notopen, 3, 1);
        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s OrgCalledNo=%s but the service is not open", 
          ChnType, ChnNo, Caller.C_Str(), OrgCalled.C_Str());
        return 3;
      }
    }
    else
    {
      if (g_GData.Get_FlwNo_By_CalledNo(Caller, Called, FlwNo) != 0)
      {
        if (g_GData.Get_FlwNo_By_CalledNo(Caller, OrgCalled, FlwNo) != 0)
        {
          //該被叫號碼對應的業務暫未開通(播放業務暫未開通提示音，然后釋放通道)
          Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_notopen, 3, 1);
          MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s OrgCalledNo=%s but the service is not open", 
            ChnType, ChnNo, Caller.C_Str(), Called.C_Str(), OrgCalled.C_Str());
          return 3;
        }
      }
    }
    if (g_RunFlwMng[FlwNo].OpenId == 0)
    {
        Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_notopen, 3, 1);
        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s but the service is close", 
          ChnType, ChnNo, Caller.C_Str(), Called.C_Str());
        return 3;
    }
    //查詢呼叫限制表
    if (g_GData.Query_RestTel(g_RunFlwMng[FlwNo].FuncGroup, g_RunFlwMng[FlwNo].FuncNo, Caller,Called) == 1)
    {
        //限制撥打
        Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_restdial, 3, 1);
        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s restrict dial", 
          ChnType, ChnNo, Caller.C_Str(), Called.C_Str());
        return 0;
    }
    
    SessionId=	g_SessionMng.Alloc_Idel_SessionId();
    if (SessionId) //分配到空閑通道
    {
      pCurrent=&g_SessionMng[SessionId];
 
      //0-外線呼入號碼(外線有效) 1-呼出的外線號碼 2-呼叫的內線坐席號 3-呼叫的坐席工號
      pCurrent->CalledType = atoi(IvrMsg.GetAttrValue(5).C_Str());

      if (pCurrent->CalledType == 0)
      {
        pCurrent->Init(g_bCmdId,//如果號碼匹配，可以跟蹤,
          ChnType,ChnNo,CALL_IN,
          Caller,Called,OrgCalled,
          0,
          IvrMsg.GetAttrValue(14),
          FlwNo,g_RunFlwMng[FlwNo].FlwId
          );
      }
      else
      {
        pCurrent->Init(g_bCmdId,//如果號碼匹配，可以跟蹤,
          ChnType,ChnNo,CALL_OUT,
          Caller,Called,OrgCalled,
          0,
          IvrMsg.GetAttrValue(14),
          FlwNo,g_RunFlwMng[FlwNo].FlwId
          );
      }
      pCurrent->CalledType = atoi(IvrMsg.GetAttrValue(5).C_Str());
      pCurrent->CallerType = atoi(IvrMsg.GetAttrValue(3).C_Str());
      pCurrent->OrgCallerNo = IvrMsg.GetAttrValue(6);
      pCurrent->ChHWType = atoi(IvrMsg.GetAttrValue(9).C_Str());
      pCurrent->ChHWNo = atoi(IvrMsg.GetAttrValue(10).C_Str());
      pCurrent->RouteNo = atoi(IvrMsg.GetAttrValue(8).C_Str());
      pCurrent->SeatType = atoi(IvrMsg.GetAttrValue(11).C_Str());
      pCurrent->SeatNo = IvrMsg.GetAttrValue(12);
      pCurrent->WorkerNo = atoi(IvrMsg.GetAttrValue(13).C_Str());
      pCurrent->GroupNo = atoi(IvrMsg.GetAttrValue(15).C_Str());
      pCurrent->BandAGFirst = atoi(IvrMsg.GetAttrValue(16).C_Str());
      pCurrent->DialDTMF = Called;
      pCurrent->CallType = atoi(IvrMsg.GetAttrValue(17).C_Str());
      pCurrent->AnswerId = atoi(IvrMsg.GetAttrValue(18).C_Str());
      pCurrent->CDRSerialNo = IvrMsg.GetAttrValue(19);
      pCurrent->RecdRootPath = IvrMsg.GetAttrValue(20);
      pCurrent->RecdFileName = IvrMsg.GetAttrValue(21);
      pCurrent->RecvDtmf = IvrMsg.GetAttrValue(22);
      pCurrent->DataBaseType = g_SessionMng.DataBaseType;
      pCurrent->SwitchType = g_SessionMng.SwitchType;

      if (g_bVarId || g_nSendTraceMsgToLOG)
      {
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SessionNo; Value=%ld(0x%08x)", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SerialNo, pCurrent->SerialNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CDRSerialNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CDRSerialNo.C_Str());

        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChanType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChnType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChanNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChnNo);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_RouteNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->RouteNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChHWType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChHWType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChHWNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChHWNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallerNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallerNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallerType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallerType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CalledNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CalledNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CalledType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CalledType);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_OrgCalledNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->OrgCalledNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_OrgCallerNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->OrgCallerNo.C_Str());
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SeatType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SeatType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SeatNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SeatNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_WorkerNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->WorkerNo);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_GroupNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->GroupNo);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_BandAGFirst; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->BandAGFirst);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_AnswerId; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->AnswerId);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallInOut; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallInOut);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_DialParam; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->DialParam.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_RecdRootPath; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->RecdRootPath.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_RecdFileName; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->RecdFileName.C_Str());

        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SwitchType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SwitchType);
      }
      Send_SetSessionNo(pCurrent);
      SendTotalSessionsToAllLOG();
      SendOneFLWSessionsToAllLOG(pCurrent->FlwNo);
    }
    else
    {
        //沒有空閑的會話資源,發送釋放該通道命令
        Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_busytone, 3, 1);
        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s oncallin not idel session", 
        		ChnType, ChnNo, Caller.C_Str(), Called.C_Str());
    }
    return 0;
}
//處理收到的調用指定流程消息
int Proc_oncallflw_Msg(  CXMLRcvMsg &IvrMsg)
{
    US OldSessionId, SessionId;
    UC ChnType;
    US ChnNo;
    US FuncNo;
    US FlwNo;
    //CH ErrMsg[MAX_MSG_BUF_LEN];
    FuncNo = atoi(IvrMsg.GetAttrValue(0).C_Str());
    ChnType = atoi(IvrMsg.GetAttrValue(1).C_Str());
    ChnNo = atoi(IvrMsg.GetAttrValue(2).C_Str());
    const CStringX &Caller=IvrMsg.GetAttrValue(3);
    const CStringX &Called=IvrMsg.GetAttrValue(4);
    
    pCurrent=0;
    
    if (!g_RunFlwMng.Get_FlwNo_By_FuncNo(FuncNo,FlwNo)) //找不到流程
    {
        Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_notopen, 3, 1);
        MyTrace(5, "ChnType=%d ChnNo=%d CallerNo=%s CalledNo=%s but the service is not open", 
        		ChnType, ChnNo, Caller.C_Str(), Called.C_Str());
        return 1;
    }

    //Edit 2007-08-09
    OldSessionId=g_SessionMng.Get_SessionId_By_Chn(ChnType, ChnNo);
    if (OldSessionId)
    {
      MyTrace(5, "ChnType=%d ChnNo=%d is online before SessionId=%d oncallflw SetOnHangOnEvent",ChnType, ChnNo, OldSessionId);
      g_SessionMng[OldSessionId].RecvHangonId = 1;
      g_SessionMng[OldSessionId].SetOnHangOnEvent();
    }
    if (g_SessionMng.IsAllSessionBusy())
    {
      if (OldSessionId) //在會話中 
      {
        //該通道正在會話中
        US CallFlwNo = pCurrent->FlwNo;
        US CallFlwId = pCurrent->FlwId;
        if (g_RunFlwMng[CallFlwNo].AutoStartId == OldSessionId)
        {
          g_RunFlwMng[CallFlwNo].AutoStartId = 0;
        }

        g_SessionMng.Free_SessionId(OldSessionId);
        SendTotalSessionsToAllLOG();
        SendOneFLWSessionsToAllLOG(CallFlwNo);

        MyTrace(5, "ChnType = %d ChnNo = %d CallerNo=%s CalledNo=%s SessionId=%d oncallflw force release", 
        	ChnType, ChnNo, Caller.C_Str(), Called.C_Str(), OldSessionId);
      }
    }
    //End Edit

    SessionId=g_SessionMng.Alloc_Idel_SessionId( );   
    if (SessionId)
    {
			pCurrent=&g_SessionMng[SessionId];

			pCurrent->Init(g_bCmdId,//如果號碼匹配，可以跟蹤
 																	 ChnType,ChnNo,CALL_OUT,
 																	 Caller,Called,IvrMsg.GetAttrValue(5),
 																	 atoi(IvrMsg.GetAttrValue(13).C_Str()),
 																	 IvrMsg.GetAttrValue(14),
 																	 FlwNo,g_RunFlwMng[FlwNo].FlwId
 																	);
      pCurrent->OrgCallerNo = IvrMsg.GetAttrValue(5);
      pCurrent->ChHWType = atoi(IvrMsg.GetAttrValue(7).C_Str());
      pCurrent->ChHWNo = atoi(IvrMsg.GetAttrValue(8).C_Str());
      pCurrent->SeatType = atoi(IvrMsg.GetAttrValue(9).C_Str());
      pCurrent->SeatNo = IvrMsg.GetAttrValue(10);
      pCurrent->WorkerNo = atoi(IvrMsg.GetAttrValue(11).C_Str());
      pCurrent->BandAGFirst = 0;
      pCurrent->CallType = 0;
      pCurrent->AnswerId = 0;
      pCurrent->DataBaseType = g_SessionMng.DataBaseType;
      pCurrent->SwitchType = g_SessionMng.SwitchType;

      if (g_bVarId || g_nSendTraceMsgToLOG)
      {
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SessionNo; Value=%ld(0x%08x)", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SerialNo, pCurrent->SerialNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChanType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChnType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChanNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChnNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChHWType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChHWType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChHWNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChHWNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallerNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallerNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CalledNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CalledNo.C_Str());
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_OrgCalledNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->OrgCalledNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_OrgCallerNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->OrgCallerNo.C_Str());
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SeatType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SeatType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SeatNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SeatNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_WorkerNo; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->WorkerNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_DialParam; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->DialParam.C_Str());
      }
      Send_SetSessionNo(pCurrent);
      SendTotalSessionsToAllLOG();
      SendOneFLWSessionsToAllLOG(pCurrent->FlwNo);
    }
    else
    {
      //沒有空閑的會話資源,發送釋放該通道命令
      Send_PlayTone_Release_Chn(  ChnType, ChnNo, TONE_busytone, 3, 1);
      MyTrace(5, "ChnType = %d ChnNo = %d CallerNo=%s CalledNo=%s oncallflw not idel session", 
            	ChnType, ChnNo, Caller.C_Str(), Called.C_Str());
    }
    return 0;
}

//處理IVR發來的一般消息處理結果消息
int Proc_onIVRcommon_Msg(  CXMLRcvMsg &IvrMsg)
{           
    US CmdAddr;       
    UC Result;
    UC ReturnType;
    UC ReturnVar;
    US FlwNo;
    US FlwId;
   
    int Offset = 0;
		
		
		unsigned short MsgId=IvrMsg.GetMsgId();
		unsigned short AttrNum=IvrMsg.GetAttrNum();
		const VXML_RECV_MSG_CMD_RULE_STRUCT &CurRule=g_FlwRuleMng.IVRXMLMsgRule[MsgId];
		
    UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
    US SessionId = (US)SerialNo;
    UL Flw_CmdAddr = atol(IvrMsg.GetAttrValue(1).C_Str());
    UC ChnType = atoi(IvrMsg.GetAttrValue(2).C_Str());
    US ChnNo = atoi(IvrMsg.GetAttrValue(3).C_Str());
    
    //if ((ChnType == 1 && ChnNo >= AuthMaxTrunk) || (ChnType == 2 && ChnNo >= AuthMaxSeat))
    //  return 1;
    
    pCurrent=0;
    if(!g_SessionMng.IsSessionNoAvail(SessionId))
    {
        //該通道未在線
        if (MsgId != MSG_onhangon && MsgId != MSG_oncallout)
        {
        	MyTrace(5, "ChnType = %d ChnNo = %d the chn is not online", ChnType, ChnNo);
        }
        return 1;
    }
    
    pCurrent=&g_SessionMng[SessionId];
    if (MsgId == MSG_oncallout && pCurrent->DialSerialNo == 0)
    {
      	MyTrace(5, "ChnType = %d ChnNo = %d the chn not callout", ChnType, ChnNo);
        return 1;
    }
    if (MsgId == MSG_onhangon)
    {
      pCurrent->RecvHangonId = 1;  
      pCurrent->WaitingCount = 0;
      pCurrent->SetOnHangOnEvent();
      return 0;
    }
    for (int i = 4; i < AttrNum; i ++)
    {
        ReturnType = CurRule.Attribute[i].ReturnType;
        ReturnVar = (UC)CurRule.Attribute[i].ReturnVar;
        if (ReturnType == 2) //2-消息結果參數
        {
            Result = atoi(IvrMsg.GetAttrValue(i).C_Str());
            if (ReturnVar == 1) //edit 2007-09-15 add
              pCurrent->EventVars[UC_OnEvent] = Result;
            MyTrace(5, "SerialNo=%ld Recv IVR event ReturnType=%d ReturnVar=%d EventId=%d EventName=%s", 
              SerialNo, ReturnType, ReturnVar, Result, Get_EventName_By_EventId(Result));
            switch (ReturnVar)
            {
                case 1: //MsgEvent
                {
                    pCurrent->EventVars[UC_MsgEvent] = Result;
                    break;
                }
                case 2: //PlayEvent
                {
                  pCurrent->RecvDtmf = IvrMsg.GetAttrValue(5);
                  pCurrent->AsrBuf = IvrMsg.GetAttrValue(6);
                  if (Result == OnTTSEnd) //edit 2008-07-05 add
                  {
                    pCurrent->EventVars[UC_TTSEvent] = Result;
                    break;
                  }
                  else if (Result == OnTTSError)
                  {
                    pCurrent->EventVars[UC_TTSEvent] = Result;
                    break;
                  }
                  pCurrent->EventVars[UC_PlayEvent] = Result;
                  break;
                }
                case 3: //TTSEvent
                {
                    pCurrent->EventVars[UC_TTSEvent] = Result;
                    break;
                }
                case 4: //ASREvent
                {
                    pCurrent->EventVars[UC_ASREvent] = Result;
                    break;
                }
                case 5: //RecordEvent
                {
                    pCurrent->EventVars[UC_RecordEvent] = Result;
                    break;
                }
                case 6: //CalloutEvent
                {
                    pCurrent->EventVars[UC_CalloutEvent] = Result;
                    break;
                }
                case 7: //ACDEvent
                {
                    pCurrent->EventVars[UC_ACDEvent] = Result;
                    break;
                }
                case 8: //DBEvent 
                {
                    pCurrent->EventVars[UC_DBEvent] = Result;
                    break;
                }  
                case 9: //FAXEvent 
                  {
                    pCurrent->EventVars[UC_FaxEvent] = Result;
                    if (Result == OnsFaxend)
                    {
                      pCurrent->RemoteFaxSID = IvrMsg.GetAttrValue(8);
                    }
                    break;
                  }  
                case 10: //ToneDetectedEvent 
                  {
                    pCurrent->EventVars[UC_ToneDetectedEvent] = Result;
                    break;
                  }  
                case 11: //LinkEvent 
                  {
                    pCurrent->EventVars[UC_LinkEvent] = Result;
                    //MyTrace(5, "pCurrent->EventVars[%d] = %d", UC_LinkEvent, Result);
                    break;
                  }  
                case 12: //FlashEvent 
                  {
                    pCurrent->EventVars[UC_FlashEvent] = Result;
                    break;
                  }  
                case 13: //CfcEvent 
                  {
                    pCurrent->EventVars[UC_CfcEvent] = Result;
                    break;
                  }  
            }
        }
        else if (ReturnType == 3) //3-返回值參數
        {
            if (SerialNo == 0)
                return 1;
  
  				
            FlwNo = (UC)(Flw_CmdAddr>>24);
            FlwId = (UC)(Flw_CmdAddr>>16);
            CmdAddr=(US)Flw_CmdAddr;

            if (CmdAddr == 0)
            {
              continue;
            }
            
            if(!g_RunFlwMng.IsFlwAddrOK(FlwNo,FlwId,CmdAddr))
            {
                MyTrace(5, "recv CmdAddr=%d is out of range SerialNo = %ld MsgId = %d", CmdAddr, SerialNo, MsgId);
                return 1;
            }
            
            pCurrentCmd=&g_RunFlwMng[FlwNo].Flw[FlwId].RunCmds[CmdAddr]; //設定當前指令(注意發送CmdAddr時的流程可能不是當前流程)
            
                       
            if (MsgId == MSG_ongetfilename && i == 6)
            {
                Offset = atoi(IvrMsg.GetAttrValue(4).C_Str());
            }
            else
            {
                Offset = 0;
            }
            
          try
          {  
            Assign_pCurrent_Str_To_Var( Offset, pCurrentCmd->ParamValues[ReturnVar].C_Str(), IvrMsg.GetAttrValue(i));
          }
          catch (...)
          {
            //pCurrent->ClearAllEvents(); //edit 2007-09-15 del
            pCurrent->SetOnHangOnEvent(); //設置掛機
						MyTrace(5, "SerialNo=%ld Received IVR Error AttrData then HangOn", SerialNo);
            return 1;
         }
        }
        else if (ReturnType == 4) //4-返回的錯誤消息
        {
            if (SerialNo == 0)
                return 1;
            pCurrent->ErrorMsg = IvrMsg.GetAttrValue(i); //edit 2009-04-16
        }
    }
    if (MsgId == MSG_oncallseat)
    {
        if (atoi(IvrMsg.GetAttrValue(4).C_Str()) == OnACDAnswer 
          || atoi(IvrMsg.GetAttrValue(4).C_Str()) == OnACDRing
          || atoi(IvrMsg.GetAttrValue(4).C_Str()) == OnACDGetSeat)
        {
            pCurrent->SeatNo = IvrMsg.GetAttrValue(7);
            pCurrent->WorkerNo = atoi(IvrMsg.GetAttrValue(8).C_Str());
        }
    }
    return 0;
}

//處理DB發來的一般消息處理結果消息
int Proc_onDBcommon_Msg(   CXMLRcvMsg &DBMsg)
{
    US CmdAddr;
    UC Result;
    UC ReturnType;
    UC ReturnVar;
    US FlwNo;
    US FlwId;
    char recordlist[1920];
    char fieldvalue[MAX_VAR_DATA_LEN];
    unsigned short n=0,j=0,k=0;
    char ch;

    int Offset = 0;

		unsigned short MsgId=DBMsg.GetMsgId();
		unsigned short AttrNum=DBMsg.GetAttrNum();
		const VXML_RECV_MSG_CMD_RULE_STRUCT &CurRule=g_FlwRuleMng.DBXMLMsgRule[MsgId];
		
    UL SerialNo = atol(DBMsg.GetAttrValue(0).C_Str());
    US SessionId = (US)SerialNo;
    UL Flw_CmdAddr = atol(DBMsg.GetAttrValue(1).C_Str());
    UC ChnType = atoi(DBMsg.GetAttrValue(2).C_Str());
    US ChnNo = atoi(DBMsg.GetAttrValue(3).C_Str());

    FlwNo = (UC)(Flw_CmdAddr>>24);
    
    pCurrent=0;
    if(!g_SessionMng.IsSessionNoAvail(SessionId) )
    {
        //該通道未在線
        //MyTrace(5, "When received DB MSG,ChnType=%d ChnNo=%d the chn is not online", ChnType, ChnNo);
        return 1;
    }
    
    pCurrent=&g_SessionMng[SessionId];
    for (int i = 4; i < AttrNum; i ++)
    {
    	  ReturnType = CurRule.Attribute[i].ReturnType;
        ReturnVar = (UC)CurRule.Attribute[i].ReturnVar;
        
        if (ReturnType == 2) //2-消息結果參數
        {
            Result = atoi(DBMsg.GetAttrValue(i).C_Str());
            
            if (FlwNo>=0 && FlwNo<MAX_FLOW_NUM)
            {
              if (g_RunFlwMng[FlwNo].DispLogId == 1)
                MyTrace(5, "Recv DB event SessionId=%ld EventId=%d EventName=%s", SerialNo, Result, Get_EventName_By_EventId(Result));
            }
            
            switch (ReturnVar)
            {
                case 1: //MsgEvent
                {
                    pCurrent->EventVars[UC_MsgEvent] = Result;
                    break;
                }
                case 8: //DBEvent 
                {
                    pCurrent->EventVars[UC_DBEvent] = Result;
                    break;
                }  
            }
        }
        else if (ReturnType == 3) //3-返回值參數
        {
            if (SerialNo == 0)
                return 1;
         
            FlwNo = (UC)(Flw_CmdAddr>>24);
            FlwId = (UC)(Flw_CmdAddr>>16);
            CmdAddr=(US)Flw_CmdAddr;
            
            if(!g_RunFlwMng.IsFlwAddrOK(FlwNo,FlwId,CmdAddr))
            {
                MyTrace(5, "ChnType=%d ChnNo=%d DB recv CmdAddr=%d is out of range SerialNo = %ld MsgId = %d", ChnType, ChnNo, CmdAddr, SerialNo, MsgId);
                return 1;
            }
       			
       			pCurrentCmd=&g_RunFlwMng[FlwNo].Flw[FlwId].RunCmds[CmdAddr]; //設定當前指令(注意發送CmdAddr時的流程可能不是當前流程)
            
         //   strcpy(VarName,	g_RunFlwMng[FlwNo].Flw[FlwId].RunCmds[CmdAddr].ParamValues[ReturnVar].C_Str());
          try
          {
            if (MsgId == MSG_ondbrecord && i == 5)
            {
              memset(recordlist, 0, 1920);
              memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
              strncpy(recordlist, DBMsg.GetAttrValue(i).C_Str(), 1920);
              for (n = 0; n < strlen(recordlist) && n < 1920; n ++)
              {
                ch = recordlist[n];
                if (ch == '`' || ch == '\0')
                {
                  Assign_pCurrent_Str_To_Var(0, pCurrentCmd->ParamValues[ReturnVar+j].C_Str(), fieldvalue) ;
                  j ++;
                  k = 0;
                  memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
                  if (j >= 32) //edit 2010-10-22
                    break;
                }
                else
                {
                  if (k < MAX_VAR_DATA_LEN)
                  {
                    fieldvalue[k] = ch;
                    k ++;
                  }
                }
              }
            }
            else if (MsgId == MSG_ondbselect && i == 6)
            {
              memset(recordlist, 0, 1920);
              memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
              strncpy(recordlist, DBMsg.GetAttrValue(i).C_Str(), 1920);
              for (n = 0; n < strlen(recordlist) && n < 1920; n ++)
              {
                ch = recordlist[n];
                if (ch == '`' || ch == '\0')
                {
                  Assign_pCurrent_Str_To_Var(0, pCurrentCmd->ParamValues[ReturnVar+j].C_Str(), fieldvalue) ;
                  j ++;
                  k = 0;
                  memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
                  if (j >= 21) //edit 2015-06-27
                    break;
                }
                else
                {
                  if (k < MAX_VAR_DATA_LEN)
                  {
                    fieldvalue[k] = ch;
                    k ++;
                  }
                }
              }
            }
            else if (MsgId == MSG_ondbspoutparam && i == 4)
            {
              memset(recordlist, 0, 1920);
              memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
              strncpy(recordlist, DBMsg.GetAttrValue(i).C_Str(), 1920);
              for (n = 0; n < strlen(recordlist) && n < 1920; n ++)
              {
                ch = recordlist[n];
                if (ch == '`' || ch == '\0')
                {
                  Assign_pCurrent_Str_To_Var(0, pCurrentCmd->ParamValues[ReturnVar+j].C_Str(), fieldvalue) ;
                  j ++;
                  k = 0;
                  memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
                  if (j >= 21)
                    break;
                }
                else
                {
                  if (k < MAX_VAR_DATA_LEN)
                  {
                    fieldvalue[k] = ch;
                    k ++;
                  }
                }
              }
            }
            else if (MsgId == MSG_ongetwebservicestr && i == 4)
            {
              //2014-01-07
              memset(recordlist, 0, 1920);
              memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
              strncpy(recordlist, DBMsg.GetAttrValue(i).C_Str(), 1920);
              for (n = 0; n <= strlen(recordlist) && n < 1920; n ++)
              {
                ch = recordlist[n];
                if (ch == '`' || ch == '\0')
                {
                  Assign_pCurrent_Str_To_Var(j, pCurrentCmd->ParamValues[ReturnVar].C_Str(), fieldvalue) ;
                  j ++;
                  k = 0;
                  memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
                  if (j >= 31)
                    break;
                }
                else
                {
                  if (k < MAX_VAR_DATA_LEN)
                  {
                    fieldvalue[k] = ch;
                    k ++;
                  }
                }
              }
            }
            else if (MsgId == MSG_ongetwebservicejson && i == 5)
            {
              memset(recordlist, 0, 1920);
              memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
              strncpy(recordlist, DBMsg.GetAttrValue(i).C_Str(), 1920);
              for (n = 0; n <= strlen(recordlist) && n < 1920; n ++)
              {
                ch = recordlist[n];
                if (ch == '`' || ch == '\0')
                {
                  Assign_pCurrent_Str_To_Var(0, pCurrentCmd->ParamValues[ReturnVar+j].C_Str(), fieldvalue) ;
                  j ++;
                  k = 0;
                  memset(fieldvalue, 0, MAX_VAR_DATA_LEN);
                  if (j >= 15) //edit 2015-06-27
                    break;
                }
                else
                {
                  if (k < MAX_VAR_DATA_LEN)
                  {
                    fieldvalue[k] = ch;
                    k ++;
                  }
                }
              }
            }
            else
            {
              Assign_pCurrent_Str_To_Var(Offset, pCurrentCmd->ParamValues[ReturnVar].C_Str(), DBMsg.GetAttrValue(i)) ;
            }
          }
          catch (...)
          {
            //pCurrent->ClearAllEvents(); //edit 2007-09-15 del
            pCurrent->SetOnHangOnEvent(); //設置掛機
						//printf("Received Error DB return Message,onhook\n");
            return 1;
          } 
        }
        else if (ReturnType == 4) //4-返回的錯誤消息
        {
            if (SerialNo == 0)
                return 1;
            //strncpy(pCurrent->ErrorBuf, MsgData->AttrValue[i], MAX_ERRORBUF_LEN);
        }
        else if (ReturnType == 5) //5-返回的系統變量
        {
          if (ReturnVar == R_DBRecordRows)
          {
            pCurrent->DBRecordRows = atoi(DBMsg.GetAttrValue(i).C_Str());
            if (g_bVarId)
            {
              MyTrace(4, "FlwName=%s CmdAddr=%ld CmdName=ondbcmd VarName=$S_DBRecordRows; Value=%d", 
                pCurrent->GetRunFlw().FlwName.C_Str(), Flw_CmdAddr, pCurrent->DBRecordRows);
            }
          }
        }
    }
    return 0;
}

//向IVR發送放信號音消息然后釋放通道
void Send_PlayTone_Release_Chn(  UC ChnType, US ChnNo, US ToneType, US Times, UC ReleaseId)
{
    CStringX Msg; 
    Msg.Format( "<playtone sessionid='0' cmdaddr='0' chantype='%d' channo='%d' dtmfruleid='0' tonetype='%d' repeat='%d' release='%d'/>",
         ChnType, ChnNo, ToneType, Times, ReleaseId);
    
    g_TcpServer.IVRSendMessage(MSG_playtone,  Msg);
}
//發送設置會話序號消息 //Edit 2007-08-09
void Send_SetSessionNo(CSession *pSession)
{
	CStringX Msg;
  Msg.Format("<answer sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' answertype='9'/>", 
			pSession->SerialNo, pSession->GetFlw_CmdAddr(), pSession->ChnType, pSession->ChnNo);
  g_TcpServer.IVRSendMessage(MSG_answer, Msg);
}
//外部數據網關啟動呼叫事件
int Proc_ongwmakecall_Msg(  CXMLRcvMsg &IvrMsg)
{
    US SessionId;
    US FuncNo;
    US FlwNo;

    const CStringX &Caller=IvrMsg.GetAttrValue(1);
    const CStringX &Called=IvrMsg.GetAttrValue(2);
    FuncNo = atoi(IvrMsg.GetAttrValue(4).C_Str());
    
    pCurrent=0;
    
    if (!g_RunFlwMng.Get_FlwNo_By_FuncNo(FuncNo,FlwNo)) //找不到流程
    {
        MyTrace(5, "ongwmakecall FuncNo=%d CallerNo=%s CalledNo=%s the service is not open", 
        		FuncNo, Caller.C_Str(), Called.C_Str());
        return 1;
    }

    SessionId=g_SessionMng.Alloc_Idel_SessionId( );
    if (SessionId)
    {
      //找到空閑的會話資源
			pCurrent=&g_SessionMng[SessionId];
			pCurrent->Init(g_bCmdId,//如果號碼匹配，可以跟蹤
										 0,0,CALL_IN,
										 Caller,Called,CStringX(""),
										 0,
										 CStringX(""),
										 FlwNo,g_RunFlwMng[FlwNo].FlwId
										);
      pCurrent->GWSerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
      pCurrent->GWAccount = IvrMsg.GetAttrValue(3);
      pCurrent->GWMsgType = atoi(IvrMsg.GetAttrValue(5).C_Str());
      pCurrent->GWMsg = IvrMsg.GetAttrValue(6);
      
      if (g_bVarId || g_nSendTraceMsgToLOG)
      {
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SessionNo; Value=%ld(0x%08x)", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SerialNo, pCurrent->SerialNo);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_GWSerialNo; Value=%ld(0x%08x)", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->GWSerialNo, pCurrent->GWSerialNo);
        
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CallerNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CallerNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_CalledNo; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->CalledNo.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_GWAccount; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->GWAccount.C_Str());
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_GWMsgType; Value=%d", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->GWMsgType);
        MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_GWMsg; Value=%s", 
          pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->GWMsg.C_Str());
      }
      SendTotalSessionsToAllLOG();
      SendOneFLWSessionsToAllLOG(pCurrent->FlwNo);
    }
    else
    {
      //沒有空閑的會話資源,發送釋放該通道命令
      MyTrace(5, "ongwmakecall FuncNo=%d CallerNo=%s CalledNo=%s not idel sessionid", 
        FuncNo, Caller.C_Str(), Called.C_Str());
    
    }
    return 0;
}
//外部數據網關向流程發送消息事件
int Proc_ongwmsgevent_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_DBGWMSGEvent] = OnDWMsg;
  pCurrent->GWMsgType = atoi(IvrMsg.GetAttrValue(2).C_Str());
  pCurrent->GWMsg = IvrMsg.GetAttrValue(3);
  return 0;
}
//電腦坐席發給流程會話的消息
int Proc_onsendflwmsg_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = atoi(IvrMsg.GetAttrValue(4).C_Str());
  pCurrent->AGMsgType = atoi(IvrMsg.GetAttrValue(5).C_Str());
  pCurrent->AGMsg = IvrMsg.GetAttrValue(6);
  return 0;
}
//電腦坐席快速轉接電話
int Proc_onagblindtrancall_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGBlindTran;
  pCurrent->AGPhoneType = atoi(IvrMsg.GetAttrValue(4).C_Str());
  pCurrent->AGTranPhone = IvrMsg.GetAttrValue(5);
  pCurrent->AGTranParam = IvrMsg.GetAttrValue(6);
  
  if (g_bVarId || g_nSendTraceMsgToLOG)
  {
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agblindtrancall VarName=$S_AGTranType; Value=%d", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGPhoneType);
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agblindtrancall VarName=$S_AGTranPhone; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranPhone.C_Str());
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agblindtrancall VarName=$S_AGTranParam; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranParam.C_Str());
  }
  return 0;
}
//電腦坐席協商轉接電話
int Proc_onagconsulttrancall_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGConsultTran;
  pCurrent->AGPhoneType = atoi(IvrMsg.GetAttrValue(4).C_Str());
  pCurrent->AGTranPhone = IvrMsg.GetAttrValue(5);
  pCurrent->AGTranParam = IvrMsg.GetAttrValue(6);
  
  if (g_bVarId || g_nSendTraceMsgToLOG)
  {
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconsulttrancall VarName=$S_AGTranType; Value=%d", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGPhoneType);
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconsulttrancall VarName=$S_AGTranPhone; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranPhone.C_Str());
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconsulttrancall VarName=$S_AGTranParam; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranParam.C_Str());
  }
  return 0;
}
//電腦坐席會議轉接電話
int Proc_onagconftrancall_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGConfTran;
  pCurrent->AGPhoneType = atoi(IvrMsg.GetAttrValue(4).C_Str());
  pCurrent->AGTranPhone = IvrMsg.GetAttrValue(5);
  pCurrent->AGTranConfNo = atoi(IvrMsg.GetAttrValue(6).C_Str());
  pCurrent->AGTranParam = IvrMsg.GetAttrValue(7);
  
  if (g_bVarId || g_nSendTraceMsgToLOG)
  {
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconftrancall VarName=$S_AGTranType; Value=%d", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGPhoneType);
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconftrancall VarName=$S_AGTranPhone; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranPhone.C_Str());
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconftrancall VarName=$S_AGTranConfNo; Value=%d", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranConfNo);
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agconftrancall VarName=$S_AGTranParam; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranParam.C_Str());
  }
  return 0;
}
//坐席停止轉接來話
int Proc_onagstoptrancall_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGStopTran;
  return 0;
}
//電腦坐席指定通道會議發言
int Proc_onagconfspeak_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGConfSpeak;
  return 0;
}
//電腦坐席指定通道旁聽會議
int Proc_onagconfaudit_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGConfAudit;
  return 0;
}
//坐席將來話轉接到ivr流程
int Proc_onagtranivr_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  pCurrent->EventVars[UC_AGMSGEvent] = OnAGTranIVR;
  pCurrent->AGTranIVRId = atoi(IvrMsg.GetAttrValue(4).C_Str());
  pCurrent->AGReturnId = atoi(IvrMsg.GetAttrValue(5).C_Str());
  pCurrent->AGTranParam = IvrMsg.GetAttrValue(6);
  
  if (g_bVarId || g_nSendTraceMsgToLOG)
  {
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agtranivr VarName=$S_AGTranIVRId; Value=%d", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranIVRId);
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agtranivr VarName=$S_AGReturnSeatId; Value=%d", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGReturnId);
    MyTrace(4, "FlwName=%s SessionNo=%ld CmdName=agtranivr VarName=$S_AGTranParam; Value=%s", 
      pCurrent->GetRunFlw().FlwName.C_Str(), SerialNo, pCurrent->AGTranParam.C_Str());
  }
  return 0;
}
//電腦坐席發給流程會話的保持/取消保持消息
int Proc_onaghold_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  if (atoi(IvrMsg.GetAttrValue(4).C_Str()) == 1)
  {
    pCurrent->EventVars[UC_AGMSGEvent] = OnAGHold;
  }
  else
  {
    pCurrent->EventVars[UC_AGMSGEvent] = OnAGUnHold;
  }
  pCurrent->AGTranParam = IvrMsg.GetAttrValue(5);
  return 0;
}
//電腦坐席發給流程會話的靜音/取消靜音消息
int Proc_onagmute_Msg(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  if (atoi(IvrMsg.GetAttrValue(4).C_Str()) == 1)
  {
    pCurrent->EventVars[UC_AGMSGEvent] = OnAGMute;
  }
  else
  {
    pCurrent->EventVars[UC_AGMSGEvent] = OnAGUnMute;
  }
  return 0;
}
int Proc_onrouteideltrks(CXMLRcvMsg &IvrMsg)
{
  int RouteNo = atoi(IvrMsg.GetAttrValue(4).C_Str());
  int IdelTrkNum = atoi(IvrMsg.GetAttrValue(5).C_Str());
  if (RouteNo >= 0 && RouteNo < MAX_ROUTE_NUM)
  {
    g_SessionMng.RouteIdelTrkNum[RouteNo] = IdelTrkNum;
  }
  return 0;
}
int Proc_onbandagentchn(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  if (atoi(IvrMsg.GetAttrValue(10).C_Str()) == OnFail)
  {
    pCurrent->EventVars[UC_OnEvent] = OnFail;
  }
  else
  {
    pCurrent->ChHWType = atoi(IvrMsg.GetAttrValue(4).C_Str());
    pCurrent->ChHWNo = atoi(IvrMsg.GetAttrValue(5).C_Str());
    pCurrent->SeatType = atoi(IvrMsg.GetAttrValue(6).C_Str());
    pCurrent->SeatNo = IvrMsg.GetAttrValue(7);
    pCurrent->WorkerNo = atoi(IvrMsg.GetAttrValue(8).C_Str());
    pCurrent->GroupNo = atoi(IvrMsg.GetAttrValue(9).C_Str());
    
    if (g_bVarId || g_nSendTraceMsgToLOG)
    {
      MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChHWType; Value=%d", 
        pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChHWType);
      MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_ChHWNo; Value=%d", 
        pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->ChHWNo);
      
      MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SeatType; Value=%d", 
        pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SeatType);
      MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_SeatNo; Value=%s", 
        pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->SeatNo.C_Str());
      MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_WorkerNo; Value=%d", 
        pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->WorkerNo);
      MyTrace(4, "FlwName=%s CmdAddr=0 CmdName=mainform VarName=$S_GroupNo; Value=%d", 
        pCurrent->GetRunFlw().FlwName.C_Str(), pCurrent->GroupNo);
    }

    pCurrent->EventVars[UC_OnEvent] = OnSuccess;
  }
  return 0;
}
//收到變化了的新的呼叫相關參數
int Proc_onnewcallparam(CXMLRcvMsg &IvrMsg)
{
  UL SerialNo = atol(IvrMsg.GetAttrValue(0).C_Str());
  US SessionId = (US)SerialNo;
  
  pCurrent=0;
  if(!g_SessionMng.IsSessionNoAvail(SessionId))
  {
    return 1;
  }
  
  pCurrent=&g_SessionMng[SessionId];
  if (IvrMsg.GetAttrValue(4).GetLength() > 0)
  {
    pCurrent->CallerNo = IvrMsg.GetAttrValue(4);
  }
  if (IvrMsg.GetAttrValue(5).GetLength() > 0)
  {
    pCurrent->CalledNo = IvrMsg.GetAttrValue(5);
  }
  if (IvrMsg.GetAttrValue(6).GetLength() > 0)
  {
    pCurrent->WorkerNo = atoi(IvrMsg.GetAttrValue(6).C_Str());
  }
  if (IvrMsg.GetAttrValue(7).GetLength() > 0)
  {
    pCurrent->CDRSerialNo = IvrMsg.GetAttrValue(7);
  }
  if (IvrMsg.GetAttrValue(8).GetLength() > 0)
  {
    pCurrent->DialParam = IvrMsg.GetAttrValue(8);
  }
  return 0;
}

//-----------------------------------------------------------------------------

static CXMLSndMsg SendLOGMsg;

void Set_FLW2LOG_Header(int MsgId, int nLOG)
{
  SendLOGMsg.GetBuf().Format("<%s logid='%ld'", g_FlwRuleMng.IVRLOGMsgRule[MsgId].MsgNameEng, LogerStatusSet[nLOG].LogerClientId);
}
//增加發送到AG的消息屬性
void Set_FLW2LOG_Item(int MsgId, int AttrId, const char *value)
{
  SendLOGMsg.AddItemToBuf(g_FlwRuleMng.IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_FLW2LOG_Item(int MsgId, int AttrId, const CStringX &value)
{
  SendLOGMsg.AddItemToBuf(g_FlwRuleMng.IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_FLW2LOG_IntItem(int MsgId, int AttrId, int value)
{
  SendLOGMsg.AddIntItemToBuf(g_FlwRuleMng.IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_FLW2LOG_LongItem(int MsgId, int AttrId, long value)
{
  SendLOGMsg.AddLongItemToBuf(g_FlwRuleMng.IVRLOGMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_FLW2LOG_Tail()
{
  SendLOGMsg.AddTailToBuf();
}
//發送登錄結果結果消息
void SendLoginResultToLOG(int nLOG, int result, const char *error)
{
  Set_FLW2LOG_Header(LOGMSG_onlogin, nLOG);
  Set_FLW2LOG_IntItem(LOGMSG_onlogin, 1, 2);
  Set_FLW2LOG_IntItem(LOGMSG_onlogin, 2, result);
  Set_FLW2LOG_Item(LOGMSG_onlogin, 3, error);
  Set_FLW2LOG_Tail();
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onlogin, SendLOGMsg.GetBuf());
}
//發送通用結果消息
void SendCommonResultToLOG(int nLOG, int onMsgId, int result, const char *error)
{
  Set_FLW2LOG_Header(onMsgId, nLOG);
  Set_FLW2LOG_IntItem(onMsgId, 1, result);
  Set_FLW2LOG_Item(onMsgId, 2, error);
  Set_FLW2LOG_Tail();
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, onMsgId, SendLOGMsg.GetBuf());
}
void SendMonitorONOFFStatus(int nLOG)
{
  Set_FLW2LOG_Header(LOGMSG_onflwonoff, nLOG);
  Set_FLW2LOG_IntItem(LOGMSG_onflwonoff, 1, LogerStatusSet[nLOG].nGetLoadFLW);
  Set_FLW2LOG_IntItem(LOGMSG_onflwonoff, 2, LogerStatusSet[nLOG].nGetFLWMsg);
  Set_FLW2LOG_Tail();
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onflwonoff, SendLOGMsg.GetBuf());
}
void SendSaveMsgONOFFStatus(int nLOG)
{
  Set_FLW2LOG_Header(LOGMSG_onsetflwsavemsgid, nLOG);
  Set_FLW2LOG_IntItem(LOGMSG_onsetflwsavemsgid, 1, g_bAlartId?1:0);
  Set_FLW2LOG_IntItem(LOGMSG_onsetflwsavemsgid, 2, g_bCommId?1:0);
  Set_FLW2LOG_IntItem(LOGMSG_onsetflwsavemsgid, 3, g_bCmdId?1:0);
  Set_FLW2LOG_IntItem(LOGMSG_onsetflwsavemsgid, 4, g_bVarId?1:0);
  Set_FLW2LOG_Tail();
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onsetflwsavemsgid, SendLOGMsg.GetBuf());
}
void SendAlarmMsg(int nLOG, int nAlarmCode, int nAlarlLevel, int nAlarmOnOff, const char *pszAlarmMsg)
{
  Set_FLW2LOG_Header(LOGMSG_onivralarmmsg, nLOG);
  Set_FLW2LOG_IntItem(LOGMSG_onivralarmmsg, 1, nAlarmCode);
  Set_FLW2LOG_IntItem(LOGMSG_onivralarmmsg, 2, nAlarlLevel);
  Set_FLW2LOG_IntItem(LOGMSG_onivralarmmsg, 3, nAlarmOnOff);
  Set_FLW2LOG_Item(LOGMSG_onivralarmmsg, 4, pszAlarmMsg);
  Set_FLW2LOG_Tail();
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onivralarmmsg, SendLOGMsg.GetBuf());
}
void SendLineMsg(int nLOG, const char *pszLineMsg, int nLineId)
{
  Set_FLW2LOG_Header(LOGMSG_onrecvtxtline, nLOG);
  Set_FLW2LOG_IntItem(LOGMSG_onrecvtxtline, 1, 2);
  Set_FLW2LOG_Item(LOGMSG_onrecvtxtline, 2, pszLineMsg);
  Set_FLW2LOG_IntItem(LOGMSG_onrecvtxtline, 3, nLineId);
  Set_FLW2LOG_Tail();
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onrecvtxtline, SendLOGMsg.GetBuf());
}
void SendINIFile(int nLOG)
{
  FILE *fp1;
  char szLineMsg[256];
  
  fp1 = fopen(g_szServiceINIFileName, "r");
  if (fp1 != NULL)
  {
    SendLineMsg(nLOG, "", 0);
    while (!feof(fp1))
    {
      if (fgets(szLineMsg, 256, fp1) > 0)
      {
        SendLineMsg(nLOG, szLineMsg, 1);
      }
    }
    SendLineMsg(nLOG, "", 2);
    fclose(fp1);
  }
}
//-----------------------------------------------------------------------------
void SendOneFLWSessionsToAllLOG(int FlwNo)
{
  if (FlwNo >= MAX_FLOW_NUM)
  {
    return;
  }
  SendOneFLWSessionsToAllLOG(&g_RunFlwMng[FlwNo]);
}
void SendOneFLWSessionsToAllLOG(CRunFlw *pRunFlw)
{
  if (pRunFlw == NULL)
  {
    return;
  }
  Set_FLW2LOG_Header(LOGMSG_onsessionscount, 0);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 1, pRunFlw->FlwNo);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 2, pRunFlw->GetOnLines());
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 3, pRunFlw->AutoStartId);
  Set_FLW2LOG_Tail();  
  //MyTrace(2, "%s", SendLOGMsg.GetBuf());
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].nGetLoadFLW == 1)
      g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onsessionscount, SendLOGMsg.GetBuf());
  }
}
void SendOneFLWSessionsToLOG(int nLOG, CRunFlw *pRunFlw)
{
  if (pRunFlw == NULL)
  {
    return;
  }
  Set_FLW2LOG_Header(LOGMSG_onsessionscount, 0);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 1, pRunFlw->FlwNo);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 2, pRunFlw->GetOnLines());
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 3, pRunFlw->AutoStartId);
  Set_FLW2LOG_Tail();  
  //MyTrace(2, "%s", SendLOGMsg.GetBuf());
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onsessionscount, SendLOGMsg.GetBuf());
}
void SendAllFLWSessionsToLOG(int nLOG)
{
  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].nGetLoadFLW == 1)
  {
    for (int i = 0; i < MAX_FLOW_NUM; i ++)
    {
      if (&g_RunFlwMng[i])
      {
        SendOneFLWSessionsToLOG(nLOG, &g_RunFlwMng[i]);
      }
    }
  }
}
void SendAllFLWSessionsToAllLOG()
{
  for (int i = 0; i < MAX_FLOW_NUM; i ++)
  {
    if (&g_RunFlwMng[i])
    {
      SendOneFLWSessionsToAllLOG(&g_RunFlwMng[i]);
    }
  }
}

void SendTotalSessionsToAllLOG()
{
  Set_FLW2LOG_Header(LOGMSG_onsessionscount, 0);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 1, 65535);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 2, g_SessionMng.GetUsedSessionNum());
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 3, 0);
  Set_FLW2LOG_Tail(); 
  //MyTrace(2, "%s", SendLOGMsg.GetBuf());
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].nGetLoadFLW == 1)
      g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onsessionscount, SendLOGMsg.GetBuf());
  }
}
void SendTotalSessionsToLOG(int nLOG)
{
  Set_FLW2LOG_Header(LOGMSG_onsessionscount, 0);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 1, 65535);
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 2, g_SessionMng.GetUsedSessionNum());
  Set_FLW2LOG_IntItem(LOGMSG_onsessionscount, 3, 0);
  Set_FLW2LOG_Tail();  
  //MyTrace(2, "%s", SendLOGMsg.GetBuf());
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_onsessionscount, SendLOGMsg.GetBuf());
}
//-----------------------------------------------------------------------------

void SendOneLoadFLWToAllLOG(const CRunFlw *pRunFlw)
{
  CStringX strAccessCodes;
  
  if (pRunFlw == NULL)
  {
    return;
  }
  strAccessCodes=pRunFlw->AccessCodes[0];
  for(unsigned int i=1;i<pRunFlw->AccessNum;i++) //連接所有參數
  {
    strAccessCodes+=',';
    strAccessCodes+=pRunFlw->AccessCodes[i];
  }
  
  Set_FLW2LOG_Header(LOGMSG_ongetloadedflws, 0);
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 1, pRunFlw->FlwNo);	
  Set_FLW2LOG_Item(LOGMSG_ongetloadedflws, 2, pRunFlw->FlwName);	
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 3, pRunFlw->FuncGroup);	
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 4, pRunFlw->FuncNo);	
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 5, pRunFlw->Flw[pRunFlw->FlwId].TotalCmds);	
  Set_FLW2LOG_Item(LOGMSG_ongetloadedflws, 6, strAccessCodes);	
  Set_FLW2LOG_Item(LOGMSG_ongetloadedflws, 7, pRunFlw->LoadTime);	
  Set_FLW2LOG_Tail();  
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].nGetLoadFLW == 1)
      g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_ongetloadedflws, SendLOGMsg.GetBuf());
  }
}
void SendOneLoadFLWToLOG(int nLOG, const CRunFlw *pRunFlw)
{
  CStringX strAccessCodes;

  strAccessCodes=pRunFlw->AccessCodes[0];
  for(unsigned int i=1;i<pRunFlw->AccessNum;i++) //連接所有參數
  {
    strAccessCodes+=',';
    strAccessCodes+=pRunFlw->AccessCodes[i];
  }
  
  Set_FLW2LOG_Header(LOGMSG_ongetloadedflws, nLOG);
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 1, pRunFlw->FlwNo);	
  Set_FLW2LOG_Item(LOGMSG_ongetloadedflws, 2, pRunFlw->FlwName);	
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 3, pRunFlw->FuncGroup);	
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 4, pRunFlw->FuncNo);	
  Set_FLW2LOG_IntItem(LOGMSG_ongetloadedflws, 5, pRunFlw->Flw[pRunFlw->FlwId].TotalCmds);	
  Set_FLW2LOG_Item(LOGMSG_ongetloadedflws, 6, strAccessCodes);	
  Set_FLW2LOG_Item(LOGMSG_ongetloadedflws, 7, pRunFlw->LoadTime);	
  Set_FLW2LOG_Tail();  
  g_TcpServer.LOGSendMessage(LogerStatusSet[nLOG].LogerClientId, LOGMSG_ongetloadedflws, SendLOGMsg.GetBuf());
}
void SendAllLoadFLWToLOG(int nLOG)
{
  if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].nGetLoadFLW == 1)
  {
    for (int i = 0; i < MAX_FLOW_NUM; i ++)
    {
      if (&g_RunFlwMng[i])
      {
        SendOneLoadFLWToLOG(nLOG, &g_RunFlwMng[i]);     
      }
    }
  }
}
void CheckLOGTraceMsgId()
{
  g_nSendTraceMsgToLOG = false;
  for (int nLOG = 0; nLOG < MAX_LOGNODE_NUM; nLOG ++)
  {
    if (LogerStatusSet[nLOG].nLoginId == 2 && LogerStatusSet[nLOG].nGetFLWMsg == 1)
    {
      g_nSendTraceMsgToLOG = true;
    }
  }
}
void Proc_Msg_From_LOG(CXMLRcvMsg &LOGMsg)
{		  
  char pszFileName[256];
  unsigned short MsgId=LOGMsg.GetMsgId();
  int nLOG, nOpenFlag, nFlwNo, nMinLen, nMaxLen;
  int nResult;
  
  if (MsgId >= MAX_LOGIVRMSG_NUM)
  {
    //指令編號超出范圍   
    MyTrace(5, "LOGmsgid = %d is out of range", MsgId);
    return ;
  }
  
  if(0!=LOGMsg.ParseSndMsgWithCheck(g_FlwRuleMng.LOGIVRMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(5, "Recv msg From loger is error: MsgId = %d MsgBuf = %s", MsgId, LOGMsg.GetBuf().C_Str());
    return;
  }
  
  nLOG = 0;
  for (int i = 0; i < MAX_LOGNODE_NUM; i ++)
  {
    if (LogerStatusSet[i].LogerClientId == atoi(LOGMsg.GetAttrValue(0).C_Str()))
    {
      nLOG = i;
      break;
    }
  }
  MyTrace(0, "LOG Msg nLOG=%d ClientId=%d", nLOG, atoi(LOGMsg.GetAttrValue(0).C_Str()));
  if (nLOG >= MAX_LOGNODE_NUM)
    return;
  switch (MsgId)
  {
  case LOGMSG_login:	//登錄
    LogerStatusSet[nLOG].nLoginId = 2;
    SendLoginResultToLOG(nLOG, OnSuccess, "");
    if (atoi(LOGMsg.GetAttrValue(3).C_Str()) == 1)
      SendINIFile(nLOG);
    
    CheckLOGTraceMsgId();
    SendMonitorONOFFStatus(nLOG);
    SendSaveMsgONOFFStatus(nLOG);
    break;
  case LOGMSG_logout:	//登出
    LogerStatusSet[nLOG].nLoginId = 1;
    break;
  case LOGMSG_getloadedflws:	//取已加載的流程信息
    nOpenFlag = atoi(LOGMsg.GetAttrValue(1).C_Str());
    LogerStatusSet[nLOG].nGetLoadFLW = nOpenFlag;
    if (nOpenFlag == 1)
    {
      SendAllLoadFLWToLOG(nLOG);
      SendAllFLWSessionsToLOG(nLOG);
      SendTotalSessionsToLOG(nLOG);
    }
    break;
  case LOGMSG_getflwtracemsg:	//取flw日志跟蹤信息
    LogerStatusSet[nLOG].nGetFLWMsg = atoi(LOGMsg.GetAttrValue(1).C_Str());
    CheckLOGTraceMsgId();
    break;
  case LOGMSG_loadflw:	//加載業務流程
    sprintf(pszFileName, "%s/%s", g_szFlwPath, LOGMsg.GetAttrValue(1).C_Str());
    nResult = g_RunFlwMng.AutoLoadFlw(pszFileName, atoi(LOGMsg.GetAttrValue(2).C_Str()), atoi(LOGMsg.GetAttrValue(3).C_Str()), nFlwNo);
    if (nResult == 0)
    {
      SendCommonResultToLOG(nLOG, LOGMSG_onloadflw, OnSuccess, "");
      SendOneLoadFLWToAllLOG(&g_RunFlwMng[nFlwNo]);
      SendOneFLWSessionsToAllLOG(&g_RunFlwMng[nFlwNo]);
      SendTotalSessionsToAllLOG();
    }
    else 
    {
      if (nResult == 1)
        SendCommonResultToLOG(nLOG, LOGMSG_onloadflw, OnFail, "對不起，該流程文件不存在!!!");
      else
      {
        SendCommonResultToLOG(nLOG, LOGMSG_onloadflw, OnFail, "對不起，該流程的主備緩沖均有通道在線，暫不能加載該流程，等備用緩沖通道釋放后再加載!!!");
      }
    }
    break;
  case LOGMSG_addaccesscode:	//添加接入號碼
    nFlwNo = atoi(LOGMsg.GetAttrValue(1).C_Str());
    if (nFlwNo >= MAX_FLOW_NUM)
    {
      SendCommonResultToLOG(nLOG, LOGMSG_onaddaccesscode, OnFail, "流程序號超出范圍");
      break;
    }
    if (&g_RunFlwMng[nFlwNo] == NULL)
    {
      SendCommonResultToLOG(nLOG, LOGMSG_onaddaccesscode, OnFail, "該流程未加載");
      break;
    }
    nMinLen = atoi(LOGMsg.GetAttrValue(3).C_Str());
    nMaxLen = atoi(LOGMsg.GetAttrValue(4).C_Str());
    g_RunFlwMng[nFlwNo].Add_AccessCodes(LOGMsg.GetAttrValue(2).C_Str(), nMinLen, nMaxLen);
    SendCommonResultToLOG(nLOG, LOGMSG_onaddaccesscode, OnSuccess, "");
    SendOneLoadFLWToAllLOG(&g_RunFlwMng[nFlwNo]);
    break;
  case LOGMSG_delaccesscode:	//刪除接入號碼
    nFlwNo = atoi(LOGMsg.GetAttrValue(1).C_Str());
    if (nFlwNo >= MAX_FLOW_NUM)
    {
      SendCommonResultToLOG(nLOG, LOGMSG_ondelaccesscode, OnFail, "流程序號超出范圍");
      break;
    }
    if (&g_RunFlwMng[nFlwNo] == NULL)
    {
      SendCommonResultToLOG(nLOG, LOGMSG_ondelaccesscode, OnFail, "該流程未加載");
      break;
    }
    g_RunFlwMng[nFlwNo].Del_AccessCodes(LOGMsg.GetAttrValue(2).C_Str());
    SendCommonResultToLOG(nLOG, LOGMSG_ondelaccesscode, OnSuccess, "");
    SendOneLoadFLWToAllLOG(&g_RunFlwMng[nFlwNo]);
    break;
  case LOGMSG_setflwsavemsgid:
    if (atoi(LOGMsg.GetAttrValue(1).C_Str()) == 0)
    {
      g_bAlartId=false;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "AlartId", "0", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "AlartId", "0", g_szServiceINIFileName);
    }
    else
    {
      g_bAlartId=true;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "AlartId", "1", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "AlartId", "1", g_szServiceINIFileName);
    }
    if (atoi(LOGMsg.GetAttrValue(2).C_Str()) == 0)
    {
      g_bCommId=false;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "CommId", "0", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "CommId", "0", g_szServiceINIFileName);
    }
    else
    {
      g_bCommId=true;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "CommId", "1", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "CommId", "1", g_szServiceINIFileName);
    }
    if (atoi(LOGMsg.GetAttrValue(3).C_Str()) == 0)
    {
      g_bCmdId=false;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "CmdId", "0", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "CmdId", "0", g_szServiceINIFileName);
    }
    else
    {
      g_bCmdId=true;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "CmdId", "1", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "CmdId", "1", g_szServiceINIFileName);
    }
    if (atoi(LOGMsg.GetAttrValue(4).C_Str()) == 0)
    {
      g_bVarId=false;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "VarId", "0", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "VarId", "0", g_szServiceINIFileName);
    }
    else
    {
      g_bVarId=true;
      if (g_nINIFileType == 0)
        WritePrivateProfileString("LOG", "VarId", "1", g_szServiceINIFileName);
      else
        WritePrivateProfileString("FLWLOG", "VarId", "1", g_szServiceINIFileName);
    }

    (g_bCommId==true || g_bCmdId==true || g_bVarId==true) ? g_bSaveId = true : g_bSaveId = false;
    break;
  case LOGMSG_modifyiniparam:
    WritePrivateProfileString((char *)LOGMsg.GetAttrValue(2).C_Str(), 
      (char *)LOGMsg.GetAttrValue(3).C_Str(), 
      (char *)LOGMsg.GetAttrValue(4).C_Str(), g_szServiceINIFileName);
    break;
  case LOGMSG_autoflwonoff:
    nFlwNo = atoi(LOGMsg.GetAttrValue(1).C_Str());
    if (atoi(LOGMsg.GetAttrValue(2).C_Str()) == 0)
    {
      MyTrace(5, "LOGMSG_autoflwonoff nFlwNo=%d stop", nFlwNo);
      g_SessionMng.StopAutoFlw(nFlwNo);
      g_RunFlwMng.StopAutoRunFlw(nFlwNo);
      SendOneFLWSessionsToAllLOG(nFlwNo);
      SendTotalSessionsToAllLOG();
    } 
    else
    {
      MyTrace(5, "LOGMSG_autoflwonoff nFlwNo=%d run", nFlwNo);
      g_RunFlwMng.ReRunAutoRunFlw(nFlwNo);
      SendOneFLWSessionsToAllLOG(nFlwNo);
      SendTotalSessionsToAllLOG();
    }
    break;
  }
}
