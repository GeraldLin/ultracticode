// TCPClient.cpp: implementation of the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TcpServer.h"
#include "extern.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

CTCPServer::CTCPServer()
{
	hdll = NULL;
  isLOGInit = false;
  strcpy(DllFile, "tmtcpserver.dll");
  ClientId = 0x0201;
  LoadDll();
}

CTCPServer::~CTCPServer()
{
  ReleaseDll();
}

bool CTCPServer::LoadDll()
{
	hdll = LoadLibrary(DllFile);
	if ( hdll != NULL )
	{
    AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(!AppInitS || !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MessageBoxA(NULL, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MessageBoxA(NULL, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		return false;
	}
}

void CTCPServer::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}
//組合對象編碼
US CTCPServer::CombineObjectID( UC ObjectType, UC ObjectNo )
{
  US o_i1, o_i2;
  o_i1 = ( ObjectType << 8 ) & 0xFF00;
  o_i2 = ObjectNo & 0x00FF;
  return (o_i1 | o_i2);
}
//組合消息類型及消息編號
US CTCPServer::CombineMessageID(UC MsgType, UC MsgNo)
{
  US m_i1, m_i2;
  m_i1 = ( MsgType << 8 ) & 0xFF00;
  m_i2 = MsgNo & 0x00FF;
  return (m_i1 | m_i2);
}
//分解對象標識及編號
void CTCPServer::GetClientID(US Msg_wParamHi, UC &ClientType, UC &ClientNo)
{
  UC C_T, C_N;
  C_T = ( Msg_wParamHi >> 8 ) & 0x00FF;
  C_N = Msg_wParamHi & 0x00FF;
  ClientType = C_T;
  ClientNo = C_N;
}
//分解消息類型及消息編號
void CTCPServer::GetMsgID(US Msg_wParamLo, UC &MsgType, UC &MsgNo)
{
  UC M_T, M_N;
  M_T = ( Msg_wParamLo >> 8 ) & 0x00FF;
  M_N = Msg_wParamLo & 0x00FF;
  MsgType = M_T;
  MsgNo = M_N;
}
//發送消息
int CTCPServer::SendMessage(US ServerId, US MsgId, int MsgLen, const CH *MsgBuf)
{
	int SendedLen;

  SendedLen = AppSendData(ServerId, MsgId, (UC *)MsgBuf, MsgLen);
  if (SendedLen != MsgLen)
    MyTrace(5, "Send msg error ServerId=%04x MsgId=%04x", ServerId, MsgId);
	return SendedLen;
}
int CTCPServer::CFGSendMessage(US MsgId, const CH *MsgBuf)
{
  MyTrace(2, MsgBuf);
  return SendMessage(CFGServer.Serverid, (MSGTYPE_CFG<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::IVRSendMessage(US MsgId, const CH *MsgBuf)
{
  MyTrace(2, MsgBuf);
  return SendMessage(IVRServer.Serverid, (MSGTYPE_IVRXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::IVRSendMessage(US MsgId, const CStringX &MsgBuf)
{
  MyTrace(2, "%s", MsgBuf.C_Str());
  return SendMessage(IVRServer.Serverid, (MSGTYPE_IVRXML<<8) | MsgId, MsgBuf.GetLength(), MsgBuf.C_Str());
}
int CTCPServer::DBSendMessage(US MsgId, const CH *MsgBuf)
{
  if ((MsgId&0xFF) != MSG_execsql || g_bDispIVRSQLId == true)
    MyTrace(2, MsgBuf);
  return SendMessage(DBServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::DBSendMessage1(US MsgId, const CH *MsgBuf)
{
  //MyTrace(2, MsgBuf);
  return SendMessage(DBServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::DBSendMessage(US MsgId, const CStringX &MsgBuf)
{
  if ((MsgId&0xFF) != MSG_execsql || g_bDispIVRSQLId == true)
    MyTrace(2, "%s", MsgBuf.C_Str());
  return SendMessage(DBServer.Serverid, (MSGTYPE_DBXML<<8) | MsgId, MsgBuf.GetLength(), MsgBuf.C_Str());
}
int CTCPServer::GWSendMessage(US gwid, US MsgId, const CH *MsgBuf)
{
  MyTrace(2, MsgBuf);
  return SendMessage(GWServer[gwid].Serverid, (MSGTYPE_DBXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::GWSendMessage1(US gwid, US MsgId, const CH *MsgBuf)
{
  //MyTrace(2, MsgBuf);
  return SendMessage(GWServer[gwid].Serverid, (MSGTYPE_DBXML<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::GWSendMessage(US gwid, US MsgId, const CStringX &MsgBuf)
{
  MyTrace(2, "%s", MsgBuf.C_Str());
  return SendMessage(GWServer[gwid].Serverid, (MSGTYPE_DBXML<<8) | MsgId, MsgBuf.GetLength(), MsgBuf.C_Str());
}

int CTCPServer::LOGSendMessage(US nLOG, US MsgId, const CH *MsgBuf)
{
  //MyTrace(2, MsgBuf);
  //printf("send log %s\n\n", MsgBuf);
  return SendMessage((NODE_LOG<<8)|nLOG, (MSGTYPE_IVRLOG<<8) | MsgId, strlen(MsgBuf), MsgBuf);
}
int CTCPServer::LOGSendMessage(US nLOG, US MsgId, const CStringX &MsgBuf)
{
  //MyTrace(2, MsgBuf.C_Str());
  //printf("%s\n", MsgBuf.C_Str());
  return SendMessage((NODE_LOG<<8)|nLOG, (MSGTYPE_IVRLOG<<8) | MsgId, MsgBuf.GetLength(), MsgBuf.C_Str());
}

bool CTCPServer::ReadIni(const char *filename)
{
  char pzSection[128];
  ClientId = CombineObjectID(NODE_XML, GetPrivateProfileInt("IVRSERVER", "ClientId", 1, filename));
  FLWServerPort = GetPrivateProfileInt("IVRSERVER", "FLWServerPort", 5229, filename);

  CFGServer.IsConnected=false;
  strcpy(CFGServer.ServerIP, "127.0.0.1");
  CFGServer.ServerPort = 5225;
  CFGServer.Serverid = 0;
  
  IVRServer.IsConnected=false;
  GetPrivateProfileString("IVRSERVER", "IVRServerIP", "127.0.0.1", IVRServer.ServerIP, 30, filename);
  IVRServer.ServerPort = GetPrivateProfileInt("IVRSERVER", "IVRServerPort", 5227, filename);
  IVRServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("IVRSERVER", "IVRServerId", 1, filename));
  
  DBServer.IsConnected=false;    
  GetPrivateProfileString("DBSERVER", "DBServerIP", "127.0.0.1", DBServer.ServerIP, 30, filename);
  DBServer.ServerPort = GetPrivateProfileInt("DBSERVER", "DBServerPort", 5228, filename);
  DBServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("DBSERVER", "DBServerId", 1, filename));
  
  for (int i = 0; i < MAX_GW_NUM; i ++)
  {
    sprintf(pzSection,"DBGW%d",i);
    GWServer[i].IsConnected=false;    
    GetPrivateProfileString(pzSection, "GWServerIP", "127.0.0.1", GWServer[i].ServerIP, 30, filename);
    GWServer[i].ServerPort = GetPrivateProfileInt(pzSection, "GWServerPort", 5000+i, filename);
    GWServer[i].Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt(pzSection, "GWServerId", 0, filename));
  }
  return true;
}    

bool CTCPServer::ReadWebSetupIni(const char *filename)
{
  char pzSection[128];
  ClientId = CombineObjectID(NODE_XML, GetPrivateProfileInt("TCPLINK", "FLWServerID", 1, filename));
  FLWServerPort = GetPrivateProfileInt("TCPLINK", "FLWServerPort", 5229, filename);

  CFGServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "CFGServerIP", "127.0.0.1", CFGServer.ServerIP, 30, filename);
  CFGServer.ServerPort = GetPrivateProfileInt("TCPLINK", "CFGServerPort", 5225, filename);
  CFGServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("TCPLINK", "CFGServerId", 1, filename));
  
  IVRServer.IsConnected=false;
  GetPrivateProfileString("TCPLINK", "IVRServerIP", "127.0.0.1", IVRServer.ServerIP, 30, filename);
  IVRServer.ServerPort = GetPrivateProfileInt("TCPLINK", "IVRServerPort", 5227, filename);
  IVRServer.Serverid = CombineObjectID(NODE_IVR,GetPrivateProfileInt("TCPLINK", "IVRServerId", 1, filename));
  
  DBServer.IsConnected=false;    
  GetPrivateProfileString("TCPLINK", "DBServerIP", "127.0.0.1", DBServer.ServerIP, 30, filename);
  DBServer.ServerPort = GetPrivateProfileInt("TCPLINK", "DBServerPort", 5228, filename);
  DBServer.Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("TCPLINK", "DBServerId", 1, filename));
 
  for (int i = 0; i < MAX_GW_NUM; i ++)
  {
    GWServer[i].IsConnected=false;    
    sprintf(pzSection,"GW[%d].ServerIP",i);
    GetPrivateProfileString("TCPLINK", pzSection, "127.0.0.1", GWServer[i].ServerIP, 30, filename);
    sprintf(pzSection,"GW[%d].ServerPort",i);
    GWServer[i].ServerPort = GetPrivateProfileInt("TCPLINK", pzSection, 5001+i, filename);
    sprintf(pzSection,"GW[%d].ServerId",i);
    GWServer[i].Serverid = CombineObjectID(NODE_DB,GetPrivateProfileInt("TCPLINK", pzSection, 0, filename));
  }
  return true;
}
int CTCPServer::ConnectCFGServer()
{
  if (IsOpen == true)
  {
    if ((CFGServer.Serverid&0xFF) == 0) return 0;
    AppInit(CFGServer.Serverid,
      ClientId,
      CFGServer.ServerIP,
      CFGServer.ServerPort,
      OnCFGLogin,
      OnCFGClose,
      OnCFGReceiveData);
    return 0;
  }
  else
  {
    //加載動態連接庫失敗
    return 1;
  }
}
int CTCPServer::ConnectIVRServer()
{
  if (IsOpen == true)
  {
    AppInit(IVRServer.Serverid,
            ClientId,
            IVRServer.ServerIP,
            IVRServer.ServerPort,
            OnIVRLogin,
            OnIVRClose,
            OnIVRReceiveData);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPServer::ConnectDBServer()
{
  if (IsOpen == true)
  {
    if ((DBServer.Serverid&0xFF) == 0) return 0;
    AppInit(DBServer.Serverid,
            ClientId,
            DBServer.ServerIP,
            DBServer.ServerPort,
            OnDBLogin,
            OnDBClose,
            OnDBReceiveData);
    return 0;
  }
  else
  {
      //加載動態連接庫失敗
      return 1;
  }
}
int CTCPServer::ConnectGWServer()
{
  if (IsOpen == true)
  {
    for (int i = 0; i < MAX_GW_NUM; i ++)
    {
      if ((GWServer[i].Serverid&0xFF) != 0)
      {
        AppInit(GWServer[i].Serverid, //serverid
               ClientId,
               GWServer[i].ServerIP,
               GWServer[i].ServerPort,
               OnGWLogin,
               OnGWClose,
               OnGWReceiveData);
      }
    }
  }
  return 0;
}

int CTCPServer::InitLOGServer()
{
  if (isLOGInit == true)
    return 2;
  if (IsOpen == true)
  {
    AppInitS(ClientId, FLWServerPort, OnLOGLogin, OnLOGClose, OnLOGReceiveData);
    isLOGInit = true;
    return 0;
  }
  else
  {
    return 1;
  }
}
int CTCPServer::ConnectServer()
{
  InitLOGServer();
  ConnectCFGServer();
  ConnectIVRServer();
  ConnectDBServer();
  ConnectGWServer();
  return 0;
}