//---------------------------------------------------------------------------
#ifndef MsgProcH
#define MsgProcH
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//處理消息
void Proc_Msg_From_IVRServer(CXMLRcvMsg &IvrMsg);
//處理消息
void Proc_Msg_From_DBServer(CXMLRcvMsg &DBMsg);
//處理IVR發來的一般消息處理結果消息
int Proc_onIVRcommon_Msg(CXMLRcvMsg &IvrMsg);
//處理DB發來的一般消息處理結果消息
int Proc_onDBcommon_Msg(CXMLRcvMsg &DBMsg);
//對自定義的全部變量進行初始化
//void Init_PubVar_InitData( CRunFlwMng &RunFlwMng, US FlwNo, US FlwId);
//電話呼入事件消息
int Proc_oncallin_Msg(CXMLRcvMsg &IvrMsg);
//電話呼入后續地址事件消息
int Proc_onrecvsam_Msg(CXMLRcvMsg &IvrMsg);
int Proc_ongetdialdtmf_Msg(CXMLRcvMsg &IvrMsg);
//處理收到的調用指定流程消息
int Proc_oncallflw_Msg(CXMLRcvMsg &IvrMsg);
//向IVR發送放信號音消息然后釋放通道
void Send_PlayTone_Release_Chn(  UC ChnType, US ChnNo, US ToneType, US Times, UC ReleaseId);
//發送設置會話序號消息
void Send_SetSessionNo(CSession *pSession);
//外部數據網關啟動呼叫事件
int Proc_ongwmakecall_Msg(CXMLRcvMsg &IvrMsg);
//外部數據網關向流程發送消息事件
int Proc_ongwmsgevent_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席發給流程會話的消息
int Proc_onsendflwmsg_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席快速轉接電話
int Proc_onagblindtrancall_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席協商轉接電話
int Proc_onagconsulttrancall_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席會議轉接電話
int Proc_onagconftrancall_Msg(CXMLRcvMsg &IvrMsg);
//坐席停止轉接來話
int Proc_onagstoptrancall_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席指定通道會議發言
int Proc_onagconfspeak_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席指定通道旁聽會議
int Proc_onagconfaudit_Msg(CXMLRcvMsg &IvrMsg);
//坐席將來話轉接到ivr流程
int Proc_onagtranivr_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席發給流程會話的保持/取消保持消息
int Proc_onaghold_Msg(CXMLRcvMsg &IvrMsg);
//電腦坐席發給流程會話的靜音/取消靜音消息
int Proc_onagmute_Msg(CXMLRcvMsg &IvrMsg);
//路由空閑中繼數消息
int Proc_onrouteideltrks(CXMLRcvMsg &IvrMsg);
//綁定坐席通道結果消息
int Proc_onbandagentchn(CXMLRcvMsg &IvrMsg);
//收到變化了的新的呼叫相關參數
int Proc_onnewcallparam(CXMLRcvMsg &IvrMsg);
//節點狀態
int Proc_onnodestate_Msg(CXMLRcvMsg &IvrMsg);
//---------------------------------------------------------------------------

//根據事件標志取事件名稱
char *Get_EventName_By_EventId(US EventId);
unsigned int GetEventVarIndexOfEvent(int event );
//---------------------------------------------------------------------------
void CheckLOGTraceMsgId();
void Proc_Msg_From_LOG(CXMLRcvMsg &LOGMsg);

void SendOneFLWSessionsToAllLOG(int FlwNo);
void SendOneFLWSessionsToAllLOG(CRunFlw *pRunFlw);
void SendOneFLWSessionsToLOG(int nLOG, CRunFlw *pRunFlw);
void SendAllFLWSessionsToLOG(int nLOG);
void SendAllFLWSessionsToAllLOG();
void SendTotalSessionsToAllLOG();
void SendTotalSessionsToLOG(int nLOG);

//---------------------------------------------------------------------------
#endif
