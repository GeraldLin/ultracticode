// TCPClient.h: interface for the CTCPClient class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_)
#define AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "typedef.h"

#define MAX_GW_NUM  16

struct TServer
{
	char  ServerIP[32];
	short ServerPort;
	unsigned short Serverid;
  bool  IsConnected;
};

class CTCPServer  
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnAppLogin,
                          ONAPPCLOSE OnAppClose,
                          ONAPPRECEIVEDATA OnAppReceiveData
                          );
  typedef void (*APPINITS)(unsigned short server_id,
    unsigned short server_port,
    ONAPPLOGIN OnAppLogin,
    ONAPPCLOSE OnAppClose,
    ONAPPRECEIVEDATA OnAppReceiveData);
private:
  TServer  CFGServer;		//CFG配置服務器
	TServer  IVRServer;		//IVR流程服務器
  TServer  DBServer;    //數據庫代理服務器
  TServer  GWServer[MAX_GW_NUM]; //數據網關

  void ReleaseDll();
public:
	CTCPServer();
	virtual ~CTCPServer();
  unsigned short ClientId;
  int FLWServerPort;

  unsigned short GetCFGServerId()
  {return CFGServer.Serverid;}
  void SetCFGLinked() {CFGServer.IsConnected=true;}
  void SetCFGUnlink() {CFGServer.IsConnected=false;}
  bool IsCFGConnected()
  {return CFGServer.IsConnected;}

  unsigned short GetIVRServerId()
  {return IVRServer.Serverid;}
  void SetIVRLinked() {IVRServer.IsConnected=true;}
  void SetIVRUnlink() {IVRServer.IsConnected=false;}
  bool IsIVRConnected()
  {return IVRServer.IsConnected;}
  
  void SetDBLinked() {DBServer.IsConnected=true;}
  void SetDBUnlink() {DBServer.IsConnected=false;}
  bool IsDBConnected()
  {return DBServer.IsConnected;}
  
  void SetGWLinked(US gwid) {GWServer[gwid].IsConnected=true;}
  void SetGWUnlink(US gwid) {GWServer[gwid].IsConnected=false;}
  bool IsGWConnected(US gwid) {return GWServer[gwid].IsConnected;}
  US GetGWServerId(US gwid) {return GWServer[gwid].Serverid;}

	char DllFile[64];
  bool LoadDll();
  bool IsOpen; //dll is opend
  bool isLOGInit;

  bool ReadIni(const char *filename);
  bool ReadWebSetupIni(const char *filename);

  int ConnectCFGServer();
  int ConnectIVRServer();
  int ConnectDBServer();
  int ConnectGWServer();
  int ConnectServer();

  int InitLOGServer();

	//申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;
  APPINITS        AppInitS;

  int SendMessage(US ServerId, US MsgId, int MsgLen, const CH *MsgBuf);
  int CFGSendMessage(US MsgId, const CH *MsgBuf);
  int IVRSendMessage(US MsgId, const CH *MsgBuf);
  int IVRSendMessage(US MsgId, const CStringX &MsgBuf);
  int DBSendMessage(US MsgId, const CH *MsgBuf);
  int DBSendMessage(US MsgId, const CStringX &MsgBuf);
  int GWSendMessage(US gwid, US MsgId, const CH *MsgBuf);
  int GWSendMessage(US gwid, US MsgId, const CStringX &MsgBuf);
  int LOGSendMessage(US nLOG, US MsgId, const CH *MsgBuf);
  int LOGSendMessage(US nLOG, US MsgId, const CStringX &MsgBuf);

  int DBSendMessage1(US MsgId, const CH *MsgBuf);
  int GWSendMessage1(US gwid, US MsgId, const CH *MsgBuf);

  //組合對象編碼
  US CombineObjectID( UC ObjectType, UC ObjectNo );
	//組合消息類型及消息編號
  US CombineMessageID(UC MsgType, UC MsgNo);
  //分解對象標識及編號
  void GetClientID(US Msg_wParamHi, UC &ClientType, UC &ClientNo);
  //分解消息類型及消息編號
  void GetMsgID(US Msg_wParamLo, UC &MsgType, UC &MsgNo);
};
//tcllinkc.dll callback function,
extern void OnCFGReceiveData(unsigned short remoteid,unsigned short msgtype,
                             const unsigned char *buf,
                             unsigned long len); //need define in main.cpp
extern void OnCFGLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnCFGClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnIVRLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnIVRClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len); //need define in main.cpp
extern void OnDBLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnDBClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnGWReceiveData(unsigned short remoteid,unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len); //need define in main.cpp
extern void OnGWLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnGWClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

extern void OnLOGReceiveData(unsigned short remoteid,unsigned short msgtype,
                             const unsigned char *buf,
                             unsigned long len); //need define in main.cpp
extern void OnLOGLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnLOGClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

//---------------------------------------------------------------------------------
#endif // !defined(AFX_TCPCLIENT_H__6AEBE68D_F1D9_4971_A224_567ACAE9DE9D__INCLUDED_)
