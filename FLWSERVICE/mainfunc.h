//---------------------------------------------------------------------------
#ifndef MainfuncH
#define MainfuncH
//---------------------------------------------------------------------------
//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-流程指令 4-變量跟蹤 5-內部調試信息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

void OnIVRLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnIVRClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function

void OnDBLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnDBClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function

void OnGWLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnGWClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
void OnGWReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function

void OnLOGLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnLOGClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
void OnLOGReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function

//處理接收到的消息
void ProcRecvMsg();
int Init();
void APPLoop();

void SendLoginCFGMsg();
void ProcCFGMsg(unsigned short MsgId, const char *msg);
//---------------------------------------------------------------------------
#endif
