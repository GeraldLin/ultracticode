#ifndef __XMLO_H__
#define __XMLO_H__
  
  //本文件實現XML解析
#include "cstring.h"  
#include "../include/msgh/vmsgrule.h"  

#define MAX_XMLMSG_ATTRIBUTE_NUM 64  //本定義必須大于等于實際系統需求

//收發消息保存的類(依賴于不同的規則)
class CXMLMsg
{
protected:	
    unsigned short ClientId;
    unsigned short MsgId; //消息編號
	  unsigned short AttrNum; //消息屬性個數
	  
	  CStringX Buf;  //存放合成好的整個串
	  CStringX MsgName;
	  
	//  CStringX AtrrNames[MAX_XMLMSG_ATTRIBUTE_NUM];//屬性名
	  CStringX AttrValues[MAX_XMLMSG_ATTRIBUTE_NUM]; //屬性值

private:
		
public:
		void Clear()  ;  
		unsigned short GetMsgId()
		{return MsgId;}
		
		unsigned short GetAttrNum()
		{return AttrNum;}
		
		CStringX &GetBuf() //取出總緩沖區
		{return Buf;} 
		
		const CStringX &GetMsgName() //取名字
		{return MsgName;}
		
		const CStringX &GetAttrValue(unsigned int index) //取出屬性值
		{return AttrValues[index];} //注意沒有檢查下標
		
		void SetClientId(unsigned short clientid)
		{ClientId=clientid;}
		unsigned short GetClientId()
		{return ClientId;}
		
//		void SetAttrNameValue(unsigned int index,const char *attrname,const char *value) //設置屬性值
//		{return AttrValues[index];} //注意沒有檢查下標
		void SetMsgBufId(unsigned short msgid,const char *buf); //輸入一個待解析的完整串		
private:		
		bool IsSplitChar(char c);//是否分割符號或者空格
	  int ParseItem(int &Pos,CStringX &ItemName,CStringX &ItemValue); //從指定位置開始解析一項,返回0成功,并返回下一個起始位置
		int ParseTail(int Pos);
		int ParseMsgNameWithCheck(const char *msgname,unsigned int attrnum);
public:
	  int ParseMsgName(); //解析出消息名,存放到MsgName
		int ParseRevMsg(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule);
		int ParseSndMsg(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule);
public:
		void SetHeadToBuf(const char *msgname); //在buf中設置頭部
		void SetHeadToBuf(const CStringX &msgname); //在buf中設置頭部
		void AddItemToBuf(const char *name,const char *value); ////在buf中添加一項
		void AddItemToBuf(const char *name,const CStringX &value); ////在buf中添加一項
		void AddItemToBufWithCheck(const char *name,const char *value); ////在buf中添加一項,內容中可能有引號
		void AddItemToBufWithCheck(const char *name,const CStringX &value); ////在buf中添加一項,內容中可能有引號
		void AddIntItemToBuf(const char *name,int value); ////在buf中添加一項整數
		void AddLongItemToBuf(const char *name,long value); ////在buf中添加一項整數
		void AddFloatItemToBuf(const char *name,double value); ////在buf中添加一項浮點數
		void AddTailToBuf(); //在buf中設置添加尾部
};
  
#endif
