//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"

#include "../include/rule/sysvar.cpp"
#include "../include/rule/intrule.cpp"
#include "../include/rule/charrule.cpp"
#include "../include/rule/strrule.cpp"
#include "../include/rule/funcrule.cpp"
#include "../include/rule/intmacro.cpp"
#include "../include/rule/strmacro.cpp"
#include "../include/rule/cmdrule.cpp"

#include "../include/msgcpp/cfgsrv.cpp"
#include "../include/msgcpp/ivrxml.cpp"
#include "../include/msgcpp/xmlivr.cpp"
#include "../include/msgcpp/dbxml.cpp"
#include "../include/msgcpp/xmldb.cpp"
#include "../include/msgcpp/logivr.cpp"
#include "../include/msgcpp/ivrlog.cpp"

//---------------------------------------------------------------------------
CFlwRuleMng::CFlwRuleMng()
{
   
    SysVar=SysVarArray;
		IntRange=IntRangeArray;
		CharRange=CharRangeArray;
		StrRange=StrRangeArray;
		FuncRule=FuncRuleArray;
		MacroIntRange=MacroIntRangeArray;
		MacroStrRange=MacroStrRangeArray;
		CmdRule=CmdRuleArray;
		XMLIVRMsgRule=XMLIVRMsgRuleArray;
		IVRXMLMsgRule=IVRXMLMsgRuleArray;
		XMLDBMsgRule=XMLDBMsgRuleArray;
		DBXMLMsgRule=DBXMLMsgRuleArray;
    LOGIVRMsgRule=LOGIVRMsgRuleArray;
    IVRLOGMsgRule=IVRLOGMsgRuleArray;
}

CFlwRuleMng::~CFlwRuleMng()
{
}


//參數值合法性檢查,合法返回0,暫時沒有使用
/*int CFlwRuleMng::Check_Var_Value(US CmdOperId, US AttrId, CH *ValueStr)
{
    UC VarType, MacroType;
    US MacroNo;
    long IntValue;
    //double FloatValue;
    int i, j, len, len1, StrLen;

    if (CmdOperId >= MAX_CMDS_NUM)
        return 1;//該指令編號不存在
        
    if (AttrId >= CmdRule[CmdOperId].AttrNum)
       return 1;//該屬性編號超出范圍
    
    VarType = CmdRule[CmdOperId].Attribute[AttrId].VarType;
    MacroType = CmdRule[CmdOperId].Attribute[AttrId].MacroType;
    MacroNo = CmdRule[CmdOperId].Attribute[AttrId].MacroNo;
    
    switch(MacroType)
    {
    	case 0: //0-不限制
      {
      	switch(VarType)
        {
        	case 0: //0未定義
            return 0;
        	case 1: //1布爾型
        		if (strcmp(ValueStr, "0") == 0 || strcmp(ValueStr, "1") == 0)
                return 0;
            else
                return 1;
          case 2: //2整數
        		return Check_long_Str(ValueStr);
          case 3: //3浮點數
        		return Check_Float_Str(ValueStr);
          case 4: //4字符
        		if (strlen(ValueStr) == 1 && isprint(ValueStr[0]))
              return 0;
            else
              return 1;            
        	case 5: //5字符串
        	case 6: //6字符串運算表達式
		      case 7: //7邏輯運算表達式
        	case 8: //8算術運算表達式
            return 0;
        	default:
        		return 1;
        }
      }  
    	case 1: //1-整數范圍
    	{
        if (Check_long_Str(ValueStr, IntValue) == 0 )
        {
            if (MacroNo < MAX_INT_RANGE_NUM)
            {
                    if ((UL)IntValue >= IntRange[MacroNo].lownum && (UL)IntValue <= IntRange[MacroNo].upnum)
                        return 0;
                    else
                        return 2;                                
            }
            else
                return 1; //宏定義超出范圍
        }
        else
            return 3;            //不是有效的整數錯誤
    	}
    	case 2: //2-單字符范圍
    	{
        if (ValueStr[0] == 0)
            return 0;
        else if (strlen(ValueStr) == 1)
        {
            if (MacroNo < MAX_CHAR_RANGE_NUM)
            {
                    len = CharRange[MacroNo].len;
                    for (i = 0; i < len; i ++)
                    {
                        if (CharRange[MacroNo].chars[i] == ValueStr[0])
                            return 0;
                    }
                    return 1;            //該參數不在規定的范圍內
        
            }
            else
                return 1;        //宏定義超出范圍
        }
        else
            return 1;          //參數值不為單字符
    	}
    	case 3: //3-字符串中允許出現的字符
    	{
        if (MacroNo < MAX_STR_RANGE_NUM)
        {
                len = strlen(ValueStr);
                if (StrRange[MacroNo].maxlen > 0 && len > StrRange[MacroNo].maxlen)
                    return 1;                    //長度超出范圍
                StrLen = len;
                len1 = StrRange[MacroNo].len;
                for (i = 0; i < len; i ++)
                {
                    for (j = 0; j < len1; j ++)
                    {
                        if (StrRange[MacroNo].chars[j] == ValueStr[i])
                        {
                            StrLen --;
                            break;
                        }
                    }
                }
                if (StrLen == 0)
                    return 0;
                else
                    return 1;                    //該參數不在規定的范圍內
        }
        else
            return 1;            //宏定義超出范圍
    	}
    	case 4: //4-整數枚舉字符串
    	{
        if (Check_long_Str(ValueStr, IntValue) == 0 )
        {
            if (MacroNo < MAX_MACRO_INT_NUM)
            {
                    len = MacroIntRange[MacroNo].stringnum;
                    for (i = 0; i < len; i ++)
                    {
                        if (MacroIntRange[MacroNo].Es[i].value == IntValue)
                            return 0;
                    }
                    return 1;                    //該參數不在規定的范圍內
            }
            else
                return 1;                //宏定義超出范圍
        }
        else
            return 1;            //參數值不為整數
    	}
    	case 5:
    	{
        if (strlen(ValueStr) > 0)
        {
            if (MacroNo < MAX_MACRO_STR_NUM)
            {
                    len = MacroStrRange[MacroNo].stringnum;
                    for (i = 0; i < len; i ++)
                    {
                        if (strcmp(MacroStrRange[MacroNo].Es[i].str, ValueStr) == 0)
                            return 0;
                    }
                    return 1;                    //該參數不在規定的范圍內
            }
            else
                return 1;                //宏定義超出范圍
        }
        else
            return 1;            //參數值空
    	}
    	default:
        return 1;        //無效的宏定義類型
		}
}
*/



//消息屬性參數值檢查,并替換相應的宏
int CFlwRuleMng::Parse_Msg_Attr_Value(const VXML_RECV_MSG_ATTR_RULE_STRUCT &AttrRule,CStringX &ValueStr)
{
    long IntValue;
   // double FloatValue;
    int i, j, len, len1, StrLen;

	 	UC VarType=AttrRule.VarType;
	 	UC MacroType=AttrRule.MacroType;
	 	US MacroNo=AttrRule.MacroNo;

    switch(MacroType)
    {
    	case 0:
        switch(VarType)
        {
        	case 0:
            return 0;
        	case 1:
            if (ValueStr.Compare("0") == 0 || ValueStr.Compare("1") == 0)
                return 0;
        		break;
        	case 2:
            if (Check_long_Str(ValueStr.C_Str()) == 0 )
                return 0;
        		break;
        	case 3:
        		if (Check_Float_Str(ValueStr.C_Str()) == 0 )
                return 0;
        		break;
        	case 4:
            if (ValueStr.GetLength() == 1 && isprint(ValueStr[0]) != 0)
                return 0;
        		break;
          case 5:
            return 0;
        }    
        break;
    
    	case 1:
        if (Check_long_Str(ValueStr.C_Str(), IntValue) == 0 )
        {
            if (MacroNo < MAX_INT_RANGE_NUM)
            {
                    if ((UL)IntValue >= IntRange[MacroNo].lownum && (UL)IntValue <= IntRange[MacroNo].upnum)
                        return 0;
            }
        }
        break;
    
    	case 2:
        if (ValueStr.GetLength() == 1)
        {
            if (MacroNo < MAX_CHAR_RANGE_NUM)
            {
                    len = CharRange[MacroNo].len;
                    for (i = 0; i < len; i ++)
                    {
                        if (CharRange[MacroNo].chars[i] == ValueStr[0])
                            return 0;
                    }
            }
        }
        break;
    
    	case 3:
        if (MacroNo < MAX_STR_RANGE_NUM)
        {
                len = ValueStr.GetLength();
                StrLen = len;
                len1 = StrRange[MacroNo].len;
                for (i = 0; i < len; i ++)
                {
                    for (j = 0; j < len1; j ++)
                    {
                        if (StrRange[MacroNo].chars[j] == ValueStr[i])
                        {
                            StrLen --;
                            break;
                        }
                    }
                }
                if (StrLen == 0)
                    return 0;
        }
        break;
    
    	case 4:
        if (ValueStr.GetLength() > 0)
        {
            if (MacroNo < MAX_MACRO_INT_NUM)
            {                                
                    len = MacroIntRange[MacroNo].stringnum;
                    for (i = 0; i < len; i ++)
                    {
                        if (ValueStr.Compare(MacroIntRange[MacroNo].Es[i].str) == 0)
                        {
                            char tmp[40];
                            Int_To_Str(MacroIntRange[MacroNo].Es[i].value, tmp);
                            ValueStr=tmp; //替換宏
                            return 0;
                        }
                    }
            }
        }
        break;
    
    	case 5:
        if (ValueStr.GetLength() > 0)
        {
            if (MacroNo < MAX_MACRO_STR_NUM)
            {
                    len = MacroStrRange[MacroNo].stringnum;
                    for (i = 0; i < len; i ++)
                    {
                        if (ValueStr.Compare(MacroStrRange[MacroNo].Es[i].str) == 0)
                            return 0;
                    }
                
            }            
        }        
        break;
    }
    
    printf("check recv attrib %s=%s rule error\n",AttrRule.AttrNameEng,ValueStr.C_Str());
    return 1;
}

		//加載流程時格式化一條指令(已經填好其它值)的所有參數和打印標志,成功返回0
int CFlwRuleMng::FormatRunCmdParams(RRunCmd &Cmd,const char *AttributesBuf)
{
   if((Cmd.CmdOperId==CMD_onevent) ||(Cmd.CmdOperId==CMD_case) || (Cmd.CmdOperId==CMD_default))
  		Cmd.Disp1=true; //是條件指令(本指令跳轉條件成立時可以打印下一條指令,否則不打印)
  	else
  		Cmd.Disp1=false; //非條件指令
  	 	
  	if((Cmd.CmdOperId==CMD_catch) || (Cmd.CmdOperId==CMD_switch) || (Cmd.CmdOperId==CMD_waiting))
		  Cmd.Disp2=false; //非條件指令時,是否可打印調試信息(True時可以打印下一條指令,否則不打印）
    else
      Cmd.Disp2=true; //非條件指令時,是否可打印調試信息(True時可以打印下一條指令,否則不打印）
 
 		Cmd.ParamNum=(UC)g_FlwRuleMng.CmdRule[Cmd.CmdOperId].AttrNum;
    
 // 	printf("CmdOperId=%d,NextCmdAddr=%d,Id=%s,disp1=%d,disp2=%d,ExtendId=%d,ParamNum=%d\n",
  //			Cmd.CmdOperId,Cmd.NextCmdAddr,Cmd.Id.C_Str(),Cmd.Disp1,Cmd.Disp2,
  //			Cmd.ExtendId,Cmd.ParamNum);
  	
		
		for(unsigned int i=0;i<Cmd.ParamNum;i++)
		{
			unsigned int Address = CmdRule[Cmd.CmdOperId].Attribute[i].Address;
    	//unsigned int Size = CmdRule[Cmd.CmdOperId].Attribute[i].Size;
    	Cmd.ParamValues[i]= AttributesBuf+Address; 	
    		
  //  	printf("--%s\n", Cmd.ParamValues[i].IsEmpty()?"NULL": Cmd.ParamValues[i].C_Str());
 		}
	
	return 0;
}

