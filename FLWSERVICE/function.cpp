//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"

static char nowtime[25];
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
//---------------------------------------------------------------------------
const DAYINMONTHS DayInMonths[2]=
{
  {365,31,28,31,30,31,30,31,31,30,31,30,31,}, //平年
  {366,31,29,31,30,31,30,31,31,30,31,30,31,}, //閏年
};

//---------------------------------------------------------------------------
//---------------類型轉換函數--------------------------------------------------
char *EncodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  rtn[j] = 'p';
  for(unsigned short i = 0; i < strlen(Pass); i++)
  {
    char Inc = (char)((GetTickCount()+i*11) % 19);
    j++;
    rtn[j] = char(Inc + 'a');
    j++;
    rtn[j] = char(Pass[i] + Inc - 15);
  }
  j++;
  rtn[j] = 's';
  Base64Encode(key, (UC *)rtn);
  return key;
}

char *DecodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  Base64Decode((UC *)key, Pass);
  for (unsigned short i=1; i<strlen(key)-2; i++)
  {
    char Inc = key[i] - 'a';
    i++;
    rtn[j++] = char(key[i] - Inc + 15);
  }
  return rtn;
}

bool IsStrMatch(const CStringX &Rule,const CStringX &code)
{
	unsigned int pos1=0;
	unsigned int pos2=0;
	unsigned int len=code.GetLength();
	while((pos1<Rule.GetLength()) && (pos2<len))
	{
		if(Rule[pos1]!=code[pos2])
		{
			if(Rule[pos1]=='*') 
			{
				pos2++;
				continue;	
			}
			else if(Rule[pos1]!='?') 
				return false;				
		}	
		pos1++;
		pos2++;
	}
	if(pos2==len)
		if(pos1==Rule.GetLength()) 
			return true;
		else if(Rule[pos1]=='*')
			return true;	
	
	return false;	
}

bool IsMatch(const CStringX &caller,const CStringX &called)
{
	return IsStrMatch(MatchCaller,caller) && IsStrMatch(MatchCalled,called);
}	

int Check_Int_Str(const char *ValueStr) //檢查是否合法整數
{
	int len;
	char c;

	len = strlen(ValueStr);
	if (len == 0) return 1;
	for (int i = 0; i < len; i ++)
	{
		c = ValueStr[i];
		if (!isdigit(c)) //不是數字
			if((c!='-') && (c!='+'))
				return 1;
			else if(i>0)
				return 1;
	}
	return 0;
}



//整數轉字符串
int Int_To_Str( int IntValue, char *ValueStr )
{
    sprintf(ValueStr,"%d",IntValue);// itoa(IntValue, ValueStr, 10);
    return 0;
}
//浮點數字符串有效判斷
int Check_Float_Str(const  char *ValueStr)
{
	int len;
	char c;

	len = strlen(ValueStr);
	if (len == 0) return 1;
	for (int i = 0; i < len; i ++)
	{
		c = ValueStr[i];
		if (!isdigit(c)) //不是數字
			if((c=='-') || (c=='+'))
			{
				if(i>0)
					return 1;
			}		
			else if(c!='.')
			{
				return 1;
			}			
	}	
	return 0;
}
//浮點數轉字符串
int Float_To_Str( double FloatValue, char *ValueStr )
{
  //_gcvt(FloatValue, 10, ValueStr);
  sprintf(ValueStr, "%f", FloatValue);
  return 0;
}
char *MyGetGUID()
{
  static char szGUID[MAX_CHAR64_LEN];
  
  memset(szGUID, 0, MAX_CHAR64_LEN);
  GUID guid = GUID_NULL;
  ::CoCreateGuid(&guid);
  if (guid == GUID_NULL)
    return "";
  sprintf(szGUID, "%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
    guid.Data1, guid.Data2, guid.Data3,
    guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
		guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);
  return szGUID;
}

//---------------------------------------------------------------------------
//---------------字符串函數--------------------------------------------------
//取字符串的長度
US MyStrLen( const char *vString )
{
    return strlen( vString );
}
//去掉字符串兩頭空格
char *MyTrim( const char *vString )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result.TrimLeft();
    result.TrimRight();
    strncpy(temp, (LPCTSTR)result, MAX_VAR_DATA_LEN);
    return temp;
}
//去掉字符串左空格
char *MyLeftTrim( const char *vString )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result.TrimLeft();
    strncpy(temp, (LPCTSTR)result, MAX_VAR_DATA_LEN);
    return temp;
}
//去掉字符串右邊空格
char *MyRightTrim( const char *vString )
{
   CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result.TrimRight();
    strncpy(temp, (LPCTSTR)result, MAX_VAR_DATA_LEN);
    return temp;
}
//從左取字符串
char *MyLeft( const char *vString, const US vLenght )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result = result.Left(vLenght);
    strcpy(temp, (LPCTSTR)result);
    return temp;
}
//從右取字符串
char *MyRight( const char *vString, const US vLenght )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result = result.Right(vLenght);
    strcpy(temp, (LPCTSTR)result);
    return temp;
}
//取子字符串
char *MySubString( const char *vString, const US vStart, const US vLenght )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result = result.Mid(vStart, vLenght);
    strcpy(temp, (LPCTSTR)result);
    return temp;
}
//字符串相加
char *MyStrAdd( const char *vString1, const char *vString2 )
{
		static char temp[MAX_VAR_DATA_LEN];
	  	
    strcpy(temp,vString1);
    strcat(temp,vString2);
    
    return temp;		
}
//字符串小寫
char *MyLower( const char *vString )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result.MakeLower();
    strncpy(temp, (LPCTSTR)result, MAX_VAR_DATA_LEN);
    return temp;
}
//字符串大寫
char *MyUpper( const char *vString )
{
    CString result;
    static char temp[MAX_VAR_DATA_LEN];
    memset(temp, 0, MAX_VAR_DATA_LEN);
    result = (CString)vString;
    result.MakeUpper();
    strncpy(temp, (LPCTSTR)result, MAX_VAR_DATA_LEN);
    return temp;
}
//取字符串位置
int MyStrPos( const char *vString1, const char *vString2 )
{
    char *result;
    US nPos;
    result = strstr(vString1, vString2);
		if(result==0)
			return -1;
    nPos = result-vString1;
    return nPos;
}
//格式化字符串
char *MyStrFmt( const char *vString, const UC vDirection, const US vLenght )
{
    int len;
    static char result[MAX_VAR_DATA_LEN];

    len = strlen( vString );
    if (len >= vLenght)
    {
        strncpy(result, vString, MAX_VAR_DATA_LEN);
    }
    else
    {
        memset(result, ' ', MAX_VAR_DATA_LEN);
        result[vLenght - len] = 0;
        if (vDirection == 1)
        {
            sprintf(result, "%s%s", result, vString);
        }
        else if (vDirection == 2)
        {
            sprintf(result, "%s%s", vString, result);
        }
        else
        {
            strcpy(result, vString);
        }
    }

    return result;
}
//切割字符串
US MySplit( const char *vString, const char vSeparator, const US vNumber, VXML_DATA_STRUCT *vArray )
{
    int i = 0, j = 0, k = 0, len;

    len = strlen(vString);
    if (len == 0)
      return 0;
    for (i = 0; i <= len; i ++)
    {
        if (vString[i] == vSeparator || vString[i] == 0)
        {
            vArray[j].Data[k] = 0;
            if (j > vNumber || j >= MAX_STRING_SPLIT_NUM) return vNumber;
            k = 0;
            j ++;
        }
        else
        {
            vArray[j].Data[k] = vString[i];
            k ++;
            vArray[j].Data[k] = 0;
        }
    }
    return j;
}

//比較字符串
UC MyStrCmp( const char *vString1, const char *vString2 )
{
    UC result;
    if ( strcmp(vString1,vString2) == 0 )
    {
        result = 0;
    }
    else
    {
        result = 1;
    }
    return result;
}
//忽略大小寫比較字符串
UC MyStrCmpi( const char *vString1, const char *vString2 )
{
    UC result;
    if ( strcmp(vString1,vString2) == 0 )
    {
        result = 0;
    }
    else
    {
        result = 1;
    }
    return result;
}
int MyIsLike(const char *vString, const char *vLike)
{
  const char *cp = NULL, *mp = NULL;

  while ((*vString) && (*vLike != '*')) 
  {
    if ((*vLike != *vString) && (*vLike != '?')) 
    {
      return 0;
    }
    vLike++;
    vString++;
  }

  while (*vString) {
    if (*vLike == '*') 
    {
      if (!*++vLike) 
      {
        return 1;
      }
      mp = vLike;
      cp = vString+1;
    } 
    else if ((*vLike == *vString) || (*vLike == '?')) 
    {
      vLike++;
      vString++;
    } 
    else 
    {
      vLike = mp;
      vString = cp++;
    }
  }

  while (*vLike == '*') 
  {
    vLike++;
  }
  return !*vLike;
}
//---------------------------------------------------------------------------
//---------------日期時間函數------------------------------------------------
int GetIncDays(int curYear, int curMonth, int curDay, int months)
{
  int days = 0, leapid, yearid, monid, dayid, decmonths;
  
  yearid = curYear;
  dayid = curDay;
	monid = curMonth;
  if (months > 0)
  {
    //頭月
    if (IsLeapYear(yearid) == true)
    	leapid = 1;
    else
    	leapid = 0;
  	days = DayInMonths[leapid].Days[monid]-dayid;
    monid ++;
    if (monid > 12)
    {
    	yearid++;
    	monid = 1;
    }
    //中間月
    if (months >= 2)
    {
	    for (int i = 1; i < months; i ++)
	    {
		    if (IsLeapYear(yearid) == true)
		    	leapid = 1;
		    else
		    	leapid = 0;
	    	days = days + DayInMonths[leapid].Days[monid];
		    monid ++;
		    if (monid > 12)
		    {
		    	yearid++;
		    	monid = 1;
		    }
	    }
  	}
  	//尾月
  	if (months >= 1)
  	{
	    if (IsLeapYear(yearid) == true)
	    	leapid = 1;
	    else
	    	leapid = 0;
	    if (curDay <= DayInMonths[leapid].Days[monid])
	    {
	    	days = days + curDay;
	    }
	    else
	    {
	    	days = days + DayInMonths[leapid].Days[monid];
	    }
  	}
  }
  else if (months < 0)
  {
    decmonths = 0-months;
    //頭月
    if (IsLeapYear(yearid) == true)
    	leapid = 1;
    else
    	leapid = 0;
  	days = dayid;
    monid --;
    if (monid <= 0)
    {
    	yearid--;
    	monid = 12;
    }
    //中間月
    if (decmonths >= 2)
    {
	    for (int i = 1; i < decmonths; i ++)
	    {
		    if (IsLeapYear(yearid) == true)
		    	leapid = 1;
		    else
		    	leapid = 0;
	    	days = days + DayInMonths[leapid].Days[monid];
		    monid --;
		    if (monid <= 0)
		    {
		    	yearid--;
		    	monid = 12;
		    }
	    }
  	}
  	//尾月
  	if (decmonths >= 1)
  	{
	    if (IsLeapYear(yearid) == true)
	    	leapid = 1;
	    else
	    	leapid = 0;
	    if (curDay < DayInMonths[leapid].Days[monid])
	    {
	    	days = days + DayInMonths[leapid].Days[monid]-curDay;
	    }
  	}
    days = 0 - days;
  }
  return days; //增長天
}
//規范日期時間字符串
int FormatDateTimeStr(const CH *pszInDTStr, CH *pszOutDTStr)
{
  int len, i, j, t, yyyy, mm, dd, hh, mi, ss;
  char c, szTemp[20];

  pszOutDTStr[0] = '\0';
  len = strlen(pszInDTStr);
  if (len < 14 || len >= 20)
  {
    return 1;
  }

  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

  j = 0;
  t = 0;
  memset(szTemp, 0, 20);
  for (i=0; i<=len; i++)
  {
    c = pszInDTStr[i];

    switch (t)
    {
    case 0: //年與月之間的分隔符 / - .
      if (c == '\0' || c == '/' || c == '-' || c == '.')
      {
        if (j >= 1 && j <= 4)
        {
          yyyy = atoi(szTemp);
          if (yyyy >= 1 && yyyy <= 12)
          {
            //直接是月開始
            strcat(pszOutDTStr, "XXXX-");
            sprintf(szTemp, "%02d-", yyyy);
            strcat(pszOutDTStr, szTemp);
            mm = yyyy;
            yyyy = local->tm_year+1900;
            t++;
          }
          else if (yyyy > 0 && yyyy < 9999)
          {
            sprintf(szTemp, "%04d-", yyyy);
            strcat(pszOutDTStr, szTemp);
          }
          else
          {
            yyyy = local->tm_year+1900;
            strcat(pszOutDTStr, "XXXX-");
          }
        }
        else
        {
          yyyy = local->tm_year+1900;
          strcat(pszOutDTStr, "XXXX-");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else if (c == '_' || c == ' ')
      {
        //直接是日開始
        dd = atoi(szTemp);
        if (dd >= 1 && dd <= 31)
        {
          sprintf(szTemp, "XXXX-XX-%02d ", dd);
          strcat(pszOutDTStr, szTemp);
        }
        else
        {
          strcat(pszOutDTStr, "XXXX-XX-XX ");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t = 3;
      }
      else if (c == ':')
      {
        //直接是時開始
        if (Check_Int_Str(szTemp, hh) == 0)
        {
          if (hh >= 0 && hh <= 23)
          {
            sprintf(szTemp, "XXXX-XX-XX %02d:", hh);
            strcat(pszOutDTStr, szTemp);
          }
          else
          {
            strcat(pszOutDTStr, "XXXX-XX-XX XX:");
          }
        }
        else
        {
          strcat(pszOutDTStr, "XXXX-XX-XX XX:");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t = 4;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 1: //月與日之間的分隔 / - .
      if (c == '\0' || c == '/' || c == '-' || c == '.')
      {
        if (j == 1 || j == 2)
        {
          mm = atoi(szTemp);
          if (mm >= 1 && mm <= 12)
          {
            sprintf(szTemp, "%02d-", mm);
            strcat(pszOutDTStr, szTemp);
          }
          else
          {
            mm = local->tm_mon+1;
            strcat(pszOutDTStr, "XX-");
          }
        }
        else
        {
          mm = local->tm_mon+1;
          strcat(pszOutDTStr, "XX-");
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 2: //日與時之間的分隔 _ 空格
      if (c == '\0' || c == '_' || c == ' ')
      {
        if (j == 1 || j == 2)
        {
          dd = atoi(szTemp);
          if (mm == 1 || mm == 3 || mm == 5 || mm == 7 || mm == 8 || mm == 10 || mm == 12)
          {
            //大月
            if (dd >= 1 && dd <= 31)
            {
              sprintf(szTemp, "%02d ", dd);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX ");
            }
          }
          else if (mm == 4 || mm == 6 || mm == 9 || mm == 11)
          {
            //小月
            if (dd >= 1 && dd <= 30)
            {
              sprintf(szTemp, "%02d ", dd);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX ");
            }
          }
          else if (mm == 2)
          {
            //二月
            if (IsLeapYear(yyyy) == true)
            {
              //閏年
              if (dd >= 1 && dd <= 29)
              {
                sprintf(szTemp, "%02d ", dd);
                strcat(pszOutDTStr, szTemp);
              }
              else
              {
                strcat(pszOutDTStr, "XX ");
              }
            }
            else
            {
              //平年
              if (dd >= 1 && dd <= 28)
              {
                sprintf(szTemp, "%02d ", dd);
                strcat(pszOutDTStr, szTemp);
              }
              else
              {
                strcat(pszOutDTStr, "XX ");
              }
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX ");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 3: //時與分之間的分隔 ：
      if (c == '\0' || c == ':')
      {
        if (j == 1 || j == 2)
        {
          if (Check_Int_Str(szTemp, hh) == 0)
          {
            if (hh >= 0 && hh <= 23)
            {
              sprintf(szTemp, "%02d:", hh);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX:");
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX:");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 4: //分與秒之間的分隔 ：
      if (c == '\0' || c == ':')
      {
        if (j == 1 || j == 2)
        {
          if (Check_Int_Str(szTemp, mi) == 0)
          {
            if (mi >= 0 && mi <= 59)
            {
              sprintf(szTemp, "%02d:", mi);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX:");
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX:");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    case 5: //秒
      if (c == '\0')
      {
        if (j == 1 || j == 2)
        {
          if (Check_Int_Str(szTemp, ss) == 0)
          {
            if (ss >= 0 && ss <= 59)
            {
              sprintf(szTemp, "%02d", ss);
              strcat(pszOutDTStr, szTemp);
            }
            else
            {
              strcat(pszOutDTStr, "XX");
            }
          }
          else
          {
            strcat(pszOutDTStr, "XX");
          }
        }
        memset(szTemp, 0, 22);
        j = 0;
        t++;
      }
      else
      {
        szTemp[j] = c;
        j++;
      }
      break;
    }
  }
  switch (t)
  {
  case 1:
    strcat(pszOutDTStr, "MM-DD HH:MI:SS");
    break;
  case 2:
    strcat(pszOutDTStr, "DD_HH:MI:SS");
    break;
  case 3:
    strcat(pszOutDTStr, "HH:MI:SS");
    break;
  case 4:
    strcat(pszOutDTStr, "MI:SS");
    break;
  case 5:
    strcat(pszOutDTStr, "SS");
    break;
  }
  return 0;
}
int isDateTimeStr(const char *pszDateTime, int &tm_year, int &tm_mon, int &tm_mday, int &tm_hour, int &tm_min, int &tm_sec)
{
  //有效格式：YYYY-MM-DD HH:MI:SS
  bool IsLeap;
  char szTemp[20], datetimestr[32];

  memset(datetimestr, 0, 32);
  if (FormatDateTimeStr(pszDateTime, datetimestr) != 0)
    return 1;
  if (strlen(datetimestr) != 19) 
    return 1;
  if (datetimestr[4] != '-' || datetimestr[7] != '-' || datetimestr[10] != ' ' 
    || datetimestr[4] != '/' || datetimestr[7] != '/' || datetimestr[10] != '_' //2016-04-25 add
    || datetimestr[13] != ':' || datetimestr[16] != ':')
    return 1;
  //分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
    tm_year = atoi(szTemp);
    IsLeap = IsLeapYear(tm_year);
  }
  else
  {
    return 1;
  }
  //分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mon = atoi(szTemp);
    if (tm_mon < 1 || tm_mon > 12)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mday = atoi(szTemp);
    if (tm_mday < 1) return 1;
    if (tm_mon == 1 || tm_mon == 3 || tm_mon == 5 || tm_mon == 7 || tm_mon == 8 || tm_mon == 10 || tm_mon == 12)
    {
      if (tm_mday > 31) return 1;
    }
    else if (tm_mon == 2)
    {
      if (IsLeap == true)
      {
        if (tm_mday > 29) return 1;
      }
      else
      {
        if (tm_mday > 28) return 1;
      }
    }
    else if (tm_mon == 4 || tm_mon == 6 || tm_mon == 9 || tm_mon == 11)
    {
      if (tm_mday > 30) return 1;
    }
  }
  else
  {
    return 1;
  }
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_hour = atoi(szTemp);
    if (tm_hour < 0 || tm_hour > 23)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_min = atoi(szTemp);
    if (tm_min < 0 || tm_min > 59)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_sec = atoi(szTemp);
    if (tm_sec < 0 || tm_sec > 59)
      return 1;
  }
  else
  {
    return 1;
  }
  return 0;
}
int String2CTime(const char *datetimestr, CTime &result)
{
  int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
  if (isDateTimeStr(datetimestr, tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec) == 0)
  {
    result = CTime(tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec);
    return 0;
  }
  return 1;
}
int dtStr2time_t(const char *datetimestr, time_t &tDateTime)
{
  //有效格式：YYYY-MM-DD HH:MI:SS
  struct tm local;
  int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
  bool IsLeap;
  char szTemp[20];

  if (strlen(datetimestr) != 19) 
    return 1;
  if (datetimestr[4] != '-' || datetimestr[7] != '-' || datetimestr[10] != ' ' 
    || datetimestr[13] != ':' || datetimestr[16] != ':')
    return 1;
  //分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
    tm_year = atoi(szTemp);
    IsLeap = IsLeapYear(tm_year);
  }
  else
  {
    return 1;
  }
  //分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mon = atoi(szTemp);
    if (tm_mon < 1 || tm_mon > 12)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mday = atoi(szTemp);
    if (tm_mday < 1) return 1;
    if (tm_mon == 1 || tm_mon == 3 || tm_mon == 5 || tm_mon == 7 || tm_mon == 8 || tm_mon == 10 || tm_mon == 12)
    {
      if (tm_mday > 31) return 1;
    }
    else if (tm_mon == 2)
    {
      if (IsLeap == true)
      {
        if (tm_mday > 29) return 1;
      }
      else
      {
        if (tm_mday > 28) return 1;
      }
    }
    else if (tm_mon == 4 || tm_mon == 6 || tm_mon == 9 || tm_mon == 11)
    {
      if (tm_mday > 30) return 1;
    }
  }
  else
  {
    return 1;
  }
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_hour = atoi(szTemp);
    if (tm_hour < 0 || tm_hour > 23)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_min = atoi(szTemp);
    if (tm_min < 0 || tm_min > 59)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_sec = atoi(szTemp);
    if (tm_sec < 0 || tm_sec > 59)
      return 1;
  }
  else
  {
    return 1;
  }
  try
  {
    local.tm_year = tm_year;
    local.tm_mon = tm_mon;
    local.tm_mday = tm_mday;
    local.tm_hour = tm_hour;
    local.tm_min = tm_min;
    local.tm_sec = tm_sec;
    tDateTime = mktime(&local);
  }
  catch (...)
  {
    return 1;
  }

  return 0;
}
int dtStr2tm(const char *datetimestr, struct tm *tDateTime)
{
  //有效格式：YYYY-MM-DD HH:MI:SS
  int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
  bool IsLeap;
  char szTemp[20];

  if (strlen(datetimestr) != 19) 
    return 1;
  if (datetimestr[4] != '-' || datetimestr[7] != '-' || datetimestr[10] != ' ' 
    || datetimestr[13] != ':' || datetimestr[16] != ':')
    return 1;
  //分析年
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[0], 4);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9' &&
		szTemp[2] >= '0' && szTemp[2] <= '9' &&
		szTemp[3] >= '0' && szTemp[3] <= '9')
	{
    tm_year = atoi(szTemp);
    IsLeap = IsLeapYear(tm_year);
  }
  else
  {
    return 1;
  }
  //分析月
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[5], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mon = atoi(szTemp);
    if (tm_mon < 1 || tm_mon > 12)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析日
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[8], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_mday = atoi(szTemp);
    if (tm_mday < 1) return 1;
    if (tm_mon == 1 || tm_mon == 3 || tm_mon == 5 || tm_mon == 7 || tm_mon == 8 || tm_mon == 10 || tm_mon == 12)
    {
      if (tm_mday > 31) return 1;
    }
    else if (tm_mon == 2)
    {
      if (IsLeap == true)
      {
        if (tm_mday > 29) return 1;
      }
      else
      {
        if (tm_mday > 28) return 1;
      }
    }
    else if (tm_mon == 4 || tm_mon == 6 || tm_mon == 9 || tm_mon == 11)
    {
      if (tm_mday > 30) return 1;
    }
  }
  else
  {
    return 1;
  }
	//分析時
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[11], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_hour = atoi(szTemp);
    if (tm_hour < 0 || tm_hour > 23)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析分
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[14], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_min = atoi(szTemp);
    if (tm_min < 0 || tm_min > 59)
      return 1;
  }
  else
  {
    return 1;
  }
	//分析秒
	memset(szTemp, 0, 20);
	strncpy(szTemp, &datetimestr[17], 2);
	if (szTemp[0] >= '0' && szTemp[0] <= '9' &&
		szTemp[1] >= '0' && szTemp[1] <= '9')
	{
    tm_sec = atoi(szTemp);
    if (tm_sec < 0 || tm_sec > 59)
      return 1;
  }
  else
  {
    return 1;
  }
  try
  {
    tDateTime->tm_year = tm_year;
    tDateTime->tm_mon = tm_mon;
    tDateTime->tm_mday = tm_mday;
    tDateTime->tm_hour = tm_hour;
    tDateTime->tm_min = tm_min;
    tDateTime->tm_sec = tm_sec;
  }
  catch (...)
  {
    return 1;
  }

  return 0;
}


const char *ConverTimeToStr(time_t tm)
{
	struct tm *local;
	
	local=localtime(&tm);

	sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;
}

void ConverTimeToStr(char *Result,time_t tm)
{
	struct tm *local;

	local=localtime(&tm);

	sprintf(Result, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
}


//取當前日期時間(yyyy-mm-dd hh-mi-ss)
const char *MyGetNow(void)
{
	//static char nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;
}
//取當前時間字符串
char *GetNowHourStr() 
{
//	static char nowtime[25];
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(nowtime, "%04d%02d%02d%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour);
	return nowtime;
}
UC MyGetNow( char *Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(Result, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
  return 0;
}
//取當前日期(yyyy-mm-dd)
UC MyGetDate( char *Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(Result, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
  return 0;
}


//取日期(yyyy-mm-dd)
UC MyGetDate( const char *vDateTime, char *Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    sprintf( Result, "%04d-%02d-%02d", sysTime.GetYear(), sysTime.GetMinute(), sysTime.GetDay());
    return 0;
  }
  return 1;
}
//取當前時間(yyyy-mm-dd)
UC MyGetTime( char *Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);

	sprintf(Result, "%02d:%02d:%02d", local->tm_hour, local->tm_min, local->tm_sec);
  return 0;
}

//取當前時間串(hhmmss)
char *MyGetTimeStr( void )
{
	time_t timep;
	struct tm *p;
	time(&timep);
	p=gmtime(&timep);
	sprintf(nowtime, "%02d%02d%02d", p->tm_hour,p->tm_min,p->tm_sec);
	return nowtime;  
}
//取時間(yyyy-mm-dd)
UC MyGetTime( const char *vDateTime, char *Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    sprintf( Result, "%02d:%02d:%02d", sysTime.GetHour(), sysTime.GetMinute(), sysTime.GetSecond());
    return 0;
  }
  return 1;
}


static const struct tm *GetNowLocalDateTime()
{
  static time_t lt;
  lt=time(NULL);
  return localtime(&lt);
}

//取當前日期時間(yyyy-mm-dd hh-mi-ss)
static const char *GetDateTimeStr()
{
  const struct tm *local;
  local=GetNowLocalDateTime();
	sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	return nowtime;
}

//取當前日期(yyyy-mm-dd)
static const char*GetDateStr()
{
  const struct tm *local;
  local=GetNowLocalDateTime();
	sprintf(nowtime, "%04d-%02d-%02d", local->tm_year+1900, local->tm_mon+1, local->tm_mday);
  return nowtime;
}

static const char *GetTimeStr()
{
  const struct tm *local;
  local=GetNowLocalDateTime();
	sprintf(nowtime, "%02d:%02d:%02d", local->tm_hour, local->tm_min, local->tm_sec);
  return nowtime;
}

static const char *GetYearStr()
{
  sprintf(nowtime,"%04d",GetNowLocalDateTime()->tm_year+1900);
  return nowtime;
}

//取當前月(mm)
static const char *GetMonthStr()
{
  sprintf(nowtime,"%02d",GetNowLocalDateTime()->tm_mon+1);
  return nowtime;
}

//取當前日(dd)
static const char *GetDayStr()
{
  sprintf(nowtime,"%02d", GetNowLocalDateTime()->tm_mday);
  return nowtime;
}

static const char *GetHourStr()
{
  sprintf(nowtime,"%02d", GetNowLocalDateTime()->tm_hour);
  return nowtime;
}

static const char *GetMinuteStr()
{
  sprintf(nowtime,"%02d", GetNowLocalDateTime()->tm_min);
  return nowtime;
}

static const char *GetSecondStr()
{
  sprintf(nowtime,"%02d", GetNowLocalDateTime()->tm_sec);
  return nowtime;
}

static const char *GetWeekDayStr()
{
  sprintf(nowtime,"%d", GetNowLocalDateTime()->tm_wday);
  return nowtime;
}

//取當前年(yyyy)
UC MyGetYear( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_year+1900;
  return 0;
}
//取年(yyyy)
UC MyGetYear( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetYear();
    return 0;
  }
  return 1;
}
//取當前月(mm)
UC MyGetMonth( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_mon+1;
  return 0;
}
//取月(mm)
UC MyGetMonth( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetMonth();
    return 0;
  }
  return 1;
}
//取當前日(dd)
UC MyGetDay( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_mday;
  return 0;
}
//取日(dd)
UC MyGetDay( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetDay();
    return 0;
  }
  return 1;
}
//取當前時(hh)
UC MyGetHour( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_hour;
  return 0;
}
//取時(hh)
UC MyGetHour( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetHour();
    return 0;
  }
  return 1;
}
//取當前分(mi)
UC MyGetMinute( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_min;
  return 0;
}
//取分(mi)
UC MyGetMinute( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetMinute();
    return 0;
  }
  return 1;
}
//取當前秒(ss)
UC MyGetSecond( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_sec;
  return 0;
}
//取秒(ss)
UC MyGetSecond( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetSecond();
    return 0;
  }
  return 1;
}
//取當前毫秒(ms)
UC MyGetMsecond( US &Result )
{
  SYSTEMTIME sysTime;
  GetLocalTime(&sysTime);
  Result = sysTime.wMilliseconds;
  return 0;
}
//取毫秒(ms)
UC MyGetMsecond( const char *vDateTime, US &Result )
{
  Result = 0;
  return 0;
}
//取當前星期(ww)
UC MyGetWeekDay( US &Result )
{
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  Result = local->tm_wday;
  return 0;
}
//取星期(ww)
UC MyGetWeekDay( const char *vDateTime, US &Result )
{
  CTime sysTime;
  if (String2CTime(vDateTime, sysTime) == 0)
  {
    Result = sysTime.GetDayOfWeek();
    return 0;
  }
  return 1;
}
//日期時間合法性判斷
UC MyIsDateTime( const char *vDateTime, char *pszDTStr )
{
  char szTemp[256], szTemp1[256], szTemp3[8];
  char szParamStr[256];
  int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
  UC nType=0;

  //2016-04-25 增加判斷日期時間合法的同時格式化字串
  struct tm *local;
  time_t lt;
  lt=time(NULL);
  local=localtime(&lt);
  
  memset(szTemp, 0, 256);
  strncpy(szTemp, vDateTime, 255);
  strcpy(szTemp, MyUpper(szTemp));

  if (strncmp(szTemp, "YYYYMMDD=", 9) == 0)
  {
    if (strlen(szTemp) == 17)
    {
      memset(szTemp1, 0, 256);
      strncpy(szTemp1, &szTemp[9], 4);
      strcat(szTemp1, "-");
      strncat(szTemp1, &szTemp[13], 2);
      strcat(szTemp1, "-");
      strncat(szTemp1, &szTemp[15], 2);
      
      strcpy(szParamStr, szTemp1);
      strcat(szParamStr, " 00:00:00");

      nType = 1;
    }
    else
    {
      return 1;
    }
  }
  else if (strncmp(szTemp, "YYYYMMDDHHMI=", 13) == 0)
  {
    if (strlen(szTemp) == 25)
    {
      memset(szTemp1, 0, 256);
      strncpy(szTemp1, &szTemp[13], 4);
      strcat(szTemp1, "-");
      strncat(szTemp1, &szTemp[17], 2);
      strcat(szTemp1, "-");
      strncat(szTemp1, &szTemp[19], 2);
      strcat(szTemp1, " ");
      strncat(szTemp1, &szTemp[21], 2);
      strcat(szTemp1, ":");
      strncat(szTemp1, &szTemp[23], 2);
      
      strcpy(szParamStr, szTemp1);
      strcat(szParamStr, ":00");

      nType = 1;
    }
    else
    {
      return 1;
    }
  }
  else if (strncmp(szTemp, "MMDD=", 5) == 0)
  {
    if (strlen(szTemp) == 9)
    {
      memset(szTemp1, 0, 256);
      memset(szTemp3, 0, 8);
      strncpy(szTemp3, &szTemp[5], 2);
      tm_mon = atoi(szTemp3);
      if (tm_mon < 1 || tm_mon > 12)
      {
        return 1;
      } 
      else if (tm_mon <= local->tm_mon+1)
      {
        sprintf(szParamStr, "%04d-%02d-", local->tm_year+1900, tm_mon);
      }
      else
      {
        sprintf(szParamStr, "%04d-%02d-", local->tm_year+1899, tm_mon);
      }
      strncpy(szTemp1, &szTemp[5], 2);
      strcat(szTemp1, "-");
      strncat(szTemp1, &szTemp[7], 2);

      strncat(szParamStr, &szTemp[7], 2);
      
      strcat(szParamStr, " 00:00:00");

      nType = 1;
    }
    else
    {
      return 1;
    }
  }
  else if (strncmp(szTemp, "MMDDHHMI=", 9) == 0)
  {
    if (strlen(szTemp) == 17)
    {
      memset(szTemp1, 0, 256);
      memset(szTemp3, 0, 8);
      strncpy(szTemp3, &szTemp[9], 2);
      tm_mon = atoi(szTemp3);
      if (tm_mon < 1 || tm_mon > 12)
      {
        return 1;
      } 
      else if (tm_mon <= local->tm_mon+1)
      {
        sprintf(szParamStr, "%04d-%02d-", local->tm_year+1900, tm_mon);
      }
      else
      {
        sprintf(szParamStr, "%04d-%02d-", local->tm_year+1899, tm_mon);
      }
      strncat(szParamStr, &szTemp[11], 2);
      strcat(szParamStr, " ");
      strncat(szParamStr, &szTemp[13], 2);
      strcat(szParamStr, ":");
      strncat(szParamStr, &szTemp[15], 2);
      strcat(szParamStr, ":00");
      
      strncpy(szTemp1, &szTemp[9], 2);
      strcat(szTemp1, "-");
      strncat(szTemp1, &szTemp[11], 2);
      strcat(szTemp1, " ");
      strncat(szTemp1, &szTemp[13], 2);
      strcat(szTemp1, ":");
      strncat(szTemp1, &szTemp[15], 2);

      nType = 1;
    }
    else
    {
      return 1;
    }
  }
  else if (strncmp(szTemp, "YYYYMM=", 7) == 0)
  {
    if (strlen(szTemp) == 13)
    {
      memset(szTemp1, 0, 256);

      memset(szTemp3, 0, 8);
      strncpy(szTemp3, &szTemp[7], 4);
      tm_mon = atoi(szTemp3);
      if (tm_mon < 1 || tm_mon > 9999)
      {
        return 1;
      } 
      strncpy(szTemp1, &szTemp[7], 4);
      strcat(szTemp1, "-");

      memset(szTemp3, 0, 8);
      strncpy(szTemp3, &szTemp[11], 2);
      tm_mon = atoi(szTemp3);
      if (tm_mon < 1 || tm_mon > 12)
      {
        return 1;
      } 
      strncat(szTemp1, &szTemp[11], 2);
      
      strcpy(szParamStr, szTemp1);
      strcat(szParamStr, "-01 00:00:00");

      nType = 1;
    }
    else
    {
      return 1;
    }
  }
  else if (strncmp(szTemp, "HHMI=", 5) == 0)
  {
    if (strlen(szTemp) == 9)
    {
      if (szTemp[5] >= '0' && szTemp[5] <= '9' && szTemp[6] >= '0' && szTemp[6] <= '9'
        && szTemp[7] >= '0' && szTemp[7] <= '9' && szTemp[8] >= '0' && szTemp[8] <= '9')
      {
        memset(szTemp1, 0, 256);
        
        memset(szTemp3, 0, 8);
        strncpy(szTemp3, &szTemp[5], 2);
        tm_hour = atoi(szTemp3);
        if (tm_hour < 0 || tm_hour > 23)
        {
          return 1;
        } 
        strncpy(szTemp1, &szTemp[5], 2);
        
        memset(szTemp3, 0, 8);
        strncpy(szTemp3, &szTemp[7], 2);
        tm_min = atoi(szTemp3);
        if (tm_min < 0 || tm_min > 59)
        {
          return 1;
        } 
        strcat(szTemp1, ":");
        strncat(szTemp1, &szTemp[7], 2);

        strcpy(pszDTStr, szTemp1);
        
        return 2;
      }
      else
      {
        return 1;
      }
    }
    else
    {
      return 1;
    }
  }
  else
  {
    strcpy(szParamStr, szTemp);
  }

  if (isDateTimeStr(szParamStr, tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec) == 0)
  {
    strcpy(pszDTStr, szTemp1);
    if (nType == 0)
      return 0;
    else
      return 2;
  }
  return 1;
}
//取日期時間間隔
UC MyDTBetween( const char *vDateTime1, const char *vDateTime2, const UC vType, int &Result )
{
    long sec = 0;
    CTime dt1, dt2;
    CTimeSpan ts;

    if (String2CTime(vDateTime1, dt1) != 0)
      return 1;
    if (String2CTime(vDateTime2, dt2) != 0)
      return 1;
    ts = dt2 - dt1;
    sec = ts.GetTotalSeconds();
    switch (vType)
    {
        case 1: //year(365.25天)
            Result = sec / 31557600;
            break;
        case 2: //month(30.4375天)
            Result = sec / 2629800;
            break;
        case 3: //day
            Result = sec / 86400;
            break;
        case 4: //hour
            Result = sec / 3600;
            break;
        case 5: //minute
            Result = sec / 60;
            break;
        case 6: //second
            Result = sec;
            break;
        case 7: //msecond
            Result = sec * 1000;
            break;
        case 8: //week
            Result = sec / 604800;
            break;
        default:
            return 1;
    }
    return 0;
}
void  DataAddMonth(COleDateTime &date,int  iMonth)  
{  
  COleDateTime DtSave(date);

  UINT nYear = DtSave.GetYear();
  UINT nMonth = DtSave.GetMonth();
  UINT nDay = DtSave.GetDay();

  int  iY = iMonth/12;
  int  iTmp = iMonth  %  12;
 
  nYear += iY;

  if(iTmp > 0) //加
  {
    int  iAllM = iTmp + nMonth;
    if(iAllM > 12)
    {
      nYear  ++;
      nMonth  =  iAllM  -  12;
    }
    else
      nMonth  =  iAllM;
  }
  if(iTmp  <  0)  //減
  {  
    if((UINT)abs(iTmp) < nMonth)
      nMonth += iTmp;
    else
    {
      nYear --;
      nMonth += 12;
      nMonth += iTmp;
    }
  }
  UINT nNextM = nMonth + 1;
  COleDateTime  DtNext(nYear,nNextM,1,0,0,0);
  COleDateTimeSpan  DtSp(1);
  DtNext -= DtSp;
  if(nDay > (UINT)DtNext.GetDay())
    nDay = DtNext.GetDay();
  date = COleDateTime(nYear,nMonth,nDay,date.GetHour(),date.GetMinute(),date.GetSecond());
}
//取日期時間增量
UC MyIncDateTime( const char *vDateTime, const UC vType, const int vNumber, char *Result )
{
 //   UC result = 0;
    int days;  
    CTime dt;
    CTimeSpan  span(0,0,0,0);

    if (String2CTime(vDateTime, dt) != 0)
      return 1;
    
    switch (vType)
    {
        case 1: //year
            days = GetIncDays(dt.GetYear(), dt.GetMinute(), dt.GetDay(), vNumber*12);  
            span = CTimeSpan(days, 0, 0, 0);
            break;
        case 2: //month
            days = GetIncDays(dt.GetYear(), dt.GetMinute(), dt.GetDay(), vNumber);  
            span = CTimeSpan(days, 0, 0, 0);
            break;
        case 3: //day
            span = CTimeSpan(vNumber, 0, 0, 0);
            break;
        case 4: //hour
            span = CTimeSpan(0, vNumber, 0, 0);
            break;
        case 5: //minute
            span = CTimeSpan(0, 0, vNumber, 0);
            break;
        case 6: //second
            span = CTimeSpan(0, 0, 0, vNumber);
            break;
        case 7: //msecond
            span = CTimeSpan(0, 0, 0, vNumber/1000);
            break;
        case 8: //week
            span = CTimeSpan(7*vNumber, 0, 0, 0);
            break;
    }
    dt = dt + span;
    sprintf(Result, "%04d-%02d-%02d %02d:%02d:%02d", dt.GetYear(), dt.GetMonth(), dt.GetDay(), dt.GetHour(), dt.GetMinute(), dt.GetSecond());
    return 0;
}
//日期時間范圍判斷(返回值：0-時間vDateTime在vDateTime1與vDateTime2范圍內)
UC MyInDateTime( const char *vDateTime, const char *vDateTime1, const char *vDateTime2 )
{
   // long sec = 0;
    CTime dt, dt1, dt2;

    if (String2CTime(vDateTime, dt) != 0)
      return 2;

    if (String2CTime(vDateTime1, dt1) != 0)
      return 2;

    if (String2CTime(vDateTime2, dt2) != 0)
      return 2;

    if (dt >= dt1 && dt <= dt2)
      return 0;
    else
      return 1;
}
//啟動定時器
UC MyTimer( const UC vTimerId, const US vTimeLen )
{
    return 0;
}
//判斷閏年
bool IsLeapYear(US year)
{
  if ((year%4) == 0 && ((year%100) != 0 || (year%400) == 0))
    return true;
  else
    return false;
}
//某月有多少天
US DaysInAMonth(US year, US month)
{
  if (month == 2)
  {
    if ((year%4) == 0 && ((year%100) != 0 || (year%400) == 0))
    {
      return 29;
    }
    else
    {
      return 28;
    }
  }
  else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
  {
    return 31;
  }
  else if (month == 4 || month == 6 || month == 9 || month == 11)
  {
    return 30;
  }
  return 0;
}
//某年有多少天
US DaysInAYear(US year)
{
  if ((year%4) == 0 && ((year%100) != 0 || (year%400) == 0))
    return 366;
  else
    return 365;
}
//檢查文件是否存在
int MyCheckFileExist(const char *FilePath)
{
	CFileFind tempFind;

	BOOL IsFinded=(BOOL)tempFind.FindFile(FilePath); 
	if(IsFinded) 
	{ 
		IsFinded=(BOOL)tempFind.FindNextFile(); 
		if(!tempFind.IsDots()) 
		{ 
			if(!tempFind.IsDirectory()) 
			{ 
				tempFind.Close();
        return 1;
			} 
		} 
	} 
	tempFind.Close(); 
	return 0;
}
//創建目錄
int MyCreatePath(CString dirPath)
{
	int i = 0, j = 3;
	CFileFind FileFind;

  int ilength = dirPath.GetLength();
	if (ilength < 4)
  {
    return 0;
  }
  if (!isalpha(dirPath[0]) || dirPath[1] != ':' || dirPath[2] != '\\')
  {
    return 0;
  }
  if (dirPath[2] == '\\')
  {
     j = 4;
  }
  if (dirPath[ilength-1] == '\\')
  {
    ilength --;
  }
  while(i < ilength)
	{
		i = dirPath.Find("\\",j+1);
		if (i == -1)
			i = dirPath.GetLength();
		CString strDirectory = dirPath.Left(i);
	
		if(!FileFind.FindFile(strDirectory,0))
		{
			if (CreateDirectory(strDirectory, NULL) == 0)
			{
				FileFind.Close();
        return 1;
			}
			FileFind.Close();
		}
		j = i;
	}
	return 0;
}
//檢查并創建文件最后一級目錄
int MyCreateLastDir(const char *FilePath)
{
  int nResult;
  char *pDest, Path[MAX_VAR_DATA_LEN];

  pDest = strrchr(FilePath, '\\');
	nResult = pDest - FilePath;
	if (pDest != NULL)
	{
		memset(Path, 0, MAX_VAR_DATA_LEN);
		strncpy(Path, FilePath, nResult);
		if (MyCreatePath((CString)Path) != 0)
		{
      return 1;
    }
  }
  return 0;
}
//---------------------------------------------------------------------------
//---------------數學函數----------------------------------------------------
//算術相加函數
double MyAdd( const double vNumber1, const double vNumber2 )
{
    return vNumber1 + vNumber2;
}
//算術相減函數
double MySub( const double vNumber1, const double vNumber2 )
{
    return vNumber1 - vNumber2;
}
//算術相乘函數
double MyMul( const double vNumber1, const double vNumber2 )
{
    return vNumber1 * vNumber2;
}
//算術相除函數
double MyDiv( const double vNumber1, const double vNumber2 )
{
    return vNumber1 / vNumber2;
}
//算術取模函數
int MyMod( const int vNumber1, const int vNumber2 )
{
    return vNumber1 % vNumber2;
}
//算術取隨機數函數
int MyRandom( const int vLowNumber, const int vUpNumber )
{
  if (vLowNumber < 0 || vUpNumber < 0 || vLowNumber == vUpNumber) return 0;
  srand( (unsigned)time( NULL ) );
  return  vLowNumber + (rand()%(abs(vUpNumber - vLowNumber)));
}
//算術取絕對值函數
int MyAbs( const int vNumber )
{
    return abs(vNumber);
}
long MyLabs( const long vNumber )
{
    return labs(vNumber);
}
double MyFabs( const double vNumber )
{
    return fabs(vNumber);
}
//取最大數函數
double MyMax( const double vNumber1, const double vNumber2 )
{
    if (vNumber1 > vNumber2)
        return vNumber1;
    else
        return vNumber2;
}
//取最小數函數
double MyMin( const double vNumber1, const double vNumber2 )
{
    if (vNumber1 < vNumber2)
        return vNumber1;
    else
        return vNumber2;
}
//------------------------------------------------------------------------------
//算術表達式運算,帶括號
UC Operation_Math_Exp( char *Exp, double &Result )
{
    char c, r, flag[MAX_VAR_DATA_LEN], ExpTemp[MAX_VAR_DATA_LEN];
    int i = 0, j = 0, k = 0, len, aCount = 0, bCount = 0;
    double result;
    char aStr[20];

    memset(flag, 0, MAX_VAR_DATA_LEN);
    memset(ExpTemp, 0, MAX_VAR_DATA_LEN);
    len = strlen(Exp);
    for (i = 0; i < len; i ++)
    {
        c = Exp[i];
        if (c == '(')
        {
            aCount ++;
        }
        if (c == ')')
        {
            bCount ++;
        }

        if (aCount < bCount)
        {
            //括號匹配錯誤
            return 1;
        }

        if ((aCount == 0) && (bCount == 0))
        {
            //第1層
            flag[j] = c;
            j ++;
        }
        else if (aCount > bCount)
        {
            //將括號內的邏輯運算寫入臨時邏輯運算表達式
            if (k > 0)
            {
                ExpTemp[k-1] = c;
            }
            k ++;
        }
        else if (aCount == bCount && aCount > 0)
        {
            //處理括號內的邏輯運算結果
            if (k > 1)
            {
                r = Operation_Math_Exp(ExpTemp, result);
                if (r == 1)
                {
                    //遞歸函數返回值錯誤
                    return 1;
                }
                else
                {
                    //aStr = FloatToStr(result);
                    //strcat(flag, aStr.c_str());
                    Float_To_Str(result, aStr);
                    strcat(flag, aStr);
                    j = strlen(flag);
                    flag[j] = '\0';
                }
                memset(ExpTemp, 0, MAX_VAR_DATA_LEN);
                k = 0;
            }
            aCount = 0;
            bCount = 0;
        }
    }
    if (j == 0)
    {
        //空表達式
        return 1;
    }
    else
    {
        r = Operation_Arithmetic_Exp(flag, Result);
        if (r == 1)
        {
            //純四則表達式運算錯誤
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
//純四則表達式運算,不帶括號
UC Operation_Arithmetic_Exp( char *Exp, double &Result )
{
    int i = 0, f = 0, s = 0, len, l;
    char c, ExpTemp[MAX_VAR_DATA_LEN], fStr[20], sStr[20], mChar=0;
    long fInt, sInt;
    double fFloat, sFloat, result = 0;
    bool fBool = false, sBool = false, mBool = false;
    char aStr[20];

    memset(ExpTemp, 0, MAX_VAR_DATA_LEN);
    memset(fStr, 0, 20);
    memset(sStr, 0, 20);
    len = strlen(Exp);
    for (i = 0; i <= len; i ++)
    {
        c = Exp[i];
        if ((c >= '0' && c <= '9') || c == '.')
        {
            if (fBool == false && mBool == false && sBool == false)
            {
                //第一個運算參數
                fStr[f] = c;
                f ++;
            }
            else if (fBool == true && mBool == true && sBool == false)
            {
                //第二個運算參數
                sStr[s] = c;
                s ++;
            }
            else
            {
                //數值元素前后操作符錯誤
                return 1;
            }
        }
        else if (c == '+' || c == '-' || c == '\0') //+或-運算符
        {
            if (fBool == false && mBool == false)
            {
                if (strlen(fStr) > 0)
                {
                    strcat(ExpTemp, fStr);
                    l = strlen(ExpTemp);
                    ExpTemp[l] = c;
                    ExpTemp[l+1] = '\0';
                    memset(fStr, 0, 20);
                    f = 0;
                }
                else
                {
                    //+-運算符前無數值元素錯誤
                    return 1;
                }
            }
            else if (fBool == true && mBool == true)
            {
                if (strlen(fStr) > 0 && strlen(sStr) > 0)
                {
                    //進行*/運算
                    fFloat = atof(fStr);
                    sFloat = atof(sStr);
                    if (mChar == '*') //*
                    {
                        fFloat = fFloat * sFloat;
                        //aStr = FloatToStr(fFloat);
                        //strcat(ExpTemp, aStr.c_str());
                        Float_To_Str(fFloat, aStr);
                        strcat(ExpTemp, aStr);
                        l = strlen(ExpTemp);
                        ExpTemp[l] = c;
                        ExpTemp[l+1] = '\0';
                        memset(fStr, 0, 20);
                        f = 0;
                        memset(sStr, 0, 20);
                        s = 0;
                        fBool = false;
                        mBool = false;
                    }
                    else if (mChar == '/') ///
                    {
                        fFloat = fFloat / sFloat;
                        //aStr = FloatToStr(fFloat);
                        //strcat(ExpTemp, aStr.c_str());
                        Float_To_Str(fFloat, aStr);
                        strcat(ExpTemp, aStr);
                        l = strlen(ExpTemp);
                        ExpTemp[l] = c;
                        ExpTemp[l+1] = '\0';
                        memset(fStr, 0, 20);
                        f = 0;
                        memset(sStr, 0, 20);
                        s = 0;
                        fBool = false;
                        mBool = false;
                    }
                    else if (mChar == '%') //%
                    {
                        fInt = (long)fFloat;
                        sInt = (long)sFloat;
                        fInt = fInt % sInt;
                        //aStr = FloatToStr(fInt);
                        //strcat(ExpTemp, aStr.c_str());
                        Float_To_Str(fInt, aStr);
                        strcat(ExpTemp, aStr);
                        l = strlen(ExpTemp);
                        ExpTemp[l] = c;
                        ExpTemp[l+1] = '\0';
                        memset(fStr, 0, 20);
                        f = 0;
                        memset(sStr, 0, 20);
                        s = 0;
                        fBool = false;
                        mBool = false;
                    }
                    else
                    {
                        //不是*/%運算符錯誤
                        return 1;
                    }
                }
                else
                {
                    //*/運算符前無數值元素錯誤
                    return 1;
                }
            }
            else
            {
                //+-運算符前無操作元素錯誤
                return 1;
            }
        }
        else if (c == '*' || c == '/' || c == '%') //*或/運算符
        {
            if (fBool == false && mBool == false)
            {
                if (strlen(fStr) > 0)
                {
                    mChar = c;
                    mBool = true;
                    fBool = true;
                }
                else
                {
                    //*/運算符前無數值元素錯誤
                    return 1;
                }
            }
            if (fBool == true && mBool == true)
            {
                if (strlen(fStr) > 0 && strlen(sStr) > 0)
                {
                    //進行*/運算
                    fFloat = atof(fStr);
                    sFloat = atof(sStr);
                    if (mChar == '*') //*
                    {
                        fFloat = fFloat * sFloat;
                        //aStr = FloatToStr(fFloat);
                        memset(fStr, 0, 20);
                        //strcpy(fStr, aStr.c_str());
                        Float_To_Str(fFloat, fStr);
                        f = strlen(fStr);
                        memset(sStr, 0, 20);
                        s = 0;
                        mChar = c;
                    }
                    else if (mChar == '/') ///
                    {
                        fFloat = fFloat / sFloat;
                        //aStr = FloatToStr(fFloat);
                        memset(fStr, 0, 20);
                        //strcpy(fStr, aStr.c_str());
                        Float_To_Str(fFloat, fStr);
                        f = strlen(fStr);
                        memset(sStr, 0, 20);
                        s = 0;
                        mChar = c;
                    }
                    else if (mChar == '%') //%
                    {
                        fInt = (int)fFloat;
                        sInt = (int)sFloat;
                        fInt = fInt % sInt;
                        //aStr = FloatToStr(fInt);
                        memset(fStr, 0, 20);
                        //strcpy(fStr, aStr.c_str());
                        Float_To_Str(fInt, fStr);
                        f = strlen(fStr);
                        memset(sStr, 0, 20);
                        s = 0;
                        mChar = c;
                    }
                    else
                    {
                        //不是*/%運算符錯誤
                        return 1;
                    }
                }
            }
            else
            {
                //*/前操作元素重復錯誤
                return 1;
            }
        }
        else
        {
            //無效的操作符
            return 1;
        }
    }
    fBool = false;
    sBool = false;
    mBool = false;
    f = 0;
    s = 0;
    memset(fStr, 0, 20);
    memset(sStr, 0, 20);
    len = strlen(ExpTemp);
    for (i = 0; i <= len; i ++)
    {
        c = ExpTemp[i];
        if ((c >= '0' && c <= '9') || c == '.')
        {
            if (fBool == false && mBool == false && sBool == false)
            {
                fStr[f] = c;
                f ++;
            }
            else if (fBool == true && mBool == true && sBool == false)
            {
                sStr[s] = c;
                s ++;
            }
            else
            {
                //數值元素錯誤
                return 1;
            }
        }
        else if (c == '+' || c == '-' || c == '\0') //+或-運算符
        {
            if (fBool == false && mBool == false)
            {
                if (strlen(fStr) > 0)
                {
                    mChar = c;
                    fBool = true;
                    mBool = true;
                    result = atof(fStr);
                    //result = StrToFloat(StrPas(fStr));
                }
                else
                {
                    //+-操作符前無數值錯誤
                    return 1;
                }
            }
            else if (fBool == true && mBool == true)
            {
                if (strlen(fStr) > 0 && strlen(sStr) > 0)
                {
                    //進行+-運算
                    fFloat = atof(fStr);
                    sFloat = atof(sStr);
                    if (mChar == '+') //+
                    {
                        fFloat = fFloat + sFloat;
                        //aStr = FloatToStr(fFloat);
                        memset(fStr, 0, 20);
                        //strcpy(fStr, aStr.c_str());
                        Float_To_Str(fFloat, fStr);
                        f = strlen(fStr);
                        memset(sStr, 0, 20);
                        s = 0;
                        mChar = c;
                        result = fFloat;
                    }
                    else if (mChar == '-') //-
                    {
                        fFloat = fFloat - sFloat;
                        //aStr = FloatToStr(fFloat);
                        memset(fStr, 0, 20);
                        //strcpy(fStr, aStr.c_str());
                        Float_To_Str(fFloat, fStr);
                        f = strlen(fStr);
                        memset(sStr, 0, 20);
                        s = 0;
                        mChar = c;
                        result = fFloat;
                    }
                    else
                    {
                        //不是+-運算符錯誤
                        return 1;
                    }
                }
                else
                {
                    //+-操作符前或后無數值錯誤
                    return 1;
                }
            }
            else
            {
                //操作符標志錯誤
                return 1;
            }
        }
    }
    Result = result;
    return 0;
}
//邏輯表達式比較運算:
UC Compare_Operation( char *Exp, char *Result )
{
    /* 操作符標志對應說明
    (:a, ):b, =:c, >:d, >=:e, <:f, <=:g, <>:h, like:i, and:j, or:k, not:l, 字符串常量:m, 數值常量:n, 變量：o, 錯誤變量名：r, 空格：s
    */
    static char sStr[MAX_VAR_DATA_LEN];
    static char fStr[MAX_VAR_DATA_LEN];
    static char flag[MAX_VAR_DATA_LEN];
    
    char c, r, CompChar=0;
    int i = 0, j = 0, len, f = 0, s = 0;
    bool fBool = false, cBool = false, aStartId = false, slashId = false, fStrId = false, sStrId = false;
    double fDouble = 0, sDouble = 0;

    memset(flag, 0, MAX_VAR_DATA_LEN);
    memset(fStr, 0, MAX_VAR_DATA_LEN);
    memset(sStr, 0, MAX_VAR_DATA_LEN);
    len = strlen(Exp);
    for (i = 0; i <= len; i ++)
    {
        c = Exp[i];
        if (c == '\\')
        {
            if (slashId == false)
            {
                slashId = true; //換義字符標志
            }
            else
            {
              if (fBool == false)
              {
                fStr[f] = c;
                f ++;
              }
              else
              {
                sStr[s] = c;
                s ++;
              }
              slashId = false;
            }
        }
        else if (c == '\'')
        {
            if (slashId == false)
            {
                if (aStartId == false)
                {
                    aStartId = true; //設置為單引號開始
                    if (fBool == false) //是否為比較符前參數
                    {
                        fStrId = true; //比較符前參數為字符串標志
                    }
                    else
                    {
                        sStrId = true; //比較符后參數為字符串標志
                    }
                }
                else
                {
                    aStartId = false; //設置為單引號結束
                }
            }
            else
            {
                if (fBool == false)
                {
                    fStr[f] = c;
                    f ++;
                }
                else
                {
                    sStr[s] = c;
                    s ++;
                }
            }
            slashId = false;
        }
        else if (c == '(' || c == ')' || c == '!')
        {
            slashId = false;
            if (aStartId == false)
            {
                if (fBool == true && cBool == true)
                {
                    //進行比較運算
                    if (fStrId == false && sStrId == false)
                    {
                        //數值比較
                        fDouble = atof(fStr);
                        sDouble = atof(sStr);
                        if (CompChar == '=') //=
                        {
                            if (fDouble == sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '?') //?
                        {
                            if (strcmp(fStr, sStr) == 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '>') //>
                        {
                            if (fDouble > sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '}') //>=
                        {
                            if (fDouble >= sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '<') //<
                        {
                            if (fDouble < sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '{') //<=
                        {
                            if (fDouble <= sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '~') //<>
                        {
                            if (fDouble != sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '%') //like
                        {
                            if (MyIsLike(fStr, sStr))
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else
                        {
                            //比較符錯誤
                            return 1;
                        }
                    }
                    else
                    {
                        //字符串比較
                        if (CompChar == '=') //=
                        {
                            if (strcmp(fStr, sStr) == 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '?') //?
                        {
                            if (strcmp(fStr, sStr) == 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '>') //>
                        {
                            if (strcmp(fStr, sStr) > 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '}') //>=
                        {
                            if (strcmp(fStr, sStr) >= 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '<') //<
                        {
                            if (strcmp(fStr, sStr) < 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '{') //<=
                        {
                            if (strcmp(fStr, sStr) <= 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '~') //<>
                        {
                            if (strcmp(fStr, sStr) != 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '%') //like
                        {
                            if (MyIsLike(fStr, sStr))
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else
                        {
                            //比較符錯誤
                            return 1;
                        }
                    }
                    flag[j] = r;
                    j ++;

                    memset(fStr, 0, MAX_VAR_DATA_LEN);
                    f = 0;
                    memset(sStr, 0, MAX_VAR_DATA_LEN);
                    s = 0;
                    fBool = false;
                    cBool = false;
                    fStrId = false;
                    sStrId = false;
                }
                flag[j] = c;
                j ++;
            }
            else
            {
                if (fBool == false)
                {
                    fStr[f] = c;
                    f ++;
                }
                else
                {
                    sStr[s] = c;
                    s ++;
                }
            }
        }
        else if (c == '=' || c == '?' || c == '>' || c == '}' || c == '<' || c == '{' || c == '~' || c == '%')
        {
            slashId = false;
            if (aStartId == false)
            {
                CompChar = c;
                cBool = true;
                fBool = true;
            }
            else
            {
                if (fBool == false)
                {
                    fStr[f] = c;
                    f ++;
                }
                else
                {
                    sStr[s] = c;
                    s ++;
                }
            }
        }
        else if (c == '@' || c == '|' || c == '\0')
        {
            slashId = false;
            if (aStartId == false)
            {
                if (fBool == true && cBool == true)
                {
                    //進行比較運算
                    if (fStrId == false && sStrId == false)
                    {
                        //數值比較
                        fDouble = atof(fStr);
                        sDouble = atof(sStr);
                        if (CompChar == '=') //=
                        {
                            if (fDouble == sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '?') //?
                        {
                            if (strcmp(fStr, sStr) == 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '>') //>
                        {
                            if (fDouble > sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '}') //>=
                        {
                            if (fDouble >= sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '<') //<
                        {
                            if (fDouble < sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '{') //<=
                        {
                            if (fDouble <= sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '~') //<>
                        {
                            if (fDouble != sDouble)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '%') //like
                        {
                            if (MyIsLike(fStr, sStr))
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else
                        {
                            //比較符錯誤
                            return 1;
                        }
                    }
                    else
                    {
                        //字符串比較
                        if (CompChar == '=') //=
                        {
                            if (strcmp(fStr, sStr) == 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '?') //?
                        {
                            if (strcmp(fStr, sStr) == 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '>') //>
                        {
                            if (strcmp(fStr, sStr) > 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '}') //>=
                        {
                            if (strcmp(fStr, sStr) >= 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '<') //<
                        {
                            if (strcmp(fStr, sStr) < 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '{') //<=
                        {
                            if (strcmp(fStr, sStr) <= 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '~') //<>
                        {
                            if (strcmp(fStr, sStr) != 0)
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else if (CompChar == '%') //like
                        {
                            if (MyIsLike(fStr, sStr))
                            {
                                r = '1';
                            }
                            else
                            {
                                r = '0';
                            }
                        }
                        else
                        {
                            //比較符錯誤
                            return 1;
                        }
                    }
                    flag[j] = r;
                    j ++;

                    memset(fStr, 0, MAX_VAR_DATA_LEN);
                    f = 0;
                    memset(sStr, 0, MAX_VAR_DATA_LEN);
                    s = 0;
                    fBool = false;
                    cBool = false;
                    fStrId = false;
                    sStrId = false;
                }
                flag[j] = c;
                j ++;
            }
            else
            {
                if (fBool == false)
                {
                    fStr[f] = c;
                    f ++;
                }
                else
                {
                    sStr[s] = c;
                    s ++;
                }
            }
        }
        else if ((c >= '0' && c <= '9') || c == '.')
        {
            slashId = false;
            if (fBool == false)
            {
                fStr[f] = c;
                if (f == 1)
                {
                    if (fStr[0] == '+' || fStr[0] == '-')
                    {
                        fStrId = false;
                    }
                }
                f ++;
            }
            else
            {
                sStr[s] = c;
                if (s == 1)
                {
                    if (sStr[0] == '+' || sStr[0] == '-')
                    {
                        sStrId = false;
                    }
                }
                s ++;
            }
        }
        else
        {
            if (slashId == true)
            {
              if (fBool == false)
              {
                fStr[f] = '\\';
                f ++;
              }
              else
              {
                sStr[s] = '\\';
                s ++;
              }
              slashId = false;
            }
            if (fBool == false)
            {
                fStr[f] = c;
                f ++;
                fStrId = true;
            }
            else
            {
                sStr[s] = c;
                s ++;
                sStrId = true;
            }
        }
    }
    strcpy(Result, flag);
    return 0;
}
//邏輯表達式邏輯運算(返回值：0=false,1=true,2=error)
UC Operation_Logic_Exp( char *Exp )
{
    char c, r, flag[MAX_VAR_DATA_LEN], ExpTemp[MAX_VAR_DATA_LEN], f=0, s=0, l=0;
    int i = 0, j = 0, k = 0, len, aCount = 0, bCount = 0;
    bool _first = false;
    bool _second = false;
    bool _logic = false;
    bool _not = false;

    memset(flag, 0, MAX_VAR_DATA_LEN);
    memset(ExpTemp, 0, MAX_VAR_DATA_LEN);
    len = strlen(Exp);
    for (i = 0; i < len; i ++)
    {
        if (Exp[i] == '(')
        {
            aCount ++;
        }
        if (Exp[i] == ')')
        {
            bCount ++;
        }

        if (aCount < bCount)
        {
            //括號匹配錯誤
            return 2;
        }

        if ((aCount == 0) && (bCount == 0))
        {
            //第1層
            flag[j] = Exp[i];
            j ++;
        }
        else if (aCount > bCount)
        {
            //將括號內的邏輯運算寫入臨時邏輯運算表達式
            if (k > 0)
            {
                ExpTemp[k-1] = Exp[i];
            }
            k ++;
        }
        else if (aCount == bCount && aCount > 0)
        {
            //處理括號內的邏輯運算結果
            if (k > 1)
            {
                r = Operation_Logic_Exp(ExpTemp);
                memset(ExpTemp, 0, MAX_VAR_DATA_LEN);
                k = 0;
                flag[j] = r+'0';
                j ++;
            }
            aCount = 0;
            bCount = 0;
        }
    }
    //運算出邏輯結果
    if (j == 0)
    {
        //空表達式錯誤
        return 2;
    }
    else if (j == 1)
    {
        if (flag[0] == '0' || flag[0] == '1')
        {
            return flag[0]-'0';
        }
        else
        {
            //表達式單操作符錯誤
            return 2;
        }
    }
    else if (j == 2)
    {
        if (flag[0] == '!' && (flag[1] == '0' || flag[1] == '1'))
        {
            return (flag[1] - '0' + 1) % 2;
        }
        else
        {
            //表達式雙操作符錯誤
            return 2;
        }
    }
    else if (j > 2)
    {
        for (i = 0; i < j; i ++)
        {
            c = flag[i];
            if (c == '!')
            {
                if (_not == false)
                {
                    _not = true;
                }
                else
                {
                    //表達式中非操作符錯誤
                    return 2;
                }
            }
            else if (c == '0' || c == '1')
            {
                if (_first == false && _second == false && _logic == false)
                {
                    _first = true;
                    f = c - '0';
                    if (_not == true)
                    {
                        f = (f + 1) % 2;
                        _not = false;
                    }
                }
                else if (_first == true && _second == false && _logic == true)
                {
                    _second = true;
                    s = c - '0';
                    if (_not == true)
                    {
                        s = (s + 1) % 2;
                        _not = false;
                    }
                }
                else
                {
                    //邏輯表達式格式錯誤
                    return 2;
                }
            }
            else if (c == '@' || c == '|')
            {
                if (_first == true && _second == false && _logic == false && _not == false)
                {
                    _logic = true;
                    l = c;
                }
                else
                {
                    //邏輯表達式格式錯誤
                    return 2;
                }
            }
            else
            {
                //邏輯表達式符號錯誤
                return 2;
            }
            if (_first == true && _second == true && _logic == true && _not == false)
            {
                if (l == '@')
                {
                    if (f == 1 && s == 1)
                        f = 1;
                    else
                        f = 0;
                }
                else if (l == '|')
                {
                    if (f == 1 || s == 1)
                        f = 1;
                    else
                        f = 0;
                }
                else
                {
                    //表達式中邏輯表達式格式錯誤
                    return 2;
                }
                _logic = false;
                _second = false;
            }
        }
        if (_first == true && _second == false && _logic == false && _not == false)
        {
            return f;
        }
        else
        {
            //表達式中邏輯表達式格式錯誤
            return 2;
        }
    }
    //表達式中邏輯表達式格式錯誤
    return 2;
}
//字符串相加運算
int Operation_String_Exp(char *Exp, char *Result)
{
    UC c;
    int i = 0, j = 0, len;
    char Str[MAX_VAR_DATA_LEN];
    bool aStartId = false, slashId = false;

    memset(Str, 0, MAX_VAR_DATA_LEN);
    len = strlen(Exp);
    for (i = 0; i <= len; i ++)
    {
        c = Exp[i];
        if (c == '\\')
        {
            if (slashId == false)
            {
                slashId = true; //換義字符標志
            }
            else
            {
                slashId = false;
                if (j < MAX_VAR_DATA_LEN)
                {
                  Str[j] = c;
                  j ++;
                }
            }
        }
        else if (c == '\'')
        {
            if (slashId == false)
            {
                if (aStartId == false)
                {
                    aStartId = true; //設置為單引號開始
                }
                else
                {
                    aStartId = false; //設置為單引號結束
                }
            }
            else
            {
                if (aStartId == true)
                {
                    if (j < MAX_VAR_DATA_LEN)
                    {
                        Str[j] = c;
                        j ++;
                    }
                }
            }
            slashId = false;
        }
        else
        {
            if (slashId == true)
            {
              if (j < MAX_VAR_DATA_LEN)
              {
                Str[j] = '\\';
                j ++;
              }
            }
            if (aStartId == true)
            {
                if (j < MAX_VAR_DATA_LEN)
                {
                    Str[j] = c;
                    j ++;
                }
            }
            slashId = false;
        }
    }
    memcpy(Result, Str, MAX_VAR_DATA_LEN);
    return 0;
}

/////////////////////////////////////變量操作
//填寫系統內容到自身的固定變量內
static int FillFixSysVar(US SysVarIndex)
{
	char s[25];
	CStringX &Var=g_GData.SysVar[SysVarIndex];
	
	switch (SysVarIndex)
  {                    //系統固定變量名
   case R_NULL:
       Var = "";
       break;
   case R_DateTime:
       Var=GetDateTimeStr();
       break;
   case R_Date:
       Var=GetDateStr();
       break;
   case R_Time:
       Var=GetTimeStr();
       break;
   case R_Year:
       Var=GetYearStr();
       break;
   case R_Month:
       Var=GetMonthStr();
       break;
   case R_Day:
       Var=GetDayStr();
       break;
   case R_Hour:
       Var=GetHourStr();
       break;
   case R_Minute:
       Var=GetMinuteStr();
       break;
   case R_Second:
       Var=GetSecondStr();
       break;
   case R_Msecond:
       Var="0";
       break;
   case R_Weekday:
       Var=GetWeekDayStr();
       break;
   case R_True:
       Var="1";
       break;
   case R_False:
       Var="0";
       break;
   case R_Yes:
       Var="1";
       break;
   case R_No:
       Var="0";
       break;
   case R_AllOnLines:
       sprintf(s, "%d", g_SessionMng.GetUsedSessionNum());
       Var=s;
       break;
   case R_VoxPath:
       Var="0";
       break;
   case R_Langue:
       Var="0";
       break;
   case R_Voice:
       Var="0";
       break;

   //流程固定變量名
   case R_SessionId:
       sprintf(s, "%d", (US)pCurrent->SerialNo);
       Var=s;
       break;
   case R_StatusId:
       Var="1";
       break;
   case R_CallInOut:
       sprintf(s, "%d", pCurrent->CallInOut);
       Var=s;
       break;
   case R_VxmlPoint:
       sprintf(s, "%d", g_TcpServer.ClientId&0xFF);
       Var=s;
       break;
   case R_ServerType:
       sprintf(s, "%d", pCurrent->GetFuncGroup());
       Var=s;
       break;
   case R_ServerNo:
       sprintf(s, "%d", pCurrent->GetFuncNo());
       Var=s;
       break;
   case R_SngDevice:
       Var="1";
       break;
   case R_SwtDevice:
       Var="1";
       break;
   case R_VoxDevice:
       Var="1";
       break;
   case R_CfcDevice:
       Var="1";
       break;
   case R_ChanType:
       sprintf(s, "%d", pCurrent->ChnType);
       Var=s;
       break;
   case R_ChanNo:
       sprintf(s, "%d", pCurrent->ChnNo);
       Var=s;
       break;
   case R_CallerNo:
       Var=pCurrent->CallerNo;
       break;
   case R_OrgCallerNo:
       Var=pCurrent->OrgCallerNo;
       break;
   case R_CalledNo:
       Var=pCurrent->CalledNo;
       break;
   case R_OrgCalledNo:
       Var=pCurrent->OrgCalledNo;
       break;
   case R_CallTime:
       Var=ConverTimeToStr(pCurrent->CallTime);
       break;
   case R_AnsTime:
       Var=ConverTimeToStr(pCurrent->AnsTime);
       break;
   case R_RelTime:
       Var=ConverTimeToStr(pCurrent->RelTime); //如果SessionId已經釋放，無法記錄
       break;
   case R_Timer0:
       sprintf(s, "%d", pCurrent->EventVars[UC_Timer0Event]);
       Var=s;
       break;
   case R_Timer1:
       sprintf(s, "%d", pCurrent->EventVars[UC_Timer1Event]);
       Var=s;
       break;
   case R_Timer2:
       sprintf(s, "%d", pCurrent->EventVars[UC_Timer2Event]);
       Var=s;
       break;
   case R_Timer3:
       sprintf(s, "%d", pCurrent->EventVars[UC_Timer3Event]);
       Var=s;
       break;
   case R_Timer4:
       sprintf(s, "%d", pCurrent->EventVars[UC_Timer4Event]);
       Var=s;
       break;
   case R_SeatNo:
       Var=pCurrent->SeatNo;
       break;
   case R_WorkerNo:
       sprintf(s, "%d", pCurrent->WorkerNo);
       Var=s;
       break;
   case R_CmdAddr:
       sprintf(s, "%d", pCurrent->CurrCmdAddr);
       Var=s;
       break;
   case R_OnLines:
       sprintf(s, "%d", pCurrent->GetFlw().Onlines);
       Var=s;
       break;
   case R_OnEvent:
       sprintf(s, "%d", pCurrent->EventVars[UC_OnEvent]);
       Var=s;
       break;
   case R_DtmfBuf:
       Var=pCurrent->RecvDtmf;
       break;
   case R_AsrBuf:
       Var=pCurrent->AsrBuf;
       break;
   case R_DialSerialNo:
       sprintf(s, "%ld", pCurrent->DialSerialNo);
       Var=s;
       break;
   case R_DialParam:
       Var=pCurrent->DialParam;
       break;
   case R_Error:
       Var=pCurrent->ErrorMsg;
       break;
   case R_SessionNo:
       sprintf(s, "%ld", pCurrent->SerialNo);
       Var=s;
       break;
   case R_ChHWType:
       sprintf(s, "%d", pCurrent->ChHWType);
       Var=s;
       break;
   case R_ChHWNo:
       sprintf(s, "%d", pCurrent->ChHWNo);
       Var=s;
       break;
   case R_SeatType:
       sprintf(s, "%d", pCurrent->SeatType);
       Var=s;
       break;
   case R_CallerType:
       sprintf(s, "%d", pCurrent->CallerType);
       Var=s;
       break;
   case R_CalledType:
       sprintf(s, "%d", pCurrent->CalledType);
       Var=s;
       break;
   case R_RouteNo:
       sprintf(s, "%d", pCurrent->RouteNo);
       Var=s;
       break;
   case R_GWSerialNo:
       sprintf(s, "%ld", pCurrent->GWSerialNo);
       Var=s;
       break;
   case R_GWAccount:
       Var=pCurrent->GWAccount;
       break;
   case R_GWMsgType:
       sprintf(s, "%d", pCurrent->GWMsgType);
       Var=s;
       break;
   case R_GWMsg:
       Var=pCurrent->GWMsg;
       break;
   case R_AGMsgType:
       sprintf(s, "%d", pCurrent->AGMsgType);
       Var=s;
       break;
   case R_AGMsg:
       Var=pCurrent->AGMsg;
       break;
   case R_SMSType:
       sprintf(s, "%d", pCurrent->SMSType);
       Var=s;
       break;
   case R_SMSMsg:
       Var=pCurrent->SMSMsg;
       break;
   case R_AGPhoneType:
       sprintf(s, "%d", pCurrent->AGPhoneType);
       Var=s;
       break;
   case R_AGTranPhone:
       Var=pCurrent->AGTranPhone;
       break;
   case R_AGTranParam:
     Var=pCurrent->AGTranParam;
     break;
   case R_AGTranIVRId:
     sprintf(s, "%d", pCurrent->AGTranIVRId);
     Var=s;
     break;
   case R_AGReturnId:
     sprintf(s, "%d", pCurrent->AGReturnId);
     Var=s;
     break;
   case R_GroupNo:
     sprintf(s, "%d", pCurrent->GroupNo);
     Var=s;
     break;
   case R_AGTranConfNo:
     sprintf(s, "%d", pCurrent->AGTranConfNo);
     Var=s;
     break;
   case R_DBRecordRows:
     sprintf(s, "%d", pCurrent->DBRecordRows);
     Var=s;
     break;
   case R_DialDTMF:
     Var=pCurrent->DialDTMF;
     break;
     
   case R_Route0Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[0]);
     Var=s;
     break;
   case R_Route1Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[1]);
     Var=s;
     break;
   case R_Route2Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[2]);
     Var=s;
     break;
   case R_Route3Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[3]);
     Var=s;
     break;
   case R_Route4Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[4]);
     Var=s;
     break;
   case R_Route5Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[5]);
     Var=s;
     break;
   case R_Route6Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[6]);
     Var=s;
     break;
   case R_Route7Idels:
     sprintf(s, "%d", g_SessionMng.RouteIdelTrkNum[7]);
     Var=s;
     break;
   case R_RunOS:
     Var="WIN32";
     break;
   case R_BandAGFirst:
     sprintf(s, "%d", pCurrent->BandAGFirst);
     Var=s;
     break;
   case R_CallType:
     sprintf(s, "%d", pCurrent->CallType);
     Var=s;
     break;
   case R_AnswerId:
     sprintf(s, "%d", pCurrent->AnswerId);
     Var=s;
     break;
   case R_CDRSerialNo:
     Var=pCurrent->CDRSerialNo;
     break;
   case R_RecdRootPath:
     Var=pCurrent->RecdRootPath;
     break;
   case R_RecdFileName:
     Var=pCurrent->RecdFileName;
     break;
   case R_SwitchType:
     sprintf(s, "%d", g_SessionMng.SwitchType);
     Var=s;
     break;
   case R_DBType:
     Var=g_SessionMng.DataBaseType;
     break;
   case R_RemoteFaxSID:
     Var=pCurrent->RemoteFaxSID;
     break;
   case R_RecSysState:
     sprintf(s, "%d", g_SessionMng.RecSysState);
     Var=s;
     break;

   default:
       ErrVar.Format("No defined fix SysVarIndex=%d",SysVarIndex);//未定義的變量
       return 1;
       //break;
  }
	return 0;	
}

            

//是否為合法的變量名格式,1=合法的單變量，2=合法的間接變量，其它=非法,合法時返回變量號
int IsVarNameFormatOK(const char *VarName,int &AddrNo1, int &AddrNo2)
{
	
	unsigned int len = strlen(VarName);
  if (len == 6) //單變量地址
  {
    if (VarName[0] != '$' || VarName[2] != '_')
      return 0;        //非法的變量名
  //  if (VarName[1] != 'S' && VarName[1] != 'P' && VarName[1] != 'R')
  //    return 0;        //非法的變量名
     
    if(!isdigit(VarName[3]) || !isdigit(VarName[4]) || !isdigit(VarName[5]))
    	return 0;
    	     
    AddrNo1=atoi(VarName+3); //從數字開始
    return 1;       //合法的單變量名
	}
	else if(len==12) //間接變量地址
	{
		 if (VarName[0] != '$' || VarName[1] != '&' || VarName[2] != '_' || VarName[7] != '_')
            return 0;      //非法的變量名
  //   if (VarName[3] != 'P' && VarName[3] == 'R' && VarName[8] != 'P' && VarName[8] == 'R')
  //          return 0;       //非法的變量名
     			
		if(!isdigit(VarName[4]) || !isdigit(VarName[5]) || !isdigit(VarName[6]))
    	return 0;
    
    if(!isdigit(VarName[9]) || !isdigit(VarName[10]) || !isdigit(VarName[11]))
    	return 0;
    
    AddrNo1=atoi(VarName+4); //從數字4開始	   
    AddrNo2=atoi(VarName+9); //從數字9開始	   
   	return 2;	
	}	
	return 0; //失敗
}

//根據變量名取變量地址C_Str()
CStringX *GetpCurrentVarPointer(const char *VarName,int Offset,const CStringX **pOrgVarName)
{
	int Vartype, AddrNo1, AddrNo2;
	const char *p;
	static CStringX SysVarOrgName;
	
	CFlw &CurFlw=pCurrent->GetFlw();
	Vartype=IsVarNameFormatOK(VarName,AddrNo1,AddrNo2);
    if (Vartype == 1) //單變量地址
    {        
        AddrNo1 += Offset;

        //單變量賦值處理
        switch (VarName[1])
        {
        	case 'S': //系統全局變量
             if (AddrNo1 < 0 || AddrNo1 > R_NULL)
             {
             		if(FillFixSysVar(AddrNo1)) //失敗
             			break;
             }	  
             if(pOrgVarName) 
             {
             		SysVarOrgName=VarName;
             	  *pOrgVarName=&SysVarOrgName;  
          	 }
          	 return &g_GData.SysVar[AddrNo1];  
             break;
            
        	case 'P': //流程全局變量
             if (AddrNo1 >= 0 && AddrNo1 < MAX_PUB_NUM)
             {
             		if(pOrgVarName) 
             	 		*pOrgVarName=&CurFlw.PubVar[AddrNo1].Name; 
                return &CurFlw.PubVar[AddrNo1].Data;
             }
             break;
        	
        	case 'R': //流程局部變量
              if (AddrNo1 >= 0 && AddrNo1 < MAX_PRI_NUM)
              {
              		if(pOrgVarName) 
             	 			*pOrgVarName=&CurFlw.PriVar[AddrNo1].Name; 
                  return &pCurrent->PriVar[AddrNo1];
            	}
            	break;

					default:
						break;
        }
    }
    else if (Vartype == 2) //間接變量地址
    {        
        //間接變量賦值
        if (VarName[8] == 'P')
            p=CurFlw.PubVar[AddrNo2].Data.C_Str();
        else if (VarName[8] == 'R')
            p=pCurrent->PriVar[AddrNo2].C_Str();
        else
        {
        	goto ErrorLB;
        }    
        
        if (Check_Int_Str(p, AddrNo2) == 0) //是整數
        {
            AddrNo1 += AddrNo2;
            AddrNo1 += Offset;
            switch (VarName[3])
            {
            	case 'P':
                if (AddrNo1 >= 0 && AddrNo1 < MAX_PUB_NUM)
                {
                	if(pOrgVarName) 
             	 			*pOrgVarName=&CurFlw.PubVar[AddrNo1].Name; 
                	return &CurFlw.PubVar[AddrNo1].Data;
                }                
								break;
                              	
            	case 'R':
                if (AddrNo1 >= 0 && AddrNo1 < MAX_PRI_NUM)
                {
                  if(pOrgVarName) 
             	 			*pOrgVarName=&CurFlw.PriVar[AddrNo1].Name; 
                  return &pCurrent->PriVar[AddrNo1];
                }
                break;
              default:
              	break;  
            }           
        }
		}

ErrorLB:								
  	ErrVar.Format("getvarpointer VarName %s error",VarName);
    throw ErrVar.C_Str();
  
  // return 0;
  
}



//根據變量名取變量的值,錯誤時彈出異常
void Get_pCurrent_VarValue_By_VarName( const char *VarName, char *Value)
{
    CStringX *pC=GetpCurrentVarPointer(VarName);
		pC->CopyOutWithTerm(Value);
}

//顯示當前執行完畢的指令
void DispCmd(const CSession *pSession)
{
  if(!g_bCmdId && !g_nSendTraceMsgToLOG)
		return;
  US CallFlwNo = pSession->FlwNo;
  if (g_RunFlwMng[CallFlwNo].DispLogId == 0)
    return;
  
  US CmdAddr=pSession->CurrCmdAddr;    
  const RRunCmd *pCmd=&pSession->GetFlw().RunCmds[CmdAddr];
  const VXML_CMD_RULE_STRUCT *pCmdRule=&g_FlwRuleMng.CmdRule[pCmd->CmdOperId];

  MyTrace(3, "SerialNo=%ld ChnType=%d ChnNo=%d FlwName=%s CmdAddr=%d id=%s CmdName=%s",
      pSession->SerialNo,pSession->ChnType,pSession->ChnNo,pSession->GetRunFlw().FlwName.C_Str(),CmdAddr,pCmd->Id.C_Str(),pCmdRule->CmdNameEng);
}

void DispVar(const CSession *pSession,const char *Name,const CStringX &OrgName,const CStringX &Value,int Offset)
{
	if(!g_bVarId && !g_nSendTraceMsgToLOG)
		return;
  US CallFlwNo = pSession->FlwNo;
  if (g_RunFlwMng[CallFlwNo].DispLogId == 0)
    return;
	if (strcmp(Name, "$S_050") == 0)
    return;
	
	char dispbuf[64];	
  US CmdAddr=pSession->CurrCmdAddr;    
  const RRunCmd *pCmd=&pSession->GetFlw().RunCmds[CmdAddr];
  const VXML_CMD_RULE_STRUCT *pCmdRule=&g_FlwRuleMng.CmdRule[pCmd->CmdOperId];
	
	if (Offset == 0)
  {
    sprintf(dispbuf, "%s", OrgName.C_Str());
  }
  else
  {
    sprintf(dispbuf, "%s[%d]", OrgName.C_Str(), Offset);
  }
  MyTrace(4, "FlwName=%s CmdAddr=%s CmdName=%s VarAddr=%s; VarName=%s; Value=%s", 
      pSession->GetRunFlw().FlwName.C_Str(),pCmd->Id.C_Str(),pCmdRule->CmdNameEng,
      Name, dispbuf, Value.C_Str());
}  

//給變量賦值-字符串
void Assign_pCurrent_Str_To_Var(int Offset,const char *VarName, const char *Value)
{
	if(pCurrent->Disp)
	{
		const CStringX *pOrgName;
		CStringX *pC=GetpCurrentVarPointer(VarName,Offset,&pOrgName);
		(*pC) = Value;
  	DispVar(pCurrent,VarName,*pOrgName,*pC,Offset);
	}
	else
	{
		CStringX *pC=GetpCurrentVarPointer(VarName,Offset);
		(*pC) = Value;
	}	
}


//給變量賦值-整數
void Assign_pCurrent_intValue_To_Var(int Offset,const char *VarName, int intValue)
{
    char Value[MAX_VAR_DATA_LEN];   
    sprintf(Value, "%d", intValue);
 		Assign_pCurrent_Str_To_Var(Offset,VarName, Value);    
}
//給變量賦值-浮點數
void Assign_pCurrent_floatValue_To_Var(   int Offset, const char *VarName, double floatValue)
{
    char Value[MAX_VAR_DATA_LEN];
    sprintf(Value, "%f", floatValue);
   	Assign_pCurrent_Str_To_Var(Offset,VarName, Value);   
}
//替換表達式中的變量值
void Replace_pCurrent_Exp_VarValues(   UC ExpType, const char *Exp, char *ReturnExp)
{
    int i, len, l = 0, vl;
    char c, VarName[MAX_VARNAME_LEN], Value[MAX_VAR_DATA_LEN], FuncStr[MAX_VAR_DATA_LEN], FuncValue[MAX_VAR_DATA_LEN];
    bool sqFlag = false, slashId = false, varId = false, FuncId = false;
  
    len = strlen(Exp);
    ReturnExp[0] = 0;
    for (i = 0; i < len; i ++)
    {
        c = Exp[i];
        if (c == '\\')
        {
            if (slashId == false)
            {
                slashId = true; //換義字符標志
            }
            else
            {
                l = strlen(ReturnExp);
                if (l >= MAX_VAR_DATA_LEN)
                {
                  //超出長度
                  goto ErrorLB;
                }
                ReturnExp[l] = '\\';
                ReturnExp[l+1] = '\0';
                slashId = false;
            }
        }
        else if (c == '\'')
        {
            if (slashId == false)
            {
                sqFlag = !sqFlag; //單引號起始標志
            }
            slashId = false;
        }
        else
        {
          if (slashId == true)
          {
            l = strlen(ReturnExp);
            if (l >= MAX_VAR_DATA_LEN)
            {
              //超出長度
              goto ErrorLB;
            }
            ReturnExp[l] = '\\';
            ReturnExp[l+1] = '\0';
            slashId = false;
          }
        }
        if (FuncId == true)
        {
            l = strlen(FuncStr);
            if (l >= MAX_VAR_DATA_LEN)
            {
                //超出長度
               goto ErrorLB;
            }
            FuncStr[l] = c;
            FuncStr[l+1] = '\0';
            if (c == ')')
            {
                //調用函數操作
                pCurrent_Func_Proc(   FuncStr, FuncValue);
                
                    if (ExpType == 3) //為字符串表達式
                    {
                        l = strlen(ReturnExp);
                        vl = strlen(FuncValue);
                        if (l+vl+2 >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        ReturnExp[l] = '\'';
                        ReturnExp[l+1] = '\0';
                        strcat(ReturnExp, FuncValue);
                        l = strlen(ReturnExp);
                        ReturnExp[l] = '\'';
                        ReturnExp[l+1] = '\0';
                        varId = false;
                    }
                    else if (ExpType == 4) //為邏輯運算表達式
                    {
                        if (Check_Float_Str(FuncValue) != 0)
                        {
                            l = strlen(ReturnExp);
                            vl = strlen(FuncValue);
                            if (l+vl+2 >= MAX_EXP_STRING_LEN)
                            {
                                //超出長度
                                goto ErrorLB;
                            }
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                            strcat(ReturnExp, FuncValue);
                            varId = false;
                            l = strlen(ReturnExp);
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                        }
                        else
                        {
                            l = strlen(ReturnExp);
                            vl = strlen(Value);
                            if (l+vl >= MAX_EXP_STRING_LEN)
                            {
                                //超出長度
                                goto ErrorLB;
                            }
                            strcat(ReturnExp, FuncValue);
                            varId = false;
                        }
                    }
                    else if (ExpType == 5) //為算術運算表達式
                    {
                        l = strlen(ReturnExp);
                        vl = strlen(FuncValue);
                        if (l+vl >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        strcat(ReturnExp, FuncValue);
                        varId = false;
                    }
                    else
                    {
                        //非法的表達式
                        goto ErrorLB;
                    }
                                
                FuncId = false;
            }
            continue;
        }
        if (sqFlag == true)
        {
            //是單引號開始
            l = strlen(ReturnExp);
            if (l >= MAX_EXP_STRING_LEN)
            {
                //超出長度
               goto ErrorLB;
            }
            ReturnExp[l] = c;
            ReturnExp[l+1] = '\0';
            //slashId = false;
        }
        else //不是單引號開始
        {
            //slashId = false;
            if (c == '$')
            {
                varId = true;
            }
            else if (c == 'S' || c == 'P' || c == 'R')
            {
                if (varId == true)
                {
                    memset(Value, 0, MAX_VAR_DATA_LEN);
                    memset(VarName, 0, MAX_VARNAME_LEN);
                    strncpy(VarName, &Exp[i-1], 6);
                    Get_pCurrent_VarValue_By_VarName( VarName, Value);
                    
                        if (ExpType == 3) //為字符串表達式
                        {
                            l = strlen(ReturnExp);
                            vl = strlen(Value);
                            if (l+vl+2 >= MAX_EXP_STRING_LEN)
                            {
                                //超出長度
                                goto ErrorLB;
                            }
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                            strcat(ReturnExp, Value);
                            i = i + 4;
                            l = strlen(ReturnExp);
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                            varId = false;
                        }
                        else if (ExpType == 4) //為邏輯運算表達式
                        {
                            if (Check_Float_Str(Value) != 0)
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl+2 >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                   goto ErrorLB;
                                }
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                                strcat(ReturnExp, Value);
                                i = i + 4;
                                varId = false;
                                l = strlen(ReturnExp);
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                            }
                            else
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                strcat(ReturnExp, Value);
                                i = i + 4;
                                varId = false;
                            }
                        }
                        else if (ExpType == 5) //為算術運算表達式
                        {
                            if (Check_Float_Str(Value) == 0)
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                strcat(ReturnExp, Value);
                                i = i + 4;
                            }
                            else
                            {
                                //算術表達式中有參數值錯誤
                                goto ErrorLB;
                            }
                            varId = false;
                        }
                        else
                        {
                            //非法的表達式
                            goto ErrorLB;
                        }
                                       
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                       goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else if (c == '&')
            {
                if (varId == true)
                {
                    memset(Value, 0, MAX_VAR_DATA_LEN);
                    memset(VarName, 0, MAX_VARNAME_LEN);
                    strncpy(VarName, &Exp[i-1], 12);
                    Get_pCurrent_VarValue_By_VarName( VarName, Value);
                    
                        if (ExpType == 3) //為字符串表達式
                        {
                            l = strlen(ReturnExp);
                            vl = strlen(Value);
                            if (l+vl+2 >= MAX_EXP_STRING_LEN)
                            {
                                //超出長度
                                goto ErrorLB;
                            }
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                            strcat(ReturnExp, Value);
                            i = i + 10;
                            l = strlen(ReturnExp);
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                            varId = false;
                        }
                        else if (ExpType == 4) //為邏輯運算表達式
                        {
                            if (Check_Float_Str(Value) != 0)
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl+2 >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                                strcat(ReturnExp, Value);
                                i = i + 10;
                                varId = false;
                                l = strlen(ReturnExp);
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                            }
                            else
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                strcat(ReturnExp, Value);
                                i = i + 10;
                                varId = false;
                            }
                        }
                        else if (ExpType == 5) //為算術運算表達式
                        {
                            if (Check_Float_Str(Value) == 0)
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                strcat(ReturnExp, Value);
                                i = i + 10;
                            }
                            else
                            {
                                //算術表達式中有參數值錯誤
                                goto ErrorLB;
                            }
                            varId = false;
                        }
                        else
                        {
                            //非法的表達式
                            goto ErrorLB;
                        }
                                       
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else if (c == 'F')
            {
                if (varId == true)
                {
                    memset(FuncStr, 0, MAX_VAR_DATA_LEN);
                    strcpy(FuncStr, "$F");
                    FuncId = true;
                    varId = false;
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else if (c == ')')
            {
                if (FuncId == true)
                {
                    l = strlen(FuncStr);
                    if (l >= MAX_VAR_DATA_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    FuncStr[l] = c;
                    FuncStr[l+1] = '\0';
                    //調用函數操作
                    pCurrent_Func_Proc(   FuncStr, FuncValue);
                    
                    FuncId = false;
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else
            {
                if (FuncId == true)
                {
                    l = strlen(FuncStr);
                    if (l >= MAX_VAR_DATA_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    FuncStr[l] = c;
                    FuncStr[l+1] = '\0';
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
        }
    }

    return ;
    
ErrorLB:								
  	ErrVar.Format("Replace_Exp_VarValues = %s error",Exp);
    throw ErrVar.C_Str();    
}
//替換表達式中的變量值
void Replace_pCurrent_Exp_VarValues_For_FindId(   UC ExpType, const char *Exp, char *ReturnExp)
{
    int i, len, l = 0, vl;
    char c, VarName[MAX_VARNAME_LEN], Value[MAX_VAR_DATA_LEN], FuncStr[MAX_VAR_DATA_LEN], FuncValue[MAX_VAR_DATA_LEN];
    //US fun_no;
    bool sqFlag = false, slashId = false, varId = false, FuncId = false, RepId = false;
  //  double FloatValue;

    len = strlen(Exp);
    ReturnExp[0] = 0;
    for (i = 0; i < len; i ++)
    {
        c = Exp[i];
        if (c == '\\')
        {
            if (slashId == false)
            {
                slashId = true; //換義字符標志
            }
            else
            {
                slashId = false;
            }
        }
        else if (c == '\'')
        {
            if (slashId == false)
            {
                sqFlag = !sqFlag;
            }
            slashId = false;
        }
        if (FuncId == true)
        {
            l = strlen(FuncStr);
            if (l > MAX_VAR_DATA_LEN)
            {
                //超出長度
                goto ErrorLB;
            }
            FuncStr[l] = c;
            FuncStr[l+1] = '\0';
            if (c == ')')
            {
                //調用函數操作
                pCurrent_Func_Proc(   FuncStr, FuncValue);
                
                    if (ExpType == 3) //為字符串表達式
                    {
                        l = strlen(ReturnExp);
                        vl = strlen(FuncValue);
                        if (l+vl+2 >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        ReturnExp[l] = '\'';
                        ReturnExp[l+1] = '\0';
                        strcat(ReturnExp, FuncValue);
                        l = strlen(ReturnExp);
                        ReturnExp[l] = '\'';
                        ReturnExp[l+1] = '\0';
                        varId = false;
                    }
                    else if (ExpType == 4) //為邏輯運算表達式
                    {
                        if (Check_Float_Str(FuncValue) != 0)
                        {
                            l = strlen(ReturnExp);
                            vl = strlen(FuncValue);
                            if (l+vl+2 >= MAX_EXP_STRING_LEN)
                            {
                                //超出長度
                                goto ErrorLB;
                            }
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                            strcat(ReturnExp, FuncValue);
                            varId = false;
                            l = strlen(ReturnExp);
                            ReturnExp[l] = '\'';
                            ReturnExp[l+1] = '\0';
                        }
                        else
                        {
                            l = strlen(ReturnExp);
                            vl = strlen(Value);
                            if (l+vl >= MAX_EXP_STRING_LEN)
                            {
                                //超出長度
                                goto ErrorLB;
                            }
                            strcat(ReturnExp, FuncValue);
                            varId = false;
                        }
                    }
                    else if (ExpType == 5) //為算術運算表達式
                    {
                        l = strlen(ReturnExp);
                        vl = strlen(FuncValue);
                        if (l+vl >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        strcat(ReturnExp, FuncValue);
                        varId = false;
                    }
                    else
                    {
                        //非法的表達式
                        goto ErrorLB;
                    }
                               
                FuncId = false;
            }
            continue;
        }
        if (sqFlag == true)
        {
            //是單引號開始
            l = strlen(ReturnExp);
            if (l >= MAX_EXP_STRING_LEN)
            {
                //超出長度
                goto ErrorLB;
            }
            ReturnExp[l] = c;
            ReturnExp[l+1] = '\0';
            slashId = false;
        }
        else //不是單引號開始
        {
            slashId = false;
            if (c == '=' || c == '?' || c == '>' || c == '}' || c == '<' || c == '{' || c == '~')
            {
                RepId = true;
            }
            if (c == '@' || c == '|' || c == '!' || c == '%')
            {
                RepId = false;
            }
            if (c == '$')
            {
                varId = true;
            }
            else if (c == 'S' || c == 'P' || c == 'R')
            {
                if (varId == true)
                {
                    if (RepId == true)
                    {
                        memset(Value, 0, MAX_VAR_DATA_LEN);
                        memset(VarName, 0, MAX_VARNAME_LEN);
                        strncpy(VarName, &Exp[i-1], 6);
                        Get_pCurrent_VarValue_By_VarName(VarName, Value);
                        
                            if (ExpType == 3) //為字符串表達式
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl+2 >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                                strcat(ReturnExp, Value);
                                i = i + 4;
                                l = strlen(ReturnExp);
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                                varId = false;
                            }
                            else if (ExpType == 4) //為邏輯運算表達式
                            {
                                if (Check_Float_Str(Value) != 0)
                                {
                                    l = strlen(ReturnExp);
                                    vl = strlen(Value);
                                    if (l+vl+2 >= MAX_EXP_STRING_LEN)
                                    {
                                        //超出長度
                                        goto ErrorLB;
                                    }
                                    ReturnExp[l] = '\'';
                                    ReturnExp[l+1] = '\0';
                                    strcat(ReturnExp, Value);
                                    i = i + 4;
                                    varId = false;
                                    l = strlen(ReturnExp);
                                    ReturnExp[l] = '\'';
                                    ReturnExp[l+1] = '\0';
                                }
                                else
                                {
                                    l = strlen(ReturnExp);
                                    vl = strlen(Value);
                                    if (l+vl >= MAX_EXP_STRING_LEN)
                                    {
                                        //超出長度
                                        goto ErrorLB;
                                    }
                                    strcat(ReturnExp, Value);
                                    i = i + 4;
                                    varId = false;
                                }
                            }
                            else if (ExpType == 5) //為算術運算表達式
                            {
                                if (Check_Float_Str(Value) == 0)
                                {
                                    l = strlen(ReturnExp);
                                    vl = strlen(Value);
                                    if (l+vl >= MAX_EXP_STRING_LEN)
                                    {
                                        //超出長度
                                        goto ErrorLB;
                                    }
                                    strcat(ReturnExp, Value);
                                    i = i + 4;
                                }
                                else
                                {
                                    //算術表達式中有參數值錯誤
                                    goto ErrorLB;
                                }
                                varId = false;
                            }
                            else
                            {
                                //非法的表達式
                                goto ErrorLB;
                            }
                                               
                    }
                    else
                    {
                        l = strlen(ReturnExp);
                        if (l >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        ReturnExp[l] = '$';
                        ReturnExp[l+1] = '\0';
                        l = strlen(ReturnExp);
                        if (l >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        ReturnExp[l] = c;
                        ReturnExp[l+1] = '\0';
                        varId = false;
                    }
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else if (c == '&')
            {
                if (varId == true)
                {
                    if (RepId == true)
                    {
                        memset(Value, 0, MAX_VAR_DATA_LEN);
                        memset(VarName, 0, MAX_VARNAME_LEN);
                        strncpy(VarName, &Exp[i-1], 12);
                        Get_pCurrent_VarValue_By_VarName( VarName, Value);
                        
                            if (ExpType == 3) //為字符串表達式
                            {
                                l = strlen(ReturnExp);
                                vl = strlen(Value);
                                if (l+vl+2 >= MAX_EXP_STRING_LEN)
                                {
                                    //超出長度
                                    goto ErrorLB;
                                }
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                                strcat(ReturnExp, Value);
                                i = i + 10;
                                l = strlen(ReturnExp);
                                ReturnExp[l] = '\'';
                                ReturnExp[l+1] = '\0';
                                varId = false;
                            }
                            else if (ExpType == 4) //為邏輯運算表達式
                            {
                                if (Check_Float_Str(Value) != 0)
                                {
                                    l = strlen(ReturnExp);
                                    vl = strlen(Value);
                                    if (l+vl+2 >= MAX_EXP_STRING_LEN)
                                    {
                                        //超出長度
                                        goto ErrorLB;
                                    }
                                    ReturnExp[l] = '\'';
                                    ReturnExp[l+1] = '\0';
                                    strcat(ReturnExp, Value);
                                    i = i + 10;
                                    varId = false;
                                    l = strlen(ReturnExp);
                                    ReturnExp[l] = '\'';
                                    ReturnExp[l+1] = '\0';
                                }
                                else
                                {
                                    l = strlen(ReturnExp);
                                    vl = strlen(Value);
                                    if (l+vl >= MAX_EXP_STRING_LEN)
                                    {
                                        //超出長度
                                        goto ErrorLB;
                                    }
                                    strcat(ReturnExp, Value);
                                    i = i + 10;
                                    varId = false;
                                }
                            }
                            else if (ExpType == 5) //為算術運算表達式
                            {
                                if (Check_Float_Str(Value) == 0)
                                {
                                    l = strlen(ReturnExp);
                                    vl = strlen(Value);
                                    if (l+vl >= MAX_EXP_STRING_LEN)
                                    {
                                        //超出長度
                                       goto ErrorLB;
                                    }
                                    strcat(ReturnExp, Value);
                                    i = i + 10;
                                }
                                else
                                {
                                    //算術表達式中有參數值錯誤
                                    goto ErrorLB;
                                }
                                varId = false;
                            }
                            else
                            {
                                //非法的表達式
                                goto ErrorLB;
                            }
                                               
                    }
                    else
                    {
                        l = strlen(ReturnExp);
                        if (l >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        ReturnExp[l] = '$';
                        ReturnExp[l+1] = '\0';
                        l = strlen(ReturnExp);
                        if (l >= MAX_EXP_STRING_LEN)
                        {
                            //超出長度
                            goto ErrorLB;
                        }
                        ReturnExp[l] = c;
                        ReturnExp[l+1] = '\0';
                        varId = false;
                    }
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else if (c == 'F')
            {
                if (varId == true)
                {
                    memset(FuncStr, 0, MAX_VAR_DATA_LEN);
                    strcpy(FuncStr, "$F");
                    FuncId = true;
                    varId = false;
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else if (c == ')')
            {
                if (FuncId == true)
                {
                    l = strlen(FuncStr);
                    if (l > MAX_VAR_DATA_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    FuncStr[l] = c;
                    FuncStr[l+1] = '\0';
                    //調用函數操作
                    pCurrent_Func_Proc(  FuncStr, FuncValue);                    
                    FuncId = false;
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
            else
            {
                if (FuncId == true)
                {
                    l = strlen(FuncStr);
                    if (l > MAX_VAR_DATA_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    FuncStr[l] = c;
                    FuncStr[l+1] = '\0';
                }
                else
                {
                    l = strlen(ReturnExp);
                    if (l >= MAX_EXP_STRING_LEN)
                    {
                        //超出長度
                        goto ErrorLB;
                    }
                    ReturnExp[l] = c;
                    ReturnExp[l+1] = '\0';
                }
            }
        }
    }

    return ;
ErrorLB:								
  	ErrVar.Format("Replace_pCurrent_Exp_VarValues_For_FindId = %s error",Exp);
    throw ErrVar.C_Str();     
}
//根據函數串調用函數
void pCurrent_Func_Proc(  char *FuncStr, char *Value)
{
    int i, len, l, p = 0, fun_no;
    long IntValue;
   // double FloatValue;
    char c, ParaType;
    char FuncName[MAX_VAR_DATA_LEN], ParaValue[MAX_VAR_DATA_LEN];
    char ParaValues[MAX_FUNC_PARA_NUM][MAX_VAR_DATA_LEN]; //參數值
    US dt;
    bool sqFlag = false;

    len = strlen(FuncStr);
    if (len < 8)
        goto ErrorLB;
    
    memset(FuncName, 0, MAX_VAR_DATA_LEN);
    strncpy(FuncName, &FuncStr[3], 3);
    if (Check_Int_Str(FuncName, fun_no) != 0)
    	goto ErrorLB;
    if (fun_no >= MAX_FUNC_NUM)
    	goto ErrorLB;
    for (i = 0; i < MAX_FUNC_PARA_NUM; i ++)
    {
        memset(ParaValues[i], 0, MAX_VAR_DATA_LEN);
    }
    for (i = 7; i < len-1; i ++)
    {
        c = FuncStr[i];
        if (c == '\'')
        {
            if (sqFlag == false)
            {
                l = strlen(ParaValues[p]);
                if (l > MAX_VAR_DATA_LEN)
                	goto ErrorLB;
                ParaValues[p][l] = c;
                ParaValues[p][l+1] = '\0';
                sqFlag = true;
            }
            else
            {
                l = strlen(ParaValues[p]);
                if (l > MAX_VAR_DATA_LEN)
                	goto ErrorLB;
                ParaValues[p][l] = c;
                ParaValues[p][l+1] = '\0';
                sqFlag = false;
            }
        }
        else if (c == ',')
        {
            p ++;
        }
        else
        {
            l = strlen(ParaValues[p]);
            if (l > MAX_VAR_DATA_LEN)
            {
                //超出長度
                goto ErrorLB;
            }
            ParaValues[p][l] = c;
            ParaValues[p][l+1] = '\0';
        }
    }
    if (len > 8)
    {
        p ++;
    }
    if (p != g_FlwRuleMng.FuncRule[fun_no].ParaNums)
    	goto ErrorLB;
    for (i = 0; i < p; i ++)
    {
        len = strlen(ParaValues[i]);
        if (len <= 0)
        	goto ErrorLB;
        if (ParaValues[i][0] == '\'') //字符串單引號開始
        {
            if (len == 1)
            	goto ErrorLB;
            else
            {
                if (ParaValues[i][len-1] == '\'')
                {
                    if (len == 2)
                    {
                        memset(ParaValues[i], 0, MAX_VAR_DATA_LEN);
                        ParaType = '5';
                    }
                    else if (len == 3)
                    {
                        ParaValues[i][0] = ParaValues[i][1];
                        ParaValues[i][1] = '\0';
                        ParaType = '4';
                    }
                    else
                    {
                        strncpy(ParaValues[i], &ParaValues[i][1], len-2);
                        ParaValues[i][len-2] = '\0';
                        ParaType = '5';
                    }
                }
                else
                	goto ErrorLB;
            }
        }
        else
        {
            if (Check_long_Str(ParaValues[i], IntValue) == 0)
            {
                if (IntValue == 0 || IntValue == 1)
                {
                    ParaType = '1';
                }
                else
                {
                    ParaType = '2';
                }
            }
            else
            {
                if (Check_Float_Str(ParaValues[i]) == 0)
                {
                    ParaType = '3';
                }
                else
                {
                    Get_pCurrent_VarValue_By_VarName( ParaValues[i], ParaValue);
                    
                        if (Check_long_Str(ParaValue, IntValue) == 0)
                        {
                            if (IntValue == 0 || IntValue == 1)
                            {
                                ParaType = '1';
                            }
                            else
                            {
                                ParaType = '2';
                            }
                        }
                        else
                        {
                            if (Check_Float_Str(ParaValue) == 0)
                            {
                                ParaType = '3';
                            }
                            else
                            {
                                if (strlen(ParaValue) == 1)
                                {
                                    ParaType = '4';
                                }
                                else
                                {
                                    ParaType = '5';
                                }
                            }
                        }
                        strcpy(ParaValues[i], ParaValue);
                    
                }
            }
        }
        switch (g_FlwRuleMng.FuncRule[fun_no].ParaType[i])
        {
            case '1':
            {
                if (ParaType != '1')
                {
                    //參數類型非法,不是有效的BOOL型
                   goto ErrorLB;
                }
                break;
            }
            case '2':
            {
                if (ParaType != '1' && ParaType != '2')
                {
                    //參數類型非法,不是有效的整數
                    goto ErrorLB;
                }
                break;
            }
            case '3':
            {
                if (ParaType != '1' && ParaType != '2' && ParaType != '3')
                {
                    //參數類型非法,不是有效的浮點數
                    goto ErrorLB;
                }
                break;
            }
            case '4':
            {
                if (ParaType != '1' && ParaType != '4')
                {
                    //參數類型非法,不是有效的單字符
                    goto ErrorLB;
                }
                break;
            }
            case '5':
            {
                break;
            }
        }
    }
    switch (fun_no)
    {
        case FUNC_len: //取字符串長度
        {
            sprintf(Value, "%d", MyStrLen(ParaValues[0]));
            break;
        }
        case FUNC_substring: //取子字符串
        {
            sprintf(Value, "%s", MySubString(ParaValues[0], atoi(ParaValues[1]), atoi(ParaValues[2])));
            break;
        }
        case FUNC_trim:	//去掉字符串兩頭空格
        {
            sprintf(Value, "%s", MyTrim(ParaValues[0]));
            break;
        }
        case FUNC_lefttrim: //去掉字符串左空格
        {
            sprintf(Value, "%s", MyLeftTrim(ParaValues[0]));
            break;
        }
        case FUNC_righttrim: //去掉字符串右邊空格
        {
            sprintf(Value, "%s", MyRightTrim(ParaValues[0]));
            break;
        }
        case FUNC_left:	//取左字符串
        {
            sprintf(Value, "%s", MyLeft(ParaValues[0], atoi(ParaValues[1])));
            break;
        }
        case FUNC_right: //取右字符串
        {
            sprintf(Value, "%s", MyRight(ParaValues[0], atoi(ParaValues[1])));
            break;
        }
        case FUNC_lower: //將字符串小寫化
        {
            sprintf(Value, "%s", MyLower(ParaValues[0]));
            break;
        }
        case FUNC_upper: //將字符串大寫化
        {
            sprintf(Value, "%s", MyUpper(ParaValues[0]));
            break;
        }
        case FUNC_strfmt: //將字符填充空格
        {
            sprintf(Value, "%s", MyStrFmt(ParaValues[0], atoi(ParaValues[1]), atoi(ParaValues[2])));
            break;
        }
        case FUNC_getnow: //取當前日期時間函數
        {
            MyGetNow(Value);
            break;
        }
        case FUNC_getdate: //取日期函數
        {
            if (MyGetDate(ParaValues[0], Value) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            break;
        }
        case FUNC_gettime: //取時間函數
        {
            if (MyGetTime(ParaValues[0], Value) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            break;
        }
        case FUNC_getyear: //取年函數
        {
            if (MyGetYear(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%04d", dt);
            break;
        }
        case FUNC_getmonth:	//取月函數
        {
            if (MyGetMonth(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%02d", dt);
            break;
        }
        case FUNC_getday: //取日函數
        {
            if (MyGetDay(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%02d", dt);
            break;
        }
        case FUNC_gethour: //取時函數
        {
            if (MyGetHour(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%02d", dt);
            break;
        }
        case FUNC_getminute: //取分函數
        {
            if (MyGetMinute(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%02d", dt);
            break;
        }
        case FUNC_getsecond: //取秒函數
        {
            if (MyGetSecond(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%02d", dt);
            break;
        }
        case FUNC_getmsecond: //取毫秒函數
        {
            if (MyGetMsecond(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%03d", dt);
            break;
        }
        case FUNC_getweekday: //取星期函數
        {
            if (MyGetWeekDay(ParaValues[0], dt) != 0)
            {
                //參數值不是有效的日期時間字符串
                goto ErrorLB;
            }
            sprintf(Value, "%d", dt);
            break;
        }
        default:
        {
            //非法的函數標志
            goto ErrorLB;
        }
    }
    return;
    
ErrorLB:								
  	ErrVar.Format("Function = %s error",FuncStr);
    throw ErrVar.C_Str();
}
//替換sql語句中的變量值
void Replace_pCurrentCmd_SqlExp_VarValues(   US AttrId, char *ReturnSqlExp)
{
    int i, len, l = 0;
    char c, VarName[MAX_VARNAME_LEN], Value[MAX_VAR_DATA_LEN];
    bool varId = false;

		//UC ExpType= pCurrentCmd->ParamType[AttrId];
  	const char *SqlExp=pCurrentCmd->ParamValues[AttrId].C_Str();
  	
 //   ErrMsg[0] = 0;
    len = strlen(SqlExp);
    ReturnSqlExp[0] = '\0';
    memset(VarName, 0, MAX_VARNAME_LEN);
    memset(Value, 0, MAX_VAR_DATA_LEN);
    for (i = 0; i < len; i ++)
    {
        c = SqlExp[i];
        if (c == '$')
        {
            varId = true;
        }
        else if (c == 'S' || c == 'P' || c == 'R')
        {
            if (varId == true)
            {
                memset(Value, 0, MAX_VAR_DATA_LEN);
                memset(VarName, 0, MAX_VARNAME_LEN);
                strncpy(VarName, &SqlExp[i-1], 6);
                Get_pCurrent_VarValue_By_VarName(VarName, Value);
                
                if (strlen(Value) + strlen(ReturnSqlExp) < MAX_SQLS_LEN)
                {
                   strcat(ReturnSqlExp, Value);
                   varId = false;
                   i = i + 4;
                }
                else
                {
                        //sql語句長度超出
             //           strcpy(ErrMsg, "ql語句長度超出");
                        goto ErrorLB;
                }
                
            }
            else
            {
                l = strlen(ReturnSqlExp);
                if (l > MAX_SQLS_LEN)
                {
                    //超出長度
             //       strcpy(ErrMsg, "ql語句長度超出");
                    goto ErrorLB;
                }
                ReturnSqlExp[l] = c;
                ReturnSqlExp[l+1] = '\0';
            }
        }
        else if (c == '&')
        {
            if (varId == true)
            {
                memset(Value, 0, MAX_VAR_DATA_LEN);
                memset(VarName, 0, MAX_VARNAME_LEN);
                strncpy(VarName, &SqlExp[i-1], 12);
                Get_pCurrent_VarValue_By_VarName( VarName, Value);
                
                if (strlen(Value) + strlen(ReturnSqlExp) < MAX_SQLS_LEN)
                {
                    strcat(ReturnSqlExp, Value);
                    varId = false;
                    i = i + 10;
                }
                else
                {
                        //sql語句長度超出
             //           strcpy(ErrMsg, "ql語句長度超出");
                        goto ErrorLB;
                }
            }
            else
            {
                l = strlen(ReturnSqlExp);
                if (l > MAX_SQLS_LEN)
                {
                    //超出長度
              //      strcpy(ErrMsg, "ql語句長度超出");
                    goto ErrorLB;
                }
                ReturnSqlExp[l] = c;
                ReturnSqlExp[l+1] = '\0';
            }
        }
        else
        {
            l = strlen(ReturnSqlExp);
            if (l > MAX_SQLS_LEN)
            {
                //超出長度
              //  strcpy(ErrMsg, "ql語句長度超出");
                goto ErrorLB;
            }
            ReturnSqlExp[l] = c;
            ReturnSqlExp[l+1] = '\0';
        }
    }
    return ;
ErrorLB:								
  	ErrVar.Format("Replace_pCurrentCmd_SqlExp_VarValues = %s error",SqlExp);
    throw ErrVar.C_Str();     
}


//根據屬性號取變量的值(返回值0-正確 1-非法的變量名)
void Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(  US AttrId,  char *Value)
{
    char ReturnExp[MAX_EXP_STRING_LEN];//, VarMsg[MAX_MSG_BUF_LEN];
    char Result[MAX_EXP_STRING_LEN];
    int LogicResult;
    double MathResult;

   // ErrMsg[0] = 0;
  
  	UC ValueType= pCurrentCmd->ParamType[AttrId];
  	const char *AttrValue=pCurrentCmd->ParamValues[AttrId].C_Str();
  
    memset(ReturnExp, 0, MAX_EXP_STRING_LEN);
    memset(Result, 0, MAX_EXP_STRING_LEN);
    if (ValueType == 1) //常量
    {
    	strncpy(Value,AttrValue,MAX_VAR_DATA_LEN-1);
    }
    else if (ValueType == 2) //變量名
    {
      Get_pCurrent_VarValue_By_VarName(AttrValue, Value);
    }
    else if (ValueType == 3) //字符串運算表達式
    {
      Replace_pCurrent_Exp_VarValues(ValueType, AttrValue, ReturnExp);
      Operation_String_Exp(ReturnExp, Value);
    }
    else if (ValueType == 4) //邏輯運算表達式
    {
      Replace_pCurrent_Exp_VarValues(ValueType, AttrValue, ReturnExp);
        
      if (Compare_Operation(ReturnExp, Result) == 0)
      {
          LogicResult = Operation_Logic_Exp(Result);
          if (LogicResult == 0)
          {
              Value[0] = '0';
              Value[1] = 0;
          }
          else if (LogicResult == 1)
          {
              Value[0] = '1';
              Value[1] = 0;
          }
          else
          {
            //  sprintf(ErrMsg, "邏輯表達式:%s 的運算結果錯誤", ReturnExp);
              goto ErrorLB;
          }
      }
      else
      {
        //  sprintf(ErrMsg, "邏輯表達式:%s 的運算錯誤", ReturnExp);
          goto ErrorLB;
      }
    }
    else if (ValueType == 5) //算術運算表達式
    {
      Replace_pCurrent_Exp_VarValues(ValueType, AttrValue, ReturnExp);
     
      if (Operation_Math_Exp(ReturnExp, MathResult) == 0)
      {
          if (MathResult == (long)MathResult)
          {
              sprintf(Value, "%ld", (long)MathResult);
          }
          else
          {
              sprintf(Value, "%f", MathResult);
          }
      }
      else
      {
        //  sprintf(ErrMsg, "算術表達式:%s 的運算錯誤", ReturnExp);
          goto ErrorLB;
      }
    }
    else if (ValueType == 6) //sql語句
    {
      Replace_pCurrentCmd_SqlExp_VarValues(AttrId, ReturnExp);       
      strcpy(Value, ReturnExp);
    }
    else
    	goto ErrorLB;
    return ;
ErrorLB:								
  	ErrVar.Format("Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue AttrId = %d error",AttrId);
    throw ErrVar.C_Str();    
}
void CreateAllDirectories(CString strDir)
{
  if (strDir.GetLength() == 0)
    return;
  if(strDir.Right(1)=="\\" || strDir.Right(1)=="/")
    strDir=strDir.Left(strDir.GetLength()-1);
  
  // base case . . .if directory exists
  if(GetFileAttributes(strDir)!=-1)
    return;
  
  // recursive call, one less directory
  int nFound1 = strDir.ReverseFind('\\');
  int nFound2 = strDir.ReverseFind('/');
  if (nFound1>nFound2)
    CreateAllDirectories(strDir.Left(nFound1));
  else
    CreateAllDirectories(strDir.Left(nFound2));
  
  // actual work
  if (strDir.GetLength() > 0)
  {
    CreateDirectory(strDir,NULL);
  }
}
void MyGetMarkerString(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, int nIndex, int nOutputMaxLen, char *pszResult, bool &bFinded)
{
  int nLen, nStartMarkerLen, nEndMarkerLen;
  char *pszStart, *pszFinded;
  int i=0;
  
  memset(pszResult, 0, nOutputMaxLen);
  bFinded = false;
  
  nStartMarkerLen = strlen(pszStartMarker);
  nEndMarkerLen = strlen(pszEndMarker);
  
  if (nStartMarkerLen == 0)
  {
    //規定無開始標記串，則起始為0開始
    if (nEndMarkerLen == 0)
    {
      //規定無開始標記串，又規定無結束標記串，則不需要處理，取原始字符串
      memcpy(pszResult, pszSource, nOutputMaxLen-1);
      bFinded = true;
    } 
    else
    {
      //規定無開始標記串，規定有結束串標記
      pszFinded = strstr(pszSource, pszEndMarker);
      if (pszFinded)
      {
        nLen = (int)(pszFinded - pszSource);
        if (nLen >= nOutputMaxLen)
        {
          nLen = nOutputMaxLen-1;
        }
        memcpy(pszResult, pszSource, nLen);
        bFinded = true;
      }
    }
  } 
  else
  {
    //規定有開始標記串
    pszStart=(char *)pszSource;
    do 
    {
      pszFinded = strstr(pszStart, pszStartMarker);
      if (pszFinded)
      {
        pszStart = pszFinded+nStartMarkerLen;
        if (i == nIndex)
        {
          pszFinded = strstr(pszStart, pszEndMarker);
          if (pszFinded)
          {
            nLen = (int)(pszFinded - pszStart);
            if (nLen >= nOutputMaxLen)
            {
              nLen = nOutputMaxLen-1;
            }
            memcpy(pszResult, pszStart, nLen);
            bFinded = false;
          }
          break;
        }
        i++;
      }
      else
      {
        break;
      }
    } while (true);
  }
}
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult)
{
  int i=0, nTemp, nLen, nOldLen, nNewLen, nCopyedLen=0;
  bool bStartReplace=false;
  char *pszStart, *pszFinded;
  
  memset(pszResult, 0, nOutputMaxLen);

  nOldLen = strlen(pszOldStr);
  nNewLen = strlen(pszNewStr);
  if (nOldLen == 0)
  {
    memcpy(pszResult, pszSource, nOutputMaxLen-1);
  }
  else
  {
    pszStart = (char *)pszSource;
    
    do 
    {
      pszFinded = strstr(pszStart, pszOldStr);
      if (pszFinded)
      {
        //找到需要替換的字符串
        if (i < nStartIndex)
        {
          //沒有到需要更換的位置索引，則直接拷貝該段字符
          nLen = (int)(pszFinded-pszStart)+nOldLen;
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strncat(pszResult, pszStart, nLen);
            pszStart = pszStart+nLen;
            nCopyedLen = nTemp;
          }
          else
          {
            //超過了限定長度則結束
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
            break;
          }
        }
        else
        {
          //到需要更換的位置索引，則拷貝該段前面不匹配的字符串，再追加新的替換字符
          nLen = (int)(pszFinded-pszStart);
          if (nLen > 0)
          {
            //拷貝前面不匹配的字符串
            nTemp = nCopyedLen + nLen;
            if (nTemp < nOutputMaxLen)
            {
              strncat(pszResult, pszStart, nLen);
              pszStart = pszStart+nLen+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszStart, nLen);
              break;
            }
            //追加新的替換字符
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
          else
          {
            //前面沒有不匹配的字符串，直接追加新的替換字符
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              pszStart = pszStart+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
        }
        i++;
        if (i>=nReplaceMaxNum)
        {
          //已經替換了需要的數目
          nLen = strlen(pszStart);
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strcat(pszResult, pszStart);
            nCopyedLen = nTemp;
          }
          else
          {
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
          }
          break;
        }
      } 
      else
      {
        //未找到需要替換的字符串
        nLen = strlen(pszStart);
        nTemp = nCopyedLen + nLen;
        if (nTemp < nOutputMaxLen)
        {
          strcat(pszResult, pszStart);
          nCopyedLen = nTemp;
        }
        else
        {
          nLen = nOutputMaxLen-nCopyedLen-1;
          strncat(pszResult, pszStart, nLen);
        }
        break;
      }
    } while (true);
  }
}
