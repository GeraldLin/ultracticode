//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
extern "C" int __declspec(dllexport) GetXmlAttrValueFromXMLString(const char *pszXMLString, const char *pszNodeRoot, int nNodeIndex, const char *pszAttrName, int &nNodeCount, char *pszAttrValue, int nMaxAttrBytes, char *pszError);
extern "C" int __declspec(dllexport) GetXmlAttrValueFromXMLFile(const char *pszXMLFile, const char *pszNodeRoot, int nNodeIndex, const char *pszAttrName, int &nNodeCount, char *pszAttrValue, int nMaxAttrBytes, char *pszError);

static US CurrCmdAddr; //當前指令地址
static US NextCmdAddr; //不轉移時下一條指令地址
static const VXML_CMD_RULE_STRUCT *pCurrentCmdRule; //指向當前指令對應的規則,本文件內有效
static CXMLSndMsg SndMsg; //發送IVR/DB消息

static UL GetCallOutSerialNo()
{
  static UL serialno=0;
  return ((serialno++)%65535) | ((g_TcpServer.ClientId<<28)&0xF0000000) | (((pCurrent->SerialNo&0x0000FFFF)<<16)&0x0FFF0000);
}

//當前流程發送IVR數據，如果失敗，設置OnHangon事件
static void IVRSendMessageBypCurrent(US MsgId, const CStringX &Msg)
{  
	int len = g_TcpServer.IVRSendMessage(MsgId, Msg);
	if(0==len) //網絡錯誤
  {
	  if (pCurrent != NULL)
      pCurrent->SetOnHangOnEvent(); //設置掛機
  }
}

static void DBSendMessageBypCurrent(US MsgId, const CStringX &Msg)
{  
	int len = g_TcpServer.DBSendMessage(MsgId, Msg);
	if(0==len) //網絡錯誤
  {
	  if (pCurrent != NULL)
      pCurrent->SetOnDBFailEvent(); //設置數據庫失敗	
  }
}

static void GWSendMessageBypCurrent(US gwid, US MsgId, const CStringX &Msg)
{  
	int len = g_TcpServer.GWSendMessage(gwid, MsgId, Msg);
	if(0==len) //網絡錯誤
  {
	  if (pCurrent != NULL)
      pCurrent->SetOnDBFailEvent(); //設置數據庫失敗	
  }
}

static void DBSendMessageBypCurrent1(US MsgId, const CStringX &Msg)
{  
  int len = g_TcpServer.DBSendMessage1(MsgId, Msg.C_Str());
  if(0==len) //網絡錯誤
  {
    if (pCurrent != NULL)
      pCurrent->SetOnDBFailEvent(); //設置數據庫失敗	
  }
}

static void GWSendMessageBypCurrent1(US gwid, US MsgId, const CStringX &Msg)
{  
  int len = g_TcpServer.GWSendMessage1(gwid, MsgId, Msg.C_Str());
  if(0==len) //網絡錯誤
  {
    if (pCurrent != NULL)
      pCurrent->SetOnDBFailEvent(); //設置數據庫失敗	
  }
}
//-----------------------------------------------------------------------------
//-------------------------------流程指令函數----------------------------------
void SendLoginMsg()
{
  CH loginmsg[256];
  sprintf(loginmsg, "<login sessionid='%d' nodetype='2' nodeno='%d' name='parvxml' psw='parvxml' logid='1'/>",
    0x02000000 | ((g_GData.GetClientId() << 16) & 0x00FF0000), g_GData.GetClientId());
  g_TcpServer.IVRSendMessage(MSG_login, loginmsg);
}

//執行子窗口調用指令
static int Proc_CMD_callsubform()
{    
	US AttrId;
    const char *subformaddr;
    int SubFormAddr;

    AttrId = 2;
    
    subformaddr = pCurrentCmd->ParamValues[AttrId].C_Str();
    if (Check_Int_Str(subformaddr, SubFormAddr) == 0)
    {        
    	  if (SubFormAddr < pCurrent->GetFlw().TotalCmds)
        {            
        	return pCurrent->PushCall(0,pCurrent->FlwNo,pCurrent->FlwId,SubFormAddr); //壓棧準備調用子form
        }
    }
    return 1;
}
//執行子窗口調用擴展指令
static int Proc_CMD_callsubformex()
{    
  US AttrId;
  char subformid[MAX_VAR_DATA_LEN];
  int SubFormAddr;
  
  //取subformname屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, subformid);
  
  SubFormAddr = pCurrent->GetCmdAddrByCmdIdName(subformid);

  if (SubFormAddr >= 0)
  {        
    if (SubFormAddr < pCurrent->GetFlw().TotalCmds)
    {            
      return pCurrent->PushCall(0,pCurrent->FlwNo,pCurrent->FlwId,SubFormAddr); //壓棧準備調用子form
    }
  }
  MyTrace(0, "callsubformex subformid=%s not exist", subformid);
  return 1;
}
//執行外部窗口調用指令
static int Proc_CMD_callextform()
{    US AttrId;
    const char *extvxml;
    const char *formid;
    const char *subformid;
    US SubFlwNo , SubFlwId;
  

    //取extvxml屬性值
    AttrId = 1;
    
    extvxml= pCurrentCmd->ParamValues[AttrId].C_Str();

    //取formid屬性值
    AttrId = 2;
    
    formid=pCurrentCmd->ParamValues[AttrId].C_Str();

    //根據調用的外部流程文件名(不包括文件擴展名)查找流程序號    
 		if(g_RunFlwMng.FindFlowNoByName(extvxml,SubFlwNo)) //找到了
    {        
      SubFlwId = g_RunFlwMng[SubFlwNo].FlwId;
      for (int i = 0; i < g_RunFlwMng[SubFlwNo].Flw[SubFlwId].TotalCmds && i < MAX_CMD_LINES; i ++)
      {            
        if (g_RunFlwMng[SubFlwNo].Flw[SubFlwId].RunCmds[i].CmdOperId == CMD_mainform 
          || g_RunFlwMng[SubFlwNo].Flw[SubFlwId].RunCmds[i].CmdOperId == CMD_subform)
        {                
          subformid=g_RunFlwMng[SubFlwNo].Flw[SubFlwId].RunCmds[i].ParamValues[0].C_Str();
          if (stricmp(formid, subformid) == 0)
          {                    
            //找到外部流程的窗口
            return pCurrent->PushCall(1,SubFlwNo,SubFlwId,i); //壓棧準備調用子form
          }
        }
      }
    }
    return 1;
}
//執行變量賦值指令
static int Proc_CMD_assign()
{    
    US AttrId;
    const char * name;
    CH Value[MAX_VAR_DATA_LEN];
    //取name屬性值
    AttrId = 1;
    name=pCurrentCmd->ParamValues[AttrId].C_Str();
        

    //取expr屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( name, Value) ;    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取其他會話通道變量值指令
static int Proc_CMD_getvalue()
{    
    US AttrId;
    
    const char * varname1;    
    const char * varname2;
    CH Value[MAX_VAR_DATA_LEN];
    int SessionId1, SessionId2;
		CSession *pSave=pCurrent;

    //取sessionid1屬性值
    AttrId = 1;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);   
   	if (Check_Int_Str(Value, SessionId1) != 0)
    {    
        return 1;
    }
     
    //取sessionid2屬性值
    AttrId = 3;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Check_Int_Str(Value, SessionId2) != 0)
    {        //屬性sessionid2的值非法
        ////sprintf(ErrMsg, "屬性sessionid2的值: %s 非法!", sessionid2);
        return 1;
    }
    if (SessionId1 == 0)
    {        SessionId1 = pCurrent->GetSessionId();
    }
    if (SessionId2 == 0)
    {        SessionId2 = pCurrent->GetSessionId();
    }
    //printf("SessionId1=%d SessionId2=%d\n", SessionId1, SessionId2);
    if(!g_SessionMng.IsSessionNoAvail(SessionId1))
    {        //該通道未在線
        ////sprintf(ErrMsg, "該通道SessionId1 = %d 未在線!", SessionId1);
        return 1;
    }
    //取varname1屬性值
    AttrId = 2;
    varname1=pCurrentCmd->ParamValues[AttrId].C_Str();
    //取varname2屬性值
    AttrId = 4;
    varname2=pCurrentCmd->ParamValues[AttrId].C_Str();
    
    if(!g_SessionMng.IsSessionNoAvail(SessionId2))
    {
      if (strcmp(varname2, "$S_071") == 0) //狀態判斷作為特例 edit by zgj 2006-09-20
        strcpy(Value, "0");
      else
        return 1;
    }
    else
    {
		  pCurrent=&g_SessionMng[SessionId2];
      Get_pCurrent_VarValue_By_VarName(varname2, Value);
    }      	
    pCurrent=&g_SessionMng[SessionId1];
  try
  {  
    Assign_pCurrent_Str_To_Var( varname1, Value);
    //printf("Get Value=%s\n", Value);
  }
  catch (...)
  {            //賦值失敗
    	pCurrent=pSave;
		  return 1;
  }
 		pCurrent=pSave;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//-----------------------------------------------------------------------------
//Edit 2007-08-09 add
static bool CheckCanSendMsg2IVR(CSession *pSession, US EventType, US nextcmdaddr)
{
  if (pSession->RecvHangonId == 1)
  {
    switch (EventType)
    {
      case UC_OnEvent: //1 通用事件
	      pSession->EventVars[UC_OnEvent] = OnFail;
        pSession->EventVars[UC_MsgEvent] = OnFail;
        break;
      case UC_MsgEvent: //2 通信處理消息響應事件
	      pSession->EventVars[UC_OnEvent] = OnFail;
        pSession->EventVars[UC_MsgEvent] = OnFail;
        break;
      case UC_PlayEvent: //3 放音收碼事件
        pSession->EventVars[UC_PlayEvent] = OnPlayError;
        break;
      case UC_RecordEvent: //6 錄音事件
        pSession->EventVars[UC_RecordEvent] = OnRecordError;
        break;
      case UC_CalloutEvent: //7 呼出事件
        pSession->EventVars[UC_CalloutEvent] = OnOutFail;
        break;
      case UC_ACDEvent: //8 坐席分配事件
        pSession->EventVars[UC_ACDEvent] = OnACDFail;
        break;
      case UC_FaxEvent: //19 傳真操作事件
        if (pSession->FaxSndRcv == 1)
        {
          pSession->EventVars[UC_FaxEvent] = OnrFaxNoSignal;
        }
        else if (pSession->FaxSndRcv == 2)
        {
          pSession->EventVars[UC_FaxEvent] = OnsFaxNoSignal;
        }
        break;
      case UC_CfcEvent: //22 會議操作事件
        pSession->EventVars[UC_CfcEvent] = OnACDFail;
        break;
    }
    pSession->CurrCmdAddr = nextcmdaddr;
    return false;
  }
  return true;
}
//----------------通信指令-----------------------------------------------------
//執行應答來話指令
static int Proc_CMD_answer()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_answer].MsgNameEng, pCurrent);

    //取answertype屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_answer, SndMsg.GetBuf());
    pCurrent->StartBillId = 1;
    pCurrent->BillTime=
    pCurrent->AnsTime=time(0);
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行發送信令指令
static int Proc_CMD_sendsignal()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendsignal].MsgNameEng, pCurrent);

    //取signaltype屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取signal屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取cause屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_sendsignal, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行掛機釋放指令
static int Proc_CMD_hangon()
{		
    US AttrId, gwid;   
    CH Value[MAX_VAR_DATA_LEN];
    UC RelChnType;
    US RelSessionId, RelChnNo,CurSessionId;
    US CallFlwNo, CallFlwId;
		const CH *AttrName;

    pCurrent->InitIVREvent();
		CurSessionId=pCurrent->GetSessionId();

    //取sessionid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
		
		Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    RelSessionId = atoi(Value);
    RelSessionId = RelSessionId & 0x0000FFFF;
    if (RelSessionId == 0)
      RelSessionId = CurSessionId;
        
    if ( !g_SessionMng.IsSessionNoAvail(RelSessionId))
    {       
    	 return 0;
    }

    //取chntype屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    RelChnType = atoi(Value);

    //取chnno屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
       
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    RelChnNo = atoi(Value);

    if (RelChnType )
        RelSessionId=g_SessionMng.Get_SessionId_By_Chn((UC)RelChnType, RelChnNo);
  
  	CSession *pSave=pCurrent;
  	pCurrent=&g_SessionMng[RelSessionId];
  	
    if (RelSessionId)
    {            	        
      if (pCurrent->RecvHangonId == 0)
      {
        SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_hangon].MsgNameEng,pCurrent);
    		SndMsg.AddTailToBuf();
        IVRSendMessageBypCurrent(MSG_hangon, SndMsg.GetBuf());
      }
    }
    else
    {        
    		SndMsg.GetBuf().Format("<hangon sessionid='0' cmdaddr='0' chantype='%d' channo='%d'/>",
            RelChnType, RelChnNo);
       
        IVRSendMessageBypCurrent(MSG_hangon, SndMsg.GetBuf());
        
        pCurrent=pSave;
        pCurrent->CurrCmdAddr = NextCmdAddr;
        return 0;
    }

    if(CurSessionId==RelSessionId)  
    {
     	if (pCurrent->DBQueryResult == 1)
      {
        gwid = pCurrent->GetDBGwId();
        if (gwid < MAX_GW_NUM && ((gwid == 0 && g_TcpServer.IsDBConnected()) || (gwid > 0 && g_TcpServer.IsGWConnected(gwid))))
        {
    		  SndMsg.GetBuf().Format("<dbclose sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d'/>",
              pCurrent->SerialNo, pCurrent->CurrCmdAddr, RelChnType, RelChnNo);
          if (gwid == 0)
            DBSendMessageBypCurrent(MSG_dbclose,  SndMsg.GetBuf());
          else
            GWSendMessageBypCurrent(gwid, MSG_dbclose, SndMsg.GetBuf());
        }
      }

      CallFlwNo = pCurrent->FlwNo;
      CallFlwId = pCurrent->FlwId;
      if (g_RunFlwMng[CallFlwNo].AutoStartId == RelSessionId)
      {
        g_RunFlwMng[CallFlwNo].AutoStartId = 0;
      }

      g_SessionMng.Free_SessionId(RelSessionId);
      pCurrent=0;
      SendTotalSessionsToAllLOG();
      SendOneFLWSessionsToAllLOG(CallFlwNo);
    }
    else
    {
      pCurrent->SetOnHangOnEvent();
      pCurrent=pSave;
      pCurrent->CurrCmdAddr = NextCmdAddr;
    }
    //沒有下一條指令了
    return 0;
}
//執行DTMF按鍵規則定義指令
static int Proc_CMD_dtmfrule()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    if (CheckCanSendMsg2IVR(pCurrent, 0, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_dtmfrule].MsgNameEng, pCurrent);

    //取ruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取digits屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取break屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取transtype屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取minlength屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取maxlength屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取timeout屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取termdigits屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取termid屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_dtmfrule, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行收碼指令
static int Proc_CMD_getdtmf()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_getdtmf].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_getdtmf,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行放音規則定義指令
static int Proc_CMD_playrule()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    if (CheckCanSendMsg2IVR(pCurrent, 0, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playrule].MsgNameEng, pCurrent);

    //取ruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取format屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取repeat屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取broadcast屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取volume屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取speech屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取voice屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取pause屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取error屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_playrule, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行ASR規則定義指令
static int Proc_CMD_asrrule()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    if (CheckCanSendMsg2IVR(pCurrent, 0, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_asrrule].MsgNameEng, pCurrent);

    //取ruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取grammar屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_asrrule, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行改變放音音量指令
static int Proc_CMD_setvolume()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_setvolume].MsgNameEng, pCurrent);

    //取volume屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    SndMsg.AddTailToBuf();    

    IVRSendMessageBypCurrent(MSG_setvolume, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行單文件放音指令
static int Proc_CMD_playfile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playfile].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取startpos屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取length屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();

    IVRSendMessageBypCurrent(MSG_playfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行改變放音指針位置指令
static int Proc_CMD_setplaypos()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_setplaypos].MsgNameEng, pCurrent);

    //取pos屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(  AttrId, Value);
      	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_setplaypos, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行多文件放音指令
static int Proc_CMD_playfiles()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playfiles].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename1屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取filename2屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename3屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
    
    IVRSendMessageBypCurrent(MSG_playfiles, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行合成串放音指令
static int Proc_CMD_playcompose()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playcompose].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取comptype屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取string屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
    
    IVRSendMessageBypCurrent(MSG_playcompose, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行多語音合成放音指令
static int Proc_CMD_multiplay()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_multiplay].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取mixtype1屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取content1屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取mixtype2屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取content2屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取mixtype3屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取content3屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
   
    IVRSendMessageBypCurrent(MSG_multiplay, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行文本串轉語音指令
static int Proc_CMD_ttsstring()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_TTSEvent] = WAIT_EVENT;
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_ttsstring].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttstype屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttsgrammar屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttsparam屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttsformat屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);
    
    //取ttsfilename屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      SndMsg.AddItemToBuf(AttrName,Value); 

    //取string屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      SndMsg.AddItemToBuf(AttrName,Value); 
    
    SndMsg.AddTailToBuf();  
  
    IVRSendMessageBypCurrent(MSG_ttsstring, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行文本文件轉語音指令
static int Proc_CMD_ttsfile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_TTSEvent] = WAIT_EVENT;
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_ttsfile].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttstype屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttsgrammar屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttsparam屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ttsformat屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);
    
    //取ttsfilename屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      SndMsg.AddItemToBuf(AttrName,Value); 

    //取filename屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      SndMsg.AddItemToBuf(AttrName,Value); 

    SndMsg.AddTailToBuf();  
 
    IVRSendMessageBypCurrent(MSG_ttsfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行發送DTMF指令
static int Proc_CMD_senddtmf()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_senddtmf].MsgNameEng, pCurrent);

    //取duration屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取pause屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取dtmfs屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_senddtmf, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行發送FSK指令
static int Proc_CMD_sendfsk()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendfsk].MsgNameEng, pCurrent);

    //取fskheader屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取fskcmd屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取fskdata屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取fskdatalen屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取crctype屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_sendfsk, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行會議室放音指令
static int Proc_CMD_playcfc()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playcfc].MsgNameEng, pCurrent);

    //取playruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取cfcno屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename1屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename2屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename3屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_playcfc, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行播放信號音指令
static int Proc_CMD_playtone()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playtone].MsgNameEng, pCurrent);

    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取tonetype屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取repeat屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取release屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_playtone, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行錄音指令
static int Proc_CMD_recordfile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_RecordEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_RecordEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_recordfile].MsgNameEng, pCurrent);

    //取filename屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取beep屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取maxtime屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取termchar屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取format屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取writemode屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
    
    IVRSendMessageBypCurrent(MSG_recordfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行監視錄音指令
static int Proc_CMD_recordcfc()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_RecordEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_RecordEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_recordcfc].MsgNameEng, pCurrent);

    //取cfcno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filename屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取maxtime屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取format屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取writemode屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
    
    IVRSendMessageBypCurrent(MSG_recordcfc, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行停止放音錄音收碼指令
static int Proc_CMD_stop()
{    
    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_stop].MsgNameEng, pCurrent);
    SndMsg.AddTailToBuf();    

    IVRSendMessageBypCurrent(MSG_stop, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行電話呼出指令
static int Proc_CMD_callout()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_CalloutEvent] = WAIT_EVENT;
    //if (CheckCanSendMsg2IVR(pCurrent, UC_CalloutEvent, NextCmdAddr) == false)
    //  return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_callout].MsgNameEng, pCurrent);

    //取serialno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取callertype屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取calltype屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value);

    //取callerno屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取calledno屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取waittime屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取routeno屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取vxmlid屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取funcno屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取times屬性值
    AttrId = 10;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);
    
    pCurrent->DialSerialNo = GetCallOutSerialNo();
    SndMsg.AddLongItemToBuf("sessionno",pCurrent->DialSerialNo);
		
    //取param屬性值
    AttrId = 11;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取outchntype屬性值
    AttrId = 17;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取outchnno屬性值
    AttrId = 18;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取callmode屬性值
    AttrId = 19;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取callpoint屬性值
    AttrId = 20;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取callinterval屬性值
    AttrId = 21;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取calledtype屬性值
    AttrId = 22;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_callout,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取消呼出指令
static int Proc_CMD_cancelcallout()
{   
    pCurrent->InitIVREvent();   

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_cancelcallout].MsgNameEng, pCurrent);
		SndMsg.AddIntItemToBuf("sessionno",pCurrent->DialSerialNo);
    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_cancelcallout, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行發送被叫號碼指令
static int Proc_CMD_sendcalledno()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    //if (CheckCanSendMsg2IVR(pCurrent, 1, NextCmdAddr) == false)
    //  return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendsam].MsgNameEng, pCurrent);

    //取calledno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

		SndMsg.AddIntItemToBuf("sessionno",pCurrent->DialSerialNo);
    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_sendsam, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行電話轉接指令
static int Proc_CMD_transfer()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_CalloutEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_CalloutEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_transfer].MsgNameEng, pCurrent);

    //取sourcechntype屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取sourcechnno屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取tranmode屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取callertype屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取calledtype屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取calltype屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value);

    //取callerno屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取calledno屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取waittime屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取routeno屬性值
    AttrId = 10;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取outchntype屬性值
    AttrId = 11;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取outchnno屬性值
    AttrId = 12;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取waitvoc屬性值
    AttrId = 13;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取param屬性值
    AttrId = 14;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
      
    pCurrent->DialSerialNo = GetCallOutSerialNo();
    SndMsg.AddLongItemToBuf("sessionno",pCurrent->DialSerialNo);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_transfer, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行分配坐席指令
static int Proc_CMD_callseat()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_ACDEvent] = WAIT_EVENT;
    //if (CheckCanSendMsg2IVR(pCurrent, UC_ACDEvent, NextCmdAddr) == false)
    //  return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_callseat].MsgNameEng, pCurrent);

    //取callerno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取calledno屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取seatno屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取workerno屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取seattype屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取acdrule屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取callmode屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取groupno屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取calltype屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取seatgroupno屬性值
    AttrId = 10;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
      
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
      
    //取level屬性值
    AttrId = 11;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取levelrule屬性值
    AttrId = 12;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取waittime屬性值
    AttrId = 13;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取ringtime屬性值
    AttrId = 14;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取vxmlid屬性值
    AttrId = 15;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取funcno屬性值
    AttrId = 16;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取waitid屬性值
    AttrId = 17;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取priority屬性值
    AttrId = 18;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取servicetype屬性值
    AttrId = 19;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取crdserialno屬性值
    AttrId = 20;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取param屬性值
    AttrId = 21;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);
  
    pCurrent->DialSerialNo = GetCallOutSerialNo();
    SndMsg.AddLongItemToBuf("sessionno",pCurrent->DialSerialNo);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_callseat,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取消分配坐席指令
static int Proc_CMD_stopcallseat()
{       
    pCurrent->InitIVREvent();  

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_stopcallseat].MsgNameEng, pCurrent);
  	SndMsg.AddIntItemToBuf("sessionno",pCurrent->DialSerialNo);  	
    SndMsg.AddTailToBuf();    

    IVRSendMessageBypCurrent(MSG_stopcallseat, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行創建會議指令
static int Proc_CMD_createcfc()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_createcfc].MsgNameEng, pCurrent);

    //取cfcno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取name屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value);

    //取onwer屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取presiders屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取talkers屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取listeners屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取password屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取autorecord屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取autofree屬性值
    AttrId = 9;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
  
    IVRSendMessageBypCurrent(MSG_createcfc, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行釋放會議資源指令
static int Proc_CMD_destroycfc()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    //if (CheckCanSendMsg2IVR(pCurrent, 1, NextCmdAddr) == false)
    //  return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_destroycfc].MsgNameEng, pCurrent);

    //取cfcno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取destroyid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_destroycfc, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行加入會議指令
static int Proc_CMD_joincfc()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    //pCurrent->InitIVREvent();
    pCurrent->EventVars[UC_CfcEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_CfcEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_joincfc].MsgNameEng, pCurrent);

    //取cfcno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取jointype屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取knocktone屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取emptyevent屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_joincfc, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行退出會議指令
static int Proc_CMD_unjoincfc()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_unjoincfc].MsgNameEng, pCurrent);

    //取cfcno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_unjoincfc, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行通道交換指令
static int Proc_CMD_link()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_link].MsgNameEng, pCurrent);

    //取linkchantype屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取linkchanno屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取linkmode屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_link, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行與對方通話指令
static int Proc_CMD_talkwith()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    int SessionId1, ChnType1, ChnNo1;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    //取sessionid屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Check_Int_Str(Value, SessionId1) != 0)
    {        return 1;
    }
    if ( !g_SessionMng.IsSessionNoAvail(SessionId1))
    {        //通道未在線錯誤
        ////sprintf(ErrMsg, "會話標志號 %d 超出!", SessionId1);
        return 1;
    }
    ChnType1 = g_SessionMng[SessionId1].ChnType;
    ChnNo1 = g_SessionMng[SessionId1].ChnNo;
   // sprintf(MsgBuf, "<talkwith sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' linkchantype='%d' linkchanno='%d'/>",
   //     pCurrent->SerialNo, pCurrent->GetFlw_CmdAddr(), ChnType, ChnNo, ChnType1, ChnNo1);
      
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_talkwith].MsgNameEng,pCurrent);
		SndMsg.AddIntItemToBuf("linkchantype",ChnType1);
		SndMsg.AddIntItemToBuf("linkchanno",ChnNo1);
		
    SndMsg.AddTailToBuf();

    IVRSendMessageBypCurrent(MSG_talkwith,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行監聽對方通話指令
static int Proc_CMD_listenfrom()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    int SessionId1, ChnType1, ChnNo1;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    //取sessionid屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Check_Int_Str(Value, SessionId1) != 0)
    {        return 1;
    }
    if ( !g_SessionMng.IsSessionNoAvail(SessionId1))
    {        //通道未在線錯誤
        ////sprintf(ErrMsg, "會話標志號 %d 超出!", SessionId1);
        return 1;
    }
    ChnType1 = g_SessionMng[SessionId1].ChnType;
    ChnNo1 = g_SessionMng[SessionId1].ChnNo;
  //  sprintf(MsgBuf, "<listenfrom sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d' linkchantype='%d' linkchanno='%d'/>",
  //      pCurrent->SerialNo, pCurrent->GetFlw_CmdAddr(), ChnType, ChnNo, ChnType1, ChnNo1);
    
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_listenfrom].MsgNameEng,pCurrent);
		SndMsg.AddIntItemToBuf("linkchantype",ChnType1);
		SndMsg.AddIntItemToBuf("linkchanno",ChnNo1);
		
    SndMsg.AddTailToBuf();
    IVRSendMessageBypCurrent(MSG_listenfrom, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行強插對方通話指令
static int Proc_CMD_inserttalk()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    int SessionId1, ChnType1=0, ChnNo1=0;
    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    //取sessionid屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Check_Int_Str(Value, SessionId1) == 0)
    {
      if (g_SessionMng.IsSessionNoAvail(SessionId1))
      {
        ChnType1 = g_SessionMng[SessionId1].ChnType;
        ChnNo1 = g_SessionMng[SessionId1].ChnNo;
      }
    }
    if (ChnType1 == 0)
    {
      //取chntype屬性值
      AttrId = 2;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      ChnType1 = atoi(Value);
      //取chnno屬性值
      AttrId = 3;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      ChnNo1 = atoi(Value);
    }
    
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_inserttalk].MsgNameEng,pCurrent);
		SndMsg.AddIntItemToBuf("linkchantype",ChnType1);
		SndMsg.AddIntItemToBuf("linkchanno",ChnNo1);
		
    SndMsg.AddTailToBuf();

    IVRSendMessageBypCurrent(MSG_inserttalk, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行三方通話指令
static int Proc_CMD_threetalk()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    int SessionId1, ChnType1=0, ChnNo1=0;
    int SessionId2, ChnType2=0, ChnNo2=0;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    //取sessionid1屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);

    if (Check_Int_Str(Value, SessionId1) == 0)
    {
      if (g_SessionMng.IsSessionNoAvail(SessionId1))
      {
        ChnType1 = g_SessionMng[SessionId1].ChnType;
        ChnNo1 = g_SessionMng[SessionId1].ChnNo;
      }
    }
    if (ChnType1 == 0)
    {
      //取chntype1屬性值
      AttrId = 2;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      ChnType1 = atoi(Value);
      //取chnno1屬性值
      AttrId = 3;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      ChnNo1 = atoi(Value);
    }

    //取sessionid2屬性值
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Check_Int_Str(Value, SessionId2) == 0)
    {
      if (g_SessionMng.IsSessionNoAvail(SessionId2))
      {
        ChnType2 = g_SessionMng[SessionId2].ChnType;
        ChnNo2 = g_SessionMng[SessionId2].ChnNo;
      }
    }
    if (ChnType2 == 0)
    {
      //取chntype2屬性值
      AttrId = 5;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      ChnType2 = atoi(Value);
      //取chnno2屬性值
      AttrId = 6;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      ChnNo2 = atoi(Value);
    }
    
		SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_threetalk].MsgNameEng,pCurrent);
		SndMsg.AddIntItemToBuf("linkchantype1",ChnType1);
		SndMsg.AddIntItemToBuf("linkchanno1",ChnNo1);
		SndMsg.AddIntItemToBuf("linkchantype2",ChnType2);
		SndMsg.AddIntItemToBuf("linkchanno2",ChnNo2);
		
    SndMsg.AddTailToBuf();
    	
    IVRSendMessageBypCurrent(MSG_threetalk,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行結束通話指令
static int Proc_CMD_stoptalk()
{
	    pCurrent->InitIVREvent();
      if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

      SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_stoptalk].MsgNameEng,pCurrent);
    	SndMsg.AddTailToBuf();
  
    IVRSendMessageBypCurrent(MSG_stoptalk, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行發送傳真指令
static int Proc_CMD_sendfax()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->EventVars[UC_FaxEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_FaxEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendfax].MsgNameEng, pCurrent);

    //取faxcsid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取resolution屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取totalpages屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取pagelist屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取faxheader屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取faxfooter屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取barcode屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取filename屬性值
    AttrId = 8;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    
    pCurrent->FaxSndRcv = 2; //發送傳真
    IVRSendMessageBypCurrent(MSG_sendfax, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行接收傳真指令
static int Proc_CMD_recvfax()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->EventVars[UC_FaxEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_FaxEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_recvfax].MsgNameEng, pCurrent);

    //取faxcsid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取barcode屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取filename屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    SndMsg.AddTailToBuf();
    
    pCurrent->FaxSndRcv = 1; //接收傳真
    IVRSendMessageBypCurrent(MSG_recvfax, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行檢查文件路徑指令
static int Proc_CMD_checkpath()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_checkpath].MsgNameEng, pCurrent);

    //取path屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_checkpath, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行檢查文件
static int Proc_CMD_checkfile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_checkfile].MsgNameEng, pCurrent);

    //取filename屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
 
    IVRSendMessageBypCurrent(MSG_checkfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行創建文件路徑指令
static int Proc_CMD_createpath()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_createpath].MsgNameEng, pCurrent);

    //取path屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_createpath, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行刪除文件路徑指令
static int Proc_CMD_deletepath()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_deletepath].MsgNameEng, pCurrent);

    //取path屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_deletepath, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取文件個數指令
static int Proc_CMD_getfilenum()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_getfilenum].MsgNameEng, pCurrent);

    //取path屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filespec屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  

    IVRSendMessageBypCurrent(MSG_getfilenum, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取文件名指令
static int Proc_CMD_getfilename()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_getfilename].MsgNameEng, pCurrent);

    //取path屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取filespec屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取maxfiles屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
  
    IVRSendMessageBypCurrent(MSG_getfilename,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行改文件名指令
static int Proc_CMD_renname()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_renname].MsgNameEng, pCurrent);

    //取oldfilename屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取newfilename屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_renname, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行拷貝文件指令
static int Proc_CMD_copyfile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_copyfile].MsgNameEng, pCurrent);

    //取soufilename屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    //取desfilename屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_copyfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

//執行刪除文件指令
static int Proc_CMD_deletefile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_deletefile].MsgNameEng, pCurrent);

    //取filename屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_deletefile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行清除放音合成緩沖區指令
static int Proc_CMD_clearmixer()
{   
    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;
    
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_clearmixer].MsgNameEng,pCurrent);
    SndMsg.AddTailToBuf();
    	
    IVRSendMessageBypCurrent(MSG_clearmixer, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加文件到放音合成緩沖區指令
static int Proc_CMD_addfile()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_addfile].MsgNameEng, pCurrent);

    //取filename屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  
    

    IVRSendMessageBypCurrent(MSG_addfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加日期時間到放音合成緩沖區指令
static int Proc_CMD_adddatetime()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_adddatetime].MsgNameEng, pCurrent);

    //取datetime屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 		SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();  

    IVRSendMessageBypCurrent(MSG_adddatetime, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加金額到放音合成緩沖區指令
static int Proc_CMD_addmoney()
{   
    US AttrId;   
    CH Value[MAX_VAR_DATA_LEN];    
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_addmoney].MsgNameEng,pCurrent);
   
    //取money屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf(); 

    IVRSendMessageBypCurrent(MSG_addmoney,  SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加數值到放音合成緩沖區指令
static int Proc_CMD_addnumber()
{   
    US AttrId;  
    CH Value[MAX_VAR_DATA_LEN];   
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

   	SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_addnumber].MsgNameEng,pCurrent);
   
    //取number屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();     

    IVRSendMessageBypCurrent(MSG_addnumber, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加數字串到放音合成緩沖區指令
static int Proc_CMD_adddigits()
{    
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];    
    const char *AttrName;

    pCurrent->InitIVREvent();   
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_adddigits].MsgNameEng,pCurrent);
    
    //取digits屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf(); 
    
    IVRSendMessageBypCurrent(MSG_adddigits, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加字符串到放音合成緩沖區指令
static int Proc_CMD_addchars()
{   
    US AttrId;  
    CH Value[MAX_VAR_DATA_LEN];   
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_addchars].MsgNameEng,pCurrent);
    
    //取chars屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf(); 
    

    IVRSendMessageBypCurrent(MSG_addchars, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加TTS字符串到放音合成緩沖區指令
static int Proc_CMD_addttsstr()
{    
    US AttrId;   
    CH Value[MAX_VAR_DATA_LEN];    
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_addttsstr].MsgNameEng,pCurrent);
    
    //取ttsstr屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf(); 

    IVRSendMessageBypCurrent(MSG_addttsstr, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行增加TTS文件到放音合成緩沖區指令
static int Proc_CMD_addttsfile()
{    
    US AttrId;   
    CH Value[MAX_VAR_DATA_LEN];    
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_addttsfile].MsgNameEng,pCurrent);
    
    //取ttsfile屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();    

    IVRSendMessageBypCurrent(MSG_addttsfile, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行播放合成緩沖區指令
static int Proc_CMD_playmixer()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;   

    //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
    pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
    if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_playmixer].MsgNameEng,pCurrent);
    
    //取dtmfruleid屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取playruleid屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 

    //取asrruleid屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();
   
    IVRSendMessageBypCurrent(MSG_playmixer, SndMsg.GetBuf());  
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//-----------------------------------------------------------------------------
//----------------文本文件指令-------------------------------------------------
//執行打開文本文件指令
static int Proc_CMD_fopen()
{    
    US AttrId;
    US GotoAddr;
    CH filename[MAX_VAR_DATA_LEN], mode[MAX_VAR_DATA_LEN];
    CH Value[MAX_VAR_DATA_LEN];
    pCurrent->RWTextResult = 0xFFFF;
    //pCurrent->CurrCmdAddr = NextCmdAddr;

    //取filename屬性值
    AttrId = 1;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Value[0] == '/')
      strcpy(filename, Value);
    else
      sprintf(filename, "%s/%s", g_szDatPath, Value);
    
    //取mode屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(mode, Value);
    
    //取failgoto屬性值
    //AttrId = 3;
     
    //取gotoaddr屬性值
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    GotoAddr = atoi(Value);
    if (MyCreateLastDir(filename) != 0)
    {
      pCurrent->CurrCmdAddr = GotoAddr;
      return 0;
    }

    if (pCurrent->Ftext != NULL)
    {        fclose(pCurrent->Ftext);
        pCurrent->Ftext = NULL;
    }
    pCurrent->Ftext = fopen(filename, mode);
    if (pCurrent->Ftext == NULL)
    {        pCurrent->RWTextResult = OnDBFail;
        pCurrent->CurrCmdAddr = GotoAddr;
    }
    else
    {        pCurrent->RWTextResult = OnDBSuccess;
        pCurrent->CurrCmdAddr = NextCmdAddr;
    }
    return 0;
}
//執行讀文本文件指令
static int Proc_CMD_fread()
{    
    US AttrId;
    US GotoAddr;
    CH filename[MAX_VAR_DATA_LEN];
    CH Value[MAX_VAR_DATA_LEN];
    CH MsgBuf[MAX_VAR_DATA_LEN];

    //取filename屬性值
    AttrId = 1;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Value[0] == '/')
      strcpy(filename, Value);
    else
      sprintf(filename, "%s/%s", g_szDatPath, Value);
    
    //取failgoto屬性值
    //AttrId = 3;
    
     //取gotoaddr屬性值
    AttrId = 4;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    GotoAddr = atoi(Value);
    if (MyCreateLastDir(filename) != 0)
    {
      pCurrent->CurrCmdAddr = GotoAddr;
      return 0;
    }

//取returnstring屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    
    if (pCurrent->Ftext == NULL)
    {       
    	 pCurrent->Ftext = fopen(filename, "r");
    }
    if (pCurrent->Ftext != NULL)
    {        if (!feof(pCurrent->Ftext))
        {            if (fgets(MsgBuf, MAX_VAR_DATA_LEN, pCurrent->Ftext) == NULL)
            {                pCurrent->CurrCmdAddr = GotoAddr;
                return 0;
            }
            if (strlen(MsgBuf) > 0)
                MsgBuf[strlen(MsgBuf)-1] = '\0';
            Assign_pCurrent_Str_To_Var( Value, MsgBuf);      
            pCurrent->CurrCmdAddr = NextCmdAddr;
        }
        else
        {            pCurrent->CurrCmdAddr = GotoAddr;
        }
    }
    else
    {        pCurrent->CurrCmdAddr = GotoAddr;
    }
    
    return 0;
}
//執行寫文本文件指令
static int Proc_CMD_fwrite()
{    
    US AttrId;
    US GotoAddr;
    CH filename[MAX_VAR_DATA_LEN];
    CH Value[MAX_VAR_DATA_LEN];
   //取filename屬性值
    AttrId = 1;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    if (Value[0] == '/')
      strcpy(filename, Value);
    else
      sprintf(filename, "%s/%s", g_szDatPath, Value);

    //取failgoto屬性值
    //AttrId = 3;
    
    //取gotoaddr屬性值
    AttrId = 4;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
    GotoAddr = atoi(Value);
    if (MyCreateLastDir(filename) != 0)
    {
      pCurrent->CurrCmdAddr = GotoAddr;
      return 0;
    }

  //取txtline屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      
    if (pCurrent->Ftext == NULL)
    {   
    	  pCurrent->Ftext = fopen(filename, "a");
    }
    if (pCurrent->Ftext != NULL)
    {        
    	  fprintf(pCurrent->Ftext, "%s\n", Value);
        fclose(pCurrent->Ftext);
        pCurrent->Ftext= NULL;
        pCurrent->CurrCmdAddr = NextCmdAddr;
    }
    else
    {        pCurrent->CurrCmdAddr = GotoAddr;
    }

    return 0;
}
//執行關閉文本文件指令
static int Proc_CMD_fclose()
{    
	if (pCurrent->Ftext != NULL)
    fclose(pCurrent->Ftext);
  pCurrent->Ftext=0;

  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//-----------------------------------------------------------------------------
//start數據庫服務器新加指令   2006-07-03 Edit by zgj

//執行向外部數據網關查詢指令
static int Proc_CMD_gwquery()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_gwquery].MsgNameEng,pCurrent);
  
  //取msgtype屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  //取item屬性值
  for (AttrId = 3; AttrId < 7; AttrId ++)
  {
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  }
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_gwquery,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_gwquery, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//----------------數據庫指令---------------------------------------------------
//執行查詢數據操作指令
static int Proc_CMD_dbquery()
{   
    US gwid;  
    US AttrId;
    CH Value[MAX_SQLS_LEN];
    const char *AttrName;

    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);    
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbquery].MsgNameEng,pCurrent);
    
    //取sqls屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
        
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 		SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  	SndMsg.AddTailToBuf();
 
    US CallFlwNo = pCurrent->FlwNo;
    if (g_RunFlwMng[CallFlwNo].DispLogId == 0)
    {
      if (gwid == 0)
        DBSendMessageBypCurrent1(MSG_dbquery,  SndMsg.GetBuf());
      else
        GWSendMessageBypCurrent1(gwid, MSG_dbquery, SndMsg.GetBuf());
    }
    else
    {
      if (gwid == 0)
        DBSendMessageBypCurrent(MSG_dbquery,  SndMsg.GetBuf());
      else
        GWSendMessageBypCurrent(gwid, MSG_dbquery, SndMsg.GetBuf());
    }
    
    pCurrent->DBQueryResult = 1;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行根據字段序號取字段值操作指令
static int Proc_CMD_dbfieldno()
{   
    US gwid;  
    US AttrId;   
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;
   
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }

    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbfieldno].MsgNameEng,pCurrent);
      
    //取fieldno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 		SndMsg.AddItemToBuf(AttrName,Value); 	
    SndMsg.AddTailToBuf();
           
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbfieldno,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbfieldno, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行根據字段名稱取字段值操作指令
static int Proc_CMD_dbfieldname()
{   
    US gwid;  
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];   
    const char *AttrName;
    
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbfieldname].MsgNameEng,pCurrent);
    
    //取fieldname屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 	
    
    SndMsg.AddTailToBuf();
                          
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbfieldname,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbfieldname, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行首記錄指令
static int Proc_CMD_dbfirst()
{   
    US gwid;  
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
	  pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbfirst].MsgNameEng,pCurrent);
    SndMsg.AddTailToBuf();
                          
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbfirst,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbfirst, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行下一記錄指令
static int Proc_CMD_dbnext()
{      
    US gwid;  
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbnext].MsgNameEng,pCurrent);
    SndMsg.AddTailToBuf();
                                          
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbnext,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbnext, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行前一記錄指令
static int Proc_CMD_dbprior()
{        
    US gwid;  
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);    
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbprior].MsgNameEng,pCurrent);
    SndMsg.AddTailToBuf();
                       
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbprior,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbprior, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行末記錄指令
static int Proc_CMD_dblast()
{      
    US gwid;  
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dblast].MsgNameEng,pCurrent);
    SndMsg.AddTailToBuf();

    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dblast,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dblast, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行移動記錄指針指令
static int Proc_CMD_dbgoto()
{   
    US gwid;  
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];  
    const char *AttrName;
  
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
		SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbgoto].MsgNameEng,pCurrent);
     
    //取recordno屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	SndMsg.AddItemToBuf(AttrName,Value); 
    SndMsg.AddTailToBuf();
            
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbgoto,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbgoto, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行插入記錄操作指令
static int Proc_CMD_dbinsert()
{ 
    US gwid;  
    US AttrId;
    CH Value[MAX_SQLS_LEN];
    const char *AttrName;

    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
		SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbinsert].MsgNameEng,pCurrent);
  
    //取sqls屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
       
    Replace_pCurrentCmd_SqlExp_VarValues( AttrId, Value);
    SndMsg.AddItemToBufWithCheck(AttrName,Value); 
    SndMsg.AddTailToBuf();

    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbinsert,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbinsert, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行修改記錄操作指令
static int Proc_CMD_dbupdate()
{    
    US gwid;  
    US AttrId;    
    CH Value[MAX_SQLS_LEN];
    const char *AttrName;

    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
  	SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbupdate].MsgNameEng,pCurrent);
   
    //取sqls屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Replace_pCurrentCmd_SqlExp_VarValues( AttrId, Value);
    SndMsg.AddItemToBufWithCheck(AttrName,Value); 	

    SndMsg.AddTailToBuf();

    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbupdate,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbupdate, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行刪除記錄操作指令
static int Proc_CMD_dbdelete()
{   
    US gwid;  
    US AttrId;
    CH Value[MAX_SQLS_LEN];
    const char *AttrName;

    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);

		SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbdelete].MsgNameEng,pCurrent);
        
    //取sqls屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
        
    Replace_pCurrentCmd_SqlExp_VarValues( AttrId, Value);
    SndMsg.AddItemToBufWithCheck(AttrName,Value); 	
    
    SndMsg.AddTailToBuf();

    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbdelete,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbdelete, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行開始計費指令
static int Proc_CMD_startbill()
{    
    if (pCurrent->StartBillId == 0)
    {   
    	  pCurrent->StartBillId = 1;
        pCurrent->BillTime=time(0);
    }
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行終止計費并寫話單指令
static int Proc_CMD_writebill()
{    
    US gwid;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];

    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);

    //if (pCurrent->StartBillId == 1)
    //{ 
    	SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_writebill].MsgNameEng,pCurrent);
    	
      SndMsg.AddIntItemToBuf("deviceid", g_TcpServer.GetIVRServerId());
      
      //取funcgroup屬性值
      AttrId = 2;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      if (strlen(Value) == 0)
    	  SndMsg.AddIntItemToBuf("funcgroup", pCurrent->GetFuncGroup());
      else
        SndMsg.AddIntItemToBuf("funcgroup", atoi(Value));

      //取funcno屬性值
      AttrId = 3;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      if (strlen(Value) == 0)
    	  SndMsg.AddIntItemToBuf("funcno", pCurrent->GetFuncNo());
      else
        SndMsg.AddIntItemToBuf("funcno", atoi(Value));

      //取callerno屬性值
      AttrId = 4;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      if (strlen(Value) == 0)
    	  SndMsg.AddItemToBuf("callerno", pCurrent->CallerNo);
      else
        SndMsg.AddItemToBuf("callerno", Value);

      //取calledno屬性值
      AttrId = 5;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      if (strlen(Value) == 0)
    	  SndMsg.AddItemToBuf("calledno", pCurrent->CalledNo);
      else
        SndMsg.AddItemToBuf("calledno", Value);

      //取account屬性值
      AttrId = 6;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      SndMsg.AddItemToBuf("account", Value);

      //取starttime屬性值
      AttrId = 7;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      if (strlen(Value) == 0)
    	  SndMsg.AddItemToBuf("starttime", ConverTimeToStr(pCurrent->BillTime));
      else
        SndMsg.AddItemToBuf("starttime", Value);

      //取endtime屬性值
      AttrId = 8;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      if (strlen(Value) == 0)
    	  SndMsg.AddItemToBuf("endtime", MyGetNow());
      else
        SndMsg.AddItemToBuf("endtime", Value);

      SndMsg.AddIntItemToBuf("callinout", pCurrent->CallInOut);

      //取tollid屬性值
      AttrId = 1;
   	  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
      SndMsg.AddIntItemToBuf("tollid", atoi(Value));

      SndMsg.AddTailToBuf();
	
      if (gwid == 0)
        DBSendMessageBypCurrent(MSG_writebill,  SndMsg.GetBuf());
      else
        GWSendMessageBypCurrent(gwid, MSG_writebill, SndMsg.GetBuf());
    //}
    //else
    //{        pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    //}
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行存儲過程指令
static int Proc_CMD_dbsp()
{        
    US gwid;  
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;
   
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    if (!g_TcpServer.IsDBConnected())
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbsp].MsgNameEng,pCurrent);
		
    //取sp屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    
    //取params屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 

    SndMsg.AddTailToBuf();
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbsp,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbsp, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執關閉查詢的數據表指令
static int Proc_CMD_dbclose()
{    
    US gwid;  
    gwid = pCurrent->GetDBGwId();
    pCurrent->DBQueryResult = 0;
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
		SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbclose].MsgNameEng,pCurrent);
		SndMsg.AddTailToBuf();
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbclose,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbclose, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//執行響應事件指令
static int Proc_CMD_onevent()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    US EventId;
    US GotoAddr;  
       
    //取event屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    EventId = atoi(Value);
   

    //取goto屬性值
    AttrId = 2;
     //取gotoaddr屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    GotoAddr = atoi(Value);
    
    UC &EventVar = pCurrent->EventVars[GetEventVarIndexOfEvent(EventId)];
//     if (EventId == OnFlash)
//     {
//       MyTrace(5, "EventVars[%d] EventVar=%d", GetEventVarIndexOfEvent(EventId), EventVar);
//     }

    if( EventVar==EventId)
    {
      EventVar = WAIT_EVENT;
      pCurrent->CurrCmdAddr = GotoAddr;
    }
    else
    {   
      pCurrent->CurrCmdAddr = NextCmdAddr;
    }
//     if (EventId == OnFlash)
//     {
//       MyTrace(5, "EventVars[20] = %d", pCurrent->EventVars[20]);
//     }
    return 0;
}

//執行條件跳轉指令
static int Proc_CMD_case()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    US EventId;
    US GotoAddr;


    //取cond屬性值
    AttrId = 1;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   EventId = atoi(Value);

    //取gotoaddr屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    GotoAddr = atoi(Value);
    if (EventId == 1)
    { 
        pCurrent->CurrCmdAddr = GotoAddr;
    }
    else
    { 
    	  pCurrent->CurrCmdAddr = NextCmdAddr;
    }
    return 0;
}
//執行默認條件跳轉指令指令
static int Proc_CMD_default()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    US GotoAddr;

    //取gotoaddr屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    GotoAddr = atoi(Value);

    pCurrent->CurrCmdAddr = GotoAddr;
    return 0;
}
//執行查找目標會話通道標志指令
static int Proc_CMD_findsessionid()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char * cond;
    US maxnum;
    const char * NumVarName;
    const char * StartVarName;
    CH Repl[MAX_EXP_STRING_LEN];

    //取cond屬性值
    AttrId = 1;
   cond= pCurrentCmd->ParamValues[AttrId].C_Str();
    
    //取maxnum屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    maxnum = atoi(Value);
    //取returnnums屬性值
    AttrId = 3;
    NumVarName= pCurrentCmd->ParamValues[AttrId].C_Str();
    
    //取returnsessionid屬性值
    AttrId = 4;
    StartVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
    
    Replace_pCurrent_Exp_VarValues_For_FindId(   4, cond, Repl);
    g_SessionMng.pCurrentFindSession(  CurrCmdAddr,  Repl, maxnum, NumVarName, StartVarName);
    

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行跳轉指令 edwin 建議廢除該指令
static int Proc_CMD_goto()
{    
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];
    US SessionId1;

    //取sessionid屬性值
    AttrId = 1;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SessionId1 = atoi(Value);
    SessionId1 = SessionId1 & 0x0000FFFF;
    if (SessionId1 == 0)
      SessionId1 = pCurrent->GetSessionId();
    if ( !g_SessionMng.IsSessionNoAvail(SessionId1))
    {        //sprintf(CmdParas, "會話序列號 %s 超出范圍", sessionid);
        return 1;
    }
   
    g_SessionMng[SessionId1].CurrCmdAddr = NextCmdAddr;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

//執行子窗口調用結束返回指令
static int Proc_CMD_return()
{    
    return pCurrent->PopReturn();
    
/*    else if (ReturnLevel > 0)
    {        pCurrent->ReturnLevel --;
        pCurrent->ReturnFlwNo[pCurrent->ReturnLevel] = 0xFFFF;
        pCurrent->ReturnFlwId[pCurrent->ReturnLevel] = 0;
        pCurrent->ReturnCallId[pCurrent->ReturnLevel] = 0;
        GotoAddr = pCurrent->ReturnCmdAddr[pCurrent->ReturnLevel];
    }

    return 0;*/
}
//執行產生事件指令 //edwin 檢查RelSessionId
static int Proc_CMD_throw()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    US RelSessionId, EventId;

    pCurrent->InitIVREvent();

    //取sessionid屬性值
    AttrId = 1;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
    RelSessionId = atoi(Value);
    RelSessionId = RelSessionId & 0x0000FFFF;
    if (RelSessionId == 0)
      RelSessionId = pCurrent->GetSessionId();
    if ( !g_SessionMng.IsSessionNoAvail(RelSessionId))
    {
      pCurrent->CurrCmdAddr = NextCmdAddr; //如果對方不在線,不返回錯誤 2009-04-12
      return 0;
    }

    //取event屬性值
   // AttrId = 2;
   // Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(  AttrId, Value);
   // strcpy(event, Value);
    
    //取eventid屬性值
    AttrId = 3;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    EventId = atoi(Value);

    
    g_SessionMng[RelSessionId].EventVars[UC_SelfEvent] = (UC)EventId;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

//-----------------------------------------------------------------------------
//----------------函數指令-----------------------------------------------------
//執行字符串相加指令
static int Proc_CMD_stradd()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    
    //取varname屬性值
    AttrId = 1;
  
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        

    //取expr屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  	Assign_pCurrent_Str_To_Var( VarName, Value);
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取字符串長度指令
static int Proc_CMD_len()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char * VarName;
   
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_intValue_To_Var( VarName, strlen(Value)) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取子字符串指令
static int Proc_CMD_substring()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char * VarName;
    int StartPos, Length;
    //取varname屬性值
    AttrId = 1;
     
    VarName= pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取startpos屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    StartPos = atoi(Value);
    //取length屬性值
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Length = atoi(Value);
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( VarName, MySubString(Value, StartPos, Length));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行去掉字符串兩頭空格指令
static int Proc_CMD_trim()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char * VarName;
    
    //取varname屬性值
    AttrId = 1;
   
    VarName= pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( VarName, MyTrim(Value));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行去掉字符串左空格指令
static int Proc_CMD_lefttrim()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char * VarName;
     
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( VarName, MyLeftTrim(Value));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行去掉字符串右邊空格指令
static int Proc_CMD_righttrim()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char * VarName;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   Assign_pCurrent_Str_To_Var( VarName, MyRightTrim(Value));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取左字符串指令
static int Proc_CMD_left()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int Length;
    //取varname屬性值
    AttrId = 1;
     
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取length屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Length = atoi(Value);
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   	Assign_pCurrent_Str_To_Var( VarName, MyLeft(Value, Length));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取右字符串指令
static int Proc_CMD_right()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int Length;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取length屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Length = atoi(Value);
    //取string屬性值
    AttrId = 2;
   
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( VarName, MyRight(Value, Length));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行區分大小寫的字符串比較指令
static int Proc_CMD_strcmp()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH String1[MAX_VAR_DATA_LEN];
    CH String2[MAX_VAR_DATA_LEN];
     
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string1屬性值
    AttrId = 2;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String1, Value);
    
    //取string2屬性值
    AttrId = 3;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String2, Value);
    
    Assign_pCurrent_intValue_To_Var( VarName, strcmp(String1, String2)) ;    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行不區分大小寫的字符串比較指令
static int Proc_CMD_strcmpi()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH String1[MAX_VAR_DATA_LEN];
    CH String2[MAX_VAR_DATA_LEN];
     
    //取varname屬性值
    AttrId = 1;
   
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string1屬性值
    AttrId = 2;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String1, Value);
    
    //取string2屬性值
    AttrId = 3;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String2, Value);
    
    Assign_pCurrent_intValue_To_Var( VarName, strcmp(String1, String2));
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行字符串相似比較指令
static int Proc_CMD_like()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH String1[MAX_VAR_DATA_LEN];
    CH String2[MAX_VAR_DATA_LEN];
        //int id;

    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string1屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String1, Value);
    
    //取string2屬性值
    AttrId = 3;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String2, Value);
    
    if (MyIsLike(String1, String2))
      Assign_pCurrent_intValue_To_Var( VarName, 1) ;
    else
      Assign_pCurrent_intValue_To_Var( VarName, 0) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行將字符串小寫化指令
static int Proc_CMD_lower()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
     //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	  Assign_pCurrent_Str_To_Var( VarName, MyLower(Value));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行將字符串大寫化指令
static int Proc_CMD_upper()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
     
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( VarName, MyUpper(Value));
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行將字符填充空格指令
static int Proc_CMD_strfmt()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    CH string[MAX_VAR_DATA_LEN];
    CH formatstr[MAX_VAR_DATA_LEN];
    CH szTemp[MAX_VAR_DATA_LEN];
    const char *VarName;
    int formattype;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, string);

    //取formatstr屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, formatstr);

    //取formattype屬性值
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    formattype = atoi(Value);
    if (formattype == 1)
    {
      memset(szTemp, 0, MAX_VAR_DATA_LEN);
      strcpy(szTemp, string);
      if (strncmp(formatstr, "YYYY-MM-DD", 10) == 0)
      {
        if (strlen(string) == 8)
        {
          memset(szTemp, 0, MAX_VAR_DATA_LEN);
          strncpy(szTemp, &string[0], 4);
          strcat(szTemp, "-");
          strncat(szTemp, &string[4], 2);
          strcat(szTemp, "-");
          strncat(szTemp, &string[6], 2);
        }
      }
      else if (strncmp(formatstr, "YYYY/MM/DD", 10) == 0)
      {
        if (strlen(string) == 8)
        {
          memset(szTemp, 0, MAX_VAR_DATA_LEN);
          strncpy(szTemp, &string[0], 4);
          strcat(szTemp, "/");
          strncat(szTemp, &string[4], 2);
          strcat(szTemp, "/");
          strncat(szTemp, &string[6], 2);
        }
      }
      else if (strncmp(formatstr, "YYYY.MM.DD", 10) == 0)
      {
        if (strlen(string) == 8)
        {
          memset(szTemp, 0, MAX_VAR_DATA_LEN);
          strncpy(szTemp, &string[0], 4);
          strcat(szTemp, ".");
          strncat(szTemp, &string[4], 2);
          strcat(szTemp, ".");
          strncat(szTemp, &string[6], 2);
        }
      }
      else
      {
        if (strlen(string) == 8)
        {
          memset(szTemp, 0, MAX_VAR_DATA_LEN);
          strncpy(szTemp, &string[0], 4);
          strcat(szTemp, "-");
          strncat(szTemp, &string[4], 2);
          strcat(szTemp, "-");
          strncat(szTemp, &string[6], 2);
        }
      }
      Assign_pCurrent_Str_To_Var( VarName, szTemp);
    }
    else if (formattype == 2)
    {
      if (strncmp(formatstr, "HH:MI:SS", 8) == 0)
      {
        if (strlen(string) == 6)
        {
          memset(szTemp, 0, MAX_VAR_DATA_LEN);
          strncpy(szTemp, &string[0], 2);
          strcat(szTemp, ":");
          strncat(szTemp, &string[2], 2);
          strcat(szTemp, ":");
          strncat(szTemp, &string[4], 2);
        }
      }
      else if (strncmp(formatstr, "HH:MI", 5) == 0)
      {
        if (strlen(string) == 4)
        {
          memset(szTemp, 0, MAX_VAR_DATA_LEN);
          strncpy(szTemp, &string[0], 2);
          strcat(szTemp, ":");
          strncat(szTemp, &string[2], 2);
        }
      }
      Assign_pCurrent_Str_To_Var( VarName, szTemp);
    }
    else
    {
      Assign_pCurrent_Str_To_Var( VarName, string);
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行根據分隔符分隔子字符串指令
static int Proc_CMD_split()
{    
    US AttrId, ReturnSplitNum;
    CH Value[MAX_VAR_DATA_LEN];
    const char *StartVarName;
    const char * NumVarName;
    CH string[MAX_VAR_DATA_LEN];
    CH separator;
    int i, splitnum;
    VXML_DATA_STRUCT vArray[MAX_STRING_SPLIT_NUM];
    for (i = 0; i < MAX_STRING_SPLIT_NUM; i ++)
    {
      memset(vArray[i].Data, 0, MAX_VAR_DATA_LEN);
    }
    //取startvarname屬性值
    AttrId = 1;
    StartVarName= pCurrentCmd->ParamValues[AttrId].C_Str();
    
    
    //取numvarname屬性值
    AttrId = 2;
    NumVarName= pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 3;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(string, Value);
    
    //取separator屬性值
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    separator = Value[0];
    //取splitnum屬性值
    AttrId = 5;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    splitnum = atoi(Value);

    ReturnSplitNum = MySplit(string, separator, splitnum, vArray);

    for (i = 0; i < ReturnSplitNum; i ++)
    {            	
        Assign_pCurrent_Str_To_Var( i, StartVarName, vArray[i].Data); 
    }
    Assign_pCurrent_intValue_To_Var( NumVarName, ReturnSplitNum);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行將數組連成一字符串，并用分隔符分開指令
static int Proc_CMD_join()
{    
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];       
    CH separator;
    int joinnum;
     const char *VarName;
    const char *StartVarName;
    CStringX JS((char)0,MAX_VAR_DATA_LEN); //預先分配足夠內存
    
    //取varname屬性值
    AttrId = 1;
    VarName = pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取separator屬性值
    AttrId = 2;
     	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    	separator = Value[0];
    
    //取joinnum屬性值
    AttrId = 3;
    
    	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
      joinnum = atoi(Value);
  
    //取startvarnum屬性值
    AttrId = 4;
     	StartVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
    		
		JS=joinnum? *GetpCurrentVarPointer(StartVarName) : "";
		for (int i = 1; i < joinnum ;i++)
		{
			JS += separator;
			JS += *GetpCurrentVarPointer(StartVarName,i);
		}
    Assign_pCurrent_Str_To_Var( VarName, JS);

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行搜索子字符串位置指令
static int Proc_CMD_strpos()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH String1[MAX_VAR_DATA_LEN];
    CH String2[MAX_VAR_DATA_LEN];
    int position;
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
    
    
    //取string1屬性值
    AttrId = 2;
  
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String1, Value);
    
    //取string2屬性值
    AttrId = 3;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(String2, Value);
    
    position = MyStrPos(String1, String2);
    Assign_pCurrent_intValue_To_Var( VarName, position);    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取當前日期時間函數指令
static int Proc_CMD_getnow()
{    
    US AttrId;
    const char *VarName;
    CH datetime[MAX_VAR_DATA_LEN];
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    MyGetNow(datetime);
    Assign_pCurrent_Str_To_Var( VarName, datetime);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取日期函數指令
static int Proc_CMD_getdate()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;   
    CH dt[MAX_VAR_DATA_LEN];
   
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
   
    
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetDate(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_Str_To_Var( VarName, dt);

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取時間函數指令
static int Proc_CMD_gettime()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;    
    CH dt[MAX_VAR_DATA_LEN];
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
   
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetTime(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_Str_To_Var( VarName, dt);

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取年函數指令
static int Proc_CMD_getyear()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;    
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetYear(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取月函數指令
static int Proc_CMD_getmonth()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
       
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetMonth(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取日函數指令
static int Proc_CMD_getday()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetDay(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取時函數指令
static int Proc_CMD_gethour()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetHour(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取分函數指令
static int Proc_CMD_getminute()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;  
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
        
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetMinute(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取秒函數指令
static int Proc_CMD_getsecond()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
   
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
       
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetSecond(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取毫秒函數指令
static int Proc_CMD_getmsecond()
{    
    US AttrId, dt;
    const char *VarName;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    if (MyGetMsecond(dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取星期函數指令
static int Proc_CMD_getweekday()
{    
    US AttrId, dt;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
        
    if (strlen(Value) <= 0)
    {        MyGetNow(Value);
    }
    if (MyGetWeekDay(Value, dt) != 0)
    {        //日期時間格式錯誤
        return 1;
    }
    Assign_pCurrent_intValue_To_Var( VarName, dt) ;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行啟動定時器指令
static int Proc_CMD_starttimer()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];   
    int timerid;

    //取timerid屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    timerid = atoi(Value);
     
    //取timelen屬性值
    AttrId = 2;
    
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    pCurrent->TimerCount[timerid] = atoi(Value) ;//1s為單位
    pCurrent->EventVars[UC_Timer0Event+timerid] = WAIT_EVENT;

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行檢查日期時間的合法性指令
static int Proc_CMD_isdatetime()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN], pszDTStr[256];
    UC nResult;
    const char *VarName;    
    
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
       
    //2016-04-25 增加判斷日期時間合法的同時格式化字串
    nResult = MyIsDateTime(Value, pszDTStr);
    if (nResult == 0)
    {        
    	Assign_pCurrent_intValue_To_Var( VarName, 1) ;      
    }
    else if (nResult == 2)
    {
      Assign_pCurrent_Str_To_Var( VarName, pszDTStr) ;        
    }
    else
    {
      Assign_pCurrent_intValue_To_Var( VarName, 0) ;        
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取日期或時間間隔指令
static int Proc_CMD_getinterval()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int type, timelen;
    CH datetime1[MAX_VAR_DATA_LEN];
    CH datetime2[MAX_VAR_DATA_LEN];
    
    //取varname屬性值
    AttrId = 1;
   
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取type屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    type = atoi(Value);
    //取datetime1屬性值
    AttrId = 3;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(datetime1, Value);
    
    //取datetime2屬性值
    AttrId = 4;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(datetime2, Value);
    
    if (MyDTBetween(datetime1, datetime2, type, timelen) == 0)
    {      
    	Assign_pCurrent_intValue_To_Var( VarName, timelen) ;        
    }
    else
    {        //參數格式錯誤
        return 1;
    }
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行日期時間增量指令
static int Proc_CMD_incdatetime()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int type, timelen;
		CH datetime[MAX_VAR_DATA_LEN];
    CH datetime1[MAX_VAR_DATA_LEN];
     
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
     
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(datetime, Value);
    
    //取type屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    type = atoi(Value);
    //取num屬性值
    AttrId = 4;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    timelen = atoi(Value);
    if (MyIncDateTime(datetime, type, timelen, datetime1) == 0)
    {        
    	Assign_pCurrent_Str_To_Var( VarName, datetime1);       
    }
    else
    {        //參數格式錯誤
        //sprintf(ErrMsg, "時間參數格式錯誤：%s", datetime);
        return 1;
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行是否在時間段內指令
static int Proc_CMD_indatetime()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH datetime[MAX_VAR_DATA_LEN];
    CH datetime1[MAX_VAR_DATA_LEN];
    CH datetime2[MAX_VAR_DATA_LEN];
    UC Result;
    
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取datetime屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(datetime, Value);
    
    //取datetime1屬性值
    AttrId = 3;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(datetime1, Value);
    
    //取datetime2屬性值
    AttrId = 4;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(datetime2, Value);
    
    
    Result = MyInDateTime(datetime, datetime1, datetime2);
    if (Result == 0)
    {       
    	Assign_pCurrent_intValue_To_Var( VarName, 1) ;       
    }
    else
    {
      Assign_pCurrent_intValue_To_Var( VarName, 0);
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行指定年有多少天指令
static int Proc_CMD_daysinyear()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    US year;
    int Result;
    
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取year屬性值
    AttrId = 2;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    year = atoi(Value);

    Result = DaysInAYear(year);
    if (Result == 0)
    {       
    	Assign_pCurrent_intValue_To_Var( VarName, Result) ;       
    }
    else
    {        //參數格式錯誤
        //sprintf(ErrMsg, "時間參數格式錯誤：year=%d", year);
        return 1;
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行指定月有多少天指令
static int Proc_CMD_daysinmonth()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    US year, month;
    int Result;
    
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取year屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   year = atoi(Value);
    //取month屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    month = atoi(Value);

    Result = DaysInAMonth(year, month);
    if (Result == 0)
    {     
    	Assign_pCurrent_intValue_To_Var( VarName, Result);       
    }
    else
    {        //參數格式錯誤
        //strcpy(ErrMsg, "參數格式錯誤");
        return 1;
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行指定年是否為閏年指令
static int Proc_CMD_isleapyear()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    US year;
    
    //取varname屬性值
    AttrId = 1;
     
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取year屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    year = atoi(Value);

    if (IsLeapYear(year) == true)
    {        
    	Assign_pCurrent_intValue_To_Var( VarName, 1);        
    }
    else
    {      
    	Assign_pCurrent_intValue_To_Var( VarName, 0) ;      
    }

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行讀配置文件函數指令
static int Proc_CMD_readinifile()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH inifile[MAX_VAR_DATA_LEN];
    CH section[MAX_VAR_DATA_LEN];
    CH paramer[MAX_VAR_DATA_LEN];
    CH defaultValue[MAX_VAR_DATA_LEN];
    
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取inifile屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value) ;
    if (Value[0] == '/')
      strcpy(inifile, Value);
    else
      sprintf(inifile, "%s/%s", g_szIniPath, Value);
    
    //取section屬性值
    AttrId = 3;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(section, Value);
    
    //取paramer屬性值
    AttrId = 4;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(paramer, Value);
    
    //取default屬性值
    AttrId = 5;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(defaultValue, Value);
    
    GetPrivateProfileString(section, paramer, defaultValue, Value, MAX_VAR_DATA_LEN, inifile);
    Assign_pCurrent_Str_To_Var( VarName, Value);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//---------------------算術運算函數--------------------------------------------
//執行算術相加函數指令
static int Proc_CMD_add()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double numeric1, numeric2;
    
    //取varname屬性值
    AttrId = 1;
  
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atof(Value);
    numeric1 = numeric1 + numeric2;

    if (numeric1 == (long)numeric1)
    	Assign_pCurrent_intValue_To_Var( VarName, (long)numeric1);       
    else
      Assign_pCurrent_floatValue_To_Var( VarName, numeric1);
      
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術相減函數指令
static int Proc_CMD_sub()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double numeric1, numeric2;
    //取varname屬性值
    AttrId = 1;
   
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atof(Value);
    numeric1 = numeric1 - numeric2;

    if (numeric1 == (long)numeric1)
    	Assign_pCurrent_intValue_To_Var( VarName, (long)numeric1);   
    else
      Assign_pCurrent_floatValue_To_Var( VarName, numeric1);
      
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術相乘函數指令
static int Proc_CMD_mul()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double numeric1, numeric2;
    
    //取varname屬性值
    AttrId = 1;
    
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atof(Value);
    numeric1 = numeric1 * numeric2;

    if (numeric1 == (long)numeric1)
    	Assign_pCurrent_intValue_To_Var( VarName, (long)numeric1);        
    else
      Assign_pCurrent_floatValue_To_Var( VarName, numeric1);
      
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術相除函數指令
static int Proc_CMD_div()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double Result, numeric1, numeric2;
    
    //取varname屬性值
    AttrId = 1;

    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
  
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atof(Value);
    if (numeric2 == 0)
    {        //除數不能為0錯誤
        //strcpy(ErrMsg, "除數不能為0錯誤");
        return 1;
    }
    Result = numeric1 / numeric2;

    if (Result == (long)Result)
    	  Assign_pCurrent_intValue_To_Var( VarName, (long)Result);        
    else
        Assign_pCurrent_floatValue_To_Var( VarName, Result);

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術取模函數指令
static int Proc_CMD_mod()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int Result, numeric1, numeric2;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atoi(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atoi(Value);
    if (numeric2 == 0)
    {
      return 1;
    }
    Result = numeric1 % numeric2;

    Assign_pCurrent_intValue_To_Var( VarName, Result) ;    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術位與指令
static int Proc_CMD_bitand()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int Result, numeric1, numeric2;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atoi(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atoi(Value);
    Result = numeric1 & numeric2;

    Assign_pCurrent_intValue_To_Var( VarName, Result) ;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術位或指令
static int Proc_CMD_bitor()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int Result, numeric1, numeric2;
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atoi(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atoi(Value);
    Result = numeric1 | numeric2;

    Assign_pCurrent_intValue_To_Var( VarName, Result);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術位非指令
static int Proc_CMD_bitnot()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    UC Result;
    UC numeric;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric = atoi(Value);
    Result = ~numeric;

    Assign_pCurrent_intValue_To_Var( VarName, Result) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術位左移指令
static int Proc_CMD_bitlmove()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    UC Result, numeric, bits;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   numeric = atoi(Value);
    //取bits屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    bits = atoi(Value);
    Result = numeric << bits;

    Assign_pCurrent_intValue_To_Var( VarName, Result) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術位右移指令
static int Proc_CMD_bitrmove()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    UC Result, numeric, bits;
   
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric = atoi(Value);
    //取bits屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    bits = atoi(Value);
    Result = numeric >> bits;

    Assign_pCurrent_intValue_To_Var( VarName, Result) ;
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術取位值指令
static int Proc_CMD_getbit()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    UC Result, numeric, bits;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric = atoi(Value);
    //取bits屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    bits = atoi(Value);
    Result = (numeric >> bits) & 1;

    Assign_pCurrent_intValue_To_Var( VarName, Result) ;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術產生隨機數指令
static int Proc_CMD_random()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    int Result, lownum, upnum;
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取lownum屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    lownum = atoi(Value);
    //取upnum屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    upnum = atoi(Value);
    srand( (unsigned)time( NULL ) );
    if (lownum <= upnum)
    {        
      Result = MyRandom(lownum, upnum);
    }
    else
    {        
      Result = MyRandom(upnum, lownum);
    }

    Assign_pCurrent_intValue_To_Var( VarName, Result);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術取絕對值指令
static int Proc_CMD_abs()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double Result, numeric;
   
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
     numeric = atof(Value);
    Result = fabs(numeric);

    if (Result == (long)Result)
    	Assign_pCurrent_intValue_To_Var( VarName, (long)Result);       
    else
      Assign_pCurrent_floatValue_To_Var( VarName, Result);
      
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取最大數函數指令
static int Proc_CMD_max()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double Result, numeric1, numeric2;
  
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
     numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atof(Value);
    if (numeric1 >= numeric2)
    {        Result = numeric1;
    }
    else
    {        Result = numeric2;
    }

    if (Result == (long)Result)
    	Assign_pCurrent_intValue_To_Var( VarName, (long)Result) ;        
    else
    	Assign_pCurrent_floatValue_To_Var( VarName, Result) ;

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行取最小數函數指令
static int Proc_CMD_min()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double Result, numeric1, numeric2;
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric2 = atof(Value);
    if (numeric1 <= numeric2)
    {        Result = numeric1;
    }
    else
    {        Result = numeric2;
    }

    if (Result == (long)Result)
    	Assign_pCurrent_intValue_To_Var( VarName, (long)Result);        
    else
      Assign_pCurrent_floatValue_To_Var( VarName, Result);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術運算表達式指令
static int Proc_CMD_math()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        

    //取expr屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    Assign_pCurrent_Str_To_Var( VarName, Value);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行算術相整除函數指令
static int Proc_CMD_idiv()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    double numeric1, numeric2;
    long Result;
    
    //取varname屬性值
    AttrId = 1;
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取numeric1屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atof(Value);
    //取numeric2屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
    numeric2 = atof(Value);
    if (numeric2 == 0)
    {        //除數不能為0錯誤
        //strcpy(ErrMsg, "除數不能為0錯誤");
        return 1;
    }
    Result = (int)(numeric1 / numeric2);
    Assign_pCurrent_intValue_To_Var( VarName, Result);    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

//執行檢查日期時間的合法性指令
static int Proc_CMD_md5()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH string[MAX_VAR_DATA_LEN];
     
    //取varname屬性值
    AttrId = 1;
    VarName= pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取string屬性值
    AttrId = 2;
        
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(string, Value);
    
    Assign_pCurrent_Str_To_Var( VarName, MDString(string));
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//發送模擬外線拍叉簧指令
static int Proc_CMD_flash()
{   
    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_flash].MsgNameEng,pCurrent);
    SndMsg.AddTailToBuf();

    IVRSendMessageBypCurrent(MSG_flash, SndMsg.GetBuf());  
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

//執行創建子進程指令
static int Proc_CMD_fork()
{       
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH callerno[MAX_VAR_DATA_LEN], calledno[MAX_VAR_DATA_LEN];
    UC chntype, inout;
		US GotoAddr, chnno;
		US NewId,ParentId;

		ParentId=pCurrent->GetSessionId();
    
    //取chntype屬性值  //edit by zgj 2006-09-17
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    chntype = atoi(Value);

    //取chnno屬性值
    AttrId = 5;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    chnno = atoi(Value);

    //取callerno屬性值
    AttrId = 6;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, callerno);

    //取calledno屬性值
    AttrId = 7;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, calledno);

    //取inout屬性值 //edit by zgj 2006-09-17
    AttrId = 8;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    inout = atoi(Value);
				
    //取varname屬性值,//保存子進程的ID號
    AttrId = 1;
    VarName= pCurrentCmd->ParamValues[AttrId].C_Str();

     //取gotoaddr屬性值,//保存子進程的起始地址
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    GotoAddr = atoi(Value);

	  NewId=g_SessionMng.Alloc_Idel_SessionId(); //分配空閑session
	  if(NewId==0) //失敗
    {
   		Assign_pCurrent_intValue_To_Var( VarName, 0); //創建失敗
    	return 1;
		}
    SendTotalSessionsToAllLOG();
    SendOneFLWSessionsToAllLOG(pCurrent->FlwNo);

 		Assign_pCurrent_intValue_To_Var( VarName, ParentId); //父進程號準備傳給子進程
    pCurrent->CloneTo(g_SessionMng[NewId],GotoAddr,chntype,chnno,callerno,calledno,inout); //子進程創建成功后需要執行的第一條指令

    if (chntype != 0)
      Send_SetSessionNo(&g_SessionMng[NewId]);

    Assign_pCurrent_intValue_To_Var( VarName, NewId); //父程序保存子進程的ID號
    pCurrent->CurrCmdAddr = NextCmdAddr; //主進程繼續執行
    return 0;
}
//執行寫配置文件函數指令
static int Proc_CMD_writeinifile()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    CH inifile[MAX_VAR_DATA_LEN];
    CH section[MAX_VAR_DATA_LEN];
    CH paramer[MAX_VAR_DATA_LEN];
    CH wrvalue[MAX_VAR_DATA_LEN];
    
    //取inifile屬性值
    AttrId = 1;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value) ;
    if (isalpha(Value[0]) && Value[1] == ':' && (Value[2] == '\\' || Value[2] == '/'))
      strcpy(inifile, Value);
    else
      sprintf(inifile, "%s\\%s", g_szIniPath, Value);
    
    CString strDir = (CString)(inifile);
    int nFound1 = strDir.ReverseFind('\\');
    int nFound2 = strDir.ReverseFind('/');
    if (nFound1<nFound2)
      nFound1 = nFound2;
    CreateAllDirectories(strDir.Left(nFound1));
    if(GetFileAttributes(strDir.Left(nFound1))==-1)
    {
      pCurrent->EventVars[UC_OnEvent] = OnFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }

    //取section屬性值
    AttrId = 2;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(section, Value);
    
    //取paramer屬性值
    AttrId = 3;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(paramer, Value);
    
    //取value屬性值
    AttrId = 4;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(wrvalue, Value);
    
    WritePrivateProfileString(section, paramer, wrvalue, inifile);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

static int Proc_CMD_UNDEFINED()
{	
  //printf("Run Undefined CurrCmdAddr=%d\n",CurrCmdAddr);
	return 1;
}	  

//執行下一條指令
static int Proc_CMD_NOP()
{    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

static int Proc_CMD_waiting()
{		//pCurrent->SetWaitNum(10); //等待10個指令周期后再執行下一條指令
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
  

//執行變量加1指令
static int Proc_CMD_inc()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    long numeric1;
    
    //取varname屬性值
    AttrId = 1;
  
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();;
        
    //取值
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atol(Value);
    numeric1 = numeric1 + 1;

  	Assign_pCurrent_intValue_To_Var( VarName, numeric1);       
      
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}

//執行變量減1指令
static int Proc_CMD_dec()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    long numeric1;
    
    //取varname屬性值
    AttrId = 1;
  
    VarName=pCurrentCmd->ParamValues[AttrId].C_Str();;
        
    //取值
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    numeric1 = atol(Value);
    numeric1 = numeric1 - 1;

  	Assign_pCurrent_intValue_To_Var( VarName, numeric1);       
      
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行IF條件判斷指令
static int Proc_CMD_if()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    US EventId;
    US ThenAddr, ElseAddr;


    //取cond屬性值
    AttrId = 1;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
   EventId = atoi(Value);

    //取thenaddr屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    ThenAddr = atoi(Value);
    //取elseaddr屬性值
    AttrId = 5;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    ElseAddr = atoi(Value);
    if (EventId == 1)
    { 
        pCurrent->CurrCmdAddr = ThenAddr;
    }
    else
    { 
    	  pCurrent->CurrCmdAddr = ElseAddr;
    }
    return 0;
}
//執行建立互斥指令
static int Proc_CMD_createmutex()
{    
    pCurrent->CreateMutex();  
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行釋放互斥指令
static int Proc_CMD_releasemutex()
{    
    pCurrent->ReleaseMutex();  
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行密碼加密指令
static int Proc_CMD_crypt()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *VarName;
    CH crypttype[MAX_VAR_DATA_LEN];
    CH key[MAX_VAR_DATA_LEN];
    CH input[MAX_VAR_DATA_LEN];
    CH output[MAX_VAR_DATA_LEN];
     
    //取varname屬性值
    AttrId = 1;
    VarName= pCurrentCmd->ParamValues[AttrId].C_Str();
        
    //取crypttype屬性值
    AttrId = 2;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(crypttype, Value);
    
    //取key屬性值
    AttrId = 3;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(key, Value);
    
    //取input屬性值
    AttrId = 4;
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    strcpy(input, Value);
    
    memset(output, 0, MAX_VAR_DATA_LEN);
    strcpy(output, EncodePass(input));
    Assign_pCurrent_Str_To_Var(VarName, output);
    
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//設置數據網關
static int Proc_CMD_setdbgateway()
{    
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    UC gwid;


    //取gwid屬性值
    AttrId = 1;
  	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    gwid = atoi(Value);
    if (gwid > MAX_GW_NUM)
    { 
        pCurrent->SetDBGwId(0);
    }
    else
    { 
    	  pCurrent->SetDBGwId(gwid);
    }
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//設置變聲參數指令
static int Proc_CMD_setvcparam()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_setvcparam].MsgNameEng, pCurrent);

    //取vcmode屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取vcparam屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取vcmix屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    
    //取noisemode屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    
    //取noiseparam屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    
    SndMsg.AddTailToBuf();  
  
    IVRSendMessageBypCurrent(MSG_setvcparam,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//根據余額取最大通話時長指令
static int Proc_CMD_gettolltime()
{    
    US gwid;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];

    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);

    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_gettolltime].MsgNameEng,pCurrent);
    
    //取tollid屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    SndMsg.AddIntItemToBuf("tollid", atoi(Value));

    //取funcgroup屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    if (strlen(Value) == 0)
    	SndMsg.AddIntItemToBuf("funcgroup", pCurrent->GetFuncGroup());
    else
      SndMsg.AddIntItemToBuf("funcgroup", atoi(Value));

    //取funcno屬性值
    AttrId = 3;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    if (strlen(Value) == 0)
    	SndMsg.AddIntItemToBuf("funcno", pCurrent->GetFuncNo());
    else
      SndMsg.AddIntItemToBuf("funcno", atoi(Value));

    //取callerno屬性值
    AttrId = 4;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    SndMsg.AddItemToBuf("callerno", Value);

    //取calledno屬性值
    AttrId = 5;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    SndMsg.AddItemToBuf("calledno", Value);

    //取account屬性值
    AttrId = 6;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    SndMsg.AddItemToBuf("account", Value);

    //取starttime屬性值
    AttrId = 7;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    SndMsg.AddItemToBuf("starttime", Value);

    //取balance屬性值
    AttrId = 8;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, Value);
    SndMsg.AddItemToBuf("balance", Value);

    SndMsg.AddTailToBuf();

    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_gettolltime,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_gettolltime, SndMsg.GetBuf());

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//暫停放音指令
static int Proc_CMD_pause()
{   
    pCurrent->InitIVREvent();   
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_pause].MsgNameEng, pCurrent);
    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_pause, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//重新放音指令
static int Proc_CMD_replay()
{   
    pCurrent->InitIVREvent();   
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_replay].MsgNameEng, pCurrent);
    SndMsg.AddTailToBuf();
    

    IVRSendMessageBypCurrent(MSG_replay, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//快速放音指令
static int Proc_CMD_fastplay()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_fastplay].MsgNameEng, pCurrent);

    //取direction屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取length屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    SndMsg.AddTailToBuf();    

    IVRSendMessageBypCurrent(MSG_fastplay, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行檢測信號音
static int Proc_CMD_checktone()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();
    if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
      return 0;

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_checktone].MsgNameEng, pCurrent);

    //取tonetype屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    
    //取toneparam屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 

    SndMsg.AddTailToBuf();  
 
    IVRSendMessageBypCurrent(MSG_checktone, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//設置會話通道號指令
static int Proc_CMD_setchannel()
{
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    int chntype, chnno;
        
    //取chntype屬性值
    AttrId = 1;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    chntype = atoi(Value);

    //取chnno屬性值
    AttrId = 2;
   	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    chnno = atoi(Value);

    pCurrent->ChnType = chntype;
    pCurrent->ChnNo = chnno;

    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行讀數據表當前記錄指令
static int Proc_CMD_dbrecord()
{   
    US gwid;  
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;
    const char *AttrValue;
    
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbrecord].MsgNameEng,pCurrent);
    
    //取recordno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 	
    
    //取autogotonext屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 	
    
    memset(Value, 0, MAX_VAR_DATA_LEN);
    for (int i=4; i<=35; i++)
    {
      AttrValue=pCurrentCmd->ParamValues[i].C_Str();
      if (strcmp(AttrValue, "$S_050") != 0)
      {
        Value[i-4] = '1';
      }
      else
      {
        Value[i-4] = '0';
      }
    }
    SndMsg.AddItemToBuf("getidlist", Value);

    SndMsg.AddTailToBuf();
                          
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbrecord,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbrecord, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行話務員登錄指令
static int Proc_CMD_workerlogin()
{    US ChnType, ChnNo;
    US AttrId;
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;

    pCurrent->InitIVREvent();

    ChnType = pCurrent->ChnType;
    ChnNo = pCurrent->ChnNo;

    SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_workerlogin].MsgNameEng, pCurrent);

    //取seatno屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);
    
    //取workerno屬性值
    AttrId = 2;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取workername屬性值
    AttrId = 3;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取password屬性值
    AttrId = 4;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取groupno屬性值
    AttrId = 5;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取class屬性值
    AttrId = 6;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value);

    //取level屬性值
    AttrId = 7;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 
    
    SndMsg.AddTailToBuf();  
  
    IVRSendMessageBypCurrent(MSG_workerlogin,SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行話務員退出指令
static int Proc_CMD_workerlogout()
{   
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();
    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_workerlogout].MsgNameEng,pCurrent);
  
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
    
  IVRSendMessageBypCurrent(MSG_workerlogout, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取存儲過程輸出參數值指令
static int Proc_CMD_dbspoutparam()
{   
    US gwid;  
    US AttrId;    
    CH Value[MAX_VAR_DATA_LEN];
    const char *AttrName;
    
    gwid = pCurrent->GetDBGwId();
    if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
    {
      pCurrent->EventVars[UC_DBEvent] = OnDBFail;
      pCurrent->CurrCmdAddr = NextCmdAddr;
      return 0;
    }
    pCurrent->InitDBEvent(g_nDBTimeOut);
    SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbspoutparam].MsgNameEng,pCurrent);
    
    //取params屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
    SndMsg.AddItemToBuf(AttrName,Value); 	
    
    SndMsg.AddTailToBuf();
                          
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbspoutparam,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbspoutparam, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//向外部數據網關發送器發起呼叫的結果指令
static int Proc_CMD_gwmakecallresult()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_gwmakecallresult].MsgNameEng,pCurrent);
  
  //取gwserialno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取callerno屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取calledno屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取account屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取result屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取param屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_gwmakecallresult,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_gwmakecallresult, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//向外部數據網關發送消息事件處理結果指令
static int Proc_CMD_gwmsgeventresult()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_gwmsgeventresult].MsgNameEng,pCurrent);
  
  //取gwserialno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取result屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取param屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_gwmsgeventresult,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_gwmsgeventresult, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//向坐席發送轉接呼叫結果指令
static int Proc_CMD_agtrancallresult()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_agtrancallresult].MsgNameEng, pCurrent);

  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取trantype屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取sessionno屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取chntype屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取chnno屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取cfcno屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取callerno屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取calledno屬性值
  AttrId = 9;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取param屬性值
  AttrId = 10;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取result屬性值
  AttrId = 11;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取errorbuf屬性值
  AttrId = 12;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_agtrancallresult,SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//向坐席發送坐席將來話轉接到ivr流程結果
static int Proc_CMD_agtranivrresult()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_agtranivrresult].MsgNameEng, pCurrent);

  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取returnflag屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取param屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取result屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取errorbuf屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_agtranivrresult,SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//向坐席發送指定通道參加會議結果
static int Proc_CMD_agjoinconfresult()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_agjoinconfresult].MsgNameEng, pCurrent);
  
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取destchntype屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取destchnno屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取confno屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取jointype屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取result屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取errorbuf屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();  
  
  IVRSendMessageBypCurrent(MSG_agjoinconfresult,SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//發送消息到電腦坐席指令
static int Proc_CMD_sendagentmsg()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendagentmsg].MsgNameEng, pCurrent);

  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取msgtype屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取item0屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取item1屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取item2屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取item3屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_sendagentmsg,SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行代接電話指令
static int Proc_CMD_pickupcall()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_pickupcall].MsgNameEng, pCurrent);
  
  //取groupno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取seatno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取selectmode屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  
  IVRSendMessageBypCurrent(MSG_pickupcall, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取ACD隊列信息指令
static int Proc_CMD_getacdqueue()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_getacdqueue].MsgNameEng, pCurrent);
  
  //取groupno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  
  IVRSendMessageBypCurrent(MSG_getacdqueue, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行設置通道流程業務狀態信息指令
static int Proc_CMD_setflwstate()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_setflwstate].MsgNameEng, pCurrent);
  
  //取flwstate屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  
  IVRSendMessageBypCurrent(MSG_setflwstate, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行將呼出或呼叫坐席結果改發送到指定的會話指令
static int Proc_CMD_swapcallresult()
{    
  US AttrId;
  
  CH Value[MAX_VAR_DATA_LEN];
  int SessionId1;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_swapcallresult].MsgNameEng, pCurrent);

  //取sessionid1屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);   
  if (Check_Int_Str(Value, SessionId1) != 0)
  {    
    return 1;
  }
  
  if(!g_SessionMng.IsSessionNoAvail(SessionId1))
  {
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  CSession *pSave=&g_SessionMng[SessionId1];
  
  SndMsg.AddIntItemToBuf("sessionno",pCurrent->DialSerialNo);
  SndMsg.AddLongItemToBuf("sessionid1",pSave->SerialNo);
  SndMsg.AddIntItemToBuf("chantype1",pSave->ChnType);
  SndMsg.AddIntItemToBuf("channo1",pSave->ChnNo);

  SndMsg.AddTailToBuf();

  pSave->DialSerialNo = pCurrent->DialSerialNo;

  IVRSendMessageBypCurrent(MSG_swapcallresult, SndMsg.GetBuf());

  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行接收FSK指令
static int Proc_CMD_recvfsk()
{
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();
  if (CheckCanSendMsg2IVR(pCurrent, UC_OnEvent, NextCmdAddr) == false)
  return 0;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_recvfsk].MsgNameEng, pCurrent);

  //取fskheader屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取crctype屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  SndMsg.AddTailToBuf();


  IVRSendMessageBypCurrent(MSG_recvfsk, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行分配IVR資源指令
static int Proc_CMD_transferivr()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_transferivr].MsgNameEng, pCurrent);
  
  //取vopno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取ivrno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取param屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_transferivr, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行釋放IVR資源指令
static int Proc_CMD_releaseivr()
{    
  US ChnType, ChnNo;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_releaseivr].MsgNameEng, pCurrent);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_releaseivr, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行播放交換機內置語音指令
static int Proc_CMD_playswitchvoice()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playswitchvoice].MsgNameEng, pCurrent);
  
  //取voiceid屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_playswitchvoice, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行發送交換機操作命令指令
static int Proc_CMD_sendswitchcmd()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendswitchcmd].MsgNameEng, pCurrent);
  
  //取cmdid屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取cmdparam屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_sendswitchcmd, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行撥打電話指令
static int Proc_CMD_dial()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->EventVars[UC_CalloutEvent] = WAIT_EVENT;
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_dial].MsgNameEng, pCurrent);
  
  //取callerno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取calledno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取routeno屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_dial, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取查詢的字段值并存放到指定的文件指令
static int Proc_CMD_dbfieldtofile()
{   
  US gwid;  
  US AttrId;    
  CH Value[MAX_VAR_DATA_LEN];   
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbfieldtofile].MsgNameEng,pCurrent);
  
  //取fieldname屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  //取path屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  //取filename屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_dbfieldtofile,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_dbfieldtofile, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行將文件內容存入數據表指令
static int Proc_CMD_dbfiletofield()
{   
  US gwid;  
  US AttrId;    
  CH Value[MAX_SQLS_LEN];   
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbfiletofield].MsgNameEng,pCurrent);
  
  //取pathname屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  //取filename屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  //取tablename屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  //取fieldname屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 	
  
  //取wheresql屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Replace_pCurrentCmd_SqlExp_VarValues( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 

  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_dbfiletofield,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_dbfiletofield, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行追加發送的傳真文件指令
static int Proc_CMD_appendfaxfile()
{
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_appendfaxfile].MsgNameEng, pCurrent);

  //取faxcsid屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取resolution屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取totalpages屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取pagelist屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取faxheader屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取faxfooter屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取barcode屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取filename屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  SndMsg.AddTailToBuf();

  pCurrent->FaxSndRcv = 2; //發送傳真
  IVRSendMessageBypCurrent(MSG_appendfaxfile, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行產生GUID碼指令
static int Proc_CMD_getguid()
{    
  US AttrId;
  const char *VarName;
  
  //取varname屬性值
  AttrId = 1;
  
  VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //產生GUID碼
  Assign_pCurrent_Str_To_Var(VarName, MyGetGUID());
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行插入記錄操作指令（針對長度>500的sql語句）
static int Proc_CMD_dbinsertex()
{ 
  US gwid;  
  US AttrId;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);
		SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbinsertex].MsgNameEng,pCurrent);
    
    //取sqls屬性值
    AttrId = 1;
    AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
    
    Replace_pCurrentCmd_SqlExp_VarValues( AttrId, Value);
    SndMsg.AddItemToBufWithCheck(AttrName,Value); 
    SndMsg.AddTailToBuf();
    
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbinsertex,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbinsertex, SndMsg.GetBuf());
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
}
//執行修改記錄操作指令（針對長度>500的sql語句）
static int Proc_CMD_dbupdateex()
{    
  US gwid;  
  US AttrId;    
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbupdateex].MsgNameEng,pCurrent);
  
  //取sqls屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Replace_pCurrentCmd_SqlExp_VarValues( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 	
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_dbupdateex,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_dbupdateex, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行查詢數據操作指令（針對長度>500的sql語句）
static int Proc_CMD_dbqueryex()
{   
  US gwid;  
  US AttrId;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbqueryex].MsgNameEng,pCurrent);
  
  //取sqls屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_dbqueryex,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_dbqueryex, SndMsg.GetBuf());
  pCurrent->DBQueryResult = 1;
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行綁定坐席通道指令
static int Proc_CMD_bandagentchn()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();
  //if (CheckCanSendMsg2IVR(pCurrent, 1, NextCmdAddr) == false)
  //  return 0;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_bandagentchn].MsgNameEng, pCurrent);

  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取phoneno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取chantype屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取channo屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取state屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();

  IVRSendMessageBypCurrent(MSG_bandagentchn, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行綁定坐席通道指令
static int Proc_CMD_sendcalltoagent()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  //if (CheckCanSendMsg2IVR(pCurrent, 1, NextCmdAddr) == false)
  //  return 0;
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_sendcalltoagent].MsgNameEng, pCurrent);
  
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取callinout屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取calltype屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取callerno屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取calledno屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取param屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_sendcalltoagent, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行向外部WEBSERVICE網關查詢指令
static int Proc_CMD_querywebservice()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_querywebservice].MsgNameEng,pCurrent);
  
  //取wsdladdress屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取soapaction屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取startenvelope屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取startbody屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取interfacestring屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取paramcount屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取paramstring屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_querywebservice,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_querywebservice, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行從外部webservice接口查詢的XML串中取數據指令
static int Proc_CMD_getwebservicexml()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_getwebservicexml].MsgNameEng,pCurrent);
  
  //取noderoot屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取nodeindex屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取attrname屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_getwebservicexml,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_getwebservicexml, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取xml串的節點數據指令
static int Proc_CMD_getxmlstringdata()
{   
  US AttrId;
  CH value[MAX_VAR_DATA_LEN];
  CH noderoot[MAX_VAR_DATA_LEN];
  int nodeindex;
  CH attrname[MAX_VAR_DATA_LEN];
  CH xmlstring[MAX_VAR_DATA_LEN];
  const char *NodesVarName;
  const char *ValueVarName;
  const char *ExistVarName;
  char szAttrValue[MAX_VAR_DATA_LEN];
  char szError[MAX_VAR_DATA_LEN];
  int nNodeCount=0;
  
  //取nodesvarname屬性值
  AttrId = 5;
  NodesVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  //取valuevarname屬性值
  AttrId = 6;
  ValueVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  //取existvarname屬性值
  AttrId = 7;
  ExistVarName=pCurrentCmd->ParamValues[AttrId].C_Str();

  //取noderoot屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, noderoot);
  
  //取nodeindex屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, value);
  nodeindex = atoi(value);
  
  //取attrname屬性值
  AttrId = 3;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, attrname);
  
  //取xmlstring屬性值
  AttrId = 4;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, xmlstring);
  
  memset(szAttrValue, 0, MAX_VAR_DATA_LEN);
  memset(szError, 0, MAX_VAR_DATA_LEN);

  if (GetXmlAttrValueFromXMLString(xmlstring, noderoot, nodeindex,
    attrname, nNodeCount, szAttrValue, MAX_VAR_DATA_LEN-1, szError) == 0)
  {
    Assign_pCurrent_intValue_To_Var(ExistVarName, 1);
    Assign_pCurrent_Str_To_Var(ValueVarName, szAttrValue);
  }
  else
  {
    Assign_pCurrent_intValue_To_Var(ExistVarName, 0);
  }
  
  Assign_pCurrent_intValue_To_Var(NodesVarName, nNodeCount);
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取xml串的節點數據指令
static int Proc_CMD_getxmlfiledata()
{   
  US AttrId;
  CH value[MAX_VAR_DATA_LEN];
  CH noderoot[MAX_VAR_DATA_LEN];
  int nodeindex;
  CH attrname[MAX_VAR_DATA_LEN];
  CH xmlfile[MAX_VAR_DATA_LEN];
  const char *NodesVarName;
  const char *ValueVarName;
  const char *ExistVarName;
  char szAttrValue[MAX_VAR_DATA_LEN];
  char szError[MAX_VAR_DATA_LEN];
  int nNodeCount=0;
  
  //取nodesvarname屬性值
  AttrId = 5;
  NodesVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  //取valuevarname屬性值
  AttrId = 6;
  ValueVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  //取existvarname屬性值
  AttrId = 7;
  ExistVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取noderoot屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, noderoot);
  
  //取nodeindex屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, value);
  nodeindex = atoi(value);
  
  //取attrname屬性值
  AttrId = 3;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, attrname);
  
  //取xmlfile屬性值
  AttrId = 4;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(AttrId, xmlfile);
  
  memset(szAttrValue, 0, MAX_VAR_DATA_LEN);
  memset(szError, 0, MAX_VAR_DATA_LEN);
  
  if (GetXmlAttrValueFromXMLFile(xmlfile, noderoot, nodeindex,
    attrname, nNodeCount, szAttrValue, MAX_VAR_DATA_LEN-1, szError) == 0)
  {
    Assign_pCurrent_intValue_To_Var(ExistVarName, 1);
    Assign_pCurrent_Str_To_Var(ValueVarName, szAttrValue);
  }
  else
  {
    Assign_pCurrent_intValue_To_Var(ExistVarName, 0);
  }
  
  Assign_pCurrent_intValue_To_Var(NodesVarName, nNodeCount);
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行關閉外部webservice接口指令
static int Proc_CMD_closewebservice()
{
  US gwid;  
  gwid = pCurrent->GetDBGwId();

  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);
	
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_closewebservice].MsgNameEng,pCurrent);
  SndMsg.AddTailToBuf();
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_closewebservice,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_closewebservice, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行字符串替換指令
static int Proc_CMD_strreplace()
{
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  CH oldstr[MAX_VAR_DATA_LEN];
  CH newstr[MAX_VAR_DATA_LEN];
  CH startindex[MAX_VAR_DATA_LEN];
  CH replacenums[MAX_VAR_DATA_LEN];
  const char *VarName;
  int nStartIndex, nReplaceNums;
  
  //取strvarname屬性值
  AttrId = 1;
  VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取string屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  
  //取oldstr屬性值
  AttrId = 3;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, oldstr);
  
  //取newstr屬性值
  AttrId = 4;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, newstr);
  
  //取startindex屬性值
  AttrId = 5;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, startindex);
  nStartIndex = atoi(startindex);
  if (nStartIndex < 0)
  {
    nStartIndex = 0;
  }
  
  //取replacenums屬性值
  AttrId = 6;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, replacenums);
  nReplaceNums = atoi(replacenums);
  if (nReplaceNums < 0)
  {
    nReplaceNums = 1;
  }

  char ReturnStr[MAX_VAR_DATA_LEN];

  MyStringReplace(Value, oldstr, newstr, nStartIndex, nReplaceNums, MAX_VAR_DATA_LEN, ReturnStr);
  Assign_pCurrent_Str_To_Var(VarName, ReturnStr);
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取特殊標簽位置的字符串指令
static int Proc_CMD_getmarkerstring()
{
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  CH startmarker[MAX_VAR_DATA_LEN];
  CH endmarker[MAX_VAR_DATA_LEN];
  CH getindex[MAX_VAR_DATA_LEN];
  const char *StrVarName;
  const char *ExistVarName;
  int nGetIndex;
  
  //取strvarname屬性值
  AttrId = 1;
  StrVarName=pCurrentCmd->ParamValues[AttrId].C_Str();

  //取existvarname屬性值
  AttrId = 2;
  ExistVarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取string屬性值
  AttrId = 3;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);

  //取startmarker屬性值
  AttrId = 4;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, startmarker);
  
  //取endmarker屬性值
  AttrId = 5;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, endmarker);
  
  //取getindex屬性值
  AttrId = 6;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, getindex);
  nGetIndex = atoi(getindex);
  if (nGetIndex < 0)
  {
    nGetIndex = 0;
  }

  CH ReturnValue[MAX_VAR_DATA_LEN];
  bool bFinded;

  memset(ReturnValue, 0, MAX_VAR_DATA_LEN);

  MyGetMarkerString(Value, startmarker, endmarker, nGetIndex, MAX_VAR_DATA_LEN, ReturnValue, bFinded);
  if (bFinded == true)
  {
    Assign_pCurrent_intValue_To_Var(ExistVarName, 1);
    Assign_pCurrent_Str_To_Var(StrVarName, ReturnValue);
  }
  else
  {
    Assign_pCurrent_intValue_To_Var(ExistVarName, 0);
    Assign_pCurrent_Str_To_Var(StrVarName, ReturnValue);
  }
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行字符串base64編碼指令
static int Proc_CMD_base64encode()
{
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  CH keystr[MAX_EXP_STRING_LEN];
  const char *VarName;
  
  //取varname屬性值
  AttrId = 1;
  
  VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取string屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  memset(keystr, 0, MAX_EXP_STRING_LEN);
  Base64Encode(keystr, (UC *)Value);
  Assign_pCurrent_Str_To_Var(VarName, keystr);
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行字符串base64解碼指令
static int Proc_CMD_base64decode()
{
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  CH keystr[MAX_VAR_DATA_LEN];
  const char *VarName;
  
  //取varname屬性值
  AttrId = 1;
  
  VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取string屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  Base64Decode((UC *)keystr, Value);
  Assign_pCurrent_Str_To_Var( VarName, keystr);
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行從外部webservice接口查詢的字符串取數據指令
static int Proc_CMD_getwebservicestr()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_getwebservicestr].MsgNameEng,pCurrent);
  
  //取startmarker屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取endmarker屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取getindex屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_getwebservicestr,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_getwebservicestr, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行替換字符串左邊指定的子字符串指令
static int Proc_CMD_leftstrreplace()
{
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  CH leftstr[MAX_VAR_DATA_LEN];
  CH newstr[MAX_VAR_DATA_LEN];
  const char *VarName;
  
  //取strvarname屬性值
  AttrId = 1;
  VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取string屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  
  //取leftstr屬性值
  AttrId = 3;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, leftstr);
  
  //取newstr屬性值
  AttrId = 4;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, newstr);
  
  char ReturnStr[MAX_VAR_DATA_LEN];
  int nOldLen, nLeftLen, nNewLen;

  memset(ReturnStr, 0, MAX_VAR_DATA_LEN);

  nOldLen = strlen(Value);
  nLeftLen = strlen(leftstr);
  nNewLen = strlen(newstr);
  if (nLeftLen == 0)
  {
    if (nNewLen > 0)
    {
      strcpy(ReturnStr, newstr);
      strncat(ReturnStr, Value, MAX_VAR_DATA_LEN-nNewLen-1);
      Assign_pCurrent_Str_To_Var(VarName, ReturnStr);
    }
    else
    {
      Assign_pCurrent_Str_To_Var(VarName, Value);
    }
  } 
  else
  {
    if (nLeftLen > nOldLen)
    {
      Assign_pCurrent_Str_To_Var(VarName, Value);
    }
    else
    {
      if (strncmp(Value, leftstr, nLeftLen) == 0)
      {
        if (nNewLen > 0)
        {
          strcpy(ReturnStr, newstr);
          strncat(ReturnStr, &Value[nLeftLen], MAX_VAR_DATA_LEN-nNewLen-1);
        }
        else
        {
          strcpy(ReturnStr, &Value[nLeftLen]);
        }
        Assign_pCurrent_Str_To_Var(VarName, ReturnStr);
      }
      else
      {
        Assign_pCurrent_Str_To_Var(VarName, Value);
      }
    }
  }
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行替換字符串右邊指定的子字符串指令
static int Proc_CMD_rightstrreplace()
{
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  CH rightstr[MAX_VAR_DATA_LEN];
  CH newstr[MAX_VAR_DATA_LEN];
  const char *VarName;
  
  //取strvarname屬性值
  AttrId = 1;
  VarName=pCurrentCmd->ParamValues[AttrId].C_Str();
  
  //取string屬性值
  AttrId = 2;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  
  //取rightstr屬性值
  AttrId = 3;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, rightstr);
  
  //取newstr屬性值
  AttrId = 4;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, newstr);
  
  //char ReturnStr[MAX_VAR_DATA_LEN];
  int nOldLen, nRightLen, nNewLen;
  
  nOldLen = strlen(Value);
  nRightLen = strlen(rightstr);
  nNewLen = strlen(newstr);
  if (nRightLen == 0)
  {
    if (nNewLen > 0)
    {
      strncat(Value, rightstr, MAX_VAR_DATA_LEN-nOldLen-1);
    }
  } 
  else
  {
    if (nOldLen >= nRightLen)
    {
      if (strcmp(&Value[nOldLen-nRightLen], rightstr) == 0)
      {
        Value[nOldLen-nRightLen] = '\0';
        if (nNewLen > 0)
        {
          strncat(Value, newstr, MAX_VAR_DATA_LEN-(nOldLen-nRightLen)-1);
        }
      }
    }
  }
  Assign_pCurrent_Str_To_Var(VarName, Value);
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行通用放音指令
static int Proc_CMD_play()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
  if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
  return 0;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_play].MsgNameEng, pCurrent);

  //取dtmfrule屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取playrule屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取asrrule屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取chntype屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取chnno屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取content1屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取content2屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取content3屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取content4屬性值
  AttrId = 9;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取content5屬性值
  AttrId = 10;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取content6屬性值
  AttrId = 11;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_play, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號指令
static int Proc_CMD_getwebserviceindex()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_getwebserviceindex].MsgNameEng,pCurrent);
  
  //取startmarker屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取endmarker屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取string屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_getwebserviceindex,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_getwebserviceindex, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行話務員登錄擴展指令
static int Proc_CMD_workerloginex()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_workerloginex].MsgNameEng, pCurrent);

  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workername屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取password屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取groupno屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取class屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取level屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取clearid屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取state屬性值
  AttrId = 9;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取dutyno屬性值
  AttrId = 10;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取autorecord屬性值
  AttrId = 11;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取calldirection屬性值
  AttrId = 12;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取accountno屬性值
  AttrId = 13;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取departmentno屬性值
  AttrId = 14;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_workerloginex,SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行話務員退出擴展指令
static int Proc_CMD_workerlogoutex()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_workerlogoutex].MsgNameEng,pCurrent);
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_workerlogoutex, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取坐席登錄的話務員信息指令
static int Proc_CMD_getseatlogindata()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_getseatlogindata].MsgNameEng,pCurrent);
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_getseatlogindata, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行發送HTTP協議請求指令
static int Proc_CMD_httprequest()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_httprequest].MsgNameEng,pCurrent);
  
  //取httpurl屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取httpparam屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取requesttype屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_httprequest,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_httprequest, SndMsg.GetBuf());

  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//取一空閑坐席并臨時鎖定，并返回該組登錄座席總數和空閑座席總數
static int Proc_CMD_getidleseat()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->InitIVREvent();

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_getidleseat].MsgNameEng, pCurrent);

  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
 	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	SndMsg.AddItemToBuf(AttrName,Value);

  //取seattype屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
 	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	SndMsg.AddItemToBuf(AttrName,Value);

  //取acdrule屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
 	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	SndMsg.AddItemToBuf(AttrName,Value);

  //取groupno屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取seatgroupno屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
      
  //取level屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
 	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	SndMsg.AddItemToBuf(AttrName,Value);

  //取levelrule屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
 	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	SndMsg.AddItemToBuf(AttrName,Value);

  //取lock屬性值
  AttrId = 9;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
 	Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
 	SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();

  IVRSendMessageBypCurrent(MSG_getidleseat,SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行解除臨時鎖定的座席指令
static int Proc_CMD_unlockseat()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_unlockseat].MsgNameEng,pCurrent);
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_unlockseat, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//更新服務評價到后臺IVR服務
static int Proc_CMD_updateservicescore()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_updateservicescore].MsgNameEng,pCurrent);
  //取seatno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取cdrserialno屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取score屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_updateservicescore, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//更新CDR字段數據
static int Proc_CMD_updatecdrfield()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_updatecdrfield].MsgNameEng,pCurrent);
  //取cdrserialno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取fieldname屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取fieldvalue屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_updatecdrfield, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//更新平臺系統參數
static int Proc_CMD_updatesysparam()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_updatesysparam].MsgNameEng,pCurrent);
  
  //取paramname屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取paramvalue屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_updatesysparam, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行外部程序指令
static int Proc_CMD_shellexecute()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_shellexecute].MsgNameEng,pCurrent);
  
  //取cmdname屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取cmdparam屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取readfile屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取timeout屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_shellexecute,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_shellexecute, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行取文本行某段字符串指令
static int Proc_CMD_gettxtlinestr()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_gettxtlinestr].MsgNameEng,pCurrent);
  
  //取readfile屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取lineno屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取separator屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取fieldno屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取startpos屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取length屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_gettxtlinestr,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_gettxtlinestr, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行設置分機呼轉指令
static int Proc_CMD_setforwarding()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_setforwarding].MsgNameEng,pCurrent);
  //取deviceno屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取forwardtype屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取forwardonoff屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取forwarddeviceno屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_setforwarding, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行通過socket通信發送查詢請求指令
static int Proc_CMD_socketrequest()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_socketrequest].MsgNameEng,pCurrent);
  
  //取serverip屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取serverport屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取streamtype屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取recvprotocol屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取sendstring屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_socketrequest,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_socketrequest, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行數據表記錄轉存指令
static int Proc_CMD_dbrecorddump()
{   
  US gwid;  
  US AttrId;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbrecorddump].MsgNameEng,pCurrent);
  
  //取sourdbid屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取querysqls屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取destdbid屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  //取insertsqls屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  US CallFlwNo = pCurrent->FlwNo;
  if (g_RunFlwMng[CallFlwNo].DispLogId == 0)
  {
    if (gwid == 0)
      DBSendMessageBypCurrent1(MSG_dbrecorddump,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent1(gwid, MSG_dbrecorddump, SndMsg.GetBuf());
  }
  else
  {
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbrecorddump,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbrecorddump, SndMsg.GetBuf());
  }
  
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行發送HTTP協議請求擴展指令
static int Proc_CMD_httprequestex()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_httprequestex].MsgNameEng,pCurrent);
  
  //取httpurl屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取httpparam屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取httpcookies屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取requesttype屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_httprequestex,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_httprequestex, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//特殊功能呼叫，如：監聽、強插
static int Proc_CMD_callfunction()
{   
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;
  
  pCurrent->InitIVREvent();
  
  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;
  
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_callfunction].MsgNameEng,pCurrent);
  //取calltype屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取seatno屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  //取workerno屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);
  
  SndMsg.AddTailToBuf();
  
  IVRSendMessageBypCurrent(MSG_callfunction, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行查詢數據操作指令
static int Proc_CMD_dbselect()
{   
  US gwid;  
  US AttrId;
  CH Value[MAX_SQLS_LEN];
  CH GetFieldList[MAX_VAR_DATA_LEN];
  const char *AttrValue;
  const char *AttrName;
  
  gwid = pCurrent->GetDBGwId();
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nDBTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_dbselect].MsgNameEng,pCurrent);
  
  //取sqls屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBufWithCheck(AttrName,Value); 
  
  memset(GetFieldList, 0, MAX_VAR_DATA_LEN);
  for (int i=4; i<=25; i++)
  {
    AttrValue=pCurrentCmd->ParamValues[i].C_Str();
    if (strcmp(AttrValue, "$S_050") != 0)
    {
      GetFieldList[i-4] = '1';
    }
    else
    {
      GetFieldList[i-4] = '0';
    }
  }
  SndMsg.AddItemToBuf("getidlist", GetFieldList);
  
  SndMsg.AddTailToBuf();
  
  US CallFlwNo = pCurrent->FlwNo;
  if (g_RunFlwMng[CallFlwNo].DispLogId == 0)
  {
    if (gwid == 0)
      DBSendMessageBypCurrent1(MSG_dbselect,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent1(gwid, MSG_dbselect, SndMsg.GetBuf());
  }
  else
  {
    if (gwid == 0)
      DBSendMessageBypCurrent(MSG_dbselect,  SndMsg.GetBuf());
    else
      GWSendMessageBypCurrent(gwid, MSG_dbselect, SndMsg.GetBuf());
  }
  
  pCurrent->DBQueryResult = 1;
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行增強型放音指令
static int Proc_CMD_playex()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
  if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
  return 0;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_playex].MsgNameEng, pCurrent);

  //取dtmfrule屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取playrule屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取asrrule屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取content1屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取content2屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取content3屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取content4屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取invalidvoc屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取timeoutvoc屬性值
  AttrId = 9;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_playex, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行從外部webservice接口查詢的JSON串中取數據指令
static int Proc_CMD_getwebservicejson()
{   
  US AttrId;
  US gwid;
  CH Value[MAX_SQLS_LEN];
  const char *AttrName;
  
  //取gwid屬性值
  AttrId = 1;
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);    
  gwid = atoi(Value);
  
  if (gwid >= MAX_GW_NUM || (gwid == 0 && !g_TcpServer.IsDBConnected()) || (gwid > 0 && gwid < MAX_GW_NUM && !g_TcpServer.IsGWConnected(gwid)))
  {
    pCurrent->EventVars[UC_DBEvent] = OnDBFail;
    pCurrent->CurrCmdAddr = NextCmdAddr;
    return 0;
  }
  pCurrent->InitDBEvent(g_nGWTimeOut);    
  SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLDBMsgRule[MSG_getwebservicejson].MsgNameEng,pCurrent);
  
  //取jsonnodetree屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取jsonnamelist屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  //取jsonfilter屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;
  
  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 
  
  SndMsg.AddTailToBuf();
  
  if (gwid == 0)
    DBSendMessageBypCurrent(MSG_getwebservicejson,  SndMsg.GetBuf());
  else
    GWSendMessageBypCurrent(gwid, MSG_getwebservicejson, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//執行文本串轉語音擴展指令 2017-04-03
static int Proc_CMD_ttsstringex()
{    
  US ChnType, ChnNo;
  US AttrId;
  CH Value[MAX_VAR_DATA_LEN];
  const char *AttrName;

  //pCurrent->EventVars[UC_OnEvent] = WAIT_EVENT; //edit 2007-09-15 del
  pCurrent->EventVars[UC_TTSEvent] = WAIT_EVENT;
  pCurrent->EventVars[UC_PlayEvent] = WAIT_EVENT;
  if (CheckCanSendMsg2IVR(pCurrent, UC_PlayEvent, NextCmdAddr) == false)
    return 0;

  ChnType = pCurrent->ChnType;
  ChnNo = pCurrent->ChnNo;

  SndMsg.SetHeadToBuf(  g_FlwRuleMng.XMLIVRMsgRule[MSG_ttsstringex].MsgNameEng, pCurrent);

  //取dtmfruleid屬性值
  AttrId = 1;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取playruleid屬性值
  AttrId = 2;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取asrruleid屬性值
  AttrId = 3;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取ttstype屬性值
  AttrId = 4;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取ttsgrammar屬性值
  AttrId = 5;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取ttsparam屬性值
  AttrId = 6;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取ttsformat屬性值
  AttrId = 7;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value);

  //取ttsfilename屬性值
  AttrId = 8;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  //取string屬性值
  AttrId = 9;
  AttrName=pCurrentCmdRule->Attribute[AttrId].AttrNameEng;

  Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue( AttrId, Value);
  SndMsg.AddItemToBuf(AttrName,Value); 

  SndMsg.AddTailToBuf();  

  IVRSendMessageBypCurrent(MSG_ttsstringex, SndMsg.GetBuf());
  pCurrent->CurrCmdAddr = NextCmdAddr;
  return 0;
}
//-----------------------------------------------------------------------------

//定義一個函數指針類型
typedef int (*Proc_CMD)();
Proc_CMD proc_cmd[]=
{
	Proc_CMD_NOP, //Proc_CMD_mainform, //主窗口定義
	Proc_CMD_NOP, //Proc_CMD_subform, //子窗口定義
	Proc_CMD_callsubform, //子窗口調用
	Proc_CMD_callextform, //外部窗口調用
	Proc_CMD_assign, //變量賦值
	Proc_CMD_getvalue, //取其他會話通道變量值
	Proc_CMD_answer, //來話應答
	Proc_CMD_sendsignal, //發送信令
	Proc_CMD_hangon, //掛機釋放
	Proc_CMD_dtmfrule, //DTMF按鍵規則定義
	Proc_CMD_getdtmf, //收碼定義
	Proc_CMD_playrule, //放音規則定義
	Proc_CMD_asrrule, //ASR規則定義
	Proc_CMD_setvolume, //改變放音音量
	Proc_CMD_playfile, //單文件放音
	Proc_CMD_setplaypos, //改變放音指針位置
	Proc_CMD_playfiles, //多文件放音
	Proc_CMD_playcompose, //合成串放音
	Proc_CMD_multiplay, //多語音合成放音
	Proc_CMD_ttsstring, //文本串轉語音
	Proc_CMD_ttsfile, //文本文件轉語音
	Proc_CMD_senddtmf, //發送DTMF
	Proc_CMD_sendfsk, //發送FSK
	Proc_CMD_playcfc, //會議室放音
	Proc_CMD_playtone, //播放信號音
	Proc_CMD_recordfile, //錄音定義
	Proc_CMD_recordcfc, //監視錄音定義
	Proc_CMD_stop, //停止放音錄音收碼
	Proc_CMD_callout, //電話呼出
	Proc_CMD_cancelcallout, //取消呼出
	Proc_CMD_sendcalledno, //發送被叫號碼
	Proc_CMD_transfer, //電話轉接
	Proc_CMD_callseat, //分配坐席
	Proc_CMD_stopcallseat, //取消分配坐席
	Proc_CMD_createcfc, //創建會議
	Proc_CMD_destroycfc, //釋放會議資源
	Proc_CMD_joincfc, //加入會議
	Proc_CMD_unjoincfc, //退出會議
	Proc_CMD_link, //通道交換
	Proc_CMD_talkwith, //與對方通話
	Proc_CMD_listenfrom, //監聽對方通話
	Proc_CMD_inserttalk, //強插對方通話
	Proc_CMD_threetalk, //三方通話
	Proc_CMD_stoptalk, //結束通話
	Proc_CMD_sendfax, //發送傳真
	Proc_CMD_recvfax, //接收傳真
	Proc_CMD_checkpath, //檢查文件路徑
	Proc_CMD_createpath, //創建文件路徑
	Proc_CMD_deletepath, //刪除文件路徑
	Proc_CMD_getfilenum, //取文件個數
	Proc_CMD_getfilename, //取文件名
	Proc_CMD_renname, //改文件名
	Proc_CMD_copyfile, //拷貝文件
	Proc_CMD_deletefile, //刪除文件
	Proc_CMD_fopen, //打開文本文件
	Proc_CMD_fread, //讀文本文件
	Proc_CMD_fwrite, //寫文本文件
	Proc_CMD_fclose, //關閉文本文件
	Proc_CMD_dbquery, //查詢數據操作
	Proc_CMD_dbfieldno, //取數據字段操作
	Proc_CMD_dbfirst, //首記錄
	Proc_CMD_dbnext, //下一記錄
	Proc_CMD_dbprior, //前一記錄
	Proc_CMD_dblast, //末記錄
	Proc_CMD_dbgoto, //移動記錄指針
	Proc_CMD_dbinsert, //插入記錄操作
	Proc_CMD_dbupdate, //修改記錄操作
	Proc_CMD_dbdelete, //刪除記錄操作
	Proc_CMD_startbill, //開始計費
	Proc_CMD_writebill, //終止計費并寫話單
	Proc_CMD_UNDEFINED, //Proc_CMD_oncallin, //響應電話呼入事件
	Proc_CMD_UNDEFINED, //Proc_CMD_onrecvsam, //收到后續地址事件
	Proc_CMD_UNDEFINED, //Proc_CMD_onsmsin, //短信呼入事件
	Proc_CMD_NOP, //Proc_CMD_catch, //響應事件指令
	Proc_CMD_onevent, //響應事件指令判斷事件
	Proc_CMD_NOP, //Proc_CMD_switch, //條件跳轉指令
	Proc_CMD_case, //條件跳轉指令判斷條件
	Proc_CMD_findsessionid, //查找目標會話通道標志
	Proc_CMD_goto, //跳轉指令
	Proc_CMD_return, //子窗口調用結束返回
	Proc_CMD_throw, //產生事件
	Proc_CMD_stradd, //字符串相加
	Proc_CMD_len, //取字符串長度
	Proc_CMD_substring, //取子字符串
	Proc_CMD_trim, //去掉字符串兩頭空格
	Proc_CMD_lefttrim, //去掉字符串左空格
	Proc_CMD_righttrim, //去掉字符串右邊空格
	Proc_CMD_left, //取左字符串
	Proc_CMD_right, //取右字符串
	Proc_CMD_strcmp, //區分大小寫的字符串比較
	Proc_CMD_strcmpi, //不區分大小寫的字符串比較
	Proc_CMD_like, //字符串相似比較
	Proc_CMD_lower, //將字符串小寫化
	Proc_CMD_upper, //將字符串大寫化
	Proc_CMD_strfmt, //將字符填充空格
	Proc_CMD_split, //根據分隔符分隔子字符串
	Proc_CMD_join, //將數組連成一字符串，并用分隔符分開
	Proc_CMD_strpos, //搜索子字符串位置
	Proc_CMD_getnow, //取當前日期時間函數
	Proc_CMD_getdate, //取日期函數
	Proc_CMD_gettime, //取時間函數
	Proc_CMD_getyear, //取年函數
	Proc_CMD_getmonth, //取月函數
	Proc_CMD_getday, //取日函數
	Proc_CMD_gethour, //取時函數
	Proc_CMD_getminute, //取分函數
	Proc_CMD_getsecond, //取秒函數
	Proc_CMD_getmsecond, //取毫秒函數
	Proc_CMD_getweekday, //取星期函數
	Proc_CMD_starttimer, //啟動定時器
	Proc_CMD_isdatetime, //檢查日期時間的合法性
	Proc_CMD_getinterval, //取日期或時間間隔
	Proc_CMD_incdatetime, //日期時間增量
	Proc_CMD_indatetime, //是否在時間段內
	Proc_CMD_daysinyear, //指定年有多少天
	Proc_CMD_daysinmonth, //指定月有多少天
	Proc_CMD_isleapyear, //指定年是否為閏年
	Proc_CMD_readinifile, //讀配置文件函數
	Proc_CMD_add, //算術相加函數
	Proc_CMD_sub, //算術相減函數
	Proc_CMD_mul, //算術相乘函數
	Proc_CMD_div, //算術除函數
	Proc_CMD_mod, //算術取模函數
	Proc_CMD_bitand, //位與
	Proc_CMD_bitor, //位或
	Proc_CMD_bitnot, //位非
	Proc_CMD_bitlmove, //位左移
	Proc_CMD_bitrmove, //位右移
	Proc_CMD_getbit, //取位值
	Proc_CMD_random, //產生隨機數
	Proc_CMD_abs, //取絕對值
	Proc_CMD_UNDEFINED, //Proc_CMD_pow, //乘方
	Proc_CMD_UNDEFINED, //Proc_CMD_sqrt, //平方根
	Proc_CMD_UNDEFINED, //Proc_CMD_sin, //正弦值
	Proc_CMD_UNDEFINED, //Proc_CMD_asin, //反正弦值
	Proc_CMD_UNDEFINED, //Proc_CMD_cos, //余弦值
	Proc_CMD_UNDEFINED, //Proc_CMD_acos, //反余弦值
	Proc_CMD_UNDEFINED, //Proc_CMD_tan, //正切值
	Proc_CMD_UNDEFINED, //Proc_CMD_atan, //反正切值
	Proc_CMD_max, //取最大數函數
	Proc_CMD_min, //取最小數函數
	Proc_CMD_math, //算術運算表達式
	Proc_CMD_UNDEFINED, //Proc_CMD_fmtdecimal, //浮點小數小數位數處理
	Proc_CMD_UNDEFINED, //Proc_CMD_isint, //整數有效判斷
	Proc_CMD_UNDEFINED, //Proc_CMD_isnumeric, //數值有效判斷
	Proc_CMD_default, //默認條件跳轉指令
	Proc_CMD_dbfieldname, //取數據字段操作
	Proc_CMD_dbsp, //執行存儲過程操作
	Proc_CMD_idiv, //執行算術整除函數操作
	Proc_CMD_waiting, //執行條件判斷等待操作
	Proc_CMD_dbclose, //關閉查詢的數據表
	Proc_CMD_checkfile, //檢查文件是否存在
	Proc_CMD_clearmixer, //清除放音合成緩沖區
	Proc_CMD_addfile, //增加文件到放音合成緩沖區
	Proc_CMD_adddatetime, //增加日期時間到放音合成緩沖區
	Proc_CMD_addmoney, //增加金額到放音合成緩沖區
	Proc_CMD_addnumber, //增加數值到放音合成緩沖區
	Proc_CMD_adddigits, //增加數字串到放音合成緩沖區
	Proc_CMD_addchars, //增加字符串到放音合成緩沖區
	Proc_CMD_playmixer, //播放合成緩沖區
	Proc_CMD_addttsstr, //增加TTS字符串到放音合成緩沖區
	Proc_CMD_addttsfile, //增加TTS文件到放音合成緩沖區
	Proc_CMD_UNDEFINED, //Proc_CMD_gotomainform, //由子窗口直接跳轉到主窗口指令
	Proc_CMD_md5, //MD5加密指令
	Proc_CMD_flash, //模擬外線拍叉簧指令
	Proc_CMD_gwquery, //查詢外部數據網關指令
	Proc_CMD_fork, //創建一個新子Session并執行
	Proc_CMD_writeinifile, //寫配置文件函數
	Proc_CMD_inc, //變量加1
	Proc_CMD_dec, //變量減1
	Proc_CMD_if, //IF條件判斷
	Proc_CMD_createmutex, //建立互斥
	Proc_CMD_releasemutex, //釋放互斥
	Proc_CMD_crypt, //密碼加密指令
	Proc_CMD_setdbgateway, //設置數據網關指令
	Proc_CMD_setvcparam, //設置變聲參數指令
	Proc_CMD_gettolltime, //根據余額取最大通話時長指令
	Proc_CMD_pause, //暫停放音指令
	Proc_CMD_replay, //重新放音指令
	Proc_CMD_fastplay, //快速放音指令
	Proc_CMD_checktone, //檢測信號音
	Proc_CMD_setchannel, //設置會話通道號
	Proc_CMD_dbrecord, //讀數據表當前記錄
	Proc_CMD_workerlogin, //話務員登錄
	Proc_CMD_workerlogout, //話務員退出
	Proc_CMD_dbspoutparam, //取存儲過程輸出參數值
	Proc_CMD_gwmakecallresult, //向外部數據網關發送發起呼叫的結果
	Proc_CMD_gwmsgeventresult, //向外部數據網關發送消息事件處理結果
	Proc_CMD_agtrancallresult, //向坐席發送轉接呼叫結果
	Proc_CMD_agtranivrresult, //向坐席發送坐席將來話轉接到ivr流程結果
	Proc_CMD_agjoinconfresult, //向坐席發送指定通道參加會議結果
	Proc_CMD_sendagentmsg, //向坐席發送消息
	Proc_CMD_pickupcall, //代接電話
	Proc_CMD_getacdqueue, //取ACD隊列信息
	Proc_CMD_setflwstate, //設置通道流程業務狀態信息
	Proc_CMD_swapcallresult, //將呼出或呼叫坐席結果改發送到指定的會話
	Proc_CMD_recvfsk, //接收FSK
	Proc_CMD_transferivr, //轉接到IVR
	Proc_CMD_releaseivr, //釋放IVR資源
	Proc_CMD_playswitchvoice, //播放交換機內置語音
	Proc_CMD_sendswitchcmd, //發送交換機操作命令
	Proc_CMD_dial, //撥打電話
	Proc_CMD_dbfieldtofile, //取查詢的字段值并存放到指定的文件
	Proc_CMD_dbfiletofield, //將文件內容存入數據表
	Proc_CMD_appendfaxfile, //追加發送的傳真文件
	Proc_CMD_getguid, //追加發送的傳真文件
	Proc_CMD_dbinsertex, //插入記錄操作（針對比較長的sql語句，小于1000字符）
	Proc_CMD_dbupdateex, //修改記錄操作（針對比較長的sql語句，小于1000字符）
	Proc_CMD_dbqueryex, //查詢數據操作（針對比較長的sql語句，小于1000字符）
	Proc_CMD_bandagentchn, //綁定坐席通道
	Proc_CMD_sendcalltoagent, //發送電話分配到電腦坐席
	Proc_CMD_querywebservice, //執行查詢外部webservice接口數據
	Proc_CMD_getwebservicexml, //從外部webservice接口查詢的XML串中取數據
	Proc_CMD_getxmlstringdata, //取xml串的節點數據
	Proc_CMD_getxmlfiledata, //取xml文件的節點數據
	Proc_CMD_closewebservice, //關閉外部webservice接口
	Proc_CMD_strreplace, //字符串替換
	Proc_CMD_getmarkerstring, //取特殊標簽位置的字符串
	Proc_CMD_base64encode, //字符串base64編碼
	Proc_CMD_base64decode, //字符串base64解碼
	Proc_CMD_getwebservicestr, //從外部webservice接口查詢的字符串取數據
	Proc_CMD_leftstrreplace, //替換字符串左邊指定的子字符串
	Proc_CMD_rightstrreplace, //替換字符串右邊指定的子字符串
	Proc_CMD_play, //通用放音指令
	Proc_CMD_getwebserviceindex, //從外部webservice接口查詢的字符串取給定的字符串所處的xml節點索引號
	Proc_CMD_workerloginex, //話務員登錄擴展指令
	Proc_CMD_workerlogoutex, //話務員退出擴展指令
	Proc_CMD_getseatlogindata, //取坐席登錄的話務員信息
	Proc_CMD_callsubformex, //子窗口調用擴展指令
	Proc_CMD_httprequest, //發送HTTP協議請求
  Proc_CMD_getidleseat, //取一空閑坐席并臨時鎖定，并返回該組登錄座席總數和空閑座席總數
  Proc_CMD_unlockseat, //解除臨時鎖定的座席
  Proc_CMD_updateservicescore, //更新服務評價到后臺IVR服務
  Proc_CMD_updatecdrfield, //更新CDR字段數據
  Proc_CMD_updatesysparam, //更新平臺系統參數
  Proc_CMD_shellexecute, //執行外部程序
  Proc_CMD_gettxtlinestr, //執行取文本行某段字符串
  Proc_CMD_setforwarding, //執行設置分機呼轉
  Proc_CMD_socketrequest, //通過socket通信發送查詢請求
  Proc_CMD_dbrecorddump, //數據表記錄轉存
  Proc_CMD_httprequestex, //發送HTTP協議請求
  Proc_CMD_callfunction, //特殊功能呼叫，如：監聽、強插
  Proc_CMD_dbselect, //查詢數據表記錄字段值操作
  Proc_CMD_playex, //增強型放音指令
  Proc_CMD_getwebservicejson, //從外部webservice接口查詢的JSON串中取數據
  Proc_CMD_ttsstringex, //文本串轉語音擴展指令 2017-04-03
};
//-----------------------------------------------------------------------------
//執行指令
int Proc_Cmd()
{    
	int Result; //指令執行結果：0-正確 1-錯誤 2-警告
         
  CurrCmdAddr=pCurrent->CurrCmdAddr;    
  pCurrentCmd=&pCurrent->GetFlw().RunCmds[CurrCmdAddr];
  pCurrentCmdRule=&g_FlwRuleMng.CmdRule[pCurrentCmd->CmdOperId];
  NextCmdAddr = pCurrentCmd->NextCmdAddr;
 
  ErrVar.Empty();
 
  try //捕獲變量操作拋出的異常
  {  
    preCmdOperId = pCurrentCmd->CmdOperId;
    if (preCmdOperId < MAX_CMDS_NUM)
      Result = proc_cmd[preCmdOperId]();
    else
      Result = 1;

    if (pCurrent) //edit 2009-06-15
    {
      if (preCmdOperId == CMD_catch || preCmdOperId == CMD_onevent
        || preCmdOperId == CMD_switch || preCmdOperId == CMD_case
        || preCmdOperId == CMD_default || preCmdOperId == CMD_waiting)
      {
        pCurrent->bWaiting = true;
      }
      else
      {
        pCurrent->bWaiting = false;
        pCurrent->WaitingCount = 0;
      }
      if ((pCurrent->WaitingCount > 600 && pCurrent->EventVars[UC_HangOn] == OnHangon) || pCurrent->WaitingCount > 7200)
      {
        MyTrace(0, "SerialNo=%ld ChnType=%d ChnNo=%d FlwName=%s CmdAddr=%d id=%s CmdName=%s WaitingCount=%d force free sessionid!!!",
          pCurrent->SerialNo,pCurrent->ChnType,pCurrent->ChnNo,pCurrent->GetRunFlw().FlwName.C_Str(),CurrCmdAddr,pCurrentCmd->Id.C_Str(),pCurrentCmdRule->CmdNameEng, pCurrent->WaitingCount);
        
        US CallFlwNo = pCurrent->FlwNo;
        US CallFlwId = pCurrent->FlwId;
        if (g_RunFlwMng[CallFlwNo].AutoStartId == pCurrent->GetSessionId())
        {
          g_RunFlwMng[CallFlwNo].AutoStartId = 0;
        }

        g_SessionMng.Free_SessionId(pCurrent->GetSessionId());
        SendTotalSessionsToAllLOG();
        SendOneFLWSessionsToAllLOG(CallFlwNo);
        return 1;
      }
    }
  }
  catch (const char *message)
  {
  	MyTrace(5, message); //輸出錯誤信息
  	Result=2; //終止當前指令
  }  
    
    if (Result ==0 ) //ok
    {    	
    	if(!pCurrent) //當前進程已經釋放
    		return 0;
    	
    	if(!pCurrent->Disp)
    		return 0;
    		
    	const RRunCmd *pCur=&pCurrent->GetFlw().RunCmds[CurrCmdAddr];
 	  	if(pCur->Disp1) //條件轉移指令
    	{    		
    		if(NextCmdAddr != pCurrent->CurrCmdAddr ) 
    		{ 
    			 	DispCmd(pCurrent);    			   			
				}
    	}
    	else //非條件轉移指令
    	{    		
    		if(pCur->Disp2) //可以打印
    			DispCmd(pCurrent);
    	}    	
    }
    else //指令執行過程中有錯誤
    {     	
      MyTrace(0, "SerialNo=%ld ChnType=%d ChnNo=%d FlwName=%s CmdAddr=%d id=%s CmdName=%s run error!!!",
        pCurrent->SerialNo,pCurrent->ChnType,pCurrent->ChnNo,pCurrent->GetRunFlw().FlwName.C_Str(),CurrCmdAddr,pCurrentCmd->Id.C_Str(),pCurrentCmdRule->CmdNameEng);

      SndMsg.SetHeadToBuf(g_FlwRuleMng.XMLIVRMsgRule[MSG_hangon].MsgNameEng,pCurrent);
    	SndMsg.AddTailToBuf();
      IVRSendMessageBypCurrent(MSG_hangon,SndMsg.GetBuf());     
     
      US CallFlwNo = pCurrent->FlwNo;
      US CallFlwId = pCurrent->FlwId;
      if (g_RunFlwMng[CallFlwNo].AutoStartId == pCurrent->GetSessionId())
      {
        g_RunFlwMng[CallFlwNo].AutoStartId = 0;
      }
      
      g_SessionMng.Free_SessionId(pCurrent->GetSessionId());
      SendTotalSessionsToAllLOG();
      SendOneFLWSessionsToAllLOG(CallFlwNo);
    }
    return Result;
}
