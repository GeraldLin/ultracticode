//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"
//---------------------------------------------------------------------------


CFlw::CFlw()
{
   // memset(FlwFileName, 0, MAX_PATH_FILE_LEN);
    Init_Flw_Data();
 //   CallCount = 0; //累計呼叫次數
    RunCmds = NULL;
}

CFlw::~CFlw()
{
    if (RunCmds != NULL)
    {
        delete[] RunCmds;
   
    }
}


	
//初始化流程參數
void CFlw::Init_Flw_Data(void)
{
  OpenId = 0; //使用打開標志 0：關閉；1：打開

  TotalCmds = 0; //總指令條數
  TotalEvents = 0; //總自定義事件數
  
  Onlines = 0; //實時在線人數
}
bool CFlw::FileExists(const CH *FileName)
{
  //增加判斷文件存在代碼

  return true;
}
//從目標文件讀流程
int CFlw::Load_Flw_From_Obj(const CH *FileName)
{
    UC Result = 0, VerId = 0;
    FILE *Fobj;
    US EventNum, PubVarNum, PriVarNum;
    VXML_FLW_PARA_NUM_STRUCT ParaNum;
    VXML_DESCRIPTION_STRUCT Descrip;
    VXML_EVENT_NAME_STRUCT EventName;
    VXML_VAR_NAME_STRUCT VarName;
    VXML_CMD_STRUCT Cmd;
    VXML_CMD_DATA_SAVE_STRUCT CmdSave;
    VXML_VAR_NAME_SAVE_STRUCT VarSave;
    int i;
    char DataBuf[2048], Attributes[MAX_CMD_ATTR_LEN], InitData[MAX_VAR_DATA_LEN]; //edit 2010-10-08
    UC ValueType[MAX_ATTRIBUTE_NUM];

    Init_Flw_Data();
    if (FileExists(FileName) == false)
    {
        //目標流程文件不存在
        Result = 1;
        return Result;
    }
    FlwFileName=FileName;

    //從流程文件中讀指令數據
    Fobj = fopen(FileName, "rb");
    if (Fobj != NULL)
    {
        if (fread(&ParaNum, sizeof(VXML_FLW_PARA_NUM_STRUCT), 1, Fobj) == 1)
        {
            if (strcmp(ParaNum.Header, "VxmlFlw Ver:1.0") == 0)
                VerId = 1;
            else if (strcmp(ParaNum.Header, "VxmlFlw Ver:2.0") == 0)
                VerId = 2;
            if (VerId == 1 || VerId == 2)
            {
                EventNum = ParaNum.Events;
                PubVarNum = ParaNum.PubVars;
                PriVarNum = ParaNum.PriVars;
                TotalCmds = ParaNum.Cmds;
                if (TotalCmds < MAX_CMD_LINES)
                {
                    if (RunCmds != NULL)
                    {
                        delete []RunCmds;
                        RunCmds = NULL;
                    }
                    //Cmds = new VXML_CMD_STRUCT[TotalCmds];
                    RunCmds =new RRunCmd[TotalCmds];
                    
                    if (RunCmds != NULL)                   
                    {                   

                        //流程概要說明
                        if (fread(&Descrip, sizeof(VXML_DESCRIPTION_STRUCT), 1, Fobj) == 1)
                        {
                        }
                        //自定義事件
                        for (i = 0; i < EventNum && i < MAX_EVENT_NUMS; i ++)
                        {
                          if (fread(&EventName, sizeof(VXML_EVENT_NAME_STRUCT), 1, Fobj) == 1)
                          {
                          }
                        }
                        //流程全局變量
                        if (VerId == 1) //版本1編譯格式
                        {
                          for (i = 0; i < PubVarNum && i < MAX_PUB_NUM; i ++)
                          {
                            if (fread(&VarName, sizeof(VXML_VAR_NAME_STRUCT), 1, Fobj) == 1)
                            {
                                PubVar[i].Name=VarName.VarName;
                                if(strcmp(VarName.InitData,"NULLDATA"))
                                   PubVar[i].Data=VarName.InitData;
                                 else
                                	 PubVar[i].Data="";
                            }
                          }
                        }
                        else if (VerId == 2) //版本2編譯格式
                        {
                          for (i = 0; i < PubVarNum && i < MAX_PUB_NUM; i ++)
                          {
                            if (fread(&VarSave, sizeof(VXML_VAR_NAME_SAVE_STRUCT), 1, Fobj) == 1)
                            {
                              PubVar[i].Name = VarSave.VarName;
                              PubVar[i].Data="";
                              if (VarSave.InitDataLen > 0)
                              {
                                if (fread(&InitData, VarSave.InitDataLen, 1, Fobj) == 1)
                                {
                                  InitData[VarSave.InitDataLen] = 0; //edit by zgj 2006-09-17
                                  if(strcmp(InitData,"NULLDATA"))
                                    PubVar[i].Data=InitData;
                                  else
                                	  PubVar[i].Data="";
                                }
                              }
                            }
                          }
                        }
                        //流程局部變量
                        if (VerId == 1) //版本1編譯格式
                        {
                          for (i = 0; i < PriVarNum && i < MAX_PRI_NUM; i ++)
                          {
                            if (fread(&VarName, sizeof(VXML_VAR_NAME_STRUCT), 1, Fobj) == 1)
                            {
                              PriVar[i].Name=VarName.VarName;
                              if(strcmp(VarName.InitData,"NULLDATA"))
                                PriVar[i].Data=VarName.InitData;
                              else
                                PriVar[i].Data="";
                            }
                          }
                        }
                        else if (VerId == 2) //版本2編譯格式
                        {
                          for (i = 0; i < PriVarNum && i < MAX_PRI_NUM; i ++)
                          {
                            if (fread(&VarSave, sizeof(VXML_VAR_NAME_SAVE_STRUCT), 1, Fobj) == 1)
                            {
                              PriVar[i].Name = VarSave.VarName;
                              PriVar[i].Data="";
                              if (VarSave.InitDataLen > 0)
                              {
                                if (fread(&InitData, VarSave.InitDataLen, 1, Fobj) == 1)
                                {
                                  InitData[VarSave.InitDataLen] = 0; //edit by zgj 2006-09-17
                                  if(strcmp(InitData,"NULLDATA"))
                                     PriVar[i].Data=InitData;
                                   else
                                	   PriVar[i].Data="";
                                }
                              }
                            }
                          }
                        }
                        //流程指令行
                        if (VerId == 1) //版本1編譯格式
                        {
                          for (i = 0; i < TotalCmds && i < MAX_CMD_LINES; i ++)
                          {
                            if (fread(&Cmd, sizeof(VXML_CMD_STRUCT), 1, Fobj) == 1)
                            {
                               ///////////////////////設置指令集
                              RunCmds[i].CmdOperId=Cmd.CmdOperId;
                              if(RunCmds[i].CmdOperId>=MAX_CMDS_NUM)
																return 10;
                              RunCmds[i].NextCmdAddr=Cmd.NextCmdAddr; 
                              if(RunCmds[i].NextCmdAddr>=TotalCmds)
                                return 9;
                              RunCmds[i].Id=Cmd.Id; 
                              RunCmds[i].ExtendId=Cmd.ExtendId; 
                              memcpy(RunCmds[i].ParamType, Cmd.ValueType, MAX_ATTRIBUTE_NUM); //復制所有屬性
                            
                              if(g_FlwRuleMng.FormatRunCmdParams(RunCmds[i],Cmd.Attributes)!=0)
                                return 8; //指令錯誤
                            }
                          }
                        }
                        else if (VerId == 2) //版本2編譯格式
                        {
                          for (int i = 0; i < TotalCmds && i < MAX_CMD_LINES; i ++)
                          {
                            if (fread(&CmdSave, sizeof(VXML_CMD_DATA_SAVE_STRUCT), 1, Fobj) == 1)
                            {
                              if (CmdSave.Header == 0xA5A50000 + i)
                              {
                                RunCmds[i].CmdOperId = CmdSave.CmdOperId;
                                if(RunCmds[i].CmdOperId>=MAX_CMDS_NUM)
																  return 10;
                                RunCmds[i].NextCmdAddr = CmdSave.NextCmdAddr;
                                if(RunCmds[i].NextCmdAddr>=TotalCmds)
                                  return 9;
                                RunCmds[i].Id=CmdSave.Id; 
                                RunCmds[i].ExtendId=(UC)CmdSave.ExtendId; 

                                if (fread(&DataBuf, CmdSave.DataBufLen, 1, Fobj) == 1)
                                {
                                  memset(ValueType, 0, MAX_ATTRIBUTE_NUM);  
                                  memset(Attributes, 0, MAX_CMD_ATTR_LEN); //edit 2010-10-08
                                  SplitAttrData(DataBuf, CmdSave.DataBufLen, CmdSave.CmdOperId, ValueType, Attributes);
                                    memcpy(RunCmds[i].ParamType, ValueType, MAX_ATTRIBUTE_NUM); //復制所有屬性
                                    if(g_FlwRuleMng.FormatRunCmdParams(RunCmds[i], Attributes)!=0)
                                      return 8; //指令錯誤
                                }
                              }
                            }
                          }
                        }
                    }
                    else
                    {
                        //建立數據區失敗
                        Result = 7;
                    }
                }
                else
                {
                    //指令行數超出范圍
                    Result = 6;
                }
            }
            else
            {
                //文件頭不正確
                Result = 5;
            }
        }
        else
        {
            //讀取數據格式錯誤
            Result = 4;
        }
        fclose(Fobj);
        if (Result == 0)
        {
            OpenId = 1;
        }
        return Result;
    }
    else
    {
        //打開流程文件錯誤
        Result = 3;
        return Result;
    }
}
//分離文本行(,)
int CFlw::SplitAttrData(const char *txtline, int len, US CmdOperId, UC *AttrType, char *AttrData)
{
  bool assingtype = false;
  int i = 0, k = 0, pos = 0, AttrId = 0, addr, size;
  char ch;

  addr = g_FlwRuleMng.CmdRule[CmdOperId].Attribute[AttrId].Address;
  size = g_FlwRuleMng.CmdRule[CmdOperId].Attribute[AttrId].Size;
  pos = addr;
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    if (ch == 0)
    {
      AttrId ++;
      addr = g_FlwRuleMng.CmdRule[CmdOperId].Attribute[AttrId].Address;
      size = g_FlwRuleMng.CmdRule[CmdOperId].Attribute[AttrId].Size;
      pos = addr;
      k = 0;
      assingtype = false;
      if (AttrId >= g_FlwRuleMng.CmdRule[CmdOperId].AttrNum)
        break;
    }
    else
    {
        if (assingtype == false)
        {
            AttrType[AttrId] = ch-'0';
            assingtype = true;
        }
        else
        {
            if (pos < MAX_CMD_ATTR_LEN && k < size) //edit 2010-10-08
            {
                AttrData[pos] = ch;
                pos ++;
                k ++;
            }
        }
    }
  }
  return AttrId;
}
int CFlw::GetCmdAddrByCmdIdName(const char *id)
{
  for (int i=0; i<TotalCmds; i++)
  {
    if (RunCmds[i].Id.CompareNoCase(id) == 0)
    {
      return i;
    }
  }
  return -1;
}
