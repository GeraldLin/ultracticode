//---------------------------------------------------------------------------
#ifndef RunFlwH
#define RunFlwH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class CRunFlw
{

		
public:
	CRunFlw(US flwno);
	virtual ~CRunFlw();


  US FlwNo;
  
  CStringX Explain; //流程業務說明
  //CStringX FlwFileName; //全路徑文件名
  CStringX FlwName; //流程名稱(即流程文件名，不包括文件擴展名)

  UC OpenId; //打開標志(1-打開 0-關閉)
  UC FuncGroup; //業務腳本功能服務組號
  US FuncNo; //流程功能號

  US FlwId; //最新加載流程標志
  CFlw Flw[2]; //流程存放
  CH LoadTime[20]; //流程加載時間
  US LoadCount; //流程在線加載次數
  US AutoStartId; //自動流程已啟動標志

  UC DispLogId; //顯示日志標志 0：不顯示 1：顯示
 
	US RunAfterLoad; //自動流程標志 0-否 1-是且啟動服務時自動啟動 2-是但啟動服務時不自動啟動

  US RestLines; //限制呼叫線數 0: 不限制 >0限制
  US AccessNum; //接入號碼個數
  //接入號碼格式：接入號碼字冠,最小長度(缺省為接入號碼字冠長度),最大長度(缺省為接入號碼字冠長度),主叫號碼(缺省為*表示所有)
  CStringX AccessCodes[MAX_ACCESSCODES_NUM]; //呼叫接入號碼字冠
  UC MinLen[MAX_ACCESSCODES_NUM]; //呼叫接入號碼最小長度
  UC MaxLen[MAX_ACCESSCODES_NUM]; //呼叫接入號碼最大長度
  UC DebugId;
    
public:
    //初始化
    void Init_Data(void);
    
    US GetOnLines()
    	{return Flw[0].Onlines+Flw[1].Onlines;}
		
		
public:    
    //讀配置文件
    void Read_Ini_File(const char *filename, const char *section);
    //讀流程接入號碼字冠
    void Read_AccessCodes(const char *filename, const char *section);
    //加載所有接入字冠
    void Load_AccessCodes(void);
    //增加接入字冠
    int Add_AccessCodes(const char *code, int minlen, int maxlen);
    //刪除接入字冠
    int Del_AccessCodes(const char *code);
};
//---------------------------------------------------------------------------



class CRunFlwMng
{
	//UI FlwNum;
	CRunFlw   *pRunFlws[MAX_FLOW_NUM] ;  
	public:
		CRunFlwMng()
		{
		//	FlwNum=MAX_FLOW_NUM;
			for(unsigned int i=0;i<MAX_FLOW_NUM;i++)
				pRunFlws[i]= 0;//new CRunFlw(i);
		}
		
		~CRunFlwMng()
		{
			for(unsigned int i=0;i<MAX_FLOW_NUM;i++)
				if(pRunFlws[i])
					delete pRunFlws[i];
		}
	
public:
	CRunFlw &operator[](unsigned int nIndex) 
	{return *pRunFlws[nIndex];}
//	CRunFlw & GetRunFlw(unsigned int nIndex) 
//	{return *pRunFlws[nIndex];}

public:
	int Get_Idel_FlwNo(CH *FlwName, US &FlwNo, US &FlwId);
  bool Get_FlwNo_By_FuncNo(unsigned short FuncNo,unsigned short &FlwNo);
  bool FindFlowNoByName(const char *extvxml,US &SubFlwNo);
  bool IsFlwNoAvail(unsigned short FlwNo)
  {	return  (FlwNo < MAX_FLOW_NUM)? true:false;} //edit by zgj
	
//  void ClearOnlines();
  US GetOnLines();
  bool IsFlwAddrOK(US flwno,US flwid,US cmdaddr);
  
public:  
 	void Send_AccessNo_To_IVR();
	void AutoLoadFlw(const char *filename);
  int AutoLoadFlw(const char *FilePath, int groupno, int funcno, int &rtFlwNo);
  void ReRunAutoRunFlw();
  void ReRunAutoRunFlw(US flwno);
  void StopAutoRunFlw(US flwno);
};

#endif
