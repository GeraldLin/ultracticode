//---------------------------------------------------------------------------
#ifndef __VARDEF_H__
#define __VARDEF_H__
//---------------------------------------------------------------------------

//全局變量、類定義
int g_LangID=1; //語言類型 0-默認 1-簡體中文 2-繁體中文 3-美國英文

bool	g_bLogId = false;
bool	g_bSaveId = false;
bool	g_bCommId = false;
bool	g_bCmdId = false;
bool	g_bVarId = false;
bool	g_bDebugId = false;
bool	g_bDispIVRSQLId = false;

bool g_bServiceDebugId = false;

bool  g_bAlarmId = false;
bool	g_bAlartId = false;

int   g_nParserAccessCodeType; //判斷接入號碼類型：1-通過被叫號碼判斷流程，2-通過原被叫號碼判斷流程，3-先按被叫號碼然后再按原被叫號碼 2016-03-31

int g_nDBTimeOut=15; //數據庫操作超時時長s
int g_nGWTimeOut=30; //數據網關操作超時時長s

CMsgfifo g_Msgfifo;
CTCPServer 	g_TcpServer; //通信消息類
CFlwRuleMng g_FlwRuleMng; //流程規則類
CGData 			g_GData; //配置、參數、變量數據類
CRunFlwMng 	g_RunFlwMng; //加載的流程類
CSessionMng g_SessionMng; //session管理類

CSession      *pCurrent=NULL; //當前處理的Session指針,用于訪問私有變量
const RRunCmd *pCurrentCmd=NULL; //指向當前的指令(注意可能不是pCurrent正在執行的指令)，用于處理指令屬性
int preCmdOperId;

static CXMLRcvMsg RcvMsg; //保存接收到的通訊消息，靜態變量防止頻繁分配內存

int SystemStartId = 0;
CStringX ErrVar; //全局用錯誤信息變量

int g_nINIFileType=0; //INI配置文件：0-默認的舊的格式 2-新的web配置格式

char g_szServiceINIFileName[MAX_PATH_FILE_LEN];
char g_szUnimeINIFileName[MAX_PATH_FILE_LEN];

char g_szRootPath[MAX_PATH_LEN];
char g_szAppPath[MAX_PATH_LEN]; //執行文件路徑
char g_szLogPath[MAX_PATH_LEN]; //日志文件路徑
char g_szFlwPath[MAX_PATH_LEN]; //流程文件路徑
char g_szIniPath[MAX_PATH_LEN]; //配置文件路徑
char g_szDatPath[MAX_PATH_LEN]; //數據文件路徑

int AuthMaxTrunk=2;
int AuthMaxSeat=2;
int AuthMaxIVR=2;
int AuthCardType=-1;
//int LocaCardType=0;
int LocaCardType=1;
CStringX MatchCaller="*";
CStringX MatchCalled="*";

int RunDays=0;
int RestDays=10;
bool AuthKeyId=false;
bool RestDayId=false;

bool g_nSendTraceMsgToLOG = false;
CLogerStatusSet LogerStatusSet[MAX_LOGNODE_NUM];
//---------------------------------------------------------------------------

#endif
