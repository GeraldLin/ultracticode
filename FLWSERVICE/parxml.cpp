#include "stdafx.h"
#include "parxml.h"

static CStringX ItemName;
static CStringX ItemValue;
	
//收發消息保存的類(依賴于不同的規則)
void CXMLMsg::Clear() //清除數據
{
	MsgId=0;
	AttrNum=0;
	Buf.Empty();
	//MsgName.Empty();
	for(int i=0;i<MAX_XMLMSG_ATTRIBUTE_NUM;i++)
		AttrValues[i].Empty();	
}	  

void CXMLMsg::SetMsgBufId(unsigned short msgid,const char *buf) //輸入一個待解析的完整串		
{
	MsgId=msgid;
	Buf=buf;
}

bool CXMLMsg::IsSplitChar(char c)//是否分割符號或者空格
{
	return (c=='=') || (c=='\'') || (c=='"') || isspace(c);
}

int CXMLMsg::ParseMsgName()//解析出消息名,存放到MsgName,返回下一個可以處理的位置
{
	int pos=0;
	int pos2;
	int len;
		
	pos=Buf.Find('<',pos); //尋找pos
	if(pos<0)
		return -1;
	
	pos=Buf.FindNotSpace(pos+1); //跳過空格
	if(pos<0)
		return -2;
	
	pos2=Buf.FindSpace(pos+1); //尋找空格
	if(pos2<0)
		return -2;
	
	len=pos2-pos;
	MsgName.CopyMidFrom(Buf,pos,len);		
	return pos2;
}


//從指定位置開始解析一項,返回0成功(Pos=下一個起始位置),
//小于0表示消息格式錯誤，大于0表示結束
//值中出現的單引號和雙引號使用2個重復值表示
int CXMLMsg::ParseItem(int &Pos,CStringX &ItemName,CStringX &ItemValue) 
{
	int pos1,pos2;
	char comma;
	int len=Buf.GetLength();
	const char * pC=Buf.C_Str();
	
	pos1=Pos;
	for(;pos1<len;pos1++)
	{
		if(!isspace(pC[pos1]))
			break;				
	}
	if(pos1>=len)
		return -1;
	
	if(pos1==(int)Pos) //第一個必須是空白分割符號
		return -1;
	
	pos2=pos1;
	for(;pos2<len;pos2++)
	{
		if(IsSplitChar(pC[pos2]))
			break;				
	}
	if(pos2>=len)
		return -2;
	ItemName.CopyMidFrom(Buf,pos1,pos2-pos1); //名字
	ItemName.TrimRight();
	
	pos1=Buf.FindNotSpace(pos2); //跳過空格
	if(pos1<0)
		return -3;

	if(pC[pos1] != '=')			
		return -4;
			
	pos1=Buf.FindNotSpace(pos1+1); //跳過空格
	if(pos1<0)
		return -5;
	comma=pC[pos1]; //前引號
		
	if((comma != '\'') && (comma != '"'))			
		return -6;
	
	//////////////////此時pos1為前引號
	ItemValue.Empty();
	pos1++;

NextFindcomma:	
	pos2=Buf.Find(comma,pos1); //找后引號
	if(pos2<0)
		return -7;
	
	if((pos2+2)>=len) //至少需要'/>'結尾
		return -8	;
	
	ItemValue.AppendMidFrom(Buf,pos1,pos2-pos1); //添加內容
//	printf("ItemValue=%s\n",ItemValue.C_Str());
	if(pC[pos2+1]==comma) //重復的引號，作為一個內容	
	{
		pos1=pos2+2;
		ItemValue+=comma;
		//printf("ItemValue=%s\n",ItemValue.C_Str());
	
		goto NextFindcomma;
	}	
	Pos=pos2+1; //返回下一個可用位置
	return 0;
}

//從當前位置開始解析尾部
int CXMLMsg::ParseTail(int Pos)
{
	int pos;
	int len=Buf.GetLength();
	const char * pC=Buf.C_Str();
	
	pos=Buf.FindNotSpace(Pos); //跳過空格
	if(pos<0)
		return -1;

	if((pos+1)>=len)
		return -2;

	if((pC[pos]!='/') || (pC[pos+1]!='>'))
		return -3;

	pos=Buf.FindNotSpace(pos+2); //跳過空格
	if(pos<0)
		return 0; //后面不應該有其他字符

	return -4;	
}	
	
//不能處理 A='B=xx' B='vvv' 等嵌套格式，必須在已經執行完ParseMsgName后才能執行
/*int CXMLMsg::ParseAttr(unsigned int index,const char *attrname) //根據名字解析buf中一個值，存放到對應位置
{
	int pos=1+MsgName.GetLength()+1; //至少跳過 <MsgName 和一個空格 
	int pos2;
	int len=strlen(attrname);
	char comma;
	
	while(1)
	{
		pos=Buf.Find(attrname,pos);
		//printf("find attrname=%s,pos=%d,in %s\n",attrname,pos,Buf.C_Str());
		if(pos<0)
			return -1;
		if(!isspace(Buf[pos-1])) //前面不是空格，不是獨立的單詞
		{
			pos+=len+1; //跳過本詞和一個可能的空格
			continue; //繼續搜索
		}	
		pos=Buf.FindNotSpace(pos+len); //跳過空格
		if(pos<0)
			return -2;
		//printf("find '=',pos=%d\n",pos);
		if(Buf[pos] != '=')			
			continue;
		
		pos=Buf.FindNotSpace(pos+1); //跳過空格
		if(pos<0)
			return -3;
		comma=Buf[pos]; //前引號
		
		if((comma != '\'') && (comma != '\"'))			
			continue;
		
		pos++;
		pos2=Buf.Find(comma,pos); //找后引號
		
		//printf("pos=%d,pos2=%d\n",pos,pos2);
		if(pos2<0)
			return -4;
		
		len=pos2-pos; //內容長度
		AttrValues[index].CopyMidFrom(Buf,pos,len);
		return 0;
	};		
}*/

void CXMLMsg::SetHeadToBuf(const char *msgname) //在buf中設置頭部
{
  Buf='<';
  Buf+=msgname;
}
	
void CXMLMsg::SetHeadToBuf(const CStringX &msgname) //在buf中設置頭部
{
  Buf='<';
  Buf+=msgname;
}
	
void CXMLMsg::AddItemToBuf(const char *name,const char *value) ////在buf中添加一項
{
	Buf+=' ';
  Buf+=name;	
  Buf+="='";
  Buf+=value;	  	
	Buf+='\x27'; //單引號
}

void CXMLMsg::AddItemToBuf(const char *name,const CStringX &value) ////在buf中添加一項
{	
	Buf+=' ';
  Buf+=name;	
  Buf+="='";
  Buf+=value;	  	
	Buf+='\x27'; //單引號
}		
void CXMLMsg::AddItemToBufWithCheck(const char *name,const char *value) ////在buf中添加一項,內容中可能有引號
{
	unsigned int i;
	unsigned int len=strlen(value);
	Buf+=' ';
  Buf+=name;	
  Buf+="=\"";
  
  for(i=0;i<len;i++)
  {
  	Buf+=value[i];	  	
  	if(value[i]=='\"')
  		Buf+=value[i];	 //變成2個雙引號
  }		 	
	Buf+='\"'; //雙引號
}

void CXMLMsg::AddItemToBufWithCheck(const char *name,const CStringX &value) ////在buf中添加一項,內容中可能有引號
{
	unsigned int i;
	unsigned int len=value.GetLength();
	Buf+=' ';
  Buf+=name;	
  Buf+="=\"";
  
  for(i=0;i<len;i++)
  {
  	Buf+=value[i];	  	
  	if(value[i]=='\"')
  		Buf+=value[i];	 //變成2個雙引號
  }		 	
	Buf+='\"'; //雙引號
}

void CXMLMsg::AddIntItemToBuf(const char *name,int value) ////在buf中添加一項整數
{
	char ValueStr[250];
	sprintf(ValueStr," %s='%d'",name,value);
	Buf+=ValueStr;
}

void CXMLMsg::AddLongItemToBuf(const char *name,long value) ////在buf中添加一項整數
{
	char ValueStr[250];
	sprintf(ValueStr," %s='%ld'",name,value);
	Buf+=ValueStr;
}

void CXMLMsg::AddFloatItemToBuf(const char *name,double value) ////在buf中添加一項浮點數
{
	char ValueStr[250];
	sprintf(ValueStr," %s='%f'",name,value);
	Buf+=ValueStr;
}

void CXMLMsg::AddTailToBuf() //在buf中設置添加尾部
{
	Buf+="/>";
}

int CXMLMsg::ParseMsgNameWithCheck(const char *msgname,unsigned int attrnum)
{
	int Pos;
	Pos=ParseMsgName(); //解析出消息名,存放到MsgName
  if(Pos<0)
  {
  	//printf("parser rcv msg name error\n");
   	return -1;
  } 	
  if(0!=MsgName.Compare(msgname))
  {
    //指令名稱不符
    //printf("msgname error,a=%s,b=%s\n",MsgName.C_Str(),MsgRule.MsgNameEng);
    return -2;
  }
  
  AttrNum = attrnum;
  for (unsigned short i = 0; i < AttrNum; i ++) //按照屬性數目清空
  {
		AttrValues[i].Empty();
	}	
	return Pos;
}

int CXMLMsg::ParseRevMsg(const VXML_RECV_MSG_CMD_RULE_STRUCT &MsgRule)
{
	unsigned short i,j;
 
	int Pos=ParseMsgNameWithCheck(MsgRule.MsgNameEng,MsgRule.AttrNum);
	if(Pos<0)
		return 1;

  for (i = 0; i < MAX_XMLMSG_ATTRIBUTE_NUM; i ++) //按照消息檢索
	{
//		printf("i=%d\n",i);
		if(0==ParseItem(Pos,ItemName,ItemValue)) //從指定位置開始解析一項,返回0成功,并返回下一個起始位置
		{
			for(j=0;j<AttrNum;j++)
			{
				if(0==ItemName.Compare(MsgRule.Attribute[j].AttrNameEng)) //匹配一個屬性
				{
					AttrValues[j]=ItemValue;												
				  break;
				}				
			}
		}
		else
			if(0!=ParseTail(Pos))
			{
				//printf("check tail error\n");
				return 2; //結尾不正確		
			}
			else
				break; //解析尾正確
	}
	
	return 0;
}

int CXMLMsg::ParseSndMsg(const VXML_SEND_MSG_CMD_RULE_STRUCT &MsgRule)
{
	unsigned short i,j;
 
	int Pos=ParseMsgNameWithCheck(MsgRule.MsgNameEng,MsgRule.AttrNum);
	if(Pos<0)
		return 1;

  for (i = 0; i < MAX_XMLMSG_ATTRIBUTE_NUM; i ++) //按照消息檢索
	{
//		printf("i=%d\n",i);
		if(0==ParseItem(Pos,ItemName,ItemValue)) //從指定位置開始解析一項,返回0成功,并返回下一個起始位置
		{
			for(j=0;j<AttrNum;j++)
			{
				if(0==ItemName.Compare(MsgRule.Attribute[j].AttrNameEng)) //匹配一個屬性
				{
					AttrValues[j]=ItemValue;												
				  break;
				}				
			}
		}
		else
			if(0!=ParseTail(Pos))
			{
				//printf("check tail error\n");
				return 2; //結尾不正確		
			}
			else
				break; //解析尾正確
	}
	
	return 0;
}
