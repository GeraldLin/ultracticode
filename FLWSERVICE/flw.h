//---------------------------------------------------------------------------
#ifndef FlwH
#define FlwH
//---------------------------------------------------------------------------
//#include "flwrule.h"
//---------------------------------------------------------------------------
//流程參數數量結構定義
typedef struct
{
  CH Header[20];
  US Events; //流程自定義事件數
	US PubVars; //流程全局變量數
	US PriVars; //流程局局變量數
	US Cmds; //流程指令數

}VXML_FLW_PARA_NUM_STRUCT;

//流程概要說明變量定義
typedef struct
{
	UC Langue; //標識語言 1：英文；2：中文
  CH Application[64];//MAX_FILENAME_LEN]; //業務腳本名稱
  CH Description[256];//MAX_VAR_DATA_LEN]; //業務腳本功能描述
  CH Version[32];//MAX_USERNAME_LEN]; //業務腳本版本號
  CH CopyRight[32];//MAX_USERNAME_LEN]; //版權所有者
  CH Author[32];//MAX_USERNAME_LEN]; //作者
  CH Maintainer[32];//MAX_USERNAME_LEN]; //維護人員
  CH EditDate[20]; //修改時間

}VXML_DESCRIPTION_STRUCT;


//變量名稱類型結構定義
typedef struct
{
  CH VarName[25];//MAX_VARNAME_LEN]; //變量名稱
  UC AreaType; //變量作用域: 1系統全局變量 2-流程全局變量 3-流程局部變量 4-系統固定變量名 5-流程固定變量名
  UC VarType; //變量類型: 0-any 1-bool 2-int 3-float 4-char 5-string
  unsigned char resv1;
  US MaxNum; //0表示單個變量，>=1表示數組的個數
  US Index; //邏輯序號,單個變量為0，數組為數組下標號
  CH InitData[256];//MAX_VAR_DATA_LEN]; //初始值

}VXML_VAR_NAME_STRUCT;

//變量名稱類型存儲結構定義
typedef struct
{
    US AreaType; //變量作用域: 1系統全局變量 2-流程全局變量 3-流程局部變量 4-系統固定變量名 5-流程固定變量名
    US VarType; //變量類型: 0-any 1-bool 2-int 3-float 4-char 5-string
    US MaxNum; //0表示單個變量，>=1表示數組的個數
    US Index; //邏輯序號,單個變量為0，數組為數組下標號
    US InitDataLen; //初始值串長度
    CH VarName[24]; //變量名稱

}VXML_VAR_NAME_SAVE_STRUCT;

//自定義事件結構定義
typedef struct
{
  CH EventName[25];//MAX_VARNAME_LEN]; //事件名稱

}VXML_EVENT_NAME_STRUCT;

//指令結構定義
typedef struct
{
  UC state; //有效狀態：0 無；1 有
  unsigned char resv1;
  US CmdAddr; //指令地址
  US CmdOperId; //指令操作代碼
  CH Id[21];//MAX_CMD_LABEL_LEN]; //流程中實際的id標識名稱
  CH CmdLabel[21];//MAX_CMD_LABEL_LEN]; //指令行標識名稱

	UC FormType; //該指令所在的窗口類型 0-mainform 1-subform
  unsigned char resv2;
  US FormIndex; //該指令所在的窗口索引號
  US Level; //樹節點層
  US Index; //樹節點索引

  CH Attributes[MAX_CMD_ATTR_LEN]; //指令屬性、參數數據存儲區 edit 2010-10-08
  UC ValueType[MAX_ATTRIBUTE_NUM]; //屬性對應的參數值類別: 0-未知 1-常量 2-變量名 3-字符串運算表達式 4-邏輯運算表達式 5-算術運算表達式 6-sql語句
	UC ExtendId; //指令屬性、參數擴展標志，0: 單指令 1：指令分支跳轉起始指令標志, 2：指令分支跳轉跳轉指令標志, 3：指令分支跳轉末指令標志, 4：多參數指令行起始指令標志, 5：多參數指令行中間指令標志, 6：多參數指令行末指令標志,

  CH NextCmdLabel[21];//MAX_CMD_LABEL_LEN]; //下一指令行標識名稱
  US NextCmdAddr; //下一指令行地址

  UC DebugBreakId; //調試中斷標志 0:不中斷 1：中斷
  unsigned char resv3;
  
  US IdCmdAddr; //指令地址（針對條件判斷指令）

}VXML_CMD_STRUCT;



//運行時指令結構
struct RRunCmd
{
  US CmdOperId; //指令操作代碼
  US NextCmdAddr; //下一指令行地址
  CStringX Id; //流程中實際的id標識名稱
  
  bool Disp1; //是否條件指令(本指令條件成立時可以打印下一條指令,否則不打印)
  bool Disp2; //非條件指令時,是否可打印調試信息(True時可以打印下一條指令,否則不打印）
 
  UC ExtendId; //指令屬性、參數擴展標志，0: 單指令 1：指令分支跳轉起始指令標志, 2：指令分支跳轉跳轉指令標志, 3：指令分支跳轉末指令標志, 4：多參數指令行起始指令標志, 5：多參數指令行中間指令標志, 6：多參數指令行末指令標志,
  UC ParamNum; //參數個數
  
///////以下參數數組
	CStringX ParamValues[MAX_ATTRIBUTE_NUM];
  UC 			ParamType[MAX_ATTRIBUTE_NUM]; //屬性對應的參數值類別: 0-未知 1-常量 2-變量名 3-字符串運算表達式 4-邏輯運算表達式 5-算術運算表達式 6-sql語句

};

//存儲指令結構
typedef struct
{
  UL Header; //指令頭，防非法格式文件(0xA5A5nnnn A5A5固定不變 nnnn為本指令地址)
  US CmdOperId; //指令操作代碼
  US NextCmdAddr; //下一指令行地址
  US ExtendId; //指令屬性、參數擴展標志，0: 單指令 1：指令分支跳轉起始指令標志, 2：指令分支跳轉跳轉指令標志, 3：指令分支跳轉末指令標志, 4：多參數指令行起始指令標志, 5：多參數指令行中間指令標志, 6：多參數指令行末指令標志,
  US IdCmdAddr; //指令地址（針對條件判斷指令）
  US ParamNum; //屬性參數值個數
  US DataBufLen; //屬性參數值數據區總長度
  
  CH Id[24]; //流程中實際的id標識名稱
  //CH CmdLabel[24]; //指令行標識名稱
  //CH NextCmdLabel[24]; //下一指令行標識名稱

  /*屬性參數值數據區格式如下：
  [T0][VALUE0]\0[T1][VALUE1]\0 ......
  Tn: 屬性對應的參數值類別: 0-未知 1-常量 2-變量名 3-字符串運算表達式 4-邏輯運算表達式 5-算術運算表達式 6-sql語句
  [VALUEn]: 屬性參數內容
  \0: 間隔標志
  注意：寫入時嚴格按定義的順序寫參數值
  */

}VXML_CMD_DATA_SAVE_STRUCT;
//---------------------------------------------------------------------------


//變量結構定義
struct RVarStruct 
{
  CStringX Name; //變量名稱
  CStringX Data;     //開始為初始值，隨后可被修改
};

class CFlw
{

		
public:
	CFlw();
	virtual ~CFlw(); 

  CStringX FlwFileName;//[MAX_PATH_FILE_LEN]; //腳本全路徑文件名
  UC OpenId; //使用打開標志 0：關閉；1：打開

  US TotalCmds; //總指令條數
  US TotalEvents; //總自定義事件數
  
  US Onlines; //實時在線人數

  //流程指令定義數據區
	//VXML_DESCRIPTION_STRUCT Description; //流程概要說明變量
	//CStringX	Events[MAX_EVENT_NUMS]; //流程自定義事件
  RVarStruct	  	PubVar[MAX_PUB_NUM]; //流程全局變量,運行時使用
  RVarStruct			PriVar[MAX_PRI_NUM]; //流程局部變量初始值,只參考用

	RRunCmd						*RunCmds; //運行時指令數組

public:
  bool FileExists(const CH *FileName);  
  //初始化流程參數
  void Init_Flw_Data(void);
  //從目標文件讀流程
  int Load_Flw_From_Obj(const CH *FileName);
  int SplitAttrData(const char *txtline, int len, US CmdOperId, UC *AttrType, char *AttrData);
  int GetCmdAddrByCmdIdName(const char *id);
};
//---------------------------------------------------------------------------
#endif
