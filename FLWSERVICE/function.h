//---------------------------------------------------------------------------
#ifndef functionH
#define functionH

//---------------------------------------------------------------------------
typedef struct
{
  CH RunTime[128];
  US RunDays;

}VXML_RUNDAYS_STRUCT;

//通用字符串緩沖結構定義
typedef struct
{
    CH Data[MAX_VAR_DATA_LEN]; //變量名稱

}VXML_DATA_STRUCT;

struct DAYINMONTHS
{
	US Days[13];
};
bool IsStrMatch(const CStringX &Rule,const CStringX &code);
bool IsMatch(const CStringX &caller,const CStringX &called);

//組合消息類型及消息編號
static inline US CombineMessageID(UC MsgType, UC MsgNo)
{
    return CombineObjectID(MsgType , MsgNo);
}


  	
//分解消息類型及消息編號
static inline void GetMsgID(US Msg_wParamLo, UC &MsgType, UC &MsgNo)
{
	GetClientID(Msg_wParamLo, MsgType, MsgNo);
}

//整數字符串有效判斷(0-有效的整數字符串)
int Check_Int_Str(const char *ValueStr); //檢查是否合法整數

static inline int Check_long_Str(const char *ValueStr)
{return Check_Int_Str(ValueStr);}


//整數字符串有效判斷(0-有效的整數字符串、并返回具體數值，1-超出范圍，2-無效的整數字符串)
static inline int Check_Int_Str(const char *ValueStr, int &IntValue)
{ 
	int result = Check_Int_Str(ValueStr);
	if(result==0)
	  IntValue = atoi(ValueStr);
	return result;
}

static inline int Check_long_Str(const char *ValueStr, long &IntValue)
{return Check_Int_Str(ValueStr,(int &)IntValue);}


//浮點數字符串有效判斷
int Check_Float_Str(const  char *ValueStr);
//浮點數字符串有效判斷
static inline int Check_Float_Str(const  char *ValueStr, double &FloatValue )
{
	int result = Check_Float_Str(ValueStr);
	if(result==0)
	  FloatValue = atof(ValueStr);
	return result;
}
char *EncodePass(const char *Pass);
char *DecodePass(const char *Pass);

char *MyGetGUID();

//---------------------------------------------------------------------------
//浮點數轉字符串
int Float_To_Str( double FloatValue, char *ValueStr );
//整數轉字符串
int Int_To_Str( int IntValue, char *ValueStr );
//---------------字符串函數--------------------------------------------------
//取字符串的長度
US MyStrLen( const char *vString );
//取子字符串
char *MySubString( const char *vString, const US vStart, const US vLenght );
//去掉字符串兩頭空格
char *MyTrim( const char *vString );
//去掉字符串左空格
char *MyLeftTrim( const char *vString );
//去掉字符串右邊空格
char *MyRightTrim( const char *vString );
//從左取字符串
char *MyLeft( const char *vString, const US vLenght );
//從右取字符串
char *MyRight( const char *vString, const US vLenght );
//字符串相加
char *MyStrAdd( const char *vString1, const char *vString2 );
//字符串小寫
char *MyLower( const char *vString );
//字符串大寫
char *MyUpper( const char *vString );
//取字符串位置
int MyStrPos( const char *vString1, const char *vString2 );
//格式化字符串
char *MyStrFmt( const char *vString, const UC vDirection, const US vLenght );
//切割字符串
US MySplit( const char *vString, const char vSeparator, const US vNumber, VXML_DATA_STRUCT *vArray );
//比較字符串
UC MyStrCmp( const char *vString1, const char *vString2 );
//忽略大小寫比較字符串
UC MyStrCmpi( const char *vString1, const char *vString2 );
int MyIsLike(const char *vString, const char *vLike);
//---------------------------------------------------------------------------
//---------------日期時間函數------------------------------------------------
int GetIncDays(int curYear, int curMonth, int curDay, int months);
int isDateTimeStr(const char *pszDateTime, int &tm_year, int &tm_mon, int &tm_mday, int &tm_hour, int &tm_min, int &tm_sec);
int String2CTime(const char *datetimestr, CTime &result);
int dtStr2time_t(const char *datetimestr, time_t &tDateTime);
int dtStr2tm(const char *datetimestr, struct tm *tDateTime);

//轉換time_t格式時間為本地字符串格式時間
const char *ConverTimeToStr(time_t tm);
void ConverTimeToStr(char *Result,time_t tm);


//取當前日期時間(yyyy-mm-dd hh-mi-ss)
const char *MyGetNow(void);
UC MyGetNow( char *Result );

//取當前日期(yyyy-mm-dd)
UC MyGetDate( char *Result );
//取日期(yyyy-mm-dd)
UC MyGetDate( const char *vDateTime, char *Result );
//取當前時間(yyyy-mm-dd)
UC MyGetTime( char *Result );
//取時間(yyyy-mm-dd)
UC MyGetTime( const char *vDateTime, char *Result );
//取當前時間串(hhmmss)
char *MyGetTimeStr( void );
//取當前年(yyyy)
UC MyGetYear( US &Result );
//取年(yyyy)
UC MyGetYear( const char *vDateTime, US &Result );
//取當前月(mm)
UC MyGetMonth( US &Result );
//取月(mm)
UC MyGetMonth( const char *vDateTime, US &Result );
//取當前日(dd)
UC MyGetDay( US &Result );
//取日(dd)
UC MyGetDay( const char *vDateTime, US &Result );
//取當前時(hh)
UC MyGetHour( US &Result );
//取時(hh)
UC MyGetHour( const char *vDateTime, US &Result );
//取當前分(mi)
UC MyGetMinute( US &Result );
//取分(mi)
UC MyGetMinute( const char *vDateTime, US &Result );
//取當前秒(ss)
UC MyGetSecond( US &Result );
//取秒(ss)
UC MyGetSecond( const char *vDateTime, US &Result );
//取當前毫秒(ms)
UC MyGetMsecond( US &Result );
//取毫秒(ms)
UC MyGetMsecond( const char *vDateTime, US &Result );
//取當前星期(ww)
UC MyGetWeekDay( US &Result );
//取星期(ww)
UC MyGetWeekDay( const char *vDateTime, US &Result );
//日期時間合法性判斷
UC MyIsDateTime( const char *vDateTime, char *pszDTStr );
//取日期時間間隔
UC MyDTBetween( const char *vDateTime1, const char *vDateTime2, const UC vType, int &Result );
//取日期時間增量
UC MyIncDateTime( const char *vDateTime, const UC vType, const int vNumber, char *Result );
//日期時間范圍判斷
UC MyInDateTime( const char *vDateTime, const char *vDateTime1, const char *vDateTime2 );
//啟動定時器
UC MyTimer( const UC vTimerId, const US vTimeLen );
bool IsLeapYear(US year);
US DaysInAMonth(US year, US month);
US DaysInAYear(US year);
int MyCheckFileExist(const char *FilePath);
//檢查并創建文件最后一級目錄
int MyCreateLastDir(const char *FilePath);
//---------------------------------------------------------------------------
//---------------數學函數----------------------------------------------------
//算術相加函數
double MyAdd( const double vNumber1, const double vNumber2 );
//算術相減函數
double MySub( const double vNumber1, const double vNumber2 );
//算術相乘函數
double MyMul( const double vNumber1, const double vNumber2 );
//算術相除函數
double MyDiv( const double vNumber1, const double vNumber2 );
//算術取模函數
int MyMod( const int vNumber1, const int vNumber2 );
//算術取隨機數函數
int MyRandom( const int vLowNumber, const int vUpNumber );
//算術取絕對值函數
int MyAbs( const int vNumber );
long MyLabs( const long vNumber );
double MyFabs( const double vNumber );
//取最大數函數
double MyMax( const double vNumber1, const double vNumber2 );
//取最小數函數
double MyMin( const double vNumber1, const double vNumber2 );
//---------------------------------------------------------------------------
//---------------文本文件操作函數--------------------------------------------

//---------------------------------------------------------------------------


//算術表達式運算,帶括號
UC Operation_Math_Exp( char *Exp, double &Result );
//純四則表達式運算,不帶括號
UC Operation_Arithmetic_Exp( char *Exp, double &Result );
//邏輯表達式比較運算:
UC Compare_Operation( char *Exp, char *Result );
//邏輯表達式邏輯運算(返回值：0=false,1=true,2=error)
UC Operation_Logic_Exp( char *Exp );
//字符串相加運算
int Operation_String_Exp(char *Exp, char *Result);


//是否為合法的變量名格式,1=合法的單變量，2=合法的間接變量，其它=非法,合法時返回變量號
int IsVarNameFormatOK(const char *VarName,int &AddrNo1, int &AddrNo2);
//根據變量名取變量地址C_Str()
CStringX *GetpCurrentVarPointer(const char *VarName,int Offset=0,const CStringX **pOrgVarName=0);
//根據變量名取變量的值(返回值0-正確 1-非法的變量名)
void Get_pCurrent_VarValue_By_VarName(   const char *VarName, char *Value);


void DispVar(const CSession *pSession,const char *Name,const CStringX &OrgName,const CStringX &Value,int Offset);
void DispCmd(const CSession *pSession);


//給變量賦值字符串
void Assign_pCurrent_Str_To_Var(int Offset,const char *VarName, const char *Value);
static inline void Assign_pCurrent_Str_To_Var(int Offset,const char *VarName, const CStringX &Value)
{ Assign_pCurrent_Str_To_Var(Offset,VarName, Value.C_Str());}
static inline void Assign_pCurrent_Str_To_Var(const char *VarName, const char *Value)
{ Assign_pCurrent_Str_To_Var(0,VarName,Value);}
static inline void Assign_pCurrent_Str_To_Var(const char *VarName, const CStringX &Value)
{ Assign_pCurrent_Str_To_Var(0,VarName, Value);}


//給變量賦值
void Assign_pCurrent_intValue_To_Var(int Offset,const char *VarName, int intValue);
static inline void Assign_pCurrent_intValue_To_Var(const char *VarName, int intValue)
{ Assign_pCurrent_intValue_To_Var(0,VarName,intValue);}
//給變量賦值
void Assign_pCurrent_floatValue_To_Var(   int Offset, const char *VarName, double floatValue);
static inline void Assign_pCurrent_floatValue_To_Var(const char *VarName, double floatValue)
{ Assign_pCurrent_floatValue_To_Var(0,VarName, floatValue);}
//替換表達式中的變量值
void Replace_pCurrent_Exp_VarValues(   UC ExpType, const char *Exp, char *ReturnExp);
//替換表達式中的變量值
void Replace_pCurrent_Exp_VarValues_For_FindId(   UC ExpType, const char *Exp, char *ReturnExp);
//根據函數串調用函數
void pCurrent_Func_Proc(   char *FuncStr, char *Value);
//替換sql語句中的變量值
void Replace_pCurrentCmd_SqlExp_VarValues(   US AttrId,  char *ReturnSqlExp);


//根據變量名取變量的值(返回值0-正確 1-非法的變量名)
void Get_pCurrentCmd_pCurrent_VarValue_By_AttrValue(   US AttrId,char *Value);

void CreateAllDirectories(CString strDir);
void MyGetMarkerString(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, int nIndex, int nOutputMaxLen, char *pszResult, bool &bFinded);
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult);
//---------------------------------------------------------------------------
#endif
