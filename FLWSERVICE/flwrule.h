//---------------------------------------------------------------------------
#ifndef FlwRuleH
#define FlwRuleH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//瑈祘砏玥摸
class CFlwRuleMng
{

public:
	CFlwRuleMng();
	virtual ~CFlwRuleMng();


    //砏玥
    const VXML_CMD_RULE_STRUCT                *CmdRule;//[MAX_CMDS_NUM];
    //ㄧ计把计粂猭砏玥
    const VXML_FUNC_RULE_STRUCT               *FuncRule;//[MAX_FUNC_NUM];
    //俱计璖瞅砏玥
    const VXML_INTEGER_RANGE_RULE_STRUCT      *IntRange;//[MAX_INT_RANGE_NUM];
    //虫才璖瞅砏玥
    const VXML_CHAR_RANGE_RULE_STRUCT         *CharRange;//[MAX_CHAR_RANGE_NUM];
    //才﹃いす砛瞷才砏玥
    const VXML_STRING_RANGE_RULE_STRUCT       *StrRange;//[MAX_STR_RANGE_NUM];
    //俱计猅羭才﹃砏玥
    const VXML_MACRO_INT_RANGE_RULE_STRUCT    *MacroIntRange;//[MAX_MACRO_INT_NUM];
    //猅羭才﹃砏玥
    const VXML_MACRO_STR_RANGE_RULE_STRUCT    *MacroStrRange;//[MAX_MACRO_STR_NUM];
    //╰参跑秖
    const VXML_SYSVAR_STRUCT                  *SysVar;//[MAX_SYS_NUM];

    const VXML_RECV_MSG_CMD_RULE_STRUCT       *CFGSRVMsgRule;
    //ivr祇癳砏玥
    const VXML_SEND_MSG_CMD_RULE_STRUCT				*XMLIVRMsgRule;
    //ivr砏玥
    const VXML_RECV_MSG_CMD_RULE_STRUCT       *IVRXMLMsgRule;
    //db祇癳砏玥
    const VXML_SEND_MSG_CMD_RULE_STRUCT       *XMLDBMsgRule;
    //db砏玥
    const VXML_RECV_MSG_CMD_RULE_STRUCT       *DBXMLMsgRule;

    //log祇癳ㄓ砏玥
    const VXML_SEND_MSG_CMD_RULE_STRUCT				*LOGIVRMsgRule;
    
    //FLWlog矪瞶挡狦
    const VXML_RECV_MSG_CMD_RULE_STRUCT       *IVRLOGMsgRule;
    
    //妮┦把计浪琩
    int Parse_Msg_Attr_Value(const VXML_RECV_MSG_ATTR_RULE_STRUCT &AttrRule,CStringX &ValueStr);
    int Parse_Msg_Attr_Value(const VXML_SEND_MSG_ATTR_RULE_STRUCT &AttrRule,CStringX &ValueStr);
    
  //  int ParseXMLstr(const VXML_RECV_MSG_ATTR_RULE_STRUCT &AttrRule,CStringX &ValueStr);

		//更瑈祘Αて兵(竒恶ㄤウ)┮Τ把计㎝ゴ夹в,Θ0
		int FormatRunCmdParams(RRunCmd &Cmd,const char *AttributesBuf);
public:
	//把计猭┦浪琩,猭0
	//int Check_Var_Value(US CmdOperId, US AttrId, CH *ValueStr);

};
//---------------------------------------------------------------------------
#endif
