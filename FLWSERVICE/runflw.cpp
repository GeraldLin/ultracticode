//---------------------------------------------------------------------------
#include "stdafx.h"
#include "extern.h"

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
//---------------------------------------------------------------------------
 //返回一個全路徑的主文件名和擴展名
static void   splitnamext(const char *fullpath, char *filename, char *extname)
{
	
	int i,j;
	int len=strlen(fullpath);
	filename[0]=extname[0]=0;
	for(i=len-1;i>=0;i--)
		if(fullpath[i]=='.')
		{
			strcpy(extname,fullpath+i+1);
			for(j=i-1;j>=0;j--)
			{
				if(fullpath[j]=='/')
				{					
					strncpy(filename,fullpath+j+1,i-j-1);
					filename[i-j-1]=0;
					return;
				}
			}
			strncpy(filename,fullpath,i);
			filename[i]=0;				
			return;	
		}
}

CRunFlw::CRunFlw(US flwno)
{
  Init_Data();
  FlwNo=flwno;
}

CRunFlw::~CRunFlw()
{
//  FlwRule = NULL;
//  FlwData = NULL;
 // Flw[0].FlwRule = NULL;
 // Flw[1].FlwRule = NULL;
}



//初始化
void CRunFlw::Init_Data(void)
{
//  state = 0;
  
//  FlwRule = NULL;

//  memset(Explain, 0, MAX_USERNAME_LEN);
//  memset(FlwFileName, 0, MAX_PATH_FILE_LEN);
//  memset(FlwName, 0, MAX_FILENAME_LEN);

  OpenId = 0;
  FuncGroup = 0xFF;
  FuncNo = 0xFFFF;

  FlwId = 0;
  memset(LoadTime, 0, 20);
  LoadCount = 0;
  AutoStartId = 0;

//  memset(TraceCaller, 0, MAX_TELECODE_LEN);
 // memset(TraceCalled, 0, MAX_TELECODE_LEN);
 
  RestLines = 0;
	AccessNum = 0;
	for (int i = 0; i < MAX_ACCESSCODES_NUM; i ++)
	{
		AccessCodes[i] = "";
    MinLen[i] = 0;
    MaxLen[i] = 0;
	}
}
CH *ChangeFileExt(CH *FilePath, CH *ext)
{
  static CH filename[MAX_PATH_FILE_LEN];
  int len;

  //需要增加代碼修改文件擴展名
  len = strlen(FilePath);
  memset(filename, 0, MAX_PATH_FILE_LEN);
  strncpy(filename, FilePath, len-4);
  strcat(filename, ext);

  return filename;
}

//讀配置文件
void CRunFlw::Read_Ini_File(const char *filename, const char *section)
{
  CH explain[MAX_USERNAME_LEN]; //流程業務說明
  
  GetPrivateProfileString(section, "Explain", "", explain, MAX_USERNAME_LEN, filename);
  Explain = explain;

  FuncGroup = GetPrivateProfileInt(section,"FuncGroup", 0, filename);
  
  FuncNo = GetPrivateProfileInt(section,"FuncNo", 0, filename);

  RestLines = GetPrivateProfileInt(section,"RestLines", 0, filename);

  AccessNum = GetPrivateProfileInt(section,"AccessNum", 0, filename);
   
  RunAfterLoad = GetPrivateProfileInt(section,"RunAfterLoad", 0, filename);
  
  if (RunAfterLoad == 0)
  {
    DispLogId = GetPrivateProfileInt(section,"SaveLogId", 1, filename);
  }
  else
  {
    DispLogId = GetPrivateProfileInt(section,"SaveLogId", 0, filename);
  }

  Read_AccessCodes(filename,section);
}

//讀流程接入號碼字冠
void CRunFlw::Read_AccessCodes(const char *filename, const char *section)
{
  char ch, ch1, ch2=0;
  CH msgbuf[MAX_VAR_DATA_LEN], str[5][50];
  CH temp[MAX_VAR_DATA_LEN];
  US h, i, j, k;

	//AccessNum = 0;
	for (i = 0; i < MAX_ACCESSCODES_NUM; i ++)
	{
		//memset(AccessCodes[i], 0, MAX_TELECODE_LEN);
    MinLen[i] = 0;
    MaxLen[i] = 0;
	}
 
  for (i = 0; i < AccessNum && i < MAX_ACCESSCODES_NUM; i ++)
  {
    sprintf(temp,"AccessCode[%d]", i);
    GetPrivateProfileString(section, temp, "", msgbuf, MAX_VAR_DATA_LEN, filename);
    //printf("---%s=%s\n",temp,msgbuf);
    
    if (strlen(msgbuf) > 0)
    {
        for (j = 0; j < 5; j ++ )
        {
            memset(str[j], 0, 50);
        }
        j = 0;
        k = 0;
        for (h = 0; h < (US)(strlen(msgbuf)); h ++ )
        {
            ch = msgbuf[h];
            ch1 = ch2;
            ch2 = ch;
            if (ch1 == '/' && ch2 == '/')
            {
                if (k > 0)
                {
                    str[j][k] = '\0';
                    j ++;
                    k --;
                }
                break;
            }
            if (ch == ',' || ch == '\0')
            {
                if (k == 0)
                {
                    continue;
                }
                else
                {
                    j ++;
                    k = 0;
                }
            }
            else
            {
                if (j < 5 && k < 50)
                {
                    str[j][k] = ch;
                    k ++;
                }
            }
        }
        if (strlen(str[0]) > 0)
        {
            AccessCodes[i]=str[0];
        }
        else
        {
            AccessCodes[i]="*";
        }
        if (strlen(str[1]) > 0)
        {
            MinLen[i] = atoi(str[1]);
        }
        else
        {
            MinLen[i] = AccessCodes[i].GetLength();
        }
        if (strlen(str[2]) > 0)
        {
            MaxLen[i] = atoi(str[2]);
        }
        else
        {
            MaxLen[i] = AccessCodes[i].GetLength();
        }
    }
  }
}
//增加接入字冠
int CRunFlw::Add_AccessCodes(const char *code, int minlen, int maxlen)
{
  for (int i = 0; i < MAX_ACCESSCODES_NUM; i ++)
  {
    if (AccessCodes[i].GetLength() == 0)
    {
      AccessCodes[i] = code;
      MinLen[i] = minlen;
      MaxLen[i] = maxlen;
      AccessNum ++;
      g_GData.AddPreCode("*", code, minlen, maxlen, FuncGroup, FuncNo, FlwNo);
      g_GData.Send_AccessNo_To_IVR(1, code, minlen, maxlen);
      return 0;
    }
  }
  return 1;
}
//刪除接入字冠
int CRunFlw::Del_AccessCodes(const char *code)
{
  for (int i = 0; i < MAX_ACCESSCODES_NUM; i ++)
  {
    if (AccessCodes[i].Compare(code) == 0)
    {
      for (int j=i; j<(MAX_ACCESSCODES_NUM-1); j++)
      {
        AccessCodes[j] = AccessCodes[j+1];
        MinLen[j] = MinLen[j+1];
        MaxLen[j] = MaxLen[j+1];
      }
      AccessCodes[MAX_ACCESSCODES_NUM-1] = _T("");
      MinLen[MAX_ACCESSCODES_NUM-1] = 0;
      MaxLen[MAX_ACCESSCODES_NUM-1] = 0;
      AccessNum --;
      g_GData.DelPreCode("*", code);
      g_GData.Send_AccessNo_To_IVR(0, code, 0, 0);
      return 0;
    }
  }
  return 1;
}
//加載所有接入字冠
void CRunFlw::Load_AccessCodes(void)
{
    //UC MsgBuf[MAX_MSG_BUF_LEN];

    for (int i = 0; i < MAX_ACCESSCODES_NUM; i ++)
    {
        if (AccessCodes[i].GetLength() > 0)
        {
            g_GData.AddPreCode("*", AccessCodes[i].C_Str(), MinLen[i], MaxLen[i], FuncGroup, FuncNo, FlwNo);
           // Send_AccessNo_To_IVR(FlwData, 1, AccessCodes[i], MinLen[i], MaxLen[i]);
        }
    }
}


//取空閑的流程序號(返回值：0-表示該流程未加載過，有空閑的流程序號 1-表示該流程加載過,有空閑的流程標志 2-表示該流程加載過,無空閑的流程標志 3-失敗)
int CRunFlwMng::Get_Idel_FlwNo(CH *FlwName, US &FlwNo, US &FlwId)
{
  int i;  
  for (i = 0; i < MAX_FLOW_NUM; i ++)
  {
    if(pRunFlws[i])
      if (pRunFlws[i]->FlwName.Compare(FlwName) == 0)
      {
          if (pRunFlws[i]->Flw[0].Onlines == 0)
          {
              FlwNo = i;
              FlwId = 0;
              return 1;
          }
          if (pRunFlws[i]->Flw[1].Onlines == 0)
          {
              FlwNo = i;
              FlwId = 1;
              return 1;
          }
          FlwNo = i;
          FlwId = 0;
          return 2;
      }
  }
  for (i = 0; i < MAX_FLOW_NUM; i ++)
  {
      if (!pRunFlws[i])
      {
          FlwNo = i;
          FlwId = 0;
          return 0;
      }
  }
  return 3;
}

bool CRunFlwMng::IsFlwAddrOK(US flwno,US flwid,US cmdaddr)
{
	if(flwno>=MAX_FLOW_NUM)
		return false;
	if(pRunFlws[flwno]==0)
		return false;
	if(flwid>=2)
		return false;
	if (cmdaddr >= pRunFlws[flwno]->Flw[flwid].TotalCmds)	
		return false;
	return true;	
}

bool  CRunFlwMng::Get_FlwNo_By_FuncNo(unsigned short FuncNo,unsigned short &FlwNo)
{
    for (int i = 0; i < MAX_FLOW_NUM; i ++)
    {
    	if (pRunFlws[i])
        if (pRunFlws[i]->FuncNo == FuncNo)
        {
            FlwNo = (unsigned short)i;
            return true;
        }
    }
    return false;
}

   //根據調用的外部流程文件名(不包括文件擴展名)查找流程序號
bool CRunFlwMng::FindFlowNoByName(const char *extvxml,US &SubFlwNo)
{	
    for (int i = 0; i < MAX_FLOW_NUM; i ++)
    {
      if (pRunFlws[i])
        if (pRunFlws[i]->OpenId)
        {
            if (pRunFlws[i]->FlwName.CompareNoCase(extvxml) == 0)
            {
                //找到外部流程
                SubFlwNo = i;
                return true;
            }
        }
    }
    return false;
}    

void CRunFlwMng::Send_AccessNo_To_IVR()
{
	for (int i = 0; i < MAX_FLOW_NUM; i ++)
  {
    if (pRunFlws[i])
    {
      for (int j = 0; j < MAX_ACCESSCODES_NUM; j ++)
      {
          if (pRunFlws[i]->AccessCodes[j].GetLength() > 0)
          {
              g_GData.Send_AccessNo_To_IVR(1, pRunFlws[i]->AccessCodes[j].C_Str(), pRunFlws[i]->MinLen[j], pRunFlws[i]->MaxLen[j]);
          }
      }
    }
  }
}    

//根據配置文件自動加載流程
void CRunFlwMng::AutoLoadFlw(const char *filename)
{
  char file[64];
  char ext[32];
  US flwnum, FlwNo, FlwId;
  char temp[32], FlwFile[MAX_FILENAME_LEN], FilePath[MAX_PATH_LEN], dt[30];

  flwnum = GetPrivateProfileInt("FLW", "FLWNUM", 0, filename);
  for (int i = 0; i < MAX_FLOW_NUM && i < flwnum; i ++)
  {
    sprintf(temp, "FLW(%d)", i );
    GetPrivateProfileString(temp, "FlwFile", "", FlwFile, MAX_FILENAME_LEN, filename);
    
    if (strlen(FlwFile) >= 5)
    {
      sprintf(FilePath, "%s\\%s", g_szFlwPath, FlwFile);
    
      if (MyCheckFileExist(FilePath) == 0) 
      {
        MyTrace(0, "FlwFile=%s not exist", FilePath);
        continue;
      }
      splitnamext(FlwFile, file, ext);
         			        
      strcpy(dt, MyGetNow());
      if (Get_Idel_FlwNo(file, FlwNo, FlwId) == 0)
      {
        CRunFlw   *pRunFlw=new CRunFlw(FlwNo);                  
        if (pRunFlw->Flw[FlwId].Load_Flw_From_Obj(FilePath) == 0)
        {
          pRunFlws[FlwNo]=pRunFlw; 
      
          pRunFlw->FlwName=file;
          pRunFlw->OpenId = 1;
          pRunFlw->FlwId = FlwId;
          pRunFlw->LoadCount ++;
          pRunFlw->Read_Ini_File(filename,temp);
          pRunFlw->Load_AccessCodes();
          ErrVar=pRunFlw->AccessCodes[0];
	        for(unsigned int j=1;j<MAX_ACCESSCODES_NUM;j++) //連接所有參數
	        {
		        if (pRunFlw->AccessCodes[j].GetLength() > 0)
            {
              ErrVar+=',';
		          ErrVar+=pRunFlw->AccessCodes[j];
            }
	        }
          strcpy(pRunFlw->LoadTime, MyGetNow());

          MyTrace(0, "AutoLoadFlw FlwNo=%d FlwName=%s FuncGroup=%d FuncNo=%d TotalCmds=%d",
            pRunFlw->FlwNo, pRunFlw->FlwName.C_Str(), 
            pRunFlw->FuncGroup, pRunFlw->FuncNo, 
            pRunFlw->Flw[pRunFlw->FlwId].TotalCmds);
        }
        else
        {
          delete pRunFlw;
          //printf("Failed!\n");                  	
        }
      }
    }
  }
}
//加載單個流程
int CRunFlwMng::AutoLoadFlw(const char *FilePath, int groupno, int funcno, int &rtFlwNo)
{
  CRunFlw   *pRunFlw;
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];
  US FlwNo, FlwId;
  int getid;

  if (MyCheckFileExist(FilePath) == 0) 
    return 1;
         			    
  _splitpath(FilePath, drive, dir, file, ext);
  getid = Get_Idel_FlwNo(file, FlwNo, FlwId);
  MyTrace(5, "AutoLoadFlw FilePath=%s funcno=%d Get_Idel_FlwNo FlwNo=%d FlwId=%d getid=%d",
    FilePath, funcno, FlwNo, FlwId, getid);
  if (getid == 0 || getid == 1)
  {
    if (getid == 0)
    {
      pRunFlw=new CRunFlw(FlwNo);
      if (pRunFlw == NULL)
        return 1;
      pRunFlw->FuncGroup = groupno;
      pRunFlw->FuncNo = funcno;
    }
    else
      pRunFlw = pRunFlws[FlwNo];

    if (pRunFlw->Flw[FlwId].Load_Flw_From_Obj(FilePath) == 0)
    {
      if (getid == 0)
        pRunFlws[FlwNo]=pRunFlw;
  
      pRunFlw->FlwName=file;
      pRunFlw->OpenId = 1;
      pRunFlw->FlwId = FlwId;
      pRunFlw->LoadCount ++;
	    ErrVar=pRunFlw->AccessCodes[0];
      strcpy(pRunFlw->LoadTime, MyGetNow());
      rtFlwNo = FlwNo;
      return 0;
    }
    else
    {
      if (getid == 0)
        delete pRunFlw;
    }
  }
  return 2;
}
//重新啟動自動運行流程
void CRunFlwMng::ReRunAutoRunFlw()
{
	US NewId;
  for (int i = 0; i < MAX_FLOW_NUM; i ++)
  {
    if (pRunFlws[i])
    {
      if (pRunFlws[i]->RunAfterLoad == 1)
      {
 				NewId=g_SessionMng.Alloc_Idel_SessionId(); //分配空閑session
   			if(NewId>0)
    		{				
    			pRunFlws[i]->AutoStartId = NewId;
          g_SessionMng[NewId].Init(
						true,
						0,0,CALL_AUTO,//ChnType,ChnNo, CallInOut,
						CStringX(""),//CallerNo,
						CStringX(""),//CalledNo,
						CStringX(""),//OrgCalledNo,
						0,//DialSerialNo,
						CStringX(""),//DialParam,
						pRunFlws[i]->FlwNo,pRunFlws[i]->FlwId
						);
          g_SessionMng[NewId].DataBaseType = g_SessionMng.DataBaseType;
          g_SessionMng[NewId].SwitchType = g_SessionMng.SwitchType;
				 }
      }
    }
  }
}
void CRunFlwMng::ReRunAutoRunFlw(US flwno)
{
	US NewId;
  if (flwno >= MAX_FLOW_NUM) return;
  if (pRunFlws[flwno])
  {
 		if (pRunFlws[flwno]->RunAfterLoad > 0)
    {
      NewId=g_SessionMng.Alloc_Idel_SessionId(); //分配空閑session
      if(NewId>0)
      {				
        pRunFlws[flwno]->AutoStartId = NewId;
        g_SessionMng[NewId].Init(
          true,//Disp,等日后打開
          0,0,CALL_AUTO,//ChnType,ChnNo, CallInOut,
          CStringX(""),//CallerNo,
          CStringX(""),//CalledNo,
          CStringX(""),//OrgCalledNo,
          0,//DialSerialNo,
          CStringX(""),//DialParam,
          pRunFlws[flwno]->FlwNo,pRunFlws[flwno]->FlwId
          );
        g_SessionMng[NewId].DataBaseType = g_SessionMng.DataBaseType;
        g_SessionMng[NewId].SwitchType = g_SessionMng.SwitchType;
        //printf("ReRunAutoFlw flwno=%d flwname=%s\n", flwno, pRunFlws[flwno]->FlwName.C_Str());
      }
    }
  }
}
void CRunFlwMng::StopAutoRunFlw(US flwno)
{
  if (flwno >= MAX_FLOW_NUM) return;
  if (pRunFlws[flwno])
  {
    pRunFlws[flwno]->AutoStartId = 0;
  }
}
