//////////////////////////////////////////////////////////////////////////
//
// odsLog.cpp 
//
//		有關程序日志（使用數據庫日志和NT系統日志兩種方式）的操作
//
// by Wang Yong Gang, 1999-10-27
// 在 張軍 1998/05/31 基礎上完成
//
//////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "odsLog.h"

//-------------------------------------------------------------------
// odsAddToSystemLog			- 在 NT 的日志表中增加一條信息
//
// 參數：
//		strSourceName			- 事件源名稱
//		strMsg					- 信息內容
//		wType					- 類型，可以為以下幾種
//								- EVENTLOG_ERROR_TYPE
//								- EVENTLOG_WARNING_TYPE
//								- EVENTLOG_INFORMATION_TYPE
//								- EVENTLOG_AUDIT_SUCCESS
//								- EVENTLOG_AUDIT_FAILURE
//-------------------------------------------------------------------
void odsAddToSystemLog( LPCTSTR strSourceName, LPCTSTR strMsg, WORD wType )
{    
    HANDLE  hEventSource;
 
    // Use event logging to log the error.
    //
    hEventSource = RegisterEventSource(NULL, strSourceName);
        
    if (hEventSource != NULL) 
	{
        ReportEvent(hEventSource,	// handle of event source
            wType,					// event type
            0,						// event category
            0,						// event ID
            NULL,					// current user's SID
            1,						// strings in lpszStrings
            0,						// no bytes of raw data
            &strMsg,				// array of error strings
            NULL);					// no raw data

        DeregisterEventSource(hEventSource);       
    }
}
void odsAddToSystemLog( LPCTSTR strSourceName, WORD wType, LPCTSTR lpszMsgFormat, ... )
{    
  HANDLE  hEventSource;
  char buf[2048];
  LPCTSTR lpstrTemp;
  
  va_list		ArgList;
  va_start(ArgList, lpszMsgFormat);
  _vsnprintf(buf, 2048, lpszMsgFormat, ArgList);
  va_end (ArgList);
  lpstrTemp = (LPCTSTR)buf;
  
  // Use event logging to log the error.
  //
  hEventSource = RegisterEventSource(NULL, strSourceName);
  
  if (hEventSource != NULL) 
  {
    ReportEvent(hEventSource,	// handle of event source
      wType,					// event type
      0,						// event category
      0,						// event ID
      NULL,					// current user's SID
      1,						// strings in lpszStrings
      0,						// no bytes of raw data
      &lpstrTemp,				// array of error strings
      NULL);					// no raw data
    
    DeregisterEventSource(hEventSource);       
  }
}
