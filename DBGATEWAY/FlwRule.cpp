//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "FlwRule.h"

#include "../include/rule/intrule.cpp"
#include "../include/rule/charrule.cpp"
#include "../include/rule/strrule.cpp"
#include "../include/rule/intmacro.cpp"
#include "../include/rule/strmacro.cpp"

#include "../include/msgcpp/xmldb.cpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
CFlwRule::CFlwRule()
{
    /*
    IntRange=IntRangeArray;
    CharRange=CharRangeArray;
    StrRange=StrRangeArray;
    MacroIntRange=MacroIntRangeArray;
    MacroStrRange=MacroStrRangeArray;
    XMLDBMsgRule=XMLDBMsgRuleArray;
    */
}

CFlwRule::~CFlwRule()
{
}


