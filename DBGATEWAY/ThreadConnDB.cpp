//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ThreadConnDB.h"
#include "main.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CThreadConnDB::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall CThreadConnDB::CThreadConnDB(bool CreateSuspended)
  : TThread(CreateSuspended)
{
    Priority = tpNormal;
    FreeOnTerminate = true;
}
//---------------------------------------------------------------------------
void __fastcall CThreadConnDB::Execute()
{
  while ( !Terminated )
  {
    QuarkCallDBGW->ReConnectDBLoop();
    ::Sleep(20);
  }
}
//---------------------------------------------------------------------------
