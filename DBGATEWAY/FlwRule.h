//---------------------------------------------------------------------------
#ifndef FlwRuleH
#define FlwRuleH
//---------------------------------------------------------------------------
#include <stdio.h>
#include "boxinc.h"
#include "../include/msgh/dbxml.h"
//---------------------------------------------------------------------------
//系統關鍵字結構定義
typedef struct
{
    UC state;
    US KeyId; //關鍵字序號
    UC KeyName[MAX_VARNAME_LEN]; //關鍵字名稱
    UC KeyType; //變量作用域: 0未知 1-指令名關鍵字 2-屬性名關鍵字 3-系統變量名關鍵字 4-函數名關鍵字 5-參數值宏定義關鍵字 6-運算符關鍵字

} VXML_KEYWORDS_STRUCT;

//系統關鍵字結構定義

//---------------------------------------------------------------------------
//流程指令規則類
class CFlwRule
{
public:
    CFlwRule();
    virtual ~CFlwRule();

    /*
    UC CallerId = 0; //是否接收主叫號碼(0-無所謂 1-必須接收主叫號碼)
    UC MinCallerLen = 0; //主叫號碼最少長度(當CallerId=1時有效)
    UC MinCalledLen = 0; //被叫號碼最少長度(當CallerId=1時有效)
    //US MaxCmdNum; //實際指令規則條數

    //整數范圍規則
    const VXML_INTEGER_RANGE_RULE_STRUCT      *IntRange;//[MAX_INT_RANGE_NUM];
    //單字符范圍規則
    const VXML_CHAR_RANGE_RULE_STRUCT         *CharRange;//[MAX_CHAR_RANGE_NUM];
    //字符串中允許出現的字符規則
    const VXML_STRING_RANGE_RULE_STRUCT       *StrRange;//[MAX_STR_RANGE_NUM];
    //整數枚舉字符串規則
    const VXML_MACRO_INT_RANGE_RULE_STRUCT    *MacroIntRange;//[MAX_MACRO_INT_NUM];
    //枚舉字符串規則
    const VXML_MACRO_STR_RANGE_RULE_STRUCT    *MacroStrRange;//[MAX_MACRO_STR_NUM];
    //返回消息規則
    const VXML_SEND_MSG_CMD_RULE_STRUCT       *XMLDBMsgRule;//[MAX_RECV_MSG_NUM];
    //系統變量名
    //const VXML_RECV_MSG_CMD_RULE_STRUCT       *LOGCLIENTMsgRule;//[MAX_RECV_MSG_NUM];
    */
};
//---------------------------------------------------------------------------
#endif
