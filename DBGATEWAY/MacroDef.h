//---------------------------------------------------------------------------
#ifndef MacroDefH
#define MacroDefH
//---------------------------------------------------------------------------
//定時器單位(ms)
#define Timer_Interval          1 //1ms

//節點類型定義
#define NODE_IVR				0  //IVR服務器
#define NODE_SWT                1  //交換接入
#define NODE_XML                2  //流程解析
#define NODE_VOX                3  //語音
#define NODE_FAX                4  //傳真
#define NODE_VIP                5  //VOIP
#define NODE_CSC				6  //坐席

//數據庫類型
#define DB_NONE		            0
#define DB_SQLSERVER		    1
#define DB_ORACLE			    2
#define DB_SYBASE			    3
#define DB_ACCESS			    4
#define DB_ODBC				    5
#define DB_MYSQL			    6

//數據庫操作超時時長（毫秒）
#define DB_PROC_TIMEOUT		    15000

//流程變量類型定義
#define anyType	                0  //任意類型
#define boolType                1  //BOOL
#define intType	                2  //integer
#define floatType               3  //float
#define charType                4  //char
#define stringType              5  //string

//宏定義常量
#define english	                1  //英語
#define chinese	                2  //普通話
#define localism	            3  //地方話

//聲音宏定義
#define female	                1  //女聲
#define male	                2  //男聲
#define child					3  //童聲

//放音內容類型定義
#define PLAYTYPE_file	        1  //文件
#define PLAYTYPE_index          2  //內存索引
#define PLAYTYPE_ttsfile        3  //TTS文件
#define PLAYTYPE_ttsstr         4  //TTS字符串
#define PLAYTYPE_tone	        5  //信號音

//語音格式定義
#define pcma					1  //PCM a律格式
#define pcmmu					2  //PCM mu律格式
#define adpcm	                3  //ADPCM格式
#define wave					4  //WAVE格式
#define mp3						5  //MP3格式
#define gsm						6  //gsm格式

//信號音標志定義
#define TONE_dialtone	        1	//撥號音
#define TONE_ringtone	        2	//回鈴音
#define TONE_busytone	        3	//忙音
#define TONE_unntone	        4	//空號音
#define TONE_conftone	        5	//證實音
#define TONE_hangtone	        6	//催掛音
#define TONE_restdial	        7	//限制撥打音
#define TONE_notopen            8 //業務未開通音

#define disconn					0  //斷開連接
#define connboth	            1  //sou<->obj雙向連接
#define connfrom				2  //sou<-obj單向連接
#define connto					3  //sou->obj單向連接

#define serial					1  //順序呼叫
#define simult					2  //同時呼叫

#define blind					1  //直接轉接
#define bridge					2  //呼通后轉接
#define consultation			3  //通話后轉接

#define presider				1  //主持
#define talker					2  //加入
#define listener				3  //旁聽

#define anc						1  //應答計費
#define ann						2  //應答不計費

//信令類型
#define SS1						1  //
#define TUP						2  //
#define ISUP					3  //
#define Q931					4  //
#define SIP						5  //
#define H323					6  //
#define POT						7  //
#define CAS						8  //
#define CSTA					9  //

//信令消息類型
#define ACM						1  //
#define GRQ						2  //
#define ANN						3  //
#define ANC						4  //
#define CFL						5  //
#define SLB						6  //
#define STB						7  //
#define SEC						8  //
#define UNN						9  //

#define LEFT					1  //從字符串左開始
#define RIGHT					2  //從字符串右開始

#define yyyy					1  //年
#define mm						2  //月
#define dd						3  //日
#define hh						4  //時
#define mi						5  //分
#define ss						6  //秒
#define ms						7  //豪秒
#define wd						8  //星期

//通道類型定義
#define CHANNEL_TRUNK           1	//中繼
#define CHANNEL_SEAT            2	//坐席
#define CHANNEL_CFC             3	//會議
#define CHANNEL_VOIP            4	//VOIP
#define CHANNEL_REC             5	//錄音

//呼叫方向
#define CALL_IN                 1	//呼入
#define CALL_OUT                2	//呼出

//---------------------------------------------------------------------------
//消息事件定義
//---------------------------------------------------------------------------
//通用操作返回事件
#define OnSuccess				0x00 //成功或不存在
#define OnFail					0x01 //失敗或存在

#define OnHangon				0x09 //通道釋放事件
#define OnLinkHangon			0x0A //連接通道釋放事件
#define OnUnLink				0x0B //連接通道斷開事件
#define OnFlash				    0x0C //坐席通道拍叉簧事件

//放音收碼事件
#define OnPlaying				0x10 //正在放音
#define OnPlayEnd				0x11 //放音結束
#define OnRecvDTMF				0x12 //收到按鍵
#define OnRecvASR				0x13 //收到識別字符串
#define OnPlayError				0x14 //放音錯誤
#define OnErrDTMF				0x15 //收收到錯誤的DTMF

//TTS事件
#define OnTTSing				0x20 //正在文本轉語音操作
#define OnTTSEnd				0x21 //文本轉語音操作結束
#define OnTTSError				0x22 //文本轉語音操作錯誤

//ASR事件
#define OnASRing				0x30 //正在語音識別操作
#define OnASREnd				0x31 //語音識別結束
#define OnASRError				0x32 //語音識別錯誤

//錄音事件
#define OnRecording				0x40 //正在錄音
#define OnRecordEnd				0x41 //結束錄音
#define OnRecordError			0x42 //錄音錯誤

//呼出事件
#define OnOutCalling			0x50 //正在呼出
#define OnOutRing				0x51 //被叫振鈴
#define OnOutBusy				0x52 //被叫占線
#define OnOutTimeout			0x53 //無人應答或超時
#define OnOutUNN				0x54 //空號
#define OnOutFail				0x55 //呼叫失敗
#define OnOutAnswer				0x56 //呼出應答
#define OnTranTalk				0x57 //轉接成功通話

//坐席分配事件
#define OnACDWait			    0x60 //正在分配
#define OnACDRing			    0x61 //坐席振鈴
#define OnACDBusy				0x62 //坐席全忙
#define OnACDTimeOut			0x63 //無人應答或超時
#define OnACDAns				0x64 //坐席應答
#define OnACDFail				0x65 //分配失敗

//數據庫操作事件
#define OnDBSuccess				0x70 //數據庫操作成功
#define OnDBFail				0x71 //數據庫操作失敗
#define OnDBBOF					0x72 //到第一條記錄
#define OnDBEOF					0x73 //到最后一條記錄

//定時器事件
#define OnTimerOver			    0x80 //定時器溢出事件
#define OnTimer0Over			0x81 //定時器0溢出事件
#define OnTimer1Over			0x82 //定時器1溢出事件
#define OnTimer2Over			0x83 //定時器2溢出事件
#define OnTimer3Over			0x84 //定時器3溢出事件
#define OnTimer4Over			0x85 //定時器4溢出事件

//傳真收發事件
#define OnsFaxing			    0x90 //正在發送
#define OnsFaxend			    0x91 //發送結束
#define OnsFaxFail		        0x92 //發送失敗
#define OnrFaxing			    0x93 //正在接收
#define OnrFaxend			    0x94 //接收結束
#define OnrFaxFail		        0x95 //接收失敗

//會議操作返回事件
#define OnCfcFirst			    0xA0 //第1個加入會議
#define OnCfcJoin			    0xA1 //加入會議通話成功
#define OnCfcListen		        0xA2 //旁聽會議成功
#define OnCfcNotOpen	        0xA3 //會議未開通失敗
#define OnCfcJoinFail           0xA4 //加入會議失敗
#define OnCfcListenFail         0xA5 //旁聽會議失敗
#define OnCfcFull               0xA6 //人滿失敗

//會議室人數變化事件
#define OnCfcOne2Many           0xB0 //會議加入人數從1人變多人
#define OnCfcMany2One           0xB1 //會議加入人數從多人變1人
#define OnCfcBusy2Idle          0xB2 //會議加入人數從忙變有空位

//自定義事件
#define OnSelfEvent0            0xE0 //自定義事件0
#define OnSelfEvent1            0xE1 //自定義事件1
#define OnSelfEvent2            0xE2 //自定義事件2
#define OnSelfEvent3            0xE3 //自定義事件3
#define OnSelfEvent4            0xE4 //自定義事件4
#define OnSelfEvent5            0xE5 //自定義事件5
#define OnSelfEvent6            0xE6 //自定義事件6
#define OnSelfEvent7            0xE7 //自定義事件7
#define OnSelfEvent8            0xE8 //自定義事件8
#define OnSelfEvent9            0xE9 //自定義事件9
#define OnSelfEvent10           0xEA //自定義事件10
#define OnSelfEvent11           0xEB //自定義事件11
#define OnSelfEvent12           0xEC //自定義事件12
#define OnSelfEvent13           0xED //自定義事件13
#define OnSelfEvent14           0xEE //自定義事件14
#define OnSelfEvent15           0xEF //自定義事件15
#define OnGotoEvent             0xB0 //跳轉事件

#define WAIT_EVENT			    0xFFFF //等待返回事件
//---------------------------------------------------------------------------
//最大值定義
//---------------------------------------------------------------------------
#define MAX_NODE_XML            10  //最大XML流程解析節點數
#define MAX_NODE_VOX            8   //最大語音節點數

#define MAX_ACCESSCODES_NUM		128	//接入號碼最大個數
#define MAX_TELECODE_LEN		32	//最長的電話號碼長度

#define MAX_PATH_LEN			128	//最長的路徑長度
#define MAX_FILENAME_LEN		64	//最長的文件名長度
#define MAX_PATH_FILE_LEN		164	//全路徑文件名最大長度

#define MAX_DTMFBUF_LEN			64	//收碼緩沖最大長度
#define MAX_DTMFRULE_LEN		16	//收碼規則串最大長度
#define MAX_ASRBUF_LEN			64	//ASR緩沖最大長度
#define MAX_FSKBUF_LEN			64	//FSK緩沖最大長度
#define MAX_SMS_LEN				200	//短消息最大長度
#define MAX_COMP_LEN			200	//合成串最大長度
#define MAX_TTS_LEN				200	//文本串最大長度

#define MAX_USERNAME_LEN        32	//最長的用戶名長度
#define MAX_PASSWORD_LEN        32	//最長的密碼長度

#define MAX_ERRORBUF_LEN        1024 //最長的錯誤信息長度

#define MAX_VARNAME_LEN         25  //變量名的最長長度
#define MAX_VAR_DATA_LEN        256 //每個變量最大數據長度
#define MAX_CMD_ATTR_LEN        680 //指令屬性區最大長度
#define MAX_PRI_NUM             200 //局部變量最大地址數(范圍：通道)
#define MAX_PUB_NUM             100 //公共變量最大地址數(范圍：流程)
#define MAX_SYS_NUM             110 //全局變量最大地址數(范圍：系統)
#define MAX_FUNC_PARA_NUM       16  //函數最大參數個數
#define MAX_FUNC_NUM            50  //函數最大個數

#define MAX_MARKER_LEN          21  //通用標簽最大長度
#define MAX_CMD_LABEL_LEN       21  //每個指令行號標識最大長度
#define MAX_CMD_NAME_LEN        21  //每個指令名稱最大長度
#define MAX_ATTRIBUTE_NUM       32  //每條指令最大屬性個數
#define MAX_ATTRIBUTE_LEN       21  //屬性名最大長度
#define MAX_CMD_LINES           1000 //每個流程最大的指令行數
#define MAX_MACRO_STRING_NUM    100  //每個宏定義最大字符串個數
#define MAX_MACRO_STRING_LEN    32  //每個宏定義字符串最大長度

#define MAX_SESSION_NUM         240 //最大會話資源個數
#define MAX_CHN_NUM				240 //最大通道數
#define MAX_FLOW_NUM            64  //最大流程個數
#define MAX_CMDS_NUM            200 //最大指令個數
#define MAX_COMMENT_NUM         4   //最大注釋定義個數
#define MAX_REPLACE_NUM         16  //最大替換定義個數
#define MAX_MACRO_NUM           100 //最大宏定義個數
#define MAX_REGULA_NUM          10  //最大正則法定義個數
#define MAX_TIMER_NUM           5   //最大定時器個數

#define MAX_KEYWORDS_NUM        256//最大關鍵字數
#define MAX_INT_RANGE_NUM       16 //最大個數
#define MAX_CHAR_RANGE_NUM      16 //最大個數
#define MAX_STR_RANGE_NUM       16 //最大個數
#define MAX_MACRO_INT_NUM       64 //最大個數
#define MAX_MACRO_STR_NUM       16 //最大個數
#define MAX_RECV_MSG_NUM        128 //最大

#define MAX_EVENT_NUMS			20	//自定義事件個數

#define MAX_FLW_RETURN_LEVEL    16	//子流程調用嵌套層數

#define MAX_MSG_BUF_LEN         2048//消息最大長度

#define MAX_EXP_STRING_LEN      2048//表達式最大長度

#define MAX_SQL_BUF_NUM         256//數據庫操作最大條數
#define MAX_SQLS_LEN            512 //SQL語句最大長度

#define MAX_SQLS_BUF_LEN        512 //SQL語句緩沖最大長度

#define MAX_STRING_SPLIT_NUM    64	//字符串分離及組合最大子串數

#define MAX_BILL_FEE_LEN        250 //話單費率最大記錄條數

#define MAX_MSG_QUEUE_NUM	      1024	//最大接收消息隊列個數
//---------------------------------------------------------------------------
//系統固定變量名
#define R_NULL					50 //空值
#define R_DateTime				51 //當前日期時間YYYY-MM-DD HH-MI-SS
#define R_Date     				52 //當前日期YYYY-MM-DD
#define R_Time     				53 //當前時間HH-MI-SS
#define R_Year        			54 //當前年(yyyy)
#define R_Month       			55 //當前月(01-12)
#define R_Day         			56 //當前天(01-31)
#define R_Hour        			57 //當前時(00-23)
#define R_Minute      			58 //當前分(00-59)
#define R_Second				59 //當前秒(00-59)
#define R_Msecond     			60 //當前毫秒(00-999)
#define R_Weekday  				61 //當前星期幾(1-7)
#define R_True					62 //是
#define R_False					63 //否
#define R_Yes					64 //是
#define R_No					65 //否
#define R_AllOnLines 			66 //系統在線數
#define R_VoxPath				67 //語音文件路徑
#define R_Langue				68 //語言
#define R_Voice					69 //聲音

//流程固定變量名
#define R_SessionId				70 //當前呼叫會話業務序列號
#define R_StatusId				71 //當前呼叫會話狀態標志(0-空閑，1-占用)
#define R_CallInOut				72 //呼叫方向 0: idle, 1: in, 2: out
#define R_VxmlPoint				73 //解析器節點號
#define R_ServerType			74 //業務腳本功能服務類型
#define R_ServerNo				75 //業務腳本功能服務號(業務流程編號)
#define R_SngDevice				76 //信令控制設備號
#define R_SwtDevice				77 //交換控制設備號
#define R_VoxDevice				78 //語音控制設備號
#define R_CfcDevice				79 //會議控制設備號
#define R_ChanType 				80 //通道類型
#define R_ChanNo				81 //通道號
#define R_CallerNo				82 //主叫號碼
#define R_OrgCallerNo			83 //原主叫號碼
#define R_CalledNo				84 //被叫號碼
#define R_OrgCalledNo			85 //原被叫號碼
#define R_CallTime				86 //呼叫時間
#define R_AnsTime   			87 //應答時間
#define R_RelTime   			88 //釋放時間
#define R_Timer0				89 //定時器0
#define R_Timer1				90 //定時器1
#define R_Timer2				91 //定時器2
#define R_Timer3				92 //定時器3
#define R_Timer4				93 //定時器4
#define R_SeatNo				94 //坐席號
#define R_WorkerNo				95 //話務員工號
#define R_CmdAddr				96 //指令地址
#define R_OnLines				97 //本流程在線人數
#define R_OnEvent				98 //事件
#define R_DtmfBuf				99 //收碼緩沖
#define R_AsrBuf				100 //Asr緩沖
#define R_DialSerialNo	        101 //撥號器呼叫序列號
#define R_DialParam			    102 //撥號器呼叫參數
#define R_Error			        103 //錯誤信息

//---------------------------------------------------------------------------
//錯誤信息定義
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//指令編號
//---------------------------------------------------------------------------
#define CMD_mainform            0//主窗口定義
#define CMD_subform             1//子窗口定義
#define CMD_callsubform         2//子窗口調用
#define CMD_callextform         3//外部窗口調用
#define CMD_assign              4//變量賦值
#define CMD_getvalue            5//取其他會話通道變量值
#define CMD_answer              6//來話應答
#define CMD_sendsignal          7//發送信令
#define CMD_hangon              8//掛機釋放
#define CMD_dtmfrule            9//DTMF按鍵規則定義
#define CMD_getdtmf             10//收碼定義
#define CMD_playrule            11//放音規則定義
#define CMD_asrrule             12//ASR規則定義
#define CMD_setvolume           13//改變放音音量
#define CMD_playfile            14//單文件放音
#define CMD_setplaypos          15//改變放音指針位置
#define CMD_playfiles           16//多文件放音
#define CMD_playcompose         17//合成串放音
#define CMD_multiplay           18//多語音合成放音
#define CMD_ttsstring           19//文本串轉語音
#define CMD_ttsfile             20//文本文件轉語音
#define CMD_senddtmf            21//發送DTMF
#define CMD_sendfsk             22//發送FSK
#define CMD_playcfc             23//會議室放音
#define CMD_playtone            24//播放信號音
#define CMD_recordfile          25//錄音定義
#define CMD_recordcfc           26//會議錄音定義
#define CMD_stop                27//停止放音錄音收碼
#define CMD_callout             28//電話呼出
#define CMD_cancelcallout       29//取消呼出
#define CMD_sendcalledno        30//發送被叫號碼
#define CMD_transfer            31//電話轉接
#define CMD_acdoperator         32//分配坐席
#define CMD_cancelacd           33//取消分配坐席
#define CMD_createcfc           34//創建會議
#define CMD_destroycfc          35//釋放會議資源
#define CMD_joincfc 	        36//加入會議
#define CMD_unjoincfc   	    37//退出會議
#define CMD_link		    	38//通道交換
#define CMD_talkwith	    	39//與對方通話
#define CMD_listenfrom	    	40//監聽對方通話
#define CMD_inserttalk	    	41//強插對方通話
#define CMD_threetalk	    	42//三方通話
#define CMD_stoptalk	    	43//結束通話
#define CMD_sendfax		    	44//發送傳真
#define CMD_recvfax		    	45//接收傳真
#define CMD_checkpath	    	46//檢查文件路徑
#define CMD_createpath	    	47//創建文件路徑
#define CMD_deletepath	    	48//刪除文件路徑
#define CMD_getfilenum	    	49//取文件個數
#define CMD_getfilename	    	50//取文件名
#define CMD_renname		    	51//改文件名
#define CMD_copyfile	    	52//拷貝文件
#define CMD_deletefile	    	53//刪除文件
#define CMD_fopen		    	54//打開文本文件
#define CMD_fread		        55//讀文本文件
#define CMD_fwrite		    	56//寫文本文件
#define CMD_fclose		    	57//關閉文本文件
#define CMD_dbquery		    	58//查詢數據操作
#define CMD_dbfieldno	    	59//取數據字段操作
#define CMD_dbfirst	    	    60//首記錄
#define CMD_dbnext		    	61//下一記錄
#define CMD_dbprior		    	62//前一記錄
#define CMD_dblast		    	63//末記錄
#define CMD_dbgoto	    	    64//移動記錄指針
#define CMD_dbinsert	    	65//插入記錄操作
#define CMD_dbupdate	    	66//修改記錄操作
#define CMD_dbdelete	    	67//刪除記錄操作
#define CMD_startbill	    	68//開始計費
#define CMD_writebill	    	69//終止計費并寫話單
#define CMD_oncallin	    	70//響應電話呼入事件
#define CMD_onrecvsam	    	71//收到后續地址事件
#define CMD_onsmsin		    	72//短信呼入事件
#define CMD_catch		    	73//響應事件指令
#define CMD_onevent		    	74//響應事件指令判斷事件
#define CMD_switch		    	75//條件跳轉指令
#define CMD_case		    	76//條件跳轉指令判斷條件
#define CMD_findsessionid	    77//查找目標會話通道標志
#define CMD_goto		    	78//跳轉指令
#define CMD_return		    	79//子窗口調用結束返回
#define CMD_throw		    	80//產生事件
#define CMD_stradd		    	81//字符串相加
#define CMD_len	    	        82//取字符串長度
#define CMD_substring	    	83//取子字符串
#define CMD_trim	    	    84//去掉字符串兩頭空格
#define CMD_lefttrim	    	85//去掉字符串左空格
#define CMD_righttrim	    	86//去掉字符串右邊空格
#define CMD_left		    	87//取左字符串
#define CMD_right		    	88//取右字符串
#define CMD_strcmp		    	89//區分大小寫的字符串比較
#define CMD_strcmpi	    	    90//不區分大小寫的字符串比較
#define CMD_like	    	    91//字符串相似比較
#define CMD_lower		    	92//將字符串小寫化
#define CMD_upper		        93//將字符串大寫化
#define CMD_strfmt	    	    94//將字符填充空格
#define CMD_split		    	95//根據分隔符分隔子字符串
#define CMD_join		    	96//將數組連成一字符串，并用分隔符分開
#define CMD_strpos		    	97//搜索子字符串位置
#define CMD_getnow	    	    98//取當前日期時間函數
#define CMD_getdate		    	99//取日期函數
#define CMD_gettime		    	100//取時間函數
#define CMD_getyear		    	101//取年函數
#define CMD_getmonth    	    102//取月函數
#define CMD_getday		    	103//取日函數
#define CMD_gethour		    	104//取時函數
#define CMD_getminute	    	105//取分函數
#define CMD_getsecond	    	106//取秒函數
#define CMD_getmsecond	    	107//取毫秒函數
#define CMD_getweekday	    	108//取星期函數
#define CMD_starttimer	    	109//啟動定時器
#define CMD_isdatetime	    	110//檢查日期時間的合法性
#define CMD_getinterval	    	111//取日期或時間間隔
#define CMD_incdatetime	    	112//日期時間增量
#define CMD_indatetime	    	113//是否在時間段內
#define CMD_daysinyear	    	114//指定年有多少天
#define CMD_daysinmonth	    	115//指定月有多少天
#define CMD_isleapyear	    	116//指定年是否為閏年
#define CMD_readinifile	    	117//讀配置文件函數
#define CMD_add		    	    118//算術相加函數
#define CMD_sub		    	    119//算術相減函數
#define CMD_mul		    	    120//算術相乘函數
#define CMD_div		    	    121//算術除函數
#define CMD_mod		    	    122//算術取模函數
#define CMD_bitand		    	123//位與
#define CMD_bitor	    	    124//位或
#define CMD_bitnot	    	    125//位非
#define CMD_bitlmove    	    126//位左移
#define CMD_bitrmove	    	127//位右移
#define CMD_getbit		    	128//取位值
#define CMD_random		    	129//產生隨機數
#define CMD_abs	    	        130//取絕對值
#define CMD_pow		    	    131//乘方
#define CMD_sqrt	    	    132//平方根
#define CMD_sin		            133//正弦值
#define CMD_asin		    	134//反正弦值
#define CMD_cos		    	    135//余弦值
#define CMD_acos		    	136//反余弦值
#define CMD_tan		    	    137//正切值
#define CMD_atan		    	138//反正切值
#define CMD_max		    	    139//取最大數函數
#define CMD_min		            140//取最小數函數
#define CMD_math		    	141//算術運算表達式
#define CMD_fmtdecimal	    	142//浮點小數小數位數處理
#define CMD_isint		    	143//整數有效判斷
#define CMD_isnumeric	    	144//數值有效判斷
#define CMD_default		    	145//默認條件跳轉指令
#define CMD_dbfieldname	    	146//取數據字段操作
#define CMD_dbsp	    	    147//執行存儲過程操作
#define CMD_idiv	    	    148//執行算術整除函數操作
#define CMD_waiting	    	    149//執行條件判斷等待操作
#define CMD_dbclose	    	    150//關閉查詢的數據表
#define CMD_checkfile    	    151//檢查文件是否存在
#define CMD_clearmixer    	    152//清除放音合成緩沖區
#define CMD_addfile    	        153//增加文件到放音合成緩沖區
#define CMD_adddatetime    	    154//增加日期時間到放音合成緩沖區
#define CMD_addmoney    	    155//增加金額到放音合成緩沖區
#define CMD_addnumber    	    156//增加數值到放音合成緩沖區
#define CMD_adddigits    	    157//增加數字串到放音合成緩沖區
#define CMD_addchars    	    158//增加字符串到放音合成緩沖區
#define CMD_playmixer    	    159//播放合成緩沖區
#define CMD_addttsstr    	    160//增加TTS字符串到放音合成緩沖區
#define CMD_addttsfile    	    161//增加TTS文件到放音合成緩沖區
#define CMD_gotomainform   	    162//由子窗口直接跳轉到主窗口指令
#define CMD_md5   	            163//MD5加密指令
#define CMD_flash   	        164//模擬外線拍叉簧指令
#define CMD_dbgateway  	        165//查詢外部數據網關指令

//---------------------------------------------------------------------------
//ivr->vxml消息編碼
//---------------------------------------------------------------------------
#define MSG_onlogin		        0	//節點注冊結果
#define MSG_onsetaccessno	    1   //設置接入號碼結果
#define MSG_oncallin	        2	//電話呼入事件
#define MSG_onrecvsam	        3	//收到后續地址事件
#define MSG_onsmsin		        4	//短信呼入事件
#define MSG_oncallflw	        5	//調用指定流程
#define MSG_onanswer	        6	//來話應答結果
#define MSG_onsendsignal	    7	//發送信令結果
#define MSG_onhangon	        8	//掛機釋放結果
#define MSG_ondtmfrule	        9	//DTMF按鍵規則定義結果
#define MSG_ongetdtmf	        10	//收碼結果
#define MSG_onplayrule	        11	//放音規則定義結果
#define MSG_onasrrule	        12  //ASR規則定義結果
#define MSG_onsetvolume	        13	//改變放音音量結果
#define MSG_onplayfile	        14  //單文件放音結果
#define MSG_onsetplaypos	    15	//改變放音指針位置結果
#define MSG_onplayfiles	        16	//多文件放音結果
#define MSG_onplaycompose	    17	//合成串放音結果
#define MSG_onmultiplay	        18	//多語音合成放音結果
#define MSG_onttsstring	        19	//文本串轉語音結果
#define MSG_onttsfile	        20	//文本文件轉語音結果
#define MSG_onsenddtmf	        21	//發送DTMF結果
#define MSG_onsendfsk	        22	//發送FSK結果
#define MSG_onplaycfc	        23	//會議室放音結果
#define MSG_onplaytone	        24	//播放信號音結果
#define MSG_onrecordfile	    25	//錄音結果
#define MSG_onrecordcfc	        26	//會議錄音結果
#define MSG_onstop		        27	//停止放音錄音收碼結果
#define MSG_oncallout	        28	//電話呼出結果
#define MSG_oncancelcallout	    29	//取消呼出結果
#define MSG_onsendsam	        30	//發送被叫號碼結果
#define MSG_ontransfer	        31	//電話轉接結果
#define MSG_oncallseat	        32	//分配坐席結果
#define MSG_onstopcallseat	    33	//取消分配坐席結果
#define MSG_oncreatecfc	        34	//創建會議結果
#define MSG_ondestroycfc	    35	//釋放會議資源結果
#define MSG_onjoincfc	        36	//加入會議結果
#define MSG_onunjoincfc	        37	//退出會議結果
#define MSG_onlink		        38	//通道交換結果
#define MSG_ontalkwith	        39	//與對方通話結果
#define MSG_onlistenfrom	    40	//監聽對方通話結果
#define MSG_oninserttalk	    41	//強插對方通話結果
#define MSG_onthreetalk	        42	//三方通話結果
#define MSG_onstoptalk	        43	//結束通話結果
#define MSG_onsendfax	        44  //發送傳真結果
#define MSG_onrecvfax	        45	//接收傳真結果
#define MSG_oncheckpath	        46	//檢查文件路徑結果
#define MSG_oncreatepath	    47	//創建文件路徑結果
#define MSG_ondeletepath	    48	//刪除文件結果
#define MSG_ongetfilenum	    49	//取文件個數結果
#define MSG_ongetfilename	    50	//取文件名結果
#define MSG_onrenname	        51	//改文件名結果
#define MSG_oncopyfile	        52	//拷貝文件結果
#define MSG_ondeletefile	    53	//刪除文件結果
#define MSG_oncheckfile	        54	//檢查文件是否存在
#define MSG_onclearmixer	    55	//清除放音合成緩沖區
#define MSG_onaddfile	        56	//增加文件到放音合成緩沖區
#define MSG_onadddatetime	    57	//增加日期時間到放音合成緩沖區
#define MSG_onaddmoney	        58	//增加金額到放音合成緩沖區
#define MSG_onaddnumber	        59	//增加數值到放音合成緩沖區
#define MSG_onadddigits	        60	//增加數字串到放音合成緩沖區
#define MSG_onaddchars	        61	//增加字符串到放音合成緩沖區
#define MSG_onplaymixer	        62	//播放合成緩沖區
#define MSG_onaddttsstr	        63	//增加TTS字符串到放音合成緩沖區
#define MSG_onaddttsfile        64	//增加TTS字符串到放音合成緩沖區
#define MSG_onplay              65	//通用放音結果
#define MSG_onlinkevent         66	//通道交換對方掛機、斷開事件
#define MSG_onrecvevent         67	//收到消息證實事件
#define MSG_ondialout           68	//撥號器呼出返回事件
#define MSG_onflash             69	//模擬外線拍叉簧結果
//start數據庫服務器新加指令   2006-07-03 Edit by zgj
#define MSG_ondbgateway         70	//查詢外部數據網關結果
#define MSG_ondbquery           71	//查詢數據表操作結果
#define MSG_ondbfield           72	//取數據字段值操作結果
#define MSG_ondbmove            73	//記錄指針移動結果
#define MSG_ondbcmd             74	//插入、修改、刪除記錄結果
//end數據庫服務器新加指令   2006-07-03 Edit by zgj

//vxml->ivr消息編碼
#define MSG_login		        0	//節點注冊
#define MSG_setaccessno	        1	//設置接入號碼
#define MSG_answer		        2	//來話應答
#define MSG_sendsignal	        3	//發送信令
#define MSG_hangon		        4	//掛機釋放
#define MSG_dtmfrule	        5	//DTMF按鍵規則定義
#define MSG_getdtmf		        6	//收碼結果
#define MSG_playrule	        7	//放音規則定義
#define MSG_asrrule		        8	//ASR規則定義
#define MSG_setvolume	        9	//改變放音音量
#define MSG_playfile	        10	//單文件放音結果
#define MSG_setplaypos	        11	//改變放音指針位置
#define MSG_playfiles	        12	//多文件放音
#define MSG_playcompose	        13	//合成串放音
#define MSG_multiplay	        14	//多語音合成放音
#define MSG_ttsstring	        15	//文本串轉語音
#define MSG_ttsfile		        16	//文本文件轉語音
#define MSG_senddtmf	        17	//發送DTMF
#define MSG_sendfsk		        18	//發送FSK
#define MSG_playcfc		        19	//會議室放音
#define MSG_playtone	        20	//播放信號音
#define MSG_recordfile	        21	//錄音
#define MSG_recordcfc	        22	//會議錄音
#define MSG_stop		        23	//停止放音錄音收碼
#define MSG_callout		        24	//電話呼出
#define MSG_cancelcallout	    25	//取消呼出
#define MSG_sendcalledno	    26	//發送被叫號碼
#define MSG_transfer	        27	//電話轉接
#define MSG_acdoperator	        28	//分配坐席
#define MSG_cancelacd	        29	//取消分配坐席
#define MSG_createcfc	        30	//創建會議
#define MSG_destroycfc	        31	//釋放會議資源
#define MSG_joincfc		        32	//加入會議
#define MSG_unjoincfc	        33	//退出會議
#define MSG_link		        34	//通道交換
#define MSG_talkwith	        35	//與對方通話
#define MSG_listenfrom	        36	//監聽對方通話
#define MSG_inserttalk	        37	//強插對方通話
#define MSG_threetalk	        38	//三方通話
#define MSG_stoptalk	        39	//結束通話
#define MSG_sendfax		        40	//發送傳真
#define MSG_recvfax		        41	//接收傳真
#define MSG_checkpath	        42	//檢查文件路徑
#define MSG_createpath	        43	//創建文件路徑
#define MSG_deletepath	        44  //刪除文件路徑
#define MSG_getfilenum	        45	//取文件個數
#define MSG_getfilename	        46	//取文件名
#define MSG_renname		        47	//改文件名結果
#define MSG_copyfile	        48	//拷貝文件結果
#define MSG_deletefile	        49	//刪除文件結果
#define MSG_checkfile	        50	//檢查文件
#define MSG_clearmixer	        51	//清除放音合成緩沖區
#define MSG_addfile				52	//增加文件到放音合成緩沖區
#define MSG_adddatetime	        53	//增加日期時間到放音合成緩沖區
#define MSG_addmoney	        54	//增加金額到放音合成緩沖區
#define MSG_addnumber	        55	//增加數值到放音合成緩沖區
#define MSG_adddigits	        56	//增加數字串到放音合成緩沖區
#define MSG_addchars	        57	//增加字符串到放音合成緩沖區
#define MSG_playmixer	        58	//播放合成緩沖區
#define MSG_addttsstr	        59	//增加TTS字符串到放音合成緩沖區
#define MSG_addttsfile          60	//增加TTS文件到放音合成緩沖區
#define MSG_dialout             61	//撥號器呼出
#define MSG_flash               62	//模擬外線拍叉簧
//start數據庫服務器新加指令   2006-07-03 Edit by zgj
#define MSG_dbgateway           63	//查詢外部數據網關
#define MSG_dbquery             64	//查詢數據操作
#define MSG_dbfieldno           65	//根據字段序號取字段值操作
#define MSG_dbfieldname         66	//根據字段名取字段值操作
#define MSG_dbfirst             67	//記錄指針移到首記錄
#define MSG_dbnext              68	//記錄指針移到下一記錄
#define MSG_dbprior             69	//記錄指針移到前一記錄
#define MSG_dblast              70	//記錄指針移到末記錄
#define MSG_dbgoto              71	//記錄指針移到指定記錄
#define MSG_dbinsert            72	//插入記錄操作
#define MSG_dbupdate            73	//修改記錄操作
#define MSG_dbdelete            74	//刪除記錄操作
#define MSG_writebill           75	//寫計費話單
#define MSG_dbsp                76	//執行存儲過程操作
#define MSG_dbclose             77	//關閉查詢的數據表
//end數據庫服務器新加指令   2006-07-03 Edit by zgj

//函數標志定義
#define FUNC_len		    0	//取字符串長度
#define FUNC_substring	    1   //取子字符串
#define FUNC_trim	        2	//去掉字符串兩頭空格
#define FUNC_lefttrim       3	//去掉字符串左空格
#define FUNC_righttrim      4	//去掉字符串右邊空格
#define FUNC_left	        5	//取左字符串
#define FUNC_right	        6	//取右字符串
#define FUNC_lower	        7	//將字符串小寫化
#define FUNC_upper	        8	//將字符串大寫化
#define FUNC_strfmt	        9	//將字符填充空格
#define FUNC_getnow	        10	//取當前日期時間函數
#define FUNC_getdate	    11	//取日期函數
#define FUNC_gettime	    12  //取時間函數
#define FUNC_getyear	    13	//取年函數
#define FUNC_getmonth	    14  //取月函數
#define FUNC_getday	        15	//取日函數
#define FUNC_gethour	    16	//取時函數
#define FUNC_getminute	    17	//取分函數
#define FUNC_getsecond	    18	//取秒函數
#define FUNC_getmsecond	    19	//取毫秒函數
#define FUNC_getweekday	    20	//取星期函數

//---------------------------------------------------------------------------
#endif

