//---------------------------------------------------------------------------

#ifndef ThreadProcTCPH
#define ThreadProcTCPH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class CThreadProcTCP : public TThread
{            
private:
protected:
  void __fastcall Execute();
public:
  __fastcall CThreadProcTCP(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
