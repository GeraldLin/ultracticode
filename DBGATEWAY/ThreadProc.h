//---------------------------------------------------------------------------

#ifndef ThreadProcH
#define ThreadProcH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include "FlwData.h"
//---------------------------------------------------------------------------
class CThreadProc : public TThread
{            
private:
protected:
    void __fastcall Execute();
public:
    __fastcall CThreadProc(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
