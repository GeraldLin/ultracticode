//---------------------------------------------------------------------------

#ifndef MsgProcH
#define MsgProcH
//---------------------------------------------------------------------------
#include "main.h"
#include <stdio.h>
#include "FlwRule.h"
#include "FlwData.h"
#include "TcpServer.h"

//---------------------------------------------------------------------------
//整數字符串有效判斷(0-有效的整數字符串、并返回具體數值，1-超出范圍，2-無效的整數字符串)
int Check_Int_Str(CH *ValueStr, int &IntValue);
int Check_long_Str(CH *ValueStr, long &IntValue);
//整數轉字符串
int Int_To_Str( int IntValue, UC *ValueStr );
//浮點數字符串有效判斷
int Check_Float_Str( UC *ValueStr, double &FloatValue );
//浮點數轉字符串
int Float_To_Str( int IntValue, UC *ValueStr );
//根據分離字符分割字符串
int SplitString(const char *txtline, const char splitchar, int maxnum, CString *arrstring);
//根據字符串名取參數類型
TFieldType GetFieldTypeByString(const char *fieldtype);
//根據字符串名取參數輸出方向
TParameterDirection GetDirectionByString(const char *directionstr);
//設置存儲過程參數
void CreateParameters(const char *params, TADOStoredProc *sdosp);
//---------------------------------------------------------------------------
//增加XML消息頭名稱
void Add_XML_Msg_Header(UC *MsgBuf, US MsgId, UC *MsgName, UL SerialNo, UL CurrCmdAddr, UC ChnType, US ChnNo);
//增加XML消息屬性內容
void Add_XML_Msg_Attr(UC *MsgBuf, UC *AttrName, UC *AttrValue);
//增加XML消息尾
void Add_XML_Msg_Trail(UC *MsgBuf);

void Get_Chn_Data(CXMLRcvMsg &XMLMsg, UL &SessionId, UL &CmdAddr, UC &ChnType, US &ChnNo);
//---------------------------------------------------------------------------
//發送消息
void SendMessage(US ClientId, US MsgId, long MsgLen, UC *msg);
void Send_GWQuery_Result(CXMLRcvMsg &XMLMsg, US Result, CH *ErrorBuf);
//發送查詢數據表結果消息
void Send_DBQuery_Result(US SqlBufId);
void Send_DBQuery_Result(CXMLRcvMsg &XMLMsg, int records, int fields, US Result, CH *ErrorBuf);
//發送查詢數據表記錄擴展指令消息
void Send_DBSelect_Result(US SqlBufId, CH *ValueList);
void Send_DBSelect_FailResult(US SqlBufId, CH *ErrorBuf);
void Send_DBSelect_Result(CXMLRcvMsg &XMLMsg, int records, int fields, CH *ValueList, US Result, CH *ErrorBuf);
//發送取字段值結果消息
void Send_DBField_Value(US SqlBufId);
void Send_DBField_Value(CXMLRcvMsg &XMLMsg, CH *Value, US Result, CH *ErrorBuf);
//發送移動記錄指針結果消息
void Send_DBMove_Result(US SqlBufId);
void Send_DBMove_Result(CXMLRcvMsg &XMLMsg, int RecdNo, US Result, CH *ErrorBuf);
//發送插入、修改、刪除記錄結果消息
void Send_DBCmd_Result(US SqlBufId);
void Send_DBCmd_Result(CXMLRcvMsg &XMLMsg, US Result, CH *ErrorBuf);
//發送寫話單結果
void Send_WriteBill_Result(US SqlBufId);
void Send_WriteBill_Result(CXMLRcvMsg &XMLMsg, double tollfee, US Result, CH *ErrorBuf);
//發送通話時長結果
void Send_TollTime_Result(CXMLRcvMsg &XMLMsg, int tolltime, US Result, CH *ErrorBuf);
//發送讀數據表當前記錄字段值結果
void Send_DBRecord_Result(CXMLRcvMsg &XMLMsg, int RecdNo, CH *ValueList, US Result, CH *ErrorBuf);
//發送發送執行存儲過程操作結果消息
void Send_DBSP_Result(US SqlBufId);
void Send_DBSP_Result(CXMLRcvMsg &XMLMsg, US Result, CH *ErrorBuf);
//發送取存儲過程輸出參數值結果
void Send_DBSPoutparam_Result(CXMLRcvMsg &XMLMsg, CH *ValueList, US Result, CH *ErrorBuf);
//發送將字段值存入文件結果消息
void Send_DBFieldToFile_Result(US SqlBufId, CH *Value);
void Send_DBFieldToFile_Result(CXMLRcvMsg &XMLMsg, CH *Value, US Result, CH *ErrorBuf);
//發送將文件內容存入數據表結果消息
void Send_DBFileToField_Result(US SqlBufId, CH *Value);
void Send_DBFileToField_Result(CXMLRcvMsg &XMLMsg, CH *Value, US Result, CH *ErrorBuf);
//外部web界面發來的http請求事件
void Send_WEBRequestEvent(const CH *Command, const CH *CommaText);
//發送連接數據庫失敗消息到IVR
void Send_ConnectDBFail(int nMsgType);
//---------------------------------------------------------------------------
//組合類型、編號
US CombineID(UC IdType, UC IdNo);
//分解類型、編號
void DisCombineID(US ID, UC &IdType, UC &IdNo);
//---------------------------------------------------------------------------
void ProcQueue(void);
//---------------------------------------------------------------------------
//處理查詢數據操作
int Proc_MSG_gwquery(CXMLRcvMsg &XMLMsg);
//查詢數據操作
int Proc_MSG_dbquery(CXMLRcvMsg &XMLMsg);
//根據字段序號取字段值操作
int Proc_MSG_dbfieldno(CXMLRcvMsg &XMLMsg);
//根據字段名取字段值操作
int Proc_MSG_dbfieldname(CXMLRcvMsg &XMLMsg);
//記錄指針移到首記錄
int Proc_MSG_dbfirst(CXMLRcvMsg &XMLMsg);
//記錄指針移到下一記錄
int Proc_MSG_dbnext(CXMLRcvMsg &XMLMsg);
//記錄指針移到前一記錄
int Proc_MSG_dbprior(CXMLRcvMsg &XMLMsg);
//記錄指針移到末記錄
int Proc_MSG_dblast(CXMLRcvMsg &XMLMsg);
//記錄指針移到指定記錄
int Proc_MSG_dbgoto(CXMLRcvMsg &XMLMsg);
//插入記錄操作
int Proc_MSG_dbinsert(CXMLRcvMsg &XMLMsg);
//修改記錄操作
int Proc_MSG_dbupdate(CXMLRcvMsg &XMLMsg);
//刪除記錄操作
int Proc_MSG_dbdelete(CXMLRcvMsg &XMLMsg);
//寫計費話單
int Proc_MSG_writebill(CXMLRcvMsg &XMLMsg);
//執行存儲過程操作
int Proc_MSG_dbsp(CXMLRcvMsg &XMLMsg);
//關閉查詢的數據表
int Proc_MSG_dbclose(CXMLRcvMsg &XMLMsg);
//根據余額取最大通話時長
int Proc_MSG_gettolltime(CXMLRcvMsg &XMLMsg);
//讀數據表當前記錄
int Proc_MSG_dbrecord(CXMLRcvMsg &XMLMsg);
//取存儲過程輸出參數值
int Proc_MSG_dbspoutparam(CXMLRcvMsg &XMLMsg);
//執行SQL語句
int Proc_MSG_execsql(CXMLRcvMsg &XMLMsg);
//取查詢的字段值并存放到指定的文件
int Proc_MSG_dbfieldtofile(CXMLRcvMsg &XMLMsg);
//將文件內容存入數據表
int Proc_MSG_dbfiletofield(CXMLRcvMsg &XMLMsg);
//插入記錄操作（針對長度>500的sql語句）
int Proc_MSG_dbinsertex(CXMLRcvMsg &XMLMsg);
//修改記錄操作（針對長度>500的sql語句）
int Proc_MSG_dbupdateex(CXMLRcvMsg &XMLMsg);
//查詢數據操作（針對長度>500的sql語句）
int Proc_MSG_dbqueryex(CXMLRcvMsg &XMLMsg);
//查詢數據表記錄擴展指令
int Proc_MSG_dbselect(CXMLRcvMsg &XMLMsg);

int Proc_LOGMSG_login(int nLOG, CXMLRcvMsg &LOGMsg); //登錄
int Proc_LOGMSG_modifyiniparam(CXMLRcvMsg &LOGMsg);	//修改INI配置文件參數
//-----------------------------------------------------------------------------
#endif


