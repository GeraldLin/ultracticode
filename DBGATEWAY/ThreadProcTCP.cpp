//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ThreadProcTCP.h"
#include "main.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CThreadProcTCP::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall CThreadProcTCP::CThreadProcTCP(bool CreateSuspended)
  : TThread(CreateSuspended)
{
    Priority = tpNormal;
    FreeOnTerminate = true;
}
//---------------------------------------------------------------------------
void __fastcall CThreadProcTCP::Execute()
{
  while ( !Terminated )
  {
    QuarkCallDBGW->MainLoop();
    ::Sleep(10);
  }
}
//---------------------------------------------------------------------------
