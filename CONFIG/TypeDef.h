//---------------------------------------------------------------------------
#ifndef TypeDefH
#define TypeDefH
//---------------------------------------------------------------------------

/*#define ENUM                    enum
#define UNION                   union
#define STRUCT                  struct
#define VOID                    void
#define	EXTERN	                extern
#define	STATIC	                static
#define CONST	                const
#define TYPEDEF	                typedef
#define BOOL	                bool*/

#define CH		                char
//#define SC                      signed char
#define UC		                unsigned char
#define US		                unsigned short
#define UI		                unsigned int
#define SI                      signed short
#define UL                      unsigned long
#define DF                      double float

// WINDOWS自定义消息编号
#define WM_USER_LOGIN           WM_USER+100
#define WM_USER_RECV            WM_USER+101
#define WM_USER_CMD             WM_USER+102
#define WM_USER_CLOSE           WM_USER+103
//---------------------------------------------------------------------------
#endif

 