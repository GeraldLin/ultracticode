//---------------------------------------------------------------------------

#ifndef msgfifoH
#define msgfifoH

#include <stdio.h>
#include <system.hpp>

//---------------------------------------------------------------------------
//接收消息缓冲结构
typedef struct
{
  unsigned short ClientId;
	unsigned short MsgId; //指令编号
  char MsgBuf[2048];

}VXML_RECV_MSG_STRUCT;

class CMsgfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_RECV_MSG_STRUCT      *pMsgs;

public:
	CMsgfifo(unsigned short len)
	{
		Head=0;
		Tail=0;
		resv=0;
		Len=len;
		pMsgs=new VXML_RECV_MSG_STRUCT[Len];
	}
	
	~CMsgfifo()
	{
		delete []pMsgs;
	}
	
	bool	IsFull()
	{
		unsigned short temp=Tail+1;
		return temp==Len?Head==0:temp==Head;
	}
	bool 	IsEmpty()
	{
		return Head==Tail;
	}
  VXML_RECV_MSG_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
  	if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(unsigned short ClientId, unsigned short MsgId, const char *MsgBuf);
  unsigned short  Read(unsigned short &ClientId, unsigned short &MsgId, char *MsgBuf);
  unsigned short  Read(VXML_RECV_MSG_STRUCT *RecvMsg);
};

//---------------------------------------------------------------------------
#endif
