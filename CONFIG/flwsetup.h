//---------------------------------------------------------------------------

#ifndef flwsetupH
#define flwsetupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormFlw : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TEdit *editFlwFile;
  TCSpinEdit *cseFuncNo;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editExplain;
  TLabel *Label5;
  TLabel *Label6;
  TComboBox *cbRunAfterLoad;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall editFlwFileChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormFlw(TComponent* Owner);

  bool bModify;
  void SetModifyType(bool bmodify, AnsiString flwfile="", AnsiString explain="", int funcno=0, int runafterload=0);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormFlw *FormFlw;
//---------------------------------------------------------------------------
#endif
