//---------------------------------------------------------------------------

#ifndef selpathH
#define selpathH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <FileCtrl.hpp>
//---------------------------------------------------------------------------
class TFormPath : public TForm
{
__published:	// IDE-managed Components
  TDriveComboBox *DriveComboBox1;
  TDirectoryListBox *DirectoryListBox1;
  TLabel *Label1;
  TLabel *Label2;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormPath(TComponent* Owner);

  int Id;
  AnsiString SelPath;
  void Setpath(int id, AnsiString path);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormPath *FormPath;
//---------------------------------------------------------------------------
#endif
