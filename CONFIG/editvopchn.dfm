object FormEditVopChn: TFormEditVopChn
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' IVR'#36890#36947#21443#25976#35373#32622
  ClientHeight = 221
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 68
    Top = 22
    Width = 60
    Height = 12
    Caption = #31680#40670#32232#34399#65306
  end
  object Label2: TLabel
    Left = 68
    Top = 49
    Width = 60
    Height = 12
    Caption = #36890#36947#24207#34399#65306
  end
  object Label3: TLabel
    Left = 68
    Top = 75
    Width = 60
    Height = 12
    Caption = #36890#36947#39006#22411#65306
  end
  object Label4: TLabel
    Left = 18
    Top = 101
    Width = 108
    Height = 12
    Caption = #36899#25509#30340#20132#25563#27231#36890#36947#65306
  end
  object Label6: TLabel
    Left = 44
    Top = 153
    Width = 84
    Height = 12
    Caption = #25209#37327#22686#21152#25976#37327#65306
  end
  object editVopNo: TEdit
    Left = 139
    Top = 17
    Width = 131
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 0
    Text = '0'
  end
  object editVopChnNo: TEdit
    Left = 139
    Top = 43
    Width = 131
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 1
    Text = '0'
  end
  object cbVopChnType: TComboBox
    Left = 139
    Top = 69
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 2
    Text = 'IVR'#36890#36947
    OnChange = cbVopChnTypeChange
    OnKeyPress = cbVopChnTypeKeyPress
    Items.Strings = (
      'IVR'#36890#36947
      #20659#30495#36890#36947
      #39006#27604#35441#27231#24231#24109#30435#37636
      #25976#20301#35441#27231#24231#24109#30435#37636
      'IP'#24231#24109#30435#37636
      #39006#27604#20013#32380#30435#37636
      #25976#20301#20013#32380#30435#37636
      #35486#38899#20013#32380)
  end
  object cbPortID: TComboBox
    Left = 139
    Top = 95
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 3
    OnChange = cbPortIDChange
    OnKeyPress = cbVopChnTypeKeyPress
  end
  object Button1: TButton
    Left = 35
    Top = 182
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 191
    Top = 182
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 5
    OnClick = Button2Click
  end
  object ckBatch: TCheckBox
    Left = 139
    Top = 121
    Width = 113
    Height = 19
    Caption = #26159#21542#25209#37327#22686#21152#65311
    TabOrder = 6
    OnClick = ckBatchClick
  end
  object cseBatchNum: TCSpinEdit
    Left = 139
    Top = 147
    Width = 131
    Height = 21
    MaxValue = 1024
    MinValue = 1
    TabOrder = 7
    Value = 1
  end
end
