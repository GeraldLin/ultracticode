//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("main.cpp", FormMain);
USEFORM("selpath.cpp", FormPath);
USEFORM("flwsetup.cpp", FormFlw);
USEFORM("setupacc.cpp", FormAccCode);
USEFORM("route.cpp", FormRoute);
USEFORM("seat.cpp", FormSeat);
USEFORM("pstn.cpp", FormPSTN);
USEFORM("extseat.cpp", FormExtSeat);
USEFORM("addcode.cpp", FormAddCode);
USEFORM("login.cpp", FormLogin);
USEFORM("modifypsw.cpp", FormModifyPSW);
USEFORM("editswitchport.cpp", FormEditSwitchPort);
USEFORM("editvopchn.cpp", FormEditVopChn);
USEFORM("workeralartset.cpp", FormWorkerAlartSet);
int gLangID;
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  try
  {
     Application->Initialize();
      HANDLE hMap=CreateFileMapping((HANDLE)0xFFFFFFFF,NULL,PAGE_READWRITE,0,128,"QuarkCallConfig1");
      if (hMap==NULL)
      {
          MessageBox(NULL,"Create File Map!!!","Warning",MB_OK|MB_ICONWARNING);
          return FALSE;
      }
      else
      if (GetLastError()==ERROR_ALREADY_EXISTS)
      {
          LPVOID lpMem=MapViewOfFile(hMap,FILE_MAP_WRITE,0,0,0);
          UnmapViewOfFile(lpMem);
          CloseHandle(hMap);
          MessageBox(NULL,"QuarkCallConfig is already running","Warning",MB_OK|MB_ICONWARNING);
          return FALSE;
      }
      else
      {
          LPVOID lpMem=MapViewOfFile(hMap,FILE_MAP_WRITE,0,0,0);
          strcpy((char *)lpMem,"QuarkCallConfig is already running");
          UnmapViewOfFile(lpMem);
      }
      LANGID seatlangid = GetSystemDefaultLangID();
      if (seatlangid == 0x0804)
      {
        gLangID = 1; //简体中文
      }
      else if (seatlangid == 0x0404 || seatlangid == 0x0c04 || seatlangid == 0x1004)
      {
        gLangID = 2; //繁体中文
      }
      else
      {
        gLangID = 1; //简体中文
      }
     if (gLangID == 1)
      Application->Title = "参数配置";
     else
      Application->Title = "把计砞﹚";

     Application->CreateForm(__classid(TFormMain), &FormMain);
     Application->CreateForm(__classid(TFormPath), &FormPath);
     Application->CreateForm(__classid(TFormFlw), &FormFlw);
     Application->CreateForm(__classid(TFormAccCode), &FormAccCode);
     Application->CreateForm(__classid(TFormRoute), &FormRoute);
     Application->CreateForm(__classid(TFormSeat), &FormSeat);
     Application->CreateForm(__classid(TFormPSTN), &FormPSTN);
     Application->CreateForm(__classid(TFormExtSeat), &FormExtSeat);
     Application->CreateForm(__classid(TFormAddCode), &FormAddCode);
     Application->CreateForm(__classid(TFormLogin), &FormLogin);
     Application->CreateForm(__classid(TFormModifyPSW), &FormModifyPSW);
     Application->CreateForm(__classid(TFormEditSwitchPort), &FormEditSwitchPort);
     Application->CreateForm(__classid(TFormEditVopChn), &FormEditVopChn);
     Application->CreateForm(__classid(TFormWorkerAlartSet), &FormWorkerAlartSet);
     Application->Run();
  }
  catch (Exception &exception)
  {
     Application->ShowException(&exception);
  }
  catch (...)
  {
     try
     {
       throw Exception("");
     }
     catch (Exception &exception)
     {
       Application->ShowException(&exception);
     }
  }
  return 0;
}
//---------------------------------------------------------------------------
