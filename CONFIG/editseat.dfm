object FormEditSeat: TFormEditSeat
  Left = 192
  Top = 114
  Width = 457
  Height = 294
  Caption = #20462#25913#22352#24109#21442#25968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 102
    Top = 19
    Width = 95
    Height = 13
    AutoSize = False
    Caption = #20869#32447#31471#21475#24207#21495':'
  end
  object Label2: TLabel
    Left = 114
    Top = 43
    Width = 82
    Height = 13
    AutoSize = False
    Caption = #22352#24109#20998#26426#21495':'
  end
  object Label3: TLabel
    Left = 126
    Top = 66
    Width = 72
    Height = 13
    AutoSize = False
    Caption = #22352#24109#31867#22411':'
  end
  object Label4: TLabel
    Left = 102
    Top = 92
    Width = 100
    Height = 13
    AutoSize = False
    Caption = #40664#35748#22352#24109#32452#21495':'
  end
  object Label5: TLabel
    Left = 90
    Top = 115
    Width = 118
    Height = 13
    AutoSize = False
    Caption = #40664#35748#35805#21153#21592#32452#21495':'
  end
  object Label6: TLabel
    Left = 90
    Top = 140
    Width = 111
    Height = 13
    AutoSize = False
    Caption = #40664#35748#35805#21153#21592#32423#21035':'
  end
  object Label7: TLabel
    Left = 90
    Top = 163
    Width = 110
    Height = 13
    AutoSize = False
    Caption = #40664#35748#35805#21153#21592#24037#21495':'
  end
  object Label8: TLabel
    Left = 16
    Top = 232
    Width = 425
    Height = 13
    AutoSize = False
    Caption = #27880': '#40664#35748#35805#21153#21592#24037#21495#20026#31354#26102','#34920#31034#31995#32479#36816#34892#21518#27809#26377#40664#35748#30340#35805#21153#21592#30331#38470'.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
  end
  object editPort: TEdit
    Left = 208
    Top = 16
    Width = 121
    Height = 20
    Color = clGrayText
    Enabled = False
    TabOrder = 0
    Text = '0'
  end
  object editSeatNo: TEdit
    Left = 208
    Top = 40
    Width = 121
    Height = 20
    TabOrder = 1
  end
  object cbSeatType: TComboBox
    Left = 208
    Top = 64
    Width = 121
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Text = #20869#32447#30005#35805#24231#24109
    OnKeyPress = cbSeatTypeKeyPress
    Items.Strings = (
      #20869#32447#30005#35805#24231#24109
      #20869#32447#30005#33041#24231#24109)
  end
  object cseSeatGroupNo: TCSpinEdit
    Left = 208
    Top = 88
    Width = 121
    Height = 22
    MaxValue = 31
    TabOrder = 3
  end
  object cseWorkerGroupNo: TCSpinEdit
    Left = 208
    Top = 112
    Width = 121
    Height = 22
    MaxValue = 31
    TabOrder = 4
  end
  object cseWorkerLevel: TCSpinEdit
    Left = 208
    Top = 136
    Width = 121
    Height = 22
    MaxValue = 15
    TabOrder = 5
  end
  object editWorkerNo: TEdit
    Left = 208
    Top = 160
    Width = 121
    Height = 20
    TabOrder = 6
  end
  object Button1: TButton
    Left = 88
    Top = 192
    Width = 75
    Height = 25
    Caption = #30830#23450
    TabOrder = 7
  end
  object Button2: TButton
    Left = 248
    Top = 192
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 8
  end
end
