object FormPSTN: TFormPSTN
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #39006#27604#22806#32218#37197#32622
  ClientHeight = 327
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 95
    Top = 21
    Width = 108
    Height = 12
    Caption = #39006#27604#22806#32218#36890#36947#24207#34399#65306
  end
  object Label2: TLabel
    Left = 121
    Top = 47
    Width = 84
    Height = 12
    Caption = #39006#27604#22806#32218#34399#30908#65306
  end
  object Label3: TLabel
    Left = 55
    Top = 74
    Width = 144
    Height = 12
    Caption = #21628#20986#33258#21205#21152#25765#30340#21344#32218#23383#20896#65306
  end
  object Label5: TLabel
    Left = 17
    Top = 100
    Width = 180
    Height = 12
    Caption = #19981#38656#35201#21152#25765#21344#32218#23383#20896#30340#20363#22806#35373#23450#65306
  end
  object Label6: TLabel
    Left = 30
    Top = 126
    Width = 168
    Height = 12
    Caption = #38263#36884#34399#30908#21069#33258#21205#21152#25765#30340'IP'#23383#20896#65306
  end
  object editPSTNCode: TEdit
    Left = 217
    Top = 43
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editPSTNCodeChange
  end
  object editPreCode: TEdit
    Left = 217
    Top = 69
    Width = 131
    Height = 20
    TabOrder = 1
    OnChange = editPSTNCodeChange
  end
  object Button1: TButton
    Left = 78
    Top = 156
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 234
    Top = 156
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object csePSTNPort: TCSpinEdit
    Left = 217
    Top = 17
    Width = 65
    Height = 21
    Color = clGrayText
    Enabled = False
    TabOrder = 4
  end
  object editIPPreCode: TEdit
    Left = 217
    Top = 121
    Width = 131
    Height = 20
    TabOrder = 5
    OnChange = editPSTNCodeChange
  end
  object editNotAddPreCode: TEdit
    Left = 217
    Top = 95
    Width = 131
    Height = 20
    TabOrder = 6
    OnChange = editPSTNCodeChange
  end
  object GroupBox1: TGroupBox
    Left = 17
    Top = 199
    Width = 357
    Height = 114
    Caption = #20633#27880#65306
    TabOrder = 7
    object Memo1: TMemo
      Left = 2
      Top = 14
      Width = 353
      Height = 98
      Align = alClient
      Color = clGrayText
      Lines.Strings = (
        #30070#25509#30340#39006#27604#22806#32218#26159#21295#32218#36890#25110#20225#26989#20839#37096#30340'PBX'#20998#27231#32218#26178#65292#38656#35201#21152
        #25765#23383#20896'(9'#25110'0)'#25165#33021#21628#20986#12290
        #20294#22914#26524#22806#32218#25509#30340#26159'PBX'#20998#27231#32218#65292#30070#21628#21483'PBX'#20839#37096#20998#27231#26178#23601#19981#38656
        #35201#21152#25765#65292#25152#20197#35201#35373#32622#20363#22806#24773#27841#12290
        #20363#22806#24773#27841#35373#32622#35215#21063#26684#24335#26159#65306'N,L;N,L;... (N'#28858#20225#26989#20839#37096#20998#27231
        #23383
        #20896',L'#28858#20225#26989#20839#37096#20998#27231#38263#24230#65292#21487#20197#35373#32622#22810#20491#65292#29992#20998#34399#38548#38283')'#12290)
      ReadOnly = True
      TabOrder = 0
    end
  end
end
