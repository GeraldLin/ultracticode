//---------------------------------------------------------------------------

#ifndef modifypswH
#define modifypswH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormModifyPSW : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TComboBox *cbUserName;
  TEdit *editOldPassword;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label3;
  TEdit *editNewPassword;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbUserNameKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
  __fastcall TFormModifyPSW(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModifyPSW *FormModifyPSW;
//---------------------------------------------------------------------------
#endif
