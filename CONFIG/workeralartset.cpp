//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "workeralartset.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormWorkerAlartSet *FormWorkerAlartSet;

extern int g_nSelWorkerItemNo;
extern int g_nSelGroupItemNo;
extern int gLangID;
//---------------------------------------------------------------------------
__fastcall TFormWorkerAlartSet::TFormWorkerAlartSet(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormWorkerAlartSet::cbAlartColorKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormWorkerAlartSet::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormWorkerAlartSet::ckAlartIdClick(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormWorkerAlartSet::Button1Click(TObject *Sender)
{
  if (AlartType == 1)
  {
    FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartId = FormWorkerAlartSet->ckAlartId->Checked==true ? 1:0;
    FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartValue = FormWorkerAlartSet->cseAlartValue->Value;
    FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].CompType = FormWorkerAlartSet->rgCompType->ItemIndex;
    if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "�x��"))
      FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor = 1;
    else if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "����"))
      FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor = 2;
    else if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "����"))
      FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor = 3;
    else if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "����"))
      FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor = 4;
    else
      FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor = 0;

    FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartSound = FormWorkerAlartSet->ckAlartSound->Checked==true ? 1:0;

    TIniFile *IniKey = new TIniFile (FormMain->LOGINIFile);

    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(g_nSelWorkerItemNo+1)+"].AlartId", FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartId);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(g_nSelWorkerItemNo+1)+"].AlartValue", FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartValue);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(g_nSelWorkerItemNo+1)+"].CompType", FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].CompType);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(g_nSelWorkerItemNo+1)+"].AlartColor", FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(g_nSelWorkerItemNo+1)+"].AlartSound", FormMain->WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartSound);

    delete IniKey;
    FormMain->DispWorkerCountValue(g_nSelWorkerItemNo);
  }
  else if (AlartType == 2)
  {
    FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartId = FormWorkerAlartSet->ckAlartId->Checked==true ? 1:0;
    FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartValue = FormWorkerAlartSet->cseAlartValue->Value;
    FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].CompType = FormWorkerAlartSet->rgCompType->ItemIndex;
    if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "�x��"))
      FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor = 1;
    else if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "����"))
      FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor = 2;
    else if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "����"))
      FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor = 3;
    else if (FormWorkerAlartSet->cbAlartColor->Text == ((gLangID == 1) ? "��ɫ" : "����"))
      FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor = 4;
    else
      FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor = 0;

    FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartSound = FormWorkerAlartSet->ckAlartSound->Checked==true ? 1:0;

    TIniFile *IniKey = new TIniFile (FormMain->LOGINIFile);

    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(g_nSelGroupItemNo+1)+"].AlartId", FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartId);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(g_nSelGroupItemNo+1)+"].AlartValue", FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartValue);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(g_nSelGroupItemNo+1)+"].CompType", FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].CompType);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(g_nSelGroupItemNo+1)+"].AlartColor", FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(g_nSelGroupItemNo+1)+"].AlartSound", FormMain->GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartSound);

    delete IniKey;
    FormMain->DispGroupCountValue(g_nSelGroupItemNo);
  }
  this->Close();
}
//---------------------------------------------------------------------------

