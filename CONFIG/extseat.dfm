object FormExtSeat: TFormExtSeat
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #36960#31471#24231#24109#21443#25976#35373#23450
  ClientHeight = 385
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 133
    Top = 21
    Width = 78
    Height = 12
    Caption = #36960#31471#22352#24109#24207#34399':'
  end
  object Label2: TLabel
    Left = 145
    Top = 47
    Width = 66
    Height = 12
    Caption = #24231#24109#20998#27231#34399':'
  end
  object Label3: TLabel
    Left = 157
    Top = 72
    Width = 54
    Height = 12
    Caption = #24231#24109#39006#22411':'
  end
  object Label4: TLabel
    Left = 120
    Top = 100
    Width = 90
    Height = 12
    Caption = #38928#35373#24231#24109#32676#32068#34399':'
  end
  object Label5: TLabel
    Left = 108
    Top = 125
    Width = 102
    Height = 12
    Caption = #38928#35373#22519#27231#21729#32676#32068#34399':'
  end
  object Label6: TLabel
    Left = 120
    Top = 152
    Width = 90
    Height = 12
    Caption = #38928#35373#22519#27231#21729#32026#21029':'
  end
  object Label7: TLabel
    Left = 120
    Top = 177
    Width = 90
    Height = 12
    Caption = #38928#35373#22519#27231#21729#32232#34399':'
  end
  object Label8: TLabel
    Left = 9
    Top = 355
    Width = 405
    Height = 12
    Caption = #27880': '#38928#35373#22519#27231#21729#24037#32232#34399#28858#31354#26178','#34920#31034#31995#32113#36939#34892#21518#27794#26377#38928#35373#30340#22519#27231#21729#30331#38520'.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #32048#26126#39636
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 84
    Top = 203
    Width = 126
    Height = 12
    Caption = #36960#31243#24231#24109#25509#30340#22806#32218#34399#30908':'
  end
  object Label10: TLabel
    Left = 71
    Top = 230
    Width = 138
    Height = 12
    Caption = #25765#25171#35442#24231#24109#38651#35441#30340#36335#30001#34399':'
  end
  object Label11: TLabel
    Left = 120
    Top = 255
    Width = 90
    Height = 12
    Caption = #38928#35373#22519#27231#21729#22995#21517':'
  end
  object Label12: TLabel
    Left = 132
    Top = 281
    Width = 78
    Height = 12
    Caption = #24231#24109#32129#23450#30340'IP:'
  end
  object editPort: TEdit
    Left = 217
    Top = 17
    Width = 131
    Height = 20
    Color = clGrayText
    Enabled = False
    TabOrder = 0
    Text = '0'
  end
  object editSeatNo: TEdit
    Left = 217
    Top = 43
    Width = 131
    Height = 20
    TabOrder = 1
    OnChange = editSeatNoChange
  end
  object cbSeatType: TComboBox
    Left = 217
    Top = 69
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 2
    Text = #36960#31243#38651#35441#24231#24109
    OnChange = editSeatNoChange
    OnKeyPress = cbSeatTypeKeyPress
    Items.Strings = (
      #36960#31243#38651#35441#24231#24109)
  end
  object cseSeatGroupNo: TCSpinEdit
    Left = 217
    Top = 95
    Width = 131
    Height = 21
    MaxValue = 31
    TabOrder = 3
    OnChange = editSeatNoChange
  end
  object cseWorkerGroupNo: TCSpinEdit
    Left = 217
    Top = 121
    Width = 131
    Height = 21
    MaxValue = 31
    TabOrder = 4
    OnChange = editSeatNoChange
  end
  object cseWorkerLevel: TCSpinEdit
    Left = 217
    Top = 147
    Width = 131
    Height = 21
    MaxValue = 15
    TabOrder = 5
    OnChange = editSeatNoChange
  end
  object editWorkerNo: TEdit
    Left = 217
    Top = 173
    Width = 131
    Height = 20
    TabOrder = 6
    OnChange = editSeatNoChange
  end
  object Button1: TButton
    Left = 87
    Top = 312
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 260
    Top = 312
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 8
    OnClick = Button2Click
  end
  object editPSTNNo: TEdit
    Left = 217
    Top = 199
    Width = 131
    Height = 20
    TabOrder = 9
    OnChange = editSeatNoChange
  end
  object cseRouteNo: TCSpinEdit
    Left = 217
    Top = 225
    Width = 131
    Height = 21
    MaxValue = 15
    TabOrder = 10
    Value = 1
    OnChange = editSeatNoChange
  end
  object editWorkerName: TEdit
    Left = 217
    Top = 251
    Width = 131
    Height = 20
    TabOrder = 11
    OnChange = editSeatNoChange
  end
  object editSeatIP: TEdit
    Left = 217
    Top = 277
    Width = 131
    Height = 20
    TabOrder = 12
    OnChange = editSeatNoChange
  end
end
