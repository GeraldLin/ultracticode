//---------------------------------------------------------------------------

#ifndef workeralartsetH
#define workeralartsetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include <ExtCtrls.hpp>
#include "public.h"
//---------------------------------------------------------------------------
class TFormWorkerAlartSet : public TForm
{
__published:	// IDE-managed Components
  TCheckBox *ckAlartId;
  TLabel *Label1;
  TRadioGroup *rgCompType;
  TLabel *Label2;
  TCheckBox *ckAlartSound;
  TComboBox *cbAlartColor;
  TCSpinEdit *cseAlartValue;
  TButton *Button1;
  TButton *Button2;
  TLabel *lblItemName;
  void __fastcall cbAlartColorKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall ckAlartIdClick(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormWorkerAlartSet(TComponent* Owner);

  int AlartType;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormWorkerAlartSet *FormWorkerAlartSet;
//---------------------------------------------------------------------------
#endif
