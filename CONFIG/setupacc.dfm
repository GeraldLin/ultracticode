object FormAccCode: TFormAccCode
  Left = 193
  Top = 114
  Width = 273
  Height = 201
  BorderIcons = [biSystemMenu]
  Caption = #25765#20837#34399#37197#32622
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 17
    Top = 21
    Width = 54
    Height = 12
    Caption = #25765#20837#34399#30908':'
  end
  object Label2: TLabel
    Left = 17
    Top = 56
    Width = 78
    Height = 12
    Caption = #26368#23567#34399#30908#38263#24230':'
  end
  object Label3: TLabel
    Left = 17
    Top = 91
    Width = 78
    Height = 12
    Caption = #26368#22823#34399#30908#38263#24230':'
  end
  object editAccCode: TEdit
    Left = 130
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editAccCodeChange
    OnExit = editAccCodeExit
  end
  object cseMinLen: TCSpinEdit
    Left = 130
    Top = 52
    Width = 131
    Height = 21
    TabOrder = 1
    OnChange = editAccCodeChange
  end
  object cseMaxLen: TCSpinEdit
    Left = 130
    Top = 87
    Width = 131
    Height = 21
    TabOrder = 2
    OnChange = editAccCodeChange
  end
  object Button1: TButton
    Left = 27
    Top = 123
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 174
    Top = 123
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
end
