//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "pstn.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormPSTN *FormPSTN;
extern int g_CurPstn;
extern int gLangID;
extern void SendmodifyextlineMsg(int nLineNo, const char *pszPSTNCode, const char *pszPreCode, const char *pszIPPreCode, const char *pszNotAddPreCode);
//---------------------------------------------------------------------------
__fastcall TFormPSTN::TFormPSTN(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormPSTN::SetModifyType(bool bmodify, int lineno, AnsiString pstncode, AnsiString precode, AnsiString ipprecode, AnsiString notaddprecode)
{
  if (bmodify == false)
  {
    bModify = bmodify;
    csePSTNPort->Value = FormMain->PstnLine.PSTNNum;
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
  }
  else
  {
    bModify = bmodify;
    csePSTNPort->Value = lineno;
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
  }
  editPSTNCode->Text = pstncode;
  editPreCode->Text = precode;
  editNotAddPreCode->Text = notaddprecode;
  editIPPreCode->Text = ipprecode;
  Button1->Enabled = false;
}

void __fastcall TFormPSTN::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormPSTN::editPSTNCodeChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormPSTN::Button1Click(TObject *Sender)
{
  int nResult;

  if (gLangID == 1)
  {
    if (bModify == false)
    {
      nResult = FormMain->PstnLine.EditApstn(csePSTNPort->Value, editPSTNCode->Text, editPreCode->Text, editIPPreCode->Text, editNotAddPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyextlineMsg(csePSTNPort->Value, editPSTNCode->Text.c_str(), editPreCode->Text.c_str(), editIPPreCode->Text.c_str(), editNotAddPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"增加模拟外线成功,请按 应用 按钮才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->PstnLine.PSTNNum++;
        FormMain->bPSTNParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispPSTN();
      }
      else
      {
        MessageBox(NULL,"增加模拟外线失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->PstnLine.EditApstn(csePSTNPort->Value, editPSTNCode->Text, editPreCode->Text, editIPPreCode->Text, editNotAddPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyextlineMsg(csePSTNPort->Value, editPSTNCode->Text.c_str(), editPreCode->Text.c_str(), editIPPreCode->Text.c_str(), editNotAddPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"修改模拟外线成功,请按 应用 按钮才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bPSTNParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispPSTN();
      }
      else
      {
        MessageBox(NULL,"修改模拟外线失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
  else
  {
    if (bModify == false)
    {
      nResult = FormMain->PstnLine.EditApstn(csePSTNPort->Value, editPSTNCode->Text, editPreCode->Text, editIPPreCode->Text, editNotAddPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyextlineMsg(csePSTNPort->Value, editPSTNCode->Text.c_str(), editPreCode->Text.c_str(), editIPPreCode->Text.c_str(), editNotAddPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"糤摸ゑ絬Θ,叫 甅ノ 秙ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->PstnLine.PSTNNum++;
        FormMain->bPSTNParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispPSTN();
      }
      else
      {
        MessageBox(NULL,"糤摸ゑ絬ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->PstnLine.EditApstn(csePSTNPort->Value, editPSTNCode->Text, editPreCode->Text, editIPPreCode->Text, editNotAddPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyextlineMsg(csePSTNPort->Value, editPSTNCode->Text.c_str(), editPreCode->Text.c_str(), editIPPreCode->Text.c_str(), editNotAddPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"э摸ゑ絬Θ,叫 甅ノ 秙ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bPSTNParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispPSTN();
      }
      else
      {
        MessageBox(NULL,"э摸ゑ絬ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
}
//---------------------------------------------------------------------------

