//---------------------------------------------------------------------------

#ifndef routeH
#define routeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormRoute : public TForm
{
__published:	// IDE-managed Components
  TComboBox *cbSelMode;
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editTrkStr;
  TCSpinEdit *cseRouteNo;
  TLabel *Label4;
  TLabel *Label5;
  TButton *Button1;
  TButton *Button2;
  TEdit *editRemark;
  TLabel *Label7;
  TLabel *Label8;
  TEdit *editIPPreCode;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall cseRouteNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormRoute(TComponent* Owner);

  bool bModify;
  void SetModifyType(bool bmodify, int routeno=0, int selmode=0, AnsiString trkstr="", AnsiString remark="", AnsiString ipprecode="");
};
//---------------------------------------------------------------------------
extern PACKAGE TFormRoute *FormRoute;
//---------------------------------------------------------------------------
#endif
