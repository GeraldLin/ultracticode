object FormRoute: TFormRoute
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #36335#30001#37197#32622
  ClientHeight = 239
  ClientWidth = 665
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 142
    Top = 13
    Width = 48
    Height = 12
    Caption = #36335#30001#34399#65306
  end
  object Label2: TLabel
    Left = 78
    Top = 64
    Width = 108
    Height = 12
    Caption = #36335#30001#20013#32380#36984#25799#27169#24335#65306
  end
  object Label3: TLabel
    Left = 90
    Top = 117
    Width = 96
    Height = 12
    Caption = #36335#30001#20013#32380#32218#36335#32676#65306
  end
  object Label4: TLabel
    Left = 17
    Top = 147
    Width = 640
    Height = 13
    Caption = #27880': '#35373#32622#35442#36335#30001#30340#20013#32380#32218#36335#32676#21443#25976#26178#21487#20197#20351#29992'","'#21644'"-"'#65292#36887#34399'","'#34920#31034#19981#36899#32396#65292#28187#34399'"-"'#34920#31034#36899#32396#35373#32622#12290
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 48
    Top = 172
    Width = 290
    Height = 13
    Caption = #22914#65306'0-3,6 '#34920#31034#35442#36335#30001#30340#20013#32380#34399#28858'0'#12289'1'#12289'2'#12289'3'#21644'6'#12290
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 130
    Top = 39
    Width = 60
    Height = 12
    Caption = #36335#30001#21517#31281#65306
  end
  object Label8: TLabel
    Left = 12
    Top = 91
    Width = 168
    Height = 12
    Caption = #38263#36884#34399#30908#21069#33258#21205#21152#25765#30340'IP'#23383#20896#65306
  end
  object cbSelMode: TComboBox
    Left = 199
    Top = 61
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    Text = #25353#38918#24207#24490#29872#36984#25799
    OnChange = cseRouteNoChange
    Items.Strings = (
      #25353#38918#24207#24490#29872#36984#25799
      #20778#20808#36984#22855#25976#38651#36335
      #20778#20808#36984#20598#25976#38651#36335
      #25353#36335#30001#38918#24207#36984#25799
      #25353#36335#30001#36870#24207#36984#25799
      #20778#20808#36984#25799#26368#20808#31354#38289#36890#36947)
  end
  object editTrkStr: TEdit
    Left = 199
    Top = 113
    Width = 325
    Height = 20
    TabOrder = 1
    OnChange = cseRouteNoChange
  end
  object cseRouteNo: TCSpinEdit
    Left = 199
    Top = 9
    Width = 131
    Height = 21
    Color = clGrayText
    Enabled = False
    MaxValue = 32
    TabOrder = 2
    Value = 1
    OnChange = cseRouteNoChange
  end
  object Button1: TButton
    Left = 208
    Top = 199
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 416
    Top = 199
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
  object editRemark: TEdit
    Left = 199
    Top = 35
    Width = 131
    Height = 20
    TabOrder = 5
    OnChange = cseRouteNoChange
  end
  object editIPPreCode: TEdit
    Left = 199
    Top = 87
    Width = 131
    Height = 20
    TabOrder = 6
    OnChange = cseRouteNoChange
  end
end
