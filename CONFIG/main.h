//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "CSPIN.h"
#include <ImgList.hpp>
#include <Menus.hpp>
#include <inifiles.hpp>
#include <Dialogs.hpp>
#include <Graphics.hpp>
#include "reinit.hpp"
#include "../include/comm/typedef.h"
#include "../include/comm/macrodef.h"
#include "../comm/msgfifo.h"
#include "../comm/cstring.h"
#include "../comm/parxml.h"
#include "public.h"
#include "TcpClient.h"
#include "msgfifo.h"
#include "FlwRule.h"
#include <wininet.h>
#include "SUISkinEngine.hpp"
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdIPWatch.hpp>
#include <Buttons.hpp>
#include <MPlayer.hpp>
//---------------------------------------------------------------------------
class CMyToolsBox
{
public:
  int m_nSpeedButtonNum;
  TBitBtn *m_pSpeedButton[32];
  TPanel *m_pToolsBoxPanel;
  TNotebook *m_pToolsBoxNotebook;

public:
  CMyToolsBox()
  {
    m_nSpeedButtonNum = 0;
    m_pToolsBoxPanel = NULL;
    m_pToolsBoxNotebook = NULL;
    for (int i=0; i<32; i++)
      m_pSpeedButton[i] = NULL;
  }
  void RemoveToolsBox(int nSpeedButtonId)
  {
    int i, j=1;

    for (i=0; i<=nSpeedButtonId && i<m_nSpeedButtonNum; i++)
    {
      if (m_pSpeedButton[i] == NULL)
        continue;
      m_pSpeedButton[i]->Left = 2;
      if (i == 0)
        m_pSpeedButton[i]->Top = 2;
      else
        m_pSpeedButton[i]->Top = m_pSpeedButton[i-1]->Top + 38;
      m_pSpeedButton[i]->Width = m_pToolsBoxPanel->Width-4;
    }

    for (i=m_nSpeedButtonNum-1; i>nSpeedButtonId; i--)
    {
      if (m_pSpeedButton[i] == NULL)
        continue;
      m_pSpeedButton[i]->Left = 2;
      if (i == m_nSpeedButtonNum-1)
        m_pSpeedButton[i]->Top = m_pToolsBoxPanel->Height-38;
      else
        m_pSpeedButton[i]->Top = m_pSpeedButton[i+1]->Top-38;
      m_pSpeedButton[i]->Width = m_pToolsBoxPanel->Width-4;
      j++;
    }

    m_pToolsBoxNotebook->Left = 2;
    m_pToolsBoxNotebook->Width = m_pToolsBoxPanel->Width-4;
    m_pToolsBoxNotebook->Height = m_pToolsBoxPanel->Height-(38*m_nSpeedButtonNum)-4;
    m_pToolsBoxNotebook->Top = m_pSpeedButton[nSpeedButtonId]->Top+38;
    m_pToolsBoxNotebook->PageIndex = nSpeedButtonId;
  }
};

class TFormMain : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel1;
  TPanel *plRight;
  TPanel *panelSel;
  TNotebook *Notebook1;
  TPanel *Panel4;
  TLabel *Label1;
  TCSpinEdit *cseIVRPORT;
  TLabel *Label3;
  TCSpinEdit *cseDBPORT;
  TImageList *ImageList1;
  TPanel *Panel2;
  TLabel *Label10;
  TEdit *editFLWPATH;
  TButton *Button4;
  TListView *lvFlw;
  TGroupBox *GroupBox1;
  TListView *lvAccCode;
  TPopupMenu *PopupMenu1;
  TPopupMenu *PopupMenu2;
  TMenuItem *N1;
  TMenuItem *N2;
  TMenuItem *N3;
  TMenuItem *N4;
  TMenuItem *N5;
  TMenuItem *N6;
  TPanel *Panel5;
  TPanel *Panel7;
  TPanel *Panel11;
  TLabel *Label19;
  TEdit *editMemVocPath;
  TLabel *Label20;
  TEdit *editPlayVocPath;
  TOpenDialog *OpenDialog1;
  TLabel *Label13;
  TEdit *editIVRAuthKey;
  TBevel *Bevel4;
  TLabel *Label14;
  TComboBox *cbCardType;
  TLabel *Label15;
  TComboBox *cbSwitchType;
  TPanel *Panel8;
  TLabel *Label26;
  TCSpinEdit *cseFLWPort;
  TPanel *Panel9;
  TPanel *Panel10;
  TPanel *Panel12;
  TPanel *Panel13;
  TPanel *Panel14;
  TListView *lvAgStatus;
  TGroupBox *GroupBox2;
  TGroupBox *GroupBox3;
  TLabel *Label27;
  TLabel *Label28;
  TButton *btStartIVRService;
  TButton *btStopIVRService;
  TButton *btStartFLWService;
  TButton *btStopFLWService;
  TButton *btRefreshIVRServiceStatus;
  TButton *btRefreshFLWServiceStatus;
  TCheckBox *ckIVRCommId;
  TCheckBox *ckIVRDebugId;
  TCheckBox *ckIVRDrvId;
  TCheckBox *ckFLWCommId;
  TCheckBox *ckFLWCmdId;
  TCheckBox *ckFLWVarId;
  TPanel *Panel15;
  TListView *lvAcdStatus;
  TImageList *ImageList2;
  TMemo *memoIVRTrace;
  TMemo *memoFLWTrace;
  TTimer *Timer1;
  TCheckBox *ckIVRAlartId;
  TCheckBox *ckFLWAlartId;
  TPanel *Panel16;
  TListView *lvLoadFlw;
  TPopupMenu *PopupMenu3;
  TMenuItem *N7;
  TPopupMenu *PopupMenu4;
  TMenuItem *N8;
  TMenuItem *N9;
  TMenuItem *N10;
  TMenuItem *N11;
  TPopupMenu *PopupMenu5;
  TMenuItem *N12;
  TMenuItem *N13;
  TMenuItem *N14;
  TPanel *Panel17;
  TPanel *Panel18;
  TButton *btOK;
  TButton *btCancel;
  TButton *btApply;
  TPanel *Panel20;
  TLabel *Label33;
  TLabel *Label34;
  TLabel *Label38;
  TLabel *lbAgLogoutCount1;
  TLabel *lbAgBusyCount;
  TLabel *lbAgIdelCount;
  TLabel *lbAglevalCount;
  TLabel *lbAgLogoutCount;
  TCheckBox *ckGetagentstatus;
  TPanel *Panel21;
  TLabel *Label44;
  TLabel *lbAcdWaitNum;
  TCheckBox *ckGetqueueinfo;
  TPanel *Panel22;
  TCheckBox *ckGetivrtracemsg;
  TCheckBox *ckSaveIVRMsg;
  TCheckBox *ckDispIVRMsg;
  TPanel *Panel23;
  TCheckBox *ckGetgetloadedflws;
  TPanel *Panel24;
  TCheckBox *ckGetgetflwtracemsg;
  TCheckBox *ckSaveFLWMsg;
  TCheckBox *ckDispFLWMsg;
  TButton *btClearIVRMsg;
  TButton *btClearFLWMsg;
  TLabel *Label31;
  TLabel *lbAgLoginCount;
  TPanel *Panel25;
  TPanel *Panel26;
  TMenuItem *N15;
  TMenuItem *N16;
  TLabel *Label51;
  TEdit *editFlwFileName;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label57;
  TEdit *editVocFileName;
  TButton *Button24;
  TButton *Button25;
  TLabel *Label56;
  TEdit *editFTPVocPath;
  TBevel *Bevel5;
  TOpenDialog *OpenDialog2;
  TPanel *Panel27;
  TCheckBox *ckGetTrkCountData;
  TListView *lvAgCountData;
  TPanel *Panel28;
  TCheckBox *ckGetAgCountData;
  TListView *lvTrkCountData;
  TCheckBox *ckLocaAlartId;
  TGroupBox *GroupBox5;
  TLabel *Label47;
  TLabel *Label48;
  TLabel *Label50;
  TLabel *Label54;
  TLabel *Label55;
  TLabel *Label59;
  TCheckBox *ckIsTheSamePC;
  TEdit *editConnectIVRIP;
  TCSpinEdit *cseConnectIVRPort;
  TCSpinEdit *cseConnectFLWPort;
  TEdit *editIVRFTPPSW;
  TEdit *editIVRFTPUser;
  TCSpinEdit *cseIVRFTPPort;
  TButton *btInstallIVRservice;
  TButton *btInstallFLWservice;
  TButton *Button26;
  TButton *Button27;
  TMenuItem *N17;
  TMenuItem *N18;
  TMenuItem *N19;
  TsuiSkinEngine *suiSkinEngine1;
  TLabel *Label6;
  TComboBox *cbOSType;
  TLabel *Label35;
  TCSpinEdit *cseLinuxPort;
  TGroupBox *GroupBox4;
  TLabel *Label36;
  TButton *btInstallDBservice;
  TButton *btRefreshDBServiceStatus;
  TButton *btStartDBService;
  TButton *btStopDBService;
  TLabel *Label5;
  TComboBox *cbSkins;
  TBevel *Bevel6;
  TLabel *Label37;
  TLabel *Label45;
  TLabel *Label61;
  TLabel *Label62;
  TLabel *Label63;
  TComboBox *cbxDBType;
  TEdit *edtServer;
  TEdit *edtDB;
  TEdit *edtUSER;
  TEdit *edtPSW;
  TLabel *Label4;
  TCSpinEdit *cseConnectDBPort;
  TBevel *Bevel1;
  TGroupBox *GroupBox6;
  TLabel *Label2;
  TLabel *Label7;
  TLabel *Label8;
  TEdit *editIVRIP;
  TEdit *editMark;
  TEdit *editGWIP;
  TGroupBox *GroupBox7;
  TLabel *Label9;
  TLabel *Label17;
  TEdit *editDNS1;
  TEdit *editDNS2;
  TRadioButton *rbAutoGetIP;
  TRadioButton *rbStaticIP;
  TRadioButton *rbAutoGetDNS;
  TRadioButton *rbStaticDNS;
  TIdIPWatch *IdIPWatch1;
  TLabel *Label16;
  TEdit *editLocaIP;
  TMenuItem *N20;
  TPanel *Panel29;
  TPanel *Panel30;
  TListView *lvSeat;
  TPanel *Panel31;
  TLabel *Label25;
  TCSpinEdit *cseSeatNum;
  TPanel *Panel32;
  TPanel *Panel33;
  TPanel *Panel34;
  TLabel *Label18;
  TCSpinEdit *cseExtSeatNum;
  TListView *lvExtSeat;
  TPanel *Panel35;
  TPanel *Panel36;
  TImage *imgConnectCTRLStatus;
  TImage *imgConnectIVRStatus;
  TImage *imgConnectFLWStatus;
  TImage *imgConnectDBStatus;
  TLabel *Label39;
  TEdit *editAuthKey;
  TGroupBox *GroupBox8;
  TLabel *Label41;
  TButton *btInstallSMSservice;
  TButton *btRefreshSMSServiceStatus;
  TButton *btStartSMSService;
  TButton *btStopSMSService;
  TGroupBox *GroupBox9;
  TLabel *Label42;
  TButton *btInstallDOCservice;
  TButton *btRefreshDOCServiceStatus;
  TButton *btStartDOCService;
  TButton *btStopDOCService;
  TPanel *plToolsBoxLeft;
  TNotebook *ntbkToolsBox;
  TPanel *Panel37;
  TListView *lvParamSel1;
  TPanel *Panel38;
  TListView *lvParamSel2;
  TPanel *Panel39;
  TListView *lvParamSel3;
  TPanel *Panel40;
  TListView *lvParamSel4;
  TPanel *Panel41;
  TListView *lvParamSel5;
  TLabel *lbSelected;
  TPanel *Panel3;
  TLabel *Label43;
  TLabel *Label49;
  TBitBtn *BitBtn1;
  TBitBtn *BitBtn2;
  TBitBtn *BitBtn3;
  TBitBtn *BitBtn4;
  TBitBtn *BitBtn5;
  TEdit *editSelitem;
  TGroupBox *GroupBox10;
  TLabel *Label52;
  TButton *btInstallWEBservice;
  TButton *btRefreshWEBServiceStatus;
  TButton *btStartWEBService;
  TButton *btStopWEBService;
  TImage *imgIVRServiceStatus;
  TImage *imgFLWServiceStatus;
  TImage *imgDBServiceStatus;
  TImage *imgWEBServiceStatus;
  TImage *imgSMSServiceStatus;
  TImage *imgDOCServiceStatus;
  TBitBtn *BitBtn6;
  TBitBtn *BitBtn7;
  TBitBtn *BitBtn8;
  TBitBtn *BitBtn9;
  TBitBtn *BitBtn10;
  TBitBtn *BitBtn11;
  TBitBtn *BitBtn21;
  TBitBtn *BitBtn22;
  TBitBtn *BitBtn23;
  TBitBtn *BitBtn18;
  TBitBtn *BitBtn19;
  TBitBtn *BitBtn20;
  TGroupBox *GroupBox11;
  TLabel *Label53;
  TImage *imgTTSServiceStatus;
  TButton *btInstallTTSservice;
  TButton *btRefreshTTSServiceStatus;
  TButton *btStartTTSService;
  TButton *btStopTTSService;
  TGroupBox *GroupBox12;
  TLabel *Label58;
  TImage *imgLINKServiceStatus;
  TButton *btInstallLINKservice;
  TButton *btRefreshLINKServiceStatus;
  TButton *btStartLINKService;
  TButton *btStopLINKService;
  TPanel *Panel42;
  TButton *btLogin;
  TButton *btModifyPsw;
  TPageControl *pageState;
  TTabSheet *TabSheet1;
  TPanel *Panel19;
  TLabel *Label29;
  TLabel *Label30;
  TLabel *lbChnBusyCount;
  TLabel *lbChnIdelCount;
  TLabel *Label46;
  TLabel *lbChnBlockCount;
  TLabel *Label32;
  TLabel *lbChnInCount;
  TLabel *Label40;
  TLabel *lbChnOutCount;
  TCheckBox *ckGetchnstatus;
  TListView *lvChnStatus;
  TTabSheet *TabSheet2;
  TListView *lvVOPState;
  TPanel *Panel44;
  TLabel *Label64;
  TLabel *Label65;
  TLabel *lbIVRBusyCount;
  TLabel *lbIVRIdelCount;
  TLabel *Label66;
  TLabel *Label67;
  TLabel *lbRECBusyCount;
  TLabel *lbRECIdelCount;
  TImageList *ImageList3;
  TLabel *Label68;
  TLabel *lbVopBlockCount;
  TPanel *Panel43;
  TGroupBox *GroupBox13;
  TCheckBox *ckBusyTrunkSound;
  TCheckBox *ckAgentLoginSound;
  TCheckBox *ckAgentLevalSound;
  TCheckBox *ckBusyAgentSound;
  TCheckBox *ckACDWaitingSound;
  TCheckBox *ckBusyIVRSound;
  TCheckBox *ckBlockTrunkSound;
  TGroupBox *GroupBox14;
  TMemo *memoAlart;
  TMediaPlayer *MediaPlayer1;
  TNotebook *Notebook2;
  TPanel *Panel45;
  TPanel *Panel46;
  TPanel *Panel47;
  TPanel *Panel48;
  TBitBtn *BitBtn24;
  TBitBtn *BitBtn25;
  TBitBtn *BitBtn26;
  TPanel *Panel49;
  TBitBtn *BitBtn27;
  TPanel *Panel6;
  TNotebook *Notebook3;
  TPanel *Panel51;
  TPanel *Panel52;
  TPanel *Panel53;
  TPanel *Panel54;
  TBitBtn *BitBtn28;
  TBitBtn *BitBtn29;
  TBitBtn *BitBtn30;
  TBitBtn *BitBtn31;
  TPanel *Panel55;
  TPanel *Panel56;
  TListView *lvVopChn;
  TLabel *Label76;
  TCSpinEdit *cseVopNum;
  TLabel *Label77;
  TCSpinEdit *cseSelVopNo;
  TCheckBox *ckVopOpen;
  TLabel *Label78;
  TEdit *editVopChnNum;
  TBitBtn *BitBtn32;
  TPanel *Panel57;
  TGroupBox *GroupBox15;
  TListView *lvWorkerList;
  TSplitter *Splitter1;
  TListView *lvWorkerCallCount;
  TPanel *Panel58;
  TButton *Button3;
  TButton *Button5;
  TPanel *Panel59;
  TSplitter *Splitter2;
  TGroupBox *GroupBox16;
  TListView *lvGroupCallCount;
  TPanel *Panel60;
  TButton *Button6;
  TButton *Button7;
  TListView *lvGroupList;
  TLabel *Label60;
  TCheckBox *ckCallCount;
  TComboBox *cbCountCycle;
  TCheckBox *ckWriteCountLog;
  TCheckBox *ckWriteCountDB;
  TLabel *Label79;
  TComboBox *cbClearCallCountType;
  TLabel *Label80;
  TEdit *editClearTimeList;
  TLabel *Label81;
  TLabel *Label82;
  TLabel *Label83;
  TLabel *Label84;
  TLabel *Label85;
  TCSpinEdit *cseMaxInRingOverTime;
  TCSpinEdit *cseMaxOutRingOverTime;
  TCSpinEdit *cseMaxTalkOverTime;
  TCSpinEdit *cseMaxACWOverTime;
  TLabel *Label69;
  TLabel *Label70;
  TLabel *Label71;
  TLabel *Label72;
  TLabel *Label73;
  TLabel *Label74;
  TCSpinEdit *cseMaxBusyTrunk;
  TCSpinEdit *cseMinAgentLogin;
  TCSpinEdit *cseMaxAgentLeval;
  TCSpinEdit *cseMaxBusyAgent;
  TCSpinEdit *cseMaxACDWaiting;
  TCSpinEdit *cseMaxBusyIVR;
  TCheckBox *ckSeatSatusSound;
  TLabel *Label86;
  TLabel *Label87;
  TLabel *Label88;
  TLabel *Label89;
  TLabel *Label90;
  TLabel *Label91;
  TLabel *Label92;
  TLabel *Label93;
  TLabel *Label94;
  TLabel *Label95;
  TLabel *Label96;
  TLabel *Label97;
  TComboBox *cbSoundType;
  TPanel *Panel62;
  TPanel *Panel61;
  TBitBtn *BitBtn12;
  TBitBtn *BitBtn13;
  TBitBtn *BitBtn14;
  TPanel *Panel64;
  TPanel *Panel63;
  TLabel *Label21;
  TLabel *Label22;
  TEdit *editCentreCode;
  TCSpinEdit *csePSTNNum;
  TListView *lvPSTN;
  TLabel *Label98;
  TEdit *editDialOutPreCode;
  TCheckBox *ckAutoDelMobilePreCode0;
  TCheckBox *ckAutoDelPreCode;
  TPanel *Panel66;
  TPanel *Panel65;
  TBitBtn *BitBtn15;
  TBitBtn *BitBtn16;
  TBitBtn *BitBtn17;
  TPanel *Panel67;
  TPanel *Panel68;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label23;
  TLabel *Label24;
  TComboBox *cbTS32;
  TComboBox *cbSelMode;
  TCSpinEdit *cseRouteNum;
  TListView *lvRoute;
  TCheckBox *ckControlCallModal;
  TLabel *Label99;
  TCheckBox *ckRecordTranOutCall;
  TLabel *Label100;
  TEdit *editWaitVocFile;
  TCheckBox *ckSendACDCallToAgent;
  TCheckBox *ckWriteSeatStatus;
  TCheckBox *ckHangonAutoCancelCall;
  TLabel *Label101;
  TCSpinEdit *cseMaxTranOutTalkTimeLen;
  TCheckBox *ckSP_InsertCDR;
  TLabel *Label102;
  TCSpinEdit *cseMinChangeDutyLen;
  TLabel *Label103;
  TEdit *editSeatRecPath;
  TLabel *Label104;
  TEdit *editSessions;
  TPopupMenu *PopupMenu6;
  TMenuItem *TAPI1;
  TMenuItem *N21;
  TMenuItem *N22;
  TMenuItem *N23;
  TPopupMenu *PopupMenu7;
  TMenuItem *N24;
  TMenuItem *N25;
  TImageList *ImageList4;
  TImageList *ImageList5;
  TImageList *ImageList6;
  TMenuItem *TAPI2;
  TMenuItem *TAPI3;
  TMenuItem *N26;
  TCheckBox *ckAutoDelDailOutPreCode;
  TPageControl *PageControl1;
  TTabSheet *TabSheet3;
  TTabSheet *TabSheet4;
  TPanel *Panel50;
  TLabel *Label75;
  TLabel *lbCTILinkType;
  TLabel *lbSwitchIP;
  TLabel *lbLoginID;
  TLabel *lbPassword;
  TLabel *lbServerID;
  TEdit *editSwitchPortNum;
  TButton *btTAPI;
  TComboBox *cbCTILinkType;
  TEdit *editSwitchIP;
  TEdit *editLoginID;
  TEdit *editPassword;
  TEdit *editServerID;
  TButton *btUnistallTAPI;
  TListView *lvSwitchPort;
  TPanel *plCreateTAPI;
  TLabel *Label105;
  TLabel *lbTAPIExtrn;
  TProgressBar *ProgressBar1;
  TLabel *Label106;
  TLabel *Label107;
  TLabel *Label108;
  TLabel *Label109;
  TEdit *editSWTDialOutPreCode;
  TEdit *editSWTIPPreCode;
  TEdit *editNotAddPreCode;
  TEdit *editSWTPreCode;
  TCheckBox *ckAutoAddDailOutPreCode;
  TCheckBox *ckAutoDelDailOutPreCode1;
  TCheckBox *ckAutoAddTranOutPreCode;
  TLabel *Label110;
  TCheckBox *ckAutoDelDailOutIPCode;
  TCheckBox *ckAutoAddDailOutIPCode;
  TCheckBox *ckCDRSwapCallerCalledNo;
  TLabel *Label111;
  TLabel *Label112;
  TComboBox *cbMultiRunMode;
  TComboBox *cbHostType;
  TLabel *Label113;
  TEdit *editLocaIVRIP;
  TLabel *lbSwitchover;
  TLabel *Label114;
  TEdit *editMasterIP;
  TGroupBox *GroupBox17;
  TLabel *Label115;
  TImage *imgVOCServiceStatus;
  TButton *btInstallVOCservice;
  TButton *btRefreshVOCServiceStatus;
  TButton *btStartVOCService;
  TButton *btStopVOCService;
  TLabel *Label116;
  TComboBox *cbTTSType;
  TGroupBox *GroupBox18;
  TLabel *Label117;
  TImage *imgWSServiceStatus;
  TButton *btInstallWSservice;
  TButton *btRefreshWSServiceStatus;
  TButton *btStartWSService;
  TButton *btStopWSService;
  void __fastcall btCancelClick(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall btOKClick(TObject *Sender);
  void __fastcall cseIVRPORTChange(TObject *Sender);
  void __fastcall btApplyClick(TObject *Sender);
  void __fastcall Button4Click(TObject *Sender);
  void __fastcall lvFlwSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvAccCodeSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall editFLWPATHChange(TObject *Sender);
  void __fastcall lvPSTNSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvRouteSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvSeatSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall editMemVocPathChange(TObject *Sender);
  void __fastcall cbCardTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall lvExtSeatSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
  void __fastcall btStartIVRServiceClick(TObject *Sender);
  void __fastcall btStopIVRServiceClick(TObject *Sender);
  void __fastcall btRefreshIVRServiceStatusClick(TObject *Sender);
  void __fastcall btStartFLWServiceClick(TObject *Sender);
  void __fastcall btStopFLWServiceClick(TObject *Sender);
  void __fastcall btRefreshFLWServiceStatusClick(TObject *Sender);
  void __fastcall Timer1Timer(TObject *Sender);
  void __fastcall ckGetchnstatusClick(TObject *Sender);
  void __fastcall ckGetagentstatusClick(TObject *Sender);
  void __fastcall ckGetqueueinfoClick(TObject *Sender);
  void __fastcall ckGetivrtracemsgClick(TObject *Sender);
  void __fastcall ckGetgetloadedflwsClick(TObject *Sender);
  void __fastcall ckGetgetflwtracemsgClick(TObject *Sender);
  void __fastcall lvAgStatusSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvChnStatusSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall N7Click(TObject *Sender);
  void __fastcall N8Click(TObject *Sender);
  void __fastcall N9Click(TObject *Sender);
  void __fastcall N10Click(TObject *Sender);
  void __fastcall N11Click(TObject *Sender);
  void __fastcall btClearIVRMsgClick(TObject *Sender);
  void __fastcall btClearFLWMsgClick(TObject *Sender);
  void __fastcall ckIVRAlartIdClick(TObject *Sender);
  void __fastcall lvLoadFlwSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall N12Click(TObject *Sender);
  void __fastcall ckLocaAlartIdClick(TObject *Sender);
  void __fastcall N16Click(TObject *Sender);
  void __fastcall N14Click(TObject *Sender);
  void __fastcall N13Click(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button24Click(TObject *Sender);
  void __fastcall Button25Click(TObject *Sender);
  void __fastcall ckGetTrkCountDataClick(TObject *Sender);
  void __fastcall ckGetAgCountDataClick(TObject *Sender);
  void __fastcall lvTrkCountDataSelectItem(TObject *Sender,
          TListItem *Item, bool Selected);
  void __fastcall lvAgCountDataSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvTrkCountDataDblClick(TObject *Sender);
  void __fastcall lvAgCountDataDblClick(TObject *Sender);
  void __fastcall cbCountCycleKeyPress(TObject *Sender, char &Key);
  void __fastcall cbTS32Change(TObject *Sender);
  void __fastcall btInstallFLWserviceClick(TObject *Sender);
  void __fastcall btInstallIVRserviceClick(TObject *Sender);
  void __fastcall Button27Click(TObject *Sender);
  void __fastcall Button26Click(TObject *Sender);
  void __fastcall N18Click(TObject *Sender);
  void __fastcall N19Click(TObject *Sender);
  void __fastcall cbSkinsChange(TObject *Sender);
  void __fastcall btInstallDBserviceClick(TObject *Sender);
  void __fastcall btRefreshDBServiceStatusClick(TObject *Sender);
  void __fastcall btStartDBServiceClick(TObject *Sender);
  void __fastcall btStopDBServiceClick(TObject *Sender);
  void __fastcall cbOSTypeChange(TObject *Sender);
  void __fastcall editIVRIPChange(TObject *Sender);
  void __fastcall rbAutoGetIPClick(TObject *Sender);
  void __fastcall rbStaticIPClick(TObject *Sender);
  void __fastcall rbAutoGetDNSClick(TObject *Sender);
  void __fastcall rbStaticDNSClick(TObject *Sender);
  void __fastcall editConnectIVRIPChange(TObject *Sender);
  void __fastcall btInstallSMSserviceClick(TObject *Sender);
  void __fastcall btRefreshSMSServiceStatusClick(TObject *Sender);
  void __fastcall btStartSMSServiceClick(TObject *Sender);
  void __fastcall btStopSMSServiceClick(TObject *Sender);
  void __fastcall btInstallDOCserviceClick(TObject *Sender);
  void __fastcall btRefreshDOCServiceStatusClick(TObject *Sender);
  void __fastcall btStartDOCServiceClick(TObject *Sender);
  void __fastcall btStopDOCServiceClick(TObject *Sender);
  void __fastcall lvParamSel1SelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvParamSel2SelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvParamSel3SelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvParamSel4SelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvParamSel5SelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall SpeedButton5Click(TObject *Sender);
  void __fastcall btLoginClick(TObject *Sender);
  void __fastcall btModifyPswClick(TObject *Sender);
  void __fastcall BitBtn1Click(TObject *Sender);
  void __fastcall BitBtn2Click(TObject *Sender);
  void __fastcall BitBtn3Click(TObject *Sender);
  void __fastcall BitBtn4Click(TObject *Sender);
  void __fastcall BitBtn5Click(TObject *Sender);
  void __fastcall lvParamSel1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
  void __fastcall btInstallWEBserviceClick(TObject *Sender);
  void __fastcall btRefreshWEBServiceStatusClick(TObject *Sender);
  void __fastcall btStartWEBServiceClick(TObject *Sender);
  void __fastcall btStopWEBServiceClick(TObject *Sender);
  void __fastcall FormResize(TObject *Sender);
  void __fastcall BitBtn6Click(TObject *Sender);
  void __fastcall BitBtn7Click(TObject *Sender);
  void __fastcall BitBtn8Click(TObject *Sender);
  void __fastcall BitBtn9Click(TObject *Sender);
  void __fastcall BitBtn10Click(TObject *Sender);
  void __fastcall BitBtn11Click(TObject *Sender);
  void __fastcall BitBtn21Click(TObject *Sender);
  void __fastcall BitBtn22Click(TObject *Sender);
  void __fastcall BitBtn23Click(TObject *Sender);
  void __fastcall BitBtn18Click(TObject *Sender);
  void __fastcall BitBtn19Click(TObject *Sender);
  void __fastcall BitBtn20Click(TObject *Sender);
  void __fastcall BitBtn15Click(TObject *Sender);
  void __fastcall BitBtn16Click(TObject *Sender);
  void __fastcall BitBtn17Click(TObject *Sender);
  void __fastcall BitBtn12Click(TObject *Sender);
  void __fastcall BitBtn13Click(TObject *Sender);
  void __fastcall BitBtn14Click(TObject *Sender);
  void __fastcall btInstallTTSserviceClick(TObject *Sender);
  void __fastcall btRefreshTTSServiceStatusClick(TObject *Sender);
  void __fastcall btStartTTSServiceClick(TObject *Sender);
  void __fastcall btStopTTSServiceClick(TObject *Sender);
  void __fastcall btInstallLINKserviceClick(TObject *Sender);
  void __fastcall btRefreshLINKServiceStatusClick(TObject *Sender);
  void __fastcall btStartLINKServiceClick(TObject *Sender);
  void __fastcall btStopLINKServiceClick(TObject *Sender);
  void __fastcall lvSwitchPortSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall BitBtn24Click(TObject *Sender);
  void __fastcall BitBtn25Click(TObject *Sender);
  void __fastcall BitBtn27Click(TObject *Sender);
  void __fastcall BitBtn26Click(TObject *Sender);
  void __fastcall cbSwitchTypeChange(TObject *Sender);
  void __fastcall cseSelVopNoChange(TObject *Sender);
  void __fastcall lvVopChnSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall BitBtn28Click(TObject *Sender);
  void __fastcall BitBtn29Click(TObject *Sender);
  void __fastcall BitBtn31Click(TObject *Sender);
  void __fastcall BitBtn30Click(TObject *Sender);
  void __fastcall ckVopOpenClick(TObject *Sender);
  void __fastcall BitBtn32Click(TObject *Sender);
  void __fastcall lvWorkerListSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvWorkerCallCountSelectItem(TObject *Sender,
          TListItem *Item, bool Selected);
  void __fastcall lvGroupListSelectItem(TObject *Sender, TListItem *Item,
          bool Selected);
  void __fastcall lvGroupCallCountSelectItem(TObject *Sender,
          TListItem *Item, bool Selected);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button5Click(TObject *Sender);
  void __fastcall Button6Click(TObject *Sender);
  void __fastcall Button7Click(TObject *Sender);
  void __fastcall lvWorkerListCustomDrawSubItem(TCustomListView *Sender,
          TListItem *Item, int SubItem, TCustomDrawState State,
          bool &DefaultDraw);
  void __fastcall lvWorkerCallCountCustomDrawSubItem(
          TCustomListView *Sender, TListItem *Item, int SubItem,
          TCustomDrawState State, bool &DefaultDraw);
  void __fastcall lvWorkerCallCountDblClick(TObject *Sender);
  void __fastcall lvWorkerListClick(TObject *Sender);
  void __fastcall lvGroupListCustomDrawSubItem(TCustomListView *Sender,
          TListItem *Item, int SubItem, TCustomDrawState State,
          bool &DefaultDraw);
  void __fastcall lvGroupCallCountCustomDrawSubItem(
          TCustomListView *Sender, TListItem *Item, int SubItem,
          TCustomDrawState State, bool &DefaultDraw);
  void __fastcall lvGroupListClick(TObject *Sender);
  void __fastcall lvGroupCallCountDblClick(TObject *Sender);
  void __fastcall ckBlockTrunkSoundClick(TObject *Sender);
  void __fastcall ckAgentLoginSoundClick(TObject *Sender);
  void __fastcall ckBusyTrunkSoundClick(TObject *Sender);
  void __fastcall ckAgentLevalSoundClick(TObject *Sender);
  void __fastcall ckACDWaitingSoundClick(TObject *Sender);
  void __fastcall ckBusyAgentSoundClick(TObject *Sender);
  void __fastcall ckBusyIVRSoundClick(TObject *Sender);
  void __fastcall ckSeatSatusSoundClick(TObject *Sender);
  void __fastcall cbSoundTypeChange(TObject *Sender);
  void __fastcall cbClearCallCountTypeChange(TObject *Sender);
  void __fastcall lvGroupListDblClick(TObject *Sender);
  void __fastcall csePSTNNumChange(TObject *Sender);
  void __fastcall cseVopNumChange(TObject *Sender);
  void __fastcall Panel4Resize(TObject *Sender);
  void __fastcall Panel2Resize(TObject *Sender);
  void __fastcall Panel14Resize(TObject *Sender);
  void __fastcall btTAPIClick(TObject *Sender);
  void __fastcall TAPI1Click(TObject *Sender);
  void __fastcall cbCTILinkTypeChange(TObject *Sender);
  void __fastcall N22Click(TObject *Sender);
  void __fastcall N23Click(TObject *Sender);
  void __fastcall N24Click(TObject *Sender);
  void __fastcall N25Click(TObject *Sender);
  void __fastcall TAPI2Click(TObject *Sender);
  void __fastcall TAPI3Click(TObject *Sender);
  void __fastcall btUnistallTAPIClick(TObject *Sender);
  void __fastcall editSwitchIPChange(TObject *Sender);
  void __fastcall cbCardTypeChange(TObject *Sender);
  void __fastcall cbHostTypeChange(TObject *Sender);
  void __fastcall btInstallVOCserviceClick(TObject *Sender);
  void __fastcall btRefreshVOCServiceStatusClick(TObject *Sender);
  void __fastcall btStartVOCServiceClick(TObject *Sender);
  void __fastcall btStopVOCServiceClick(TObject *Sender);
  void __fastcall btInstallWSserviceClick(TObject *Sender);
  void __fastcall btRefreshWSServiceStatusClick(TObject *Sender);
  void __fastcall btStartWSServiceClick(TObject *Sender);
  void __fastcall btStopWSServiceClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormMain(TComponent* Owner);
  void SetlvParamSelImage1(int nSelIndex);
  void SetlvParamSelImage2(int nSelIndex);
  void SetlvParamSelImage3(int nSelIndex);
  void SetlvParamSelImage4(int nSelIndex);
  void SetlvParamSelImage5(int nSelIndex);

  CMyToolsBox m_MyToolsBox;

  void GetServerIp();
  void GetServerGw();
  void GetServerDns();
  void WriteAlartMsg(LPCTSTR lpszFormat, ...);

  void WriteSkins(int nSkinId);
  void ProcLogMsg();
  void ProconrecvtxtlineMsg(CXMLMsg &XMLMsg);
  void ReLoadIVRIniFile();
  void ReLoadCTILINKIniFile();
  void ReLoadFLWIniFile();
  void ReLoadDBIniFile();
  void ReLoadIP();
  void ReLoadGW();
  void ReLoadDNS();
  void ProcOnLoginMsg(CXMLMsg &XMLMsg);
  void ProcOnivronoffMsg(CXMLMsg &XMLMsg);
  void ProcOnflwonoffMsg(CXMLMsg &XMLMsg);
  void InitDispChnStatus(int nMaxChnNum);
  void DispChnStatus(CXMLMsg &XMLMsg);
  void CountChnState();
  void DispVopStatus(CXMLMsg &XMLMsg);
  void CountVopState();
  void InitDispAgStatus(int nMaxAgNum);
  void ProconmodifyseatdataMsg(CXMLMsg &XMLMsg);
  void DispAgStatus(CXMLMsg &XMLMsg);
  void CountAgState();
  void DispAcdStatus(CXMLMsg &XMLMsg);
  void DispLoadFlw(CXMLMsg &XMLMsg);
  void DispSessionsCount(CXMLMsg &XMLMsg);
  void DispHARunStatus(CXMLMsg &XMLMsg);
  void DispMsg(TMemo *Memo, const char *msg);
  void SaveMsg(const char *filename, const char *msg);
  void ProcIVRMsg(int msgtype, const char *msg);
  void ProcFLWMsg(int msgtype, const char *msg);
  void ProcIniParam(CXMLMsg &XMLMsg);
  void ProcontrkcountdataMsg(CXMLMsg &XMLMsg);
  void ProconagcountdataMsg(CXMLMsg &XMLMsg);
  void Proconmodifypassword(CXMLMsg &XMLMsg);

  AnsiString IVRINIFile;
  AnsiString CTILINKINIFile;
  AnsiString FLWINIFile;
  AnsiString DBINIFile;
  AnsiString LOGINIFile; //配置文件名
  AnsiString TTSINIFile;

  AnsiString IPINIFile;
  AnsiString GWINIFile;
  AnsiString DNSINIFile;

  TIniFile *IVRIniKey;
  TIniFile *CTILINKIniKey;
  TIniFile *FLWIniKey;
  TIniFile *DBIniKey;
  TIniFile *LOGIniKey;
  TIniFile *TTSIniKey;

  int nCURPage;
  bool bIPParamChange;
  bool bTCPParamChange; //网络参数改动标志
  bool bFLWParamChange; //流程参数改动标志
  bool bPSTNParamChange; //模拟外线参数改动标志
  bool bROUTEParamChange; //呼出路由参数改动标志
  bool bSeatParamChange; //座席参数改动标志
  bool bExtSeatParamChange; //远端座席参数改动标志
  bool bOtherParamChange; //其他参数改动标志
  bool bSaveMsgIdParamChange; //服务保存消息开关参数改动标志
  bool bLOGParamChange; //监控器参数改动标志
  bool bAlartParamChange; //告警提醒参数改动标志
  bool bSwitchPortParamChange; //交换机端口参数改动标志
  bool bVopParamChange; //交换机资源节点参数改动标志

  void ReadLOGParam();
  void WriteLOGParam();
  void WriteLOGAutoLogin(AnsiString strUser, AnsiString strPsw, bool bAutoLoginId);

  void ReadAuthData();
  void ReadTcpParam();
  void WriteTcpParam();

  void WriteIPParam();

  CFlwFileList FlwFileList;
  void AddDispFlw(CFlwFile FlwFile);
  void DispFlw();
  void AddDispAccCode(CAccessCode AccessCode);
  void DispAccCode(CFlwFile FlwFile);
  void ReadFlwParam();
  void WriteFlwParam();

  CAPSTN PstnLine;
  void ReadPSTNParam();
  void WritePSTNParam();
  void DispPSTN();

  CSwitchPortList SwitchPortList;
  void ReadSwitchPortParam();
  void WriteSwitchPortParam();
  void DispSwitchPort();

  CRouteList RouteList;
  void ReadRouteParam();
  void WriteRouteParam();
  void DispRoute();

  CVopGroup VopGroup;
  void ReadVopParam();
  void WriteVopParam();
  void DispVop();

  CSeatList SeatList;
  void ReadSeatParam();
  void WriteSeatParam();
  void DispSeat();

  CExtSeatList ExtSeatList;
  void ReadExtSeatParam();
  void WriteExtSeatParam();
  void DispExtSeat();

  void ReadOtherParam();
  void WriteOtherParam();

  void ReadSaveMsgId();
  void WriteSaveMsgId();

  void ReadAlartParam();
  void WriteAlartParam();

  void InitTrkCountDispData(int nCountCycle);
  void InitAgCountDispData(int nCountCycle);

  CCallCountItemSetList WorkerCountItemSetList;
  CCallCountItemSetList GroupCountItemSetList;

  CAgentCallCountParamList WorkerCallCountParamList;
  CGroupCallCountParamList GroupCallCountParamList;

  void ReadWorkerCountItemSet();
  void WriteWorkerCountItemSet();
  void DispWorkerCountItemSet();
  void DispWorkerCountValue();
  void DispWorkerCountValue(int nItemNo);
  void DispSeatStatusData(CXMLMsg &XMLMsg);

  void ReadGroupCountItemSet();
  void WriteGroupCountItemSet();
  void DispGroupCountItemSet();
  void DispGroupStatusCountItemSet();
  void DispGroupCountValue();
  void DispGroupCountValue(int nItemNo);
  void DispGroupStatusData(CXMLMsg &XMLMsg);

  HINSTANCE hdll; //动态库句柄
  bool LoadTAPIDll();
  void ReleaseTAPIDll();

  void GetLoadedTSPList();
  long AddTAPIDrv(LPCSTR pszTSPFile, DWORD &dwProviderID);
  void RemoveTAPIDrv();
  void RemoveTAPIDrv(DWORD dwProviderID);
  int CreateTAPIDrv(int nDeviceID, DWORD &dwProviderID);
  void CreateTAPIRegLite(int nDeviceID);
  void CreateTAPIRegPro();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
