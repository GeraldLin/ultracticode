//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "route.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormRoute *FormRoute;
extern int g_CurRouteNo;
extern int gLangID;
extern void SendmodifyroutedataMsg(int nRouteNo, const char *pszChnList, int nSelMode, const char *pszIPPreCode);
//---------------------------------------------------------------------------
__fastcall TFormRoute::TFormRoute(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormRoute::SetModifyType(bool bmodify, int routeno, int selmode, AnsiString trkstr, AnsiString remark, AnsiString ipprecode)
{
  if (gLangID == 1)
  {
    if (bmodify == false)
    {
      bModify = bmodify;
      cseRouteNo->Value = FormMain->RouteList.RouteNum +1;
      Button1->Caption = "增加";
    }
    else
    {
      bModify = bmodify;
      cseRouteNo->Value = routeno;
      Button1->Caption = "修改";
    }
    switch (selmode)
    {
    case 0:
      cbSelMode->Text = "按顺序循环选择";
      break;
    case 1:
      cbSelMode->Text = "优先选奇数电路";
      break;
    case 2:
      cbSelMode->Text = "优先选偶数电路";
      break;
    case 3:
      cbSelMode->Text = "按路由顺序选择";
      break;
    case 4:
      cbSelMode->Text = "按路由逆序选择";
      break;
    case 5:
      cbSelMode->Text = "优先选择最先空闲通道";
      break;
    default:
      cbSelMode->Text = "按顺序循环选择";
      break;
    }
  }
  else
  {
    if (bmodify == false)
    {
      bModify = bmodify;
      cseRouteNo->Value = FormMain->RouteList.RouteNum +1;
      Button1->Caption = "糤";
    }
    else
    {
      bModify = bmodify;
      cseRouteNo->Value = routeno;
      Button1->Caption = "э";
    }
    switch (selmode)
    {
    case 0:
      cbSelMode->Text = "抖碻吏匡拒";
      break;
    case 1:
      cbSelMode->Text = "纔匡计硄笵";
      break;
    case 2:
      cbSelMode->Text = "纔匡案计硄笵";
      break;
    case 3:
      cbSelMode->Text = "隔パ抖匡拒";
      break;
    case 4:
      cbSelMode->Text = "隔パ癴匡拒";
      break;
    case 5:
      cbSelMode->Text = "纔匡拒程丁硄笵";
      break;
    default:
      cbSelMode->Text = "抖碻吏匡拒";
      break;
    }
  }
  editTrkStr->Text = trkstr;
  editRemark->Text = remark;
  editIPPreCode->Text = ipprecode;
  Button1->Enabled = false;
}

void __fastcall TFormRoute::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormRoute::cseRouteNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormRoute::Button1Click(TObject *Sender)
{
  int nResult, nTemp;

  if (gLangID == 1)
  {
    if (editTrkStr->Text == "")
    {
      MessageBox(NULL,"请设置路由中继线路群!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (cseRouteNo->Value == 0)
    {
      MessageBox(NULL,"请设置路由号!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (cbSelMode->Text == "按顺序循环选择")
      nTemp = 0;
    else if (cbSelMode->Text == "优先选奇数电路")
      nTemp = 1;
    else if (cbSelMode->Text == "优先选偶数电路")
      nTemp = 2;
    else if (cbSelMode->Text == "按路由顺序选择")
      nTemp = 3;
    else if (cbSelMode->Text == "按路由逆序选择")
      nTemp = 4;
    else if (cbSelMode->Text == "优先选择最先空闲通道")
      nTemp = 5;
    else
      nTemp = 0;
    if (bModify == false)
    {
      nResult = FormMain->RouteList.EditRoute(cseRouteNo->Value, nTemp, editTrkStr->Text, editRemark->Text, editIPPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyroutedataMsg(cseRouteNo->Value, editTrkStr->Text.c_str(), nTemp, editIPPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"增加路由成功,请按 应用 按钮才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->RouteList.RouteNum++;
        FormMain->bROUTEParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispRoute();
      }
      else
      {
        MessageBox(NULL,"增加路由失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->RouteList.EditRoute(cseRouteNo->Value, nTemp, editTrkStr->Text, editRemark->Text, editIPPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyroutedataMsg(cseRouteNo->Value, editTrkStr->Text.c_str(), nTemp, editIPPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"修改路由成功,请按 应用 按钮才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bROUTEParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispRoute();
      }
      else
      {
        MessageBox(NULL,"修改路由失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
  else
  {
    if (editTrkStr->Text == "")
    {
      MessageBox(NULL,"叫砞﹚隔パい膥竤舱!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (cseRouteNo->Value == 0)
    {
      MessageBox(NULL,"叫砞﹚隔パ絪腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (cbSelMode->Text == "抖碻吏匡拒")
      nTemp = 0;
    else if (cbSelMode->Text == "纔匡计硄笵")
      nTemp = 1;
    else if (cbSelMode->Text == "纔匡案计硄笵")
      nTemp = 2;
    else if (cbSelMode->Text == "隔パ抖匡拒")
      nTemp = 3;
    else if (cbSelMode->Text == "隔パ癴匡拒")
      nTemp = 4;
    else if (cbSelMode->Text == "纔匡拒程丁硄笵")
      nTemp = 5;
    else
      nTemp = 0;
    if (bModify == false)
    {
      nResult = FormMain->RouteList.EditRoute(cseRouteNo->Value, nTemp, editTrkStr->Text, editRemark->Text, editIPPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyroutedataMsg(cseRouteNo->Value, editTrkStr->Text.c_str(), nTemp, editIPPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"糤隔パΘ,叫 甅ノ 秙ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->RouteList.RouteNum++;
        FormMain->bROUTEParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispRoute();
      }
      else
      {
        MessageBox(NULL,"糤隔パア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->RouteList.EditRoute(cseRouteNo->Value, nTemp, editTrkStr->Text, editRemark->Text, editIPPreCode->Text);
      if (nResult == 0)
      {
        //SendmodifyroutedataMsg(cseRouteNo->Value, editTrkStr->Text.c_str(), nTemp, editIPPreCode->Text.c_str());
        this->Close();
        MessageBox(NULL,"э隔パΘ,叫 甅ノ 秙ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bROUTEParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispRoute();
      }
      else
      {
        MessageBox(NULL,"э隔パア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
}
//---------------------------------------------------------------------------

