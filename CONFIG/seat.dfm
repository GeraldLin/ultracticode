object FormSeat: TFormSeat
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #24231#24109#21443#25976#35373#23450
  ClientHeight = 518
  ClientWidth = 453
  Color = clBtnFace
  Font.Charset = CHINESEBIG5_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 110
    Top = 14
    Width = 78
    Height = 12
    Caption = #20998#27231#36890#36947#24207#34399':'
  end
  object Label2: TLabel
    Left = 110
    Top = 40
    Width = 78
    Height = 12
    Caption = #24231#24109#20998#27231#34399#30908':'
  end
  object Label3: TLabel
    Left = 135
    Top = 65
    Width = 54
    Height = 12
    Caption = #24231#24109#39006#22411':'
  end
  object Label4: TLabel
    Left = 98
    Top = 91
    Width = 90
    Height = 12
    Caption = #38928#35373#24231#24109#32676#32068#34399':'
  end
  object Label5: TLabel
    Left = 75
    Top = 118
    Width = 114
    Height = 12
    Caption = #38928#35373#35441#22519#27231#21729#32676#32068#34399':'
  end
  object Label6: TLabel
    Left = 99
    Top = 145
    Width = 90
    Height = 12
    Caption = #38928#35373#22519#27231#21729#32026#21029':'
  end
  object Label7: TLabel
    Left = 100
    Top = 170
    Width = 90
    Height = 12
    Caption = #38928#35373#22519#27231#21729#32232#34399':'
  end
  object Label8: TLabel
    Left = 33
    Top = 312
    Width = 366
    Height = 12
    Caption = #27880': '#31995#32113#21855#21205#26178','#33258#21205#30331#32160#22519#24190#21729','#38656#38928#35373#22519#27231#21729#32232#34399#28858#21644#22995#21517#12290
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #32048#26126#39636
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 98
    Top = 194
    Width = 90
    Height = 12
    Caption = #38928#35373#22519#27231#21729#22995#21517':'
  end
  object Label10: TLabel
    Left = 87
    Top = 243
    Width = 102
    Height = 12
    Caption = #24231#24109#32129#23450#30340'IP'#20301#22336':'
  end
  object Label11: TLabel
    Left = 111
    Top = 445
    Width = 78
    Height = 12
    Caption = #25209#37327#22686#21152#25976#37327':'
  end
  object Label12: TLabel
    Left = 106
    Top = 338
    Width = 84
    Height = 12
    Caption = #30331#32160'PBX'#30340#23494#30908':'
  end
  object Label13: TLabel
    Left = 90
    Top = 362
    Width = 102
    Height = 12
    Caption = #30331#32160'PBX'#30340'ACD'#32676#32068':'
  end
  object Label14: TLabel
    Left = 85
    Top = 218
    Width = 102
    Height = 12
    Caption = #20998#27231#25152#22312#37096#38272#32232#34399':'
  end
  object Label15: TLabel
    Left = 85
    Top = 266
    Width = 102
    Height = 12
    Caption = #24231#24109#26412#22320#24066#35441#21312#34399':'
  end
  object editPort: TEdit
    Left = 199
    Top = 9
    Width = 131
    Height = 20
    Color = clGrayText
    Enabled = False
    TabOrder = 0
    Text = '0'
  end
  object editSeatNo: TEdit
    Left = 199
    Top = 35
    Width = 131
    Height = 20
    TabOrder = 1
    OnChange = editSeatNoChange
  end
  object cbSeatType: TComboBox
    Left = 199
    Top = 61
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 2
    Text = #20998#27231#38651#35441#24231#24109
    OnChange = editSeatNoChange
    OnKeyPress = cbSeatTypeKeyPress
    Items.Strings = (
      #20998#27231#38651#35441#24231#24109
      #20998#27231#38651#33126#24231#24109)
  end
  object cseSeatGroupNo: TCSpinEdit
    Left = 199
    Top = 87
    Width = 131
    Height = 21
    MaxValue = 31
    TabOrder = 3
    OnChange = editSeatNoChange
  end
  object cseWorkerLevel: TCSpinEdit
    Left = 199
    Top = 139
    Width = 131
    Height = 21
    MaxValue = 15
    TabOrder = 4
    OnChange = editSeatNoChange
  end
  object editWorkerNo: TEdit
    Left = 199
    Top = 165
    Width = 131
    Height = 20
    TabOrder = 5
    OnChange = editSeatNoChange
  end
  object Button1: TButton
    Left = 95
    Top = 475
    Width = 82
    Height = 27
    Caption = #30906#23450
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 269
    Top = 475
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 7
    OnClick = Button2Click
  end
  object editWorkerName: TEdit
    Left = 199
    Top = 190
    Width = 131
    Height = 20
    TabOrder = 8
    OnChange = editSeatNoChange
  end
  object editSeatIP: TEdit
    Left = 199
    Top = 239
    Width = 131
    Height = 20
    TabOrder = 9
    OnChange = editSeatNoChange
  end
  object ckBatch: TCheckBox
    Left = 199
    Top = 406
    Width = 114
    Height = 18
    Caption = #26159#21542#25209#37327#22686#21152#65311
    TabOrder = 10
    OnClick = ckBatchClick
  end
  object cseBatchNum: TCSpinEdit
    Left = 199
    Top = 440
    Width = 131
    Height = 21
    MaxValue = 1024
    MinValue = 1
    TabOrder = 11
    Value = 1
  end
  object ckAutoLogin: TCheckBox
    Left = 88
    Top = 288
    Width = 273
    Height = 17
    Caption = #24179#21488#36939#34892#21518','#26159#21542#33258#21205#30331#32160#38928#35373#30340#22519#27231#21729'?'
    Checked = True
    State = cbChecked
    TabOrder = 12
    OnClick = editSeatNoChange
  end
  object editWorkerGroupNo: TEdit
    Left = 199
    Top = 114
    Width = 131
    Height = 20
    TabOrder = 13
    OnChange = editSeatNoChange
  end
  object ckLoginSwitchID: TCheckBox
    Left = 88
    Top = 384
    Width = 273
    Height = 17
    Caption = #22519#27231#21729#30331#32160#26178','#26159#21542#33258#21205#30331#32160#21040'PBX'#30340'ACD'#32676#32068'?'
    Checked = True
    State = cbChecked
    TabOrder = 14
    OnClick = editSeatNoChange
  end
  object editLoginSwitchPSW: TEdit
    Left = 199
    Top = 334
    Width = 131
    Height = 20
    TabOrder = 15
    OnChange = editSeatNoChange
  end
  object editLoginSwitchGroupID: TEdit
    Left = 199
    Top = 358
    Width = 131
    Height = 20
    TabOrder = 16
    OnChange = editSeatNoChange
  end
  object ckRecordId: TCheckBox
    Left = 88
    Top = 408
    Width = 97
    Height = 17
    Caption = #26159#21542#32160#38899'?'
    TabOrder = 17
  end
  object editDepartmentNo: TEdit
    Left = 199
    Top = 214
    Width = 131
    Height = 20
    TabOrder = 18
    OnChange = editSeatNoChange
  end
  object editLocaAreaCode: TEdit
    Left = 199
    Top = 262
    Width = 131
    Height = 20
    TabOrder = 19
    OnChange = editSeatNoChange
  end
end
