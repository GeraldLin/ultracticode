object FormModifyPSW: TFormModifyPSW
  Left = 192
  Top = 114
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #20462#25913#30331#37636#23494#30908
  ClientHeight = 184
  ClientWidth = 390
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 33
    Top = 65
    Width = 120
    Height = 12
    Caption = #38656#35201#20462#25913#23494#30908#30340#24115#34399#65306
  end
  object Label2: TLabel
    Left = 82
    Top = 22
    Width = 72
    Height = 12
    Caption = #26412#20154#30340#23494#30908#65306
  end
  object Label3: TLabel
    Left = 108
    Top = 100
    Width = 48
    Height = 12
    Caption = #26032#23494#30908#65306
  end
  object cbUserName: TComboBox
    Left = 165
    Top = 61
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    Text = 'administrator'
    OnKeyPress = cbUserNameKeyPress
    Items.Strings = (
      'administrator'
      'manager'
      'operator'
      'monitor'
      'board')
  end
  object editOldPassword: TEdit
    Left = 165
    Top = 17
    Width = 131
    Height = 20
    PasswordChar = '*'
    TabOrder = 1
  end
  object Button1: TButton
    Left = 52
    Top = 139
    Width = 81
    Height = 27
    Caption = #20462#25913
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 269
    Top = 139
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editNewPassword: TEdit
    Left = 165
    Top = 95
    Width = 131
    Height = 20
    PasswordChar = '*'
    TabOrder = 4
  end
end
