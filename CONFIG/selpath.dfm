object FormPath: TFormPath
  Left = 192
  Top = 114
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #36984#25799#25991#20214#22846
  ClientHeight = 407
  ClientWidth = 278
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 9
    Top = 9
    Width = 66
    Height = 12
    Caption = #35531#36984#25799#30436#31526':'
  end
  object Label2: TLabel
    Left = 9
    Top = 52
    Width = 78
    Height = 12
    Caption = #35531#36984#25799#25991#20214#22846':'
  end
  object DriveComboBox1: TDriveComboBox
    Left = 9
    Top = 26
    Width = 261
    Height = 18
    DirList = DirectoryListBox1
    TabOrder = 0
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 9
    Top = 69
    Width = 261
    Height = 296
    ItemHeight = 16
    TabOrder = 1
  end
  object Button1: TButton
    Left = 26
    Top = 373
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 165
    Top = 373
    Width = 81
    Height = 27
    Caption = #25918#26820
    TabOrder = 3
    OnClick = Button2Click
  end
end
