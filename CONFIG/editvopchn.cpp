//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editvopchn.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditVopChn *FormEditVopChn;
extern int gLangID;
//---------------------------------------------------------------------------
__fastcall TFormEditVopChn::TFormEditVopChn(TComponent* Owner)
  : TForm(Owner)
{
  nModify = 0;
}
//---------------------------------------------------------------------------
void TFormEditVopChn::AddPortIDItemList(int nVopchnType)
{
  cbPortID->Items->Clear();
  cbPortID->Items->Add((gLangID == 1) ? "未启用" : "ゼ币ノ");
  for (int i=0; i<FormMain->SwitchPortList.SwitchPortNum; i++)
  {
    if ((nVopchnType == 0 || nVopchnType == 1) && FormMain->SwitchPortList.SwitchPort[i].m_nLgChType == 3)
      cbPortID->Items->Add(FormMain->SwitchPortList.SwitchPort[i].m_strPortID);
    else if (nVopchnType == 2 && FormMain->SwitchPortList.SwitchPort[i].m_nLgChType == 4)
      cbPortID->Items->Add(FormMain->SwitchPortList.SwitchPort[i].m_strPortID);
    if ((nVopchnType == 3 || nVopchnType == 4 || nVopchnType == 5) && FormMain->SwitchPortList.SwitchPort[i].m_nLgChType == 2)
      cbPortID->Items->Add(FormMain->SwitchPortList.SwitchPort[i].m_strPortID);
    if ((nVopchnType == 6 || nVopchnType == 7) && FormMain->SwitchPortList.SwitchPort[i].m_nLgChType == 1)
      cbPortID->Items->Add(FormMain->SwitchPortList.SwitchPort[i].m_strPortID);
    if ((nVopchnType == 8) && FormMain->SwitchPortList.SwitchPort[i].m_nLgChType == 1)
      cbPortID->Items->Add(FormMain->SwitchPortList.SwitchPort[i].m_strPortID);
  }
}
void TFormEditVopChn::SetModifyType(int nmodify, CVopChn &vopchn)
{
  AddPortIDItemList(vopchn.m_nVopChnType);
  nModify = nmodify;
  m_VopChn = vopchn;
  if (nmodify == 1)
  {
    //增加
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = true;
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
  else if (nmodify == 2)
  {
    //插入
    Button1->Caption = (gLangID == 1) ? "插入" : "础";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = true;
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
  else
  {
    //修改
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = false;
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
  editVopNo->Text = vopchn.m_nVopNo;
  editVopChnNo->Text = vopchn.m_nVopChnNo;

  switch (vopchn.m_nVopChnType)
  {
    case 0:
    case 1:
      cbVopChnType->Text = (gLangID == 1) ? "IVR资源" : "IVR硄笵";
      break;
    case 2:
      cbVopChnType->Text = (gLangID == 1) ? "传真资源" : "肚痷硄笵";
      break;
    case 3:
      cbVopChnType->Text = (gLangID == 1) ? "模拟话机座席监录" : "摸ゑ杠诀畒畊菏魁";
      break;
    case 4:
      cbVopChnType->Text = (gLangID == 1) ? "数字话机座席监录" : "计杠诀畒畊菏魁";
      break;
    case 5:
      cbVopChnType->Text = (gLangID == 1) ? "IP座席监录" : "IP畒畊菏魁";
      break;
    case 6:
      cbVopChnType->Text = (gLangID == 1) ? "模拟中继监录" : "摸ゑい膥菏魁";
      break;
    case 7:
      cbVopChnType->Text = (gLangID == 1) ? "数字中继监录" : "计い膥菏魁";
      break;
    case 8:
      cbVopChnType->Text = (gLangID == 1) ? "语音中继" : "粂い膥";
      break;
    default:
      cbVopChnType->Text = (gLangID == 1) ? "未知" : "ゼ";
      break;
  }
  if (vopchn.m_strPortID == "")
    cbPortID->Text = (gLangID == 1) ? "未启用" : "ゼ币ノ";
  else
    cbPortID->Text = vopchn.m_strPortID;

  Button1->Enabled = false;
}

void __fastcall TFormEditVopChn::cbVopChnTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditVopChn::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditVopChn::cbVopChnTypeChange(TObject *Sender)
{
  if (gLangID == 1)
  {
    if (cbVopChnType->Text == "IVR资源")
      m_VopChn.m_nVopChnType = 0;
    else if (cbVopChnType->Text == "传真资源")
      m_VopChn.m_nVopChnType = 2;
    else if (cbVopChnType->Text == "模拟话机坐席监录")
      m_VopChn.m_nVopChnType = 3;
    else if (cbVopChnType->Text == "数字话机坐席监录")
      m_VopChn.m_nVopChnType = 4;
    else if (cbVopChnType->Text == "IP坐席监录")
      m_VopChn.m_nVopChnType = 5;
    else if (cbVopChnType->Text == "模拟中继监录")
      m_VopChn.m_nVopChnType = 6;
    else if (cbVopChnType->Text == "数字中继监录")
      m_VopChn.m_nVopChnType = 7;
    else if (cbVopChnType->Text == "语音中继")
      m_VopChn.m_nVopChnType = 8;
  }
  else
  {
    if (cbVopChnType->Text == "IVR硄笵")
      m_VopChn.m_nVopChnType = 0;
    else if (cbVopChnType->Text == "肚痷硄笵")
      m_VopChn.m_nVopChnType = 2;
    else if (cbVopChnType->Text == "摸ゑ杠诀畒畊菏魁")
      m_VopChn.m_nVopChnType = 3;
    else if (cbVopChnType->Text == "计杠诀畒畊菏魁")
      m_VopChn.m_nVopChnType = 4;
    else if (cbVopChnType->Text == "IP畒畊菏魁")
      m_VopChn.m_nVopChnType = 5;
    else if (cbVopChnType->Text == "摸ゑい膥菏魁")
      m_VopChn.m_nVopChnType = 6;
    else if (cbVopChnType->Text == "计い膥菏魁")
      m_VopChn.m_nVopChnType = 7;
    else if (cbVopChnType->Text == "粂い膥")
      m_VopChn.m_nVopChnType = 8;
  }

  AddPortIDItemList(m_VopChn.m_nVopChnType);
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditVopChn::Button1Click(TObject *Sender)
{
  char dispbuf[256];
  int nResult, count=0;

  if (gLangID == 1)
  {
    if (cbVopChnType->Text == "IVR资源")
      m_VopChn.m_nVopChnType = 0;
    else if (cbVopChnType->Text == "传真资源")
      m_VopChn.m_nVopChnType = 2;
    else if (cbVopChnType->Text == "模拟话机座席监录")
      m_VopChn.m_nVopChnType = 3;
    else if (cbVopChnType->Text == "数字话机座席监录")
      m_VopChn.m_nVopChnType = 4;
    else if (cbVopChnType->Text == "IP座席监录")
      m_VopChn.m_nVopChnType = 5;
    else if (cbVopChnType->Text == "模拟中继监录")
      m_VopChn.m_nVopChnType = 6;
    else if (cbVopChnType->Text == "数字中继监录")
      m_VopChn.m_nVopChnType = 7;
    else if (cbVopChnType->Text == "语音中继")
      m_VopChn.m_nVopChnType = 8;
  }
  else
  {
    if (cbVopChnType->Text == "IVR硄笵")
      m_VopChn.m_nVopChnType = 0;
    else if (cbVopChnType->Text == "肚痷硄笵")
      m_VopChn.m_nVopChnType = 2;
    else if (cbVopChnType->Text == "摸ゑ杠诀畒畊菏魁")
      m_VopChn.m_nVopChnType = 3;
    else if (cbVopChnType->Text == "计杠诀畒畊菏魁")
      m_VopChn.m_nVopChnType = 4;
    else if (cbVopChnType->Text == "IP畒畊菏魁")
      m_VopChn.m_nVopChnType = 5;
    else if (cbVopChnType->Text == "摸ゑい膥菏魁")
      m_VopChn.m_nVopChnType = 6;
    else if (cbVopChnType->Text == "计い膥菏魁")
      m_VopChn.m_nVopChnType = 7;
    else if (cbVopChnType->Text == "粂い膥")
      m_VopChn.m_nVopChnType = 8;
  }

  m_VopChn.m_PortNo = FormMain->SwitchPortList.GetPortNo(cbPortID->Text);
  if (m_VopChn.m_PortNo >= 0)
  {
    m_VopChn.m_strPortID = cbPortID->Text;
  }
  else
  {
    m_VopChn.m_strPortID = "";
  }
  if (nModify == 1)
  {
    //增加
    if (ckBatch->Checked == true)
    {
      for (int i=0; i<cseBatchNum->Value; i++)
      {
        nResult = FormMain->VopGroup.m_Vops[m_VopChn.m_nVopNo].AddVopChn(m_VopChn);
        if (nResult == 0)
        {
          count++;
          m_VopChn.m_PortNo++;
          m_VopChn.m_nVopChnNo++;
        }
        else
        {
          break;
        }
      }
      if (count > 0)
      {
        this->Close();
        if (gLangID == 1)
        {
          sprintf(dispbuf, "共增加了 %d 个资源通道，请按 应用 键修改后的参数才能生效!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
        }
        else
        {
          sprintf(dispbuf, "糤 %d 场IVR硄笵叫 甅ノ 龄э把计ネ!", count);
          MessageBox(NULL,dispbuf,"獺矗ボ",MB_OK|MB_ICONINFORMATION);
        }
        FormMain->bVopParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispVop();
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"增加交换机资源通道失败,可能是超过限定的通道数量!","信息提示",MB_OK|MB_ICONINFORMATION);
        else
          MessageBox(NULL,"糤ユ传场IVR方硄笵ア毖,琌禬筁﹚硄笵计秖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
      return;
    }
    nResult = FormMain->VopGroup.m_Vops[m_VopChn.m_nVopNo].AddVopChn(m_VopChn);
    if (nResult == 0)
    {
      this->Close();
      if (gLangID == 1)
        MessageBox(NULL,"增加交换机资源通道成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"糤ユ传诀场IVR硄笵Θ叫 莱ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      FormMain->bVopParamChange = true;
      FormMain->btApply->Enabled = true;
      FormMain->DispVop();
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"增加交换机资源通道失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"糤ユ传诀场IVR硄笵ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    }
  }
  else if (nModify == 2)
  {
    //插入
    if (ckBatch->Checked == true)
    {
      for (int i=0; i<cseBatchNum->Value; i++)
      {
        nResult = FormMain->VopGroup.m_Vops[m_VopChn.m_nVopNo].InsertVopChn(m_VopChn.m_nVopChnNo, m_VopChn);
        if (nResult == 0)
        {
          count++;
          m_VopChn.m_PortNo++;
          m_VopChn.m_nVopChnNo++;
        }
        else
        {
          break;
        }
      }
      if (count > 0)
      {
        this->Close();
        if (gLangID == 1)
        {
          sprintf(dispbuf, "共插入了 %d 个资源通道，请按 应用 键修改后的参数才能生效!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
        }
        else
        {
          sprintf(dispbuf, "础 %d 场IVR硄笵叫 甅ノ 龄э把计ネ!", count);
          MessageBox(NULL,dispbuf,"獺矗ボ",MB_OK|MB_ICONINFORMATION);
        }
        FormMain->bVopParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispVop();
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"插入交换机资源通道失败,可能是超过限定的通道数量!","信息提示",MB_OK|MB_ICONINFORMATION);
        else
          MessageBox(NULL,"础ユ传诀场IVR硄笵ア毖,琌禬筁﹚硄笵计秖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
      return;
    }
    nResult = FormMain->VopGroup.m_Vops[m_VopChn.m_nVopNo].InsertVopChn(m_VopChn.m_nVopChnNo, m_VopChn);
    if (nResult == 0)
    {
      this->Close();
      if (gLangID == 1)
        MessageBox(NULL,"插入交换机资源通道成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"础ユ传诀场IVR硄笵Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      FormMain->bVopParamChange = true;
      FormMain->btApply->Enabled = true;
      FormMain->DispVop();
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"插入交换机资源通道失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"础ユ传诀场IVR硄笵ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    }
  }
  else
  {
    //修改
    nResult = FormMain->VopGroup.m_Vops[m_VopChn.m_nVopNo].EditVopChn(m_VopChn.m_nVopChnNo, m_VopChn);
    if (nResult == 0)
    {
      this->Close();
      if (gLangID == 1)
        MessageBox(NULL,"修改交换机资源通道成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"эユ传诀场IVR硄笵Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      FormMain->bVopParamChange = true;
      FormMain->btApply->Enabled = true;
      FormMain->DispVop();
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"修改交换机资源通道失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"эユ传诀场IVR硄笵ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditVopChn::cbPortIDChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormEditVopChn::ckBatchClick(TObject *Sender)
{
  if (ckBatch->Checked == true)
  {
    Label6->Visible = true;
    cseBatchNum->Visible = true;
  }
  else
  {
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
}
//---------------------------------------------------------------------------

