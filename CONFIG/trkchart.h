//---------------------------------------------------------------------------

#ifndef trkchartH
#define trkchartH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//---------------------------------------------------------------------------
class TFormTrkChart : public TForm
{
__published:	// IDE-managed Components
  TChart *Chart1;
  TBarSeries *Series1;
  TPanel *Panel1;
  TButton *Button1;
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormTrkChart(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormTrkChart *FormTrkChart;
//---------------------------------------------------------------------------
#endif
