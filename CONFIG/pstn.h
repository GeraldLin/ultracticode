//---------------------------------------------------------------------------

#ifndef pstnH
#define pstnH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormPSTN : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editPSTNCode;
  TEdit *editPreCode;
  TButton *Button1;
  TButton *Button2;
  TCSpinEdit *csePSTNPort;
  TLabel *Label5;
  TEdit *editIPPreCode;
  TLabel *Label6;
  TEdit *editNotAddPreCode;
  TGroupBox *GroupBox1;
  TMemo *Memo1;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editPSTNCodeChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormPSTN(TComponent* Owner);

  bool bModify;
  void SetModifyType(bool bmodify, int lineno=0, AnsiString pstncode="", AnsiString precode="", AnsiString ipprecode="", AnsiString notaddprecode="");
};
//---------------------------------------------------------------------------
extern PACKAGE TFormPSTN *FormPSTN;
//---------------------------------------------------------------------------
#endif
