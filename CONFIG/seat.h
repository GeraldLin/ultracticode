//---------------------------------------------------------------------------

#ifndef seatH
#define seatH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormSeat : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TEdit *editPort;
  TEdit *editSeatNo;
  TComboBox *cbSeatType;
  TCSpinEdit *cseSeatGroupNo;
  TCSpinEdit *cseWorkerLevel;
  TEdit *editWorkerNo;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label8;
  TLabel *Label9;
  TEdit *editWorkerName;
  TLabel *Label10;
  TEdit *editSeatIP;
  TCheckBox *ckBatch;
  TLabel *Label11;
  TCSpinEdit *cseBatchNum;
  TCheckBox *ckAutoLogin;
  TEdit *editWorkerGroupNo;
  TCheckBox *ckLoginSwitchID;
  TLabel *Label12;
  TEdit *editLoginSwitchPSW;
  TLabel *Label13;
  TEdit *editLoginSwitchGroupID;
  TCheckBox *ckRecordId;
  TLabel *Label14;
  TEdit *editDepartmentNo;
  TLabel *Label15;
  TEdit *editLocaAreaCode;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSeatNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbSeatTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall ckBatchClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormSeat(TComponent* Owner);

  int nModify; //修改类型：1-增加 2-插入 3-修改
  void SetModifyType(int nmodify, CSeat &seat);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSeat *FormSeat;
//---------------------------------------------------------------------------
#endif
