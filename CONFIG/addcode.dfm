object FormAddCode: TFormAddCode
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #22686#21152#25765#20837#34399#30908
  ClientHeight = 166
  ClientWidth = 264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 19
    Width = 54
    Height = 12
    Caption = #25765#20837#34399#30908':'
  end
  object Label2: TLabel
    Left = 16
    Top = 52
    Width = 78
    Height = 12
    Caption = #26368#23567#34399#30908#38263#24230':'
  end
  object Label3: TLabel
    Left = 16
    Top = 84
    Width = 78
    Height = 12
    Caption = #26368#22823#34399#30908#38263#24230':'
  end
  object editAccCode: TEdit
    Left = 120
    Top = 16
    Width = 121
    Height = 20
    TabOrder = 0
    OnChange = editAccCodeChange
    OnExit = editAccCodeExit
  end
  object cseMinLen: TCSpinEdit
    Left = 120
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = editAccCodeChange
  end
  object cseMaxLen: TCSpinEdit
    Left = 120
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 2
    OnChange = editAccCodeChange
  end
  object Button1: TButton
    Left = 32
    Top = 128
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 168
    Top = 128
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 4
    OnClick = Button2Click
  end
end
