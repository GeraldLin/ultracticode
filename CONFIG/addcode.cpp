//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "addcode.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormAddCode *FormAddCode;
extern int gLangID;
extern int g_CurSelFlw;
extern int gLangID;
extern void SendAddAccessCodeMsg(int nFlwNo, const char *pszCode, int nMinLen, int nMaxLen);
//---------------------------------------------------------------------------
__fastcall TFormAddCode::TFormAddCode(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormAddCode::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormAddCode::editAccCodeChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------
void __fastcall TFormAddCode::editAccCodeExit(TObject *Sender)
{
  cseMinLen->Value = editAccCode->Text.Length();
  cseMaxLen->Value = editAccCode->Text.Length();
}
//---------------------------------------------------------------------------
void __fastcall TFormAddCode::Button1Click(TObject *Sender)
{
  if (editAccCode->Text == "")
  {
    if (gLangID == 1)
      MessageBox(NULL,"请设置接入号码!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫砞﹚挤腹絏!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  SendAddAccessCodeMsg(g_CurSelFlw, editAccCode->Text.c_str(), cseMinLen->Value, cseMaxLen->Value);
  this->Close();
}
//---------------------------------------------------------------------------
