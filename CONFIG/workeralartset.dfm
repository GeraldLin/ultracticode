object FormWorkerAlartSet: TFormWorkerAlartSet
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #35373#23450#21578#35686#25552#31034#21443#25976
  ClientHeight = 243
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 53
    Top = 68
    Width = 72
    Height = 12
    Caption = #21578#35686#38272#38480#20540#65306
  end
  object Label2: TLabel
    Left = 56
    Top = 156
    Width = 60
    Height = 12
    Caption = #21578#35686#38991#33394#65306
  end
  object lblItemName: TLabel
    Left = 8
    Top = 8
    Width = 66
    Height = 12
    Caption = 'lblItemName'
  end
  object ckAlartId: TCheckBox
    Left = 56
    Top = 40
    Width = 113
    Height = 17
    Caption = #26159#21542#25171#38283#21578#35686#65311
    TabOrder = 0
    OnClick = ckAlartIdClick
  end
  object rgCompType: TRadioGroup
    Left = 56
    Top = 96
    Width = 172
    Height = 49
    Caption = #27604#36611#26041#27861#65306
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      #31561#20110
      #22823#20110
      #23567#20110)
    TabOrder = 1
    OnClick = ckAlartIdClick
  end
  object ckAlartSound: TCheckBox
    Left = 56
    Top = 184
    Width = 113
    Height = 17
    Caption = #25171#38283#32882#38899#21578#35686#65311
    TabOrder = 2
    OnClick = ckAlartIdClick
  end
  object cbAlartColor: TComboBox
    Left = 120
    Top = 152
    Width = 109
    Height = 20
    ItemHeight = 12
    TabOrder = 3
    Text = #32005#33394
    OnChange = ckAlartIdClick
    OnKeyPress = cbAlartColorKeyPress
    Items.Strings = (
      #31811#33394
      #40643#33394
      #27225#33394
      #32005#33394)
  end
  object cseAlartValue: TCSpinEdit
    Left = 128
    Top = 64
    Width = 100
    Height = 21
    TabOrder = 4
    OnChange = ckAlartIdClick
  end
  object Button1: TButton
    Left = 32
    Top = 208
    Width = 75
    Height = 25
    Caption = #30906#23450
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 168
    Top = 208
    Width = 75
    Height = 25
    Caption = #21462#28040
    TabOrder = 6
    OnClick = Button2Click
  end
end
