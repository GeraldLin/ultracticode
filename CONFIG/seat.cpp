//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "seat.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormSeat *FormSeat;

extern bool g_bSeatModifyId;
extern int gLangID;
extern int MyIsNumber(AnsiString str);
extern int MyIsDigits(AnsiString str);
extern int CheckIPv4Address(const char *pszIPAddr);
//---------------------------------------------------------------------------
__fastcall TFormSeat::TFormSeat(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormSeat::SetModifyType(int nmodify, CSeat &seat)
{
  nModify = nmodify;
  if (nModify == 1)
  {
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = true;
    Label11->Visible = false;
    cseBatchNum->Visible = false;
  }
  else if (nModify == 2)
  {
    Button1->Caption = (gLangID == 1) ? "插入" : "础";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = true;
    Label11->Visible = false;
    cseBatchNum->Visible = false;
  }
  else
  {
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = false;
    Label11->Visible = false;
    cseBatchNum->Visible = false;
  }
  editPort->Text = seat.CscNo;
  editSeatNo->Text = seat.SeatNo;
  switch (seat.SeatType)
  {
  case 1:
    cbSeatType->Text = (gLangID == 1) ? "内线电话座席" : "だ诀筿杠畒畊";
    break;
  case 2:
    cbSeatType->Text = (gLangID == 1) ? "内线电脑座席" : "だ诀筿福畒畊";
    break;
  }
  cseSeatGroupNo->Value = seat.SeatGroupNo;
  editWorkerGroupNo->Text = seat.GroupNo;
  cseWorkerLevel->Value = seat.WorkerLevel;
  editWorkerNo->Text = seat.WorkerNo;
  ckAutoLogin->Checked = seat.AutoLogin == 0 ? false : true;
  editWorkerName->Text = seat.WorkerName;
  editSeatIP->Text = seat.SeatIP;
  ckLoginSwitchID->Checked = seat.LoginSwitchID == 0 ? false : true;
  editLoginSwitchPSW->Text = seat.LoginSwitchPSW;
  editLoginSwitchGroupID->Text = seat.LoginSwitchGroupID;
  ckRecordId->Checked = seat.RecordID == 0 ? false : true;
  editDepartmentNo->Text = seat.DepartmentNo;
  editLocaAreaCode->Text = seat.LocaAreaCode;

  Button1->Enabled = false;
}

void __fastcall TFormSeat::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------

void __fastcall TFormSeat::editSeatNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormSeat::Button1Click(TObject *Sender)
{
  CSeat seat;
  char dispbuf[256];
  int nResult, nTemp, count=0, nSeatNo, nWorkerNo;

  if (gLangID == 1)
  {
    if (editSeatNo->Text == "")
    {
      MessageBox(NULL,"请设置座席分机号!","信息提示",MB_OK|MB_ICONWARNING);
      return;
    }
    if (MyIsNumber(editSeatNo->Text) != 0)
    {
      MessageBox(NULL,"设置的座席分机号有误!","信息提示",MB_OK|MB_ICONWARNING);
      return;
    }
    if (editWorkerNo->Text != "" && MyIsNumber(editWorkerNo->Text) != 0)
    {
      MessageBox(NULL,"设置话务员工号有误!","信息提示",MB_OK|MB_ICONWARNING);
      return;
    }
    if (editSeatIP->Text != "")
    {
      if (CheckIPv4Address(editSeatIP->Text.c_str()) != 0)
      {
        MessageBox(NULL,"绑定的座席IP地址不正确!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    seat.CscNo = StrToInt(editPort->Text);
    if (nModify == 1 || nModify == 2)
    {
      //增加
      if (FormMain->SeatList.isTheSeatNoExist(-1, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(-1, editSeatNo->Text))
      {
        MessageBox(NULL,"该座席分机号已存在!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)))
      {
        MessageBox(NULL,"该话务员工号已存在!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(-1, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(-1, editSeatIP->Text)))
      {
        MessageBox(NULL,"该IP地址已绑定!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    else
    {
      if (FormMain->SeatList.isTheSeatNoExist(seat.CscNo, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(seat.CscNo, editSeatNo->Text))
      {
        MessageBox(NULL,"该座席分机号已存在!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(seat.CscNo, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(seat.CscNo, editWorkerNo->Text)))
      {
        MessageBox(NULL,"该话务员工号已存在!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(seat.CscNo, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(seat.CscNo, editSeatIP->Text)))
      {
        MessageBox(NULL,"该IP地址已绑定!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }

    seat.SeatNo = editSeatNo->Text;
    if (cbSeatType->Text == "内线电话座席")
      seat.SeatType = 1;
    else if (cbSeatType->Text == "内线电脑座席")
      seat.SeatType = 2;
    seat.SeatGroupNo = cseSeatGroupNo->Value;
    seat.GroupNo = editWorkerGroupNo->Text;
    seat.WorkerLevel = cseWorkerLevel->Value;
    seat.WorkerNo = editWorkerNo->Text;
    seat.WorkerName = editWorkerName->Text;
    if (ckAutoLogin->Checked == true)
    {
      if (editWorkerNo->Text == "" || editWorkerNo->Text == "0")
        seat.AutoLogin = 0;
      else
        seat.AutoLogin = 1;
    }
    else
      seat.AutoLogin = 0;
    seat.SeatIP = editSeatIP->Text;

    if (ckLoginSwitchID->Checked == true)
      seat.LoginSwitchID = 1;
    else
      seat.LoginSwitchID = 0;
    seat.LoginSwitchPSW = editLoginSwitchPSW->Text;
    seat.LoginSwitchGroupID = editLoginSwitchGroupID->Text;
    if (ckRecordId->Checked == true)
      seat.RecordID = 1;
    else
      seat.RecordID = 0;
    seat.DepartmentNo = editDepartmentNo->Text;
    seat.LocaAreaCode = editLocaAreaCode->Text;

    seat.ModifyId = true;

    nSeatNo = StrToInt(editSeatNo->Text);
    if (editWorkerNo->Text == "")
      nWorkerNo = 0;
    else
      nWorkerNo = StrToInt(editWorkerNo->Text);
    
    if (nModify == 1)
    {
      g_bSeatModifyId = true;
      if (ckBatch->Checked == true)
      {
        for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SeatList.isTheSeatNoExist(-1, seat.SeatNo))
          {
            nResult = FormMain->SeatList.AddSeat(seat);
            if (nResult == 0)
            {
              count++;
              seat.CscNo++;
            }
          }
          nSeatNo++;
          seat.SeatNo = IntToStr(nSeatNo);
          if (nWorkerNo > 0)
          {
            nWorkerNo++;
            seat.WorkerNo = IntToStr(nWorkerNo);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "共增加了 %d 个座席，请按 应用 键修改后的参数才能生效，并需要重启IVR服务!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
          FormMain->bSeatParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSeat();
        }
        else
        {
          MessageBox(NULL,"增加座席失败,可能是分机号码重复,或超过限定的端口数量!","信息提示",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }
      nResult = FormMain->SeatList.AddSeat(seat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"增加座席成功，请按 应用 键修改后的参数才能生效，并需要重启IVR服务!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSeat();
      }
      else
      {
        MessageBox(NULL,"增加座席失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else if (nModify == 2)
    {
      g_bSeatModifyId = true;
      if (ckBatch->Checked == true)
      {
        for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SeatList.isTheSeatNoExist(-1, seat.SeatNo))
          {
            nResult = FormMain->SeatList.InsertSeat(seat.CscNo, seat);
            if (nResult == 0)
            {
              count++;
              seat.CscNo++;
            }
          }
          nSeatNo++;
          seat.SeatNo = IntToStr(nSeatNo);
          if (nWorkerNo > 0)
          {
            nWorkerNo++;
            seat.WorkerNo = IntToStr(nWorkerNo);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "共增加了 %d 个座席，请按 应用 键修改后的参数才能生效，并需要重启IVR服务!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
          FormMain->bSeatParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSeat();
        }
        else
        {
          MessageBox(NULL,"增加座席失败,可能是分机号码重复,或超过限定的端口数量!","信息提示",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }
      nResult = FormMain->SeatList.InsertSeat(seat.CscNo, seat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"插入座席成功，请按 应用 键修改后的参数才能生效，并需要重启IVR服务!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSeat();
      }
      else
      {
        MessageBox(NULL,"插入座席失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->SeatList.EditSeat(seat.CscNo, seat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"修改座席参数成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSeat();
      }
      else
      {
        MessageBox(NULL,"修改座席失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
  else
  {
    if (editSeatNo->Text == "")
    {
      MessageBox(NULL,"叫砞﹚畒畊だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
      return;
    }
    if (MyIsNumber(editSeatNo->Text) != 0)
    {
      MessageBox(NULL,"砞﹚畒畊だ诀腹絏Τ粇!","獺矗ボ",MB_OK|MB_ICONWARNING);
      return;
    }
    if (editWorkerNo->Text != "" && MyIsNumber(editWorkerNo->Text) != 0)
    {
      MessageBox(NULL,"砞﹚磅诀絪腹Τ粇!","獺矗ボ",MB_OK|MB_ICONWARNING);
      return;
    }
    if (editSeatIP->Text != "")
    {
      if (CheckIPv4Address(editSeatIP->Text.c_str()) != 0)
      {
        MessageBox(NULL,"竕﹚畒畊IPぃタ絋!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    seat.CscNo = StrToInt(editPort->Text);
    if (nModify == 1 || nModify == 2)
    {
      //增加
      if (FormMain->SeatList.isTheSeatNoExist(-1, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(-1, editSeatNo->Text))
      {
        MessageBox(NULL,"赣畒畊だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)))
      {
        MessageBox(NULL,"赣磅诀絪腹!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(-1, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(-1, editSeatIP->Text)))
      {
        MessageBox(NULL,"赣IP竕﹚!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    else
    {
      if (FormMain->SeatList.isTheSeatNoExist(seat.CscNo, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(seat.CscNo, editSeatNo->Text))
      {
        MessageBox(NULL,"赣畒畊だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(seat.CscNo, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(seat.CscNo, editWorkerNo->Text)))
      {
        MessageBox(NULL,"赣杠磅诀絪腹!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(seat.CscNo, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(seat.CscNo, editSeatIP->Text)))
      {
        MessageBox(NULL,"赣IP竕﹚!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
    }

    seat.SeatNo = editSeatNo->Text;
    if (cbSeatType->Text == "だ诀筿杠畒畊")
      seat.SeatType = 1;
    else if (cbSeatType->Text == "だ诀筿福畒畊")
      seat.SeatType = 2;
    seat.SeatGroupNo = cseSeatGroupNo->Value;
    seat.GroupNo = editWorkerGroupNo->Text;
    seat.WorkerLevel = cseWorkerLevel->Value;
    seat.WorkerNo = editWorkerNo->Text;
    seat.WorkerName = editWorkerName->Text;
    if (ckAutoLogin->Checked == true)
    {
      if (editWorkerNo->Text == "" || editWorkerNo->Text == "0")
        seat.AutoLogin = 0;
      else
        seat.AutoLogin = 1;
    }
    else
      seat.AutoLogin = 0;
    seat.SeatIP = editSeatIP->Text;

    if (ckLoginSwitchID->Checked == true)
      seat.LoginSwitchID = 1;
    else
      seat.LoginSwitchID = 0;
    seat.LoginSwitchPSW = editLoginSwitchPSW->Text;
    seat.LoginSwitchGroupID = editLoginSwitchGroupID->Text;
    if (ckRecordId->Checked == true)
      seat.RecordID = 1;
    else
      seat.RecordID = 0;
    seat.DepartmentNo = editDepartmentNo->Text;
    seat.LocaAreaCode = editLocaAreaCode->Text;

    seat.ModifyId = true;

    nSeatNo = StrToInt(editSeatNo->Text);
    if (editWorkerNo->Text == "")
      nWorkerNo = 0;
    else
      nWorkerNo = StrToInt(editWorkerNo->Text);
    
    if (nModify == 1)
    {
      g_bSeatModifyId = true;
      if (ckBatch->Checked == true)
      {
        for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SeatList.isTheSeatNoExist(-1, seat.SeatNo))
          {
            nResult = FormMain->SeatList.AddSeat(seat);
            if (nResult == 0)
            {
              count++;
              seat.CscNo++;
            }
          }
          nSeatNo++;
          seat.SeatNo = IntToStr(nSeatNo);
          if (nWorkerNo > 0)
          {
            nWorkerNo++;
            seat.WorkerNo = IntToStr(nWorkerNo);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "糤 %d 畒畊叫 甅ノ 龄э把计ネ惠璶币IVR狝叭!", count);
          MessageBox(NULL,dispbuf,"獺矗ボ",MB_OK|MB_ICONINFORMATION);
          FormMain->bSeatParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSeat();
        }
        else
        {
          MessageBox(NULL,"糤畒畊ア毖,琌だ诀腹絏確,┪禬筁﹚硈钡梆计秖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }
      nResult = FormMain->SeatList.AddSeat(seat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"糤畒畊Θ叫 甅ノ 龄э把计ネ惠璶币IVR狝叭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSeat();
      }
      else
      {
        MessageBox(NULL,"糤畒畊ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else if (nModify == 2)
    {
      g_bSeatModifyId = true;
      if (ckBatch->Checked == true)
      {
        for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SeatList.isTheSeatNoExist(-1, seat.SeatNo))
          {
            nResult = FormMain->SeatList.InsertSeat(seat.CscNo, seat);
            if (nResult == 0)
            {
              count++;
              seat.CscNo++;
            }
          }
          nSeatNo++;
          seat.SeatNo = IntToStr(nSeatNo);
          if (nWorkerNo > 0)
          {
            nWorkerNo++;
            seat.WorkerNo = IntToStr(nWorkerNo);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "糤 %d 畒畊叫 甅ノ 龄э把计ネ惠璶币IVR狝叭!", count);
          MessageBox(NULL,dispbuf,"獺矗ボ",MB_OK|MB_ICONINFORMATION);
          FormMain->bSeatParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSeat();
        }
        else
        {
          MessageBox(NULL,"糤畒畊ア毖,琌だ诀腹絏確,┪禬筁﹚硈钡梆计秖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }
      nResult = FormMain->SeatList.InsertSeat(seat.CscNo, seat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"础畒畊Θ叫 甅ノ 龄э把计ネ惠璶币IVR狝叭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSeat();
      }
      else
      {
        MessageBox(NULL,"础畒畊ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->SeatList.EditSeat(seat.CscNo, seat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"э畒畊把计Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSeat();
      }
      else
      {
        MessageBox(NULL,"э畒畊ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormSeat::cbSeatTypeKeyPress(TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormSeat::ckBatchClick(TObject *Sender)
{
  if (ckBatch->Checked == true)
  {
    Label11->Visible = true;
    cseBatchNum->Visible = true;
  }
  else
  {
    Label11->Visible = false;
    cseBatchNum->Visible = false;
  }
}
//---------------------------------------------------------------------------

