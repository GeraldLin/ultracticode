//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "extseat.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormExtSeat *FormExtSeat;
extern int gLangID;
extern int MyIsNumber(AnsiString str);
extern int MyIsDigits(AnsiString str);
extern int CheckIPv4Address(const char *pszIPAddr);
//---------------------------------------------------------------------------
__fastcall TFormExtSeat::TFormExtSeat(TComponent* Owner)
  : TForm(Owner)
{
  bmodify = false;
}
//---------------------------------------------------------------------------
void TFormExtSeat::SetModifyType(bool bmodify, CExtSeat &extseat)
{
  if (bmodify == false)
  {
    bModify = bmodify;
    editPort->Text = FormMain->ExtSeatList.ExtSeatNum;
    editSeatNo->Text = "";
    cbSeatType->Text = (gLangID == 1) ? "远端电话座席" : "环狠筿杠畒畊";
    cseSeatGroupNo->Value = 0;
    cseWorkerGroupNo->Value = 0;
    cseWorkerLevel->Value = 0;
    editWorkerNo->Text = "";
    editPSTNNo->Text = "";
    cseRouteNo->Value = 1;
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
  }
  else
  {
    bModify = bmodify;
    editPort->Text = extseat.CscNo;
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
  }
  editSeatNo->Text = extseat.SeatNo;
  cbSeatType->Text = (gLangID == 1) ? "远端电话座席" : "环狠筿杠畒畊";
  cseSeatGroupNo->Value = extseat.SeatGroupNo;
  cseWorkerGroupNo->Value = extseat.GroupNo;
  cseWorkerLevel->Value = extseat.WorkerLevel;
  editWorkerNo->Text = extseat.WorkerNo;
  editPSTNNo->Text = extseat.PSTNNo;
  cseRouteNo->Value = extseat.RouteNo;
  editWorkerName->Text = extseat.WorkerName;
  editSeatIP->Text = extseat.SeatIP;
  Button1->Enabled = false;
}
void __fastcall TFormExtSeat::cbSeatTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;  
}
//---------------------------------------------------------------------------
void __fastcall TFormExtSeat::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormExtSeat::editSeatNoChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormExtSeat::Button1Click(TObject *Sender)
{
  CExtSeat extseat;
  int nResult, nTemp;
  if (gLangID == 1)
  {
    if (editSeatNo->Text == "")
    {
      MessageBox(NULL,"请设置座席分机号!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (MyIsNumber(editSeatNo->Text) != 0)
    {
      MessageBox(NULL,"设置的座席分机号有误!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (editWorkerNo->Text != "" && MyIsNumber(editWorkerNo->Text) != 0)
    {
      MessageBox(NULL,"设置话务员工号有误!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (MyIsDigits(editPSTNNo->Text) != 0)
    {
      MessageBox(NULL,"设置的座席外线号码有误!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (editSeatIP->Text != "")
    {
      if (CheckIPv4Address(editSeatIP->Text.c_str()) != 0)
      {
        MessageBox(NULL,"绑定的座席IP地址不正确!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    extseat.CscNo = StrToInt(editPort->Text);
    if (bModify == false)
    {
      //增加
      if (FormMain->SeatList.isTheSeatNoExist(-1, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(-1, editSeatNo->Text))
      {
        MessageBox(NULL,"该座席分机号已存在!","信息提示",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)))
      {
        MessageBox(NULL,"该话务员工号已存在!","信息提示",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(-1, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(-1, editSeatIP->Text)))
      {
        MessageBox(NULL,"该IP地址已绑定!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    else
    {
      if (FormMain->SeatList.isTheSeatNoExist(-1, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(extseat.CscNo, editSeatNo->Text))
      {
        MessageBox(NULL,"该座席分机号已存在!","信息提示",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(extseat.CscNo, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(extseat.CscNo, editWorkerNo->Text)))
      {
        MessageBox(NULL,"该话务员工号已存在!","信息提示",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(extseat.CscNo, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(extseat.CscNo, editSeatIP->Text)))
      {
        MessageBox(NULL,"该IP地址已绑定!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
    }

    extseat.SeatNo = editSeatNo->Text;
    extseat.SeatType = 5;
    extseat.SeatGroupNo = cseSeatGroupNo->Value;
    extseat.GroupNo = cseWorkerGroupNo->Value;
    extseat.WorkerLevel = cseWorkerLevel->Value;
    extseat.WorkerNo = editWorkerNo->Text;
    extseat.PSTNNo = editPSTNNo->Text;
    extseat.RouteNo = cseRouteNo->Value;
    extseat.WorkerName = editWorkerName->Text;
    extseat.SeatIP = editSeatIP->Text;
    extseat.ModifyId = true;

    if (bModify == false)
    {
      nResult = FormMain->ExtSeatList.EditExtSeat(extseat.CscNo, extseat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"增加座席成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->ExtSeatList.ExtSeatNum++;
        FormMain->bExtSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispExtSeat();
      }
      else
      {
        MessageBox(NULL,"增加座席失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->ExtSeatList.EditExtSeat(extseat.CscNo, extseat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"修改座席成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bExtSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispExtSeat();
      }
      else
      {
        MessageBox(NULL,"修改座席失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
  else
  {
    if (editSeatNo->Text == "")
    {
      MessageBox(NULL,"叫砞﹚畒畊だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (MyIsNumber(editSeatNo->Text) != 0)
    {
      MessageBox(NULL,"砞﹚畒畊だ诀腹絏Τ粇!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (editWorkerNo->Text != "" && MyIsNumber(editWorkerNo->Text) != 0)
    {
      MessageBox(NULL,"砞﹚杠叭絪腹Τ粇!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (MyIsDigits(editPSTNNo->Text) != 0)
    {
      MessageBox(NULL,"砞﹚畒畊絬腹絏Τ粇!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (editSeatIP->Text != "")
    {
      if (CheckIPv4Address(editSeatIP->Text.c_str()) != 0)
      {
        MessageBox(NULL,"竕﹚畒畊IPぃタ絋!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    extseat.CscNo = StrToInt(editPort->Text);
    if (bModify == false)
    {
      //增加
      if (FormMain->SeatList.isTheSeatNoExist(-1, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(-1, editSeatNo->Text))
      {
        MessageBox(NULL,"赣畒畊だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(-1, editWorkerNo->Text)))
      {
        MessageBox(NULL,"赣磅诀絪腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(-1, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(-1, editSeatIP->Text)))
      {
        MessageBox(NULL,"赣IP竕﹚!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
    }
    else
    {
      if (FormMain->SeatList.isTheSeatNoExist(-1, editSeatNo->Text)
        || FormMain->ExtSeatList.isTheSeatNoExist(extseat.CscNo, editSeatNo->Text))
      {
        MessageBox(NULL,"赣畒畊だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editWorkerNo->Text != "" && (FormMain->SeatList.isTheWorkerNoExist(extseat.CscNo, editWorkerNo->Text)
        || FormMain->ExtSeatList.isTheWorkerNoExist(extseat.CscNo, editWorkerNo->Text)))
      {
        MessageBox(NULL,"赣磅诀絪腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        return;
      }
      if (editSeatIP->Text != "" && (FormMain->SeatList.isTheSeatIPExist(extseat.CscNo, editSeatIP->Text)
        || FormMain->ExtSeatList.isTheSeatIPExist(extseat.CscNo, editSeatIP->Text)))
      {
        MessageBox(NULL,"赣IP竕﹚!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
    }

    extseat.SeatNo = editSeatNo->Text;
    extseat.SeatType = 5;
    extseat.SeatGroupNo = cseSeatGroupNo->Value;
    extseat.GroupNo = cseWorkerGroupNo->Value;
    extseat.WorkerLevel = cseWorkerLevel->Value;
    extseat.WorkerNo = editWorkerNo->Text;
    extseat.PSTNNo = editPSTNNo->Text;
    extseat.RouteNo = cseRouteNo->Value;
    extseat.WorkerName = editWorkerName->Text;
    extseat.SeatIP = editSeatIP->Text;
    extseat.ModifyId = true;

    if (bModify == false)
    {
      nResult = FormMain->ExtSeatList.EditExtSeat(extseat.CscNo, extseat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"糤畒畊Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->ExtSeatList.ExtSeatNum++;
        FormMain->bExtSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispExtSeat();
      }
      else
      {
        MessageBox(NULL,"糤畒畊ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      nResult = FormMain->ExtSeatList.EditExtSeat(extseat.CscNo, extseat);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"э畒畊Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bExtSeatParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispExtSeat();
      }
      else
      {
        MessageBox(NULL,"э畒畊ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
}
//---------------------------------------------------------------------------
