//---------------------------------------------------------------------------

#ifndef extseatH
#define extseatH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormExtSeat : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TEdit *editPort;
  TEdit *editSeatNo;
  TComboBox *cbSeatType;
  TCSpinEdit *cseSeatGroupNo;
  TCSpinEdit *cseWorkerGroupNo;
  TCSpinEdit *cseWorkerLevel;
  TEdit *editWorkerNo;
  TButton *Button1;
  TButton *Button2;
  TLabel *Label9;
  TEdit *editPSTNNo;
  TLabel *Label10;
  TCSpinEdit *cseRouteNo;
  TLabel *Label11;
  TLabel *Label12;
  TEdit *editWorkerName;
  TEdit *editSeatIP;
  void __fastcall cbSeatTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editSeatNoChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormExtSeat(TComponent* Owner);

  bool bModify;
  void SetModifyType(bool bmodify, CExtSeat &extseat);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormExtSeat *FormExtSeat;
//---------------------------------------------------------------------------
#endif
