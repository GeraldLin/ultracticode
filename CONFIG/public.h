//---------------------------------------------------------------------------
#ifndef __PUBLIC_H_
#define __PUBLIC_H_
//---------------------------------------------------------------------------

//业务流程管理
class CAccessCode
{
public:
  int state;
  int AccNo;
  int MinLen;
  int MaxLen;
  AnsiString PreCalled;
  AnsiString PreCaller;
public:
  CAccessCode()
  {
    state = 0;
  }
  virtual ~CAccessCode(){}

  const CAccessCode& operator=(const CAccessCode& acccode)
  {
    state = acccode.state;
    AccNo = acccode.AccNo;
    MinLen = acccode.MinLen;
    MaxLen = acccode.MaxLen;
    PreCalled = acccode.PreCalled;
    PreCaller = acccode.PreCaller;
    return *this;
  }
};
class CFlwFile
{
public:
  int state;
  int FlwNo;
  int RunAfterLoad;
  int FuncGroup;
  int FuncNo;
  int RestLines;
  int AccessNum;

  AnsiString FlwFile;
  AnsiString Explain;

  CAccessCode AccessCode[256];
public:
  CFlwFile()
  {
    state = 0;
  }
  virtual ~CFlwFile(){}
  const CFlwFile& operator=(const CFlwFile& flwfile)
  {
    state = flwfile.state;
    FlwNo = flwfile.FlwNo;
    RunAfterLoad = flwfile.RunAfterLoad;
    FuncGroup = flwfile.FuncGroup;
    FuncNo = flwfile.FuncNo;
    RestLines = flwfile.RestLines;
    AccessNum = flwfile.AccessNum;

    FlwFile = flwfile.FlwFile;
    Explain = flwfile.Explain;
    for (int i=0; i<256; i++)
      AccessCode[i] = flwfile.AccessCode[i];
    return *this;
  }
  int AddAccCode(AnsiString acccode, int minlen, int maxlen)
  {
    if (AccessNum >= 256)
      return 1;
    AccessCode[AccessNum].state = 1;
    AccessCode[AccessNum].AccNo = AccessNum;
    AccessCode[AccessNum].MinLen = minlen;
    AccessCode[AccessNum].MaxLen = maxlen;
    AccessCode[AccessNum].PreCalled = acccode;
    AccessCode[AccessNum].PreCaller = "*";
    AccessNum ++;
    return 0;
  }
  int EditAccCode(int accno, AnsiString acccode, int minlen, int maxlen)
  {
    if (accno >= 256)
      return 1;
    AccessCode[accno].state = 1;
    AccessCode[accno].AccNo = AccessNum;
    AccessCode[accno].MinLen = minlen;
    AccessCode[accno].MaxLen = maxlen;
    AccessCode[accno].PreCalled = acccode;
    AccessCode[accno].PreCaller = "*";
    return 0;
  }
  void DelAccCode(int accno)
  {
    for (int i=accno; i<AccessNum-1; i++)
    {
      AccessCode[i] = AccessCode[i+1];
    }
    if (AccessNum > 0)
      AccessNum --;
  }
};
class CFlwFileList
{
public:
  int FlwNum;
  CFlwFile FlwFile[64];
public:
  CFlwFileList()
  {
    FlwNum = 0;
  }
  int AddFlwFile(AnsiString flwfile, AnsiString explain, int funcno, int runafterload)
  {
    if (FlwNum >= 64)
      return 1;
    FlwFile[FlwNum].state = 1;
    FlwFile[FlwNum].FlwNo = FlwNum;
    FlwFile[FlwNum].FlwFile = flwfile;
    FlwFile[FlwNum].Explain = explain;
    FlwFile[FlwNum].FuncNo = funcno;
    FlwFile[FlwNum].RunAfterLoad = runafterload;
    FlwNum++;
    return 0;
  }
  int EditFlwFile(int flwno, AnsiString flwfile, AnsiString explain, int funcno, int runafterload)
  {
    if (flwno >= 64)
      return 1;
    FlwFile[flwno].state = 1;
    FlwFile[flwno].FlwNo = FlwNum;
    FlwFile[flwno].FlwFile = flwfile;
    FlwFile[flwno].Explain = explain;
    FlwFile[flwno].FuncNo = funcno;
    FlwFile[flwno].RunAfterLoad = runafterload;
    return 0;
  }
  void DelFlwFile(int flwno)
  {
    for (int i=flwno; i<FlwNum-1; i++)
    {
      FlwFile[i] = FlwFile[i+1];
    }
    if (FlwNum > 0)
      FlwNum --;
  }
  bool isTheFuncNoExist(int flwno, int funcno)
  {
    for (int i=0; i<FlwNum; i++)
    {
      if (funcno == FlwFile[i].FuncNo && i != flwno)
        return true;
    }
    return false;
  }
};

//模拟外线接入号
class CAPSTN
{
public:
  int PSTNNum; //模拟外线数
  AnsiString PSTNCode[128];
  AnsiString PreCode[128];
  AnsiString IPPreCode[128];
  AnsiString NotAddPreCode[128];
  bool bModifyId[128];
public:
  CAPSTN()
  {
    PSTNNum = 0;
    for (int i=0; i<128; i++)
    {
      bModifyId[i] = false;
    }
  }
  int AddApstn(AnsiString pstn)
  {
    if (PSTNNum >= 128)
      return 1;
    PSTNCode[PSTNNum] = pstn;
    PSTNNum++;
    return 0;
  }
  int EditApstn(int lineno, AnsiString pstncode, AnsiString precode, AnsiString ipprecode, AnsiString notaddprecode)
  {
    if (lineno >= 128)
      return 1;
    PSTNCode[lineno] = pstncode;
    PreCode[lineno] = precode;
    IPPreCode[lineno] = ipprecode;
    NotAddPreCode[lineno] = notaddprecode;
    bModifyId[lineno] = true;
    return 0;
  }
  void DelApstn(int lineno)
  {
    for (int i=lineno; i<PSTNNum-1; i++)
    {
      PSTNCode[i] = PSTNCode[i+1];
    }
    if (PSTNNum > 0)
      PSTNNum --;
  }
};

//路由参数
class CRoute
{
public:
  int SelMode;
  AnsiString TrkStr;
  AnsiString Remark;
  AnsiString IPPreCode;
  bool bModifyId;
public:
  CRoute()
  {
    SelMode = 0;
    TrkStr = "";
    Remark = "";
    IPPreCode = "";
    bModifyId = false;
  }
  virtual ~CRoute(){}
  const CRoute& operator=(const CRoute& route)
  {
    SelMode = route.SelMode;
    TrkStr = route.TrkStr;
    Remark = route.Remark;
    IPPreCode = route.IPPreCode;
    bModifyId = route.bModifyId;
    return *this;
  }
};
class CRouteList
{
public:
  int DTUseTs32;
  int E1TrkSelMode;
  int RouteNum;
  CRoute Route[32];
public:
  CRouteList()
  {
    DTUseTs32 = 0;
    E1TrkSelMode = 0;
    RouteNum = 0;
  }
  int EditRoute(int routeno, int selmode, AnsiString trkstr, AnsiString remark, AnsiString ipprecode)
  {
    if (routeno >= 32)
      return 1;
    Route[routeno].SelMode = selmode;
    Route[routeno].TrkStr = trkstr;
    Route[routeno].Remark = remark;
    Route[routeno].IPPreCode = ipprecode;
    Route[routeno].bModifyId = true;
    return 0;
  }
  void DelRoute(int routeno)
  {
    Route[routeno].SelMode = 0;
    Route[routeno].TrkStr = "";
    Route[routeno].Remark = "";
    Route[routeno].IPPreCode = "";
    Route[routeno].bModifyId = true;
  }
};

//坐席参数
class CSeat
{
public:
  int state;
  int CscNo;
  AnsiString SeatNo;
  int SeatType;
  int SeatGroupNo;
  AnsiString GroupNo;
  int WorkerLevel;
  AnsiString WorkerNo;
  AnsiString WorkerName;
  AnsiString SeatIP;
  int AutoLogin;
  int LoginSwitchID;
  AnsiString LoginSwitchPSW;
  AnsiString LoginSwitchGroupID;
  int RecordID;
  AnsiString DepartmentNo;
  AnsiString LocaAreaCode;

  bool ModifyId;
public:
  CSeat()
  {
    state = 0;
    SeatNo = "";
    SeatType = 1;
    SeatGroupNo = 0;
    GroupNo = "0";
    WorkerLevel = 0;
    WorkerNo = "";
    WorkerName = "";
    SeatIP = "";
    AutoLogin = 1;
    LoginSwitchID = 0;
    LoginSwitchPSW = "";
    LoginSwitchGroupID = "";
    RecordID = 1;
    DepartmentNo = "";
    LocaAreaCode = "";
    ModifyId = false;
  }
  virtual ~CSeat(){}
  const CSeat& operator=(const CSeat& seat)
  {
    state = seat.state;
    CscNo = seat.CscNo;
    SeatNo = seat.SeatNo;
    SeatType = seat.SeatType;
    SeatGroupNo = seat.SeatGroupNo;
    GroupNo = seat.GroupNo;
    WorkerLevel = seat.WorkerLevel;
    WorkerNo = seat.WorkerNo;
    WorkerName = seat.WorkerName;
    SeatIP = seat.SeatIP;
    AutoLogin = seat.AutoLogin;
    LoginSwitchID = seat.LoginSwitchID;
    LoginSwitchPSW = seat.LoginSwitchPSW;
    LoginSwitchGroupID = seat.LoginSwitchGroupID;
    RecordID = seat.RecordID;
    DepartmentNo = seat.DepartmentNo;
    LocaAreaCode = seat.LocaAreaCode;
    ModifyId = seat.ModifyId;
    return *this;
  }
};

#define MAX_AGENT_NUM 1024

class CSeatList
{
public:
  int SeatNum;
  CSeat Seat[MAX_AGENT_NUM];
public:
  CSeatList()
  {
    SeatNum = 0;
  }
  int AddSeat(CSeat& seat)
  {
    if (SeatNum >= MAX_AGENT_NUM)
      return 1;
    Seat[SeatNum] = seat;
    SeatNum++;
    return 0;
  }
  int EditSeat(int cscno, CSeat& seat)
  {
    if (cscno >= MAX_AGENT_NUM)
      return 1;
    Seat[cscno] = seat;
    return 0;
  }
  int InsertSeat(int cscno, CSeat& seat)
  {
    if (cscno >= MAX_AGENT_NUM)
      return 1;
    for (int i=SeatNum; i>=cscno; i--)
    {
      Seat[i+1] = Seat[i];
      Seat[i+1].CscNo = i+1;
    }

    Seat[cscno] = seat;
    SeatNum++;
    return 0;
  }
  void DelSeat(int cscno)
  {
    for (int i=cscno; i<SeatNum-1; i++)
    {
      Seat[i] = Seat[i+1];
      Seat[i].CscNo = i;
    }
    if (SeatNum > 0)
      SeatNum --;
  }
  bool isTheSeatNoExist(int cscno, AnsiString seatno)
  {
    for (int i=0; i<SeatNum; i++)
    {
      if (seatno == Seat[i].SeatNo && i != cscno)
        return true;
    }
    return false;
  }
  bool isTheWorkerNoExist(int cscno, AnsiString workerno)
  {
    for (int i=0; i<SeatNum; i++)
    {
      if (workerno == Seat[i].WorkerNo && i != cscno)
        return true;
    }
    return false;
  }
  bool isTheSeatIPExist(int cscno, AnsiString seatip)
  {
    for (int i=0; i<SeatNum; i++)
    {
      if (seatip == Seat[i].SeatIP && i != cscno)
        return true;
    }
    return false;
  }
};

//远端坐席参数
class CExtSeat
{
public:
  int state;
  int CscNo;
  AnsiString SeatNo;
  int SeatType;
  int SeatGroupNo;
  int GroupNo;
  int WorkerLevel;
  AnsiString WorkerNo;
  AnsiString PSTNNo;
  int RouteNo;
  AnsiString WorkerName;
  AnsiString SeatIP;

  bool ModifyId;
public:
  CExtSeat()
  {
    state = 0;
    SeatNo = "";
    SeatType = 1;
    SeatGroupNo = 0;
    GroupNo = 0;
    WorkerLevel = 0;
    WorkerNo = "";
    PSTNNo = "";
    RouteNo = 1;
    WorkerName = "";
    SeatIP = "";
    ModifyId = false;
  }
  virtual ~CExtSeat(){}
  const CExtSeat& operator=(const CExtSeat& extseat)
  {
    state = extseat.state;
    CscNo = extseat.CscNo;
    SeatNo = extseat.SeatNo;
    SeatType = extseat.SeatType;
    SeatGroupNo = extseat.SeatGroupNo;
    GroupNo = extseat.GroupNo;
    WorkerLevel = extseat.WorkerLevel;
    WorkerNo = extseat.WorkerNo;
    PSTNNo = extseat.PSTNNo;
    RouteNo = extseat.RouteNo;
    WorkerName = extseat.WorkerName;
    SeatIP = extseat.SeatIP;
    ModifyId = extseat.ModifyId;
    return *this;
  }
};
class CExtSeatList
{
public:
  int ExtSeatNum;
  CExtSeat ExtSeat[256];
public:
  CExtSeatList()
  {
    ExtSeatNum = 0;
  }
  int AddExtSeat(CExtSeat& extseat)
  {
    if (ExtSeatNum >= 256)
      return 1;
    ExtSeat[ExtSeatNum] = extseat;
    ExtSeatNum++;
    return 0;
  }
  int EditExtSeat(int cscno, CExtSeat& extseat)
  {
    if (cscno >= 256)
      return 1;
    ExtSeat[cscno] = extseat;
    return 0;
  }
  void DelExtSeat(int cscno)
  {
    for (int i=cscno; i<ExtSeatNum-1; i++)
    {
      ExtSeat[i] = ExtSeat[i+1];
    }
    if (ExtSeatNum > 0)
      ExtSeatNum --;
  }
  bool isTheSeatNoExist(int cscno, AnsiString seatno)
  {
    for (int i=0; i<ExtSeatNum; i++)
    {
      if (seatno == ExtSeat[i].SeatNo && i != cscno)
        return true;
    }
    return false;
  }
  bool isTheWorkerNoExist(int cscno, AnsiString workerno)
  {
    for (int i=0; i<ExtSeatNum; i++)
    {
      if (workerno == ExtSeat[i].WorkerNo && i != cscno)
        return true;
    }
    return false;
  }
  bool isTheSeatIPExist(int cscno, AnsiString seatip)
  {
    for (int i=0; i<ExtSeatNum; i++)
    {
      if (seatip == ExtSeat[i].SeatIP && i != cscno)
        return true;
    }
    return false;
  }
};

class CChnStatus
{
public:
  bool bDispID; //是否显示
  short nListItemNo;
  
  short nChType;
  short nChIndex;
  short nLgChnType;
  short nLgChnNo;

	short hwState; //硬件状态:	1: 未初始化	0: 可用	2: 闭塞	3: 关闭
	short lnState; //线路状态: 0-空闲 1-占用 2-收到线路释放 3-发送释放后等待回应
	short ssState; //信令状态: 参考宏定义
	short CallInOut; //呼叫方向：1-呼入 2-呼出
public:
  CChnStatus()
  {
    bDispID = false;
    nListItemNo = -1;

    nChType = 0;
    nChIndex = -1;
    nLgChnType = 0;
    nLgChnNo = -1;

    hwState = 0;
    lnState = 0;
    ssState = 0;
    CallInOut = 0;
  }
};
class CVopStatus
{
public:
  bool bDispID; //是否显示
  short nListItemNo;
  
  short nVopIndex;
  short nVopChnType; //0-IVR/FAX 1-IVR 2-FAX 3-监录
  short nVopChnNo;

	short lnState; //硬件状态:	1: 未初始化	0: 可用	2: 闭塞	3: 关闭
	short svState; //服务状态: 0-空闲 1-正在放音 2-正在录音 3-正在发送传真 4-正在接收传真
  short ssState; //呼叫信令状态 0-空闲 1-占用
public:
  CVopStatus()
  {
    bDispID = false;
    nListItemNo = -1;

    nVopIndex = -1;
    nVopChnType = -1;
    nVopChnNo = -1;

    lnState = 0;
    svState = 0;
    ssState = 0;
  }
};

class CAgStatus
{
public:
  short state; //1-存在
	short LogState; //登录状态
	short svState; //服务状态
	short Disturbid; //打扰状态
	short Leaveid; //离席状态
public:
  CAgStatus()
  {
    state = 0;
    LogState = 0;
    svState = 0;
    Disturbid = 0;
    Leaveid = 0;
  }
};
//某时段中继占用统计
class CTrkCount
{
public:
  int TrkValues[20]; //参数值
  //参数序号对应值含义
  //0 TrkCallCount; //中继呼入呼出总次数
  //1 TrkBusyMin; //中继最小同时占用数
  //2 TrkBusyMax; //中继最大同时占用数

  //3 TrkInCount; //中继呼入总数
  //4 TrkInAnsCount; //中继呼入应答总数
  //5 TrkInMin; //呼入中继最小同时占用数
  //6 TrkInMax; //呼入中继最大同时占用数

  //7 TrkOutCount; //中继呼出总数
  //8 TrkOutAnsCount; //中继呼出应答总数
  //9 TrkOutMin; //呼出中继最小同时占用数
  //10 TrkOutMax; //呼出中继最大同时占用数

  //11 TrkBusy0Per; //中继线无占用百分比概率
  //12 TrkBusy0_25Per; //中继线占用<=%25百分比概率
  //13 TrkBusy25_50Per; //中继线占用>%25 and <=50%百分比概率
  //14 TrkBusy50_75Per; //中继线占用>%50 and <=75%百分比概率
  //15 TrkBusy75_90Per; //中继线占用>75% and <=90%百分比概率
  //16 TrkBusy90_100Per; //中继线占用>90%百分比概率

public:
  void Init()
  {
    for (int i=0; i<20; i++)
      TrkValues[i] = 0;
  }
	CTrkCount()
  {
    Init();
  }
	virtual ~CTrkCount()
  {
  }
};
//某时段坐席占用统计
class CAgCount
{
public:
  int AgValues[20]; //参数值
  //参数序号对应值含义
  //0 AgLoginMax; //最大坐席登录数
  //1 AgLoginMin; //最小坐席登录数
  //2 AgBusyMin; //座席最小同时占用数
  //3 AgBusyMax; //座席最大同时占用数

  //4 AgBusy0Per; //座席无占用百分比概率
  //5 AgBusy0_25Per; //座席占用<=%25百分比概率
  //6 AgBusy25_50Per; //座席占用>%25 and <=50%百分比概率
  //7 AgBusy50_75Per; //座席占用>%50 and <=75%百分比概率
  //8 AgBusy75_90Per; //座席占用>75% and <=90%百分比概率
  //9 AgBusy90_100Per; //座席占用>90%百分比概率

  //10 AcdCallCount; //ACD分配坐席总呼叫次数
  //11 AcdCallFailCount; //ACD分配失败总次数
  //12 AcdCallSuccCount; //ACD分配成功总次数（表示已分配到空闲坐席）
  //13 AcdCallGiveUpCount; //ACD分配放弃总次数
  //14 AcdCallAnsCount; //ACD分配坐席应答总次数
  //15 AcdCallAvgWaitTime; //ACD分配平均排队时长(从呼入到坐席应答之间的时长平均值)
  //16 AcdCallAvgAnsTime; //ACD分配坐席平均等待应答时长
  //17 AcdCallAvgGiveUpTime; //ACD分配坐席平均放弃等待时长

public:
  void Init()
  {
    for (int i=0; i<20; i++)
      AgValues[i] = 0;
  }
	CAgCount()
  {
    Init();
  }
	virtual ~CAgCount()
  {
  }
};

//交换机端口参数
class CSwitchPort
{
public:
  int state;
  int m_nPortNo;
  AnsiString m_strPortID; //分机号
  AnsiString m_strFlwCode; //流程接入号
  int m_nLgChType; //1-中继 2-坐席 3-IVR 4-录音 5-传真
  int m_nChStyle; //1-模拟坐席通道 2-模拟外线通道 3-2B+D数字话机 8-数字通道（中国No.1信令） 9-TUP中继 10-ISUP中继 11-PRI用户侧 12-PRI网络侧 13-硬传真资源 14-软传真资源 15-VOIP-H323 16-VOIP(sip) 17-4线E&M 18-SIP-TRUNKING
  int m_nPhyPortNo;

  bool ModifyId;

  DWORD dwProviderID;
public:
  CSwitchPort()
  {
    state = 0;
    m_nPortNo = 0;
    m_strPortID = "";
    m_strFlwCode = "";
    m_nLgChType = 0;
    m_nChStyle = 0;
    m_nPhyPortNo = 0;
    ModifyId = false;
  }
  virtual ~CSwitchPort(){}
  const CSwitchPort& operator=(const CSwitchPort& switchport)
  {
    state = switchport.state;
    m_nPortNo = switchport.m_nPortNo;
    m_strPortID = switchport.m_strPortID;
    m_strFlwCode = switchport.m_strFlwCode;
    m_nLgChType = switchport.m_nLgChType;
    m_nChStyle = switchport.m_nChStyle;
    m_nPhyPortNo = switchport.m_nPhyPortNo;
    ModifyId = switchport.ModifyId;
    return *this;
  }
};

#define MAX_SWITCHPORT_NUM 1024

class CSwitchPortList
{
public:
  int SwitchPortNum;
  CSwitchPort SwitchPort[MAX_SWITCHPORT_NUM];

  int CTILinkType;
  AnsiString SwitchIP;

  AnsiString ServerID;
  AnsiString LoginID;
  AnsiString Password;

  DWORD dwProviderID;
public:
  CSwitchPortList()
  {
    SwitchPortNum = 0;
    CTILinkType = 1;
    SwitchIP = "192.168.1.100";

    ServerID = "";
    LoginID = "";
    Password = "";

    dwProviderID = 0;
  }
  int AddSwitchPort(CSwitchPort& switchport)
  {
    if (SwitchPortNum >= MAX_SWITCHPORT_NUM)
      return 1;
    SwitchPort[SwitchPortNum] = switchport;
    SwitchPortNum++;
    return 0;
  }
  int EditSwitchPort(int portno, CSwitchPort& switchport)
  {
    if (portno < 0 || portno >= MAX_SWITCHPORT_NUM)
      return 1;
    SwitchPort[portno] = switchport;
    return 0;
  }
  int InsertSwitchPort(int portno, CSwitchPort& switchport)
  {
    if (portno < 0 || portno >= MAX_SWITCHPORT_NUM)
      return 1;
    if (SwitchPortNum >= MAX_SWITCHPORT_NUM)
      return 2;
    for (int i=SwitchPortNum; i>=portno; i--)
    {
      SwitchPort[i+1].m_strPortID = SwitchPort[i].m_strPortID;
      SwitchPort[i+1].m_strFlwCode = SwitchPort[i].m_strFlwCode;
      SwitchPort[i+1].m_nLgChType = SwitchPort[i].m_nLgChType;
      SwitchPort[i+1].m_nChStyle = SwitchPort[i].m_nChStyle;
      SwitchPort[i+1].m_nPhyPortNo = SwitchPort[i].m_nPhyPortNo;
    }
    SwitchPort[portno] = switchport;

    SwitchPortNum++;
    return 0;
  }
  void DelSwitchPort(int portno)
  {
    for (int i=portno; i<SwitchPortNum-1; i++)
    {
      SwitchPort[i].m_strPortID = SwitchPort[i+1].m_strPortID;
      SwitchPort[i].m_strFlwCode = SwitchPort[i+1].m_strFlwCode;
      SwitchPort[i].m_nLgChType = SwitchPort[i+1].m_nLgChType;
      SwitchPort[i].m_nChStyle = SwitchPort[i+1].m_nChStyle;
      SwitchPort[i].m_nPhyPortNo = SwitchPort[i+1].m_nPhyPortNo;
    }
    if (SwitchPortNum > 0)
      SwitchPortNum --;
  }
  bool isThePortIDExist(int portno, AnsiString portid)
  {
    for (int i=0; i<SwitchPortNum; i++)
    {
      if (portid == SwitchPort[i].m_strPortID && i != portno)
        return true;
    }
    return false;
  }
  AnsiString GetPortID(int portno)
  {
    if (portno < 0 || portno >= MAX_SWITCHPORT_NUM)
      return "";
    return SwitchPort[portno].m_strPortID;
  }
  int GetPortNo(AnsiString portid)
  {
    for (int i=0; i<SwitchPortNum; i++)
    {
      if (portid == SwitchPort[i].m_strPortID)
        return i;
    }
    return -1;
  }
};

//语音资源通道类
class CVopChn
{
public:
  short m_nValid; //0-未配置 1-启动 2-关闭

  short m_nVopNo; //对应的语音机编号 0-表示为IVR本地语音节点
  short m_nVopChnNo; //对应的语音资源号（每个语音机的资源通道编号）
  short m_nVopChnType; //对应的语音资源类型：-1表示未知 0-IVR/FAX 1-IVR 2-FAX 3-模拟话机坐席监录 4-数字话机坐席监录 5-VOIP坐席监录 6-模拟中继监录 7-数字中继监录

  short m_PortNo;
  AnsiString m_strPortID;

public:
  CVopChn()
  {
    m_nValid = 0;
    m_nVopNo = -1;
    m_nVopChnNo = -1;
    m_nVopChnType = 0;
    m_PortNo = 0;
    m_strPortID = "";
  }
  virtual ~CVopChn(){}
  const CVopChn& operator=(const CVopChn& vopchn)
  {
    m_nValid = vopchn.m_nValid;
    m_nVopNo = vopchn.m_nVopNo;
    m_nVopChnNo = vopchn.m_nVopChnNo;
    m_nVopChnType = vopchn.m_nVopChnType;
    m_PortNo = vopchn.m_PortNo;
    m_strPortID = vopchn.m_strPortID;
    return *this;
  }
};

#define MAX_VOPCHN_NUM 512

//语音机资源通道列表
class CVop
{
public:
  short m_nValid; //该语音机是否启用 1-是 0否
  short m_nVopChnNum; //该语音机总的语音通道数量
  CVopChn m_VopChns[MAX_VOPCHN_NUM];

public:
  CVop()
  {
    m_nVopChnNum = 0;
  }
  int AddVopChn(CVopChn& vopchn)
  {
    if (m_nVopChnNum >= MAX_VOPCHN_NUM)
      return 1;
    m_VopChns[m_nVopChnNum] = vopchn;
    m_nVopChnNum++;
    return 0;
  }
  int EditVopChn(int vopchnno, CVopChn& vopchn)
  {
    if (vopchnno < 0 || vopchnno >= MAX_VOPCHN_NUM)
      return 1;
    m_VopChns[vopchnno] = vopchn;
    return 0;
  }
  int InsertVopChn(int vopchnno, CVopChn& vopchn)
  {
    if (vopchnno < 0 || vopchnno >= MAX_VOPCHN_NUM)
      return 1;
    if (m_nVopChnNum >= MAX_VOPCHN_NUM)
      return 2;
    for (int i=m_nVopChnNum; i>=vopchnno; i--)
    {
      m_VopChns[i+1].m_nVopChnNo = m_VopChns[i].m_nVopChnNo;
      m_VopChns[i+1].m_nVopChnType = m_VopChns[i].m_nVopChnType;
      m_VopChns[i+1].m_PortNo = m_VopChns[i].m_PortNo;
      m_VopChns[i+1].m_strPortID = m_VopChns[i].m_strPortID;
    }
    m_VopChns[vopchnno] = vopchn;

    m_nVopChnNum++;
    return 0;
  }
  void DelVopChn(int vopchnno)
  {
    for (int i=vopchnno; i<m_nVopChnNum-1; i++)
    {
      m_VopChns[i].m_nVopChnNo = m_VopChns[i+1].m_nVopChnNo;
      m_VopChns[i].m_nVopChnType = m_VopChns[i+1].m_nVopChnType;
      m_VopChns[i].m_PortNo = m_VopChns[i+1].m_PortNo;
      m_VopChns[i].m_strPortID = m_VopChns[i+1].m_strPortID;
    }
    if (m_nVopChnNum > 0)
      m_nVopChnNum --;
  }
};

#define MAX_VOP_NUM 9

//语音机列表
class CVopGroup
{
public:
  short m_nVopNum;
  CVop m_Vops[MAX_VOP_NUM];

public:
  CVopGroup()
  {
    m_nVopNum = 0;
  }
};

#define MAX_AGENTCALLCOUNT_NUM 100

//统计项目显示设置
class CCallCountItemSet
{
public:
  int ItemNo; //项目编号
  AnsiString ItemName; //项目名称
  int DispId; //是否显示 0-否 1-是
  int ItemListIndex; //显示的列表序号
  int AlartId; //是否告警 0-否 1-是
  int AlartValue; //告警门限值
  int CompType; //比较方法 0=等于 1-大于 2-小于
  int ClearAlartId; //已清楚告警标志 0-否 1-是
  int AlartColor; //告警颜色 0-无 1-篮、2-黄、3-橙、4-红色
  int AlartSound; //告警声音 0-无 1-有
  int ValueType; //统计数值类型 1-时长 2-次数 2-百分比

public:
  CCallCountItemSet()
  {
    ItemNo = -1;
    ItemName = "";
    DispId = 0;
    ItemListIndex = -1;
    AlartId = 0;
    AlartValue = 0;
    CompType = 0;
    ClearAlartId = 0;
    AlartColor = 0;
    AlartSound = 0;
    ValueType = 0;
  }
};

class CCallCountItemSetList
{
public:
  int ItemNum;
  CCallCountItemSet CallCountItemSet[MAX_AGENTCALLCOUNT_NUM];

  int StatusCountItemNum;
  AnsiString StatusCountItemName[32];

public:
  CCallCountItemSetList()
  {
    ItemNum = 0;
    StatusCountItemNum = 0;
  }

  int GetItemNoByItemListIndex(int nItemIndex)
  {
    for (int i=0; i<MAX_AGENTCALLCOUNT_NUM; i++)
    {
      if (CallCountItemSet[i].ItemListIndex == nItemIndex)
        return i;
    }
    return -1;
  }
};

//坐席话务统计参数
class CAgentCallCountParam
{
public:
  int WorkerNo;
  AnsiString WorkerName;
  AnsiString SeatNo;
  int curStatus;
  /*
  0	未登录
  1	登录
  2	示忙态
  3	空闲态
  4	呼入振铃
  5	呼出占用
  6	呼出振铃
  7	通话状态
  8	保持状态
  9 话后处理
  10 离席小休态
  11 来话放弃
  */
  int curTimeLen;
  int ItemData[MAX_AGENTCALLCOUNT_NUM];
  /*
  01 累计未登录时长
  02 累计登录时长
  03 累计示忙时长
  04 累计离席时长
  05 累计示闲时长
  06 来话分配总次数
  07 来话应答总次数
  08 来话未应答总次数
  09 来话放弃总次数
  10 呼入累计等待时长

  11 呼入最长等待时长
  12 呼入最短等待时长
  13 呼入平均等待时长
  14 呼入应答超时总次数
  15 呼入累计放弃前等待时长
  16 呼入放弃前最长等待时长
  17 呼入放弃前最短等待时长
  18 呼入放弃前平均等待时长
  19 呼入累计通话时长
  20 呼入最长通话时长

  21 呼入最短通话时长
  22 呼入平均通话时长
  23 呼入通话超时总次数
  24 呼入累计话后处理时长
  25 呼入最长话后处理时长
  26 呼入最短话后处理时长
  27 呼入平均话后处理时长
  28 呼入话后处理超时总次数
  29 呼入累计话务处理时长
  30 呼入最长话务处理时长

  31 呼入最短话务处理时长
  32 呼入平均话务处理时长
  33 来话放弃率
  34 来话接通率
  35 呼出总次数
  36 呼出应答总次数
  37 呼出未应答总次数
  38 呼出累计等待时长
  39 呼出最长等待时长
  40 呼出最短等待时长

  41 呼出平均等待时长
  42 呼出应答超时总次数
  43 呼出累计通话时长
  44 呼出最长通话时长
  45 呼出最短通话时长
  46 呼出平均通话时长
  47 呼出通话超时总次数
  48 呼出累计话后处理时长
  49 呼出最长话后处理时长
  50 呼出最短话后处理时长

  51 呼出平均话后处理时长
  52 呼出话后处理超时总次数
  53 呼出累计话务处理时长
  54 呼出最长话务处理时长
  55 呼出最短话务处理时长
  56 呼出平均话务处理时长
  57 呼出失败率
  58 呼出接通率
  59 通话超时总次数
  60 累计通话总时长

  61 累计通话总次数
  62 话后处理超时总次数
  63 累计话后处理总时长
  64 累计话务处理时长
  65 最长话务处理时长
  66 最短话务处理时长
  67 平均话务处理时长
  68 保持总次数
  69 累计保持时长
  70 最长保持时长

  71 最短保持时长
  72 平均保持时长
  73 转接总次数
  */
  int ItemListIndex; //显示的列表序号
  int AlartingId[MAX_AGENTCALLCOUNT_NUM]; //正在告警标志 0-否 1-是
  int AlartingFlag;

public:
  void Reset()
  {
    WorkerNo = 0;
    WorkerName = "";
    SeatNo = "";
    curStatus = 0;
    curTimeLen = 0;
    for (int i=0; i<MAX_AGENTCALLCOUNT_NUM; i++)
    {
      ItemData[i] = 0;
      AlartingId[i] = 0;
    }
    ItemListIndex = -1;
    AlartingFlag = 0;
  }

  CAgentCallCountParam()
  {
    Reset();
  }
  virtual ~CAgentCallCountParam(){};
};

class CAgentCallCountParamList
{
public:
  CAgentCallCountParam AgentCallCountParam[256];

public:
  CAgentCallCountParamList(){};
  virtual ~CAgentCallCountParamList(){};

  int GetSaveWorkerIndex(int nWorkerNo)
  {
    for (int i=0; i<256; i++)
    {
      if (AgentCallCountParam[i].WorkerNo == nWorkerNo)
        return i;
    }
    for (int i=0; i<256; i++)
    {
      if (AgentCallCountParam[i].WorkerNo == 0)
        return i;
    }
    for (int i=0; i<256; i++)
    {
      if (AgentCallCountParam[i].curStatus == 0)
        return i;
    }
    return -1;
  }
  int GetSaveWorkerIndexByDispIndex(int nDispIndex)
  {
    for (int i=0; i<256; i++)
    {
      if (AgentCallCountParam[i].ItemListIndex == nDispIndex)
        return i;
    }
    return 0;
  }
  bool IsAlarting()
  {
    for (int i=0; i<256; i++)
    {
      if (AgentCallCountParam[i].AlartingFlag > 0)
        return true;
    }
    return false;
  }
};

//坐席话务统计参数
class CGroupCallCountParam
{
public:
  int GroupNo;
  AnsiString GroupName;
  int StatusCount[16];

  int ItemData[MAX_AGENTCALLCOUNT_NUM];

  int ItemListIndex; //显示的列表序号
  int AlartingId[MAX_AGENTCALLCOUNT_NUM]; //正在告警标志 0-否 1-是
  int AlartingFlag;

public:
  void Reset()
  {
    GroupNo = 0;
    GroupName = "";
    for (int i=0; i<16; i++)
    {
      StatusCount[i] = 0;
    }

    for (int i=0; i<MAX_AGENTCALLCOUNT_NUM; i++)
    {
      ItemData[i] = 0;
      AlartingId[i] = 0;
    }
    ItemListIndex = -1;
    AlartingFlag = 0;
  }

  CGroupCallCountParam()
  {
    Reset();
  }
  virtual ~CGroupCallCountParam(){};
};

class CGroupCallCountParamList
{
public:
  CGroupCallCountParam GroupCallCountParam[32];

public:
  CGroupCallCountParamList(){};
  virtual ~CGroupCallCountParamList(){};

  int GetSaveGroupIndex(int nGroupNo)
  {
    if (nGroupNo == 0)
      return 0;
    for (int i=1; i<32; i++)
    {
      if (GroupCallCountParam[i].GroupNo == nGroupNo)
        return i;
    }
    for (int i=1; i<32; i++)
    {
      if (GroupCallCountParam[i].GroupNo == 0)
        return i;
    }
    return -1;
  }
  int GetSaveGroupIndexByDispIndex(int nDispIndex)
  {
    for (int i=0; i<32; i++)
    {
      if (GroupCallCountParam[i].ItemListIndex == nDispIndex)
        return i;
    }
    return 0;
  }
  bool IsAlarting()
  {
    for (int i=0; i<32; i++)
    {
      if (GroupCallCountParam[i].AlartingFlag > 0)
        return true;
    }
    return false;
  }
};

//---------------------------------------------------------------------------
#endif

