//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include <stdlib.h>
#include <sys\types.h>
#include <time.h>
#include <dir.h>
#include <registry.hpp>
#include <TAPI.h>
#include "../comm/base64.h"
#include "main.h"
#include "selpath.h"
#include "flwsetup.h"
#include "setupacc.h"
#include "route.h"
#include "seat.h"
#include "extseat.h"
#include "pstn.h"
#include "addcode.h"
//#include "trkchart.h"
#include "login.h"
#include "modifypsw.h"
#include "editswitchport.h"
#include "editvopchn.h"
#include "workeralartset.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "SUISkinEngine"
#pragma resource "*.dfm"
TFormMain *FormMain;

extern int gLangID;

AnsiString strAPPTitle;

#define QUARKCALL_PLATFORM

#ifdef QUARKCALL_PLATFORM
#define IVR_SERVICE_NAME      "QuarkCallIVRService"
#define FLW_SERVICE_NAME      "QuarkCallFLWService"
#define DB_SERVICE_NAME       "QuarkCallDBServer"
#define CTILINK_SERVICE_NAME  "QuarkCallCTILINKService"
#define VOC_SERVICE_NAME      "QuarkCallVOCService"
#define WEB_SERVICE_NAME      "QuarkCallWEBService"
#define TTS_SERVICE_NAME      "QuarkCallTTSService"
#define SMS_SERVICE_NAME      "QuarkCallSMSService"
#define DOC_SERVICE_NAME      "QuarkCallDOCService"
#define WS_SERVICE_NAME       "QuarkCallWebSocket"
#else
#define IVR_SERVICE_NAME      "UnimeCallIVRServer"
#define FLW_SERVICE_NAME      "UnimeCallFLWServer"
#define DB_SERVICE_NAME       "UnimeCallDBService"
#define CTILINK_SERVICE_NAME  "UnimeCTILINKServer"
#define VOC_SERVICE_NAME      "UnimeCallVOCService"
#define WEB_SERVICE_NAME      "UnimeCallWEBServer"
#define TTS_SERVICE_NAME      "UnimeCallTTSServer"
#define SMS_SERVICE_NAME      "UnimeCallSMSServer"
#define WS_SERVICE_NAME      "UnimeCallWebSocket"
#endif

#define MAX_MSG_QUEUE_NUM 1024 //最大消息队列数
extern void MDString (char *string, char *AuthKey);
int g_nSkinId=0;
AnsiString strSelSkinFile="";
AnsiString strSkinFile[11], strSkinName[11];

//全局变量定义
// Handle of Internet Session (InternetOpen)
HINTERNET hInternet = NULL;
// Handle of Internet Service Session (InternetConnect)
HINTERNET hConnect = NULL;

int g_nIVRAlarmId=0; //IVR告警标志
int g_nFLWAlarmId=0; //FLW告警标志
int g_nDBAlarmId=0; //DB告警标志
int g_nCHAlarmId=0; //中继通道告警标志

int g_nAuthSwitchType=0, g_nAuthMaxSeat=0, g_nAuthMaxIVR=0, g_nAuthMaxCTIPort=0;

AnsiString imgRedFile, imgBlueFile, imgYellowFile, imgGreenFile, imgStartFile, imgStopFile;

int g_nLoginId=0; //登录平台标志 0-未登录 1-登录成功
int g_nLoginlevelId=0; //登录级别 1-administrator 2-manager 3-operator 4-monitor 5-board
AnsiString g_strLoginUsername="", g_strPassword="";
AnsiString g_strAdminPSW, g_strManagerPSW, g_strOperatorPSW, g_strMonitorPSW;
bool g_bAutoLoginId=false;
bool g_bRunDebug=false;

int g_nConnectIVRId=0; //0-启动状态还TCP未连接 1-已断开 2-登录失败 3-tcp已连接 4-已登录
int g_nConnectFLWId=0; //0-启动状态还TCP未连接 1-已断开 2-登录失败 3-tcp已连接 4-已登录
int g_nConnectDBId=0; //0-启动状态还TCP未连接 1-已断开 2-登录失败 3-tcp已连接 4-已登录
int g_nConnectCTRLId=0; //0-启动状态还TCP未连接 1-已断开 2-登录失败 3-tcp已连接 4-已登录

int g_nRunMode=0; //0-运行在windows版本平台非本机上 1-运行在windows版本平台本机上 2-linux版本
int g_nSwitchMode=0;
int g_nModifyRunMode=0;
bool g_bLocaAlartId=false; //本地告警音开关标志
bool g_bRunOnServicePCId=true; //运行在服务程序机器标志 0-否 1-是

AnsiString g_strExecPath; //执行文件目录
AnsiString g_strUcommPath; //平台安装目录

CXMLMsg g_RecvMsg; //接收的消息体
char g_szSendMsgBuf[2048]; //发送的消息字符串

CFlwRule *g_pFlwRule = NULL; //消息规则
CMsgfifo *g_pMsgfifo = NULL; //消息队列指针
CTCPClient *g_pTcpClient = NULL; //TCP客户端

//通道显示状态
int g_nMaxChnNum=0;
int g_nDispChnNum=0;
int g_nMaxVopNum=0;
int g_nDispVopNum=0;
CChnStatus *g_pChnStatus = NULL;
CVopStatus *g_pVopStatus = NULL;
int g_nMaxAgNum;
CAgStatus *g_pAgStatus = NULL;

int g_nLOGClientId = 1; //监控器客户端编号
bool g_bGetqueueinfo = false; //取ACD分配队列信息
bool g_bGetagentstatus = false; //取所有坐席的状态信息
bool g_bGetchnstatus = false; //取所有通道的状态信息
bool g_bGetivrtracemsg = false; //取ivr日志跟踪信息
bool g_bGetloadedflws = false; //取已加载的流程信息
bool g_bGetflwtracemsg = false; //取flw日志跟踪信息
bool g_bGettrkcallcount = false;
bool g_bGetagcallcount = false;

int g_nChnBusyCount=0;
int g_nChnIdelCount=0;
int g_nChnBlockCount=0;
int g_nChnInCount=0;
int g_nChnOutCount=0;

int g_nIVRBusyCount=0;
int g_nIVRIdelCount=0;
int g_nRecBusyCount=0;
int g_nRecIdelCount=0;
int g_nVopBlockCount=0;

int g_nAgLoginCount = 1024;
int g_nAgLogoutCount = 0;
int g_nAgIdelCount = 0;
int g_nAgBusyCount = 0;
int g_nAgLeavelCount = 0;

int g_nAcdWaitNum = 0;

bool g_bSeatModifyId=false; //坐席参数修改中是否增加或删除过坐席数量，如果坐席数量有变动，就需要重启IVR服务

//告警提醒参数
int  g_nMaxBusyTrunk=0;
bool g_bBusyTrunkSound=false;
bool g_bBusyTrunkColor=false;
bool g_bBlockTrunkSound=false;

int  g_nMinAgentLogin=0;
bool g_bAgentLoginSound=false;
bool g_bAgentLoginColor=false;

int  g_nMaxAgentLeval=0;
bool g_bAgentLevalSound=false;
bool g_bAgentLevalColor=false;

int  g_nMaxBusyAgent=0;
bool g_bBusyAgentSound=false;
bool g_bBusyAgentColor=false;

int  g_nMaxACDWaiting=0;
bool g_bACDWaitingSound=false;
bool g_bACDWaitingColor=false;

int  g_nMaxBusyIVR=0;
bool g_bBusyIVRSound=false;
bool g_bBusyIVRColor=false;
bool g_bBlockIVRSound=false;

//当前选择的参数项索引号
int g_nCurrParamSelIndex[5];
AnsiString g_nCurrParamSelName[5];

AnsiString g_strAlartWavFile;
int g_nAlartWavId=1;

//当前选择的表单行号
int g_CurFlwNo=-1;
int g_CurAccNo=-1;
int g_CurPstn=-1;
int g_CurSwitchPort=-1;
int g_CurRouteNo=-1;
int g_CurVopNo=0;
int g_CurVopChnNo=-1;
int g_CurSeatPort=-1;
int g_CurExtSeatPort=-1;
int g_CurAgNo=-1;
AnsiString g_CurSeatNo="";
int g_CurChnNo=-1;
int g_CurSelFlw=-1;
int g_CurSelTrkCount=-1;
int g_CurSelAgCount=-1;
int g_CurSelToolButton=0;

int g_nTrkItemNum;
AnsiString TrkRemark[20];
int g_nAgItemNum;
AnsiString AgRemark[20];

//实时状态统计
int g_nSelDispWorkerIndex=0;
int g_nSelDispGroupIndex=0;
int g_nSelWorkerIndex=0;
int g_nSelGroupIndex=0;

int g_nSelDispWorkerCallIndex=0;
int g_nSelDispGroupCallIndex=0;
int g_nSelWorkerItemNo=0;
int g_nSelGroupItemNo=0;

int g_nCallCountTotal=48; //话务统计的时段数（24，48）
CTrkCount g_TodayTrkCount[48]; //当日中继话务统计数据
CAgCount g_TodayAgCount[48]; //当日坐席话务统计数据

//字符串分割变量
TStringList *pstrChnList = NULL;
TStringList *pstrAgList = NULL;
TStringList *pstrAcdList1 = NULL;
TStringList *pstrAcdList2 = NULL;
TStringList *pstrTrkCountList = NULL;
TStringList *pstrAgCountList = NULL;

int MyIsNumber(AnsiString str);
int MyIsDigits(AnsiString str);

int MySplit(const char *pszSource, TStringList *pstrList);
int MySplit(const char *pszSource, char cSplitChar, TStringList *pstrList);
void SendmodifyiniparamMSG(int nSrvType, const char *pszIniFileName, const char *pszSectName, const char *pszKeyName, const char *pszValue);

AnsiString SERVICEIP, FTPUser, FTPPSW;
int FTPPort;
/*
各个参数如下:
DEVICE=物理设备名
ONBOOT=[yes|no]（引导时是否激活设备）
BOOTPROTO=[none|static|bootp|dhcp]（引导时不使用协议|静态分配|BOOTP协议|DHCP协议）
HWADDR = MAC地址
IPADDR=IP地址
NETMASK=掩码值
GATEWAY=网关地址
NETWORK=网络地址
BROADCAST=广播地址
USERCTL=[yes|no]（非root用户是否可以控制该设备）
IPV6INIT=[yes|no]（是否允许IPV6）
IPV6ADDR=IPV6地址
*/

//0-DEVICE= 1-ONBOOT= 2-BOOTPROTO= 3-HWADDR= 4-IPADDR= 5-NETMASK= 6-GATEWAY= 7-BROADCAST=
AnsiString strServerIp[10];
//0-NETWORKING=  1-HOSTNAME=  2-GATEWAY=
AnsiString strServerGw[5];
//0-dns1 1-dns2 2-domain 3-search
AnsiString strServerDns[5];

TStringList *strTSPList = NULL;
DWORD ldPermanentProviderID[1024];

typedef long (WINAPI *MYLINEADDPROVIDER)(LPCSTR lpszProviderFilename, HWND hwndOwner, LPDWORD lpdwPermanentProviderID);
MYLINEADDPROVIDER mylineAddProvider;

typedef long (WINAPI *MYLINEGETPROVIDERLIST)(DWORD dwAPIVersion, LPLINEPROVIDERLIST lpProviderList);
MYLINEGETPROVIDERLIST mylineGetProviderList;

typedef long (WINAPI *MYLINEREMOVEPROVIDER)(DWORD dwPermanentProviderID, HWND hwndOwner);
MYLINEREMOVEPROVIDER mylineRemoveProvider;

typedef long (WINAPI *MYLINECONFIGPROVIDER)(HWND hwndOwner, DWORD dwPermanentProviderID);
MYLINECONFIGPROVIDER mylineConfigProvider;

bool TFormMain::LoadTAPIDll()
{
	hdll = LoadLibrary("tapi32.dll");
	if ( hdll != NULL )
  {
    mylineAddProvider = (MYLINEADDPROVIDER)GetProcAddress( hdll, "lineAddProvider" );
    mylineGetProviderList = (MYLINEGETPROVIDERLIST)GetProcAddress( hdll, "lineGetProviderList" );
    mylineRemoveProvider = (MYLINEREMOVEPROVIDER)GetProcAddress( hdll, "lineRemoveProvider" );
    mylineConfigProvider = (MYLINECONFIGPROVIDER)GetProcAddress( hdll, "lineConfigProvider" );
    return 0;
  }
  return 1;
}
void TFormMain::ReleaseTAPIDll()
{
  if ( hdll != NULL )
  {
    FreeLibrary( hdll );
  }
}

//-----------------------------------------------------------------------------
__fastcall TFormMain::TFormMain(TComponent* Owner)
  : TForm(Owner)
{
  LOGINIFile = ExtractFilePath(Application->ExeName)+"config.ini";
  if (gLangID == 1)
  {
    g_nCurrParamSelIndex[0] = 0;
    g_nCurrParamSelName[0] = "监控端参数";
    g_nCurrParamSelIndex[1] = 2;
    g_nCurrParamSelName[1] = "平台网络参数";
    g_nCurrParamSelIndex[2] = 9;
    g_nCurrParamSelName[2] = "通道状态显示";
    g_nCurrParamSelIndex[3] = 12;
    g_nCurrParamSelName[3] = "IVR跟踪消息";
    g_nCurrParamSelIndex[4] = 15;
    g_nCurrParamSelName[4] = "中继占用统计";
  }
  else
  {
    g_nCurrParamSelIndex[0] = 0;
    g_nCurrParamSelName[0] = "菏北狠把计";
    g_nCurrParamSelIndex[1] = 2;
    g_nCurrParamSelName[1] = "キ籓呼隔把计";
    g_nCurrParamSelIndex[2] = 9;
    g_nCurrParamSelName[2] = "硄笵篈陪ボ";
    g_nCurrParamSelIndex[3] = 12;
    g_nCurrParamSelName[3] = "IVR蛤萝";
    g_nCurrParamSelIndex[4] = 15;
    g_nCurrParamSelName[4] = "い膥ノ参璸";
  }
}
int CheckIPv4Address(const char *pszIPAddr);
int MyGetIPd(AnsiString strIP);
void TFormMain::ReadLOGParam()
{
  int i;
  TIniFile *IniKey = new TIniFile (LOGINIFile);

  strAPPTitle = IniKey->ReadString ( "CONFIG", "Title", "" );

  if (gLangID == 1)
    g_nAlartWavId = IniKey->ReadInteger ( "ALART", "AlartId", 1);
  else
    g_nAlartWavId = IniKey->ReadInteger ( "ALART", "AlartId", 1);
  switch (g_nAlartWavId)
  {
  case 1:
    if (gLangID == 1)
      cbSoundType->Text = "警笛";
    else
      cbSoundType->Text = "牡裁";
    g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart1.wav";
    break;
  case 2:
    if (gLangID == 1)
      cbSoundType->Text = "嘀嘀";
    else
      cbSoundType->Text = "箍箍";
    g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart2.wav";
    break;
  case 3:
    if (gLangID == 1)
      cbSoundType->Text = "哔哔";
    else
      cbSoundType->Text = "雇雇";
    g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart3.wav";
    break;
  default:
    if (gLangID == 1)
      cbSoundType->Text = "警笛";
    else
      cbSoundType->Text = "牡裁";
    g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart1.wav";
    break;
  }

  MediaPlayer1->FileName = g_strAlartWavFile;

  g_bAutoLoginId = IniKey->ReadBool("CONFIG", "AutoLoginId", false);
  g_strLoginUsername = IniKey->ReadString ( "CONFIG", "AutoLoginUser", "administrator" );
  g_strPassword = IniKey->ReadString ( "CONFIG", "AutoLoginPsw", "" );
  g_bRunDebug = IniKey->ReadBool("CONFIG", "RunDebug", false);

  g_nSkinId = IniKey->ReadInteger ( "SKIN", "SkinId", 0 );
  for (int i=1; i<11; i++)
  {
    strSkinName[i] = IniKey->ReadString ( "SKIN", "Skins[" + IntToStr(i) + "].Name", "" );
    strSkinFile[i] = IniKey->ReadString ( "SKIN", "Skins[" + IntToStr(i) + "].File", "" );

    strSelSkinFile = ExtractFilePath(Application->ExeName)+"Skins\\"+strSkinFile[i];
    if (strSkinName[i].Length() > 0 && FileExists(strSelSkinFile) == true)
    {
      cbSkins->Items->Add(strSkinName[i]);
    }
  }
  if (g_nSkinId == 0)
  {
    cbSkins->Text = (gLangID == 1) ? "无显示风格" : "礚陪ボ";
  }
  else if (g_nSkinId > 0 && g_nSkinId <= 10 && strSkinName[g_nSkinId].Length() > 0 && strSkinFile[g_nSkinId].Length() > 0)
  {
    strSelSkinFile = ExtractFilePath(Application->ExeName)+"Skins\\"+strSkinFile[g_nSkinId];
    if (FileExists(strSelSkinFile) == true)
    {
      cbSkins->Text = strSkinName[g_nSkinId];
      suiSkinEngine1->SkinFile = strSelSkinFile;
      suiSkinEngine1->Active = true;
    }
  }

  g_nTrkItemNum = IniKey->ReadInteger("TRKCOUNTITEM", "ITEMNUM", 0);
  for (i = 0; i < g_nTrkItemNum; i ++)
  {
    if (gLangID == 1)
      TrkRemark[i] = IniKey->ReadString("TRKCOUNTITEM", "ITEM["+IntToStr(i)+"]", "统计项目["+IntToStr(i)+"]");
    else
      TrkRemark[i] = IniKey->ReadString("TRKCOUNTITEM-CHT", "ITEM["+IntToStr(i)+"]", "参璸兜ヘ["+IntToStr(i)+"]");
  }
  g_nAgItemNum = IniKey->ReadInteger("AGCOUNTITEM", "ITEMNUM", 0);
  for (i = 0; i < g_nAgItemNum; i ++)
  {
    if (gLangID == 1)
      AgRemark[i] = IniKey->ReadString("AGCOUNTITEM", "ITEM["+IntToStr(i)+"]", "统计项目["+IntToStr(i)+"]");
    else
      AgRemark[i] = IniKey->ReadString("AGCOUNTITEM-CHT", "ITEM["+IntToStr(i)+"]", "参璸兜ヘ["+IntToStr(i)+"]");
  }
  suiSkinEngine1->Active = true;

  IVRINIFile = IniKey->ReadString("CONFIG", "IVRINIFile", "");
  CTILINKINIFile = IniKey->ReadString("CONFIG", "CTILINKINIFile", "");
  FLWINIFile = IniKey->ReadString("CONFIG", "FLWINIFile", "");
  DBINIFile = IniKey->ReadString("CONFIG", "DBINIFile", "");
  TTSINIFile = IniKey->ReadString("CONFIG", "TTSINIFile", "");

  editLocaIP->Text = IdIPWatch1->LocalIP();
  g_nLOGClientId = MyGetIPd(IdIPWatch1->LocalIP());
  IniKey->WriteInteger("CONFIG", "ClientId", g_nLOGClientId);

  //g_nLOGClientId = IniKey->ReadInteger("CONFIG", "ClientId", 1);
  //cseLOGId->Value = g_nLOGClientId;

  editConnectIVRIP->Text = IniKey->ReadString("CONFIG", "ServerIP", "127.0.0.1");
  SERVICEIP = editConnectIVRIP->Text;

  cseLinuxPort->Value = IniKey->ReadInteger("CONFIG", "CTRLServerPort", 5226);
  cseConnectIVRPort->Value = IniKey->ReadInteger("CONFIG", "IVRServerPort", 5227);
  cseConnectDBPort->Value = IniKey->ReadInteger("CONFIG", "DBServerPort", 5228);
  cseConnectFLWPort->Value = IniKey->ReadInteger("CONFIG", "FLWServerPort", 5229);

  editIVRFTPUser->Text = IniKey->ReadString("CONFIG", "FTPUser", "ftpuser");
  FTPUser = editIVRFTPUser->Text;
  editIVRFTPPSW->Text = IniKey->ReadString ( "CONFIGURE", "FTPPSW", "ftpuser" );
  FTPPSW = editIVRFTPPSW->Text;
  cseIVRFTPPort->Value = IniKey->ReadInteger("CONFIG", "FTPPort", 21);
  FTPPort = cseIVRFTPPort->Value;

  g_bRunOnServicePCId = IniKey->ReadBool("CONFIG", "RunOnServicePCId", 1);
  ckIsTheSamePC->Checked = g_bRunOnServicePCId;

  //0-运行在windows版本平台非本机上 1-运行在windows版本平台本机上 2-linux版本
  g_nRunMode = IniKey->ReadInteger("CONFIG", "RunMode", 1);
  g_nModifyRunMode = g_nRunMode;
  if (g_nRunMode == 2)
  {
    cbOSType->Text = "LINUX";
    Label35->Visible = true;
    cseLinuxPort->Visible = true;
    ckIsTheSamePC->Visible = false;
    imgConnectCTRLStatus->Picture->LoadFromFile(imgRedFile);
    imgConnectIVRStatus->Picture->LoadFromFile(imgRedFile);
    imgConnectFLWStatus->Picture->LoadFromFile(imgRedFile);
    imgConnectDBStatus->Picture->LoadFromFile(imgRedFile);
  }
  else
  {
    if (g_nRunMode == 1)
    {
      Button4->Visible = true;
      //Button17->Visible = true;
      //Button21->Visible = true;
      //GroupBox8->Visible = true;
      //GroupBox9->Visible = true;
    }
    cbOSType->Text = "WIN32";
    Label35->Visible = false;
    cseLinuxPort->Visible = false;
    ckIsTheSamePC->Visible = true;
    imgConnectCTRLStatus->Picture->LoadFromFile(imgGreenFile);
    if (gLangID == 1)
      FormMain->imgConnectCTRLStatus->Hint = "已登录到平台服务器";
    else
      FormMain->imgConnectCTRLStatus->Hint = "祅魁キ籓狝竟";
    imgConnectIVRStatus->Picture->LoadFromFile(imgRedFile);
    imgConnectFLWStatus->Picture->LoadFromFile(imgRedFile);
    imgConnectDBStatus->Picture->LoadFromFile(imgRedFile);

    GroupBox6->Visible = false;
    GroupBox7->Visible = false;

    //Label37->Visible = false;
    //Label45->Visible = false;
    //Label61->Visible = false;
    //Label62->Visible = false;
    //Label63->Visible = false;
    //cbxDBType->Visible = false;
    //edtServer->Visible = false;
    //edtDB->Visible = false;
    //edtUSER->Visible = false;
    //edtPSW->Visible = false;
  }


  g_bLocaAlartId = IniKey->ReadBool("CONFIG", "LocaAlartId", true);
  ckLocaAlartId->Checked = g_bLocaAlartId;

  ckBlockTrunkSound->Checked = IniKey->ReadBool("ALART", "BlockTrunkSound", true);
  ckAgentLoginSound->Checked = IniKey->ReadBool("ALART", "AgentLoginSound", true);
  ckBusyTrunkSound->Checked = IniKey->ReadBool("ALART", "BusyTrunkSound", true);
  ckAgentLevalSound->Checked = IniKey->ReadBool("ALART", "AgentLevalSound", true);
  ckACDWaitingSound->Checked = IniKey->ReadBool("ALART", "ACDWaitingSound", true);
  ckBusyAgentSound->Checked = IniKey->ReadBool("ALART", "BusyAgentSound", true);
  ckBusyIVRSound->Checked = IniKey->ReadBool("ALART", "BusyIVRSound", true);
  ckSeatSatusSound->Checked = IniKey->ReadBool("ALART", "SeatSatusSound", true);

  g_bGetqueueinfo = IniKey->ReadBool("CONFIG", "Getqueueinfo", true); //取ACD分配队列信息
  ckGetqueueinfo->Checked = g_bGetqueueinfo;

  g_bGetagentstatus = IniKey->ReadBool("CONFIG", "Getagentstatus", true); //取所有坐席的状态信息
  ckGetagentstatus->Checked = g_bGetagentstatus;

  g_bGetchnstatus = IniKey->ReadBool("CONFIG", "Getchnstatus", true); //取所有通道的状态信息
  ckGetchnstatus->Checked = g_bGetchnstatus;

  g_bGetivrtracemsg = IniKey->ReadBool("CONFIG", "Getivrtracemsg", false); //取ivr日志跟踪信息
  ckGetivrtracemsg->Checked = g_bGetivrtracemsg;

  g_bGetloadedflws = IniKey->ReadBool("CONFIG", "Getloadedflws", true); //取已加载的流程信息
  ckGetgetloadedflws->Checked = g_bGetloadedflws;

  g_bGetflwtracemsg = IniKey->ReadBool("CONFIG", "Getflwtracemsg", false); //取flw日志跟踪信息
  ckGetgetflwtracemsg->Checked = g_bGetflwtracemsg;

  g_bGettrkcallcount = IniKey->ReadBool("CONFIG", "Gettrkcallcount", true);
  ckGetTrkCountData->Checked = g_bGettrkcallcount;

  g_bGetagcallcount = IniKey->ReadBool("CONFIG", "Getagcallcount", true);
  ckGetAgCountData->Checked = g_bGetagcallcount;

  delete IniKey;

  if (g_bRunOnServicePCId == false)
  {
    if (IVRINIFile == "" || !(FileExists(IVRINIFile)))
      IVRINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\IVRSERVICE.ini";
    if (CTILINKINIFile == "" || !(FileExists(CTILINKINIFile)))
      CTILINKINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\CTILINKSERVICE.ini";
    if (FLWINIFile == "" || !(FileExists(FLWINIFile)))
      FLWINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\FLWSERVICE.ini";
    if (DBINIFile == "" || !(FileExists(DBINIFile)))
      DBINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\DBSERVICE.ini";
    if (TTSINIFile == "" || !(FileExists(TTSINIFile)))
      TTSINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\TTSSERVICE.ini";
  }
  else
  {
    IVRINIFile = g_strUcommPath+"IVRSERVICE\\IVRSERVICE.ini";
    CTILINKINIFile = g_strUcommPath+"CTILINKSERVICE\\CTILINKSERVICE.ini";
    FLWINIFile = g_strUcommPath+"FLWSERVICE\\FLWSERVICE.ini";
    DBINIFile = g_strUcommPath+"DBSERVICE\\DBSERVICE.ini";
    TTSINIFile = g_strUcommPath+"TTSSERVICE\\TTSSERVICE.ini";
  }

  if (IPINIFile == "" || !(FileExists(IPINIFile)))
    IPINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\SERVICEIP.ini";
  if (GWINIFile == "" || !(FileExists(GWINIFile)))
    GWINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\SERVICEGW.ini";
  if (DNSINIFile == "" || !(FileExists(DNSINIFile)))
    DNSINIFile = ExtractFilePath(Application->ExeName)+"INITEMP\\SERVICEDNS.ini";
}
void TFormMain::WriteLOGParam()
{
  if (bLOGParamChange == true)
  {
    LOGIniKey = new TIniFile (LOGINIFile);

    //LOGIniKey->WriteInteger("CONFIG", "ClientId", cseLOGId->Value);
    LOGIniKey->WriteString("CONFIG", "ServerIP", editConnectIVRIP->Text);

    LOGIniKey->WriteInteger("CONFIG", "CTRLServerPort", cseLinuxPort->Value);
    LOGIniKey->WriteInteger("CONFIG", "IVRServerPort", cseConnectIVRPort->Value);
    LOGIniKey->WriteInteger("CONFIG", "DBServerPort", cseConnectDBPort->Value);
    LOGIniKey->WriteInteger("CONFIG", "FLWServerPort", cseConnectFLWPort->Value);

    LOGIniKey->WriteString("CONFIG", "FTPUser", editIVRFTPUser->Text);
    FTPUser = editIVRFTPUser->Text;
    LOGIniKey->WriteString("CONFIG", "FTPPSW", editIVRFTPPSW->Text);
    FTPPSW = editIVRFTPPSW->Text;
    LOGIniKey->WriteInteger("CONFIG", "FTPPort", cseIVRFTPPort->Value);
    FTPPort = cseIVRFTPPort->Value;

    LOGIniKey->WriteBool("CONFIG", "RunOnServicePCId", ckIsTheSamePC->Checked);
    g_bRunOnServicePCId = ckIsTheSamePC->Checked;

    if (cbOSType->Text == "LINUX")
    {
      LOGIniKey->WriteInteger("CONFIG", "RunMode", 2);
      g_nModifyRunMode = 2;
    }
    else
    {
      if (g_bRunOnServicePCId == true)
      {
        LOGIniKey->WriteInteger("CONFIG", "RunMode", 1);
        g_nModifyRunMode = 1;
      }
      else
      {
        LOGIniKey->WriteInteger("CONFIG", "RunMode", 0);
        g_nModifyRunMode = 0;
      }
    }


    LOGIniKey->WriteBool("CONFIG", "LocaAlartId", ckLocaAlartId->Checked);
    delete LOGIniKey;
    bLOGParamChange = false;
  }
  btApply->Enabled = false;
}
void TFormMain::WriteLOGAutoLogin(AnsiString strUser, AnsiString strPsw, bool bAutoLoginId)
{
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  IniKey->WriteString("CONFIG", "AutoLoginUser", strUser);
  IniKey->WriteString("CONFIG", "AutoLoginPsw", strPsw);
  IniKey->WriteBool("CONFIG", "AutoLoginId", bAutoLoginId);
  delete IniKey;
}
AnsiString GetTimeString(int nSecondCount)
{
  AnsiString strTemp="";
  int nHour = nSecondCount/3600;
  int nMinute = (nSecondCount%3600)/60;
  int nSecond = nSecondCount%60;
  if (nHour > 0)
  {
    if (gLangID == 1)
      strTemp = IntToStr(nHour)+"时"+IntToStr(nMinute)+"分"+IntToStr(nSecond)+"秒";
    else
      strTemp = IntToStr(nHour)+""+IntToStr(nMinute)+"だ"+IntToStr(nSecond)+"";
  }
  else
  {
    if (gLangID == 1)
    {
      if (nMinute > 0)
        strTemp = IntToStr(nMinute)+"分"+IntToStr(nSecond)+"秒";
      else
        strTemp = IntToStr(nSecond)+"秒";
    }
    else
    {
      if (nMinute > 0)
        strTemp = IntToStr(nMinute)+"だ"+IntToStr(nSecond)+"";
      else
        strTemp = IntToStr(nSecond)+"";
    }
  }
  return strTemp;
}
AnsiString GetCallCountValue(int nValue, int nValueType)
{
  AnsiString strTemp="";
  if (nValueType == 1)
  {
    strTemp = GetTimeString(nValue);
  }
  else if (nValueType == 2)
  {
    if (gLangID == 1)
      strTemp = IntToStr(nValue)+"次";
    else
      strTemp = IntToStr(nValue)+"Ω";
  }
  else if (nValueType == 3)
  {
    strTemp = "%"+IntToStr(nValue);
  }
  else
  {
    strTemp = IntToStr(nValue);
  }
  return strTemp;
}

int MyStrToInt(AnsiString str)
{
  int nResult=0;
  try
  {
    if (str != "")
      nResult = StrToInt(str);
  }
  catch (...)
  {
  }
  return nResult;
}
int CheckIPv4Address(const char *pszIPAddr)
{
 //检查长度
  int nIPlen=strlen(pszIPAddr);

  if(nIPlen < 7 || nIPlen > 15)
  {
    return 1;
  }
  //检查点分隔符的个数
  int i, nDotCount=0;
  for(i=0;i<nIPlen;i++)
  {
    if (pszIPAddr[i] != '.' && (pszIPAddr[i] < '0' || pszIPAddr[i] > '9'))
      return 2;
    if(pszIPAddr[i]=='.')
      nDotCount++;
  }

  if(nDotCount != 3)
  {
    return 2;
  }
  char strIp[4][20];
  for(i=0;i<4;i++)
    memset(strIp[i],0,20);
  sscanf(pszIPAddr, "%[^.].%[^.].%[^.].%[^.]", strIp[0],strIp[1],strIp[2],strIp[3]);

  for(i=0;i<4;i++)
  {
    if (strlen(strIp[i]) == 0)
      return 3;
    if(atoi(strIp[i]) > 255 || atoi(strIp[i]) < 0)
    {
      return 4;
    }
  }
  return 0;
}
int MyGetIPd(AnsiString strIP)
{
  int nIPd;
  char strIp[4][20];
  for(int i=0;i<4;i++)
    memset(strIp[i],0,20);
  sscanf(strIP.c_str(), "%[^.].%[^.].%[^.].%[^.]", strIp[0],strIp[1],strIp[2],strIp[3]);
  if (strlen(strIp[3]) == 0)
    return 0;
  nIPd = atoi(strIp[3]);
  if(nIPd > 255 || nIPd < 0)
    return 0;
  return nIPd;
}

void TFormMain::GetServerIp()
{
  FILE *fp1;
  char szLine[256];
  AnsiString strTemp, strLine="";
  int i, nPos;

  fp1 = fopen(IPINIFile.c_str(), "r");
  if (fp1 != NULL)
  {
    while (!feof(fp1))
    {
      strLine="";
      if (fgets(szLine, 256, fp1) > 0)
      {
        strTemp = (char *)szLine;
        for (i=1; i<=strTemp.Length(); i++)
        {
          if (strTemp[i] == ' ' || strTemp[i] == '\n')
            continue;
          strLine += strTemp[i];
        }
        strTemp = (strLine.Trim()).UpperCase();
        nPos = strTemp.AnsiPos("DEVICE=");
        if (nPos > 0)
        {
          strServerIp[0] = strLine.SubString(nPos+7,50);
          continue;
        }
        nPos = strTemp.AnsiPos("ONBOOT=");
        if (nPos > 0)
        {
          strServerIp[1] = strLine.SubString(nPos+7,50);
          continue;
        }
        nPos = strTemp.AnsiPos("BOOTPROTO=");
        if (nPos > 0)
        {
          strServerIp[2] = strLine.SubString(nPos+10,50);
          if (strServerIp[2].AnsiCompareIC("STATIC") == 0 || strServerIp[2].AnsiCompareIC("NONE"))
          {
            rbAutoGetIP->Checked = false;
            rbStaticIP->Checked = true;
            editIVRIP->Text = "";
            editMark->Text = "";
            editGWIP->Text = "";
            editIVRIP->Enabled = true;
            editMark->Enabled = true;
            editGWIP->Enabled = true;
          }
          else
          {
            rbAutoGetIP->Checked = true;
            rbStaticIP->Checked = false;
            editIVRIP->Enabled = false;
            editMark->Enabled = false;
            editGWIP->Enabled = false;
          }
          continue;
        }
        nPos = strTemp.AnsiPos("HWADDR=");
        if (nPos > 0)
        {
          strServerIp[3] = strLine.SubString(nPos+7,50);
          //editMAC->Text = strServerIp[3];
          continue;
        }
        nPos = strTemp.AnsiPos("IPADDR=");
        if (nPos > 0)
        {
          strServerIp[4] = strLine.SubString(nPos+7,50);
          editIVRIP->Text = strServerIp[4];
          continue;
        }
        nPos = strTemp.AnsiPos("NETMASK=");
        if (nPos > 0)
        {
          strServerIp[5] = strLine.SubString(nPos+8,50);
          editMark->Text = strServerIp[5];
          continue;
        }
        nPos = strTemp.AnsiPos("GATEWAY=");
        if (nPos > 0)
        {
          strServerIp[6] = strLine.SubString(nPos+8,50);
          editGWIP->Text = strServerIp[6];
          continue;
        }
      }
    }
    fclose(fp1);
  }
  bIPParamChange = false;
}
void TFormMain::GetServerGw()
{
  FILE *fp1;
  char szLine[256];
  AnsiString strTemp, strLine="";
  int i, nPos;

  fp1 = fopen(IPINIFile.c_str(), "r");
  if (fp1 != NULL)
  {
    while (!feof(fp1))
    {
      strLine="";
      if (fgets(szLine, 256, fp1) > 0)
      {
        strTemp = (char *)szLine;
        for (i=1; i<=strTemp.Length(); i++)
        {
          if (strTemp[i] == ' ' || strTemp[i] == '\n')
            continue;
          strLine += strTemp[i];
        }
        strTemp = (strLine.Trim()).UpperCase();
        nPos = strTemp.AnsiPos("NETWORKING=");
        if (nPos > 0)
        {
          strServerGw[0] = strLine.SubString(nPos+11,50);
          continue;
        }
        nPos = strTemp.AnsiPos("HOSTNAME=");
        if (nPos > 0)
        {
          strServerGw[1] = strLine.SubString(nPos+9,50);
          continue;
        }
        nPos = strTemp.AnsiPos("GATEWAY=");
        if (nPos > 0)
        {
          strServerGw[2] = strLine.SubString(nPos+8,50);
          continue;
        }
      }
    }
    fclose(fp1);
  }
  bIPParamChange = false;
}
void TFormMain::GetServerDns()
{
  FILE *fp1;
  char szLine[256];
  AnsiString strTemp, strTemp1, strLine="";
  int i, l=0, nPos;

  strServerDns[0] = "";
  strServerDns[1] = "";

  fp1 = fopen(DNSINIFile.c_str(), "r");
  if (fp1 != NULL)
  {
    while (!feof(fp1))
    {
      strLine="";
      if (fgets(szLine, 256, fp1) > 0)
      {
        strTemp = (char *)szLine;
        for (i=1; i<=strTemp.Length(); i++)
        {
          if (strTemp[i] == ' ' || strTemp[i] == '\n')
            continue;
          strLine += strTemp[i];
        }
        strTemp = (strLine.Trim()).LowerCase();
        nPos = strTemp.AnsiPos("nameserver");
        if (nPos > 0)
        {
          strTemp1 = strLine.SubString(nPos+10,50);
          if (strTemp1.Length() > 0)
          {
            if (l == 0)
            {
              strServerDns[0] = strTemp1;
              editDNS1->Text = strServerDns[0];
              l++;
            }
            else if (l == 1)
            {
              strServerDns[1] = strTemp1;
              editDNS2->Text = strServerDns[1];
              l++;
            }
          }
        }
        nPos = strTemp.AnsiPos("domain");
        if (nPos > 0)
        {
          strServerDns[2] = strLine.SubString(nPos+6,50);;
        }
        nPos = strTemp.AnsiPos("search");
        if (nPos > 0)
        {
          strServerDns[3] = strLine.SubString(nPos+6,50);;
        }
      }
    }
    fclose(fp1);
  }
  if (strServerDns[0] == "" && strServerDns[1] == "")
  {
    rbAutoGetDNS->Checked = true;
    rbStaticDNS->Checked = false;
    editDNS1->Text = "";
    editDNS2->Text = "";
    editDNS1->Enabled = false;
    editDNS2->Enabled = false;
  }
  else
  {
    rbAutoGetDNS->Checked = false;
    rbStaticDNS->Checked = true;
    editDNS1->Enabled = true;
    editDNS2->Enabled = true;
  }
  bIPParamChange = false;
}
void TFormMain::WriteAlartMsg(LPCTSTR lpszFormat, ...)
{
  char szAlartMsg[1024], buf[512], filename[128];
  va_list	ArgList;
	va_start(ArgList, lpszFormat);
	_vsnprintf(buf, 512, lpszFormat, ArgList);
	va_end (ArgList);

  if (gLangID == 1)
    sprintf(szAlartMsg, "%s 告警提醒：%s",
      Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str(), buf);
  else
    sprintf(szAlartMsg, "%s 牡矗眶: %s",
      Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str(), buf);
  memoAlart->Lines->Add((char *)szAlartMsg);
  Word Year, Month, Day;
  DecodeDate(Now(), Year, Month, Day );
  sprintf(filename, "%s\\ALART\\ALT_%04d%02d%02d.log", g_strExecPath.c_str(), Year, Month, Day);
  SaveMsg(filename, szAlartMsg);
}
bool ConnectFTP(AnsiString FTPServerIP, int FTPPort, AnsiString UserName, AnsiString UserPSW)
{
  char lpFTPSite[32];
  char lpUsername[32];
  char lpPassword[32];

  wsprintf((LPSTR) lpFTPSite, "%s", FTPServerIP);
  wsprintf((LPSTR) lpUsername, "%s", UserName);
  wsprintf((LPSTR) lpPassword, "%s", UserPSW);

  hInternet = InternetOpen("Application", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, INTERNET_FLAG_NO_CACHE_WRITE) ;

  if (hInternet != NULL)
  {
    hConnect = InternetConnect(hInternet,
                            lpFTPSite,
                            FTPPort,
                            lpUsername,
                            lpPassword,
                            INTERNET_SERVICE_FTP,
                            INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_PASSIVE,
                            0) ;
    if (hConnect != NULL)
    {
      return true;
    }
  }
  return false;
}
BOOL DownLoadFile(AnsiString RemoteFile, AnsiString LocalFile)
{
  if (hConnect == NULL)
    return FALSE;
  return FtpGetFile(hConnect,
             RemoteFile.c_str() ,
             LocalFile.c_str(),
             false,
             FILE_ATTRIBUTE_ARCHIVE,
             FTP_TRANSFER_TYPE_BINARY,
             0) ;
}
BOOL PutFile(AnsiString LocalFile, AnsiString RemoteFile)
{
  if (hConnect == NULL)
    return FALSE;
  return FtpPutFile(hConnect,
             LocalFile.c_str() ,
             RemoteFile.c_str(),
             FTP_TRANSFER_TYPE_BINARY,
             0) ;
}
void CloseFTP()
{
  if (hConnect != NULL)
    InternetCloseHandle(hConnect) ;
  if (hInternet != NULL)
    InternetCloseHandle(hInternet) ;
  hConnect = NULL;
  hInternet = NULL;
}

bool MyDownLoadFile(AnsiString ServerIp, int FTPPort, AnsiString UserName, AnsiString Password, AnsiString LocalFile, AnsiString RemoteFile)
{
  bool bResult=false;
  if (ConnectFTP(ServerIp, FTPPort, UserName, Password))
  {
    if (DownLoadFile(LocalFile, RemoteFile) == TRUE);
      bResult = true;
    CloseFTP();
  }
  return bResult;
}

bool MyUpLoadFile(AnsiString ServerIp, int FTPPort, AnsiString UserName, AnsiString Password, AnsiString LocalFile, AnsiString RemoteFile)
{
  bool bResult=false;
  if (ConnectFTP(ServerIp, FTPPort, UserName, Password))
  {
    if (PutFile(LocalFile, RemoteFile) == TRUE);
      bResult = true;
    CloseFTP();
  }
  return bResult;
}

//-----------------------------------------------------------------------------
void WriteIniParam(int nIniType, TIniFile *pIniKey, const char *pszSectName, const char *pszKeyName, const char *pszValue)
{
  if (pIniKey == NULL) return;
  pIniKey->WriteString((char *)pszSectName, (char *)pszKeyName, (char *)pszValue);

  if (g_bRunOnServicePCId == true)
    return;

  if (nIniType == 1)
    SendmodifyiniparamMSG(nIniType, "IVRSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 2)
    SendmodifyiniparamMSG(nIniType, "FLWSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 3)
    SendmodifyiniparamMSG(nIniType, "DBSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 4)
    SendmodifyiniparamMSG(nIniType, "CTILINKSERVICE.ini", pszSectName, pszKeyName, pszValue);
}
void WriteIniParam(int nIniType, TIniFile *pIniKey, AnsiString strSectName, AnsiString strKeyName, AnsiString strValue)
{
  if (pIniKey == NULL) return;
  pIniKey->WriteString(strSectName, strKeyName, strValue);

  if (g_bRunOnServicePCId == true)
    return;

  if (nIniType == 1)
    SendmodifyiniparamMSG(nIniType, "IVRSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), strValue.c_str());
  else if (nIniType == 2)
    SendmodifyiniparamMSG(nIniType, "FLWSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), strValue.c_str());
  else if (nIniType == 3)
    SendmodifyiniparamMSG(nIniType, "DBSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), strValue.c_str());
  else if (nIniType == 4)
    SendmodifyiniparamMSG(nIniType, "CTILINKSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), strValue.c_str());
}
void WriteIniParam(int nIniType, TIniFile *pIniKey, const char *pszSectName, const char *pszKeyName, int nValue)
{
  if (pIniKey == NULL) return;
  pIniKey->WriteInteger((char *)pszSectName, (char *)pszKeyName, nValue);

  if (g_bRunOnServicePCId == true)
    return;

  char pszValue[32];
  sprintf(pszValue, "%d", nValue);
  if (nIniType == 1)
    SendmodifyiniparamMSG(nIniType, "IVRSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 2)
    SendmodifyiniparamMSG(nIniType, "FLWSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 3)
    SendmodifyiniparamMSG(nIniType, "DBSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 4)
    SendmodifyiniparamMSG(nIniType, "CTILINKSERVICE.ini", pszSectName, pszKeyName, pszValue);
}
void WriteIniParam(int nIniType, TIniFile *pIniKey, AnsiString strSectName, AnsiString strKeyName, int nValue)
{
  if (pIniKey == NULL) return;
  pIniKey->WriteInteger(strSectName, strKeyName, nValue);

  if (g_bRunOnServicePCId == true)
    return;

  char pszValue[32];
  sprintf(pszValue, "%d", nValue);
  if (nIniType == 1)
    SendmodifyiniparamMSG(nIniType, "IVRSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
  else if (nIniType == 2)
    SendmodifyiniparamMSG(nIniType, "FLWSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
  else if (nIniType == 3)
    SendmodifyiniparamMSG(nIniType, "DBSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
  else if (nIniType == 4)
    SendmodifyiniparamMSG(nIniType, "CTILINKSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
}
void WriteIniParam(int nIniType, TIniFile *pIniKey, const char *pszSectName, const char *pszKeyName, bool bValue)
{
  if (pIniKey == NULL) return;
  pIniKey->WriteBool((char *)pszSectName, (char *)pszKeyName, bValue);

  if (g_bRunOnServicePCId == true)
    return;

  char pszValue[32];
  sprintf(pszValue, "%d", bValue);
  if (nIniType == 1)
    SendmodifyiniparamMSG(nIniType, "IVRSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 2)
    SendmodifyiniparamMSG(nIniType, "FLWSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 3)
    SendmodifyiniparamMSG(nIniType, "DBSERVICE.ini", pszSectName, pszKeyName, pszValue);
  else if (nIniType == 4)
    SendmodifyiniparamMSG(nIniType, "CTILINKSERVICE.ini", pszSectName, pszKeyName, pszValue);
}
void WriteIniParam(int nIniType, TIniFile *pIniKey, AnsiString strSectName, AnsiString strKeyName, bool bValue)
{
  if (pIniKey == NULL) return;
  pIniKey->WriteBool(strSectName, strKeyName, bValue);

  if (g_bRunOnServicePCId == true)
    return;

  char pszValue[32];
  sprintf(pszValue, "%d", bValue);
  if (nIniType == 1)
    SendmodifyiniparamMSG(nIniType, "IVRSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
  else if (nIniType == 2)
    SendmodifyiniparamMSG(nIniType, "FLWSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
  else if (nIniType == 3)
    SendmodifyiniparamMSG(nIniType, "DBSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
  else if (nIniType == 4)
    SendmodifyiniparamMSG(nIniType, "CTILINKSERVICE.ini", strSectName.c_str(), strKeyName.c_str(), pszValue);
}
//-----------------------------------------------------------------------------
int MyIsNumber(AnsiString str)
{
  try
  {
    str.ToInt();
    return 0;
  }
  catch (...)
  {
    return 1;
  }
}
int MyIsDigits(AnsiString str)
{
  for (int i=1; i<=str.Length(); i++)
  {
    if (!isdigit(str[i]))
      return 1;
  }
  return 0;
}
//加密密码
char *EncodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  rtn[j] = 'p';
  for(unsigned int i = 0; i < strlen(Pass); i++)
  {
    char Inc = (char)((::GetTickCount()+i*11) % 19);
    j++;
    rtn[j] = char(Inc + 'a');
    j++;
    rtn[j] = char(Pass[i] + Inc - 15);
  }
  j++;
  rtn[j] = 's';
  Base64Encode(key, rtn);
  return key;
}
AnsiString EncodePass(AnsiString Pass)
{
  return (char *)EncodePass(Pass.c_str());
}
//解密密码
char *DecodePass(const char *Pass)
{
  static char rtn[256], key[256];
  int j=0;
  if (strlen(Pass) == 0)
  {
    return "";
  }
  memset(rtn, 0, 256);
  Base64Decode(key, Pass);
  for (unsigned int i=1; i<strlen(key)-2; i++)
  {
    char Inc = key[i] - 'a';
    i++;
    rtn[j++] = char(key[i] - Inc + 15);
  }
  return rtn;
}
AnsiString DecodePass(AnsiString Pass)
{
  return (char *)DecodePass(Pass.c_str());
}
int MySplit(const char *pszSource, TStringList *pstrList)
{
  char fieldvalue[128];
  unsigned short n,j=0,k=0, len;
  char ch;

  pstrList->Clear();
  len = strlen(pszSource);
  memset(fieldvalue, 0, 128);
  for (n = 0; n < len+1 && n < 512; n ++)
  {
    ch = pszSource[n];
    if (ch == ',' || ch == '\0')
    {
      if (strlen(fieldvalue) == 0)
        pstrList->Add("");
      else
        pstrList->Add((char *)fieldvalue);
      j ++;
      k = 0;
      memset(fieldvalue, 0, 128);
      if (j >= 128)
        break;
    }
    else
    {
      if (k < 128)
      {
        fieldvalue[k] = ch;
        k ++;
      }
    }
  }
  return j;
}
int MySplit(const char *pszSource, char cSplitChar, TStringList *pstrList)
{
  char fieldvalue[128];
  unsigned short n,j=0,k=0, len;
  char ch;

  pstrList->Clear();
  len = strlen(pszSource);
  memset(fieldvalue, 0, 128);
  for (n = 0; n < len+1 && n < 512; n ++)
  {
    ch = pszSource[n];
    if (ch == cSplitChar || ch == '\0')
    {
      pstrList->Add((char *)fieldvalue);
      j ++;
      k = 0;
      memset(fieldvalue, 0, 128);
      if (j >= 128)
        break;
    }
    else
    {
      if (k < 128)
      {
        fieldvalue[k] = ch;
        k ++;
      }
    }
  }
  return j;
}
//---------------------------------------------------------------------------
//增加XML消息头名称
void Add_XML_Msg_Header(int nMsgId, int nLOG)
{
    sprintf(g_szSendMsgBuf, "<%s logid='%d'", g_pFlwRule->LOGIVRMsgRule[nMsgId].MsgNameEng, nLOG);
}
//增加XML消息属性内容
void Add_XML_Msg_Attr(int nMsgId, int nAttrId, const char *pszAttrValue)
{
    char szMsgTemp[MAX_MSG_BUF_LEN];

    sprintf(szMsgTemp, " %s='%s'", g_pFlwRule->LOGIVRMsgRule[nMsgId].Attribute[nAttrId].AttrNameEng, pszAttrValue);
    strcat(g_szSendMsgBuf, szMsgTemp);
}
void Add_XML_Msg_Attr(int nMsgId, int nAttrId, int nAttrValue)
{
    char szMsgTemp[MAX_MSG_BUF_LEN];

    sprintf(szMsgTemp, " %s='%d'", g_pFlwRule->LOGIVRMsgRule[nMsgId].Attribute[nAttrId].AttrNameEng, nAttrValue);
    strcat(g_szSendMsgBuf, szMsgTemp);
}
//增加XML消息尾
void Add_XML_Msg_Trail()
{
    strcat(g_szSendMsgBuf, "/>");
}
void SendIVRMsg(int nMsgId, const char* pszMsg)
{
  if (g_nConnectIVRId >= 3)
    g_pTcpClient->IVRSendMessage(nMsgId, pszMsg);
}
void SendFLWMsg(int nMsgId, const char* pszMsg)
{
  if (g_nConnectFLWId >= 3)
    g_pTcpClient->FLWSendMessage(nMsgId, pszMsg);
}
void SendDBMsg(int nMsgId, const char* pszMsg)
{
  if (g_nConnectDBId >= 3)
    g_pTcpClient->DBSendMessage(nMsgId, pszMsg);
}
void SendCTRLMsg(int nMsgId, const char* pszMsg)
{
  if (g_nConnectCTRLId >= 3)
    g_pTcpClient->CTRLSendMessage(nMsgId, pszMsg);
}
void SendmodifyiniparamMSG(int nSrvType, const char *pszIniFileName, const char *pszSectName, const char *pszKeyName, const char *pszValue)
{
  if (nSrvType == 4)
    Add_XML_Msg_Header(LOGMSG_modifyctilinkdata, g_nLOGClientId);
  else
    Add_XML_Msg_Header(LOGMSG_modifyiniparam, g_nLOGClientId);

  Add_XML_Msg_Attr(LOGMSG_modifyiniparam, 1, pszIniFileName);
  Add_XML_Msg_Attr(LOGMSG_modifyiniparam, 2, pszSectName);
  Add_XML_Msg_Attr(LOGMSG_modifyiniparam, 3, pszKeyName);
  Add_XML_Msg_Attr(LOGMSG_modifyiniparam, 4, pszValue);
  Add_XML_Msg_Trail();
  if (nSrvType == 1)
    SendIVRMsg(LOGMSG_modifyiniparam, g_szSendMsgBuf);
  else if (nSrvType == 2)
    SendFLWMsg(LOGMSG_modifyiniparam, g_szSendMsgBuf);
  else if (nSrvType == 3)
    SendDBMsg(LOGMSG_modifyiniparam, g_szSendMsgBuf);
  else if (nSrvType == 4)
    SendIVRMsg(LOGMSG_modifyctilinkdata, g_szSendMsgBuf);
}
void SendLoginIVRMsg(AnsiString strUserName, AnsiString strPassword)
{
  Add_XML_Msg_Header(LOGMSG_login, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_login, 1, strUserName.c_str());
  Add_XML_Msg_Attr(LOGMSG_login, 2, EncodePass(strPassword.c_str()));
  Add_XML_Msg_Attr(LOGMSG_login, 3, (g_bRunOnServicePCId==true) ? 0 : 1);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_login, g_szSendMsgBuf);
}
void SendLogoutIVRMsg()
{
  Add_XML_Msg_Header(LOGMSG_logout, g_nLOGClientId);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_logout, g_szSendMsgBuf);
}
void SendModifyPswMsg(AnsiString strUserName, AnsiString strOldPassword, AnsiString strNewPassword)
{
  if (g_bRunOnServicePCId == true)
  {
    return;
  }

  Add_XML_Msg_Header(LOGMSG_modifypassword, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_modifypassword, 1, strUserName.c_str());
  Add_XML_Msg_Attr(LOGMSG_modifypassword, 2, EncodePass(strOldPassword.c_str()));
  Add_XML_Msg_Attr(LOGMSG_modifypassword, 3, EncodePass(strNewPassword.c_str()));
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_modifypassword, g_szSendMsgBuf);
}
void SendLoginFLWMsg(AnsiString strUserName, AnsiString strPassword)
{
  Add_XML_Msg_Header(LOGMSG_login, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_login, 1, strUserName.c_str());
  Add_XML_Msg_Attr(LOGMSG_login, 2, EncodePass(strPassword.c_str()));
  Add_XML_Msg_Attr(LOGMSG_login, 3, (g_bRunOnServicePCId==true) ? 0 : 1);
  Add_XML_Msg_Trail();
  SendFLWMsg(LOGMSG_login, g_szSendMsgBuf);
}
void SendLoginDBMsg(AnsiString strUserName, AnsiString strPassword)
{
  Add_XML_Msg_Header(LOGMSG_login, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_login, 1, strUserName.c_str());
  Add_XML_Msg_Attr(LOGMSG_login, 2, EncodePass(strPassword.c_str()));
  Add_XML_Msg_Attr(LOGMSG_login, 3, (g_bRunOnServicePCId==true) ? 0 : 1);
  Add_XML_Msg_Trail();
  SendDBMsg(LOGMSG_login, g_szSendMsgBuf);
}
void SendLoginCTRLMsg(AnsiString strUserName, AnsiString strPassword)
{
  Add_XML_Msg_Header(LOGMSG_login, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_login, 1, strUserName.c_str());
  Add_XML_Msg_Attr(LOGMSG_login, 2, EncodePass(strPassword.c_str()));
  Add_XML_Msg_Attr(LOGMSG_login, 3, (g_bRunOnServicePCId==true) ? 0 : 1);
  Add_XML_Msg_Trail();
  SendCTRLMsg(LOGMSG_login, g_szSendMsgBuf);
}
void SendGetStatusMsg(int nSrvType, int nMsgId, int nOpenFlag)
{
  Add_XML_Msg_Header(nMsgId, g_nLOGClientId);
  Add_XML_Msg_Attr(nMsgId, 1, nOpenFlag);
  Add_XML_Msg_Trail();
  if (nSrvType == 1)
    SendIVRMsg(nMsgId, g_szSendMsgBuf);
  else
    SendFLWMsg(nMsgId, g_szSendMsgBuf);
}
void SendreleasechnMsg(int nChn)
{
  Add_XML_Msg_Header(LOGMSG_releasechn, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_releasechn, 1, nChn);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_releasechn, g_szSendMsgBuf);
}
void SendforceclearMsg(const char *pszSeatNo)
{
  Add_XML_Msg_Header(LOGMSG_forceclear, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_forceclear, 1, pszSeatNo);
  Add_XML_Msg_Attr(LOGMSG_forceclear, 2, 0);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_forceclear, g_szSendMsgBuf);
}
void SendforcelogoutMsg(const char *pszSeatNo)
{
  Add_XML_Msg_Header(LOGMSG_forcelogout, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_forcelogout, 1, pszSeatNo);
  Add_XML_Msg_Attr(LOGMSG_forcelogout, 2, 0);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_forcelogout, g_szSendMsgBuf);
}
void SendforcedisturbMsg(const char *pszSeatNo)
{
  Add_XML_Msg_Header(LOGMSG_forcedisturb, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_forcedisturb, 1, pszSeatNo);
  Add_XML_Msg_Attr(LOGMSG_forcedisturb, 2, 0);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_forcedisturb, g_szSendMsgBuf);
}
void SendforcereadyMsg(const char *pszSeatNo)
{
  Add_XML_Msg_Header(LOGMSG_forceready, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_forceready, 1, pszSeatNo);
  Add_XML_Msg_Attr(LOGMSG_forceready, 2, 0);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_forceready, g_szSendMsgBuf);
}
void SendmodifyroutedataMsg(int nRouteNo, const char *pszChnList, int nSelMode, const char *pszIPPreCode)
{
  Add_XML_Msg_Header(LOGMSG_modifyroutedata, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_modifyroutedata, 1, nRouteNo);
  Add_XML_Msg_Attr(LOGMSG_modifyroutedata, 2, pszChnList);
  Add_XML_Msg_Attr(LOGMSG_modifyroutedata, 3, nSelMode);
  Add_XML_Msg_Attr(LOGMSG_modifyroutedata, 4, pszIPPreCode);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_modifyroutedata, g_szSendMsgBuf);
}
void SendmodifyseatnoMsg(int nAG, const char *pszNewSeatNo)
{
  Add_XML_Msg_Header(LOGMSG_modifyseatno, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_modifyseatno, 1, nAG);
  Add_XML_Msg_Attr(LOGMSG_modifyseatno, 2, pszNewSeatNo);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_modifyseatno, g_szSendMsgBuf);
}
void SendmodifyseatdataMsg(int nPortType, int nPortNo, const char *pszSeatData)
{
  Add_XML_Msg_Header(LOGMSG_modifyseatdata, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_modifyseatdata, 1, nPortType);
  Add_XML_Msg_Attr(LOGMSG_modifyseatdata, 2, nPortNo);
  Add_XML_Msg_Attr(LOGMSG_modifyseatdata, 3, pszSeatData);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_modifyseatdata, g_szSendMsgBuf);
}
void SendmodifyextlineMsg(int nLineNo, const char *pszPSTNCode, const char *pszPreCode, const char *pszIPPreCode, const char *pszNotAddPreCode)
{
  Add_XML_Msg_Header(LOGMSG_modifyextline, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_modifyextline, 1, nLineNo);
  Add_XML_Msg_Attr(LOGMSG_modifyextline, 2, pszPSTNCode);
  Add_XML_Msg_Attr(LOGMSG_modifyextline, 3, pszPreCode);
  Add_XML_Msg_Attr(LOGMSG_modifyextline, 4, pszIPPreCode);
  Add_XML_Msg_Attr(LOGMSG_modifyextline, 5, pszNotAddPreCode);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_modifyextline, g_szSendMsgBuf);
}
void SendsetivrsavemsgidMsg(bool bAlartId, bool bCommId, bool bDebugId, bool bDrvId)
{
  Add_XML_Msg_Header(LOGMSG_setivrsavemsgid, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_setivrsavemsgid, 1, bAlartId);
  Add_XML_Msg_Attr(LOGMSG_setivrsavemsgid, 2, bCommId);
  Add_XML_Msg_Attr(LOGMSG_setivrsavemsgid, 3, bDebugId);
  Add_XML_Msg_Attr(LOGMSG_setivrsavemsgid, 4, bDrvId);
  Add_XML_Msg_Trail();
  SendIVRMsg(LOGMSG_setivrsavemsgid, g_szSendMsgBuf);
}
void SendsetflwsavemsgidMsg(bool bAlartId, bool bCommId, bool bCmdsId, bool bVarsId)
{
  Add_XML_Msg_Header(LOGMSG_setflwsavemsgid, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_setflwsavemsgid, 1, bAlartId);
  Add_XML_Msg_Attr(LOGMSG_setflwsavemsgid, 2, bCommId);
  Add_XML_Msg_Attr(LOGMSG_setflwsavemsgid, 3, bCmdsId);
  Add_XML_Msg_Attr(LOGMSG_setflwsavemsgid, 4, bVarsId);
  Add_XML_Msg_Trail();
  SendFLWMsg(LOGMSG_setflwsavemsgid, g_szSendMsgBuf);
}
void SendReLoadFlwMsg(const char *pszFlwFileName, int nGroupNo, int nFuncNo)
{
  Add_XML_Msg_Header(LOGMSG_loadflw, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_loadflw, 1, pszFlwFileName);
  Add_XML_Msg_Attr(LOGMSG_loadflw, 2, nGroupNo);
  Add_XML_Msg_Attr(LOGMSG_loadflw, 3, nFuncNo);
  Add_XML_Msg_Trail();
  SendFLWMsg(LOGMSG_loadflw, g_szSendMsgBuf);
}
void SendAddAccessCodeMsg(int nFlwNo, const char *pszCode, int nMinLen, int nMaxLen)
{
  Add_XML_Msg_Header(LOGMSG_addaccesscode, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_addaccesscode, 1, nFlwNo);
  Add_XML_Msg_Attr(LOGMSG_addaccesscode, 2, pszCode);
  Add_XML_Msg_Attr(LOGMSG_addaccesscode, 3, nMinLen);
  Add_XML_Msg_Attr(LOGMSG_addaccesscode, 4, nMaxLen);
  Add_XML_Msg_Trail();
  SendFLWMsg(LOGMSG_addaccesscode, g_szSendMsgBuf);
}
void SendDelAccessCodeMsg(int nFlwnNo, const char *pszCode)
{
  Add_XML_Msg_Header(LOGMSG_delaccesscode, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_delaccesscode, 1, nFlwnNo);
  Add_XML_Msg_Attr(LOGMSG_delaccesscode, 2, pszCode);
  Add_XML_Msg_Trail();
  SendFLWMsg(LOGMSG_delaccesscode, g_szSendMsgBuf);
}
void SendAutoFlwOnOffMsg(int nFlwNo, int nOnOff)
{
  Add_XML_Msg_Header(LOGMSG_autoflwonoff, g_nLOGClientId);
  Add_XML_Msg_Attr(LOGMSG_autoflwonoff, 1, nFlwNo);
  Add_XML_Msg_Attr(LOGMSG_autoflwonoff, 2, nOnOff);
  Add_XML_Msg_Trail();
  SendFLWMsg(LOGMSG_autoflwonoff, g_szSendMsgBuf);
}
//-----------------------------------------------------------------------------
void OnIVRReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //need define in main.cpp
{
  ((unsigned char *)buf)[len] = 0;
  g_pMsgfifo->Write(remoteid, msgtype, (char *)buf);
}
void OnIVRLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect IVR service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectIVRId = 3;
  FormMain->imgIVRServiceStatus->Picture->LoadFromFile(imgStartFile);
  FormMain->ProcIVRMsg(0, szTemp);

  if (g_nLoginId == 1 || g_bAutoLoginId == true)
    SendLoginIVRMsg(g_strLoginUsername, g_strPassword);

  FormMain->imgConnectIVRStatus->Picture->LoadFromFile(imgYellowFile);
  if (gLangID == 1)
    FormMain->imgConnectIVRStatus->Hint = "已连接到IVR服务";
  else
    FormMain->imgConnectIVRStatus->Hint = "硈钡IVR狝叭";
  if (g_nRunMode > 0)
  {
    FormMain->btStartIVRService->Enabled = false;
    FormMain->btStopIVRService->Enabled = true;
  }
  if (g_bRunOnServicePCId == true)
    FormMain->ReadAuthData();
}
void OnIVRClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect IVR service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectIVRId = 1;
  FormMain->ProcIVRMsg(0, szTemp);
  if (g_pChnStatus != NULL)
  {
    delete []g_pChnStatus;
    g_pChnStatus = NULL;
  }
  if (g_pVopStatus != NULL)
  {
    delete []g_pVopStatus;
    g_pVopStatus = NULL;
  }
  if (g_pAgStatus != NULL)
  {
    delete []g_pAgStatus;
    g_pAgStatus = NULL;
  }
  FormMain->lvChnStatus->Clear();
  FormMain->lvVOPState->Clear();
  FormMain->lvAgStatus->Clear();
  FormMain->imgConnectIVRStatus->Picture->LoadFromFile(imgRedFile);
  if (gLangID == 1)
    FormMain->imgConnectIVRStatus->Hint = "已与IVR服务断开";
  else
    FormMain->imgConnectIVRStatus->Hint = "籔IVR狝叭耞秨";
  if (g_nRunMode > 0)
  {
    FormMain->imgIVRServiceStatus->Picture->LoadFromFile(imgStopFile);
    FormMain->btStartIVRService->Enabled = true;
    FormMain->btStopIVRService->Enabled = true;
  }
}
void OnFLWReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //need define in main.cpp
{
  ((unsigned char *)buf)[len] = 0;
  g_pMsgfifo->Write(remoteid, msgtype, (char *)buf);
}
void OnFLWLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect FLW service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectFLWId = 3;
  FormMain->imgFLWServiceStatus->Picture->LoadFromFile(imgStartFile);
  FormMain->ProcFLWMsg(0, szTemp);

  if (g_nLoginId == 1 || g_bAutoLoginId == true)
    SendLoginFLWMsg(g_strLoginUsername, g_strPassword);

  FormMain->imgConnectFLWStatus->Picture->LoadFromFile(imgYellowFile);
  if (gLangID == 1)
    FormMain->imgConnectFLWStatus->Hint = "已连接到流程服务";
  else
    FormMain->imgConnectFLWStatus->Hint = "硈钡瑈祘狝叭";
  if (g_nRunMode > 0)
  {
    FormMain->btStartFLWService->Enabled = false;
    FormMain->btStopFLWService->Enabled = true;
  }
}
void OnFLWClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect FLW service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectFLWId = 1;
  FormMain->ProcFLWMsg(0, szTemp);
  FormMain->imgConnectFLWStatus->Picture->LoadFromFile(imgRedFile);
  if (gLangID == 1)
    FormMain->imgConnectFLWStatus->Hint = "已与流程服务断开";
  else
    FormMain->imgConnectFLWStatus->Hint = "籔瑈祘狝叭耞秨";
  if (g_nRunMode > 0)
  {
    FormMain->imgFLWServiceStatus->Picture->LoadFromFile(imgStopFile);
    FormMain->btStartFLWService->Enabled = true;
    FormMain->btStopFLWService->Enabled = true;
  }
}
void OnDBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //need define in main.cpp
{
  ((unsigned char *)buf)[len] = 0;
  g_pMsgfifo->Write(remoteid, msgtype, (char *)buf);
}
void OnDBLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect DB service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectDBId = 3;
  if (g_nLoginId == 1 || g_bAutoLoginId == true)
    SendLoginDBMsg(g_strLoginUsername, g_strPassword);
  FormMain->imgDBServiceStatus->Picture->LoadFromFile(imgStartFile);
  FormMain->imgConnectDBStatus->Picture->LoadFromFile(imgYellowFile);
  if (gLangID == 1)
    FormMain->imgConnectDBStatus->Hint = "已连接到数据库网关服务";
  else
    FormMain->imgConnectDBStatus->Hint = "硈钡戈畐筯笵竟狝叭";
  FormMain->ProcFLWMsg(0, szTemp);
  if (g_nRunMode > 0)
  {
    FormMain->btStartDBService->Enabled = false;
    FormMain->btStopDBService->Enabled = true;
  }
}
void OnDBClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect DB service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectDBId = 1;
  FormMain->ProcFLWMsg(0, szTemp);
  FormMain->imgConnectDBStatus->Picture->LoadFromFile(imgRedFile);
  if (gLangID == 1)
    FormMain->imgConnectDBStatus->Hint = "已与数据库网关服务断开";
  else
    FormMain->imgConnectDBStatus->Hint = "籔戈畐筯笵竟狝叭耞秨";
  if (g_nRunMode > 0)
  {
    FormMain->imgDBServiceStatus->Picture->LoadFromFile(imgStopFile);
    FormMain->btStartDBService->Enabled = true;
    FormMain->btStopDBService->Enabled = true;
  }
}
void OnCTRLReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //need define in main.cpp
{
  ((unsigned char *)buf)[len] = 0;
  g_pMsgfifo->Write(remoteid, msgtype, (char *)buf);
}
void OnCTRLLogin(unsigned short serverid,unsigned short clientid)    //need define in main.cpp
{
  char szTemp[256];
  sprintf(szTemp, "%s >> ALRM: Connect CTRL service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
  g_nConnectCTRLId = 3;
  //SendLoginCTRLMsg();
  FormMain->imgConnectCTRLStatus->Picture->LoadFromFile(imgYellowFile);
  if (gLangID == 1)
    FormMain->imgConnectCTRLStatus->Hint = "已连接到平台服务器";
  else
    FormMain->imgConnectCTRLStatus->Hint = "硈钡キ籓狝竟";
  FormMain->ProcFLWMsg(0, szTemp);

  if (g_nRunMode == 2)
  {
    FormMain->btStartIVRService->Visible = true;
    FormMain->btStopIVRService->Visible = true;
    FormMain->btStartFLWService->Visible = true;
    FormMain->btStopFLWService->Visible = true;
    FormMain->btStartDBService->Visible = true;
    FormMain->btStopDBService->Visible = true;
  }
}
void OnCTRLClose(unsigned short serverid,unsigned short clientid)   //need define in main.cpp
{
  FormMain->imgConnectCTRLStatus->Picture->LoadFromFile(imgRedFile);
  if (gLangID == 1)
    FormMain->imgConnectCTRLStatus->Hint = "已与平台服务器断开";
  else
    FormMain->imgConnectCTRLStatus->Hint = "籔キ籓狝竟耞秨";
  if (g_nRunMode == 2)
  {
    FormMain->btStartIVRService->Visible = false;
    FormMain->btStopIVRService->Visible = false;
    FormMain->btStartFLWService->Visible = false;
    FormMain->btStopFLWService->Visible = false;
    FormMain->btStartDBService->Visible = false;
    FormMain->btStopDBService->Visible = false;
  }
}

//-----------------------------------------------------------------------------
void TFormMain::WriteSkins(int nSkinId)
{
  TIniFile *IniKey = new TIniFile ( ExtractFilePath(Application->ExeName)+"config.ini" ) ;
  IniKey->WriteInteger( "SKIN", "SkinId", nSkinId );
  delete IniKey;
}
void TFormMain::ProcLogMsg()
{
	unsigned short count = 0, ClientId, MsgId;
  char MsgBuf[2048];

  while(!g_pMsgfifo->IsEmpty() && count < 64) //一次最多处理64条信息
  {
    if (g_pMsgfifo->Read(ClientId, MsgId, MsgBuf) != 0)
    {
      MsgId = MsgId & 0xFF;
      if (g_bRunDebug == true)
      {
        if (MsgId != LOGMSG_ongetivrtracemsg && MsgId != LOGMSG_ongetflwtracemsg && MsgId != LOGMSG_onivralarmmsg
          && MsgId != LOGMSG_onflwalarmmsg && MsgId != LOGMSG_ondbalarmmsg)
          ProcIVRMsg(0, MsgBuf);
      }
        
      g_RecvMsg.SetMsgBufId(MsgId,(char *)MsgBuf);  //装载到Cstring中
      g_RecvMsg.SetClientId(ClientId);
      if (MsgId != LOGMSG_ongetivrtracemsg && MsgId != LOGMSG_ongetflwtracemsg)
      {
        if (0 != g_RecvMsg.ParseRevMsg(g_pFlwRule->IVRLOGMsgRule[MsgId]))
        {
          count ++;
          continue;
        }
      }
      switch (MsgId)
      {
      case LOGMSG_onlogin:	  //登录结果
        ProcOnLoginMsg(g_RecvMsg);
        break;
      case LOGMSG_onivronoff:	  //IVR监控信息开关状态
        ProcOnivronoffMsg(g_RecvMsg);
        btApply->Enabled = false;
        break;
      case LOGMSG_onflwonoff:	  //FLW监控信息开关状态
        ProcOnflwonoffMsg(g_RecvMsg);
        btApply->Enabled = false;
        break;
      case LOGMSG_onivralarmmsg:	  //取ivr告警跟踪信息结果
        g_nIVRAlarmId = atoi(g_RecvMsg.GetAttrValue(1).C_Str());
        ProcIVRMsg(0, g_RecvMsg.GetAttrValue(2).C_Str());
        break;
      case LOGMSG_onflwalarmmsg:	  //取flw告警跟踪信息结果
        g_nFLWAlarmId = atoi(g_RecvMsg.GetAttrValue(1).C_Str());
        ProcFLWMsg(0, g_RecvMsg.GetAttrValue(2).C_Str());
        break;
      case LOGMSG_ondbalarmmsg:	  //取db告警跟踪信息结果
        g_nDBAlarmId = atoi(g_RecvMsg.GetAttrValue(1).C_Str());
        ProcFLWMsg(0, g_RecvMsg.GetAttrValue(2).C_Str());
        break;

      case LOGMSG_ongetqueueinfo:	  //取ACD分配队列信息结果
        DispAcdStatus(g_RecvMsg);
        break;
      case LOGMSG_ongetagentstatus:	  //取所有坐席的状态信息结果
        DispAgStatus(g_RecvMsg);
        break;
      case LOGMSG_ongetchnstatus:	  //取所有通道的状态信息结果
        DispChnStatus(g_RecvMsg);
        break;
      case LOGMSG_ongetivrtracemsg:	  //取ivr日志跟踪信息结果
        ProcIVRMsg(1, MsgBuf);
        break;
      case LOGMSG_ongetloadedflws:	  //取已加载的流程信息结果
        DispLoadFlw(g_RecvMsg);
        break;
      case LOGMSG_ongetflwtracemsg:	//取flw日志跟踪信息结果
        ProcFLWMsg(1, MsgBuf);
        break;

      case LOGMSG_onsetivrsavemsgid:	//当前设置的IVR服务自动保存日志标志
        ckIVRAlartId->Checked = atoi(g_RecvMsg.GetAttrValue(1).C_Str());
        ckIVRCommId->Checked = atoi(g_RecvMsg.GetAttrValue(2).C_Str());
        ckIVRDebugId->Checked = atoi(g_RecvMsg.GetAttrValue(3).C_Str());
        ckIVRDrvId->Checked = atoi(g_RecvMsg.GetAttrValue(4).C_Str());
        bSaveMsgIdParamChange = false;
        btApply->Enabled = false;
        break;
      case LOGMSG_onsetflwsavemsgid:	//当前设置的FLW服务自动保存日志标志
        ckFLWAlartId->Checked = atoi(g_RecvMsg.GetAttrValue(1).C_Str());
        ckFLWCommId->Checked = atoi(g_RecvMsg.GetAttrValue(2).C_Str());
        ckFLWCmdId->Checked = atoi(g_RecvMsg.GetAttrValue(3).C_Str());
        ckFLWVarId->Checked = atoi(g_RecvMsg.GetAttrValue(4).C_Str());
        bSaveMsgIdParamChange = false;
        btApply->Enabled = false;
        break;

      case LOGMSG_onrecviniparam:  //收到的ini参数
        ProcIniParam(g_RecvMsg);
        break;

      case LOGMSG_onrecvtxtline:	//收到的ini文件行
        ProconrecvtxtlineMsg(g_RecvMsg);
        break;

      case LOGMSG_ontrkcountdata:	//中继话务实时统计数据
        ProcontrkcountdataMsg(g_RecvMsg);
        break;

      case LOGMSG_onagcountdata:	//坐席话务实时统计数据
        ProconagcountdataMsg(g_RecvMsg);
        break;

      case LOGMSG_onmodifyseatdata:	//修改坐席参数结果
        ProconmodifyseatdataMsg(g_RecvMsg);
        break;
      case LOGMSG_onmodifypassword:
        Proconmodifypassword(g_RecvMsg);
        break;
      case LOGMSG_ongetvopstatus:
        DispVopStatus(g_RecvMsg);
        break;
      case LOGMSG_onseatstatusdata:
        DispSeatStatusData(g_RecvMsg);
        break;
      case LOGMSG_ongroupstatusdata:
        DispGroupStatusData(g_RecvMsg);
        break;
      case LOGMSG_onsessionscount:
        DispSessionsCount(g_RecvMsg);
        break;
      case LOGMSG_onharunstatus:
        DispHARunStatus(g_RecvMsg);
        break;
      /*
      case LOGMSG_onreleasechn:  //强行释放通道结果
        break;
      case LOGMSG_onforceclear:  //强拆其他坐席的电话结果
        break;
      case LOGMSG_onforcelogout:  //强制将其他话务员退出结果
        break;
      case LOGMSG_onforcedisturb:  //强制其他坐席免打扰结果
        break;
      case LOGMSG_onforceready:	//强制其他坐席空闲结果
        break;

      case LOGMSG_onmodifyroutedata:	//修改路由参数结果
        break;
      case LOGMSG_onmodifyseatno:	//修改坐席分机号结果
        break;
      case LOGMSG_onmodifyextline:	//修改模拟外线接入号码参数结果
        break;

      case LOGMSG_onloadflw:	//加载业务流程结果
        break;
      case LOGMSG_onaddaccesscode:	//添加接入号码结果
        break;
      case LOGMSG_ondelaccesscode:	//删除接入号码结果
        break;
      */
      }
    }
    count ++;
  }
}
void TFormMain::ProcIniParam(CXMLMsg &XMLMsg)
{
  if (g_bRunOnServicePCId == true)
    return;

  //int nServiceType = atoi(XMLMsg.GetAttrValue(1).C_Str());
  AnsiString strIniFile = (char *)(XMLMsg.GetAttrValue(2).C_Str());
  AnsiString strSectName = (char *)(XMLMsg.GetAttrValue(3).C_Str());
  AnsiString strKeyName = (char *)(XMLMsg.GetAttrValue(4).C_Str());
  AnsiString strValue = (char *)(XMLMsg.GetAttrValue(5).C_Str());

  TIniFile *IniKey = new TIniFile (strIniFile);
  IniKey->WriteString(strSectName, strKeyName, strValue);
  delete IniKey;
}
void TFormMain::ProconrecvtxtlineMsg(CXMLMsg &XMLMsg)
{
  if (g_bRunOnServicePCId == true)
  {
    ::Sleep(1);
    return;
  }

  FILE *pFile;
  int nIniType = atoi(XMLMsg.GetAttrValue(1).C_Str());
  int nLineId = atoi(XMLMsg.GetAttrValue(3).C_Str());

  switch (nLineId)
  {
  case 0:
    if (nIniType == 1)
      DeleteFile(IVRINIFile);
    else if (nIniType == 2)
      DeleteFile(FLWINIFile);
    else if (nIniType == 3)
      DeleteFile(DBINIFile);
    else if (nIniType == 4)
      DeleteFile(CTILINKINIFile);
    else if (nIniType == 5)
      DeleteFile(GWINIFile);
    else if (nIniType == 6)
      DeleteFile(DNSINIFile);
    else if (nIniType == 7)
      DeleteFile(IPINIFile);
    break;
  case 1:
    if (nIniType == 1)
    {
      pFile = fopen(IVRINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    else if (nIniType == 2)
    {
      pFile = fopen(FLWINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    else if (nIniType == 3)
    {
      pFile = fopen(DBINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    else if (nIniType == 4)
    {
      pFile = fopen(CTILINKINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    else if (nIniType == 5)
    {
      pFile = fopen(GWINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    else if (nIniType == 6)
    {
      pFile = fopen(DNSINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    else if (nIniType == 7)
    {
      pFile = fopen(IPINIFile.c_str(), "a+t");
      if (pFile != NULL)
      {
        fprintf(pFile, "%s", XMLMsg.GetAttrValue(2).C_Str());
        fclose(pFile);
      }
    }
    break;
  case 2:
    if (nIniType == 1)
      ReLoadIVRIniFile();
    else if (nIniType == 2)
      ReLoadFLWIniFile();
    else if (nIniType == 3)
      ReLoadDBIniFile();
    else if (nIniType == 4)
      ReLoadCTILINKIniFile();
    else if (nIniType == 5)
      ReLoadGW();
    else if (nIniType == 6)
      ReLoadDNS();
    else if (nIniType == 7)
      ReLoadIP();
    break;
  }
}
void TFormMain::InitTrkCountDispData(int nCountCycle)
{
  TListItem  *ListItem1;
  int i, j;

  lvTrkCountData->Columns->Clear();
  lvTrkCountData->Clear();

  lvTrkCountData->Columns->Add();
  if (gLangID == 1)
  {
  if (nCountCycle == 1)
    lvTrkCountData->Columns->Items[0]->Caption = "统计项目(每一小时统计)";
  else
    lvTrkCountData->Columns->Items[0]->Caption = "统计项目(每半小时统计)";
  }
  else
  {
  if (nCountCycle == 1)
    lvTrkCountData->Columns->Items[0]->Caption = "参璸兜ヘ(–参璸)";
  else
    lvTrkCountData->Columns->Items[0]->Caption = "参璸兜ヘ(–参璸)";
  }
  lvTrkCountData->Columns->Items[0]->Width = 160;
  for (i=0; i<g_nCallCountTotal; i++)
  {
    lvTrkCountData->Columns->Add();
    lvTrkCountData->Columns->Items[i+1]->Caption = i;
  }

  for (i=0; i<g_nTrkItemNum; i++)
  {
    ListItem1 = lvTrkCountData->Items->Add();
    ListItem1->Caption = TrkRemark[i];

    for (int j=0; j<g_nCallCountTotal; j++)
      ListItem1->SubItems->Insert(j, "0");
  }
}
void TFormMain::InitAgCountDispData(int nCountCycle)
{
  TListItem  *ListItem1;
  int i, j;

  lvAgCountData->Columns->Clear();
  lvAgCountData->Clear();

  lvAgCountData->Columns->Add();
  if (gLangID == 1)
  {
  if (nCountCycle == 1)
    lvAgCountData->Columns->Items[0]->Caption = "统计项目(每一小时统计)";
  else
    lvAgCountData->Columns->Items[0]->Caption = "统计项目(每半小时统计)";
  }
  else
  {
  if (nCountCycle == 1)
    lvAgCountData->Columns->Items[0]->Caption = "参璸兜ヘ(–参璸)";
  else
    lvAgCountData->Columns->Items[0]->Caption = "参璸兜ヘ(–参璸)";
  }
  lvAgCountData->Columns->Items[0]->Width = 160;
  for (int i=0; i<g_nCallCountTotal; i++)
  {
    lvAgCountData->Columns->Add();
    lvAgCountData->Columns->Items[i+1]->Caption = i;
  }

  for (i=0; i<g_nAgItemNum; i++)
  {
    ListItem1 = lvAgCountData->Items->Add();
    ListItem1->Caption = AgRemark[i];

    for (j=0; j<g_nCallCountTotal; j++)
      ListItem1->SubItems->Insert(j, "0");
  }
}

void TFormMain::ProcontrkcountdataMsg(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nCountTimeId, nItemNum;

  nCountTimeId = atoi(XMLMsg.GetAttrValue(1).C_Str());
  if (nCountTimeId >= g_nCallCountTotal) return;
  nItemNum = MySplit(XMLMsg.GetAttrValue(2).C_Str(), ',', pstrTrkCountList);
  if (nItemNum>=20)
  {
    for (int i=0; i<g_nTrkItemNum; i++)
      g_TodayTrkCount[nCountTimeId].TrkValues[i] = StrToInt(pstrTrkCountList->Strings[i+3]);
  }
  else
  {
    g_TodayTrkCount[nCountTimeId].Init();
  }

  for (int i=0; i<g_nTrkItemNum; i++)
  {
    ListItem1 = lvTrkCountData->Items->Item[i];
    ListItem1->SubItems->Strings[nCountTimeId] = g_TodayTrkCount[nCountTimeId].TrkValues[i];
  }
}
void TFormMain::ProconagcountdataMsg(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nCountTimeId, nItemNum;

  nCountTimeId = atoi(XMLMsg.GetAttrValue(1).C_Str());
  if (nCountTimeId >= g_nCallCountTotal) return;
  nItemNum = MySplit(XMLMsg.GetAttrValue(2).C_Str(), ',', pstrAgCountList);
  if (nItemNum>=21)
  {
    for (int i=0; i<g_nAgItemNum; i++)
      g_TodayAgCount[nCountTimeId].AgValues[i] = StrToInt(pstrAgCountList->Strings[i+3]);
  }
  else
  {
    g_TodayAgCount[nCountTimeId].Init();
  }

  for (int i=0; i<g_nAgItemNum; i++)
  {
    ListItem1 = lvAgCountData->Items->Item[i];
    ListItem1->SubItems->Strings[nCountTimeId] = g_TodayAgCount[nCountTimeId].AgValues[i];
  }
}

void TFormMain::ProcOnLoginMsg(CXMLMsg &XMLMsg)
{
  char szTemp[256];
  int nServiceId = atoi(XMLMsg.GetAttrValue(1).C_Str());
  int nResult = atoi(XMLMsg.GetAttrValue(2).C_Str());
  switch (nServiceId)
  {
  case 1:
    if (nResult == 0)
    {
      sprintf(szTemp, "%s >> ALRM: Login IVR service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
      //FormMain->lbConnectIVRStatus->Caption = "已登录！";
      FormMain->imgConnectIVRStatus->Picture->LoadFromFile(imgGreenFile);
      if (gLangID == 1)
        FormMain->imgConnectIVRStatus->Hint = "已登录到IVR服务";
      else
        FormMain->imgConnectIVRStatus->Hint = "祅魁IVR狝叭";
      g_nConnectIVRId = 4;
      SendGetStatusMsg(1, LOGMSG_getchnstatus, g_bGetchnstatus);
      SendGetStatusMsg(1, LOGMSG_getagentstatus, g_bGetagentstatus);
      SendGetStatusMsg(1, LOGMSG_getqueueinfo, g_bGetqueueinfo);
      SendGetStatusMsg(1, LOGMSG_getivrtracemsg, g_bGetivrtracemsg);
      SendGetStatusMsg(1, LOGMSG_gettrkcallcount, g_bGettrkcallcount);
      SendGetStatusMsg(1, LOGMSG_getagcallcount, g_bGetagcallcount);

      if (g_nConnectFLWId != 4)
        SendLoginFLWMsg(g_strLoginUsername, g_strPassword);
      if (g_nConnectDBId != 4)
        SendLoginDBMsg(g_strLoginUsername, g_strPassword);
      if (g_nRunMode == 2 && g_nConnectCTRLId != 4)
        SendLoginCTRLMsg(g_strLoginUsername, g_strPassword);
      if (gLangID == 1)
        btLogin->Caption = "登出";
      else
        btLogin->Caption = "癶";
      btModifyPsw->Enabled = true;
      if (g_strLoginUsername == "administrator")
      {
        g_nLoginlevelId =1;
      }
      else if (g_strLoginUsername == "manager")
      {
        g_nLoginlevelId =2;
      }
      else if (g_strLoginUsername == "operator")
      {
        g_nLoginlevelId =3;
      }
      else if (g_strLoginUsername == "monitor")
      {
        g_nLoginlevelId =4;
      }
      g_nLoginId = 1;
      WriteLOGAutoLogin(g_strLoginUsername, g_strPassword, g_bAutoLoginId);
      //if (g_bRunOnServicePCId == true)
      //  ReadAuthData();

      if (gLangID == 1)
      {
      #ifdef QUARKCALL_PLATFORM
      FormMain->Caption = strAPPTitle+"("+g_strLoginUsername+")";
      if (g_bAutoLoginId == false)
        MessageBox(NULL,"登录平台成功!!!","警告",MB_OK|MB_ICONINFORMATION);
      #else
      FormMain->Caption = strAPPTitle+"("+g_strLoginUsername+")";
      if (g_bAutoLoginId == false)
        MessageBox(NULL,"登录平台成功!!!","警告",MB_OK|MB_ICONINFORMATION);
      #endif
      }
      else
      {
      #ifdef QUARKCALL_PLATFORM
      FormMain->Caption = strAPPTitle+"("+g_strLoginUsername+")";
      if (g_bAutoLoginId == false)
        MessageBox(NULL,"祅魁キ籓Θ!!!","牡",MB_OK|MB_ICONINFORMATION);
      #else
      FormMain->Caption = strAPPTitle+"("+g_strLoginUsername+")";
      if (g_bAutoLoginId == false)
        MessageBox(NULL,"祅魁キ籓Θ!!!","牡",MB_OK|MB_ICONINFORMATION);
      #endif
      }
    }
    else
    {
      if (gLangID == 1)
      {
        btLogin->Caption = "登录";
        btModifyPsw->Enabled = false;
        g_nLoginId = 0;
        sprintf(szTemp, "%s >> ALRM: Login IVR service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
        #ifdef QUARKCALL_PLATFORM
        MessageBox(NULL,"登录平台失败!!!","警告",MB_OK|MB_ICONWARNING);
        #else
        MessageBox(NULL,"登录平台失败!!!","警告",MB_OK|MB_ICONWARNING);
        #endif
      }
      else
      {
        btLogin->Caption = "祅魁";
        btModifyPsw->Enabled = false;
        g_nLoginId = 0;
        sprintf(szTemp, "%s >> ALRM: Login IVR service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
        #ifdef QUARKCALL_PLATFORM
        MessageBox(NULL,"祅魁キ籓ア毖!!!","牡",MB_OK|MB_ICONWARNING);
        #else
        MessageBox(NULL,"祅魁キ籓ア毖!!!","牡",MB_OK|MB_ICONWARNING);
        #endif
      }
    }
    FormMain->ProcIVRMsg(0, szTemp);
    break;
  case 2:
    if (nResult == 0)
    {
      sprintf(szTemp, "%s >> ALRM: Login FLW service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
      FormMain->imgConnectFLWStatus->Picture->LoadFromFile(imgGreenFile);
      if (gLangID == 1)
        FormMain->imgConnectFLWStatus->Hint = "已登录到流程服务";
      else
        FormMain->imgConnectFLWStatus->Hint = "祅魁瑈祘狝叭";
      g_nConnectFLWId = 4;
      SendGetStatusMsg(2, LOGMSG_getloadedflws, g_bGetloadedflws);
      SendGetStatusMsg(2, LOGMSG_getflwtracemsg, g_bGetflwtracemsg);
    }
    else
      sprintf(szTemp, "%s >> ALRM: Login FLW service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
    FormMain->ProcFLWMsg(0, szTemp);
    break;
  case 3:
    if (nResult == 0)
    {
      FormMain->imgConnectDBStatus->Picture->LoadFromFile(imgGreenFile);
      if (gLangID == 1)
        FormMain->imgConnectDBStatus->Hint = "已登录到数据库网关服务";
      else
        FormMain->imgConnectDBStatus->Hint = "祅魁戈畐筯笵竟狝叭";
      sprintf(szTemp, "%s >> ALRM: Login DB service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
      g_nConnectDBId = 4;
    }
    else
      sprintf(szTemp, "%s >> ALRM: Login DB service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
    FormMain->ProcFLWMsg(0, szTemp);
    break;
  case 4:
    if (nResult == 0)
    {
      FormMain->imgConnectCTRLStatus->Picture->LoadFromFile(imgGreenFile);
      if (gLangID == 1)
        FormMain->imgConnectCTRLStatus->Hint = "已登录到平台服务器";
      else
        FormMain->imgConnectCTRLStatus->Hint = "祅魁キ籓狝竟";
      sprintf(szTemp, "%s >> ALRM: Login CTRL service success!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
      g_nConnectCTRLId = 4;
    }
    else
      sprintf(szTemp, "%s >> ALRM: Login CTRL service fail!",Now().FormatString("yyyy-mm-dd hh:nn:ss").c_str());
    FormMain->ProcIVRMsg(0, szTemp);
    break;
  }
}
void TFormMain::ProcOnivronoffMsg(CXMLMsg &XMLMsg)
{
  if (g_pChnStatus != NULL)
  {
    delete []g_pChnStatus;
    g_pChnStatus = NULL;
  }
  if (g_pVopStatus != NULL)
  {
    delete []g_pVopStatus;
    g_pVopStatus = NULL;
  }
  if (g_pAgStatus != NULL)
  {
    delete []g_pAgStatus;
    g_pAgStatus = NULL;
  }
  g_nMaxChnNum = atoi(XMLMsg.GetAttrValue(1).C_Str());
  g_nDispChnNum = 0;
  g_pChnStatus = new CChnStatus[g_nMaxChnNum];
  lvChnStatus->Clear();

  g_nMaxVopNum = atoi(XMLMsg.GetAttrValue(9).C_Str());
  g_nDispVopNum = 0;
  g_pVopStatus = new CVopStatus[g_nMaxVopNum];
  lvVOPState->Clear();

  g_nMaxAgNum = atoi(XMLMsg.GetAttrValue(2).C_Str());
  //g_pAgStatus = new CAgStatus[g_nMaxAgNum];
  g_pAgStatus = new CAgStatus[MAX_AGENT_NUM];
  if (g_nMaxAgNum > MAX_AGENT_NUM)
    g_nMaxAgNum = MAX_AGENT_NUM;
  for (int i=0; i<MAX_AGENT_NUM; i++)
    g_pAgStatus[i].state = 0;
  InitDispAgStatus(g_nMaxAgNum);
  
  if (atoi(XMLMsg.GetAttrValue(3).C_Str()) == 1)
  {
    ckGetqueueinfo->Checked = true;
  }
  else
  {
    ckGetqueueinfo->Checked = false;
  }
  if (atoi(XMLMsg.GetAttrValue(4).C_Str()) == 1)
  {
    ckGetagentstatus->Checked = true;
  }
  else
  {
    ckGetagentstatus->Checked = false;
  }
  if (atoi(XMLMsg.GetAttrValue(5).C_Str()) == 1)
  {
    ckGetchnstatus->Checked = true;
  }
  else
  {
    ckGetchnstatus->Checked = false;
  }
  if (atoi(XMLMsg.GetAttrValue(6).C_Str()) == 1)
  {
    ckGetivrtracemsg->Checked = true;
  }
  else
  {
    ckGetivrtracemsg->Checked = false;
  }
  if (atoi(XMLMsg.GetAttrValue(7).C_Str()) == 1)
  {
    ckGetTrkCountData->Checked = true;
  }
  else
  {
    ckGetTrkCountData->Checked = false;
  }
  if (atoi(XMLMsg.GetAttrValue(8).C_Str()) == 1)
  {
    ckGetAgCountData->Checked = true;
  }
  else
  {
    ckGetAgCountData->Checked = false;
  }
}
void TFormMain::ProcOnflwonoffMsg(CXMLMsg &XMLMsg)
{
  if (atoi(XMLMsg.GetAttrValue(1).C_Str()) == 1)
  {
    ckGetgetloadedflws->Checked = true;
  }
  else
  {
    ckGetgetloadedflws->Checked = false;
  }
  if (atoi(XMLMsg.GetAttrValue(2).C_Str()) == 1)
  {
    ckGetgetflwtracemsg->Checked = true;
  }
  else
  {
    ckGetgetflwtracemsg->Checked = false;
  }
  lvLoadFlw->Items->Clear();
}
void TFormMain::InitDispChnStatus(int nMaxChnNum)
{
  TListItem  *ListItem1;
  lvChnStatus->Clear();
  for (int i=0; i<nMaxChnNum; i++)
  {
    ListItem1 = lvChnStatus->Items->Add();
    ListItem1->Caption = IntToStr(i);
    ListItem1->SubItems->Insert(0, "");
    ListItem1->SubItems->Insert(1, "");
    ListItem1->SubItems->Insert(2, "");
    ListItem1->SubItems->Insert(3, "");
    ListItem1->SubItems->Insert(4, "");
    ListItem1->SubItems->Insert(5, "");
    ListItem1->SubItems->Insert(6, "");
    ListItem1->SubItems->Insert(7, "");
    ListItem1->SubItems->Insert(8, "");
    ListItem1->SubItems->Insert(9, "");
    ListItem1->SubItems->Insert(10, "");
    ListItem1->SubItems->Insert(11, "");
    ListItem1->SubItems->Insert(12, "");
    ListItem1->SubItems->Insert(13, "");
    ListItem1->ImageIndex = 0;
  }
}
void TFormMain::DispChnStatus(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nItemCount, nListItemNo;
  int nChn, nChType, nChIndex, nLgChnType, nLgChnNo;
  int nHwState, nLineState, nSsState, nCallInOut, nBlockId;

  nChn = atoi(XMLMsg.GetAttrValue(1).C_Str());
  if (nChn >= g_nMaxChnNum)
    return;

  nChType = atoi(XMLMsg.GetAttrValue(2).C_Str());
  if (nChType <= 0)
    return;
  nLgChnType = atoi(XMLMsg.GetAttrValue(4).C_Str());
  if (nChType == CH_A_NONE)
    return;
  if (nLgChnType != 1 && nLgChnType != 2 && nLgChnType != 3 && nLgChnType != 4)
    return;

  nChIndex = atoi(XMLMsg.GetAttrValue(3).C_Str());
  nLgChnNo = atoi(XMLMsg.GetAttrValue(5).C_Str());

  nHwState = atoi(XMLMsg.GetAttrValue(6).C_Str());
  nLineState = atoi(XMLMsg.GetAttrValue(7).C_Str());
  nSsState = atoi(XMLMsg.GetAttrValue(8).C_Str());
  nCallInOut = atoi(XMLMsg.GetAttrValue(9).C_Str());

  g_pChnStatus[nChn].nChType = nChType;
  g_pChnStatus[nChn].nChIndex = nChIndex;

  g_pChnStatus[nChn].nLgChnType = nLgChnType;
  g_pChnStatus[nChn].nLgChnNo = nLgChnNo;

  g_pChnStatus[nChn].hwState = nHwState;
  g_pChnStatus[nChn].lnState = nLineState;
  g_pChnStatus[nChn].ssState = nSsState;
  g_pChnStatus[nChn].CallInOut = nCallInOut;
  CountChnState();

  nItemCount = lvChnStatus->Items->Count;
  if (g_pChnStatus[nChn].bDispID == false)
  {
    ListItem1 = lvChnStatus->Items->Add();
    ListItem1->Caption = IntToStr(nItemCount);
    ListItem1->SubItems->Insert(0, "");
    ListItem1->SubItems->Insert(1, "");
    ListItem1->SubItems->Insert(2, "");
    ListItem1->SubItems->Insert(3, "");
    ListItem1->SubItems->Insert(4, "");
    ListItem1->SubItems->Insert(5, "");
    ListItem1->SubItems->Insert(6, "");
    ListItem1->SubItems->Insert(7, "");
    ListItem1->SubItems->Insert(8, "");
    ListItem1->SubItems->Insert(9, "");
    ListItem1->SubItems->Insert(10, "");
    ListItem1->SubItems->Insert(11, "");
    ListItem1->SubItems->Insert(12, "");
    ListItem1->SubItems->Insert(13, "");
    ListItem1->ImageIndex = 0;
    g_pChnStatus[nChn].bDispID = true;
    g_pChnStatus[nChn].nListItemNo = nItemCount;
  }
  nListItemNo = g_pChnStatus[nChn].nListItemNo;

  nItemCount = lvChnStatus->Items->Count;
  if (nListItemNo < nItemCount)
  {
    ListItem1 = lvChnStatus->Items->Item[nListItemNo];
    ListItem1->SubItems->Strings[0] = (char *)(XMLMsg.GetAttrValue(15).C_Str());
    //硬件通道类型
	  switch (nChType)
	  {
	  case CH_NONE:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "未知类型" : "ゼ摸";
		  break;
	  case CH_A_SEAT:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "模拟内线" : "摸ゑだ诀";
		  break;
	  case CH_A_EXTN:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "模拟中继" : "摸ゑい膥";
		  break;
	  case CH_D_SEAT:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "ISDN坐席" : "ISDN畒畊";
		  break;
	  case CH_D_EXTN:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "ISDN外线" : "ISDN絬";
		  break;
	  case CH_A_RECD:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "模拟录音" : "摸ゑ魁";
		  break;
	  case CH_D_RECD:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "ISDN录音" : "ISDN魁";
		  break;
	  case CH_E_RECD:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "数字录音" : "计魁";
		  break;
	  case CH_E_SS1:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "CSS1中继" : "CSS1い膥";
		  break;
	  case CH_E_TUP:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "TUP中继" : "TUPい膥";
		  break;
	  case CH_E_ISUP:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "ISUP中继" : "ISUPい膥";
		  break;
	  case CH_E_ISDN_U:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "ISDN用户" : "ISDNノめ";
		  break;
	  case CH_E_ISDN_N:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "ISDN网络" : "ISDN呼蹈";
		  break;
	  case CH_D_FAX:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "传真通道" : "肚痷硄笵";
		  break;
	  case CH_S_FAX:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "软传真" : "硁肚痷";
		  break;
	  case CH_D_VOIP:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "H323分机" : "H323だ诀";
		  break;
	  case CH_S_VOIP:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "SIP分机" : "SIPだ诀";
		  break;
	  case CH_A_EM4:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "E&M中继" : "E&Mい膥";
		  break;
	  case CH_S_TRUNK:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "SIP中继" : "SIPい膥";
		  break;
	  case CH_H_TRUNK:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "H323中继" : "H323い膥";
		  break;
	  case CH_A_NONE:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "未插模块" : "ゼ础家遏";
		  break;
	  case CH_E_LINESIDE:
		  ListItem1->SubItems->Strings[1] = "LineSide";
		  break;
	  default:
		  ListItem1->SubItems->Strings[1] = (gLangID == 1) ? "未知类型" : "ゼ摸";
		  break;
	  }
    ListItem1->SubItems->Strings[2] = (char *)(XMLMsg.GetAttrValue(3).C_Str()); //硬件通道号
    //业务通道类型
	  switch (nLgChnType)
    {
    case CHANNEL_TRUNK: 
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "中继" : "い膥";
      break;
    case CHANNEL_SEAT:
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "坐席" : "畒畊";
      break;
    case CHANNEL_VOIP:
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "VOIP" : "VOIP";
      break;
    case CHANNEL_REC:
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "监录" : "菏魁";
      break;
    case CHANNEL_FAX:
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "传真" : "肚痷";
      break;
    case CHANNEL_IVR:
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "IVR" : "IVR";
      break;
    default:
      ListItem1->SubItems->Strings[3] = (gLangID == 1) ? "未知" : "ゼ";
      break;
    }
    ListItem1->SubItems->Strings[4] = (char *)(XMLMsg.GetAttrValue(5).C_Str()); //业务通道号
    nBlockId;
    //硬件状态
    switch (nHwState)
	  {
	  case CHN_HW_NOINIT:
		  ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "未用" : "ゼノ";
      nBlockId = 0;
      break;
	  case CHN_HW_VALID:
		  ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "可用" : "タ盽";
      nBlockId = 1;
		  break;
	  case CHN_HW_BLOCK1:
		  ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "电路阻塞" : "癹隔超峨";
      nBlockId = 0;
		  break;
	  case CHN_HW_BLOCK2:
		  ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "链路阻塞" : "渺隔超峨";
      nBlockId = 0;
		  break;
	  case CHN_HW_BLOCK3:
		  ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "阻塞" : "超峨";
      nBlockId = 0;
		  break;
    default:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "其他阻塞" : "ㄤ超峨";
      nBlockId = 0;
      break;
	  }
    //线路状态
    switch (nLineState)
	  {
	  case CHN_LN_IDLE:
		  ListItem1->SubItems->Strings[6] = (gLangID == 1) ? "空闲" : "丁";
		  break;
	  case CHN_LN_SEIZE:
		  ListItem1->SubItems->Strings[6] = (gLangID == 1) ? "占用" : "ノ";
      nBlockId = 4;
		  break;
	  case CHN_LN_RELEASE:
		  ListItem1->SubItems->Strings[6] = (gLangID == 1) ? "释放" : "睦";
      nBlockId = 4;
		  break;
	  case CHN_LN_WAIT_REL:
		  ListItem1->SubItems->Strings[6] = (gLangID == 1) ? "释放等待" : "睦单";
      nBlockId = 4;
		  break;
    default:
      ListItem1->SubItems->Strings[6] = "";
      nBlockId = 4;
      break;
	  }
    //信令状态
	  switch (nSsState)
	  {
	  case CHN_SNG_IDLE:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_IDLE";
		  break;
	  case CHN_SNG_DELAY:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_DELAY";
		  break;
	  case CHN_SNG_IN_ARRIVE:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_IN_ARRIVE";
		  break;
	  case CHN_SNG_IN_WAIT:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_IN_WAIT";
		  break;
	  case CHN_SNG_IN_RING:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_IN_RING";
		  break;
	  case CHN_SNG_IN_TALK:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_IN_TALK";
		  break;
	  case CHN_SNG_OT_READY:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_READY";
		  break;
	  case CHN_SNG_OT_HANGOFF:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_HANGOFF";
		  break;
	  case CHN_SNG_OT_DTMF:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_DTMF";
		  break;
	  case CHN_SNG_OT_WAIT:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_WAIT";
		  break;
	  case CHN_SNG_OT_RING:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_RING";
		  break;
	  case CHN_SNG_OT_TALK:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_TALK";
		  break;
	  case CHN_SNG_IN_FAIL:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_IN_FAIL";
		  break;
	  case CHN_SNG_OT_FAIL:
		  ListItem1->SubItems->Strings[7] = "CHN_SNG_OT_FAIL";
		  break;
	  default:
		  ListItem1->SubItems->Strings[7] = "";
		  break;
	  }
	  //方向
    switch (nCallInOut)
	  {
	  case CALL_IN:
		  ListItem1->SubItems->Strings[8] = (gLangID == 1) ? "呼入" : "挤";
		  break;
	  case CALL_OUT:
		  ListItem1->SubItems->Strings[8] = (gLangID == 1) ? "呼出" : "挤";
		  break;
	  case CALL_TRAN_IN:
		  ListItem1->SubItems->Strings[8] = (gLangID == 1) ? "转接呼入" : "锣钡挤";
		  break;
	  case CALL_TRAN_OUT:
		  ListItem1->SubItems->Strings[8] = (gLangID == 1) ? "转接呼出" : "锣钡挤";
		  break;
	  case CALL_AUTO:
		  ListItem1->SubItems->Strings[8] = (gLangID == 1) ? "自动流程" : "笆瑈祘";
		  break;
	  default:
		  ListItem1->SubItems->Strings[8] = "";
		  break;
	  }
    ListItem1->SubItems->Strings[9] = (char *)(XMLMsg.GetAttrValue(10).C_Str()); //主叫号码
    ListItem1->SubItems->Strings[10] = (char *)(XMLMsg.GetAttrValue(11).C_Str()); //被叫号码
    ListItem1->SubItems->Strings[11] = (char *)(XMLMsg.GetAttrValue(12).C_Str()); //呼叫时间
    ListItem1->SubItems->Strings[12] = (char *)(XMLMsg.GetAttrValue(13).C_Str()); //应答时间
    ListItem1->SubItems->Strings[13] = (char *)(XMLMsg.GetAttrValue(14).C_Str()); //附加信息
    ListItem1->ImageIndex = nBlockId;
  }
}
void TFormMain::CountChnState()
{
  int nChnBusyCount=0;
  int nChnIdelCount=0;
  int nChnBlockCount=0;
  int nChnInCount=0;
  int nChnOutCount=0;

  for (int i = 0; i < g_nMaxChnNum; i ++)
  {
    if (g_pChnStatus[i].nLgChnType != 1 && g_pChnStatus[i].nLgChnType != 2
        && g_pChnStatus[i].nLgChnType != 3 && g_pChnStatus[i].nLgChnType != 4)
      continue;
    if (g_pChnStatus[i].hwState != 0)
      nChnBlockCount ++;
    if (g_pChnStatus[i].lnState != 0)
      nChnBusyCount ++;
    else
    {
      if (g_pChnStatus[i].hwState == 0)
        nChnIdelCount ++;
    }
    if (g_pChnStatus[i].CallInOut == 1)
      nChnInCount ++;
    else if (g_pChnStatus[i].CallInOut == 2)
      nChnOutCount ++;
  }
  lbChnBusyCount->Caption = nChnBusyCount;
  lbChnIdelCount->Caption = nChnIdelCount;
  lbChnBlockCount->Caption = nChnBlockCount;
  lbChnInCount->Caption = nChnInCount;
  lbChnOutCount->Caption = nChnOutCount;

  if (nChnBusyCount > g_nMaxBusyTrunk && g_nMaxBusyTrunk > 0)
  {
    if (nChnBusyCount > g_nChnBusyCount)
    {
      g_bBusyTrunkSound = true;
      ckBusyTrunkSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("通道占用数 %d 大于设定值 %d 告警提示!!!",
          nChnBusyCount, g_nMaxBusyTrunk);
      else
        WriteAlartMsg("硄笵ノ计 %d 砞﹚ %d 牡矗ボ!!!",
          nChnBusyCount, g_nMaxBusyTrunk);
    }
  }
  else
  {
    if (g_bBusyTrunkSound == true)
    {
      g_bBusyTrunkSound = false;
      ckBusyTrunkSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("中继通道占用数超过设定值告警消除提示!!!");
      else
        WriteAlartMsg("い膥硄笵ノ计禬筁砞﹚牡埃矗ボ!!!");
    }
  }

  if (nChnBlockCount > 0)
  {
    if (nChnBlockCount > g_nChnBlockCount)
    {
      g_bBlockTrunkSound = true;
      ckBlockTrunkSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("中继阻塞数 %d 告警提示!!!", nChnBlockCount);
      else
        WriteAlartMsg("い膥超峨计 %d 牡矗ボ!!!", nChnBlockCount);
    }
  }
  else
  {
    if (g_bBlockTrunkSound == true)
    {
      g_bBlockTrunkSound = false;
      if (g_bBlockIVRSound == false)
        ckBlockTrunkSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("中继阻塞告警消除提示!!!");
      else
        WriteAlartMsg("い膥超峨牡埃矗ボ!!!");
    }
  }

  g_nChnBusyCount = nChnBusyCount;
  g_nChnIdelCount = nChnIdelCount;
  g_nChnBlockCount = nChnBlockCount;
  g_nChnInCount = nChnInCount;
  g_nChnOutCount = nChnOutCount;
}
void TFormMain::DispVopStatus(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nItemCount, nListItemNo;
  int nVopIndex, nVopNo, nVopChnNo, nVopChnType, nChIndex;
  int nLnState, nSvState, nSsState, nImgIndex=0;

  nVopIndex = atoi(XMLMsg.GetAttrValue(1).C_Str());
  if (nVopIndex >= g_nMaxVopNum)
    return;

  nVopChnType = atoi(XMLMsg.GetAttrValue(4).C_Str());
  nLnState = atoi(XMLMsg.GetAttrValue(8).C_Str());
  if (nVopChnType < 0 || nLnState == 2 || nLnState == 3)
    return;

  nVopNo = atoi(XMLMsg.GetAttrValue(2).C_Str());
  nVopChnNo = atoi(XMLMsg.GetAttrValue(3).C_Str());
  nChIndex = atoi(XMLMsg.GetAttrValue(6).C_Str());
  nSsState = atoi(XMLMsg.GetAttrValue(9).C_Str());
  nSvState = atoi(XMLMsg.GetAttrValue(10).C_Str());

  g_pVopStatus[nVopIndex].nVopIndex = nVopIndex;
  g_pVopStatus[nVopIndex].nVopChnNo = nVopChnNo;
  g_pVopStatus[nVopIndex].nVopChnType = nVopChnType;
  g_pVopStatus[nVopIndex].lnState = nLnState;
  g_pVopStatus[nVopIndex].svState = nSvState;
  g_pVopStatus[nVopIndex].ssState = nSsState;

  CountVopState();
  nItemCount = lvVOPState->Items->Count;
  if (g_pVopStatus[nVopIndex].bDispID == false)
  {
    ListItem1 = lvVOPState->Items->Add();
    ListItem1->Caption = IntToStr(nItemCount);
    ListItem1->SubItems->Insert(0, "");
    ListItem1->SubItems->Insert(1, "");
    ListItem1->SubItems->Insert(2, "");
    ListItem1->SubItems->Insert(3, "");
    ListItem1->SubItems->Insert(4, "");
    ListItem1->SubItems->Insert(5, "");
    ListItem1->SubItems->Insert(6, "");
    ListItem1->SubItems->Insert(7, "");
    ListItem1->SubItems->Insert(8, "");
    ListItem1->SubItems->Insert(9, "");
    ListItem1->ImageIndex = 0;
    g_pVopStatus[nVopIndex].bDispID = true;
    g_pVopStatus[nVopIndex].nListItemNo = nItemCount;
  }
  nListItemNo = g_pVopStatus[nVopIndex].nListItemNo;

  nItemCount = lvVOPState->Items->Count;
  if (nListItemNo < nItemCount)
  {
    ListItem1 = lvVOPState->Items->Item[nListItemNo];
    ListItem1->SubItems->Strings[0] = IntToStr(nVopNo);
    ListItem1->SubItems->Strings[1] = IntToStr(nVopChnNo);
    if (nVopChnType == 0 || nVopChnType == 1)
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "IVR资源" : "IVR硄笵";
    else if (nVopChnType == 2)
      ListItem1->SubItems->Strings[2] = "FAX";
    else if (nVopChnType == 3 || nVopChnType == 4 || nVopChnType == 5)
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "坐席监录" : "畒畊菏魁";
    else if (nVopChnType == 6 || nVopChnType == 7)
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "中继监录" : "い膥菏魁";
    else if (nVopChnType == 8)
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "语音中继" : "粂い膥";
    else
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "未知" : "ゼ";
    ListItem1->SubItems->Strings[3] = IntToStr(nChIndex);
    switch (nLnState)
    {
    case 0:
      ListItem1->SubItems->Strings[4] = (gLangID == 1) ? "可用" : "タ盽";
      nImgIndex = 1;
      break;
    case 1:
      ListItem1->SubItems->Strings[4] = (gLangID == 1) ? "阻塞" : "峨";
      break;
    case 2:
      ListItem1->SubItems->Strings[4] = (gLangID == 1) ? "关闭" : "闽超";
      break;
    case 3:
      ListItem1->SubItems->Strings[4] = (gLangID == 1) ? "阻塞" : "峨";
      break;
    default:
      ListItem1->SubItems->Strings[4] = (gLangID == 1) ? "未知" : "ゼ";
      break;
    }
    switch (nSvState)
    {
    case 0:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "空闲" : "丁";
      break;
    case 1:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "正在放音" : "タ";
      if (nLnState != 0)
        nImgIndex = 2;
      break;
    case 2:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "正在录音" : "タ魁";
      if (nLnState != 0)
        nImgIndex = 3;
      break;
    case 3:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "发送传真" : "祇癳肚痷";
      if (nLnState != 0)
        nImgIndex = 3;
      break;
    case 4:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "接收传真" : "钡Μ肚痷";
      if (nLnState != 0)
        nImgIndex = 3;
      break;
    default:
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "未知" : "ゼ";
      break;
    }
    ListItem1->SubItems->Strings[6] = (char *)(XMLMsg.GetAttrValue(11).C_Str()); //连接的交换机的分机号
    ListItem1->SubItems->Strings[7] = (char *)(XMLMsg.GetAttrValue(12).C_Str()); //主叫号码
    ListItem1->SubItems->Strings[8] = (char *)(XMLMsg.GetAttrValue(13).C_Str()); //呼叫时间
    if (nSsState == 0)
      ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "空闲" : "秪";
    else
      ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "占用" : "ノ";
    ListItem1->ImageIndex = nImgIndex;
  }
}
void TFormMain::CountVopState()
{
  int nIVRBusyCount=0;
  int nIVRIdelCount=0;
  int nRecBusyCount=0;
  int nRecIdelCount=0;
  int nVopBlockCount=0;

  for (int i = 0; i < g_nMaxVopNum; i ++)
  {
    if (g_pVopStatus[i].nVopChnType < 0)
      continue;
    if (g_pVopStatus[i].lnState == 0)
    {
      if (g_pVopStatus[i].nVopChnType >= 3 && g_pVopStatus[i].nVopChnType <= 7)
      {
        if (g_pVopStatus[i].ssState == 0)
          nRecIdelCount++;
        else
          nRecBusyCount++;
      }
      else
      {
        if (g_pVopStatus[i].ssState == 0)
          nIVRIdelCount++;
        else
          nIVRBusyCount++;
      }
    }
    else
    {
      nVopBlockCount ++;
    }
  }
  lbIVRBusyCount->Caption = nIVRBusyCount;
  lbIVRIdelCount->Caption = nIVRIdelCount;
  lbRECBusyCount->Caption = nRecBusyCount;
  lbRECIdelCount->Caption = nRecIdelCount;
  lbVopBlockCount->Caption = nVopBlockCount;

  if (nIVRBusyCount > g_nMaxBusyIVR && g_nMaxBusyIVR > 0)
  {
    if (nIVRBusyCount > g_nIVRBusyCount)
    {
      g_bBusyIVRSound = true;
      ckBusyIVRSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("IVR占用数 %d 大于设定值 %d 警告提示!!!",
          nIVRBusyCount, g_nMaxBusyIVR);
      else
        WriteAlartMsg("IVRノ计 %d 砞﹚ %d 牡矗ボ!!!",
          nIVRBusyCount, g_nMaxBusyIVR);
    }
  }
  else
  {
    if (g_bBusyIVRSound == true)
    {
      g_bBusyIVRSound = false;
      ckBusyIVRSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("IVR占用数超过设定值告警消除提示!!!");
      else
        WriteAlartMsg("IVRノ计禬筁砞﹚牡埃矗ボ!!!");
    }
  }

  if (nVopBlockCount > 0)
  {
    if (nVopBlockCount > g_nVopBlockCount)
    {
      g_bBlockIVRSound = true;
      ckBlockTrunkSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("IVR通道阻塞数 %d 告警提示!!!",
          nVopBlockCount);
      else
        WriteAlartMsg("IVR硄笵峨计 %d 牡矗ボ!!!",
          nVopBlockCount);
    }
  }
  else
  {
    if (g_bBlockIVRSound == true)
    {
      g_bBlockIVRSound = false;
      if (g_bBlockTrunkSound == false)
        ckBlockTrunkSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("IVR通道阻塞告警消除提示!!!");
      else
        WriteAlartMsg("IVR硄笵峨牡埃矗ボ!!!");
    }
  }

  g_nIVRBusyCount = nIVRBusyCount;
  g_nIVRIdelCount = nIVRIdelCount;
  g_nRecBusyCount = nRecBusyCount;
  g_nRecIdelCount = nRecIdelCount;
  g_nVopBlockCount = nVopBlockCount;
}
void TFormMain::InitDispAgStatus(int nMaxAgNum)
{
  TListItem  *ListItem1;
  lvAgStatus->Clear();
  for (int i=0; i<nMaxAgNum; i++)
  {
    g_pAgStatus[i].state = 1;
    ListItem1 = lvAgStatus->Items->Add();
    ListItem1->Caption = IntToStr(i);
    ListItem1->SubItems->Insert(0, "");
    ListItem1->SubItems->Insert(1, "");
    ListItem1->SubItems->Insert(2, "");
    ListItem1->SubItems->Insert(3, "");
    ListItem1->SubItems->Insert(4, "");
    ListItem1->SubItems->Insert(5, "");
    ListItem1->SubItems->Insert(6, "");
    ListItem1->SubItems->Insert(7, "");
    ListItem1->SubItems->Insert(8, "");
    ListItem1->SubItems->Insert(9, "");
    ListItem1->SubItems->Insert(10, "");
    ListItem1->SubItems->Insert(11, "");
    ListItem1->SubItems->Insert(12, "");
    ListItem1->SubItems->Insert(13, "");
    ListItem1->ImageIndex = 2;
  }
}
void TFormMain::ProconmodifyseatdataMsg(CXMLMsg &XMLMsg)
{
  if (atoi(XMLMsg.GetAttrValue(3).C_Str()) != 0)
    return;
  int nTemp = atoi(XMLMsg.GetAttrValue(2).C_Str());
  if (nTemp > MAX_AGENT_NUM)
    nTemp = MAX_AGENT_NUM;
  if (g_nMaxAgNum >= nTemp)
    return;

  TListItem  *ListItem1;
  for (int i=g_nMaxAgNum; i<nTemp; i++)
  {
    g_pAgStatus[i].state = 1;
    ListItem1 = lvAgStatus->Items->Add();
    ListItem1->Caption = IntToStr(i);
    ListItem1->SubItems->Insert(0, "");
    ListItem1->SubItems->Insert(1, "");
    ListItem1->SubItems->Insert(2, "");
    ListItem1->SubItems->Insert(3, "");
    ListItem1->SubItems->Insert(4, "");
    ListItem1->SubItems->Insert(5, "");
    ListItem1->SubItems->Insert(6, "");
    ListItem1->SubItems->Insert(7, "");
    ListItem1->SubItems->Insert(8, "");
    ListItem1->SubItems->Insert(9, "");
    ListItem1->SubItems->Insert(10, "");
    ListItem1->SubItems->Insert(11, "");
    ListItem1->SubItems->Insert(12, "");
    ListItem1->SubItems->Insert(13, "");
    ListItem1->ImageIndex = 2;
  }
  g_nMaxAgNum = nTemp;
}
void TFormMain::Proconmodifypassword(CXMLMsg &XMLMsg)
{
  if (atoi(XMLMsg.GetAttrValue(1).C_Str()) == 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"修改密码成功！","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"э盞絏Θ:","獺矗ボ",MB_OK|MB_ICONINFORMATION);
  }
  else
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，修改密码失败！","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬э盞絏ア毖:","獺矗ボ",MB_OK|MB_ICONWARNING);
  }
}
void TFormMain::DispAgStatus(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nAg, nItemCount;
  int nChnType, nChnNo;
  char szTemp[128];

  nAg = atoi(XMLMsg.GetAttrValue(1).C_Str());
  nItemCount = lvAgStatus->Items->Count;
  if (nAg < nItemCount)
  {
    ListItem1 = lvAgStatus->Items->Item[nAg];

    ListItem1->SubItems->Strings[0] = (char *)(XMLMsg.GetAttrValue(2).C_Str()); //坐席分机号
    ListItem1->SubItems->Strings[1] = (char *)(XMLMsg.GetAttrValue(3).C_Str()); //客户端编号
    //坐席类型
    switch (atoi(XMLMsg.GetAttrValue(4).C_Str()))
    {
    case SEATTYPE_AG_TEL:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "内线电话座席" : "だ诀筿杠畒畊";
      break;
    case SEATTYPE_AG_PC:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "内线电脑座席" : "だ诀筿福畒畊";
      break;
    case SEATTYPE_DT_TEL:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "数字线电话座席" : "计絬筿杠畒畊";
      break;
    case SEATTYPE_DT_PC:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "数字线电脑座席" : "计絬筿福畒畊";
      break;
    case SEATTYPE_RT_TEL:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "远程电话座席" : "环祘筿杠畒畊";
      break;
    case SEATTYPE_RT_VOIP:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "远程VOIP座席" : "环祘VOIP畒畊";
      break;
    case SEATTYPE_SWT_IVR:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "交换机IVR端口" : "ユ传诀IVR硈钡梆";
      break;
    default:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "其它类型" : "ㄤウ摸";
      break;
    }
    ListItem1->SubItems->Strings[3] = (char *)(XMLMsg.GetAttrValue(5).C_Str()); //坐席组号
    ListItem1->SubItems->Strings[4] = (char *)(XMLMsg.GetAttrValue(6).C_Str()); //坐席ip
    if (XMLMsg.GetAttrValue(7).Compare("0") == 0)
    {
      ListItem1->SubItems->Strings[5] = (gLangID == 1) ? "未登录" : "ゼ祅魁";
      ListItem1->SubItems->Strings[6] = (char *)(XMLMsg.GetAttrValue(8).C_Str()); //话务员登录时间

      if (XMLMsg.GetAttrValue(19).GetLength() > 0)
      {
        if (gLangID == 1)
          sprintf(szTemp, "[账号=%s]", XMLMsg.GetAttrValue(19).C_Str());
        else
          sprintf(szTemp, "[姐腹=%s]", XMLMsg.GetAttrValue(19).C_Str());
        ListItem1->SubItems->Strings[7] = (char *)(szTemp);
      }
      else
      {
        ListItem1->SubItems->Strings[7] = "";
      }

      ListItem1->SubItems->Strings[8] = "";
      ListItem1->SubItems->Strings[9] = "";
      ListItem1->SubItems->Strings[10] = "";
	    ListItem1->SubItems->Strings[11] = (char *)(XMLMsg.GetAttrValue(14).C_Str()); //应答总次数
	    ListItem1->SubItems->Strings[12] = (char *)(XMLMsg.GetAttrValue(15).C_Str()); //未应答总次数
	    nChnType = atoi(XMLMsg.GetAttrValue(17).C_Str());
	    nChnNo = atoi(XMLMsg.GetAttrValue(18).C_Str());
      if (nChnType == 1)
      {
        ListItem1->SubItems->Strings[13] = ((gLangID == 1) ? "中继通道:" : "い膥硄笵:")+IntToStr(nChnNo);
      }
      else if (nChnType == 2)
      {
        ListItem1->SubItems->Strings[13] = ((gLangID == 1) ? "坐席通道:" : "畒畊硄笵:")+IntToStr(nChnNo);
      }
      else
      {
        ListItem1->SubItems->Strings[13] = (gLangID == 1) ? "未绑定" : "ゼ竕﹚";
      }
      ListItem1->ImageIndex = 2;

      if (nAg < g_nMaxAgNum)
      {
        g_pAgStatus[nAg].LogState = 0;
        g_pAgStatus[nAg].svState = 0;
        g_pAgStatus[nAg].Disturbid = 0;
        g_pAgStatus[nAg].Leaveid = 0;
      }
    }
    else
    {
      ListItem1->SubItems->Strings[5] = (char *)(XMLMsg.GetAttrValue(7).C_Str()); //话务员工号
	    ListItem1->SubItems->Strings[6] = (char *)(XMLMsg.GetAttrValue(8).C_Str()); //话务员登录时间

      if (XMLMsg.GetAttrValue(19).GetLength() > 0)
      {
        if (gLangID == 1)
          sprintf(szTemp, "[姓名=%s][账号=%s]", XMLMsg.GetAttrValue(9).C_Str(), XMLMsg.GetAttrValue(19).C_Str());
        else
          sprintf(szTemp, "[﹎=%s][眀腹=%s]", XMLMsg.GetAttrValue(9).C_Str(), XMLMsg.GetAttrValue(19).C_Str());
      }
      else
        sprintf(szTemp, "%s", XMLMsg.GetAttrValue(9).C_Str());

      ListItem1->SubItems->Strings[7] = (char *)szTemp; //话务员姓名/账号

	    ListItem1->SubItems->Strings[8] = (char *)(XMLMsg.GetAttrValue(10).C_Str()); //话务员组号
	
	    int nSrvState = atoi(XMLMsg.GetAttrValue(11).C_Str());
	    switch (nSrvState)
	    {
		  case 0:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[空闲]" : "[丁]";
		    break;
		  case 1:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[延时态]" : "[┑篈]";
		    break;
		  case 2:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[呼入占用]" : "[挤ノ]";
		    break;
		  case 3:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[正在呼出]" : "[タ挤]";
		    break;
		  case 4:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[呼入振铃]" : "[挤筧]";
		    break;
		  case 5:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[呼出振铃]" : "[挤筧]";
		    break;
		  case 6:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[通话状态]" : "[硄杠篈]";
		    break;
		  case 7:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[等待挂机]" : "[单本诀]";
		    break;
		  case 8:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[放音状态]" : "[篈]";
		    break;
		  case 9:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[话后处理]" : "[杠矪瞶]";
		    break;
		  case 10:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[保持状态]" : "[玂篈]";
		    break;
		  case 11:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[等待IVR]" : "[单IVR]";
		    break;
		  case 12:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[摘机呼出]" : "[篕诀挤]";
		    break;
		  default:
		    ListItem1->SubItems->Strings[9] = (gLangID == 1) ? "[其他]" : "[ㄤ]";
		    break;
	    }
	    int nDisturb = atoi(XMLMsg.GetAttrValue(12).C_Str());
	    int nLeavel = atoi(XMLMsg.GetAttrValue(13).C_Str());
	    AnsiString strState;
	    if (nDisturb == 0)
	    {
	      strState = (gLangID == 1) ? "[示闲]" : "[丁]";
	    }
	    else if (nDisturb == 1)
	    {
	      strState = (gLangID == 1) ? "[示忙]" : "[Γ魁]";
	    }
	    else if (nDisturb == 2)
	    {
	      strState = (gLangID == 1) ? "[等待IVR返回]" : "[单IVR]";
	    }
	    else if (nDisturb == 3)
	    {
	      strState = (gLangID == 1) ? "[外出值班]" : "[痁]";
	    }
	    if (nLeavel == 0)
	    {
        if (nDisturb == 3)
	        strState = strState+"["+(char *)(XMLMsg.GetAttrValue(16).C_Str())+"]";
        else
	        strState = strState+((gLangID == 1) ? "[在席]" : "[]");
	    }
	    else
	    {
	      strState = strState+((gLangID == 1) ? "[离席:" : "[瞒:")+(char *)(XMLMsg.GetAttrValue(16).C_Str())+"]";
	    }
	    ListItem1->SubItems->Strings[10] = strState;
	    ListItem1->SubItems->Strings[11] = (char *)(XMLMsg.GetAttrValue(14).C_Str()); //应答总次数
	    ListItem1->SubItems->Strings[12] = (char *)(XMLMsg.GetAttrValue(15).C_Str()); //未应答总次数
	    nChnType = atoi(XMLMsg.GetAttrValue(17).C_Str());
	    nChnNo = atoi(XMLMsg.GetAttrValue(18).C_Str());
      if (nChnType == 1)
      {
        ListItem1->SubItems->Strings[13] = ((gLangID == 1) ? "中继通道:" : "い膥硄笵:")+IntToStr(nChnNo);
      }
      else if (nChnType == 2)
      {
        ListItem1->SubItems->Strings[13] = ((gLangID == 1) ? "坐席通道:" : "畒畊硄笵:")+IntToStr(nChnNo);
      }
      else
      {
        ListItem1->SubItems->Strings[13] = (gLangID == 1) ? "未绑定" : "ゼ竕﹚";
      }

	    if (nSrvState == 0)
	    {
	      if (XMLMsg.GetAttrValue(7).Compare("0") == 0)
	        ListItem1->ImageIndex = 2;
	      else if (nDisturb != 0 || nLeavel != 0)
	        ListItem1->ImageIndex = 5;
	      else
	        ListItem1->ImageIndex = 3;
	    }
	    else
	    {
	      ListItem1->ImageIndex = 4;
	    }
      if (lvAgStatus->ViewStyle == vsReport)
      {
        ListItem1->Caption = IntToStr(nAg);
      }
      else
      {
        ListItem1->Caption = ListItem1->SubItems->Strings[0] + '\n' + ListItem1->SubItems->Strings[9];
      }
      if (nAg < g_nMaxAgNum)
      {
        g_pAgStatus[nAg].LogState = 1;
        g_pAgStatus[nAg].svState = nSrvState;
        g_pAgStatus[nAg].Disturbid = nDisturb;
        g_pAgStatus[nAg].Leaveid = nLeavel;
      }
    }
    CountAgState();
  }
}
void TFormMain::CountAgState()
{
  int nAgLoginCount = 0;
  int nAgLogoutCount = 0;
  int nAgIdelCount = 0;
  int nAgBusyCount = 0;
  int nAgLeavelCount = 0;

  for (int i = 0; i < g_nMaxAgNum; i ++)
  {
    if (g_pAgStatus[i].state == 0)
      continue;
    if (g_pAgStatus[i].LogState != 0)
      nAgLoginCount ++;
    else
      nAgLogoutCount ++;
    if (g_pAgStatus[i].svState != 0)
      nAgBusyCount ++;
    else
    {
      if (g_pAgStatus[i].LogState != 0)
        nAgIdelCount ++;
    }
    if (g_pAgStatus[i].Disturbid != 0 || g_pAgStatus[i].Leaveid != 0)
      nAgLeavelCount ++;
  }
  lbAgLoginCount->Caption = nAgLoginCount;
  lbAgLogoutCount->Caption = nAgLogoutCount;
  lbAgIdelCount->Caption = nAgIdelCount;
  lbAglevalCount->Caption = nAgLeavelCount;
  lbAgBusyCount->Caption = nAgBusyCount;

  if (nAgLoginCount < g_nMinAgentLogin && g_nMinAgentLogin > 0)
  {
    if (nAgLoginCount < g_nAgLoginCount)
    {
      g_bAgentLoginSound = true;
      ckAgentLoginSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("坐席登录数 %d 小于设定值 %d 警告提示!!!",
          nAgLoginCount, g_nMinAgentLogin);
      else
        WriteAlartMsg("畒畊祅魁计 %d 砞﹚ %d 牡矗ボ!!!",
          nAgLoginCount, g_nMinAgentLogin);
    }
  }
  else
  {
    if (g_bAgentLoginSound == true)
    {
      g_bAgentLoginSound = false;
      ckAgentLoginSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("坐席登录数小于设定值告警消除提示!!!");
      else
        WriteAlartMsg("畒畊祅魁计砞﹚牡埃矗ボ!!!");
    }
  }

  if (nAgLeavelCount > g_nMaxAgentLeval && g_nMaxAgentLeval > 0)
  {
    if (nAgLeavelCount > g_nAgLeavelCount)
    {
      g_bAgentLevalSound = true;
      ckAgentLevalSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("坐席离席数 %d 大于设定值 %d 警告提示!!!",
          nAgLeavelCount, g_nMaxAgentLeval);
      else
        WriteAlartMsg("畒畊瞒计 %d 砞﹚ %d 牡矗ボ!!!",
          nAgLeavelCount, g_nMaxAgentLeval);
    }
  }
  else
  {
    if (g_bAgentLevalSound == true)
    {
      g_bAgentLevalSound = false;
      ckAgentLevalSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("坐席离席数超过设定值告警消除提示!!!");
      else
        WriteAlartMsg("畒畊瞒计禬筁砞﹚牡埃矗ボ!!!");
    }
  }

  if (nAgBusyCount > g_nMaxBusyAgent && g_nMaxBusyAgent > 0)
  {
    if (nAgBusyCount > g_nAgBusyCount)
    {
      g_bBusyAgentSound = true;
      ckBusyAgentSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("坐席忙总数 %d 大于设定值 %d 警告提示!!!",
          nAgBusyCount, g_nMaxBusyAgent);
      else
        WriteAlartMsg("畒畊Γ魁羆计 %d 砞﹚ %d 牡矗ボ!!!",
          nAgBusyCount, g_nMaxBusyAgent);
    }
  }
  else
  {
    if (g_bBusyAgentSound == true)
    {
      g_bBusyAgentSound = false;
      ckBusyAgentSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("坐席忙总数超过设定值告警消除提示!!!");
      else
        WriteAlartMsg("畒畊Γ魁羆计禬筁砞﹚牡埃矗ボ!!!");
    }
  }

  g_nAgLoginCount = nAgLoginCount;
  g_nAgLogoutCount = nAgLogoutCount;
  g_nAgIdelCount = nAgIdelCount;
  g_nAgLeavelCount = nAgLeavelCount;
  g_nAgBusyCount = nAgBusyCount;
}
void TFormMain::DispAcdStatus(CXMLMsg &XMLMsg)
{
  int nItemNum1, nItemNum2;
  TListItem  *ListItem1;

  lvAcdStatus->Items->Clear();
  lbAcdWaitNum->Caption = "0";

  nItemNum1 = MySplit(XMLMsg.GetAttrValue(2).C_Str(), ';', pstrAcdList1);
  if (nItemNum1 < 1)
    return;
  int nAcdWaitNum = atoi(XMLMsg.GetAttrValue(1).C_Str());
  lbAcdWaitNum->Caption = nAcdWaitNum;
  for (int i=0; i<nItemNum1; i++)
  {
    nItemNum2 = MySplit(pstrAcdList1->Strings[i].c_str(), ',', pstrAcdList2);
    if (nItemNum2>=5)
    {
      ListItem1 = lvAcdStatus->Items->Add();
      ListItem1->Caption = IntToStr(i);
      ListItem1->SubItems->Insert(0, pstrAcdList2->Strings[1]);
      ListItem1->SubItems->Insert(1, pstrAcdList2->Strings[2]);
      switch (atoi(pstrAcdList2->Strings[3].c_str()))
      {
      case 1:
        ListItem1->SubItems->Insert(2, (gLangID == 1) ? "正在呼叫坐席" : "タ挤畒畊");
        break;
      case 2:
        ListItem1->SubItems->Insert(2, (gLangID == 1) ? "坐席正在振铃" : "畒畊タ筧");
        break;
      default:
        ListItem1->SubItems->Insert(2, (gLangID == 1) ? "等待分配坐席" : "单だ皌畒畊");
        break;
      }
      ListItem1->SubItems->Insert(3, pstrAcdList2->Strings[4]);
      ListItem1->ImageIndex = 4;
    }
  }
  if (nAcdWaitNum > g_nMaxACDWaiting && g_nMaxACDWaiting > 0)
  {
    if (nAcdWaitNum > g_nAcdWaitNum)
    {
      g_bACDWaitingSound = true;
      ckACDWaitingSound->Color = clRed;
      if (gLangID == 1)
        WriteAlartMsg("ACD等待数 %d 超过设定值 %d 警告提示!!!",
          nAcdWaitNum, g_nMaxACDWaiting);
      else
        WriteAlartMsg("ACD单计 %d 禬筁砞﹚ %d 牡矗ボ!!!",
          nAcdWaitNum, g_nMaxACDWaiting);
    }
  }
  else
  {
    if (g_bACDWaitingSound == true)
    {
      g_bACDWaitingSound = false;
      ckACDWaitingSound->Color = clSkyBlue;
      if (gLangID == 1)
        WriteAlartMsg("ACD等待数超过设定值告警消除提示!!!");
      else
        WriteAlartMsg("ACD单计禬筁砞﹚牡埃矗ボ!!!");
    }
  }
  g_nAcdWaitNum = nAcdWaitNum;
}
void TFormMain::DispLoadFlw(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int flwno, ItemCount;

  flwno = atoi(XMLMsg.GetAttrValue(1).C_Str());
  ItemCount = lvLoadFlw->Items->Count;
  for (int i = 0; i < ItemCount; i ++)
  {
    ListItem1 = lvLoadFlw->Items->Item[i];
    if (ListItem1->Caption == IntToStr(flwno))
    {
      ListItem1->SubItems->Strings[0] = (char *)(XMLMsg.GetAttrValue(2).C_Str());
      ListItem1->SubItems->Strings[1] = (char *)(XMLMsg.GetAttrValue(3).C_Str());
      ListItem1->SubItems->Strings[2] = (char *)(XMLMsg.GetAttrValue(4).C_Str());
      ListItem1->SubItems->Strings[3] = (char *)(XMLMsg.GetAttrValue(5).C_Str());
      ListItem1->SubItems->Strings[4] = (char *)(XMLMsg.GetAttrValue(6).C_Str());
      ListItem1->SubItems->Strings[5] = (char *)(XMLMsg.GetAttrValue(7).C_Str());
      return;
    }
  }

  ListItem1 = lvLoadFlw->Items->Add();
  ListItem1->Caption = IntToStr(flwno);
  ListItem1->SubItems->Insert( 0, (char *)(XMLMsg.GetAttrValue(2).C_Str()));
  ListItem1->SubItems->Insert( 1, (char *)(XMLMsg.GetAttrValue(3).C_Str()));
  ListItem1->SubItems->Insert( 2, (char *)(XMLMsg.GetAttrValue(4).C_Str()));
  ListItem1->SubItems->Insert( 3, (char *)(XMLMsg.GetAttrValue(5).C_Str()));
  ListItem1->SubItems->Insert( 4, (char *)(XMLMsg.GetAttrValue(6).C_Str()));
  ListItem1->SubItems->Insert( 5, (char *)(XMLMsg.GetAttrValue(7).C_Str()));
  ListItem1->SubItems->Insert( 6, 0);
  ListItem1->SubItems->Insert( 7, "");
}
void TFormMain::DispSessionsCount(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int flwno, ItemCount, autoid;

  flwno = atoi(XMLMsg.GetAttrValue(1).C_Str());
  if (flwno == 65535)
  {
    editSessions->Text = atoi(XMLMsg.GetAttrValue(2).C_Str());
    return;
  }
  ItemCount = lvLoadFlw->Items->Count;
  for (int i = 0; i < ItemCount; i ++)
  {
    ListItem1 = lvLoadFlw->Items->Item[i];
    if (ListItem1->Caption == IntToStr(flwno))
    {
      ListItem1->SubItems->Strings[6] = (char *)(XMLMsg.GetAttrValue(2).C_Str());
      autoid = atoi(XMLMsg.GetAttrValue(3).C_Str());
      if (autoid == 0)
        ListItem1->SubItems->Strings[7] = "";
      else
        ListItem1->SubItems->Strings[7] = (gLangID == 1) ? "已启动" : "币笆";
      break;
    }
  }
}
void TFormMain::DispHARunStatus(CXMLMsg &XMLMsg)
{
  AnsiString strTemp;
  int hosttype = atoi(XMLMsg.GetAttrValue(1).C_Str());
  int activeid = atoi(XMLMsg.GetAttrValue(2).C_Str());

  if (hosttype == 0)
  {
    if (activeid == 1)
    {
      strTemp = (gLangID == 1) ? "主机状态: 运行,切换时间: " : "诀篈: 笲︽,ち传丁: ";
      lbSwitchover->Caption = strTemp + (char *)XMLMsg.GetAttrValue(3).C_Str();
      FormMain->imgIVRServiceStatus->Picture->LoadFromFile(imgStartFile);
    }
    else
    {
      strTemp = (gLangID == 1) ? "主机状态: 待机,切换时间: " : "诀篈: 诀,ち传丁: ";
      lbSwitchover->Caption = strTemp + (char *)XMLMsg.GetAttrValue(3).C_Str();
      FormMain->imgIVRServiceStatus->Picture->LoadFromFile(imgYellowFile);
    }
  }
  else
  {
    if (activeid == 1)
    {
      strTemp = (gLangID == 1) ? "备机状态：运行,切换时间: " : "称诀篈笲︽,ち传丁: ";
      lbSwitchover->Caption = strTemp + (char *)XMLMsg.GetAttrValue(3).C_Str();
      FormMain->imgIVRServiceStatus->Picture->LoadFromFile(imgStartFile);
    }
    else
    {
      strTemp = (gLangID == 1) ? "备机状态：待机,切换时间: " : "称诀篈诀,ち传丁: ";
      lbSwitchover->Caption = strTemp + (char *)XMLMsg.GetAttrValue(3).C_Str();
      FormMain->imgIVRServiceStatus->Picture->LoadFromFile(imgYellowFile);
    }
  }
  lbSwitchover->Visible = true;
}
void TFormMain::DispMsg(TMemo *Memo, const char *msg)
{
  if (Memo->Lines->Count > 1000)
  {
    Memo->Lines->Clear();
  }
  Memo->Lines->Add((char *)msg);
}
void TFormMain::SaveMsg(const char *filename, const char *msg)
{
  FILE *LogFile;

  LogFile = fopen( filename, "a+t");
  if (LogFile != NULL)
  {
    fprintf(LogFile, "%s\n", msg);
    fclose(LogFile);
  }
}
void TFormMain::ProcIVRMsg(int msgtype, const char *msg)
{
  Word Year, Month, Day, Hour, Min, Sec, MSec;
  char filename[256];
  
  if (ckSaveIVRMsg->Checked == true || msgtype == 0)
  {
    DecodeDate(Now(), Year, Month, Day );
    DecodeTime(Now(), Hour, Min, Sec, MSec );
    sprintf(filename, "%s\\ivrmsg\\IVR_%04d%02d%02d%02d%d.log", g_strExecPath.c_str(), Year, Month, Day, Hour, Min/10);
    SaveMsg(filename, msg);
  }
  if (ckDispIVRMsg->Checked == true || msgtype == 0)
  {
    DispMsg(memoIVRTrace, msg);
  }
}
void TFormMain::ProcFLWMsg(int msgtype, const char *msg)
{
  Word Year, Month, Day, Hour, Min, Sec, MSec;
  char filename[256];

  if (ckSaveFLWMsg->Checked == true || msgtype == 0)
  {
    DecodeDate(Now(), Year, Month, Day );
    DecodeTime(Now(), Hour, Min, Sec, MSec );
    sprintf(filename, "%s\\flwmsg\\FLW_%04d%02d%02d%02d%d.log", g_strExecPath.c_str(), Year, Month, Day, Hour, Min/10);
    SaveMsg(filename, msg);
  }
  if (ckDispFLWMsg->Checked == true || msgtype == 0)
  {
    DispMsg(memoFLWTrace, msg);
  }
}
//---------------------------------------------------------------------------
void TFormMain::SetlvParamSelImage1(int nSelIndex)
{
  lvParamSel1->Items->Item[0]->ImageIndex = 1;
  lvParamSel1->Items->Item[1]->ImageIndex = 2;
  lvParamSel1->Items->Item[nSelIndex]->ImageIndex = 0;
}
void TFormMain::SetlvParamSelImage2(int nSelIndex)
{
  lvParamSel2->Items->Item[0]->ImageIndex = 3;
  lvParamSel2->Items->Item[1]->ImageIndex = 4;
  lvParamSel2->Items->Item[2]->ImageIndex = 5;
  lvParamSel2->Items->Item[3]->ImageIndex = 6;
  lvParamSel2->Items->Item[4]->ImageIndex = 7;
  lvParamSel2->Items->Item[5]->ImageIndex = 8;
  lvParamSel2->Items->Item[6]->ImageIndex = 9;
  lvParamSel2->Items->Item[nSelIndex]->ImageIndex = 0;
}
void TFormMain::SetlvParamSelImage3(int nSelIndex)
{
  lvParamSel3->Items->Item[0]->ImageIndex = 10;
  lvParamSel3->Items->Item[1]->ImageIndex = 11;
  lvParamSel3->Items->Item[2]->ImageIndex = 12;
  lvParamSel3->Items->Item[3]->ImageIndex = 14;
  lvParamSel3->Items->Item[nSelIndex]->ImageIndex = 0;
}
void TFormMain::SetlvParamSelImage4(int nSelIndex)
{
  lvParamSel4->Items->Item[0]->ImageIndex = 13;
  lvParamSel4->Items->Item[1]->ImageIndex = 15;
  lvParamSel4->Items->Item[nSelIndex]->ImageIndex = 0;
}
void TFormMain::SetlvParamSelImage5(int nSelIndex)
{
  lvParamSel5->Items->Item[0]->ImageIndex = 16;
  lvParamSel5->Items->Item[1]->ImageIndex = 17;
  lvParamSel5->Items->Item[nSelIndex]->ImageIndex = 0;
}
//---------------------------------------------------------------------------
void TFormMain::AddDispFlw(CFlwFile FlwFile)
{
  TListItem  *ListItem1;

  ListItem1 = lvFlw->Items->Add();
  ListItem1->Caption = IntToStr(FlwFile.FlwNo);
  ListItem1->SubItems->Insert(0, FlwFile.Explain);
  ListItem1->SubItems->Insert(1, FlwFile.FlwFile);
  ListItem1->SubItems->Insert(2, IntToStr(FlwFile.FuncNo));
  if (FlwFile.RunAfterLoad == 0)
    ListItem1->SubItems->Insert(3, (gLangID == 1) ? "电话" : "筿杠");
  else
    ListItem1->SubItems->Insert(3, (gLangID == 1) ? "自动" : "笆");
  ListItem1->SubItems->Insert(4, IntToStr(FlwFile.AccessNum));
}
void TFormMain::DispFlw()
{
  lvFlw->Items->Clear();
  for (int flwno=0; flwno<FlwFileList.FlwNum; flwno++)
  {
    AddDispFlw(FlwFileList.FlwFile[flwno]);
  }
}
void TFormMain::AddDispAccCode(CAccessCode AccessCode)
{
  TListItem  *ListItem1;

  ListItem1 = lvAccCode->Items->Add();
  ListItem1->Caption = IntToStr(AccessCode.AccNo);
  ListItem1->SubItems->Insert(0, AccessCode.PreCalled);
  ListItem1->SubItems->Insert(1, IntToStr(AccessCode.MinLen));
  ListItem1->SubItems->Insert(2, IntToStr(AccessCode.MaxLen));
}
void TFormMain::DispAccCode(CFlwFile FlwFile)
{
  lvAccCode->Items->Clear();
  for (int accno=0; accno<FlwFile.AccessNum; accno++)
  {
    AddDispAccCode(FlwFile.AccessCode[accno]);
  }
}

void __fastcall TFormMain::btCancelClick(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
void TFormMain::ReLoadIVRIniFile()
{
  ReadTcpParam();
  ReadPSTNParam();
  ReadRouteParam();
  ReadSeatParam();
  ReadExtSeatParam();
  ReadOtherParam();
  ReadAlartParam();
  ReadVopParam();

  bTCPParamChange = false;
  bPSTNParamChange = false;
  bROUTEParamChange = false;
  bSeatParamChange = false;
  bExtSeatParamChange = false;
  bOtherParamChange = false;
  bAlartParamChange = false;
  bVopParamChange = false;

  btApply->Enabled = false;
}
void TFormMain::ReLoadCTILINKIniFile()
{
  ReadSwitchPortParam();
  bSwitchPortParamChange = false;
}
void TFormMain::ReLoadFLWIniFile()
{
  ReadFlwParam();
  bFLWParamChange = false;

  btApply->Enabled = false;
}
void TFormMain::ReLoadDBIniFile()
{
  ReadTcpParam();
  bTCPParamChange = false;

  btApply->Enabled = false;
}
void TFormMain::ReLoadIP()
{
  GetServerIp();
}
void TFormMain::ReLoadGW()
{
  GetServerGw();
}
void TFormMain::ReLoadDNS()
{
  GetServerDns();
}
void __fastcall TFormMain::FormCreate(TObject *Sender)
{
  char szFull[256];
  char drive[MAXDRIVE];
  char dir[MAXDIR];
  char file[MAXFILE];
  char ext[MAXEXT];
  TListItem  *ListItem1;

  GetModuleFileName(NULL, szFull, 256);
  fnsplit(szFull,drive,dir,file,ext);
  strcpy(szFull, drive);
  strcat(szFull, dir);

  g_strExecPath = (char *)szFull;

  g_strUcommPath = g_strExecPath.UpperCase();
  int nPos = g_strUcommPath.AnsiPos("CONFIG");
  g_strUcommPath = g_strExecPath.SubString(1, nPos-1);

  if (gLangID == 1)
  {
    #ifdef QUARKCALL_PLATFORM
    FormMain->Caption = "QuarkCall快客呼配置监控器";
    //ListItem1 = lvParamSel1->Items->Item[0];
    //ListItem1->Caption = "祅厚硈钡把计";
    //ListItem1 = lvParamSel1->Items->Item[1];
    //ListItem1->Caption = "狝叭笲︽篈";
    #else
    FormMain->Caption = "Unime平台配置监控器";
    #endif
  }
  else
  {
    #ifdef QUARKCALL_PLATFORM
    FormMain->Caption = "QuarkCallе㊣皌竚菏北竟";
    #else
    FormMain->Caption = "Unimeキ籓皌竚菏北竟";
    #endif
  }
  Notebook1->PageIndex = 0;

  m_MyToolsBox.m_pSpeedButton[0] = BitBtn1;
  m_MyToolsBox.m_pSpeedButton[1] = BitBtn2;
  m_MyToolsBox.m_pSpeedButton[2] = BitBtn3;
  m_MyToolsBox.m_pSpeedButton[3] = BitBtn4;
  m_MyToolsBox.m_pSpeedButton[4] = BitBtn5;
  m_MyToolsBox.m_nSpeedButtonNum = 5;
  m_MyToolsBox.m_pToolsBoxPanel = plToolsBoxLeft;
  m_MyToolsBox.m_pToolsBoxNotebook = ntbkToolsBox;

  pstrChnList = new TStringList;
  pstrAgList = new TStringList;
  pstrAcdList1 = new TStringList;
  pstrAcdList2 = new TStringList;
  pstrTrkCountList = new TStringList;
  pstrAgCountList = new TStringList;

  LOGINIFile = ExtractFilePath(Application->ExeName)+"config.ini";
  imgRedFile = ExtractFilePath(Application->ExeName)+"red.ico";
  imgBlueFile = ExtractFilePath(Application->ExeName)+"blue.ico";
  imgYellowFile = ExtractFilePath(Application->ExeName)+"Yellow.ico";
  imgGreenFile = ExtractFilePath(Application->ExeName)+"green.ico";
  imgStartFile = ExtractFilePath(Application->ExeName)+"start.ico";
  imgStopFile = ExtractFilePath(Application->ExeName)+"stop.ico";

  ReadLOGParam();
  if (strAPPTitle != "")
  {
    FormMain->Caption = strAPPTitle;
  }
  else
  {
    strAPPTitle = FormMain->Caption;
  }
  Label43->Caption = strAPPTitle;

  ReadWorkerCountItemSet();
  ReadGroupCountItemSet();
  DispWorkerCountItemSet();
  DispGroupCountItemSet();
  DispGroupStatusCountItemSet();

  LoadTAPIDll();

  strTSPList = new TStringList;
  //GetLoadedTSPList();

  g_pFlwRule = new CFlwRule;
  if (g_pFlwRule == NULL)
  {
    MessageBox(NULL,"Create path=FlwRule fail!!!","warning",MB_OK|MB_ICONWARNING);
  }
  g_pMsgfifo = new CMsgfifo(MAX_MSG_QUEUE_NUM);

  IVRIniKey = NULL;
  FLWIniKey = NULL;
  DBIniKey = NULL;

  InitTrkCountDispData(2);
  InitAgCountDispData(2);

  if (!DirectoryExists( g_strExecPath + "\\ivrmsg"))
  {
      if (!CreateDir(g_strExecPath + "\\ivrmsg"))
      {
          MessageBox(NULL,"Create path=ivrmsg fail!!!","warning",MB_OK|MB_ICONWARNING);
      }
  }
  if (!DirectoryExists( g_strExecPath + "\\flwmsg"))
  {
      if (!CreateDir(g_strExecPath + "\\flwmsg"))
      {
          MessageBox(NULL,"Create path=flwmsg fail!!!","warning",MB_OK|MB_ICONWARNING);
      }
  }
  if (!DirectoryExists( g_strExecPath + "\\INITEMP"))
  {
      if (!CreateDir(g_strExecPath + "\\INITEMP"))
      {
          MessageBox(NULL,"Create path=flwmsg fail!!!","warning",MB_OK|MB_ICONWARNING);
      }
  }
  if (!DirectoryExists( g_strExecPath + "\\ALART"))
  {
      if (!CreateDir(g_strExecPath + "\\ALART"))
      {
          MessageBox(NULL,"Create path=ALART fail!!!","warning",MB_OK|MB_ICONWARNING);
      }
  }

  if (g_nRunMode == 1)
  {
    if (g_bRunOnServicePCId == true)
    {
      ReLoadCTILINKIniFile();
      ReLoadIVRIniFile();
      ReLoadFLWIniFile();
      ReLoadDBIniFile();
    }

    SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
    if (scm != NULL)
    {
      SC_HANDLE svc1 = OpenService(scm, IVR_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc1!=NULL)
      {
        btInstallIVRservice->Visible = false;
        SERVICE_STATUS ServiceStatus1;
        QueryServiceStatus(svc1, &ServiceStatus1);
        if (ServiceStatus1.dwCurrentState == SERVICE_RUNNING)
        {
          imgIVRServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartIVRService->Enabled = false;
          btStopIVRService->Enabled = true;
        }
        else
        {
          imgIVRServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartIVRService->Enabled = true;
          btStopIVRService->Enabled = false;
        }
        CloseServiceHandle(svc1);
      }
      else
      {
        imgIVRServiceStatus->Visible = false;
        btStartIVRService->Enabled = false;
        btStopIVRService->Enabled = false;
        btInstallIVRservice->Visible = true;
      }

      SC_HANDLE svc2 = OpenService(scm,FLW_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc2!=NULL)
      {
        btInstallFLWservice->Visible = false;
        SERVICE_STATUS ServiceStatus2;
        QueryServiceStatus(svc2, &ServiceStatus2);
        if (ServiceStatus2.dwCurrentState == SERVICE_RUNNING)
        {
          imgFLWServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartFLWService->Enabled = false;
          btStopFLWService->Enabled = true;
        }
        else
        {
          imgFLWServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartFLWService->Enabled = true;
          btStopFLWService->Enabled = false;
        }
        CloseServiceHandle(svc2);
      }
      else
      {
        imgFLWServiceStatus->Visible = false;
        btStartFLWService->Enabled = false;
        btStopFLWService->Enabled = false;
        btInstallFLWservice->Visible = true;
      }

      SC_HANDLE svc3 = OpenService(scm,DB_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc3!=NULL)
      {
        btInstallDBservice->Visible = false;
        SERVICE_STATUS ServiceStatus3;
        QueryServiceStatus(svc3, &ServiceStatus3);
        if (ServiceStatus3.dwCurrentState == SERVICE_RUNNING)
        {
          imgDBServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartDBService->Enabled = false;
          btStopDBService->Enabled = true;
        }
        else
        {
          imgDBServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartDBService->Enabled = true;
          btStopDBService->Enabled = false;
        }
        CloseServiceHandle(svc3);
      }
      else
      {
        imgDBServiceStatus->Visible = false;
        btStartDBService->Enabled = false;
        btStopDBService->Enabled = false;
        btInstallDBservice->Visible = true;
      }

      SC_HANDLE svc4 = OpenService(scm,SMS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc4!=NULL)
      {
        btInstallSMSservice->Visible = false;
        SERVICE_STATUS ServiceStatus4;
        QueryServiceStatus(svc4, &ServiceStatus4);
        if (ServiceStatus4.dwCurrentState == SERVICE_RUNNING)
        {
          imgSMSServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartSMSService->Enabled = false;
          btStopSMSService->Enabled = true;
        }
        else
        {
          imgSMSServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartSMSService->Enabled = true;
          btStopSMSService->Enabled = false;
        }
        CloseServiceHandle(svc4);
      }
      else
      {
        imgSMSServiceStatus->Visible = false;
        btStartSMSService->Enabled = false;
        btStopSMSService->Enabled = false;
        btInstallSMSservice->Visible = true;
      }

      SC_HANDLE svc5 = OpenService(scm,DOC_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc5!=NULL)
      {
        btInstallDOCservice->Visible = false;
        SERVICE_STATUS ServiceStatus5;
        QueryServiceStatus(svc5, &ServiceStatus5);
        if (ServiceStatus5.dwCurrentState == SERVICE_RUNNING)
        {
          imgDOCServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartDOCService->Enabled = false;
          btStopDOCService->Enabled = true;
        }
        else
        {
          imgDOCServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartDOCService->Enabled = true;
          btStopDOCService->Enabled = false;
        }
        CloseServiceHandle(svc5);
      }
      else
      {
        imgDOCServiceStatus->Visible = false;
        btStartDOCService->Enabled = false;
        btStopDOCService->Enabled = false;
        btInstallDOCservice->Visible = true;
      }

      SC_HANDLE svc6 = OpenService(scm,WEB_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc6!=NULL)
      {
        btInstallWEBservice->Visible = false;
        SERVICE_STATUS ServiceStatus6;
        QueryServiceStatus(svc6, &ServiceStatus6);
        if (ServiceStatus6.dwCurrentState == SERVICE_RUNNING)
        {
          imgWEBServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartWEBService->Enabled = false;
          btStopWEBService->Enabled = true;
        }
        else
        {
          imgWEBServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartWEBService->Enabled = true;
          btStopWEBService->Enabled = false;
        }
        CloseServiceHandle(svc6);
      }
      else
      {
        imgWEBServiceStatus->Visible = false;
        btStartWEBService->Enabled = false;
        btStopWEBService->Enabled = false;
        btInstallWEBservice->Visible = true;
      }

      SC_HANDLE svc7 = OpenService(scm,TTS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc7!=NULL)
      {
        btInstallTTSservice->Visible = false;
        SERVICE_STATUS ServiceStatus7;
        QueryServiceStatus(svc7, &ServiceStatus7);
        if (ServiceStatus7.dwCurrentState == SERVICE_RUNNING)
        {
          imgTTSServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartTTSService->Enabled = false;
          btStopTTSService->Enabled = true;
        }
        else
        {
          imgTTSServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartTTSService->Enabled = true;
          btStopTTSService->Enabled = false;
        }
        CloseServiceHandle(svc7);
      }
      else
      {
        imgTTSServiceStatus->Visible = false;
        btStartTTSService->Enabled = false;
        btStopTTSService->Enabled = false;
        btInstallTTSservice->Visible = true;
      }

      SC_HANDLE svc8 = OpenService(scm,CTILINK_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc8!=NULL)
      {
        btInstallLINKservice->Visible = false;
        SERVICE_STATUS ServiceStatus8;
        QueryServiceStatus(svc8, &ServiceStatus8);
        if (ServiceStatus8.dwCurrentState == SERVICE_RUNNING)
        {
          imgLINKServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartLINKService->Enabled = false;
          btStopLINKService->Enabled = true;
        }
        else
        {
          imgLINKServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartLINKService->Enabled = true;
          btStopLINKService->Enabled = false;
        }
        CloseServiceHandle(svc8);
      }
      else
      {
        imgLINKServiceStatus->Visible = false;
        btStartLINKService->Enabled = false;
        btStopLINKService->Enabled = false;
        btInstallLINKservice->Visible = true;
      }

      SC_HANDLE svc9 = OpenService(scm,VOC_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc9!=NULL)
      {
        btInstallVOCservice->Visible = false;
        SERVICE_STATUS ServiceStatus9;
        QueryServiceStatus(svc9, &ServiceStatus9);
        if (ServiceStatus9.dwCurrentState == SERVICE_RUNNING)
        {
          imgVOCServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartVOCService->Enabled = false;
          btStopVOCService->Enabled = true;
        }
        else
        {
          imgVOCServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartVOCService->Enabled = true;
          btStopVOCService->Enabled = false;
        }
        CloseServiceHandle(svc9);
      }
      else
      {
        imgVOCServiceStatus->Visible = false;
        btStartVOCService->Enabled = false;
        btStopVOCService->Enabled = false;
        btInstallVOCservice->Visible = true;
      }

      SC_HANDLE svc10 = OpenService(scm,WS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc10!=NULL)
      {
        btInstallWSservice->Visible = false;
        SERVICE_STATUS ServiceStatus10;
        QueryServiceStatus(svc10, &ServiceStatus10);
        if (ServiceStatus10.dwCurrentState == SERVICE_RUNNING)
        {
          imgWSServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartWSService->Enabled = false;
          btStopWSService->Enabled = true;
        }
        else
        {
          imgWSServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartWSService->Enabled = true;
          btStopWSService->Enabled = false;
        }
        CloseServiceHandle(svc10);
      }
      else
      {
        imgWSServiceStatus->Visible = false;
        btStartWSService->Enabled = false;
        btStopWSService->Enabled = false;
        btInstallWSservice->Visible = true;
      }

      CloseServiceHandle(scm);
    }
  }
  else if (g_nRunMode == 0)
  {
    btInstallIVRservice->Visible = false;
    btRefreshIVRServiceStatus->Visible = false;
    btStartIVRService->Visible = false;
    btStopIVRService->Visible = false;

    btInstallFLWservice->Visible = false;
    btRefreshFLWServiceStatus->Visible = false;
    btStartFLWService->Visible = false;
    btStopFLWService->Visible = false;

    btInstallDBservice->Visible = false;
    btRefreshDBServiceStatus->Visible = false;
    btStartDBService->Visible = false;
    btStopDBService->Visible = false;
  }
  else if (g_nRunMode == 2)
  {
    btInstallIVRservice->Visible = false;
    btRefreshIVRServiceStatus->Visible = false;
    btStartIVRService->Visible = true;
    btStopIVRService->Visible = true;

    btInstallFLWservice->Visible = false;
    btRefreshFLWServiceStatus->Visible = false;
    btStartFLWService->Visible = true;
    btStopFLWService->Visible = true;

    btInstallDBservice->Visible = false;
    btRefreshDBServiceStatus->Visible = false;
    btStartDBService->Visible = true;
    btStopDBService->Visible = true;
  }

  g_pTcpClient = new CTCPClient;
  if (g_pTcpClient != NULL)
  {
      g_pTcpClient->ReadIni(LOGINIFile.c_str());
      if (g_nRunMode == 2)
      {
        if (g_pTcpClient->ConnectCTRLServer() != 0)
        {
          if (gLangID == 1)
            MessageBox(NULL,"连接服务失败!!!","警告",MB_OK|MB_ICONWARNING);
          else
            MessageBox(NULL,"硈钡狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
        }
      }
      if (g_pTcpClient->ConnectServer() != 0)
      {
          if (gLangID == 1)
            MessageBox(NULL,"连接服务失败!!!","警告",MB_OK|MB_ICONWARNING);
          else
            MessageBox(NULL,"硈钡狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
  }
  else
  {
      if (gLangID == 1)
        MessageBox(NULL,"加载动态库失败!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"更笆篈畐ア毖!!!","牡",MB_OK|MB_ICONWARNING);
  }
  nCURPage = 0;
  bTCPParamChange = false;
  bIPParamChange = false;
  bFLWParamChange = false;
  bPSTNParamChange = false;
  bROUTEParamChange = false;
  bSeatParamChange = false;
  bExtSeatParamChange = false;
  bOtherParamChange = false;
  bSaveMsgIdParamChange = false;
  bLOGParamChange = false;
  bAlartParamChange = false;
  bSwitchPortParamChange = false;

  btApply->Enabled = false;

  Timer1->Enabled = true;
  m_MyToolsBox.RemoveToolsBox(0);
  Notebook1->PageIndex = 17;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btOKClick(TObject *Sender)
{
  if (g_nLoginId == 0)
  {
    this->Close();
    return;
  }
  if (bLOGParamChange == true)
    WriteLOGParam();
  if (bTCPParamChange == true)
    WriteTcpParam();
  if (bIPParamChange == true)
    WriteIPParam();
  if (bFLWParamChange == true)
    WriteFlwParam();
  if (bPSTNParamChange == true)
    WritePSTNParam();
  if (bROUTEParamChange == true)
    WriteRouteParam();
  if (bSeatParamChange == true)
    WriteSeatParam();
  if (bExtSeatParamChange == true)
    WriteExtSeatParam();
  if (bOtherParamChange == true)
    WriteOtherParam();
  if (bAlartParamChange == true)
    WriteAlartParam();
  if (bSwitchPortParamChange == true)
    WriteSwitchPortParam();
  if (bVopParamChange == true)
    WriteVopParam();
  btApply->Enabled = false;
  if (gLangID == 1)
  {
    if ( MessageBox( NULL, "所有的修改已保存，您需要退出配置监控程序吗?", "退出系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      this->Close();
  }
  else
  {
    if ( MessageBox( NULL, "┮Τэ纗眤惠璶癶皌竚菏北祘Α盾?", "癶╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      this->Close();
  }
}
//---------------------------------------------------------------------------
void TFormMain::ReadAuthData()
{
  char md5psw[256], AuthMd5Psw[256], szTemp[128], AuthEndDate[20], TempData[256];

  IVRIniKey = new TIniFile (IVRINIFile);
  //授权
  AnsiString strAuthData = IVRIniKey->ReadString("AUTHPSW", "AuthData", ""); //服务器序列号
  strAuthData = "imfromhengyang"+strAuthData;
  MDString(strAuthData.c_str(), md5psw);

  editIVRAuthKey->Text = IVRIniKey->ReadString("AUTHPSW", "SerialNo", "none"); //授权序列号
  strcpy(AuthMd5Psw, editIVRAuthKey->Text.c_str());

  if (strncmp(AuthMd5Psw, "@#$", 3) == 0)
  {
    strcpy(TempData, &AuthMd5Psw[3]);
    memset(AuthMd5Psw, 0, 256);

    int len = strlen(TempData);
    int n = len/2;
    int m = len%2;
    int i;
    for (i=0; i<n; i++)
    {
      AuthMd5Psw[i] = TempData[i*2];
      AuthMd5Psw[len-i-1] = TempData[i*2+1];
    }
    if (m == 1)
    {
      AuthMd5Psw[n] = TempData[len-1];
    }
  }

  if (strncmp(AuthMd5Psw, md5psw, 32) != 0)
  {
    editAuthKey->Text = (gLangID == 1) ? "未获得授权!!!" : "ゼ莉眔甭舦!!!";
  }
  else
  {
    if (strlen(AuthMd5Psw) >= 120)
    {
      strcpy(md5psw, &AuthMd5Psw[32]);

      memset(AuthMd5Psw, 0, 256);
      strcpy(AuthMd5Psw, DecodePass(md5psw));

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[2], 4);
      int AuthMaxTrunk = atoi(szTemp);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[7], 4);
      int AuthMaxSeat = atoi(szTemp);
      g_nAuthMaxSeat = AuthMaxSeat;

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[23], 4);
      int AuthMaxFax = atoi(szTemp);
      if (AuthMaxFax > 0 && g_nRunMode == 1)
        GroupBox9->Visible = true;

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[28], 4);
      int AuthMaxSms = atoi(szTemp);
      if (AuthMaxSms > 0 && g_nRunMode == 1)
        GroupBox8->Visible = true;

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[33], 4);
      int AuthMaxIVR = atoi(szTemp);
      g_nAuthMaxIVR = AuthMaxIVR;

      g_nAuthMaxCTIPort = g_nAuthMaxSeat+g_nAuthMaxIVR;

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[38], 4);
      int AuthMaxRECD = atoi(szTemp);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[43], 2);
      g_nAuthSwitchType = atoi(szTemp);

      memset(AuthEndDate, 0, 20);
      strncpy(AuthEndDate, &AuthMd5Psw[12], 10);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[46], 4);
      int AuthMaxTTS = atoi(szTemp);

      memset(szTemp, 0, 128);

      if (gLangID == 1)
        sprintf(szTemp, "交换机类型=%d;中继数=%d;坐席数=%d;IVR数=%d;监录数=%d;传真数=%d;短信数=%d;TTS数=%d;有效日期=%s",
          g_nAuthSwitchType, AuthMaxTrunk, AuthMaxSeat, AuthMaxIVR, AuthMaxRECD, AuthMaxFax, AuthMaxSms, AuthMaxTTS, AuthEndDate);
      else
        sprintf(szTemp, "ユ传诀摸=%d;い膥硄笵计=%d;畒畊硄笵计=%d;IVR硄笵计=%d;菏魁硄笵计=%d;肚痷硄笵计=%d;虏癟硄笵计=%d;TTS硄笵计=%d;Τら戳=%s",
          g_nAuthSwitchType, AuthMaxTrunk, AuthMaxSeat, AuthMaxIVR, AuthMaxRECD, AuthMaxFax, AuthMaxSms, AuthMaxTTS, AuthEndDate);

      editAuthKey->Text = (char *)szTemp;
    }
    else
    {
      editAuthKey->Text = (gLangID == 1) ? "授权序号非法!!!" : "甭舦腹獶猭!!!";
    }
  }

  delete IVRIniKey;
  btApply->Enabled = true;
}

void TFormMain::ReadTcpParam()
{
  TListItem  *ListItem1;
  char md5psw[256], AuthMd5Psw[256], szTemp[128], AuthEndDate[20];

  IVRIniKey = new TIniFile (IVRINIFile);

  g_strAdminPSW = DecodePass(IVRIniKey->ReadString("LOGUSER", "administrator", ""));
  g_strManagerPSW = DecodePass(IVRIniKey->ReadString("LOGUSER", "manager", ""));
  g_strOperatorPSW = DecodePass(IVRIniKey->ReadString("LOGUSER", "operator", ""));
  g_strMonitorPSW = DecodePass(IVRIniKey->ReadString("LOGUSER", "monitor", ""));

  g_nSwitchMode = IVRIniKey->ReadInteger("CONFIGURE", "SwitchMode", 0);
  switch (g_nSwitchMode)
  {
  case 1:
    cbSwitchType->Text = (gLangID == 1) ? "AVAYA-IPO系列" : "AVAYA-IPO╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];

    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = true;
    cbCTILinkType->Visible = true;
    btTAPI->Visible = true;
    btUnistallTAPI->Visible = true;
    lbSwitchIP->Visible = true;
    editSwitchIP->Visible = true;
    lvSwitchPort->PopupMenu = PopupMenu6;

    lbServerID->Visible = false;
    editServerID->Visible = false;
    lbLoginID->Visible = false;
    editLoginID->Visible = false;
    lbPassword->Visible = false;
    editPassword->Visible = false;
    break;
  case 2:
    cbSwitchType->Text = (gLangID == 1) ? "AVAYA-S系列" : "AVAYA-S╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = true;
    editServerID->Visible = true;
    lbLoginID->Visible = true;
    editLoginID->Visible = true;
    lbPassword->Visible = true;
    editPassword->Visible = true;
    break;
  case 3:
    cbSwitchType->Text = (gLangID == 1) ? "Alcatel-OXO系列" : "Alcatel-OXO╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = false;
    editServerID->Visible = false;
    lbLoginID->Visible = false;
    editLoginID->Visible = false;
    lbPassword->Visible = false;
    editPassword->Visible = false;
    break;
  case 4:
    cbSwitchType->Text = (gLangID == 1) ? "Alcatel-OXE系列" : "Alcatel-OXE╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = true;
    editServerID->Visible = true;
    lbLoginID->Visible = true;
    editLoginID->Visible = true;
    lbPassword->Visible = true;
    editPassword->Visible = true;
    break;
  case 5:
    cbSwitchType->Text = (gLangID == 1) ? "Simens系列" : "Simens╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = true;
    editServerID->Visible = true;
    lbLoginID->Visible = true;
    editLoginID->Visible = true;
    lbPassword->Visible = true;
    editPassword->Visible = true;
    break;
  case 6:
    cbSwitchType->Text = (gLangID == 1) ? "Panasonic系列" : "Panasonic╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = true;
    editServerID->Visible = true;
    lbLoginID->Visible = true;
    editLoginID->Visible = true;
    lbPassword->Visible = true;
    editPassword->Visible = true;
    break;
  case 12:
    cbSwitchType->Text = (gLangID == 1) ? "Cisco系列" : "Cisco╰";
    GroupBox12->Visible = true;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = true;
    editServerID->Visible = true;
    lbLoginID->Visible = true;
    editLoginID->Visible = true;
    lbPassword->Visible = true;
    editPassword->Visible = true;
    break;
  case 10:
    cbSwitchType->Text = (gLangID == 1) ? "普通PBX" : "炊硄PBX";
    GroupBox12->Visible = false;
    pageState->Pages[1]->TabVisible = true;
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = (gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计";
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = (gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵";

    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = false;
    editServerID->Visible = false;
    lbLoginID->Visible = false;
    editLoginID->Visible = false;
    lbPassword->Visible = false;
    editPassword->Visible = false;
    break;
  default:
    cbSwitchType->Text = (gLangID == 1) ? "无" : "礚";
    GroupBox12->Visible = false;
    pageState->Pages[1]->TabVisible = false;
    Notebook2->ActivePage = "BoardTrunk";
    Notebook3->ActivePage = "BoardRoute";
    break;
  }

  if (IVRIniKey->ReadInteger("CONFIGURE", "MultiRunMode", 0) == 0)
  {
    cbMultiRunMode->Text = (gLangID == 1) ? "单机运行" : "虫诀笲︽";
  }
  else
  {
    cbMultiRunMode->Text = (gLangID == 1) ? "双机热备" : "蛮诀荐称";
    if (IVRIniKey->ReadInteger("CONFIGURE", "HostType", 0) == 0)
      GroupBox2->Caption = (gLangID == 1) ? "CTI/IVR服务(主机)：" : "CTI/IVR狝叭(诀):";
    else
      GroupBox2->Caption = (gLangID == 1) ? "CTI/IVR服务(备机)：" : "CTI/IVR狝叭(称诀):";
  }
  if (IVRIniKey->ReadInteger("CONFIGURE", "HostType", 0) == 0)
  {
    cbHostType->Text = (gLangID == 1) ? "主机" : "诀";
    Label114->Visible = false;
    editMasterIP->Visible = false;
  }
  else
  {
    cbHostType->Text = (gLangID == 1) ? "备机" : "称诀";
    Label114->Visible = true;
    editMasterIP->Visible = true;
  }

  editLocaIVRIP->Text = IVRIniKey->ReadString("CONFIGURE", "LocaIVRIP", "");
  editMasterIP->Text = IVRIniKey->ReadString("IVRSERVER", "MASTERIVRServerIP", "127.0.0.1");

  int nCardType = IVRIniKey->ReadInteger("CONFIGURE", "CardType", 1);
  switch (nCardType)
  {
  case 0:
    cbCardType->Text = (gLangID == 1) ? "无" : "礚";
    break;
  case 1:
    cbCardType->Text = (gLangID == 1) ? "三汇" : "蹲";
    break;
  case 2:
    cbCardType->Text = "DIVA";
    break;
  case 3:
    cbCardType->Text = "NMS";
    break;
  case 4:
    cbCardType->Text = "DONJIN";
    break;
  case 5:
    cbCardType->Text = "CISCO";
    break;
  case 9:
    cbCardType->Text = (gLangID == 1) ? "声卡" : "羘";
    break;
  default:
    cbCardType->Text = (gLangID == 1) ? "三汇" : "蹲";
    break;
  }
  //授权
  AnsiString strAuthData = IVRIniKey->ReadString("AUTHPSW", "AuthData", ""); //服务器序列号
  strAuthData = "imfromhengyang"+strAuthData;
  MDString(strAuthData.c_str(), md5psw);

  editIVRAuthKey->Text = IVRIniKey->ReadString("AUTHPSW", "SerialNo", "none"); //授权序列号
  strcpy(AuthMd5Psw, editIVRAuthKey->Text.c_str());

  if (strncmp(AuthMd5Psw, md5psw, 32) != 0)
  {
    editAuthKey->Text = (gLangID == 1) ? "未获得授权!!!" : "ゼ莉眔甭舦!!!";
  }
  else
  {
    if (strlen(AuthMd5Psw) >= 120)
    {
      strcpy(md5psw, &AuthMd5Psw[32]);

      memset(AuthMd5Psw, 0, 256);
      strcpy(AuthMd5Psw, DecodePass(md5psw));

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[2], 4);
      int AuthMaxTrunk = atoi(szTemp);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[7], 4);
      int AuthMaxSeat = atoi(szTemp);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[23], 4);
      int AuthMaxFax = atoi(szTemp);
      if (AuthMaxFax > 0 && g_nRunMode == 1)
        GroupBox9->Visible = true;

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[28], 4);
      int AuthMaxSms = atoi(szTemp);
      if (AuthMaxSms > 0 && g_nRunMode == 1)
        GroupBox8->Visible = true;

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[33], 4);
      int AuthMaxIVR = atoi(szTemp);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[38], 4);
      int AuthMaxRECD = atoi(szTemp);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[43], 2);
      g_nAuthSwitchType = atoi(szTemp);

      memset(AuthEndDate, 0, 20);
      strncpy(AuthEndDate, &AuthMd5Psw[12], 10);

      memset(szTemp, 0, 128);
      strncpy(szTemp, &AuthMd5Psw[46], 4);
      int AuthMaxTTS = atoi(szTemp);

      g_nAuthMaxCTIPort = g_nAuthMaxSeat+g_nAuthMaxIVR;

      memset(szTemp, 0, 128);

      if (gLangID == 1)
        sprintf(szTemp, "中继数=%d;坐席数=%d;IVR数=%d;监录数=%d;传真数=%d;短信数=%d;TTS数=%d;有效日期=%s",
          AuthMaxTrunk, AuthMaxSeat, AuthMaxIVR, AuthMaxRECD, AuthMaxFax, AuthMaxSms, AuthMaxTTS, AuthEndDate);
      else
        sprintf(szTemp, "ユ传诀摸=%d い膥硄笵计=%d;畒畊硄笵计=%d;IVR硄笵计=%d;菏魁硄笵计=%d;肚痷硄笵计=%d;虏癟硄笵计=%d;TTS硄笵计=%d;Τら戳=%s",
          g_nAuthSwitchType, AuthMaxTrunk, AuthMaxSeat, AuthMaxIVR, AuthMaxRECD, AuthMaxFax, AuthMaxSms, AuthMaxTTS, AuthEndDate);

      editAuthKey->Text = (char *)szTemp;
    }
    else
    {
      editAuthKey->Text = (gLangID == 1) ? "授权序号非法!!!" : "甭舦腹獶猭!!!";
    }
  }

  delete IVRIniKey;

  FLWIniKey = new TIniFile (FLWINIFile);
  cseIVRPORT->Value = FLWIniKey->ReadInteger("IVRSERVER", "IVRServerPort", 5227);
  cseFLWPort->Value = FLWIniKey->ReadInteger("IVRSERVER", "FLWServerPort", 5229);
  cseDBPORT->Value = FLWIniKey->ReadInteger("DBSERVER", "DBServerPort", 5228);

  //cseGWID->Value = FLWIniKey->ReadInteger("DBGW1", "GWServerId", 0);
  //cseGWPORT->Value = FLWIniKey->ReadInteger("DBGW1", "GWServerPort", 5001);
  //editGWIP->Text = FLWIniKey->ReadString("DBGW1", "GWServerIP", "127.0.0.1");

  //editFLWAuthKey->Text = FLWIniKey->ReadString("AUTHPSW", "SerialNo", "none");
  delete FLWIniKey;

  DBIniKey = new TIniFile (DBINIFile);

  cbxDBType->Text = DBIniKey->ReadString ( "CONFIGURE", "DBTYPE", "MYSQL" );
  edtServer->Text = DBIniKey->ReadString ( "CONFIGURE", "DBSERVER", "127.0.0.1" );
  edtDB->Text = DBIniKey->ReadString ( "CONFIGURE", "DATABASE", "callcenterdb" );
  edtUSER->Text = DBIniKey->ReadString ( "CONFIGURE", "USERID", "root" );
  //if (g_nRunMode == 2)
    edtPSW->Text = DecodePass(DBIniKey->ReadString ( "CONFIGURE", "PASSWORD", "cGtvY2Jua2ZfcXRpaGFXbHBz" ));

  delete DBIniKey;

  TTSIniKey = new TIniFile (TTSINIFile);
  cbTTSType->Text = TTSIniKey->ReadString ( "TTS", "TTSType", "iFlyTTS" );
  delete TTSIniKey;
}
void TFormMain::WriteTcpParam()
{
  AnsiString strSourFileName, strDestFileName;

  if (g_nConnectIVRId > 1)
  {
      if (gLangID == 1)
        MessageBox(NULL,"请先停止CTI/IVR服务、CTI-LINK服务、TTS服务!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"叫氨ゎCTI/IVR狝叭CTI-LINK狝叭TTS狝叭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
  }

  IVRIniKey = new TIniFile (IVRINIFile);
  if (gLangID == 1)
  {
    if (cbSwitchType->Text == "AVAYA-IPO系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 1);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TAPI\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "AVAYA-S系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 2);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TSAPI\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Alcatel-OXO系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 3);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TAPIOXO\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Alcatel-OXE系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 4);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TSAPIOXE\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Simens系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 5);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\Simens\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Panasonic系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 6);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\Panasonic\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Cisco系列")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 12);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\Cisco\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "普通PBX")
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 10);
    else
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 0);

    if (cbMultiRunMode->Text == "单机运行")
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "MultiRunMode", 0);
    else
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "MultiRunMode", 1);

    if (cbHostType->Text == "主机")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "HostType", 0);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "IVRServerid", 1);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "MASTERIVRServerid", 1);
    }
    else
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "HostType", 1);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "IVRServerid", 2);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "MASTERIVRServerid", 1);
    }

    if (cbCardType->Text == "无")
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 0);
    else if (cbCardType->Text == "三汇")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 1);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\SANHUI\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "DIVA")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 2);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\DIVA\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "NMS")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 3);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\NMS\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "DONJIN")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 4);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\DONJIN\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "FREESWITCH")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 5);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\FREESWITCH\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "声卡")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 9);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\TEST\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
  }
  else
  {
    if (cbSwitchType->Text == "AVAYA-IPO╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 1);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TAPI\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "AVAYA-S╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 2);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TSAPI\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Alcatel-OXO╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 3);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TSAPIOXO\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Alcatel-OXE╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 4);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\TSAPIOXE\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Simens╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 5);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\Simens\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Panasonic╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 6);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\Panasonic\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "Cisco╰")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 12);
      strSourFileName = g_strUcommPath+"CTILINKSERVICE\\Cisco\\CTILink.dll";
      strDestFileName = g_strUcommPath+"CTILINKSERVICE\\CTILink.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbSwitchType->Text == "炊硄PBX")
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 10);
    else
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "SwitchMode", 0);

    if (cbMultiRunMode->Text == "虫诀笲︽")
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "MultiRunMode", 0);
    else
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "MultiRunMode", 1);

    if (cbHostType->Text == "诀")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "HostType", 0);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "IVRServerid", 1);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "MASTERIVRServerid", 1);
    }
    else
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "HostType", 1);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "IVRServerid", 2);
      WriteIniParam(1, IVRIniKey, "IVRSERVER", "MASTERIVRServerid", 1);
    }

    if (cbCardType->Text == "礚")
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 0);
    else if (cbCardType->Text == "蹲")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 1);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\SANHUI\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "DIVA")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 2);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\DIVA\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "NMS")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 3);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\NMS\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "DONJIN")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 4);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\DONJIN\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "FREESWITCH")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 5);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\FREESWITCH\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
    else if (cbCardType->Text == "羘")
    {
      WriteIniParam(1, IVRIniKey, "CONFIGURE", "CardType", 9);
      strSourFileName = g_strUcommPath+"IVRSERVICE\\CARDDRV\\TEST\\ivrdrvdll.dll";
      strDestFileName = g_strUcommPath+"IVRSERVICE\\ivrdrvdll.dll";
      CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    }
  }

  WriteIniParam(1, IVRIniKey,"CONFIGURE", "LocaIVRIP", editLocaIVRIP->Text);

  WriteIniParam(1, IVRIniKey, "IVRSERVER", "IVRServerPort", cseIVRPORT->Value);
  WriteIniParam(1, IVRIniKey,"IVRSERVER", "IVRServerIP", editLocaIVRIP->Text);

  WriteIniParam(1, IVRIniKey, "IVRSERVER", "MASTERIVRServerPort", cseIVRPORT->Value);
  WriteIniParam(1, IVRIniKey,"IVRSERVER", "MASTERIVRServerIP", editMasterIP->Text);

  WriteIniParam(1, IVRIniKey,"AUTHPSW", "SerialNo", editIVRAuthKey->Text);
  delete IVRIniKey;

  FLWIniKey = new TIniFile (FLWINIFile);
  WriteIniParam(2, FLWIniKey, "IVRSERVER", "IVRServerPort", cseIVRPORT->Value);
  //WriteIniParam(2, FLWIniKey, "IVRSERVER", "IVRServerIP", editIVRIP->Text);
  WriteIniParam(2, FLWIniKey, "IVRSERVER", "FLWServerPort", cseFLWPort->Value);
  //WriteIniParam(2, FLWIniKey, "IVRSERVER", "FLWServerIP", editIVRIP->Text);

  //WriteIniParam(2, FLWIniKey, "DBSERVER", "DBServerID", 1);
  WriteIniParam(2, FLWIniKey, "DBSERVER", "DBServerPort", cseDBPORT->Value);
  //WriteIniParam(2, FLWIniKey, "DBSERVER", "DBServerIP", editIVRIP->Text);

  //WriteIniParam(2, FLWIniKey, "DBGW1", "GWServerId", cseGWID->Value);
  //WriteIniParam(2, FLWIniKey, "DBGW1", "GWServerPort", cseGWPORT->Value);
  //WriteIniParam(2, FLWIniKey, "DBGW1", "GWServerIP", editGWIP->Text);

  //WriteIniParam(2, FLWIniKey, "AUTHPSW", "SerialNo", editFLWAuthKey->Text);
  delete FLWIniKey;

  DBIniKey = new TIniFile (DBINIFile);

  WriteIniParam(3, DBIniKey, "CONFIGURE", "ServerPort", cseDBPORT->Value);

  //if (g_nRunMode == 2)
  {
    WriteIniParam(3, DBIniKey, "CONFIGURE", "DBTYPE", cbxDBType->Text );
    WriteIniParam(3, DBIniKey, "CONFIGURE", "DBSERVER", edtServer->Text );
    WriteIniParam(3, DBIniKey, "CONFIGURE", "DATABASE", edtDB->Text );
    WriteIniParam(3, DBIniKey, "CONFIGURE", "USERID", edtUSER->Text );
    WriteIniParam(3, DBIniKey, "CONFIGURE", "PASSWORD", EncodePass(edtPSW->Text));
  }

  delete DBIniKey;

  TTSIniKey = new TIniFile (TTSINIFile);

  TTSIniKey->WriteString("TTS", "TTSType", cbTTSType->Text);

  if (cbTTSType->Text == "iFlyTTS")
  {
    strSourFileName = g_strUcommPath+"TTSSERVICE\\iFlyTTS\\iFlyTTS.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\iFlyTTS.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);

    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTS_Audio.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTS_Audio.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\JTTS_MA.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\JTTS_MA.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTS_ML.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTS_ML.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTSSAPI5.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTSSAPI5.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTSSapi.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTSSapi.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);

    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\Key.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\Key.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\libBase.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\libBase.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\libtclchp8.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\libtclchp8.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\libTts.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\libTts.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\Sh_TTSU.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\Sh_TTSU.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
  }
  else if (cbTTSType->Text == "jTTS")
  {
    strSourFileName = g_strUcommPath+"TTSSERVICE\\iFlyTTS\\iFlyTTS.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\iFlyTTS.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);

    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS_Audio.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\JTTS_MA.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS_ML.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTSSAPI5.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTSSapi.dll";
    DeleteFile(strSourFileName.c_str());

    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\Key.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\Key.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\libBase.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\libBase.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\libtclchp8.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\libtclchp8.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\libTts.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\libTts.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\shTTS\\Sh_TTSU.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\Sh_TTSU.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
  }
  else if (cbTTSType->Text == "shTTS")
  {
    strSourFileName = g_strUcommPath+"TTSSERVICE\\iFlyTTS\\iFlyTTS.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\iFlyTTS.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);

    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTS_Audio.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTS_Audio.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\JTTS_MA.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\JTTS_MA.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTS_ML.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTS_ML.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTSSAPI5.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTSSAPI5.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);
    strSourFileName = g_strUcommPath+"TTSSERVICE\\jTTS\\jTTSSapi.dll";
    strDestFileName = g_strUcommPath+"TTSSERVICE\\jTTSSapi.dll";
    CopyFile(strSourFileName.c_str(), strDestFileName.c_str(), false);

    strSourFileName = g_strUcommPath+"TTSSERVICE\\Key.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\libBase.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\libtclchp8.1.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\libTts.dll";
    DeleteFile(strSourFileName.c_str());
    strSourFileName = g_strUcommPath+"TTSSERVICE\\Sh_TTSU.dll";
    DeleteFile(strSourFileName.c_str());
  }

  delete TTSIniKey;

  bTCPParamChange = false;
  btApply->Enabled = false;
}

void __fastcall TFormMain::cseIVRPORTChange(TObject *Sender)
{
  bTCPParamChange = true;
  btApply->Enabled = true;
}

//0-DEVICE= 1-ONBOOT= 2-BOOTPROTO= 3-HWADDR= 4-IPADDR= 5-NETMASK= 6-GATEWAY=
void TFormMain::WriteIPParam()
{
  char szMsg[256];

  if (g_nRunMode != 2)
  {
    return;
  }
  if (rbStaticIP->Checked == true)
  {
    if (CheckIPv4Address(editIVRIP->Text.c_str()) != 0)
    {
      if (gLangID == 1)
        MessageBox(NULL,"服务器IP地址非法!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"叭竟IP獶猭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (CheckIPv4Address(editMark->Text.c_str()) != 0)
    {
      if (gLangID == 1)
        MessageBox(NULL,"子网掩码IP地址非法!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"呼被絏IP獶猭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (CheckIPv4Address(editGWIP->Text.c_str()) != 0)
    {
      if (gLangID == 1)
        MessageBox(NULL,"默认网关IP地址非法!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"箇砞筯笵竟IP獶猭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
  }
  if (rbStaticDNS->Checked == true)
  {
    if (CheckIPv4Address(editDNS1->Text.c_str()) != 0)
    {
      if (gLangID == 1)
        MessageBox(NULL,"首选DNS服务器IP地址非法!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"匡DNS狝竟IP獶猭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (CheckIPv4Address(editDNS2->Text.c_str()) != 0)
    {
      if (gLangID == 1)
        MessageBox(NULL,"备用DNS服务器IP地址非法!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"称ノDNS狝竟IP獶猭!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
  }

  SendCTRLMsg(1, "start write ifcfg-eth0");

  sprintf(szMsg, "DEVICE=%s", strServerIp[0].c_str());
  SendCTRLMsg(2, szMsg);

  sprintf(szMsg, "ONBOOT=yes");
  SendCTRLMsg(2, szMsg);

  if (rbAutoGetIP->Checked == true)
  {
    strServerIp[1] = "dhcp";
    sprintf(szMsg, "BOOTPROTO=dhcp");
  }
  else
  {
    strServerIp[1] = "static";
    sprintf(szMsg, "BOOTPROTO=static");
  }
  SendCTRLMsg(2, szMsg);

  sprintf(szMsg, "HWADDR=%s", strServerIp[3].c_str());
  SendCTRLMsg(2, szMsg);

  if (rbStaticIP->Checked == true)
  {
    strServerIp[4] = editIVRIP->Text;
    sprintf(szMsg, "IPADDR=%s", strServerIp[4].c_str());
    SendCTRLMsg(2, szMsg);

    strServerIp[5] = editMark->Text;
    sprintf(szMsg, "NETMASK=%s", strServerIp[5].c_str());
    SendCTRLMsg(2, szMsg);

    strServerIp[6] = editGWIP->Text;
    sprintf(szMsg, "GATEWAY=%s", strServerIp[6].c_str());
    SendCTRLMsg(2, szMsg);
  }

  //----------
  SendCTRLMsg(5, "start write resolv.conf");

  if (rbStaticDNS->Checked == true)
  {
    strServerDns[0] = editDNS1->Text;
    sprintf(szMsg, "nameserver %s", strServerDns[0].c_str());
    SendCTRLMsg(6, szMsg);

    strServerDns[1] = editDNS2->Text;
    sprintf(szMsg, "nameserver %s", strServerDns[1].c_str());
    SendCTRLMsg(6, szMsg);
  }

  bIPParamChange = false;
  btApply->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btApplyClick(TObject *Sender)
{
  //if (Notebook1->PageIndex > 1)
  //  return;
  switch (Notebook1->PageIndex)
  {
  case 0: //监控端参数
    WriteLOGParam();
    break;
  case 1: //
    WriteSaveMsgId();
    break;
  case 2: //网络参数
    if (bTCPParamChange == true)
      WriteTcpParam();
    if (bIPParamChange == true)
      WriteIPParam();
    break;
  case 3: //流程参数
    WriteFlwParam();
    break;
  case 4: //模拟外线参数
    if (g_nSwitchMode == 0)
      WritePSTNParam();
    else
      WriteSwitchPortParam();
    break;
  case 5: //呼出路由参数
    if (g_nSwitchMode == 0)
      WriteRouteParam();
    else
      WriteVopParam();
    break;
  case 6: //坐席参数
    WriteSeatParam();
    break;
  case 7: //远端坐席参数
    WriteExtSeatParam();
    break;
  case 8: //其它参数
    WriteOtherParam();
    break;
  }
}
//---------------------------------------------------------------------------
void TFormMain::ReadFlwParam()
{
  AnsiString flwfile, strSection, strKey, strAccess;
  int flwnum, flwno=0, accno;
  FLWIniKey = new TIniFile(FLWINIFile);
  TStringList *strList = new TStringList;

  editFLWPATH->Text = FLWIniKey->ReadString("PATH", "FlwPath", ExtractFilePath(Application->ExeName)+"flwservice\\oml");
  flwnum = FLWIniKey->ReadInteger("FLW", "FLWNUM", 0);
  lvFlw->Items->Clear();
  for (int i=0; i<flwnum&&i<64; i++)
  {
    strSection = "FLW(" + IntToStr(i) + ")";
    flwfile = FLWIniKey->ReadString(strSection, "FlwFile", "");
    if (flwfile.Length() > 5)
    {
      FlwFileList.FlwFile[flwno].state = 1;

      FlwFileList.FlwFile[flwno].FlwFile = flwfile;
      FlwFileList.FlwFile[flwno].FlwNo = flwno;
      FlwFileList.FlwFile[flwno].RunAfterLoad = FLWIniKey->ReadInteger(strSection, "RunAfterLoad", 0);
      FlwFileList.FlwFile[flwno].FuncGroup = FLWIniKey->ReadInteger(strSection, "FuncGroup", 0);
      FlwFileList.FlwFile[flwno].FuncNo = FLWIniKey->ReadInteger(strSection, "FuncNo", 0);
      FlwFileList.FlwFile[flwno].RestLines = 0;
      FlwFileList.FlwFile[flwno].AccessNum = FLWIniKey->ReadInteger(strSection, "AccessNum", 0);
      FlwFileList.FlwFile[flwno].Explain = FLWIniKey->ReadString(strSection, "Explain", "");
      accno = 0;
      for (int j=0; j<256; j++)
      {
        strKey = "AccessCode[" + IntToStr(j) + "]";
        strAccess = FLWIniKey->ReadString(strSection, strKey, "");
        if (MySplit(strAccess.c_str(), strList) > 3)
        {
          FlwFileList.FlwFile[flwno].AccessCode[accno].state = 1;
          FlwFileList.FlwFile[flwno].AccessCode[accno].AccNo = accno;
          FlwFileList.FlwFile[flwno].AccessCode[accno].MinLen = StrToInt(strList->Strings[1]);
          FlwFileList.FlwFile[flwno].AccessCode[accno].MaxLen = StrToInt(strList->Strings[2]);
          FlwFileList.FlwFile[flwno].AccessCode[accno].PreCalled = strList->Strings[0];
          FlwFileList.FlwFile[flwno].AccessCode[accno].PreCaller = strList->Strings[3];
          accno ++;
        }
      }
      AddDispFlw(FlwFileList.FlwFile[flwno]);
      flwno ++;
    }
  }
  FlwFileList.FlwNum = flwno;
  delete strList;
  delete FLWIniKey;
}
void TFormMain::WriteFlwParam()
{
  AnsiString strSection, strKey, strAccess;
  FLWIniKey = new TIniFile(FLWINIFile);

  WriteIniParam(2, FLWIniKey, "PATH", "FlwPath", editFLWPATH->Text);
  WriteIniParam(2, FLWIniKey, "FLW", "FLWNUM", FlwFileList.FlwNum);
  for (int flwno=0; flwno<FlwFileList.FlwNum; flwno++)
  {
    strSection = "FLW(" + IntToStr(flwno) + ")";
    WriteIniParam(2, FLWIniKey, strSection, "FlwFile", FlwFileList.FlwFile[flwno].FlwFile);
    WriteIniParam(2, FLWIniKey, strSection, "Explain", FlwFileList.FlwFile[flwno].Explain);
    WriteIniParam(2, FLWIniKey, strSection, "RunAfterLoad", FlwFileList.FlwFile[flwno].RunAfterLoad);
    WriteIniParam(2, FLWIniKey, strSection, "FuncGroup", 1);
    WriteIniParam(2, FLWIniKey, strSection, "FuncNo", FlwFileList.FlwFile[flwno].FuncNo);
    WriteIniParam(2, FLWIniKey, strSection, "AccessNum", FlwFileList.FlwFile[flwno].AccessNum);
    for (int accno=0; accno<FlwFileList.FlwFile[flwno].AccessNum; accno++)
    {
      strKey = "AccessCode[" + IntToStr(accno) + "]";
      strAccess = FlwFileList.FlwFile[flwno].AccessCode[accno].PreCalled+","+IntToStr(FlwFileList.FlwFile[flwno].AccessCode[accno].MinLen)+","+IntToStr(FlwFileList.FlwFile[flwno].AccessCode[accno].MaxLen)+",*";
      WriteIniParam(2, FLWIniKey, strSection, strKey, strAccess);
    }
  }
  bFLWParamChange = false;
  delete FLWIniKey;
  btApply->Enabled = false;
}


void __fastcall TFormMain::Button4Click(TObject *Sender)
{
  FormPath->Setpath(1, editFLWPATH->Text);
  FormPath->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvFlwSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  DispAccCode(FlwFileList.FlwFile[Item->Index]);
  g_CurFlwNo = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvAccCodeSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurAccNo = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::editFLWPATHChange(TObject *Sender)
{
  bFLWParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------


void TFormMain::ReadPSTNParam()
{
  AnsiString strKey;

  IVRIniKey = new TIniFile (IVRINIFile);

  PstnLine.PSTNNum = IVRIniKey->ReadInteger("PSTN", "PSTNNum", 8);
  csePSTNNum->Value = PstnLine.PSTNNum;
  editCentreCode->Text = IVRIniKey->ReadString("PSTN", "PSTNCode", "");
  editDialOutPreCode->Text = IVRIniKey->ReadString("PSTN", "DialOutPreCode", "9");
  ckAutoDelMobilePreCode0->Checked = IVRIniKey->ReadBool("PSTN", "AutoDelMobilePreCode0", 1);
  ckAutoDelPreCode->Checked = IVRIniKey->ReadBool("PSTN", "AutoDelPreCode", 1);

  ckAutoAddDailOutPreCode->Checked = IVRIniKey->ReadBool("PSTN", "AutoAddDailOutPreCode", 0);
  ckAutoAddTranOutPreCode->Checked = IVRIniKey->ReadBool("PSTN", "AutoAddTranOutPreCode", 0);

  ckAutoDelDailOutPreCode->Checked = IVRIniKey->ReadBool("PSTN", "AutoDelDailOutPreCode", 1);
  ckAutoDelDailOutPreCode1->Checked = IVRIniKey->ReadBool("PSTN", "AutoDelDailOutPreCode", 1);

  ckAutoAddDailOutIPCode->Checked = IVRIniKey->ReadBool("PSTN", "AutoAddDailOutIPCode", 0);
  ckAutoDelDailOutIPCode->Checked = IVRIniKey->ReadBool("PSTN", "AutoDelDailOutIPCode", 1);

  for (int i=0; i<PstnLine.PSTNNum; i++)
  {
    PstnLine.bModifyId[i] = false;
    strKey = "PSTNCode[" + IntToStr(i) + "]";
    PstnLine.PSTNCode[i] = IVRIniKey->ReadString("PSTN", strKey, "");
    strKey = "PreCode[" + IntToStr(i) + "]";
    PstnLine.PreCode[i] = IVRIniKey->ReadString("PSTN", strKey, "");
    strKey = "IPPreCode[" + IntToStr(i) + "]";
    PstnLine.IPPreCode[i] = IVRIniKey->ReadString("PSTN", strKey, "");
    strKey = "NotAddPreCode[" + IntToStr(i) + "]";
    PstnLine.NotAddPreCode[i] = IVRIniKey->ReadString("PSTN", strKey, "");
  }

  delete IVRIniKey;
  DispPSTN();
}
void TFormMain::WritePSTNParam()
{
  AnsiString strKey;

  IVRIniKey = new TIniFile (IVRINIFile);

  WriteIniParam(1, IVRIniKey, "PSTN", "PSTNNum", PstnLine.PSTNNum);
  WriteIniParam(1, IVRIniKey,"PSTN", "PSTNCode", editCentreCode->Text);
  WriteIniParam(1, IVRIniKey,"PSTN", "DialOutPreCode", editDialOutPreCode->Text);
  WriteIniParam(1, IVRIniKey,"PSTN", "AutoDelMobilePreCode0", ckAutoDelMobilePreCode0->Checked);
  WriteIniParam(1, IVRIniKey,"PSTN", "AutoDelPreCode", ckAutoDelPreCode->Checked);

  WriteIniParam(1, IVRIniKey,"PSTN", "AutoDelDailOutPreCode", ckAutoDelDailOutPreCode->Checked);

  for (int i=0; i<128; i++)
  {
    if (PstnLine.bModifyId[i] == true)
    {
      SendmodifyextlineMsg(i, PstnLine.PSTNCode[i].c_str(), PstnLine.PreCode[i].c_str(), PstnLine.IPPreCode[i].c_str(), PstnLine.NotAddPreCode[i].c_str());
      PstnLine.bModifyId[i] = false;

      strKey = "PSTNCode[" + IntToStr(i) + "]";
      WriteIniParam(1, IVRIniKey,"PSTN", strKey, PstnLine.PSTNCode[i]);
      strKey = "PreCode[" + IntToStr(i) + "]";
      WriteIniParam(1, IVRIniKey,"PSTN", strKey, PstnLine.PreCode[i]);
      strKey = "IPPreCode[" + IntToStr(i) + "]";
      WriteIniParam(1, IVRIniKey,"PSTN", strKey, PstnLine.IPPreCode[i]);
      strKey = "NotAddPreCode[" + IntToStr(i) + "]";
      WriteIniParam(1, IVRIniKey,"PSTN", strKey, PstnLine.NotAddPreCode[i]);
    }
  }
  delete IVRIniKey;
  bPSTNParamChange = false;
  btApply->Enabled = false;
}
void TFormMain::DispPSTN()
{
  TListItem  *ListItem1;

  csePSTNNum->Value = PstnLine.PSTNNum;
  lvPSTN->Items->Clear();
  for (int i=0; i<PstnLine.PSTNNum; i++)
  {
    ListItem1 = lvPSTN->Items->Add();
    ListItem1->Caption = IntToStr(i);
    if (PstnLine.PSTNCode[i] == "")
      ListItem1->SubItems->Insert(0, editCentreCode->Text);
    else
      ListItem1->SubItems->Insert(0, PstnLine.PSTNCode[i]);
    ListItem1->SubItems->Insert(1, PstnLine.PreCode[i]);
    ListItem1->SubItems->Insert(2, PstnLine.IPPreCode[i]);
    ListItem1->SubItems->Insert(3, PstnLine.NotAddPreCode[i]);
  }
}

void __fastcall TFormMain::lvPSTNSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurPstn = Item->Index;
}
//---------------------------------------------------------------------------

void TFormMain::ReadSwitchPortParam()
{
  AnsiString strKey, atrSwitchPort;

  IVRIniKey = new TIniFile (IVRINIFile);
  CTILINKIniKey = new TIniFile (CTILINKINIFile);
  TStringList *strList = new TStringList;

  SwitchPortList.SwitchPortNum = CTILINKIniKey->ReadInteger("PORT", "PORTNUM", 0);
  SwitchPortList.CTILinkType = CTILINKIniKey->ReadInteger("PORT", "CTILINKTYPE", 1);
  SwitchPortList.SwitchIP = CTILINKIniKey->ReadString("SWITCH", "SWITCHIP", "192.168.1.100");

  SwitchPortList.ServerID = CTILINKIniKey->ReadString("TSAPILink", "ServerID", "");
  SwitchPortList.LoginID = CTILINKIniKey->ReadString("TSAPILink", "LoginID", "");
  SwitchPortList.Password = CTILINKIniKey->ReadString("TSAPILink", "Password", "");

  editSWTDialOutPreCode->Text = IVRIniKey->ReadString("SWITCH", "DialOutPreCode", "");
  editSWTPreCode->Text = IVRIniKey->ReadString("SWITCH", "PreCode", "");
  editNotAddPreCode->Text = IVRIniKey->ReadString("SWITCH", "NotAddPreCode", "");
  editSWTIPPreCode->Text = IVRIniKey->ReadString("SWITCH", "IPPreCode", "");

  for (int i=0; i<SwitchPortList.SwitchPortNum; i++)
  {
    strKey = "PORT[" + IntToStr(i) + "]";
    atrSwitchPort = CTILINKIniKey->ReadString("PORT", strKey, "");
    if (MySplit(atrSwitchPort.c_str(), ';', strList) > 3)
    {
      SwitchPortList.SwitchPort[i].state = 1;
      SwitchPortList.SwitchPort[i].m_nPortNo = i;
      SwitchPortList.SwitchPort[i].m_strPortID = strList->Strings[0];
      SwitchPortList.SwitchPort[i].m_nLgChType = StrToInt(strList->Strings[1]);
      SwitchPortList.SwitchPort[i].m_nChStyle = StrToInt(strList->Strings[2]);
      SwitchPortList.SwitchPort[i].m_nPhyPortNo = StrToInt(strList->Strings[3]);
      SwitchPortList.SwitchPort[i].m_strFlwCode = strList->Strings[4];
    }
    SwitchPortList.SwitchPort[i].ModifyId = false;
  }
  delete strList;
  delete CTILINKIniKey;
  delete IVRIniKey;

  GetLoadedTSPList();
  DispSwitchPort();
}

void TFormMain::WriteSwitchPortParam()
{
  AnsiString strKey;
  char atrSwitchPort[256];

  IVRIniKey = new TIniFile (IVRINIFile);
  CTILINKIniKey = new TIniFile (CTILINKINIFile);

  WriteIniParam(4, CTILINKIniKey,"PORT", "PORTNUM", SwitchPortList.SwitchPortNum);
  if (cbCTILinkType->Text == ((gLangID == 1) ? "CTILinkPro(收费)" : "CTILinkPro(盡穨)"))
  {
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    WriteIniParam(4, CTILINKIniKey,"PORT", "CTILINKTYPE", 1);
    SwitchPortList.CTILinkType = 1;
  }
  else
  {
    btTAPI->Visible = true;
    btUnistallTAPI->Visible = true;
    WriteIniParam(4, CTILINKIniKey,"PORT", "CTILINKTYPE", 0);
    SwitchPortList.CTILinkType = 0;
  }
  WriteIniParam(4, CTILINKIniKey,"SWITCH", "SWITCHIP", editSwitchIP->Text);

  WriteIniParam(4, CTILINKIniKey,"TSAPILink", "ServerID", editServerID->Text);
  WriteIniParam(4, CTILINKIniKey,"TSAPILink", "LoginID", editLoginID->Text);
  WriteIniParam(4, CTILINKIniKey,"TSAPILink", "Password", editPassword->Text);

  WriteIniParam(1, IVRIniKey,"SWITCH", "DialOutPreCode", editSWTDialOutPreCode->Text);
  WriteIniParam(1, IVRIniKey,"SWITCH", "PreCode", editSWTPreCode->Text);
  WriteIniParam(1, IVRIniKey,"SWITCH", "NotAddPreCode", editNotAddPreCode->Text);
  WriteIniParam(1, IVRIniKey,"SWITCH", "IPPreCode", editSWTIPPreCode->Text);

  WriteIniParam(1, IVRIniKey,"PSTN", "AutoAddDailOutPreCode", ckAutoAddDailOutPreCode->Checked);
  WriteIniParam(1, IVRIniKey,"PSTN", "AutoAddTranOutPreCode", ckAutoAddTranOutPreCode->Checked);
  WriteIniParam(1, IVRIniKey,"PSTN", "AutoDelDailOutPreCode", ckAutoDelDailOutPreCode1->Checked);
  WriteIniParam(1, IVRIniKey,"PSTN", "AutoAddDailOutIPCode", ckAutoAddDailOutIPCode->Checked);
  WriteIniParam(1, IVRIniKey,"PSTN", "AutoDelDailOutIPCode", ckAutoDelDailOutIPCode->Checked);

  for (int i=0; i<SwitchPortList.SwitchPortNum; i++)
  {
    strKey = "PORT[" + IntToStr(i) + "]";
    sprintf(atrSwitchPort,"%s;%d;%d;%d;%s;",
                    SwitchPortList.SwitchPort[i].m_strPortID.c_str(),
                    SwitchPortList.SwitchPort[i].m_nLgChType,
                    SwitchPortList.SwitchPort[i].m_nChStyle,
                    SwitchPortList.SwitchPort[i].m_nPhyPortNo,
                    SwitchPortList.SwitchPort[i].m_strFlwCode.c_str());
    WriteIniParam(4, CTILINKIniKey,"PORT", strKey.c_str(), atrSwitchPort);

    SwitchPortList.SwitchPort[i].ModifyId = false;
  }

  bool bReloadSeatList = false;
  if (SeatList.SeatNum == 1)
  {
    if (gLangID == 1)
    {
      if ( MessageBox( NULL, "请选择是否需要根据设置的交换机端口参数，重新设置坐席参数?", "提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
        bReloadSeatList = true;
    }
    else
    {
      if ( MessageBox( NULL, "叫匡拒琌惠璶沮砞﹚ユ传诀硈钡梆把计穝砞﹚畒畊把计?", "矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
        bReloadSeatList = true;
    }
  }

  delete CTILINKIniKey;
  delete IVRIniKey;

  bSwitchPortParamChange = false;
  btApply->Enabled = false;

  if (SeatList.SeatNum == 0 || bReloadSeatList == true)
  {
    SeatList.SeatNum = 0;
    for (int i=0; i<SwitchPortList.SwitchPortNum; i++)
    {
      if (SwitchPortList.SwitchPort[i].m_nLgChType == 2)
      {
        SeatList.Seat[SeatList.SeatNum].CscNo = i;
        SeatList.Seat[SeatList.SeatNum].SeatNo = SwitchPortList.SwitchPort[i].m_strPortID;
        SeatList.Seat[SeatList.SeatNum].SeatType = 1;
        SeatList.Seat[SeatList.SeatNum].SeatGroupNo = 1;
        SeatList.Seat[SeatList.SeatNum].GroupNo = "0";
        SeatList.Seat[SeatList.SeatNum].WorkerLevel = 1;
        SeatList.Seat[SeatList.SeatNum].WorkerNo = "";
        SeatList.Seat[SeatList.SeatNum].WorkerName = "";
        SeatList.Seat[SeatList.SeatNum].SeatIP = "";
        SeatList.Seat[SeatList.SeatNum].ModifyId = false;
        SeatList.SeatNum ++;
        if (SeatList.SeatNum >= MAX_AGENT_NUM)
          break;
      }
    }
    DispSeat();
    WriteSeatParam();
  }

  GetLoadedTSPList();
}

void TFormMain::DispSwitchPort()
{
  TListItem  *ListItem1;

  editSwitchPortNum->Text = SwitchPortList.SwitchPortNum;
  if (g_nSwitchMode == 1)
  {
    lbCTILinkType->Visible = true;
    cbCTILinkType->Visible = true;
    btTAPI->Visible = true;
    btUnistallTAPI->Visible = true;
    lbSwitchIP->Visible = true;
    editSwitchIP->Visible = true;
    lvSwitchPort->PopupMenu = PopupMenu6;

    lbServerID->Visible = false;
    editServerID->Visible = false;
    lbLoginID->Visible = false;
    editLoginID->Visible = false;
    lbPassword->Visible = false;
    editPassword->Visible = false;
  }
  else if (g_nSwitchMode == 2)
  {
    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = true;
    editServerID->Visible = true;
    lbLoginID->Visible = true;
    editLoginID->Visible = true;
    lbPassword->Visible = true;
    editPassword->Visible = true;
  }
  else
  {
    lbCTILinkType->Visible = false;
    cbCTILinkType->Visible = false;
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
    lbSwitchIP->Visible = false;
    editSwitchIP->Visible = false;
    lvSwitchPort->PopupMenu = NULL;

    lbServerID->Visible = false;
    editServerID->Visible = false;
    lbLoginID->Visible = false;
    editLoginID->Visible = false;
    lbPassword->Visible = false;
    editPassword->Visible = false;
  }
  if (SwitchPortList.CTILinkType == 1)
  {
    cbCTILinkType->Text = (gLangID == 1) ? "CTILinkPro(收费)" : "CTILinkPro(盡穨)";
  }
  else
  {
    cbCTILinkType->Text = (gLangID == 1) ? "CTILinkLite(免费)" : "CTILinkPro(膀セ)";
  }
  editSwitchIP->Text = SwitchPortList.SwitchIP;

  editServerID->Text = SwitchPortList.ServerID;
  editLoginID->Text = SwitchPortList.LoginID;
  editPassword->Text = SwitchPortList.Password;

  lvSwitchPort->Items->Clear();
  for (int i=0; i<SwitchPortList.SwitchPortNum; i++)
  {
    ListItem1 = lvSwitchPort->Items->Add();
    ListItem1->Caption = IntToStr(i);
    ListItem1->SubItems->Insert(0, SwitchPortList.SwitchPort[i].m_strPortID);
    switch (SwitchPortList.SwitchPort[i].m_nLgChType)
    {
    case 1:
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "中继" : "い膥");
      break;
    case 2:
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "座席" : "畒畊");
      break;
    case 3:
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "IVR" : "IVR");
      break;
    case 4:
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "录音" : "魁");
      break;
    case 5:
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "传真" : "肚痷");
      break;
    default:
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "未知" : "ゼ");
      break;
    }
    switch (SwitchPortList.SwitchPort[i].m_nChStyle)
    {
    case 1:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "模拟分机" : "摸ゑだ诀");
      break;
    case 2:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "模拟中继" : "摸ゑい膥");
      break;
    case 3:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "2B+D数字话机" : "2B+D计杠诀");
      break;
    case 8:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "DSS1数字中继" : "DSS1计い膥");
      break;
    case 9:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "TUP数字中继" : "TUP计い膥");
      break;
    case 10:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "ISUP数字中继" : "ISUP计い膥");
      break;
    case 11:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "PRI用户侧" : "PRI-USER");
      break;
    case 12:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "PRI网络侧" : "PRI-NET");
      break;
    case 15:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "H323分机" : "H323だ诀");
      break;
    case 16:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "SIP分机" : "SIPだ诀");
      break;
    case 17:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "E&M中继" : "E&Mい膥");
      break;
    case 18:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "SIP中继" : "SIPい膥");
      break;
    case 19:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "H323中继" : "H323い膥");
      break;
    case 21:
      ListItem1->SubItems->Insert(2, "LineSide");
      break;
    default:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "未知" : "ゼ");
      break;
    }
    ListItem1->SubItems->Insert(3, IntToStr(SwitchPortList.SwitchPort[i].m_nPhyPortNo));
    if (SwitchPortList.SwitchPort[i].m_nLgChType == 1 && SwitchPortList.SwitchPort[i].m_nChStyle == 2)
      ListItem1->SubItems->Insert(4, SwitchPortList.SwitchPort[i].m_strFlwCode);
    else
      ListItem1->SubItems->Insert(4, "");
  }
}

void __fastcall TFormMain::lvSwitchPortSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurSwitchPort = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn24Click(TObject *Sender)
{
  //增加
  CSwitchPort SwitchPort;

  SwitchPort.m_nPortNo = SwitchPortList.SwitchPortNum;
  SwitchPort.m_strPortID = "";
  SwitchPort.m_strFlwCode = "";
  SwitchPort.m_nLgChType = 2;
  SwitchPort.m_nChStyle = 1;
  SwitchPort.m_nPhyPortNo = 0;
  FormEditSwitchPort->SetModifyType(1, SwitchPort);
  FormEditSwitchPort->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn25Click(TObject *Sender)
{
  CSwitchPort SwitchPort;
  if (g_CurSwitchPort < 0)
  {
    SwitchPort.m_nPortNo = SwitchPortList.SwitchPortNum;
    SwitchPort.m_strPortID = "";
    SwitchPort.m_strFlwCode = "";
    SwitchPort.m_nLgChType = 2;
    SwitchPort.m_nChStyle = 1;
    SwitchPort.m_nPhyPortNo = 0;
    FormEditSwitchPort->SetModifyType(1, SwitchPort);
    FormEditSwitchPort->ShowModal();
  }
  else
  {
    SwitchPort.m_nPortNo = g_CurSwitchPort;
    SwitchPort.m_strPortID = "";
    SwitchPort.m_strFlwCode = "";
    SwitchPort.m_nLgChType = 2;
    SwitchPort.m_nChStyle = 1;
    SwitchPort.m_nPhyPortNo = 0;
    FormEditSwitchPort->SetModifyType(2, SwitchPort);
    FormEditSwitchPort->ShowModal();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn27Click(TObject *Sender)
{
  if (g_CurSwitchPort < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的交换机端口序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶эユ传诀硈钡梆腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormEditSwitchPort->SetModifyType(3, SwitchPortList.SwitchPort[g_CurSwitchPort]);
  FormEditSwitchPort->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn26Click(TObject *Sender)
{
  char dispbuf[256];

  if (g_CurSwitchPort < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要删除的交换机端口序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶埃ユ传诀硈钡梆腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (gLangID == 1)
    sprintf(dispbuf, "您真的要将交换机端口 %s  删除吗?", SwitchPortList.SwitchPort[g_CurSwitchPort].m_strPortID.c_str());
  else
    sprintf(dispbuf, "眤痷璶盢ユ传诀硈钡梆 %s  埃盾?", SwitchPortList.SwitchPort[g_CurSwitchPort].m_strPortID.c_str());

  if ( MessageBox( NULL, dispbuf, ((gLangID == 1) ? "系统提示:" : "╰参矗ボ:"), MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    SwitchPortList.DelSwitchPort(g_CurSwitchPort);
    bSwitchPortParamChange = true;
    btApply->Enabled = true;
    DispSwitchPort();
  }
}
//---------------------------------------------------------------------------

void TFormMain::ReadRouteParam()
{
  AnsiString strKey;

  IVRIniKey = new TIniFile (IVRINIFile);

  RouteList.DTUseTs32 = IVRIniKey->ReadInteger("ROUTE", "DTUseTs32", 0);
  RouteList.E1TrkSelMode = IVRIniKey->ReadInteger("ROUTE", "SelMode", 0);
  RouteList.RouteNum = IVRIniKey->ReadInteger("ROUTE", "RouteNum", 0);

  RouteList.Route[0].SelMode = 0;
  RouteList.Route[0].TrkStr = "";
  for (int i=1; i<=RouteList.RouteNum; i++)
  {
    strKey = "Route[" + IntToStr(i) + "]";
    RouteList.Route[i].TrkStr = IVRIniKey->ReadString("ROUTE", strKey, "");
    if (RouteList.Route[i].TrkStr.Length() > 0)
    {
      strKey = "Route[" + IntToStr(i) + "].SelMode";
      RouteList.Route[i].SelMode = IVRIniKey->ReadInteger("ROUTE", strKey, 0);

      strKey = "Route[" + IntToStr(i) + "].Remark";
      RouteList.Route[i].Remark = IVRIniKey->ReadString("ROUTE", strKey, "");

      strKey = "IPPreCode[" + IntToStr(i) + "].Remark";
      RouteList.Route[i].IPPreCode = IVRIniKey->ReadString("ROUTE", strKey, "");
    }
  }

  delete IVRIniKey;
  DispRoute();
}
void TFormMain::WriteRouteParam()
{
  AnsiString strKey;

  IVRIniKey = new TIniFile (IVRINIFile);

  WriteIniParam(1, IVRIniKey, "ROUTE", "RouteNum", RouteList.RouteNum);
  if (gLangID == 1)
  {
    if (cbTS32->Text == "是")
      WriteIniParam(1, IVRIniKey,"ROUTE", "DTUseTs32", 1);
    else
      WriteIniParam(1, IVRIniKey,"ROUTE", "DTUseTs32", 0);

    if (cbSelMode->Text == "按顺序循环选择")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 0);
    else if (cbSelMode->Text == "优先选奇数电路")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 1);
    else if (cbSelMode->Text == "优先选偶数电路")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 2);
    else if (cbSelMode->Text == "按路由顺序选择")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 3);
    else if (cbSelMode->Text == "按路由逆序选择")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 4);
    else if (cbSelMode->Text == "优先选择最先空闲通道")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 5);
    else
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 0);
  }
  else
  {
    if (cbTS32->Text == "琌")
      WriteIniParam(1, IVRIniKey,"ROUTE", "DTUseTs32", 1);
    else
      WriteIniParam(1, IVRIniKey,"ROUTE", "DTUseTs32", 0);

    if (cbSelMode->Text == "抖碻吏匡拒")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 0);
    else if (cbSelMode->Text == "纔匡计硄笵")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 1);
    else if (cbSelMode->Text == "纔匡案计硄笵")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 2);
    else if (cbSelMode->Text == "隔パ抖匡拒")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 3);
    else if (cbSelMode->Text == "隔パ癴匡拒")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 4);
    else if (cbSelMode->Text == "纔匡拒程丁硄笵")
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 5);
    else
      WriteIniParam(1, IVRIniKey,"ROUTE", "SelMode", 0);
  }

  for (int i=1; i<=32; i++)
  {
    if (RouteList.Route[i].bModifyId == true)
    {
      SendmodifyroutedataMsg(i, RouteList.Route[i].TrkStr.c_str(), RouteList.Route[i].SelMode, RouteList.Route[i].IPPreCode.c_str());
      RouteList.Route[i].bModifyId == false;

      strKey = "Route[" + IntToStr(i) + "].Remark";
      WriteIniParam(1, IVRIniKey,"ROUTE", strKey, RouteList.Route[i].Remark);
      strKey = "Route[" + IntToStr(i) + "].SelMode";
      WriteIniParam(1, IVRIniKey,"ROUTE", strKey, RouteList.Route[i].SelMode);
      strKey = "Route[" + IntToStr(i) + "].IPPreCode";
      WriteIniParam(1, IVRIniKey,"ROUTE", strKey, RouteList.Route[i].IPPreCode);
      strKey = "Route[" + IntToStr(i) + "]";
      WriteIniParam(1, IVRIniKey,"ROUTE", strKey, RouteList.Route[i].TrkStr);
    }
  }

  delete IVRIniKey;
  bROUTEParamChange = false;
  btApply->Enabled = false;
}
void TFormMain::DispRoute()
{
  TListItem  *ListItem1;

  if (gLangID == 1)
  {
    if (RouteList.DTUseTs32 == 1)
      cbTS32->Text = "是";
    else
      cbTS32->Text = "否";
    cseRouteNum->Value = RouteList.RouteNum;
    lvRoute->Items->Clear();
    for (int i=1; i<=RouteList.RouteNum; i++)
    {
      ListItem1 = lvRoute->Items->Add();
      ListItem1->Caption = IntToStr(i);
      ListItem1->SubItems->Insert(0, RouteList.Route[i].Remark);
      switch (RouteList.Route[i].SelMode)
      {
      case 0:
        ListItem1->SubItems->Insert(1, "按顺序循环选择");
        break;
      case 1:
        ListItem1->SubItems->Insert(1, "优先选奇数电路");
        break;
      case 2:
        ListItem1->SubItems->Insert(1, "优先选偶数电路");
        break;
      case 3:
        ListItem1->SubItems->Insert(1, "按路由顺序选择");
        break;
      case 4:
        ListItem1->SubItems->Insert(1, "按路由逆序选择");
        break;
      case 5:
        ListItem1->SubItems->Insert(1, "优先选择最先空闲通道");
        break;
      default:
        ListItem1->SubItems->Insert(1, "按顺序循环选择");
        break;
      }
      if (RouteList.Route[i].TrkStr == "")
        ListItem1->SubItems->Insert(2, "未设置");
      else
        ListItem1->SubItems->Insert(2, RouteList.Route[i].TrkStr);
      ListItem1->SubItems->Insert(3, RouteList.Route[i].IPPreCode);
    }
  }
  else
  {
    if (RouteList.DTUseTs32 == 1)
      cbTS32->Text = "琌";
    else
      cbTS32->Text = "";
    cseRouteNum->Value = RouteList.RouteNum;
    lvRoute->Items->Clear();
    for (int i=1; i<=RouteList.RouteNum; i++)
    {
      ListItem1 = lvRoute->Items->Add();
      ListItem1->Caption = IntToStr(i);
      ListItem1->SubItems->Insert(0, RouteList.Route[i].Remark);
      switch (RouteList.Route[i].SelMode)
      {
      case 0:
        ListItem1->SubItems->Insert(1, "抖碻吏匡拒");
        break;
      case 1:
        ListItem1->SubItems->Insert(1, "纔匡计硄笵");
        break;
      case 2:
        ListItem1->SubItems->Insert(1, "纔匡案计硄笵");
        break;
      case 3:
        ListItem1->SubItems->Insert(1, "隔パ抖匡拒");
        break;
      case 4:
        ListItem1->SubItems->Insert(1, "隔パ癴匡拒");
        break;
      case 5:
        ListItem1->SubItems->Insert(1, "纔匡拒程丁硄笵");
        break;
      default:
        ListItem1->SubItems->Insert(1, "抖碻吏匡拒");
        break;
      }
      if (RouteList.Route[i].TrkStr == "")
        ListItem1->SubItems->Insert(2, "ゼ砞﹚");
      else
        ListItem1->SubItems->Insert(2, RouteList.Route[i].TrkStr);
      ListItem1->SubItems->Insert(3, RouteList.Route[i].IPPreCode);
    }
  }
}

void __fastcall TFormMain::lvRouteSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurRouteNo = Item->Index+1;
}
//---------------------------------------------------------------------------
void TFormMain::ReadVopParam()
{
  AnsiString strSection, strKey;
  IVRIniKey = new TIniFile (IVRINIFile);

  VopGroup.m_nVopNum = IVRIniKey->ReadInteger("VOP", "VopNum", 0);
  //if (VopGroup.m_nVopNum > 0)
  //  cseSelVopNo->MaxValue = VopGroup.m_nVopNum-1;

  for (int i=0; i<VopGroup.m_nVopNum; i++)
  {
    strSection = "VOP(" + IntToStr(i) + ")";
    VopGroup.m_Vops[i].m_nValid = IVRIniKey->ReadInteger(strSection, "Open", 0);
    if (VopGroup.m_Vops[i].m_nValid == 0)
      continue;
    VopGroup.m_Vops[i].m_nVopChnNum = IVRIniKey->ReadInteger(strSection, "VopChnNum", 0);
    for (int j=0; j<VopGroup.m_Vops[i].m_nVopChnNum; j++)
    {
      strKey = "VopChn[" + IntToStr(j) + "].Type";
      VopGroup.m_Vops[i].m_VopChns[j].m_nVopChnType = IVRIniKey->ReadInteger(strSection, strKey, 0);
      strKey = "VopChn[" + IntToStr(j) + "].PortID";
      VopGroup.m_Vops[i].m_VopChns[j].m_PortNo = IVRIniKey->ReadInteger(strSection, strKey, -1);
      VopGroup.m_Vops[i].m_VopChns[j].m_nVopNo = i;
      VopGroup.m_Vops[i].m_VopChns[j].m_nVopChnNo = j;
      VopGroup.m_Vops[i].m_VopChns[j].m_strPortID = SwitchPortList.GetPortID(VopGroup.m_Vops[i].m_VopChns[j].m_PortNo);
    }
  }

  delete IVRIniKey;
  DispVop();
}

void TFormMain::WriteVopParam()
{
  AnsiString strSection, strKey;
  IVRIniKey = new TIniFile (IVRINIFile);

  WriteIniParam(1, IVRIniKey, "VOP", "VopNum", VopGroup.m_nVopNum);
  for (int i=0; i<VopGroup.m_nVopNum; i++)
  {
    strSection = "VOP(" + IntToStr(i) + ")";

    WriteIniParam(1, IVRIniKey, strSection, "Open", VopGroup.m_Vops[i].m_nValid);
    if (VopGroup.m_Vops[i].m_nValid == 0)
      continue;

    WriteIniParam(1, IVRIniKey, strSection, "VopChnNum", VopGroup.m_Vops[i].m_nVopChnNum);
    for (int j=0; j<VopGroup.m_Vops[i].m_nVopChnNum; j++)
    {
      strKey = "VopChn[" + IntToStr(j) + "].Type";
      WriteIniParam(1, IVRIniKey, strSection, strKey, VopGroup.m_Vops[i].m_VopChns[j].m_nVopChnType);
      strKey = "VopChn[" + IntToStr(j) + "].PortID";
      WriteIniParam(1, IVRIniKey, strSection, strKey, VopGroup.m_Vops[i].m_VopChns[j].m_PortNo);
    }
  }

  delete IVRIniKey;
  bVopParamChange = false;
  btApply->Enabled = false;
}

void TFormMain::DispVop()
{
  TListItem  *ListItem1;
  int nPortNo;

  cseVopNum->Value = VopGroup.m_nVopNum;
  if (cseSelVopNo->Value >= VopGroup.m_nVopNum)
  {
    cseSelVopNo->Value = 0;
    g_CurVopNo = 0;
  }
  else
  {
    g_CurVopNo = cseSelVopNo->Value;
  }
  lvVopChn->Items->Clear();
  if (VopGroup.m_Vops[g_CurVopNo].m_nValid == 0)
  {
    ckVopOpen->Checked = false;
    editVopChnNum->Text = 0;
    return;
  }
  ckVopOpen->Checked = true;
  editVopChnNum->Text = VopGroup.m_Vops[g_CurVopNo].m_nVopChnNum;
  for (int j=0; j<VopGroup.m_Vops[g_CurVopNo].m_nVopChnNum; j++)
  {
    ListItem1 = lvVopChn->Items->Add();
    ListItem1->Caption = IntToStr(j);
    nPortNo = VopGroup.m_Vops[g_CurVopNo].m_VopChns[j].m_PortNo;

    if (nPortNo < 0)
    {
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "未启用" : "ゼ币ノ");
      ListItem1->SubItems->Insert(1, "");
      ListItem1->SubItems->Insert(2, "");
      continue;
    }
    //0-IVR/FAX 1-IVR 2-FAX 3-模拟话机坐席监录 4-数字话机坐席监录 5-VOIP坐席监录 6-模拟中继监录 7-数字中继监录
    switch (VopGroup.m_Vops[g_CurVopNo].m_VopChns[j].m_nVopChnType)
    {
    case 0:
    case 1:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "IVR资源" : "IVR硄笵");
      break;
    case 2:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "传真资源" : "肚痷硄笵");
      break;
    case 3:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "模拟话机坐席监录" : "摸ゑ杠诀畒畊菏魁");
      break;
    case 4:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "数字话机坐席监录" : "计杠诀畒畊菏魁");
      break;
    case 5:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "IP坐席监录" : "IP畒畊菏魁");
      break;
    case 6:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "模拟中继监录" : "摸ゑい膥菏魁");
      break;
    case 7:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "数字中继监录" : "计い膥菏魁");
      break;
    case 8:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "语音中继" : "粂い膥");
      break;
    default:
      ListItem1->SubItems->Insert(0, (gLangID == 1) ? "未知" : "ゼ");
      break;
    }
    ListItem1->SubItems->Insert(1, nPortNo);
    ListItem1->SubItems->Insert(2, SwitchPortList.GetPortID(nPortNo));
  }
}

//---------------------------------------------------------------------------

void TFormMain::ReadSeatParam()
{
  AnsiString strKey, atrSeat;

  IVRIniKey = new TIniFile (IVRINIFile);
  TStringList *strList = new TStringList;

  SeatList.SeatNum = IVRIniKey->ReadInteger("SEAT", "SeatNum", 0);

  for (int i=0; i<SeatList.SeatNum; i++)
  {
    strKey = "Seat[" + IntToStr(i) + "]";
    atrSeat = IVRIniKey->ReadString("SEAT", strKey, "");
    if (MySplit(atrSeat.c_str(), strList) > 4)
    {
      SeatList.Seat[i].CscNo = i;
      SeatList.Seat[i].SeatNo = strList->Strings[0];
      SeatList.Seat[i].SeatType = StrToInt(strList->Strings[1]);
      SeatList.Seat[i].SeatGroupNo = StrToInt(strList->Strings[2]);
      SeatList.Seat[i].GroupNo = strList->Strings[3];
      SeatList.Seat[i].WorkerLevel = StrToInt(strList->Strings[4]);
      if (strList->Count >= 6)
        SeatList.Seat[i].WorkerNo = strList->Strings[5];
      else
        SeatList.Seat[i].WorkerNo = "";
      if (strList->Count >= 7)
        SeatList.Seat[i].WorkerName = strList->Strings[6];
      else
        SeatList.Seat[i].WorkerName = "";
      if (strList->Count >= 8)
        SeatList.Seat[i].SeatIP = strList->Strings[7];
      else
        SeatList.Seat[i].SeatIP = "";
      if (strList->Count >= 9)
        SeatList.Seat[i].AutoLogin = StrToInt(strList->Strings[8]);
      else
      {
        if (SeatList.Seat[i].WorkerNo == "" || SeatList.Seat[i].WorkerNo == "0")
          SeatList.Seat[i].AutoLogin = 0;
        else
          SeatList.Seat[i].AutoLogin = 1;
        SeatList.Seat[i].RecordID = 1;
        SeatList.Seat[i].DepartmentNo = "";
        SeatList.Seat[i].LocaAreaCode = "";
      }
      if (strList->Count >= 10)
        SeatList.Seat[i].RecordID = StrToInt(strList->Strings[9]);
      else
      {
        SeatList.Seat[i].RecordID = 1;
      }
      if (strList->Count >= 11)
        SeatList.Seat[i].DepartmentNo = strList->Strings[10];
      else
      {
        SeatList.Seat[i].DepartmentNo = "";
      }
      if (strList->Count >= 12)
        SeatList.Seat[i].LocaAreaCode = strList->Strings[11];
      else
      {
        SeatList.Seat[i].LocaAreaCode = "";
      }
      SeatList.Seat[i].LoginSwitchID = IVRIniKey->ReadInteger("SEAT", strKey+".LoginSwitchID", 0);
      SeatList.Seat[i].LoginSwitchPSW = IVRIniKey->ReadString("SEAT", strKey+".LoginSwitchPSW", "");
      SeatList.Seat[i].LoginSwitchGroupID = IVRIniKey->ReadString("SEAT", strKey+".LoginSwitchGroupID", "");
    }
    SeatList.Seat[i].ModifyId = false;
  }
  delete strList;
  delete IVRIniKey;
  DispSeat();
}
void TFormMain::WriteSeatParam()
{
  AnsiString strKey;
  char atrSeat[256];

  IVRIniKey = new TIniFile (IVRINIFile);

  WriteIniParam(1, IVRIniKey,"SEAT", "SeatNum", SeatList.SeatNum);
  for (int i=0; i<SeatList.SeatNum; i++)
  {
    strKey = "Seat[" + IntToStr(i) + "]";
    sprintf(atrSeat,"%s,%d,%d,%s,%d,%s,%s,%s,%d,%d,%s,%s",
                    SeatList.Seat[i].SeatNo.c_str(),
                    SeatList.Seat[i].SeatType,
                    SeatList.Seat[i].SeatGroupNo,
                    SeatList.Seat[i].GroupNo.c_str(),
                    SeatList.Seat[i].WorkerLevel,
                    SeatList.Seat[i].WorkerNo.c_str(),
                    SeatList.Seat[i].WorkerName.c_str(),
                    SeatList.Seat[i].SeatIP.c_str(),
                    SeatList.Seat[i].AutoLogin,
                    SeatList.Seat[i].RecordID,
                    SeatList.Seat[i].DepartmentNo.c_str(),
                    SeatList.Seat[i].LocaAreaCode.c_str());
    WriteIniParam(1, IVRIniKey,"SEAT", strKey.c_str(), atrSeat);
    WriteIniParam(1, IVRIniKey,"SEAT", strKey+".LoginSwitchID", SeatList.Seat[i].LoginSwitchID);
    WriteIniParam(1, IVRIniKey,"SEAT", strKey+".LoginSwitchPSW", SeatList.Seat[i].LoginSwitchPSW);
    WriteIniParam(1, IVRIniKey,"SEAT", strKey+".LoginSwitchGroupID", SeatList.Seat[i].LoginSwitchGroupID);

    if (g_bSeatModifyId == true)
      continue;
    if (SeatList.Seat[i].ModifyId == false)
      continue;
    SendmodifyseatdataMsg(SeatList.Seat[i].SeatType, i, atrSeat);

    SeatList.Seat[i].ModifyId = false;
  }

  delete IVRIniKey;
  bSeatParamChange = false;
  btApply->Enabled = false;
}
void TFormMain::DispSeat()
{
  TListItem  *ListItem1;

  cseSeatNum->Value = SeatList.SeatNum;
  lvSeat->Items->Clear();
  for (int i=0; i<SeatList.SeatNum; i++)
  {
    ListItem1 = lvSeat->Items->Add();
    ListItem1->Caption = IntToStr(i);
    ListItem1->SubItems->Insert(0, SeatList.Seat[i].SeatNo);
    if (SeatList.Seat[i].SeatType == 1)
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "内线电话座席" : "だ诀筿杠畒畊");
    else if (SeatList.Seat[i].SeatType == 2)
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "内线电脑座席" : "だ诀筿福畒畊");
    else
      ListItem1->SubItems->Insert(1, (gLangID == 1) ? "内线电话座席" : "だ诀筿杠畒畊");
    ListItem1->SubItems->Insert(2, IntToStr(SeatList.Seat[i].SeatGroupNo));
    ListItem1->SubItems->Insert(3, SeatList.Seat[i].GroupNo);
    ListItem1->SubItems->Insert(4, IntToStr(SeatList.Seat[i].WorkerLevel));
    ListItem1->SubItems->Insert(5, SeatList.Seat[i].WorkerNo);
    ListItem1->SubItems->Insert(6, SeatList.Seat[i].WorkerName);
    ListItem1->SubItems->Insert(7, SeatList.Seat[i].SeatIP);
    if (SeatList.Seat[i].AutoLogin == 1)
      ListItem1->SubItems->Insert(8, (gLangID == 1) ? "是" : "琌");
    else
      ListItem1->SubItems->Insert(8, (gLangID == 1) ? "否" : "");
    if (SeatList.Seat[i].LoginSwitchID == 1)
      ListItem1->SubItems->Insert(9, (gLangID == 1) ? "是" : "琌");
    else
      ListItem1->SubItems->Insert(9, (gLangID == 1) ? "否" : "");
    ListItem1->SubItems->Insert(10, SeatList.Seat[i].LoginSwitchPSW);
    ListItem1->SubItems->Insert(11, SeatList.Seat[i].LoginSwitchGroupID);
    if (SeatList.Seat[i].RecordID == 1)
      ListItem1->SubItems->Insert(12, (gLangID == 1) ? "是" : "琌");
    else
      ListItem1->SubItems->Insert(12, (gLangID == 1) ? "否" : "");
    ListItem1->SubItems->Insert(13, SeatList.Seat[i].DepartmentNo);
    ListItem1->SubItems->Insert(14, SeatList.Seat[i].LocaAreaCode);
  }
}

void __fastcall TFormMain::lvSeatSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurSeatPort = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::editMemVocPathChange(TObject *Sender)
{
  bOtherParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------
void TFormMain::ReadOtherParam()
{
  IVRIniKey = new TIniFile (IVRINIFile);

  ckControlCallModal->Checked = IVRIniKey->ReadBool("CONFIGURE", "ControlCallModal", false);
  ckRecordTranOutCall->Checked = IVRIniKey->ReadBool("CONFIGURE", "RecordTranOutCall", true);
  ckSendACDCallToAgent->Checked = IVRIniKey->ReadBool("CONFIGURE", "SendACDCallToAgent", false);
  ckWriteSeatStatus->Checked = IVRIniKey->ReadBool("CONFIGURE", "WriteSeatStatus", false);
  ckHangonAutoCancelCall->Checked = IVRIniKey->ReadBool("CONFIGURE", "HangonAutoCancelCall", false);
  ckSP_InsertCDR->Checked = IVRIniKey->ReadBool("CONFIGURE", "SP_InsertCDR", false);
  ckCDRSwapCallerCalledNo->Checked = IVRIniKey->ReadBool("CONFIGURE", "CDRSwapCallerCalledNo", false);

  cseMaxTranOutTalkTimeLen->Value = IVRIniKey->ReadInteger("CONFIGURE", "MaxTranOutTalkTimeLen", 900);
  cseMinChangeDutyLen->Value = IVRIniKey->ReadInteger("CONFIGURE", "MinChangeDutyLen", 2);

  if (IVRIniKey->ReadInteger("CALLCOUNT", "CountCycle", 1) == 1)
  {
    cbCountCycle->Text = (gLangID == 1) ? "每一小时" : "–";
    g_nCallCountTotal = 24;
    InitTrkCountDispData(1);
    InitAgCountDispData(1);
  }
  else
  {
    cbCountCycle->Text = (gLangID == 1) ? "每半小时" : "–";
    g_nCallCountTotal = 48;
    InitTrkCountDispData(2);
    InitAgCountDispData(2);
  }
  ckCallCount->Checked = IVRIniKey->ReadBool("CALLCOUNT", "AutoCountCall", true);
  ckWriteCountLog->Checked = IVRIniKey->ReadBool("CALLCOUNT", "WriteCountCallTxt", true);
  ckWriteCountDB->Checked = IVRIniKey->ReadBool("CALLCOUNT", "WriteCountCallDB", true);

  switch (IVRIniKey->ReadInteger("CALLCOUNT", "ClearCallCountType", 0))
  {
  case 0:
    cbClearCallCountType->Text = (gLangID == 1) ? "不清零" : "ぃ睲箂";
    break;
  case 1:
    cbClearCallCountType->Text = (gLangID == 1) ? "每天0时清零" : "–ぱ0睲箂";
    break;
  case 2:
    cbClearCallCountType->Text = (gLangID == 1) ? "整点清零" : "俱翴睲箂";
    break;
  case 3:
    cbClearCallCountType->Text = (gLangID == 1) ? "不同工号登录时清零" : "ぃ絪腹祅魁睲箂";
    break;
  case 4:
    cbClearCallCountType->Text = (gLangID == 1) ? "不同班次号登录时清零" : "ぃ痁Ω腹祅魁睲箂";
    break;
  case 5:
    cbClearCallCountType->Text = (gLangID == 1) ? "按定制时间清零" : "﹚丁睲箂";
    break;
  default:
    cbClearCallCountType->Text = (gLangID == 1) ? "不清零" : "ぃ睲箂";
    break;
  }
  if (cbClearCallCountType->Text == ((gLangID == 1) ? "按定制时间清零" : "﹚丁睲箂"))
  {
    Label80->Visible = true;
    editClearTimeList->Visible = true;
    Label81->Visible = true;
  }
  else
  {
    Label80->Visible = false;
    editClearTimeList->Visible = false;
    Label81->Visible = false;
  }

  cseMaxInRingOverTime->Value = IVRIniKey->ReadInteger("CONFIGURE", "MaxInRingOverTime", 0);
  cseMaxOutRingOverTime->Value = IVRIniKey->ReadInteger("CONFIGURE", "MaxOutRingOverTime", 0);
  cseMaxTalkOverTime->Value = IVRIniKey->ReadInteger("CONFIGURE", "MaxTalkOverTime", 0);
  cseMaxACWOverTime->Value = IVRIniKey->ReadInteger("CONFIGURE", "MaxACWOverTime", 0);

  cseMaxBusyTrunk->Value = IVRIniKey->ReadInteger("ALART", "MaxBusyTrunk", 0);
  g_nMaxBusyTrunk = cseMaxBusyTrunk->Value;

  cseMinAgentLogin->Value = IVRIniKey->ReadInteger("ALART", "MinAgentLogin", 0);
  g_nMinAgentLogin = cseMinAgentLogin->Value;

  cseMaxAgentLeval->Value = IVRIniKey->ReadInteger("ALART", "MaxAgentLeval", 0);
  g_nMaxAgentLeval = cseMaxAgentLeval->Value;

  cseMaxBusyAgent->Value = IVRIniKey->ReadInteger("ALART", "MaxBusyAgent", 0);
  g_nMaxBusyAgent = cseMaxBusyAgent->Value;

  cseMaxACDWaiting->Value = IVRIniKey->ReadInteger("ALART", "MaxACDWaiting", 0);
  g_nMaxACDWaiting = cseMaxACDWaiting->Value;

  cseMaxBusyIVR->Value = IVRIniKey->ReadInteger("ALART", "MaxBusyIVR", 0);
  g_nMaxBusyIVR = cseMaxBusyIVR->Value;

  editWaitVocFile->Text = IVRIniKey->ReadString("CONFIGURE", "WaitVocFile", "callcenter/wait.wav");

  editMemVocPath->Text = IVRIniKey->ReadString("MEDIA", "MemIndexPath", "");
  editPlayVocPath->Text = IVRIniKey->ReadString("MEDIA", "PlayFilePath", "");
  editSeatRecPath->Text = IVRIniKey->ReadString("MEDIA", "SeatRecPath", "d:/record");
  delete IVRIniKey;
}
void TFormMain::WriteOtherParam()
{
  IVRIniKey = new TIniFile (IVRINIFile);

  WriteIniParam(1, IVRIniKey,"CONFIGURE", "ControlCallModal", ckControlCallModal->Checked);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "RecordTranOutCall", ckRecordTranOutCall->Checked);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "SendACDCallToAgent", ckSendACDCallToAgent->Checked);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "WriteSeatStatus", ckWriteSeatStatus->Checked);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "HangonAutoCancelCall", ckHangonAutoCancelCall->Checked);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "SP_InsertCDR", ckSP_InsertCDR->Checked);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "CDRSwapCallerCalledNo", ckCDRSwapCallerCalledNo->Checked);

  WriteIniParam(1, IVRIniKey,"CONFIGURE", "MaxTranOutTalkTimeLen", cseMaxTranOutTalkTimeLen->Value);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "MinChangeDutyLen", cseMinChangeDutyLen->Value);

  if (cbCountCycle->Text == ((gLangID == 1) ? "每一小时" : "–"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 1);
  else
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 2);
  WriteIniParam(1, IVRIniKey,"CALLCOUNT", "AutoCountCall", ckCallCount->Checked);
  WriteIniParam(1, IVRIniKey,"CALLCOUNT", "WriteCountCallTxt", ckWriteCountLog->Checked);
  WriteIniParam(1, IVRIniKey,"CALLCOUNT", "WriteCountCallDB", ckWriteCountDB->Checked);

  if (cbClearCallCountType->Text == ((gLangID == 1) ? "不清零" : "ぃ睲箂"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "ClearCallCountType", 0);
  else if (cbClearCallCountType->Text == ((gLangID == 1) ? "每天0时清零" : "–ぱ0睲箂"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 1);
  else if (cbClearCallCountType->Text == ((gLangID == 1) ? "整点清零" : "俱翴睲箂"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 2);
  else if (cbClearCallCountType->Text == ((gLangID == 1) ? "不同工号登录时清零" : "ぃ絪腹祅魁睲箂"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 3);
  else if (cbClearCallCountType->Text == ((gLangID == 1) ? "不同班次号登录时清零" : "ぃ痁Ω腹祅魁睲箂"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 4);
  else if (cbClearCallCountType->Text == ((gLangID == 1) ? "按定制时间清零" : "﹚丁睲箂"))
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 5);
  else
    WriteIniParam(1, IVRIniKey,"CALLCOUNT", "CountCycle", 0);
  WriteIniParam(1, IVRIniKey,"CALLCOUNT", "ClearTimeList", editClearTimeList->Text);

  WriteIniParam(1, IVRIniKey,"CONFIGURE", "MaxInRingOverTime", cseMaxInRingOverTime->Value);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "MaxOutRingOverTime", cseMaxOutRingOverTime->Value);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "MaxTalkOverTime", cseMaxTalkOverTime->Value);
  WriteIniParam(1, IVRIniKey,"CONFIGURE", "MaxACWOverTime", cseMaxACWOverTime->Value);

  WriteIniParam(1, IVRIniKey,"ALART", "MaxBusyTrunk", cseMaxBusyTrunk->Value);
  g_nMaxBusyTrunk = cseMaxBusyTrunk->Value;
  WriteIniParam(1, IVRIniKey,"ALART", "MinAgentLogin", cseMinAgentLogin->Value);
  g_nMinAgentLogin = cseMinAgentLogin->Value;
  WriteIniParam(1, IVRIniKey,"ALART", "MaxAgentLeval", cseMaxAgentLeval->Value);
  g_nMaxAgentLeval = cseMaxAgentLeval->Value;
  WriteIniParam(1, IVRIniKey,"ALART", "MaxBusyAgent", cseMaxBusyAgent->Value);
  g_nMaxBusyAgent = cseMaxBusyAgent->Value;
  WriteIniParam(1, IVRIniKey,"ALART", "MaxACDWaiting", cseMaxACDWaiting->Value);
  g_nMaxACDWaiting = cseMaxACDWaiting->Value;
  WriteIniParam(1, IVRIniKey,"ALART", "MaxBusyIVR", cseMaxBusyIVR->Value);
  g_nMaxBusyIVR = cseMaxBusyIVR->Value;

  WriteIniParam(1, IVRIniKey,"CONFIGURE", "WaitVocFile", editWaitVocFile->Text);

  WriteIniParam(1, IVRIniKey,"MEDIA", "MemIndexPath", editMemVocPath->Text);
  WriteIniParam(1, IVRIniKey,"MEDIA", "PlayFilePath", editPlayVocPath->Text);
  WriteIniParam(1, IVRIniKey,"MEDIA", "BoxRecPath", editPlayVocPath->Text);
  WriteIniParam(1, IVRIniKey,"MEDIA", "SeatRecPath", editSeatRecPath->Text);

  delete IVRIniKey;
  bOtherParamChange = false;
  btApply->Enabled = false;
}
void TFormMain::ReadAlartParam()
{
  IVRIniKey = new TIniFile (IVRINIFile);

  delete IVRIniKey;
}
void TFormMain::WriteAlartParam()
{
  IVRIniKey = new TIniFile (IVRINIFile);

  delete IVRIniKey;
  bAlartParamChange = false;
  btApply->Enabled = false;
}
//---------------------------------------------------------------------------
void TFormMain::ReadSaveMsgId()
{

}
void TFormMain::WriteSaveMsgId()
{
  if (bSaveMsgIdParamChange == true)
  {
    SendsetivrsavemsgidMsg(ckIVRAlartId->Checked, ckIVRCommId->Checked, ckIVRDebugId->Checked, ckIVRDrvId->Checked);
    SendsetflwsavemsgidMsg(ckFLWAlartId->Checked, ckFLWCommId->Checked, ckFLWCmdId->Checked, ckFLWVarId->Checked);
    bSaveMsgIdParamChange = false;
  }
  btApply->Enabled = false;
}
//---------------------------------------------------------------------------


void __fastcall TFormMain::cbCardTypeKeyPress(TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void TFormMain::ReadExtSeatParam()
{
  AnsiString strKey, atrSeat;

  IVRIniKey = new TIniFile (IVRINIFile);
  TStringList *strList = new TStringList;

  ExtSeatList.ExtSeatNum = IVRIniKey->ReadInteger("EXTSEAT", "SeatNum", 0);

  for (int i=0; i<ExtSeatList.ExtSeatNum; i++)
  {
    strKey = "Seat[" + IntToStr(i) + "]";
    atrSeat = IVRIniKey->ReadString("EXTSEAT", strKey, "");
    if (MySplit(atrSeat.c_str(), strList) > 6)
    {
      ExtSeatList.ExtSeat[i].CscNo = i;
      ExtSeatList.ExtSeat[i].SeatNo = strList->Strings[0];
      ExtSeatList.ExtSeat[i].SeatType = StrToInt(strList->Strings[1]);
      ExtSeatList.ExtSeat[i].PSTNNo = strList->Strings[2];
      ExtSeatList.ExtSeat[i].RouteNo = StrToInt(strList->Strings[3]);
      ExtSeatList.ExtSeat[i].SeatGroupNo = StrToInt(strList->Strings[4]);
      ExtSeatList.ExtSeat[i].GroupNo = StrToInt(strList->Strings[5]);
      ExtSeatList.ExtSeat[i].WorkerLevel = StrToInt(strList->Strings[6]);
      if (strList->Count >= 8)
        ExtSeatList.ExtSeat[i].WorkerNo = strList->Strings[7];
      else
        ExtSeatList.ExtSeat[i].WorkerNo = "";
      if (strList->Count >= 9)
        ExtSeatList.ExtSeat[i].WorkerName = strList->Strings[8];
      else
        ExtSeatList.ExtSeat[i].WorkerName = "";
      if (strList->Count >= 10)
        ExtSeatList.ExtSeat[i].SeatIP = strList->Strings[9];
      else
        ExtSeatList.ExtSeat[i].SeatIP = "";
    }
    ExtSeatList.ExtSeat[i].ModifyId = false;
  }
  delete strList;
  delete IVRIniKey;
  DispExtSeat();
}
void TFormMain::WriteExtSeatParam()
{
  AnsiString strKey;
  char atrSeat[256];

  IVRIniKey = new TIniFile (IVRINIFile);

  WriteIniParam(1, IVRIniKey,"EXTSEAT", "SeatNum", ExtSeatList.ExtSeatNum);
  for (int i=0; i<ExtSeatList.ExtSeatNum; i++)
  {
    if (ExtSeatList.ExtSeat[i].ModifyId == false)
      continue;
    strKey = "Seat[" + IntToStr(i) + "]";
    sprintf(atrSeat, "%s,%d,%s,%d,%d,%d,%d,%s,%s,%s",
                    ExtSeatList.ExtSeat[i].SeatNo.c_str(),
                    ExtSeatList.ExtSeat[i].SeatType,
                    ExtSeatList.ExtSeat[i].PSTNNo.c_str(),
                    ExtSeatList.ExtSeat[i].RouteNo,
                    ExtSeatList.ExtSeat[i].SeatGroupNo,
                    ExtSeatList.ExtSeat[i].GroupNo,
                    ExtSeatList.ExtSeat[i].WorkerLevel,
                    ExtSeatList.ExtSeat[i].WorkerNo.c_str(),
                    ExtSeatList.ExtSeat[i].WorkerName.c_str(),
                    ExtSeatList.ExtSeat[i].SeatIP.c_str());
    WriteIniParam(1, IVRIniKey,"EXTSEAT", strKey.c_str(), atrSeat);

    SendmodifyseatdataMsg(ExtSeatList.ExtSeat[i].SeatType, i, atrSeat);

    ExtSeatList.ExtSeat[i].ModifyId = false;
  }

  delete IVRIniKey;
  bExtSeatParamChange = false;
  btApply->Enabled = false;
}
void TFormMain::DispExtSeat()
{
  TListItem  *ListItem1;

  cseExtSeatNum->Value = ExtSeatList.ExtSeatNum;
  lvExtSeat->Items->Clear();
  for (int i=0; i<ExtSeatList.ExtSeatNum; i++)
  {
    ListItem1 = lvExtSeat->Items->Add();
    ListItem1->Caption = IntToStr(i);
    ListItem1->SubItems->Insert(0, ExtSeatList.ExtSeat[i].SeatNo);
    ListItem1->SubItems->Insert(1, (gLangID == 1) ? "远程电话座席" : "环祘筿杠畒畊");
    ListItem1->SubItems->Insert(2, ExtSeatList.ExtSeat[i].PSTNNo);
    ListItem1->SubItems->Insert(3, IntToStr(ExtSeatList.ExtSeat[i].RouteNo));
    ListItem1->SubItems->Insert(4, IntToStr(ExtSeatList.ExtSeat[i].SeatGroupNo));
    ListItem1->SubItems->Insert(5, IntToStr(ExtSeatList.ExtSeat[i].GroupNo));
    ListItem1->SubItems->Insert(6, IntToStr(ExtSeatList.ExtSeat[i].WorkerLevel));
    ListItem1->SubItems->Insert(7, ExtSeatList.ExtSeat[i].WorkerNo);
    ListItem1->SubItems->Insert(8, ExtSeatList.ExtSeat[i].WorkerName);
    ListItem1->SubItems->Insert(9, ExtSeatList.ExtSeat[i].SeatIP);
  }
}

void __fastcall TFormMain::lvExtSeatSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurExtSeatPort = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  Timer1->Enabled = false;
  delete pstrChnList;
  delete pstrAgList;
  delete pstrAcdList1;
  delete pstrAcdList2;
  delete pstrTrkCountList;
  delete pstrAgCountList;
  if (g_pTcpClient != NULL)
  {
      delete g_pTcpClient;
      g_pTcpClient = NULL;
  }
  if (g_pMsgfifo != NULL)
  {
      delete g_pMsgfifo;
      g_pMsgfifo = NULL;
  }
  if (g_pFlwRule != NULL)
  {
      delete g_pFlwRule;
      g_pFlwRule = NULL;
  }
  if (g_pChnStatus != NULL)
  {
    delete []g_pChnStatus;
    g_pChnStatus = NULL;
  }
  if (g_pAgStatus != NULL)
  {
    delete []g_pAgStatus;
    g_pAgStatus = NULL;
  }

  if (strTSPList != NULL)
  {
    delete strTSPList;
    strTSPList = NULL;
  }
  ReleaseTAPIDll();
  
  CanClose = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartIVRServiceClick(TObject *Sender)
{
  if (g_nRunMode == 2)
  {
    g_pTcpClient->CTRLSendMessage(11, "Start QuarkCallIVRService!");
  }
  else
  {
    SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
    if (scm!=NULL)
    {
      SC_HANDLE svc = OpenService(scm, IVR_SERVICE_NAME, SERVICE_START);
      if (svc != NULL)
      {
        if (StartService(svc, 0, NULL))
        {
          imgIVRServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartIVRService->Enabled = false;
          btStopIVRService->Enabled = true;
        }
        else
        {
          if (gLangID == 1)
            MessageBox(NULL,"启动IVR服务失败!!!","警告",MB_OK|MB_ICONWARNING);
          else
            MessageBox(NULL,"币笆IVR狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
        }
        CloseServiceHandle(svc);
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"IVR服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"IVR狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(scm);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopIVRServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nRunMode == 2)
  {
    g_pTcpClient->CTRLSendMessage(12, "Stop QuarkCallIVRService!");
  }
  else
  {
    SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
    if (scm != NULL)
    {
      SC_HANDLE svc = OpenService(scm,IVR_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc!=NULL)
      {
        SERVICE_STATUS ServiceStatus;
        QueryServiceStatus(svc, &ServiceStatus);
        if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
        {
          if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
          {
            imgIVRServiceStatus->Picture->LoadFromFile(imgStopFile);
            btStartIVRService->Enabled = true;
            btStopIVRService->Enabled = false;
          }
        }
        CloseServiceHandle(svc);
      }
      CloseServiceHandle(scm);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshIVRServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,IVR_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgIVRServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartIVRService->Enabled = false;
        btStopIVRService->Enabled = true;
      }
      else
      {
        imgIVRServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartIVRService->Enabled = true;
        btStopIVRService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartFLWServiceClick(TObject *Sender)
{
  if (g_nRunMode == 2)
  {
    g_pTcpClient->CTRLSendMessage(13, "Start QuarkCallFLWService!");
  }
  else
  {
    SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
    if (scm!=NULL)
    {
      SC_HANDLE svc = OpenService(scm, FLW_SERVICE_NAME, SERVICE_START);
      if (svc != NULL)
      {
        if (StartService(svc, 0, NULL))
        {
          imgFLWServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartFLWService->Enabled = false;
          btStopFLWService->Enabled = true;
        }
        else
        {
          if (gLangID == 1)
            MessageBox(NULL,"启动FLW服务失败!!!","警告",MB_OK|MB_ICONWARNING);
          else
            MessageBox(NULL,"币笆FLW狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
        }
        CloseServiceHandle(svc);
      }
      else
      {
        if (gLangID == 1)
        MessageBox(NULL,"FLW服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
        else
        MessageBox(NULL,"FLW狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(scm);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopFLWServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nRunMode == 2)
  {
    g_pTcpClient->CTRLSendMessage(14, "Stop QuarkCallFLWService!");
  }
  else
  {
    SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
    if (scm != NULL)
    {
      SC_HANDLE svc = OpenService(scm,FLW_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc!=NULL)
      {
        SERVICE_STATUS ServiceStatus;
        QueryServiceStatus(svc, &ServiceStatus);
        if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
        {
          if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
          {
            imgFLWServiceStatus->Picture->LoadFromFile(imgStopFile);
            btStartFLWService->Enabled = true;
            btStopFLWService->Enabled = false;
          }
        }
        CloseServiceHandle(svc);
      }
      CloseServiceHandle(scm);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshFLWServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,FLW_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgFLWServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartFLWService->Enabled = false;
        btStopFLWService->Enabled = true;
      }
      else
      {
        imgFLWServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartFLWService->Enabled = true;
        btStopFLWService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Timer1Timer(TObject *Sender)
{
	if(g_pTcpClient->IsOpen)
  {
    g_pTcpClient->AppMainLoop();
    ProcLogMsg();
  }
  TListItem  *ListItem1;
  static int runcount = 0;
  static time_t oldt = time(0);
  time_t newt;
  int nItemIndex, c;

  if ((runcount ++) >= 75 )
  {
    newt = time(0);
    c = newt - oldt;
    for (int i=0; i<256; i++)
    {
      if (WorkerCallCountParamList.AgentCallCountParam[i].WorkerNo > 0)
      {
        WorkerCallCountParamList.AgentCallCountParam[i].curTimeLen += c;
        nItemIndex = WorkerCallCountParamList.AgentCallCountParam[i].ItemListIndex;
        if (nItemIndex >= 0 && nItemIndex < lvWorkerList->Items->Count)
        {
          ListItem1 = lvWorkerList->Items->Item[nItemIndex];
          ListItem1->SubItems->Strings[3] = GetTimeString(WorkerCallCountParamList.AgentCallCountParam[i].curTimeLen);
        }
      }
    }
    oldt = newt;
    runcount = 0;
  }
  if (ckLocaAlartId->Checked == false)
    return;

  static int count = 100;
  static bool bCanPlay=true;
  bool bWorkerCallCount = WorkerCallCountParamList.IsAlarting();
  bool bGroupCallCount = GroupCallCountParamList.IsAlarting();
  if (bWorkerCallCount == false && bGroupCallCount == false && ckSeatSatusSound->Color == clRed)
  {
    ckSeatSatusSound->Color = clSkyBlue;
  }
  if ((bWorkerCallCount == true || bGroupCallCount == true) && ckSeatSatusSound->Color != clRed)
  {
    ckSeatSatusSound->Color = clRed;
  }

  if (g_nIVRAlarmId != 0 || g_nFLWAlarmId != 0 || g_nDBAlarmId != 0 || g_nConnectIVRId == 1 || g_nConnectFLWId == 1
      || (g_bBusyTrunkSound == true && ckBusyTrunkSound->Checked == true)
      || ((g_bBlockTrunkSound == true || g_bBlockIVRSound == true) && ckBlockTrunkSound->Checked == true)
      || (g_bAgentLoginSound == true && ckAgentLoginSound->Checked == true)
      || (g_bAgentLevalSound == true && ckAgentLevalSound->Checked == true)
      || (g_bBusyAgentSound == true && ckBusyAgentSound->Checked == true)
      || (g_bACDWaitingSound == true && ckACDWaitingSound->Checked == true)
      || (g_bBusyIVRSound == true && ckBusyIVRSound->Checked == true)
      || (bWorkerCallCount == true && ckSeatSatusSound->Checked == true)
      || (bGroupCallCount == true && ckSeatSatusSound->Checked == true)
      )
  {
    if (bCanPlay == true)
    {
      if ((count++) > 30)
      {
        count = 0;
        try
        {
          MediaPlayer1->FileName = g_strAlartWavFile;
          MediaPlayer1->Open();
          MediaPlayer1->Play();
        }
        catch (...)
        {
          bCanPlay = false;
        }
      }
    }
    else
    {
      if (count ++ > 5)
      {
        Beep( 1000, 200 );
        count = 0;
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetchnstatusClick(TObject *Sender)
{
  if (g_nConnectIVRId != 4)
    return;
  g_bGetchnstatus = ckGetchnstatus->Checked;
  SendGetStatusMsg(1, LOGMSG_getchnstatus, g_bGetchnstatus);
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getchnstatus", g_bGetchnstatus);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetagentstatusClick(TObject *Sender)
{
  if (g_nConnectIVRId != 4)
    return;
  g_bGetagentstatus = ckGetagentstatus->Checked;
  SendGetStatusMsg(1, LOGMSG_getagentstatus, g_bGetagentstatus);
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getagentstatus", g_bGetagentstatus);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetqueueinfoClick(TObject *Sender)
{
  if (g_nConnectIVRId != 4)
    return;
  g_bGetqueueinfo = ckGetqueueinfo->Checked;
  SendGetStatusMsg(1, LOGMSG_getqueueinfo, g_bGetqueueinfo);
  TIniFile *IniKey = new TIniFile(LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getqueueinfo", g_bGetqueueinfo);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetivrtracemsgClick(TObject *Sender)
{
  if (g_nConnectIVRId != 4)
    return;
  g_bGetivrtracemsg = ckGetivrtracemsg->Checked;
  SendGetStatusMsg(1, LOGMSG_getivrtracemsg, g_bGetivrtracemsg);
  TIniFile *IniKey = new TIniFile(LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getivrtracemsg", g_bGetivrtracemsg);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetgetloadedflwsClick(TObject *Sender)
{
  if (g_nConnectFLWId != 4)
    return;
  g_bGetloadedflws = ckGetgetloadedflws->Checked;
  SendGetStatusMsg(2, LOGMSG_getloadedflws, g_bGetloadedflws);
  TIniFile *IniKey = new TIniFile(LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getloadedflws", g_bGetloadedflws);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetgetflwtracemsgClick(TObject *Sender)
{
  if (g_nConnectFLWId != 4)
    return;
  g_bGetflwtracemsg = ckGetgetflwtracemsg->Checked;
  SendGetStatusMsg(2, LOGMSG_getflwtracemsg, g_bGetflwtracemsg);
  TIniFile *IniKey = new TIniFile(LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getflwtracemsg", g_bGetflwtracemsg);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvAgStatusSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurAgNo = Item->Index;
  g_CurSeatNo = Item->SubItems->Strings[0];
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvChnStatusSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  for (short i = 0; i < g_nMaxChnNum; i ++)
  {
    if (g_pChnStatus[i].nListItemNo == Item->Index)
      g_CurChnNo = i;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N7Click(TObject *Sender)
{
  char dispbuf[256];
  if (g_CurChnNo < 0 || g_CurChnNo >= g_nMaxChnNum)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要释放的通道!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶睦硄笵!","獺矗ボ",MB_OK|MB_ICONINFORMATION);

    return;
  }
  if (gLangID == 1)
  {
  if (g_pChnStatus[g_CurChnNo].nLgChnType == 1)
    sprintf(dispbuf, "您真的要释放序号为 %d 的中继通道吗?", g_pChnStatus[g_CurChnNo].nLgChnNo);
  else if (g_pChnStatus[g_CurChnNo].nLgChnType == 2)
    sprintf(dispbuf, "您真的要释放序号为 %d 的内线通道吗?", g_pChnStatus[g_CurChnNo].nLgChnNo);
  else if (g_pChnStatus[g_CurChnNo].nLgChnType == 3)
    sprintf(dispbuf, "您真的要释放序号为 %d 的IVR通道吗?", g_pChnStatus[g_CurChnNo].nLgChnNo);
  else
    return;
  if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    SendreleasechnMsg(g_CurChnNo);
  }
  }
  else
  {
  if (g_pChnStatus[g_CurChnNo].nLgChnType == 1)
    sprintf(dispbuf, "眤痷璶睦硄笵腹 %d い膥硄笵盾?", g_pChnStatus[g_CurChnNo].nLgChnNo);
  else if (g_pChnStatus[g_CurChnNo].nLgChnType == 2)
    sprintf(dispbuf, "眤痷璶睦硄笵腹 %d だ诀硄笵盾?", g_pChnStatus[g_CurChnNo].nLgChnNo);
  else if (g_pChnStatus[g_CurChnNo].nLgChnType == 3)
    sprintf(dispbuf, "眤痷璶睦硄笵腹 %d IVR硄笵盾?", g_pChnStatus[g_CurChnNo].nLgChnNo);
  else
    return;
  if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
  {
    SendreleasechnMsg(g_CurChnNo);
  }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N8Click(TObject *Sender)
{
  char dispbuf[256];
  if (g_CurAgNo < 0 || g_CurSeatNo == "")
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要操作的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶巨畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (gLangID == 1)
    sprintf(dispbuf, "您真的要将 %s 号坐席强行退出吗?", g_CurSeatNo.c_str());
  else
    sprintf(dispbuf, "眤痷璶盢 %s 腹畒畊眏癶盾?", g_CurSeatNo.c_str());
  if ( MessageBox( NULL, dispbuf, ((gLangID == 1) ? "系统提示：" : "╰参矗ボ:"), MB_YESNO|MB_ICONQUESTION ) == IDYES )
    SendforcelogoutMsg(g_CurSeatNo.c_str());
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N9Click(TObject *Sender)
{
  char dispbuf[256];
  if (gLangID == 1)
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"请选择需要操作的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    sprintf(dispbuf, "您真的要将 %s 号坐席强行示忙吗?", g_CurSeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      SendforcedisturbMsg(g_CurSeatNo.c_str());
  }
  else
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"叫匡拒惠璶巨畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    sprintf(dispbuf, "眤痷璶盢 %s 腹畒畊眏︽Γ魁盾?", g_CurSeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      SendforcedisturbMsg(g_CurSeatNo.c_str());
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N10Click(TObject *Sender)
{
  char dispbuf[256];
  if (gLangID == 1)
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"请选择需要操作的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    sprintf(dispbuf, "您真的要将 %s 号坐席强行示闲吗?", g_CurSeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      SendforcereadyMsg(g_CurSeatNo.c_str());
  }
  else
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"叫匡拒惠璶巨畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    sprintf(dispbuf, "眤痷璶盢 %s 腹畒畊眏︽丁盾?", g_CurSeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      SendforcereadyMsg(g_CurSeatNo.c_str());
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N11Click(TObject *Sender)
{
  char dispbuf[256];
  if (gLangID == 1)
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"请选择需要操作的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    sprintf(dispbuf, "您真的要将 %s 号坐席强行释放吗?", g_CurSeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      SendforceclearMsg(g_CurSeatNo.c_str());
  }
  else
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"叫匡拒惠璶巨畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    sprintf(dispbuf, "眤痷璶盢 %s 腹畒畊眏︽睦盾?", g_CurSeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
      SendforceclearMsg(g_CurSeatNo.c_str());
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btClearIVRMsgClick(TObject *Sender)
{
  memoIVRTrace->Lines->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btClearFLWMsgClick(TObject *Sender)
{
  memoFLWTrace->Lines->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckIVRAlartIdClick(TObject *Sender)
{
  bSaveMsgIdParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvLoadFlwSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurSelFlw = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N12Click(TObject *Sender)
{
  TListItem  *ListItem1;
  char pszFlwFileName[128], dispbuf[256];

  if (gLangID == 1)
  {
    if (g_CurSelFlw < 0)
    {
      MessageBox(NULL,"请选择需要加载的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }

    if (g_CurSelFlw >= lvLoadFlw->Items->Count)
      return;
    ListItem1 = lvLoadFlw->Items->Item[g_CurSelFlw];

    sprintf(dispbuf, "您真的要重新加载 %s 流程吗?", ListItem1->SubItems->Strings[0].c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      sprintf(pszFlwFileName, "%s.oml", ListItem1->SubItems->Strings[0].c_str());
      SendReLoadFlwMsg(pszFlwFileName,
        StrToInt(ListItem1->SubItems->Strings[1]),
        StrToInt(ListItem1->SubItems->Strings[2]));
    }
  }
  else
  {
    if (g_CurSelFlw < 0)
    {
      MessageBox(NULL,"叫匡拒惠璶更瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }

    if (g_CurSelFlw >= lvLoadFlw->Items->Count)
      return;
    ListItem1 = lvLoadFlw->Items->Item[g_CurSelFlw];

    sprintf(dispbuf, "眤痷璶穝更 %s 瑈祘盾?", ListItem1->SubItems->Strings[0].c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      sprintf(pszFlwFileName, "%s.oml", ListItem1->SubItems->Strings[0].c_str());
      SendReLoadFlwMsg(pszFlwFileName,
        StrToInt(ListItem1->SubItems->Strings[1]),
        StrToInt(ListItem1->SubItems->Strings[2]));
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckLocaAlartIdClick(TObject *Sender)
{
  g_bLocaAlartId = ckLocaAlartId->Checked;
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("CONFIG", "LocaAlartId", ckLocaAlartId->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N16Click(TObject *Sender)
{
  char dispbuf[256];
  AnsiString strNewSeatNo;

  if (gLangID == 1)
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"请选择需要操作的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (InputQuery("输入提示：", "请输入新的坐席分机号(注意号码不能重复)：", strNewSeatNo))
    {
      if (strNewSeatNo == "") return;

      sprintf(dispbuf, "您真的要将坐席分机号 %s 改为 %s 吗?", g_CurSeatNo.c_str(), strNewSeatNo.c_str());
      if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
        SendmodifyseatnoMsg(g_CurAgNo, strNewSeatNo.c_str());
    }
  }
  else
  {
    if (g_CurAgNo < 0 || g_CurSeatNo == "")
    {
      MessageBox(NULL,"叫匡拒惠璶巨畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (InputQuery("块矗ボ:", "叫块穝畒畊だ诀腹絏(猔種腹絏ぃ確):", strNewSeatNo))
    {
      if (strNewSeatNo == "") return;

      sprintf(dispbuf, "眤痷璶盢畒畊だ诀腹絏 %s э %s 盾?", g_CurSeatNo.c_str(), strNewSeatNo.c_str());
      if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
        SendmodifyseatnoMsg(g_CurAgNo, strNewSeatNo.c_str());
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N14Click(TObject *Sender)
{
  AnsiString strAccessCode;

  if (gLangID == 1)
  {
    if (g_CurSelFlw < 0)
    {
      MessageBox(NULL,"请选择需要操作的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }

    if (g_CurSelFlw >= lvLoadFlw->Items->Count)
      return;

    if (InputQuery("输入提示：", "请输入要删除的接入号码：", strAccessCode))
    {
      if (strAccessCode == "") return;
      SendDelAccessCodeMsg(g_CurSelFlw, strAccessCode.c_str());
    }
  }
  else
  {
    if (g_CurSelFlw < 0)
    {
      MessageBox(NULL,"叫匡拒惠璶巨瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }

    if (g_CurSelFlw >= lvLoadFlw->Items->Count)
      return;

    if (InputQuery("块矗ボ:", "叫块璶埃挤腹絏:", strAccessCode))
    {
      if (strAccessCode == "") return;
      SendDelAccessCodeMsg(g_CurSelFlw, strAccessCode.c_str());
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N13Click(TObject *Sender)
{
  if (g_CurSelFlw < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要操作的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶巨瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }

  if (g_CurSelFlw >= lvLoadFlw->Items->Count)
    return;
  FormAddCode->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button1Click(TObject *Sender)
{
  if (OpenDialog1->Execute())
    editFlwFileName->Text = OpenDialog1->FileName;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button2Click(TObject *Sender)
{
  AnsiString strDestFlwFileName;
  if (gLangID == 1)
  {
    if (editFlwFileName->Text == "")
    {
      MessageBox(NULL,"请选择流程文件!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (FileExists(editFlwFileName->Text) == false)
    {
      MessageBox(NULL,"对不起，您选择的流程文件不存在!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (g_nRunMode == 1)
    {
      strDestFlwFileName = g_strUcommPath+"FLWSERVICE\\OML\\"+ExtractFileName(editFlwFileName->Text);
      if (CopyFile(editFlwFileName->Text.c_str(), strDestFlwFileName.c_str(), FALSE) == TRUE)
        MessageBox(NULL,"上传流程文件成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"上传流程文件失败!","信息提示",MB_OK|MB_ICONWARNING);
      return;
    }

    if (MyUpLoadFile(SERVICEIP, FTPPort, FTPUser, FTPPSW, editFlwFileName->Text, "OML/" + ExtractFileName(editFlwFileName->Text)) == true)
      MessageBox(NULL,"上传流程文件成功!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"上传流程文件失败!","信息提示",MB_OK|MB_ICONWARNING);
  }
  else
  {
    if (editFlwFileName->Text == "")
    {
      MessageBox(NULL,"叫匡拒瑈祘ゅン!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (FileExists(editFlwFileName->Text) == false)
    {
      MessageBox(NULL,"癸ぃ癬眤匡拒瑈祘ゅンぃ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (g_nRunMode == 1)
    {
      strDestFlwFileName = g_strUcommPath+"FLWSERVICE\\OML\\"+ExtractFileName(editFlwFileName->Text);
      if (CopyFile(editFlwFileName->Text.c_str(), strDestFlwFileName.c_str(), FALSE) == TRUE)
        MessageBox(NULL,"肚瑈祘ゅンΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"肚瑈祘ゅンア毖!","獺矗ボ",MB_OK|MB_ICONWARNING);
      return;
    }

    if (MyUpLoadFile(SERVICEIP, FTPPort, FTPUser, FTPPSW, editFlwFileName->Text, "OML/" + ExtractFileName(editFlwFileName->Text)) == true)
      MessageBox(NULL,"肚瑈祘ゅンΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"肚瑈祘ゅンア毖!","獺矗ボ",MB_OK|MB_ICONWARNING);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button24Click(TObject *Sender)
{
  if (OpenDialog2->Execute())
    editVocFileName->Text = OpenDialog2->FileName;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button25Click(TObject *Sender)
{
  bool nResult;
  AnsiString strDestFlwFileName;
  if (gLangID == 1)
  {
    if (editVocFileName->Text == "")
    {
      MessageBox(NULL,"请选择语音文件!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (FileExists(editVocFileName->Text) == false)
    {
      MessageBox(NULL,"对不起，您选择的语音文件不存在!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }

    if (editFTPVocPath->Text == "")
    {
      MessageBox(NULL,"对不起，请选择上传流程语音路径!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (g_nRunMode == 1)
    {
      strDestFlwFileName = g_strUcommPath+"IVRSERVICE\\IVRVOC\\"+editFTPVocPath->Text+"\\"+ExtractFileName(editVocFileName->Text);
      if (CopyFile(editVocFileName->Text.c_str(), strDestFlwFileName.c_str(), FALSE) == TRUE)
        MessageBox(NULL,"上传语音文件成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"上传语音文件失败!","信息提示",MB_OK|MB_ICONWARNING);

      return;
    }

    nResult = MyUpLoadFile(SERVICEIP, FTPPort, FTPUser, FTPPSW, editVocFileName->Text, editFTPVocPath->Text + "/" + ExtractFileName(editVocFileName->Text));
    if (nResult == true)
      MessageBox(NULL,"上传语音文件成功!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"上传语音文件失败!","信息提示",MB_OK|MB_ICONWARNING);
  }
  else
  {
    if (editVocFileName->Text == "")
    {
      MessageBox(NULL,"叫匡拒粂ゅン!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (FileExists(editVocFileName->Text) == false)
    {
      MessageBox(NULL,"癸ぃ癬眤匡拒粂ゅンぃ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }

    if (editFTPVocPath->Text == "")
    {
      MessageBox(NULL,"癸ぃ癬叫匡拒肚瑈祘粂隔畖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (g_nRunMode == 1)
    {
      strDestFlwFileName = g_strUcommPath+"IVRSERVICE\\IVRVOC\\"+editFTPVocPath->Text+"\\"+ExtractFileName(editVocFileName->Text);
      if (CopyFile(editVocFileName->Text.c_str(), strDestFlwFileName.c_str(), FALSE) == TRUE)
        MessageBox(NULL,"肚粂ゅンΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"肚粂ゅンア毖!","獺矗ボ",MB_OK|MB_ICONWARNING);

      return;
    }

    nResult = MyUpLoadFile(SERVICEIP, FTPPort, FTPUser, FTPPSW, editVocFileName->Text, editFTPVocPath->Text + "/" + ExtractFileName(editVocFileName->Text));
    if (nResult == true)
      MessageBox(NULL,"肚粂ゅンΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"肚粂ゅンア毖!","獺矗ボ",MB_OK|MB_ICONWARNING);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetTrkCountDataClick(TObject *Sender)
{
  if (g_nConnectIVRId != 4)
    return;
  g_bGettrkcallcount = ckGetTrkCountData->Checked;
  SendGetStatusMsg(1, LOGMSG_gettrkcallcount, g_bGettrkcallcount);
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  IniKey->WriteBool("CONFIG", "Gettrkcallcount", g_bGettrkcallcount);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckGetAgCountDataClick(TObject *Sender)
{
  if (g_nConnectIVRId != 4)
    return;
  g_bGetagcallcount = ckGetAgCountData->Checked;
  SendGetStatusMsg(1, LOGMSG_getagcallcount, g_bGetagcallcount);
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  IniKey->WriteBool("CONFIG", "Getagcallcount", g_bGetagcallcount);
  delete IniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvTrkCountDataSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurSelTrkCount = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvAgCountDataSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurSelAgCount = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvTrkCountDataDblClick(TObject *Sender)
{
  /*if (g_CurSelTrkCount < 0)
    return;
  FormTrkChart->Series1->Clear();
  FormTrkChart->Caption = "中继["+TrkRemark[g_CurSelTrkCount]+"]实时统计分析图";
  for( int i = 0; i < g_nCallCountTotal; i++ )
  {
    FormTrkChart->Series1->Add(g_TodayTrkCount[i].TrkValues[g_CurSelTrkCount],IntToStr(i),clRed);
  }
  FormTrkChart->Show();*/
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvAgCountDataDblClick(TObject *Sender)
{
  /*if (g_CurSelAgCount < 0)
    return;
  FormTrkChart->Series1->Clear();
  FormTrkChart->Caption = "坐席["+AgRemark[g_CurSelAgCount]+"]实时统计分析图";
  for( int i = 0; i < g_nCallCountTotal; i++ )
  {
    FormTrkChart->Series1->Add(g_TodayAgCount[i].AgValues[g_CurSelAgCount],IntToStr(i),clRed);
  }
  FormTrkChart->Show();*/
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbCountCycleKeyPress(TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbTS32Change(TObject *Sender)
{
  bROUTEParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallFLWserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "FLWSERVICE\\QuarkCallFLWService.exe -install";
  #else
  strTemp = strTemp + "FLWSERVICE\\UnimeCallFLWServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallIVRserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "IVRSERVICE\\QuarkCallIVRService.exe -install";
  #else
  strTemp = strTemp + "IVRSERVICE\\UnimeCallIVRServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button27Click(TObject *Sender)
{
  SendGetStatusMsg(1, LOGMSG_getagcallcount, 2);  
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button26Click(TObject *Sender)
{
  SendGetStatusMsg(1, LOGMSG_gettrkcallcount, 2);  
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N18Click(TObject *Sender)
{
  if (g_CurSelFlw < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要操作的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶巨瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }

  if (g_CurSelFlw >= lvLoadFlw->Items->Count)
    return;

  SendAutoFlwOnOffMsg(g_CurSelFlw, 1);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N19Click(TObject *Sender)
{
  if (g_CurSelFlw < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要操作的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶巨瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }

  if (g_CurSelFlw >= lvLoadFlw->Items->Count)
    return;

  SendAutoFlwOnOffMsg(g_CurSelFlw, 0);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbSkinsChange(TObject *Sender)
{
  if (cbSkins->Text == ((gLangID == 1) ? "无显示风格" : "礚陪ボ"))
  {
    WriteSkins(0);
    return;
  }
  for (int i=1; i<11; i++)
  {
    if (cbSkins->Text == strSkinName[i])
    {
      strSelSkinFile = ExtractFilePath(Application->ExeName)+"Skins\\"+strSkinFile[i];
      if (FileExists(strSelSkinFile) == true)
      {
        suiSkinEngine1->SkinFile = strSelSkinFile;
        suiSkinEngine1->Active = true;
        WriteSkins(i);
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallDBserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "DBSERVICE\\QuarkCallDBService.exe -install";
  #else
  strTemp = strTemp + "DBSERVICE\\UnimeCallDBServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshDBServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,DB_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgDBServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartDBService->Enabled = false;
        btStopDBService->Enabled = true;
      }
      else
      {
        imgDBServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartDBService->Enabled = true;
        btStopDBService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartDBServiceClick(TObject *Sender)
{
  if (g_nRunMode == 2)
  {
    g_pTcpClient->CTRLSendMessage(15, "Start QuarkCallDBService!");
  }
  else
  {
    SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
    if (scm!=NULL)
    {
      SC_HANDLE svc = OpenService(scm, DB_SERVICE_NAME, SERVICE_START);
      if (svc != NULL)
      {
        if (StartService(svc, 0, NULL))
        {
          imgDBServiceStatus->Picture->LoadFromFile(imgStartFile);
          btStartDBService->Enabled = false;
          btStopDBService->Enabled = true;
        }
        else
        {
          if (gLangID == 1)
            MessageBox(NULL,"启动DB服务失败!!!","警告",MB_OK|MB_ICONWARNING);
          else
            MessageBox(NULL,"币笆DB狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
        }
        CloseServiceHandle(svc);
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"DB服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"DB狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(scm);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopDBServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nRunMode == 2)
  {
    g_pTcpClient->CTRLSendMessage(16, "Stop QuarkCallDBService!");
  }
  else
  {
    SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
    if (scm != NULL)
    {
      SC_HANDLE svc = OpenService(scm,DB_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
      if (svc!=NULL)
      {
        SERVICE_STATUS ServiceStatus;
        QueryServiceStatus(svc, &ServiceStatus);
        if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
        {
          if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
          {
            imgDBServiceStatus->Picture->LoadFromFile(imgStopFile);
            btStartDBService->Enabled = true;
            btStopDBService->Enabled = false;
          }
        }
        CloseServiceHandle(svc);
      }
      CloseServiceHandle(scm);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbOSTypeChange(TObject *Sender)
{
  bLOGParamChange = true;
  btApply->Enabled = true;
  if (cbOSType->Text == "LINUX")
  {
    ckIsTheSamePC->Visible = false;
    Label35->Visible = true;
    cseLinuxPort->Visible = true;
  }
  else
  {
    ckIsTheSamePC->Visible = true;
    Label35->Visible = false;
    cseLinuxPort->Visible = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::editIVRIPChange(TObject *Sender)
{
  bIPParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::rbAutoGetIPClick(TObject *Sender)
{
  bIPParamChange = true;
  btApply->Enabled = true;
  if (rbAutoGetIP->Checked == true)
  {
    editIVRIP->Text = "";
    editMark->Text = "";
    editGWIP->Text = "";
    editIVRIP->Enabled = false;
    editMark->Enabled = false;
    editGWIP->Enabled = false;
  }
  else
  {
    editIVRIP->Enabled = true;
    editMark->Enabled = true;
    editGWIP->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::rbStaticIPClick(TObject *Sender)
{
  bIPParamChange = true;
  btApply->Enabled = true;
  if (rbStaticIP->Checked == true)
  {
    editIVRIP->Text = strServerIp[4];
    if (strServerIp[5] == "")
      editMark->Text = "255.255.255.0";
    else
      editMark->Text = strServerIp[5];
    editGWIP->Text = strServerIp[6];
    editIVRIP->Enabled = true;
    editMark->Enabled = true;
    editGWIP->Enabled = true;
  }
  else
  {
    editIVRIP->Text = "";
    editMark->Text = "";
    editGWIP->Text = "";
    editIVRIP->Enabled = false;
    editMark->Enabled = false;
    editGWIP->Enabled = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::rbAutoGetDNSClick(TObject *Sender)
{
  bIPParamChange = true;
  btApply->Enabled = true;
  if (rbAutoGetDNS->Checked == true)
  {
    editDNS1->Text = "";
    editDNS2->Text = "";
    editDNS1->Enabled = false;
    editDNS2->Enabled = false;
  }
  else
  {
    editDNS1->Text = strServerDns[0];
    editDNS2->Text = strServerDns[1];
    editDNS1->Enabled = true;
    editDNS2->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::rbStaticDNSClick(TObject *Sender)
{
  bIPParamChange = true;
  btApply->Enabled = true;
  if (rbStaticDNS->Checked == true)
  {
    editDNS1->Text = strServerDns[0];
    editDNS2->Text = strServerDns[1];
    editDNS1->Enabled = true;
    editDNS2->Enabled = true;
  }
  else
  {
    editDNS1->Text = "";
    editDNS2->Text = "";
    editDNS1->Enabled = false;
    editDNS2->Enabled = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::editConnectIVRIPChange(TObject *Sender)
{
  bLOGParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallSMSserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "DBSERVICE\\QuarkCallSMSService.exe -install";
  #else
  strTemp = strTemp + "DBSERVICE\\UnimeCallSMSServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshSMSServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,SMS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgSMSServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartSMSService->Enabled = false;
        btStopSMSService->Enabled = true;
      }
      else
      {
        imgSMSServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartSMSService->Enabled = true;
        btStopSMSService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartSMSServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, SMS_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgSMSServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartSMSService->Enabled = false;
        btStopSMSService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动SMS服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆SMS狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"SMS服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"SMS狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopSMSServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,SMS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgSMSServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartSMSService->Enabled = true;
          btStopSMSService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallDOCserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "DBSERVICE\\QuarkCallDOCService.exe -install";
  #else
  strTemp = strTemp + "DBSERVICE\\UnimeCallDOCServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshDOCServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,DOC_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgDOCServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartDOCService->Enabled = false;
        btStopDOCService->Enabled = true;
      }
      else
      {
        imgDOCServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartDOCService->Enabled = true;
        btStopDOCService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartDOCServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, DOC_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgDOCServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartDOCService->Enabled = false;
        btStopDOCService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动DOC服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆DOC狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"DOC服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"DOC狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopDOCServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,DOC_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgDOCServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartDOCService->Enabled = true;
          btStopDOCService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvParamSel1SelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;

  if (g_nLoginlevelId == 4)
  {
    Notebook1->PageIndex = 0;
    g_nCurrParamSelName[0] = (gLangID == 1) ? "监控端参数" : "菏北狠把计";
    g_nCurrParamSelIndex[0] = 0;
    editSelitem->Text = (gLangID == 1) ? "监控端参数" : "菏北狠把计";
    //editSelitem->SetFocus();
    //SetlvParamSelImage1(Item->Index);
    //FormMain->WindowState = wsNormal;
    if (bLOGParamChange == true)
      btApply->Enabled = true;
    else
      btApply->Enabled = false;
    return;
  }

  switch (Item->Index)
  {
  case 0: //监控端参数
    Notebook1->PageIndex = 0;
    g_nCurrParamSelName[0] = Item->Caption;
    g_nCurrParamSelIndex[0] = 0;
    editSelitem->Text = Item->Caption;
    //editSelitem->SetFocus();
    //SetlvParamSelImage1(Item->Index);
    //FormMain->WindowState = wsNormal;
    if (bLOGParamChange == true)
      btApply->Enabled = true;
    else
      btApply->Enabled = false;
    break;
  case 1: //服务程序状态
    Notebook1->PageIndex = 1;
    g_nCurrParamSelName[0] = Item->Caption;
    g_nCurrParamSelIndex[0] = 1;
    editSelitem->Text = Item->Caption;
    //editSelitem->SetFocus();
    //SetlvParamSelImage1(Item->Index);
    //FormMain->WindowState = wsNormal;
    if (bSaveMsgIdParamChange == true || bLOGParamChange == true)
      btApply->Enabled = true;
    else
      btApply->Enabled = false;
    break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvParamSel2SelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;

  //if (g_nLoginId == 0)
  //  return;
  switch (Item->Index)
  {
  case 0: //平台网络参数
    //if (g_nConnectIVRId == 4 || g_nConnectCTRLId == 4)
    {
      Notebook1->PageIndex = 2;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 2;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bTCPParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  case 1: //流程参数
    //if (g_nConnectFLWId == 4)
    {
      Notebook1->PageIndex = 3;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 3;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bFLWParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  case 2: //模拟外线参数
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 4;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 4;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bPSTNParamChange == true || bSwitchPortParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  case 3: //呼出路由参数
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 5;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 5;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bROUTEParamChange == true || bVopParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  case 4: //坐席参数
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 6;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 6;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bSeatParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  case 5: //远端坐席参数
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 7;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 7;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bExtSeatParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  case 6: //放音路径参数
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 8;
      g_nCurrParamSelName[1] = Item->Caption;
      g_nCurrParamSelIndex[1] = 8;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage2(Item->Index);
      //FormMain->WindowState = wsNormal;
      if (bOtherParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvParamSel3SelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nCurrParamSelIndex[2] = Item->Index;

  if (g_nLoginId == 0)
    return;
  switch (Item->Index)
  {
  case 0: //通道状态显示
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 9;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 9;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage3(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 1: //坐席状态显示
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 10;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 10;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage3(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 2: //坐席实时话务
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 19;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 19;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage3(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 3: //坐席实时话务
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 20;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 20;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage3(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 4: //ACD排队信息
    //if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 11;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 11;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage3(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 5: //流程加载信息
    //if (g_nConnectFLWId == 4)
    {
      Notebook1->PageIndex = 13;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 13;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage3(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 6:
    {
      Notebook1->PageIndex = 18;
      g_nCurrParamSelName[2] = Item->Caption;
      g_nCurrParamSelIndex[2] = 18;
      editSelitem->Text = Item->Caption;
      if (bAlartParamChange == true)
        btApply->Enabled = true;
      else
        btApply->Enabled = false;
    }
    break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvParamSel4SelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nCurrParamSelIndex[3] = Item->Index;

  if (g_nLoginId == 0)
    return;
  switch (Item->Index)
  {
  case 0: //IVR跟踪消息
    if (g_nConnectIVRId == 4)
    {
      Notebook1->PageIndex = 12;
      g_nCurrParamSelName[3] = Item->Caption;
      g_nCurrParamSelIndex[3] = 12;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage4(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  case 1: //流程跟踪消息
    if (g_nConnectFLWId == 4)
    {
      Notebook1->PageIndex = 14;
      g_nCurrParamSelIndex[3] = 14;
      editSelitem->Text = Item->Caption;
      //editSelitem->SetFocus();
      //SetlvParamSelImage4(Item->Index);
      //FormMain->WindowState = wsMaximized;
      btApply->Enabled = false;
    }
    break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvParamSel5SelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nCurrParamSelIndex[4] = Item->Index;

  if (g_nLoginId == 0)
    return;
  switch (Item->Index)
  {
  case 0: //中继占用统计
    Notebook1->PageIndex = 15;
    g_nCurrParamSelName[4] = Item->Caption;
    g_nCurrParamSelIndex[4] = 15;
    editSelitem->Text = Item->Caption;
    //editSelitem->SetFocus();
    //SetlvParamSelImage5(Item->Index);
    //FormMain->WindowState = wsMaximized;
    btApply->Enabled = false;
    break;
  case 1: //坐席占用统计
    Notebook1->PageIndex = 16;
    g_nCurrParamSelName[4] = Item->Caption;
    g_nCurrParamSelIndex[4] = 16;
    editSelitem->Text = Item->Caption;
    //editSelitem->SetFocus();
    //SetlvParamSelImage5(Item->Index);
    //FormMain->WindowState = wsMaximized;
    btApply->Enabled = false;
    break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::SpeedButton5Click(TObject *Sender)
{
  if (g_nLoginId == 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，请先登录!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬叫祅魁!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nLoginlevelId != 1 && g_nLoginlevelId != 2)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  m_MyToolsBox.RemoveToolsBox(4);
  //SetlvParamSelImage5(0);
  Notebook1->PageIndex = 15;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btLoginClick(TObject *Sender)
{
  if (g_bRunOnServicePCId == false && g_nConnectIVRId != 3 && g_nConnectIVRId != 4)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，与平台服务网络连接失败,请先启动服务程序!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬籔キ籓狝叭呼蹈硈钡ア毖,叫币笆狝叭祘!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nLoginId == 0 || g_bRunOnServicePCId == true)
  {
    FormLogin->cbUserName->Text = g_strLoginUsername;
    FormLogin->editPassword->Text = g_strPassword;
    FormLogin->ckAutoLogin->Checked = g_bAutoLoginId;
    FormLogin->ShowModal();
  }
  else
  {
    SendLogoutIVRMsg();
    g_nLoginId = 0;
    btLogin->Caption = (gLangID == 1) ? "登录" : "祅魁";
    Notebook1->PageIndex = 17;
    btModifyPsw->Enabled = false;
    m_MyToolsBox.RemoveToolsBox(0);
    Notebook1->PageIndex = 17;
    FormMain->Caption = strAPPTitle;
    /*if (gLangID == 1)
    {
      #ifdef QUARKCALL_PLATFORM
      FormMain->Caption = strAPPTitle;
      #else
      FormMain->Caption = "Unime平台配置监控器";
      #endif
    }
    else
    {
      #ifdef QUARKCALL_PLATFORM
      FormMain->Caption = "QuarkCallе㊣皌竚菏北竟";
      #else
      FormMain->Caption = "Unimeキ籓皌竚菏北竟";
      #endif
    }*/
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btModifyPswClick(TObject *Sender)
{
  if (g_nLoginId == 0)
    return;
  FormModifyPSW->cbUserName->Items->Clear();
  switch (g_nLoginlevelId)
  {
  case 1:
    FormModifyPSW->cbUserName->Items->Add("administrator");
    FormModifyPSW->cbUserName->Items->Add("manager");
    FormModifyPSW->cbUserName->Items->Add("operator");
    FormModifyPSW->cbUserName->Items->Add("monitor");
    FormModifyPSW->cbUserName->Items->Add("board");
    break;
  case 2:
    FormModifyPSW->cbUserName->Items->Add("manager");
    FormModifyPSW->cbUserName->Items->Add("operator");
    FormModifyPSW->cbUserName->Items->Add("monitor");
    FormModifyPSW->cbUserName->Items->Add("board");
    break;
  case 3:
    FormModifyPSW->cbUserName->Items->Add("operator");
    break;
  case 4:
    FormModifyPSW->cbUserName->Items->Add("monitor");
    break;
  case 5:
    FormModifyPSW->cbUserName->Items->Add("board");
    break;
  }
  FormModifyPSW->cbUserName->Text = g_strLoginUsername;
  FormModifyPSW->editOldPassword->Text = "";
  FormModifyPSW->editNewPassword->Text = "";
  FormModifyPSW->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn1Click(TObject *Sender)
{
  editSelitem->Text = g_nCurrParamSelName[0];
  editSelitem->SetFocus();
  g_CurSelToolButton = 0;
  m_MyToolsBox.RemoveToolsBox(0);
  //SetlvParamSelImage1(0);
  Notebook1->PageIndex = g_nCurrParamSelIndex[0];
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn2Click(TObject *Sender)
{
  if (g_nLoginId == 0)
  {
    if (g_bRunOnServicePCId == true)
    {
      editSelitem->Text = g_nCurrParamSelName[1];
      editSelitem->SetFocus();
      g_CurSelToolButton = 1;
      m_MyToolsBox.RemoveToolsBox(1);
      Notebook1->PageIndex = g_nCurrParamSelIndex[1];

      /*AnsiString inPSW="administrator";

      if (gLangID == 1)
      {
        if (InputQuery("登录提示", "请输入administrator的登录密码：", inPSW) == false)
          return;
      }
      else
      {
        if (InputQuery("祅魁矗ボ", "叫块administrator祅魁盞絏:", inPSW) == false)
          return;
      }
      if (inPSW == g_strAdminPSW)
      {
        editSelitem->Text = g_nCurrParamSelName[1];
        editSelitem->SetFocus();
        g_CurSelToolButton = 1;
        m_MyToolsBox.RemoveToolsBox(1);
        Notebook1->PageIndex = g_nCurrParamSelIndex[1];
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"对不起，输入的administrator登录密码!","信息提示",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"癸ぃ癬块administrator祅魁盞絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
      }*/
      return;
    }
    if (gLangID == 1)
      MessageBox(NULL,"对不起，请先登录!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬叫祅魁!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  editSelitem->Text = g_nCurrParamSelName[1];
  editSelitem->SetFocus();
  g_CurSelToolButton = 1;
  m_MyToolsBox.RemoveToolsBox(1);
  //SetlvParamSelImage2(0);
  Notebook1->PageIndex = g_nCurrParamSelIndex[1];
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn3Click(TObject *Sender)
{
  if (g_nLoginId == 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，请先登录!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬叫祅魁!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  editSelitem->Text = g_nCurrParamSelName[2];
  editSelitem->SetFocus();
  g_CurSelToolButton = 2;
  m_MyToolsBox.RemoveToolsBox(2);
  //SetlvParamSelImage3(0);
  Notebook1->PageIndex = g_nCurrParamSelIndex[2];
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn4Click(TObject *Sender)
{
  if (g_nLoginId == 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，请先登录!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬叫祅魁!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nLoginlevelId != 1 && g_nLoginlevelId != 2)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  editSelitem->Text = g_nCurrParamSelName[3];
  editSelitem->SetFocus();
  g_CurSelToolButton = 3;
  m_MyToolsBox.RemoveToolsBox(3);
  //SetlvParamSelImage4(0);
  Notebook1->PageIndex = g_nCurrParamSelIndex[3];
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn5Click(TObject *Sender)
{
  if (g_nLoginId == 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，请先登录!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬叫祅魁!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_nLoginlevelId != 1 && g_nLoginlevelId != 2)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  editSelitem->Text = g_nCurrParamSelName[4];
  editSelitem->SetFocus();
  g_CurSelToolButton = 4;
  m_MyToolsBox.RemoveToolsBox(4);
  //SetlvParamSelImage5(0);
  Notebook1->PageIndex = g_nCurrParamSelIndex[4];
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvParamSel1MouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  editSelitem->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallWEBserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "WEBSERVICE\\QuarkCallWEBService.exe -install";
  #else
  strTemp = strTemp + "WEBSERVICE\\UnimeCallWEBServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshWEBServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,WEB_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgWEBServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartWEBService->Enabled = false;
        btStopWEBService->Enabled = true;
      }
      else
      {
        imgWEBServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartWEBService->Enabled = true;
        btStopWEBService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartWEBServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, WEB_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgWEBServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartWEBService->Enabled = false;
        btStopWEBService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动WEB服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆WEB狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"WEB服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"WEB狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopWEBServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,WEB_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgWEBServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartWEBService->Enabled = true;
          btStopWEBService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormResize(TObject *Sender)
{
  m_MyToolsBox.RemoveToolsBox(g_CurSelToolButton);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn6Click(TObject *Sender)
{
  FormFlw->SetModifyType(false);
  FormFlw->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn7Click(TObject *Sender)
{
  FormAccCode->SetModifyType(false);
  FormAccCode->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn8Click(TObject *Sender)
{
  if (g_CurFlwNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶э瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormFlw->SetModifyType(true, FlwFileList.FlwFile[g_CurFlwNo].FlwFile,
                         FlwFileList.FlwFile[g_CurFlwNo].Explain,
                         FlwFileList.FlwFile[g_CurFlwNo].FuncNo,
                         FlwFileList.FlwFile[g_CurFlwNo].RunAfterLoad);
  FormFlw->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn9Click(TObject *Sender)
{
  if (g_CurFlwNo < 0 || g_CurAccNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的流程接入号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶э瑈祘钡腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormAccCode->SetModifyType(true, FlwFileList.FlwFile[g_CurFlwNo].AccessCode[g_CurAccNo].PreCalled,
                         FlwFileList.FlwFile[g_CurFlwNo].AccessCode[g_CurAccNo].MinLen,
                         FlwFileList.FlwFile[g_CurFlwNo].AccessCode[g_CurAccNo].MaxLen);
  FormAccCode->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn10Click(TObject *Sender)
{
  char dispbuf[256];

  if (g_CurFlwNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要删除的流程!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶埃瑈祘!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (gLangID == 1)
  {
    sprintf(dispbuf, "您真的要删除 %s 流程吗?", FlwFileList.FlwFile[g_CurFlwNo].FlwFile.c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  else
  {
    sprintf(dispbuf, "眤痷璶埃 %s 瑈祘盾?", FlwFileList.FlwFile[g_CurFlwNo].FlwFile.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  FlwFileList.DelFlwFile(g_CurFlwNo);
  FormMain->DispFlw();
  bFLWParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn11Click(TObject *Sender)
{
  char dispbuf[256];

  if (g_CurFlwNo < 0 || g_CurAccNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要删除的流程接入号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶埃瑈祘钡腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (gLangID == 1)
  {
    sprintf(dispbuf, "您真的要删除 %s 流程的接入号 %s 吗?", FlwFileList.FlwFile[g_CurFlwNo].FlwFile.c_str(), FlwFileList.FlwFile[g_CurFlwNo].AccessCode[g_CurAccNo].PreCalled.c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  else
  {
    sprintf(dispbuf, "眤痷璶埃 %s 瑈祘钡腹 %s 盾?", FlwFileList.FlwFile[g_CurFlwNo].FlwFile.c_str(), FlwFileList.FlwFile[g_CurFlwNo].AccessCode[g_CurAccNo].PreCalled.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  FlwFileList.FlwFile[g_CurFlwNo].DelAccCode(g_CurAccNo);
  DispAccCode(FlwFileList.FlwFile[g_CurFlwNo]);
  bFLWParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn21Click(TObject *Sender)
{
  CExtSeat extseat;
  FormExtSeat->SetModifyType(false, extseat);
  FormExtSeat->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn22Click(TObject *Sender)
{
  if (g_CurExtSeatPort < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶э畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormExtSeat->SetModifyType(true, ExtSeatList.ExtSeat[g_CurExtSeatPort]);
  FormExtSeat->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn23Click(TObject *Sender)
{
  if (ExtSeatList.ExtSeatNum > 0)
  {
    ExtSeatList.ExtSeatNum--;
    DispExtSeat();
    bExtSeatParamChange = true;
    btApply->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn18Click(TObject *Sender)
{
  CSeat seat;

  seat.CscNo = SeatList.SeatNum;
  seat.SeatNo = "";
  seat.SeatType = 1;
  seat.SeatGroupNo = 1;
  seat.GroupNo = "1";
  seat.WorkerLevel = 1;
  seat.WorkerNo = "";
  seat.WorkerName = "";
  seat.SeatIP = "";
  seat.ModifyId = false;

  FormSeat->SetModifyType(1, seat);
  FormSeat->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn32Click(TObject *Sender)
{
  CSeat seat;

  if (g_CurSeatPort < 0)
  {
    seat.CscNo = SeatList.SeatNum;
    seat.SeatNo = "";
    seat.SeatType = 1;
    seat.SeatGroupNo = 1;
    seat.GroupNo = "1";
    seat.WorkerLevel = 1;
    seat.WorkerNo = "";
    seat.WorkerName = "";
    seat.SeatIP = "";
    seat.ModifyId = false;

    FormSeat->SetModifyType(1, seat);
    FormSeat->ShowModal();
  }
  else
  {
    seat.CscNo = g_CurSeatPort;
    seat.SeatNo = "";
    seat.SeatType = 1;
    seat.SeatGroupNo = 1;
    seat.GroupNo = "1";
    seat.WorkerLevel = 1;
    seat.WorkerNo = "";
    seat.WorkerName = "";
    seat.SeatIP = "";
    seat.ModifyId = false;

    FormSeat->SetModifyType(2, seat);
    FormSeat->ShowModal();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn19Click(TObject *Sender)
{
  if (g_CurSeatPort < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的坐席!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶э畒畊!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormSeat->SetModifyType(3, SeatList.Seat[g_CurSeatPort]);
  FormSeat->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn20Click(TObject *Sender)
{
  char dispbuf[256];

  if (g_CurSeatPort < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要删除的坐席端口序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶埃畒畊硈钡梆腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (gLangID == 1)
  {
    sprintf(dispbuf, "您真的要将坐席 %s  删除吗?", SeatList.Seat[g_CurSeatPort].SeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      g_bSeatModifyId = true;
      SeatList.DelSeat(g_CurSeatPort);
      bSeatParamChange = true;
      btApply->Enabled = true;
      DispSeat();
    }
  }
  else
  {
    sprintf(dispbuf, "眤痷璶盢畒畊 %s  埃盾?", SeatList.Seat[g_CurSeatPort].SeatNo.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      g_bSeatModifyId = true;
      SeatList.DelSeat(g_CurSeatPort);
      bSeatParamChange = true;
      btApply->Enabled = true;
      DispSeat();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn15Click(TObject *Sender)
{
  FormRoute->SetModifyType(false);
  FormRoute->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn16Click(TObject *Sender)
{
  if (g_CurRouteNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的路由!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶э隔パ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormRoute->SetModifyType(true, g_CurRouteNo,
                         RouteList.Route[g_CurRouteNo].SelMode,
                         RouteList.Route[g_CurRouteNo].TrkStr,
                         RouteList.Route[g_CurRouteNo].Remark,
                         RouteList.Route[g_CurRouteNo].IPPreCode);
  FormRoute->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn17Click(TObject *Sender)
{
  if (RouteList.RouteNum > 0)
  {
    RouteList.Route[RouteList.RouteNum].SelMode = 0;
    RouteList.Route[RouteList.RouteNum].TrkStr = "";
    RouteList.Route[RouteList.RouteNum].IPPreCode = "";
    RouteList.Route[RouteList.RouteNum].Remark = "";
    RouteList.Route[RouteList.RouteNum].bModifyId = true;
    RouteList.RouteNum--;
    DispRoute();
    bROUTEParamChange = true;
    btApply->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn12Click(TObject *Sender)
{
  FormPSTN->SetModifyType(false);
  FormPSTN->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn13Click(TObject *Sender)
{
  if (g_CurPstn < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的模拟外线序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶э摸ゑ絬腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormPSTN->SetModifyType(true, g_CurPstn, PstnLine.PSTNCode[g_CurPstn], PstnLine.PreCode[g_CurPstn], PstnLine.IPPreCode[g_CurPstn], PstnLine.NotAddPreCode[g_CurPstn]);
  FormPSTN->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn14Click(TObject *Sender)
{
  if (PstnLine.PSTNNum > 0)
  {
    PstnLine.PSTNNum--;
    PstnLine.PSTNCode[PstnLine.PSTNNum] = "";
    PstnLine.PreCode[PstnLine.PSTNNum] = "";
    PstnLine.IPPreCode[PstnLine.PSTNNum] = "";
    PstnLine.NotAddPreCode[PstnLine.PSTNNum] = "";
    PstnLine.bModifyId[PstnLine.PSTNNum] = true;
    DispPSTN();
    bPSTNParamChange = true;
    btApply->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallTTSserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "TTSSERVICE\\QuarkCallTTSService.exe -install";
  #else
  strTemp = strTemp + "TTSSERVICE\\UnimeCallTTSServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshTTSServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,TTS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgTTSServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartTTSService->Enabled = false;
        btStopTTSService->Enabled = true;
      }
      else
      {
        imgTTSServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartTTSService->Enabled = true;
        btStopTTSService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartTTSServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, TTS_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgTTSServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartTTSService->Enabled = false;
        btStopTTSService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动TTS服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆TTS狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"TTS服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"TTS狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopTTSServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,TTS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgTTSServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartTTSService->Enabled = true;
          btStopTTSService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallLINKserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "CTILINKSERVICE\\QuarkCallCTILINKService.exe -install";
  #else
  strTemp = strTemp + "CTILINKSERVICE\\UnimeCTILINKServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshLINKServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,CTILINK_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgLINKServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartLINKService->Enabled = false;
        btStopLINKService->Enabled = true;
      }
      else
      {
        imgLINKServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartLINKService->Enabled = true;
        btStopLINKService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartLINKServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, CTILINK_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgLINKServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartLINKService->Enabled = false;
        btStopLINKService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动CTI-LINK服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆CTI-LINK狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"CTI-LINK服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"CTI-LINK狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopLINKServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,CTILINK_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgLINKServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartLINKService->Enabled = true;
          btStopLINKService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbSwitchTypeChange(TObject *Sender)
{
  TListItem  *ListItem1;
  if (cbSwitchType->Text == ((gLangID == 1) ? "无" : "礚"))
  {
    Notebook2->ActivePage = "BoardTrunk";
    Notebook3->ActivePage = "BoardRoute";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = ((gLangID == 1) ? "模拟外线参数" : "摸ゑ絬把计");
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = ((gLangID == 1) ? "呼出路由资源" : "挤隔パ硄笵");
    g_nSwitchMode = 0;
    GroupBox12->Visible = false;
  }
  else
  {
    if (cbSwitchType->Text == ((gLangID == 1) ? "AVAYA-IPO系列" : "AVAYA-IPO╰"))
    {
      g_nSwitchMode = 1;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = true;
      cbCTILinkType->Visible = true;
      btTAPI->Visible = true;
      btUnistallTAPI->Visible = true;
      lbSwitchIP->Visible = true;
      editSwitchIP->Visible = true;
      lvSwitchPort->PopupMenu = PopupMenu6;

      lbServerID->Visible = false;
      editServerID->Visible = false;
      lbLoginID->Visible = false;
      editLoginID->Visible = false;
      lbPassword->Visible = false;
      editPassword->Visible = false;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "AVAYA-S系列" : "AVAYA-S╰"))
    {
      g_nSwitchMode = 2;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = true;
      editServerID->Visible = true;
      lbLoginID->Visible = true;
      editLoginID->Visible = true;
      lbPassword->Visible = true;
      editPassword->Visible = true;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "Alcatel-OXO系列" : "Alcatel-OXO╰"))
    {
      g_nSwitchMode = 3;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = false;
      editServerID->Visible = false;
      lbLoginID->Visible = false;
      editLoginID->Visible = false;
      lbPassword->Visible = false;
      editPassword->Visible = false;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "Alcatel-OXE系列" : "Alcatel-OXE╰"))
    {
      g_nSwitchMode = 4;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = true;
      editServerID->Visible = true;
      lbLoginID->Visible = true;
      editLoginID->Visible = true;
      lbPassword->Visible = true;
      editPassword->Visible = true;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "Simens系列" : "Simens╰"))
    {
      g_nSwitchMode = 5;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = true;
      editServerID->Visible = true;
      lbLoginID->Visible = true;
      editLoginID->Visible = true;
      lbPassword->Visible = true;
      editPassword->Visible = true;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "Panasonic系列" : "Panasonic╰"))
    {
      g_nSwitchMode = 6;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = true;
      editServerID->Visible = true;
      lbLoginID->Visible = true;
      editLoginID->Visible = true;
      lbPassword->Visible = true;
      editPassword->Visible = true;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "Cisco系列" : "Cisco╰"))
    {
      g_nSwitchMode = 12;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = true;
      editServerID->Visible = true;
      lbLoginID->Visible = true;
      editLoginID->Visible = true;
      lbPassword->Visible = true;
      editPassword->Visible = true;
    }
    else if (cbSwitchType->Text == ((gLangID == 1) ? "普通PBX" : "炊硄PBX"))
    {
      g_nSwitchMode = 10;
      GroupBox12->Visible = false;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = false;
      editServerID->Visible = false;
      lbLoginID->Visible = false;
      editLoginID->Visible = false;
      lbPassword->Visible = false;
      editPassword->Visible = false;
    }
    else
    {
      g_nSwitchMode = 1;
      GroupBox12->Visible = true;

      lbCTILinkType->Visible = false;
      cbCTILinkType->Visible = false;
      btTAPI->Visible = false;
      btUnistallTAPI->Visible = false;
      lbSwitchIP->Visible = false;
      editSwitchIP->Visible = false;
      lvSwitchPort->PopupMenu = NULL;

      lbServerID->Visible = false;
      editServerID->Visible = false;
      lbLoginID->Visible = false;
      editLoginID->Visible = false;
      lbPassword->Visible = false;
      editPassword->Visible = false;
    }
    Notebook2->ActivePage = "SwitchPort";
    Notebook3->ActivePage = "SwitchVop";
    ListItem1 = lvParamSel2->Items->Item[2];
    ListItem1->Caption = ((gLangID == 1) ? "交换机端口参数" : "ユ传诀硈钡梆把计");
    ListItem1 = lvParamSel2->Items->Item[3];
    ListItem1->Caption = ((gLangID == 1) ? "交换机外接资源" : "ユ传诀场硄笵");
  }
  bTCPParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cseSelVopNoChange(TObject *Sender)
{
  DispVop();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvVopChnSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_CurVopChnNo = Item->Index;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn28Click(TObject *Sender)
{
  //增加
  CVopChn VopChn;

  if (cseVopNum->Value <= cseSelVopNo->Value)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，资源节点总数比当前选择的节点编号小，请增加资源节点总数!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬场IVR竊翴羆计ゑ讽玡匡拒竊翴絪腹叫糤场IVR竊翴羆计!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (ckVopOpen->Checked == false)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，当前选择的节点未启用!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬讽玡匡拒竊翴ゼ币ノ!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  VopChn.m_nVopNo = g_CurVopNo;
  VopChn.m_nVopChnNo = VopGroup.m_Vops[g_CurVopNo].m_nVopChnNum;
  VopChn.m_nVopChnType = 0;
  VopChn.m_PortNo = -1;
  VopChn.m_strPortID = "";
  FormEditVopChn->SetModifyType(1, VopChn);
  FormEditVopChn->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn29Click(TObject *Sender)
{
  CVopChn VopChn;

  if (cseVopNum->Value <= cseSelVopNo->Value)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，资源节点总数比当前选择的节点编号小，请增加资源节点总数!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬场IVR竊翴羆计ゑ讽玡匡拒竊翴絪腹叫糤场IVR竊翴羆计!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (ckVopOpen->Checked == false)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，当前选择的节点未启用!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬讽玡匡拒竊翴ゼ币ノ!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_CurVopChnNo < 0)
  {
    VopChn.m_nVopNo = g_CurVopNo;
    VopChn.m_nVopChnNo = VopGroup.m_Vops[g_CurVopNo].m_nVopChnNum;
    VopChn.m_nVopChnType = 0;
    VopChn.m_PortNo = -1;
    VopChn.m_strPortID = "";
    FormEditVopChn->SetModifyType(1, VopChn);
    FormEditVopChn->ShowModal();
  }
  else
  {
    VopChn.m_nVopNo = g_CurVopNo;
    VopChn.m_nVopChnNo = g_CurVopChnNo;
    VopChn.m_nVopChnType = 0;
    VopChn.m_PortNo = -1;
    VopChn.m_strPortID = "";
    FormEditVopChn->SetModifyType(2, VopChn);
    FormEditVopChn->ShowModal();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn31Click(TObject *Sender)
{
  if (cseVopNum->Value <= cseSelVopNo->Value)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，资源节点总数比当前选择的节点编号小，请增加资源节点总数!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬场IVR竊翴羆计ゑ讽玡匡拒竊翴絪腹叫糤场IVR竊翴羆计!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (ckVopOpen->Checked == false)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，当前选择的节点未启用!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬讽玡匡拒竊翴ゼ币ノ!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (g_CurVopChnNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的交换机资源通道序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶эユ传诀场IVR硄笵腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  FormEditVopChn->SetModifyType(3, VopGroup.m_Vops[g_CurVopNo].m_VopChns[g_CurVopChnNo]);
  FormEditVopChn->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::BitBtn30Click(TObject *Sender)
{
  char dispbuf[256];

  if (g_CurVopChnNo < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要修改的交换机资源通道序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶эユ传诀场IVR硄笵腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (ckVopOpen->Checked == false)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，当前选择的节点未启用!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬讽玡匡拒竊翴ゼ币ノ!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  if (gLangID == 1)
  {
    sprintf(dispbuf, "您真的要将交换机资源通道 %s  删除吗?", VopGroup.m_Vops[g_CurVopNo].m_VopChns[g_CurVopChnNo].m_strPortID.c_str());
    if ( MessageBox( NULL, dispbuf, "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      VopGroup.m_Vops[g_CurVopNo].DelVopChn(g_CurVopChnNo);
      bVopParamChange = true;
      btApply->Enabled = true;
      DispVop();
    }
  }
  else
  {
    sprintf(dispbuf, "眤痷璶盢ユ传诀场IVR硄笵 %s  埃盾?", VopGroup.m_Vops[g_CurVopNo].m_VopChns[g_CurVopChnNo].m_strPortID.c_str());
    if ( MessageBox( NULL, dispbuf, "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDYES )
    {
      VopGroup.m_Vops[g_CurVopNo].DelVopChn(g_CurVopChnNo);
      bVopParamChange = true;
      btApply->Enabled = true;
      DispVop();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckVopOpenClick(TObject *Sender)
{
  if (ckVopOpen->Checked == true)
  {
    VopGroup.m_Vops[g_CurVopNo].m_nValid = 1;
  }
  else
  {
    VopGroup.m_Vops[g_CurVopNo].m_nValid = 0;
  }
  //DispVop();
}
//---------------------------------------------------------------------------
void TFormMain::ReadWorkerCountItemSet()
{
  AnsiString strSection;
  TIniFile *IniKey = new TIniFile (LOGINIFile);

  if (gLangID == 1)
    strSection = "WORKERSTATUS";
  else
    strSection = "WORKERSTATUS-CHT";

  WorkerCountItemSetList.ItemNum = IniKey->ReadInteger("WORKERSTATUS", "ITEMNUM", 0);
  for (int i = 0; i < WorkerCountItemSetList.ItemNum; i ++)
  {
    WorkerCountItemSetList.CallCountItemSet[i].ItemNo = i;
    WorkerCountItemSetList.CallCountItemSet[i].ItemName = IniKey->ReadString(strSection, "ITEM["+IntToStr(i+1)+"].NAME", ((gLangID == 1) ? "统计项目[" : "参璸兜ヘ[")+IntToStr(i+1)+"]");
    WorkerCountItemSetList.CallCountItemSet[i].DispId = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].DISP", 0);
    WorkerCountItemSetList.CallCountItemSet[i].AlartId = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartId", 0);
    WorkerCountItemSetList.CallCountItemSet[i].AlartValue = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartValue", 0);
    WorkerCountItemSetList.CallCountItemSet[i].CompType = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].CompType", 0);
    WorkerCountItemSetList.CallCountItemSet[i].ClearAlartId = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].ClearAlartId", 0);
    WorkerCountItemSetList.CallCountItemSet[i].AlartColor = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartColor", 0);
    WorkerCountItemSetList.CallCountItemSet[i].AlartSound = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartSound", 0);
    WorkerCountItemSetList.CallCountItemSet[i].ValueType = IniKey->ReadInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].TYPE", 0);

    WorkerCountItemSetList.CallCountItemSet[i].ItemListIndex = -1;
  }

  delete IniKey;
}
void TFormMain::WriteWorkerCountItemSet()
{
  TIniFile *IniKey = new TIniFile (LOGINIFile);

  for (int i = 0; i < WorkerCountItemSetList.ItemNum; i ++)
  {
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].DISP", WorkerCountItemSetList.CallCountItemSet[i].DispId);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartId", WorkerCountItemSetList.CallCountItemSet[i].AlartId);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartValue", WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].CompType", WorkerCountItemSetList.CallCountItemSet[i].CompType);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartColor", WorkerCountItemSetList.CallCountItemSet[i].AlartColor);
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(i+1)+"].AlartSound", WorkerCountItemSetList.CallCountItemSet[i].AlartSound);
  }

  delete IniKey;
}
void TFormMain::DispWorkerCountItemSet()
{
  TListItem  *ListItem1;
  int j=0;
  lvWorkerCallCount->Clear();
  for (int i=0; i<WorkerCountItemSetList.ItemNum; i++)
  {
    if (WorkerCountItemSetList.CallCountItemSet[i].DispId == 1)
    {
      ListItem1 = lvWorkerCallCount->Items->Add();
      ListItem1->Checked = true;
      ListItem1->Caption = IntToStr(j+1);

      ListItem1->SubItems->Insert(0, WorkerCountItemSetList.CallCountItemSet[i].ItemName);
      ListItem1->SubItems->Insert(1, GetCallCountValue(0, WorkerCountItemSetList.CallCountItemSet[i].ValueType));
      if (gLangID == 1)
      {
        if (WorkerCountItemSetList.CallCountItemSet[i].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "未设置");
        }
        else
        {
          if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 0)
          {
            if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"秒时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"次时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "等于%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 1)
          {
            if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"秒时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"次时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "大于%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 2)
          {
            if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"秒时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"次时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "小于%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "未设置");
          }
        }
      }
      else
      {
        if (WorkerCountItemSetList.CallCountItemSet[i].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "ゼ砞﹚");
        }
        else
        {
          if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 0)
          {
            if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"Ω牡");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "单%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, "单"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 1)
          {
            if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"Ω牡");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 2)
          {
            if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"Ω牡");
            else if (WorkerCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "ゼ砞﹚");
          }
        }
      }
      WorkerCountItemSetList.CallCountItemSet[i].ItemListIndex = j;
      j++;
    }
  }
}
void TFormMain::DispWorkerCountValue()
{
  TListItem  *ListItem1;
  int nItemIndex;

  for (int i = 0; i < WorkerCountItemSetList.ItemNum; i ++)
  {
    if (WorkerCountItemSetList.CallCountItemSet[i].DispId == 1)
    {
      nItemIndex = WorkerCountItemSetList.CallCountItemSet[i].ItemListIndex;
      if (nItemIndex >= 0 && nItemIndex < lvWorkerCallCount->Items->Count)
      {
        ListItem1 = lvWorkerCallCount->Items->Item[nItemIndex];
        ListItem1->SubItems->Strings[1] = GetCallCountValue(WorkerCallCountParamList.AgentCallCountParam[g_nSelWorkerIndex].ItemData[i], WorkerCountItemSetList.CallCountItemSet[i].ValueType);
      }
    }
  }
}
void TFormMain::DispWorkerCountValue(int nItemNo)
{
  TListItem  *ListItem1;
  int nItemIndex;

  if (WorkerCountItemSetList.CallCountItemSet[nItemNo].DispId == 1)
  {
    nItemIndex = WorkerCountItemSetList.CallCountItemSet[nItemNo].ItemListIndex;
    if (nItemIndex >= 0 && nItemIndex < lvWorkerCallCount->Items->Count)
    {
      ListItem1 = lvWorkerCallCount->Items->Item[nItemIndex];
      ListItem1->SubItems->Strings[1] = GetCallCountValue(WorkerCallCountParamList.AgentCallCountParam[g_nSelWorkerIndex].ItemData[nItemNo], WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType);
      if (gLangID == 1)
      {
        if (WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "未设置");
        }
        else
        {
          if (WorkerCountItemSetList.CallCountItemSet[nItemNo].CompType == 0)
          {
            if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"秒时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"次时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "等于%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].CompType == 1)
          {
            if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"秒时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"次时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "大于%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].CompType == 2)
          {
            if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"秒时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"次时告警");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "小于%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "未设置");
          }
        }
      }
      else
      {
        if (WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "ゼ砞﹚");
        }
        else
        {
          if (WorkerCountItemSetList.CallCountItemSet[nItemNo].CompType == 0)
          {
            if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"Ω牡");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "单%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, "单"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].CompType == 1)
          {
            if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"Ω牡");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
          }
          else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].CompType == 2)
          {
            if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"Ω牡");
            else if (WorkerCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "ゼ砞﹚");
          }
        }
      }
    }
  }
}
void TFormMain::DispSeatStatusData(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nItemDataValue;
  int nWorkerNo = atoi(XMLMsg.GetAttrValue(3).C_Str());
  if (nWorkerNo == 0)
    return;
  int nSaveIndex = WorkerCallCountParamList.GetSaveWorkerIndex(nWorkerNo);
  if (nSaveIndex < 0)
    return;

  AnsiString strSeatNo = (char *)(XMLMsg.GetAttrValue(2).C_Str());
  AnsiString strWorkerName = (char *)(XMLMsg.GetAttrValue(4).C_Str());
  int nCurStatus = atoi(XMLMsg.GetAttrValue(5).C_Str());
  int nCurTimeLen = atoi(XMLMsg.GetAttrValue(6).C_Str());
  int nItemNum = MySplit(XMLMsg.GetAttrValue(7).C_Str(), ',', pstrAgCountList);

  WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].WorkerNo = nWorkerNo;
  WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].WorkerName = strWorkerName;
  WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].SeatNo = strSeatNo;
  WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].curStatus = nCurStatus;
  WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].curTimeLen = nCurTimeLen;
  WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingFlag = 0;
  for (int i=0; i<nItemNum; i++)
  {
    nItemDataValue = MyStrToInt(pstrAgCountList->Strings[i]);
    if (WorkerCountItemSetList.CallCountItemSet[i].AlartId == 1)
    {
      if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 0)
      {
        if (nItemDataValue == WorkerCountItemSetList.CallCountItemSet[i].AlartValue)
        {
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingId[i] = 1;
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingFlag ++;
          if (nItemDataValue != WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemData[i])
          {
            if (gLangID == 1)
            {
              WriteAlartMsg("工号=%d 姓名=%s 统计项=%s 统计值等于 %d 告警!!!",
                nWorkerNo, strWorkerName,
                WorkerCountItemSetList.CallCountItemSet[i].ItemName,
                WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
            }
            else
            {
              WriteAlartMsg("絪腹=%d ﹎=%s 参璸兜=%s 参璸单 %d 牡!!!",
                nWorkerNo, strWorkerName,
                WorkerCountItemSetList.CallCountItemSet[i].ItemName,
                WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
            }
          }
        }
        else
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingId[i] = 0;
      }
      else if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 1)
      {
        if (nItemDataValue > WorkerCountItemSetList.CallCountItemSet[i].AlartValue)
        {
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingId[i] = 1;
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingFlag ++;

          if (nItemDataValue != WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemData[i])
          {
            if (gLangID == 1)
              WriteAlartMsg("工号=%d 姓名=%s 统计项=%s 统计值 %d 大于设定值 %d 告警!!!",
                nWorkerNo, strWorkerName,
                WorkerCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
            else
              WriteAlartMsg("絪腹=%d ﹎=%s 参璸兜=%s 参璸 %d 砞﹚ %d 牡!!!",
                nWorkerNo, strWorkerName,
                WorkerCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
          }
        }
        else
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingId[i] = 0;
      }
      else if (WorkerCountItemSetList.CallCountItemSet[i].CompType == 2)
      {
        if (nItemDataValue < WorkerCountItemSetList.CallCountItemSet[i].AlartValue)
        {
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingId[i] = 1;
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingFlag ++;

          if (nItemDataValue != WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemData[i])
          {
            if (gLangID == 1)
              WriteAlartMsg("工号=%d 姓名=%s 统计项=%s 统计值 %d 小于设定值 %d 告警!!!",
                nWorkerNo, strWorkerName,
                WorkerCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
            else
              WriteAlartMsg("絪腹=%d ﹎=%s 参璸兜=%s 参璸 %d 砞﹚ %d 牡!!!",
                nWorkerNo, strWorkerName,
                WorkerCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                WorkerCountItemSetList.CallCountItemSet[i].AlartValue);
          }
        }
        else
          WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].AlartingId[i] = 0;
      }
    }
    WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemData[i] = nItemDataValue;
  }
  if (WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemListIndex < 0)
  {
    WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemListIndex = lvWorkerList->Items->Count;
    ListItem1 = lvWorkerList->Items->Add();
    ListItem1->Caption = IntToStr(nWorkerNo);
    ListItem1->SubItems->Insert(0, strSeatNo);
    ListItem1->SubItems->Insert(1, strWorkerName);
    switch (nCurStatus)
    {
    case 0:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "退出" : "癶");
      ListItem1->ImageIndex = 0;
      break;
    case 1:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "登录" : "祅魁");
      ListItem1->ImageIndex = 1;
      break;
    case 2:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "示忙" : "Γ魁");
      ListItem1->ImageIndex = 2;
      break;
    case 3:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "空闲" : "丁");
      ListItem1->ImageIndex = 3;
      break;
    case 4:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "呼入振铃" : "挤筧");
      ListItem1->ImageIndex = 4;
      break;
    case 5:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "呼出占用" : "挤ノ");
      ListItem1->ImageIndex = 5;
      break;
    case 6:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "呼出振铃" : "挤筧");
      ListItem1->ImageIndex = 5;
      break;
    case 7:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "通话状态" : "硄杠篈");
      ListItem1->ImageIndex = 6;
      break;
    case 8:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "保持状态" : "玂篈");
      ListItem1->ImageIndex = 6;
      break;
    case 9:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "话后处理" : "杠矪瞶");
      ListItem1->ImageIndex = 7;
      break;
    case 10:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "离席小休态" : "瞒畊ヰ篈");
      ListItem1->ImageIndex = 8;
      break;
    default:
      ListItem1->SubItems->Insert(2, (gLangID == 1) ? "未知" : "ゼ");
      ListItem1->ImageIndex = 2;
      break;
    }
    ListItem1->SubItems->Insert(3, GetCallCountValue(nCurTimeLen, 1));
    ListItem1->SubItems->Insert(4, "");
  }
  else
  {
    ListItem1 = lvWorkerList->Items->Item[WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemListIndex];
    ListItem1->SubItems->Strings[0] = strSeatNo;
    ListItem1->SubItems->Strings[1] = strWorkerName;
    switch (nCurStatus)
    {
    case 0:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "退出" : "癶";
      ListItem1->ImageIndex = 0;
      break;
    case 1:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "登录" : "祅魁";
      ListItem1->ImageIndex = 2;
      break;
    case 2:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "示忙" : "Γ魁";
      ListItem1->ImageIndex = 3;
      break;
    case 3:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "空闲" : "丁";
      ListItem1->ImageIndex = 4;
      break;
    case 4:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "呼入振铃" : "挤筧";
      ListItem1->ImageIndex = 5;
      break;
    case 5:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "呼出占用" : "挤ノ";
      ListItem1->ImageIndex = 6;
      break;
    case 6:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "呼出振铃" : "挤筧";
      ListItem1->ImageIndex = 6;
      break;
    case 7:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "通话状态" : "硄杠篈";
      ListItem1->ImageIndex = 7;
      break;
    case 8:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "保持状态" : "玂篈";
      ListItem1->ImageIndex = 7;
      break;
    case 9:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "话后处理" : "杠矪瞶";
      ListItem1->ImageIndex = 8;
      break;
    case 10:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "离席小休态" : "瞒ヰ篈";
      ListItem1->ImageIndex = 9;
      break;
    default:
      ListItem1->SubItems->Strings[2] = (gLangID == 1) ? "未知" : "ゼ";
      ListItem1->ImageIndex = 2;
      break;
    }
    ListItem1->SubItems->Strings[3] = GetCallCountValue(nCurTimeLen, 1);
  }
  if (WorkerCallCountParamList.AgentCallCountParam[nSaveIndex].ItemListIndex == g_nSelDispWorkerIndex)
  {
    DispWorkerCountValue();
  }
}
//------------------------------------------------------------------------------
void TFormMain::ReadGroupCountItemSet()
{
  AnsiString strSection;
  TIniFile *IniKey = new TIniFile (LOGINIFile);

  if (gLangID == 1)
    strSection = "GROUPSTATUS";
  else
    strSection = "GROUPSTATUS-CHT";

  GroupCountItemSetList.ItemNum = IniKey->ReadInteger("GROUPSTATUS", "ITEMNUM", 0);
  for (int i = 0; i < GroupCountItemSetList.ItemNum; i ++)
  {
    GroupCountItemSetList.CallCountItemSet[i].ItemNo = i;
    GroupCountItemSetList.CallCountItemSet[i].ItemName = IniKey->ReadString(strSection, "ITEM["+IntToStr(i+1)+"].NAME", ((gLangID == 1) ? "统计项目[" : "参璸兜ヘ[")+IntToStr(i+1)+"]");
    GroupCountItemSetList.CallCountItemSet[i].DispId = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].DISP", 0);
    GroupCountItemSetList.CallCountItemSet[i].AlartId = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartId", 0);
    GroupCountItemSetList.CallCountItemSet[i].AlartValue = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartValue", 0);
    GroupCountItemSetList.CallCountItemSet[i].CompType = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].CompType", 0);
    GroupCountItemSetList.CallCountItemSet[i].ClearAlartId = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].ClearAlartId", 0);
    GroupCountItemSetList.CallCountItemSet[i].AlartColor = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartColor", 0);
    GroupCountItemSetList.CallCountItemSet[i].AlartSound = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartSound", 0);
    GroupCountItemSetList.CallCountItemSet[i].ValueType = IniKey->ReadInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].TYPE", 0);

    GroupCountItemSetList.CallCountItemSet[i].ItemListIndex = -1;
  }
  if (gLangID == 1)
    strSection = "GROUPCOUNT";
  else
    strSection = "GROUPCOUNT-CHT";
  GroupCountItemSetList.StatusCountItemNum = IniKey->ReadInteger("GROUPCOUNT", "ITEMNUM", 0);
  for (int i = 0; i < GroupCountItemSetList.StatusCountItemNum; i ++)
  {
    GroupCountItemSetList.StatusCountItemName[i] = IniKey->ReadString(strSection, "ITEM["+IntToStr(i+1)+"].NAME", ((gLangID == 1) ? "状态["+IntToStr(i+1)+"]总数" : "篈["+IntToStr(i+1)+"]羆计"));
  }
  delete IniKey;
}
void TFormMain::WriteGroupCountItemSet()
{
  TIniFile *IniKey = new TIniFile (LOGINIFile);

  for (int i = 0; i < GroupCountItemSetList.ItemNum; i ++)
  {
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].DISP", GroupCountItemSetList.CallCountItemSet[i].DispId);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartId", GroupCountItemSetList.CallCountItemSet[i].AlartId);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartValue", GroupCountItemSetList.CallCountItemSet[i].AlartValue);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].CompType", GroupCountItemSetList.CallCountItemSet[i].CompType);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartColor", GroupCountItemSetList.CallCountItemSet[i].AlartColor);
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(i+1)+"].AlartSound", GroupCountItemSetList.CallCountItemSet[i].AlartSound);
  }

  delete IniKey;
}
void TFormMain::DispGroupCountItemSet()
{
  TListItem  *ListItem1;
  int j=0;
  lvGroupCallCount->Clear();
  for (int i=0; i<GroupCountItemSetList.ItemNum; i++)
  {
    if (GroupCountItemSetList.CallCountItemSet[i].DispId == 1)
    {
      ListItem1 = lvGroupCallCount->Items->Add();
      ListItem1->Checked = true;
      ListItem1->Caption = IntToStr(j+1);

      ListItem1->SubItems->Insert(0, GroupCountItemSetList.CallCountItemSet[i].ItemName);
      ListItem1->SubItems->Insert(1, GetCallCountValue(0, GroupCountItemSetList.CallCountItemSet[i].ValueType));
      if (gLangID == 1)
      {
        if (GroupCountItemSetList.CallCountItemSet[i].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "未设置");
        }
        else
        {
          if (GroupCountItemSetList.CallCountItemSet[i].CompType == 0)
          {
            if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"秒时告警");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"次时告警");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "等于%"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
          }
          else if (GroupCountItemSetList.CallCountItemSet[i].CompType == 1)
          {
            if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"秒时告警");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"次时告警");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "大于%"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
          }
          else if (GroupCountItemSetList.CallCountItemSet[i].CompType == 2)
          {
            if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"秒时告警");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"次时告警");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "小于%"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"时告警");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "未设置");
          }
        }
      }
      else
      {
        if (GroupCountItemSetList.CallCountItemSet[i].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "ゼ砞﹚");
        }
        else
        {
          if (GroupCountItemSetList.CallCountItemSet[i].CompType == 0)
          {
            if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"Ω牡");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "单%"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, "单"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
          }
          else if (GroupCountItemSetList.CallCountItemSet[i].CompType == 1)
          {
            if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"Ω牡");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
          }
          else if (GroupCountItemSetList.CallCountItemSet[i].CompType == 2)
          {
            if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"Ω牡");
            else if (GroupCountItemSetList.CallCountItemSet[i].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[i].AlartValue)+"牡");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "ゼ砞﹚");
          }
        }
      }
      GroupCountItemSetList.CallCountItemSet[i].ItemListIndex = j;
      j++;
    }
  }
}
void TFormMain::DispGroupStatusCountItemSet()
{
  TListColumn  *NewColumn;

  for (int i = 0; i < GroupCountItemSetList.StatusCountItemNum; i ++)
  {
    NewColumn = lvGroupList->Columns->Add();
    NewColumn->Caption = GroupCountItemSetList.StatusCountItemName[i];
    NewColumn->Width = NewColumn->Caption.Length()*7;
  }
}
void TFormMain::DispGroupCountValue()
{
  TListItem  *ListItem1;
  int nItemIndex;

  for (int i = 0; i < GroupCountItemSetList.ItemNum; i ++)
  {
    if (GroupCountItemSetList.CallCountItemSet[i].DispId == 1)
    {
      nItemIndex = GroupCountItemSetList.CallCountItemSet[i].ItemListIndex;
      if (nItemIndex >= 0 && nItemIndex < lvGroupCallCount->Items->Count)
      {
        ListItem1 = lvGroupCallCount->Items->Item[nItemIndex];
        ListItem1->SubItems->Strings[1] = GetCallCountValue(GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].ItemData[i], GroupCountItemSetList.CallCountItemSet[i].ValueType);
      }
    }
  }
}
void TFormMain::DispGroupCountValue(int nItemNo)
{
  TListItem  *ListItem1;
  int nItemIndex;

  if (GroupCountItemSetList.CallCountItemSet[nItemNo].DispId == 1)
  {
    nItemIndex = GroupCountItemSetList.CallCountItemSet[nItemNo].ItemListIndex;
    if (nItemIndex >= 0 && nItemIndex < lvGroupCallCount->Items->Count)
    {
      if (gLangID == 1)
      {
        ListItem1 = lvGroupCallCount->Items->Item[nItemIndex];
        ListItem1->SubItems->Strings[1] = GetCallCountValue(GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].ItemData[nItemNo], GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType);
        if (GroupCountItemSetList.CallCountItemSet[nItemNo].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "未设置");
        }
        else
        {
          if (GroupCountItemSetList.CallCountItemSet[nItemNo].CompType == 0)
          {
            if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"秒时告警");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"次时告警");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "等于%"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "等于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
          }
          else if (GroupCountItemSetList.CallCountItemSet[nItemNo].CompType == 1)
          {
            if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"秒时告警");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"次时告警");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "大于%"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "大于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
          }
          else if (GroupCountItemSetList.CallCountItemSet[nItemNo].CompType == 2)
          {
            if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"秒时告警");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"次时告警");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "小于%"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
            else
              ListItem1->SubItems->Insert(2, "小于"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"时告警");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "未设置");
          }
        }
      }
      else
      {
        ListItem1 = lvGroupCallCount->Items->Item[nItemIndex];
        ListItem1->SubItems->Strings[1] = GetCallCountValue(GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].ItemData[nItemNo], GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType);
        if (GroupCountItemSetList.CallCountItemSet[nItemNo].AlartId == 0)
        {
          ListItem1->SubItems->Insert(2, "ゼ砞﹚");
        }
        else
        {
          if (GroupCountItemSetList.CallCountItemSet[nItemNo].CompType == 0)
          {
            if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, "单"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"Ω牡");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "单%"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, "单"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
          }
          else if (GroupCountItemSetList.CallCountItemSet[nItemNo].CompType == 1)
          {
            if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"Ω牡");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
          }
          else if (GroupCountItemSetList.CallCountItemSet[nItemNo].CompType == 2)
          {
            if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 1)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 2)
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"Ω牡");
            else if (GroupCountItemSetList.CallCountItemSet[nItemNo].ValueType == 3)
              ListItem1->SubItems->Insert(2, "%"+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
            else
              ListItem1->SubItems->Insert(2, ""+IntToStr(GroupCountItemSetList.CallCountItemSet[nItemNo].AlartValue)+"牡");
          }
          else
          {
            ListItem1->SubItems->Insert(2, "ゼ砞﹚");
          }
        }
      }
    }
  }
}
void TFormMain::DispGroupStatusData(CXMLMsg &XMLMsg)
{
  TListItem  *ListItem1;
  int nItemDataValue;
  int i, nItemNum;
  int nGroupNo = atoi(XMLMsg.GetAttrValue(1).C_Str());
  int nSaveIndex = GroupCallCountParamList.GetSaveGroupIndex(nGroupNo);
  if (nSaveIndex < 0)
    return;

  AnsiString strGroupName = (char *)(XMLMsg.GetAttrValue(2).C_Str());

  GroupCallCountParamList.GroupCallCountParam[nSaveIndex].GroupNo = nGroupNo;
  GroupCallCountParamList.GroupCallCountParam[nSaveIndex].GroupName = strGroupName;

  nItemNum = MySplit(XMLMsg.GetAttrValue(3).C_Str(), ',', pstrAgCountList);
  for (i=0; i<nItemNum; i++)
  {
    GroupCallCountParamList.GroupCallCountParam[nSaveIndex].StatusCount[i] = MyStrToInt(pstrAgCountList->Strings[i]);
  }

  nItemNum = MySplit(XMLMsg.GetAttrValue(4).C_Str(), ',', pstrAgCountList);
  GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingFlag = 0;
  for (int i=0; i<nItemNum; i++)
  {
    nItemDataValue = MyStrToInt(pstrAgCountList->Strings[i]);
    if (GroupCountItemSetList.CallCountItemSet[i].AlartId == 1)
    {
      if (GroupCountItemSetList.CallCountItemSet[i].CompType == 0)
      {
        if (nItemDataValue == GroupCountItemSetList.CallCountItemSet[i].AlartValue)
        {
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingId[i] = 1;
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingFlag ++;

          if (nItemDataValue != GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemData[i])
          {
            if (gLangID == 1)
              WriteAlartMsg("组号=%d 名称=%s 统计项=%s 统计值等于设定值 %d 告警!!!",
                nGroupNo, strGroupName,
                GroupCountItemSetList.CallCountItemSet[i].ItemName,
                GroupCountItemSetList.CallCountItemSet[i].AlartValue);
            else
              WriteAlartMsg("舱腹=%d 嘿=%s 参璸兜=%s 参璸单砞﹚ %d 牡!!!",
                nGroupNo, strGroupName,
                GroupCountItemSetList.CallCountItemSet[i].ItemName,
                GroupCountItemSetList.CallCountItemSet[i].AlartValue);
          }
        }
        else
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingId[i] = 0;
      }
      else if (GroupCountItemSetList.CallCountItemSet[i].CompType == 1)
      {
        if (nItemDataValue > GroupCountItemSetList.CallCountItemSet[i].AlartValue)
        {
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingId[i] = 1;
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingFlag ++;

          if (nItemDataValue != GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemData[i])
          {
            if (gLangID == 1)
              WriteAlartMsg("组号=%d 名称=%s 统计项=%s 统计值 %d 大于设定值 %d告警!!!",
                nGroupNo, strGroupName,
                GroupCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                GroupCountItemSetList.CallCountItemSet[i].AlartValue);
            else
              WriteAlartMsg("舱腹=%d 嘿=%s 参璸兜=%s 参璸 %d 砞﹚ %d牡!!!",
                nGroupNo, strGroupName,
                GroupCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                GroupCountItemSetList.CallCountItemSet[i].AlartValue);
          }
        }
        else
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingId[i] = 0;
      }
      else if (GroupCountItemSetList.CallCountItemSet[i].CompType == 2)
      {
        if (nItemDataValue < GroupCountItemSetList.CallCountItemSet[i].AlartValue)
        {
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingId[i] = 1;
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingFlag ++;

          if (nItemDataValue != GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemData[i])
          {
            if (gLangID == 1)
              WriteAlartMsg("组号=%d 名称=%s 统计项=%s 统计值 %d 小于设定值 %d告警!!!",
                nGroupNo, strGroupName,
                GroupCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                GroupCountItemSetList.CallCountItemSet[i].AlartValue);
            else
              WriteAlartMsg("舱腹=%d 嘿=%s 参璸兜=%s 参璸 %d 砞﹚ %d牡!!!",
                nGroupNo, strGroupName,
                GroupCountItemSetList.CallCountItemSet[i].ItemName,
                nItemDataValue,
                GroupCountItemSetList.CallCountItemSet[i].AlartValue);
          }
        }
        else
          GroupCallCountParamList.GroupCallCountParam[nSaveIndex].AlartingId[i] = 0;
      }
    }
    GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemData[i] = nItemDataValue;
  }
  if (GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemListIndex < 0)
  {
    GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemListIndex = lvGroupList->Items->Count;
    ListItem1 = lvGroupList->Items->Add();
    ListItem1->Caption = IntToStr(nGroupNo);
    ListItem1->SubItems->Insert(0, strGroupName);
    for (i=0; i<GroupCountItemSetList.StatusCountItemNum; i++)
    {
      ListItem1->SubItems->Insert(i+1, IntToStr(GroupCallCountParamList.GroupCallCountParam[nSaveIndex].StatusCount[i]));
    }
  }
  else
  {
    ListItem1 = lvGroupList->Items->Item[GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemListIndex];
    ListItem1->SubItems->Strings[0] = strGroupName;
    for (i=0; i<GroupCountItemSetList.StatusCountItemNum; i++)
    {
      ListItem1->SubItems->Strings[i+1] = IntToStr(GroupCallCountParamList.GroupCallCountParam[nSaveIndex].StatusCount[i]);
    }
  }
  if (GroupCallCountParamList.GroupCallCountParam[nSaveIndex].ItemListIndex == g_nSelDispGroupIndex)
  {
    DispGroupCountValue();
  }
}

void __fastcall TFormMain::lvWorkerListSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nSelDispWorkerIndex = Item->Index;
  g_nSelWorkerIndex = WorkerCallCountParamList.GetSaveWorkerIndexByDispIndex(g_nSelDispWorkerIndex);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvWorkerCallCountSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nSelDispWorkerCallIndex = Item->Index;
  g_nSelWorkerItemNo = WorkerCountItemSetList.GetItemNoByItemListIndex(g_nSelDispWorkerCallIndex);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvGroupListSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nSelDispGroupIndex = Item->Index;
  g_nSelGroupIndex = GroupCallCountParamList.GetSaveGroupIndexByDispIndex(g_nSelDispGroupIndex);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvGroupCallCountSelectItem(TObject *Sender,
      TListItem *Item, bool Selected)
{
  if (Selected == false) return;
  g_nSelDispGroupCallIndex = Item->Index;
  g_nSelGroupItemNo = GroupCountItemSetList.GetItemNoByItemListIndex(g_nSelDispGroupCallIndex);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button3Click(TObject *Sender)
{
  for (int i = 0; i < WorkerCountItemSetList.ItemNum; i ++)
  {
    WorkerCountItemSetList.CallCountItemSet[i].DispId = 1;
  }
  DispWorkerCountItemSet();
  DispWorkerCountValue();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button5Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int nItemNo;
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  for (int i=0; i<lvWorkerCallCount->Items->Count; i++)
  {
    ListItem1 = lvWorkerCallCount->Items->Item[i];
    nItemNo = WorkerCountItemSetList.GetItemNoByItemListIndex(i);
    if (nItemNo < 0)
      continue;
    if (ListItem1->Checked == true)
    {
      WorkerCountItemSetList.CallCountItemSet[nItemNo].DispId = 1;
    }
    else
    {
      WorkerCountItemSetList.CallCountItemSet[nItemNo].DispId = 0;
    }
    IniKey->WriteInteger("WORKERSTATUS", "ITEM["+IntToStr(nItemNo+1)+"].DISP", WorkerCountItemSetList.CallCountItemSet[nItemNo].DispId);
  }
  delete IniKey;
  DispWorkerCountItemSet();
  DispWorkerCountValue();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button6Click(TObject *Sender)
{
  for (int i = 0; i < GroupCountItemSetList.ItemNum; i ++)
  {
    GroupCountItemSetList.CallCountItemSet[i].DispId = 1;
  }
  DispGroupCountItemSet();
  DispGroupCountValue();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button7Click(TObject *Sender)
{
  TListItem  *ListItem1;
  int nItemNo;
  TIniFile *IniKey = new TIniFile (LOGINIFile);
  for (int i=0; i<lvGroupCallCount->Items->Count; i++)
  {
    ListItem1 = lvGroupCallCount->Items->Item[i];
    nItemNo = GroupCountItemSetList.GetItemNoByItemListIndex(i);
    if (nItemNo < 0)
      continue;
    if (ListItem1->Checked == true)
    {
      GroupCountItemSetList.CallCountItemSet[nItemNo].DispId = 1;
    }
    else
    {
      GroupCountItemSetList.CallCountItemSet[nItemNo].DispId = 0;
    }
    IniKey->WriteInteger("GROUPSTATUS", "ITEM["+IntToStr(nItemNo+1)+"].DISP", GroupCountItemSetList.CallCountItemSet[nItemNo].DispId);
  }
  delete IniKey;
  DispGroupCountItemSet();
  DispGroupCountValue();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvWorkerListCustomDrawSubItem(
      TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
  DefaultDraw = false;

  int nWorkerItemNo = WorkerCallCountParamList.GetSaveWorkerIndexByDispIndex(Item->Index);
  if (nWorkerItemNo >= 0)
  {
    if (WorkerCallCountParamList.AgentCallCountParam[nWorkerItemNo].AlartingFlag > 0)
    {
      Sender->Canvas->Font->Color = clWindow;
      Sender->Canvas->Brush->Color = clRed;
    }
  }

  DefaultDraw = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvWorkerCallCountCustomDrawSubItem(
      TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
  DefaultDraw = false;

  //if (SubItem == 1)
  {
    int nItemNo = WorkerCountItemSetList.GetItemNoByItemListIndex(Item->Index);
    if (nItemNo >= 0)
    {
      if (WorkerCallCountParamList.AgentCallCountParam[g_nSelWorkerIndex].AlartingId[nItemNo] > 0)
      {
        Sender->Canvas->Font->Color = clWindow;
        switch (WorkerCountItemSetList.CallCountItemSet[nItemNo].AlartColor)
        {
        case 1:
          Sender->Canvas->Brush->Color = clBlue;
          break;
        case 2:
          Sender->Canvas->Brush->Color = clYellow;
          break;
        case 3:
          Sender->Canvas->Brush->Color = clAqua;
          break;
        case 4:
          Sender->Canvas->Brush->Color = clRed;
          break;
        }
        Sender->Canvas->Brush->Color = clRed;
      }
    }
  }
  DefaultDraw = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvWorkerCallCountDblClick(TObject *Sender)
{
  FormWorkerAlartSet->AlartType = 1;
  FormWorkerAlartSet->lblItemName->Caption = WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].ItemName + ((gLangID == 1) ? "-告警参数设置：" : "-牡把计砞﹚:");
  FormWorkerAlartSet->ckAlartId->Checked = WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartId==1 ? true:false;
  FormWorkerAlartSet->cseAlartValue->Value = WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartValue;
  FormWorkerAlartSet->rgCompType->ItemIndex = WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].CompType;
  switch (WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartColor)
  {
  case 1:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "篮色" : "膞︹";
    break;
  case 2:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "黄色" : "独︹";
    break;
  case 3:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "橙色" : "卷︹";
    break;
  case 4:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "红色" : "︹";
    break;
  }
  FormWorkerAlartSet->ckAlartSound->Checked = WorkerCountItemSetList.CallCountItemSet[g_nSelWorkerItemNo].AlartSound==1 ? true:false;
  FormWorkerAlartSet->Button1->Enabled = false;
  FormWorkerAlartSet->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvWorkerListClick(TObject *Sender)
{
  DispWorkerCountValue();
  GroupBox15->Caption = WorkerCallCountParamList.AgentCallCountParam[g_nSelWorkerIndex].WorkerName + ((gLangID == 1) ? "-详细话务数据：" : "-冈灿杠叭计沮:");
}
//---------------------------------------------------------------------------


void __fastcall TFormMain::lvGroupListCustomDrawSubItem(
      TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
  DefaultDraw = false;

  int nGroupItemNo = GroupCallCountParamList.GetSaveGroupIndexByDispIndex(Item->Index);
  if (nGroupItemNo >= 0)
  {
    if (GroupCallCountParamList.GroupCallCountParam[nGroupItemNo].AlartingFlag > 0)
    {
      Sender->Canvas->Font->Color = clWindow;
      Sender->Canvas->Brush->Color = clRed;
    }
  }

  DefaultDraw = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvGroupCallCountCustomDrawSubItem(
      TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
  DefaultDraw = false;

  //if (SubItem == 1)
  {
    int nItemNo = GroupCountItemSetList.GetItemNoByItemListIndex(Item->Index);
    if (nItemNo >= 0)
    {
      if (GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].AlartingId[nItemNo] > 0)
      {
        Sender->Canvas->Font->Color = clWindow;
        switch (GroupCountItemSetList.CallCountItemSet[nItemNo].AlartColor)
        {
        case 1:
          Sender->Canvas->Brush->Color = clBlue;
          break;
        case 2:
          Sender->Canvas->Brush->Color = clYellow;
          break;
        case 3:
          Sender->Canvas->Brush->Color = clAqua;
          break;
        case 4:
          Sender->Canvas->Brush->Color = clRed;
          break;
        }
        Sender->Canvas->Brush->Color = clRed;
      }
    }
  }
  DefaultDraw = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvGroupListClick(TObject *Sender)
{
  DispGroupCountValue();
  GroupBox16->Caption = GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].GroupName + ((gLangID == 1) ? "-详细话务数据：" : "-冈灿杠叭计沮:");
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvGroupCallCountDblClick(TObject *Sender)
{
  FormWorkerAlartSet->AlartType = 2;
  FormWorkerAlartSet->lblItemName->Caption = GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].ItemName + ((gLangID == 1) ? "-告警参数设置：" : "-牡把计砞﹚:");
  FormWorkerAlartSet->ckAlartId->Checked = GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartId==1 ? true:false;
  FormWorkerAlartSet->cseAlartValue->Value = GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartValue;
  FormWorkerAlartSet->rgCompType->ItemIndex = GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].CompType;
  switch (GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartColor)
  {
  case 1:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "篮色" : "膞︹";
    break;
  case 2:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "黄色" : "独︹";
    break;
  case 3:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "橙色" : "卷︹";
    break;
  case 4:
    FormWorkerAlartSet->cbAlartColor->Text = (gLangID == 1) ? "红色" : "︹";
    break;
  }
  FormWorkerAlartSet->ckAlartSound->Checked = GroupCountItemSetList.CallCountItemSet[g_nSelGroupItemNo].AlartSound==1 ? true:false;
  FormWorkerAlartSet->Button1->Enabled = false;
  FormWorkerAlartSet->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckBlockTrunkSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "BlockTrunkSound", ckBlockTrunkSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckAgentLoginSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "AgentLoginSound", ckAgentLoginSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckBusyTrunkSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "BusyTrunkSound", ckBusyTrunkSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckAgentLevalSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "AgentLevalSound", ckAgentLevalSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckACDWaitingSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "ACDWaitingSound", ckACDWaitingSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckBusyAgentSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "BusyAgentSound", ckBusyAgentSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckBusyIVRSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "BusyIVRSound", ckBusyIVRSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ckSeatSatusSoundClick(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);
  LOGIniKey->WriteBool("ALART", "SeatSatusSound", ckSeatSatusSound->Checked);
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbSoundTypeChange(TObject *Sender)
{
  LOGIniKey = new TIniFile (LOGINIFile);

  if (gLangID == 1)
  {
    if (cbSoundType->Text == "")
    {
      g_nAlartWavId = 1;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart1.wav";
    }
    else if (cbSoundType->Text == "")
    {
      g_nAlartWavId = 2;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart2.wav";
    }
    else if (cbSoundType->Text == "")
    {
      g_nAlartWavId = 3;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart3.wav";
    }
    else
    {
      g_nAlartWavId = 1;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart1.wav";
    }
  }
  else
  {
    if (cbSoundType->Text == "")
    {
      g_nAlartWavId = 1;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart1.wav";
    }
    else if (cbSoundType->Text == "")
    {
      g_nAlartWavId = 2;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart2.wav";
    }
    else if (cbSoundType->Text == "")
    {
      g_nAlartWavId = 3;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart3.wav";
    }
    else
    {
      g_nAlartWavId = 1;
      g_strAlartWavFile = ExtractFilePath(Application->ExeName)+"Alart1.wav";
    }
  }
  LOGIniKey->WriteInteger ( "ALART", "AlartId", g_nAlartWavId);

  MediaPlayer1->FileName = g_strAlartWavFile;
  delete LOGIniKey;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbClearCallCountTypeChange(TObject *Sender)
{
  if (cbClearCallCountType->Text == ((gLangID == 1) ? "按定制时间清零" : "﹚丁睲箂") )
  {
    Label80->Visible = true;
    editClearTimeList->Visible = true;
    Label81->Visible = true;
  }
  else
  {
    Label80->Visible = false;
    editClearTimeList->Visible = false;
    Label81->Visible = false;
  }
  bOtherParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::lvGroupListDblClick(TObject *Sender)
{
  TListItem  *ListItem1;
  AnsiString strNewGroupName, strKey;

  strNewGroupName = GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].GroupName;
  if (InputQuery( ((gLangID == 1) ? "输入提示：" : "块矗ボ:"), ((gLangID == 1) ? "请修改话务组名称：" : "叫э杠叭舱嘿:"), strNewGroupName))
  {
    IVRIniKey = new TIniFile (IVRINIFile);
    strKey = "GroupName["+IntToStr(GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].GroupNo)+"]";
    GroupCallCountParamList.GroupCallCountParam[g_nSelGroupIndex].GroupName = strNewGroupName;

    ListItem1 = lvGroupList->Items->Item[g_nSelDispGroupIndex];
    ListItem1->SubItems->Strings[0] = strNewGroupName;

    WriteIniParam(1, IVRIniKey, "GROUPNAME", strKey, strNewGroupName);

    delete IVRIniKey;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::csePSTNNumChange(TObject *Sender)
{
  bPSTNParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cseVopNumChange(TObject *Sender)
{
  VopGroup.m_nVopNum = cseVopNum->Value;
  bVopParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Panel4Resize(TObject *Sender)
{
  Bevel4->Width = Panel4->Width -1;
  Bevel6->Width = Panel4->Width -1;
  Bevel1->Width = Panel4->Width -1;
  editIVRAuthKey->Width = Panel4->Width -58;
  editAuthKey->Width = Panel4->Width -58;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Panel2Resize(TObject *Sender)
{
  Button4->Left = Panel2->Width -44;
  BitBtn6->Left = Panel2->Width -44;
  BitBtn8->Left = Panel2->Width -44;
  BitBtn10->Left = Panel2->Width -44;
  BitBtn7->Left = Panel2->Width -44;
  BitBtn9->Left = Panel2->Width -44;
  BitBtn11->Left = Panel2->Width -44;
  BitBtn7->Top = Panel2->Height -125;
  BitBtn9->Top = Panel2->Height -85;
  BitBtn11->Top = Panel2->Height -45;

  Button1->Left = Panel2->Width -117;
  Button2->Left = Panel2->Width -83;
  Button24->Left = Panel2->Width -117;
  Button25->Left = Panel2->Width -117;

  editFLWPATH->Width = Panel2->Width -186;
  lvFlw->Width = Panel2->Width -58;
  editFlwFileName->Width = Panel2->Width -193;
  editVocFileName->Width = Panel2->Width -193;
  editFTPVocPath->Width = Panel2->Width -193;
  GroupBox1->Width = Panel2->Width -58;
  GroupBox1->Height = Panel2->Height -260;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Panel14Resize(TObject *Sender)
{
  GroupBox2->Width = Panel14->Width -17;
  GroupBox3->Width = Panel14->Width -17;
  GroupBox4->Width = Panel14->Width -17;
  GroupBox12->Width = Panel14->Width -17;
  GroupBox10->Width = Panel14->Width -17;
  GroupBox11->Width = Panel14->Width -17;
  GroupBox8->Width = Panel14->Width -17;
  GroupBox9->Width = Panel14->Width -17;
  GroupBox17->Width = Panel14->Width -17;
  GroupBox18->Width = Panel14->Width -17;
}
//---------------------------------------------------------------------------
void TFormMain::GetLoadedTSPList()
{
  char msg[256];
  char *pos1, *pos2, len, szDeviceID[20];
  int i, nPortNo;

  strTSPList->Clear();
  for (i=0; i<1024; i++)
  {
    ldPermanentProviderID[i] = 0;
  }
  for (i=0; i<SwitchPortList.SwitchPortNum; i++)
  {
    SwitchPortList.SwitchPort[i].dwProviderID = 0;
  }
  LINEPROVIDERLIST *lProviderList = (LINEPROVIDERLIST *)LocalAlloc(LPTR, 0x4000);
  lProviderList->dwTotalSize = 0x4000;
  mylineGetProviderList(0x00020002, lProviderList);
  LINEPROVIDERENTRY *lProvider = (LINEPROVIDERENTRY *)((BYTE *)lProviderList + lProviderList->dwProviderListOffset);

  //sprintf(msg, "dwTotalSize=%d dwNeededSize=%d dwUsedSize=%d dwNumProviders=%d dwProviderListSize=%d dwProviderListOffset=%d",
  //  lProviderList->dwTotalSize, lProviderList->dwNeededSize, lProviderList->dwUsedSize, lProviderList->dwNumProviders, lProviderList->dwProviderListSize, lProviderList->dwProviderListOffset);
  int j=0;
  for (DWORD i = 0; i < lProviderList->dwNumProviders; i++)
  {
    sprintf(msg, "%s", (LPTSTR)lProviderList + lProvider[i].dwProviderFilenameOffset);
    if (strncmpi(msg, "tspi2w", 6) != 0)
      continue;
    strTSPList->Add((char *)msg);
    ldPermanentProviderID[j] = lProvider[i].dwPermanentProviderID;
    j++;
    if (strcmpi(msg, "tspi2w.tsp") == 0)
    {
      SwitchPortList.dwProviderID = lProvider[i].dwPermanentProviderID;
    }
    pos1 = strchr(msg, '_');
    if (pos1)
    {
      pos2 = strchr(pos1, '.');
      if (pos2)
      {
        len = pos2-pos1-1;
        pos1++;
        memset(szDeviceID, 0, 20);
        strncpy(szDeviceID, pos1, len);
        nPortNo = SwitchPortList.GetPortNo(szDeviceID);
        if (nPortNo >= 0)
        {
          SwitchPortList.SwitchPort[nPortNo].dwProviderID = lProvider[i].dwPermanentProviderID;
        }
      }
    }
  }
  LocalFree(lProviderList);
}
long TFormMain::AddTAPIDrv(LPCSTR pszTSPFile, DWORD &dwProviderID)
{
  dwProviderID = 0;
  for (int i=0; i<strTSPList->Count; i++)
  {
    if (strcmpi(pszTSPFile, strTSPList->Strings[i].c_str()) == 0)
      return -1;
  }
  return mylineAddProvider(pszTSPFile, NULL, &dwProviderID);
}

void TFormMain::RemoveTAPIDrv()
{
  GetLoadedTSPList();

  for (int i=0; i<strTSPList->Count; i++)
  {
    if (ldPermanentProviderID[i] == 0)
      continue;
    mylineRemoveProvider(ldPermanentProviderID[i], NULL);
  }
}

void TFormMain::RemoveTAPIDrv(DWORD dwProviderID)
{
  mylineRemoveProvider(dwProviderID, NULL);
}

int TFormMain::CreateTAPIDrv(int nDeviceID, DWORD &dwProviderID)
{
  char oldfilename[256];
  char tspfilename[128];
  char newfilename[256];
  char szSystemPath[256],c1[1],c2[1],c3[1],c4[1];
  FILE *Ftapi;

  char szFull[256];
  char drive[MAXDRIVE];
  char dir[MAXDIR];
  char file[MAXFILE];
  char ext[MAXEXT];

  memset(szSystemPath, 0, 256);
  GetSystemDirectory(szSystemPath, 256);
  sprintf(tspfilename, "tspi2w_%d.tsp", nDeviceID);
  sprintf(newfilename, "%s\\tspi2w_%d.tsp", szSystemPath, nDeviceID);

  if (nDeviceID > 9999)
    return 1;
  sprintf(oldfilename, "%stspi2w.tsp", g_strExecPath.c_str());
  if (CopyFile(oldfilename, newfilename, false) != 0)
  {
    c1[0] = nDeviceID/1000+0x30;
    c2[0] = (nDeviceID%1000)/100+0x30;
    c3[0] = (nDeviceID%100)/10+0x30;
    c4[0] = nDeviceID%10+0x30;

    Ftapi = fopen(newfilename, "r+b");
    if (Ftapi)
    {
      if (fseek(Ftapi, 0x1AD61, SEEK_SET) == 0)
      {
        fwrite(c1, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1AD98, SEEK_SET) == 0)
      {
        fwrite(c1, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1ADC8, SEEK_SET) == 0)
      {
        fwrite(c1, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x289C8, SEEK_SET) == 0)
      {
        fwrite(c1, sizeof(char), 1, Ftapi);
      }

      if (fseek(Ftapi, 0x1AD62, SEEK_SET) == 0)
      {
        fwrite(c2, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1AD99, SEEK_SET) == 0)
      {
        fwrite(c2, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1ADC9, SEEK_SET) == 0)
      {
        fwrite(c2, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x289CA, SEEK_SET) == 0)
      {
        fwrite(c2, sizeof(char), 1, Ftapi);
      }

      if (fseek(Ftapi, 0x1AD63, SEEK_SET) == 0)
      {
        fwrite(c3, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1AD9A, SEEK_SET) == 0)
      {
        fwrite(c3, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1ADCA, SEEK_SET) == 0)
      {
        fwrite(c3, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x289CC, SEEK_SET) == 0)
      {
        fwrite(c3, sizeof(char), 1, Ftapi);
      }

      if (fseek(Ftapi, 0x1AD64, SEEK_SET) == 0)
      {
        fwrite(c4, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1AD9B, SEEK_SET) == 0)
      {
        fwrite(c4, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x1ADCB, SEEK_SET) == 0)
      {
        fwrite(c4, sizeof(char), 1, Ftapi);
      }
      if (fseek(Ftapi, 0x289CE, SEEK_SET) == 0)
      {
        fwrite(c4, sizeof(char), 1, Ftapi);
      }

      fclose(Ftapi);
      //Ftapi = NULL;
      //SetFileAttributes(newfilename, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_READONLY);
      return AddTAPIDrv(tspfilename, dwProviderID);
    }
  }
  return 2;
}

void TFormMain::CreateTAPIRegLite(int nDeviceID)
{
  char szExtn[10];
  AnsiString strKey, strTemp;
  TRegistry *MyReg=new TRegistry();// 建立实例

  MyReg->RootKey=HKEY_LOCAL_MACHINE;

  sprintf(szExtn, "I%04d", nDeviceID);
  strTemp = (char *)szExtn;
  strKey = "\\SOFTWARE\\Avaya\\"+strTemp+"\\TSPI";
  MyReg->OpenKey(strKey, true);

  MyReg->WriteString("ipaddress", editSwitchIP->Text);
  MyReg->WriteString("Lines", "1");
  MyReg->WriteString("OptionFlags", "80");
  MyReg->WriteString("Options", "Extn"+IntToStr(nDeviceID));
  MyReg->WriteString("password", "");
  MyReg->WriteString("SilenceBase", "78");
  MyReg->WriteString("ULaw", "Y");

  MyReg->CloseKey();
  delete MyReg;
}

void TFormMain::CreateTAPIRegPro()
{
  char szExtn[10];
  AnsiString strKey;
  TRegistry *MyReg=new TRegistry();// 建立实例

  MyReg->RootKey=HKEY_LOCAL_MACHINE;

  strKey = "\\SOFTWARE\\Avaya\\IP400\\TSPI";
  MyReg->OpenKey(strKey, true);

  MyReg->WriteString("ipaddress", editSwitchIP->Text);
  MyReg->WriteString("OptionFlags", "1");
  MyReg->WriteString("Options", "");
  //MyReg->WriteString("password", "");
  MyReg->WriteString("SilenceBase", "78");
  MyReg->WriteString("ULaw", "Y");

  MyReg->CloseKey();
  delete MyReg;
}

void __fastcall TFormMain::btTAPIClick(TObject *Sender)
{
  int nDeviceID;

  if (gLangID == 1)
  {
    if ( MessageBox( NULL, "您确认要重新加载所有TAPI驱动吗，加载前请停止CTILINK服务？", "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  else
  {
    if ( MessageBox( NULL, "眤絋粄璶穝更┮ΤTAPI臱笆盾更玡叫氨ゎCTILINK狝叭?", "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }

  //RemoveTAPIDrv();

  GetLoadedTSPList();

  //plCreateTAPI->Top = lvSwitchPort->Height/2-25;
  plCreateTAPI->Left = lvSwitchPort->Width/2-180;
  plCreateTAPI->Visible = true;

  if (strTSPList->Count > 0)
  {
    Label105->Caption = (gLangID == 1) ? "正在卸载驱动：" : "タ更臱笆:";
    lbTAPIExtrn->Caption = "";
    ProgressBar1->Max = strTSPList->Count;

    for (int i=0; i<strTSPList->Count; i++)
    {
      ProgressBar1->Position = i;
      Application->ProcessMessages();
      if (ldPermanentProviderID[i] == 0)
        continue;
      mylineRemoveProvider(ldPermanentProviderID[i], NULL);
    }
  }

  GetLoadedTSPList();

  if (SwitchPortList.CTILinkType == 1)
  {
    AddTAPIDrv("tspi2w.tsp", SwitchPortList.dwProviderID);
    CreateTAPIRegPro();
    plCreateTAPI->Visible = false;
    return;
  }

  if (g_nSwitchMode != 1)
   return;
  /*if (g_nAuthSwitchType != 9)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您没有授权加载TAPI驱动!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"癸ぃ癬眤⊿Τ甭舦更TAPI臱笆!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }*/

  Label105->Caption = (gLangID == 1) ? "正在重新加载驱动：" : "タ穝更臱笆:";
  lbTAPIExtrn->Caption = "";

  ProgressBar1->Max = SwitchPortList.SwitchPortNum;
  for (int i=0; i<SwitchPortList.SwitchPortNum; i++)
  {
    if (SwitchPortList.SwitchPort[i].m_nChStyle != 1
      && SwitchPortList.SwitchPort[i].m_nChStyle != 3
      && SwitchPortList.SwitchPort[i].m_nChStyle != 15
      && SwitchPortList.SwitchPort[i].m_nChStyle != 16)
      return;
    ProgressBar1->Position = i;
    //lbTAPIExtrn->Caption = SwitchPortList.SwitchPort[i].m_strPortID;
    //Application->ProcessMessages();
    try
    {
      nDeviceID = StrToInt(SwitchPortList.SwitchPort[i].m_strPortID);
      if (CreateTAPIDrv(nDeviceID, SwitchPortList.SwitchPort[i].dwProviderID) == 0)
      {
        CreateTAPIRegLite(nDeviceID);
        lbTAPIExtrn->Caption = SwitchPortList.SwitchPort[i].m_strPortID + ((gLangID == 1) ? "成功" : "Θ");
        if (gLangID == 1)
          WriteAlartMsg("加载 %d TAPI驱动成功！", nDeviceID);
        else
          WriteAlartMsg("更 %d TAPI臱笆Θ!", nDeviceID);
      }
      else
      {
        lbTAPIExtrn->Caption = SwitchPortList.SwitchPort[i].m_strPortID + ((gLangID == 1) ? "失败" : "ア毖");
        if (gLangID == 1)
          WriteAlartMsg("加载 %d TAPI驱动失败！", nDeviceID);
        else
          WriteAlartMsg("更 %d TAPI臱笆ア毖!", nDeviceID);
        break;
      }
    }
    catch (...)
    {
      lbTAPIExtrn->Caption = SwitchPortList.SwitchPort[i].m_strPortID + ((gLangID == 1) ? "失败" : "ア毖");
      if (gLangID == 1)
        WriteAlartMsg("加载 %d TAPI驱动失败！", nDeviceID);
      else
        WriteAlartMsg("更 %d TAPI臱笆ア毖!", nDeviceID);
      break;
    }
    Application->ProcessMessages();
  }
  plCreateTAPI->Visible = false;
  GetLoadedTSPList();
  if (gLangID == 1)
    MessageBox(NULL,"加载所有TAPI驱动完成!","信息提示",MB_OK|MB_ICONINFORMATION);
  else
    MessageBox(NULL,"更┮ΤTAPI臱笆ЧΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::TAPI1Click(TObject *Sender)
{
  if (g_nSwitchMode != 1)
    return;
  if (SwitchPortList.CTILinkType == 1)
  {
    mylineConfigProvider(NULL, SwitchPortList.dwProviderID);
    return;
  }
  if (g_CurSwitchPort < 0)
  {
    if (gLangID == 1)
      MessageBox(NULL,"请选择需要配置的交换机端口序号!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫匡拒惠璶皌竚ユ传诀硈钡梆腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }
  if (SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 1
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 3
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 15
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 16)
  {
    return;
  }
  mylineConfigProvider(NULL, SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbCTILinkTypeChange(TObject *Sender)
{
  bSwitchPortParamChange = true;
  btApply->Enabled = true;
  if (cbCTILinkType->Text == "CTILinkLite(膀セ)")
  {
    btTAPI->Visible = true;
    btUnistallTAPI->Visible = true;
  }
  else
  {
    btTAPI->Visible = false;
    btUnistallTAPI->Visible = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N22Click(TObject *Sender)
{
  TListItem  *ListItem1;
  lvAgStatus->ViewStyle = vsReport;
  int nItemCount = lvAgStatus->Items->Count;
  for (int i=0; i<nItemCount; i++)
  {
    ListItem1 = lvAgStatus->Items->Item[i];
    ListItem1->Caption = IntToStr(i);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N23Click(TObject *Sender)
{
  TListItem  *ListItem1;
  lvAgStatus->ViewStyle = vsIcon;
  int nItemCount = lvAgStatus->Items->Count;
  for (int i=0; i<nItemCount; i++)
  {
    ListItem1 = lvAgStatus->Items->Item[i];
    ListItem1->Caption = ListItem1->SubItems->Strings[0] + '\n' + ListItem1->SubItems->Strings[9];
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N24Click(TObject *Sender)
{
  lvWorkerList->ViewStyle = vsReport;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::N25Click(TObject *Sender)
{
  lvWorkerList->ViewStyle = vsIcon;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::TAPI2Click(TObject *Sender)
{
  if (g_nSwitchMode != 1)
    return;
  if (SwitchPortList.CTILinkType == 1)
  {
    return;
  }
  if (SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 1
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 3
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 15
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 16)
  {
    return;
  }
  if (gLangID == 1)
  {
    if ( MessageBox( NULL, "您确认要加载选择分机的TAPI驱动吗，加载前请停止CTILINK服务？", "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
    if (g_CurSwitchPort < 0)
    {
      MessageBox(NULL,"请选择需要加载的交换机端口序号!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    GetLoadedTSPList();
    RemoveTAPIDrv(SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID);
    try
    {
      int nDeviceID = StrToInt(SwitchPortList.SwitchPort[g_CurSwitchPort].m_strPortID);
      if (CreateTAPIDrv(nDeviceID, SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID) == 0)
      {
        CreateTAPIRegLite(nDeviceID);
        MessageBox(NULL,"加载该分机的TAPI驱动成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
      else
      {
        MessageBox(NULL,"加载该分机的TAPI驱动失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    catch (...)
    {
      MessageBox(NULL,"加载该分机的TAPI驱动失败!","信息提示",MB_OK|MB_ICONINFORMATION);
    }
  }
  else
  {
    if ( MessageBox( NULL, "眤絋粄璶更匡拒だ诀TAPI臱笆盾更玡叫氨ゎCTILINK狝叭?", "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
    if (g_CurSwitchPort < 0)
    {
      MessageBox(NULL,"叫匡拒惠璶更ユ传诀硈钡梆腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    GetLoadedTSPList();
    RemoveTAPIDrv(SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID);
    try
    {
      int nDeviceID = StrToInt(SwitchPortList.SwitchPort[g_CurSwitchPort].m_strPortID);
      if (CreateTAPIDrv(nDeviceID, SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID) == 0)
      {
        CreateTAPIRegLite(nDeviceID);
        MessageBox(NULL,"更赣だ诀TAPI臱笆Θ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
      else
      {
        MessageBox(NULL,"更赣だ诀TAPI臱笆ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    catch (...)
    {
      MessageBox(NULL,"更赣だ诀TAPI臱笆ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::TAPI3Click(TObject *Sender)
{
  char newfilename[256], szSystemPath[256];
  if (g_nSwitchMode != 1)
    return;
  if (SwitchPortList.CTILinkType == 1)
  {
    return;
  }
  if (SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 1
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 3
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 15
    && SwitchPortList.SwitchPort[g_CurSwitchPort].m_nChStyle != 16)
  {
    return;
  }
  if (gLangID == 1)
  {
    if ( MessageBox( NULL, "您确认要卸载选择分机的TAPI驱动吗，卸载前请停止CTILINK服务？", "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
    if (g_CurSwitchPort < 0)
    {
      MessageBox(NULL,"请选择需要卸载的交换机端口序号!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    GetLoadedTSPList();
    if (SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID == 0)
    {
      return;
    }
    RemoveTAPIDrv(SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID);
    memset(szSystemPath, 0, 256);
    GetSystemDirectory(szSystemPath, 256);
    sprintf(newfilename, "%s\\tspi2w_%s.tsp", szSystemPath, SwitchPortList.SwitchPort[g_CurSwitchPort].m_strPortID.c_str());
    DeleteFile(newfilename);
    MessageBox(NULL,"该分机端口的TAPI驱动已卸载!","信息提示",MB_OK|MB_ICONINFORMATION);
  }
  else
  {
    if ( MessageBox( NULL, "眤絋粄璶更匡拒だ诀TAPI臱笆盾更玡叫氨ゎCTILINK狝叭?", "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
    if (g_CurSwitchPort < 0)
    {
      MessageBox(NULL,"叫匡拒惠璶更ユ传诀硈钡梆腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    GetLoadedTSPList();
    if (SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID == 0)
    {
      return;
    }
    RemoveTAPIDrv(SwitchPortList.SwitchPort[g_CurSwitchPort].dwProviderID);
    memset(szSystemPath, 0, 256);
    GetSystemDirectory(szSystemPath, 256);
    sprintf(newfilename, "%s\\tspi2w_%s.tsp", szSystemPath, SwitchPortList.SwitchPort[g_CurSwitchPort].m_strPortID.c_str());
    DeleteFile(newfilename);
    MessageBox(NULL,"赣だ诀硈钡梆TAPI臱笆更!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btUnistallTAPIClick(TObject *Sender)
{
  char newfilename[256], szSystemPath[256];
  int nDeviceID;

  if (gLangID == 1)
  {
    if ( MessageBox( NULL, "您确认要卸载所有TAPI驱动吗，卸载前请停止CTILINK服务？", "系统提示：", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  else
  {
    if ( MessageBox( NULL, "眤絋粄璶更┮ΤTAPI臱笆盾更玡叫氨ゎCTILINK狝叭?", "╰参矗ボ:", MB_YESNO|MB_ICONQUESTION ) == IDNO )
      return;
  }
  memset(szSystemPath, 0, 256);
  GetSystemDirectory(szSystemPath, 256);

  GetLoadedTSPList();

  //plCreateTAPI->Top = lvSwitchPort->Height/2-25;
  plCreateTAPI->Left = lvSwitchPort->Width/2-180;
  plCreateTAPI->Visible = true;

  if (strTSPList->Count > 0)
  {
    Label105->Caption = (gLangID == 1) ?"正在卸载驱动：" : "タ更臱笆:";
    lbTAPIExtrn->Caption = "";
    ProgressBar1->Max = strTSPList->Count;

    for (int i=0; i<strTSPList->Count; i++)
    {
      ProgressBar1->Position = i;
      lbTAPIExtrn->Caption = "ProviderID="+IntToStr(ldPermanentProviderID[i]);
      Application->ProcessMessages();
      if (ldPermanentProviderID[i] == 0)
        continue;
      mylineRemoveProvider(ldPermanentProviderID[i], NULL);
    }
  }

  for (int i=0; i<SwitchPortList.SwitchPortNum; i++)
  {
    sprintf(newfilename, "%s\\tspi2w_%s.tsp", szSystemPath, SwitchPortList.SwitchPort[i].m_strPortID.c_str());
    DeleteFile(newfilename);
  }
  GetLoadedTSPList();
  plCreateTAPI->Visible = false;
  if (gLangID == 1)
    MessageBox(NULL, "卸载所有TAPI驱动完成!","信息提示",MB_OK|MB_ICONINFORMATION);
  else
    MessageBox(NULL, "更┮ΤTAPI臱笆ЧΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::editSwitchIPChange(TObject *Sender)
{
  bSwitchPortParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbCardTypeChange(TObject *Sender)
{
  bTCPParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cbHostTypeChange(TObject *Sender)
{
  if (cbHostType->Text == ((gLangID == 1) ? "主机" : "诀"))
  {
    Label114->Visible = false;
    editMasterIP->Visible = false;
  }
  else
  {
    Label114->Visible = true;
    editMasterIP->Visible = true;
  }
  bTCPParamChange = true;
  btApply->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallVOCserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "VOCSERVICE\\QuarkCallVOCService.exe -install";
  #else
  strTemp = strTemp + "VOCSERVICE\\UnimeCallVOCServer.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshVOCServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,VOC_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgVOCServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartVOCService->Enabled = false;
        btStopVOCService->Enabled = true;
      }
      else
      {
        imgVOCServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartVOCService->Enabled = true;
        btStopVOCService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartVOCServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, VOC_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgVOCServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartVOCService->Enabled = false;
        btStopVOCService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动VOC语音资源服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆VOC粂戈方狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"VOC语音资源服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"VOC粂戈方狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopVOCServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,VOC_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgVOCServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartVOCService->Enabled = true;
          btStopVOCService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btInstallWSserviceClick(TObject *Sender)
{
  char szPath[256];
  int nPos;
  AnsiString strTemp;

  strTemp = g_strExecPath.UpperCase();
  nPos = strTemp.AnsiPos("CONFIG");
  strTemp = g_strExecPath.SubString(1, nPos-1);
  #ifdef QUARKCALL_PLATFORM
  strTemp = strTemp + "WEBSOCKET\\QuarkCallWebSocket.exe -install";
  #else
  strTemp = strTemp + "WEBSOCKET\\UnimeCallWebSocket.exe -install";
  #endif
  FormMain->ProcFLWMsg(0, strTemp.c_str());
  WinExec(strTemp.c_str(), SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btRefreshWSServiceStatusClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,WS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        imgWSServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartWSService->Enabled = false;
        btStopWSService->Enabled = true;
      }
      else
      {
        imgWSServiceStatus->Picture->LoadFromFile(imgStopFile);
        btStartWSService->Enabled = true;
        btStopWSService->Enabled = false;
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStartWSServiceClick(TObject *Sender)
{
  SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
  if (scm!=NULL)
  {
    SC_HANDLE svc = OpenService(scm, WS_SERVICE_NAME, SERVICE_START);
    if (svc != NULL)
    {
      if (StartService(svc, 0, NULL))
      {
        imgWSServiceStatus->Picture->LoadFromFile(imgStartFile);
        btStartWSService->Enabled = false;
        btStopWSService->Enabled = true;
      }
      else
      {
        if (gLangID == 1)
          MessageBox(NULL,"启动WS服务失败!!!","警告",MB_OK|MB_ICONWARNING);
        else
          MessageBox(NULL,"币笆WS狝叭ア毖!!!","牡",MB_OK|MB_ICONWARNING);
      }
      CloseServiceHandle(svc);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"WS服务未安装!!!","警告",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"WS狝叭ゼ杆!!!","牡",MB_OK|MB_ICONWARNING);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btStopWSServiceClick(TObject *Sender)
{
  if (g_nLoginlevelId != 1)
  {
    if (gLangID == 1)
      MessageBox(NULL,"对不起，您无权进入该项操作!","信息提示",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"癸ぃ癬眤礚舦秈赣兜巨!","獺矗ボ",MB_OK|MB_ICONWARNING);
    return;
  }
  SC_HANDLE scm = OpenSCManager(NULL,NULL, SC_MANAGER_ALL_ACCESS);
  if (scm != NULL)
  {
    SC_HANDLE svc = OpenService(scm,WS_SERVICE_NAME, SERVICE_STOP|SERVICE_QUERY_STATUS);
    if (svc!=NULL)
    {
      SERVICE_STATUS ServiceStatus;
      QueryServiceStatus(svc, &ServiceStatus);
      if (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
      {
        if (ControlService(svc, SERVICE_CONTROL_STOP, &ServiceStatus))
        {
          imgWSServiceStatus->Picture->LoadFromFile(imgStopFile);
          btStartWSService->Enabled = true;
          btStopWSService->Enabled = false;
        }
      }
      CloseServiceHandle(svc);
    }
    CloseServiceHandle(scm);
  }
}
//---------------------------------------------------------------------------

