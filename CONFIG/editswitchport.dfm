object FormEditSwitchPort: TFormEditSwitchPort
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #20132#25563#27231#36890#36947#31649#29702
  ClientHeight = 283
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 67
    Top = 23
    Width = 60
    Height = 12
    Caption = #36890#36947#24207#34399#65306
  end
  object Label2: TLabel
    Left = 68
    Top = 48
    Width = 60
    Height = 12
    Caption = #20998#27231#34399#30908#65306
  end
  object Label3: TLabel
    Left = 70
    Top = 102
    Width = 60
    Height = 12
    Caption = #36890#36947#39006#22411#65306
  end
  object Label4: TLabel
    Left = 70
    Top = 128
    Width = 60
    Height = 12
    Caption = #30828#20214#39006#22411#65306
  end
  object Label5: TLabel
    Left = 57
    Top = 155
    Width = 72
    Height = 12
    Caption = #29289#29702#36890#36947#34399#65306
  end
  object Label6: TLabel
    Left = 44
    Top = 207
    Width = 84
    Height = 12
    Caption = #25209#37327#22686#21152#25976#37327#65306
  end
  object Label9: TLabel
    Left = 44
    Top = 72
    Width = 84
    Height = 12
    Caption = #27969#31243#25765#20837#34399#30908#65306
  end
  object editPortID: TEdit
    Left = 139
    Top = 43
    Width = 131
    Height = 20
    MaxLength = 20
    TabOrder = 0
    OnChange = editPortIDChange
  end
  object cbLgChType: TComboBox
    Left = 139
    Top = 97
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 1
    Text = #24231#24109
    OnChange = cbLgChTypeChange
    OnKeyPress = cbLgChTypeKeyPress
    Items.Strings = (
      #20013#32380
      #24231#24109
      'IVR'
      #20659#30495)
  end
  object cbChStyle: TComboBox
    Left = 139
    Top = 123
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 2
    Text = #39006#27604#20998#27231
    OnChange = cbLgChTypeChange
    OnKeyPress = cbLgChTypeKeyPress
    Items.Strings = (
      #39006#27604#20998#27231
      #39006#27604#20013#32380
      '2B+D'#25976#20301#35441#27231
      'DSS1'#25976#20301#20013#32380
      'TUP'#25976#20301#20013#32380
      'ISUP'#25976#20301#20013#32380
      'PRI-USER'
      'PRI-NET'
      'H323'#20998#27231
      'SIP'#20998#27231
      'E&M'#20013#32380
      'SIP'#20013#32380
      'H323'#20013#32380
      'LineSide')
  end
  object csePhyPortNo: TCSpinEdit
    Left = 139
    Top = 149
    Width = 132
    Height = 21
    TabOrder = 3
    OnChange = editPortIDChange
  end
  object Button1: TButton
    Left = 61
    Top = 245
    Width = 81
    Height = 27
    Caption = #30906#23450
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 234
    Top = 245
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 5
    OnClick = Button2Click
  end
  object editPortNo: TEdit
    Left = 139
    Top = 17
    Width = 131
    Height = 20
    Color = clGrayText
    ReadOnly = True
    TabOrder = 6
  end
  object ckBatch: TCheckBox
    Left = 139
    Top = 175
    Width = 113
    Height = 19
    Caption = #26159#21542#25209#37327#22686#21152#65311
    TabOrder = 7
    OnClick = ckBatchClick
  end
  object cseBatchNum: TCSpinEdit
    Left = 139
    Top = 201
    Width = 131
    Height = 21
    MaxValue = 1024
    MinValue = 1
    TabOrder = 8
    Value = 1
  end
  object editFlwCode: TEdit
    Left = 139
    Top = 67
    Width = 131
    Height = 20
    MaxLength = 20
    TabOrder = 9
    OnChange = editPortIDChange
  end
end
