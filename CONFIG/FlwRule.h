//---------------------------------------------------------------------------
#ifndef FlwRuleH
#define FlwRuleH
//---------------------------------------------------------------------------
#include <stdio.h>
#include "boxinc.h"
#include "../include/msgh/ivrlog.h"
//---------------------------------------------------------------------------
//系统关键字结构定义
typedef struct
{
    UC state;
    US KeyId; //关键字序号
    UC KeyName[MAX_VARNAME_LEN]; //关键字名称
    UC KeyType; //变量作用域: 0未知 1-指令名关键字 2-属性名关键字 3-系统变量名关键字 4-函数名关键字 5-参数值宏定义关键字 6-运算符关键字

} VXML_KEYWORDS_STRUCT;

//系统关键字结构定义

//---------------------------------------------------------------------------
//流程指令规则类
class CFlwRule
{
public:
    CFlwRule();
    virtual ~CFlwRule();

    UC CallerId; //是否接收主叫号码(0-无所谓 1-必须接收主叫号码)
    UC MinCallerLen; //主叫号码最少长度(当CallerId=1时有效)
    UC MinCalledLen; //被叫号码最少长度(当CallerId=1时有效)
    //US MaxCmdNum; //实际指令规则条数

    //整数范围规则
    const VXML_INTEGER_RANGE_RULE_STRUCT      *IntRange;//[MAX_INT_RANGE_NUM];
    //单字符范围规则
    const VXML_CHAR_RANGE_RULE_STRUCT         *CharRange;//[MAX_CHAR_RANGE_NUM];
    //字符串中允许出现的字符规则
    const VXML_STRING_RANGE_RULE_STRUCT       *StrRange;//[MAX_STR_RANGE_NUM];
    //整数枚举字符串规则
    const VXML_MACRO_INT_RANGE_RULE_STRUCT    *MacroIntRange;//[MAX_MACRO_INT_NUM];
    //枚举字符串规则
    const VXML_MACRO_STR_RANGE_RULE_STRUCT    *MacroStrRange;//[MAX_MACRO_STR_NUM];
    //返回消息规则
    const VXML_RECV_MSG_CMD_RULE_STRUCT             *IVRLOGMsgRule;
    //返回消息规则
    const VXML_SEND_MSG_CMD_RULE_STRUCT             *LOGIVRMsgRule;
};
//---------------------------------------------------------------------------
#endif
