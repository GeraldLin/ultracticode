//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "setupacc.h"
#include "main.h"
extern int g_CurFlwNo;
extern int g_CurAccNo;
extern int gLangID;
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormAccCode *FormAccCode;
//---------------------------------------------------------------------------
__fastcall TFormAccCode::TFormAccCode(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormAccCode::SetModifyType(bool bmodify, AnsiString acccode, int minlen, int maxlen)
{
  if (bmodify == false)
  {
    bModify = bmodify;
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
  }
  else
  {
    bModify = bmodify;
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
  }
  editAccCode->Text = acccode;
  cseMinLen->Value = minlen;
  cseMaxLen->Value = maxlen;
  Button1->Enabled = false;
}

void __fastcall TFormAccCode::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------


void __fastcall TFormAccCode::editAccCodeExit(TObject *Sender)
{
  cseMinLen->Value = editAccCode->Text.Length();
  cseMaxLen->Value = editAccCode->Text.Length();
}
//---------------------------------------------------------------------------

void __fastcall TFormAccCode::editAccCodeChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormAccCode::Button1Click(TObject *Sender)
{
  int nResult;
  if (editAccCode->Text == "")
  {
    if (gLangID == 1)
      MessageBox(NULL,"请设置接入号码!","信息提示",MB_OK|MB_ICONINFORMATION);
    else
      MessageBox(NULL,"叫砞﹚挤腹絏!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    return;
  }

  if (bModify == false)
  {
    nResult = FormMain->FlwFileList.FlwFile[g_CurFlwNo].AddAccCode(editAccCode->Text, cseMinLen->Value, cseMaxLen->Value);
    if (nResult == 0)
    {
      this->Close();
      if (gLangID == 1)
        MessageBox(NULL,"增加接入号码成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"糤挤腹絏Θ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      FormMain->bFLWParamChange = true;
      FormMain->btApply->Enabled = true;
      FormMain->DispAccCode(FormMain->FlwFileList.FlwFile[g_CurFlwNo]);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"增加接入号码失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"糤挤腹絏ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    }
  }
  else
  {
    nResult = FormMain->FlwFileList.FlwFile[g_CurFlwNo].EditAccCode(g_CurAccNo, editAccCode->Text, cseMinLen->Value, cseMaxLen->Value);
    if (nResult == 0)
    {
      this->Close();
      if (gLangID == 1)
        MessageBox(NULL,"修改接入号码成功!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"э挤腹絏Θ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      FormMain->bFLWParamChange = true;
      FormMain->btApply->Enabled = true;
      FormMain->DispAccCode(FormMain->FlwFileList.FlwFile[g_CurFlwNo]);
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"修改接入号码失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      else
        MessageBox(NULL,"э挤腹絏ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
    }
  }
}
//---------------------------------------------------------------------------

