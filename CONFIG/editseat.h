//---------------------------------------------------------------------------

#ifndef editseatH
#define editseatH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditSeat : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TEdit *editPort;
  TEdit *editSeatNo;
  TComboBox *cbSeatType;
  TCSpinEdit *cseSeatGroupNo;
  TCSpinEdit *cseWorkerGroupNo;
  TCSpinEdit *cseWorkerLevel;
  TEdit *editWorkerNo;
  TButton *Button1;
  TButton *Button2;
  void __fastcall cbSeatTypeKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSeat(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSeat *FormEditSeat;
//---------------------------------------------------------------------------
#endif
