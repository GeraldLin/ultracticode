object FormLogin: TFormLogin
  Left = 192
  Top = 114
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #30331#37636#24179#33274
  ClientHeight = 171
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 27
    Top = 39
    Width = 60
    Height = 12
    Caption = #30331#37636#24115#34399#65306
  end
  object Label2: TLabel
    Left = 27
    Top = 74
    Width = 60
    Height = 12
    Caption = #30331#37636#23494#30908#65306
  end
  object cbUserName: TComboBox
    Left = 95
    Top = 35
    Width = 130
    Height = 20
    ItemHeight = 12
    TabOrder = 0
    Text = 'administrator'
    OnKeyPress = cbUserNameKeyPress
    Items.Strings = (
      'administrator'
      'manager'
      'operator'
      'monitor')
  end
  object editPassword: TEdit
    Left = 95
    Top = 69
    Width = 130
    Height = 20
    PasswordChar = '*'
    TabOrder = 1
  end
  object Button1: TButton
    Left = 26
    Top = 130
    Width = 81
    Height = 27
    Caption = #30331#37636
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 147
    Top = 130
    Width = 82
    Height = 27
    Caption = #25918#26820
    TabOrder = 3
    OnClick = Button2Click
  end
  object ckAutoLogin: TCheckBox
    Left = 26
    Top = 104
    Width = 140
    Height = 18
    Caption = #19979#27425#26159#21542#33258#21205#30331#37636#65311
    TabOrder = 4
  end
end
