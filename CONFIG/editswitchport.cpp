//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "editswitchport.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormEditSwitchPort *FormEditSwitchPort;
extern int gLangID;
extern int MyIsNumber(AnsiString str);
extern int MyIsDigits(AnsiString str);
//---------------------------------------------------------------------------
__fastcall TFormEditSwitchPort::TFormEditSwitchPort(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormEditSwitchPort::SetModifyType(int nmodify, CSwitchPort &switchport)
{
  nModify = nmodify;
  if (nmodify == 1)
  {
    //增加
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = true;
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
  else if (nmodify == 2)
  {
    //插入
    Button1->Caption = (gLangID == 1) ? "插入" : "础";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = true;
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
  else
  {
    //修改
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
    ckBatch->Checked = false;
    cseBatchNum->Value = 1;
    ckBatch->Visible = false;
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
  editPortNo->Text = switchport.m_nPortNo;
  editPortID->Text = switchport.m_strPortID;
  if (switchport.m_nLgChType == 1 && switchport.m_nChStyle == 2)
  {
    Label9->Visible = true;
    editFlwCode->Visible = true;
  }
  else
  {
    Label9->Visible = false;
    editFlwCode->Visible = false;
  }
  editFlwCode->Text = switchport.m_strFlwCode;
  
  switch (switchport.m_nLgChType)
  {
  case 1:
    cbLgChType->Text = (gLangID == 1) ? "中继" : "い膥";
    break;
  case 2:
    cbLgChType->Text = (gLangID == 1) ? "座席" : "畒畊";
    break;
  case 3:
    cbLgChType->Text = (gLangID == 1) ? "IVR" : "IVR";
    break;
  case 4:
    cbLgChType->Text = (gLangID == 1) ? "录音" : "魁";
    break;
  case 5:
    cbLgChType->Text = (gLangID == 1) ? "传真" : "肚痷";
    break;
  default:
    cbLgChType->Text = (gLangID == 1) ? "未知" : "ゼ";
    break;
  }
  switch (switchport.m_nChStyle)
  {
  case 1:
    cbChStyle->Text = (gLangID == 1) ? "模拟分机" : "摸ゑだ诀";
    break;
  case 2:
    cbChStyle->Text = (gLangID == 1) ? "模拟中继" : "摸ゑい膥";
    break;
  case 3:
    cbChStyle->Text = (gLangID == 1) ? "2B+D数字话机" : "2B+D计杠诀";
    break;
  case 8:
    cbChStyle->Text = (gLangID == 1) ? "DSS1数字中继" : "DSS1计い膥";
    break;
  case 9:
    cbChStyle->Text = (gLangID == 1) ? "TUP数字中继" : "TUP计い膥";
    break;
  case 10:
    cbChStyle->Text = (gLangID == 1) ? "ISUP数字中继" : "ISUP计い膥";
    break;
  case 11:
    cbChStyle->Text = (gLangID == 1) ? "PRI用户侧" : "PRI-USER";
    break;
  case 12:
    cbChStyle->Text = (gLangID == 1) ? "PRI网络侧" : "PRI-NET";
    break;
  case 15:
    cbChStyle->Text = (gLangID == 1) ? "H323分机" : "H323だ诀";
    break;
  case 16:
    cbChStyle->Text = (gLangID == 1) ? "SIP分机" : "SIPだ诀";
    break;
  case 17:
    cbChStyle->Text = (gLangID == 1) ? "E&M中继" : "E&Mい膥";
    break;
  case 18:
    cbChStyle->Text = (gLangID == 1) ? "SIP中继" : "SIPい膥";
    break;
  case 19:
    cbChStyle->Text = (gLangID == 1) ? "H323中继" : "H323い膥";
    break;
  case 21:
    cbChStyle->Text = "LineSide";
    break;
  default:
    cbChStyle->Text = (gLangID == 1) ? "未知" : "ゼ";
    break;
  }
  csePhyPortNo->Value = switchport.m_nPhyPortNo;

  Button1->Enabled = false;
}

void __fastcall TFormEditSwitchPort::cbLgChTypeKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSwitchPort::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSwitchPort::editPortIDChange(TObject *Sender)
{
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSwitchPort::Button1Click(TObject *Sender)
{
  CSwitchPort SwitchPort;
  AnsiString strDeviceID;
  char dispbuf[256];
  int nResult, n, count=0;
  if (gLangID == 1)
  {
    if (editPortID->Text == "")
    {
      MessageBox(NULL,"请设置设备分机号码!","信息提示",MB_OK|MB_ICONWARNING);
      return;
    }
    SwitchPort.m_nPortNo = StrToInt(editPortNo->Text);
    SwitchPort.m_strPortID = editPortID->Text;
    SwitchPort.m_strFlwCode = editFlwCode->Text;

    if (cbLgChType->Text == "中继")
    {
      SwitchPort.m_nLgChType = 1;
    }
    else if (cbLgChType->Text == "座席")
    {
      SwitchPort.m_nLgChType = 2;
    }
    else if (cbLgChType->Text == "IVR")
    {
      SwitchPort.m_nLgChType = 3;
    }
    else if (cbLgChType->Text == "传真")
    {
      SwitchPort.m_nLgChType = 5;
    }

    if (cbChStyle->Text == "模拟分机")
    {
      SwitchPort.m_nChStyle = 1;
    }
    else if (cbChStyle->Text == "模拟中继")
    {
      SwitchPort.m_nChStyle = 2;
    }
    else if (cbChStyle->Text == "2B+D数字话机")
    {
      SwitchPort.m_nChStyle = 3;
    }
    else if (cbChStyle->Text == "DSS1数字中继")
    {
      SwitchPort.m_nChStyle = 8;
    }
    else if (cbChStyle->Text == "TUP数字中继")
    {
      SwitchPort.m_nChStyle = 9;
    }
    else if (cbChStyle->Text == "ISUP数字中继")
    {
      SwitchPort.m_nChStyle = 10;
    }
    else if (cbChStyle->Text == "PRI用户侧")
    {
      SwitchPort.m_nChStyle = 11;
    }
    else if (cbChStyle->Text == "PRI网络侧")
    {
      SwitchPort.m_nChStyle = 12;
    }
    else if (cbChStyle->Text == "H323分机")
    {
      SwitchPort.m_nChStyle = 15;
    }
    else if (cbChStyle->Text == "SIP分机")
    {
      SwitchPort.m_nChStyle = 16;
    }
    else if (cbChStyle->Text == "E&M中继")
    {
      SwitchPort.m_nChStyle = 17;
    }
    else if (cbChStyle->Text == "SIP中继")
    {
      SwitchPort.m_nChStyle = 18;
    }
    else if (cbChStyle->Text == "H323中继")
    {
      SwitchPort.m_nChStyle = 19;
    }
    else if (cbChStyle->Text == "LineSide")
    {
      SwitchPort.m_nChStyle = 21;
    }
    SwitchPort.m_nPhyPortNo = csePhyPortNo->Value;

    if (SwitchPort.m_nLgChType == 1)
    {
      if (MyIsNumber(editPortID->Text) == 0)
      {
        n = StrToInt(editPortID->Text);
      }
      else
      {
        if (editPortID->Text.SubString(1,1) != "T")
        {
          MessageBox(NULL,"设置的中继设备号有误!","信息提示",MB_OK|MB_ICONWARNING);
          return;
        }
        if (MyIsNumber(editPortID->Text.SubString(2,20)) != 0)
        {
          MessageBox(NULL,"设置的中继设备号有误!","信息提示",MB_OK|MB_ICONWARNING);
          return;
        }
        n = StrToInt(editPortID->Text.SubString(2,20));
      }
    }
    else
    {
      if (MyIsNumber(editPortID->Text) != 0)
      {
        MessageBox(NULL,"设置的分机号有误!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      n = StrToInt(editPortID->Text);
    }
    if (nModify == 1)
    {
      //增加
      if (ckBatch->Checked == true)
      {
        for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SwitchPortList.isThePortIDExist(-1, SwitchPort.m_strPortID))
          {
            nResult = FormMain->SwitchPortList.AddSwitchPort(SwitchPort);
            if (nResult == 0)
            {
              count++;
              SwitchPort.m_nPortNo++;
              if (SwitchPort.m_nPhyPortNo > 0)
              {
                SwitchPort.m_nPhyPortNo++;
              }
            }
          }
          n++;
          if (SwitchPort.m_nLgChType == 1)
          {
            if (cseBatchNum->Value < 100)
              sprintf(dispbuf, "T%02d", n);
            else
              sprintf(dispbuf, "T%03d", n);
            SwitchPort.m_strPortID = (char *)dispbuf;
          }
          else
          {
            SwitchPort.m_strPortID = IntToStr(n);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "共增加了 %d 个端口，请按 应用 键修改后的参数才能生效!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
          FormMain->bSwitchPortParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSwitchPort();
        }
        else
        {
          MessageBox(NULL,"增加交换机端口失败,可能是分机号码重复,或超过限定的端口数量!","信息提示",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }

      if (FormMain->SwitchPortList.isThePortIDExist(-1, editPortID->Text))
      {
        MessageBox(NULL,"该设备分机号码已存在!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      nResult = FormMain->SwitchPortList.AddSwitchPort(SwitchPort);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"增加交换机端口成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bSwitchPortParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSwitchPort();
      }
      else
      {
        MessageBox(NULL,"增加交换机端口失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else if (nModify == 2)
    {
      //插入
      if (ckBatch->Checked == true)
      {
          for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SwitchPortList.isThePortIDExist(-1, SwitchPort.m_strPortID))
          {
            nResult = FormMain->SwitchPortList.InsertSwitchPort(SwitchPort.m_nPortNo, SwitchPort);
            if (nResult == 0)
            {
              count++;
              SwitchPort.m_nPortNo++;
              if (SwitchPort.m_nPhyPortNo > 0)
              {
                SwitchPort.m_nPhyPortNo++;
              }
            }
          }
          n++;
          if (SwitchPort.m_nLgChType == 1)
          {
            if (editPortID->Text.SubString(1,1) == "T")
            {
              if (cseBatchNum->Value < 100)
                sprintf(dispbuf, "T%02d", n);
              else
                sprintf(dispbuf, "T%03d", n);
            }
            else
            {
              sprintf(dispbuf, "%d", n);
            }
            SwitchPort.m_strPortID = (char *)dispbuf;
          }
          else
          {
            SwitchPort.m_strPortID = IntToStr(n);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "共插入了 %d 个端口，请按 应用 键修改后的参数才能生效!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
          FormMain->bSwitchPortParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSwitchPort();
        }
        else
        {
          MessageBox(NULL,"插入交换机端口失败,可能是分机号码重复,或超过限定的端口数量!","信息提示",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }

      if (FormMain->SwitchPortList.isThePortIDExist(-1, editPortID->Text))
      {
        MessageBox(NULL,"该设备分机号码已存在!","信息提示",MB_OK|MB_ICONWARNING);
        return;
      }
      nResult = FormMain->SwitchPortList.InsertSwitchPort(SwitchPort.m_nPortNo, SwitchPort);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"插入交换机端口成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bSwitchPortParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSwitchPort();
      }
      else
      {
        MessageBox(NULL,"插入交换机端口失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      //修改
      //if (FormMain->SwitchPortList.isThePortIDExist(SwitchPort.m_nPortNo, editPortID->Text))
      //{
      //  MessageBox(NULL,"该分机设备号码已存在!","信息提示",MB_OK|MB_ICONWARNING);
      //  return;
      //}
      nResult = FormMain->SwitchPortList.EditSwitchPort(SwitchPort.m_nPortNo, SwitchPort);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"修改交换机端口成功，请按 应用 键修改后的参数才能生效!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bSwitchPortParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSwitchPort();
      }
      else
      {
        MessageBox(NULL,"修改交换机端口失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
  else
  {
    if (editPortID->Text == "")
    {
      MessageBox(NULL,"叫砞﹚砞称だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
      return;
    }
    SwitchPort.m_nPortNo = StrToInt(editPortNo->Text);
    SwitchPort.m_strPortID = editPortID->Text;

    if (cbLgChType->Text == "い膥")
    {
      SwitchPort.m_nLgChType = 1;
    }
    else if (cbLgChType->Text == "畒畊")
    {
      SwitchPort.m_nLgChType = 2;
    }
    else if (cbLgChType->Text == "IVR")
    {
      SwitchPort.m_nLgChType = 3;
    }
    else if (cbLgChType->Text == "肚痷")
    {
      SwitchPort.m_nLgChType = 5;
    }

    if (cbChStyle->Text == "摸ゑだ诀")
    {
      SwitchPort.m_nChStyle = 1;
    }
    else if (cbChStyle->Text == "摸ゑい膥")
    {
      SwitchPort.m_nChStyle = 2;
    }
    else if (cbChStyle->Text == "2B+D计杠诀")
    {
      SwitchPort.m_nChStyle = 3;
    }
    else if (cbChStyle->Text == "DSS1计い膥")
    {
      SwitchPort.m_nChStyle = 8;
    }
    else if (cbChStyle->Text == "TUP计い膥")
    {
      SwitchPort.m_nChStyle = 9;
    }
    else if (cbChStyle->Text == "ISUP计い膥")
    {
      SwitchPort.m_nChStyle = 10;
    }
    else if (cbChStyle->Text == "PRI-USER")
    {
      SwitchPort.m_nChStyle = 11;
    }
    else if (cbChStyle->Text == "PRI-NET")
    {
      SwitchPort.m_nChStyle = 12;
    }
    else if (cbChStyle->Text == "H323だ诀")
    {
      SwitchPort.m_nChStyle = 15;
    }
    else if (cbChStyle->Text == "SIPだ诀")
    {
      SwitchPort.m_nChStyle = 16;
    }
    else if (cbChStyle->Text == "E&Mい膥")
    {
      SwitchPort.m_nChStyle = 17;
    }
    else if (cbChStyle->Text == "SIPい膥")
    {
      SwitchPort.m_nChStyle = 18;
    }
    else if (cbChStyle->Text == "H323い膥")
    {
      SwitchPort.m_nChStyle = 19;
    }
    else if (cbChStyle->Text == "LineSide")
    {
      SwitchPort.m_nChStyle = 21;
    }
    SwitchPort.m_nPhyPortNo = csePhyPortNo->Value;

    if (SwitchPort.m_nLgChType == 1)
    {
      if (MyIsNumber(editPortID->Text) == 0)
      {
        n = StrToInt(editPortID->Text);
      }
      else
      {
        if (editPortID->Text.SubString(1,1) != "T")
        {
          MessageBox(NULL,"砞﹚い膥砞称腹Τ粇!","獺矗ボ",MB_OK|MB_ICONWARNING);
          return;
        }
        if (MyIsNumber(editPortID->Text.SubString(2,20)) != 0)
        {
          MessageBox(NULL,"砞﹚い膥砞称腹Τ粇!","獺矗ボ",MB_OK|MB_ICONWARNING);
          return;
        }
        n = StrToInt(editPortID->Text.SubString(2,20));
      }
    }
    else
    {
      if (MyIsNumber(editPortID->Text) != 0)
      {
        MessageBox(NULL,"砞﹚だ诀腹絏Τ粇!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      n = StrToInt(editPortID->Text);
    }
    if (nModify == 1)
    {
      //增加
      if (ckBatch->Checked == true)
      {
        for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SwitchPortList.isThePortIDExist(-1, SwitchPort.m_strPortID))
          {
            nResult = FormMain->SwitchPortList.AddSwitchPort(SwitchPort);
            if (nResult == 0)
            {
              count++;
              SwitchPort.m_nPortNo++;
              if (SwitchPort.m_nPhyPortNo > 0)
              {
                SwitchPort.m_nPhyPortNo++;
              }
            }
          }
          n++;
          if (SwitchPort.m_nLgChType == 1)
          {
            if (editPortID->Text.SubString(1,1) == "T")
            {
              if (cseBatchNum->Value < 100)
                sprintf(dispbuf, "T%02d", n);
              else
                sprintf(dispbuf, "T%03d", n);
            }
            else
            {
              sprintf(dispbuf, "%d", n);
            }
            SwitchPort.m_strPortID = (char *)dispbuf;
          }
          else
          {
            SwitchPort.m_strPortID = IntToStr(n);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "糤 %d 硈钡梆叫 甅ノ 龄э把计ネ!", count);
          MessageBox(NULL,dispbuf,"信息提示",MB_OK|MB_ICONINFORMATION);
          FormMain->bSwitchPortParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSwitchPort();
        }
        else
        {
          MessageBox(NULL,"糤ユ传诀硈钡梆ア毖,琌だ诀腹絏確,┪禬筁﹚硈钡梆计秖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }

      if (FormMain->SwitchPortList.isThePortIDExist(-1, editPortID->Text))
      {
        MessageBox(NULL,"赣砞称だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      nResult = FormMain->SwitchPortList.AddSwitchPort(SwitchPort);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"糤ユ传诀狠Θ叫 莱ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bSwitchPortParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSwitchPort();
      }
      else
      {
        MessageBox(NULL,"糤ユ传诀狠ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else if (nModify == 2)
    {
      //插入
      if (ckBatch->Checked == true)
      {
          for (int i=0; i<cseBatchNum->Value; i++)
        {
          if (!FormMain->SwitchPortList.isThePortIDExist(-1, SwitchPort.m_strPortID))
          {
            nResult = FormMain->SwitchPortList.InsertSwitchPort(SwitchPort.m_nPortNo, SwitchPort);
            if (nResult == 0)
            {
              count++;
              SwitchPort.m_nPortNo++;
              if (SwitchPort.m_nPhyPortNo > 0)
              {
                SwitchPort.m_nPhyPortNo++;
              }
            }
          }
          n++;
          if (SwitchPort.m_nLgChType == 1)
          {
            if (cseBatchNum->Value < 100)
              sprintf(dispbuf, "T%02d", n);
            else
              sprintf(dispbuf, "T%03d", n);
            SwitchPort.m_strPortID = (char *)dispbuf;
          }
          else
          {
            SwitchPort.m_strPortID = IntToStr(n);
          }
        }
        if (count > 0)
        {
          this->Close();
          sprintf(dispbuf, "础 %d 硈钡梆叫 甅ノ 龄э把计ネ!", count);
          MessageBox(NULL,dispbuf,"獺矗ボ",MB_OK|MB_ICONINFORMATION);
          FormMain->bSwitchPortParamChange = true;
          FormMain->btApply->Enabled = true;
          FormMain->DispSwitchPort();
        }
        else
        {
          MessageBox(NULL,"础ユ传诀硈钡梆ア毖,琌だ诀腹絏確,┪禬筁﹚硈钡梆计秖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        }
        return;
      }

      if (FormMain->SwitchPortList.isThePortIDExist(-1, editPortID->Text))
      {
        MessageBox(NULL,"赣砞称だ诀腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
        return;
      }
      nResult = FormMain->SwitchPortList.InsertSwitchPort(SwitchPort.m_nPortNo, SwitchPort);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"础ユ传诀硈钡梆Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bSwitchPortParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSwitchPort();
      }
      else
      {
        MessageBox(NULL,"础ユ传诀硈钡梆ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      //修改
      //if (FormMain->SwitchPortList.isThePortIDExist(SwitchPort.m_nPortNo, editPortID->Text))
      //{
      //  MessageBox(NULL,"赣だ诀砞称腹絏!","獺矗ボ",MB_OK|MB_ICONWARNING);
      //  return;
      //}
      nResult = FormMain->SwitchPortList.EditSwitchPort(SwitchPort.m_nPortNo, SwitchPort);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"эユ传诀硈钡梆Θ叫 甅ノ 龄э把计ネ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bSwitchPortParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispSwitchPort();
      }
      else
      {
        MessageBox(NULL,"эユ传诀硈钡梆ア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSwitchPort::ckBatchClick(TObject *Sender)
{
  if (ckBatch->Checked == true)
  {
    Label6->Visible = true;
    cseBatchNum->Visible = true;
  }
  else
  {
    Label6->Visible = false;
    cseBatchNum->Visible = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormEditSwitchPort::cbLgChTypeChange(TObject *Sender)
{
  if (gLangID == 1)
  {
    if (cbLgChType->Text == "中继" && cbChStyle->Text == "模拟中继")
    {
      Label9->Visible = true;
      editFlwCode->Visible = true;
    }
    else
    {
      Label9->Visible = false;
      editFlwCode->Visible = false;
    }
  }
  else
  {
    if (cbLgChType->Text == "い膥" && cbChStyle->Text == "摸ゑい膥")
    {
      Label9->Visible = true;
      editFlwCode->Visible = true;
    }
    else
    {
      Label9->Visible = false;
      editFlwCode->Visible = false;
    }
  }
  Button1->Enabled = true;
}
//---------------------------------------------------------------------------

