object FormFlw: TFormFlw
  Left = 193
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #27969#31243#37197#32622
  ClientHeight = 217
  ClientWidth = 407
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 78
    Top = 21
    Width = 105
    Height = 14
    AutoSize = False
    Caption = #33139#26412#25991#20214#21517#31281':'
  end
  object Label2: TLabel
    Left = 78
    Top = 75
    Width = 88
    Height = 14
    AutoSize = False
    Caption = #26989#21209#21151#33021#34399':'
  end
  object Label3: TLabel
    Left = 9
    Top = 173
    Width = 351
    Height = 13
    Caption = #27880': '#35531#23559#35373#23450#30340#27969#31243#25991#20214#25335#35997#25110#19978#20659#21040#27969#31243#25991#20214#23384#25918#30340#25991#20214#22846'.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 38
    Top = 191
    Width = 195
    Height = 13
    Caption = #27969#31243#21151#33021#34399#19981#33021#37325#24489'.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 78
    Top = 47
    Width = 78
    Height = 12
    Caption = #27969#31243#21151#33021#25551#36848':'
  end
  object Label6: TLabel
    Left = 78
    Top = 98
    Width = 54
    Height = 12
    Caption = #21855#21205#26041#24335':'
  end
  object editFlwFile: TEdit
    Left = 191
    Top = 17
    Width = 131
    Height = 20
    TabOrder = 0
    OnChange = editFlwFileChange
  end
  object cseFuncNo: TCSpinEdit
    Left = 191
    Top = 69
    Width = 131
    Height = 21
    MaxValue = 64
    TabOrder = 1
    Value = 1
    OnChange = editFlwFileChange
  end
  object Button1: TButton
    Left = 95
    Top = 130
    Width = 82
    Height = 27
    Caption = #30906#23450
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 269
    Top = 130
    Width = 81
    Height = 27
    Caption = #21462#28040
    TabOrder = 3
    OnClick = Button2Click
  end
  object editExplain: TEdit
    Left = 191
    Top = 43
    Width = 131
    Height = 20
    TabOrder = 4
    OnChange = editFlwFileChange
  end
  object cbRunAfterLoad: TComboBox
    Left = 191
    Top = 95
    Width = 131
    Height = 20
    ItemHeight = 12
    TabOrder = 5
    Text = #38651#35441
    OnChange = editFlwFileChange
    Items.Strings = (
      #38651#35441
      #33258#21205)
  end
end
