//---------------------------------------------------------------------------

#ifndef editvopchnH
#define editvopchnH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "public.h"
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormEditVopChn : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TEdit *editVopNo;
  TEdit *editVopChnNo;
  TComboBox *cbVopChnType;
  TComboBox *cbPortID;
  TButton *Button1;
  TButton *Button2;
  TCheckBox *ckBatch;
  TLabel *Label6;
  TCSpinEdit *cseBatchNum;
  void __fastcall cbVopChnTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall cbVopChnTypeChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall cbPortIDChange(TObject *Sender);
  void __fastcall ckBatchClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditVopChn(TComponent* Owner);

  int nModify;
  CVopChn m_VopChn;
  void AddPortIDItemList(int nVopchnType);
  void SetModifyType(int nmodify, CVopChn &vopchn);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditVopChn *FormEditVopChn;
//---------------------------------------------------------------------------
#endif
