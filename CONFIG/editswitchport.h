//---------------------------------------------------------------------------

#ifndef editswitchportH
#define editswitchportH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include "public.h"
//---------------------------------------------------------------------------
class TFormEditSwitchPort : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TEdit *editPortID;
  TComboBox *cbLgChType;
  TComboBox *cbChStyle;
  TCSpinEdit *csePhyPortNo;
  TButton *Button1;
  TButton *Button2;
  TEdit *editPortNo;
  TCheckBox *ckBatch;
  TLabel *Label6;
  TCSpinEdit *cseBatchNum;
  TLabel *Label9;
  TEdit *editFlwCode;
  void __fastcall cbLgChTypeKeyPress(TObject *Sender, char &Key);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editPortIDChange(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall ckBatchClick(TObject *Sender);
  void __fastcall cbLgChTypeChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormEditSwitchPort(TComponent* Owner);

  int nModify;
  void SetModifyType(int nmodify, CSwitchPort &switchport);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormEditSwitchPort *FormEditSwitchPort;
//---------------------------------------------------------------------------
#endif
