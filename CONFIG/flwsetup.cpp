//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "flwsetup.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFormFlw *FormFlw;
extern int g_CurFlwNo;
extern int g_CurAccNo;
extern int gLangID;
//---------------------------------------------------------------------------
__fastcall TFormFlw::TFormFlw(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormFlw::SetModifyType(bool bmodify, AnsiString flwfile, AnsiString explain, int funcno, int runafterload)
{
  if (bmodify == false)
  {
    bModify = bmodify;
    Button1->Caption = (gLangID == 1) ? "增加" : "糤";
  }
  else
  {
    bModify = bmodify;
    Button1->Caption = (gLangID == 1) ? "修改" : "э";
  }
  editFlwFile->Text = flwfile;
  editExplain->Text = explain;
  cseFuncNo->Value = funcno;
  if (runafterload == 0)
    cbRunAfterLoad->Text = (gLangID == 1) ? "电话" : "筿杠";
  else
    cbRunAfterLoad->Text = (gLangID == 1) ? "自动" : "笆";
  Button1->Enabled = false;
}

void __fastcall TFormFlw::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------


void __fastcall TFormFlw::Button1Click(TObject *Sender)
{
  int nResult, nTemp;

  if (gLangID == 1)
  {
    if (editFlwFile->Text == "")
    {
      MessageBox(NULL,"请设置流程脚本文件名!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (cseFuncNo->Value == 0)
    {
      MessageBox(NULL,"请设置流程功能号!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (bModify == false)
      nTemp = -1;
    else
      nTemp = g_CurFlwNo;
    if (FormMain->FlwFileList.isTheFuncNoExist(nTemp, cseFuncNo->Value))
    {
      MessageBox(NULL,"该流程功能号已存在!","信息提示",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (bModify == false)
    {
      if (cbRunAfterLoad->Text == "电话")
        nResult = FormMain->FlwFileList.AddFlwFile(editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 0);
      else
        nResult = FormMain->FlwFileList.AddFlwFile(editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 1);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"增加流程脚本成功!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bFLWParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispFlw();
      }
      else
      {
        MessageBox(NULL,"增加流程脚本失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      if (cbRunAfterLoad->Text == "电话")
        nResult = FormMain->FlwFileList.EditFlwFile(g_CurFlwNo, editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 0);
      else
        nResult = FormMain->FlwFileList.EditFlwFile(g_CurFlwNo, editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 1);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"修改流程脚本成功!","信息提示",MB_OK|MB_ICONINFORMATION);
        FormMain->bFLWParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispFlw();
      }
      else
      {
        MessageBox(NULL,"修改流程脚本失败!","信息提示",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
  else
  {
    if (editFlwFile->Text == "")
    {
      MessageBox(NULL,"叫砞砞﹚瑈祘竲セゅン!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (cseFuncNo->Value == 0)
    {
      MessageBox(NULL,"叫砞﹚瑈祘絪腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (bModify == false)
      nTemp = -1;
    else
      nTemp = g_CurFlwNo;
    if (FormMain->FlwFileList.isTheFuncNoExist(nTemp, cseFuncNo->Value))
    {
      MessageBox(NULL,"赣瑈祘絪腹!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      return;
    }
    if (bModify == false)
    {
      if (cbRunAfterLoad->Text == "筿杠")
        nResult = FormMain->FlwFileList.AddFlwFile(editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 0);
      else
        nResult = FormMain->FlwFileList.AddFlwFile(editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 1);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"糤瑈祘竲セΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bFLWParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispFlw();
      }
      else
      {
        MessageBox(NULL,"糤瑈祘竲セア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
    else
    {
      if (cbRunAfterLoad->Text == "筿杠")
        nResult = FormMain->FlwFileList.EditFlwFile(g_CurFlwNo, editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 0);
      else
        nResult = FormMain->FlwFileList.EditFlwFile(g_CurFlwNo, editFlwFile->Text, editExplain->Text, cseFuncNo->Value, 1);
      if (nResult == 0)
      {
        this->Close();
        MessageBox(NULL,"э瑈祘竲セΘ!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
        FormMain->bFLWParamChange = true;
        FormMain->btApply->Enabled = true;
        FormMain->DispFlw();
      }
      else
      {
        MessageBox(NULL,"э瑈祘竲セア毖!","獺矗ボ",MB_OK|MB_ICONINFORMATION);
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormFlw::editFlwFileChange(TObject *Sender)
{
  Button1->Enabled = true;  
}
//---------------------------------------------------------------------------

