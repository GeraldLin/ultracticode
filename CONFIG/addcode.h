//---------------------------------------------------------------------------

#ifndef addcodeH
#define addcodeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFormAddCode : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *editAccCode;
  TCSpinEdit *cseMinLen;
  TCSpinEdit *cseMaxLen;
  TButton *Button1;
  TButton *Button2;
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall editAccCodeChange(TObject *Sender);
  void __fastcall editAccCodeExit(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAddCode(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAddCode *FormAddCode;
//---------------------------------------------------------------------------
#endif
