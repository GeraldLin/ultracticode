//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "modifypsw.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormModifyPSW *FormModifyPSW;
extern int gLangID;
extern void SendModifyPswMsg(AnsiString strUserName, AnsiString strOldPassword, AnsiString strNewPassword);
//---------------------------------------------------------------------------
__fastcall TFormModifyPSW::TFormModifyPSW(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormModifyPSW::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormModifyPSW::Button1Click(TObject *Sender)
{
  SendModifyPswMsg(cbUserName->Text, editOldPassword->Text, editNewPassword->Text);
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormModifyPSW::cbUserNameKeyPress(TObject *Sender,
      char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------

