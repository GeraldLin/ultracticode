//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "selpath.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormPath *FormPath;
//---------------------------------------------------------------------------
__fastcall TFormPath::TFormPath(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TFormPath::Setpath(int id, AnsiString path)
{
  Id = id;
  if (path == "")
    return;
  try
  {
    DriveComboBox1->Drive = path[1];
    DirectoryListBox1->Directory = path;
  }
  catch (...)
  {
  }
}

void __fastcall TFormPath::Button1Click(TObject *Sender)
{
  switch (Id)
  {
  case 1:
    SelPath = DirectoryListBox1->Directory;
    FormMain->editFLWPATH->Text = SelPath;
    break;
  case 2:
    SelPath = DirectoryListBox1->Directory;
    FormMain->editMemVocPath->Text = SelPath;
    break;
  case 3:
    SelPath = DirectoryListBox1->Directory;
    FormMain->editPlayVocPath->Text = SelPath;
    break;
  case 4:
    SelPath = DirectoryListBox1->Directory;
    //FormMain->editIVRMsgSavePath->Text = SelPath;
    break;
  case 5:
    SelPath = DirectoryListBox1->Directory;
    //FormMain->editFLWMsgSavePath->Text = SelPath;
    break;
  }
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormPath::Button2Click(TObject *Sender)
{
  this->Close();
}
//---------------------------------------------------------------------------
