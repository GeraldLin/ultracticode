//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "login.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormLogin *FormLogin;

extern int gLangID;
extern int g_nLoginlevelId, g_nLoginId;
extern AnsiString g_strLoginUsername, g_strPassword;
extern AnsiString g_strAdminPSW, g_strManagerPSW, g_strOperatorPSW, g_strMonitorPSW;
extern bool g_bAutoLoginId;
extern bool g_bRunOnServicePCId;
extern void SendLoginIVRMsg(AnsiString strUserName, AnsiString strPassword);
//---------------------------------------------------------------------------
__fastcall TFormLogin::TFormLogin(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormLogin::cbUserNameKeyPress(TObject *Sender, char &Key)
{
  Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TFormLogin::Button2Click(TObject *Sender)
{
  this->Close();  
}
//---------------------------------------------------------------------------
void __fastcall TFormLogin::Button1Click(TObject *Sender)
{
  g_strLoginUsername = cbUserName->Text;
  g_strPassword = editPassword->Text;
  g_bAutoLoginId = ckAutoLogin->Checked;

  if (g_bRunOnServicePCId == true)
  {
    if (g_strLoginUsername == "administrator" && g_strPassword == g_strAdminPSW)
    {
      g_nLoginlevelId =1;
      g_nLoginId = 1;
    }
    else if (g_strLoginUsername == "manager" && g_strPassword == g_strManagerPSW)
    {
      g_nLoginlevelId =2;
      g_nLoginId = 1;
    }
    else if (g_strLoginUsername == "operator"&& g_strPassword == g_strOperatorPSW)
    {
      g_nLoginlevelId =3;
      g_nLoginId = 1;
    }
    else if (g_strLoginUsername == "monitor"&& g_strPassword == g_strMonitorPSW)
    {
      g_nLoginlevelId =4;
      g_nLoginId = 1;
    }
    else
    {
      if (gLangID == 1)
        MessageBox(NULL,"��¼�������!!!","����",MB_OK|MB_ICONWARNING);
      else
        MessageBox(NULL,"�n���K�X���~!!!","ĵ�i",MB_OK|MB_ICONWARNING);
      return;
    }
    if (gLangID == 1)
      FormMain->btLogin->Caption = "�ǳ�";
    else
      FormMain->btLogin->Caption = "�n�X";
    FormMain->btModifyPsw->Enabled = true;

    if (gLangID == 1)
      MessageBox(NULL,"��¼ƽ̨�ɹ�!!!","����",MB_OK|MB_ICONWARNING);
    else
      MessageBox(NULL,"�n�����O���\!!!","ĵ�i",MB_OK|MB_ICONWARNING);
  }

  SendLoginIVRMsg(cbUserName->Text, editPassword->Text);
  this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormLogin::FormShow(TObject *Sender)
{
  editPassword->SetFocus();  
}
//---------------------------------------------------------------------------

