//---------------------------------------------------------------------------
#ifndef MainfuncH
#define MainfuncH
//---------------------------------------------------------------------------
//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-內部調試信息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
void MyWriteFile(LPCTSTR msg);
//取當前日期時間字符串
char *GetNowTime();
void CreateAllDirectories(CString strDir);
//根據參數名取參數值
short GetParamByName(const char *paramstr, const char *paramname, short defaultval, short minval, short maxval);
//檢查接收消息隊列
void CheckMsgFifo();
//-------------------IVR通信事件處理-------------------------------------------
void OnWEBLogin(unsigned short serverid,unsigned short clientid);     //tcllinkc.dll callback function
void OnWEBReceiveData(unsigned short remoteid,unsigned short msgtype, const unsigned char *buf, unsigned long len); //tcllinkc.dll callback function
void OnWEBClose(unsigned short serverid,unsigned short clientid);    //tcllinkc.dll callback function
//讀總的服務的配置文件（返回0表示成功）
int ReadSERVICEIniFile();
void ReadCustomerFieldSetup();
void ProcQueryCostomerResult(const char *pXMLResult, const char *pszCallerNo);

int ConnectDB();
void ProcSqlFiFo();

//初始化用到的類資源（返回0表示成功）
int InitResourceMem();
//運行前初始化所有（返回0表示成功）
int InitRun(void);
//釋放資源
void ReleaseAllResouce();
//主循環處理
void APPLoop();

//---------------------------------------------------------------------------
#endif
