// TCPServer.h: interface for the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#ifndef __TCPLINK__H__
#define __TCPLINK__H__
//---------------------------------------------------------------------------
//#include "stdafx.h"
//#include "../comm/include/tlinko.h"

struct TServer
{
	char  ServerIP[32];
	short ServerPort;
	unsigned short Serverid;
  bool  IsConnected;
};

class CTCPLink
{
	HINSTANCE hdll; //動態庫句柄；

  typedef int  (*APPCLOSEALL)(void);
  typedef int  (*APPMAINLOOP)(void);
  typedef int  (*APPSENDDATA)(unsigned short remoteid,
                              unsigned short msgtype,
                              const unsigned char *buf,
                              unsigned long len);

  typedef void (*ONAPPRECEIVEDATA)(unsigned short remoteid,
                                  unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len);
  typedef void (*ONAPPLOGIN)(unsigned short serverid,unsigned short clientid);
  typedef void (*ONAPPCLOSE)(unsigned short serverid,unsigned short clientid);

  typedef void (*APPINIT)(unsigned short server_id,
                          unsigned short client_id,
                          const char *server_ip_address,
                          unsigned short server_port,
                          ONAPPLOGIN OnLOGLogin,
                          ONAPPCLOSE OnLOGClose,
                          ONAPPRECEIVEDATA OnLOGReceiveData
                          );
  typedef void (*APPINITS)(unsigned short server_id,
                          unsigned short server_port,
                          ONAPPLOGIN OnIVRLogin,
                          ONAPPCLOSE OnIVRClose,
                          ONAPPRECEIVEDATA OnIVRReceiveData
                          );
private:
	void ReleaseDll();
public:
	CTCPLink();
	virtual ~CTCPLink();
  unsigned short WEBServerId; //本端ID標號
  int WEBServerPort;
  
  char DllFile[64];
  bool LoadDll();
  bool IsOpen; //dll is opend
  bool isWEBInit; //服務器初始化標志
	
  //申明函數變量
  APPCLOSEALL     AppCloseAll;
  APPMAINLOOP     AppMainLoop;
  APPSENDDATA     AppSendData;
  APPINIT         AppInit;
  APPINITS        AppInitS;

public:
  int InitWEBServer();
  int SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf);
  int SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len);

  bool ReadIni(char *filename);

public:
	//組合消息類型及消息編號
	US CombineID(UC IdType, UC IdNo)
	{
	  US m_i1, m_i2;
	  m_i1 = ( IdType << 8 ) & 0xFF00;
	  m_i2 = IdNo & 0x00FF;
	  return (m_i1 | m_i2);
	}
	//分解對象標識及編號
	void DisCombineID(US ID, UC &IdType, UC &IdNo)
	{
	  IdType = ( ID >> 8 ) & 0x00FF;
	  IdNo = ID & 0x00FF;
	}

};

extern void OnWEBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                  const unsigned char *buf,
                                  unsigned long len); //need define in main.cpp
extern void OnWEBLogin(unsigned short serverid,unsigned short clientid);    //need define in main.cpp
extern void OnWEBClose(unsigned short serverid,unsigned short clientid);   //need define in main.cpp

//---------------------------------------------------------------------------
#endif // !defined(AFX_TCPSERVER_H__A15B7B15_5824_444E_9B1E_A087BAA5B4CC__INCLUDED_)
