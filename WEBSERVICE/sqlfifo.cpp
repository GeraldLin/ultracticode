//---------------------------------------------------------------------------
/*
接收消息先進先出隊列
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "sqlfifo.h"

//---------------------------------------------------------------------------
unsigned short CSQLSfifo::Write(const char *SqlBuf, unsigned short SqlLen)         
{
  if (SqlLen == 0)
    return 0;
  m_cCriticalSection.Lock();
	if(IsFull()) //full
  {
    m_cCriticalSection.Unlock();
		return 0;
  }

  if (SqlLen >= 4096)
    pSqls[Tail].SqlLen = 4095;
  else
    pSqls[Tail].SqlLen = SqlLen;
  memset(pSqls[Tail].SqlBuf, 0, 4096);
  memcpy(pSqls[Tail].SqlBuf, SqlBuf, pSqls[Tail].SqlLen);
  pSqls[Tail].SqlBuf[pSqls[Tail].SqlLen] = 0;

	if(++Tail >= Len)
    Tail=0;

  m_cCriticalSection.Unlock();
	return 1;
}

unsigned short CSQLSfifo::Read(char *SqlBuf, unsigned short &SqlLen)
{
  m_cCriticalSection.Lock();
  if(Head==Tail)
  {
    m_cCriticalSection.Unlock();
    return 0;//Buf Null
  }
	
  SqlLen = pSqls[Head].SqlLen;
  memcpy(SqlBuf, pSqls[Head].SqlBuf, SqlLen);
  SqlBuf[SqlLen] = 0;

	DiscardHead();

  m_cCriticalSection.Unlock();
	return 1;  
}
