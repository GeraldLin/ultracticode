//---------------------------------------------------------------------------
#include "stdafx.h"
#include "flwrule.h"


#include "../include/rule/intrule.cpp"
#include "../include/rule/charrule.cpp"
#include "../include/rule/strrule.cpp"
#include "../include/rule/intmacro.cpp"
#include "../include/rule/strmacro.cpp"

#include "../include/msgcpp/xmldb.cpp"
#include "../include/msgcpp/dbxml.cpp"
//---------------------------------------------------------------------------
CFlwRule::CFlwRule()
{
	IntRange=IntRangeArray;
	CharRange=CharRangeArray;
	StrRange=StrRangeArray;
	MacroIntRange=MacroIntRangeArray;
	MacroStrRange=MacroStrRangeArray;
	DBXMLMsgRule=DBXMLMsgRuleArray;
	XMLDBMsgRule=XMLDBMsgRuleArray;
}
CFlwRule::~CFlwRule()
{
}


