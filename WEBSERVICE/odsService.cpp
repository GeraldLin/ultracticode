//////////////////////////////////////////////////////////////////////////
//
// odsService.cpp
//
//		有關 NT 服務控制的通用函數實現
//
//
//////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "odsService.h"
#include "main.h"

//-----------------------------------------------------------------------
// odsServiceMessageBox				- 服務中彈出的對話框
//-----------------------------------------------------------------------
void odsServiceMessageBox( LPCTSTR lpszText )
{
	OSVERSIONINFO	osVersionInfo;
	UINT			uType;

	uType = MB_OK | MB_ICONSTOP;

	osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	if (!GetVersionEx(&osVersionInfo))	
		return;

	if (osVersionInfo.dwMajorVersion < 4)
		uType |= MB_SERVICE_NOTIFICATION_NT3X;
	else
		uType |= MB_SERVICE_NOTIFICATION;

	MessageBox( NULL, lpszText, _T("SERVICE"), uType );
}

//-----------------------------------------------------------------------
// odsReportStatusToSCMgr				- 將自己的狀態通知服務控制器
//
// 參數：
//		hServiceStatus					- 服務狀態句柄
//		dwCurrentState					- 要設置的狀態
//		DWORD dwErrorCode				- 錯誤碼，0 表示無錯誤
//		dwWaitHint						- 控制器等待間隔
//		pstError						- 接收錯誤信息指針（可以為 NULL ）
// 返回：
//		bSuccess						- 成功與否
//-----------------------------------------------------------------------
BOOL odsReportStatusToSCMgr(SERVICE_STATUS_HANDLE hServiceStatus,
							DWORD dwCurrentState,
							DWORD dwErrorCode,
							DWORD dwWaitHint,
							STODSERROR* pstError )
{
    static DWORD dwCheckPoint = 0;    
	SERVICE_STATUS	ssStatus;

	memset( &ssStatus, 0, sizeof(ssStatus) );
 
	// 
	// * 注意：如果下面的設置中沒有 SERVICE_ACCEPT_SHUTDOWN，則服務在運行時將
	// * 不接收 NT 重新啟動的控制消息，而此時將不做 Cleanup 的工作，與 ORACLE 
	// * 的連接沒有釋放。將造成 ORACLE 服務端的垃圾進程保留，重啟動多次后，會
	// * 耗盡 ORACLE 的服務端進程，使 ORACLE 癱瘓。
	//
    if ( dwCurrentState == SERVICE_RUNNING )  
        ssStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
    else
        ssStatus.dwControlsAccepted = 0;

	ssStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    ssStatus.dwCurrentState = dwCurrentState;

	if (dwErrorCode != 0)
		ssStatus.dwWin32ExitCode = ERROR_SERVICE_SPECIFIC_ERROR;
	else
		ssStatus.dwWin32ExitCode = NO_ERROR;
	ssStatus.dwServiceSpecificExitCode = dwErrorCode;
    ssStatus.dwWaitHint = dwWaitHint;

    if ( ( dwCurrentState == SERVICE_RUNNING ) ||
         ( dwCurrentState == SERVICE_STOPPED ) ||
		 ( dwCurrentState == SERVICE_PAUSED ) )
        dwCheckPoint = 0;
    else
        dwCheckPoint++;
	ssStatus.dwCheckPoint = dwCheckPoint;

    // Report the status of the service to the service control manager.
    //
    if (!SetServiceStatus( hServiceStatus, &ssStatus)) 
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_REPORTSTATUS );
        odsMakeError( pstError, ODS_ERR_REPORTSTATUS, NULL );
		return FALSE;
    }    
    else
		return TRUE;
}


//-----------------------------------------------------------------------
// odsInstallService				- 安裝服務
// 
// 參數：
//		strComputerName				- 計算機名（NULL表示本地計算機）
//		strServiceName				- 服務名稱
//		strServicePath				- 服務路徑
//		strDisplayName				- 服務的顯示名稱
//		dwStartType					- 服務的啟動方式
//									- SERVICE_AUTO_START 或 SERVICE_DEMAND_START
//		strDependencies				- 服務的依賴關系（可以為 NULL ）
//		pstError					- 接收錯誤信息指針（可以為 NULL ）
// 返回：
//		bSuccess					- 成功與否
//-----------------------------------------------------------------------
BOOL odsInstallService( LPCTSTR strComputerName, 
					   LPCTSTR strServiceName, 
					   LPCTSTR strServicePath, 
					   LPCTSTR strDisplayName, 
					   DWORD dwStartType,
					   LPCTSTR strDependencies,
					   STODSERROR* pstError )
{
	BOOL		bRet = TRUE;
    SC_HANDLE   schService;
    SC_HANDLE   schSCManager;
    //char szShowMsg[128];

    schSCManager = OpenSCManager(
                        strComputerName,        // machine (NULL == local)
                        NULL,                   // database (NULL == default)
                        SC_MANAGER_ALL_ACCESS   // access required
                        );
    if ( schSCManager )
    {
        schService = CreateService(
            schSCManager,               // SCManager database
            strServiceName,		        // name of service
            strDisplayName,				// name to display
            SERVICE_ALL_ACCESS,         // desired access
            SERVICE_WIN32_OWN_PROCESS,  // service type
            dwStartType,				// start type
            SERVICE_ERROR_NORMAL,       // error control type
            strServicePath,             // service's binary
            NULL,                       // no load ordering group
            NULL,                       // no tag identifier
            strDependencies,			// dependencies
            NULL,                       // LocalSystem account
            NULL);                      // no password

        if ( schService )
        {            
            CloseServiceHandle(schService);
        }
        else
        {
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_INSTSERVICE );
            odsMakeError( pstError, ODS_ERR_INSTSERVICE, NULL );
			bRet = FALSE;
        }

        CloseServiceHandle(schSCManager);
    }
    else
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSCM );
		odsMakeError( pstError, ODS_ERR_OPENSCM, NULL );
		bRet = FALSE;
	}
//   if (bRet)
//   {
//     sprintf(szShowMsg, "%s install success!", SRV_SERVICEDISPLAYNAME);
//     MessageBox(NULL,szShowMsg,"Information:",MB_OK|MB_ICONINFORMATION);
//   } 
//   else
//   {
//     sprintf(szShowMsg, "%s install fail!", SRV_SERVICEDISPLAYNAME);
//     MessageBox(NULL,szShowMsg,"Warning:",MB_OK|MB_ICONWARNING);
//   }
	return bRet;
}


//----------------------------------------------------------------
// odsRemoveService					- 刪除某計算機上的服務
//
// 參數：
//		strComputerName				- 計算機名，為 NULL 時表示本地計算機
//		strServiceName				- 服務名
//		dwWaitTime					- 等待時間
//		pstError					- 錯誤信息結構指針，可以為 NULL
// 返回：
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsRemoveService( LPCTSTR strComputerName, LPCTSTR strServiceName, DWORD dwWaitTime, STODSERROR* pstError )
{
	BOOL			bRet = TRUE;
	BOOL			bStopped = FALSE;
    SC_HANDLE		schService;
    SC_HANDLE		schSCManager;
	SERVICE_STATUS	ssStatus;
	DWORD			dwSleepLeft = dwWaitTime;
	DWORD			dwSleep = min( 1000, dwSleepLeft );
  //char szShowMsg[128];

    schSCManager = OpenSCManager(
                        strComputerName,        // machine (NULL == local)
                        NULL,                   // database (NULL == default)
                        SC_MANAGER_ALL_ACCESS   // access required
                        );
    if ( schSCManager )
    {
        schService = OpenService( schSCManager, strServiceName, SERVICE_ALL_ACCESS );

        if (schService)
        {
            // try to stop the service
            if ( ControlService( schService, SERVICE_CONTROL_STOP, &ssStatus ) )
            {
                do
				{
					Sleep( dwSleep );
					dwSleepLeft -= dwSleep;
					dwSleep = min( 1000, dwSleepLeft );

					QueryServiceStatus( schService, &ssStatus );
                
                    if ( ssStatus.dwCurrentState == SERVICE_STOPPED )
					{
						bStopped = TRUE;
						break;
					}
                }while( dwSleepLeft > 0 );
            }
            // now remove the service
            if( !DeleteService(schService) )
			{
				odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_DELSERVICE );
				odsMakeError( pstError, ODS_ERR_DELSERVICE, NULL );
				bRet = FALSE;
			}

            CloseServiceHandle(schService);
        }
        else
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSERVICE );
            odsMakeError( pstError, ODS_ERR_OPENSERVICE, NULL );
			bRet = FALSE;
		}

        CloseServiceHandle(schSCManager);
    }
    else
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSCM );
		odsMakeError( pstError, ODS_ERR_OPENSCM, NULL );
		bRet = FALSE;
	}

//   if (bRet)
//   {
//     sprintf(szShowMsg, "%s uninstall success!", SRV_SERVICEDISPLAYNAME);
//     MessageBox(NULL,szShowMsg,"Information:",MB_OK|MB_ICONINFORMATION);
//   } 
//   else
//   {
//     sprintf(szShowMsg, "%s uninstall fail!", SRV_SERVICEDISPLAYNAME);
//     MessageBox(NULL,szShowMsg,"Warning:",MB_OK|MB_ICONWARNING);
//   }
	return bRet;
}

//----------------------------------------------------------------
// odsGetServiceStatus			- 取得指定機器上的服務程序的狀態
//
// 參數:
//		lpszComputerName		- 服務程序所在的計算機名稱，如果為 NULL
//								- 則指本地機器
//		lpszServiceName			- 服務程序名稱
//		lpdwServiceStatus		- 返回服務程序的狀態
//		pstError				- 錯誤信息結構指針，可以為 NULL//
// 返回值:
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsGetServiceStatus( LPCTSTR strComputerName, LPCTSTR strServiceName, 
						  LPDWORD lpdwServiceStatus, STODSERROR* pstError )
{
	BOOL			bRet = TRUE;
	SC_HANDLE		schSCManager;			//服務控制管理器的句柄
	SC_HANDLE		schService;				//服務程序的句柄
	SERVICE_STATUS	ssServiceStatus;		//服務狀態結構
	
	schSCManager = OpenSCManager(
                        strComputerName,				// machine (NULL == local)
                        NULL,							// database (NULL == default)
                        GENERIC_READ				    // access required
                        );
    if ( schSCManager )
    {
        schService = OpenService( schSCManager, strServiceName, GENERIC_READ );

        if (schService)
        {
            if ( QueryServiceStatus(schService, &ssServiceStatus) )
			{
				if (lpdwServiceStatus != NULL)
					(*lpdwServiceStatus) = ssServiceStatus.dwCurrentState;
			}
			else
			{
				odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_QUERYSERVICE );
				odsMakeError( pstError, ODS_ERR_QUERYSERVICE, NULL );
				bRet = FALSE;
			}
            CloseServiceHandle(schService);
        }
        else
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSERVICE );
            odsMakeError( pstError, ODS_ERR_OPENSERVICE, NULL );
			bRet = FALSE;
		}

        CloseServiceHandle(schSCManager);
    }
    else
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSCM );
		odsMakeError( pstError, ODS_ERR_OPENSCM, NULL );
		bRet = FALSE;
	}
	
	return bRet;
}

//----------------------------------------------------------------
// odsStartService					- 啟動服務
//
// 參數:
//		lpszComputerName			- 服務程序所在的計算機名稱，如果為 NULL
//									- 則指本地計算機
//		lpszServiceName				- 要啟動服務的名稱
//		dwWaitTime					- 等待服務啟動的時間，以(毫秒)為單位
//		pstError					- 錯誤信息結構指針，可以為 NULL
//
// 返回:
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsStartService(LPCTSTR strComputerName, LPCTSTR strServiceName, 
				   DWORD dwWaitTime, STODSERROR* pstError)
{
	BOOL			bRet = TRUE;
	BOOL			bStarted = FALSE;
    SC_HANDLE		schService;
    SC_HANDLE		schSCManager;
	SERVICE_STATUS	ssStatus;
	DWORD			dwSleepLeft = dwWaitTime;
	DWORD			dwSleep = min( 1000, dwSleepLeft );

    schSCManager = OpenSCManager(
                        strComputerName,        // machine (NULL == local)
                        NULL,                   // database (NULL == default)
                        SC_MANAGER_ALL_ACCESS   // access required
                        );
    if ( schSCManager )
    {
        schService = OpenService( schSCManager, strServiceName, SERVICE_ALL_ACCESS );

        if (schService)
        {
            // try to stop the service
            if (  StartService( schService, 0, NULL ) )
            {
                do
				{
					Sleep( dwSleep );
					dwSleepLeft -= dwSleep;
					dwSleep = min( 1000, dwSleepLeft );

					QueryServiceStatus( schService, &ssStatus );
                
                    if ( ssStatus.dwCurrentState == SERVICE_RUNNING )
					{
						bStarted = TRUE;
						break;
					}
                }while( dwSleepLeft > 0 );
            }
			if ( !bStarted )
			{
				odsMakeError( pstError, ODS_ERR_STARTSERVICE, ODS_MSG_STARTSERVICE );
				bRet = FALSE;
			} 
            CloseServiceHandle(schService);
        }
        else
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSERVICE );
            odsMakeError( pstError, ODS_ERR_OPENSERVICE, NULL );
			bRet = FALSE;
		}

        CloseServiceHandle(schSCManager);
    }
    else
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSCM );
		odsMakeError( pstError, ODS_ERR_OPENSCM, NULL );
		bRet = FALSE;
	}

	return bRet;
}

//----------------------------------------------------------------
// odsStopService					- 停止服務
//
// 參數:
//		lpszComputerName			- 服務程序所在的計算機名稱，如果為 NULL
//									- 則指本地計算機
//		lpszServiceName				- 要啟動服務的名稱
//		dwWaitTime					- 等待服務啟動的時間，以(毫秒)為單位
//		pstError					- 錯誤信息結構指針，可以為 NULL
//
// 返回:
//		bSuccess					- 成功與否
//----------------------------------------------------------------
BOOL odsStopService( LPCTSTR strComputerName, LPCTSTR strServiceName, 
					DWORD dwWaitTime, STODSERROR* pstError )
{
	BOOL			bRet = TRUE;
	BOOL			bStopped = FALSE;
    SC_HANDLE		schService;
    SC_HANDLE		schSCManager;
	SERVICE_STATUS	ssStatus;
	DWORD			dwSleepLeft = dwWaitTime;
	DWORD			dwSleep = min( 1000, dwSleepLeft );

    schSCManager = OpenSCManager(
                        strComputerName,        // machine (NULL == local)
                        NULL,                   // database (NULL == default)
                        SC_MANAGER_ALL_ACCESS   // access required
                        );
    if ( schSCManager )
    {
        schService = OpenService( schSCManager, strServiceName, SERVICE_ALL_ACCESS );

        if (schService)
        {
            // try to stop the service
            if ( ControlService( schService, SERVICE_CONTROL_STOP, &ssStatus ) )
            {
                do
				{
					Sleep( dwSleep );
					dwSleepLeft -= dwSleep;
					dwSleep = min( 1000, dwSleepLeft );

					QueryServiceStatus( schService, &ssStatus );
                
                    if ( ssStatus.dwCurrentState == SERVICE_STOPPED )
					{
						bStopped = TRUE;
						break;
					}
                }while( dwSleepLeft > 0 );                
            }
			if ( !bStopped )
			{
				odsMakeError( pstError, ODS_ERR_STOPSERVICE, ODS_MSG_STOPSERVICE );
				bRet = FALSE;
			} 
            CloseServiceHandle(schService);
        }
        else
		{
			odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSERVICE );
            odsMakeError( pstError, ODS_ERR_OPENSERVICE, NULL );
			bRet = FALSE;
		}

        CloseServiceHandle(schSCManager);
    }
    else
	{
		odsFormatError( pstError, FORMAT_WITH_SYSTEM_AND_STRING, ODS_MSG_OPENSCM );
		odsMakeError( pstError, ODS_ERR_OPENSCM, NULL );
		bRet = FALSE;
	}

	return bRet;
}