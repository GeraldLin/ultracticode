//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procflw.h"
#include "extern.h"
#include "Request.h"
#include "GenericHTTPClient.h"
//---------------------------------------------------------------------------
extern _FagentCallWebService	*_agentCallWebService;
extern _FagentFreeMemory		  *_agentFreeMemory;

extern "C" int __declspec(dllexport) GetXmlAttrValueFromXMLString(const char *pszXMLString, const char *pszNodeRoot, int nNodeIndex, const char *pszAttrName, int &nNodeCount, char *pszAttrValue, int nMaxAttrBytes, char *pszError);
extern "C" int __declspec(dllexport) GetXmlAttrValueFromXMLFile(const char *pszXMLFile, const char *pszNodeRoot, int nNodeIndex, const char *pszAttrName, int &nNodeCount, char *pszAttrValue, int nMaxAttrBytes, char *pszError);

extern void ProcQueryCostomerResult(const char *pXMLResult, const char *pszCallerNo);

//發送消息到坐席
void SendMsg2FLW(US ClientId, US MsgId, const CH *msg)
{
  pTcpLink->SendMessage2CLIENT(ClientId, (MSGTYPE_DBXML<<8) | MsgId, msg);
  MyTrace(2, "%s", msg);
}
void SendMsg2FLW(US ClientId, US MsgId, const CStringX &msg)
{
  SendMsg2FLW(ClientId, MsgId, msg.C_Str());
}
//設置發送到AG的消息頭
void Set_WEB2FLW_Header(int MsgId, long SessionId, long CmdAddr)
{
  WEBSndMsg.GetBuf().Format("<%s sessionid='%ld' cmdaddr='%ld' chantype='0' channo='0'", 
    pFlwRule->DBXMLMsgRule[MsgId].MsgNameEng, SessionId, CmdAddr);
}
void Set_WEB2FLW_Header(int MsgId, long SessionId, long CmdAddr, short ChanType, short ChanNo)
{
  WEBSndMsg.GetBuf().Format("<%s sessionid='%ld' cmdaddr='%ld' chantype='%d' channo='%d'", 
    pFlwRule->DBXMLMsgRule[MsgId].MsgNameEng, SessionId, CmdAddr, ChanType, ChanNo);
}
//增加發送到AG的消息屬性
void Set_WEB2FLW_StrItem(int MsgId, int AttrId, const char *value)
{
  WEBSndMsg.AddItemToBuf(pFlwRule->DBXMLMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_WEB2FLW_StrItem(int MsgId, int AttrId, const CStringX &value)
{
  WEBSndMsg.AddItemToBuf(pFlwRule->DBXMLMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_WEB2FLW_IntItem(int MsgId, int AttrId, int value)
{
  WEBSndMsg.AddIntItemToBuf(pFlwRule->DBXMLMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_WEB2FLW_LongItem(int MsgId, int AttrId, long value)
{
  WEBSndMsg.AddLongItemToBuf(pFlwRule->DBXMLMsgRule[MsgId].Attribute[AttrId].AttrNameEng, value);
}
void Set_WEB2FLW_BoolItem(int MsgId, int AttrId, bool value)
{
  if (value == true)
  {
    WEBSndMsg.AddIntItemToBuf(pFlwRule->DBXMLMsgRule[MsgId].Attribute[AttrId].AttrNameEng, 1);
  }
  else
  {
    WEBSndMsg.AddIntItemToBuf(pFlwRule->DBXMLMsgRule[MsgId].Attribute[AttrId].AttrNameEng, 0);
  }
}
void Set_WEB2FLW_Tail()
{
  WEBSndMsg.AddTailToBuf();
}

void Send_querywebservice_Result(unsigned short ClientId, unsigned long  SessionId, 
                                 unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                                 const char* pszXMLString, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_onquerywebservice, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_onquerywebservice, 4, pszXMLString);
  Set_WEB2FLW_IntItem(MSG_onquerywebservice, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_onquerywebservice, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_onquerywebservice, WEBSndMsg.GetBuf());
}
void Send_querywebservice_Result(CQWebServiceSession *pWebServiceSession, const char* pszXMLString, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_onquerywebservice, pWebServiceSession->m_ulSessionId, pWebServiceSession->m_ulCmdAddr);
  Set_WEB2FLW_StrItem(MSG_onquerywebservice, 4, pszXMLString);
  Set_WEB2FLW_IntItem(MSG_onquerywebservice, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_onquerywebservice, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(pWebServiceSession->m_usClientId, MSG_onquerywebservice, WEBSndMsg.GetBuf());
}

void Send_getwebservicexml_Result(unsigned short ClientId, unsigned long  SessionId, 
                                  unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                                  const char* pszAttrValue, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebservicexml, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicexml, 4, pszAttrValue);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicexml, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicexml, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_ongetwebservicexml, WEBSndMsg.GetBuf());
}
void Send_getwebservicexml_Result(CQWebServiceSession *pWebServiceSession, const char* pszAttrValue, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebservicexml, pWebServiceSession->m_ulSessionId, pWebServiceSession->m_ulCmdAddr);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicexml, 4, pszAttrValue);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicexml, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicexml, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(pWebServiceSession->m_usClientId, MSG_ongetwebservicexml, WEBSndMsg.GetBuf());
}

void Send_getwebservicestr_Result(unsigned short ClientId, unsigned long  SessionId, 
                                 unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                                 const char* pszAttrValue, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebservicestr, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicestr, 4, pszAttrValue);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicestr, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicestr, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_ongetwebservicestr, WEBSndMsg.GetBuf());
}
void Send_getwebservicestr_Result(CQWebServiceSession *pWebServiceSession, const char* pszAttrValue, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebservicestr, pWebServiceSession->m_ulSessionId, pWebServiceSession->m_ulCmdAddr);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicestr, 4, pszAttrValue);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicestr, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicestr, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(pWebServiceSession->m_usClientId, MSG_ongetwebservicestr, WEBSndMsg.GetBuf());
}

void Send_getwebserviceindex_Result(unsigned short ClientId, unsigned long  SessionId, 
                                  unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                                  int nIndex, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebserviceindex, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_IntItem(MSG_ongetwebserviceindex, 4, nIndex);
  Set_WEB2FLW_IntItem(MSG_ongetwebserviceindex, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebserviceindex, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_ongetwebserviceindex, WEBSndMsg.GetBuf());
}
void Send_getwebserviceindex_Result(CQWebServiceSession *pWebServiceSession, int nIndex, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebserviceindex, pWebServiceSession->m_ulSessionId, pWebServiceSession->m_ulCmdAddr);
  Set_WEB2FLW_IntItem(MSG_ongetwebserviceindex, 4, nIndex);
  Set_WEB2FLW_IntItem(MSG_ongetwebserviceindex, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebserviceindex, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(pWebServiceSession->m_usClientId, MSG_ongetwebserviceindex, WEBSndMsg.GetBuf());
}
void Send_httprequest_Result(unsigned short ClientId, unsigned long  SessionId, 
                                 unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                                 const char* pszXMLString, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_onhttprequest, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_onhttprequest, 4, pszXMLString);
  Set_WEB2FLW_IntItem(MSG_onhttprequest, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_onhttprequest, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_onhttprequest, WEBSndMsg.GetBuf());
}
void Send_httprequestex_Result(unsigned short ClientId, unsigned long  SessionId, 
                             unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                             const char* pszXMLString, const char* pszXMLCookies, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_onhttprequestex, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_onhttprequestex, 4, pszXMLString);
  Set_WEB2FLW_StrItem(MSG_onhttprequestex, 5, pszXMLCookies);
  Set_WEB2FLW_IntItem(MSG_onhttprequestex, 6, nResult);
  Set_WEB2FLW_StrItem(MSG_onhttprequestex, 7, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_onhttprequestex, WEBSndMsg.GetBuf());
}
void Send_shellexecute_Result(unsigned short ClientId, unsigned long  SessionId, 
                             unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                             const char* pszXMLString, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_onshellexecute, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_onshellexecute, 4, pszXMLString);
  Set_WEB2FLW_IntItem(MSG_onshellexecute, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_onshellexecute, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_onshellexecute, WEBSndMsg.GetBuf());
}
void Send_gettxtlinestr_Result(unsigned short ClientId, unsigned long  SessionId, 
                              unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                              const char* pszXMLString, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongettxtlinestr, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_StrItem(MSG_ongettxtlinestr, 4, pszXMLString);
  Set_WEB2FLW_IntItem(MSG_ongettxtlinestr, 5, nResult);
  Set_WEB2FLW_StrItem(MSG_ongettxtlinestr, 6, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_ongettxtlinestr, WEBSndMsg.GetBuf());
}
void Send_getwebservicejson_Result(unsigned short ClientId, unsigned long  SessionId, 
                                  unsigned long  CmdAddr, unsigned short ChnType, unsigned short ChnNo, 
                                  const char* pszAttrValue, int nRecords, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebservicejson, SessionId, CmdAddr, ChnType, ChnNo);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicejson, 4, nRecords);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicejson, 5, pszAttrValue);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicejson, 6, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicejson, 7, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(ClientId, MSG_ongetwebservicejson, WEBSndMsg.GetBuf());
}
void Send_getwebservicejson_Result(CQWebServiceSession *pWebServiceSession, const char* pszAttrValue, int nRecords, int nResult, const char* pszErrorBuf)
{
  Set_WEB2FLW_Header(MSG_ongetwebservicejson, pWebServiceSession->m_ulSessionId, pWebServiceSession->m_ulCmdAddr);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicejson, 4, nRecords);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicejson, 5, pszAttrValue);
  Set_WEB2FLW_IntItem(MSG_ongetwebservicejson, 6, nResult);
  Set_WEB2FLW_StrItem(MSG_ongetwebservicejson, 7, pszErrorBuf);
  Set_WEB2FLW_Tail();
  SendMsg2FLW(pWebServiceSession->m_usClientId, MSG_ongetwebservicejson, WEBSndMsg.GetBuf());
}
//-----------------------------------------------------------------------------
int Proc_Msg_From_FLW(CXMLMsg &FLWMsg)
{
  unsigned short MsgId=FLWMsg.GetMsgId();
  
  if(MsgId >= MAX_XMLDBMSG_NUM)	//ag-->ivr
  {
    //指令編號超出范圍
    MyTrace(0, "FLWmsgid = %d is out of range\n", MsgId);
    return 1;
  }
  
  if(0!=FLWMsg.ParseSndMsg(pFlwRule->XMLDBMsgRule[MsgId]))
  {
    //接收消息錯誤
    MyTrace(0, "parse FLW msg error: MsgId = %d MsgBuf = %s\n", MsgId, FLWMsg.GetBuf().C_Str());
    return 2;
  }
  return 0;
}
//-----------------------------------------------------------------------------
int Proc_FLWMSG_gwquery(CXMLMsg &FLWMsg)
{
  return 0;
}
int Proc_FLWMSG_querywebservice(CXMLMsg &FLWMsg)
{
  CQWebServiceSession *pWebServiceSession;
  char szXMLString[1024], szRetXMLString1[16384], szRetXMLString2[16384];
  char *pReturn	= NULL;
  char *pError	= NULL;

  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());

  short SessionBufId = g_pWebServiceSessionMng->GetIdelSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "Query buf over!");
    return 1;
  }

  pWebServiceSession = &g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId];

  pWebServiceSession->m_ulSessionId = SessionId;
  pWebServiceSession->m_usClientId = ClientId;
  pWebServiceSession->m_ulCmdAddr = CmdAddr;
  pWebServiceSession->m_usDeviceNo = DeviceNo;
  pWebServiceSession->m_usChnType = ChnType;
  pWebServiceSession->m_usChnNo = ChnNo;

  pWebServiceSession->m_WebServiceData.m_strWSDLAddress = FLWMsg.GetAttrValue(4).C_Str();
  pWebServiceSession->m_WebServiceData.m_strSoapAction = FLWMsg.GetAttrValue(5).C_Str();
  pWebServiceSession->m_WebServiceData.m_strStartEnvelopeString = FLWMsg.GetAttrValue(6).C_Str();
  pWebServiceSession->m_WebServiceData.m_strStartBody = FLWMsg.GetAttrValue(7).C_Str();
  pWebServiceSession->m_WebServiceData.m_strInterfaceString = FLWMsg.GetAttrValue(8).C_Str();
  pWebServiceSession->m_WebServiceData.m_nParamCount = atoi(FLWMsg.GetAttrValue(9).C_Str());
  
  memset(szXMLString, 0, 1024);
  int j=0;
  int len = strlen(FLWMsg.GetAttrValue(10).C_Str());
  for (int i=0; i<len; i++)
  {
    if (FLWMsg.GetAttrValue(10)[i] != '\\')
    {
      if (FLWMsg.GetAttrValue(10)[i] == '`')
        szXMLString[j] = '\'';
      else
        szXMLString[j] = FLWMsg.GetAttrValue(10)[i];
      j++;
    }
  }
  pWebServiceSession->m_WebServiceData.m_strParamString = szXMLString;
  
  pWebServiceSession->m_WebServiceData.m_strReturnXmlString = _T("");
  pWebServiceSession->m_WebServiceData.m_strErrorMsg = _T("");
  pWebServiceSession->m_WebServiceData.m_nCurIndex = 0;

  pWebServiceSession->m_WebServiceData.m_nQueryType = 1;
  pWebServiceSession->m_WebServiceData.m_nQueryResult = 1;
  pWebServiceSession->m_WebServiceData.m_bSendQueryResult = false;

  pWebServiceSession->timer = GetTickCount();
  pWebServiceSession->state = 1;

  if (g_bTestId == true)
  {
    char szTestData[256], szSection[128];

    int nPos = pWebServiceSession->m_WebServiceData.m_strInterfaceString.Find('|');
    memset(szSection, 0, 128);
    strncpy(szSection, FLWMsg.GetAttrValue(8).C_Str(), nPos);

    MyTrace(3, "SessionId=%ld Read %s TestData", SessionId, szSection);
    GetPrivateProfileString(szSection, "RESULT", "OK", szTestData, 250, g_szServiceINIFileName);

    if (strcmp(szTestData, "OK") == 0)
    {
      GetPrivateProfileString(szSection, "RETURN", "", szTestData, 250, g_szServiceINIFileName);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTestData);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBSuccess, "");
    }
    else
    {
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString = "";
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "");
    }
    return 0;
  }
  if (g_nRunMode == 0)
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (_agentCallWebService((char *)pWebServiceSession->m_WebServiceData.m_strWSDLAddress.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strSoapAction.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strStartEnvelopeString.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strStartBody.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strInterfaceString.operator LPCTSTR(),
      pWebServiceSession->m_WebServiceData.m_nParamCount,
      (char *)pWebServiceSession->m_WebServiceData.m_strParamString.operator LPCTSTR(),
      &pReturn,
      &pError
      ) == 1)
    {
      MyTrace(3, "SessionId=%ld _agentCallWebService ok", SessionId);

      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBSuccess, "");
      
      MyStringReplace(pReturn, "&lt;", "<", 0, 65535, 16384, szRetXMLString1);
      MyStringReplace(szRetXMLString1, "&gt;", ">", 0, 65535, 16384, szRetXMLString2);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szRetXMLString2);
      
      MyWriteFile((LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString);
    }
    else
    {
      MyTrace(3, "SessionId=%ld _agentCallWebService fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strErrorMsg);
    }
    _agentFreeMemory(pReturn, pError);
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (pWebServiceSession->StartWebThreadProc() != 0)
    {
      MyTrace(3, "SessionId=%ld StartWebThreadProc fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strErrorMsg);
    }
  }
  
  return 0;
}
int Proc_FLWMSG_getwebservicexml(CXMLMsg &FLWMsg)
{
  return 0;
}
int Proc_FLWMSG_getwebservicestr(CXMLMsg &FLWMsg)
{
  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());
  CStringX ArrString1[64], ArrString2[64];
  int num1, num2;
  char szAttrValue[MAX_VAR_DATA_LEN], szTemp[256];
  bool bFinded;
  
  short SessionBufId = g_pWebServiceSessionMng->GetSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_getwebservicestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "have not Query before!");
    return 1;
  }

  g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Lock();
  if (g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nQueryResult != 4)
  {
    Send_getwebservicestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "have not query success before!");
    g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Unlock();
    return 1;
  }

  memset(szAttrValue, 0, MAX_VAR_DATA_LEN);
  num1 = SplitString(FLWMsg.GetAttrValue(4).C_Str(), '`', 64, ArrString1);
  num2 = SplitString(FLWMsg.GetAttrValue(5).C_Str(), '`', 64, ArrString2);
  if (num1 == 1)
  {
    MyGetMarkerString((LPCSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strReturnXmlString,
      FLWMsg.GetAttrValue(4).C_Str(),
      FLWMsg.GetAttrValue(5).C_Str(),
      atoi(FLWMsg.GetAttrValue(6).C_Str()),
      MAX_VAR_DATA_LEN,
      szAttrValue,
      bFinded);
  }
  else
  {
    //2014-01-07
    for (int i=0; i<num1; i++)
    {
      //MyTrace(3, "ArrString1[i]=%s ArrString2[i]=%s", ArrString1[i].C_Str(), ArrString2[i].C_Str());
      memset(szTemp, 0, 256);
      MyGetMarkerString((LPCSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strReturnXmlString,
        ArrString1[i].C_Str(),
        ArrString2[i].C_Str(),
        0,
        256,
        szTemp,
        bFinded);
      if (i > 0)
        strcat(szAttrValue, "`");
      strcat(szAttrValue, szTemp);
      if (strlen(szAttrValue) > (MAX_VAR_DATA_LEN-257))
        break;
    }
    bFinded = true;
  }
  g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Unlock();
  if (bFinded == true)
  {
    MyTrace(3, "SessionId=%ld getwebservicestr success Value=%s", SessionId, szAttrValue);
    Send_getwebservicestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, szAttrValue, OnDBSuccess, "");
  }
  else
  {
    MyTrace(3, "SessionId=%ld getwebservicestr fail", SessionId);
    Send_getwebservicestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "");
  }
  
  return 0;
}
int Proc_FLWMSG_closewebservice(CXMLMsg &FLWMsg)
{
  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  short SessionBufId = g_pWebServiceSessionMng->GetSession(SessionId);
  if (SessionBufId >= 0)
  {
    g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Lock();

    if (g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nQueryResult != 2)
    {
      g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].Init();
      g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nTestIndex = 0;
    }
    else
    {
      g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nQueryResult = 5;
    }

    g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Unlock();
  }
  return 0;
}
int Proc_FLWMSG_getwebserviceindex(CXMLMsg &FLWMsg)
{
  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());
  
  char szAttrValue[MAX_VAR_DATA_LEN];
  int nIndex;
  
  short SessionBufId = g_pWebServiceSessionMng->GetSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_getwebserviceindex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, 0, OnDBFail, "have not Query before!");
    return 1;
  }
  
  if (g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nQueryResult != 4)
  {
    Send_getwebserviceindex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, 0, OnDBFail, "have not query success before!");
    return 1;
  }
  memset(szAttrValue, 0, MAX_VAR_DATA_LEN);
  nIndex = MyGetMarkerStringIndex((LPCSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strReturnXmlString,
    FLWMsg.GetAttrValue(4).C_Str(),
    FLWMsg.GetAttrValue(5).C_Str(),
    FLWMsg.GetAttrValue(6).C_Str()); 
  if (nIndex >= 0)
  {
    MyTrace(3, "SessionId=%ld getwebserviceindex success Index=%d", SessionId, nIndex);
    Send_getwebserviceindex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, nIndex, OnDBSuccess, "");
  }
  else
  {
    MyTrace(3, "SessionId=%ld getwebserviceindex fail", SessionId);
    Send_getwebserviceindex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, 0, OnDBFail, "");
  }
  
  return 0;
}
int Proc_FLWMSG_httprequest(CXMLMsg &FLWMsg)
{
  CQWebServiceSession *pWebServiceSession;

  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());

  short SessionBufId = g_pWebServiceSessionMng->GetIdelSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "Query buf over!");
    return 1;
  }

  pWebServiceSession = &g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId];

  pWebServiceSession->m_ulSessionId = SessionId;
  pWebServiceSession->m_usClientId = ClientId;
  pWebServiceSession->m_ulCmdAddr = CmdAddr;
  pWebServiceSession->m_usDeviceNo = DeviceNo;
  pWebServiceSession->m_usChnType = ChnType;
  pWebServiceSession->m_usChnNo = ChnNo;
  
  pWebServiceSession->m_WebServiceData.m_strReturnXmlString = _T("");
  pWebServiceSession->m_WebServiceData.m_strReturnCookiesString = _T("");
  pWebServiceSession->m_WebServiceData.m_strErrorMsg = _T("");
  pWebServiceSession->m_WebServiceData.m_nCurIndex = 0;

  pWebServiceSession->m_WebServiceData.m_nQueryType = 2;
  pWebServiceSession->m_WebServiceData.m_nQueryResult = 1;
  pWebServiceSession->timer = GetTickCount();
  pWebServiceSession->state = 1;

  Request	myRequest;
  CString strResult, strHeaderSend;
  
  pWebServiceSession->m_WebServiceData.m_strHTTPurl = FLWMsg.GetAttrValue(4).C_Str();
  pWebServiceSession->m_WebServiceData.m_strHTTPheader = FLWMsg.GetAttrValue(5).C_Str();
  pWebServiceSession->m_WebServiceData.m_strHTTPcookies = "";
  if (stricmp(FLWMsg.GetAttrValue(6).C_Str(), "GET") == 0)
  {
    pWebServiceSession->m_WebServiceData.m_nHTTPType = 1;
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nHTTPType = 2;
  }
  
  if (g_bTestId == true)
  {
    char szTestData[256], szSection[128];
    
    strcpy(szSection, "HTTPTEST");
    
    MyTrace(3, "SessionId=%ld Read %s TestData", SessionId, szSection);
    GetPrivateProfileString(szSection, "RESULT", "OK", szTestData, 250, g_szServiceINIFileName);
    
    if (strcmp(szTestData, "OK") == 0)
    {
      GetPrivateProfileString(szSection, "RETURN", "", szTestData, 250, g_szServiceINIFileName);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTestData);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, szTestData, OnDBSuccess, "");
    }
    else
    {
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString = "";
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "");
    }
    return 0;
  }

  strResult = "FAIL";
  if (g_nRunMode == 0)
  {
    if (myRequest.SendRequest(true, 
      (LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPurl, 
      (LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPcookies,
      pWebServiceSession->m_WebServiceData.m_strHTTPheader, 
      pWebServiceSession->m_WebServiceData.m_strErrorMsg, 
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString,
      pWebServiceSession->m_WebServiceData.m_strReturnCookiesString) == 0)
    {
      if (pWebServiceSession->m_WebServiceData.m_strErrorMsg.Find("HTTP/1.1 200 OK") >= 0)
      {
        strResult = "OK";
      }
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString, OnDBSuccess, "");
      return 0;
    }
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
    pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
    Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)strResult, OnDBFail, "");
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (pWebServiceSession->StartHTTPThreadProc() != 0)
    {
      MyTrace(3, "SessionId=%ld StartHTTPThreadProc fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)strResult, OnDBFail, "");
    }
  }

  return 0;
}
int Proc_FLWMSG_httprequestex(CXMLMsg &FLWMsg)
{
  CQWebServiceSession *pWebServiceSession;

  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());

  MyTrace(3, "SessionId=%ld Proc_FLWMSG_httprequestex", SessionId);

  short SessionBufId = g_pWebServiceSessionMng->GetIdelSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_httprequest_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "Query buf over!");
    return 1;
  }

  pWebServiceSession = &g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId];

  pWebServiceSession->m_ulSessionId = SessionId;
  pWebServiceSession->m_usClientId = ClientId;
  pWebServiceSession->m_ulCmdAddr = CmdAddr;
  pWebServiceSession->m_usDeviceNo = DeviceNo;
  pWebServiceSession->m_usChnType = ChnType;
  pWebServiceSession->m_usChnNo = ChnNo;
  
  pWebServiceSession->m_WebServiceData.m_strReturnXmlString = _T("");
  pWebServiceSession->m_WebServiceData.m_strReturnCookiesString = _T("");
  pWebServiceSession->m_WebServiceData.m_strErrorMsg = _T("");
  pWebServiceSession->m_WebServiceData.m_nCurIndex = 0;

  pWebServiceSession->m_WebServiceData.m_nQueryType = 5;
  pWebServiceSession->m_WebServiceData.m_nQueryResult = 1;
  pWebServiceSession->timer = GetTickCount();
  pWebServiceSession->state = 1;

  Request	myRequest;
  CString strResult, strHeaderSend;
  
  pWebServiceSession->m_WebServiceData.m_strHTTPurl = FLWMsg.GetAttrValue(4).C_Str();
  pWebServiceSession->m_WebServiceData.m_strHTTPheader = FLWMsg.GetAttrValue(5).C_Str();
  pWebServiceSession->m_WebServiceData.m_strHTTPcookies = FLWMsg.GetAttrValue(6).C_Str();
  if (stricmp(FLWMsg.GetAttrValue(7).C_Str(), "GET") == 0)
  {
    pWebServiceSession->m_WebServiceData.m_nHTTPType = 1;
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nHTTPType = 2;
  }
  
  if (g_bTestId == true)
  {
    char szTestData[256], szTestCookies[256], szSection[128];
    
    strcpy(szSection, "HTTPTEST");
    
    MyTrace(3, "SessionId=%ld Read %s TestData", SessionId, szSection);
    GetPrivateProfileString(szSection, "RESULT", "OK", szTestData, 250, g_szServiceINIFileName);
    
    if (strcmp(szTestData, "OK") == 0)
    {
      GetPrivateProfileString(szSection, "RETURN", "", szTestData, 250, g_szServiceINIFileName);
      GetPrivateProfileString(szSection, "COOKIES", "", szTestCookies, 250, g_szServiceINIFileName);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTestData);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_httprequestex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, szTestData, szTestCookies, OnDBSuccess, "");
    }
    else
    {
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString = "";
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_httprequestex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", "", OnDBFail, "");
    }
    return 0;
  }

  strResult = "FAIL";
  if (g_nRunMode == 0)
  {
    if (myRequest.SendRequest(true, 
      (LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPurl, 
      (LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPcookies,
      pWebServiceSession->m_WebServiceData.m_strHTTPheader, 
      pWebServiceSession->m_WebServiceData.m_strErrorMsg, 
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString,
      pWebServiceSession->m_WebServiceData.m_strReturnCookiesString) == 0)
    {
      if (pWebServiceSession->m_WebServiceData.m_strErrorMsg.Find("HTTP/1.1 200 OK") >= 0)
      {
        strResult = "OK";
      }
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      Send_httprequestex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString, 
        (LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnCookiesString, OnDBSuccess, "");
      return 0;
    }
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
    pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
    Send_httprequestex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)strResult, "", OnDBFail, "");
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (pWebServiceSession->StartHTTPThreadProc() != 0)
    {
      MyTrace(3, "SessionId=%ld StartHTTPThreadProc fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_httprequestex_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)strResult, "", OnDBFail, "");
    }
  }

  return 0;
}
int Proc_FLWMSG_shellexecute(CXMLMsg &FLWMsg)
{
  CQWebServiceSession *pWebServiceSession;
  
  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());
  
  short SessionBufId = g_pWebServiceSessionMng->GetIdelSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_shellexecute_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "Query buf over!");
    return 1;
  }
  
  pWebServiceSession = &g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId];
  
  pWebServiceSession->m_ulSessionId = SessionId;
  pWebServiceSession->m_usClientId = ClientId;
  pWebServiceSession->m_ulCmdAddr = CmdAddr;
  pWebServiceSession->m_usDeviceNo = DeviceNo;
  pWebServiceSession->m_usChnType = ChnType;
  pWebServiceSession->m_usChnNo = ChnNo;
  
  pWebServiceSession->m_WebServiceData.m_strReturnXmlString = _T("");
  pWebServiceSession->m_WebServiceData.m_strErrorMsg = _T("");
  
  pWebServiceSession->m_WebServiceData.m_nQueryType = 3;
  pWebServiceSession->m_WebServiceData.m_nQueryResult = 1;
  pWebServiceSession->timer = GetTickCount();
  pWebServiceSession->state = 1;
  
  Request	myRequest;
  CString strResult, strHeaderSend;
  
  pWebServiceSession->m_WebServiceData.m_strEXECCMDName = FLWMsg.GetAttrValue(4).C_Str();
  pWebServiceSession->m_WebServiceData.m_strEXECCMDParam = FLWMsg.GetAttrValue(5).C_Str();
  pWebServiceSession->m_WebServiceData.m_strEXECReadFile = FLWMsg.GetAttrValue(6).C_Str();
  pWebServiceSession->m_WebServiceData.m_nEXECTimeOut = 10000; //atoi(FLWMsg.GetAttrValue(7).C_Str())*1000;

  if (DeleteFile(pWebServiceSession->m_WebServiceData.m_strEXECReadFile) == TRUE)
    MyTrace(3, "DeleteFile %s success", pWebServiceSession->m_WebServiceData.m_strEXECReadFile);

  if (g_bTestId == true)
  {
    char szTestData[256], szSection[128];
    
    strcpy(szSection, "EXECTEST");
    
    MyTrace(3, "SessionId=%ld Read %s TestData", SessionId, szSection);
    GetPrivateProfileString(szSection, "RESULT", "OK", szTestData, 250, g_szServiceINIFileName);
    
    if (strcmp(szTestData, "OK") == 0)
    {
      GetPrivateProfileString(szSection, "RETURN", "", szTestData, 250, g_szServiceINIFileName);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTestData);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_shellexecute_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, szTestData, OnDBSuccess, "");
    }
    else
    {
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString = "";
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_shellexecute_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "");
    }
    return 0;
  }
  
  if (g_nRunMode == 0)
  {
    if ((DWORD)ShellExecute(NULL,"open",FLWMsg.GetAttrValue(4).C_Str(),FLWMsg.GetAttrValue(5).C_Str(),NULL,SW_SHOWNORMAL) < 32)
    {
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_shellexecute_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "");
    }
    else
    {
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 6; //延時等待ShellExecute結果
    }
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (pWebServiceSession->StartEXECThreadProc() != 0)
    {
      MyTrace(3, "SessionId=%ld StartEXECThreadProc fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_shellexecute_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, (LPCSTR)strResult, OnDBFail, "");
    }
  }
  
  return 0;
}
int Proc_FLWMSG_gettxtlinestr(CXMLMsg &FLWMsg)
{
  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());
  CStringX ArrString1[64];
  char cSeparator, szTxtFileName[256], szTxtLine[1024], szAttrValue[MAX_VAR_DATA_LEN];
  int l, len, num, nLineNo, nFieldNo, nStartPos, nLength;
  bool bFinded;
  
  if (strlen(FLWMsg.GetAttrValue(4).C_Str()) == 0)
  {
    short SessionBufId = g_pWebServiceSessionMng->GetSession(SessionId);
    if (SessionBufId < 0)
    {
      Send_gettxtlinestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "have not Query before!");
      return 1;
    }
    
    g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Lock();
    if (g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nQueryResult != 4)
    {
      Send_gettxtlinestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "have not query success before!");
      g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Unlock();
      return 1;
    }
    strcpy(szTxtFileName, (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strEXECReadFile);
  }
  else
  {
    strcpy(szTxtFileName, FLWMsg.GetAttrValue(4).C_Str());
  }
  FILE *fp;
  fp = fopen(szTxtFileName, "r");
  if (fp == NULL)
  {
    Send_gettxtlinestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "fopen txt file fail!");
    return 1;
  }
  MyTrace(3, "SessionId=%ld Proc_FLWMSG_gettxtlinestr fopen szTxtFileName=%s success", SessionId, szTxtFileName);
  
  nLineNo = atoi(FLWMsg.GetAttrValue(5).C_Str());
  nFieldNo = atoi(FLWMsg.GetAttrValue(7).C_Str());
  nStartPos = atoi(FLWMsg.GetAttrValue(8).C_Str());
  nLength = atoi(FLWMsg.GetAttrValue(9).C_Str());
  if (strlen(FLWMsg.GetAttrValue(6).C_Str()) > 0)
  {
    cSeparator = FLWMsg.GetAttrValue(6)[0];
  }
  else
  {
    cSeparator = '\0';
  }

  l = 0;
  bFinded=false;
  memset(szAttrValue, 0, MAX_VAR_DATA_LEN);

  while (!feof(fp))
  {
    if (fgets(szTxtLine, 1024, fp) > 0)
    {
      len = strlen(szTxtLine);
      if (len > 0)
        szTxtLine[len-1] = '\0';
      if (l == nLineNo)
      {
        if (cSeparator == '\0')
        {
          if (nFieldNo == 0)
          {
            strncpy(szAttrValue, szTxtLine, MAX_VAR_DATA_LEN-1);
            bFinded = true;
          }
        } 
        else
        {
          num = SplitString(szTxtLine, cSeparator, 64, ArrString1);
          if (num > nFieldNo)
          {
            strncpy(szAttrValue, ArrString1[nFieldNo].C_Str(), MAX_VAR_DATA_LEN-1);
            bFinded = true;
          }
        }
        break;
      }
      l++;
      if (l > 20)
        break;
    }
  }
  if (bFinded == false)
  {
    Send_gettxtlinestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "search str fail!");
  } 
  else
  {
    memset(szTxtLine, 0, MAX_VAR_DATA_LEN);
    l = strlen(szAttrValue);
    if (l > nStartPos)
    {
      if (nLength > 0)
        strncpy(szTxtLine, &szAttrValue[nStartPos], nLength);
      else
        strcpy(szTxtLine, &szAttrValue[nStartPos]);
    }
    Send_gettxtlinestr_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, szTxtLine, OnDBSuccess, "");
  }

  fclose(fp);
  MyTrace(3, "SessionId=%ld Proc_FLWMSG_gettxtlinestr fclose", SessionId);

  return 0;
}

int Proc_FLWMSG_querycustomer(CXMLMsg &FLWMsg)
{
  CQWebServiceSession *pWebServiceSession;
  char szXMLString[1024], szRetXMLString1[16384], szRetXMLString2[16384];
  char *pReturn	= NULL;
  char *pError	= NULL;

  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());

  short SessionBufId = g_pWebServiceSessionMng->GetIdelSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "Query buf over!");
    return 1;
  }

  pWebServiceSession = &g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId];

  pWebServiceSession->m_ulSessionId = SessionId;
  pWebServiceSession->m_usClientId = ClientId;
  pWebServiceSession->m_ulCmdAddr = CmdAddr;
  pWebServiceSession->m_usDeviceNo = DeviceNo;
  pWebServiceSession->m_usChnType = ChnType;
  pWebServiceSession->m_usChnNo = ChnNo;

  pWebServiceSession->m_WebServiceData.m_strWSDLAddress = g_WebServiceParam.WS_wsdladdress;
  pWebServiceSession->m_WebServiceData.m_strSoapAction = g_WebServiceParam.WS_soapaction;
  pWebServiceSession->m_WebServiceData.m_strStartEnvelopeString = g_WebServiceParam.WS_startenvelope;
  pWebServiceSession->m_WebServiceData.m_strStartBody = g_WebServiceParam.WS_startbody;
  pWebServiceSession->m_WebServiceData.m_strInterfaceString = g_WebServiceParam.WS_interface;
  pWebServiceSession->m_WebServiceData.m_nParamCount = g_WebServiceParam.WS_paramcount;
  
  MyTrace(1, "paramstring1=%s", g_WebServiceParam.WS_paramstring[0]);
  memset(szXMLString, 0, 1024);
  MyStringReplace(g_WebServiceParam.WS_paramstring[0], "$S_CallerNo", FLWMsg.GetAttrValue(4).C_Str(), 0, 1, 1024, szXMLString);
  pWebServiceSession->m_WebServiceData.m_strParamString = szXMLString;

  pWebServiceSession->m_WebServiceData.m_strCallerNo = FLWMsg.GetAttrValue(4).C_Str();

  MyTrace(1, "wsdladdress=%s", (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strWSDLAddress);
  MyTrace(1, "soapaction=%s", (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strSoapAction);
  MyTrace(1, "startenvelope=%s", (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strStartEnvelopeString);
  MyTrace(1, "startbody=%s", (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strStartBody);
  MyTrace(1, "interface=%s", (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strInterfaceString);
  MyTrace(1, "paramstring1=%s", (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strParamString);
  
  pWebServiceSession->m_WebServiceData.m_strReturnXmlString = _T("");
  pWebServiceSession->m_WebServiceData.m_strErrorMsg = _T("");

  pWebServiceSession->m_WebServiceData.m_nQueryType = 4; //4-查客戶資料
  pWebServiceSession->m_WebServiceData.m_nQueryResult = 1;
  pWebServiceSession->m_WebServiceData.m_bSendQueryResult = false;

  pWebServiceSession->timer = GetTickCount();
  pWebServiceSession->state = 1;

  if (g_bTestId == true)
  {
    char szTestData[256], szSection[128];

    int nPos = pWebServiceSession->m_WebServiceData.m_strInterfaceString.Find('|');
    memset(szSection, 0, 128);
    strncpy(szSection, FLWMsg.GetAttrValue(8).C_Str(), nPos);

    MyTrace(3, "SessionId=%ld Read %s TestData", SessionId, szSection);
    GetPrivateProfileString(szSection, "RESULT", "OK", szTestData, 250, g_szServiceINIFileName);

    if (strcmp(szTestData, "OK") == 0)
    {
      GetPrivateProfileString(szSection, "RETURN", "", szTestData, 250, g_szServiceINIFileName);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTestData);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBSuccess, "");
    }
    else
    {
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString = "";
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, "");
    }
    return 0;
  }
  if (g_nRunMode == 0)
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (_agentCallWebService((char *)pWebServiceSession->m_WebServiceData.m_strWSDLAddress.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strSoapAction.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strStartEnvelopeString.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strStartBody.operator LPCTSTR(),
      (char *)pWebServiceSession->m_WebServiceData.m_strInterfaceString.operator LPCTSTR(),
      pWebServiceSession->m_WebServiceData.m_nParamCount,
      (char *)pWebServiceSession->m_WebServiceData.m_strParamString.operator LPCTSTR(),
      &pReturn,
      &pError
      ) == 1)
    {
      MyTrace(3, "SessionId=%ld _agentCallWebService ok", SessionId);

      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBSuccess, "");
      
      MyStringReplace(pReturn, "&lt;", "<", 0, 65535, 16384, szRetXMLString1);
      MyStringReplace(szRetXMLString1, "&gt;", ">", 0, 65535, 16384, szRetXMLString2);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szRetXMLString2);
      
      MyWriteFile((LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString);
    }
    else
    {
      MyTrace(3, "SessionId=%ld _agentCallWebService fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strErrorMsg);
    }
    _agentFreeMemory(pReturn, pError);
  }
  else
  {
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 2;
    if (pWebServiceSession->StartWebThreadProc() != 0)
    {
      MyTrace(3, "SessionId=%ld StartWebThreadProc fail", SessionId);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
      pWebServiceSession->m_WebServiceData.m_bSendQueryResult = true;
      Send_querywebservice_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", OnDBFail, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strErrorMsg);
    }
  }
  
  return 0;
}
int Proc_FLWMSG_getwebservicejson(CXMLMsg &FLWMsg)
{
  unsigned long  SessionId = atol(FLWMsg.GetAttrValue(0).C_Str());
  unsigned short ClientId = FLWMsg.GetClientId();
  unsigned long  CmdAddr = atol(FLWMsg.GetAttrValue(1).C_Str());
  unsigned short DeviceNo = 1;
  unsigned short ChnType = atoi(FLWMsg.GetAttrValue(2).C_Str());
  unsigned short ChnNo = atoi(FLWMsg.GetAttrValue(3).C_Str());
  CStringX ArrString1[64], ArrString2[64];
  int i, j, k, num1, num2;
  char szAttrValue[MAX_VAR_DATA_LEN], szTemp[256];
  bool bFinded=false, bFilter=false;

  cJSON *pJSONroot, *pJSONTemp, *pJSONTemp2, *pJSONItem[64];
  char szArrayName[256];
  int nArrayIndex[64], nRecords=0;
  char szFilterFieldName[128], szFilterCondValue[128];
  int nFilterType=0; //篩選判斷類型：0-不判斷，1-等于，2-小于，3-大于，4-小于或等于，5-大于或等于 6-不等于
  int nFilterFlag[256];
  
  short SessionBufId = g_pWebServiceSessionMng->GetSession(SessionId);
  if (SessionBufId < 0)
  {
    Send_getwebservicejson_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", 0, OnDBFail, "have not Query before!");
    return 1;
  }
  
  g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Lock();
  if (g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nQueryResult != 4)
  {
    Send_getwebservicejson_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", 0, OnDBFail, "have not query success before!");
    g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Unlock();
    return 1;
  }
  
  memset(szAttrValue, 0, MAX_VAR_DATA_LEN);
  num1 = SplitString(FLWMsg.GetAttrValue(4).C_Str(), '.', 64, ArrString1);
  num2 = SplitString(FLWMsg.GetAttrValue(5).C_Str(), ',', 64, ArrString2);

  //解析判斷條件
  memset(szFilterFieldName, 0, 128);
  memset(szFilterCondValue, 0, 128);
  if (FLWMsg.GetAttrValue(6).GetLength() > 0)
  {
    if (ParserFilter(FLWMsg.GetAttrValue(6).C_Str(), szFilterFieldName, szFilterCondValue, nFilterType) == true)
    {
      MyTrace(3, "SessionId=%ld ParserFilter %s %s %d", SessionId, szFilterFieldName, szFilterCondValue, nFilterType);
    }
  }

  for (i=0; i<num1; i++)
  {
    memset(szArrayName, 0, 256);
    nArrayIndex[i] = 0;
    if (GetArrayNameAndIndex(ArrString1[i].C_Str(), szArrayName, nArrayIndex[i]))
      ArrString1[i] = szArrayName;
    MyTrace(3, "SessionId=%ld GetArrayNameAndIndex %s %d", SessionId, ArrString1[i].C_Str(), nArrayIndex[i]);
  }

  for (i=0; i<64; i++)
  {
    pJSONItem[i] = NULL;
  }

  for (i=0; i<256; i++)
  {
    nFilterFlag[i] = 0;
  }

  pJSONroot = cJSON_Parse((LPCSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strReturnXmlString);
  if (pJSONroot)
  {
    //MyTrace(3, "SessionId=%ld cJSON_Parse %s success", SessionId, (LPCSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strReturnXmlString);
    j = 0;
    for (i=0; i<num1 && i<64; i++)
    {
      if (ArrString1[i].GetLength() == 0)
      {
        break;
      }
      if (i == 0)
        pJSONItem[j] = cJSON_GetObjectItem(pJSONroot, ArrString1[i].C_Str());
      else
        pJSONItem[j] = cJSON_GetObjectItem(pJSONItem[j-1], ArrString1[i].C_Str());
      if (!pJSONItem[j])
      {
        break;
      }
      switch (pJSONItem[j]->type)
      {
      case cJSON_False:
        strcpy(szAttrValue, "0");
        nRecords = 1;
        bFinded = true;
        break;
      case cJSON_True:
        strcpy(szAttrValue, "1");
        nRecords = 1;
        bFinded = true;
        break;
      case cJSON_NULL:
        strcpy(szAttrValue, "");
        nRecords = 1;
        bFinded = true;
        break;
      case cJSON_Number:
        sprintf(szAttrValue, "%d", pJSONItem[j]->valueint);
        nRecords = 1;
        bFinded = true;
        break;
      case cJSON_String:
        strncpy(szAttrValue, pJSONItem[j]->valuestring, MAX_VAR_DATA_LEN-1);
        nRecords = 1;
        bFinded = true;
        break;
      case cJSON_Array: //數組記錄
        {
          int nReadIndex;
          int nArrayNum = cJSON_GetArraySize(pJSONItem[j]); //取JSON實際數組記錄數
          if (nArrayNum == 0)
          {
            break;
          }

          if (nFilterType > 0)
          {
            //有篩選條件
            for (k=0; k<nArrayNum; k++)
            {
              //按順序號取JSON記錄指針
              pJSONTemp = cJSON_GetArrayItem(pJSONItem[j], k);
              if (!pJSONTemp)
                continue;
              //取篩選字段JSON實體
              pJSONTemp2 = cJSON_GetObjectItem(pJSONTemp, szFilterFieldName);
              if (!pJSONTemp2)
                continue;

              bFilter = false;

              //篩選條件判斷
              memset(szTemp, 0, 256);
              switch (pJSONTemp2->type)
              {
              case cJSON_False:
                strcpy(szTemp, "0");
                break;
              case cJSON_True:
                strcpy(szTemp, "1");
                break;
              case cJSON_Number:
                sprintf(szTemp, "%d", pJSONTemp2->valueint);
                break;
              case cJSON_String:
                strncpy(szTemp, pJSONTemp2->valuestring, 255);
                break;
              }
              switch (nFilterType)
              {
              case 1: //等于
                if (strcmp(szTemp, szFilterCondValue) == 0)
                {
                  bFilter = true;
                }
                break;
              case 2: //小于
                if (strcmp(szTemp, szFilterCondValue) < 0)
                {
                  bFilter = true;
                }
                break;
              case 3: //大于
                if (strcmp(szTemp, szFilterCondValue) > 0)
                {
                  bFilter = true;
                }
                break;
              case 4: //小于或等于
                if (strcmp(szTemp, szFilterCondValue) <= 0)
                {
                  bFilter = true;
                }
                break;
              case 5: //大于或等于
                if (strcmp(szTemp, szFilterCondValue) >= 0)
                {
                  bFilter = true;
                }
                break;
              case 6: //不等于
                if (strcmp(szTemp, szFilterCondValue) != 0)
                {
                  bFilter = true;
                }
                break;
              }
              if (bFilter == true)
              {
                nFilterFlag[k] = 1;
                nRecords++;
              }
            }
            bool bTemp=false;
            nReadIndex = 0;
            //取篩選后的記錄
            for (k=0; k<nArrayNum; k++)
            {
              if (nFilterFlag[k] == 1)
              {
                if (nArrayIndex[i] >= 0)
                {
                  //指定記錄索引號
                  if (nReadIndex == nArrayIndex[i])
                  {
                    nReadIndex = k;
                    bTemp = true;
                    break;
                  }
                }
                else
                {
                  //自動讀下一條記錄索引號
                  if (k >= g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nCurIndex)
                  {
                    nReadIndex = k;
                    bTemp = true;
                    break;
                  }
                }
                nReadIndex++;
              }
            }
            if (bTemp == false)
            {
              bFinded = true;
              break;
            }
            
            pJSONTemp = cJSON_GetArrayItem(pJSONItem[j], nReadIndex);
            if (pJSONTemp)
            {
              //數組記錄指針+1
              g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nCurIndex = nReadIndex+1;
              
              for (k=0; k<num2; k++)
              {
                //取每個字段的值
                pJSONTemp2 = cJSON_GetObjectItem(pJSONTemp, ArrString2[k].C_Str());
                if (pJSONTemp2)
                {
                  memset(szTemp, 0, 256);
                  switch (pJSONTemp2->type)
                  {
                  case cJSON_False:
                    strcpy(szTemp, "0");
                    break;
                  case cJSON_True:
                    strcpy(szTemp, "1");
                    break;
                  case cJSON_Number:
                    sprintf(szTemp, "%d", pJSONTemp2->valueint);
                    break;
                  case cJSON_String:
                    strncpy(szTemp, pJSONTemp2->valuestring, 255);
                    break;
                  }
                  if (k > 0)
                    strcat(szAttrValue, "`");
                  strcat(szAttrValue, szTemp);
                  if (strlen(szAttrValue) > (MAX_VAR_DATA_LEN-257))
                    break;
                }
                else
                {
                  if (k > 0)
                    strcat(szAttrValue, "`");
                }
              }
              bFinded = true;
            }
          }
          else
          {
            //沒有篩選條件
            nRecords = nArrayNum;
            nReadIndex = nArrayIndex[i];
            if (nArrayIndex[i] >= 0)
              nReadIndex = nArrayIndex[i]; //指定記錄索引號
            else
              nReadIndex = g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nCurIndex; //自動讀下一條記錄索引號

            pJSONTemp = cJSON_GetArrayItem(pJSONItem[j], nReadIndex);
            if (pJSONTemp)
            {
              //數組記錄指針+1
              g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_nCurIndex = nReadIndex+1;
              
              for (k=0; k<num2; k++)
              {
                //取每個字段的值
                pJSONTemp2 = cJSON_GetObjectItem(pJSONTemp, ArrString2[k].C_Str());
                if (pJSONTemp2)
                {
                  memset(szTemp, 0, 256);
                  switch (pJSONTemp2->type)
                  {
                  case cJSON_False:
                    strcpy(szTemp, "0");
                    break;
                  case cJSON_True:
                    strcpy(szTemp, "1");
                    break;
                  case cJSON_Number:
                    sprintf(szTemp, "%d", pJSONTemp2->valueint);
                    break;
                  case cJSON_String:
                    strncpy(szTemp, pJSONTemp2->valuestring, 255);
                    break;
                  }
                  if (k > 0)
                    strcat(szAttrValue, "`");
                  strcat(szAttrValue, szTemp);
                  if (strlen(szAttrValue) > (MAX_VAR_DATA_LEN-257))
                    break;
                }
                else
                {
                  if (k > 0)
                    strcat(szAttrValue, "`");
                }
              }
              bFinded = true;
            }
          }

        }
        break;
      case cJSON_Object:
      	break;
      }
      if (bFinded == true)
      {
        break;
      }
      
      j++;
    }
    cJSON_Delete(pJSONroot);
  }

  g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_csQuery.Unlock();
  if (bFinded == true)
  {
    MyTrace(3, "SessionId=%ld getwebservicejson success Value=%s", SessionId, szAttrValue);
    Send_getwebservicejson_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, szAttrValue, nRecords, OnDBSuccess, "");
  }
  else
  {
    //MyTrace(3, "SessionId=%ld cJSON_Parse %s fail", SessionId, (LPCSTR)g_pWebServiceSessionMng->m_pWebServiceSession[SessionBufId].m_WebServiceData.m_strReturnXmlString);
    MyTrace(3, "SessionId=%ld cJSON_Parse fail", SessionId);
    Send_getwebservicejson_Result(ClientId, SessionId, CmdAddr, ChnType, ChnNo, "", 0, OnDBFail, "");
  }
  
  return 0;
}

//處理流程發來的消息
void ProcFLWMsg(CXMLMsg &LOGMsg)
{
  switch (LOGMsg.GetMsgId())
  {
  case MSG_gwquery:
    Proc_FLWMSG_gwquery(LOGMsg);
    break;
  case MSG_querywebservice:
    Proc_FLWMSG_querywebservice(LOGMsg);
    break;
  case MSG_getwebservicexml:
    Proc_FLWMSG_getwebservicexml(LOGMsg);
    break;
  case MSG_getwebservicestr:
    Proc_FLWMSG_getwebservicestr(LOGMsg);
    break;
  case MSG_closewebservice:
    Proc_FLWMSG_closewebservice(LOGMsg);
    break;
  case MSG_getwebserviceindex:
    Proc_FLWMSG_getwebserviceindex(LOGMsg);
    break;
  case MSG_httprequest:
    Proc_FLWMSG_httprequest(LOGMsg);
    break;
  case MSG_shellexecute:
    Proc_FLWMSG_shellexecute(LOGMsg);
    break;
  case MSG_gettxtlinestr:
    Proc_FLWMSG_gettxtlinestr(LOGMsg);
    break;
  case MSG_querycustomer:
    Proc_FLWMSG_querycustomer(LOGMsg);
    break;
  case MSG_httprequestex:
    Proc_FLWMSG_httprequestex(LOGMsg);
    break;
  case MSG_getwebservicejson:
    Proc_FLWMSG_getwebservicejson(LOGMsg);
    break;
  }
}
void SendQueryWebResult()
{
  if (g_nRunMode == 0)
    return;
  for (short i=0; i<g_pWebServiceSessionMng->m_nSessionNum; i++)
  {
    g_pWebServiceSessionMng->m_pWebServiceSession[i].m_csQuery.Lock();
    
    if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryResult == 6)
    {
      if ((GetTickCount() - g_pWebServiceSessionMng->m_pWebServiceSession[i].timer) > g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nEXECTimeOut)
      {
        FILE *fp1;
        fp1 = fopen(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strEXECReadFile, "r");
        if (fp1 != NULL)
        {
          char szTemp[256];
          if (fgets(szTemp, 255, fp1) > 0)
          {
            int len = strlen(szTemp);
            if (len > 0)
              szTemp[len-1] = '\0';

            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString.Format("%s", szTemp);
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryResult = 4; //查詢成功
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult = true;
            Send_shellexecute_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo,
              szTemp, OnDBSuccess, "");
          }
          else
          {
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryResult = 4; //查詢成功
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult = true;
            Send_shellexecute_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
              g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo,
              "", OnDBSuccess, "");
          }
          fclose(fp1);
        }
        else
        {
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryResult = 3; //查詢失敗
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult = true;
          Send_shellexecute_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo,
            "", OnDBFail, "");
        }
      }
      
      g_pWebServiceSessionMng->m_pWebServiceSession[i].m_csQuery.Unlock();
      continue;
    }

    if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult == false
      && g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryResult == 4)
    {
      //1-WEBSERVICE查詢結果成功
      if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 1)
      {
        ProcQueryCostomerResult((LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString,
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strCallerNo);

        if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString.GetLength() < 256)
          Send_querywebservice_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
            (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString, OnDBSuccess, "");
        else
          Send_querywebservice_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
            g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
            "", OnDBSuccess, "");
      }
      //2-HTTP
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 2)
      {
        ProcQueryCostomerResult((LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString,
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strCallerNo);
        
        if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString.GetLength() < 256)
          Send_httprequest_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString, OnDBSuccess, "");
        else
          Send_httprequest_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
          "", OnDBSuccess, "");
      }
      //3-EXEC
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 3)
        Send_shellexecute_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
        (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString, OnDBSuccess, "");
      //4-查客戶資料
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 4)
      {
        ProcQueryCostomerResult((LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString,
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strCallerNo);
        g_pWebServiceSessionMng->m_pWebServiceSession[i].Init(); //初始化會話
      }
      //5-HTTPex
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 5)
      {
        ProcQueryCostomerResult((LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString,
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strCallerNo);
        
        if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString.GetLength() < 256)
          Send_httprequestex_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnXmlString, 
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnCookiesString, 
          OnDBSuccess, "");
        else
          Send_httprequestex_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
          "", 
          (LPCTSTR)g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_strReturnCookiesString, 
          OnDBSuccess, "");
      }
      
      g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult = true;
      if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId == 1) //當客戶端是IVRSERVICE時，就要主動初始化，釋放會話通道
        g_pWebServiceSessionMng->m_pWebServiceSession[i].Init(); //初始化會話
    }
    else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult == false
      && g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryResult == 3)
    {
      if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 1)
        Send_querywebservice_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
          "", OnDBFail, "");
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 2)
        Send_httprequest_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
          g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
          "FAIL", OnDBFail, "");
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 3)
        Send_shellexecute_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
        "", OnDBFail, "");
      else if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_nQueryType == 5)
        Send_httprequestex_Result(g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulSessionId, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_ulCmdAddr, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnType, 
        g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usChnNo, 
        "FAIL", "", OnDBFail, "");

      g_pWebServiceSessionMng->m_pWebServiceSession[i].m_WebServiceData.m_bSendQueryResult = true;
      if (g_pWebServiceSessionMng->m_pWebServiceSession[i].m_usClientId == 1) //當客戶端是IVRSERVICE時，就要主動初始化，釋放會話通道
        g_pWebServiceSessionMng->m_pWebServiceSession[i].Init(); //初始化會話
    }
    g_pWebServiceSessionMng->m_pWebServiceSession[i].m_csQuery.Unlock();
  }
}
void MyGetMarkerString(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, int nIndex, int nMaxLen, char *pszResult, bool &bFinded)
{
  int nLen, nStartMarkerLen, nEndMarkerLen;
  char *pszStart, *pszFinded;
  int i=0;
  
  memset(pszResult, 0, nMaxLen);
  bFinded = false;

  nStartMarkerLen = strlen(pszStartMarker);
  nEndMarkerLen = strlen(pszEndMarker);

  if (nStartMarkerLen == 0)
  {
    //規定無開始標記串，則起始為0開始
    if (nEndMarkerLen == 0)
    {
      //規定無開始標記串，又規定無結束標記串，則不需要處理，取原始字符串
      memcpy(pszResult, pszSource, nMaxLen-1);
      bFinded = true;
    } 
    else
    {
      //規定無開始標記串，規定有結束串標記
      pszFinded = strstr(pszSource, pszEndMarker);
      if (pszFinded)
      {
        nLen = (int)(pszFinded - pszSource);
        if (nLen >= nMaxLen)
        {
          nLen = nMaxLen-1;
        }
        memcpy(pszResult, pszSource, nLen);
        bFinded = true;
      }
    }
  } 
  else
  {
    //規定有開始標記串
    pszStart=(char *)pszSource;
    do 
    {
      pszFinded = strstr(pszStart, pszStartMarker);
      if (pszFinded)
      {
        pszStart = pszFinded+nStartMarkerLen;
        if (i == nIndex)
        {
          pszFinded = strstr(pszStart, pszEndMarker);
          if (pszFinded)
          {
            nLen = (int)(pszFinded - pszStart);
            if (nLen >= nMaxLen)
            {
              nLen = nMaxLen-1;
            }
            memcpy(pszResult, pszStart, nLen);
            bFinded = true;
          }
          break;
        }
        i++;
      }
      else
      {
        break;
      }
    } while (true);
  }
}
int MyGetMarkerStringIndex(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, const char *pszString)
{
  int nLen, nStartMarkerLen, nEndMarkerLen;
  char *pszStart, *pszFinded, szFind[256];
  int i=0, nResult=-1;
  
  nStartMarkerLen = strlen(pszStartMarker);
  nEndMarkerLen = strlen(pszEndMarker);

  if (nStartMarkerLen == 0 || nEndMarkerLen == 0)
  {
    return nResult;
  }
  
  //規定有開始標記串
  pszStart = (char *)pszSource;
  do 
  {
    pszFinded = strstr(pszStart, pszStartMarker);
    if (pszFinded)
    {
      pszStart = pszFinded+nStartMarkerLen;
      pszFinded = strstr(pszStart, pszEndMarker);
      if (pszFinded)
      {
        nLen = (int)(pszFinded - pszStart);
        if (nLen >= 256)
        {
          nLen = 255;
        }
        strncpy(szFind, pszStart, nLen);
        if (strcmp(szFind, pszString) == 0)
        {
          nResult = i;
          break;
        }
      }
      else
      {
        break;
      }
      i++;
    }
    else
    {
      break;
    }
  } while (true);
  return nResult;
}
//UNICODE字符串轉換到UNICODE數據
//返回轉換后的UNICODE數據長度  
int ConvUniStr2Unicode(LPCSTR szUnicodeString, WCHAR *pWchar, int iBuffSize)
{
  UINT wChar = -1;
  bool bU=false;
  int l=0;
  int len = strlen(szUnicodeString);
  for (int i=0; i<len; i++)
  {
    if (l >= iBuffSize)
      break;
    if (szUnicodeString[i] == '\\')
    {
      if (bU == true)
      {
        pWchar[l] = '\\';
        l++;
      }
      else
      {
        bU = true;
      }
    }
    else if (szUnicodeString[i] == 'u')
    {
      if (bU == true)
      {
        i++;
        int nR = sscanf(&szUnicodeString[i], "%4X", &wChar);
        if (nR > 0)
        {
          pWchar[l] = wChar;
          l++;
          i+=3;
        }
        else
        {
          pWchar[l] = '\\';
          l++;
          pWchar[l] = szUnicodeString[i];
          l++;
        }
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else if (szUnicodeString[i] == 'x')
    {
      if (bU == true)
      {
        i++;
        int nR = sscanf(&szUnicodeString[i], "%2X", &wChar);
        if (nR > 0)
        {
          pWchar[l] = wChar;
          l++;
          i+=1;
        }
        else
        {
          pWchar[l] = '\\';
          l++;
          pWchar[l] = szUnicodeString[i];
          l++;
        }
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else if (szUnicodeString[i] == '/')
    {
      if (bU == true)
      {
        pWchar[l] = '/';
        l++;
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else if (szUnicodeString[i] == 'n')
    {
      if (bU == true)
      {
        pWchar[l] = '\r';
        l++;
        pWchar[l] = '\n';
        l++;
        bU = false;
      }
      else
      {
        pWchar[l] = szUnicodeString[i];
        l++;
      }
    }
    else
    {
      if (bU == true)
      {
        pWchar[l] = '\\';
        l++;
        bU = false;
      }
      pWchar[l] = szUnicodeString[i];
      l++;
    }
  }
  pWchar[l] = 0;
  return l;
}
//**************************************
// unicode字符串轉ansi字符串
// 返回大于0成功，小于0失敗
//**************************************
int ustr_astr( WCHAR *unicodestr, char *ansistr )
{
    int result = 0;
    try
    {
        int needlen = WideCharToMultiByte( CP_ACP, 0, unicodestr, -1, NULL, 0, NULL, NULL );
        if( needlen < 0 )
        {
            return needlen;
        }
 
        result = WideCharToMultiByte( CP_ACP, 0, unicodestr, -1, ansistr, needlen + 1, NULL, NULL );
        if( result < 0 )
        {
            return result;
        }
        return strlen( ansistr );
    }
    catch( ... )
    {

    }
    return result;
}
