/********************************************************************
*	文件名:		AgentImportDefine.h.h
*	目的  :		使用SOAP ToolKit 3.0 API 封裝調用WebService的接口
*	創建時間:	2011年3月21日   11時58分
*	作者: 		luodongxi
/*********************************************************************/

//1.0 SDK interface definitions

//************************************************************************
//	函數名:		_FagentSetAgentConfig
//	參數  :		nTimeOutSecs:			調用 WebService 的超時時間, 單位為秒, 默認設置為 60 秒
//				bWriteLog:				是否記錄日志信息, true 表示記錄, false 表示不記錄, 默認設置為 false
//  返回值:		true or false
//  功能說明:	是否記錄日志信息
//	創建時間:	2011年3月21日   10時02分
//	作者: 		luodongxi
//************************************************************************
typedef int _FagentSetAgentConfig(int nTimeOutSecs, bool bWriteLog);


//************************************************************************
//	函數名:		_FagentCallWebService
//	參數  :		strWSDLAddress:			SOAP協議中的 EndPointURL 值, 不需要帶后面的 ?wsdl, 
//										例如: http://10.160.130.127:9080/eemis/services/CrjWsYyxtInterface
//				strSoapAction:			SOAP協議中的 SoapAction 值, 接口中一般直接填寫: ""
//				strStartEnvelopeString:	開始構建SOAP消息, 這是一個格式化的串, 由3個字符串通過豎線("|")連接起來, 接口中一般直接填寫: "|NONE|" 3個字符串的原型為: string par_Prefix, string par_enc_style_uri, string par_encoding 具體的說明可以參考 Soap ToolKit 3.0 開發手冊
//				strStartBody:			開始構建SOAP消息體, 接口中一般直接填寫: "NONE" 
//				strInterfaceString:		開始設置接口的函數信息, 這是一個格式化的串, 由4個字符串通過豎線("|")連接起來, 4個字符串依次表示: 接口函數名稱, 接口函數的命名空間, 接口函數的編碼風格(缺省為NONE), 命名空間的前綴, 
//										例如: "GetWaitingTask|http://ws.webapp.com/|NONE|m"
//				nParamCount:			參數的個數
//				strParamString:			所有參數的集合, 多個參數之間通過豎線("|")連接起來, 每個參數由5個字符串組成, 每個字符串通過分號(";")連接起來, 5個字符串分別表示: 參數名稱, 參數的命名空間(一般為空), 參數的編碼風格(一般為NONE), 參數的前綴(一般為空), 參數的值
//										例如: 當 nParamCount 為2的時候, strParamString為: "topNumber;;NONE;;10|serverIP;;NONE;;192.168.0.179"
//				strReturnXmlString:		接口返回的完整的 xml 數據包, 由調用方自己解析具體的字段
//				strErrorMsg:			如果接口調用失敗, 返回的錯誤信息
//  返回值:		true or false, true表示成功, false表示失敗
//  功能說明:	通過 SOAP TOOLKIT 3.0 的API來調用 WebService
//	創建時間:	2011年3月21日   10時22分
//	作者: 		luodongxi
//************************************************************************
typedef int _FagentCallWebService(char *strWSDLAddress, char *strSoapAction, char *strStartEnvelopeString, char *strStartBody, char *strInterfaceString, int nParamCount, char *strParamString, char **strReturnXmlString, char **strErrorMsg);

//注意: 上述 _agentCallWebService 參數說明是參考如下 WebService 接口寫的.
/////////////////////////////////////////////////////////////////////////////////
//Request
/*
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<SOAP-ENV:Body>
		<m:GetWaitingTask xmlns:m="http://ws.webapp.com/">
			<topNumber>100</topNumber>
			<serverIP>192.168.0.152</serverIP>
		</m:GetWaitingTask>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
*/

//Failed Response
/*
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<ns1:GetWaitingTaskResponse xmlns:ns1="http://ws.webapp.com/">
			<return>
				<strResult>-1</strResult>
			</return>
		</ns1:GetWaitingTaskResponse>
	</soap:Body>
</soap:Envelope>
*/

//Succeed Response
/*
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<ns1:GetWaitingTaskResponse xmlns:ns1="http://ws.webapp.com/">
			<return>
				<strResult>1</strResult>
				<getFaxSendTaskList>
					<Subject>退電通知單</Subject>
					<Receiver>美國</Receiver>
					<PhoneNumber>206;888;999;555</PhoneNumber>
					<TaskSubmitTime>2010-04-14 15:08:45</TaskSubmitTime>
					<SendState>1</SendState>
					<BoardServerName>center</BoardServerName>
					<Channel>0</Channel>
					<BeginTime>1900-01-01 00:00:00</BeginTime>
					<EndTime>1900-01-01 00:00:00</EndTime>
					<LastErrorMsg/>
					<ScheduledDateTime>1900-01-01 00:00:00</ScheduledDateTime>
					<FailedTimes>0</FailedTimes>
					<username>admin</username>
				</getFaxSendTaskList>
			</return>
		</ns1:GetWaitingTaskResponse>
	</soap:Body>
</soap:Envelope>
*/
/////////////////////////////////////////////////////////////////////////////////

//************************************************************************
//	函數名:		_FagentFreeMemory
//	參數  :		pReturnXmlMemory:		在 agentCallWebService 接口中返回XML結果的時候, 申請的內存地址
//				pErrorMsgMemory:		在 agentCallWebService 接口中返回錯誤信息的時候, 申請的內存地址
//  返回值:		true or false
//  功能說明:	釋放 agentCallWebService 接口中申請的內存地址
//	創建時間:	2011年3月21日   10時02分
//	作者: 		luodongxi
//************************************************************************
typedef int _FagentFreeMemory(char *pReturnXmlMemory, char *pErrorMsgMemory);

