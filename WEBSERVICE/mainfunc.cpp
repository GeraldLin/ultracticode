//
//主處理函數部分
//

#include "stdafx.h"
#include "odsLog.h"
#include "vardef.h"
#include "main.h"
#include "mainfunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#define CONSOLE_RUN_MODE
char szDBServer[32], szDBName[32], szDBUser[32];

//消息類型定義：0-告警信息 1-接收消息 2-發送消息 3-內部調試信息
void MyTrace(int MsgType, LPCTSTR lpszFormat, ...)
{
  g_cLogCriticalSection.Lock();
  char buf[8192], msgtype[16];
  char LogFile[256];
  FILE	*Ftrace;	//跟蹤日志文件
  SYSTEMTIME sysTime;
  
  va_list		ArgList;
  va_start(ArgList, lpszFormat);
  _vsnprintf(buf, 8191, lpszFormat, ArgList);
  va_end (ArgList);
  CString dispbuf;
  buf[8191] = 0;
  
  switch (MsgType)
  {
  case 0:
    strcpy(msgtype, "ALRM: ");
    break;
  case 1:
    strcpy(msgtype, "RECV: ");
    break;
  case 2:
    strcpy(msgtype, "SEND: ");
    break;
  case 3:
    strcpy(msgtype, "DEBG: ");
    break;
  default:
    strcpy(msgtype, "REST: ");
    break;
  }
  if (g_bSaveId == true || MsgType == 0)
  {
    GetLocalTime(&sysTime);
    
    sprintf(LogFile, "%s\\WEB_%04d%02d%02d%02d.log", g_szLogPath, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour);
    Ftrace = fopen(LogFile, "a+t");
    if (Ftrace != NULL)
    {
      fprintf(Ftrace, "%s >> %s%s\n", GetNowTime(), msgtype, buf);
    }
    if (Ftrace != NULL)
    {
      fclose(Ftrace);
      Ftrace = NULL;
    }
  }
  #ifdef CONSOLE_RUN_MODE
  printf("%s >> %s%s\n", GetNowTime(), msgtype, buf);
  #endif
  g_cLogCriticalSection.Unlock();
}
void MyWriteFile(LPCTSTR msg)
{
  if (g_bSaveId == false)
    return;

  g_cLogCriticalSection.Lock();

  char LogFile[256];
  FILE	*Ftrace;	//跟蹤日志文件
  SYSTEMTIME sysTime;

  GetLocalTime(&sysTime);
  
  sprintf(LogFile, "%s\\WEB_%04d%02d%02d%02d.log", g_szLogPath, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour);
  Ftrace = fopen(LogFile, "a+t");
  if (Ftrace != NULL)
  {
    fprintf(Ftrace, "RecvData:\n");
    fprintf(Ftrace, "%s\n\n", msg);
    fclose(Ftrace);
    Ftrace = NULL;
  }
  g_cLogCriticalSection.Unlock();
}
char *GetNowTime() 
{
  SYSTEMTIME sysTime;
  static CH nowtime[25];
  GetLocalTime(&sysTime);
  
  sprintf(nowtime, "%04d-%02d-%02d %02d:%02d:%02d", sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
  return nowtime;
}
char *MyGetCustomNo()
{
  static int nIndex=1;
  static char strCustomNo[64];
  SYSTEMTIME sysTime;
  GetLocalTime(&sysTime);
  sprintf(strCustomNo, "%04d%02d%02d%02d%02d%02d%d%04d", sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond, 9, nIndex);
  nIndex++;
  return strCustomNo;
}
char *MyGetGUID()
{
  static char szGUID[MAX_CHAR64_LEN];
  
  memset(szGUID, 0, MAX_CHAR64_LEN);
  GUID guid = GUID_NULL;
  ::CoCreateGuid(&guid);
  if (guid == GUID_NULL)
    return "";
  sprintf(szGUID, "%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
    guid.Data1, guid.Data2, guid.Data3,
    guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
    guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);
  return szGUID;
}

void CreateAllDirectories(CString strDir)
{
  // remove ending / if exists
  if (strDir.GetLength() == 0)
    return;
  if(strDir.Right(1)=="\\" || strDir.Right(1)=="/")
    strDir=strDir.Left(strDir.GetLength()-1);
  
  // base case . . .if directory exists
  if(GetFileAttributes(strDir)!=-1)
    return;
  
  // recursive call, one less directory
  int nFound1 = strDir.ReverseFind('\\');
  int nFound2 = strDir.ReverseFind('/');
  if (nFound1>nFound2)
    CreateAllDirectories(strDir.Left(nFound1));
  else
    CreateAllDirectories(strDir.Left(nFound2));
  
  // actual work
  if (strDir.GetLength() > 0)
  {
    CreateDirectory(strDir,NULL);
  }
}

//根據參數名取參數值
short GetParamByName(const char *paramstr, const char *paramname, short defaultval, short minval, short maxval)
{
  char *pos1, *pos2, ParamName[64], param[64];
  short l, len, value;
  
  sprintf(ParamName, "%s=", paramname);
  l = strlen(ParamName);
  pos1 = strstr(paramstr, ParamName);
  if (pos1 == NULL)
  {
    return defaultval;
  }
  pos1 +=l;
  pos2 = strstr(pos1, ";");
  memset(param, 0, 64);
  if (pos2 == NULL)
  {
    strncpy(param, pos1, 32);
  }
  else
  {
    len = pos2 - pos1;
    strncpy(param, pos1, len);
  }
  value = atoi(param);
  if (value < minval || value > maxval)
    return defaultval;
  else
    return value;
}
//數據庫處理線程
UINT DbProc(LPVOID pParam)
{
  while (1)
  {
    if (g_nExistThread == 1)
    {
      break;
    }
    ProcSqlFiFo();

    Sleep(100);
  }
  return 0;
}

//-------------------通信消息處理-------------------------------------------
//檢查接收消息隊列
void CheckMsgFifo()
{
	US count = 0, ClientId, MsgNo, MsgId;
  CH MsgBuf[MAX_MSG_BUF_LEN];

	while(!RMsgFifo->IsEmpty() && count < 64) //一次最多處理64條信息
	{
		if (RMsgFifo->Read(ClientId, MsgNo, MsgBuf) != 0)
    {
      MsgId = MsgNo & 0xFF;
      if ((MsgNo>>8) == MSGTYPE_DBXML)
      {
        WEBRcvMsg.SetMsgBufId(MsgId,(char *)MsgBuf);
        WEBRcvMsg.SetClientId(ClientId);
        if (Proc_Msg_From_FLW(WEBRcvMsg) == 0)
        {
          ProcFLWMsg(WEBRcvMsg);
        }
      }
    }
		count ++;
	}
}		
//-------------------IVR通信事件處理-------------------------------------------
void OnWEBLogin(unsigned short serverid,unsigned short clientid)     //tcllinkc.dll callback function
{
  UC NodeType, NodeId;
  //int nAG;
	pTcpLink->DisCombineID(clientid, NodeType, NodeId);
	if (NodeType == NODE_XML)
	{
		MyTrace(0, "ParseFlw[%d] Login", NodeId);
    #ifndef CONSOLE_RUN_MODE
    odsAddToSystemLog(SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "ParseFlw[%d] Login", NodeId);
    #endif
	}
  else
  {
    MyTrace(0, "Other Node[0x%04x] Login", clientid);
    #ifndef CONSOLE_RUN_MODE
    odsAddToSystemLog(SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "Other Node[%d] Login", clientid);
    #endif
  }
}

void OnWEBReceiveData(unsigned short remoteid,unsigned short msgtype,
                                    const unsigned char *buf,
                                    unsigned long len) //tcllinkc.dll callback function
{
 	UC msgid=msgtype>>8;
  ((unsigned char *)buf)[len] = 0;
  
  if(msgid == MSGTYPE_DBXML)
  {
    MyTrace(1, "0x%04x %s", remoteid, (char *)buf);
  	if (RMsgFifo->Write(remoteid, msgtype, (char *)buf) == 0)
  		MyTrace(0, "Recv Msg Fifo over");
  }
  else
  	MyTrace(0, "OnWEBReceiveData other msgtype");
}

void OnWEBClose(unsigned short serverid,unsigned short clientid)    //tcllinkc.dll callback function
{
  UC NodeType, NodeId;
	
  pTcpLink->DisCombineID(clientid, NodeType, NodeId);
	if (NodeType == NODE_XML)
	{
    MyTrace(0, "ParseFlw[%d] Logout", NodeId);
    #ifndef CONSOLE_RUN_MODE
    odsAddToSystemLog(SRV_APPNAME, EVENTLOG_WARNING_TYPE, "ParseFlw[%d] Logout", NodeId);
    #endif
	}
  else
  {
    MyTrace(0, "Other Node[0x%04x] Logout", clientid);
#ifndef CONSOLE_RUN_MODE
    odsAddToSystemLog(SRV_APPNAME, EVENTLOG_INFORMATION_TYPE, "Other Node[%d] Logout", clientid);
#endif
  }
}
//-----------------------------------------------------------------------------
//讀總的服務的配置文件（返回0表示成功）
int ReadSERVICEIniFile()
{
  //取程序所在路徑
  TCHAR szFull[_MAX_PATH];
  TCHAR szDrive[_MAX_DRIVE];
  TCHAR szDir[_MAX_DIR];
  char szDBtype[32], szPsw[64], szTemp[64];
  CString strPSW;

  GetModuleFileName(NULL, szFull, sizeof(szFull)/sizeof(TCHAR));
  _tsplitpath(szFull, szDrive, szDir, NULL, NULL);
  _tcscpy(szFull, szDrive);
  _tcscat(szFull, szDir);
  strcpy(g_szAppPath, szFull);
  memset(g_szRootPath, 0, MAX_PATH_LEN);
  strncpy(g_szRootPath, g_szAppPath, strlen(g_szAppPath)-12);
  
  //IVR配置文件名
  sprintf(g_szServiceINIFileName, "%sWEBSERVICE.ini", g_szAppPath);
  sprintf(g_szDBINIFileName, "%s\\DBSERVICE\\DBSERVICE.ini", g_szRootPath);
  
  GetPrivateProfileString("LOG", "LogPath", "", g_szLogPath, 128, g_szServiceINIFileName);
  if (strlen(g_szLogPath)==0)
    sprintf(g_szLogPath, "%smsg", g_szAppPath);
  CreateAllDirectories((CString)g_szLogPath);
  
  MyTrace(0, "--start--------------------------------------------------------------------------------");

  GetPrivateProfileString("CONFIGURE","DBTYPE","MYSQL",szDBtype,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","DBSERVER","127.0.0.1",szDBServer,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","DATABASE","callcenterdb",szDBName,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","USERID","root",szDBUser,32,g_szDBINIFileName);
  GetPrivateProfileString("CONFIGURE","PASSWORD","",szTemp,64,g_szDBINIFileName);
  strcpy(szPsw, DecodePass(szTemp)); //密碼加密
  //strcpy(szPsw, szTemp); //密碼不加密
  if (stricmp(szDBtype, "SQLSERVER") == 0) //sql server
  {
    sprintf(g_szConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s", 
      szPsw, szDBUser, szDBName, szDBServer);
  }
  else if (stricmp(szDBtype, "ORACLE") == 0) //oracle
  {
    sprintf(g_szConnectionString, "Provider=OraOLEDB.Oracle.1;Password=%s;Persist Security Info=True;User ID=%s;Data Source=%s", 
      szPsw, szDBUser, szDBServer );
  }
  else if (stricmp(szDBtype, "SYBASE") == 0) //sybase
  {
    sprintf(g_szConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s", 
      szPsw, szDBUser, szDBName, szDBServer);
  }
  else if (stricmp(szDBtype, "ACCESS") == 0) //access
  {
    sprintf(g_szConnectionString, "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%s", szDBName);
  }
  else if (stricmp(szDBtype, "MYSQL") == 0) //mysql
  {
    sprintf(g_szConnectionString, "Driver={MySQL ODBC 5.1 Driver};server=%s;uid=%s;pwd=%s;database=%s;", 
      szDBServer, szDBUser, szPsw, szDBName);
  }
  else if (stricmp(szDBtype, "ODBC") == 0) //odbc
  {
    sprintf(g_szConnectionString, "Data Source=%s;UID=%s;PWD=%s;", szDBName, szDBUser, szPsw);
  }
  else
  {
    sprintf(g_szConnectionString, "Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s", 
      szPsw, szDBUser, szDBName, szDBServer);
  }

  //HTTP消息是否是unicode轉義編碼，如漢字會變為：\u6d4b
  if (GetPrivateProfileInt("HTTP","UnicodeDecode", 0, g_szServiceINIFileName) == 1)
  {
    g_bHTTPUnicodeDecode = true;
  }

  //讀webservice接口參數
  GetPrivateProfileString("WEBSERVICEPARAM", "wsdladdress", "", g_WebServiceParam.WS_wsdladdress, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "soapaction", "", g_WebServiceParam.WS_soapaction, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "startenvelope", "", g_WebServiceParam.WS_startenvelope, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "startbody", "", g_WebServiceParam.WS_startbody, 255, g_szServiceINIFileName);
  GetPrivateProfileString("WEBSERVICEPARAM", "interface", "", g_WebServiceParam.WS_interface, 255, g_szServiceINIFileName);
  g_WebServiceParam.WS_paramcount = GetPrivateProfileInt("WEBSERVICEPARAM", "paramcount", 0, g_szServiceINIFileName);
  for (int i=0; i<g_WebServiceParam.WS_paramcount; i++)
  {
    sprintf(szTemp, "paramstring%d", i+1);
    GetPrivateProfileString("WEBSERVICEPARAM", szTemp, "", g_WebServiceParam.WS_paramstring[i], 255, g_szServiceINIFileName);
  }
  MyTrace(0, "wsdladdress=%s", g_WebServiceParam.WS_wsdladdress);
  MyTrace(0, "soapaction=%s", g_WebServiceParam.WS_soapaction);
  MyTrace(0, "startenvelope=%s", g_WebServiceParam.WS_startenvelope);
  MyTrace(0, "startbody=%s", g_WebServiceParam.WS_startbody);
  MyTrace(0, "interface=%s", g_WebServiceParam.WS_interface);
  MyTrace(0, "paramstring1=%s", g_WebServiceParam.WS_paramstring[0]);

  //讀是否保存日志標志
  if (GetPrivateProfileInt("LOG","SaveId", 0, g_szServiceINIFileName) == 1)
  {
    g_bSaveId = true;
  }
  if (GetPrivateProfileInt("LOG","TestId", 0, g_szServiceINIFileName) == 1)
  {
    g_bTestId = true;
  }
  g_nRunMode = GetPrivateProfileInt("WEBSERVICE","RunMode", 0, g_szServiceINIFileName);
  g_nWebServiceOutTime = GetPrivateProfileInt("WEBSERVICE","WebServiceOutTime", 60, g_szServiceINIFileName);

  return 0;
}

void ReadCustomerFieldSetup()
{
  int num1,num2;
  char szTemp[1024], szSection[64];
  CStringX ArrString1[4], ArrString2[4];

  if (GetPrivateProfileInt("QUERYCUSTOMER","OPENFLAG", 0, g_szServiceINIFileName) == 1)
  {
    g_CustomerField.bOpenFlag = true;
  }
  GetPrivateProfileString("QUERYCUSTOMER", "RESULT", "", szTemp, 255, g_szServiceINIFileName);
  if (strlen(szTemp) > 0)
  {
    num1 = SplitString(szTemp, '`', 3, ArrString1);
    if (num1 >= 3)
    {
      strncpy(g_CustomerField.CF_RESULT_STARTTAG, ArrString1[0].C_Str(), 127);
      strncpy(g_CustomerField.CF_RESULT_ENDTAG, ArrString1[2].C_Str(), 127);
      num2 = SplitString(ArrString1[1].C_Str(), '|', 2, ArrString2);
      if (num2 >= 2)
      {
        strncpy(g_CustomerField.CF_RESULT_OK, ArrString2[0].C_Str(), 127);
        strncpy(g_CustomerField.CF_RESULT_FAIL, ArrString2[1].C_Str(), 127);
        g_CustomerField.bResultTag = true;
      }
    }
  }
  MyTrace(0, "QUERYCUSTOMER RESULT CF_RESULT_STARTTAG=%s CF_RESULT_ENDTAG=%s CF_RESULT_OK=%s", 
    g_CustomerField.CF_RESULT_STARTTAG, g_CustomerField.CF_RESULT_ENDTAG, g_CustomerField.CF_RESULT_OK);
  GetPrivateProfileString("QUERYCUSTOMER", "RECORDNUM", "", szTemp, 255, g_szServiceINIFileName);
  if (strlen(szTemp) > 0)
  {
    num1 = SplitString(szTemp, '`', 2, ArrString1);
    if (num1 >= 2)
    {
      strncpy(g_CustomerField.CF_RECORDNUM_STARTTAG, ArrString1[0].C_Str(), 127);
      strncpy(g_CustomerField.CF_RECORDNUM_ENDTAG, ArrString1[1].C_Str(), 127);
      g_CustomerField.bRecordNumTag = true;
    }
    else
    {
      g_CustomerField.RECORDNUM = atoi(szTemp);
      if (g_CustomerField.RECORDNUM <= 0)
        g_CustomerField.RECORDNUM = 1;
    }
  }
  MyTrace(0, "QUERYCUSTOMER RECORDNUM CF_RECORDNUM_STARTTAG=%s CF_RECORDNUM_ENDTAG=%s", 
    g_CustomerField.CF_RECORDNUM_STARTTAG, g_CustomerField.CF_RECORDNUM_ENDTAG);

  int j=0;
  g_CustomerField.FIELDNUM = GetPrivateProfileInt("QUERYCUSTOMER","FIELDNUM", 0, g_szServiceINIFileName);
  if (g_CustomerField.FIELDNUM > 0)
  {
    g_CustomerField.Init(g_CustomerField.FIELDNUM);
  }
  for (int i=0; i<g_CustomerField.FIELDNUM; i++)
  {
    sprintf(szSection, "FIELD[%d]", i);
    GetPrivateProfileString("QUERYCUSTOMER", szSection, "", szTemp, 255, g_szServiceINIFileName);
    if (strlen(szTemp) > 0)
    {
      num1 = SplitString(szTemp, '`', 3, ArrString1);
      if (num1 >= 3)
      {
        strncpy(g_CustomerField.pFIELD[j].CF_FIELD_STARTTAG, ArrString1[0].C_Str(), 127);
        strncpy(g_CustomerField.pFIELD[j].CF_FIELD_NAME, ArrString1[1].C_Str(), 127);
        strncpy(g_CustomerField.pFIELD[j].CF_FIELD_ENDTAG, ArrString1[2].C_Str(), 127);
        
        MyTrace(0, "QUERYCUSTOMER FIELD CF_FIELD_STARTTAG=%s CF_FIELD_NAME=%s CF_FIELD_ENDTAG=%s", 
          g_CustomerField.pFIELD[j].CF_FIELD_STARTTAG, g_CustomerField.pFIELD[j].CF_FIELD_NAME, g_CustomerField.pFIELD[j].CF_FIELD_ENDTAG);

        j++;
      }
    }
  }
  GetPrivateProfileString("QUERYCUSTOMER", "INSERTSQL", "", g_CustomerField.INSERTSQL, 2047, g_szServiceINIFileName);
}

void ProcQueryCostomerResult(const char *pXMLResult, const char *pszCallerNo)
{
  bool bFinded, bSuccess=false;
  char szResult[1024], szOldSql[4096], szNewSql[4096];
  int i, nRecordNum=0;

  if (g_CustomerField.bOpenFlag == false)
    return;
  MyTrace(1, "ProcQueryCostomerResult\n%s", pXMLResult);
  if (g_CustomerField.bResultTag == true)
  {
    MyGetMarkerString(pXMLResult, g_CustomerField.CF_RESULT_STARTTAG, g_CustomerField.CF_RESULT_ENDTAG, 0, 1024, szResult, bFinded);
    MyTrace(1, "MyGetMarkerString CF_RESULT_STARTTAG=%s CF_RESULT_ENDTAG=%s CF_RESULT_OK=%s szResult=%s bFinded=%d", 
      g_CustomerField.CF_RESULT_STARTTAG, g_CustomerField.CF_RESULT_ENDTAG, g_CustomerField.CF_RESULT_OK, szResult, bFinded);
    if (bFinded == true)
    {
      if (strcmp(szResult, g_CustomerField.CF_RESULT_OK) == 0)
      {
        bSuccess = true;
      }
    }
  }
  else
  {
    bSuccess = true;
  }
  if (bSuccess == false)
  {
    strcpy(szOldSql, g_CustomerField.INSERTSQL);
    MyStringReplace(szOldSql, "$S_CustomNo", "", 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    MyStringReplace(szOldSql, "$S_GUID", "", 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    sprintf(szResult, "%d", -1);
    MyStringReplace(szOldSql, "$S_RecordNo", szResult, 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    for (i=0; i<g_CustomerField.FIELDNUM; i++)
    {
      if (strcmp(g_CustomerField.pFIELD[i].CF_FIELD_NAME, "$S_MobileNo") == 0)
        MyStringReplace(szOldSql, g_CustomerField.pFIELD[i].CF_FIELD_NAME, pszCallerNo, 0, 16, 4096, szNewSql);
      else
        MyStringReplace(szOldSql, g_CustomerField.pFIELD[i].CF_FIELD_NAME, "", 0, 16, 4096, szNewSql);
      memcpy(szOldSql, szNewSql, 4096);
    }
    SQLSfifo.Write(szNewSql, strlen(szNewSql));
    
    return;
  }

  if (g_CustomerField.bRecordNumTag == true)
  {
    MyGetMarkerString(pXMLResult, g_CustomerField.CF_RECORDNUM_STARTTAG, g_CustomerField.CF_RECORDNUM_ENDTAG, 0, 1024, szResult, bFinded);
    if (bFinded == true)
    {
      nRecordNum = atoi(szResult);
      if (nRecordNum > 128)
      {
        nRecordNum = 128;
      }
    }
  }
  else
  {
    nRecordNum = g_CustomerField.RECORDNUM;
  }
  if (nRecordNum == 0)
    return;
  int nIndex=0;
  
  while (nIndex < nRecordNum)
  {
    strcpy(szOldSql, g_CustomerField.INSERTSQL);
    MyStringReplace(szOldSql, "$S_CustomNo", MyGetCustomNo(), 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    MyStringReplace(szOldSql, "$S_GUID", MyGetGUID(), 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);
    sprintf(szResult, "%d", nIndex);
    MyStringReplace(szOldSql, "$S_RecordNo", szResult, 0, 16, 4096, szNewSql);
    memcpy(szOldSql, szNewSql, 4096);

    for (i=0; i<g_CustomerField.FIELDNUM; i++)
    {
      MyGetMarkerString(pXMLResult, g_CustomerField.pFIELD[i].CF_FIELD_STARTTAG, g_CustomerField.pFIELD[i].CF_FIELD_ENDTAG, nIndex, 1024, szResult, bFinded);
      //MyTrace(1, "%s %s %s %s %s", g_CustomerField.pFIELD[i].CF_FIELD_STARTTAG, g_CustomerField.pFIELD[i].CF_FIELD_NAME, g_CustomerField.pFIELD[i].CF_FIELD_ENDTAG, szResult, bFinded==true ? "true" : "false");

      if (bFinded == true)
      {
        if (strcmp(g_CustomerField.pFIELD[i].CF_FIELD_NAME, "$S_MobileNo") == 0
          || strcmp(g_CustomerField.pFIELD[i].CF_FIELD_NAME, "$S_TeleNo") == 0
          || strcmp(g_CustomerField.pFIELD[i].CF_FIELD_NAME, "$S_FaxNo") == 0
          )
        {
          ClearCharInCallerNo(szResult);
        }
        MyStringReplace(szOldSql, g_CustomerField.pFIELD[i].CF_FIELD_NAME, szResult, 0, 16, 4096, szNewSql);
        memcpy(szOldSql, szNewSql, 4096);
      }
      else
      {
        MyStringReplace(szOldSql, g_CustomerField.pFIELD[i].CF_FIELD_NAME, "", 0, 16, 4096, szNewSql);
        memcpy(szOldSql, szNewSql, 4096);
      }
    }
    SQLSfifo.Write(szNewSql, strlen(szNewSql));
    nIndex++;
  }
}

int ConnectDB()
{
  if (g_CustomerField.bOpenFlag == false) //2016-12-01如果沒有打開查詢客戶資料就不連數據庫
    return 0;
  try
  {
    if (g_pDbConnect.Open(g_szConnectionString))
    {
      MyTrace(0, "ConnectDB success DBServer=%s DBName=%s DBUser=%s", szDBServer, szDBName, szDBUser);
      g_nConnectDbState = 1;
      return 0;
    }
    else
    {
      MyTrace(0, "ConnectDB fail DBServer=%s DBName=%s DBUser=%s", szDBServer, szDBName, szDBUser);
      g_nConnectDbState = 2;
    }
  }
  catch (CADOException e)
  {
    MyTrace(0, "ConnectDB fail=%s DBServer=%s DBName=%s DBUser=%s", (LPCTSTR)e.GetErrorMessage(), szDBServer, szDBName, szDBUser);
    g_nConnectDbState = 2;
  }
  return 1;
}

void ProcSqlFiFo()
{
  CH SqlBuf[4096];
  US count=0, len;
  static int count1=0;

  if (g_CustomerField.bOpenFlag == false) //2016-12-01如果沒有打開查詢客戶資料就不處理sql
    return;
  if (g_nConnectDbState != 1 && count1++ > 1000)
  {
    ConnectDB();
    count1 = 0;
  }
  if (g_nConnectDbState != 1)
    return;
  while (count < 64 && SQLSfifo.Read(SqlBuf, len) != 0)
  {
    try
    {
      MyTrace(1, "ExecSql sql=%s", SqlBuf);
      g_pDbConnect.Execute(SqlBuf);
    }
    catch (CADOException e)
    {
      MyTrace(1, "ExecSql fail %s", (LPCTSTR)e.GetErrorMessage());
      if (strstr((LPCTSTR)e.GetErrorMessage(), "連接失敗") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "超時已過期") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "Missing Connection or ConnectionString") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "gone away") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "not connected to ORACLE") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "超出打開游標的最大數") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "一般性網絡錯誤") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "未連接到") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "硈??毖") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "?�Z�I??蕩�Z����") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "筄��?戳") != NULL
        || strstr((LPCTSTR)e.GetErrorMessage(), "ORA-03114") != NULL
        )
      {
        g_nConnectDbState = 2;
        g_pDbConnect.Close();
      }
    }
    if (g_nExistThread == 1)
    {
      break;
    }

    count++;
  }
}

//初始化用到的類資源（返回0表示成功）
int InitResourceMem()
{
  //通信動態庫
  ConnectDB();

  pTcpLink = new CTCPLink;
  if (pTcpLink == NULL) 
  {
    return 1;
  }
  else
  {
    pTcpLink->ReadIni(g_szServiceINIFileName);
    //初始化服務端
    if(pTcpLink->InitWEBServer() != 0)
    {
      return 2;
    }  
  }
  //通信消息規則
  pFlwRule = new CFlwRule;
  if (pFlwRule == NULL) 
  {
    return 3;
  }
  //接收消息緩沖初始化	
  RMsgFifo = new CMsgfifo(MAX_MSG_QUEUE_NUM);
  if (RMsgFifo == NULL) 
  {
    return 4;
  }
  //初始化會話
  g_pWebServiceSessionMng = new CQWebServiceSessionMng;
  if (g_pWebServiceSessionMng == NULL) 
  {
    return 5;
  }
  g_pWebServiceSessionMng->Init(MAX_SESSION_NUM);
  if (Fun_LoadAgentDLL() == false)
  {
    return 6;
  }
  return 0;
}
//運行前初始化所有（返回0表示成功）
int InitRun(void)
{
  ::CoInitialize(NULL);
  if (ReadSERVICEIniFile() != 0)
  {
    return 1;
  }
  ReadCustomerFieldSetup();
  if (InitResourceMem() != 0)
  {
#ifndef CONSOLE_RUN_MODE
    odsAddToSystemLog(SRV_APPNAME, EVENTLOG_WARNING_TYPE, "InitMsg: InitResourceMem fail");
#endif
    return 2;
  }
  g_hDbThrdProc = AfxBeginThread(DbProc, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
  if (g_hDbThrdProc)
  {
    g_hDbThrdProc->m_bAutoDelete = true;
    g_hDbThrdProc->ResumeThread();
  }
  return 0;
}
void ReleaseAllResouce()
{
  if (pTcpLink != NULL) //通信消息類
  {
    delete pTcpLink;
    pTcpLink = NULL;
  }
  if (pFlwRule != NULL) //流程規則類
  {
    delete pFlwRule;
    pFlwRule = NULL;
  }
  if (RMsgFifo != NULL) //接收消息隊列
  {
    delete RMsgFifo;
    RMsgFifo = NULL;
  }
  if (g_pWebServiceSessionMng != NULL)
  {
    delete g_pWebServiceSessionMng;
    g_pWebServiceSessionMng = NULL;
  }
  Fun_FreeAgentDLL();
  g_pDbConnect.Close();
  ::CoUninitialize();
}
//主循環
void APPLoop() 
{
  pTcpLink->AppMainLoop();
	CheckMsgFifo();
  SendQueryWebResult();
  ProcSqlFiFo();
}
