// Request.cpp: implementation of the Request class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Request.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Request::Request()
{

}

Request::~Request()
{

}


//*******************************************************************************************************
//MemBufferCreate: 
//					Passed a MemBuffer structure, will allocate a memory buffer 
//                   of MEM_BUFFER_SIZE.  This buffer can then grow as needed.
//*******************************************************************************************************
void Request::MemBufferCreate(MemBuffer *b)
{
    b->size = MEM_BUFFER_SIZE;
    b->buffer =(unsigned	char *) malloc( b->size );
    b->position = b->buffer;
}

//*******************************************************************************************************
// MemBufferGrow:  
//					Double the size of the buffer that was passed to this function. 
//*******************************************************************************************************
void Request::MemBufferGrow(MemBuffer *b)
{
    size_t sz;
    sz = b->position - b->buffer;
    b->size = b->size *2;
    b->buffer =(unsigned	char *) realloc(b->buffer,b->size);
    b->position = b->buffer + sz;	// readjust current position
}

//*******************************************************************************************************
// MemBufferAddByte: 
//					Add a single byte to the memory buffer, grow if needed.
//*******************************************************************************************************
void Request::MemBufferAddByte(MemBuffer *b,unsigned char byt)
{
    if( (size_t)(b->position-b->buffer) >= b->size )
        MemBufferGrow(b);

    *(b->position++) = byt;
}

//*******************************************************************************************************
// MemBufferAddBuffer:
//					Add a range of bytes to the memory buffer, grow if needed.
//*******************************************************************************************************
void Request::MemBufferAddBuffer(MemBuffer *b,
                    unsigned char *buffer, size_t size)
{
    while( ((size_t)(b->position-b->buffer)+size) >= b->size )
        MemBufferGrow(b);

    memcpy(b->position,buffer,size);
    b->position+=size;
}

//*******************************************************************************************************
// GetHostAddress: 
//					Resolve using DNS or similar(WINS,etc) the IP 
//                   address for a domain name such as www.wdj.com. 
//*******************************************************************************************************
DWORD Request::GetHostAddress(LPCSTR host)
{
    struct hostent *phe;
    char *p;

    phe = gethostbyname( host );
            
    if(phe==NULL)
        return 0;
    
    p = *phe->h_addr_list;
    return *((DWORD*)p);
}

//*******************************************************************************************************
// SendString: 
//					Send a string(null terminated) over the specified socket.
//*******************************************************************************************************
bool Request::SendString(SOCKET sock,LPCSTR str)
{
    if (send(sock,str,strlen(str),0) == SOCKET_ERROR)
    {
      closesocket(sock);
      return false;
    }
    else
      return true;
}

//*******************************************************************************************************
// ValidHostChar: 
//					Return TRUE if the specified character is valid
//						for a host name, i.e. A-Z or 0-9 or -.: 
//*******************************************************************************************************
BOOL Request::ValidHostChar(char ch)
{
    return( isalpha(ch) || isdigit(ch)
        || ch=='-' || ch=='.' || ch==':' );
}


//*******************************************************************************************************
// ParseURL: 
//					Used to break apart a URL such as 
//						http://www.localhost.com:80/TestPost.htm into protocol, port, host and request.
//*******************************************************************************************************
void Request::ParseURL(LPCSTR url,LPSTR protocol,int lprotocol,
        LPSTR host,int lhost,LPSTR request,int lrequest,int *port)
{
    char *work,*ptr,*ptr2;

    *protocol = *host = *request = 0;
    *port=80;

    work = strdup(url);
    strupr(work);

    ptr = strchr(work,':');							// find protocol if any
    if(ptr!=NULL)
    {
        *(ptr++) = 0;
        lstrcpyn(protocol,work,lprotocol);
    }
    else
    {
        lstrcpyn(protocol,"HTTP",lprotocol);
        ptr = work;
    }

    if( (*ptr=='/') && (*(ptr+1)=='/') )			// skip past opening /'s 
        ptr+=2;

    ptr2 = ptr;										// find host
    while( ValidHostChar(*ptr2) && *ptr2 )
        ptr2++;

    *ptr2=0;
    lstrcpyn(host,ptr,lhost);

    lstrcpyn(request,url + (ptr2-work),lrequest);	// find the request

    ptr = strchr(host,':');							// find the port number, if any
    if(ptr!=NULL)
    {
        *ptr=0;
        *port = atoi(ptr+1);
    }

    free(work);
}
//根據HTTP消息頭去消息體長度
int Request::GetContentLength(LPCSTR headerReceive)
{
  char szTemp[16];
  memset(szTemp, 0, 8);
  char *fstr1 = strstr(headerReceive, "Content-Length: ");
  if (fstr1)
  {
    char *fstr2 = strstr(fstr1, "\r\n");
    if (fstr2)
    {
      int len = (int)(fstr2 - fstr1) - 16;
      if (len < 16)
      {
        strncpy(szTemp, fstr1+16, len);
        return atoi(szTemp);
      }
    }
  }
  return -1;
}

//*******************************************************************************************************
// SendHTTP: 
//					Main entry point for this code.  
//					  url			- The URL to GET/POST to/from.
//					  headerSend		- Headers to be sent to the server.
//					  post			- Data to be posted to the server, NULL if GET.
//					  postLength	- Length of data to post.
//					  req			- Contains the message and headerSend sent by the server.
//
//					  returns 1 on failure, 0 on success.
//*******************************************************************************************************
int Request::SendHTTP(LPCSTR url,LPCSTR cookies,LPCSTR headerReceive,BYTE *post,
        DWORD postLength,HTTPRequest *req)
{
    WSADATA			WsaData;
    SOCKADDR_IN	sin;
    SOCKET			sock;
    char			  buffer[16384];
    char			  protocol[20],host[256],request[1024],cookiefield[256];
    int				  rc, port, chars, err, TimeOut;
    MemBuffer		headersBuffer,messageBuffer;
	  char			  headerSend[1024];
    BOOL			  done;
    DWORD       dwOldTimer = GetTickCount();
    unsigned long cmd;
    int nContentLength;

    ParseURL(url,protocol,sizeof(protocol),host,sizeof(host),		// Parse the URL
        request,sizeof(request),&port);
    if (strcmp(protocol,"HTTP"))
        return 1;

    err = WSAStartup(0x0101, &WsaData);							// Init Winsock
    if (err!=0)
        return 1;

    sock = socket(AF_INET, SOCK_STREAM, 0);
	  if (sock == INVALID_SOCKET)
        return 2;
    
    TimeOut=6000;   //設置發送超時6秒   
    if (setsockopt(sock,SOL_SOCKET,SO_SNDTIMEO,(char *)&TimeOut,sizeof(TimeOut))==SOCKET_ERROR)
    {   
      closesocket(sock);
      return 3;   
    }   
    TimeOut=6000;//設置接收超時6秒   
    if (setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&TimeOut,sizeof(TimeOut))==SOCKET_ERROR)
    {   
      closesocket(sock);
      return 3;   
    }   
    
    //把socket設置成非阻塞方式
    cmd = 1;
    ioctlsocket(sock, FIONBIO, &cmd);
    
    sin.sin_family = AF_INET;										//Connect to web sever
    sin.sin_port = htons( (unsigned short)port );
    sin.sin_addr.s_addr = GetHostAddress(host);

    err = connect(sock, (LPSOCKADDR)&sin, sizeof(SOCKADDR_IN));

    //select   模型，
    struct timeval timeout;   
    fd_set r;   
    
    FD_ZERO(&r);   
    FD_SET(sock, &r);   
    timeout.tv_sec = 10;   //連接超時10秒   
    timeout.tv_usec = 0;   
    err = select(0, 0, &r, 0, &timeout); 
    if (err <= 0)   
    {   
      closesocket(sock);
      TRACE("connect fail timer=%d\n", GetTickCount()-dwOldTimer);
      return 4;   
    }   
    //再設回阻塞模式   
    cmd = 0;   
    err = ioctlsocket(sock, FIONBIO, (unsigned long*)&cmd);   
    if (err==SOCKET_ERROR)
    {   
      closesocket(sock);   
      return 5;   
    }

    //開始發送http消息
    if( !*request )
        lstrcpyn(request,"/",sizeof(request));

    if( post == NULL )
	  {
		    strcpy(headerSend, "GET ");
	  }
    else 
	  {
		    strcpy(headerSend, "POST ");
	  }
		strcat(headerSend, request);
		strcat(headerSend, " HTTP/1.1\r\n");
		strcat(headerSend, "Accept-Encoding: identity\r\n");
		strcat(headerSend, "User-Agent: CTICALLCLIENT\r\n");
    if(postLength)
    {
        wsprintf(buffer,"Content-Length: %ld\r\n",postLength);
		    strcat(headerSend, buffer);
    }
    strcat(headerSend, "Connection: close\r\n");
    if (strlen(cookies) > 0)
    {
      wsprintf(cookiefield, "Cookie: %s\r\n", cookies);
      strcat(headerSend, cookiefield);
    }
		strcat(headerSend, "Host: ");
		strcat(headerSend, host);
		strcat(headerSend, "\r\n");
    if ( (headerReceive!=NULL) && *headerReceive )
	  {
		    strcat(headerSend, headerReceive);
	  }
    
		strcat(headerSend, "\r\n");

    if( (post!=NULL) && postLength )
	  {
		    post[postLength]	= '\0';
		    strcat(headerSend, (const char*)post);
	  }
    if (SendString(sock,headerSend) == false)								// Send a blank line to signal end of HTTP headerReceive
      return 6;
    
    //保存發送的消息內容
	  req->headerSend	= (char*) malloc( sizeof(char*) * strlen(headerSend));
	  strcpy(req->headerSend, (char*) headerSend );
    //Sleep(50);

    MemBufferCreate(&headersBuffer ); //申請接收消息頭緩沖內存
    MemBufferCreate(&messageBuffer); //申請接收消息體緩沖內存
    chars = 0;
    done = FALSE;
    
#ifndef SOCKET_BLOCK_RECV_DATA
    //-----------------------------------------------------------------------------
    //以下為非阻塞方式接收
    bool bRecvHeader=false, bRecvBody=false;
    
    cmd = 1;
    err = ioctlsocket(sock, FIONBIO, (unsigned long*)&cmd);   
    if (err==SOCKET_ERROR)
    {   
      closesocket(sock);   
      return 7;   
    }

    //循環select(如果超時就退出)
    fd_set rset;
    struct timeval wtime;
    wtime.tv_sec = 0; //秒值
    wtime.tv_usec = 1000; //毫秒
    int pos=0, recvlen=0;
    unsigned char c;

    req->headerReceive = (char*) headersBuffer.buffer;
    req->message = (char*) messageBuffer.buffer;
    //開始計時
    while (true)
    {
      if(GetTickCount()-dwOldTimer > 15000)
      {
        closesocket(sock);
        TRACE("Recv Timeout=%d\n", GetTickCount()-dwOldTimer);
        if (bRecvBody == true)
        {
          return 0;
        }
        else
        {
          return 8;
        }
      }
      FD_ZERO(&rset);
      FD_SET(sock, &rset);
      err = select(sock+1, &rset, NULL, NULL, &wtime);
      if(err == 0)	
      {
        //服務端響應超時就退出
        if(GetTickCount()-dwOldTimer > 10000)
        {
          closesocket(sock);
          TRACE("Recv Timeout=%d\n", GetTickCount()-dwOldTimer);
          if (bRecvBody == true)
          {
            return 0;
          }
          else
          {
            return 9;
          }
        }
        else
          continue;
      }
      else if (err > 0)
      {
        rc = recv(sock, buffer, 1, MSG_PEEK);
        if(rc<0) 
          err = GetLastError();
        
        if(rc<0 && err!=WSAEMSGSIZE)
        {
          closesocket(sock);
          if (bRecvBody == true)
          {
            return 0;
          }
          else
          {
            return 10;
          }
        }
        else if(rc == 0)
        {
          closesocket(sock);
          if (bRecvBody == true)
          {
            return 0;
          }
          else
          {
            return 11;
          }
        }
        else
        {
          //可接收
          rc = recv(sock,buffer,sizeof(buffer)-1,0);
          *(buffer+rc)=0;
          pos = 0;
          done = FALSE;
          chars = 0;
          if (rc == SOCKET_ERROR)
          {
            closesocket(sock);
            if (bRecvBody == true)
            {
              return 0;
            }
            else
            {
              return 12;
            }
          }
          else if (rc > 0)
          {
            if (bRecvHeader == false)
            {
              while(!done && pos < rc)
              {
                c = buffer[pos];
                switch(c)
                {
                case '\r':
                  break;
                case '\n':
                  if(chars==0)
                  {
                    bRecvHeader = true;
                    done = TRUE;
                  }
                  chars=0;
                  break;
                default:
                  chars++;
                  break;
                }
                
                MemBufferAddByte(&headersBuffer,c);
                pos ++;
                if (done == TRUE)
                {
                  req->headerReceive	= (char*) headersBuffer.buffer;
                  *(headersBuffer.position) = 0;
                  nContentLength = GetContentLength(req->headerReceive);
                  //TRACE("Content-Length=%d\n", nContentLength);
                }
              }
            }
            if (bRecvHeader == true && bRecvBody == false)
            {
              if (pos < rc)
              {
                MemBufferAddBuffer(&messageBuffer, (unsigned char*)&buffer[pos], rc-pos);
                *messageBuffer.position = 0;
                req->message = (char*) messageBuffer.buffer;
                req->messageLength = (messageBuffer.position - messageBuffer.buffer);
                recvlen = recvlen+rc-pos;
                if (recvlen >= nContentLength)
                {
                  closesocket(sock);
                  //TRACE("Recv Body len=%d\n", recvlen);
                  return 0;
                }
              }
            }
          }
        }
      }
      else
      {
        closesocket(sock);
        if (bRecvBody == true)
        {
          return 0;
        }
        else
        {
          return 13;
        }
      }
    }
    //-----------------------------------------------------------------------------
#else
    //-----------------------------------------------------------------------------
    //以下為阻塞方式接收
    //取接收的消息頭
    while(!done)
    {
        rc = recv(sock,buffer,1,0);
        if(rc<0)
            done=TRUE;

        switch(*buffer)
        {
            case '\r':
                break;
            case '\n':
                if(chars==0)
                    done = TRUE;
                chars=0;
                break;
            default:
                chars++;
                break;
        }

        MemBufferAddByte(&headersBuffer,*buffer);
    }

    req->headerReceive	= (char*) headersBuffer.buffer;
    *(headersBuffer.position) = 0;
    nContentLength = GetContentLength(req->headerReceive);
    TRACE("Content-Length=%d\n", nContentLength);
    
    //取接收的消息體
    do
    {
        rc = recv(sock,buffer,sizeof(buffer)-1,0);
        if(rc<0)
            break;
        *(buffer+rc)=0;
        MemBufferAddBuffer(&messageBuffer, (unsigned char*)&buffer, rc);
    } while(rc>0);
    *messageBuffer.position = 0;
    req->message = (char*) messageBuffer.buffer;
    req->messageLength = (messageBuffer.position - messageBuffer.buffer);
    if (req->messageLength < nContentLength)
    {
      TRACE("Recv HTTP body len error\n");
    }
    //-----------------------------------------------------------------------------
#endif

    //接收完成
    closesocket(sock);											// Cleanup
    TRACE("Total timer=%d\n",GetTickCount()-dwOldTimer);
    return 0;
}


//*******************************************************************************************************
// SendRequest
//
//*******************************************************************************************************
//void Request::SendRequest(bool IsPost, LPCSTR url, char *pszHeaderSend, char *pszHeaderReceive, char *pszMessage)
int Request::SendRequest(bool IsPost, LPCSTR url, LPCSTR cookies, CString &psHeaderSend, CString &psHeaderReceive, CString &psMessage, CString &psCookies)
{
	HTTPRequest	req;
  int					i,rtn;
  LPSTR				buffer;

  req.headerSend		= NULL;
	req.headerReceive	= NULL;
  req.message				= NULL;

  //Read in arguments

	if(IsPost)
  {													
    /* POST */
		i = psHeaderSend.GetLength();
		buffer  = (char*) malloc(i+1);
		strcpy(buffer, (LPCTSTR)psHeaderSend);

		rtn	= SendHTTP(	url,
									cookies,
                  "Content-Type: application/x-www-form-urlencoded\r\n",
									(unsigned char*)buffer,
									i,
									&req);
        
    free(buffer);
  }
  else
  {
    /* GET */
    rtn = SendHTTP(url,NULL,NULL,NULL,0,&req);
  }

	if(!rtn)											//Output message and/or headerSend 
	{
		psHeaderSend		= req.headerSend;
		psHeaderReceive		= req.headerReceive;
		psMessage			= req.message;

    //取cookies
    char szTemp[512], *p1, *p2;
    int len;
    
    p1 = strstr(req.headerReceive, "Set-Cookie: ");
    if (p1)
    {
      p2 = strstr(p1, "\r\n");
      if (p2)
      {
        p1 += 12;
        len = (int)(p2-p1);
        if (len > 0)
        {
          memset(szTemp, 0, 512);
          if (len >= 512)
            len = 511;
          strncpy(szTemp, p1, len);
          psCookies = szTemp;
        }
      }
    }

    if (req.headerSend)
    {
      free(req.headerSend);
      req.headerSend = NULL;
    }
    if (req.headerReceive)
    {
      free(req.headerReceive);
      req.headerReceive = NULL;
    }
    if (req.message)
    {
      free(req.message);
      req.message = NULL;
    }
    
    return 0;
  }
  else
	{
		//MessageBox(0, "Retrieve Failed", "", 0);
    if (req.headerSend)
    {
      free(req.headerSend);
      req.headerSend = NULL;
    }
    if (req.headerReceive)
    {
      free(req.headerReceive);
      req.headerReceive = NULL;
    }
    if (req.message)
    {
      free(req.message);
      req.message = NULL;
    }
    return 1;
	}
}
//0-表示返回成功
int Request::RequestSocket(LPCSTR host, int port, LPCSTR sendmsg, char *recvmsg)
{
    WSADATA			WsaData;
    SOCKADDR_IN	sin;
    SOCKET			sock;
    char			  buffer[512];
    MemBuffer   messageBuffer;
    int				  rc, err, TimeOut;
    DWORD       dwOldTimer = GetTickCount();
    unsigned long cmd;

    if (strlen(sendmsg) == 0)
        return 1;

    err = WSAStartup(0x0101, &WsaData);							// Init Winsock
    if (err!=0)
        return 1;

    sock = socket(AF_INET, SOCK_STREAM, 0);
	  if (sock == INVALID_SOCKET)
        return 2;
    
    TimeOut=6000;   //設置發送超時6秒   
    if (setsockopt(sock,SOL_SOCKET,SO_SNDTIMEO,(char *)&TimeOut,sizeof(TimeOut))==SOCKET_ERROR)
    {   
      closesocket(sock);
      return 3;   
    }   
    TimeOut=6000;//設置接收超時6秒   
    if (setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&TimeOut,sizeof(TimeOut))==SOCKET_ERROR)
    {   
      closesocket(sock);
      return 3;   
    }   
    
    //把socket設置成非阻塞方式
    cmd = 1;
    ioctlsocket(sock, FIONBIO, &cmd);
    
    sin.sin_family = AF_INET;										//Connect to web sever
    sin.sin_port = htons( (unsigned short)port );
    sin.sin_addr.s_addr = GetHostAddress(host);

    err = connect(sock, (LPSOCKADDR)&sin, sizeof(SOCKADDR_IN));

    //select   模型，
    struct timeval timeout;   
    fd_set r;   
    
    FD_ZERO(&r);   
    FD_SET(sock, &r);   
    timeout.tv_sec = 10;   //連接超時10秒   
    timeout.tv_usec = 0;   
    err = select(0, 0, &r, 0, &timeout); 
    if (err <= 0)   
    {   
      closesocket(sock);
      TRACE("connect fail timer=%d\n", GetTickCount()-dwOldTimer);
      return 4;   
    }   
    //再設回阻塞模式   
    cmd = 0;   
    err = ioctlsocket(sock, FIONBIO, (unsigned long*)&cmd);   
    if (err==SOCKET_ERROR)
    {   
      closesocket(sock);   
      return 5;   
    }

    //開始發送消息
    if (SendString(sock,sendmsg) == false)								// Send a blank line to signal end of HTTP headerReceive
      return 6;
    
    MemBufferCreate(&messageBuffer);

    //-----------------------------------------------------------------------------
    //以下為非阻塞方式接收
    bool bRecvHeader=false, bRecvTailer=false, bRecvBody=false;
    
    cmd = 1;
    err = ioctlsocket(sock, FIONBIO, (unsigned long*)&cmd);   
    if (err==SOCKET_ERROR)
    {   
      closesocket(sock);   
      return 7;   
    }

    //循環select(如果超時就退出)
    fd_set rset;
    struct timeval wtime;
    wtime.tv_sec = 0; //秒值
    wtime.tv_usec = 1000; //毫秒
    int pos=0, recvlen=0;

    //開始計時
    while (true)
    {
      if(GetTickCount()-dwOldTimer > 15000)
      {
        closesocket(sock);
        TRACE("Recv Timeout=%d\n", GetTickCount()-dwOldTimer);
        if (bRecvBody == true)
        {
          return 0;
        }
        else
        {
          return 8;
        }
      }
      FD_ZERO(&rset);
      FD_SET(sock, &rset);
      err = select(sock+1, &rset, NULL, NULL, &wtime);
      if(err == 0)	
      {
        //服務端響應超時就退出
        if(GetTickCount()-dwOldTimer > 10000)
        {
          closesocket(sock);
          TRACE("Recv Timeout=%d\n", GetTickCount()-dwOldTimer);
          if (bRecvBody == true)
          {
            return 0;
          }
          else
          {
            return 9;
          }
        }
        else
          continue;
      }
      else if (err > 0)
      {
        rc = recv(sock, buffer, 1, MSG_PEEK);
        if(rc<0) 
          err = GetLastError();
        
        if(rc<0 && err!=WSAEMSGSIZE)
        {
          closesocket(sock);
          if (bRecvBody == true)
          {
            return 0;
          }
          else
          {
            return 10;
          }
        }
        else if(rc == 0)
        {
          closesocket(sock);
          if (bRecvBody == true)
          {
            return 0;
          }
          else
          {
            return 11;
          }
        }
        else
        {
          //可接收
          rc = recv(sock,buffer,sizeof(buffer)-1,0);
          *(buffer+rc)=0;
          if (rc == SOCKET_ERROR)
          {
            closesocket(sock);
            if (bRecvBody == true)
            {
              return 0;
            }
            else
            {
              return 12;
            }
          }
          else if (rc > 0)
          {
            closesocket(sock);
            //TRACE("Recv msg=%s\n", buffer);
            strcpy(recvmsg, buffer);
            return 0;
          }
        }
      }
      else
      {
        closesocket(sock);
        if (bRecvBody == true)
        {
          return 0;
        }
        else
        {
          return 13;
        }
      }
    }
    //-----------------------------------------------------------------------------

    //接收完成
    closesocket(sock);											// Cleanup
    TRACE("Total timer=%d\n",GetTickCount()-dwOldTimer);
    return 0;
}
