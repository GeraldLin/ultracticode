//---------------------------------------------------------------------------
#include "stdafx.h"
#include "funcstr.h"
//---------------------------------------------------------------------------
//---------------才﹃ㄧ计--------------------------------------------------
//才﹃
US MyStrLen( const CH *vString )
{
    static US result;
    result = strlen( vString );
    return result;
}
//奔才﹃ㄢ繷
CH *MyTrim( const CH *vString )
{
		static char temp[MAX_VAR_DATA_LEN];
    int i,j;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	if(!isspace(vString[i]))
    		break;
    for(j=len-1;j>=0;j--)
    	if(!isspace(vString[j]))
    		break;
    len=j+1-i;		
    if(len>=0)
    {
    	memcpy(temp,vString+i,len);
    	temp[len]=0;
    }
    else
    	temp[0]=0;	
    return temp;			
}
//奔才﹃オ
CH *MyLeftTrim( const CH *vString )
{
		static char temp[MAX_VAR_DATA_LEN];
    int i;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	if(!isspace(vString[i]))
    		break;
    memcpy(temp,vString+i,len-i);
    temp[len-i]=0;
    return temp;			
}
//奔才﹃娩
CH *MyRightTrim( const CH *vString )
{
		static char temp[MAX_VAR_DATA_LEN];
    int i;
    int len=strlen(vString);
    for(i=len-1;i>=0;i--)
    	if(!isspace(vString[i]))
    		break;
    memcpy(temp,vString,i+1);
    temp[i+1]=0;
    	
    return temp;			
}
//眖オ才﹃
CH *MyLeft( const CH *vString, const US vLenght )
{
		static char temp[MAX_VAR_DATA_LEN];
		strcpy(temp,vString);
		temp[vLenght]=0;
		return temp;
}
//眖才﹃
CH *MyRight( const CH *vString, const US vLenght )
{
		static char temp[MAX_VAR_DATA_LEN];
	  US len=strlen(vString);
    US mlen=vLenght>len?len:vLenght;
	  	
    memcpy(temp,vString+(len-mlen),mlen);
    temp[mlen]=0;
    	
    return temp;			
}
//才﹃
CH *MySubString( const CH *vString, const US vStart, const US vLenght )
{
		static char temp[MAX_VAR_DATA_LEN];
	  US len=strlen(vString);
	  US mstart=vStart>=len?len-1:vStart;
    US mlen=vLenght>(len-mstart)?(len-mstart):vLenght;
	  	
    memcpy(temp,vString+mstart,mlen);
    temp[mlen]=0;
    	
    return temp;			
}
//才﹃
CH *MyStrAdd( const CH *vString1, const CH *vString2 )
{
		static char temp[MAX_VAR_DATA_LEN];
	  	
    strcpy(temp,vString1);
    strcat(temp,vString2);
    
    return temp;		
}
//才﹃糶
CH *MyLower( const CH *vString )
{
    static char temp[MAX_VAR_DATA_LEN];
 		int i;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	temp[i]=tolower(vString[i]);
    temp[len]=0;	
   	return temp;
}
//才﹃糶
CH *MyUpper( const CH *vString )
{
	 static char temp[MAX_VAR_DATA_LEN];
 		int i;
    int len=strlen(vString);
    for(i=0;i<len;i++)
    	temp[i]=toupper(vString[i]);
    temp[len]=0;	
    return temp;
}
//才﹃竚
US MyStrPos( const CH *vString1, const CH *vString2 )
{
    char *result;
    US nPos;
    result = strstr(vString1, vString2);
		if(result==0)
			return 0xffff;
    nPos = result-vString1;
    return nPos;
}
//ゑ耕才﹃
UC MyStrCmp( const CH *vString1, const CH *vString2 )
{
    UC result;
    if ( strcmp(vString1,vString2) == 0 )
    {
        result = 0;
    }
    else
    {
        result = 1;
    }
    return result;
}
//┛菠糶ゑ耕才﹃
UC MyStrCmpNoCase( const CH *vString1, const CH *vString2 )
{
  UC result;
#ifdef WIN32 //win32吏挂
  if ( strcmpi(vString1,vString2) == 0 )
#else //LINUX吏挂
    if ( strcasecmp(vString1,vString2) == 0 )
#endif
    {
      result = 0;
    }
    else
    {
      result = 1;
    }
    return result;
}
void ClearCharInCallerNo(char *phoneno)
{
  char callerno[64];
  int i, j=0, len;
  
  memset(callerno, 0, 64);
  len = strlen(phoneno);
  for (i = 0; i < len && i < 64; i ++)
  {
    if (phoneno[i] >= '0' && phoneno[i] <= '9')
    {
      callerno[j] = phoneno[i];
      j ++;
    }
  }
  strcpy(phoneno, callerno);
}
//だ瞒ゅセ︽(,)
int SplitTxtLine(const char *txtline, int maxnum, CStringX *arrstring)
{
  int len, i = 0, k = 0, num = 0;
  char ch, ch1, ch2 = 0, strtemp[64];

  memset(strtemp, 0, 64);
  len = strlen(txtline);
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    ch1 = ch2;
    ch2 = ch;
    if (ch1 == '/' && ch2 == '/')
    {
      //称猔夹в  
      if (k > 0)
      {
        strtemp[k] = '\0';
        arrstring[num] = strtemp;
        num ++;
      }
      break;
    }
    if (ch == ',' || ch == ';'   || ch == ' ' || ch == '\n' || ch == '\t' || ch == 0)
    {
      //丁筳夹в
      if (k == 0 && (ch != ',' && ch != ';'))
        continue;
      strtemp[k] = '\0';
      arrstring[num] = strtemp;  
      num ++;
      k = 0;
      if (num >= maxnum)
        break;
    }
    else
    {
      if (k < 64)
      {
        strtemp[k] = ch;
        k ++;
      }
    }
  }
  return num;
}
//沮だ瞒才だ澄才﹃
int SplitString(const char *txtline, const char splitchar, int maxnum, CStringX *arrstring)
{
  int len, i = 0, k = 0, num = 0;
  char ch, strtemp[64];

  memset(strtemp, 0, 64);
  len = strlen(txtline);
  for (i = 0; i <= len; i ++ )
  {
    ch = txtline[i];
    if (ch == splitchar || ch == 0)
    {
      //丁筳夹в
      strtemp[k] = '\0';
      arrstring[num] = strtemp;  
      num ++;
      k = 0;
      if (num >= maxnum)
        break;
    }
    else
    {
      if (k < 64)
      {
        strtemp[k] = ch;
        k ++;
      }
    }
  }
  return num;
}
//浪琩code 琌で皌Rule(珹硄皌才)
bool IsStrMatch(const CStringX &Rule,const CStringX &code)
{
	unsigned int pos1=0;
	unsigned int pos2=0;
	unsigned int len=code.GetLength();
	while((pos1<Rule.GetLength()) && (pos2<len))
	{
		if(Rule[pos1]!=code[pos2])
		{
			if(Rule[pos1]=='*') 
			{
				pos2++;
				continue;	
			}
			else if(Rule[pos1]!='?') 
				return false;				
		}	
		pos1++;
		pos2++;
	}
	if(pos2==len)
		if(pos1==Rule.GetLength()) 
			return true;
		else if(Rule[pos1]=='*')
			return true;	
	
	return false;	
}

int CheckIPv4Address(const char *pszIPAddr)
{
  //浪琩
  int nIPlen=strlen(pszIPAddr);
  
  if(nIPlen < 7 || nIPlen > 15)
  {
    return 1;
  }
  //浪琩翴だ筳才计
  int i, nDotCount=0;
  for(i=0;i<nIPlen;i++)
  {
    if (pszIPAddr[i] != '.' && (pszIPAddr[i] < '0' || pszIPAddr[i] > '9'))
      return 2;
    if(pszIPAddr[i]=='.')
      nDotCount++;
  }
  
  if(nDotCount != 3)
  {
    return 2;
  }
  char strIp[4][20];
  for(i=0;i<4;i++)
    memset(strIp[i],0,20);
  sscanf(pszIPAddr, "%20[^.].%20[^.].%20[^.].%20[^.]", strIp[0],strIp[1],strIp[2],strIp[3]);
  
  for(i=0;i<4;i++)
  {
    if (strlen(strIp[i]) == 0)
      return 3;
    if(atoi(strIp[i]) > 255 || atoi(strIp[i]) < 0)
    {
      return 4;
    }
  }
  return 0;
}
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult)
{
  int i=0, nTemp, nLen, nOldLen, nNewLen, nCopyedLen=0;
  bool bStartReplace=false;
  char *pszStart, *pszFinded;
  
  memset(pszResult, 0, nOutputMaxLen);

  nOldLen = strlen(pszOldStr);
  nNewLen = strlen(pszNewStr);
  if (nOldLen == 0)
  {
    memcpy(pszResult, pszSource, nOutputMaxLen-1);
  }
  else
  {
    pszStart = (char *)pszSource;
    
    do 
    {
      pszFinded = strstr(pszStart, pszOldStr);
      if (pszFinded)
      {
        //т惠璶蠢传才﹃
        if (i < nStartIndex)
        {
          //⊿Τ惠璶传竚ま玥钡ī赣琿才
          nLen = (int)(pszFinded-pszStart)+nOldLen;
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strncat(pszResult, pszStart, nLen);
            pszStart = pszStart+nLen;
            nCopyedLen = nTemp;
          }
          else
          {
            //禬筁﹚玥挡
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
            break;
          }
        }
        else
        {
          //惠璶传竚ま玥ī赣琿玡ぃで皌才﹃發穝蠢传才
          nLen = (int)(pszFinded-pszStart);
          if (nLen > 0)
          {
            //ī玡ぃで皌才﹃
            nTemp = nCopyedLen + nLen;
            if (nTemp < nOutputMaxLen)
            {
              strncat(pszResult, pszStart, nLen);
              pszStart = pszStart+nLen+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszStart, nLen);
              break;
            }
            //發穝蠢传才
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
          else
          {
            //玡⊿Τぃで皌才﹃钡發穝蠢传才
            nTemp = nCopyedLen + nNewLen;
            if (nTemp < nOutputMaxLen)
            {
              strcat(pszResult, pszNewStr);
              pszStart = pszStart+nOldLen;
              nCopyedLen = nTemp;
            }
            else
            {
              nLen = nOutputMaxLen-nCopyedLen-1;
              strncat(pszResult, pszNewStr, nLen);
              break;
            }
          }
        }
        i++;
        if (i>=nReplaceMaxNum)
        {
          //竒蠢传惠璶计ヘ
          nLen = strlen(pszStart);
          
          nTemp = nCopyedLen + nLen;
          if (nTemp < nOutputMaxLen)
          {
            strcat(pszResult, pszStart);
            nCopyedLen = nTemp;
          }
          else
          {
            nLen = nOutputMaxLen-nCopyedLen-1;
            strncat(pszResult, pszStart, nLen);
          }
          break;
        }
      } 
      else
      {
        //ゼт惠璶蠢传才﹃
        nLen = strlen(pszStart);
        nTemp = nCopyedLen + nLen;
        if (nTemp < nOutputMaxLen)
        {
          strcat(pszResult, pszStart);
          nCopyedLen = nTemp;
        }
        else
        {
          nLen = nOutputMaxLen-nCopyedLen-1;
          strncat(pszResult, pszStart, nLen);
        }
        break;
      }
    } while (true);
  }
}
bool GetArrayNameAndIndex(const char *pszSource, char *pszArrayName, int &nArrayIndex)
{
  int len;
  char *pszStart, *pszEnd, szTemp[16];

  pszStart = strchr(pszSource, '[');
  if (!pszStart)
  {
    return false;
  }
  pszEnd = strchr(pszSource, ']');
  if (!pszEnd)
  {
    return false;
  }
  if (pszStart >= pszEnd)
  {
    return false;
  }
  len = (int)(pszStart-pszSource);
  if (len >= 256)
    len = 255;
  strncpy(pszArrayName, pszSource, len);
  len = (int)(pszEnd-pszStart)-1;
  memset(szTemp, 0, 16);
  pszStart++;
  if (len > 0)
    strncpy(szTemp, pszStart, len);
  if (stricmp(szTemp, "auto") == 0)
    nArrayIndex = -1;
  else
    nArrayIndex = atoi(szTemp);
  return true;
}

bool ParserFilter(const char *pszSource, char *pszFilterFieldName, char *pszFilterCondValue, int &nFilterType)
{
  int len;
  char *pszStart;

  nFilterType = 0;

  pszStart = strstr(pszSource, "<>");
  if (pszStart)
  {
    nFilterType = 6;
    len = (int)(pszStart-pszSource);
    if (len == 0)
    {
      return false;
    }
    if (len >= 128)
      len = 127;
    strncpy(pszFilterFieldName, pszSource, len);
    pszStart += 2;
    strncpy(pszFilterCondValue, pszStart, 127);
    return true;
  }

  pszStart = strstr(pszSource, ">=");
  if (pszStart)
  {
    nFilterType = 5;
    len = (int)(pszStart-pszSource);
    if (len == 0)
    {
      return false;
    }
    if (len >= 128)
      len = 127;
    strncpy(pszFilterFieldName, pszSource, len);
    pszStart += 2;
    strncpy(pszFilterCondValue, pszStart, 127);
    return true;
  }

  pszStart = strstr(pszSource, "<=");
  if (pszStart)
  {
    nFilterType = 4;
    len = (int)(pszStart-pszSource);
    if (len == 0)
    {
      return false;
    }
    if (len >= 128)
      len = 127;
    strncpy(pszFilterFieldName, pszSource, len);
    pszStart += 2;
    strncpy(pszFilterCondValue, pszStart, 127);
    return true;
  }

  pszStart = strchr(pszSource, '>');
  if (pszStart)
  {
    nFilterType = 3;
    len = (int)(pszStart-pszSource);
    if (len == 0)
    {
      return false;
    }
    if (len >= 128)
      len = 127;
    strncpy(pszFilterFieldName, pszSource, len);
    pszStart += 1;
    strncpy(pszFilterCondValue, pszStart, 127);
    return true;
  }

  pszStart = strchr(pszSource, '<');
  if (pszStart)
  {
    nFilterType = 2;
    len = (int)(pszStart-pszSource);
    if (len == 0)
    {
      return false;
    }
    if (len >= 128)
      len = 127;
    strncpy(pszFilterFieldName, pszSource, len);
    pszStart += 1;
    strncpy(pszFilterCondValue, pszStart, 127);
    return true;
  }

  pszStart = strchr(pszSource, '=');
  if (pszStart)
  {
    nFilterType = 1;
    len = (int)(pszStart-pszSource);
    if (len == 0)
    {
      return false;
    }
    if (len >= 128)
      len = 127;
    strncpy(pszFilterFieldName, pszSource, len);
    pszStart += 1;
    strncpy(pszFilterCondValue, pszStart, 127);
    return true;
  }

  return false;
}
//---------------------------------------------------------------------------
