//---------------------------------------------------------------------------
#ifndef FlwRuleH
#define FlwRuleH
//---------------------------------------------------------------------------
//消息方向說明：

//發送消息結構：(客戶端 >> 服務器) VXML_SEND_MSG_ATTR_RULE_STRUCT  [upload]

//接收消息結構：(客戶端 << 服務器) VXML_RECV_MSG_ATTR_RULE_STRUCT  [download]

//注：但LOG服務器收發均采用發送消息結構
//---------------------------------------------------------------------------
//流程指令規則類
class CFlwRule
{

public:
	CFlwRule();
	virtual ~CFlwRule();

  //整數范圍規則
  const VXML_INTEGER_RANGE_RULE_STRUCT      *IntRange;
  //單字符范圍規則
  const VXML_CHAR_RANGE_RULE_STRUCT         *CharRange;
  //字符串中允許出現的字符規則
  const VXML_STRING_RANGE_RULE_STRUCT       *StrRange;
  //整數枚舉字符串規則
  const VXML_MACRO_INT_RANGE_RULE_STRUCT    *MacroIntRange;
  //枚舉字符串規則
  const VXML_MACRO_STR_RANGE_RULE_STRUCT    *MacroStrRange;

  //DB返回消息規則
  const VXML_RECV_MSG_CMD_RULE_STRUCT       *DBXMLMsgRule;
  
  //parser發送的消息規則
  const VXML_SEND_MSG_CMD_RULE_STRUCT				*XMLDBMsgRule;
};
//---------------------------------------------------------------------------
#endif
