//---------------------------------------------------------------------------
#ifndef ProcFLWH
#define ProcFLWH
//---------------------------------------------------------------------------

void SendMsg2FLW(int nLOG, US MsgId, const CH *msg);
void SendMsg2FLW(int nLOG, US MsgId, const CStringX &msg);

void Set_WEB2FLW_Header(int MsgId);
void Set_WEB2FLW_Header(int MsgId, long SessionId, long CmdAddr);
void Set_WEB2FLW_Header(int MsgId, long SessionId, long CmdAddr, short ChanType, short ChanNo);
void Set_WEB2FLW_StrItem(int MsgId, int AttrId, const char *value);
void Set_WEB2FLW_StrItem(int MsgId, int AttrId, const CStringX &value);
void Set_WEB2FLW_IntItem(int MsgId, int AttrId, int value);
void Set_WEB2FLW_LongItem(int MsgId, int AttrId, long value);
void Set_WEB2FLW_BoolItem(int MsgId, int AttrId, bool value);
void Set_WEB2FLW_Tail();

void Send_querywebservice_Result(CQWebServiceSession *pWebServiceSession, const char* pszXMLString, int nResult, const char* pszErrorBuf);
void Send_querywebservice_Result(CQWebServiceSession *pWebServiceSession, int nNodeCount, const char* pszAttrValue, int nResult, const char* pszErrorBuf);

int Proc_Msg_From_FLW(CXMLMsg &FLWMsg);
int Proc_FLWMSG_gwquery(CXMLMsg &FLWMsg);
int Proc_FLWMSG_querywebservice(CXMLMsg &FLWMsg);
int Proc_FLWMSG_getwebservicexml(CXMLMsg &FLWMsg);
int Proc_FLWMSG_getwebservicestr(CXMLMsg &FLWMsg);
int Proc_FLWMSG_closewebservice(CXMLMsg &FLWMsg);
int Proc_FLWMSG_getwebserviceindex(CXMLMsg &FLWMsg);
int Proc_FLWMSG_httprequest(CXMLMsg &FLWMsg);
int Proc_FLWMSG_shellexecute(CXMLMsg &FLWMsg);
int Proc_FLWMSG_gettxtlinestr(CXMLMsg &FLWMsg);
int Proc_FLWMSG_querycustomer(CXMLMsg &FLWMsg);
int Proc_FLWMSG_httprequestex(CXMLMsg &FLWMsg);
int Proc_FLWMSG_getwebservicejson(CXMLMsg &FLWMsg);

void ProcFLWMsg(CXMLMsg &LOGMsg);
void SendQueryWebResult();

void MyGetMarkerString(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, int nIndex, int nMaxLen, char *pszResult, bool &bFinded);
int  MyGetMarkerStringIndex(const char *pszSource, const char *pszStartMarker, const char *pszEndMarker, const char *pszString);

int ConvUniStr2Unicode(LPCSTR szUnicodeString, WCHAR *pWchar, int iBuffSize);
int ustr_astr( WCHAR *unicodestr, char *ansistr );
//---------------------------------------------------------------------------
#endif
