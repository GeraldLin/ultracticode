////////////////////////////////////////////////////////////////////////
//
// odsError.h
//
//		ODS 有關錯誤處理的定義文件
//
//
////////////////////////////////////////////////////////////////////////


#if !defined(_ODS_20_ERROR_H_)
#define _ODS_20_ERROR_H_


#define LEN_ERR_MSG			200

////////////////////////////////////////////////////////////////////////
// 結構定義

// 錯誤信息結構
typedef struct tabSTODSERROR
{
	DWORD nCode;									// 錯誤代碼
	TCHAR strMsg[LEN_ERR_MSG + 1];					// 錯誤描述
}STODSERROR;

////////////////////////////////////////////////////////////////////////
// 宏定義

// 錯誤信息和錯誤代碼
#include "odsErrorCode.h"

// 其他宏定義

#define FORMAT_WITH_SYSTEM					1
#define FORMAT_WITH_STRING					2
#define FORMAT_WITH_SYSTEM_AND_STRING		( FORMAT_WITH_SYSTEM | FORMAT_WITH_STRING )

////////////////////////////////////////////////////////////////////////
// 函數定義

//-------------------------------------------------------------
//	用缺省的語言取得系統錯誤碼對應的錯誤信息( 格式 1 )
//-------------------------------------------------------------
void odsGetSysErrMsg( DWORD dwErrCode, CString& strMsg );

//-------------------------------------------------------------
//	用缺省的語言取得系統錯誤碼對應的錯誤信息( 格式 1 )
//-------------------------------------------------------------
void odsGetSysErrMsg( DWORD dwErrCode, LPTSTR strBuf, int lenMsg );

//-------------------------------------------------------------
// 在錯誤信息中填充錯誤代碼和錯誤信息，strMsg為NULL時只填充錯誤代碼
//-------------------------------------------------------------
void odsMakeError( STODSERROR* pstError, DWORD nCode, LPCTSTR strMsg );

//-------------------------------------------------------------
// 按指定的方式，用錯誤信息來填充錯誤信息結構
//
// uFlag 可以取的值有：
//		FORMAT_WITH_SYSTEM						- 取系統錯誤信息
//												- ( pstError->nCode 將被置換成系統錯誤代碼 )
//		FORMAT_WITH_STRING						- 自己生成錯誤信息
//												- ( pstError->nCode 不變 )
//		FORMAT_WITH_SYSTEM_AND_STRING			- 將系統錯誤信息和自己生成的錯誤信息結合起來
//												- ( pstError->nCode 將被置換成系統錯誤代碼 )
// 參數 lpszFormat, ... 的定義同 printf 中的格式串定義
//-------------------------------------------------------------
void odsFormatError( STODSERROR* pstError, UINT uFlag, LPCTSTR lpszFormat, ...);

#endif // _ODS_20_ERROR_H_