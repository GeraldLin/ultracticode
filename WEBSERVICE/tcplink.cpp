// TCPServer.cpp: implementation of the CTCPServer class.
//
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------
#include "stdafx.h"
#include "tcplink.h"
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);

CTCPLink::CTCPLink()
{
	IsOpen = false;
  isWEBInit = false;
	hdll = NULL;
  strcpy(DllFile, "tmtcpserver.dll");
  WEBServerId = 0x1002;
  WEBServerPort = 5001;
  LoadDll();
}

CTCPLink::~CTCPLink()
{
  ReleaseDll();
}

bool CTCPLink::LoadDll()
{
	hdll = LoadLibrary(DllFile);
	if ( hdll != NULL )
	{
    AppInitS     = (APPINITS)GetProcAddress( hdll, "AppInitS" );
    AppInit     = (APPINIT)GetProcAddress( hdll, "AppInit" );
    AppCloseAll = (APPCLOSEALL)GetProcAddress( hdll, "AppCloseAll" );
    AppMainLoop = (APPMAINLOOP)GetProcAddress( hdll, "AppMainLoop" );
    AppSendData = (APPSENDDATA)GetProcAddress( hdll, "AppSendData" );

    if(  !AppInit || !AppCloseAll || !AppMainLoop || !AppSendData)
    {
      MyTrace(0, "GetProcAddress tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		  return false;
    }
    IsOpen=true;
		return true;
	}
	else
	{
		MyTrace(0, "LoadLibrary tcpdll eror","Warning",MB_OK|MB_ICONWARNING);
		return false;
	}
}

void CTCPLink::ReleaseDll()
{
  if ( hdll != NULL )
  {
    AppCloseAll();
	  FreeLibrary( hdll );
  }
}

bool CTCPLink::ReadIni(char *filename)
{
  isWEBInit = false;
  WEBServerId = CombineID(NODE_DB,GetPrivateProfileInt("WEBSERVICE", "ServerId", 2, filename));
  WEBServerPort = GetPrivateProfileInt("WEBSERVICE", "ServerPort", 5001, filename);
  return true;
}    

//IVR�o�e������Ȥ��
int CTCPLink::SendMessage2CLIENT(US Clientid, US MsgId, const CH *MsgBuf)
{
	if (Clientid == 0) return 0;
	int SendedLen, MsgLen;
  MsgLen = strlen(MsgBuf);
  SendedLen = AppSendData(Clientid, MsgId, (UC *)MsgBuf , MsgLen);
  //printf("Clientid=%04x MsgId=%04x MsgBuf=%s\n", Clientid, MsgId, MsgBuf);
  if (SendedLen != MsgLen)
  {
    MyTrace(0, "WEBSERVICE send msg error Clientid=%04x MsgId=%d MsgBuf=%s", Clientid, MsgId, MsgBuf);
  }
	return SendedLen;
}
int CTCPLink::SendMessage2CLIENT(US Clientid, US MsgId, const UC *MsgBuf, int len)
{
	if (Clientid == 0) return 0;
	int SendedLen;
  SendedLen = AppSendData(Clientid, MsgId, MsgBuf , len);
  if (SendedLen != len)
  {
    MyTrace(0, "WEBSERVICE send msg error Clientid=%04x MsgId=%d\n", Clientid, MsgId);
  }
	return SendedLen;
}

int CTCPLink::InitWEBServer()
{
  if (isWEBInit == true)
    return 2;
  if (IsOpen == true)
  {
    AppInitS(WEBServerId, WEBServerPort, OnWEBLogin, OnWEBClose, OnWEBReceiveData);
    isWEBInit = true;
    return 0;
  }
  else
  {
    return 1;
  }
}
