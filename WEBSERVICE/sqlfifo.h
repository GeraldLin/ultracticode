//---------------------------------------------------------------------------
#ifndef __RSQLFIFO_H_
#define __RSQLFIFO_H_
//---------------------------------------------------------------------------
#include <afxmt.h>
//---------------------------------------------------------------------------

typedef struct
{
  unsigned short SqlLen;
  char SqlBuf[4096];

}VXML_SQLS_STRUCT;

class CSQLSfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_SQLS_STRUCT          *pSqls;
  
  CCriticalSection m_cCriticalSection;
  
public:
  CSQLSfifo()
  {
    Head=0;
    Tail=0;
    resv=0;
    Len=512;
    pSqls=new VXML_SQLS_STRUCT[Len];
  }
  
  ~CSQLSfifo()
  {
    if (pSqls != NULL)
    {
      delete []pSqls;
      pSqls = NULL;
    }
  }
  
  bool	IsFull()
  {
    unsigned short temp=Tail+1;
    return temp==Len?Head==0:temp==Head;
  }
  bool 	IsEmpty()
  {
    bool bResult;
    
    m_cCriticalSection.Lock();
    Head==Tail ? bResult=true : bResult=false;
    m_cCriticalSection.Unlock();
    
    return bResult;
  }
  VXML_SQLS_STRUCT &GetOnHead()
  {return pSqls[Head];}
  
  void DiscardHead()
  {	
    if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(const char *SqlBuf, unsigned short SqlLen);
  unsigned short  Read(char *SqlBuf, unsigned short &SqlLen);
};

//---------------------------------------------------------------------------
#endif
