//---------------------------------------------------------------------------
#ifndef ExternH
#define ExternH
//---------------------------------------------------------------------------
extern CTCPLink  *pTcpLink; //通信消息類
extern CFlwRule  *pFlwRule; //流程規則類
extern CMsgfifo  *RMsgFifo; //XML消息緩沖隊列
extern CSQLSfifo SQLSfifo;
extern CQWebServiceSessionMng *g_pWebServiceSessionMng;

extern CXMLMsg   WEBRcvMsg;
extern CXMLMsg   WEBSndMsg;

//---------------------------------------------------------------------------
extern int g_nRunMode;
extern int g_nWebServiceOutTime;

extern bool g_bRunId; //運行標志
extern bool g_bSaveId; //保存消息
extern bool g_bTestId; //測試標志

extern char g_szServiceINIFileName[MAX_PATH_FILE_LEN]; //配置文件
extern char g_szDBINIFileName[MAX_PATH_FILE_LEN]; //db配置文件
extern char g_szConnectionString[256];

extern char g_szRootPath[MAX_PATH_LEN]; //平臺安裝根路徑
extern char g_szAppPath[MAX_PATH_LEN]; //執行文件路徑
extern char g_szLogPath[MAX_PATH_LEN]; //日志文件路徑

extern CCriticalSection g_cLogCriticalSection;
extern volatile int g_nExistThread;
extern CWinThread*	g_hDbThrdProc;

extern int g_nConnectDbState; //0-未連接數據庫，1-連接成功，2-連接失敗
extern CADODatabase  g_pDbConnect;

extern CWebServiceParam g_WebServiceParam;
extern CCustomerField g_CustomerField;

extern bool g_bHTTPUnicodeDecode;

//---------------------------------------------------------------------------
extern void CheckMsgFifo();
extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
extern void MyWriteFile(LPCTSTR msg);

//---------------------------------------------------------------------------
#endif
