//---------------------------------------------------------------------------
#include "stdafx.h"
#include "procwebservice.h"
#include "extern.h"
//---------------------------------------------------------------------------
HMODULE									g_hAgentDll				= NULL;	

//數據庫模塊導出函數
_FagentSetAgentConfig	*_agentSetAgentConfig	= NULL;	
_FagentCallWebService	*_agentCallWebService	= NULL;	
_FagentFreeMemory		  *_agentFreeMemory	= NULL;

//************************************************************************
//				Load dynamic link library <QuarkCallWebServiceAgent.dll>
//************************************************************************
bool Fun_LoadAgentDLL()
{
  //#ifdef QUARKCALL_PLATFORM
  char sDllFile[] = "QuarkCallWebServiceAgent.dll";
  //#else
  //char sDllFile[] = "CTICallWebService.dll";
  //#endif
  
  g_hAgentDll = LoadLibrary(sDllFile);
  if (g_hAgentDll == NULL) 
  {	
    return false;
  }
  
  _agentSetAgentConfig = (_FagentSetAgentConfig *)GetProcAddress(g_hAgentDll, "agentSetAgentConfig");
  if (_agentSetAgentConfig == NULL)
  {
    goto ERR_DOCDB_FLAG;
  }
  
  _agentCallWebService = (_FagentCallWebService *)GetProcAddress(g_hAgentDll, "agentCallWebService");
  if (_agentCallWebService == NULL) 
  {
    goto ERR_DOCDB_FLAG;
  } 
  
  _agentFreeMemory = (_FagentFreeMemory *)GetProcAddress(g_hAgentDll, "agentFreeMemory");
  if (_agentFreeMemory == NULL) 
  {
    goto ERR_DOCDB_FLAG;
  } 

  if (g_bSaveId == true)
  {
    _agentSetAgentConfig(g_nWebServiceOutTime, true);
  }

  return true;
  
ERR_DOCDB_FLAG:
  Fun_FreeAgentDLL();
  
  return false;
}

//************************************************************************
//				Free dynamic link library <QuarkCallWebServiceAgent.dll>
//************************************************************************
bool Fun_FreeAgentDLL()
{
  if (g_hAgentDll != NULL)
  {
    FreeLibrary(g_hAgentDll);
    
    g_hAgentDll					= NULL;
    _agentSetAgentConfig		= NULL;
    _agentCallWebService		= NULL;
  }
  return true;
}
