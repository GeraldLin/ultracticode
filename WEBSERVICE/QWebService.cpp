// QWebService.cpp: implementation of the CQWebService class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "QWebService.h"
#include "Request.h"
#include "GenericHTTPClient.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern bool g_bHTTPUnicodeDecode;
extern _FagentCallWebService	*_agentCallWebService;
extern _FagentFreeMemory		  *_agentFreeMemory;	
extern void MyWriteFile(LPCTSTR msg);

UINT WebServiceProc(LPVOID pParam)
{
  CQWebServiceSession * pWebServiceSession =  (CQWebServiceSession *)pParam;
  char szRetXMLString1[16384], szRetXMLString2[16384];
  char *pReturn	= NULL;
  char *pError	= NULL;

  if (_agentCallWebService((char *)pWebServiceSession->m_WebServiceData.m_strWSDLAddress.operator LPCTSTR(),
    (char *)pWebServiceSession->m_WebServiceData.m_strSoapAction.operator LPCTSTR(),
    (char *)pWebServiceSession->m_WebServiceData.m_strStartEnvelopeString.operator LPCTSTR(),
    (char *)pWebServiceSession->m_WebServiceData.m_strStartBody.operator LPCTSTR(),
    (char *)pWebServiceSession->m_WebServiceData.m_strInterfaceString.operator LPCTSTR(),
    pWebServiceSession->m_WebServiceData.m_nParamCount,
    (char *)pWebServiceSession->m_WebServiceData.m_strParamString.operator LPCTSTR(),
    &pReturn,
    &pError
    ) == 1)
  {
    pWebServiceSession->m_csQuery.Lock();
    if (pWebServiceSession->state == 1 && pWebServiceSession->m_WebServiceData.m_nQueryResult == 2)
    {
      MyStringReplace(pReturn, "&lt;", "<", 0, 65535, 16384, szRetXMLString1);
      MyStringReplace(szRetXMLString1, "&gt;", ">", 0, 65535, 16384, szRetXMLString2);
      
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szRetXMLString2);
      MyWriteFile((LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString);
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
    }
    else if (pWebServiceSession->state == 1 && pWebServiceSession->m_WebServiceData.m_nQueryResult == 5) //2014-01-07
    {
      //提前取消查詢了
      pWebServiceSession->Init();
      pWebServiceSession->m_WebServiceData.m_nTestIndex = 0;
    }
    pWebServiceSession->m_csQuery.Unlock();
  }
  else
  {
    pWebServiceSession->m_csQuery.Lock();
    if (pWebServiceSession->state == 1 && pWebServiceSession->m_WebServiceData.m_nQueryResult == 2)
    {
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3; //查詢失敗
    }
    else if (pWebServiceSession->state == 1 && pWebServiceSession->m_WebServiceData.m_nQueryResult == 5) //2014-01-07
    {
      //提前取消查詢了
      pWebServiceSession->Init();
      pWebServiceSession->m_WebServiceData.m_nTestIndex = 0;
    }
    pWebServiceSession->m_csQuery.Unlock();
  }
  _agentFreeMemory(pReturn, pError);

  return 0;
}

UINT HTTPRequestProc(LPVOID pParam)
{
  int len;
  TCHAR szTemp1[16384];
  TCHAR szTemp2[16384];
  WCHAR pwbBuffer[16384];

  CQWebServiceSession * pWebServiceSession =  (CQWebServiceSession *)pParam;

  if (pWebServiceSession->m_WebServiceData.m_nHTTPType == 1) //2016-06-25
  {
    GenericHTTPClient httpClient;
    if(httpClient.Request((LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPurl))
    {
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString = httpClient.QueryHTTPResponse();
      pWebServiceSession->m_csQuery.Lock();
      
      if (g_bHTTPUnicodeDecode == true)
      {
        memset(szTemp1, 0, 16384);
        memset(szTemp2, 0, 16384);
        memset(pwbBuffer, 0, sizeof(pwbBuffer));
        
        strncpy(szTemp1, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString, 16383);
        len = 16383;
        if (ConvUniStr2Unicode(szTemp1, pwbBuffer, len) > 0)
        {
          ustr_astr(pwbBuffer, szTemp2);
          memset(szTemp1, 0, 16384);
          MyStringReplace(szTemp2, "\\/", "/", 0, 65535, 16384, szTemp1);
          pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTemp1);
        }
        MyWriteFile((LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString);
      }
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_csQuery.Unlock();
    }
    else
    {
      pWebServiceSession->m_csQuery.Lock();
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3;
      pWebServiceSession->m_csQuery.Unlock();
    }
  }
  else
  {
    Request	myRequest;

    if (myRequest.SendRequest(true, 
      (LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPurl, 
      (LPCSTR)pWebServiceSession->m_WebServiceData.m_strHTTPcookies,
      pWebServiceSession->m_WebServiceData.m_strHTTPheader, 
      pWebServiceSession->m_WebServiceData.m_strErrorMsg, 
      pWebServiceSession->m_WebServiceData.m_strReturnXmlString,
      pWebServiceSession->m_WebServiceData.m_strReturnCookiesString) == 0)
    {
      pWebServiceSession->m_csQuery.Lock();

      if (g_bHTTPUnicodeDecode == true)
      {
        memset(szTemp1, 0, 16384);
        memset(szTemp2, 0, 16384);
        memset(pwbBuffer, 0, sizeof(pwbBuffer));
      
        strncpy(szTemp1, (LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString, 16383);
        len = 16383;
        if (ConvUniStr2Unicode(szTemp1, pwbBuffer, len) > 0)
        {
          ustr_astr(pwbBuffer, szTemp2);
          memset(szTemp1, 0, 16384);
          MyStringReplace(szTemp2, "\\/", "/", 0, 65535, 16384, szTemp1);
          pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTemp1);
        }
        MyWriteFile((LPCSTR)pWebServiceSession->m_WebServiceData.m_strReturnXmlString);
      }
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
      pWebServiceSession->m_csQuery.Unlock();
    }
    else
    {
      pWebServiceSession->m_csQuery.Lock();
      pWebServiceSession->m_WebServiceData.m_nQueryResult = 3;
      pWebServiceSession->m_csQuery.Unlock();
    }
  }

  return 0;
}

DWORD WinExecAndWait32(LPCTSTR lpszAppPath,   // 執行程序的路徑
                       LPCTSTR lpParameters,  // 參數
                       LPCTSTR lpszDirectory, // 執行環境目錄
                       DWORD dwMilliseconds)  // 最大等待時間, 超過這個時間強行終止
{
  SHELLEXECUTEINFO ShExecInfo = {0};
  ShExecInfo.cbSize	= sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask	= SEE_MASK_NOCLOSEPROCESS;
  ShExecInfo.hwnd		= NULL;
  ShExecInfo.lpVerb	= NULL;
  ShExecInfo.lpFile	= lpszAppPath;		
  ShExecInfo.lpParameters = lpParameters;	
  ShExecInfo.lpDirectory	= lpszDirectory;
  ShExecInfo.nShow	= SW_HIDE;
  ShExecInfo.hInstApp = NULL;	
  ShellExecuteEx(&ShExecInfo);
  
  // 指定時間沒結束
  if (WaitForSingleObject(ShExecInfo.hProcess, dwMilliseconds) == WAIT_TIMEOUT)
  {	// 強行殺死進程
    TerminateProcess(ShExecInfo.hProcess, 0);
    return 1;	//強行終止
  }
  
  //DWORD dwExitCode;
  //BOOL bOK = GetExitCodeProcess(ShExecInfo.hProcess, &dwExitCode);
  //ASSERT(bOK);
  
  return 0;
}

UINT EXECRequestProc(LPVOID pParam)
{
  CQWebServiceSession * pWebServiceSession =  (CQWebServiceSession *)pParam;
  
  DWORD dwExitCode = -1;

  dwExitCode = WinExecAndWait32((LPCTSTR)pWebServiceSession->m_WebServiceData.m_strEXECCMDName,
                                (LPCTSTR)pWebServiceSession->m_WebServiceData.m_strEXECCMDParam,
                                NULL, pWebServiceSession->m_WebServiceData.m_nEXECTimeOut);
  if (dwExitCode == 0)
  {
    pWebServiceSession->m_csQuery.Lock();
    FILE *fp1;
    fp1 = fopen(pWebServiceSession->m_WebServiceData.m_strEXECReadFile, "r");
    if (fp1 != NULL)
    {
      char szTemp[256];
      if (fgets(szTemp, 255, fp1) > 0)
      {
        int len = strlen(szTemp);
        if (len > 0)
          szTemp[len-1] = '\0';
        pWebServiceSession->m_WebServiceData.m_strReturnXmlString.Format("%s", szTemp);
      }
      fclose(fp1);
    }
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 4; //查詢成功
    pWebServiceSession->m_csQuery.Unlock();
  }
  else
  {
    pWebServiceSession->m_csQuery.Lock();
    pWebServiceSession->m_WebServiceData.m_nQueryResult = 3;
    pWebServiceSession->m_csQuery.Unlock();
  }
  
  return 0;
}

void CQWebServiceData::Init()
{
  m_nQueryResult = 0;
  m_strWSDLAddress = _T("");
  m_strSoapAction = _T("");
  m_strStartEnvelopeString = _T("");
  m_strStartBody = _T("");
  m_strInterfaceString = _T("");
  m_nParamCount = 0;
  m_strParamString = _T("");
  m_strReturnXmlString = _T("");
  m_strReturnCookiesString = _T("");
  m_strErrorMsg = _T("");
  m_nHTTPType = 2;
  m_strHTTPurl = _T("");
  m_strHTTPheader = _T("");
  m_strHTTPcookies = _T("");
  m_strEXECCMDName = _T("");
  m_strEXECCMDParam = _T("");
  m_strEXECReadFile = _T("");
  m_strCallerNo = _T("");
  m_strCalledNo = _T("");
  m_nEXECTimeOut = 5000;
  m_bSendQueryResult = false;
  m_nCurIndex = 0;
  m_nQueryType = 0;
  m_nTestIndex = 0;
}

CQWebServiceData::CQWebServiceData()
{
  Init();
  m_nTestIndex = 0;
}

CQWebServiceData::~CQWebServiceData()
{

}

//-----------------------------------------------------------------------------
void CQWebServiceSession::Init()
{
  state = 0;
  timer = GetTickCount();
  
  m_ulSessionId = 0;
  m_usClientId = 0;
  m_ulCmdAddr = 0;
  m_usDeviceNo = 0;
  m_usChnType = 0;
  m_usChnNo = 0;

  m_WebServiceData.Init();

  m_hThrdProc = NULL;
}

CQWebServiceSession::CQWebServiceSession()
{
  Init();
}

CQWebServiceSession::~CQWebServiceSession()
{
  
}

int CQWebServiceSession::StartWebThreadProc()
{
  CWinThread*	hThrdProc = AfxBeginThread(WebServiceProc, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
  if(!hThrdProc)
    return -1; //創建線程失敗

  hThrdProc->m_bAutoDelete = true;
  hThrdProc->ResumeThread();
  return 0;
}
int CQWebServiceSession::StartHTTPThreadProc()
{
  CWinThread*	hThrdProc = AfxBeginThread(HTTPRequestProc, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
  if(!hThrdProc)
    return -1; //創建線程失敗
  
  hThrdProc->m_bAutoDelete = true;
  hThrdProc->ResumeThread();
  return 0;
}
int CQWebServiceSession::StartEXECThreadProc()
{
  CWinThread*	hThrdProc = AfxBeginThread(EXECRequestProc, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
  if(!hThrdProc)
    return -1; //創建線程失敗
  
  hThrdProc->m_bAutoDelete = true;
  hThrdProc->ResumeThread();
  return 0;
}
//-----------------------------------------------------------------------------
void CQWebServiceSessionMng::Init(short nSessionNum)
{
  if (m_pWebServiceSession)
  {
    delete []m_pWebServiceSession;
    m_pWebServiceSession = NULL;
  }
  m_pWebServiceSession = new CQWebServiceSession[nSessionNum];
  if (m_pWebServiceSession)
  {
    m_nSessionNum = nSessionNum;
  }
}

CQWebServiceSessionMng::CQWebServiceSessionMng()
{
  m_nSessionNum = 0;
  m_pWebServiceSession = NULL;
}

CQWebServiceSessionMng::~CQWebServiceSessionMng()
{
  if (m_pWebServiceSession)
  {
    delete []m_pWebServiceSession;
    m_pWebServiceSession = NULL;
  }
  m_nSessionNum = 0;
}

short CQWebServiceSessionMng::GetIdelSession(unsigned long  SessionId)
{
  short nResult = -1;
  for (short i = 0; i < m_nSessionNum; i ++)
  {
    if (m_pWebServiceSession[i].m_ulSessionId == SessionId)
    {
      m_pWebServiceSession[i].Init();
      nResult = i;
      break;
    }
  }
  if (nResult < 0 || nResult >= m_nSessionNum)
  {
    for (int i = 0; i < m_nSessionNum; i ++)
    {
      if (m_pWebServiceSession[i].state == 0)
      {
        nResult = i;
        break;
      }
    }
  }
  return nResult;
}

short CQWebServiceSessionMng::GetSession(unsigned long  SessionId)
{
  short nResult = -1;
  for (short i = 0; i < m_nSessionNum; i ++)
  {
    if (m_pWebServiceSession[i].m_ulSessionId == SessionId)
    {
      nResult = i;
      break;
    }
  }
  return nResult;
}