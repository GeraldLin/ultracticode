#ifndef __MAIN_H__
#define __MAIN_H__

//---------------------------------------------------------------------------
//所有的類指針
CTCPLink  *pTcpLink=NULL; //通信消息類
CFlwRule  *pFlwRule=NULL; //流程規則類
CMsgfifo  *RMsgFifo=NULL; //XML消息緩沖隊列
CSQLSfifo SQLSfifo;
CQWebServiceSessionMng *g_pWebServiceSessionMng=NULL;

CXMLMsg    WEBRcvMsg;
CXMLMsg    WEBSndMsg;

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
int g_nRunMode=0; //運行模式：0-單線程 1-多線程
int g_nWebServiceOutTime=60;

bool g_bRunId = false; //運行標志
bool g_bSaveId = false; //保存消息
bool g_bTestId = false; //測試標志

char g_szServiceINIFileName[MAX_PATH_FILE_LEN]; //配置文件
char g_szDBINIFileName[MAX_PATH_FILE_LEN]; //db配置文件
char g_szConnectionString[256];

char g_szRootPath[MAX_PATH_LEN]; //平臺安裝根路徑
char g_szAppPath[MAX_PATH_LEN]; //執行文件路徑
char g_szLogPath[MAX_PATH_LEN]; //日志文件路徑

CCriticalSection g_cLogCriticalSection;
volatile int g_nExistThread=0;
CWinThread*	g_hDbThrdProc=NULL;

int g_nConnectDbState=0; //0-未連接數據庫，1-連接成功，2-連接失敗
CADODatabase  g_pDbConnect;

CWebServiceParam g_WebServiceParam;
CCustomerField g_CustomerField;

bool g_bHTTPUnicodeDecode=false; //是否需要對HTTP返回的消息unicode解碼

//---------------------------------------------------------------------------

#endif
