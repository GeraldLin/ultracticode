//---------------------------------------------------------------------------
#ifndef __RMSGFIFO_H_
#define __RMSGFIFO_H_
//---------------------------------------------------------------------------
#include "flwrule.h"
//---------------------------------------------------------------------------
//
typedef struct
{
	US ClientId; //節點編號
  US MsgId; //指令編號
  CStringX MsgBuf;

}VXML_RECV_MSG_STRUCT;

class CMsgfifo
{
  unsigned short            Head;
  unsigned short            Tail;
  unsigned short 					  Len;
  unsigned short 					  resv;
  VXML_RECV_MSG_STRUCT      *pMsgs;

public:
	CMsgfifo(unsigned short len)
	{
		Head=0;
		Tail=0;
		resv=0;
		Len=len;
		pMsgs=new VXML_RECV_MSG_STRUCT[Len];
	}
	
	~CMsgfifo()
	{
		if (pMsgs != NULL)
    {
      delete []pMsgs;
      pMsgs = NULL;
    }
	}
	
	bool	IsFull()
	{
		unsigned short temp=Tail+1;
		return temp==Len?Head==0:temp==Head;
	}
	bool 	IsEmpty()
	{
		return Head==Tail;
	}
  VXML_RECV_MSG_STRUCT &GetOnHead()
  {return pMsgs[Head];}
  
  void DiscardHead()
  {	
  	if(++Head >= Len)
      Head=0;
  }
  unsigned short  Write(US ClientId, US MsgId, const char *MsgBuf);
  unsigned short  Read(US &ClientId, US &MsgId, char *MsgBuf);
  unsigned short  Read(VXML_RECV_MSG_STRUCT *RecvMsg);
};

//---------------------------------------------------------------------------
#endif
