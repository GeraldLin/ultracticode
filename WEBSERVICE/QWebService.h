// QWebService.h: interface for the CQWebService class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_QWEBSERVICE_H__3B1F779D_41ED_47B6_B7BB_CDF885CA02AA__INCLUDED_)
#define AFX_QWEBSERVICE_H__3B1F779D_41ED_47B6_B7BB_CDF885CA02AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_SESSION_NUM   1024 //最大會話資源個數

UINT WebServiceProc(LPVOID pParam);
UINT HTTPRequestProc(LPVOID pParam);

//與第三方WEBSERVICE接口配置參數
class CWebServiceParam
{
public:
  char WS_wsdladdress[256];
  char WS_soapaction[256];
  char WS_startenvelope[256];
  char WS_startbody[256];
  char WS_interface[256];
  short WS_paramcount;
  char WS_paramstring[32][256];
public:
  CWebServiceParam()
  {
    memset(WS_wsdladdress, 0, 256);
    memset(WS_soapaction, 0, 256);
    memset(WS_startenvelope, 0, 256);
    memset(WS_startbody, 0, 256);
    memset(WS_interface, 0, 256);
    WS_paramcount = 0;
    for (int i=0; i<32; i++)
    {
      memset(WS_paramstring[i], 0, 256);
    }
  }
};

//客戶資料字段參數定義
typedef struct
{
  char CF_FIELD_STARTTAG[128];
  char CF_FIELD_ENDTAG[128];
  char CF_FIELD_NAME[128];
  
}VXML_CUSTOM_FIELD_STRUCT;

class CCustomerField
{
public:
  bool bOpenFlag;
  bool bResultTag;
  char CF_RESULT_STARTTAG[128];
  char CF_RESULT_ENDTAG[128];
  char CF_RESULT_OK[128];
  char CF_RESULT_FAIL[128];

  bool bRecordNumTag;
  int  RECORDNUM;
  char CF_RECORDNUM_STARTTAG[128];
  char CF_RECORDNUM_ENDTAG[128];

  char INSERTSQL[2048];

  int  FIELDNUM;
  VXML_CUSTOM_FIELD_STRUCT *pFIELD;
public:
  CCustomerField()
  {
    bOpenFlag = false;
    bResultTag = false;
    memset(CF_RESULT_STARTTAG, 0, 128);
    memset(CF_RESULT_ENDTAG, 0, 128);
    memset(CF_RESULT_OK, 0, 128);
    memset(CF_RESULT_FAIL, 0, 128);
    
    bRecordNumTag = false;
    RECORDNUM = 1;
    memset(CF_RECORDNUM_STARTTAG, 0, 128);
    memset(CF_RECORDNUM_ENDTAG, 0, 128);
    
    memset(INSERTSQL, 0, 2048);

    FIELDNUM = 0;
    pFIELD = NULL;
  }
  virtual ~CCustomerField()
  {
    if (pFIELD)
    {
      delete [] pFIELD;
      pFIELD = NULL;
      FIELDNUM = 0;
    }
  }
  bool Init(int fieldnum)
  {
    if (fieldnum <= 0)
    {
      return false;
    }
    pFIELD = new VXML_CUSTOM_FIELD_STRUCT[fieldnum];
    if (pFIELD)
    {
      FIELDNUM = fieldnum;
      return true;
    }
    else
    {
      return false;
    }
  }
};

class CQWebServiceData
{
public:
  CString m_strWSDLAddress;
  CString m_strSoapAction;
  CString m_strStartEnvelopeString;
  CString m_strStartBody;
  CString m_strInterfaceString;
  int     m_nParamCount;
  CString m_strParamString;

  short   m_nHTTPType; //http方式：1-GET,2-POST 2016-06-25
  CString m_strHTTPurl;
  CString m_strHTTPheader;
  CString m_strHTTPcookies;

  CString m_strEXECCMDName;
  CString m_strEXECCMDParam;
  CString m_strEXECReadFile;
  DWORD   m_nEXECTimeOut;

  CString m_strCallerNo;
  CString m_strCalledNo;

  CString m_strReturnXmlString;
  CString m_strReturnCookiesString;
  CString m_strErrorMsg;
  volatile int m_nQueryResult; //查詢結果 0-沒有查詢任務 1-等待執行查詢 2-等待查詢結果 3-查詢失敗 4-查詢成功 5-取消查詢（2014-01-07，只有在多線程下才會發生） 6-延時等待ShellExecute結果
  bool    m_bSendQueryResult; //發送查詢結果標志：false-未發送 true-已發送
  int     m_nCurIndex; //當前記錄索引號

  short   m_nQueryType; //查詢類型：1-WEBSERVICE,2-HTTP,3-EXEC,4-查客戶資料,5-HTTPex
  short   m_nTestIndex; //測試查詢索引

public:
  void Init();
	CQWebServiceData();
	virtual ~CQWebServiceData();

};

class CQWebServiceSession
{
public:
  short state; //數據狀態 0: 無 1：有
  DWORD timer;
  
  unsigned long  m_ulSessionId; //會話序列號
  unsigned short m_usClientId; //客戶端編號
  unsigned long  m_ulCmdAddr; //指令地址
  unsigned short m_usDeviceNo; //接入設備號
  unsigned short m_usChnType; //通道類型
  unsigned short m_usChnNo; //通道號

  CCriticalSection m_csQuery;

  CQWebServiceData m_WebServiceData; //webservice接口數據

private:
  CWinThread*	m_hThrdProc;

public:
  void Init();
  CQWebServiceSession();
  virtual ~CQWebServiceSession();

  int StartWebThreadProc();
  int StartHTTPThreadProc();
  int StartEXECThreadProc();
};

class CQWebServiceSessionMng
{
public:
  short m_nSessionNum;
  CQWebServiceSession *m_pWebServiceSession;

public:
  void Init(short nSessionNum);
  CQWebServiceSessionMng();
  virtual ~CQWebServiceSessionMng();

  short GetIdelSession(unsigned long  SessionId);
  short GetSession(unsigned long  SessionId);
};

//------------------------------------------------------------------------------------
#endif // !defined(AFX_QWEBSERVICE_H__3B1F779D_41ED_47B6_B7BB_CDF885CA02AA__INCLUDED_)
