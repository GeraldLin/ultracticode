//---------------------------------------------------------------------------
/*
接收消息先進先出隊列
*/
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "rmsgfifo.h"

extern void MyTrace(int MsgType, LPCTSTR lpszFormat, ...);
//---------------------------------------------------------------------------
unsigned short CMsgfifo::Write(US ClientId, US MsgId, const char *MsgBuf)         
{
	  if(IsFull()) //full
			return 0;

    pMsgs[Tail].ClientId = ClientId;
    pMsgs[Tail].MsgId = MsgId;
    pMsgs[Tail].MsgBuf = MsgBuf;

	  if(++Tail >= Len)
         Tail=0;
	  return 1;
}

unsigned short CMsgfifo::Read(US &ClientId, US &MsgId, char *MsgBuf)
{  	
  if(IsEmpty())
	  return 0;//Buf Null
	
	ClientId = pMsgs[Head].ClientId;
	MsgId = pMsgs[Head].MsgId;
  pMsgs[Head].MsgBuf.CopyOutWithTerm(MsgBuf);

	DiscardHead();
	return 1;  
}
unsigned short CMsgfifo::Read(VXML_RECV_MSG_STRUCT *RecvMsg)
{
  if(IsEmpty())
	  return 0;//Buf Null
	
	RecvMsg->MsgId = pMsgs[Head].MsgId;
  RecvMsg->MsgBuf = pMsgs[Head].MsgBuf;

	DiscardHead();
	return 1;  
}
