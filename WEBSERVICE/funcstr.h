//---------------------------------------------------------------------------
#ifndef FuncstrH
#define FuncstrH
//---------------------------------------------------------------------------
//---------------才﹃ㄧ计--------------------------------------------------
//才﹃
US MyStrLen( const CH *vString );
//才﹃
CH *MySubString( const CH *vString, const US vStart, const US vLenght );
//奔才﹃ㄢ繷
CH *MyTrim( const CH *vString );
//奔才﹃オ
CH *MyLeftTrim( const CH *vString );
//奔才﹃娩
CH *MyRightTrim( const CH *vString );
//眖オ才﹃
CH *MyLeft( const CH *vString, const US vLenght );
//眖才﹃
CH *MyRight( const CH *vString, const US vLenght );
//才﹃
CH *MyStrAdd( const CH *vString1, const CH *vString2 );
//才﹃糶
CH *MyLower( const CH *vString );
//才﹃糶
CH *MyUpper( const CH *vString );
//才﹃竚
US MyStrPos( const CH *vString1, const CH *vString2 );
//ゑ耕才﹃
UC MyStrCmp( const CH *vString1, const CH *vString2 );
//┛菠糶ゑ耕才﹃
UC MyStrCmpNoCase( const CH *vString1, const CH *vString2 );
void ClearCharInCallerNo(char *phoneno);
int SplitTxtLine(const char *txtline, int maxnum, CStringX *arrstring);
//沮だ瞒才だ澄才﹃
int SplitString(const char *txtline, const char splitchar, int maxnum, CStringX *arrstring);
bool IsStrMatch(const CStringX &Rule,const CStringX &code);
//---------------------------------------------------------------------------
int CheckIPv4Address(const char *pszIPAddr);
void MyStringReplace(const char *pszSource, const char *pszOldStr, const char *pszNewStr, int nStartIndex, int nReplaceMaxNum, int nOutputMaxLen, char *pszResult);
bool GetArrayNameAndIndex(const char *pszSource, char *pszArrayName, int &nArrayIndex);

bool ParserFilter(const char *pszSource, char *pszFilterFieldName, char *pszFilterCondValue, int &nFilterType);
//---------------------------------------------------------------------------
#endif
